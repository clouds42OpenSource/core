﻿using System;

namespace Clouds42.Logger
{
    /// <summary>
    /// Абстракция над конкретной имплементацией логгера
    /// </summary>
    public interface ILogger42 
    {
        /// <summary>
        /// Логирование Debub
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Debug(string message);

        /// <summary>
        /// Логирование Debub с форматированием
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="args">Аргументы</param>
        void Debug(string message, params object[] args);

        /// <summary>
        /// Логирование Error
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Error(string message);

        /// <summary>
        /// Логирование Error
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="message">Сообщение</param>
        void Error(Exception exception, string message);

        /// <summary>
        /// Логирование Error с форматированием
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="args">Аргументы</param>
        void Error(string message, params object[] args);

        /// <summary>
        /// Логирование Warning
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Warn(string message);

        /// <summary>
        /// Логирование Warning
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Warn(Exception ex, string message);

        /// <summary>
        /// Логирование Warning с форматированием
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="args">Аргументы</param>

        void Warn(string message, params object[] args);

        /// <summary>
        /// Логирование Trace
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Trace(string message);

        /// <summary>
        /// Логирование Trace с форматированием
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="args">Аргументы</param>
        void Trace(string message, params object[] args);

        /// <summary>
        /// Логирование Info
        /// </summary>
        /// <param name="message">Сообщение</param>
        void Info(string message);

        /// <summary>
        /// Логирование Info с форматированием
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="args">Аргументы</param>
        void Info(string message, params object[] args);
    }
}
