﻿

using System;

namespace Clouds42.Logger
{
    public static class Logger42
    {
        private static ILogger42 _logger;

        public static void Init(IServiceProvider provider)
        {
            _logger = (ILogger42)provider.GetService(typeof(ILogger42));
        }

        public static ILogger42 GetLogger() => _logger;
    }
}
