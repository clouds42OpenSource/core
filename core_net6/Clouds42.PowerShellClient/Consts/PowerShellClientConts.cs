﻿namespace Clouds42.PowerShellClient.Consts
{
    /// <summary>
    /// Константы для клиента PowerShell
    /// </summary>
    public static class PowerShellClientConts
    {
        /// <summary>
        /// Выполнить комманду
        /// </summary>
        public const string InvokeCommand = "invoke-command";

        /// <summary>
        /// Название машины/сервера
        /// </summary>
        public const string ComputerName = nameof(ComputerName);

        /// <summary>
        /// Блок для выполнения скрипта
        /// </summary>
        public const string ScriptBlock = nameof(ScriptBlock);
    }
}
