﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Runtime.Serialization;

namespace Clouds42.PowerShellClient.Extensions
{
    /// <summary>
    /// Расширения для объекта PowerShell
    /// </summary>
    public static class PsObjectExtensions
    {
        /// <summary>
        /// Десериализовать результат работы скрипта PowerShell в требуемый тип данных
        /// </summary>
        /// <typeparam name="TOutModel">Тип выходного параметра</typeparam>
        /// <param name="psObjectProperties">Результат выполнения PowerShell скрипта</param>
        /// <returns>Требуемый тип данных</returns>
        public static TOutModel DeserializeFromPowerShell<TOutModel>(this PSMemberInfoCollection<PSPropertyInfo> psObjectProperties)
            where TOutModel : class
        {
            var result = Activator.CreateInstance<TOutModel>();

            var properties = typeof(TOutModel).GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(DataMemberAttribute)) && !string.IsNullOrEmpty(p.Name)).ToList();

            properties.ForEach(property =>
            {
                var dataMember =
                    ((DataMemberAttribute[])property.GetCustomAttributes(typeof(DataMemberAttribute), true))
                    .FirstOrDefault();

                if (dataMember == null || string.IsNullOrEmpty(dataMember.Name))
                    return;

                var value = GetValueFromPsObjectPropertiesByKey(dataMember.Name, psObjectProperties);
                property.SetValue(result, Convert.ChangeType(value, property.PropertyType));
            });

            return result;
        }

        /// <summary>
        /// Получить значение из списка свойств объекта PowerShell
        /// </summary>
        /// <param name="keyName">Ключ для поиска</param>
        /// <param name="psObjectProperties">Список свойств объекта PowerShell</param>
        /// <returns>Значение из списка свойств объекта PowerShell</returns>
        private static object GetValueFromPsObjectPropertiesByKey(string keyName, IEnumerable<PSPropertyInfo> psObjectProperties)
        {
            var dataRow =
                psObjectProperties.FirstOrDefault(item => item.Name.Equals(keyName, StringComparison.InvariantCultureIgnoreCase))
                ?? throw new InvalidOperationException($"Не удалось получить элемент данных по ключу {keyName}");

            return dataRow.Value;
        }
    }
}
