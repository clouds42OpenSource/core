﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using Clouds42.Common.Extensions;
using Clouds42.Logger;
using Clouds42.PowerShellClient.Consts;

namespace Clouds42.PowerShellClient
{
    /// <summary>
    /// Клиент для работы с PowerShell
    /// </summary>
    public class PowerShellClientWrapper(ILogger42 logger)
    {
        /// <summary>
        /// Выполнить скрипт
        /// </summary>
        /// <param name="powerShellScript">Скрипт PowerShell</param>
        /// <param name="serverName">Название сервера</param>
        /// <returns>Список PowerShell объектов в результате выполнения скрипта</returns>
        public List<PSObject> ExecuteScript(string powerShellScript, string serverName)
        {

            InitialSessionState runspaceConfiguration = InitialSessionState.CreateDefault2();

            using Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
            runspace.Open();
            using PowerShell powerShell = PowerShell.Create(runspace);
            try
            {
                logger.Info("Создали экземпляр PowerShell");
                powerShell.AddCommand(PowerShellClientConts.InvokeCommand)
                    .AddParameter(PowerShellClientConts.ComputerName, serverName)
                    .AddParameter(PowerShellClientConts.ScriptBlock, ScriptBlock.Create(powerShellScript));
                logger.Info($"Добавили команды PowerShell {powerShellScript} на сервере {serverName}");
                var result = powerShell.Invoke().ToList();
                logger.Info($"Получили результат PowerShell, ошибка {powerShell.HadErrors}");
                if (!powerShell.HadErrors)
                    return result;

                logger.Warn(GetErrorMessage(powerShell.Streams.Error));
                return result;
            }
            catch (Exception ex)
            {
                powerShell.Dispose();
                logger.Warn(
                    $"При выполнении скрипта PowerShell {powerShellScript} произошла ошибка. Причина: {ex.GetFullInfo()}. Ошибки в PowerShell {GetErrorMessage(powerShell.Streams.Error)}");
                throw;
            }
        }

        /// <summary>
        /// Получить сообщение об ошибке
        /// </summary>
        /// <param name="errorRecords">Список ошибок</param>
        /// <returns>Сообщение об ошибке</returns>
        private static string GetErrorMessage(IEnumerable<ErrorRecord> errorRecords)
            => $"Сообщение в потоке ошибок: {string.Join(".", errorRecords.Select(error => error.ToString()))}";

    }
}
