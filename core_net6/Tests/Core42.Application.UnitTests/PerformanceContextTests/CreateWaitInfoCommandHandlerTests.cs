﻿using System.Linq.Expressions;
using AutoMapper;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Handlers;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.UnitTests.PerformanceContextTests.Helpers;
using FluentAssertions;
using Moq;

namespace Core42.Application.UnitTests.PerformanceContextTests;

[TestFixture]
public class CreateWaitInfoCommandHandlerTests
{
    private Mock<IUnitOfWork> _mockUnitOfWork;
    private Mock<ILogger42> _mockLogger;
    private Mock<IMapper> _mockMapper;
    private CreateWaitInfoCommandHandler _handler;
    
    private const string TestWaitType = "TestType";
    private const string ExistingWaitInfoMessage = $"Тип ожидания {TestWaitType} уже существует";
    private const string CreationProblemMessage = "Проблема с созданием данных об ожидании";

   [SetUp]
        public void SetUp()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger42>();
            _mockMapper = new Mock<IMapper>();
            _handler = new CreateWaitInfoCommandHandler(_mockUnitOfWork.Object, _mockLogger.Object, _mockMapper.Object);
        }

        [Test]
        public async Task Handle_WhenWaitInfoAlreadyExists_ShouldReturnPreconditionFailed()
        {
            // Arrange
            var request = new CreateWaitInfoCommand { WaitInfo = new WaitDto { WaitType = TestWaitType } };
    
            // Создаем список с одним элементом WaitInfo, чтобы Any вернул true
            var data = new List<WaitInfo>
            {
                new WaitInfo { WaitType = TestWaitType }
            }.AsQueryable();
    
            // Используем TestDbAsyncHelper для создания мока DbSet
            var mockSet = TestDbAsyncHelper.CreateMockDbSet(data);
    
            // Настраиваем _mockUnitOfWork для использования нашего мока
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryable()).Returns(mockSet.Object);

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.Message.Should().Contain(ExistingWaitInfoMessage);
            _mockLogger.Verify(l => l.Info(It.Is<string>(s => s.Contains(ExistingWaitInfoMessage))), Times.Once);
    
            _mockUnitOfWork.Verify(u => u.WaitsInfoRepository.Insert(It.IsAny<WaitInfo>()), Times.Never);
            _mockUnitOfWork.Verify(u => u.SaveAsync(), Times.Never);
        }

        [Test]
        public async Task Handle_WhenNewWaitInfoIsCreated_ShouldReturnOkResult()
        {
            // Arrange
            var request = new CreateWaitInfoCommand { WaitInfo = new WaitDto { WaitType = TestWaitType } };
    
            // Создаем пустой список WaitInfo
            var data = new List<WaitInfo>().AsQueryable();
    
            // Используем TestDbAsyncHelper для создания мока DbSet
            var mockSet = TestDbAsyncHelper.CreateMockDbSet(data);
    
            // Настраиваем _mockUnitOfWork для использования нашего мока
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryable()).Returns(mockSet.Object);
    
            _mockMapper.Setup(m => m.Map<WaitInfo>(It.IsAny<WaitDto>())).Returns(new WaitInfo { WaitType = TestWaitType });
    
            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.Code.Should().Be(200); 
            result.Result.WaitType.Should().Be(TestWaitType);
            _mockUnitOfWork.Verify(u => u.WaitsInfoRepository.Insert(It.IsAny<WaitInfo>()), Times.Once);
            _mockUnitOfWork.Verify(u => u.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task Handle_WhenExceptionIsThrown_ShouldReturnPreconditionFailed()
        {
            // Arrange
            var request = new CreateWaitInfoCommand { WaitInfo = new WaitDto { WaitType = TestWaitType } };
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.Any(It.IsAny<Expression<Func<WaitInfo, bool>>>())).Throws(new Exception("Test Exception"));

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            result.Should().NotBeNull();
            result.Message.Should().Contain(CreationProblemMessage);
            _mockLogger.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
}

