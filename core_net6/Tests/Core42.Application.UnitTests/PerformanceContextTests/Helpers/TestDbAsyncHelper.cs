﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Moq;

namespace Core42.Application.UnitTests.PerformanceContextTests.Helpers;

// Вспомогательный класс для создания моков DbSet для асинхронного тестирования
public static class TestDbAsyncHelper
{
    public static Mock<DbSet<T>> CreateMockDbSet<T>(IQueryable<T> data) where T : class
    {
        var mockSet = new Mock<DbSet<T>>();
        mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(new TestDbAsyncQueryProvider<T>(data.Provider));
        mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
        mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
        mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator);
        mockSet.As<IAsyncEnumerable<T>>().Setup(m => m.GetAsyncEnumerator(It.IsAny<CancellationToken>()))
            .Returns(new TestDbAsyncEnumerator<T>(data.GetEnumerator()));

        return mockSet;

    }

    // Внутренний класс для асинхронного провайдера запросов
    private class TestDbAsyncQueryProvider<TEntity> : IAsyncQueryProvider
    {
        private readonly IQueryProvider _inner;

        internal TestDbAsyncQueryProvider(IQueryProvider inner)
        {
            _inner = inner;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new TestDbAsyncEnumerable<TEntity>(expression);
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new TestDbAsyncEnumerable<TElement>(expression);
        }

        public object Execute(Expression expression)
        {
            return _inner.Execute(expression) ?? default;
        }

        public TResult Execute<TResult>(Expression expression)
        {
            // Проверяем, является ли выражение вызовом метода
            if (expression is not MethodCallExpression methodCallExpression)
            {
                return _inner.Execute<TResult>(expression);
            }

            // Извлекаем источник данных
            var source = Execute<IEnumerable<TEntity>>(methodCallExpression.Arguments[0]) ?? [];

            // Проверяем имя метода
            switch (methodCallExpression.Method.Name)
            {
                case nameof(Queryable.FirstOrDefault):
                    switch (methodCallExpression.Arguments.Count)
                    {
                        // Если метод FirstOrDefault без предиката
                        case 1:
                            return (TResult?)(object?)(source.FirstOrDefault() ?? default(TEntity));
                        // Если метод FirstOrDefault с предикатом
                        case > 1 when
                            methodCallExpression.Arguments[1] is UnaryExpression unaryExpression:
                        {
                            var predicate = (Expression<Func<TEntity, bool>>)unaryExpression.Operand;
                            var compiledPredicate = predicate.Compile();
                            return (TResult?)(object?)(source.FirstOrDefault(compiledPredicate) ?? default(TEntity));
                        }
                    }

                    break;

                case nameof(Queryable.Any):
                    switch (methodCallExpression.Arguments.Count)
                    {
                        // Если метод Any без предиката
                        case 1:
                            return (TResult)(object)source.Any();
                        // Если метод Any с предикатом
                        case > 1 when
                            methodCallExpression.Arguments[1] is UnaryExpression unaryExpressionForAny:
                        {
                            var predicate = (Expression<Func<TEntity, bool>>)unaryExpressionForAny.Operand;
                            var compiledPredicate = predicate.Compile();
                            return (TResult)(object)source.Any(compiledPredicate);
                        }
                    }

                    break;

                default:
                    // Не поддерживаемый метод
                    throw new NotSupportedException($"Method '{methodCallExpression.Method.Name}' is not supported.");
            }

            // Если метод не обрабатывается, используем внутренний провайдер
            return _inner.Execute<TResult>(expression);
        }



        public TResult ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken = default)
        {
            // Выполняем запрос через синхронный Execute
            var result = Execute(expression);

            // Если TResult является Task<T>, оборачиваем результат в Task
            if (typeof(TResult).IsGenericType && typeof(TResult).GetGenericTypeDefinition() == typeof(Task<>))
            {
                var innerType = typeof(TResult).GetGenericArguments()[0];

                // Создаём Task вручную с корректным типом
                var taskResult = typeof(Task)
                    .GetMethod(nameof(Task.FromResult))
                    .MakeGenericMethod(innerType)
                    .Invoke(null, new[] { result });

                return (TResult)taskResult;
            }

            // Если TResult == Task, возвращаем CompletedTask
            if (typeof(TResult) == typeof(Task))
            {
                return (TResult)(object)Task.CompletedTask;
            }

            // Возвращаем Task с результатом
            return (TResult)(object)Task.FromResult(result);
        }

    }

    // Внутренний класс для асинхронного перечисления
    public class TestDbAsyncEnumerable<T> : EnumerableQuery<T>, IAsyncEnumerable<T>, IQueryable<T>
    {
        public TestDbAsyncEnumerable(IEnumerable<T> enumerable) : base(enumerable) { }

        public TestDbAsyncEnumerable(Expression expression) : base(expression) { }

        public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = default)
        {
            return new TestDbAsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
        }

        IQueryProvider IQueryable.Provider => new TestDbAsyncQueryProvider<T>(this);
    }

    // Внутренний класс для асинхронного перечислителя
    public class TestDbAsyncEnumerator<T>(IEnumerator<T> inner) : IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T> _inner = inner ?? throw new ArgumentNullException(nameof(inner));

        public ValueTask DisposeAsync()
        {
            _inner.Dispose();
            return ValueTask.CompletedTask;
        }

        public ValueTask<bool> MoveNextAsync()
        {
            return ValueTask.FromResult(_inner.MoveNext());
        }

        public T Current => _inner.Current;
    }
}
