﻿using AutoMapper;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Handlers;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.Features.PerformanceContext.Queries;
using Core42.Application.Features.PerformanceContext.Queries.Filters;
using Core42.Application.UnitTests.PerformanceContextTests.Helpers;
using FluentAssertions;
using Moq;

namespace Core42.Application.UnitTests.PerformanceContextTests;

[TestFixture]
    public class GetWaitsQueryHandlerTests
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<ILogger42> _mockLogger;
        private Mock<IMapper> _mockMapper;
        private GetWaitsQueryHandler _handler;

        private const string ErrorMessage = "Не удалось получить информацию об ожиданиях";

        [SetUp]
        public void SetUp()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger42>();
            _mockMapper = new Mock<IMapper>();
            _handler = new GetWaitsQueryHandler(_mockUnitOfWork.Object, _mockLogger.Object, _mockMapper.Object);
        }

        [Test]
        public async Task Handle_WhenWaitsExist_ShouldReturnOkResult()
        {
            var waitTypes = new [] { "Type1", "Type2" };
            var request = new GetWaitsQuery { Filter = new WaitInfoFilter { WaitTypes = waitTypes } };
            var waitInfos = new List<WaitInfo>
            {
                new() { WaitType = "Type1" },
                new() { WaitType = "Type2" }
            }.AsQueryable();

            var waitDtos = new List<WaitDto>
            {
                new() { WaitType = "Type1" },
                new() { WaitType = "Type2" }
            };

            var mockDbSet = TestDbAsyncHelper.CreateMockDbSet(waitInfos);

            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryableNoTracking())
                .Returns(mockDbSet.Object);
            _mockMapper.Setup(m => m.Map<List<WaitDto>>(It.IsAny<List<WaitInfo>>()))
                .Returns(waitDtos);

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Code.Should().Be(200);
            result.Result.Should().BeEquivalentTo(waitDtos);
        }
        
        [Test]
        public async Task Handle_WhenNoWaitsExist_ShouldReturnEmptyList()
        {
            var waitTypes = new [] { "Type1", "Type2" };
            var request = new GetWaitsQuery { Filter = new WaitInfoFilter { WaitTypes = waitTypes } };
            var waitInfos = new List<WaitInfo>().AsQueryable();

            var mockDbSet = TestDbAsyncHelper.CreateMockDbSet(waitInfos);
            var waitDtos = new List<WaitDto>();

            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryableNoTracking())
                .Returns(mockDbSet.Object);
            _mockMapper.Setup(m => m.Map<List<WaitDto>>(It.IsAny<List<WaitInfo>>()))
                .Returns(waitDtos);

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Code.Should().Be(200); 
            result.Result.Should().BeEmpty();
        }

        [Test]
        public async Task Handle_WhenExceptionIsThrown_ShouldReturnPreconditionFailed()
        {
            var waitTypes = new [] { "Type1", "Type2" };
            var request = new GetWaitsQuery { Filter = new WaitInfoFilter { WaitTypes = waitTypes } };

            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryableNoTracking())
                           .Throws(new Exception("Test Exception"));

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Message.Should().Contain(ErrorMessage);
            _mockLogger.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
    }


