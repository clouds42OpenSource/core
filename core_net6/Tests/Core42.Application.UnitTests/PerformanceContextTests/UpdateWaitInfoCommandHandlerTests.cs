﻿using AutoMapper;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Handlers;
using Core42.Application.Features.PerformanceContext.Models;
using Core42.Application.UnitTests.PerformanceContextTests.Helpers;
using FluentAssertions;
using Moq;

namespace Core42.Application.UnitTests.PerformanceContextTests;

[TestFixture]
    public class UpdateWaitInfoCommandHandlerTests
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<ILogger42> _mockLogger;
        private Mock<IMapper> _mockMapper;
        private UpdateWaitInfoCommandHandler _handler;
        
        private const string TestWaitType = "TestType";
        private const string UpdateExceptionMessage = "Проблема с изменением данных об ожидании";

        [SetUp]
        public void SetUp()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger42>();
            _mockMapper = new Mock<IMapper>();
            _handler = new UpdateWaitInfoCommandHandler(_mockUnitOfWork.Object, _mockLogger.Object, _mockMapper.Object);
        }

        [Test]
        public async Task Handle_WhenWaitInfoDoesNotExist_ShouldReturnPreconditionFailed()
        {
            var request = new UpdateWaitInfoCommand { Type = TestWaitType, WaitInfo = new WaitDto { WaitType = TestWaitType } };
            var waitInfos = new List<WaitInfo>().AsQueryable();
            var mockDbSet = TestDbAsyncHelper.CreateMockDbSet(waitInfos);

            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryable()).Returns(mockDbSet.Object);

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Message.Should().Contain($"Тип ожидания {TestWaitType} не существует");
            _mockLogger.Verify(l => l.Info(It.Is<string>(s => s.Contains($"Тип ожидания {TestWaitType} не существует"))), Times.Once);
            _mockUnitOfWork.Verify(u => u.WaitsInfoRepository.Update(It.IsAny<WaitInfo>()), Times.Never);
            _mockUnitOfWork.Verify(u => u.SaveAsync(), Times.Never);
        }

        [Test]
        public async Task Handle_WhenWaitInfoIsUpdated_ShouldReturnOkResult()
        {
            var request = new UpdateWaitInfoCommand { WaitInfo = new WaitDto { WaitType = TestWaitType } };
            var waitInfo = new WaitInfo { WaitType = TestWaitType };
            var waitInfos = new List<WaitInfo> { waitInfo }.AsQueryable();
            var mockDbSet = TestDbAsyncHelper.CreateMockDbSet(waitInfos);

            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryable()).Returns(mockDbSet.Object);
            _mockMapper.Setup(m => m.Map(It.IsAny<WaitDto>(), It.IsAny<WaitInfo>()))
                       .Callback<WaitDto, WaitInfo>((src, dest) => dest.WaitType = src.WaitType);

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Code.Should().Be(200);
            result.Result.WaitType.Should().Be(TestWaitType);
            _mockUnitOfWork.Verify(u => u.WaitsInfoRepository.Update(It.IsAny<WaitInfo>()), Times.Once);
            _mockUnitOfWork.Verify(u => u.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task Handle_WhenExceptionIsThrown_ShouldReturnPreconditionFailed()
        {
            var request = new UpdateWaitInfoCommand { WaitInfo = new WaitDto { WaitType = TestWaitType } };
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.AsQueryable())
                           .Throws(new Exception("Test Exception"));

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Message.Should().Contain(UpdateExceptionMessage);
            _mockLogger.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
    }
