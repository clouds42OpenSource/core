﻿using System.Linq.Expressions;
using AutoMapper;
using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Handlers;
using Core42.Application.Features.PerformanceContext.Models;
using FluentAssertions;
using Moq;

namespace Core42.Application.UnitTests.PerformanceContextTests;

[TestFixture]
    public class DeleteWaitInfoCommandHandlerTests
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<ILogger42> _mockLogger;
        private Mock<IMapper> _mockMapper;
        private DeleteWaitInfoCommandHandler _handler;

        private const string TestWaitType = "TestType";
        private const string NonExistentWaitInfoMessage = $"Тип ожидания {TestWaitType} не существуе";
        private const string DeletionProblemMessage = "Проблема с удалением данных об ожидании";

        [SetUp]
        public void SetUp()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockLogger = new Mock<ILogger42>();
            _mockMapper = new Mock<IMapper>();
            _handler = new DeleteWaitInfoCommandHandler(_mockUnitOfWork.Object, _mockLogger.Object, _mockMapper.Object);
        }

        [Test]
        public async Task Handle_WhenWaitInfoDoesNotExist_ShouldReturnPreconditionFailed()
        {
            var request = new DeleteWaitInfoCommand { WaitType = TestWaitType };
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.FirstOrDefaultAsync(It.IsAny<Expression<Func<WaitInfo, bool>>>()))
                           .ReturnsAsync((WaitInfo)null);

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Message.Should().Contain(NonExistentWaitInfoMessage);
            _mockLogger.Verify(l => l.Info(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public async Task Handle_WhenWaitInfoExists_ShouldReturnOkResult()
        {
            var request = new DeleteWaitInfoCommand { WaitType = TestWaitType };
            var waitInfo = new WaitInfo { WaitType = TestWaitType };
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.FirstOrDefaultAsync(It.IsAny<Expression<Func<WaitInfo, bool>>>()))
                           .ReturnsAsync(waitInfo);
            _mockMapper.Setup(m => m.Map<WaitDto>(It.IsAny<WaitInfo>())).Returns(new WaitDto { WaitType = TestWaitType });

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Code.Should().Be(200);
            result.Result.WaitType.Should().Be(TestWaitType);
            _mockUnitOfWork.Verify(u => u.WaitsInfoRepository.Delete(It.IsAny<WaitInfo>()), Times.Once);
            _mockUnitOfWork.Verify(u => u.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task Handle_WhenExceptionIsThrown_ShouldReturnPreconditionFailed()
        {
            var request = new DeleteWaitInfoCommand { WaitType = TestWaitType };
            _mockUnitOfWork.Setup(u => u.WaitsInfoRepository.FirstOrDefaultAsync(It.IsAny<Expression<Func<WaitInfo, bool>>>()))
                           .ThrowsAsync(new Exception("Test Exception"));

            var result = await _handler.Handle(request, CancellationToken.None);

            result.Should().NotBeNull();
            result.Message.Should().Contain(DeletionProblemMessage);
            _mockLogger.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()), Times.Once);
        }
    }
