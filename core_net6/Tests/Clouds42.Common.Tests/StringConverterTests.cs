﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.BLL.Common.Helpers;

namespace Clouds42.Common.UnitTests
{
    /// <summary>
    /// Тесты на конвертацию числа в строковый вид
    /// </summary>
    [TestClass]
    public class StringConverterTests
    {
        /// <summary>
        /// Тест на корректную конвертацию числа в строковый вид
        /// </summary>
        /// <param name="amount">Сумма средств</param>
        /// <param name="localeCurrency">Локальная Валюта</param>
        /// <param name="expected">Ожидаемое значение</param>
        [DataTestMethod]
        [DataRow(5823.5, "руб", "Пять тысяч восемьсот двадцать три рубля 50 копеек")]
        [DataRow(6020.301, "тг", "Шесть тысяч двадцать  тенге 30 тиын")]
        [DataRow(268.11, "грн", "Двісті шістдесят вісім гривень 11 копійок")]
        [DataRow(0, "руб", "Ноль рублей 00 копеек")]
        [DataRow(+6020.301, "тг", "Шесть тысяч двадцать  тенге 30 тиын")]
        public void CheckConvertionWithCorrectSum(double amount, string localeCurrency,string expected)
        {
            Assert.AreEqual(StringConverter.CurrencyToTxt((decimal)amount, localeCurrency), expected);
        }
            
        /// <summary>
        /// Тест на неверный тип валюты
        /// </summary>
        /// <param name="amount">Сумма средств</param>
        /// <param name="localeCurrency">Локальная валюта</param>
        [DataTestMethod]
        [DataRow(6020.301, "сом")]
        [DataRow(6020.301, "юань")]
        [DataRow(6020.301, "сум")]
        [DataRow(6020.301, "евро")]
        [DataRow(6020.301, "доллар")]
        [DataRow(30.5, "рупия")]
        [DataRow(625, "крона")]
        public void CheckConvertionWithInvalidCurrencyType(double amount, string localeCurrency)
        {
            try
            {
                var result = StringConverter.CurrencyToTxt((decimal)amount, localeCurrency);
                Assert.IsNull(result);
            }
            catch (Exception e)
            {
                Assert.IsNotNull(e);

            }
        }

        /// <summary>
        /// Проверка конвертации с невалидными данными
        /// </summary>
        /// <param name="amount">Сумма средств</param>
        /// <param name="localeCurrency">Локальная валюта</param>
        [DataTestMethod]
        [DataRow("-125.52", "тг")]
        [DataRow(null, "тг")]
        [DataRow("12.r0", "руб")]
        [DataRow("", "")]
        [DataRow("23", null)]
        [DataRow("12.r0", "руб")]
        [DataRow("0", "руб")]
        public void CheckConvertionWithInvalidSum(string amount, string localeCurrency)
        { 
            try
            {
                var result = StringConverter.CurrencyToTxt(Decimal.Parse(amount), localeCurrency);
                Assert.IsNull(result);
            }
            catch (Exception e)
            {
                Assert.IsNotNull(e);
            }
        }

    }
}
