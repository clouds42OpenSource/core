﻿using Clouds42.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests.Helpers
{
    /// <summary>
    /// Тест для проверки функции сравнения версий
    /// </summary>
    [TestClass]
    public class BillingServiceVersionComparerTests
    {
        /// <summary>
        /// Если старая версия равна новой - то false
        /// </summary>
        /// <param name="oldVersion"> Старая версия </param>
        /// <param name="newVersion"> Новая версия </param>
        [DataTestMethod]
        [DataRow("1.1.11.11", "1.1.11.11")]
        [DataRow("1.1.11", "1.1.11")]
        [DataRow("235", "235")]
        public void CompareOldSameNewTest(string oldVersion, string newVersion)
        {
            var result = BillingServiceVersionComparer.Compare(oldVersion, newVersion);
            Assert.IsFalse(result, $"Старая версия {oldVersion} равна новой {newVersion} версии");
        }

        /// <summary>
        /// Если старая версия больше новой - то false
        /// </summary>
        /// <param name="oldVersion"> Старая версия </param>
        /// <param name="newVersion"> Новая версия </param>
        [DataTestMethod]
        [DataRow("1.1.11.22", "1.1.11.11")]
        [DataRow("1.1.22", "1.1.11.11")]
        [DataRow("432", "235")]
        public void CompareOldSmallerNewTest(string oldVersion, string newVersion)
        {
            var result = BillingServiceVersionComparer.Compare(oldVersion, newVersion);
            Assert.IsFalse(result, $"Старая версия {oldVersion} больше новой {newVersion} версии");
        }

        /// <summary>
        /// Если старая версия меньше нвоой - то true
        /// </summary>
        /// <param name="oldVersion"> Старая версия </param>
        /// <param name="newVersion"> Новая версия </param>
        [DataTestMethod]
        [DataRow("1.1.11.11", "1.1.11.22")]
        [DataRow("1.1.11.11", "1.1.22")]
        [DataRow("235", "432")]
        public void CompareOldBiggerNewTest(string oldVersion, string newVersion)
        {
            var result = BillingServiceVersionComparer.Compare(oldVersion, newVersion);
            Assert.IsTrue(result, $"Старая версия {oldVersion} меньше новой {newVersion} версии");
        }

        /// <summary>
        /// Проверка с раздичными не валидными комбинациями символов - вернёт false
        /// </summary>
        /// <param name="oldVersion"> Старая версия </param>
        /// <param name="newVersion"> Новая версия </param>
        [DataTestMethod]
        [DataRow("1,1,11,22", "1,1,11,11")]
        [DataRow("1.1.11.22", "1,1,11,11")]
        [DataRow("1,1,11,22", "1.1.11.11")]
        [DataRow("1.1.11.22", "1.1.11.as")]
        [DataRow("1.1.11.as", "1.1.11.11")]
        [DataRow("1.1.11.**", "1.1.11.11")]
        [DataRow("1.1.11.22", "1.1.11.**")]
        [DataRow("textData", "textData")]
        [DataRow("text Data", "text Data")]
        [DataRow("text Data", "textData")]
        [DataRow("textData", "text Data")]
        [DataRow("textString", "textData")]
        [DataRow("text String", "text Data")]
        [DataRow("text String", "textData")]
        [DataRow("textString", "text Data")]
        [DataRow("textData", "text Data")]
        [DataRow("11111", "11111")]
        [DataRow("1111 111", "1111 111")]
        [DataRow("1111 111", "1111111")]
        [DataRow("1111111", "1111 111")]
        [DataRow("1111_111", "1111_111")]
        [DataRow("символыКирилицы", "символыКирилицы")]
        [DataRow("символы Кирилицы", "символы Кирилицы")]
        [DataRow("символы Кирилицы", "символыКирилицы")]
        [DataRow("символыКирилицы", "символы Кирилицы")]
        [DataRow("latinAndКирилица", "latinAndКирилица")]
        [DataRow("latin And Кирилица", "latin And Кирилица")]
        [DataRow("latin And Кирилица", "latinAndКирилица")]
        [DataRow("latinAndКирилица", "latin And Кирилица")]
        [DataRow("+-/+_(){}[]*&^%$#@!''\"?><,.`~", "+-/+_(){}[]*&^%$#@!''\"?><,.`~")]
        public void CompareBrokenDataTest(string oldVersion, string newVersion)
        {
            var result = BillingServiceVersionComparer.Compare(oldVersion, newVersion);
            Assert.IsFalse(result, "Входящие параметры не верны");
        }
    }
}