﻿using System;
using Clouds42.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests
{
    /// <summary>
    /// Тесты на парсинг значений
    /// </summary>
    [TestClass]
    public class ParsingTypeTests
    {
        /// <summary>
        /// Тест на парсинг decimal значения из строки
        /// </summary>
        [TestMethod]
        public void DecimalParserFromStringTest()
        {
            #region 1. Строковое значение Decimal с точкой

            Assert.AreEqual(DecimalPareserHelper.Parse("1.00"), (decimal)1.00);
            Assert.AreEqual(DecimalPareserHelper.Parse("1.18"), (decimal)1.18);
            Assert.AreEqual(DecimalPareserHelper.Parse("1.123549"), (decimal)1.123549);
            Assert.AreEqual(DecimalPareserHelper.Parse("1.99"), (decimal)1.99);

            #endregion

            #region 2. Строковое значение Decimal с запятой

            Assert.AreEqual(DecimalPareserHelper.Parse("1,00"), (decimal)1.00);
            Assert.AreEqual(DecimalPareserHelper.Parse("1,18"), (decimal)1.18);
            Assert.AreEqual(DecimalPareserHelper.Parse("1,123549"), (decimal)1.123549);
            Assert.AreEqual(DecimalPareserHelper.Parse("1,99"), (decimal)1.99);

            #endregion

            #region 3. Неверные строковые значения представления Decimal

            try
            {
                DecimalPareserHelper.Parse("1.9s9");
            }
            catch (FormatException)
            {
            }

            #endregion
        }
    }
}
