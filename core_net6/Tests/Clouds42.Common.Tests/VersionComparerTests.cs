﻿using System;
using Clouds42.Common.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests
{
    /// <summary>
    /// Тесты на реализацию интерфейса компарера версий
    /// </summary>
    [TestClass]
    public class VersionComparerTests
    {
        private readonly VersionComparer _versionComparer = new();
        /// <summary>
        /// Проверить сравнение с корректными значениями версий
        /// </summary>
        /// <param name="ver1">Версия 1</param>
        /// <param name="ver2">Версия 2</param>
        /// <param name="expectedComparisonResult">Ожидаемый результат сравнения</param>
        [DataTestMethod]
        [DataRow("127", "153", -1)]
        [DataRow("1367", "256", 1)]
        [DataRow("455", "455", 0)]
        [DataRow("23.4", "12.5", 1)]
        [DataRow("156.4", "156.5", -1)]
        [DataRow("-169", "1", -1)]
        [DataRow("34", "-5", 1)]
        [DataRow("0", "0", 0)]
        [DataRow("1.0.2321", "5.6.8", -1)]
        [DataRow("8.8.8", "8.8.9", -1)]
        [DataRow("10.23.123.153", "10.23.123.152", 1)]
        [DataRow("10.12.1356", "10.12.1356", 0)]
        public void CheckComparisonWithCorrectValues(string ver1, string ver2,int expectedComparisonResult) 
        {
            Assert.AreEqual(_versionComparer.Compare(ver1,ver2), expectedComparisonResult);
        }   

        /// <summary>
        /// Проверить сравнение с null значениями версий
        /// </summary>
        /// <param name="ver1">Версия 1</param>
        /// <param name="ver2">Версия 2</param>
        [DataTestMethod]
        [DataRow(null, "125")]
        [DataRow("345", null)]
        public void CheckComparisonWithNullValues(string ver1, string ver2)
        {
            try
            {
                var result = _versionComparer.Compare(ver1, ver2);
                Assert.IsNull(result);
            }
            catch (Exception e)
            {
                Assert.IsNotNull(e);
            }
        }

        /// <summary>
        /// Проверить сранение с некорректными значениями версий
        /// </summary>
        /// <param name="ver1">Версия 1</param>
        /// <param name="ver2">Версия 2</param>
        /// <param name="expectedComparisonResult">Ожидаемый результат сравнения</param>
        [DataTestMethod]
        [DataRow("asdasd", "15", 0)]
        [DataRow("156", "www5", 0)]
        [DataRow("2r", "46", 0)]
        [DataRow("12,1", "35", 0)]
        [DataRow("1.0.2321.35325", "5.6.7", 0)]
        [DataRow("1.0", "8.12.1335.12", 0)]
        public void CheckComparisonWithInvalidValues(string ver1, string ver2,int expectedComparisonResult)
        {
             Assert.AreEqual( _versionComparer.Compare(ver1, ver2),expectedComparisonResult);
        }
    }
}