﻿using Clouds42.Common.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests
{
    /// <summary>
    /// Тесты на проверку класса расширений FullNameExtension 
    /// </summary>
    [TestClass]
    public class FullNameExtensionTests
    {
        /// <summary>
        /// Проверить метод GetShortName с различными входными данными
        /// </summary>
        [DataTestMethod]
        [DataRow("Иванов Иван Петрович", "Иванов И.П.")]
        [DataRow("Сергеев Вадим Олегович", "Сергеев В.О.")]
        [DataRow("Пупкин Исмаил Дмитриевич", "Пупкин И.Д.")]
        [DataRow("Королев Дмитрий Иванович", "Королев Д.И.")]
        [DataRow("Потёмкин Григорий Александрович", "Потёмкин Г.А.")]
        [DataRow("Крымский Василий Михайлович", "Крымский В.М.")]
        [DataRow("Sergeev Anatoly Viktorovich", "Sergeev Anatoly Viktorovich")]
        [DataRow("", "")]
        [DataRow(null, "")]
        [DataRow("Иванов Сергей", "Иванов С.")]
        public void CheckGetShortNameMethodWithDifferentData(string fullName, string expectedShortName)
        {
            var shortName = fullName.GetShortName();
            Assert.AreEqual(expectedShortName, shortName,
                $"Не верное преобразование. Ожидали: {expectedShortName}, получили: {shortName}");
        }
    }
}