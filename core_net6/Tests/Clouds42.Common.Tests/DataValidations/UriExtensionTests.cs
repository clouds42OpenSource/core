﻿using System;
using Clouds42.Common.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests.DataValidations
{
    /// <summary>
    /// Тесты для проверки расширения для URI
    /// </summary>
    [TestClass]
    public class UriExtensionTests
    {
        /// <summary>
        /// Проверить получение информации с корректным адресом
        /// </summary>
        /// <param name="url">Адрес</param>
        /// <param name="expectedHostName">Ожидаемое название хоста</param>
        /// <param name="expectedRegistrableDomain">Ожидаемый регистрируемый домен</param>
        [DataTestMethod]
        [DataRow("https://testingcp.42clouds.com", "testingcp.42clouds.com", "42clouds.com")]
        [DataRow("https://testingcp.42clouds.pro", "testingcp.42clouds.pro", "42clouds.pro")]
        [DataRow("https://testingcp.42clouds.kz", "testingcp.42clouds.kz", "42clouds.kz")]
        [DataRow("https://betacp.42clouds.com", "betacp.42clouds.com", "42clouds.com")]
        [DataRow("https://betacp.42clouds.pro", "betacp.42clouds.pro", "42clouds.pro")]
        [DataRow("https://betacp.42clouds.kz", "betacp.42clouds.kz", "42clouds.kz")]
        [DataRow("https://cp.42clouds.com", "cp.42clouds.com", "42clouds.com")]
        [DataRow("https://cp.42clouds.pro", "cp.42clouds.pro", "42clouds.pro")]
        [DataRow("https://cp.42clouds.kz", "cp.42clouds.kz", "42clouds.kz")]
        [DataRow("https://42clouds.kz", "42clouds.kz", "42clouds.kz")]
        [DataRow("https://42clouds.ru", "42clouds.ru", "42clouds.ru")]
        [DataRow("https://42clouds.pro", "42clouds.pro", "42clouds.pro")]
        [DataRow("https://devprovider.42clouds.com", "devprovider.42clouds.com", "42clouds.com")]
        [DataRow("https://betaprovider.42clouds.com", "betaprovider.42clouds.com", "42clouds.com")]
        [DataRow("https://provider.42clouds.com", "provider.42clouds.com", "42clouds.com")]
        [DataRow("https://localhost", "localhost", "localhost")]
        [DataRow("https://localhost:8080", "localhost", "localhost")]
        [DataRow("https://cp.42clouds.kz:413", "cp.42clouds.kz", "42clouds.kz")]
        public void CheckDataWithCorrectUri(string url, string expectedHostName, string expectedRegistrableDomain)
        {
            var domainInfo = new Uri(url).GetDomainInfo();
            Assert.AreEqual(expectedHostName, domainInfo.HostName, "Не верно определено название хоста");
            Assert.AreEqual(expectedRegistrableDomain, domainInfo.RegistrableDomain, "Не верно определен регистрируемый домен");
        }

        /// <summary>
        /// Проверить получение информации с некорректным адресом
        /// </summary>
        /// <param name="url">Адрес</param>
        [DataTestMethod]
        [DataRow("")]
        [DataRow("bla-bla-bla")]
        [DataRow("1231241245213")]
        [DataRow("Casdfc1234125r")]
        public void CheckDataWithInvalidUrl(string url)
        {
            try
            {
                var domainInfo = new Uri(url).GetDomainInfo();
                Assert.IsNull(domainInfo);
            }
            catch (Exception ex)
            {
                Assert.IsNotNull(ex);
            }
        }

        /// <summary>
        /// Проверить получение информации с дополнительными параметрами в адресе
        /// </summary>
        /// <param name="url">Адрес</param>
        /// <param name="expectedHostName">Ожидаемое название хоста</param>
        /// <param name="expectedRegistrableDomain">Ожидаемый регистрируемый домен</param>
        [DataTestMethod]
        [DataRow("https://testingcp.42clouds.com/ResetPassword/Change?resetcode=d21d66aeaa8a4c9ff305a720d24deb2b",
            "testingcp.42clouds.com", "42clouds.com")]
        [DataRow("https://testingcp.42clouds.pro/ResetPassword/Change?resetcode=d21d66aeaa8a4c9ff305a720d24deb2b",
            "testingcp.42clouds.pro", "42clouds.pro")]
        [DataRow("https://testingcp.42clouds.kz/ResetPassword/Change?resetcode=d21d66aeaa8a4c9ff305a720d24deb2b",
            "testingcp.42clouds.kz", "42clouds.kz")]
        [DataRow("https://testingcp.42clouds.com/document/d/1geQ9pvnMFD7dn5KbIYeZS4s3RqQgCbF9OpAQ72IhAq4/edit?pli=1#",
            "testingcp.42clouds.com", "42clouds.com")]
        [DataRow("https://testingcp.42clouds.pro/document/d/1geQ9pvnMFD7dn5KbIYeZS4s3RqQgCbF9OpAQ72IhAq4/edit?pli=1#",
            "testingcp.42clouds.pro", "42clouds.pro")]
        [DataRow("https://testingcp.42clouds.kz/document/d/1geQ9pvnMFD7dn5KbIYeZS4s3RqQgCbF9OpAQ72IhAq4/edit?pli=1#",
            "testingcp.42clouds.kz", "42clouds.kz")]
        [DataRow(
            "https://devprovider.42clouds.com/api/BetaProvider?cmd=logout&openid.return_to=https://devcp.42clouds.com/SignIn",
            "devprovider.42clouds.com", "42clouds.com")]
        [DataRow(
            "https://devprovider.42clouds.com:413/api/BetaProvider?cmd=logout&openid.return_to=https://devcp.42clouds.com/SignIn",
            "devprovider.42clouds.com", "42clouds.com")]
        public void CheckDataWithAdditionalParamsInUrl(string url, string expectedHostName,
            string expectedRegistrableDomain)
        {
            var domainInfo = new Uri(url).GetDomainInfo();
            Assert.AreEqual(expectedHostName, domainInfo.HostName, "Не верно определено название хоста");
            Assert.AreEqual(expectedRegistrableDomain, domainInfo.RegistrableDomain, "Не верно определен регистрируемый домен");
        }
    }
}
