﻿using Clouds42.AccountDatabase.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests.DataValidations
{
    /// <summary>
    /// Тест для проверки валидатора названия инф. базы
    /// </summary>
    [TestClass]
    public class AccountDbCaptionValidatorTest
    {
        /// <summary>
        /// Проверим , что не вернется ошибка при валидном названии базы.
        /// </summary>
        /// <param name="dbCaption">Вилидное название инф. базы</param>
        [DataTestMethod]
        [DataRow("Тестовое название базы")]
        [DataRow("Новая база (New)")]
        public void CheckDbCaptionSuccess(string dbCaption)
        {
            var result = AccountDbCaptionValidator.Validate(dbCaption);
            Assert.IsTrue(result.Success, "Не прошла валидация валидного названия инф. базы");
        }

        /// <summary>
        /// Проверим , что вернется ошибка при невалидном названии базы.
        /// </summary>
        /// <param name="dbCaption">Вилидное название инф. базы</param>
        [DataTestMethod]
        [DataRow("")]
        [DataRow("Тестовое название базы, унф ?!//")]
        public void CheckAgentFioFail(string dbCaption)
        {
            var result = AccountDbCaptionValidator.Validate(dbCaption);
            Assert.IsFalse(result.Success, "Успешная валидация невалидного названия инф. базы");
        }

        /// <summary>
        /// Проверим, что вернется ошибка валидации при недопустимой длине названия инф. базы
        /// </summary>
        [TestMethod]
        public void ChecDbCaptionNoAcceptableMaxLengthFail()
        {
            var result = AccountDbCaptionValidator.Validate(new string('a', AccountDbCaptionValidator.MaxLengthDbCaption + 1));
            Assert.IsFalse(result.Success, "Успешная валидация при недопустимой длине названия инф. базы");
        }
    }
}
