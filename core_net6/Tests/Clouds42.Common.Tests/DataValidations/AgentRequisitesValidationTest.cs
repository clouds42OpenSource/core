﻿using Clouds42.Validators.Partner.Dictionary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests.DataValidations
{
    /// <summary>
    /// Тест для проверки валидации полей в реквизитах агента для выплаты
    /// </summary>
    [TestClass]
    public class AgentRequisitesValidationTest
    {
        /// <summary>
        /// Проверим, что ошибка не будет возвращена при валидации валидного ФИО.
        /// </summary>
        /// <param name="fio">Валидное значени ФИО</param>
        [DataTestMethod]
        [DataRow("Асгардиев Тор Одинсон")]
        [DataRow("Асгардиёв Тор Одинсон")]
        public void CheckAgentFioSuccess(string fio)
        {
            var result = AgentRequisitesRegexReference.FullNameRegx.IsMatch(fio);
            Assert.IsTrue(result, "Не прошла валидация валидного ФИО агента");
        }

        /// <summary>
        /// Проверим, что ошибка будет возвращена, если ФИО содержит недопустимые символы
        /// </summary>
        /// <param name="fio"></param>
        [DataTestMethod]
        [DataRow("Асгардиёв Тор Одинсон 123")]
        public void CheckAgentFioFail(string fio)
        {
            var result = AgentRequisitesRegexReference.FullNameRegx.IsMatch(fio);
            Assert.IsFalse(result, "Прошла валидация невалидного ФИО агента");
        }
    }
}
