﻿using Clouds42.Common.DataValidations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Common.UnitTests.DataValidations
{
    /// <summary>
    /// Тесты валидатора адреса электронной почты
    /// </summary>
    [TestClass]
    public class EmailValidationTests
    {
        /// <summary>
        /// Отсутствие обязательного сегмента
        /// </summary>
        [DataTestMethod]
        [DataRow("@mail.box")]
        [DataRow("e@.box")]
        [DataRow("e@mail.")]
        [DataRow("e@mail.box.")]
        [DataRow("email.box")]
        [DataRow("@.")]
        public void MissingSegment(string email)
        {
            var validationResult = new EmailValidation(email).EmailMaskIsValid();
            Assert.IsFalse(validationResult, "Отсутствует обязательный сегмент адреса");
        }

        /// <summary>
        /// Присутствие недопустимых символов
        /// </summary>
        [DataTestMethod]
        [DataRow("e%^@mail.box")]
        [DataRow("e++@mail.box")]
        [DataRow("e@mail.box!")]
        [DataRow("e@(mail).box")]
        [DataRow("e@@mail.box")]
        public void UnacceptableSymbols(string email)
        {
            var res = new EmailValidation(email).EmailMaskIsValid();
            Assert.IsFalse(res, "Присутствуют недопустимые символы");
        }

        /// <summary>
        /// Валидность правильных адресов
        /// </summary>
        [DataTestMethod]
        [DataRow("e@mail.box")]
        [DataRow("Ee1.2-3@mail1-box.ru")]
        public void ValidEmail(string email)
        {
            var res = new EmailValidation(email).EmailMaskIsValid();
            Assert.IsTrue(res, "Почта валидна");
        }
    }
}
