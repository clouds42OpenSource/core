﻿using Core42;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.IntegrationTests
{
    public class CustomWebApplicationFactory(Action<IServiceCollection>? overrideDependencies = null)
        : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(x => x.AddJsonFile("appsettings.Development.json"));
            builder.ConfigureServices(services => overrideDependencies?.Invoke(services));
        }
    }
}
