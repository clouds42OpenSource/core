﻿using System.Net.Http.Headers;
using System.Text;
using Clouds42.AccountDatabase.Access;
using Clouds42.AccountDatabase.Activity.Managers;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.ServiceExtensions.Managers;
using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.AccountUsers.Contracts;
using Clouds42.CloudServices;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.DataContracts.MyDisk;
using Clouds42.DataContracts.Service.Link;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.Link42.Managers;
using Clouds42.Resources.Managers;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.InvoiceContext.Dtos;
using Core42.Application.Features.InvoiceContext.Queries;
using Core42.Models;
using MediatR;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace Clouds42.IntegrationTests;

public class ExternalRequestsTests
{

    [Fact]
    public async Task Invoices_get_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ISender>(mock => mock.Setup(m => m.Send(It.IsAny<GetInvoiceByFilterQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ManagerResult<InvoiceDto>
                {
                    State = ManagerResultState.Ok, 
                    Result = new InvoiceDto{AccountAdminEmail = "1"}
                })); 
        });

        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Add("Token", "8f0c1a22-3f00-4349-86ed-b013608ae60d");
        // Act
        var response = await client.GetAsync($"/invoices/1/?accountId={Guid.NewGuid()}&sum={1}");
        var content = JsonConvert.DeserializeObject<ResponseResultDto<InvoiceDto>>(await response.Content.ReadAsStringAsync());

        // Assert
        response.EnsureSuccessStatusCode();

        Assert.True(content?.Success ?? false);

        Assert.NotNull(content.Data);
        Assert.True(content.Data.AccountAdminEmail == "1");
    }

    [Fact]
    public async Task Invoices_get_all_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ISender>(mock => mock.Setup(m => m.Send(It.IsAny<GetAllInvoicesQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ManagerResult<List<InvoiceDto>>
                {
                    State = ManagerResultState.Ok,
                    Result = [new InvoiceDto { AccountAdminEmail = "1" }]
                }));
        });

        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Add("Token", "8f0c1a22-3f00-4349-86ed-b013608ae60d");
        // Act
        var response = await client.GetAsync($"/invoices");
        var content = JsonConvert.DeserializeObject<ResponseResultDto<List<InvoiceDto>>>(await response.Content.ReadAsStringAsync());

        // Assert
        response.EnsureSuccessStatusCode();

        Assert.True(content?.Success ?? false);

        Assert.NotNull(content.Data);

        Assert.True(content.Data?.Any(x => x.AccountAdminEmail == "1") ?? false);
    }


    [Fact]
    public async Task Account_get_id_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ISender>(mock => mock.Setup(m => m.Send(It.IsAny<GetAccountIdByFilterQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ManagerResult<Guid?>
                {
                    State = ManagerResultState.Ok,
                    Result = Guid.Empty
                }));
        });

        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");
        // Act
        var response = await client.GetAsync("/accounts/id");
        var content = JsonConvert.DeserializeObject<ResponseResultDto<Guid?>>(await response.Content.ReadAsStringAsync());

        // Assert
        response.EnsureSuccessStatusCode();

        Assert.True(content?.Success ?? false);

        Assert.NotNull(content.Data);

        Assert.True(content.Data == Guid.Empty);
    }

    [Fact]
    public async Task AccountCsResource_get_resources_value_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountCsResourceValuesDataManager>(mock => mock.Setup(m => m.GetValueAsync(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<DateTime?>()))
                .ReturnsAsync(new ManagerResult<int>
                {
                    State = ManagerResultState.Ok,
                    Result = int.MaxValue
                }));
        });

        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");
        // Act
        var response = await client.GetAsync($"/api_v2/AccountCSResourceValues/GetResourcesValue?accountId={Guid.NewGuid()}&csResourceId={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();
            
        var content =
            JsonConvert.DeserializeObject<ServiceResourceValueDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.ServiceResourceValue == int.MaxValue);
    }

    [Fact]
    public async Task AccountCsResource_is_assigned_resource_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountCsResourceValuesManager>(mock => mock.Setup(m => m.IsAssignedResource(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(new ManagerResult<bool>
                {
                    State = ManagerResultState.Ok,
                    Result = true
                }));
        });

        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");
        // Act
        var response = await client.GetAsync($"/api_v2/AccountCSResourceValues/IsAssignedResource?accountId={Guid.NewGuid()}&accountUserId={Guid.NewGuid()}&resourcesType={string.Empty}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<BoolDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.Result);
    }

    [Fact]
    public async Task AccountCsResource_is_paid_service_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountCsResourceValuesManager>(mock => mock.Setup(m => m.IsActiveService(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new ManagerResult<bool>
                {
                    State = ManagerResultState.Ok,
                    Result = true
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");
        // Act
        var response = await client.GetAsync($"/api_v2/AccountCSResourceValues/IsPaidService?accountId={Guid.NewGuid()}&subject={Guid.NewGuid()}&serviceName={string.Empty}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<BoolDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.Result);
    }

    [Fact]
    public async Task AccountDatabaseCard_get_account_database_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabasesDataManager>(mock => mock.Setup(m => m.GetDatabasesDataByFilter(It.IsAny<AccountDatabasesFilterParamsDto>()))
                .Returns(new ManagerResult<AccountDatabaseDataDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountDatabaseDataDto{AllDatabasesCount = 1}
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");
        var accountId = Guid.NewGuid();
        var route = "/api_v2/AccountDatabases/{0}";
        // Act
        var response = await client.GetAsync(string.Format(route, accountId));

        // Assert
        response.EnsureSuccessStatusCode();
        var content =
            JsonConvert.DeserializeObject<ResponseResult<AccountDatabaseDataDto>>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.Data.AllDatabasesCount > 0);
    }

    [Fact]
    public async Task AccountDatabases_get_launch_instruction_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ISender>(mock => mock.Setup(m => m.Send(It.IsAny<GetLaunchDatabaseInstructionsQuery>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new ManagerResult<AccountDatabaseLaunchInstructionsDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountDatabaseLaunchInstructionsDto { LaunchAllowed = true }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/GetLaunchDatabaseInstructions");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountDatabaseLaunchInstructionsDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.LaunchAllowed);
    }

    [Fact]
    public async Task AccountDatabases_get_remote_app_params_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabaseManager>(mock => mock.Setup(m => m.GetRemoteAppParams(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(new ManagerResult<AccountDatabaseRemoteParamsDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountDatabaseRemoteParamsDto()
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/GetRemoteAppParams");
        client.DefaultRequestHeaders.Add("Token", "46c022df-8d12-453b-a6b1-84b07108a703");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountDatabaseRemoteParamsDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
    }

    [Fact]
    public async Task AccountDatabases_build_remote_desktop_link_string_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabaseManager>(mock => mock.Setup(m => m.BuildRemoteDesktopLinkString(It.IsAny<Guid>()))
                .Returns(new ManagerResult<string>
                {
                    State = ManagerResultState.Ok,
                    Result = "1"
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/BuildRemoteDesktopLinkString");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountDatabaseRemoteParamsDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountDatabaseRemoteParams.Length > 0);
    }

    [Fact]
    public async Task AccountDatabases_get_account_balance_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabaseManager>(mock => mock.Setup(m => m.GetAccountBallanse(It.IsAny<Guid>()))
                .Returns(new ManagerResult<double>
                {
                    State = ManagerResultState.Ok,
                    Result = 1
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/GetAccountBallanse");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountDatabasesBalanceDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountBallanse > 0);
    }

    [Fact]
    public async Task AccountDatabases_get_access_database_list_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabaseDataManager>(mock => mock.Setup(m => m.GetAccessDatabaseListAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<List<AccountDatabasePropertiesDto>>
                {
                    State = ManagerResultState.Ok,
                    Result = [new() { AccountCaption = "1" }]
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/GetAccessDatabaseList");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountDatabaseListDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountDatabaseList?.Any(x => x.AccountCaption == "1") ?? false);
    }

    [Fact]
    public async Task AccountDatabases_get_local_account_databases_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountDatabaseManager>(mock => mock.Setup(m => m.GetLocalAccountDatabases(It.IsAny<Guid>()))
                .Returns(new ManagerResult<List<LocalAccountDatabasesDto>>
                {
                    State = ManagerResultState.Ok,
                    Result = [new() { AccountID = Guid.Empty }]
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync("/api_v2/AccountDatabases/GetLocalAccountDatabases");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<LocalAccountDatabasesListDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.List?.Any(x => x.AccountID == Guid.Empty) ?? false);
    }

    [Fact]
    public async Task AccountDatabases_fixation_last_activity_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IFixationAccountDatabaseActivityManager>(mock => mock.Setup(m => m.FixLastActivity(It.IsAny<Guid>()))
                .Returns(new ManagerResult
                {
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.PostAsync("/api_v2/AccountDatabases/FixationLastActivity", new StringContent(JsonConvert.SerializeObject(new PostRequestSetActivityDateDto()), Encoding.UTF8, "application/json"));

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<EmptyResultDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
    }

    [Fact]
    public async Task AccountDatabasesServices_install_service_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IManageServiceExtensionDatabaseManager>(mock => mock.Setup(m => m.InstallServiceToDatabase(It.IsAny<InstallServiceExtensionToDatabaseDto>()))
                .Returns(new ManagerResult
                {
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.PostAsync("/api_v2/account-database-services", new StringContent(JsonConvert.SerializeObject(new InstallServiceExtensionToDatabaseDto()), Encoding.UTF8, "application/json"));

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<EmptyResultDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
    }

    [Fact]
    public async Task AccountDatabasesServices_uninstall_service_return_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IManageServiceExtensionDatabaseManager>(mock => mock.Setup(m => m.DeleteServiceFromDatabase(It.IsAny<Guid>(), It.IsAny<Guid>()))
                .Returns(new ManagerResult
                {
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.DeleteAsync($"/api_v2/account-database-services?serviceId={Guid.NewGuid()}&accountDatabaseId={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<EmptyResultDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
    }

    [Fact]
    public async Task Accounts_get_properties_return_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountsManager>(mock => mock.Setup(m => m.GetPropertiesInfoAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<AccountInfoQueryDataDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountInfoQueryDataDto{AccountAdminId = newGuid }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/Accounts/GetProperties?accountID={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountPropertiesDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.ManagerId == newGuid);
    }

    [Fact]
    public async Task Accounts_get_caption_return_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountsManager>(mock => mock.Setup(m => m.GetPropertiesInfoAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<AccountInfoQueryDataDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountInfoQueryDataDto { AccountCaption = newGuid.ToString() }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/Accounts/GetAccountCaption?accountID={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountCaptionDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountCaption == newGuid.ToString());
    }

    [Fact]
    public async Task AccountUserImpersonateData_get_by_token_result_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUserImpersonateDataManager>(mock => mock.Setup(m => m.GetByTokenAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<AccountUserImpersonateDataDto>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountUserImpersonateDataDto {AccountProperties = new AccountCommonDataDto{AccountCaption = newGuid.ToString()} }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUserImpersonateData/GetByToken?token={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountUserImpersonateDataDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.NotNull(content.AccountProperties);
        Assert.True(content.AccountProperties.AccountCaption == newGuid.ToString());
    }

    [Fact]
    public async Task AccountUsers_get_properties_result_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUsersManager>(mock => mock.Setup(m => m.GetProperties(It.IsAny<Guid>()))
                .Returns(new ManagerResult<AccountUser>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountUser { Id = newGuid} 
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUsers/GetProperties?AccountUserID={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountUserPropertiesDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.ID == newGuid);
    }

    [Fact]
    public async Task AccountUsers_check_admin_role_result_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUsersManager>(mock => mock.Setup(m => m.IsAccountAdmin(It.IsAny<Guid>()))
                .Returns(new ManagerResult<ResultDto<bool>>
                {
                    State = ManagerResultState.Ok,
                    Result = new ResultDto<bool> { Data = true, Head = string.Empty}
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUsers/CheckAdminUserRole?accountUserId={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AdminUserRoleDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AdminUserRole);
    }

    [Fact]
    public async Task AccountUsers_get_account_id_result_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUsersManager>(mock => mock.Setup(m => m.GetProperties(It.IsAny<Guid>()))
                .Returns(new ManagerResult<AccountUser>
                {
                    State = ManagerResultState.Ok,
                    Result = new AccountUser { AccountId = newGuid }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUsers/GetAccountId?accountUserId={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountUserAccountIDDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountID == newGuid);
    }

    [Fact]
    public async Task AccountUsers_get_ids_result_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUsersManager>(mock => mock.Setup(m => m.GetIDs(It.IsAny<Guid>()))
                .Returns(new ManagerResult<ResultDto<List<Guid>>>
                {
                    State = ManagerResultState.Ok,
                    Result = new ResultDto<List<Guid>>{ Data = [newGuid], Head = string.Empty }
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUsers/GetIDs?accountID={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountUsersIDsListDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountUserIDs?.List?.Any(x => x == newGuid));
    }

    [Fact]
    public async Task AccountUserSessions_check_token_validity_result_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUserSessionManager>(mock => mock.Setup(m => m.CheckTokenValidityAsync(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<bool>
                {
                    State = ManagerResultState.Ok,
                    Result = true
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUserSessions/CheckTokenValidity?token={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccounUserTokenValidityDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.TokenValidity);
    }

    [Fact]
    public async Task AccountUserSessions_get_accounts_user_id_by_token_result_success()
    {
        var newGuid = Guid.NewGuid();
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAccountUserSessionManager>(mock => mock.Setup(m => m.GetAccountUserIdByToken(It.IsAny<Guid>()))
                .Returns(new ManagerResult<ResultDto<Guid>>
                {
                    State = ManagerResultState.Ok,
                    Result = new ResultDto<Guid> {Data = newGuid, Head = string.Empty}
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/AccountUserSessions/GetAccountUserIDByToken?token={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content =
            JsonConvert.DeserializeObject<AccountUserIDDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AccountUserID == newGuid);
    }

    [Fact]
    public async Task AcDbAccesses_check_is_locked_result_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IAcDbAccessesManager>(mock => mock.Setup(m => m.IsLocked(It.IsAny<AcDbAccessCheckLockModelDto>()))
                .Returns(new ManagerResult<bool>
                {
                    Result = true,
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.PostAsync("/api_v2/AcDbAccesses/IsLocked", new StringContent(JsonConvert.SerializeObject(new AcDbAccessCheckLockModelDto()), Encoding.UTF8, "application/json"));

        // Assert
        response.EnsureSuccessStatusCode();

        var content = JsonConvert.DeserializeObject<BoolDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.Result);
    }

    [Fact]
    public async Task CloudFileStorageServers_get_file_storage_server_path_result_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ICloudFileStorageServerManager>(mock => mock.Setup(m => m.GetFileStorageServerPath(It.IsAny<Guid>()))
                .ReturnsAsync(new ManagerResult<string>
                {
                    Result = "1",
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/CloudFileStorageServers/GetFileStorageServerPath?FileStorageServerID={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content = JsonConvert.DeserializeObject<CloudFileStorageServerPathDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.StorageServerPath == "1");
    }

    [Fact]
    public async Task CloudServices_get_my_disk_by_account_result_success()
    {
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<ICloudServiceManager>(mock => mock.Setup(m => m.GetMyDiscByAccount(It.IsAny<Guid>()))
                .Returns(new ManagerResult<ResultDto<MyDiskInfoModelDto>>
                {
                    Result = new ResultDto<MyDiskInfoModelDto> {Data = new MyDiskInfoModelDto{AvailableSize = 1}, Head = string.Empty},
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/CloudServices/GetMyDiscByAccount?accountId={Guid.NewGuid()}");

        // Assert
        response.EnsureSuccessStatusCode();

        var content = JsonConvert.DeserializeObject<MyDiskInfoModelDto>(await response.Content.ReadAsStringAsync());

        Assert.NotNull(content);
        Assert.True(content.AvailableSize == 1);
    }

    [Fact]
    public async Task ResourcesConfiguration_get_expire_date_result_success()
    {
        var nowDate = DateTime.Now;
        await using var appFactory = new CustomWebApplicationFactory(services =>
        {
            services.Mock<IResourcesConfigurationManager>(mock => mock.Setup(m => m.GetServiceExpireDate(It.IsAny<Guid>(), It.IsAny<string>()))
                .ReturnsAsync(new ManagerResult<DateTime>
                {
                    Result = nowDate,
                    State = ManagerResultState.Ok
                }));
        });
        var client = appFactory.CreateClient();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        // Act
        var response = await client.GetAsync($"/api_v2/ResourceConfiguration/GetResourceConfigurationExpireDate?accountId={Guid.NewGuid()}&service=1");

        var stringContent = await response.Content.ReadAsStringAsync();
        // Assert
        response.EnsureSuccessStatusCode();

        var content = JsonConvert.DeserializeObject<DateTimeDto>(stringContent);

        Assert.NotNull(content);
        Assert.True(content.Date == nowDate);
    }

}
