﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation;
using Clouds42.Test.Runner;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling.HybridServiceTests;

namespace AppCommon.Scenario.Test.ProvidersTests
{
    /// <summary>
    /// Тест для проверки активации сервиса
    /// </summary>
    [TestClass]
    public class ActivateServiceForRegistrationAccountProviderTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста для проверки создания базы
        /// после активации сервиса для нового аккаунта в случае если у него только одна совместимая конфигурация
        /// </summary>
        [TestMethod]
        public void ChecDatabaseCreationAfterServiceActivationForNewAccount()
        {
            _testRunner.Run<ChecDatabaseCreationAfterServiceActivationForNewAccount>();
        }

        /// <summary>
        /// Сценарий теста для проверки активации сервиса в случае если у него несколько совместимых конфигураций
        /// </summary>
        [TestMethod]
        public void CheckNoDatabaseCreatedAndExtensionInstalledForExistingUserIfHasSeveralCompatibleConfig1C()
        {
            _testRunner.Run<CheckNoDatabaseCreatedAndExtensionInstalledForExistingUserIfHasSeveralCompatibleConfig1C>();
        }

        /// <summary>
        /// Сценарий теста для проверки создания базы
        /// после активации сервиса в случае если у него только одна совместимая конфигурация
        /// </summary>
        [TestMethod]
        public void CheckDatabaseCreationAfterServiceActivationIfHasOnlyOneCompatibleConfig1C()
        {
            _testRunner.Run<CheckDatabaseCreationAfterServiceActivationIfHasOnlyOneCompatibleConfig1C>();
        }

        /// <summary>
        /// Сценарий проверки регистрации аккаунта и активации сервиса через апи если у него несколько совместимых конфигураций
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountAndActivateServiceThroughApiIfHasSeveralCompatibleConfig1C()
        {
            _testRunner.Run<ScenarioRegisterAccountAndActivateServiceThroughApiIfHasSeveralCompatibleConfig1C>();
        }


        #region Гибридные сервисы

        /// <summary>
        /// Сценарий активации гибридного сервиса
        /// </summary>
        [TestMethod]
        public void ActivateHybridServiceTest()
        {
            _testRunner.Run<ActivateHybridServiceTest>();
        }

        /// <summary>
        /// Сценарий покупки подписки гибридного сервиса
        /// </summary>
        [TestMethod]
        public void ApplyDemoHybridServiceSubscribeTest()
        {
            _testRunner.Run<ApplyDemoHybridServiceSubscribeTest>();
        }

        /// <summary>
        /// Сценарий покупки гибридного сервиса при наличии денег на счету
        /// </summary>
        [TestMethod]
        public void PayHybridServiceWithMoneyTest()
        {
            _testRunner.Run<PayHybridServiceWithMoneyTest>();
        }

        /// <summary>
        /// Сценарий покупки гибридного сервиса при помощи ОП
        /// </summary>
        [TestMethod]
        public void PayHybridServiceWithPromisePaymentTest()
        {
            _testRunner.Run<PayHybridServiceWithPromisePaymentTest>();
        }

        /// <summary>
        /// Сценарий покупки подписки гибридного сервиса при помощи ОП
        /// </summary>
        [TestMethod]
        public void ApplyDemoHybridServiceSubscribeUsingPromisePaymentTest()
        {
            _testRunner.Run<ApplyDemoHybridServiceSubscribeUsingPromisePaymentTest>();
        }

        /// <summary>
        /// Сценарий активации бесплатного сервиса
        /// </summary>
        [TestMethod]
        public void ActivationFreeServiceTest()
        {
            _testRunner.Run<ActivationFreeServiceTest>();
        }
        #endregion
    }
}
