﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.AgencyAgreementsTests
{
    /// <summary>
    /// Тесты для агентских соглашений
    /// </summary>
    [TestClass]
    public class TestsForAgencyAgreement
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Расчет агентского вознаграждения за Аренду, Диск и Кастомный сервис
        /// </summary>
        [TestMethod]
        public void AgentRewardByActualAgencyAgreement()
        {
            _testRunner.Run<AgentRewardByActualAgencyAgreement>();
        }

        /// <summary>
        /// Получить актуальное агентское соглашение
        /// </summary>
        [TestMethod]
        public void GetActualAgencyAgreementTest()
        {
            _testRunner.Run<GetActualAgencyAgreementTest>();
        }

        /// <summary>
        /// Получить детальную информацию об агентском соглашении
        /// </summary>
        [TestMethod]
        public void GetAgencyAgreementDetailsTest()
        {
            _testRunner.Run<GetAgencyAgreementDetailsTest>();
        }

        /// <summary>
        /// Получить данные агентских соглашений
        /// </summary>
        [TestMethod]
        public void GetAgencyAgreementsDataTest()
        {
            _testRunner.Run<GetAgencyAgreementsDataTest>();
        }

        /// <summary>
        /// Получить следующее агентское соглашение
        /// которое вступит в силу(если такое соглашение существует)
        /// </summary>
        [TestMethod]
        public void TryGetNextEffectiveAgencyAgreementTest()
        {
            _testRunner.Run<TryGetNextEffectiveAgencyAgreementTest>();
        }

        /// <summary>
        /// Принять агентское соглашение
        /// </summary>
        [TestMethod]
        public void ApplyAgencyAgreementTest()
        {
            _testRunner.Run<ApplyAgencyAgreementTest>();
        }
    }
}
