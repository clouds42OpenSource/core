﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DeleteAccountsData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test
{
    /// <summary>
    /// Тесты выборки, уведомления перед удалением данных аккаунтов
    /// </summary>
    [TestClass]
    public class DeleteAccountsDataTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Уведомление о скорой архивации баз и файлов аккаунта
        /// </summary>
        [Obsolete("Переписать")]
        public void BeforeArchiveDataNotificationTest()
        {
            _testRunner.Run<BeforeArchiveDataNotificationTest>();
        }

        /// <summary>
        /// Выборка аккаунтов для удаления данных
        /// </summary>
        [TestMethod]
        public void GetAccountsForDeleteDataTest()
        {
            _testRunner.Run<GetAccountsForDeleteDataTest>();
        }

        /// <summary>
        /// Проверка выборки аккаунтов для удаления данных
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountsForDeleteDataInSampleTest()
        {
            _testRunner.Run<CheckAccountsForDeleteDataInSampleTest>();
        }
    }
}
