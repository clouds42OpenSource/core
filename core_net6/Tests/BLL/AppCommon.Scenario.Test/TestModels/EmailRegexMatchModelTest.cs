﻿namespace AppCommon.Scenario.Test.TestModels
{
    /// <summary>
    /// Модель тестирования валидации почты регулярным выражением
    /// </summary>
    public class EmailRegexMatchModelTest
    {
        /// <summary>
        /// Почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Ожидаемый результат
        /// </summary>
        public bool Result { get; set; }
    }
}
