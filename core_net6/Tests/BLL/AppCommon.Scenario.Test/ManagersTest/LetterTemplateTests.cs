﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджер для работы с шаблоном письма
    /// </summary>
    [TestClass]
    public class LetterTemplateTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест редактирования шаблона письма
        /// </summary>
        [TestMethod]
        public void ScenarioEditLetterTemplateTest()
        {
            _testRunner.Run<EditLetterTemplateTest>();
        }
    }
}
