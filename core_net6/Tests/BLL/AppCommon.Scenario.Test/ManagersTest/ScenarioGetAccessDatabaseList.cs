﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты получения списка доступов в базу
    /// </summary>
    [TestClass]
    public class ScenarioGetAccessDatabaseListTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
        /// </summary>
        [TestMethod]
        public void GetAccessDatabaseListTest1() => _testRunner.Run<GetAccessDatabaseListTest1>();

        /// <summary>
        /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
        /// </summary>
        [TestMethod]
        public void GetAccessDatabaseListTest2() => _testRunner.Run<GetAccessDatabaseListTest2>();

        /// <summary>
        /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public void GetAccessDatabaseListTest3() => _testRunner.Run<GetAccessDatabaseListTest3>();

        /// <summary>
        /// Проверка чистки таблицы AcDbAccesses после удаление пользователя
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task ScenarioDeleteUserAndCheckAcDbAccesses() =>
            await _testRunner.RunAsync<ScenarioDeleteUserAndCheckAcDbAccesses>();
    }
}
