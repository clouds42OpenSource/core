﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Hosting;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class HostTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Создание и изменение хостинга
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAndUpdateHostingTest()
        {
            _testRunner.Run<ScenarioCreateAndUpdateHostingTest>();
        }

        /// <summary>
        /// Создание и удаление хостинга
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAndDeleteHostingTest()
        {
            _testRunner.Run<ScenarioCreateAndDeleteHostingTest>();
        }

        /// <summary>
        /// Создание и удаление хостинга, который указан в сегменте
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteUsedHostingTest()
        {
            _testRunner.Run<ScenarioDeleteUsedHostingTest>();
        }

    }
}
