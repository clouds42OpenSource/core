﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CalculateDaysToDeleteOrArchivingDatabase;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на проверку Количества дней до удаления и архивации данных аккаунта
    /// </summary>
    [TestClass]
    public class CalculateDaysBeforeArchiveAndDeleteAccountData
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Проверка рассчета количества дней до удаления данных неактивного демо аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCalculateDaysBeforeDeleteAccountDataTest()
        {
            _testRunner.Run<CalculateDaysBeforeDeleteAccountDataTest>();
        }

        /// <summary>
        /// Проверка расчета количества дней до архивации данных неактивного аккаунта(не демо)
        /// </summary>
        [TestMethod]
        public void ScenarioCalculateDaysBeforeArchivingAccountDataTest()
        {
            _testRunner.Run<CalculateDaysBeforeArchivingAccountDataTest>();
        }
    }
}

