﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1C;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста спонсирования арендой 1С пользователей другого аккаунта
    /// при наличии у пользователей аренды 1С(наличии доступных лицензий)
    /// </summary>
    [TestClass]
    public class SponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1CTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста спонсирования арендой 1С пользователей другого аккаунта
        /// при наличии у пользователей аренды 1С
        /// </summary>
        [TestMethod]
        public void ScenarioSponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1CTest()
        {
            _testRunner.Run<ScenarioSponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1CTest>();
        }

        /// <summary>
        /// Сценарий теста на поиск внешнего пользователя для спонсирования
        /// </summary>
        [TestMethod]
        public void ScenarioSearchExternalUserForSponsoringTest()
        {
            _testRunner.Run<ScenarioSearchExternalUserForSponsoringTest>();
        }
    }
}
