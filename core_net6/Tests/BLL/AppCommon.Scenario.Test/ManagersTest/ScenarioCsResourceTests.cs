﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CsResources;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера управления данными движемых ресурсов.
    /// </summary>
    [TestClass]
    public class ScenarioCsResourceTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест списания ресурсов
        /// </summary>
        [TestMethod]
        public void ScenarioDecreaseCsResourceValueTest()
        {
            _testRunner.Run<DecreaseCsResourceValueTest>();
        }

        /// <summary>
        /// Тест начисления ресурсов
        /// </summary>
        [TestMethod]
        public void ScenarioIncreaseCsResourceValueTest()
        {
            _testRunner.Run<IncreaseCsResourceValueTest>();
        }

    }
}
