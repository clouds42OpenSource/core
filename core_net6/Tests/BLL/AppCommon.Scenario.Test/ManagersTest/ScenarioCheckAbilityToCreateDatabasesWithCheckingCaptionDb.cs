﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioCheckAbilityToCreateDatabasesWithCheckingCaptionDb;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз с проверкой на имя базы
    /// </summary>
    [TestClass]
    public class ScenarioCheckAbilityToCreateDatabasesWithCheckingCaptionDb
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void ScenarioCheckAbilityToCreateDatabasesWithFalseCaption()
        {
            _testRunner.Run<ScenarioCheckAbilityToCreateDatabasesWithFalseCaption>();
        }

        [TestMethod]
        public void ScenarioCheckAbilityToCreateDatabasesWithTrueCaption()
        {
            _testRunner.Run<ScenarioCheckAbilityToCreateDatabasesWithTrueCaption>();
        }
    }
}
