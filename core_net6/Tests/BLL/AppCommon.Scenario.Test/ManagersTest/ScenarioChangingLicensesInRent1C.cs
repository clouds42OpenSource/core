﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты проверки изменения лицензий Аренды 1С
    /// </summary>
    [TestClass]
    public class ScenarioChangingLicensesInRent1C
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Cценарий подключения нового пользователя при недостаточном балансе
        /// </summary>
        [TestMethod]
        public void ConnectionUserToRent1CWithZeroBalance()
        {
            _testRunner.Run<ConnectionUserToRent1CWithZeroBallanceTest>();
        }

        /// <summary>
        /// Сценарий подключения пользователя при достаточном кол-ве денег на балансе
        /// </summary>
        [TestMethod]
        public void ConnectionUserToRentWithMoneyOnBalance()
        {
            _testRunner.Run<ConnectionUserToRent1CWithMoneyOnBalanceTest>();
        }

        /// <summary>
        /// Сценарий отключения пользователя от Стандарта с активным сервисом,
        /// с деньгами на балансе
        /// </summary>
        [TestMethod]
        public void DisconnectionUserFromRent1C()
        {
            _testRunner.Run<DisconnectionUserFromRent1CTest>();
        }

        /// <summary>
        /// Сценарий отключения пользователя при неактивном сервисе + баланса достаточно
        /// для продления измененной подписки
        /// </summary>
        [TestMethod]
        public void DisconnectionUserFromRentWithFrozenService()
        {
            _testRunner.Run<DisconnectionUserFromRent1CWithFrozenServiceTest>();
        }

        /// <summary>
        /// Сценарий отключения пользователя от Стандарт-а с активным сервисом,
        /// без денег на балансе
        /// </summary>
        [TestMethod]
        public void DisconnectionUserFromRentWithZeroBalance()
        {
            _testRunner.Run<DisconnectionUserFromRent1CWithZeroBalanceTest>();
        }
        /// <summary>
        /// Сценарий отключения пользователей от Стандарт-а при заблокированом сервисе,
        /// без денег на балансе
        /// </summary>
        [TestMethod]
        public void DisconnectionWithFrozenServiceAndZeroBalance()
        {
            _testRunner.Run<DisconnectionWithFrozenServiceAndZeroBalanceTest>();
        }

        /// <summary>
        /// Сценарий подключения нового пользователя к Аренде(Стандарт)
        /// при неактивном сервисе + баланса достаточно для продления измененной подписки
        /// </summary>
        [TestMethod]
        public void ConnectionToRentWithFrozenServiceAndMoney()
        {
            _testRunner.Run<ConnectionToRentWithFrozenServiceAndMoney>();
        }

        /// <summary>
        /// Сценарий отключения пользователя от Аренды(Стандарт)
        /// при неактивном сервисе + баланса достаточно для продления измененной подписки
        /// </summary>
        [TestMethod]
        public void DisconnectionToRentWithFrozenServiceAndMoney()
        {
            _testRunner.Run<DisconnectionToRentWithFrozenServiceAndMoney>();
        }

        /// <summary>
        /// Сценарий отключения пользователя от Стандарт-а с заблокированным
        /// сервисом из за просроченного ОП
        /// </summary>
        [TestMethod]
        public async Task ProcessExpiredPromisePaymentsWithoutMoneyTest()
        {
            await _testRunner.RunAsync<ProcessExpiredPromisePaymentsWithoutMoneyTest>();
        }

        /// <summary>
        /// Сценарий проверки добавления нового пользователя и блокировки его
        /// </summary>
        [TestMethod]
        public void CheckNotActivateUserInAdGroupsTest()
        {
            _testRunner.Run<CheckNotActivateUserInADGroupsTest>();
        }
    }
}
