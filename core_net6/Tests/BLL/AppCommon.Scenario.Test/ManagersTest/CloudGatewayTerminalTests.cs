﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CloudGatewayTerminal;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера для работы с терминальными шлюзами
    /// </summary>
    [TestClass]
    public class CloudGatewayTerminalTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания/удаления/редактирования терминального шлюза
        /// </summary>
        [TestMethod]
        public void ScenarioCrudCloudGatewayTerminalTest()
        {
            _testRunner.Run<CrudCloudGatewayTerminalTest>();
        }

        /// <summary>
        /// Тест выборки терминальных шлюзов по фильтру
        /// </summary>
        [TestMethod]
        public async Task ScenarioCloudGatewayTerminalSamplingTest()
        {
            await _testRunner.RunAsync<CloudGatewayTerminalSamplingTest>();
        }
    }
}
