﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.TerminalFarm;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер ферм ТС
    /// </summary>
    [TestClass]
    public class TerminalFarmTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания/удаления/редактирования фермы ТС
        /// </summary>
        [TestMethod]
        public void ScenarioCrudTerminalFarmTest()
        {
            _testRunner.Run<CrudTerminalFarmTest>();
        }

        /// <summary>
        /// Тест выборки ферм ТС по фильтру
        /// </summary>
        [TestMethod]
        public void ScenarioTerminalFarmSamplingTest()
        {
            _testRunner.Run<TerminalFarmSamplingTest>();
        }
    }
}
