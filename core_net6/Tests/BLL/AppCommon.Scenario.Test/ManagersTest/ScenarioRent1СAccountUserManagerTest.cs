﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.MixedPayments;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesConfigurationScenario;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioRent1СAccountUserManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Подключение Аренды 1С стандарт для спонсируемого аккаунта который заблокирован
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CStandardSponsorAccountUser()
        {            
            _testRunner.Run<ConnectionRent1CStandartSponsorAccountUserTest>();
        }

        /// <summary>
        /// Подключение Аренды 1С для спонсируемого аккаунта
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CWebSponsorAccountUser()
        {
            _testRunner.Run<ConnectionRent1CWebSponsorAccountUserTest>();
        }

        /// <summary>
        /// Отключение Аренды 1С стандарт для спонсируемого аккаунта
        /// </summary>
        [TestMethod]
        public void DisconnectionRent1CSponsorAccountUser()
        {            
            _testRunner.Run<DisconnectionRent1CSponsorAccountUser>();
        }

        /// <summary>
        /// Сценарий подключения нового пользователя при недостаточном балансе при помощи ОП
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CStandardAccountUser()
        {
            _testRunner.Run<ConnectionRent1CStandardAccountUserWithThePromisedPaymentTest>();
        }

        /// <summary>
        /// Подключение Аренды 1С  для спонсируемого аккаунта
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CWebAccountUser()
        {
            _testRunner.Run<ConnectionRent1CWebAccountUserTest>();
        }

        /// <summary>
        /// Отключение Аренды 1С стандарт для пользователя
        /// </summary>
        [TestMethod]
        public void DisconnectionRent1CAccountUser()
        {
            _testRunner.Run<DisconnectionRent1CAccountUser>();
        }

        /// <summary>
        /// Подключение Аренды 1С стандарт для многих аккаунтов
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CManyAccountUser()
        {
            _testRunner.Run<ConnectionRent1CManyAccountUserTest>();
        }

        /// <summary>
        /// Проверка на отправку сообщения о блокировке Аренды за пять дней
        /// </summary>
        [TestMethod]
        public void NotifyBefore5DaysLock()
        {
            _testRunner.Run<EmailNotificationBeforeLockRent1CTest>();
        }
        
        /// <summary>
        /// Сценарий подключения нового пользователя при недостаточном балансе
        /// </summary>
	    [TestMethod]
	    public void ConnectionUserAndSponsorUserToRent1CWithZeroBalanceTest()
	    {
		    _testRunner.Run<ConnectionUserAndSponsorUserToRent1CWithZeroBalanceTest>();
	    }

        /// <summary>
        /// Сценарий подключения нового пользователя при недостаточном балансе при помощи ОП
        /// </summary>
	    [TestMethod]
	    public void ConnectionRent1CStandardSponsorAccountUserWithThePromisedPaymentTest()
	    {
		    _testRunner.Run<ConnectionRent1CStandardSponsorAccountUserWithThePromisedPaymentTest>();
	    }

        /// <summary>
        /// Подключение Аренды 1С стандарт для многих аккаунтов при помощи ОП
        /// </summary>
	    [TestMethod]
	    public void ConnectionRent1CManyAccountUserWithThePromisedPaymentTest()
	    {
		    _testRunner.Run<ConnectionRent1CManyAccountUserWithThePromisedPaymentTest>();
	    }

        /// <summary>
        /// Сценарий блокировки сервиса Аренда 1С и проверки того
        /// что выводится сообщение о блокировке
        /// </summary>
        [TestMethod]
        public void LockRent1CWithPromisePaymentTest()
        {
            _testRunner.Run<LockRent1CWithPromisePaymentTest>();
        }

        /// <summary>
        /// Сценарий подсчета итоговой стоимости аренды при удалении спонсируемого пользователя
        /// </summary>
        [TestMethod]
        public void RemoveSponsorAccountUserWithConnectionRent1CStandardTest()
        {
            _testRunner.Run<RemoveSponsorAccountUserWithConnectionRent1CStandardTest>();
        }

        /// <summary>
        /// Активация Аренды по спонсорской лицензии
        /// </summary>
        [TestMethod]
        public void ActiveRentBySponsorLicenseTest()
        {
            _testRunner.Run<ActiveRentBySponsorLicenseTest>();
        }

        /// <summary>
        /// Сценарий теста на спонсирование с проверкой на демо аккаунт
        /// </summary>
        [TestMethod]
        public void ScenarioSponsoringWithCheckingForDemoAccountTest()
        {
            _testRunner.Run<ScenarioSponsoringWithCheckingForDemoAccountTest>();
        }

        /// <summary>
        /// Сценарий теста на спонсирование при помощи ОП
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CWebSponsorAccountUserWithThePromisedPaymentTest()
        {
            _testRunner.Run<ConnectionRent1CWebSponsorAccountUserWithThePromisedPaymentTest>();
        }

        /// <summary>
        /// Сценарий теста на погашение ОП и не продление сервиса
        /// </summary>
        [TestMethod]
        public void LockRent1CWithPromisePaymentAndExpiredPromiseTest()
        {
            _testRunner.Run<LockRent1CWithPromisePaymentAndExpiredPromiseTest>();
        }

        /// <summary>
        /// Сценарий теста на продление аренды при наличии ОП с совпадающей датой окончания
        /// </summary>
        [TestMethod]
        public void LockRent1CWithPromisePaymentAndExpiredRent1CwithNoMoney()
        {
            _testRunner.Run<LockRent1CWithPromisePaymentAndExpiredRent1CwithNoMoney>();
        }

        /// <summary>
        /// Сценарий проверки продления дэмо-периода
        /// </summary>
        [TestMethod]
        public void Rent1CDemoProlongationTest()
        {
            _testRunner.Run<ScenarioRent1CDemoProlongation>();
        }

        /// <summary>
        /// Сценарий проверки неудачного продления дэмо-периода
        /// </summary>
        [TestMethod]
        public void Rent1CDemoProlongationButTooLate()
        {
            _testRunner.Run<ScenarioRent1CDemoProlongationButTooLate>();
        }

        [TestMethod]
        public void ResourcesConfigurationTest()
        {
            _testRunner.Run<ResourcesConfigurationTest>();
        }

        /// <summary>
        /// Покупка веба за бонусы
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CWebWithMixedPayment()
        {
            _testRunner.Run<ConnectionRent1CWebWithMixedPayment>();
        }

        /// <summary>
        /// Покупка стандарта за деньги + бонусы
        /// </summary>
        [TestMethod]
        public void ConnectionRent1CStandardWithMixedPayment()
        {
            _testRunner.Run<ConnectionRent1CStandartWithMixedPayment>();
        }

        /// <summary>
        /// Подключение сервиса Аренда 1С пользователю
        /// </summary>
        [TestMethod]
        public void ScenarioConnectUserToRent1C()
        {
            _testRunner.Run<ConnectUserToRent1C>();
        }

        /// <summary>
        /// Подключение к сервису Аренда 1С пользователя спонсируемого аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioConnectSponsoredUserToRent1C()
        {
            _testRunner.Run<ConnectSponsoredUserToRent1C>();
        }

        /// <summary>
        /// Проверка подключения пользователя к Аренде, если на балансе не достаточно денег
        /// </summary>
        [TestMethod]
        public void ScenarioConnectUserToRent1CWithZeroBalance()
        {
            _testRunner.Run<ConnectUserToRent1CWithZeroBalance>();
        }

        /// <summary>
        /// Тест проверяющий, что ресурсы активных демо сервисов не удаляются при пролонгации
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDemoServiceResourcesAfterProlongRent1CTest()
        {
            _testRunner.Run<CheckDemoServiceResourcesAfterProlongRent1CTest>();
        }

    }
}
