﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AdvertisingBannerTemplateTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера для работы с рекламными баннерами
    /// </summary>
    [TestClass]
    public class AdvertisingBannerTemplateTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания, удаления, редактирования шаблона рекламного баннера
        /// </summary>
        [TestMethod]
        public void ScenarioCrudAdvertisingBannerTemplateTest()
        {
            _testRunner.Run<CrudAdvertisingBannerTemplateTest>();
        }

        /// <summary>
        /// Тест подсчета аудитории рекламного баннера
        /// </summary>
        [TestMethod]
        public void ScenarioCalculateAdvertisingBannerAudienceTest()
        {
            _testRunner.Run<CalculateAdvertisingBannerAudienceTest>();
        }

        /// <summary>
        /// Тест ошибочного создания рекламного баннера
        /// </summary>
        [TestMethod]
        public void ScenarioErrorCreateAdvertisingBannerTemplateTest()
        {
            _testRunner.Run<ErrorCreateAdvertisingBannerTemplateTest>();
        }
    }
}
