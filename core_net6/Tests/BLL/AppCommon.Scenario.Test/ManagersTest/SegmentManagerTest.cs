﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Segment;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class SegmentManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Удаление пустого сегмента
        /// </summary>
        [TestMethod]
        public void DeleteCloudServicesSegmentTest()
        {
            _testRunner.Run<DeleteEmptySegmentTest>();            
        }

        /// <summary>
        /// Смена флага GroupByAccount с последующей публикацией
        /// базы и проверкой генерируемого пути
        /// </summary>
        [TestMethod]
        public void ChangeGroupByAccountInContentServerTest()
        {
            _testRunner.Run<ChangeGroupByAccountInContentServerTest>();
        }

        /// <summary>
        /// Тестовый сценарий смены GroupByAccount
        /// </summary>
        [TestMethod]
        public void ChangeGroupByAccountWithPublishedDbTest()
        {
            _testRunner.Run<ChangeGroupByAccountWithPublishedDbTest>();
        }

        /// <summary>
        /// Тест смены версий платформы в сегменте
        /// </summary>
        [TestMethod]
        public async Task ChangePlatformVersionsTest()
        {
            await _testRunner.RunAsync<ChangePlatformVersionsTest>();
        }

        /// <summary>
        /// Тест на проверку смены альфа версии
        /// </summary>
        [TestMethod]
        public async Task ChangeAlphaPlatformVersionTest()
        {
            await _testRunner.RunAsync<ChangeAlphaPlatformVersionTest>();
        }

        /// <summary>
        /// Тест на переопубликацию списка баз в сегменте
        /// при смене версии платформы
        /// </summary>
        [TestMethod]
        public async Task RepublishSegmentDatabasesTest()
        {
            await _testRunner.RunAsync<RepublishSegmentDatabasesTest>();
        }

        /// <summary>
        /// Тест на проверку смены сервера публикации в сегменте.
        /// </summary>
        [TestMethod]
        public async Task ChangingContentServerInSegment()
        {
            await _testRunner.RunAsync<ChangingContentServerInSegment>();
        }

        /// <summary>
        /// Проверка логирования изменений в сегменте
        /// </summary>
        [TestMethod]
        public async Task WorkWithSegmentLogging()
        {
            await _testRunner.RunAsync<WorkWithSegmentLogging>();
        }
    }
}
