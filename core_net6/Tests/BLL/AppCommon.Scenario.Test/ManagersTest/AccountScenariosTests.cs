﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Accounts;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для аккаунта
    /// </summary>
    [TestClass]
    public class AccountScenariosTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий получения списка аккаунтов.
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountListTest()
        {
            _testRunner.Run<ScenarioGetAccountListTest>();
        }

        /// <summary>
        /// Сценарий получения информационных баз аккаунта для миграций
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountDatabasesForMigrationTest()
        {
            _testRunner.Run<ScenarioGetAccountDatabasesForMigrationTest>();
        }
        
    }
}