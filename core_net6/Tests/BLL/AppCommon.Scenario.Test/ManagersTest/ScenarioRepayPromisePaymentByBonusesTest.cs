﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тестирование погашения ОП денежными и бонусными средствами
    /// Сценарий:
    /// 1) Создаём аккаунт
    /// 2) Редактируем таблицу BillingAccaunt якобы есть ОП 
    /// 3) Создаём входящую бонусну транзакцию на сумму меньшую ОП
    /// и проверяем, что ОП не гасился частично и что бонусы пополнились
    /// 4) Создаём входящую денежную транзакцию
    /// 5) Проверяем, что ОП погашен
    /// </summary>
    public class ScenarioRepayPromisePaymentByBonusesTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var testRestSum = 111;
            var promisePaymentSum = 1500;
            var cashSum = 1000;
            var bonusSum = 500 + testRestSum;

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            billingAccount.PromisePaymentSum = promisePaymentSum;
            billingAccount.PromisePaymentDate = DateTime.Now;
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<IBillingServiceDataProvider>();
            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Пополнение бонусного баланса",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = bonusSum,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Bonus
            });

            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());
            billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(billingAccount.PromisePaymentSum,promisePaymentSum, "Сумма ОП не должна была изменится");
            Assert.AreEqual(billingAccount.Balance,0,"Сумма на счету должна быть 0");
            Assert.AreEqual(billingAccount.BonusBalance, bonusSum, $"Бонусов должно быть {bonusSum}");

            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Пополнение денежного баланса",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cashSum,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Money
            });

            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());
            billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(billingAccount.PromisePaymentSum, 0, "ОП должен был погасится");
            Assert.AreEqual(billingAccount.Balance, 0, "Сумма на счету должна быть 0");
            Assert.AreEqual(billingAccount.BonusBalance, testRestSum, $"Бонусов должно быть {testRestSum}");
        }
    }
}
