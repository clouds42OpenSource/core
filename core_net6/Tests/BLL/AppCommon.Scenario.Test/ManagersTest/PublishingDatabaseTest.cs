﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingDatabasesTest;

using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки процесса публикации информационных баз
    /// </summary>
    [TestClass]
    public class PublishingDatabaseTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки статуса публикации ИБ при попытке опубликовать
        /// </summary>
        [TestMethod]
        public void ScenarioErrorPublishingDatabases()
        {
            _testRunner.Run<ScenarioErrorPublishingDatabases>();
        }

        /// <summary>
        /// Сценарий теста проверки статуса публикации ИБ при попытке отменить публикацию
        /// </summary>
        [TestMethod]
        public void ScenarioErrorCancelPublishingDatabase()
        {
            _testRunner.Run<ScenarioErrorCancelPublishingDatabase>();
        }

        /// <summary>
        /// Сценарий теста проверки статуса публикации ИБ при попытке переопубликовать
        /// </summary>
        [TestMethod]
        public void ScenarioErrorRePublishingDatabase()
        {
            _testRunner.Run<ScenarioErrorRePublishingDatabase>();
        }

        /// <summary>
        /// Сценарий теста проверки публикации базы когда одна из нод недоступна
        /// </summary>
        [TestMethod]
        public void ScenarioPublishingDatabaseWhenOneNodeIsUnavailable()
        {
            _testRunner.Run<ScenarioPublishingDatabaseWhenOneNodeIsUnavailable>();
        }
    }
}
