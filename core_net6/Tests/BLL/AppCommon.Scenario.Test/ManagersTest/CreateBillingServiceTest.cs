﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CreateBillingService;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки создания сервиса
    /// </summary>
    [TestClass]
    public class CreateBillingServiceTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста успешного создания сервиса
        /// </summary>
        [TestMethod]
        public async Task ScenarioForSuccessfulServiceCreationTest()
        {
            await _testRunner.RunAsync<ScenarioForSuccessfulServiceCreationTest>();
        }

        /// <summary>
        /// Сценарий теста ошибочного создания сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioForErrorServiceCreationTest()
        {
            _testRunner.Run<ScenarioForErrorServiceCreationTest>();
        }

        /// <summary>
        /// Сценарий теста успешного сохранения изменений сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioForSuccessSaveBillingServiceChangesTest()
        {
            _testRunner.Run<ScenarioForSuccessSaveBillingServiceChangesTest>();
        }

        /// <summary>
        /// Сценарий теста ошибочного сохранения изменений сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioForErrorSaveBillingServiceChangesTest()
        {
            _testRunner.Run<ScenarioForErrorSaveBillingServiceChangesTest>();
        }

    }
}
