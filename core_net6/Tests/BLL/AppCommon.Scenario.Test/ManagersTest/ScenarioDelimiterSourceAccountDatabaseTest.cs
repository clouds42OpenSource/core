﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DelimiterSourceAccountDatabaseTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста для проверки работы
    /// с материнскими базами разделителей
    /// </summary>
    [TestClass]
    public class ScenarioDelimiterSourceAccountDatabaseTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки создания материнской базы разделителей
        /// </summary>
        [TestMethod]
        public void ScenarioCreateDelimiterSourceAccountDatabaseTest()
        {
            _testRunner.Run<ScenarioCreateDelimiterSourceAccountDatabaseTest>();
        }

        /// <summary>
        /// Сценарий теста проверки редактирования материнской базы разделителей
        /// </summary>
        [TestMethod]
        public async Task ScenarioEditDelimiterSourceAccountDatabaseTest()
        {
            await _testRunner.RunAsync<ScenarioEditDelimiterSourceAccountDatabaseTest>();
        }

        /// <summary>
        /// Сценарий теста проверки редактирования материнской базы разделителей
        /// </summary>
        [TestMethod]
        public void ScenarioRemoveDelimiterSourceAccountDatabaseTest()
        {
            _testRunner.Run<ScenarioRemoveDelimiterSourceAccountDatabaseTest>();
        }
    }
}
