﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.RunTaskForYookassaAggregatorManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по запуску задачи на проверку
    /// статуса платежа агрегатора ЮKassa
    /// </summary>
    [TestClass]
    public class RunTaskForYookassaAggregatorManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста запуска задачи по проверке
        /// статуса платежа агрегатора ЮKassa, для платежа со статусом Pending
        /// </summary>
        [TestMethod]
        public void RunTaskToCheckYookassaPaymentStatusForPendingPaymentTest()
        {
            _testRunner.Run<RunTaskToCheckYookassaPaymentStatusForPendingPaymentTest>();
        }
    }
}
