﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по работе
    /// с заявкой сервиса на модерацию
    /// </summary>
    [TestClass]
    public class ChangeServiceRequestManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста, по проверке получения
        /// заявки на создание сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetCreateServiceRequestTest()
        {
            _testRunner.Run<ScenarioGetCreateServiceRequestTest>();
        }

        /// <summary>
        /// Сценарий теста, по проверке получения
        /// заявки на редактирование сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetEditServiceRequestTest()
        {
            _testRunner.Run<ScenarioGetEditServiceRequestTest>();
        }

        /// <summary>
        /// Сценарий теста, по проверке  полей услуг сервиса,
        /// при получении заявки на редактирование сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckServiceTypesAtGetEditServiceRequestTest()
        {
            _testRunner.Run<ScenarioCheckServiceTypesAtGetEditServiceRequestTest>();
        }
    }
}
