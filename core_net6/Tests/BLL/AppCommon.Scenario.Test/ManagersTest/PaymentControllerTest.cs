﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Payments;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест проверки методов PaymentController
    /// </summary>
    [TestClass]
    public class PaymentControllerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Получить информацию о ПС
        /// </summary>
        [TestMethod]
        public void GetPaymentSystemRefillDTOTest()
        {
            _testRunner.Run<GetPaymentSystemRefillDTOTest>();
        }

        /// <summary>
        /// Получить инфойс или создать новый
        /// </summary>
        [TestMethod]
        public void GetInvoiceTest()
        {
            _testRunner.Run<GetInvoiceTest>();
        }

        /// <summary>
        /// Получить или создать новый счет на оплату по заданным параметрам.
        /// </summary>
        [TestMethod]
        public void GetProcessingOrCreateNewInvoiceTest()
        {
            _testRunner.Run<GetProcessingOrCreateNewInvoiceTest>();
        }

        /// <summary>
        /// Получить необработанный инвойс на заданную сумму
        /// </summary>
        [TestMethod]
        public void GetProcessingInvoiceTest()
        {
            _testRunner.Run<GetProcessingInvoiceTest>();
        }

        /// <summary>
        /// Провести платеж путем принятия счета на оплату
        /// </summary>
        [TestMethod]
        public void ConfirmationInvoiceTest()
        {
            _testRunner.Run<ConfirmationInvoiceTest>();
        }

        /// <summary>
        /// Тест метода для отмены ППВх
        /// </summary>
        [TestMethod]
        public void CancelInvoiceTest()
        {
            _testRunner.Run<CancelInvoiceTest>();
        }

        /// <summary>
        /// Установить номер акта для счета на оплату
        /// </summary>
        [TestMethod]
        public void SetStatementTest()
        {
            _testRunner.Run<SetStatementTest>();
        }

        /// <summary>
        /// Получить список платежей агрегаторов
        /// </summary>
        [TestMethod]
        public void GetAggregatorInvoicesListTest()
        {
            _testRunner.Run<GetAggregatorInvoicesListTest>();
        }

        /// <summary>
        /// Тест на проверку получения списка платежей агрегаторов если платеж не успешен
        /// </summary>
        [TestMethod]
        public void GetAggregatorInvoicesListWithoutSuccessPaymentsTest()
        {
            _testRunner.Run<GetAggregatorInvoicesListWithoutSuccessPaymentsTest>();
        }

        /// <summary>
        /// Получить сумму платежей в разрере платежных систем
        /// </summary>
        [TestMethod]
        public void GetPaymentSystemsMoneyAmountTest()
        {
            _testRunner.Run<GetPaymentSystemsMoneyAmountTest>();
        }

    }
}
