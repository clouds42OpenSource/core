﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для проверки реквизитов и заявок агента (создание, редактирование, удаление и т.д)
    /// </summary>
    [TestClass]
    public class PartnerDocumentTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий успешного создания реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioCreatingAgentRequisites()
        {
            _testRunner.Run<ScenarioCreatingAgentRequisites>();
        }

        /// <summary>
        /// Сценарий не успешного создания реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioErrorCreatingAgentRequisites()
        {
            _testRunner.Run<ScenarioErrorCreatingAgentRequisites>();
        }

        /// <summary>
        /// Сценарий смены статуса реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAgentRequisitesStatus()
        {
            _testRunner.Run<ScenarioChangeAgentRequisitesStatus>();
        }

        /// <summary>
        /// Сценарий ошибочной смены статуса реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioErrorChangeAgentRequisitesStatus()
        {
            _testRunner.Run<ScenarioErrorChangeAgentRequisitesStatus>();
        }

        /// <summary>
        /// Сценарий удаления реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioRemoveAgentRequisites()
        {
            _testRunner.Run<ScenarioRemoveAgentRequisites>();
        }

        /// <summary>
        /// Сценарий ошибочного удаления реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioErrorRemoveAgentRequisites()
        {
            _testRunner.Run<ScenarioErrorRemoveAgentRequisites>();
        }

        /// <summary>
        /// Сценарий смены типа реквизитов агента в режиме черновика
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAgentRequisitesType()
        {
            _testRunner.Run<ScenarioChangeAgentRequisitesType>();
        }

        /// <summary>
        /// Сценарий успешного создания заявки на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioCreatingAgentCashOutRequest()
        {
            _testRunner.Run<ScenarioCreatingAgentCashOutRequest>();
        }

        /// <summary>
        /// Сценарий не успешного создания заявки на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioErrorCreatingAgentCashOutRequest()
        {
            _testRunner.Run<ScenarioErrorCreatingAgentCashOutRequest>();
        }

        /// <summary>
        /// Сценарий успешного удаления заявки на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioRemoveAgentCashOutRequest()
        {
            _testRunner.Run<ScenarioRemoveAgentCashOutRequest>();
        }

        /// <summary>
        /// Сценарий не успешного удаления заявки на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioErrorRemoveAgentCashOutRequest()
        {
            _testRunner.Run<ScenarioErrorRemoveAgentCashOutRequest>();
        }

        /// <summary>
        /// Сценарий смены статуса заявки на вывод средств и проверки списания суммы с кошелька агента
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAgentCashOutRequestStatus()
        {
            _testRunner.Run<ScenarioChangeAgentCashOutRequestStatus>();
        }

        /// <summary>
        /// Сценарий создания двух заявок на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioAttemptCreateSecondAgentCashOutRequest()
        {
            _testRunner.Run<ScenarioAttemptCreateSecondAgentCashOutRequest>();
        }

        /// <summary>
        /// Сценарий теста смены статуса реквизитам
        /// по которым создана заявка на расходование средств
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAgentRequisitesStatusWhichCreatedAgentCashOutReqestTest()
        {
            _testRunner.Run<ScenarioChangeAgentRequisitesStatusWhichCreatedAgentCashOutReqestTest>();
        }

        /// <summary>
        /// Тест получения модели реквизитов агента для редактирования
        /// </summary>
        [TestMethod]
        public void ScenarioGetAgentRequisitesByIdTest()
        {
            _testRunner.Run<GetAgentRequisitesByIdTest>();
        }
    }
}
