﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAgentTransferBalanceRequestManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера по созданию заявки на перевод баланса агента
    /// </summary>
    [TestClass]
    public class CreateAgentTransferBalanceRequestManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки ошибочного создания заявки
        /// на перевод баланса агента
        /// </summary>
        [TestMethod]
        public void CreateAgentTransferBalanceRequestErrorTest()
        {
            _testRunner.Run<CreateAgentTransferBalanseRequestErrorTest>();
        }

        /// <summary>
        /// Сценарий теста проверки успешного создания заявки
        /// на перевод баланса агента
        /// </summary>
        [TestMethod]
        public void CreateAgentTransferBalanceRequestSuccessTest()
        {
            _testRunner.Run<CreateAgentTransferBalanceRequestSuccessTest>();
        }

        /// <summary>
        /// Сценарий теста проверки погашения ОП
        /// при создании заявки на перевод баланса агента
        /// </summary>
        [TestMethod]
        public void RepayPromisePaymentWhenCreateAgentTransferBalanceRequestTest()
        {
            _testRunner.Run<RepayPromisePaymentWhenCreateAgentTransferBalanceRequestTest>();
        }

        /// <summary>
        /// Сценарий теста проверки покупки/разблокировки заблокированных сервисов аккаунта
        /// при создании заявки на перевод баланса агента
        /// </summary>
        [TestMethod]
        public void UnlockServicesWhenCreateAgentTransferBalanceRequestTest()
        {
            _testRunner.Run<UnlockServicesWhenCreateAgentTransferBalanceRequestTest>();
        }
    }
}
