﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeFastaDemoPeriodTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на проверку изменения демо периода сервиса Fasta
    /// </summary>
    [TestClass]
    public class ChangeFastaDemoPeriodTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста на не успешное изменение демо периода
        /// для сервиса Fasta
        /// </summary>
        [TestMethod]
        public void ScenarioChangeFastaDemoPeriodErrorTest()
        {
            _testRunner.Run<ScenarioChangeFastaDemoPeriodErrorTest>();
        }

        /// <summary>
        /// Сценарий теста на успешное изменение демо периода
        /// для сервиса Fasta
        /// </summary>
        [TestMethod]
        public void ScenarioChangeFastaDemoPeriodSuccessTest()
        {
            _testRunner.Run<ScenarioChangeFastaDemoPeriodSuccessTest>();
        }
    }
}
