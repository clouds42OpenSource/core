﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimiters;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер базы на разделителях
    /// </summary>
    [TestClass]
    public sealed class ScenarioDbTemplateDelimitersTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста добавления/удаления/редактирования базы на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioCrudDbTemplatesTest()
        {
            _testRunner.Run<CrudDbTemplateDelimiters>();
        }
    }
}