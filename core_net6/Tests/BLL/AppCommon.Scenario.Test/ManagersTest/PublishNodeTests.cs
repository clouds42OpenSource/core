﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PublishNodeScenarios;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// ТЕсты на менеджер для работы с нодами публикаций
    /// </summary>
    [TestClass]
    public class PublishNodeTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки создания/удаления/редактирования ноды публикации
        /// </summary>
        [TestMethod]
        public async Task ScenarioCrudPublishNodeTest()
        {
            await _testRunner.RunAsync<CrudPublishNodeTest>();
        }

        /// <summary>
        /// Тест логирования действий с нодами публикаций
        /// </summary>
        [TestMethod]
        public async Task ScenarioPublishNodeLoggingTest()
        {
            await _testRunner.RunAsync<PublishNodeLoggingTest>();
        }

        /// <summary>
        /// Тест доступности ноды публикации
        /// </summary>
        [TestMethod]
        public async Task ScenarioCheckPublishNodeAvailabilityTest()
        {
            await _testRunner.RunAsync<CheckPublishNodeAvailabilityTest>();
        }
    }
}
