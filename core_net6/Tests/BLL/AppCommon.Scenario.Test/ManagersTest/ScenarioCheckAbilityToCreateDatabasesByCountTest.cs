﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CheckAbilityToCreateDatabasesByCount;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз по количеству
    /// </summary>
    [TestClass]
    public class ScenarioCheckAbilityToCreateDatabasesByCountTest
    {
        private readonly TestRunner _testRunner = new();


        [TestMethod]
        public void ScenarioCheckAbilityToCreateDatabasesByCount()
        {
            _testRunner.Run<ScenarioCheckAbilityToCreateDatabasesByCountWhenDemoAccount>();
        }

        [TestMethod]
        public void ScenarioCheckAbilityToCreateDatabasesByCountWhenNotDemoAccount()
        {
            _testRunner.Run<ScenarioCheckAbilityToCreateDatabasesByCountWhenNotDemoAccount>();
        }
    }
}
