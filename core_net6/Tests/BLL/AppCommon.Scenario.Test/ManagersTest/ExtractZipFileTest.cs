﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ExtractZipFile;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ExtractZipFileTest
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void LoadingIncorrectZipFileTest()
        {
            _testRunner.Run<LoadingIncorrectZipFileTest>();
        }

        [TestMethod]
        public void LoadingEmptyZipFileTest()
        {
            _testRunner.Run<LoadingEmptyZipFileTest>();
        }

        [TestMethod]
        public void RemoveUploadFileTest()
        {
            _testRunner.Run<RemoveUploadFileTest>();
        }
    }
}