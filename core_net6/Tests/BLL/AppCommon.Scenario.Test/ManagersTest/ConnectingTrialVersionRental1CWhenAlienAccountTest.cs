﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ConnectingTrialVersionRental1CWhenAlienAccountTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста подключения пробной
    /// версии аренды 1С когда находишься в чужом аккаунте
    /// </summary>
    [TestClass]
    public class ConnectingTrialVersionRental1CWhenAlienAccountTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста подключения пробной версии аренды 1С
        /// с указанием пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioConnectingTrialVersionRental1CIndicatingUserTest()
        {
            _testRunner.Run<ScenarioConnectingTrialVersionRental1CIndicatingUserTest>();
        }

        /// <summary>
        /// Сценарий теста подключения пробной версии аренды 1С
        /// без указания пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioConnectingTrialVersionRental1CWithOutUserTest()
        {
            _testRunner.Run<ScenarioConnectingTrialVersionRental1CWithOutUserTest>();
        }
    }
}
