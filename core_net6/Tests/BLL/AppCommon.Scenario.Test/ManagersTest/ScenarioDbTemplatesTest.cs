﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplates;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер шаблонов
    /// </summary>
    [TestClass]
    public sealed class ScenarioDbTemplatesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста добавления/удаления/редактирования шаблона
        /// </summary>
        [TestMethod]
        public void ScenarioCrudDbTemplatesTest()
        {
            _testRunner.Run<CrudDbTemplatesTest>();
        }
    }
}