﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseCardManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки получения данных для карточки инф. базы
    /// </summary>
    [TestClass]
    public class AccountDatabaseCardManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного и неуспешного получения технической поддержки инф. базы для карточки информационной базы
        /// </summary>
        [TestMethod]
        public void ScenarioGetDatabaseCardAcDbSupportDataSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetDatabaseCardAcDbSupportDataSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного получения основных данных карточки инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioGetDatabaseCardGeneralDataSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetDatabaseCardGeneralDataSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного получения бэкапов инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioGetDatabaseCardDbBackupsDataByFilterSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetDatabaseCardDbBackupsDataByFilterSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного получения данных доступов к инф. базам
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetDatabaseCardAcDbAccessesDataByFilterSuccessAndFail()
        {
            await _testRunner.RunAsync<ScenarioGetDatabaseCardAcDbAccessesDataByFilterSuccessAndFail>();
        }
    }
}
