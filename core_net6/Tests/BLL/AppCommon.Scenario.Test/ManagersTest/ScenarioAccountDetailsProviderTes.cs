﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails;
using Clouds42.Scenarios.Tests.Libs.Scenarios.EditAccountManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест класс для проверки получения детальной информации об аккаунте
    /// </summary>
    [TestClass]
    public class ScenarioAccountDetailsProviderTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста для проверки получения
        /// детальной информации об аккаунте
        /// </summary>
        [TestMethod]
        public void GetAccountDetailsTest()
        {            
            _testRunner.Run<GetAccountDetailsTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки редактирования названия компинии и вип статуса
        /// </summary>
        [TestMethod]
        public void ScenarioEditAccountNameAndVipStatus()
        {
            _testRunner.Run<ScenarioEditAccountName>();
        }

        /// <summary>
        /// Закрепление ответственного за аккаунт
        /// </summary>
        [TestMethod]
        public void ScenarioAttachSaleManagerToAccountTest()
        {
            _testRunner.Run<ScenarioAttachSaleManagerToAccountTest>();
        }

        /// <summary>
        /// Проверка валидации при закреплении ответственного за аккаунт
        /// </summary>
        [TestMethod]
        public void ScenarioAttachSaleManagerToAccountValidationTest()
        {
            _testRunner.Run<ScenarioAttachSaleManagerToAccountValidationTest>();
        }

        /// <summary>
        /// Отключение ответственного от аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioRemoveSaleManagerFromAccountTest()
        {
            _testRunner.Run<ScenarioRemoveSaleManagerFromAccountTest>();
        }

        /// <summary>
        /// Сценарий проверки прав на папку аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioAuditAccoutFolderAccessRules()
        {
            _testRunner.Run<ScenarioAuditAccoutFolderAccessRules>();
        }

        /// <summary>
        /// Сценарий проверки прав на папку аккаунта когда Аренда 1С заблокирована
        /// </summary>
        [TestMethod]
        public void ScenarioAuditAccountFolderAccessRulesWhenRentFrozen()
        {
            _testRunner.Run<ScenarioAuditAccountFolderAccessRulesWhenRentFrozen>();
        }

        /// <summary>
        /// Тест получения сейла для аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountSaleManagerTest()
        {
            _testRunner.Run<GetAccountSaleManagerTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки общей стоимости сервисов аккаунта,
        /// при получении детальной информации об аккаунте
        /// </summary>
        [TestMethod]
        public void ScenarioCheckServiceTotalSumWhenGettingAccountDetailsTest()
        {
            _testRunner.Run<ScenarioCheckServiceTotalSumWhenGettingAccountDetailsTest>();
        }
    }
}
