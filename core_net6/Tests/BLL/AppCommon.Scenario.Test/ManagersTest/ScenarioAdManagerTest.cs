﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioAdManagerTest
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void CreateUserTest()
        {            
            _testRunner.Run<ScenarioCreateUserinAdTest>();
        }

        [TestMethod]
        public void DeleteUserTest()
        {
            _testRunner.Run<ScenarioDeleteUserinAdTest>();
        }

        [TestMethod]
        public void SetActivateStatusTest()
        {
            _testRunner.Run<ScenarioSetActivateDeactivateStatusinAdTest>();
        }

        [TestMethod]
        public void IsValidUserTest()
        {
            _testRunner.Run<ScenarioIsValidUserinAdTest>();
        }

        [TestMethod]
        public void ChangePasswordTest()
        {
            _testRunner.Run<ScenarioChangePasswordTest>();
        }

        [TestMethod]
        public void ChangeDisplayNameTest()
        {
            _testRunner.Run<ScenarioChangeDisplayNameinAdTest>();
        }

        [TestMethod]
        public void ChangeLastNameTest()
        {
            _testRunner.Run<ScenarioChangeLastNameNameTest>();
        }

        [TestMethod]
        public void ChangeMiddleNameTest()
        {
            _testRunner.Run<ScenarioChangeMiddleNameTest>();
        }

        [TestMethod]
        public void ChangeNameTest()
        {
            _testRunner.Run<ScenarioChangeNameTest>();
        }

        [TestMethod]
        public void ChangePhoneNumberTest()
        {
            _testRunner.Run<ScenarioChangePhoneNumberTest>();
        }

        [TestMethod]
        public void CreateGroupTest()
        {
            _testRunner.Run<ScenarioCreateGroupTest>();
        }

        [TestMethod]
        public void DeleteGroupTest()
        {
            _testRunner.Run<ScenarioDeleteGroupTest>();
        }

        [TestMethod]
        public void AddToGroupTest()
        {
            _testRunner.Run<ScenarioAddToGroupTest>();
        }

        [TestMethod]
        public void AddToGroupIfNoExistTest()
        {
            _testRunner.Run<ScenarioAddToGroupIfNoExistTest>();
        }

        [TestMethod]
        public void RemoveFromGroupTest()
        {
            _testRunner.Run<ScenarioRemoveFromGroupTest>();
        }

        [TestMethod]
        public void RemoveFromGroupIfExistTest()
        {
            _testRunner.Run<ScenarioRemoveFromGroupIfExistTest>();
        }
    }
}
