﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseWebPublishPathHelperTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест проверки хелпера по работе с путями публикации инф. базы 
    /// </summary>
    [TestClass]
    public class AccountDatabaseWebPublishPathHelperTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки получения пути публикации
        /// для базы на разделителях
        /// </summary>
        [TestMethod]
        public void GetPublishPathForDbOnDelimitersTest()
        {
            _testRunner.Run<GetPublishPathForDbOnDelimitersTest>();
        }
    }
}
