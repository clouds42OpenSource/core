﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для получения аккаунтов на отображение
    /// </summary>
    [TestClass]
    public class ScenarioGetAccountsViewModel
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста c ложными статусами UserAccount при выборке
        /// </summary>
        [TestMethod]
        public void ScenarioWithFalseUserAccountStatusesTest()
        {
            _testRunner.Run<ScenarioWithFalseUserAccountStatusesTest>();
        }

        /// <summary>
        /// Сценарий теста когда нужны только vip аккаунты
        /// </summary>
        [TestMethod]
        public void ScenarioWhenNeededVipAccountsTest()
        {
            _testRunner.Run<ScenarioWhenNeededVipAccountsTest>();
        }

        /// <summary>
        /// Сценарий теста когда нужны аккаунты только для менеджера продаж
        /// </summary>
        [TestMethod]
        public void ScenarioWhenYouNeedAccountsOnlyForSaleManagerTest()
        {
            _testRunner.Run<ScenarioWhenYouNeedAccountsOnlyForSaleManagerTest>();
        }

        /// <summary>
        /// Сценарий теста когда фильтр выборки содержит персональные данные
        /// </summary>
        [TestMethod]
        public void ScenarioWhenFilterSampleContainsPersonalDataTest()
        {
            _testRunner.Run<ScenarioWhenFilterSampleContainsPersonalDataTest>();
        }

        /// <summary>
        /// Сценарий теста когда фильтр выборки содержит временный интервал
        /// </summary>
        [TestMethod]
        public void ScenarioWhenFilterSampleContainsTimeGapsTest()
        {
            _testRunner.Run<ScenarioWhenFilterSampleContainsTimeGapsTest>();
        }

        /// <summary>
        /// Сценарий теста когда надо получить аккаунт по счету
        /// </summary>
        [TestMethod]
        public void ScenarioWhenYouNeedToGetInvoiceAccountTest()
        {
            _testRunner.Run<ScenarioWhenYouNeedToGetInvoiceAccountTest>();
        }
    }
}
