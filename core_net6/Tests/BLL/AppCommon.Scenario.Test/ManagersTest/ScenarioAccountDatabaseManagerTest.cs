﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioAccountDatabaseManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки на остаток каталогов после отмены публикации базы
        /// </summary>
        [TestMethod]
        public void CancelPublish_And_CheckFolders()
        {            
            _testRunner.Run<UnpublishDatabaseAndCheckIISFoldersTest>();
        }

        /// <summary>
        /// Сценарий проверки на остаток каталогов после удаления опубликованой базы
        /// </summary>
        [TestMethod]
        public void DeletePublishDb_And_CheckFolders()
        {            
            _testRunner.Run<DeleteDatabaseAndCheckIisFoldersTest>();
        }

        /// <summary>
        /// Сценарий проверки на нахождение базы по указанному пути
        /// </summary>
        [TestMethod]
        public void СheckAccountDatabaseFilePath()
        {
            _testRunner.Run<AccountDatabaseFilePathTest>();
        }

        /// <summary>
        /// Сценарий проверки строки подключения к базе
        /// </summary>
        [TestMethod]
        public void СheckAccountDatabaseConnectionString()
        {
            _testRunner.Run<AccountDatabaseConnectionStringTest>();
        }

        /// <summary>
        /// Смена типа дистрибутива если есть указана альфа версия в сегменте
        /// </summary>
        [TestMethod]
        public async Task ChangeDbDistributionTypeTest()
        {
            await _testRunner.RunAsync<ChangeDbDistributionTypeTest>();
        }

        /// <summary>
        /// Смена дистрибутива для опубликованной базы при условии что указана альфа версия в сегменте
        /// </summary>
        [TestMethod]
        public async Task ChangeDistributionTypeForPublishedDbTest()
        {
            await _testRunner.RunAsync<ChangeDistributionTypeForPublishedDbTest>();
        }

        /// <summary>
        /// Смена типа информационной базы с серверной на файловую и обратно
        /// </summary>
        [TestMethod]
        public void ChangeAccountDatabaseTypeTest()
        {
            _testRunner.Run<ChangeAccountDatabaseTypeTest>();
        }

        /// <summary>
        /// Сценарий проверки смены модели восстановления для инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAccountDatabaseRestoreModelType()
        {
            _testRunner.Run<ScenarioChangeAccountDatabaseRestoreModelType>();
        }

        /// <summary>
        /// Сценарий проверки смены модели восстановления для инф. базы на полную
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAccountDatabaseRestoreModelTypeToFull()
        {
            _testRunner.Run<ScenarioChangeAccountDatabaseRestoreModelTypeToFull>();
        }

        /// <summary>
        /// Сценарий проверки работы джобы аудита и корректировки модели восстановления инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioCheckManageAcDbRestoreModelJob()
        {
            _testRunner.Run<ScenarioCheckManageAcDbRestoreModelJob>();
        }

        /// <summary>
        /// Проверка результата удаления базы в статусе "Ошибка при создании"
        /// </summary>
        [TestMethod]
        public void DeleteDatabaseWithStatusOtherThanReadyTest()
        {
            _testRunner.Run<DeleteDatabaseWithStatusOtherThanReadyTest>();
        }

        /// <summary>
        /// Проверка результата удаления базы когда папка базы пуска
        /// </summary>
        [TestMethod]
        public void DeleteDatabaseInEmptyFolderTest()
        {
            _testRunner.Run<DeleteDatabaseInEmptyFolderTest>();
        }

        /// <summary>
        /// Проверка метода сохранения локальной БД аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckSavingLocalAccountDb()
        {
            _testRunner.Run<ScenarioCheckSavingLocalAccountDb>();
        }

        /// <summary>
        /// Тест проверяет редактирование номер БД V82Name
        /// </summary>
        [TestMethod]
        public void ChangeAccountDatabaseV82NameTest()
        {
            _testRunner.Run<ChangeAccountDatabaseV82NameTest>();
        }

        /// <summary>
        /// Сценарий теста проверки поля "База на разделителях должна использовать вэб сервис"
        /// при получении данных по инф. базе
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDelimiterDatabaseMustUseWebServiceAtGetAccountDatabasesByIdTest()
        {
            _testRunner.Run<ScenarioCheckDelimiterDatabaseMustUseWebServiceAtGetAccountDatabasesByIdTest>();
        }

        /// <summary>
        /// Сценарий теста проверки поля "Версия платформы на разделителях"
        /// при получении данных по инф. базе
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdTest()
        {
            _testRunner.Run<ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdTest>();
        }

        /// <summary>
        /// Сценарий теста проверки поля "Версия платформы на разделителях"
        /// при получении данных по демо базе на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdWhenDbIsDemoTest()
        {
            _testRunner.Run<ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdWhenDbIsDemoTest>();
        }

        /// <summary>
        /// Сценарий теста проверки поля "Адрес публикации"
        /// при получении данных базе на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDelimiterPublicationAddressAtGetAccountDatabasesByIdTest()
        {
            _testRunner.Run<ScenarioCheckDelimiterPublicationAddressAtGetAccountDatabasesByIdTest>();
        }

        /// <summary>
        /// Тест установки даты последней активности в информационной базе
        /// </summary>
        [TestMethod]
        public void ScenarioSetLastActivityDateByDatabaseIdTest()
        {
            _testRunner.Run<SetLastActivityDateByDatabaseIdTest>();
        }

        /// <summary>
        /// Тест фиксации последней активности запуска инф. базы или RDP
        /// </summary>
        [TestMethod]
        public void ScenarioFixationDatabaseLaunchActivityTest()
        {
            _testRunner.Run<FixationDatabaseLaunchActivityTest>();
        }
    }
}
