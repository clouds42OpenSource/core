﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера очереди задач воркера
    /// </summary>
    [TestClass]
    public class CoreWorkerTasksQueueManager
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий добавления задачи воркера в очередь
        /// </summary>
        [TestMethod]
        public void ScenarioAddCoreWorkerTaskTest()
        {
            _testRunner.Run<ScenarioAddCoreWorkerTaskTest>();
        }

        /// <summary>
        /// Сценарий проверки списка задач воркера (для указанной задачи не будут видны параметры)
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCoreWorkerTasksQueueViewModelTest()
        {
            _testRunner.Run<ScenarioCheckCoreWorkerTasksQueueViewModelTest>();
        }

        /// <summary>
        /// Сценарий проверки выбора списка задач в очереди
        /// </summary>
        [TestMethod]
        public void ScenarioSelectCoreWorkerTaskQueueTest()
        {
            _testRunner.Run<ScenarioSelectCoreWorkerTaskQueueTest>();
        }

        /// <summary>
        /// Сценарий выборки задач
        /// </summary>
        [TestMethod]
        public void ScenarioSelectCoreWorkerTasksTest()
        {
            _testRunner.Run<ScenarioSelectCoreWorkerTasksTest>();
        }

        /// <summary>
        /// Сценарий выборки задач связанных с воркером
        /// </summary>
        [TestMethod]
        public void ScenarioSelectCoreWorkerTaskBagsTest()
        {
            _testRunner.Run<ScenarioSelectCoreWorkerTaskBagsTest>();
        }
    }
}
