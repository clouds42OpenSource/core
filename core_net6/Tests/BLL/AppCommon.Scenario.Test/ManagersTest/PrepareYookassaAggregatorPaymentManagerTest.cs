﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.PrepareYookassaAggregatorPaymentManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по подготовке платежа 
    /// агрегатора ЮKassa для оплаты
    /// </summary>
    [TestClass]
    public class PrepareYookassaAggregatorPaymentManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста по подготовке платежа 
        /// агрегатора ЮKassa с ошибкой(не успешные кейсы) 
        /// </summary>
        [TestMethod]
        public void ScenarioPrepareYookassaPaymentWithErrorTest()
        {
            _testRunner.Run<ScenarioPrepareYookassaPaymentWithErrorTest>();
        }

        /// <summary>
        /// Сценарий теста по подготовке платежа 
        /// агрегатора ЮKassa(успешный кейс)
        /// </summary>
        [TestMethod]
        public void ScenarioPrepareYookassaPaymentWithSuccessTest()
        {
            _testRunner.Run<ScenarioPrepareYookassaPaymentWithSuccessTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки модели подтверждения платежа,
        /// при подготовке платежа агрегатора ЮKassa
        /// </summary>
        [TestMethod]
        public void ScenarioCheckConfirmationDataAtPrepareYookassaPaymentTest()
        {
            _testRunner.Run<ScenarioCheckConfirmationDataAtPrepareYookassaPaymentTest>();
        }
    }
}
