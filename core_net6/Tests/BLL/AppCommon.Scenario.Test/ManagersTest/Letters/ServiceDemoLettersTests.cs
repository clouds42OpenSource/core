﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodEndedPaymentRequiredLetter;
using Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodIsComingToEndLetter;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest.Letters
{
    /// <summary>
    /// Тесты менеджер для работы с шаблоном письма
    /// </summary>
    [TestClass]
    public class ServiceDemoLettersTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест на отправку письма, о том, что демо период скоро закончиться
        /// </summary>
        [TestMethod]
        public void DemoPeriodIsComingToEndLetterTest()
        {
            _testRunner.Run<DemoPeriodIsComingToEndLetterTest>();
        }

        /// <summary>
        /// Тест на отправку письма, о том, что демо период закончился нужна оплата
        /// </summary>
        [TestMethod]
        public void DemoPeriodEndedPaymentRequiredLetterTest()
        {
            _testRunner.Run<DemoPeriodEndedPaymentRequiredLetterTest>();
        }
        
    }
}
