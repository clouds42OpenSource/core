﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.StateMachine;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки работы механизма отката
    /// </summary>
    [TestClass]
    public class StateMachineTest
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void DeleteUserWithErrors()
        {
            _testRunner.Run<ScenarioDeleteUserWithErrors>();
        }
    }
}
