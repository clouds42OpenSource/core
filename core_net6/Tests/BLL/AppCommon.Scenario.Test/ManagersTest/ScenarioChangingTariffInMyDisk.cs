﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk;
using Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioChangingTariffInMyDisk
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий добавления дискового пространства при активном
        /// сервисе и недостаточном балансе
        /// </summary>
        [TestMethod]
        public void IncreaseSizeWithZeroBalance()
        {
            _testRunner.Run<IncreaseSizeWithZeroBalanceTest>();
        }

        /// <summary>
        /// Сценарий добавления дискового пространства при активном
        /// сервисе и при достаточном балансе
        /// </summary>
        [TestMethod]
        public void IncreaseSizeWithMoneyOnBalance()
        {
            _testRunner.Run<IncreaseSizeWithMoneyOnBalanceTest>();
        }

        /// <summary>
        /// Сценарий уменьшения дискового пространства при активном сервисе 
        /// </summary>
        [TestMethod]
        public void DecreaseSizeWithMoneyAndActiveService()
        {
            _testRunner.Run<DecreaseSizeWithMoneyAndActiveServiceTest>();
        }

        /// <summary>
        /// Сценарий уменьшения дискового пространства при неактивном сервисе + баланса 
        /// достаточно для продления измененной подписки
        /// </summary>
        [TestMethod]
        public void DecreaseSizeWithMoneyAndFrozenService()
        {
            _testRunner.Run<DecreaseSizeWithMoneyAndFrozenServiceTest>();
        }

        /// <summary>
        /// Сценарий добавления дискового пространства при заблокированом
        /// сервисе и достаточном балансе
        /// </summary>
        [TestMethod]
        public void IncreaseSizeWithFrozenServiceAndEnoughMoney()
        {
            _testRunner.Run<IncreaseSizeWithFrozenServiceAndEnoughMoney>();
        }
        
        /// <summary>
        /// Проверка блокировки сейлменеджеру править "мой диск" и "Аренда"
        /// в чужом аккаунте.
        /// </summary>
        [TestMethod]
        public void ScenarioChangingLicenseInAlienAccAsSaleManagerTest()
        {
            _testRunner.Run<ScenarioChangingLicenseInAlienAccAsSaleManagerTest>();
        }
        
        /// <summary>
        /// Сценарий смены тарифа диска за бонусы
        /// </summary>
        [TestMethod]
        public void ScenarioChangeMyDiskTariffForBonuses()
        {
            _testRunner.Run<ChangeMyDiskTariffForBonuses>();
        }

        /// <summary>
        /// Сценарий смены тарифа диска за бонусы и деньги
        /// </summary>
        [TestMethod]
        public void ScenarioChangingMyDiskTariffByBonusAndMoneyBalance()
        {
            _testRunner.Run<ChangingMyDiskTariffByBonusAndMoneyBalance>();
        }

        /// <summary>
        /// Смена тарифа сервиса Мой диск (оплата тарифа обещанным платежом)
        /// </summary>
        [TestMethod]
        public void ScenarioChangeMyDiskTariffByPromisePayment()
        {
            _testRunner.Run<ChangeMyDiskTariffByPromisePayment>();
        }

        /// <summary>
        /// Начисление агентского вознаграждения при смене тарифа севиса Мой диск
        /// </summary>
        [TestMethod]
        public void ScenarioAgentRewardWhenReferralChangeMyDiskTariff()
        {
            _testRunner.Run<AgentRewardWhenReferralChangeMyDiskTariff>();
        }

    }
}
