﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки запусков процессов с действиями над инф. базами
    /// </summary>
    [TestClass]
    public class StartProcessToWorkDatabaseManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешной и неуспешной проверки запуска процессов удаления инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioStartProcessOfRemoveDatabaseSuccessAndFail()
        {
            _testRunner.Run<ScenarioStartProcessOfRemoveDatabaseSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешной и неуспешной проверки запуска процессов архивации инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioStartProcessOfArchiveDatabaseToTombSuccessAndFail()
        {
            _testRunner.Run<ScenarioStartProcessOfArchiveDatabaseToTombSuccessAndFail>();
        }

        /// <summary>
        /// Сценари для успешного запуска процесса на выдачу внутренних доступов в инф. базу
        /// </summary>
        [TestMethod]
        public void ScenarioStartProcessesOfGrandInternalAccessesSuccess()
        {
            _testRunner.Run<ScenarioStartProcessesOfGrandInternalAccessesSuccess>();
        }

        /// <summary>
        /// Сценари для успешного запуска процесса на выдачу внутренних доступов в инф. базу
        /// </summary>
        [TestMethod]
        public void ScenarioStartProcessesOfGrandInternalAccessesFail()
        {
            _testRunner.Run<ScenarioStartProcessesOfGrandInternalAccessesFail>();
        }

        /// <summary>
        /// Сценари для успешного и неуспешного запуска процесса на удаление внутренних доступов в инф. базу
        /// </summary>
        [TestMethod]
        public void ScenarioStartProcessesOfRemoveAccessesSuccessAndFail()
        {
            _testRunner.Run<ScenarioStartProcessesOfRemoveAccessesSuccessAndFail>();
        }
    }
}
