﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserDataManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера
    /// по работе с данными пользователя
    /// </summary>
    [TestClass]
    public class ScenarioAccountUserDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста получения хеша пароля по логину
        /// </summary>
        [TestMethod]
        public void ScenarioGetPasswordHashByLoginTest()
        {
            _testRunner.Run<ScenarioGetPasswordHashByLoginTest>();
        }
    }
}
