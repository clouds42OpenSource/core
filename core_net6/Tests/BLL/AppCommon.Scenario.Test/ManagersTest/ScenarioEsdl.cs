﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ESDL;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioEsdl
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public async Task BuyingPagesWithoutLicenseWhenRegisterTest()
        {
            await _testRunner.RunAsync<BuyingPagesWithoutLicenseWhenRegisterTest>();
        }

	    [TestMethod]
	    public async Task BuyingLicenseAndPagesWhenLockServiceTest()
	    {
		    await _testRunner.RunAsync<BuyingLicenseAndPagesWhenLockServiceTest>();
	    }

        /// <summary>
        /// Покупка года Rec и 1000 страниц сразу после регистрации
        /// </summary>
	    [TestMethod]
	    public async Task BuyingLicenseAndPagesWhenRegisterTest()
	    {
            await _testRunner.RunAsync<BuyingLicenseAndPagesWhenRegisterTest>();
	    }


	    [TestMethod]
	    public async Task BuyingLicenseWithoutPagesWhenLockServiceTest()
	    {
            await _testRunner.RunAsync<BuyingLicenseWithoutPagesWhenLockServiceTest>();
	    }


	    [TestMethod]
	    public async Task BuyingLicenseWithoutPagesWhenRegisterTest()
	    {
            await _testRunner.RunAsync<BuyingLicenseWithoutPagesWhenRegisterTest>();
	    }

	    [TestMethod]
	    public async Task BuyingPagesWithoutLicenseWhenLockServiceTest()
	    {
		    await _testRunner.RunAsync<BuyingPagesWithoutLicenseWhenLockServiceTest>();
	    }

	    [TestMethod]
	    public async Task BuyingPagesWithoutLicenseWhenNoMoneyTes()
	    {
            await _testRunner.RunAsync<BuyingPagesWithoutLicenseWhenNoMoneyTes>();
	    }

	    [TestMethod]
	    public void DocLoaderByRent1CProlongationJobTest()
	    {
		    _testRunner.Run<DocLoaderByRent1CProlongationJobTest>();
	    }

	    [TestMethod]
	    public void AutoIncreseDocLoaderResourcesTest()
	    {
		    _testRunner.Run<AutoIncreseDocLoaderResourcesTest>();
	    }

        [TestMethod]
        public async Task PaymenThePromisePaymentAndBuyFasta()
        {
            await _testRunner.RunAsync<PaymenThePromisePaymentAndBuyFasta>();
        }

    }
}
