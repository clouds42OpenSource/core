﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioPartnerBillingServicesDataManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера данных партерских сервисов
    /// </summary>
    [TestClass]
    public class TestForPartnerBillingServicesDataManager
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Получить список услуг партнерского сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetServiceTypesList()
        {
            _testRunner.Run<ScenarioGetServiceTypesList>();
        }

        /// <summary>
        /// Получить детальную информацию о услуге сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetServiceTypeInfo()
        {
            _testRunner.Run<ScenarioGetServiceTypeInfo>();
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента(По Id клиента)
        /// </summary>
        [TestMethod]
        public void ScenarioGetServiceTypesStateForUser()
        {
            _testRunner.Run<ScenarioGetServiceTypesStateForUser>();
        }

        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента (По логину клиента)
        /// </summary>
        [TestMethod]
        public void ScenarioGetServiceTypeStateForUserByLogin()
        {
            _testRunner.Run<ScenarioGetServiceTypeStateForUserByLogin>();
        }

        /// <summary>
        /// Проверить статус указанной услуги сервиса у клиента
        /// </summary>
        [TestMethod]
        public void ScenarioCheckServiceTypeStatusForUserByLogin()
        {
            _testRunner.Run<ScenarioCheckServiceTypeStatusForUserByLogin>();
        }
    }
}
