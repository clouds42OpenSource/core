﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios._1cConfigurations;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер конфигураций 1С
    /// </summary>
    [TestClass]
    public class Configurations1CManagerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест добавления/удаления/редактирования новой конфигурации 1С
        /// </summary>
        [TestMethod]
        public async Task ScenarioCrudConfigurations1CTest()
        {
            await _testRunner.RunAsync<CrudConfigurations1CTest>();
        }

        /// <summary>
        /// Тест валидации названия конфигурации, чтобы не было повторений в базе
        /// </summary>
        [TestMethod]
        public void ScenarioCheckConfigurationNameTest()
        {
            _testRunner.Run<CheckConfigurationNameTest>();
        }
    }
}
