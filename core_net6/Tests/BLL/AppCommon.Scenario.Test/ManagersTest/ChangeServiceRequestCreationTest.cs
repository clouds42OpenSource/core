﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки создания
    /// заявки сервиса на модерацию
    /// </summary>
    [TestClass]
    public class ChangeServiceRequestCreationTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки создания заявки сервиса на модерацию,
        /// при создании сервиса 
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCreateServiceRequestWhenCreatingServiceTest()
        {
            _testRunner.Run<ScenarioCheckCreateServiceRequestWhenCreatingServiceTest>();
        }

        /// <summary>
        /// Сценарий теста проверки  создания заявки сервиса на модерацию,
        /// при отправке черновика сервиса на модерацию
        /// </summary>
        [TestMethod]
        public void ScenarioCheckSendingServiceDraftToModerationTest()
        {
            _testRunner.Run<ScenarioCheckSendingServiceDraftToModerationTest>();
        }

        /// <summary>
        /// Сценарий теста, по проверке полей заявки на редактирование,
        /// с типом редактирование 'Редактирование'
        /// </summary>
        [TestMethod]
        public void ScenarioCheckEditServiceRequestFieldsWithTypeEditingTest()
        {
            _testRunner.Run<ScenarioCheckEditServiceRequestFieldsWithTypeEditingTest>();
        }

        /// <summary>
        /// Сценарий теста, по проверке полей заявки на редактирование,
        /// с типом 'Editing'
        /// </summary>
        [TestMethod]
        public void ScenarioCheckEditServiceRequestFieldsWithTypeUpdateTest()
        {
            _testRunner.Run<ScenarioCheckEditServiceRequestFieldsWithTypeUpdateTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке создания заявки на редактирование сервиса,
        /// при редактировании сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckEditServiceRequestWhenEditingServiceTest()
        {
            _testRunner.Run<ScenarioCheckEditServiceRequestWhenEditingServiceTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке создания заявки на редактирование сервиса,
        /// при редактировании услуг сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckEditServiceRequestWhenEditingServiceTypesTest()
        {
            _testRunner.Run<ScenarioCheckEditServiceRequestWhenEditingServiceTypesTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке создания заявки на редактирование сервиса,
        /// при удалении услуг сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckEditServiceRequestWhenDeleteServiceTypesTest()
        {
            _testRunner.Run<ScenarioCheckEditServiceRequestWhenDeleteServiceTypesTest>();
        }
    }
}
