﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabaseDataManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для менеджера данных для создания инф. баз
    /// </summary>
    [TestClass]
    public class CreateAccountDatabaseDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для проверки получения данных для создания инф. баз из шаблона
        /// </summary>
        [TestMethod]
        public void ScenarioGetDataForCreateAcDbFromTemplate()
        {
            _testRunner.Run<ScenarioGetDataForCreateAcDbFromTemplate>();
        }

        /// <summary>
        /// Сценарий для проверки получения списка пользователей для предоставления доступа
        /// </summary>
        [TestMethod]
        public void ScenarioGetAvailableUsersForGrantAccess()
        {
            _testRunner.Run<ScenarioGetAvailableUsersForGrantAccess>();
        }

        /// <summary>
        /// Сценарий для успешного получения списка пользователей для предоставления доступа в базу восстанавливаемую из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioGetAvailableUsersForGrantAccessToRestoringDbSuccess()
        {
            _testRunner.Run<ScenarioGetAvailableUsersForGrantAccessToRestoringDbSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного получения списка пользователей для предоставления доступа в базу восстанавливаемую из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioGetAvailableUsersForGrantAccessToRestoringDbFail()
        {
            _testRunner.Run<ScenarioGetAvailableUsersForGrantAccessToRestoringDbFail>();
        }
    }
}
