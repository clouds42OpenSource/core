﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера данных биллинга
    /// </summary>
    [TestClass]
    public class BillingDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест на метод получения данных для формы пополнения баланса
        /// </summary>
        [TestMethod]
        public void ScenarioGetDataForReplenishBalanceForVipAccountTest()
        {
            _testRunner.Run<ScenarioGetDataForReplenishBalanceForVipAccountTest>();
        }

        /// <summary>
        /// Тест на получение разницы суммы бонусного и денежного баланса для страницы
        /// </summary>
        [TestMethod]
        public void ScenarioGetPageDifferenceSumTest()
        {
            _testRunner.Run<ScenarioGetPageDifferenceSumTest>();
        }

        /// <summary>
        /// Тест на получение разницы суммы бонусного и денежного счета при изменении фильтра по дате
        /// </summary>
        [TestMethod]
        public void ScenarioGetDateDifferenceSumTest()
        {
            _testRunner.Run<ScenarioGetDateDifferenceSumTest>();
        }

    }
}
