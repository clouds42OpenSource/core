﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.RecalculateServiceCostAfterChangesTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки пересчета стоимости
    /// сервиса после изменений
    /// </summary>
    [TestClass]
    public class RecalculateServiceCostAfterChangesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверке пересчета
        /// стоимости сервиса для аккаунтов после изменений
        /// </summary>
        [TestMethod]
        public async Task ScenarioCheckRecalculateServiceCostAfterChangesTest()
        {
            await _testRunner.RunAsync<ScenarioCheckRecalculateServiceCostAfterChangesTest>();
        }
    }
}
