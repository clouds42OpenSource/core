﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AgentCashOutRequestDataManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста проверки менеджера
    /// по работе с данными агентских заявок
    /// </summary>
    [TestClass]
    public class AgentCashOutRequestDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста получения заявок на расходование средств в статусе "Новая"
        /// </summary>
        [TestMethod]
        public void ScenarioGetAgentCashOutRequestsInStatusNewTest()
        {
            _testRunner.Run<ScenarioGetAgentCashOutRequestsInStatusNewTest>();
        }

        /// <summary>
        /// Получить сумму вознаграждения агента, доступную для выплаты
        /// </summary>
        [TestMethod]
        public void ScenarioGetAvailableSumForCashOutRequestTest()
        {
            _testRunner.Run<GetAvailableSumForCashOutRequestTest>();
        }

        /// <summary>
        /// Тест получения списка активных реквизитов агента
        /// </summary>
        [TestMethod]
        public void ScenarioGetActiveAgentRequisitesTest()
        {
            _testRunner.Run<GetActiveAgentRequisitesTest>();
        }

        /// <summary>
        /// Проверить возможность создания заявки на вывод
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAbilityToCreateCashOutRequestTest()
        {
            _testRunner.Run<CheckAbilityToCreateCashOutRequestTest>();
        }
    }
}
