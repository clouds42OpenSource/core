﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CloudBackupStorage;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер по работе с хранилищами бэкапов
    /// </summary>
    [TestClass]
    public class CloudBackupStorageTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки создания/удаления/редактирования хранилища бэкапов
        /// </summary>
        [TestMethod]
        public void ScenarioCrudCloudBackupStorage()
        {
            _testRunner.Run<CrudCloudBackupStorageTest>();
        }

        /// <summary>
        /// Тест проверки фильтра при получении хранилищ бекапов
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetFilteredCloudBackupStorage()
        {
            await _testRunner.RunAsync<GetFilteredCloudBackupStorageTest>();
        }
    }
}
