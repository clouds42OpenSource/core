﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CloudTerminalServerSessions;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{

    /// <summary>
    /// Тест проверки методов CloudTerminalServerSessionsManager
    /// </summary>
    [TestClass]
    public class CloudTerminalServerSessionsTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Получение и звершение сессий пользователей
        /// </summary>
        [TestMethod]
        public void ScenarioGetUserRdpSessionsTest()
        {
            _testRunner.Run<ScenarioGetAndCloseUserRdpSessionsTest>();
        }
    }
}
