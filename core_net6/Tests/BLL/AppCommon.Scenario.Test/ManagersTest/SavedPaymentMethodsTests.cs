﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class SavedPaymentMethodsTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Получить способы оплат (успех при 0 количестве сохраненных способов оплат)
        /// </summary>
        [TestMethod]
        public void GetSavedPaymentMethods()
            => _testRunner.Run<ScenarioGetSavedPaymentMethodsTest>();

        /// <summary>
        /// Получить способы оплат
        /// </summary>
        [TestMethod]
        public void GetSavedPaymentMethodsExistSavedPaymentMethod()
            => _testRunner.Run<ScenarioGetSavedPaymentMethodsExistSavedPaymentMethodTest>();

        /// <summary>
        /// Удалить сохраненный способ оплаты
        /// </summary>
        [TestMethod]
        public void DeleteSavedPaymentMethod()
            => _testRunner.Run<ScenarioDeleteSavedPaymentMethodTest>();

        /// <summary>
        /// Удалить сохраненный способ оплаты с неверным идентификатором способа оплаты
        /// </summary>
        [TestMethod]
        public void DeleteSavedPaymentMethodWithInvalidId()
            => _testRunner.Run<ScenarioDeleteSavedPaymentMethodWithInvalidIdTest>();

        /// <summary>
        /// Установить способ оплаты по умолчанию для автоплатежей
        /// </summary>
        [TestMethod]
        public void MakeDefaultSavedPaymentMethod()
            => _testRunner.Run<ScenarioMakeDefaultSavedPaymentMethodTest>();

        /// <summary>
        /// Установить способ оплаты по умолчанию для автоплатежей с неверным идентификатором способа оплаты
        /// </summary>
        [TestMethod]
        public void MakeDefaultSavedPaymentMethodWithInvalidPaymentMethodId()
            => _testRunner.Run<ScenarioMakeDefaultSavedPaymentMethodWithInvalidPaymentMethodIdTest>();

        /// <summary>
        /// Установить способ оплаты по умолчанию для автоплатежей с неверным идентификатором аккаунта
        /// </summary>
        [TestMethod]
        public void MakeDefaultSavedPaymentMethodWithInvalidAccountId()
            => _testRunner.Run<ScenarioMakeDefaultSavedPaymentMethodWithInvalidAccountIdTest>();

    }
}
