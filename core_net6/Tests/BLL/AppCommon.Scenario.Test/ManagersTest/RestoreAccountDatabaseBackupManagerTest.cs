﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.RestoreAccountDatabaseBackupManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки восстановления инф. базы из бэкапа
    /// </summary>
    [TestClass]
    public class RestoreAccountDatabaseBackupManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для проверки успешного восстановления инф. базы из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioRestoreDbFromBackupSuccess()
        {
            _testRunner.Run<ScenarioRestoreDbFromBackupSuccess>();
        }

        /// <summary>
        /// Сценарий для проверки неуспешного восстановления инф. базы из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioRestoreDbFromBackupFail()
        {
            _testRunner.Run<ScenarioRestoreDbFromBackupFail>();
        }
    }
}
