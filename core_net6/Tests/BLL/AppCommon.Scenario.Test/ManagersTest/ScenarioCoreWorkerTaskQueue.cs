﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты для CoreWorkerTaskQueue и CoreWorkerTaskQueueLive
    /// </summary>
    [TestClass]
    public class ScenarioCoreWorkerTaskQueue
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания задачи в очереди
        /// </summary>
        [TestMethod]
        public void ScenarioAddCoreWorkerTaskQueue() 
            => _testRunner.Run<AddCoreWorkerTaskQueueTestScenario>();

        /// <summary>
        /// Тест добавления комментария к задачи в очереди
        /// </summary>
        [TestMethod]
        public void ScenarioSetCommentToCoreWorkerTaskQueue()
            => _testRunner.Run<SetCommentForCoreWorkerTaskQueueScenario>();

        /// <summary>
        /// Тест проверки метода АПИ, возвращающего Id задач, по статусу
        /// </summary>
        [TestMethod]
        public void ScenarioGetTasksByStatusFromTaskQueue() 
            => _testRunner.Run<GetTasksByStatusFromQueueTestScenario>();
        
        /// <summary>
        /// Тест проверки удаления задач из очереди
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDeleteTasksFromTaskQueue()
            => _testRunner.Run<CheckDeleteTasksFromTaskQueueLiveTestScenario>();

        /// <summary>
        /// Тест проверки захвата задач из очереди
        /// </summary>
        [TestMethod]
        public async Task CaptureCoreWorkerTaskQueue()
            => await _testRunner.RunAsync<CaptureCoreWorkerTaskQueueTestScenario>();
    }
}
