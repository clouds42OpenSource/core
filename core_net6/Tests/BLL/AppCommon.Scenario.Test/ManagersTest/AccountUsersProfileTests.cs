﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ResetPassword;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера профилей пользователей
    /// </summary>
    [TestClass]
    public class AccountUsersProfileTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест установки нового пароля для пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioSetNewPasswordTest()
        {
            _testRunner.Run<SetNewPasswordTest>();
        }

        /// <summary>
        /// Тест установуи пароля для пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioSetPasswordTest()
        {
            _testRunner.Run<SetPasswordTest>();
        }

        /// <summary>
        /// Тест установки логина для пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioSetLoginTest()
        {
            _testRunner.Run<SetLoginTest>();
        }

        /// <summary>
        /// Тест сброса пароля для пользователя
        /// </summary>
        [TestMethod]
        public void ResetPasswordTest()
        {
            _testRunner.Run<ResetPasswordTest>();
        }
    }
}
