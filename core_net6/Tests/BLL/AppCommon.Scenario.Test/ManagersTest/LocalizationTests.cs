﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Localization;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты локализации
    /// </summary>
    [TestClass]
    public class LocalizationTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки локализованных фраз
        /// </summary>
        [TestMethod]
        public void ScenarioCompareLocalizedPhrasesTest()
        {
            _testRunner.Run<CompareLocalizedPhrasesTest>();
        }

        /// <summary>
        /// Тест механизма локализации
        /// </summary>
        [TestMethod]
        public void ScenarioCompareLocalizedPhrasesBeforeAndAfterPerformingLocalizationTest()
        {
            _testRunner.Run<CompareLocalizedPhrasesBeforeAndAfterPerformingLocalizationTest>();
        }

        /// <summary>
        /// Тест проверки локализованного значения сервиса Аренда 1С
        /// </summary>
        [TestMethod]
        public void ScenarioBillingServiceNameLocalizationTest()
        {
            _testRunner.Run<BillingServiceNameLocalizationTest>();
        }

        /// <summary>
        /// Тест редактивроания конфигураций локалей
        /// </summary>
        [TestMethod]
        public void ScenarioEditLocaleConfigurationTest()
        {
            _testRunner.Run<EditLocaleConfigurationTest>();
        }

        /// <summary>
        /// Тест получения конфигураций локалей по фильтру
        /// </summary>
        [TestMethod]
        public void ScenarioGetFilteredLocaleConfigurationsTest()
        {
            _testRunner.Run<GetFilteredLocaleConfigurationsTest>();
        }
    }
}
