﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по обработке
    /// результата модерации заявки на изменения сервиса
    /// </summary>
    [TestClass]
    public class ModerationResultManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста процесса по отклонению заявки
        /// на изменения сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioRejectChangeServiceRequestProcessTest()
        {
            _testRunner.Run<ScenarioRejectChangeServiceRequestProcessTest>();
        }

        /// <summary>
        /// Сценарий теста процесса
        /// по принятию заявки на создание сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioApplyCreateServiceRequestProcessTest()
        {
            _testRunner.Run<ScenarioApplyCreateServiceRequestProcessTest>();
        }

        /// <summary>
        /// Сценарий теста процесса
        /// по принятию заявки на редактирование сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioApplyEditServiceRequestProcessTest()
        {
            _testRunner.Run<ScenarioApplyEditServiceRequestProcessTest>();
        }

        /// <summary>
        /// Сценарий теста проверки процесса по отклонению заявки
        /// на создание сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioRejectCreateServiceRequestTest()
        {
            _testRunner.Run<ScenarioRejectCreateServiceRequestTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
        /// при редактировании услуг сервиса (Название, Описание)
        /// </summary>
        [TestMethod]
        public void ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypesTest()
        {
            _testRunner.Run<ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypesTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке отклонения заявки на редактирование сервиса,
        /// при редактировании услуг сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckRejectEditServiceRequestWhenEditingServiceTypesTest()
        {
            _testRunner.Run<ScenarioCheckRejectEditServiceRequestWhenEditingServiceTypesTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
        /// при редактировании стоимости услуг сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypeCostTest()
        {
            _testRunner.Run<ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypeCostTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
        /// при создании новой услуги сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckApplyEditServiceRequestWhenCreationServiceTypeTest()
        {
            _testRunner.Run<ScenarioCheckApplyEditServiceRequestWhenCreationServiceTypeTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке отклонения заявки на редактирование сервиса,
        /// при создании новой услуги сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckRejectEditServiceRequestWhenCreationServiceTypeTest()
        {
            _testRunner.Run<ScenarioCheckRejectEditServiceRequestWhenCreationServiceTypeTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
        /// при удалении услуги сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckApplyEditServiceRequestWhenDeleteServiceTypeTest()
        {
            _testRunner.Run<ScenarioCheckApplyEditServiceRequestWhenDeleteServiceTypeTest>();
        }
    }
}
