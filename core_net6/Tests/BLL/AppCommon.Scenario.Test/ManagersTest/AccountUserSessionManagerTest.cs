﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class AccountUserSessionManagerTest
    {
               
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки валидности токена
        /// </summary>
        [TestMethod]
        public void CheckTokenValidity()
        {
            _testRunner.Run<CheckTokenValidityTest>();            
        }

        /// <summary>
        /// Тест получения пользователя аккаунта по токену
        /// </summary>
        [TestMethod]
        public void GetAccountUserIdByToken()
        {
            _testRunner.Run<GetAccountUserIdByTokenTest>();           
        }

        /// <summary>
        /// Тест получения списка сессий пользователя аккаунта
        /// </summary>
        [TestMethod]
        public void GetIDs()
        {
            _testRunner.Run<AccountUsersSessionManagerGetIdsTest>();
        }

        /// <summary>
        /// Тест получения деталей сесии
        /// </summary>
        [TestMethod]
        public void GetProperties()
        {
            _testRunner.Run<AccountUsersSessionManagerGetPropertiesTest>();            
        }

        /// <summary>
        /// Тест получения количества сессий пользователя аккаунта
        /// </summary>
        [TestMethod]
        public void Count()
        {
            _testRunner.Run<AccountUsersSessionManagerGetCountTest>();            
        }

        /// <summary>
        /// Тест проверки логина и пароля
        /// </summary>
        [TestMethod]
        public void CheckLoginToCoreTest()
        {
            _testRunner.Run<CheckLoginToCoreTest>();
        }

        /// <summary>
        /// Тест получения деталей сесии
        /// </summary>
        [TestMethod]
        public void CreateSession()
        {
            _testRunner.Run<AccountUsersSessionManagerCreateSessionTest>();            
        }

        /// <summary>
        /// Выполнить вход в облако по средствам логин/пароля
        /// </summary>
        [TestMethod]
        public void LoginTest()
        {
            _testRunner.Run<AccountUserSessionManagerLoginTest>();
        }

        /// <summary>
        /// Выполнить вход в облако по средствам логин/пароля, если пользователь заблокирован
        /// </summary>
        [TestMethod]
        public void LoginNotActivatedUserTest()
        {
            _testRunner.Run<LoginNotActivatedUserTest>();
        }

        /// <summary>
        /// Выполняем вход в облако несуществующего пользователя
        /// </summary>
        [TestMethod]
        public void LoginNotCorrectLoginOrPasswordTest()
        {
            _testRunner.Run<LoginAccountUserNotExistTest>();
        }

        /// <summary>
        /// Выполняем вход в облако пользователя(указываем неправильный пароль)
        /// </summary>
        [TestMethod]
        public void LoginIncorrectPasswordTest()
        {
            _testRunner.Run<LoginIncorrectPasswordTest>();
        }

        /// <summary>
        /// Сценарий проверки правил валидации пароля
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPasswordValidationRulesTest()
        {
            _testRunner.Run<ScenarioCheckPasswordValidationRulesTest>();
        }

        /// <summary>
        /// Сценарий проверки валидации хэша введенного пароля пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountUserPasswordHashValidation()
        {
            _testRunner.Run<ScenarioCheckAccountUserPasswordHashValidation>();
        }

        /// <summary>
        /// Тест логина посредством почты и пароля
        /// </summary>
        [TestMethod]
        public void ScenarioLoginByEmailTest()
        {
            _testRunner.Run<LoginByEmailTest>();
        }

        /// <summary>
        /// Попытаться авторизоваться в логине если пароль пустой или null
        /// </summary>
        [TestMethod]
        public void ScenarioTryToLoginWithoutPasswordTest()
        {
            _testRunner.Run<TryToLoginWithoutPasswordTest>();
        }
    }
}
