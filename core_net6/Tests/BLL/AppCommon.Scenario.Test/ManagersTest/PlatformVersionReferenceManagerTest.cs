﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class PlatformVersionReferenceManagerTest
    {
        private readonly TestRunner _testRunner = new();

        [Obsolete("Нужно переписать. Тест совсем не актуален.")]
        public void CreatePlatformTest()
        {
            _testRunner.Run<CreatePlatformTest>();
        }

        [TestMethod]
        public void DeletePlatformTest()
        {
            _testRunner.Run<DeletePlatformTest>();
        }

        [TestMethod]
        public void GetPlatformTest()
        {
            _testRunner.Run<GetPlatformTest>();
        }

        [TestMethod]
        public void UpdatePlatformTest()
        {
            _testRunner.Run<UpdatePlatformTest>();
        }

        [TestMethod]
        public void CheckAccessToPlatformReferenceTest()
        {
            _testRunner.Run<CheckAccessToPlatformReferenceTest>();
        }
    }
}
