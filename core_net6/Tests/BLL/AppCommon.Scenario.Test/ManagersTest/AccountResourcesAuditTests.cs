﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountResourcesAudit;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты таски аудита проверки целостности услуг ресурсов
    /// </summary>
    [TestClass]
    public class AccountResourcesAuditTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест получения всех типов пропущенных услуг ресурсов для аккаунтов
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountResourcesSampling()
        {
            _testRunner.Run<CheckAccountResourcesSamplingTest>();
        }
    }
}
