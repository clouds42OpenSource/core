﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.VersionComparer;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты сравнения версий расширения сервиса
    /// </summary>
    [TestClass]
    public class BillingServiceVersionComparerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест провеки сравнения версий файлов разработки сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCompareBillingServiceVersionsTest()
        {
            _testRunner.Run<CompareBillingServiceVersionsTest>();
        }
    }
}
