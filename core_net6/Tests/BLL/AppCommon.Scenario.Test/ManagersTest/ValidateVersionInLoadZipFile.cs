﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateVersionInLoadZipFile;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ValidateVersionInLoadZipFile
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void LoadingCorrectVersionFileTest()
        {
            _testRunner.Run<LoadingCorrectVersionFileTest>();
        }

        [TestMethod]
        public void LoadingIncorrectConfigFileTest()
        {
            _testRunner.Run<LoadingIncorrectConfigFileTest>();
        }

        [TestMethod]
        public void LoadingUpVersionFileTest()
        {
            _testRunner.Run<LoadingUpVersionFileTest>();
        }

        [TestMethod]
        public void LoadingMinVersionFileTest()
        {
            _testRunner.Run<LoadingMinVersionFileTest>();
        }
        
    }
}