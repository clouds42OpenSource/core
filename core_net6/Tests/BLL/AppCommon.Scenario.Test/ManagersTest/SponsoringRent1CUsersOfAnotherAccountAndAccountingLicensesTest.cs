﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountAndAccountingLicensesTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста спонсирования арендой 1С пользователей другого аккаунта и учет этих лицензий
    /// </summary>
    [TestClass]
    public class SponsoringRent1CUsersOfAnotherAccountAndAccountingLicensesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста при отмене спонсирования "Стандарта" у другого аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioAtCancellationOfSponsoringStandartAnotherAccountTest()
        {
            _testRunner.Run<ScenarioAtCancellationOfSponsoringStandartAnotherAccountTest>();
        }

        /// <summary>
        /// Сценарий теста при отмене спонсирования "Веб" у другого аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioAtCancellationOfSponsoringWebAnotherAccountTest()
        {
            _testRunner.Run<ScenarioAtCancellationOfSponsoringWebAnotherAccountTest>();
        }

        /// <summary>
        /// Сценарий теста спонсирования при наличии свободных лицензий
        /// </summary>
        [TestMethod]
        public void ScenarioWhenSponsoringWithLicensesAvailable()
        {
            _testRunner.Run<ScenarioWhenSponsoringWithLicensesAvailable>();
        }
    }
}
