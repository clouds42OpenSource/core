﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerServiceDataTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class PartnerBillingServicesDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Проверка состояния комбинированых услуг при подключении сервиса аккаунту
        /// </summary>
        [TestMethod]
        public void CheckingServiceTypeStatusForUserTest()
        {
            _testRunner.Run<ScenarioCheckServiceTypeStatusForUserTest>();
        }
        
        /// <summary>
        /// Сценарий проверки статуса услуг сервиса у аккаунта
        /// </summary>
        [TestMethod]
        public void CheckServiceTypeStatusForSponsoredUser()
        {
            _testRunner.Run<ScenarioCheckServiceTypeStatusForSponsoredUser>();
        }

        /// <summary>
        /// Сценарий проверки статуса услуг сервиса у аккаунта
        /// </summary>
        [TestMethod]
        public void CheckServiceTypeStatusForSponsoredUserWhenSponsorServiceIsLocked()
        {
            _testRunner.Run<ScenarioCheckServiceTypeStatusForSponsoredUserWhenSponsorServiceIsLocked>();
        }

        /// <summary>
        /// Сценарий проверки статуса сервиса у аккаунта
        /// </summary>
        [TestMethod]
        public void GetServiceTypesStateForUserTest()
        {
            _testRunner.Run<ScenarioGetServiceTypesStateForUser>();
        }

        /// <summary>
        /// Сценарий проверки статуса сервиса у аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetServiceTypesStateDemoPeriodForUser()
        {
            _testRunner.Run<ScenarioGetServiceTypesStateDemoPeriodForUser>();
        }

        /// <summary>
        /// Сценарий проверки статуса сервиса у аккаунта
        /// </summary>
        [TestMethod]
        public void GetServiceTypeStateForUserWithSponsorship()
        {
            _testRunner.Run<ScenarioGetServiceTypeStateForUserWithSponsorship>();
        }

        /// <summary>
        /// Проверка ресурса после отмены спонсирования
        /// </summary>
		[TestMethod]
        public void CheckResourceAfterCancellationOfSponsorship()
        {
            _testRunner.Run<ScenarioCheckResourceAfterCancellationOfSponsorship>();
        }

        /// <summary>
        /// Проверяем что после регистрации новому аккаунту не будут подключены все активные
        /// кастомные сервисы регистрирующего.
        /// </summary>
        [TestMethod]
        public void CheckRegistrationWithReferralAccountServices()
        {
            _testRunner.Run<ScenarioCheckRegistrationWithReferralAccountServices>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта по ссылке сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountByServiceReferralLink()
        {
            _testRunner.Run<ScenarioRegisterAccountByServiceReferralLink>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта по ссылке сервиса зависимого от аренды веб
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountByDependOfWebServiceReferralLink()
        {
            _testRunner.Run<ScenarioRegisterAccountByDependOfWebServiceReferralLink>();
        }

        /// <summary>
        /// Проверка бесплатных лицензий Rent1 после подписки
        /// /// </summary>
        [TestMethod]
        public void CheckFreeRent1CLicensesAfterCustomServiceSubscription()
        {
            _testRunner.Run<ScenarioCheckFreeRent1CLicensesAfterCustomServiceSubscription>();
        }

        /// <summary>
        /// Проверить корректность активации лицензий по пользователям
        /// </summary>
        [TestMethod]
        public void CheckResourcesAfterCustServiceActivationWhenRentIsZero()
        {
            _testRunner.Run<ScenarioCheckResourcesAfterCustServiceActivationWhenRentIsZero>();
        }
        /// <summary>
        /// Продление аренды, дэмосервис остаётся дэмо
        /// </summary>
        [TestMethod]
        public void CheckServiceStatusAfterRentProlongation()
        {
            _testRunner.Run<ScenarioCheckServiceStatusAfterRentProlongation>();
        }

        /// <summary>
        /// Проверка того, чтобы при покупки более дорогой лицензии не оставалось
        /// free более мелкий лицензий
        /// </summary>
        [TestMethod]
        public void CheckFreeResourcesAfterPurchaseLargerLicense()
        {
            _testRunner.Run<ScenarioCheckFreeResourcesAfterPurchaseLargerLicense>();
        }

        [TestMethod]
        public void ScenarioActivateDemoServiceWithSponsoredRent()
        {
            _testRunner.Run<ScenarioActivateDemoServiceWithSponsoredRent>();
        }

        /// <summary>
        /// После удаления пользователя его ресурсы должны правильно освобождаться
        /// и общая стоимость сервиса должна пересчитаться 
        /// </summary>
        [TestMethod]
        public void CheckServiceCostAndResourcesAfterUserDeleting()
        {
            _testRunner.Run<ScenarioCheckServiceCostAndResourcesAfterUserDeleting>();
        }


        //работа с локалями сервиса
        [TestMethod]
        public void GetLocaleOfServiceOwnerOnRegistration()
        {
            _testRunner.Run<GetLocaleOfServiceOwnerOnRegistration>();
        }


        /// <summary>
        /// Проверка заполнения полей вознаграждения и начисления
        /// </summary>
        [TestMethod]
        public async Task ScenarioCheckRemunerationAndAccruals()
        {
            await _testRunner.RunAsync<ScenarioCheckRemunerationAndAccruals>();
        }

        /// <summary>
        /// Проверка запрета подключать кастомные сервисы не на РУ
        /// </summary>
        [TestMethod]
        public void ScenarioCheckActivationCustomServiceForExistingUkrAccountTest()
        {
            _testRunner.Run<ScenarioCheckActivationCustomServiceForExistingUkrAccountTest>();
        }

        /// <summary>
        /// Проверка активации сервиса пользователем
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCustomServiceActivationForAccountUser()
        {
            _testRunner.Run<ScenarioCheckCustomServiceActivationForAccountUser>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта по реф ссылке сервиса Тардис
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountWithTardisService()
        {
            _testRunner.Run<ScenarioRegisterAccountWithTardisService>();
        }

        /// <summary>
        /// Сценарий подключения услуги Тардис для пользователя
        /// </summary>
        [TestMethod]
        public void ScenarionEnableTardisLicenseForAccountUser()
        {
            _testRunner.Run<ScenarionEnableTardisLicenseForAccountUser>();
        }

        /// <summary>
        /// Сценарий отключения услуги Тардис для пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioDisableTardisLicenseForAccountUser()
        {
            _testRunner.Run<ScenarioDisableTardisLicenseForAccountUser>();
        }

        /// <summary>
        /// Сценарий проверки получения баз для запуска со страницы сервиса Тардис
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountDatabasesToRunForTardisService()
        {
            _testRunner.Run<ScenarioGetAccountDatabasesToRunForTardisService>();
        }

        /// <summary>
        /// Проверка типа продления кастомного сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCustomServicePayOrSubscribeTest()
        {
            _testRunner.Run<ScenarionCheckCustomServicePayOrSubscribe>();
        }

        /// <summary>
        /// Создание клиента в разделе партнёрки
        /// </summary>
        [TestMethod]
        public void ScenarioCreateNewClientInAdminPanelTest()
        {
            _testRunner.Run<ScenarioCreateNewClientInAdminPanelTest>();
        }

        /// <summary>
        /// Проверка работы привязки аккаунта к партнёру. Удачный сценарий
        /// </summary>
        [TestMethod]
        public void ScenarioSuccessBindingAccountToPartnerTest()
        {
            _testRunner.Run<ScenarioSuccessBindingAccountToPartnerTest>();
        }

        /// <summary>
        /// Тестирование валидатора привязки клиента к партнёру
        /// </summary>
        [TestMethod]
        public void ScenarioErrorBindingAccountToPartnerTest()
        {
            _testRunner.Run<ScenarioErrorBindingAccountToPartnerTest>();
        }

        /// <summary>
        /// Тестирование корректности ресурсов когда две услуги содержат в себе третью
        /// </summary>
        [TestMethod]
        public void ScenarioCheckResourcesWhenTwoSTypesIncludeThirdSType()
        {
            _testRunner.Run<ScenarioCheckResourcesWhenTwoStypesIncludeThirdStype>();
        }
        
        /// <summary>
        /// Сценарий теста проверки поля Демо период
        /// при выборке клиентов партнера
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDemoPeriodWhenFetchingPartnerClientsTest()
        {
            _testRunner.Run<ScenarioCheckDemoPeriodWhenFetchingPartnerClientsTest>();
        }

        /// <summary>
        /// Подтверждение подписки демо сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioApplyDemoServiceSubscribeTest()
        {
            _testRunner.Run<ApplyDemoServiceSubscribeTest>();
        }

        /// <summary>
        /// Подтверждение подписки демо сервиса обещанным платежом
        /// </summary>
        [TestMethod]
        public void ScenarioApplyDemoServiceSubscribeUsingPromisePayment()
        {
            _testRunner.Run<ApplyDemoServiceSubscribeUsingPromisePayment>();
        }

        /// <summary>
        /// Потверждение подписки демо сервиса если дата окончания
        /// Аренды больше чем дата окончания сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioApplyDemoServiceSubscribeIfRent1CExpireDateIsBigger()
        {
            _testRunner.Run<ApplyDemoServiceSubscribeIfRent1CExpireDateIsBigger>();
        }

        /// <summary>
        /// Проверка агентского вознаграждения при оплате кастомного сервиса бонусами
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAgentRewardIfServicePurchasedByBonusBalance()
        {
            _testRunner.Run<CheckAgentRewardIfServicePurchasedByBonusBalance>();
        }
        
        /// <summary>
        /// Получение списка сервисов аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountServicesListTest()
        {
            _testRunner.Run<GetAccountServicesListTest>();
        }

        /// <summary>
        /// Сценарий для проверки возможности установки расширения для кастомного сервиса Деланс
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAbilityToInstallExtensionToDelansCustomService()
        {
            _testRunner.Run<ScenarioCheckAbilityToInstallExtensionToDelansCustomService>();
        }

        /// <summary>
        /// Сценарий для проверки запрета установки расширения для внутреннего облачного сервиса кроме Саюри
        /// </summary>
        [TestMethod]
        public void ScenarioCheckProhibitionToInstallExtensionToAnotherInternalCloudServiceExceptSauri()
        {
            _testRunner.Run<ScenarioCheckProhibitionToInstallExtensionToAnotherInternalCloudServiceExceptSauri>();
        }

        /// <summary>
        /// Сценарий для получения "Всего заработано" в партнерском кабинете
        /// </summary>
        [TestMethod]
        public void ScenarioCheckTotalEarnedSum()
        {
            _testRunner.Run<ScenarioCheckTotalEarnedSum>();
        }
    }
}
