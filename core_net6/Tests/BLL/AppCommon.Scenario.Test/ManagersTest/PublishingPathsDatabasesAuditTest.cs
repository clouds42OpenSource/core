﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingPathsDatabasesAuditTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки аудита путей публикации информационных баз
    /// </summary>
    [TestClass]
    public class PublishingPathsDatabasesAuditTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки аудита путей публикации файловых информационных баз
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPublishingPathsFileDatabasesAuditTest()
        {
            _testRunner.Run<ScenarioCheckPublishingPathsFileDatabasesAuditTest>();
        }

        /// <summary>
        /// Сценарий теста проверки аудита путей публикации серверных информационных баз
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPublishingPathsServerDatabasesAuditTest()
        {
            _testRunner.Run<ScenarioCheckPublishingPathsServerDatabasesAuditTest>();
        }

        /// <summary>
        /// Сценарий теста проверки аудита путей публикации баз
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPublishingPathsDatabasesAudit()
        {
            _testRunner.Run<ScenarioCheckPublishingPathsDatabasesAudit>();
        }

        /// <summary>
        /// Сценарий проверки путей публикации для серверных баз
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPublishPathsForServerDatabases()
        {
            _testRunner.Run<ScenarioCheckPublishPathsForServerDatabases>();
        }
    }
}
