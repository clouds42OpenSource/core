﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ExecuteProcessForWorkingWithDatabaseManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки выполнения процесса для работы с информационной базой
    /// </summary>
    [TestClass]
    public class ExecuteProcessForWorkingWithDatabaseManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для проверки выполнения обработки процесса удаления инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckExecuteProcessOfRemoveDatabaseSuccessAndFail()
        {
            _testRunner.Run<ScenarioCheckExecuteProcessOfRemoveDatabaseSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для проверки выполнения обработки процесса архивации инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckExecuteProcessOfArchiveDatabaseToTombSuccessAndFail()
        {
            _testRunner.Run<ScenarioCheckExecuteProcessOfArchiveDatabaseToTombSuccessAndFail>();
        }
    }
}
