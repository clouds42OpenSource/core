﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера для работы с аккаунтом биллинга
    /// </summary>
    [TestClass]
    public class BillingAccountTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания счёта для аккаунта на указанную сумму
        /// </summary>
        [TestMethod]
        public void ScenarioCreateInvoiceForSpecifiedInvoiceAmountTest()
        {
            _testRunner.Run<CreateInvoiceForSpecifiedInvoiceAmountTest>();
        }


        /// <summary>
        /// Тест удаления счета на указанную сумму
        /// </summary>
        [TestMethod]
        public void ScenarioCancelInvoiceForPromisePayment()
        {
            _testRunner.Run<CancelInvoiceForPromisePayment>();
        }


        /// <summary>
        /// Тест удаления счета при взятии ОП
        /// </summary>
        [TestMethod]
        public void ScenarioCancelInvoiceTest()
        {
            _testRunner.Run<CancelInvoiceTest>();
        }

        /// <summary>
        /// Тест преждeвременного погашения обещанного платежа
        /// </summary>
        [TestMethod]
        public void ScenarioPromisePaymentEarlyRepayTest()
        {
            _testRunner.Run<PromisePaymentEarlyRepayTest>();
        }

        /// <summary>
        /// Тест продления обещанного платёжа
        /// </summary>
        [TestMethod]
        public void ScenarioProlongPromisePaymentTest()
        {
            _testRunner.Run<ProlongPromisePaymentTest>();
        }

        /// <summary>
        /// Тест увеличения обещанного платёжа
        /// </summary>
        [TestMethod]
        public async Task ScenarioIncreasePromisePaymentTest()
        {
            await _testRunner.RunAsync<IncreasePromisePaymentTest>();
        }

        /// <summary>
        /// Тест получения баланса аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountBalanceTest()
        {
            _testRunner.Run<GetAccountBalanceTest>();
        }

        /// <summary>
        /// Тест проверки валидности входной модели о результате платежа Paybox
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPayboxInputModelValid()
        {
            _testRunner.Run<CheckPayboxInputModelTest>();
        }

    }
}
