﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceAccounts;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер служебных аккаунтов
    /// </summary>
    [TestClass]
    public class ServiceAccountTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания/удаления служебного аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAndDeleteServiceAccountTest()
        {
            _testRunner.Run<CreateAndDeleteServiceAccountTest>();
        }
    }
}
