﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestDataManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по работе
    /// данными заявки сервиса на модерацию
    /// </summary>
    [TestClass]
    public class ChangeServiceRequestDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста, по проверке получения порции данных
        /// заявок сервиса на модерацию
        /// </summary>
        [TestMethod]
        public void ScenarioGetChangeServiceRequestsTest()
        {
            _testRunner.Run<ScenarioGetChangeServiceRequestsTest>();
        }
    }
}
