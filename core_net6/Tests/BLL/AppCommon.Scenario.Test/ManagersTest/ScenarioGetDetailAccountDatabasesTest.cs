﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.GetDetailAccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста получения детальной информации по базам
    /// </summary>
    [TestClass]
    public class ScenarioGetDetailAccountDatabasesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки инициализации
        /// модели отображения информационной базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckInitAccountDatabaseDcTest()
        {
            _testRunner.Run<ScenarioCheckInitAccountDatabaseDcTest>();
        }

        /// <summary>
        /// Сценарий теста проверки таймера в случае если база в процессе создания
        /// и ответа от МС нет в течении часа
        /// </summary>
        [TestMethod]
        public void ScenarioCheckTimerNewItemOrangeTest()
        {
            _testRunner.Run<ScenarioCheckTimerNewItemOrangeTest>();
        }

        /// <summary>
        /// Сценарий теста проверки таймера в случае если база в процессе создания
        /// </summary>
        [TestMethod]
        public void ScenarioCheckTimerNewTimeGreenTest()
        {
            _testRunner.Run<ScenarioCheckTimerNewTimeGreenTest>();
        }

    }
}
