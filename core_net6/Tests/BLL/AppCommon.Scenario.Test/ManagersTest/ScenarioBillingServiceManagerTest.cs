﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста проверки менеджера сервиса биллинга
    /// </summary>
    [TestClass]
    public class ScenarioBillingServiceManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста получения модели для запуска базы
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountDatabaseToRunTest()
        {
            _testRunner.Run<ScenarioGetAccountDatabaseToRunTest>();
        }

        /// <summary>
        /// Сценарий теста получения базы на разделителях для запуска
        /// </summary>
        [TestMethod]
        public void ScenarioGetAccountDatabaseOnDelimitersToRunTest()
        {
            _testRunner.Run<ScenarioGetAccountDatabaseOnDelimitersToRunTest>();
        }

        /// <summary>
        /// Сценарий теста получения транзакций партнера
        /// </summary>
        [TestMethod]
        public void ScenarioGetPartnerTransactionsTest()
        {
            _testRunner.Run<ScenarioGetPartnerTransactionsTest>();
        }

        /// <summary>
        /// Сценарий теста получения транзакций партнера
        /// где имеются транзакции только по системным сервисам
        /// </summary>
        [TestMethod]
        public void ScenarioGetPartnerTransactionsWhereOnlyBySystemServicesTest()
        {
            _testRunner.Run<ScenarioGetPartnerTransactionsWhereOnlyBySystemServicesTest>();
        }

        /// <summary>
        /// Сценарий теста получения транзакций партнера по фильтру
        /// </summary>
        [TestMethod]
        public void ScenarioGetPartnerTransactionsByFilterTest()
        {
            _testRunner.Run<ScenarioGetPartnerTransactionsByFilterTest>();
        }

        [TestMethod]
        public void ScenarioGetPartnerTransactionsWhenClientProlongRent()
        {
            _testRunner.Run<ScenarioGetPartnerTransactionsWhenClientProlongRent>();
        }

        /// <summary>
        /// Проверка пересчёта агенсткого кошелька после входящих транзакций и вывода
        /// </summary>
        [TestMethod]
        public void ScenarioCombinedEditAgentWalletTest()
        {
            _testRunner.Run<ScenarioCombinedEditAgentWalletTest>();
        }

        /// <summary>
        /// Проверка корректности заполнения агенского отчёта о выводе средств
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCorrectnessPrintAgentReportTest()
        {
            _testRunner.Run<ScenarioCheckCorrectnessPrintAgentReportTest>();
        }

        /// <summary>
        /// Сценарий теста проверки агентского вознаграждения по платежу вип аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckingAgentRewardByPaymentOfVipAccountTest()
        {
            _testRunner.Run<ScenarioCheckingAgentRewardByPaymentOfVipAccountTest>();
        }
    }
}
