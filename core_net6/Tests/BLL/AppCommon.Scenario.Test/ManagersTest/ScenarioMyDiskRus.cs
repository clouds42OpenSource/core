﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioMyDiskRus
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void ChangeSizeOfServicePreview()
        {
            _testRunner.Run<CalculateMyDiskCostForRussia>();
        }

        [TestMethod]
        public void TryToChangeTariff()
        {
            _testRunner.Run<ChangeMyDiskTariffRus>();
        }

        [TestMethod]
        public async Task CalculateAndChangeTariff()
        {
            await _testRunner.RunAsync<CalculateAndChangeTariffRus>();
        }

        /// <summary>
        /// Тест пролонгации диска после пересчёта его места
        /// </summary>
        [TestMethod]
        public void ScenarioProlongDiskAfterRecalculateAccountSizeTest()
        {
            _testRunner.Run<ScenarioProlongDiskAfterRecalculateAccountSizeTest>();
        }

        /// <summary>
        /// Проверка запрета создавать базы если диск переполнен
        /// </summary>
        [TestMethod]
        public void ScenarioBlockDbCreationIfDiskFullTest()
        {
            _testRunner.Run<ScenarioBlockDbCreationIfDiskFullTest>();
        }
    }
}
