﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseAccessManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки выполнения процесса для выдачи и удаления доступа пользователю в инф. базу
    /// </summary>
    [TestClass]
    public class AccountDatabaseAccessManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешной и неуспешной проверки обработки процесса выдачи доступа пользователю в инф. базу
        /// </summary>
        [TestMethod]
        public void ScenarioManageGrantAcDbAccessForAccountUserSuccessAndFail()
        {
            _testRunner.Run<ScenarioManageGrantAcDbAccessForAccountUserSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешной и неуспешной проверки обработки процесса удаление доступа пользователю в инф. базу
        /// </summary>
        [TestMethod]
        public void ScenarioManageDeleteAcDbAccessForAccountUserSuccessAndFail()
        {
            _testRunner.Run<ScenarioManageDeleteAcDbAccessForAccountUserSuccessAndFail>();
        }
    }
}
