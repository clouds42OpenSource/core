﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ContentServer;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты справчника серверов публикации
    /// </summary>
    [TestClass]
    public class CloudContentServerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// 
        /// Тест создания/удаления/редактирования сервера публикации
        /// </summary>
        [TestMethod]
        public void ScenarioCrudCloudContentServerTest()
        {
            _testRunner.Run<CrudCloudContentServerTest>();
        }

        /// <summary>
        /// Тест выборки серверов публикации по фильтру
        /// </summary>
        [TestMethod]
        public async Task ScenarioCloudContentServerSamplingTest()
        {
            await _testRunner.RunAsync<CloudContentServerSamplingTest>();
        }

        /// <summary>
        /// Тест получения доступных нод публикации
        /// </summary>
        [TestMethod]
        public void ScenarioGetAvailablePublishNodesTest()
        {
            _testRunner.Run<GetAvailablePublishNodesTest>();
        }
    }
}
