﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CloudFileStorage;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер для работы с файловым хранилищем
    /// </summary>
    [TestClass]
    public class CloudFileStorageManagerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания/удаления/редактирования файлового хранилища
        /// </summary>
        [TestMethod]
        public void ScenarioCrudFileStorageServerTest()
        {
            _testRunner.Run<CrudFileStorageServerTest>();
        }

        /// <summary>
        /// Тест выборки файловых хранилищ по фильтру
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingFileStorageServerTest()
        {
            _testRunner.Run<SamplingFileStorageServerTest>();
        }
    }
}
