﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.EditBillingService;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки редактирования сервиса
    /// </summary>
    [TestClass]
    public class EditBillingServiceTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста успешного редактирования сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioForSuccessfulServiceEditingTest()
        {
            _testRunner.Run<ScenarioForSuccessfulServiceEditingTest>();
        }

        /// <summary>
        /// Сценарий теста ошибочного редактирования сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioForErrorServiceEditingTest()
        {
            _testRunner.Run<ScenarioForErrorServiceEditingTest>();
        }
    }
}
