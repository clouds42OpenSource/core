﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест менеджера для управления сервисами
    /// </summary>
    [TestClass]
    public class BillingServiceManagerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий получения Id сервиса по ключу
        /// </summary>
        [TestMethod]
        public void ScenarioTryGetServiceIdByKeyTest()
        {
            _testRunner.Run<TryGetServiceIdByKeyTest>();
        }

        /// <summary>
        /// Сценарий получения Id услуги сервиса по ключу
        /// </summary>
        [TestMethod]
        public void ScenarioTryGetServiceTypeIdByKeyTest()
        {
            _testRunner.Run<TryGetServiceTypeIdByKeyTest>();
        }

        /// <summary>
        /// Тест получения информации о сервисе для его активации
        /// </summary>
        [TestMethod]
        public void ScenarioGetBillingServiceForActivationTest()
        {
            _testRunner.Run<GetBillingServiceForActivationTest>();
        }

        /// <summary>
        /// Тест получения информации об услугах сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetBillingServiceTypesInfoTest()
        {
            _testRunner.Run<GetBillingServiceTypesInfoTest>();
        }

        /// <summary>
        /// Тест проверки уникальности названия нового сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckServiceNameUniquenessTest()
        {
            _testRunner.Run<CheckServiceNameUniquenessTest>();
        }

        /// <summary>
        /// Тест проверки получения активных услуг сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioGetActiveAccountServiceTypes()
        {
            _testRunner.Run<ScenarioGetActiveAccountServiceTypes>();
        }

        /// <summary>
        /// Тест проверки уникальности имени сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUniquenessOfServiceName()
        {
            _testRunner.Run<ScenarioCheckUniquenessOfServiceName>();
        }

        /// <summary>
        /// Тест проверки поиска внешнего пользователя для спонсирования
        /// </summary>
        [TestMethod]
        public void ScenarioSearchExternalAccountUserForSponsorship()
        {
            _testRunner.Run<ScenarioSearchExternalAccountUserForSponsorship>();
        }

        /// <summary>
        /// Тест проверки сохранения новых данных из управления сервисом
        /// </summary>
        [TestMethod]
        public void ScenarioSaveNewOptionsControl()
        {
            _testRunner.Run<ScenarioSaveNewOptionsControl>();
        }

        /// <summary>
        /// Тест проверки покупки и активации сервиса Дополнительные сеансы за счет ОП
        /// </summary>
        [TestMethod]
        public void ScenarioPayAvailableSessionWithThePromisedPayment()
        {
            _testRunner.Run<ScenarioPayAvailableSessionWithThePromisedPayment>();
        }

        /// <summary>
        /// Тест проверки покупки и активации сервиса Дополнительные сеансы за счет собственных средств
        /// </summary>
        [TestMethod]
        public void ScenarioPayAvailableSession()
        {
            _testRunner.Run<ScenarioPayAvailableSession>();
        }

        /// <summary>
        /// Тест проверки на изменение количества уже купленного сервиса Дополнительные сеансы 
        /// </summary>
        [TestMethod]
        public void ScenarioChangeCountAvailableSession()
        {
            _testRunner.Run<ScenarioChangeCountAvailableSession>();
        }

        /// <summary>
        /// Тест проверки на изменение количества уже купленного сервиса Дополнительные сеансы и последующей истечения аренды
        /// </summary>
        [TestMethod]
        public void ScenarioChangeCountAvailableSessionandBlockRent1C()
        {
            _testRunner.Run<ScenarioChangeCountAvailableSessionBlockRent1C>();
        }

        /// <summary>
        /// Тест проверки блокировки сервиса Дополнительные сеансы при просроченом ОП
        /// </summary>
        [TestMethod]
        public void LockAvailableSessionWithPromisePaymentAndExpiredPromiseTest()
        {
            _testRunner.Run<LockAvailableSessionWithPromisePaymentAndExpiredPromiseTest>();
        }

        /// <summary>
        /// Тест проверки блокировки сервиса Дополнительные сеансы при блокировки Аренды1С
        /// </summary>
        [TestMethod]
        public void LockAvailableSessionTest()
        {
            _testRunner.Run<LockAvailableSessionTest>();
        }

        /// <summary>
        /// Тест проверки разблокировки сервиса Дополнительные сеансы при блокировки Аренды1С при взятии ОП
        /// </summary>
        [TestMethod]
        public void UnlockAvailableSessionWithPromisePaymentTest()
        {
            _testRunner.Run<UnlockAvailableSessionWithPromisePaymentTest>();
        }

        /// <summary>
        /// Тест проверки разблокировки сервиса Дополнительные сеансы при блокировки Аренды1С при достаточной сумме на счету
        /// </summary>
        [TestMethod]
        public void UnlockAvailableSessionWithMoneyTest()
        {
            _testRunner.Run<UnlockAvailableSessionWithMoneyTest>();
        }

        /// <summary>
        /// Тест проверки разблокировки сервиса Дополнительные сеансы при блокировки Аренды1С
        /// при не достаточной сумме на счету при уменьшении количества сеансов
        /// </summary>
        [TestMethod]
        public void UnlockAvailableSessionWithMoneyAndChangeCountTest()
        {
            _testRunner.Run<UnlockAvailableSessionWithMoneyAndChangeCountTest>();
        }

        /// <summary>
        /// Тест проверки валидации суммы по аккаунту, если баланс меньше нужной суммы
        /// </summary>
        [TestMethod]
        public void CheckAccountBalanceValidationIfNoMoneyTest()
        {
            _testRunner.Run<CheckAccountBalanceValidationIfNoMoneyTest>();
        }

        /// <summary>
        /// Тест проверки валидации суммы по аккаунту, если баланс больше или соотвествует сумме
        /// </summary>
        [TestMethod]
        public void CheckAccountBalanceValidationIfHaveMoneyTest()
        {
            _testRunner.Run<CheckAccountBalanceValidationIfHaveMoneyTest>();
        }

        /// <summary>
        /// Тест проверки на возможность взятия ОП, если есть платежи
        /// </summary>
        [TestMethod]
        public void CheckingForTheAbilityToTakeThePromisedPaymentIfAccountHavePaymentsTest()
        {
            _testRunner.Run<CheckingForTheAbilityToTakeThePromisedPaymentIfAccountHavePaymentsTest>();
        }

        /// <summary>
        /// Тест проверки на возможность взятия ОП, если нет платежей
        /// </summary>
        [TestMethod]
        public void CheckingForTheAbilityToTakeThePromisedPaymentIfAccountNotHavePaymentsTest()
        {
            _testRunner.Run<CheckingForTheAbilityToTakeThePromisedPaymentIfAccountNotHavePaymentsTest>();
        }
    }
}
