﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для проверки восстановления инф. баз из бэкапа
    /// </summary>
    [TestClass]
    public class AccountDatabaseBackupScenariosTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки восстановления ИБ в новую копию с файлового архива.
        /// </summary>
        [TestMethod]
        public void FileAccountDatabaseRestoreNewCopyTest()
        {
            _testRunner.Run<FileAccountDatabaseRestoreNewCopyTest>();
        }

        /// <summary>
        /// Сценарий проверки на правильный путь после восстановления базы в тоже место из склепа
        /// </summary>
        [TestMethod]
	    public async Task CheckForCorrectPathAfterRestoreAccountDatabaseToCurrentFromTombTest()
	    {
		    await _testRunner.RunAsync<CheckForCorrectPathAfterRestoreAccountDatabaseToCurrentFromTombTest>();
	    }

        /// <summary>
        /// Сценарий проверки на правильный путь после восстановления базы из склепа
        /// </summary>
        [TestMethod]
	    public async Task CheckForCorrectPathAfterRestoreAccountDatabaseToNewFromTombTest()
	    {
		    await _testRunner.RunAsync<CheckForCorrectPathAfterRestoreAccountDatabaseToNewFromTombTest>();
	    }

        /// <summary>
        /// Сценарий проверки восстановления базы из склепа
        /// </summary>
        [TestMethod]
        public void RestoreAccountDatabaseFromTombTest()
        {
            _testRunner.Run<RestoreAccountDatabaseFromTombTest>();
        }

        /// <summary>
        /// Сценарий проверки восстановления базы, когда бэкапа физически не существует
        /// </summary>
        [TestMethod]
        public void ScenarioRestoreAccountDatabaseWhenBackupNotExist()
        {
            _testRunner.Run<ScenarioRestoreAccountDatabaseWhenBackupNotExist>();
        }

        /// <summary>
        /// Сценарий удаления бэкапа инф. базы через АПИ
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteAccountDatabaseBackupThroughApi()
        {
            _testRunner.Run<ScenarioDeleteAccountDatabaseBackupThroughApi>();
        }

        /// <summary>
        /// Сценарий удаления бэкапа инф. базы через АПИ когда указан не верный ID бэкапа от МС
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteAcDbBackupThroughApiWithIncorrectSmId()
        {
            _testRunner.Run<ScenarioDeleteAcDbBackupThroughApiWithIncorrectSmId>();
        }

        /// <summary>
        /// Сценарий для проверки обновления уже существующей записи о бэкапе
        /// </summary>
        [TestMethod]
        public void ScenarioUpdateExistingBackupRecord()
        {
            _testRunner.Run<ScenarioUpdateExistingBackupRecord>();
        }

        /// <summary>
        /// Тест проверки регистрации бекапа
        /// </summary>
        [TestMethod]
        public void ScenarioAccountDatabaseRegisterBackupTest()
        {
            _testRunner.Run<AccountDatabaseRegisterBackupTest>();
        }
    }
}
