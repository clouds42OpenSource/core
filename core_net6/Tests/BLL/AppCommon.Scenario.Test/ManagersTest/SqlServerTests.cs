﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.SqlServer;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер для работы с sql сервером
    /// </summary>
    [TestClass]
    public class SqlServerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки создания/редактирования/удаления sql сервера
        /// </summary>
        [TestMethod]
        public void ScenarioCrudSqlServerTest()
        {
            _testRunner.Run<CrudSqlServerTest>();
        }

        /// <summary>
        /// Тест получения Sql серверов по фильтру
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetFilteredSqlServerTest()
        {
            await _testRunner.RunAsync<GetFilteredSqlServerTest>();
        }
    }
}
