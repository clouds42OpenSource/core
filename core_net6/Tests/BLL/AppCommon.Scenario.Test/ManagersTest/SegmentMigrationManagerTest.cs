﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Segment;
using Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для миграции аккаунтов между сегментов
    /// </summary>
    [TestClass]
    public class SegmentMigrationManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий миграции аккаунта без серверных баз 
        /// </summary>
        [TestMethod]
        public void MigrateAccountWithoutUsingDbTest()
        {
            _testRunner.Run<MigrateAccountWithoutUsingDbTest>();            
        }

        /// <summary>
        /// Сценарий миграции с проверкой на доступность хранилища
        /// </summary>
        [TestMethod]
        public void MigrateAccountWithCheckingPermissionTest()
        {
            _testRunner.Run<MigrateAccountWithCheckingPermissionTest>();
        }

        /// <summary>
        /// Сценарий миграции одной базы
        /// </summary>
        [TestMethod]
        public void MigrateAccountDatabaseTest()
        {
            _testRunner.Run<MigrateAccountDatabaseTest>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта с базами в разных статусах
        /// </summary>
        [TestMethod]
        public void MigrateAccountWithAccountDatabasesTest()
        {
            _testRunner.Run<MigrateAccountWithAccountDatabasesTest>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта с проверкой на доступность хранилища
        /// </summary>
        [TestMethod]
        public void CorrectDeletionOfDirectoryTest()
        {
            _testRunner.Run<CorrectDeletionOfDirectoryTest>();
        }

        /// <summary>
        /// Сценарий удаления директории с открытым файлом
        /// </summary>
        [TestMethod]
        public void IncorrectDeletionOfDirectoryTest()
        {
            _testRunner.Run<IncorrectDeletionOfDirectoryTest>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта с серверными базами (1 опубликована, вторая нет)
        /// </summary>
        [TestMethod]
        public void MigrateAccountWithPublishServerAccountDatabases()
        {
            _testRunner.Run<MigrateAccountWithPublishServerAccountDatabases>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта в сегмент с одинаковыми файловыми хранилищами
        /// </summary>
        [TestMethod]
        public async Task MigrateAccountToSegmentWithEqualFileStorage()
        {
            await _testRunner.RunAsync<MigrateAccountToSegmentWithEqualFileStorage>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта в сегмент с меньшей версией платформы
        /// </summary>
        [TestMethod]
        public void MigrateAccountToSegmentWithLowerPlatformVersion()
        {
            _testRunner.Run<MigrateAccountToSegmentWithLowerPlatformVersion>();
        }

        /// <summary>
        /// При миграции аккаунта в новый сегмент для серверных баз меняем платформу на Stable.
        /// </summary>
        [TestMethod]
        public void MigrateAccountWithServerAlphaAccountDatabasesTest()
        {
            _testRunner.Run<MigrateAccountWithServerAccountDatabasesTest>();
        }

        /// <summary>
        /// Сценарий миграции аккаунта в сегмент с другим сервером публикаций
        /// </summary>
        [TestMethod]
        public void ScenarioMigrateAccountToSegmentWithDifferentContentServer()
        {
            _testRunner.Run<ScenarioMigrateAccountToSegmentWithDifferentContentServer>();
        }
    }
}
