﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки работы менеджера оплаты
    /// </summary>
    [TestClass]
    public class PaymentReceiptManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки работы онлайн кассы (Успешная работа)
        /// </summary>
        [TestMethod]
        public void OnlineCashierRussianOkTest()
        {
            _testRunner.Run<OnlineCashierRussianOkTest>();
        }

        /// <summary>
        /// Сценарий теста проверки работы онлайн кассы (Выполнение с ошибкой)
        /// </summary>
        [TestMethod]
        public void OnlineCashierRussianFailTest()
        {
            _testRunner.Run<OnlineCashierRussianFailTest>();
        }

        /// <summary>
        /// Сценарий теста проверки формирования документа фискального чека
        /// </summary>
        [TestMethod]
        public void ReceiptDocumentBuilderRussianTest()
        {
            _testRunner.Run<ReceiptDocumentBuilderRussianTest>();
        }

        /// <summary>
        /// Сценарий теста проверки создания повторяющегося фиксального чека
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCreateRepetitiveFiscalReceiptTest()
        {
            _testRunner.Run<ScenarioCheckCreateRepetitiveFiscalReceiptTest>();
        }
    }
}