﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CrudEnterpriseServer;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты менеджера для работы с серверами 1С:Предприятие
    /// </summary>
    [TestClass]
    public class EnterpriseServerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки создания/удаления/редактированрия сервера 1С:Предприятие
        /// </summary>
        [TestMethod]
        public async Task ScenarioCrudEnterpriseServerTest()
        {
            await _testRunner.RunAsync<CrudEnterpriseServerTest>();
        }

        /// <summary>
        /// Тест проверки валидации при создании сервера 1С:Предприятие
        /// </summary>
        [TestMethod]
        public void ScenarioEnterpriseServerCreateValidationTest()
        {
            _testRunner.Run<EnterpriseServerCreateValidationTest>();
        }

        /// <summary>
        /// Тест проверки валидации при удалении сервера 1С:Предприятие
        /// </summary>
        [TestMethod]
        public async Task ScenarioEnterpriseServerDeleteValidationTest()
        {
            await _testRunner.RunAsync<EnterpriseServerDeleteValidationTest>();
        }

        /// <summary>
        /// Тест получения серверов предприятия по фильтру
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetFilteredEnterpriseServersTest()
        {
            await _testRunner.RunAsync<GetFilteredEnterpriseServersTest>();
        }

    }
}
