﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioMyDiskKz
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void ChangeSizeOfServicePreview()
        {
            _testRunner.Run<CalculateMyDiskCostForKz>();
        }

        [TestMethod]
        public void TryToChangeTariff()
        {
            _testRunner.Run<ChangeMyDiskTariffKz>();
        }

        [TestMethod]
        public async Task CalculateAndChangeTariff()
        {
            await _testRunner.RunAsync<CalculateAndChangeTariffKz>();
        }
    }
}
