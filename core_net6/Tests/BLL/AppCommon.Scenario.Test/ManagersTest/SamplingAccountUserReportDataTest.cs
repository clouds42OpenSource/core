﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка пользователей для отчета
    /// </summary>
    [TestClass]
    public class SamplingAccountUserReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка пользователей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingAccountUserReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingAccountUserReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста выборки списка пользователей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountUserIsActiveInSamplingAccountUserReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountUserIsActiveInSamplingAccountUserReportDataTest>();
        }

        /// <summary>
        /// Сценарий проверки роли пользователя в выборке списка пользователей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountUserRoleInSamplingAccountUserReportDataTest()
        {

            _testRunner.Run<ScenarioCheckAccountUserRoleInSamplingAccountUserReportDataTest>();
        }

        /// <summary>
        /// Сценарий проверки логина пользователя в выборке списка пользователей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountUsersLoginInSamplingAccountUserReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountUsersLoginInSamplingAccountUserReportDataTest>();
        }
    }
}
