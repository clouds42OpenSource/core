﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка инф. баз для отчета
    /// </summary>
    [TestClass]
    public class SamplingAccountDatabaseReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка инф. баз для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingAccountDbReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingAccountDbReportDataTest>();
        }
    }
}
