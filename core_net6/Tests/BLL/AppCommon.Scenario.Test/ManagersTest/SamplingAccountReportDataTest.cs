﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка аккаунтов для отчета
    /// </summary>
    [TestClass]
    public class SamplingAccountReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверка количества аккаунтов в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCreateRndomAccountsCountInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCreateRandomAccountsCountInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверка соответствия статуса IsVip аккаунту пользователей в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountsIsVipInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountsIsVipInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки количества ВИП аккаунтов в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountsIsVipCountInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountsIsVipCountInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки количества количества баз у аккаунта в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountDatabasesInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountDatabasesInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверка названий аккаунтов в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAccountCaptionInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckAccountCaptionInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверка количества пользователей в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUsersCountInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckUsersCountInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки данных сейл менеджера
        /// в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckSaleManagerDataInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckSaleManagerDataInSamplingAccountReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки бонусного баланса
        /// в выборке списка аккаунтов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckBonusBalanceDataInSamplingAccountReportDataTest()
        {
            _testRunner.Run<ScenarioCheckBonusBalanceDataInSamplingAccountReportDataTest>();
        }
    }
}
