﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.MeasuringTimeToGetData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест по замеру времени на получение данных
    /// </summary>
    [TestClass]
    public class ScenarioMeasuringTimeToGetDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста при замере времени на получение аккаунтов и пользователей аккаунтов
        /// </summary>
        [TestMethod]
        public void ScenarioWithMeasuringTimeToGetAccountAndAccountUserTest()
        {
            _testRunner.Run<ScenarioWithMeasuringTimeToGetAccountAndAccountUserTest>();
        }

        /// <summary>
        /// Сценарий теста при замере времени на получение информационных баз
        /// </summary>
        [TestMethod]
        public void ScenarioWithMeasuringTimeToGetAccountDatabasesTest()
        {
            _testRunner.Run<ScenarioWithMeasuringTimeToGetAccountDatabasesTest>();
        }
    }
}
