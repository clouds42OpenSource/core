﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта
    /// </summary>
    [TestClass]
    public class ScenarioChangeLocaleAccountTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда у счета нет продуктов счета.
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenInvoiceHasNoInvoiceProductsTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenInvoiceHasNoInvoiceProductsTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда есть счет на произвольную сумму
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenInvoiceOnSuggestedSumTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenInvoiceOnSuggestedSumTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда есть не системный кастомный сервис
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenHasCustomServiceTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenHasCustomServiceTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда есть счет для сервисов Загрузка документов
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenInvoiceForEsdlServicesTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenInvoiceForEsdlServicesTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда есть оплаченный счет
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenHasPaidInvoiceTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenHasPaidInvoiceTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта
        /// когда есть платеж
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenHasPaymentTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenHasPaymentTest>();
        }

        /// <summary>
        /// Сценарий теста успешного изменения локали аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioSuccessfulChangeLocaleAccountTest()
        {
            _testRunner.Run<ScenarioSuccessfulChangeLocaleAccountTest>();
        }

        /// <summary>
        /// Сценарий теста изменения локали аккаунта когда у него есть реферал.
        /// </summary>
        [TestMethod]
        public void ScenarioChangeLocaleAccountWhenHasReferralTest()
        {
            _testRunner.Run<ScenarioChangeLocaleAccountWhenHasReferralTest>();
        }
    }
}
