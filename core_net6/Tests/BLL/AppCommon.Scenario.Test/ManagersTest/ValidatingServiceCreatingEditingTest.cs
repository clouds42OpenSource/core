﻿using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки валидации полей сервиса (Name, ShortDescription) при создании и редактировании
    /// </summary>
    [TestClass]
    public class ValidatingServiceCreatingEditingTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного создания сервиса с валидными полями
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnCreatingSuccess()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnCreatingSuccess>();
        }

        /// <summary>
        /// Сценарий для неудачного создания сервиса с невалидными полями
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnCreatingFail()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnCreatingFail>();
        }

        /// <summary>
        /// Сценарий для удачной валидации когда черновик отправляется на модерацию
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnEditingDraftSuccess()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnEditingDraftSuccess>();
        }

        /// <summary>
        /// Сценарий для неудачной валидации когда черновик отправляется на модерацию
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnEditingDraftFail()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnEditingDraftFail>();
        }

        /// <summary>
        /// Сценарий для неудачного сохранения черновика
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnEditingFail()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnEditingFail>();
        }

        /// <summary>
        /// Сценарий для удачного сохранения черновика
        /// </summary>
        [TestMethod]
        public void ValidatingServiceNameOnEditingSuccess()
        {
            _testRunner.Run<ScenarioValidatingServiceNameOnEditingSuccess>();
        }

        /// <summary>
        /// Сценарий для удачного создания сервиса с валидным заполнением файлов 1с
        /// </summary>
        [TestMethod]
        public void ValidatingService1CFilesOnCreatingSuccess()
        {
            _testRunner.Run<ScenarioValidatingService1CFilesOnCreatingSuccess>();
        }


        /// <summary>
        /// Сценарий для удачного сохранения сервиса в статусе черновик с валидным заполнением файлов 1с
        /// </summary>
        [TestMethod]
        public void ValidatingService1CFilesOnEditingDraftSuccess()
        {
            _testRunner.Run<ScenarioValidatingService1CFilesOnEditingDraftSuccess>();
        }

    }
}
