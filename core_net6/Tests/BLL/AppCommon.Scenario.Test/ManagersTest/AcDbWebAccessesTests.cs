﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseWebAccessTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты предоставления доступов в базу
    /// </summary>
    [TestClass]
    public class AcDbWebAccessesTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест предоставления доступа в информационную базу внутреннего пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioGrantInternalUserDbAccessTest()
        {
            _testRunner.Run<GrantInternalUserDbAccessTest>();
        }

        /// <summary>
        /// Тест предоставления доступа в информационную базу внешнего пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioGrantDbAccessToExternalUserTest()
        {
            _testRunner.Run<GrantDbAccessToExternalUserTest>();
        }

        /// <summary>
        /// Тест удаления доступа в информационную базу
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteAcDbAccessTest()
        {
            _testRunner.Run<DeleteAcDbAccessTest>();
        }
    }
}
