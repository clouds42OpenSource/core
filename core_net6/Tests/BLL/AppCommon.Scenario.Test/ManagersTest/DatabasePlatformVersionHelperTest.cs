﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.DatabasePlatformVersionHelperTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки хелпера по работе
    /// с версией платформы инф. базы
    /// </summary>
    [TestClass]
    public class DatabasePlatformVersionHelperTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки получения
        /// версии платформы для инф. базы на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioGetPlatformVersionForDbTest()
        {
            _testRunner.Run<ScenarioCheckPlatformVersionForDbOnDelimiterTest>();
        }

        /// <summary>
        /// Сценарий теста проверки получения
        /// версии платформы для инф. базы (не файловой)
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPlatformVersionForNotFileDbTest()
        {
            _testRunner.Run<ScenarioCheckPlatformVersionForNotFileDbTest>();
        }

        /// <summary>
        /// Сценарий теста проверки получения
        /// версии платформы для файловой инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPlatformVersionForFileDbTest()
        {
            _testRunner.Run<ScenarioCheckPlatformVersionForFileDbTest>();
        }

        /// <summary>
        /// Сценарий теста проверки получения
        /// версии платформы для файловой инф. базы
        /// у которой тип распространения "Альфа версия"
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPlatformVersionForFileDbWhenDistributionTypeAlphaTest()
        {
            _testRunner.Run<ScenarioCheckPlatformVersionForFileDbWhenDistributionTypeAlphaTest>();
        }

        /// <summary>
        /// Сценарий теста проверки получения
        /// версии платформы для демо базы на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPlatformVersionForDemoDbOnDelimiterTest()
        {
            _testRunner.Run<ScenarioCheckPlatformVersionForDemoDbOnDelimiterTest>();
        }
    }
}
