﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.RedirectAccountDatabase;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioRedirectPublishAccountDatabaseTest
	{
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void RedirectPublishDatabase()
        {            
            _testRunner.Run<RedirectPublishDatabaseWhenServicesAreLockedTest>();
        }

        [TestMethod]
        public void FailRedirectPublish()
        {            
            _testRunner.Run<FailRedirectPublishDatabaseWhenServicesAreLockedTest>();
        }

        
    }
}
