﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ActivityServices;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для проверки активности 
    /// сервисов(Методы которые возвращают статус сервисов)
    /// </summary>
    [TestClass]
    public class ScenarioCheckActivityServicesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки получение информации
        /// о заблокированном сервисе для аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckGetLockServiceInformationTest()
        {
            _testRunner.Run<ScenarioCheckGetLockServiceInformationTest>();
        }

        /// <summary>
        /// Сценарий теста проверки активности сервиса
        /// при получении набора опций инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDbControlOptionsDomainModelProcessorTest()
        {
            _testRunner.Run<ScenarioCheckDbControlOptionsDomainModelProcessorTest>();
        }

        /// <summary>
        /// Сценарий теста проверки активности сервиса
        /// при использовании менеджера по работе с ресурсами
        /// </summary>
        [TestMethod]
        public void ScenarioCheckIsActiveServiceTest()
        {
            _testRunner.Run<ScenarioCheckIsActiveServiceTest>();
        }

        /// <summary>
        /// Сценарий теста проверки на активность сервиса
        /// при поиске пользователя для спонсирования
        /// </summary>
        [TestMethod]
        public void ScenarioCheckActivityServiceWhenSearchExternalUser()
        {
            _testRunner.Run<ScenarioCheckActivityServiceWhenSearchExternalUser>();
        }
    }
}
