﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk42CloudServiceTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки работы
    /// класса управления сервисом "Мой диск"
    /// </summary>
    [TestClass]
    public class MyDisk42CloudServiceTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста проверки запрета изменения даты пролонгации
        /// при управлении сервисом мой Диск
        /// </summary>
        [TestMethod]
        public void ScenarioCheckProhibitionChangingExpireDateWhenManagedMyDiskTest()
        {
            _testRunner.Run<ScenarioCheckProhibitionChangingExpireDateWhenManagedMyDiskTest>();
        }
    }
}
