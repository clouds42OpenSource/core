﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки редактирования шаблонов конфигураций
    /// </summary>
    [TestClass]
    public class DbTemplateDelimitersReferencesManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного обновления релизной версии шаблона конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioUpdateConfigurationReleaseVersionSuccess()
        {
            _testRunner.Run<ScenarioUpdateConfigurationReleaseVersionSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного обновления релизной версии шаблона конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioUpdateConfigurationReleaseVersionFail()
        {
            _testRunner.Run<ScenarioUpdateConfigurationReleaseVersionFail>();
        }

        /// <summary>
        /// Сценарий для успешного обновления минимальной версии шаблона конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioUpdateConfigurationMinVersionSuccess()
        {
            _testRunner.Run<ScenarioUpdateConfigurationMinVersionSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного обновления минимальной версии шаблона конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioUpdateConfigurationMinVersionFail()
        {
            _testRunner.Run<ScenarioUpdateConfigurationMinVersionFail>();
        }

        /// <summary>
        /// Сценарий для успешного добавления нового шаблона конфигурации баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioAddNewDbTemplateDelimitersSuccess()
        {
            _testRunner.Run<ScenarioAddNewDbTemplateDelimitersSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного добавления нового шаблона конфигурации баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioAddNewDbTemplateDelimitersFail()
        {
            _testRunner.Run<ScenarioAddNewDbTemplateDelimitersFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного удаления шаблона конфигурации баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteDbTemplateDelimitersSuccessAndFail()
        {
            _testRunner.Run<ScenarioDeleteDbTemplateDelimitersSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного редактирования шаблона конфигурации баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioEditExistingConfigurationSuccessAndFail()
        {
            _testRunner.Run<ScenarioEditExistingConfigurationSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного получения шаблона конфигурации баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioGetDbTemplateDelimitersSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetDbTemplateDelimitersSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного получения пагинированного списка конфигураций баз на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioGetPagedListOfDbTemplateDelimitersSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetPagedListOfDbTemplateDelimitersSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для успешного и неуспешного поиска информационных баз для выпадающего списка
        /// </summary>
        [TestMethod]
        public void ScenarioFindAccountDatabaseSuccessAndFail()
        {
            _testRunner.Run<ScenarioFindAccountDatabaseSuccessAndFail>();
        }
    }
}
