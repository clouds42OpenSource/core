﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты предоставления доступа пользователя к базам
    /// </summary>
    [TestClass]
    public class ScenarioAccessAcDbForUser
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий удаления доступа к ИБ пользователю
        /// </summary>
        [TestMethod]
        public void DeleteAccessFromDbForAccountUserTest()
        {
            _testRunner.Run<DeleteAccessFromDbForAccountUserTest>();
        }

        /// <summary>
        /// Сценарий предоставления доступа к ИБ пользователям с включенной арендой у всех
        /// </summary>
        [TestMethod]
        public void GrandAccessForAllUsersForAllActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandAccessForAllUsersForAllActivRent1CAccountUserTest>();
        }

        /// <summary>
        /// Сценарий предоставления доступа к ИБ пользователям с включенной арендой у всех
        /// </summary>
        [TestMethod]
        public void GrandAccessForAllUsersForPartActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandAccessForAllUsersForPartActivRent1CAccountUserTest>();
        }

        /// <summary>
        /// Сценарий предоставления доступа к ИБ внешнему пользователю с включенной арендой
        /// </summary>
        [TestMethod]
        public void GrandExternalAccessToDbForActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandExternalAccessToDbForActivRent1CAccountUserTest>();
        }

        /// <summary>
        /// Сценарий предоставления доступа к ИБ внешнему пользователю с отключенной арендой
        /// </summary>
        [TestMethod]
        public void GrandExternalAccessToDbForNoActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandExternalAccessToDbForNoActivRent1CAccountUserTest>();
        }

        /// <summary>
        /// Сценарий предоставления доступа к ИБ пользователю с включенной арендой
        /// </summary>
        [TestMethod]
        public void GrandInternalAccessToDbForActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandInternalAccessToDbForActivRent1CAccountUserTest>();
        }

        [TestMethod]
        public void GrandInternalAccessToDbForNoActivRent1CAccountUserTest()
        {
            _testRunner.Run<GrandInternalAccessToDbForNoActivRent1CAccountUserTest>();
        }

        /// <summary>
        /// Тест на корректность получения признака необходимости отображать
        /// ссылку и кнопку запуска БД
        /// </summary>
        [TestMethod]
        public void ScenarioCheckNeedToRunButtonShowingTest()
        {
            _testRunner.Run<ScenarioCheckNeedToRunButtonShowingTest>();
        }

        /// <summary>
        /// Проверка удаления доступа к базе на разделителях когда тот находится в оброботке
        /// </summary>
        [TestMethod]
        public void ScenarioWrongDeletingAccessToDbOnDelimitersTest()
        {
            _testRunner.Run<ScenarioWrongDeletingAccessToDbOnDelimitersTest>();
        }

        /// <summary>
        /// Проверка добавления доступа к базе на разделителях когда доступ уже есть
        /// </summary>
        [TestMethod]
        public void ScenarioWrongGrandAccessToDbOnDelimitersTest()
        {
            _testRunner.Run<ScenarioWrongGrandAccessToDbOnDelimitersTest>();
        }

        /// <summary>
        /// Сценарий выдачи доступа к удаленной инф. базе
        /// </summary>
        [TestMethod]
        public void ScenarioGrantAccessToDeletedAccountDatabaseTest()
        {
            _testRunner.Run<ScenarioGrantAccessToDeletedAccountDatabaseTest>();
        }

        /// <summary>
        /// Сценарий выдачи доступа к инф. базе в архиве
        /// </summary>
        [TestMethod]
        public void ScenarioGrantAccessToArchivedAccountDatabaseTest()
        {
            _testRunner.Run<ScenarioGrantAccessToArchivedAccountDatabaseTest>();
        }
    }
}
