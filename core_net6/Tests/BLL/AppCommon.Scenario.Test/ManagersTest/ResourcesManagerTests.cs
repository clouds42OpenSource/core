﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки получения моделей управления серввисом и редактирования ресурса
    /// </summary>
    [TestClass]
    public class ResourcesManagerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий успешного и неуспешного получения модели управления сервисом "Мой диск"
        /// </summary>
        [TestMethod]
        public void ScenarionGetServiceMyDiskManagerInfoSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetServiceMyDiskManagerInfoSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий успешного и неуспешного получения модели управления ресурсом
        /// </summary>
        [TestMethod]
        public void ScenarionGetRent1CUserManagerDcSuccessAndFail()
        {
            _testRunner.Run<ScenarioGetRent1CUserManagerDcSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий успешного и неуспешного редактирования ресурса
        /// </summary>
        [TestMethod]
        public void ScenarionEditResourceCostSuccessAndFail()
        {
            _testRunner.Run<ScenarioEditResourceCostSuccessAndFail>();
        }
    }
}
