﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeAgentCashOutRequestStatusManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста проверки менеджера
    /// по смене статуса заявки на вывод средств
    /// </summary>
    [TestClass]
    public class ChangeAgentCashOutRequestStatusManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста смены статуса заявки на вывод средств
        /// </summary>
        [TestMethod]
        public void ScenarioChangeAgentCashOutRequestStatusTest()
        {
            _testRunner.Run<ScenarioChangeAgentCashOutRequestStatusTest>();
        }

        /// <summary>
        /// Сценарий теста смены статуса заявки на "Оплачено"
        /// если есть более ранние не оплаченные заявки
        /// </summary>
        [TestMethod]
        public void ScenarioChangeStatusWhenAvailabilityOfEarlyUnpaidAgentCashOutRequestTest()
        {
            _testRunner.Run<ScenarioChangeStatusWhenAvailabilityOfEarlyUnpaidAgentCashOutRequestTest>();
        }

        /// <summary>
        /// Сценарий теста смены статуса заявки на "Новая" или "В работе"
        /// если есть более поздние выплаты
        /// </summary>
        [TestMethod]
        public void ScenarioChangeStatusWhenAvailabilityOfLaterPaymentsTest()
        {
            _testRunner.Run<ScenarioChangeStatusWhenAvailabilityOfLaterPaymentsTest>();
        }
    }
}
