﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Payments;
using Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentsManagerTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class PaymentsManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест на взятие ОП при активных сервисах. Проверяем создался ли ОП.
        /// </summary>
        [TestMethod]
        public void GetPromisePaymentTest()
        {
            _testRunner.Run<PaymentManagerGetPromisePaymentTest>();
        }

        /// <summary>
        /// Тест на взятие ОП с заблокированными сервисами.
        /// На выходе сервисы должны быть разблокированные сервисы и деньги должны быть списаны.
        /// </summary>
        [TestMethod]
        public void GetPromisePaymentWhenServicesAreLockedTest()
        {
            _testRunner.Run<PaymentManagerGetPromisePaymentWhenServicesAreLockedTest>();
        }

        /// <summary>
        /// Проверяем возможность сразу же погасить ОП, если текущий баланс равен сумме задолженности по ОП
        /// </summary>
        [TestMethod]
        public void GetPromisePaymentAndCheckThatWeCanRepayItImmediately()
        {
            _testRunner.Run<PaymentManagerRepayPromisePaymentWhenDebtEqualsCurrentBalance>();
        }

        /// <summary>
        /// Создаем счет и отправляем уведомление на почту, проверяем отослалось сообщение или нет
        /// </summary>
        [TestMethod]
        public void AddInvoiceAndNotifyAdmin()
        {
            _testRunner.Run<AddInvoiceAndNotifyTest>();
        }

        /// <summary>
        /// Сценарий теста на проверку подтверждения счета на оплату с бонусным вознаграждением
        /// </summary>
        [TestMethod]
        public void ConfirmInvoiceWithBonusRewardTest()
        {
            _testRunner.Run<ConfirmInvoiceWithBonusRewardTest>();
        }

        /// <summary>
        /// Проверка того, что при оплате счёта неполной суммой бонусы не начисляются
        /// </summary>
        [TestMethod]
        public void ConfirmInvoiceWithIncompleteAmountAndBonusTest()
        {
            _testRunner.Run<ConfirmInvoiceWithIncompleteAmountAndBonusTest>();
        }

        /// <summary>
        /// Создаем счет, но не отправляем уведомление пользователю, проверяем наличие уведомления в базе
        /// </summary>
        [TestMethod]
        public void AddInvoiceWithoutNotify()
        {
            _testRunner.Run<AddInvoiceWithoutNotifyTest>();
        }

        /// <summary>
        /// Формирование счёта при наличии спонсируемых лицензий
        /// </summary>
        [TestMethod]
        public void AddInvoiceWithSponsorship()
        {
            _testRunner.Run<AddInvoiceWithSponsorship>();
        }

        /// <summary>
        ///  Берем обещанный платеж и проверяем отправилось ли уведомление
        /// </summary>
        [TestMethod]
        public void AddPromisePaymentAndNotify()
        {
            _testRunner.Run<PromisePaymentNotifyTest>();
        }

        /// <summary>
        /// Тест на внесение оплаты с заблокированными сервисами.
        /// На выходе сервисы должны быть разблокированные сервисы.
        /// </summary>
        [TestMethod]
        public void AddPaymentWhenServicesAreLockedTest()
        {
            _testRunner.Run<AddPaymentWhenServicesAreLockedTest>();
        }

        /// <summary>
        /// Тест на внесение оплаты с просроченным ОП и заблокированными сервисами.
        /// На выходе сервисы должны быть разблокированные и дата не должна быть изменена.
        /// </summary>
        [TestMethod]
        public void AddPaymentWhenPromisePaymentAreLockedTest()
        {
            _testRunner.Run<AddPaymentWhenPromisePaymentAreLockedTest>();
        }

        /// <summary>
        /// Погашение ОП
        /// </summary>
        [TestMethod]
        public void RepayPromisePaymentTest()
        {
            _testRunner.Run<RepayPromisePaymentTest>();
        }

        /// <summary>
        /// Разблокировка сервисов после входящего платежа
        /// </summary>
        [TestMethod]
        public void AddPaymentAndUnlockMyEnterpriseAndMyDisk()
        {
            _testRunner.Run<AddPaymentAndUnlockMyEnterpriseAndMyDisk>();
        }

        /// <summary>
        /// Тест для проверки правильности расчета данных на вкладке баланс
        /// </summary>
        [TestMethod]
        public void CheckCalculatedBillingDataTest()
        {
            _testRunner.Run<CheckCalculatedBillingDataTest>();
        }

        /// <summary>
        /// Создание нового поставщика
        /// </summary>
        [TestMethod]
        public void ScenarioAddSupplierTest()
        {
            _testRunner.Run<ScenarioAddSupplierTest>();
        }

        /// <summary>
        /// Привязываем к поставщику аккаунт-реферал
        /// </summary>
        [TestMethod]
        public void ScenarioAddSupplierReferralAccount()
        {
            _testRunner.Run<ScenarioAddSupplierReferralAccount>();
        }


        /// <summary>
        /// Привязываем аккаунт-реферал к двум поставщикам
        /// </summary>
        [TestMethod]
        public void ScenarioAddSupplierReferralAccountTwice()
        {
            _testRunner.Run<ScenarioAddSupplierReferralAccountTwice>();
        }


        /// <summary>
        /// Тест создания платежа на произвольную сумму
        /// </summary>
        [TestMethod]
        public void AddInvoiceWithoutPeriodForSuggestedPaymentSumTest()
        {
            _testRunner.Run<AddInvoiceWithoutPeriodForSuggestedPaymentSumTest>();
        }

        /// <summary>
        /// Проверка работы создания печатных форм
        /// </summary>
        [TestMethod]
        public void ScenarioCreateNewPrintedHtmlFormTest()
        {
            _testRunner.Run<ScenarioCreateNewPrintedHtmlFormTest>();
        }

        /// <summary>
        /// Проверка работы редактирования печатных форм
        /// </summary>
        [TestMethod]
        public void ScenarioChangePrintedHtmlFormTest()
        {
            _testRunner.Run<ScenarioChangePrintedHtmlFormTest>();

        }

        /// <summary>
        /// Проверка создания/не создания счёта перед оповещением о блокировки
        /// </summary>
        [TestMethod]
        public void AddInvoiceAndNotifyBeforeLockTest()
        {
            _testRunner.Run<AddInvoiceAndNotifyBeforeLockTest>();
        }


        /// <summary>
        /// Тестирование работы вкладки Баланс/Транзакции
        /// </summary>
        [TestMethod]
        public void WorkWithTransactionsPageTest()
        {
            _testRunner.Run<WorkWithTransactionsPageTest>();
        }

        /// <summary>
        /// Тестирование погашения ОП денежными и бонусными средствами
        /// </summary>
        [TestMethod]
        public void ScenarioRepayPromisePaymentByBonusesTest()
        {
            _testRunner.Run<ScenarioRepayPromisePaymentByBonusesTest>();
        }

        /// <summary>
        /// Тест проверки создания цен для услуг сервиса, если они = 0
        /// </summary>
        [TestMethod]
        public void ScenarioCheckRatesCreatedIfServiceCostIsZero()
        {
            _testRunner.Run<CheckRatesCreatedIfServiceCostIsZero>();
        }

        /// <summary>
        /// Тест проверки открытия инструкции сервиса если при создании не указывать ContentType
        /// </summary>
        [TestMethod]
        public void ScenarioOpenServiceInstructionIfContentTypeIsEmptyTest()
        {
            _testRunner.Run<OpenServiceInstructionIfContentTypeIsEmptyTest>();
        }

        /// <summary>
        /// Тест подготовки и получения модели платежа UkrPay
        /// </summary>
        [TestMethod]
        public void ScenarioGetUkrPayPaymentModelTest()
        {
            _testRunner.Run<GetUkrPayPaymentModelTest>();
        }

        /// <summary>
        /// Тест подготовки и получения модели платежа Paybox
        /// </summary>
        [TestMethod]
        public void ScenarioGetPayboxPaymentModelTest()
        {
            _testRunner.Run<GetPayboxPaymentModelTest>();
        }

        /// <summary>
        /// Тест подготовки и получения модели платежа Robokassa
        /// </summary>
        [TestMethod]
        public void ScenarioGetRobokassaPaymentModelTest()
        {
            _testRunner.Run<GetRobokassaPaymentModelTest>();
        }

        /// <summary>
        /// Тест подготовки и получения модели платежа Yookassa
        /// </summary>
        [TestMethod]
        public void ScenarioGetYookassaPaymentModelTest()
        {
            _testRunner.Run<GetYookassaPaymentModelTest>();
        }
    }
}
