﻿using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарии тестов менеджера калькулятора инвойсов
    /// </summary>
    [TestClass]
    public class ScenarioInvoiceCalculatorManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест сценария создания инвойса на произвольную сумму
        /// </summary>
        [TestMethod]
        public void ScenarioCreateInvoiceArbitraryAmountTest()
        {
            _testRunner.Run<ScenarioCreateInvoiceArbitraryAmountTest>();
        }

        /// <summary>
        /// Тест сценария создания инвойса из калькуляции
        /// </summary>
        [TestMethod]
        public void ScenarioCreateInvoiceFromCalculationTest()
        {
            _testRunner.Run<ScenarioCreateInvoiceFromCalculationTest>();
        }


        /// <summary>
        /// Тест получения калькулятора инвойса на подписочные сервисы для обычного аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetInvoiceCalculatorForSubscriptionServicesForNormalAccount()
        {
            _testRunner.Run<ScenarioGetInvoiceCalculatorForSubscriptionServicesForNormalAccount>();
        }

        /// <summary>
        /// Тест получения калькулятора инвойса на подписочные сервисы для VIP аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioGetInvoiceCalculatorForSubscriptionServicesForVipAccount()
        {
            _testRunner.Run<ScenarioGetInvoiceCalculatorForSubscriptionServicesForVipAccount>();
        }

        /// <summary>
        /// Тест получения калькулятора инвойса на сервисы Fasta
        /// </summary>
        [TestMethod]
        public void ScenarioGetInvoiceCalculatorForFastaServices()
        {
            _testRunner.Run<ScenarioGetInvoiceCalculatorForFastaServices>();
        }
    }
}
