﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class RegistrationScenarios
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void ProfitAccountRegistrationTest()
        {
            _testRunner.Run<RegistrationProfitAccountTest>();
        }

        [TestMethod]
        public void AbonCenterRegistrationTest()
        {
            _testRunner.Run<RegistrationAbonCenterTest>();
        }

        [TestMethod]
        public void RegistrationLoginLenghtTest()
        {
            _testRunner.Run<RegistrationLoginLenghtTest>();
        }

        [TestMethod]
        public void RegistrationPasswordLengthTest()
        {
            _testRunner.Run<RegistrationPasswordLengthTest>();
        }

        [TestMethod]
        public async Task RegistrationEsdlExpireDateTest()
        {
            await _testRunner.RunAsync<RegistrationEsdlExpireDateTest>();
        }

        [TestMethod]
        public void DmitriyYashchenkoRegistrationTest()
        {
            _testRunner.Run<RegistrationDmitriyYashchenkoTest>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта по внешней ссылке (Маркет)
        /// </summary>
        [TestMethod]
        public void RegistrationMarketTest()
        {
            _testRunner.Run<RegistrationMarketTest>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта когда пользователь выбрал "Перейти в личный кабинет"
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountWithUnknownCloudService()
        {
            _testRunner.Run<ScenarioRegisterAccountWithUnknownCloudService>();
        }

        /// <summary>
        /// Сценарий регистрации аккаунта когда пользователь выбрал "Аренда 1С"
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterAccountWithEnterpriseCloudService()
        {
            _testRunner.Run<ScenarioRegisterAccountWithEnterpriseCloudService>();
        }

        /// <summary>
        /// Сценарий реристрации аккаунта по реферальной ссылке с активацией демо периода сервиса,
        /// добавление записи активации демо периода в логировании
        /// </summary>
        [TestMethod]
        public void ScenarioRegisterWithReferralLinkTest()
        {
            _testRunner.Run<ScenarioRegisterWithReferralLink>();
        }
    }
}
