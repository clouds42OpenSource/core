﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabasesCommonDataManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки получения обших данных при создании инф. баз
    /// </summary>
    [TestClass]
    public class AccountDatabasesCommonDataManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного получения общих данных при создании инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioGetCommonDataForWorkingWithDatabasesSuccess()
        {
            _testRunner.Run<ScenarioGetCommonDataForWorkingWithDatabasesSuccess>();
        }
    }
}
