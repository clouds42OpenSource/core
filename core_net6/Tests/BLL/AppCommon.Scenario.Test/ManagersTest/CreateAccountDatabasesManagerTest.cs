﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager.ServerAccountDatabasesTests;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест на создания инф. баз
    /// </summary>
    [TestClass]
    public class CreateAccountDatabasesManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного создания инф. базы из dt
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAccountDatabaseFromDtFileSuccess()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromDtFileSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного создания инф. базы из dt
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAccountDatabaseFromDtFileFail()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromDtFileFail>();
        }

        /// <summary>
        /// Сценарий для успешного создания инф. базы из zip
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAccountDatabaseFromZipFileSuccess()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromZipFileSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного создания инф. базы из zip
        /// </summary>
        [TestMethod] public void ScenarioCreateAccountDatabaseFromZipFileFail()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromZipFileFail>();
        }

        /// <summary>
        /// Сценарий для успешного  создания инф. базы из шаблона
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAccountDatabaseFromTemplateSuccess()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromTemplateSuccess>();
        }


        /// <summary>
        /// Сценарий для неуспешного  создания инф. базы из шаблона
        /// </summary>
        [TestMethod]
        public void ScenarioCreateAccountDatabaseFromTemplateFail()
        {
            _testRunner.Run<ScenarioCreateAccountDatabaseFromTemplateFail>();
        }

        /// <summary>
        /// Сценарий для успешного создания инф. базы на разделителях из бекапа
        /// </summary>
        [TestMethod]
        public void CreateAcDbOnDelimitersFromBackupSuccess()
        {
            _testRunner.Run<CreateAcDbOnDelimitersFromBackupSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного создания инф. базы на разделителях из бекапа
        /// </summary>
        [TestMethod]
        public void CreateAcDbOnDelimitersFromBackupFail()
        {
            _testRunner.Run<CreateAcDbOnDelimitersFromBackupFail>();
        }

        /// <summary>
        /// Сценарий для успешной и неуспешной активации Аренды 1С при создании базы
        /// из шаблона и выгрузки кастомной
        /// </summary>
        [TestMethod]
        public void ActivateRent1CIfNeededSuccessAndFail()
        {
            _testRunner.Run<ActivateRent1CIfNeededSuccessAndFail>();
        }

        /// <summary>
        /// Сценарий для проверки создания заявки на смену реижма базы
        /// </summary>
        [TestMethod]
        public void RequestChangeAccountDatabaseTypeTest()
        {
            _testRunner.Run<RequstToChangeAccountDatabaseTypeTest>();
        }
    }
}
