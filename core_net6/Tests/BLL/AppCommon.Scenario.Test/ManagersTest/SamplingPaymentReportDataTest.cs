﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка платежей для отчета
    /// </summary>
    [TestClass]
    public class SamplingPaymentReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка платежей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingPaymentReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingPaymentReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки типа транзакции
        /// в выборке списка платежей для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckTransactionTypeDataInSamplingPaymentReportDataTest()
        {
            _testRunner.Run<ScenarioCheckTransactionTypeDataInSamplingPaymentReportDataTest>();
        }
    }
}
