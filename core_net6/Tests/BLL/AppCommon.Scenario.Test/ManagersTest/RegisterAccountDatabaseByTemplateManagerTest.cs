﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки создания базы 1С из шаблона при активации сервиса биллинга
    /// </summary>
    [TestClass]
    public class RegisterAccountDatabaseByTemplateManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки регистрации/создания инф. базу по шаблону при активации сервиса биллинга
        /// </summary>
        [TestMethod]
        public void ScenarioCheckRegistrationDatabaseUponServiceActivationSuccess()
        {
            _testRunner.Run<ScenarioCheckRegistrationDatabaseUponServiceActivationSuccess>();
        }

        /// <summary>
        /// Сценарий проверки регистрации/создания инф. базу по шаблону при невалидных данных
        /// </summary>
        [TestMethod]
        public void ScenarioCheckRegistrationDatabaseUponServiceActivationFailConditions()
        {
            _testRunner.Run<ScenarioCheckRegistrationDatabaseUponServiceActivationFail>();
        }
    }
}