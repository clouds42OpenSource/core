﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.CheckYookassaAgregatorPaymentStatusManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест для проверки менеджера по проверке статуса платежа агрегатора ЮKassa
    /// </summary>
    [TestClass]
    public class CheckYookassaAggregatorPaymentStatusManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста для не успешной проверки
        /// изменения статуса платежа агрегатора ЮKassa(не успешные кейсы)
        /// </summary>
        [TestMethod]
        public void ScenarioCheckYookassaPaymentStatusChangeErrorTest()
        {
            _testRunner.Run<ScenarioCheckYookassaPaymentStatusChangeErrorTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки изменения статуса платежа,
        /// для успешного платежа
        /// </summary>
        [TestMethod]
        public void ScenarioCheckYookassaPaymentStatusChangeForSuccessPaymentTest()
        {
            _testRunner.Run<ScenarioCheckYookassaPaymentStatusChangeForSuccessPaymentTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки изменения статуса платежа,
        /// для отклоненного платежа
        /// </summary>
        [TestMethod]
        public void ScenarioCheckYookassaPaymentStatusChangeForCanceledPaymentTest()
        {
            _testRunner.Run<ScenarioCheckYookassaPaymentStatusChangeForCanceledPaymentTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки изменения статуса платежа
        /// для отклоненного платежа
        /// </summary>
        [TestMethod]
        public void ScenarioCheckYookassaPaymentStatusChangeForSuccessPaymentAndSavedPaymentMethodTest()
        {
            _testRunner.Run<ScenarioCheckYookassaPaymentStatusChangeForSuccessPaymentAndSavedPaymentMethodTest>();
        }
    }
}
