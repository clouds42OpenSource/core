﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ResourceReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка ресурсов для отчета
    /// </summary>
    [TestClass]
    public class SamplingResourceReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка ресурсов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingResourceReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingResourceReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста выборки списка ресурсов для отчета по датам
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingResourceReportDataExpireDateTest()
        {
            _testRunner.Run<ScenarioSamplingResourceReportDataExpireDateTest>();
        }
    }
}
