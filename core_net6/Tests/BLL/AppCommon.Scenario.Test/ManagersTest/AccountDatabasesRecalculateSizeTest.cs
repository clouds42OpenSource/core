﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class AccountDatabasesRecalculateSizeTest
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void ScenarioCheckSizeOfCorrectAccountDb()
        {
            _testRunner.Run<ScenarioCheckSizeOfCorrectAccountDb>();
        }

        [TestMethod]
        public void ScenarioCheckSizeOfEmptyDb()
        {
            _testRunner.Run<ScenarioCheckSizeOfEmptyDb>();
        }
    }
}
