﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ProvidedServices;
using Clouds42.Test.Runner;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ProvidedServicesTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// покупка esdl и recognition
        /// </summary>
        [TestMethod]
        public void ScenarioCheckProvidedServicesAfterBuyingPagesWithLicenseTest()
        {
            _testRunner.Run<ScenarioCheckProvidedServicesAfterBuyingPagesWithLicenseTest>();
        }

        /// <summary>
        /// покупка стандарта для двух пользователей одновременно
        /// </summary>
        [TestMethod]
        public void ScenarioCheckProvidedServicesAfterBuyingTwoStandartLicensesTest()
        {
            _testRunner.Run<ScenarioCheckProvidedServicesAfterBuyingTwoStandartLicensesTest>();
        }

        /// <summary>
        /// покупка web и следом standart для того же пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioCheckProvidedServicesAfterBuyingStandartAfterWebTest()
        {
            _testRunner.Run<ScenarioCheckProvidedServicesAfterBuyingStandartAfterWebTest>();
        }

        /// <summary>
        /// Проверка редактирования даты окончания услуги при редактированиидаты окончания Аренды
        /// </summary>
        [TestMethod]
        public void ScenarioChangingProvidedServiceExpireDate()
        {
            _testRunner.Run<ScenarioChangingProvidedServiceExpireDate>();
        }

    }
}
