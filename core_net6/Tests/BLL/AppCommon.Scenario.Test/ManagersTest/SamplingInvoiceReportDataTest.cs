﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReportData;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Сценарий теста выборки списка счетов для отчета
    /// </summary>
    [TestClass]
    public class SamplingInvoiceReportDataTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста выборки списка счетов для отчета
        /// </summary>
        [TestMethod]
        public void ScenarioSamplingInvoiceReportDataTest()
        {
            _testRunner.Run<ScenarioSamplingInvoiceReportDataTest>();
        }

        /// <summary>
        /// Сценарий теста проверки выборки счетов для отчета по периоду
        /// </summary>
        [TestMethod]
        public void ScenarioCheckSamplingInvoiceByPeriodReportDataTest()
        {
            _testRunner.Run<ScenarioCheckSamplingInvoiceByPeriodReportDataTest>();
        }
    }
}
