﻿using Clouds42.Scenarios.ADTests.Libs.Scenarios;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на проверку пользователей и групп в АД 
    /// </summary>
    [TestClass]
    public class ActiveDirectoryTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест регистрации без активации Аренды 1С, проверяющий, что пользователь существует в АД и у него проставлены группы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUserExistInGroupsWithoutActivateRent1CTest()
        {
            _testRunner.Run<CheckUserExistInGroupsWithoutActivateRent1CTest>();
        }

        /// <summary>
        /// Тест регистрации с активацией Аренды 1С, проверяющий, что пользователь существует в АД и у него проставлены группы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUserExistInGroupsWithActivateRent1CTest()
        {
            _testRunner.Run<CheckUserExistInGroupsWithActivateRent1CTest>();
        }

        /// <summary>
        /// Тест создания нового пользователя аккаунта и проверка проставления у него групп после покупки Аренды
        /// </summary>
        [TestMethod]
        public void ScenarioCheckAdGroupsAndOfNewCreatedUser()
        {
            _testRunner.Run<CheckAdGroupsAndOfNewCreatedUserTest>();
        }

        /// <summary>
        /// Тест проверки исключения ползователя из групп в АД после отключения
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDisabledUserExcludedFromAdTest()
        {
            _testRunner.Run<CheckDisabledUserExcludedFromAdTest>();
        }

        /// <summary>
        /// Тест удаления пользователя из АД
        /// </summary>
        [TestMethod]
        public void ScenarioUserDeletedFromAdTest()
        {
            _testRunner.Run<UserDeletedFromAdTest>();
        }

        /// <summary>
        /// Тест проверки исключения из групп пользователя после блокировки Аренды 1С
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUserExcludedFromAdGroupsAfterBlockRent1C()
        {
            _testRunner.Run<CheckUserExcludedFromAdGroupsAfterBlockRent1C>();
        }

        /// <summary>
        /// Тест джобы, редактирующей логин пользователя в АД
        /// </summary>
        [TestMethod]
        public void ScenarioChangeUserLoginInAdJobTest()
        {
            _testRunner.Run<ChangeUserLoginInAdJobTest>();
        }
    }
}
