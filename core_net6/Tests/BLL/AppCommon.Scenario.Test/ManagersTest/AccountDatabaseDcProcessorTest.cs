﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест на проверку менеджера получения данных по инф. базам
    /// </summary>
    [TestClass]
    public class AccountDatabaseDcProcessorTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки получения строки подключения к файловой инф. базе
        /// </summary>
        [TestMethod]
        public void ScenarioGetFileDatabaseConnectionPathTest()
        {
            _testRunner.Run<ScenarioGetFileDatabaseConnectionPathTest>();
        }

        /// <summary>
        /// Сценарий проверки получения строки подключения к серверной инф. базе
        /// </summary>
        [TestMethod]
        public void ScenarioGetServerDatabaseConnectionPathTest()
        {
            _testRunner.Run<ScenarioGetServerDatabaseConnectionPathTest>();
        }

        /// <summary>
        /// Сценарий проверки получения строки подключения к инф. базе на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioGetDatabaseOnDelimitersConnectionPathTest()
        {
            _testRunner.Run<ScenarioGetDatabaseOnDelimitersConnectionPathTest>();
        }

        /// <summary>
        /// Сценарий проверки получения инф. баз
        /// </summary>
        [TestMethod]
        public void ScenarioGetDatabaseListTest()
        {
            _testRunner.Run<ScenarioGetDatabaseListTest>();
        }
        
    }
}
