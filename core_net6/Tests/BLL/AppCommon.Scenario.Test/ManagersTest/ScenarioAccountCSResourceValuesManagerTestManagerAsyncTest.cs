﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountCSResourceValuesManager;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    [TestClass]
    public class ScenarioAccountCsResourceValuesManagerAsyncTest
    {
        private readonly TestRunner _testRunner = new();

        [TestMethod]
        public void AccountCSResourceValuesManagerGetValueAsyncTest()
        {
            _testRunner.Run<AccountCsResourceValuesManagerGetValueAsyncTest>();
        }
    }
}
