﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Карта сценариев для проверки управления пользователями аккаунта
    /// </summary>
    [TestClass]
    public class ScenarioAccountUsersManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий успешного редактирования пользователя
        /// </summary>
        [TestMethod]
        public void SuccessUserEdit()
        {
            _testRunner.Run<SuccessEditAccountUser>();
        }

        /// <summary>
        /// Сценарий отката при редактировании пользователя
        /// </summary>
        [TestMethod]
        public void RollbackUserEdit()
        {
            _testRunner.Run<RollbackEditAndDeleteAccountUser>();
        }

        /// <summary>
        /// Сценарий редактирования пользователя клауд админом
        /// </summary>
	    [TestMethod]
	    public void CloudAdminUserEdit()
	    {
		    _testRunner.Run<CloudAdminEditAndDeleteAccountUser>();
	    }

        /// <summary>
        /// Сценарий добавления нового пользвателя к аккаунту
        /// </summary>
	    [TestMethod]
	    public void CloudAdminUserAddNewUserAndEdit()
	    {
		    _testRunner.Run<CloudAdminAddEditAndDeleteAccountUser>();
	    }

        /// <summary>
        /// Сценарий смены пароля для пользователя абон центра
        /// </summary>
        [TestMethod]
        public void ChangePasswordForAbonCenterUserTest()
        {
            _testRunner.Run<ChangePasswordForAbonCenterUserTest>();
        }

        /// <summary>
        /// Сценарий смены ролей для пользователя
        /// </summary>
        [TestMethod]
        public void ChangeAccountUserRolesTest()
        {
            _testRunner.Run<ChangeAccountUserRolesTest>();
        }

        /// <summary>
        /// Сценарий проверки сортировки пользователей
        /// </summary>
        [TestMethod]
        public void AccountUsersSortingTest()
        {
            _testRunner.Run<AccountUsersSortingTest>();
        }

        /// <summary>
        /// Сценарий проверки создания папки 1CStart при регистрации
        /// </summary>
        [TestMethod]
        public void AccountUsersCreate1CStartFolderTest()
        {
            _testRunner.Run<AccountUsersCreate1CStartFolderTest>();
        }

        /// <summary>
        /// Сценарий отключения пользователя
        /// </summary>
        [TestMethod]
        public void AccountUserDeactivateTest()
        {
            _testRunner.Run<AccountUserDeactivateTest>();
        }

        /// <summary>
        /// Сценарий ошибочного добавления пользователя в аккаунт
        /// </summary>
        [TestMethod]
        public void ScenarioErrorAddAccountUserToAccount()
        {
            _testRunner.Run<ScenarioErrorAddAccountUserToAccount>();
        }

        /// <summary>
        /// Тест проверки текущего пароля пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioCheckUserCurrentPasswordTest()
        {
            _testRunner.Run<CheckUserCurrentPasswordTest>();
        }

        /// <summary>
        /// Тест логирования события редактирования пользователя
        /// </summary>
        [TestMethod]
        public void AccountUserChangeLoggingTest()
        {
            _testRunner.Run<AccountUserChangeLoggingTest>();
        }
		
        /// <summary>
        /// Сценарий отключения услуг у пользователя с подключенной услугой "Аренда 1С"
        /// </summary>
        [TestMethod]
        public void ScenarioResourcesChangesHistoryCommitTest()
        {
            _testRunner.Run<ResourcesChangesHistoryCommitTest>();
        }


        /// <summary>
        /// Cценарий удаления пользователя
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteUser()
        {
            _testRunner.Run<SuccesDeleteUserTest>();
        }
    }
}
