﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.BackupManagerTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тест менеджера для работы с бэкапами инф. баз
    /// </summary>
    [TestClass]
    public class BackupManagerTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для проверки успешного восстановления инф. базы из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioRestoreAccountDatabaseSuccess()
        {
            _testRunner.Run<ScenarioRestoreAccountDatabaseSuccess>();
        }

        /// <summary>
        /// Сценарий для проверки неуспешного восстановления инф. базы из бэкапа
        /// </summary>
        [TestMethod]
        public void ScenarioRestoreAccountDatabaseFail()
        {
            _testRunner.Run<ScenarioRestoreAccountDatabaseFail>();
        }

        /// <summary>
        /// Тест на калькулятор для рассчета времени восстановления базы
        /// </summary>
        [TestMethod]
        public void ScenarioBackupRestoringTimeCalculatorTestTest()
        {
            _testRunner.Run<BackupRestoringTimeCalculatorTest>();
        }
    }
}
