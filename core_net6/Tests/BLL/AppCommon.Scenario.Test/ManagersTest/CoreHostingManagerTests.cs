﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Scenarios.CoreHosting;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ManagersTest
{
    /// <summary>
    /// Тесты на менеджер хостинга облака
    /// </summary>
    [TestClass]
    public class CoreHostingManagerTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест создания/удаления/редактирования хостинга
        /// </summary>
        [TestMethod]
        public void ScenarioCrudCoreHostingTest()
        {
            _testRunner.Run<CrudCoreHostingTest>();
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetAllHostingsTest()
        {
            await _testRunner.RunAsync<GetAllHostingsTest>();
        }

        /// <summary>
        /// Тест получения отфильтрованных хостингов
        /// </summary>
        [TestMethod]
        public async Task ScenarioGetFilteredHostingsTest()
        {
            await _testRunner.RunAsync<GetFilteredHostingsTest>();
        }
    }
}
