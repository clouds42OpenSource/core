﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ProcessDeletedServiceTypesForAccountsTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ProcessDeletedServiceTypesForAccounts
{
    /// <summary>
    /// Тест для проверки обработки удаленных услуг
    /// для аккаунтов 
    /// </summary>
    [TestClass]
    public class ProcessDeletedServiceTypesForAccountsTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста по проверке обработки удаленной услуги с моментальным пересчетом
        /// </summary>
        [TestMethod]
        public void ScenarioProcessDeletedServiceTypesWithInstantRecalculationTest()
        {
            _testRunner.Run<ScenarioProcessDeletedServiceTypesWithInstantRecalculationTest>();
        }

        /// <summary>
        /// Сценарий теста по проверке обработки удаленной услуги с отложенным пересчетом
        /// </summary>
        [TestMethod]
        public void ScenarioProcessDeletedServiceTypesWithDelayRecalculationTest()
        {
            _testRunner.Run<ScenarioProcessDeletedServiceTypesWithDelayRecalculationTest>();
        }
    }
}
