﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.AccessProvidersTests
{
    /// <summary>
    /// Карта сценариев для проверки работы ApiAccessProvider
    /// </summary>
    [TestClass]
    public class ScenarioApiAccessProviderTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки работы методов ApiAccessProvider для пользователя
        /// </summary>
        [TestMethod]
        public void ApiAccessProviderUserGroupsTest()
        {
            _testRunner.Run<ApiAccessProviderUserTest>();
        }

        /// <summary>
        /// Сценарий проверки работы методов ApiAccessProvider для службы
        /// </summary>
        [TestMethod]
        public void ApiAccessProviderServiceGroupsTest()
        {
            _testRunner.Run<ApiAccessProviderServiceTest>();
        }
    }
}
