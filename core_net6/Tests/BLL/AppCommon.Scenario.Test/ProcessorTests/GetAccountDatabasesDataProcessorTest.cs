﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.ProcessorTests
{
    /// <summary>
    /// Тест для получения инф. баз аккаунта
    /// </summary>
    [TestClass]
    public class GetAccountDatabasesDataProcessorTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий для успешного получения своих инф. баз аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckGettingAccountMyDatabasesSuccess()
        {
            _testRunner.Run<ScenarioCheckGettingAccountMyDatabasesSuccess>();
        }

        /// <summary>
        /// Сценарий для неуспешного получения своих инф. баз аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckGettingAccountMyDatabasesFail()
        {
            _testRunner.Run<ScenarioCheckGettingAccountMyDatabasesFail>();
        }

        /// <summary>
        /// Сценарий для успешного получения расшаренный инф. баз аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckGettingSharedAccountDatabasesSuccess()
        {
            _testRunner.Run<ScenarioCheckGettingSharedAccountDatabasesSuccess>();
        }


        /// <summary>
        /// Сценарий для неуспешного получения расшаренный инф. баз аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioCheckGettingSharedAccountDatabasesFail()
        {
            _testRunner.Run<ScenarioCheckGettingSharedAccountDatabasesFail>();
        }
    }
}
