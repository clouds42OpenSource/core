﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Cloud42ServiceTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppCommon.Scenario.Test.Cloud42ServiceTests
{
    /// <summary>
    /// Тесты на калькулятор стоимости сервисов
    /// </summary>
    [TestClass]
    public class CLoud42ServiceCostCalculatorTests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест рассчета частичной стоимости сервиса 
        /// </summary>
        [Obsolete("Переписать. Убрать завязку на рандомные значения суммы")]
        public void ScenarioServicePartialCostCalculatorTest()
        {
            _testRunner.Run<ServicePartialCostCalculatorTest>();
        }
    }
}
