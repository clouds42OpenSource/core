﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.NewSupportTest;
using Clouds42.Test.Runner;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AppCommon.Scenario.Test.SupportTests
{
    /// <summary>
    /// Тесты АО и ТиИ
    /// </summary>
    [TestClass]
    public class TehSupportTest
    {

        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест выполнения АО
        /// </summary>
        [TestMethod]
        public void AutoUpdateProcessorTest()
        {
            _testRunner.Run<AutoUpdateProcessorTest>();
        }

        /// <summary>
        /// Сценарий проверки выполения ТиИ подключенной базы.
        /// </summary>
        [Obsolete("Тест устарел новый называется ")]
        [TestMethod]
        public void TehSupportProcessorTest()
        {
            _testRunner.Run<TehSupportProcessorTest>();
        }

        /// <summary>
        /// Проверка создания/не создания сописи в таблицу AcDbSupport
        /// при создании базы с шаблона
        /// </summary>
        [TestMethod]
        public void ScenarioAddRecordInDbSupportAtDbCreatingTest()
        {
            _testRunner.Run<ScenarioAddRecordInDbSupportAtDbCreatingTest>();
        }

        /// <summary>
        /// Сценарий получения метаданных из информационной базы
        /// </summary>
        [TestMethod]
        public void ScenarioGetMetadataFromInfoBaseTest()
        {
            _testRunner.Run<ScenarioGetMetadataFromInfoBaseTest>();
        }

        /// <summary>
        /// Сценарий получения метаданных из информационной базы через 1С Предприятие
        /// </summary>
        [Obsolete("Проблема с запуском 1С предприятия. Есть тест проверяющий этот кейс")]
        public void ScenarioGetAcDbMetadataThrough1CEnterprise()
        {
            _testRunner.Run<ScenarioGetAcDbMetadataThrough1CEnterprise>();
        }

        /// <summary>
        /// Сценарий подключения инф. базы к ТиИ
        /// </summary>
        [TestMethod]
        public void ScenarioConnectAccountDatabaseToTehSupport()
        {
            _testRunner.Run<ScenarioConnectAccountDatabaseToSupport>();
        }

        /// <summary>
        /// Сценарий подключения базы к автообновлению
        /// </summary>
        [TestMethod]
        public void ScenarioConnectAccountDatabaseToAutoUpdate()
        {
            _testRunner.Run<ScenarioConnectAccountDatabaseToAutoUpdate>();
        }

        /// <summary>
        /// Сценарий отключения инф. базы от ТиИ
        /// </summary>
        [TestMethod]
        public void ScenarioDisconnectAccountDatabaseFromTehSupport()
        {
            _testRunner.Run<ScenarioDisconnectAccountDatabaseFromTehSupport>();
        }

        /// <summary>
        /// Сценарий отключения базы от автообновления
        /// </summary>
        [TestMethod]
        public void ScenarioDisconnectAccountDatabaseFromAutoUpdate()
        {
            _testRunner.Run<ScenarioDisconnectAccountDatabaseFromAutoUpdate>();
        }

        /// <summary>
        /// Сценарий создания конфигурации 1С
        /// </summary>
        [TestMethod]
        public void ScenarioCreateConfiguration1C()
        {
            _testRunner.Run<ScenarioCreateConfiguration1C>();
        }

        /// <summary>
        /// Сценарий ошибочного создания конфигурации 1С
        /// </summary>
        [TestMethod]
        public void ScenarioErrorCreateConfiguration1C()
        {
            _testRunner.Run<ScenarioErrorCreateConfiguration1C>();
        }

        /// <summary>
        /// Сценарий планирования АО когда в модели поддержки инф. базы заполнено поле конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioPlanAutoUpdateWhenAcDbConfigurationExist()
        {
            _testRunner.Run<ScenarioPlanAutoUpdateWhenAcDbConfigurationExist>();
        }

        /// <summary>
        /// Сценарий планирования АО когда в модели поддержки инф. базы не заполнено поле конфигурации
        /// </summary>
        [TestMethod]
        public void ScenarioPlanAutoUpdateWhenAcDbConfigurationIsEmpty()
        {
            _testRunner.Run<ScenarioPlanAutoUpdateWhenAcDbConfigurationIsEmpty>();
        }

        /// <summary>
        /// Сценарий проверки удаления временной директории после авто обновления базы
        /// </summary>
        [TestMethod]
        public void ScenarioCheckDeletionTempDirectoryAfterAutoUpdate()
        {
            _testRunner.Run<ScenarioCheckDeletionTempDirectoryAfterAutoUpdate>();
        }

        /// <summary>
        /// Сценарий получения списка активных сессий инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioGetAcDbSessions()
        {
            _testRunner.Run<ScenarioGetAcDbSessions>();
        }

        /// <summary>
        /// Сценарий получения списка активных сессий инф. базы когда ее не существует на сервере
        /// </summary>
        [TestMethod]
        public void ScenarioGetAcDbSessionsWhenDatabaseNotExistOnServer()
        {
            _testRunner.Run<ScenarioGetAcDbSessionsWhenDatabaseNotExistOnServer>();
        }

        /// <summary>
        /// Сценарий получения списка активных сессий инф. базы когда путь к платформе x64 не указан
        /// </summary>
        [TestMethod]
        public void ScenarioGetAcDbSessionsWhenX64PlatformPathEmpty()
        {
            _testRunner.Run<ScenarioGetAcDbSessionsWhenX64PlatformPathEmpty>();
        }

        /// <summary>
        /// Сценарий проверки завершения сессий на кластере
        /// </summary>
        [TestMethod]
        public void ScenarioDropSessionsFromCluster()
        {
            _testRunner.Run<ScenarioDropSessionsFromCluster>();
        }

        /// <summary>
        /// Сценарий проверки смены флага наличия доработок для инф. базы
        /// </summary>
        [TestMethod]
        public void ScenarioChangeHasModificationsFlag()
        {
            _testRunner.Run<ScenarioChangeHasModificationsFlag>();
        }

        /// <summary>
        /// Сценарий подключения базы к АО после смены флага наличия доработок для инф. базы на false
        /// </summary>
        [TestMethod]
        public void ScenarioConnectAcDbToAuAfterChangeHasModificationsFlagToFalse()
        {
            _testRunner.Run<ScenarioConnectAcDbToAuAfterChangeHasModificationsFlagToFalse>();
        }

        /// <summary>
        /// Сценарий подключения базы к АО когда флаг наличия доработок для инф. базы установлен на true
        /// </summary>
        [TestMethod]
        public void ScenarioConnectAcDbToAuWhenHasModificationsFlagIsTrue()
        {
            _testRunner.Run<ScenarioConnectAcDbToAuWhenHasModificationsFlagIsTrue>();
        }

        [TestMethod]
        public void ImportToObnovlyatorTest()
        {
            _testRunner.Run<ImportToObnovlyatorTest>();
        }

        [TestMethod]
        public void HandleObnovlyatorUpdateReportTest()
        {
            _testRunner.Run<HandleObnovlyatorUpdateReportTest>();
        }

        [TestMethod]
        public void HandleObnovlyatorTehSupportReportTest()
        {
            _testRunner.Run<HandleObnovlyatorTehSupportReportTest>();
        }

        [TestMethod]
        public void PlugUnknowConfigurationToAUTest()
        {
            _testRunner.Run<PlugUnknowConfigurationToAUTest>();
        }
        [TestMethod]
        public void HandleObnovlyatorErrorReportTest()
        {
            _testRunner.Run<HandleObnovlyatorErrorReportTest>();
        }
    }
}
