﻿using Clouds42.AccountUsers.AccountUser.Providers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access;
using Clouds42.Repositories.Interfaces;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Xunit;

namespace Clouds42.BLL.UnitTests.AccountUsers
{
    public class AccountUserPermissionsProviderTests
    {
        readonly List<AccountUser> AccountUsers;
        readonly List<AccountUserRole> UserRoles;
        readonly List<AccountSaleManager> SaleManagers;
        readonly List<AccessModel> AccessModels;

        readonly AccountUser User;
        readonly AccountUserGroup UserGroup;

        readonly AccountUser OtherUser;
        readonly AccountUserGroup OtherUserGroup;

        readonly List<(ObjectAction action, AccessLevel level)> TestActionsSet;

        public AccountUserPermissionsProviderTests()
        {
            var intSequence = new IntegerSequence();

            User = new AccountUser { Id = Guid.NewGuid(), AccountId = Guid.NewGuid() };
            OtherUser = new AccountUser { Id = Guid.NewGuid(), AccountId = Guid.NewGuid() };
            AccountUsers = [User, OtherUser];

            UserGroup = (AccountUserGroup)intSequence.Next();
            OtherUserGroup = (AccountUserGroup)intSequence.Next();
            UserRoles =
            [
                new() { AccountUserGroup = UserGroup, AccountUserId = User.Id, },

                new() { AccountUserGroup = OtherUserGroup, AccountUserId = OtherUser.Id, }
            ];

            SaleManagers = [];


            TestActionsSet =
            [
                ((ObjectAction)intSequence.Next(), AccessLevel.Denide),
                ((ObjectAction)intSequence.Next(), AccessLevel.Allow),
                ((ObjectAction)intSequence.Next(), AccessLevel.HimSelfAccount),
                ((ObjectAction)intSequence.Next(), AccessLevel.HimSelfUser),
                ((ObjectAction)intSequence.Next(), AccessLevel.ControlledAccounts)
            ];
            AccessModels = [];
            AccessModels.AddRange(TestActionsSet.Select(x => new AccessModel
            {
                Action = x.action,
                Group = UserGroup,
                Level = x.level
            }));
            AccessModels.AddRange(TestActionsSet.Select(x => new AccessModel
            {
                Action = x.action,
                Group = OtherUserGroup,
                Level = x.level
            }));
        }

        [Fact]
        public void GetUserPermissionsForTargets_SameAccountAndUser_ShouldReturnAllAvailablePermissions()
        {
            //Arrange                        
            var expectedPermissions = TestActionsSet
                .Where(x => x.level != AccessLevel.Denide)
                .Select(x => x.action)
                .Distinct()
                .ToList();
            var (provider, _, _, _, _) = CreateProvider();

            //Act
            var permissions = provider.GetUserPermissionsForTargets(User.Id, User.AccountId, User.Id);

            //Assert
            permissions.Should().BeEquivalentTo(expectedPermissions);
        }

        [Fact]
        public void GetUserPermissionsForTargets_DifferentAccountId_ShouldReturnOnlyAllowedLevelPermissions()
        {
            //Arrange
            var expectedPermissions = TestActionsSet
                .Where(x => x.level == AccessLevel.Allow)
                .Select(x => x.action)
                .Distinct()
                .ToList();
            var (provider, _, _, _, _) = CreateProvider();


            //Act
            var permissions = provider.GetUserPermissionsForTargets(User.Id, OtherUser.AccountId, null);

            //Assert
            permissions.Should().BeEquivalentTo(expectedPermissions);
        }

        [Fact]
        public void GetUserPermissionsForTargets_DifferentAccountByUserId_ShouldReturnOnlyAllowedLevelPermissions()
        {
            //Arrange
            var expectedPermissions = TestActionsSet
                .Where(x => x.level == AccessLevel.Allow)
                .Select(x => x.action)
                .Distinct()
                .ToList();
            var (provider, _, _, _, _) = CreateProvider();


            //Act
            var permissions = provider.GetUserPermissionsForTargets(User.Id, null, OtherUser.Id);

            //Assert
            permissions.Should().BeEquivalentTo(expectedPermissions);
        }

        [Fact]
        public void GetUserPermissionsForTargets_ControlledAccount_ShouldReturnOnlyAllowedLevelAndControlledAccountPermissions()
        {
            //Arrange
            SaleManagers.Add(new AccountSaleManager
            {
                AccountId = OtherUser.AccountId,
                SaleManagerId = User.Id
            });

            var expectedPermissions = TestActionsSet
                .Where(x => x.level == AccessLevel.Allow || x.level == AccessLevel.ControlledAccounts)
                .Select(x => x.action)
                .Distinct()
                .ToList();
            var (provider, _, _, _, _) = CreateProvider();


            //Act
            var permissions = provider.GetUserPermissionsForTargets(User.Id, OtherUser.AccountId, null);

            //Assert
            permissions.Should().BeEquivalentTo(expectedPermissions);
        }


        [Fact]
        public void GetUserPermissionsForTargets_UserGroupHasNoPermissions_ShouldReturnEmptyResult()
        {
            //Arrange
            AccessModels.RemoveAll(x => x.Group == UserGroup);
            var (provider, _, _, _, _) = CreateProvider();

            //Act
            var permissions = provider.GetUserPermissionsForTargets(User.Id, User.AccountId, User.Id);

            //Assert
            permissions.Should().BeEmpty();
        }


        private (
            AccountUserPermissionsProvider provider,
            Mock<IUnitOfWork> unitOfWorkMock,
            Mock<IAccountUserRepository> accountUserRepositoryMock,
            Mock<IAccountUserRoleRepository> accountUserRolesRepositoryMock,
            Mock<IAccountSaleManagerRepository> accountSaleManagerRepositoryMock
            ) CreateProvider()
        {
            var accountUserRepository = new Mock<IAccountUserRepository>();
            var accountUserRolesRepositoryMock = new Mock<IAccountUserRoleRepository>();
            var accountSaleManagerRepository = new Mock<IAccountSaleManagerRepository>();

            accountUserRepository.Setup(x => x.GetAccountUser(It.IsAny<Guid>()))
                .Returns((Guid id) => AccountUsers.FirstOrDefault(x => x.Id == id));
            accountUserRolesRepositoryMock.Setup(x => x.GetUserRoles(It.IsAny<Guid>()))
                .Returns((Guid id) => UserRoles.Where(x => x.AccountUserId == id).ToList());
            accountSaleManagerRepository.Setup(x => x.GetForAccount(It.IsAny<Guid>()))
                .Returns((Guid id) => SaleManagers.FirstOrDefault(x => x.AccountId == id));

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.SetupGet(x => x.AccountUserRoleRepository).Returns(accountUserRolesRepositoryMock.Object);
            unitOfWorkMock.SetupGet(x => x.AccountUsersRepository).Returns(accountUserRepository.Object);
            unitOfWorkMock.SetupGet(x => x.AccountSaleManagerRepository).Returns(accountSaleManagerRepository.Object);

            var accessMappingProvider = new Mock<IAccessMappingsProvider>();
            accessMappingProvider
                .Setup(x => x.GetAccess(It.IsAny<IEnumerable<AccountUserGroup>>()))
                .Returns((IEnumerable<AccountUserGroup> groups) =>
                    groups == null ? [] :
                    AccessModels.Where(x => groups.Contains(x.Group)));
            accessMappingProvider
                .Setup(x => x.GetAccess(It.IsAny<IEnumerable<AccountUserGroup>>(), It.IsAny<IEnumerable<ObjectAction>>()))
                .Returns((IEnumerable<AccountUserGroup> groups, IEnumerable<ObjectAction> actions) =>
                    groups == null || actions == null ?
                        [] :
                        AccessModels.Where(x => groups.Contains(x.Group))
                        .Where(x => actions.Contains(x.Action)));

            var provider = new AccountUserPermissionsProvider(unitOfWorkMock.Object, accessMappingProvider.Object);
            return
            (
                provider,
                unitOfWorkMock,
                accountUserRepository,
                accountUserRolesRepositoryMock,
                accountSaleManagerRepository
            );
        }

        private class IntegerSequence
        {
            int currentValue = 1;

            public int Next() => currentValue++;

        }

    }
}
