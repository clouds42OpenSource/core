﻿namespace Clouds42.Test.Runner
{
    public interface IScenario : ICommand
    {
        void WaitAllBackgroundTasks();
        void ClearGarbage();
    }
}