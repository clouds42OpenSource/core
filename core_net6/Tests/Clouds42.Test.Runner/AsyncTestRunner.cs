﻿using System.Threading.Tasks;

namespace Clouds42.Test.Runner
{
    public static class AsyncTestRunner
    {
        public static async Task Run<TAsyncScenario>() where TAsyncScenario : IAsyncScenario,
            new ()
        {
            var scenario = new TAsyncScenario();
            try
            {
                await scenario.ClearGarbage();
                await scenario.Run();
            }            
            finally
            {
                scenario.WaitAllBackgraundTasks();
                await scenario.ClearGarbage();
            }            
        }

    }
}
