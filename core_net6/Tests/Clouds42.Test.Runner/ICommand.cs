﻿using System.Threading.Tasks;

namespace Clouds42.Test.Runner
{
    public interface ICommand
    {
        void Run();
        Task RunAsync();
    }
}
