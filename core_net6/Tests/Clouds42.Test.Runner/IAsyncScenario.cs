﻿using System.Threading.Tasks;

namespace Clouds42.Test.Runner
{
    public interface IAsyncScenario : IAsyncCommand
    {
        void WaitAllBackgraundTasks();
        Task ClearGarbage();
    }
}