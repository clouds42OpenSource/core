﻿using System.Threading.Tasks;

namespace Clouds42.Test.Runner
{
    public class TestRunner
    {
        public void Run<TScenario>() where TScenario : IScenario,
            new ()
        {
            var scenario = new TScenario();
            try
            {
                scenario.ClearGarbage();
                scenario.Run();
            }            
            finally
            {
                scenario.WaitAllBackgroundTasks();
                scenario.ClearGarbage();
            }           
        }

        public async Task RunAsync<TScenario>() where TScenario : IScenario,
            new()
        {
            var scenario = new TScenario();
            try
            {
                scenario.ClearGarbage();
                await scenario.RunAsync();
            }
            finally
            {
                scenario.WaitAllBackgroundTasks();
                scenario.ClearGarbage();
            }
        }

    }
}
