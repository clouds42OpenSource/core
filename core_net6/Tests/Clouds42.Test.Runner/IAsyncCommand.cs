﻿using System.Threading.Tasks;

namespace Clouds42.Test.Runner
{
    public interface IAsyncCommand
    {
        Task Run();
    }
}