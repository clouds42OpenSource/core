﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.ArchivDatabases;
using Clouds42.Test.Runner;
using CoreWorker.Scenario.Tests.Scenarios;
using CoreWorker.Scenario.Tests.Scenarios.DeleteInactiveAccountDataTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests
{
    /// <summary>
    /// Тесты архивации информационных баз
    /// </summary>
    [TestClass]
    public class ArchiveDatabaseTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий проверки архивации и удаления в склеп ИБ созданных из шаблонов неактивных аккаунтов
        /// </summary>
        [TestMethod]
        public void ArchivDatabaseTest()
        {
            _testRunner.Run<ArchivDatabaseTest>();
        }

        /// <summary>
        /// Сценарий удаления данных неактивного аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteInactiveAccountDataTest()
        {
            _testRunner.Run<ScenarioDeleteInactiveAccountDataTest>();
        }

        /// <summary>
        /// Сценарий удаления данных неактивного аккаунта с базами без бэкапов
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteInactiveAccountDataWithDbWithoutBackupsTest()
        {
            _testRunner.Run<ScenarioDeleteInactiveAccountDataWithDbWithoutBackupsTest>();
        }

        /// <summary>
        /// Сценарий удаления данных неактивного аккаунта без баз, а только с файлами
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteInactiveAccountDataOnlyWithFilesTest()
        {
            _testRunner.Run<ScenarioDeleteInactiveAccountDataOnlyWithFilesTest>();
        }

        /// <summary>
        /// Проверка отправки уведомления после архивации баз неактивных аккаунтов
        /// </summary>
        [TestMethod]
        public void ScenarioArchivDatabaseNotification()
        {
            _testRunner.Run<BeforeArchivDatabaseNotificationTest>();
        }

        /// <summary>
        /// Сценрации провеки выборки аккаунтов для архивации баз и файлов
        /// </summary>
        [TestMethod]
        public void ScenarioArchivDatabaseJobSamplesTest()
        {
            _testRunner.Run<ArchivDatabaseJobSamplesTest>();
        }

        /// <summary>
        /// Сценарий для проверки удаления данных неактивного аккаунта,
        /// если есть демо база на разделителях
        /// </summary>
        [TestMethod]
        public void ScenarioDeleteInactiveAccountDataIfThereIsDbOnDelimiterDemoTest()
        {
            _testRunner.Run<ScenarioDeleteInactiveAccountDataIfThereIsDbOnDelimiterDemoTest>();
        }
    }
}
