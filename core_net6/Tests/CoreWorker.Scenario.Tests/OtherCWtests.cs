﻿using Clouds42.Test.Runner;
using CoreWorker.Scenario.Tests.Scenarios;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests
{
    [TestClass]
    public class OtherCWtests
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Частичная проверка работа джобы по архивации файлов аккаунта
        /// </summary>
        [TestMethod]
        public void ScenarioArchiveAccountFilesTest()
        {
            _testRunner.Run<ScenarioArchiveAccountFilesTest>();
        }

        /// <summary>
        /// Сценарий проверки получения данных для страницы логирования
        /// </summary>
        [TestMethod]
        public void ScenarioGetLoggingDataTest()
        {
            _testRunner.Run<ScenarioGetLoggingDataTest>();
        }

    }
}
