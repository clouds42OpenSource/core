﻿using Clouds42.Billing;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки уведомления о скорой блокировки сервиса
    /// когда есть недавние не оплаченные счета
    /// </summary>
    public class ScenarioCheckLockServiceNotificationWhenThereIsAnotherUnpaidInvoiceTest : ScenarioBase
    {
        private readonly int _defaultPeriodForInvoice;
        private readonly IBillingManager _billingManager;
        private readonly TestDataGenerator _generator;

        public ScenarioCheckLockServiceNotificationWhenThereIsAnotherUnpaidInvoiceTest()
        {
            _defaultPeriodForInvoice = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            _billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            _generator = new TestDataGenerator(Configuration,DbLayer);
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteAccountResources();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var resConfExpireDate = DateTime.Now.AddDays(5);

            ChangeResourceConfigurationExpireDate(resConfExpireDate);

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            ScenarioWhenThereIsUnpaidInvoiceByDefaultPeriod(createAccountCommand.AccountId, createAccountCommand.Inn,
                resourceRent1C.Cost);

            ScenarioWhenThereIsUnpaidInvoiceWithPaidInvoicePeriod(createAccountCommand.AccountId,
                createAccountCommand.Inn,
                resourceRent1C.Cost);

            ScenarioWhenThereIsUnpaidInvoiceButAmountDoesNotMatchInvoicePaid(createAccountCommand.AccountId,
                createAccountCommand.Inn,
                resourceRent1C.Cost);
        }

        /// <summary>
        /// Сценарий теста когда есть не оплаченный счет со случайным периодом
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="inn">Инн</param>
        /// <param name="resourceCost">Стоимость ресурса</param>
        public void ScenarioWhenThereIsUnpaidInvoiceByDefaultPeriod(Guid accountId, string inn, decimal resourceCost)
        {
            var unpaidInvoice = _generator.GenerateInvoice(accountId, inn, resourceCost * _defaultPeriodForInvoice,
                InvoiceStatus.Processing, _defaultPeriodForInvoice, DateTime.Now.AddDays(-4));

            DbLayer.InvoiceRepository.AddInvoice(unpaidInvoice);
            DbLayer.Save();

            _billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.Id != unpaidInvoice.Id && w.AccountId == accountId);

            Assert.IsNull(anotherInvoice, "Другой счет создавать не нужно так как существует не оплаченный счет на эту же сумму");

            DbLayer.InvoiceRepository.AsQueryable().ExecuteDelete();
        }

        /// <summary>
        /// Сценарий теста когда есть не оплаченный счет
        /// с тем же периодом что и у оплаченного счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="inn">Инн</param>
        /// <param name="resourceCost">Стоимость ресурса</param>
        public void ScenarioWhenThereIsUnpaidInvoiceWithPaidInvoicePeriod(Guid accountId, string inn,
            decimal resourceCost)
        {
            var paidInvoice = _generator.GenerateInvoice(accountId, inn, resourceCost * 7,
                InvoiceStatus.Processed, 7, DateTime.Now.AddDays(-4));

            var unpaidInvoice = _generator.GenerateInvoice(accountId, inn, resourceCost * 7,
                InvoiceStatus.Processing, 7, DateTime.Now.AddDays(-2));

            DbLayer.InvoiceRepository.AddInvoice(paidInvoice);
            DbLayer.InvoiceRepository.AddInvoice(unpaidInvoice);
            DbLayer.Save();

            _billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w =>
                w.Id != unpaidInvoice.Id && w.Id != paidInvoice.Id && w.AccountId == accountId);

            Assert.IsNull(anotherInvoice, "Другой счет создавать не нужно так как существует не оплаченный счет на эту же сумму");

            DbLayer.InvoiceRepository.AsQueryable().ExecuteDelete();
        }

        /// <summary>
        /// Сценарий теста когда есть не оплаченный счет
        /// но сумма не совпадает с оплаченным счетом
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="inn">Инн</param>
        /// <param name="resourceCost">Стоимость ресурса</param>
        public void ScenarioWhenThereIsUnpaidInvoiceButAmountDoesNotMatchInvoicePaid(Guid accountId, string inn,
            decimal resourceCost)
        {
            var paidInvoice = _generator.GenerateInvoice(accountId, inn, resourceCost * 7,
                InvoiceStatus.Processed, 7, DateTime.Now.AddDays(-4));

            var unpaidInvoice = _generator.GenerateInvoice(accountId, inn, resourceCost * 5,
                InvoiceStatus.Processing, 5, DateTime.Now.AddDays(-2));

            DbLayer.InvoiceRepository.AddInvoice(paidInvoice);
            DbLayer.InvoiceRepository.AddInvoice(unpaidInvoice);
            DbLayer.Save();

            _billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w =>
                w.Id != unpaidInvoice.Id && w.Id != paidInvoice.Id && w.AccountId == accountId);

            Assert.IsNotNull(anotherInvoice, "Другой счет создавать не нужно так как существует не оплаченный счет на эту же сумму");
        }
    }
}
