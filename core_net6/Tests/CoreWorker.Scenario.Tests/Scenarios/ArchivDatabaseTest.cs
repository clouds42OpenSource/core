﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///  Сценарий проверки архивации и удаления в склеп ИБ созданных из шаблонов неактивных аккаунтов,
    ///  аккаунт базы источника на разделителях в список удаления попасть не должен
    /// 
    ///  Предусловия: аккаунтом test_account в тестовом периоде было создано две базы
    ///  одна обычная из шаблонов и одна ИБ на разделителях
    ///  Добавлен аккаунт с базой источником для разделителей
    /// 
    ///  Действия: создаем нового пользователя, создаем 2 базы, и меняем дату аренды на -46 дней,
    ///  создаем второго пользователя с базой и добавляем в справочник баз на разделителях
    ///  вызываем менеджер который выберет нужный список баз для архивации
    /// 
    ///  Проверка: базы на разделителях и база аккаунта добавленного в справочник не должны содержаться в списке
    /// </summary>
    public class ArchivDatabaseTest : ScenarioBase
    {
        private readonly ServiceAccountManager _serviceAccountManager;

        public ArchivDatabaseTest()
        {
            _serviceAccountManager = TestContext.ServiceProvider.GetRequiredService<ServiceAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            sourceAccountDatabaseCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            _serviceAccountManager.AddServiceAccountItem(new AddServiceAccountDto
            {
                AccountId = sourceAccountDatabaseCommand.AccountDetails.AccountId,
            });
            
            CreateDelimiterTemplate();

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, DateTime.Now.AddDays(-46))
                    .SetProperty(y => y.Frozen, false));
           
            var accountManager = ServiceProvider.GetRequiredService<IAccountDatabaseManager>();

            var archiveDatabaseModels = accountManager.GetArchiveDatabaseList().Result;
            var accountDatabases = DbLayer.DatabasesRepository.All().ToList();

            Assert.AreNotEqual(archiveDatabaseModels.Count, accountDatabases.Count);

            Assert.IsNull(archiveDatabaseModels.FirstOrDefault(b => b.AccountId == sourceAccountDatabaseCommand.AccountDetails.AccountId));
        }
    }
}
