﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Archiving.Managers;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.StateMachine.Contracts.ArchiveAccountFilesToTombProcessFlow;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Частичная проверка работа джобы по архивации файлов аккаунта
    /// Действия:
    /// 1) Создаём аккаунт
    /// 2) Запускаем архивацию файлов аккаунта.
    ///   Ожидаем, что архива не будет, так как у аккаунта нет файлов.
    /// 3) Добавляем в папку аккаунта текстовый файл
    /// 4) Проверяем его на наличие
    /// 5) Повторно запускаем архивацию.
    /// 6) Проверяем наличие новосозданного zip и отсутствие файлов аккаунта в
    ///     папке filestorage
    /// </summary>
    public class ScenarioArchiveAccountFilesTest: ScenarioBase
    {
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IArchiveAccountFilesToTombProcessFlow, TestArchiveAccountFilesToTombProcessFlow>();
        }
        public override void Run()
        {
            var  createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            var account = createAcc.Account;
            var segmentHelper = new SegmentHelper(DbLayer, CloudLocalizer);
            var accountFilesStoragePath = segmentHelper.GetFullClientFileStoragePath(account);

            var archivePath = segmentHelper.GetBackupStorage(account);

            var accountFilesBackupFilePath = Path.Combine(archivePath, $"account_{account.IndexNumber}",
                $"{account.IndexNumber}_{DateTime.Now:yyyy-MM-dd}_files.zip");

            var archiveAccounFilesToTombManager = ServiceProvider.GetRequiredService<ArchiveAccounFilesToTombManager>();
            var result = archiveAccounFilesToTombManager.DetachAccounFilesToTomb(new DeleteAccountFilesToTombJobParamsDto {AccountId = account.Id });
            Assert.IsFalse(result.Error,"Ошибка при попытке архивации файлов аккаунта");
            Assert.AreEqual(result.State,ManagerResultState.Ok, "Ошибка при попытке архивации файлов аккаунта");

            if (File.Exists(accountFilesBackupFilePath))
            {throw new InvalidOperationException("Архив не должен был создастся");}

            string testFilepath = Path.Combine(accountFilesStoragePath, "testFile.txt");

            var createTextFileHelper = ServiceProvider.GetRequiredService<CreateTextFileHelper>();
            createTextFileHelper.CreateTextFileForTest(testFilepath);

            result = archiveAccounFilesToTombManager.DetachAccounFilesToTomb(new DeleteAccountFilesToTombJobParamsDto { AccountId = account.Id });
            Assert.IsFalse(result.Error, "Ошибка при попытке архивации файлов аккаунта");
            Assert.AreEqual(result.State, ManagerResultState.Ok, "Ошибка при попытке архивации файлов аккаунта");

            if (!File.Exists(accountFilesBackupFilePath))
            {throw new InvalidOperationException("Не создался архив файлов аккаунта");}

            if (File.Exists(testFilepath))
            {throw  new InvalidOperationException("Не удалились файлы аккаунта");}

        }
    }
}
