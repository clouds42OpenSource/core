﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки блокирования просроченного сервиса если нет денег.
    /// 1) Создаем кастомный зависящий от Арендый1С сервис. 
    /// 2) Регистрируем аккаунт с кастомным сервисом.
    /// 3) Устанавливаем просроченную дату списания.
    /// 4) Вызываем обработчик
    /// 5) Убеждаемся что сервис Аренда 1С и кастомный платный сервис заблокированны.   
    /// </summary>
    public class LockExpiredServiceWithDependenciesTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });
            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id                    
                }
            });
            createAccountCommand.Run();

            var id = GetRent1CServiceOrThrowException().Id;
            var resConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(w => w.BillingServiceId == id);
            resConfig.ExpireDate = DateTime.Now.AddDays(-1);
            resConfig.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resConfig);
            DbLayer.Save();
            DbLayer.RefreshAll();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();


            var rent1CService = GetRent1CServiceOrThrowException();

            var rent1CResConfig =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(rent1CResConfig);
            Assert.AreEqual(rent1CResConfig.FrozenValue, true);

            var customServiceResConfig =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, createCustomService.Id);

            Assert.IsNotNull(customServiceResConfig);
            Assert.AreEqual(customServiceResConfig.Frozen, null);
        }
    }
}
