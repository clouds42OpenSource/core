﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость больше чем старая стоимость и на счету не достаточно денег,
    ///     и есть обещанный платеж.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем его стоимость
    ///         4) Добавляем обещанный платеж
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис успешно разблокирован и продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnoughWithPromisePayment : ScenarioBase
    {
        private readonly IRateProvider _rateProvider;
        private readonly BillingServiceManager _billingServiceManager;

        public ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnoughWithPromisePayment()
        {
            _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>(); 
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });
            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            RefreshDbCashContext(TestContext.DbLayer.BillingAccountRepository.GetAll());

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            RefreshDbCashContext(TestContext.DbLayer.BillingServiceTypeRepository.GetAll());

            var myEntUser =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);

            var myEntUserWeb =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);

            var usrCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto
                {
                    PaymentSum = usrCost.Cost + usrWebCost.Cost + 3 * billingServiceTypes[0].ServiceTypeCost
                },
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);

            Assert.IsTrue(promisePayment.Result, "Ошибка предоставления обещанного платежа");

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

           Context.ChangeTracker.Clear();

            var customResources = DbLayer.ResourceRepository.Where(w =>
                w.AccountId == createAccountCommand.AccountId &&
                w.BillingServiceType.ServiceId == createCustomService.Id);

            DbLayer.ResourceRepository.DeleteRange(customResources);

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);
            
            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = true,
                BillingServiceTypeId = billingServiceTypes.FirstOrDefault().Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id, false, null, dataAccountUsers);

            Context.ChangeTracker.Clear();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);

            RefreshDbCashContext(DbLayer.BillingServiceTypeRepository.GetAll());

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount.PromisePaymentSum);
            Assert.AreNotEqual(billingAccount.Balance, invoiceRequest.SuggestedPayment.PaymentSum);
        }
    }
}
