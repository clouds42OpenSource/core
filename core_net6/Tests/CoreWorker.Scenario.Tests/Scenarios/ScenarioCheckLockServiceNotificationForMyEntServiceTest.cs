﻿using System;
using System.Threading.Tasks;
using Clouds42.Billing;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки уведомления о скорой блокировки сервиса Аренды 1С
    /// Создаем ресурсы сервиса Аренды 1С, прикрепляем к пользователям, пересчитываем стоимость сервиса и выполняем пролонгацию
    /// </summary>
    public class ScenarioCheckLockServiceNotificationForMyEntServiceTest : ScenarioBase
    {
        private readonly IRateProvider _rateProvider;


        public ScenarioCheckLockServiceNotificationForMyEntServiceTest()
        {
            _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var defaultPeriodForInvoice = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();
            var generator = new TestDataGenerator(Configuration, DbLayer);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteAccountResources();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var accountUserOne = generator.GenerateAccountUser(createAccountCommand.AccountId);
            var accountUserTwo = generator.GenerateAccountUser(createAccountCommand.AccountId);

            var billingAccount = DbLayer.BillingAccountRepository.GetBillingAccount(createAccountCommand.AccountId);

            var myEntUser = await DbLayer.BillingServiceTypeRepository.FirstOrDefaultAsync(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = await DbLayer.BillingServiceTypeRepository.FirstOrDefaultAsync(x => x.SystemServiceType == ResourceType.MyEntUserWeb);

            var rdpMonthlyPerUser = _rateProvider.GetOptimalRate(billingAccount.Id,myEntUser.Id)?.Cost;
            var webMonthlyPerUser = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id)?.Cost;

            var resourceMyEntUser = generator.GenerateResource(createAccountCommand.AccountId, ResourceType.MyEntUser, accountUserOne.Id, null, rdpMonthlyPerUser ?? 0);

            var resourceMyEntUserWeb = generator.GenerateResource(createAccountCommand.AccountId, ResourceType.MyEntUserWeb, accountUserOne.Id, null, webMonthlyPerUser ?? 0);

            var resourceMyEntUserWebTwo = generator.GenerateResource(createAccountCommand.AccountId, ResourceType.MyEntUserWeb, accountUserTwo.Id, null, webMonthlyPerUser ?? 0);

            var resourceMyEntUserWebThree = generator.GenerateResource(createAccountCommand.AccountId, ResourceType.MyEntUserWeb, null, null, webMonthlyPerUser ?? 0);            

            DbLayer.AccountUsersRepository.Insert(accountUserOne);
            DbLayer.AccountUsersRepository.Insert(accountUserTwo);
            DbLayer.ResourceRepository.Insert(resourceMyEntUser);
            DbLayer.ResourceRepository.Insert(resourceMyEntUserWeb);
            DbLayer.ResourceRepository.Insert(resourceMyEntUserWebTwo);
            DbLayer.ResourceRepository.Insert(resourceMyEntUserWebThree);
            
            await DbLayer.SaveAsync();

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            using var transaction = DbLayer.SmartTransaction.Get();
            await Mediator.Send(new RecalculateResourcesConfigurationCostCommand(
                [createAccountCommand.AccountId], [billingService.Id],
                billingService.SystemService));
            transaction.Commit();
            var resConfExpireDate = DateTime.Now.AddDays(5);

            ChangeResourceConfigurationExpireDate(resConfExpireDate);
            
            Services.AddTransient<IProlongServicesProvider, Clouds42.Billing.Billing.Providers.BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            billingManager.NotifyBeforeLockServices();

            var invoice = await DbLayer.InvoiceRepository.FirstOrDefaultAsync(w => w.AccountId == createAccountCommand.AccountId);

            Assert.IsFalse(invoice.Period != defaultPeriodForInvoice ||
                invoice.Sum != defaultPeriodForInvoice * resourceRent1C.Cost,
                "Не корректно работает создание нового счета");
            
            var invoiceProductForMyEntUser = await DbLayer.InvoiceProductRepository.FirstOrDefaultAsync(w =>
                w.InvoiceId == invoice.Id && w.ServiceName == myEntUser.Name);

            Assert.IsNotNull(invoiceProductForMyEntUser, "Не корректно создаются продукты счета для ресурса СТАНДАРТ");
            Assert.AreEqual(invoiceProductForMyEntUser.Count, 2,
                "Не корректно создаются продукты счета для ресурса СТАНДАРТ");
            
            var invoiceProductForMyEntUserWeb = await DbLayer.InvoiceProductRepository.FirstOrDefaultAsync(w =>
                w.InvoiceId == invoice.Id && w.ServiceName == myEntUserWeb.Name);

            Assert.IsNotNull(invoiceProductForMyEntUserWeb, "Не корректно создаются продукты счета для ресурса ВЭБ");
            Assert.AreEqual(invoiceProductForMyEntUserWeb.Count, 1,
                "Не корректно создаются продукты счета для ресурса ВЭБ");
            Assert.AreEqual(invoiceProductForMyEntUserWeb.ServiceSum + invoiceProductForMyEntUser.ServiceSum,
                invoice.Sum, "Сумма у продуктов счета не совпадает с суммой самого счета");
        }
    }
}
