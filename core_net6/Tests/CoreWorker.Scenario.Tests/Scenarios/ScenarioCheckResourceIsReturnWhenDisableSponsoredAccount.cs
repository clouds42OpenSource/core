﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий проверки что ресурс возвращается спонсору когда отключается спонсируемый аккаунт
    /// Действия:
    ///     1) Создаем кастомный сервис
    ///     2) Подключаем к нему спонсируемый аккаунт
    ///     3) Проверяем что у спонсируемого пользователя активировался сервис
    ///     4) Отключаем спонсируемый аккаунт
    ///     5) Проверяем что ресурс вернулся к спонсору 
    /// </summary>
    public class ScenarioCheckResourceIsReturnWhenDisableSponsoredAccount : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IRateProvider _rateProvider;


        public ScenarioCheckResourceIsReturnWhenDisableSponsoredAccount()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
        }
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });

            createAccountCommand.Run();

            var sponsoredAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = "AccountTest_ForBSM",
                FirstName = "AccountTest_ForBSM",
                Email = "AccountTest_ForBSM@bsm.kg",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            sponsoredAccount.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var myEntUser =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);

            var myEntUserWeb =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);

            var usrCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var billingService = GetRent1CServiceOrThrowException();

            var paymentTotalSum = usrCost.Cost + usrWebCost.Cost + 4 * billingServiceTypes[0].ServiceTypeCost;
            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);
            
            var billingServiceType = DbLayer.ResourceRepository.FirstOrDefault(w =>
                w.Subject == createAccountCommand.AccountAdminId && w.AccountId == billingAccount.Id &&
                w.BillingServiceType.ServiceId == createCustomService.Id);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = sponsoredAccount.AccountAdminId,
                Status = true,
                BillingServiceTypeId = billingServiceType.BillingServiceTypeId,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = true,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id,
                false, null, dataAccountUsers);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var resource = DbLayer.ResourceRepository.FirstOrDefault(r =>
                r.AccountId == sponsoredAccount.AccountId && r.Subject == sponsoredAccount.AccountAdminId &&
                r.BillingServiceType.ServiceId == createCustomService.Id);

            Assert.IsNotNull(resource);
            
            var deactivateCustomServiceInSponsoredAccount = new List<CalculateBillingServiceTypeDto>();
            var sponsoredUser = new CalculateBillingServiceTypeDto
            {
                Subject = sponsoredAccount.AccountAdminId,
                Status = false,
                BillingServiceTypeId = billingServiceType.BillingServiceTypeId,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = true,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            deactivateCustomServiceInSponsoredAccount.Add(sponsoredUser);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id,
                false, null, deactivateCustomServiceInSponsoredAccount);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            
            var returnedResource = DbLayer.ResourceRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommand.AccountId && r.Subject == null &&
                r.BillingServiceType.ServiceId == createCustomService.Id);
            Assert.IsNotNull(returnedResource);
        }
    }
}