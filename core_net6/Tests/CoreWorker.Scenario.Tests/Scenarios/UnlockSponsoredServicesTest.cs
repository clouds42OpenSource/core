﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.CloudServices.Rent1C.Manager;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления заблокированного сервиса, который спонсируется других активным аккаунтом.
    /// 1) Создаем аккаунт-1 с ктивной арендой 1С.
    /// 2) Создаем аккаунт-2 с неактивной арендой 1С.
    /// 3) аккаунт-1 спонсирует аккаунт-2 единсвенную лицензию
    /// 4) аккаунт-2 устанавливаем признак блокировки аренды 1С.
    /// 5) Запускаем обработку по разблокировке
    /// 6) Проверяем что аккаунт-2 продлился на месяц.
    /// </summary>
    public class UnlockSponsoredServicesTest : ScenarioBase
    {
        private readonly Rent1CManager _rent1CManager;
        private readonly IBillingManager _billingManager;
        private readonly Rent1CConfigurationAccessManager _accessManager;

        public UnlockSponsoredServicesTest()
        {
            _rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            _billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            _accessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
        }

        public override void Run()
        {
            var createAccountCommand = RunCreateAccountCommand();
            var createSecondAccountCommand = RunCreateAccountCommand();

            BlockRent1C(createSecondAccountCommand.AccountId);
            var rent1CBillingService = GetRent1CServiceOrThrowException();

            var standardRent1CCost =
                CreateInflowPaymentAndGetStandartCost(createAccountCommand, rent1CBillingService.Id);
            var billingAccount = GetBillingAccount(createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);
            Assert.AreEqual(standardRent1CCost * 2, billingAccount.Balance,
                $"Ошибка! Баланс должен быть {standardRent1CCost}");

            ConfigurateRent1CAccesses(createAccountCommand.AccountId, createSecondAccountCommand.AccountAdminId);

            RefreshDbCashContext(DbLayer.BillingServiceTypeRepository.GetAll());
            billingAccount = GetBillingAccount(createAccountCommand.AccountId);
            Assert.AreEqual(standardRent1CCost, billingAccount.Balance, "Ошибка! Баланс должен быть 0");

            BlockRent1C(createSecondAccountCommand.AccountId);
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var unlockSponsoredServices = _billingManager.UnlockSponsoredServices();
            Assert.IsFalse(unlockSponsoredServices.Error, unlockSponsoredServices.Message);
            RefreshDbCashContext(DbLayer.BillingServiceTypeRepository.GetAll());

            billingAccount = GetBillingAccount(createAccountCommand.AccountId);
            Assert.AreEqual(standardRent1CCost, billingAccount.Balance,
                $"Ошибка! Баланс должен быть {standardRent1CCost}");

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            var resourceConfiguration =
                GetResourcesConfigurationOrThrowException(createSecondAccountCommand.AccountId,
                    rent1CBillingService.Id);

            Assert.IsNotNull(resourceConfiguration, "Не удалось найти ресурс конфигурацию");
            Assert.AreEqual(false, resourceConfiguration.FrozenValue, "Сервис должен быть разблокирован");
            Assert.IsNotNull(resourceConfiguration.ExpireDate, "Дата окончания аренды null");
            Assert.AreEqual(DateTime.Now.AddMonths(1).Date, resourceConfiguration.ExpireDate.Value.Date,
                $"Дата окончания аренды должна быть {DateTime.Now.AddMonths(1).Date}");
            Assert.IsNotNull(billingAccount, "Не удалось найти аккаунт биллинга");
            Assert.AreEqual(standardRent1CCost, billingAccount.Balance, $"Баланс должне быть {standardRent1CCost}");
        }

        /// <summary>
        /// Запустить команду создания аккаунта
        /// </summary>
        /// <returns>Команда создания аккаунта</returns>
        private CreateAccountCommand RunCreateAccountCommand()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            return createAccountCommand;
        }

        /// <summary>
        /// Заблокировать аренду 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void BlockRent1C(Guid accountId)
        {
            var yesterdayDate = DateTime.Now.AddDays(-1);
            _rent1CManager.ManageRent1C(accountId, new Rent1CServiceManagerDto
            {
                ExpireDate = yesterdayDate
            });
            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            var resourceConfiguration =
                GetResourcesConfigurationOrThrowException(accountId, GetRent1CServiceOrThrowException().Id);
            Assert.IsNotNull(resourceConfiguration.ExpireDate);
            Assert.AreEqual(yesterdayDate.Date, resourceConfiguration.ExpireDate.Value.Date);
        }

        /// <summary>
        /// Создать входящий платеж и получить стоимость услуги Стандарт
        /// </summary>
        /// <param name="accountDetails">Данные аккаунта</param>
        /// <param name="rent1CServiceId">ID сервиса Аренда 1С</param>
        /// <returns>Стоимость услуги Стандарт</returns>
        private decimal CreateInflowPaymentAndGetStandartCost(IAccountDetails accountDetails, Guid rent1CServiceId)
        {
            var localeId = GetAccountLocaleId(accountDetails.AccountId);

            var standardRent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == rent1CServiceId &&
                r.AccountType == "Standart").Sum(r => r.Cost);

            var paymentTotalSum = standardRent1CCost * 2;
            CreateInflowPayment(accountDetails.AccountId, rent1CServiceId, paymentTotalSum);
            RefreshDbCashContext(DbLayer.BillingServiceTypeRepository.GetAll());

            return standardRent1CCost;
        }

        /// <summary>
        /// Сконфигурировать доступы пользователей к Аренде 1С
        /// </summary>
        /// <param name="sponsorAccountId">ID аккаунта-спонсора</param>
        /// <param name="sponsoredAccountAdminId">ID админа спонсируемого аккаунта</param>
        private void ConfigurateRent1CAccesses(Guid sponsorAccountId, Guid sponsoredAccountAdminId)
        {
            var result = _accessManager.ConfigureAccesses(sponsorAccountId, [
                new() { AccountUserId = sponsoredAccountAdminId, StandartResource = true }
            ]);
            Assert.IsFalse(result.Error);
        }
    }
}