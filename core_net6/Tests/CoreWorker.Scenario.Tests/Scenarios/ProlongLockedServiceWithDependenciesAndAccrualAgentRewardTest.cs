﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления заблокированного сервиса с зависимыми сервисами,
    /// при условии что есть деньги на счету и у аккаунта есть реферальный аккаунт.
    /// Должно произойти начисление вознаграждения
    /// 1) Создаем сервис Аренда 1С.
    /// 2) Создаем кастомный зависимый сервис.
    /// 3) Закидываем денег на счет.
    /// 4) Блокируем сервис Аренда 1С.
    /// 5) Запускаем обработку.
    /// 6) Убеждаемся что сервис Аренда 1С разблокирован и вознаграждение начислилось агенту
    /// </summary>
    public class ProlongLockedServiceWithDependenciesAndAccrualAgentRewardTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });

            createCustomService.Run();

            var referralAccountId = createCustomService.CreateBillingServiceTest.Value.AccountId;

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id,
                    ReferralAccountId = referralAccountId
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>{
                new()
                {
                Subject = createAccountCommand.AccountAdminId,
                Status = true,
                BillingServiceTypeId = serviceTypeDtos[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            }};


            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id,
                false, null, dataAccountUsers);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var resConf1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var paymentTotalSum = resConf1C.Cost + serviceTypeDtos[0].ServiceTypeCost;

            CreateInflowPayment(createAccountCommand.AccountId, null, paymentTotalSum);

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongLockedServices();

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);

            var agentPayments = DbLayer.AgentPaymentRepository.Where(ap => ap.AccountOwnerId == referralAccountId)
                .ToList();

            Assert.AreEqual(agentPayments.Count, 2);

            RefreshDbCashContext(DbLayer.GetGenericRepository<AgentWallet>().GetAll());
            var agentWallets =
                DbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == referralAccountId);

            Assert.IsNotNull(agentWallets);
            Assert.AreEqual(agentWallets.AvailableSum, agentPayments[0].Sum + agentPayments[1].Sum);
        }
    }
}
