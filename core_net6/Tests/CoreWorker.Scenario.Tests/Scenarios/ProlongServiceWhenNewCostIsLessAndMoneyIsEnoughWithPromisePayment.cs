﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость меньше чем старая стоимость и на счету достаточно денег,
    ///     но есть обещанный платеж.
    ///     Действия:
    ///         1) Создаем кастомный сервис 
    ///         2) Блокируем его
    ///         3) Закидываем деньги чтобы их не хватило
    ///         4) Добавляем обещанный платеж, чтобы денег не хватило
    ///         5) Понижаем стоимость сервиса
    ///         6) Проверяем что сервис успешно разблокирован и продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsLessAndMoneyIsEnoughWithPromisePayment : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        public ProlongServiceWhenNewCostIsLessAndMoneyIsEnoughWithPromisePayment()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();
            
            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var resources = DbLayer.ResourceConfigurationRepository.Where(w => w.BillingServiceId != createCustomService.Id);
            foreach (var resource in resources)
            {
                resource.ExpireDate = DateTime.Now.AddDays(-1);
                resource.Frozen = true;
                DbLayer.ResourceConfigurationRepository.Update(resource);
            }

            DbLayer.Save();

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var billingService = GetRent1CServiceOrThrowException();

            var resConf1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, resConf1C.Cost);

            #region Suggested payment

            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto
                {
                    PaymentSum =  2 * billingServiceTypes[0].ServiceTypeCost - 100
                },
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);

            Assert.IsTrue(promisePayment.Result, "Ошибка предоставления обещанного платежа");

            #endregion Suggested payment
            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            var billingAccountBefore =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = false,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = createAccountCommand.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);
            
            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(createAccountCommand.AccountId, createCustomService.Id, false, null, dataAccountUsers);

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C, "Ошибка создания обещанного платежа");
            Assert.AreEqual(billingAccount.Balance, billingAccountBefore.Balance, "Деньги не списались");
        }
    }
}
