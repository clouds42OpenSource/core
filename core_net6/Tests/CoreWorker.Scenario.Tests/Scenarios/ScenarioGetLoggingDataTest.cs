﻿using System;
using System.Linq;
using Clouds42.Common.ManagersResults;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.CloudChangesContext.Dtos;
using Core42.Application.Features.CloudChangesContext.Queries;
using Core42.Application.Features.CloudChangesContext.Queries.Filters;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PagedListExtensionsNetFramework;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий проверки получения данных для страницы логирования
    /// Действия:
    ///     1) Создаем аккаунт и инф. базу
    ///     2) Имитируем обращение к странице "Логирование"
    ///     3) Проверяем что возвращенная модель не пустая
    /// </summary>
    public class ScenarioGetLoggingDataTest : ScenarioBase
    {
        private readonly ISender _mediator;
        
        public ScenarioGetLoggingDataTest()
        {
            _mediator = ServiceProvider.GetRequiredService<ISender>();
        }

        public override void Run()
        {
            var createAccountAndAcDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountAndAcDb.Run();

            var loggingDataWithoutFiltersResult = _mediator.Send(new CloudChangesQuery
            {
                PageNumber = 1,
                Filter = new CloudChangesFilter()
            }).Result;
            CheckGetDataResult(loggingDataWithoutFiltersResult);

            var loggingDataWithFiltersResult = _mediator.Send(new CloudChangesQuery
            {
                PageNumber = 1,
                Filter = new CloudChangesFilter
                {
                    CreateCloudChangeDateFrom = DateTime.Now.AddDays(-1),
                    CreateCloudChangeDateTo = DateTime.Now,
                    SearchLine = createAccountAndAcDb.AccountDetails.Account.AccountCaption
                }
            }).Result;
            CheckGetDataResult(loggingDataWithFiltersResult);
        }

        /// <summary>
        /// Проверить результат получения записей логирования
        /// </summary>
        /// <param name="loggingDataResult">Результат получения записей логирования</param>
        private static void CheckGetDataResult(ManagerResult<PagedDto<CloudChangesLogRecordDto>>loggingDataResult)
        {
            Assert.IsNotNull(loggingDataResult.Result);
            Assert.IsFalse(loggingDataResult.Error);
            Assert.IsTrue(loggingDataResult.Result.Records.Any());
        }
    }
}
