﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.MyDiskContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Создаем аккаунт с просроченным ОП и деньгами на счету.
    /// При обработке должен списать ОП и сняться деньги за списание.
    /// </summary>
    public class ProcessExpiredPromisePaymentsWithMoneyTest : ScenarioBase
    {
        public ProcessExpiredPromisePaymentsWithMoneyTest()
        {
            TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingService = GetRent1CServiceOrThrowException();

            var resConf = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resConf);

            var promisePaymentSum = resConf.Cost;
            
            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = promisePaymentSum},
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);
              
            Assert.IsTrue(promisePayment.Result, "Ошибка предоставления обещанного платежа");

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, promisePaymentSum);

            var promisePaymentDate = DateTime.Now.AddDays(-8);

            await DbLayer.BillingAccountRepository
                .AsQueryable()
                .Where(x => x.Id == createAccountCommand.AccountId)
                .ExecuteUpdateAsync(x => x.SetProperty(y => y.PromisePaymentSum, promisePaymentSum)
                    .SetProperty(y => y.PromisePaymentDate, promisePaymentDate));

            DropDbContextCache();

            ServiceProvider.GetRequiredService<IBillingManager>().ProcessExpiredPromisePayments();
            DropDbContextCache();

            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CInfo = rent1CManager.GetRent1CInfo(createAccountCommand.AccountId);
            var myDiskInfo = await Mediator.Send(new GetMyDiskDataQuery { AccountId = createAccountCommand.AccountId });

            resConf = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            Assert.IsNotNull(resConf);
            Assert.AreEqual(resConf.FrozenValue, false);

            var billingAccount = await 
                TestContext.DbLayer.BillingAccountRepository.AsQueryableNoTracking().FirstOrDefaultAsync(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(billingAccount.PromisePaymentDate);
            Assert.AreEqual(billingAccount.Balance, 0.0000M);
            Assert.AreEqual(billingAccount.PromisePaymentSum, 0.0000M);
            Assert.IsFalse(rent1CInfo.Error);
            Assert.IsFalse(rent1CInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsFalse(myDiskInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsFalse(rent1CInfo.Result.ServiceStatus.ServiceIsLocked);
            Assert.IsFalse(myDiskInfo.Result.ServiceStatus.ServiceIsLocked);
        }
    }
}
