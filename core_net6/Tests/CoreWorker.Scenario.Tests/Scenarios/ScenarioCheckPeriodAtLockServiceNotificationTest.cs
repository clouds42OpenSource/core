﻿using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.Billing;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки по периоду при уведомления о скорой блокиоровки сервиса
    /// </summary>
    public class ScenarioCheckPeriodAtLockServiceNotificationTest : ScenarioBase
    {
        public override void Run()
        {
            var defaultPeriodForInvoice = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();
            var generator = new TestDataGenerator(Configuration,DbLayer);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteAccountResources();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var resConfExpireDate = DateTime.Now.AddDays(5);

            ChangeResourceConfigurationExpireDate(resConfExpireDate);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            var invoice = CreateInvoiceForAccount(createAccountCommand.Account, resourceRent1C.Cost * 2,
                InvoiceStatus.Processed, 2);

            billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.Id != invoice.Id && w.AccountId == createAccountCommand.AccountId);

            Assert.IsNotNull(anotherInvoice, "Ошибка, не создался новый счет");
            Assert.AreEqual(anotherInvoice.Sum, invoice.Sum, "Суммы в счетах должны совпадать");
            Assert.AreEqual(anotherInvoice.Period, invoice.Period, "Периоды в счетах должны совпадать");
            DbLayer.InvoiceRepository.AsQueryable().ExecuteDelete();
            billingManager.NotifyBeforeLockServices();

            invoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);
            Assert.AreEqual(invoice.Period, defaultPeriodForInvoice, "Периоды должны совпадать");
            Assert.AreEqual(invoice.Sum, defaultPeriodForInvoice * resourceRent1C.Cost, "Суммы должны совпадать");
            DbLayer.InvoiceRepository.AsQueryable().ExecuteDelete();

            CreateInvoiceForAccount(createAccountCommand.Account, resourceRent1C.Cost * 12,
                InvoiceStatus.Processed, 12);

            invoice = generator.GenerateInvoice(createAccountCommand.AccountId, createAccountCommand.Inn,
                resourceRent1C.Cost * 12);

            billingManager.NotifyBeforeLockServices();

            anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w =>
                w.Id != invoice.Id && w.AccountId == createAccountCommand.AccountId && w.Period != null);

            Assert.IsNotNull(anotherInvoice, "Ошибка, не создался новый счет");
            Assert.AreEqual(anotherInvoice.Sum, invoice.Sum, "Суммы в счетах должны совпадать");
            Assert.IsNotNull(anotherInvoice.Period, "Период должен быть 12 месяцев");
        }

        /// <summary>
        /// Создать счет на оплату для аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="cost">Сумма счета</param>
        /// <param name="invoiceStatus">Статус счета</param>
        /// <param name="period">Период оплаты</param>
        /// <returns>Cчет на оплату для аккаунта</returns>
        private Invoice CreateInvoiceForAccount(Account account, decimal cost, InvoiceStatus invoiceStatus, int? period)
        {
            var generator = new TestDataGenerator(Configuration, DbLayer);

            var inn = TestContext.ServiceProvider.GetRequiredService<IAccountRequisitesDataProvider>()
                .GetIfExistsOrCreateNew(account.Id).Inn;

            var invoice = generator.GenerateInvoice(account.Id, inn,
                cost, invoiceStatus, period);

            DbLayer.InvoiceRepository.AddInvoice(invoice);
            DbLayer.Save();

            return invoice;
        }
    }
}
