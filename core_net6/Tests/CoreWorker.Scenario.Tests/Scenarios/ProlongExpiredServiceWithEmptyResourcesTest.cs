﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления просроченного сервиса если есть деньги и есть свободные платные ресурсы.
    /// </summary>
    public class ProlongExpiredServiceWithEmptyResourcesTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteServiceTypeResourcesByAccountUser();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var resConfExpireDate = DateTime.Now;
            ChangeResourceConfigurationExpireDate(resConfExpireDate);
            
            var serviceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s => (int) s.SystemServiceType == 2).Id;

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var rateProvider = TestContext.ServiceProvider.GetRequiredService<IOptimalRateProvider>();

            var usrCost = rateProvider.GetOptimalRate(billingAccount, ResourceType.MyEntUser).Cost;
            var usrWebCost = rateProvider.GetOptimalRate(billingAccount, ResourceType.MyEntUserWeb).Cost;

            AddEmptyRent1CResource(createAccountCommand, serviceTypeId, usrWebCost);
            serviceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s => s.SystemServiceType == ResourceType.MyEntUserWeb).Id;
            AddEmptyRent1CResource(createAccountCommand, serviceTypeId, usrCost);

            var billingService = GetRent1CServiceOrThrowException();

            var paymentTotalSum = usrCost + usrWebCost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            RefreshDbCashContext(TestContext.DbLayer.GetGenericRepository<MyDatabasesResource>().GetAll());

            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);

            var hasEmptyResources = DbLayer.ResourceRepository.Where(r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !r.Subject.HasValue).Any();

            Assert.AreEqual(hasEmptyResources, false);
        }

        /// <summary>
        /// Добавить пустой ресурс Аренды 1С
        /// </summary>
        /// <param name="account">Модель аккаунта</param>
        /// <param name="billingServiceTypeId">Id услуги сервиса</param>
        /// <param name="cost">Стоимость</param>
        private void AddEmptyRent1CResource(IAccountDetails account, Guid billingServiceTypeId, decimal cost)
        {
            DbLayer.ResourceRepository.Insert(new Resource
            {
                Id = Guid.NewGuid(),
                AccountId = account.AccountId,
                Cost = cost,
                IsFree = false,
                BillingServiceTypeId = billingServiceTypeId
            });

            DbLayer.Save();
        }
    }
}
