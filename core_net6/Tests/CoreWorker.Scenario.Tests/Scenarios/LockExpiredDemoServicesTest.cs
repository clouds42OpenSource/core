﻿using System;
using System.Linq;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий блокировки сервисов с просроченным демо периодом
    ///     Действия:
    ///         1) Создаем кастомный сервис и устанавливаем ему просроченный демо период
    ///         2) Вызываем метод блокировки просроченных демо сервисов
    ///         3) Проверяем что сервис заблокировался, а Аренда 1С нет 
    /// </summary>
    public class LockExpiredDemoServicesTest : ScenarioBase
    {
        private readonly IBillingManager _billingManager;
        public LockExpiredDemoServicesTest()
        {
            _billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
        }

        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });
            createCustomService.Run();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.IsDemoPeriod && x.BillingServiceId == createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate).SetProperty(y => y.Frozen, false));

            DbLayer.RefreshAll();

            _billingManager.LockExpiredDemoServices();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.IsFalse(resourceRent1C.FrozenValue);

            var customResConfig =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, createCustomService.Id);
            
            Assert.IsNotNull(customResConfig);
            Assert.IsTrue(customResConfig.FrozenValue);
        }
    }
}
