﻿using Clouds42.Billing;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки по периоду при уведомления о скорой блокиоровки сервиса
    /// и зависимых от него сервисов
    /// Создаем зависимый от аренды сервис. Создаем счет по периуду 7 месяцев.
    /// Выполняем пролонгацию и новый счет должен создасться за такой же период
    /// </summary>
    public class ScenarioCheckPeriodAtLockServiceAndDependentServicesNotificationTest : ScenarioBase
    {
        private readonly IResourceConfigurationDataProvider _resourceConfigurationDataProvider;

        public ScenarioCheckPeriodAtLockServiceAndDependentServicesNotificationTest()
        {
            _resourceConfigurationDataProvider = TestContext.ServiceProvider.GetRequiredService<IResourceConfigurationDataProvider>();
        }

        public override void Run()
        {
            var standardServiceType = GetStandardServiceTypeOrThrowException();

            var generator = new TestDataGenerator(Configuration,DbLayer);
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standardServiceType.Id);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                    {BillingServiceTypes = billingServiceTypes});

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            var paymentSum = 100;

            CreateInflowPayment(createAccountCommand.AccountId, null, paymentSum);

            var resConfExpireDate = DateTime.Now.AddDays(4);

            var rent1CService = GetRent1CServiceOrThrowException();

            ChangeResourceConfigurationExpireDate(resConfExpireDate, rent1CService.Id);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            var resConfModel = _resourceConfigurationDataProvider.GetConfigWithDependencies(resourceRent1C);
            var totalCost = _resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel);

            var invoice = generator.GenerateInvoice(createAccountCommand.AccountId, createAccountCommand.Inn,
                totalCost * 7, InvoiceStatus.Processed, 7);

            DbLayer.InvoiceRepository.AddInvoice(invoice);
            DbLayer.Save();
            
            billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.Id != invoice.Id && w.AccountId == createAccountCommand.AccountId);

            Assert.IsNotNull(anotherInvoice, "Ошибка, не создался новый счет");
            Assert.AreEqual(invoice.Sum, anotherInvoice.Sum, "Не совпадают счета");
            Assert.AreEqual(invoice.Period, anotherInvoice.Period, "Не совпадают счета");
        }
    }
}
