﻿using System;
using Clouds42.AccountDatabase.Delete.Managers;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios.DeleteInactiveAccountDataTests
{
    /// <summary>
    /// Сценарий для проверки удаления данных неактивного аккаунта,
    /// если есть демо база на разделителях
    ///
    ///         1) Создаем обычный аккаунт с демо базой на разделителях
    ///         2) Устанавливаем дату окончания аренды 1С меньше чем граничная дата перед удалением данных (-90 дней от текущей)
    ///         3) Имитируем запуск джобы по удалению данных
    ///         4) Проверяем что запрос в МС на удаление не отправляется(при вызове команды произойдет ошибка)
    ///         5) Проверяем что база аккаунта была удалена (сменился статус на DeletedFromCloud)
    /// </summary>
    public class ScenarioDeleteInactiveAccountDataIfThereIsDbOnDelimiterDemoTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            sourceAccountDatabaseCommand.Run();

            ServiceProvider.GetRequiredService<ServiceAccountManager>().AddServiceAccountItem(new AddServiceAccountDto
            {
                AccountId = sourceAccountDatabaseCommand.AccountDetails.AccountId,
            });

            var templateDelimiters =
                DbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault();

            var dbTemplateItemDto = new DbTemplateItemDto {Name = templateDelimiters.Template.Name};
            
            var dbOnDelimiter = new InfoDatabaseDomainModelTest(TestContext, dbTemplateItemDto)
            {
                DbTemplateDelimiters = true, 
                DataBaseName = "Тестовая база на разделителях"
            };

            var createDbOnDelimiterCommand = new CreateAccountDatabaseViaDelimiterCommand(TestContext, createAccountCommand, dbOnDelimiter);
            createDbOnDelimiterCommand.Run();

            var databaseOnDelimiter = DbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(db =>
                db.AccountDatabaseId == createDbOnDelimiterCommand.AccountDatabaseId);

            Assert.IsNotNull(databaseOnDelimiter);

            databaseOnDelimiter.IsDemo = true;
            DbLayer.AccountDatabaseDelimitersRepository.Update(databaseOnDelimiter);
            DbLayer.Save();

            var rent1CExpireDate = DateTime.Now.AddDays(-CloudConfigurationProvider.ConfigRental1C.ServiceExpired
                .GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData()).AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, rent1CExpireDate)
                    .SetProperty(y => y.Frozen, false));

            Services.AddTransient<IDeleteDelimiterCommand, DeleteDelimiterCommandErrorTestAdapter>();
            ServiceProvider.GetRequiredService<DeleteInactiveAccountDataManager>().DeleteInactiveAccountData();

            var accountDatabases =
                DbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.AccountId == createAccountCommand.AccountId);

            Assert.IsNotNull(accountDatabases);
            Assert.AreEqual(accountDatabases.StateEnum, DatabaseState.DeletedFromCloud);
        }
    }
}
