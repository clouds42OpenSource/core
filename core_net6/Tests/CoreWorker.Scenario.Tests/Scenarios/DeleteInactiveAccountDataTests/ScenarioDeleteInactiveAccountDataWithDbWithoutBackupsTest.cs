﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Delete.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios.DeleteInactiveAccountDataTests
{
    /// <summary>
    /// Сценарий для проверки удаления данных неактивного аккаунта с базами без бэкапов
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базами без бэкапов
    ///         2) Устанавливаем дату окончания аренды 1С меньше чем граничная дата перед удалением данных (-90 дней от текущей)
    ///         3) Имитируем запуск джобы по удалению данных 
    ///         4) Проверяем что базы аккаунта были удалены (сменился статус на DeletedFromCloud и база удалена физически)
    /// </summary>
    public class ScenarioDeleteInactiveAccountDataWithDbWithoutBackupsTest : ScenarioBase
    {
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly DeleteInactiveAccountDataManager _deleteInactiveAccountDataManager;
        private readonly Lazy<int> _daysCountAfterRent1CExpiredForAccount;

        public ScenarioDeleteInactiveAccountDataWithDbWithoutBackupsTest()
        {
            _daysCountAfterRent1CExpiredForAccount = new Lazy<int>(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
                .GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData);

            _deleteInactiveAccountDataManager = TestContext.ServiceProvider.GetRequiredService<DeleteInactiveAccountDataManager>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            CreateAccountDatabasesByCount(createAccountCommand, 5);

            var rent1CExpireDate = DateTime.Now.AddDays(-_daysCountAfterRent1CExpiredForAccount.Value).AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, rent1CExpireDate)
                    .SetProperty(y => y.Frozen, false));

            _deleteInactiveAccountDataManager.DeleteInactiveAccountData();

            var account = DbLayer.AccountsRepository.GetAccount(createAccountCommand.AccountId);

            Assert.IsNotNull(account);

            CheckDeletionOfAccountDatabases(createAccountCommand.AccountId);
        }

        /// <summary>
        /// Проверит что базы аккаунта удалены
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void CheckDeletionOfAccountDatabases(Guid accountId)
        {
            var accountDatabases = DbLayer.DatabasesRepository.Where(acDb => acDb.AccountId == accountId).ToList();

            accountDatabases.ForEach(database =>
            {
                Assert.AreEqual(database.StateEnum, DatabaseState.DeletedFromCloud);
                var accountDatabasePath = _accountDatabasePathHelper.GetPath(database);
                Assert.IsFalse(Directory.Exists(accountDatabasePath));
            });
        }

        /// <summary>
        /// Создать инф. базы для аккаунта
        /// </summary>
        /// <param name="accountDetails">Информация об аккаунте</param>
        /// <param name="databasesCount">Количество баз</param>
        private void CreateAccountDatabasesByCount(IAccountDetails accountDetails, int databasesCount)
        {
            for (var i = 0; i < databasesCount; i++)
            {
                var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetails);
                createAccountDatabaseCommand.Run();
            }
        }
    }
}
