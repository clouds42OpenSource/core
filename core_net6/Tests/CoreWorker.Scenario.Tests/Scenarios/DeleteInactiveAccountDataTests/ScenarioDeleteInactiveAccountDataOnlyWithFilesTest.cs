﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Delete.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios.DeleteInactiveAccountDataTests
{
    /// <summary>
    /// Сценарий для проверки удаления данных неактивного аккаунта
    ///     Действия:
    ///         1) Создаем аккаунт с файлами на файловом хранилище и с одной инф. базой (что бы выборка отработала)
    ///         2) Устанавливаем дату окончания аренды 1С меньше чем граничная дата перед удалением данных (-90 дней от текущей)
    ///         3) Имитируем запуск джобы по удалению данных 
    ///         4) Проверяем что удалены файлы аккаунта
    /// </summary>
    public class ScenarioDeleteInactiveAccountDataOnlyWithFilesTest : ScenarioBase
    {
        private readonly DeleteInactiveAccountDataManager _deleteInactiveAccountDataManager;
        private readonly Lazy<int> _daysCountAfterRent1CExpiredForAccount;
        private readonly CreateTextFileHelper _createTextFileHelper;
        private readonly CreateZipFileHelper _createZipFileHelper;

        public ScenarioDeleteInactiveAccountDataOnlyWithFilesTest()
        {
            _daysCountAfterRent1CExpiredForAccount = new Lazy<int>(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
                .GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData);

            _deleteInactiveAccountDataManager = TestContext.ServiceProvider.GetRequiredService<DeleteInactiveAccountDataManager>();
            _createTextFileHelper = TestContext.ServiceProvider.GetRequiredService<CreateTextFileHelper>();
            _createZipFileHelper = TestContext.ServiceProvider.GetRequiredService<CreateZipFileHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, accountModel: new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            GenerateAccountFiles(createAccountCommand.AccountDetails.Account, 8);

            var rent1CExpireDate = DateTime.Now.AddDays(-_daysCountAfterRent1CExpiredForAccount.Value).AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, rent1CExpireDate)
                    .SetProperty(y => y.Frozen, false));

            _deleteInactiveAccountDataManager.DeleteInactiveAccountData();

            var account = DbLayer.AccountsRepository.GetAccount(createAccountCommand.AccountDetails.AccountId);

            Assert.IsNotNull(account);

            CheckAccountFilesDeletion(account);
        }

        /// <summary>
        /// Сгенерировать файлы аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="filesCount">Количество файлов</param>
        private void GenerateAccountFiles(Account account, int filesCount)
        {
            var segmentHelper = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>();

            var accountFileStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
            var accountArchiveStoragePath = Path.Combine(segmentHelper.GetBackupStorage(account), $"account_{account.IndexNumber}");

            for (var fileIndex = 0; fileIndex < filesCount; fileIndex++)
            {
                _createTextFileHelper.CreateTextFileForTest(Path.Combine(accountFileStoragePath,
                    $"file_{account.IndexNumber}_{fileIndex}.txt"));
            }

            if (!Directory.Exists(accountArchiveStoragePath))
                Directory.CreateDirectory(accountArchiveStoragePath);

            _createZipFileHelper.CreateZipFileWithDumpInfo(Path.Combine(accountArchiveStoragePath, "AccountFilesBackup.zip"));
        }

        /// <summary>
        /// Проверить удаление файлов аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        private void CheckAccountFilesDeletion(Account account)
        {
            var segmentHelper = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>();

            var accountFileStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
            var accountArchiveStoragePath = Path.Combine(segmentHelper.GetBackupStorage(account), $"account_{account.IndexNumber}");
            
            Assert.IsFalse(Directory.Exists(accountFileStoragePath));
            Assert.IsFalse(Directory.Exists(accountArchiveStoragePath));
        }
    }
}
