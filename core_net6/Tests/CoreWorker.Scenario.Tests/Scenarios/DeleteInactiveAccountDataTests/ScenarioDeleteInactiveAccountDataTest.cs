﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Delete.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios.DeleteInactiveAccountDataTests
{
    /// <summary>
    /// Сценарий для проверки удаления данных неактивного аккаунта
    ///     Действия:
    ///         1) Создаем обычный аккаунт с инф. базами, с файлами, бэками инф. баз
    ///         2) Устанавливаем дату окончания аренды 1С меньше чем граничная дата перед удалением данных (-90 дней от текущей)
    ///         3) Имитируем запуск джобы по удалению данных 
    ///         4) Проверяем что базы аккаунта были удалены (сменился статус на DeletedFromCloud и база удалена физически)
    ///         5) Проверяем что все бэкапы удалены с хранилищ и также удалены сами записи о бэкапах
    ///         6) Проверяем что удалены файлы аккаунта
    /// </summary>
    public class ScenarioDeleteInactiveAccountDataTest : ScenarioBase
    {
        private readonly DeleteInactiveAccountDataManager _deleteInactiveAccountDataManager;
        private readonly Lazy<int> _daysCountAfterRent1CExpiredForAccount;
        private readonly CreateTextFileHelper _createTextFileHelper;
        private readonly CreateZipFileHelper _createZipFileHelper;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly ServiceAccountManager _serviceAccountManager;

        public ScenarioDeleteInactiveAccountDataTest()
        {
            _daysCountAfterRent1CExpiredForAccount = new Lazy<int>(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
                .GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData);

            _deleteInactiveAccountDataManager = TestContext.ServiceProvider.GetRequiredService<DeleteInactiveAccountDataManager>();
            _createTextFileHelper = TestContext.ServiceProvider.GetRequiredService<CreateTextFileHelper>();
            _createZipFileHelper = TestContext.ServiceProvider.GetRequiredService<CreateZipFileHelper>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _serviceAccountManager = TestContext.ServiceProvider.GetRequiredService<ServiceAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            CreateServiceAccount();

            CreateAccountData(createAccountCommand);

            var accountDatabaseBackups =
                DbLayer.AccountDatabaseBackupRepository.Where(w =>
                    w.AccountDatabase.AccountId == createAccountCommand.AccountId);

            Assert.IsTrue(accountDatabaseBackups.Any());

            var rent1CExpireDate = DateTime.Now.AddDays(-_daysCountAfterRent1CExpiredForAccount.Value).AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, rent1CExpireDate)
                .SetProperty(y => y.Frozen, false));

            Services.AddTransient<IDeleteDelimiterCommand, DeleteDelimiterCommandTestAdapter>();
            _deleteInactiveAccountDataManager.DeleteInactiveAccountData();

            var account = DbLayer.AccountsRepository.GetAccount(createAccountCommand.AccountId);

            Assert.IsNotNull(account);

            CheckAccountFilesDeletion(account);
            CheckDeletionOfAccountDatabases(account.Id);
        }

        /// <summary>
        /// Создать бэкап для инф. базы
        /// </summary>
        /// <param name="accountDatabaseDetails">Данные об инф. базе</param>
        private void CreateAccountDatabasesBackups(IAccountDatabseDetails accountDatabaseDetails)
        {
            var databaseBackupCommand = new CreateAccountDatabaseBackupCommand(TestContext, accountDatabaseDetails);
            databaseBackupCommand.Run();
        }

        /// <summary>
        /// Сгенерировать файлы аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="filesCount">Количество файлов</param>
        private void GenerateAccountFiles(Account account, int filesCount)
        {
            var segmentHelper = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>();

            var accountFileStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
            var accountArchiveStoragePath = Path.Combine(segmentHelper.GetBackupStorage(account), $"account_{account.IndexNumber}");

            for (var fileIndex = 0; fileIndex < filesCount; fileIndex++)
            {
                _createTextFileHelper.CreateTextFileForTest(Path.Combine(accountFileStoragePath,
                    $"file_{account.IndexNumber}_{fileIndex}.txt"));
            }

            if (!Directory.Exists(accountArchiveStoragePath))
                Directory.CreateDirectory(accountArchiveStoragePath);

            _createZipFileHelper.CreateZipFileWithDumpInfo(Path.Combine(accountArchiveStoragePath, "AccountFilesBackup.zip"));
        }

        /// <summary>
        /// Проверить удаление файлов аккаунта
        /// </summary>
        /// <param name="account">Аккаунт</param>
        private void CheckAccountFilesDeletion(Account account)
        {
            var segmentHelper = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>();

            var accountFileStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
            var accountArchiveStoragePath = Path.Combine(segmentHelper.GetBackupStorage(account), $"account_{account.IndexNumber}");

            Assert.IsFalse(Directory.Exists(accountFileStoragePath));
            Assert.IsFalse(Directory.Exists(accountArchiveStoragePath));
        }

        /// <summary>
        /// Проверит что базы аккаунта удалены
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void CheckDeletionOfAccountDatabases(Guid accountId)
        {
            var accountDatabases = DbLayer.DatabasesRepository
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountDatabaseBackups)
                .Where(acDb => acDb.AccountId == accountId)
                .ToList();

            accountDatabases.ForEach(database =>
            {
                if (database.IsDelimiter())
                {
                    Assert.IsFalse(database.AccountDatabaseBackups.Any());
                    Assert.AreEqual(database.StateEnum, DatabaseState.DeletedFromCloud);
                    return;
                }
                Assert.AreEqual(database.StateEnum, DatabaseState.DeletedFromCloud);
                var accountDatabasePath = _accountDatabasePathHelper.GetPath(database);
                Assert.IsFalse(Directory.Exists(accountDatabasePath));
                Assert.IsFalse(database.AccountDatabaseBackups.Any());
            });
        }

        /// <summary>
        /// Создать аккаунт с материнской базой
        /// </summary>
        private void CreateServiceAccount()
        {
            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });
            
            var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext,
                new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id));
            sourceAccountDatabaseCommand.Run();

            _serviceAccountManager.AddServiceAccountItem(new AddServiceAccountDto
            {
                AccountId = sourceAccountDatabaseCommand.AccountDetails.AccountId,
            });
        }

        /// <summary>
        /// Создать данные для аккаунта (файлы и базы)
        /// </summary>
        /// <param name="accountDetails">Данные об аккаунте</param>
        private void CreateAccountData(IAccountDetails accountDetails)
        {
            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetails);
            createAccountDatabaseCommand.Run();

            var template =
                DbLayer.DbTemplateRepository.FirstOrDefault(dbTemplate =>
                    dbTemplate.Name == DbTemplatesNameConstants.BpName);

            Assert.IsNotNull(template);

            var dbTemplateModelTest = new DbTemplateModelTest(DbLayer)
            {
                Name = template.Name
            };

            var dbOnDelimiter = new InfoDatabaseDomainModelTest(TestContext, dbTemplateModelTest)
            {
                DbTemplateDelimiters = true, DataBaseName = "Тестовая база на разделителях"
            };
            var createDbOnDelimiterCommand = new CreateAccountDatabaseViaDelimiterCommand(TestContext, accountDetails, dbOnDelimiter);
            createDbOnDelimiterCommand.Run();

            GenerateAccountFiles(accountDetails.Account, 8);

            CreateAccountDatabasesBackups(createAccountDatabaseCommand);
        }
    }
}
