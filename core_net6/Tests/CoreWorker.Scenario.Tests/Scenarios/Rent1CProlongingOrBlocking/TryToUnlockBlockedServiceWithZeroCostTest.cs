﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.CoreWorker;

namespace CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking
{
    /// <summary>
    ///     Сценарий проверки на отсутствие продления сервиса при отключенной
    /// аренде, цене - 0, и уже заблокированном сервисе, без спонсируемых пользователей.
    /// 
    ///     Предусловия: у аккаунта test_account отключена аренда, пользователь 
    /// отключен от аренды; баланс 0 руб; дата окончания сервиса -1 месяц от текущей даты;
    /// спонсируемые пользователи отсутствуют.
    /// 
    ///     Действия: запускаем таску 42CloudsProlongationJob.
    /// 
    ///     Проверка: сервис должен остаться заблокированным, цена - 0,
    /// дата окончания сервиса -1 месяц от текущей даты.
    /// </summary>
    public class TryToUnlockBlockedServiceWithZeroCostTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };

            rent1CManager.ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);

            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, rent1CService.Id);
            
            var billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(-1).Date, resourceRent1C.ExpireDateValue.Date);
            Assert.AreEqual(0, resourceRent1C.Cost);

            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(item =>
                item.TaskName == CoreWorkerTaskType.Regular42CloudServiceProlongationJob.ToString());

            using var scope = ServiceProvider.CreateScope();
            var prolongJob = scope.ServiceProvider.GetRequiredService<Regular42CloudServiceProlongationJob>();
            prolongJob.Execute(coreWorkerTask.ID, Guid.NewGuid());

            var resourceConfAfterProlong =
                GetResourcesConfigurationOrThrowException(
                    createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, rent1CService.Id);

            Assert.IsNotNull(resourceConfAfterProlong);
            Assert.IsTrue(resourceConfAfterProlong.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(-1).Date, resourceConfAfterProlong.ExpireDateValue.Date);
            Assert.AreEqual(0, resourceConfAfterProlong.Cost);
        }
    }
}
