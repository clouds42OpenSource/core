﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;

namespace CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking
{
    /// <summary>
    ///     Сценарий проверки на продление при заблокированном
    /// сервисе, один пользователь подключен, цена - 1500, без спонсируемых.
    /// 
    ///     Предусловия: у аккаунта test_account аренда заблокирована, к аренде подключен
    /// админ аккаунта; баланс 1500 руб; дата окончания сервиса -7 дней от сегодняшней даты;
    /// без спонсируемых пользователей.
    /// 
    ///     Действия: запускаем таску 42CloudsProlongationJob.
    /// 
    ///     Проверка: сервис должно разблокировать, цена - 1500,
    /// дата окончания сервиса +1 месяц от текущей даты, баланс - 0, пользователь состоит в нужных
    /// группах AD.
    /// </summary>
    public class UnlockExpiredServiceWithMoneyTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            
            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();

            var billingService = GetRent1CServiceOrThrowException();


            var locale = GetLocaleByNameOrThrowException(LocaleConst.Russia);
            var rate = DbLayer.RateRepository
                .Where(r => r.BillingServiceType.ServiceId == billingService.Id && r.LocaleId == locale.ID)
                .Sum(r => r.Cost);

            var accountUsersCount = 2;
            var paymentSum = rate * accountUsersCount;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentSum);

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-7)
            };
            rent1CManager.ManageRent1C(createAccountCommand.AccountId, blockRent1C);

            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(item =>
                item.TaskName == CoreWorkerTaskType.Regular42CloudServiceProlongationJob.ToString());

            using var scope = ServiceProvider.CreateScope();
            var prolongJob = scope.ServiceProvider.GetRequiredService<Regular42CloudServiceProlongationJob>();
            prolongJob.Execute(coreWorkerTask.ID, Guid.NewGuid());

            DbLayer.RefreshAll();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(DateTime.Now.AddMonths(1).Date, resourceRent1C.ExpireDateValue.Date);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var hasEmptyResources = DbLayer.ResourceRepository.Where(r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !r.Subject.HasValue).Any();

            Assert.IsFalse(hasEmptyResources);
        }
    }
}
