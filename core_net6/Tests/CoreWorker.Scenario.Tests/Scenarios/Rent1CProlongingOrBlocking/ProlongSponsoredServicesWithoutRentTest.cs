﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;

namespace CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking
{
    /// <summary>
    ///     Сценарий проверки на продления сервиса при отключенной
    /// аренде, цене - 0, и уже заблокированном сервисе, есть спонсируемый пользователей.
    /// 
    ///     Предусловия: у аккаунта test_account активна аренда, пользователи 
    /// не подключены; баланс 0 руб; дата окончания сервиса -1 месяц от текущей даты;
    /// есть пользователь который спонсируется другим аккаунтом.
    /// 
    ///     Действия: запускаем таску 42CloudsProlongationJob.
    /// 
    ///      Проверка: сервис должно разблокировать, цена - 0,
    /// дата окончания сервиса +1 месяц от текущей даты.
    /// </summary>
    public class ProlongSponsoredServicesWithoutRentTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<BillingServiceDataProvider>();

            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 1500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };

            rent1CManager.ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);

            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            accessRent1C.First().StandartResource = true;

            var res = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, rent1CService.Id);

            var resourceSecondRent1C =
                GetResourcesConfigurationOrThrowException(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, rent1CService.Id);
            
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(acc =>
                acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
             where access.AccountUserID == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId
             select access).ToList();

            Assert.IsFalse(accessList.Any(t => t.IsLock), "Ошибка спонсирования аренды 1С");

            Assert.IsNull(res.Result.ErrorMessage);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.IsNotNull(resourceSecondRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).Date, resourceRent1C.ExpireDateValue.Date);
            Assert.AreEqual(0, resourceRent1C.Cost);
            Assert.IsFalse(resourceSecondRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddDays(7).Date, resourceSecondRent1C.ExpireDateValue.Date);
            Assert.AreEqual(3000, resourceSecondRent1C.Cost);

            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(item =>
                item.TaskName == CoreWorkerTaskType.Regular42CloudServiceProlongationJob.ToString());

            using var scope = ServiceProvider.CreateScope();
            var prolongJob = scope.ServiceProvider.GetRequiredService<Regular42CloudServiceProlongationJob>();
            prolongJob.Execute(coreWorkerTask.ID, Guid.NewGuid());

            var sponsoredResourceConfAfterProlong = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                rent1CService.Id);

            Assert.IsNotNull(sponsoredResourceConfAfterProlong);
            Assert.IsFalse(sponsoredResourceConfAfterProlong.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).Date, sponsoredResourceConfAfterProlong.ExpireDateValue.Date);
            Assert.AreEqual(0, sponsoredResourceConfAfterProlong.Cost);

            var secondResourceConfAfterProlong = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                rent1CService.Id);
            
            Assert.IsNotNull(secondResourceConfAfterProlong);
            Assert.IsFalse(secondResourceConfAfterProlong.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddDays(7).Date, secondResourceConfAfterProlong.ExpireDateValue.Date);
            Assert.AreEqual(3000, secondResourceConfAfterProlong.Cost);
        }
    }
}
