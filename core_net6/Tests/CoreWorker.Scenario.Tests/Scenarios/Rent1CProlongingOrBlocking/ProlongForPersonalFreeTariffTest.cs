﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;

namespace CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking
{
    /// <summary>
    ///     Сценарий проверки на продление сервиса, один подключенный пользователь,
    /// установлен персональный тариф - 0, без спонсируемых пользователей.
    /// 
    ///     Предусловия: у аккаунта test_account активна аренда, один подключенный
    /// пользователь; баланс 0 руб; дата окончания сервиса - сегодня;
    /// установлен персональный тариф - 0; без спонсируемых пользователей.
    /// 
    ///     Действия: запускаем таску 42CloudsProlongationJob.
    /// 
    ///     Проверка: сервис продлен, цена - 0, дата окончания сервиса +1 месяц от
    /// текущей даты.
    /// </summary>
    public class ProlongForPersonalFreeTariffTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddHours(3),
                CostOfRdpLicense = 0,
                CostOfWebLicense = 0
            };

            rent1CManager.ManageRent1C(createAccountCommand.AccountId, blockRent1C);

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            var billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.AreEqual(blockRent1C.ExpireDate.Date, resourceRent1C.ExpireDateValue.Date);
            Assert.AreEqual(0, resourceRent1C.Cost);
            
            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(item =>
                item.TaskName == CoreWorkerTaskType.Regular42CloudServiceProlongationJob.ToString());

            using var scope = ServiceProvider.CreateScope();
            var prolongJob = scope.ServiceProvider.GetRequiredService<Regular42CloudServiceProlongationJob>();
            prolongJob.Execute(coreWorkerTask.ID, Guid.NewGuid());

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var resourceConfAfterProlong = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            Assert.IsNotNull(resourceConfAfterProlong);
            Assert.IsFalse(resourceConfAfterProlong.FrozenValue);
            Assert.AreEqual(0, resourceConfAfterProlong.Cost);
        }
    }
}
