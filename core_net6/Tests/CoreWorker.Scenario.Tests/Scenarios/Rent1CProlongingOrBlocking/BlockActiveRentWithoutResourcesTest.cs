﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking
{
    /// <summary>
    ///     Сценарий блокировки сервиса при активной аренде, пользователь не подключен
    /// без спонсируемых пользователей.
    /// 
    ///     Предусловия: у аккаунта test_account есть активная аренда, но пользователи 
    /// не подключены; баланс 0 руб; дата окончания сервиса - сегодня;
    /// спонсируемые пользователи отсутствуют.
    /// 
    ///     Действия: запускаем таску 42CloudsProlongationJob.
    /// 
    ///     Проверка: сервис должно заблокировать, лицензии отсутствовать.
    /// </summary>
    public class BlockActiveRentWithoutResourcesTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DbLayer.ResourceRepository.AsQueryable().Where(x => !x.Subject.HasValue).ExecuteDelete();

            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountCommand.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };

            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountCommand.AccountId, accessRent1C);

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddHours(1)
            };
            rent1CManager.ManageRent1C(createAccountCommand.AccountId, blockRent1C);

            var coreWorkerTask = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(item =>
                item.TaskName == CoreWorkerTaskType.Regular42CloudServiceProlongationJob.ToString());

            var prolongJob = ServiceProvider.GetRequiredService<Regular42CloudServiceProlongationJob>();
            prolongJob.Execute(coreWorkerTask.ID, Guid.NewGuid());
            
            TestContext.DbLayer.RefreshAll();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDateValue.Date, blockRent1C.ExpireDate.Date);

            var hasEmptyResources = DbLayer.ResourceRepository.Where(r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !r.Subject.HasValue)
                .Any();

            Assert.IsFalse(hasEmptyResources);
        }
    }
}
