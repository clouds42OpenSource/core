﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки уведомления о скорой блокировки сервисов Аренда 1С и Мой диск
    /// </summary>
    public class ScenarioCheckLockServiceNotificationForRent1CAndMyDiskServiceTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var defaultPeriodForInvoice = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();
            var generator = new TestDataGenerator(Configuration, DbLayer);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var diskServiceType =
                await DbLayer.BillingServiceTypeRepository.FirstOrDefaultAsync(
                    st => st.SystemServiceType == ResourceType.DiskSpace);

            await DbLayer.ResourceRepository
                .AsQueryable()
                .Where(x => x.BillingServiceTypeId == diskServiceType.Id)
                .ExecuteDeleteAsync();

            var resource = generator.GenerateResource(createAccountCommand.AccountId, ResourceType.DiskSpace,
                createAccountCommand.AccountId, null, 2000);
            DbLayer.ResourceRepository.Insert(resource);
            await DbLayer.SaveAsync();

            const int diskSize = 999;

            var diskResource = new DiskResource
            {
                VolumeGb = diskSize,
                ResourceId = resource.Id
            };

            DbLayer.DiskResourceRepository.Insert(diskResource);
            await DbLayer.SaveAsync();

            const int paymentSum = 100;

            CreateInflowPayment(createAccountCommand.AccountId, null, paymentSum);

            var serviceType =
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyDisk);
            using var transaction = DbLayer.SmartTransaction.Get();
            await Mediator.Send(new RecalculateResourcesConfigurationCostCommand(
               [createAccountCommand.AccountId], [serviceType.Id],
                serviceType.SystemService));
            transaction.Commit();
            var resConfExpireDate = DateTime.Now.AddDays(5);

            ChangeResourceConfigurationExpireDate(resConfExpireDate);

            Services.AddTransient<IProlongServicesProvider, Clouds42.Billing.Billing.Providers.BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var rent1CService = GetRent1CServiceOrThrowException();
            var myDiskService = GetMyDiskServiceOrThrowException();

            var resourceConfMyDisk = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            var resourceConfRent1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, myDiskService.Id);
            
            billingManager.NotifyBeforeLockServices();

            var invoice = await DbLayer.InvoiceRepository.FirstOrDefaultAsync(w => w.AccountId == createAccountCommand.AccountId);

            var invoiceCost = defaultPeriodForInvoice * resourceConfMyDisk.Cost +
                              defaultPeriodForInvoice * resourceConfRent1C.Cost;

            if (invoice.Period != defaultPeriodForInvoice || invoice.Sum != invoiceCost)
                throw new InvalidOperationException("Не корректно работает создание нового счета");

            var invoiceProducts = await DbLayer.InvoiceProductRepository.AsQueryable().Where(w =>
                w.InvoiceId == invoice.Id).ToListAsync();

            if (invoiceProducts == null ||
                !invoiceProducts.Any() ||
                invoiceProducts.Sum(ip => ip.ServiceSum) != invoiceCost)
                throw new InvalidOperationException("Не корректно создаются продукты счета");

            var invoiceProductMyDisk =
                invoiceProducts.FirstOrDefault(ip => ip.ServiceName == Clouds42Service.MyDisk.ToString());

            if (invoiceProductMyDisk == null || invoiceProductMyDisk.Count != diskSize)
                throw new InvalidOperationException("Не корректно создаются продукты счета");
        }
    }
}
