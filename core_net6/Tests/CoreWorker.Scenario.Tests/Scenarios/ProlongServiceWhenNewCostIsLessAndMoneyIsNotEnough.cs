﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость меньше чем старая стоимость, но на счету не достаточно денег.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем стоимость подписки
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис не разблокирован и не продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsLessAndMoneyIsNotEnough : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;

        public ProlongServiceWhenNewCostIsLessAndMoneyIsNotEnough()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            #region customservice
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();
            #endregion customservice

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            var resources = DbLayer.ResourceConfigurationRepository.Where(w => w.BillingServiceId != createCustomService.Id);

            foreach (var resource in resources)
            {
                resource.ExpireDate = DateTime.Now.AddDays(-1);
                resource.Frozen = true;
                DbLayer.ResourceConfigurationRepository.Update(resource);
            }
            DbLayer.Save();

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = false,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id, false, null, dataAccountUsers);

            #region PaymentSum

            var billingService = GetRent1CServiceOrThrowException();

            var resConf1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            var paymentTotalSum = resConf1C.Cost - 300;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);

            #endregion PaymentSum

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, true);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddDays(-1).Date);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.AreEqual(billingAccount.Balance, resConf1C.Cost - 300);
        }
    }
}
