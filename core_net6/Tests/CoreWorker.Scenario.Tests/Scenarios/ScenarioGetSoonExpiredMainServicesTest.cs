﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста выборки сервисов, которые можно заблокировать
    /// через n дней
    ///
    /// 1) Создаем аккаунт и обновляем дату аренды 1С (аккаунт должен попасть в выборку о скорой блокировки сервиса)
    /// 2) Выполняем метод "Уведомления о скорой блокировки сервиса" и ожидаем что счет для ааккаунта не создался.(нет входящих транзакций)
    /// 3) Создаем входящий платеж и повторяем вызов метода, ожидая что счет на оплату будет создан для аккаунта
    /// </summary>
    public class ScenarioGetSoonExpiredMainServicesTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var rent1CService = GetRent1CServiceOrThrowException();

            var rent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            rent1C.ExpireDate = DateTime.Now.AddDays(4);
            TestContext.Context.ResourcesConfigurations.Update(rent1C);
            TestContext.Context.SaveChanges();

            var prolongServicesProvider = ServiceProvider.GetRequiredService<IProlongServicesProvider>();
            prolongServicesProvider.NotifyBeforeLockServices();

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);

            if (invoice != null)
                throw new InvalidOperationException("Демо аккаунт не должен попадать в выборку на уведомление");

            var paymentSum = 1000;

            CreateInflowPayment(createAccountCommand.AccountId, null, paymentSum);
            
            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            prolongServicesProvider.NotifyBeforeLockServices();

            invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);

            if (invoice == null)
                throw new InvalidOperationException("Не корректно работает выборка аккаунтов на уведомление");
        }
    }
}
