﻿using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Проверка невозможности блокировки Аренды у Сервисного аккаунта
    /// Сценарий:
    /// 1) Создаём аккаунт с арендой и делаем его серверным
    /// 2) Изменяем дату окончания сервиса на "вчера"
    /// 3) Запускаем задачу пролонгации/блокировки просроченых сервисов
    /// 4) Проверяем, что Аренда нашего сервисоного аккаунта активна
    /// </summary>
    public class TryToBlockServiceAccountRent: ScenarioBase
    {
        private readonly ServiceAccountManager _serviceAccountManager;

        public TryToBlockServiceAccountRent()
        {
            _serviceAccountManager = TestContext.ServiceProvider.GetRequiredService<ServiceAccountManager>();
        }
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            _serviceAccountManager.AddServiceAccountItem(new AddServiceAccountDto
            {
                AccountId = createAccountCommand.AccountId,
            });

            var rent1CService = GetRent1CServiceOrThrowException();

            var rent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            rent1C.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.Save();

            RefreshDbCashContext(TestContext.DbLayer.AccountConfigurationRepository.GetAll());
            RefreshDbCashContext(TestContext.DbLayer.ServiceAccountRepository.GetAll());
            
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());
            rent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            Assert.IsFalse(rent1C.FrozenValue,"Аренда служебного аккаунта не должна блокироватся");

        }
    }
}
