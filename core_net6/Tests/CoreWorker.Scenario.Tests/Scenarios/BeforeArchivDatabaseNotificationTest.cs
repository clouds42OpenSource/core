﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Accounts.Account.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Проверка отправки уведомления после архивации баз неактивных аккаунтов
    /// Сценарий:
    ///         1) Создаем аккаунт, делаем оплачиваем Аренду
    ///         2) Делаем дату Аренды -30 дней с сегодняшнего дня
    ///         3) Отправляем уведомлениеЮ, проверяем, что в NotificationBuffer есть запись
    /// </summary>
    public class BeforeArchivDatabaseNotificationTest :ScenarioBase
    {
        private readonly IAccountDatabaseManager _accountDatabaseManager;
        private readonly AccountFilesToTombManager _accountFilesToTombManager;

        public BeforeArchivDatabaseNotificationTest()
        {
            _accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();
            _accountFilesToTombManager = TestContext.ServiceProvider.GetRequiredService<AccountFilesToTombManager>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            var service = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>().GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 100;
            CreateInflowPayment(account.Id, service.Id, paymentSum);

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-30)
            });

            var archiveDatabasesList = _accountDatabaseManager.GetArchiveDatabaseList();
            Assert.IsFalse(archiveDatabasesList.Error, archiveDatabasesList.Message);

            var archiveDatabaseModel = archiveDatabasesList.Result.First();
            Assert.IsNotNull(archiveDatabasesList);
            Assert.AreEqual(account.Id, archiveDatabaseModel.AccountId);
            Assert.AreEqual(createAccountDatabaseAndAccountCommand.AccountDatabaseId, archiveDatabaseModel.DataBaseId);

            var archivingResult = _accountFilesToTombManager.ArchiveAccountFilesToTomb(account.Id);
            Assert.IsFalse(archivingResult.Error);
        }
    }
}
