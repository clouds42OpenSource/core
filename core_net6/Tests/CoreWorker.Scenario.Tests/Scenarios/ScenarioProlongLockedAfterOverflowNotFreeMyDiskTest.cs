﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Проверка разблокировки платного диска после того
    /// как на нём освободили место
    /// Действия:
    /// 1) Создаём аккаунт, кладём на счёт деньги, докупаем пространство до 20Гб
    /// 2) Блокируем
    /// 3) Освобождаем место до 10Гб
    /// 4) Запускаем таску пролонгации
    /// 5) Проверяем, что дата сервис разблокирован,
    ///     дата окончания сервиса продлилась, деньги сняты
    /// </summary>
    public class ScenarioProlongLockedAfterOverflowNotFreeMyDiskTest: ScenarioBase
    {
        public override void Run()
        {
            #region step1
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            var accountId = createAccountCommand.AccountId;

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount, "Ошибка создания аккаунта");

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);
            var startBalance = 8000;
            var testDate = DateTime.Now.AddDays(-1);

            CreateInflowPayment(accountId, billingService.Id, startBalance);

            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = accountId, SizeGb = 20, ByPromisedPayment = false };
            resourcesManager.ChangeMyDiskTariff(changemydiskmodel);

            var diskCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == billingService.SystemService).Cost;

            #endregion step1

            ChangeResourceConfigurationExpireDate(testDate);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.Frozen, true));

            SetMyDiskFilesSizeInMbForAccount(createAccountCommand.AccountId, 10240);

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongLockedServices();

            #region checking
            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());
            RefreshDbCashContext(TestContext.DbLayer.BillingServiceTypeRepository.GetAll());

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            var rentResConf =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            Assert.IsNotNull(resourcesConfiguration);
            Assert.AreEqual(rentResConf.FrozenValue, false, "Должна быть разморозка.");
            Assert.AreEqual(resourcesConfiguration.Cost, diskCost, "Стоимость должна быть 475.");
            #endregion checking
        }
    }
}
