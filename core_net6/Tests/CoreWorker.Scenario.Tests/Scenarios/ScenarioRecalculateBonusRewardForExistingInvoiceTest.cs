﻿using System;
using System.Linq;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест пересчёта бонусов в уже существующем счёте
    /// Сценарий:
    /// 1) Создаём аккаунт с подключённой арендой
    /// 2) Кладём деньги на счёт чтобы купить тариф Диска
    /// 3) Докупаем дисковое пространство
    /// 4) Меняем дату окончания Аренды на +4 дней
    ///     от сегодняшней даты
    /// 5) Запускаем процесс оповещения о завершении срока лицензии
    ///  и в полученных счетах обнуляем поле BonusReward имитируя ситуацию, когда
    ///  счёт был создан до введения системы бонусов.
    /// 6) Ещё раз запускаем процесс оповещения.
    /// 7) Проверяем корректность полей BonusReward.
    /// </summary>
    public class ScenarioRecalculateBonusRewardForExistingInvoiceTest: ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAcc.Run();

            var accountId = createAcc.AccountId;

            DbLayer.ResourceRepository
                .AsQueryable()
                .Where(x => !x.Subject.HasValue)
                .ExecuteDelete();

            var billingServiceDataProvider = ServiceProvider.GetRequiredService<BillingServiceDataProvider>();

            var diskBillingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyDisk);
            var rentBillingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            var paymentSum = 2575;

            CreateInflowPayment(accountId, diskBillingService.Id, paymentSum);
            
            var myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var newDiskSize = 75;

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = accountId, SizeGb = newDiskSize, ByPromisedPayment = false };
            var result = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsTrue(result.Result,"Ошибка смены тарифа диска");

            var resConfExpireDate = DateTime.Now.AddDays(4);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId == rentBillingService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, false));
            

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var rentResConf = GetResourcesConfigurationOrThrowException(accountId, rentBillingService.Id);

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(b => b.Id == accountId);

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.NotifyBeforeLockServices();

            var invoiceProvider = ServiceProvider.GetRequiredService<IInvoiceProvider>();

            var rentInvoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(rentResConf, billingAccount);
            var trueRentBonuses = rentInvoice.BonusReward;

            Assert.IsTrue(trueRentBonuses > 0, "Сейчас бонус за аренду обязан быть больше нуля.");
            rentInvoice.BonusReward = 0;
            DbLayer.InvoiceRepository.Update(rentInvoice);
            DbLayer.Save();

            billingManager.NotifyBeforeLockServices();
            RefreshDbCashContext(DbLayer.InvoiceRepository.GetAll());

            var newRentInvoice = invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(rentResConf, billingAccount);

            Assert.AreEqual(rentInvoice.Id, newRentInvoice.Id, "Счёт аренды должен остаться прежним");
            Assert.AreEqual(newRentInvoice.BonusReward, trueRentBonuses, "Неверное число бонусов в счёте за Аренду");

        }
    }
}
