﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость больше чем старая стоимость и на счету достаточно денег.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем его стоимость
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис успешно разблокирован и продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsMoreAndMoneyIsEnough : ScenarioBase
    {
        public override void Run()
        {
            #region customservice

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });
            createCustomService.Run();
            
            #endregion customservice

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();

            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = false,
                BillingServiceTypeId = billingServiceTypes[1].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id, false, null, dataAccountUsers);

            var myEntUser =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>x.SystemServiceType == ResourceType.MyEntUserWeb);
            var _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
            var usrCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);
            var billingService = GetRent1CServiceOrThrowException();

            var resources = DbLayer.ResourceConfigurationRepository.Where(w => w.BillingServiceId != createCustomService.Id);
            foreach (var resource in resources)
            {
                resource.ExpireDate = DateTime.Now.AddDays(-1);
                resource.Frozen = true;
                DbLayer.ResourceConfigurationRepository.Update(resource);
            }

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            dataAccountUsers[0].Status = true;

            billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();

            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id, false, null, dataAccountUsers);

            var paymentTotalSum = usrCost.Cost + usrWebCost.Cost + 3 * billingServiceTypes[0].ServiceTypeCost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
        }
    }
}