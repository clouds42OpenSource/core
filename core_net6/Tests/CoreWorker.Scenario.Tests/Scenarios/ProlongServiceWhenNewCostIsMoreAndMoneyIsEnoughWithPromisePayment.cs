﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.Configuration;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.CloudServices;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость больше чем старая стоимость и на счету достаточно денег,
    ///     но есть обещанный платеж.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем его стоимость
    ///         4) Добавляем обещанный платеж
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис успешно разблокирован и продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsMoreAndMoneyIsEnoughWithPromisePayment : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        public ProlongServiceWhenNewCostIsMoreAndMoneyIsEnoughWithPromisePayment()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            #region customservice
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            #endregion customservice

            #region account

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();
            var inMemorySettings = new Dictionary<string, string> {
            {"EnableCache", "false"},
            };

            new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();
            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, // default - "dynamic" session token
                ClientDescription = "GetTokenByLogin", // client description
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]", // user agent
                ClientIPAddress = AccessProvider.GetUserHostAddress(), // user ip
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();
            #endregion account

            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            #region Payment
            RefreshDbCashContext(DbLayer.BillingServiceTypeRepository.GetAll());

            var myEntUserWeb =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);
            
            var billingService =
                new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            Context.ChangeTracker.Clear();

            var resConf1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, resConf1C.Cost);

            #endregion Payment

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            Context.ChangeTracker.Clear();

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var model = new CalculateBillingServiceTypeDto
            {
                Subject = res.Result,
                Status = true,
                BillingServiceTypeId = myEntUserWeb.Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = createAccountCommand.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(model);
            var model2 = new CalculateBillingServiceTypeDto
            {
                Subject = res.Result,
                Status = true,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = createAccountCommand.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(model2);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(createAccountCommand.AccountId, createCustomService.Id, false, null, dataAccountUsers);
           
            #region OP3000
            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto
                {
                    PaymentSum = resConf1C.Cost + 3 * billingServiceTypes[0].ServiceTypeCost
                },
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };
            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);

            if (!promisePayment.Result)
                throw new InvalidOperationException ("Ошибка предоставления обещанного платежа");
            #endregion OP3000

            Context.ChangeTracker.Clear();

            var resourceRent1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            var billingAccount =DbLayer.BillingAccountRepository
                .FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount.PromisePaymentSum, "Ошибка создания обещанного платежа");
            Assert.AreNotEqual(billingAccount.Balance, 2 * invoiceRequest.SuggestedPayment.PaymentSum,
                "Деньги не списались");
            Assert.AreEqual(billingAccount.Balance, 0, "неправильный расчёт денег");
        }
    }
}
