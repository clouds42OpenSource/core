﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки уведомления о скорой блокировке сервиса Аренды 1С
    /// и зависимых от Аренды 1С сервисов
    /// Создаем кастомный сервис который зависит от аренды, формируем счет. Счет должен быть на общую сумму
    /// и в номенклатуре должны быть услуги аренды
    /// </summary>
    public class ScenarioCheckLockServiceNotificationForMyEntServiceAndDependentServices : ScenarioBase
    {
        private readonly IResourceConfigurationDataProvider _resourceConfigurationDataProvider;

        public ScenarioCheckLockServiceNotificationForMyEntServiceAndDependentServices()
        {
            _resourceConfigurationDataProvider = TestContext.ServiceProvider.GetRequiredService<IResourceConfigurationDataProvider>();
        }

        public override void Run()
        {
            var standardServiceType = GetStandardServiceTypeOrThrowException();
            var defaultPeriodForInvoice = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);
            billingServiceTypes[0].DependServiceTypeId = standardServiceType.Id;

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            const int paymentSum = 100;

            CreateInflowPayment(createAccountCommand.AccountId, null, paymentSum);

            var resConfExpireDate = DateTime.Now.AddDays(4);
            var rent1CService = GetRent1CServiceOrThrowException();

            ChangeResourceConfigurationExpireDate(resConfExpireDate, rent1CService.Id);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            var resConfModel = _resourceConfigurationDataProvider.GetConfigWithDependencies(resourceRent1C);
            var totalCost = _resourceConfigurationDataProvider.GetTotalServiceCost(resConfModel) * defaultPeriodForInvoice;
            
            billingManager.NotifyBeforeLockServices();

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);

            var invoicesCount = DbLayer.InvoiceRepository.Count();

            Assert.AreEqual(invoicesCount, 1, "Должен был создаться только 1 счет");
            Assert.IsFalse(invoice.Period != defaultPeriodForInvoice || invoice.Sum != totalCost,
                "Не корректно работает создание нового счета");
            
            var invoiceProductsCount = DbLayer.InvoiceProductRepository.Count();
            
            Assert.AreEqual(1, invoiceProductsCount, "Должны был создаться только 1 продукт счета");

            var invoiceProductForMyEntUser = DbLayer.InvoiceProductRepository.FirstOrDefault(w =>
                w.InvoiceId == invoice.Id && w.ServiceName == standardServiceType.Name);

            Assert.IsFalse(invoiceProductForMyEntUser is not { Count: 1 },
                "Не корректно создаются продукты счета для ресурса СТАНДАРТ");
        }
    }
}
