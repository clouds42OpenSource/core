﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления заблокированного сервиса с зависимыми сервисами, при условии что есть денги на счету.
    /// 1) Создаем сервис Аренда 1С.
    /// 2) Создаем кастомный зависимый сервис.
    /// 3) Блокируем сервис Аренда 1С.
    /// 4) Закидываем денег на счет.
    /// 5) Запускаем обработку.
    /// 6) Убеждаемся что сервис Аренда 1С разблокирован и деньги списаны за сервис.
    /// 7) Убеждаемся что Frozen ExpireDate не затронулись у зависимого сервиса.
    /// </summary>
    public class ProlongLockedServiceWithDependenciesTest : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IBillingManager _billingManager;

        public ProlongLockedServiceWithDependenciesTest()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
        }

        public override void Run()
        {
            #region CreateCustomService

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createCustomService = new CreateBillingServiceCommand(TestContext, (serviceOwnerAccountId) => 
                new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                    {BillingServiceTypes = serviceTypeDtos});
            createCustomService.Run();

            #endregion CreateCustomService

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();
            
            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = true,
                BillingServiceTypeId = serviceTypeDtos[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = createAccountCommand.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(createAccountCommand.AccountId, createCustomService.Id, false, null, dataAccountUsers);

            #region PaymentSum

            var billingService = GetRent1CServiceOrThrowException();

            var resConf1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var totalPaymentCost = resConf1C.Cost + serviceTypeDtos[0].ServiceTypeCost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, totalPaymentCost);

            #endregion PaymentSum

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();

            _billingManager.ProlongLockedServices();

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);

            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.AreEqual(billingAccount.Balance, 0);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            
            var customResConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == createAccountCommand.AccountId && rc.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(customResConfig);
            Assert.AreEqual(customResConfig.Frozen, null);
        }
    }
}
