﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста для проверки создания счета с сервисами
    /// имеющими демо период
    ///
    /// 1) Создаем аккаунт владельца сервиса  #region CreateAccount
    ///
    /// 2) Создаем кастомный сервис #region CreateService
    ///
    /// 3) Создаем клиентский аккаунт с новым кастомным сервисом #region CreateClientAccountCommand
    ///
    /// 4) Проверяем создание счета с демо сервисом.
    /// Ожидаем что в счет он не попадет и не будет записи в продукте счета #region CheckInvoiceWithDemoService
    ///
    /// 5) Принимаем подписку кастомного демо сервиса для клиентского аккаунта #region ApplyDemoServiceSubscribe
    ///
    /// 6) Удаляем счет и продукт счета #region RemoveInvoice
    ///
    /// 7) Проверяем создание счета не имея демо сервиса. #region CheckInvoiceWithoutDemoService
    /// Ожидаем что кастомный сервис попадет в счет и не будет запись в продукте счета #region CheckInvoiceWithDemoService
    /// 
    /// </summary>
    public class ScenarioCheckCreatingInvoiceWithDemoServicesTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IInvoiceProvider _invoiceProvider;
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioCheckCreatingInvoiceWithDemoServicesTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _invoiceProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceProvider>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var defaultInvoicePeriod = CloudConfigurationProvider.Invoices.DefaultPeriodForInvoice();

            #region CreateAccount

            var createOwnerAccountCommand = new CreateAccountCommand(TestContext);
            createOwnerAccountCommand.Run();

            #endregion

            #region CreateService

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var createDbTemplateDelimiters = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimiters.Run();


            var billingServiceType = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Тестовый Сервис",
                AccountId = createOwnerAccountCommand.AccountId,
                BillingServiceTypes = billingServiceType
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);
            Assert.IsNotNull(service);

            #endregion

            #region CreateClientAccountCommand

            var createClientAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = "Test account2",
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                Email = "TestEmail2@efsol.ru",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = service.Id
                }
            });

            createClientAccountCommand.Run();

            var resConfRent1C = GetResourcesConfigurationOrThrowException(createClientAccountCommand.AccountId,
                Clouds42Service.MyEnterprise);

            var billingAccount = GetBillingAccount(createClientAccountCommand.AccountId);

            #endregion

            #region CheckInvoiceWithDemoService

            var invoice = _invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(resConfRent1C, billingAccount);
            Assert.IsNotNull(invoice);
            Assert.AreEqual(invoice.Sum, resConfRent1C.Cost * defaultInvoicePeriod);

            var invoiceProducts = GetInvoiceProducts(invoice.Id);

            Assert.AreEqual(invoiceProducts.Count, 1);
            Assert.AreEqual(invoiceProducts.First().ServiceSum, resConfRent1C.Cost * defaultInvoicePeriod);

            #endregion

            #region ApplyDemoServiceSubscribe

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, createClientAccountCommand.AccountId, false);

            #endregion

            #region RemoveInvoice

            RemoveInvoice(invoice);

            #endregion

            #region CheckInvoiceWithoutDemoService

            var totalSum =
                GetResourcesConfigurationOrThrowException(createClientAccountCommand.AccountId, service.Id).Cost +
                resConfRent1C.Cost;

            invoice = _invoiceProvider.GetOrCreateInvoiceByResourcesConfiguration(resConfRent1C, billingAccount);

            Assert.IsNotNull(invoice);
            Assert.AreEqual(invoice.Sum, totalSum * defaultInvoicePeriod);

            invoiceProducts = GetInvoiceProducts(invoice.Id);

            Assert.AreEqual(invoiceProducts.Count, 2);

            #endregion
        }

        /// <summary>
        /// Получить продукты счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <returns>Продукты счета</returns>
        private List<InvoiceProduct> GetInvoiceProducts(Guid invoiceId) =>
            DbLayer.InvoiceProductRepository.WhereLazy(i => i.InvoiceId == invoiceId).ToList();

        /// <summary>
        /// Удалить счет на опалту
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        private void RemoveInvoice(Invoice invoice)
        {
            DbLayer.InvoiceProductRepository.DeleteRange(invoice.InvoiceProducts);
            DbLayer.InvoiceRepository.DeleteInvoice(invoice);
            DbLayer.Save();
        }
    }
}