﻿using System;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста с проверкой пролонгации Аренды 1С при переполненном диске
    ///
    /// 1) Создаем аккаунт с Арендой 1С #CreateAccount
    ///
    /// 2) Создаем входящий платеж для стартового баланса #CreateInflowPayment
    ///
    /// 3) Меняем тариф диска на платный(20гб) #ChangeDiskTariff
    ///
    /// 4) Блокируем Аренду 1С #LockService
    ///
    /// 5) Заполняем дисковое пространство на 30гб #FillDisk
    ///
    /// 6) Запускаем пролонгацию Аренды 1С #ProlongLockedService
    ///
    /// 7) Проверяем что Аренда разблокировалась и стоимость диска не изменилась #CheckProlongService
    /// 
    /// </summary>
    public class ScenarioProlongOfRent1CInCaseOfFullDiskDriveTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount, "Ошибка создания аккаунта");

            var accountId = createAccountCommand.AccountId;

            #endregion

            #region CreateInflowPayment

            var billingService = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>().GetSystemService(Clouds42Service.MyDisk);
            var startBalance = 3500;

            CreateInflowPayment(accountId, billingService.Id, startBalance);

            #endregion

            #region ChangeDiskTariff

            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = accountId, SizeGb = 20, ByPromisedPayment = false };
            resourcesManager.ChangeMyDiskTariff(changemydiskmodel);

            var diskCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == billingService.SystemService).Cost;

            #endregion

            #region LockService

            var testDate = DateTime.Now.AddDays(-1);
            SetFrozenResourceConfigurationForAccount(createAccountCommand.AccountId, Clouds42Service.MyEnterprise, testDate);

            #endregion

            #region FillDisk

            SetMyDiskFilesSizeInMbForAccount(createAccountCommand.AccountId, 30720);

            #endregion

            #region ProlongLockedService

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongLockedServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());
            RefreshDbCashContext(TestContext.DbLayer.BillingServiceTypeRepository.GetAll());

            #endregion

            #region CheckProlongService

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            var rent1ResourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            Assert.IsNotNull(resourcesConfiguration);
            Assert.IsNotNull(rent1ResourcesConfiguration);
            Assert.AreEqual(rent1ResourcesConfiguration.FrozenValue, false, "Должна быть разморозка.");
            Assert.AreEqual(resourcesConfiguration.Cost, diskCost, $"Стоимость должна быть {diskCost}.");

            #endregion
        }
    }
}
