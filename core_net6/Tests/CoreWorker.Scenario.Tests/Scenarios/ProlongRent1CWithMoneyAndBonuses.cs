﻿using Clouds42.Billing.Billing.Internal;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Пролонгация сервиса за деньги и бонусы
    /// Сценарий:
    ///         1) Создаем аккаунт и его реферала
    ///         2) Создаем бонусный и денежный платежи рефералу
    ///         3) Устанавливаем дату окончания Аренды на прошедшую
    ///         4) Запускаем пролонгацию
    ///         5) Проверяем, что Аренда разблокирована и пользователь состоит в группах АД
    /// </summary>
    public class ProlongRent1CWithMoneyAndBonuses : ScenarioBase
    {
        private readonly IBillingManager _billingManager;
        private readonly ResourcesService _resourcesService;

        public ProlongRent1CWithMoneyAndBonuses()
        {
            _billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            _resourcesService = ServiceProvider.GetRequiredService<ResourcesService>();
        }

        public override void Run()
        {
            var createReferrerAccountCommand = new CreateAccountCommand(TestContext);
            createReferrerAccountCommand.Run();

            var createReferralAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = createReferrerAccountCommand.AccountId
                }
            });
            createReferralAccountCommand.Run();

            var service = ServiceProvider.GetRequiredService<BillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);

            var localeId = GetAccountLocaleId(createReferralAccountCommand.AccountId);

            var rent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == service.Id && r.AccountType == "Standart").Sum(r => r.Cost);

            var rent1CHalfCost = rent1CCost / 2;

            CreatePayment(createReferralAccountCommand.AccountId, rent1CHalfCost, TransactionType.Money, service.Id);
            CreatePayment(createReferralAccountCommand.AccountId, rent1CHalfCost, TransactionType.Bonus, service.Id);

            var defaultDaysCount = -1;

            ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createReferralAccountCommand.AccountId,
                new Rent1CServiceManagerDto
                {
                    ExpireDate = DateTime.Now.AddDays(defaultDaysCount)
                });
            _billingManager.ProlongLockedServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());
            RefreshDbCashContext(TestContext.DbLayer.BillingAccountRepository.GetAll());

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            var rent1CResourceConfiguration =
                _resourcesService.GetResourceConfig(createReferralAccountCommand.AccountId,
                    Clouds42Service.MyEnterprise);

            var referralBillingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createReferralAccountCommand.AccountId);
            Assert.IsTrue(rent1CResourceConfiguration.ExpireDateValue > DateTime.Now,
                "Дата окончания Аренды доджна быть больше чем текущая");
            Assert.AreEqual(0, referralBillingAccount.BonusBalance, "Бонусный баланс должен быть 0");
            Assert.AreEqual(0, referralBillingAccount.Balance, "Денежный баланс должен быть 0");
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <param name="serviceId"></param>
        private void CreatePayment(Guid accountId, decimal serviceTypeCost, TransactionType transactionType,
            Guid serviceId)
        {
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = serviceTypeCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = transactionType,
                BillingServiceId = serviceId
            });
        }
    }
}