﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки блокирования просроченного сервиса если нет денег.
    /// </summary>
    public class LockExpiredServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            for (var i = 0; i < 3; i++)
            {
                var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
                connectUserToRentCommand.Run();
            }

            var resConfExpireDate = DateTime.Now;

            ChangeResourceConfigurationExpireDate(resConfExpireDate);            
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();
            
            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());
            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, true);
        }
    }
}
