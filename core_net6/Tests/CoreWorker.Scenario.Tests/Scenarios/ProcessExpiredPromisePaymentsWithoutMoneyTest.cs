﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.MyDiskContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Создаем аккаунт с просроченным ОП и на счету 0.
    /// При обработке должны заблокироваться все платные сервисы и ОП должен остаться.
    /// </summary>
    public class ProcessExpiredPromisePaymentsWithoutMoneyTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteServiceTypeResourcesByAccountUser();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var rent1CService = GetRent1CServiceOrThrowException();

            var resConf =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);


            Assert.IsNotNull(resConf);

            var promisePaymentSum = resConf.Cost;

            var promisePaymentDate = DateTime.Now.AddDays(-8);

            await DbLayer.BillingAccountRepository
                .AsQueryable()
                .Where(x => x.Id == createAccountCommand.AccountId)
                .ExecuteUpdateAsync(x => x.SetProperty(y => y.PromisePaymentSum, promisePaymentSum)
                    .SetProperty(y => y.PromisePaymentDate, promisePaymentDate));

            DropDbContextCache();

            ServiceProvider.GetRequiredService<IBillingManager>().ProcessExpiredPromisePayments();
            DropDbContextCache();

            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            
            var rent1CInfo = rent1CManager.GetRent1CInfo(createAccountCommand.AccountId);
            var myDiskInfo = await Mediator.Send(new GetMyDiskDataQuery { AccountId = createAccountCommand.AccountId });

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, true);

            var billingAccount = await 
                TestContext.DbLayer.BillingAccountRepository.AsQueryableNoTracking().FirstOrDefaultAsync(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(billingAccount.PromisePaymentDate);
            Assert.AreEqual(billingAccount.Balance, 0.0000M);
            Assert.AreEqual(billingAccount.PromisePaymentSum, promisePaymentSum);
            Assert.IsTrue(rent1CInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsTrue(myDiskInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsTrue(rent1CInfo.Result.ServiceStatus.ServiceIsLocked);
            Assert.IsTrue(myDiskInfo.Result.ServiceStatus.ServiceIsLocked);
        }
    }
}
