﻿using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления заблокированного сервиса если есть денеги.
    /// 1) Создаем сервис Аренда 1С.
    /// 2) Блокируем сервис Аренда 1С.
    /// 3) Закидываем денег на счет.
    /// 4) Запускаем обработку.
    /// 5) Убеждаемся что сервис Аренда 1С разблокирован и деньги списаны за сервис.
    /// </summary>
    public class ProlongLockedServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            var _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
            var myEntUser = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
            var usrCost =  _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var totalPaymentSum = usrCost.Cost + usrWebCost.Cost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, totalPaymentSum);

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            Services.AddTransient<IProlongServicesProvider, Clouds42.Billing.Billing.Providers.BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongLockedServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.AreEqual(billingAccount.Balance, 0);
        }
    }
}
