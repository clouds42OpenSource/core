﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки продления просроченного сервиса если есть денеги.
    /// </summary>
    public class ProlongExpiredServiceTest : ScenarioBase
    {

        public ProlongExpiredServiceTest()
        {
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteServiceTypeResourcesByAccountUser();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var resConfExpireDate = DateTime.Now;

            ChangeResourceConfigurationExpireDate(resConfExpireDate);
            
            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            var _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
            var myEntUser = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
            var usrCost =  _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var paymentTotalCost = usrCost.Cost + usrWebCost.Cost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalCost);
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.DbLayer.ResourceConfigurationRepository.GetAll());

            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
        }
    }
}