﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.CloudServices;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест пересчёта бонусов в уже существующем счёте для випов
    /// Сценарий:
    /// 1) Создаём аккаунт с включённой арендой
    /// 2) Меняем дату окончания Аренды на +4 дней от сегодняшней даты
    /// 3) Запускаем процесс оповещения о завершении срока лицензии
    /// 4) Проверяем, что в счёте есть бонусы
    /// 5) Делаем аккаунт VIP
    /// 6) Ещё раз запускаем процесс оповещения о завершении срока лицензии
    /// 7) Проверяем, что теперь в счёте 0 бонусов.
    /// </summary>
    public class ScenarioRecalculateBonusRewardInExistingInvoiceForVipTest: ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAcc.Run();
            var accountId = createAcc.AccountId;
            var rentBillingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var paymentSum = 100;

            CreateInflowPayment(accountId, null, paymentSum);

            var resConfExpireDate = DateTime.Now.AddDays(4);

            ChangeResourceConfigurationExpireDate(resConfExpireDate, rentBillingService.Id);
            
            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.NotifyBeforeLockServices();

            var invoiceBonus = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == accountId).BonusReward;
            Assert.IsTrue(invoiceBonus > 0,"Должны были назначится бонусы за Аренду");

            TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
                .UpdateSignVipAccount(createAcc.AccountId, true);

            billingManager.NotifyBeforeLockServices();
            RefreshDbCashContext(DbLayer.InvoiceRepository.GetAll());
            invoiceBonus = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == accountId).BonusReward;
            Assert.IsTrue(invoiceBonus==0,"Бонус для випов должен быть 0");
        }
    }
}
