﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Тест проверки уведомления о скорой блокиоровки сервиса.
    /// </summary>
    public class BeforeLockServiceNotificationTest : ScenarioBase
    {
        public override void Run()
        {
            var cloudConfiguration = DbLayer.CloudConfigurationRepository.FirstOrDefault(w => w.Key == "Invoices.DefaultPeriodForInvoice");
            var period = Convert.ToInt32(cloudConfiguration.Value);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            DeleteAccountResources();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var resConfExpireDate = DateTime.Now.AddDays(5);

            ChangeResourceConfigurationExpireDate(resConfExpireDate);

            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.NotifyBeforeLockServices();

            var billingService = GetRent1CServiceOrThrowException();

            var resourceRent1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);

            Assert.IsNotNull(resourceRent1C);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(r => r.AccountId == createAccountCommand.AccountId);

            Assert.IsNotNull(invoice);
            Assert.AreEqual(invoice.Sum, resourceRent1C.Cost * period);
        }
    }
}