﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий теста проверки по периоду при уведомления о скорой блокиоровки кастомного сервиса
    /// Создаем кастомный сервис. Создаем счет по периуду 7 месяцев.
    /// Выполняем пролонгацию и новый счет должен создасться за такой же период
    /// </summary>
    public class ScenarioCheckPeriodAtLockCustomServiceNotificationTest : ScenarioBase
    {
        public override void Run()
        {
            var generator = new TestDataGenerator(Configuration, DbLayer);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes =
                    [
                        new()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Кастомная услуга",
                            Description = "Кастомная услуга",
                            BillingType = BillingTypeEnum.ForAccountUser,
                            ServiceTypeCost = 500,
                            DependServiceTypeId = null
                        }
                    ]
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });

            createAccountCommand.Run();

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId == createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, DateTime.Now.AddDays(4))
                    .SetProperty(y => y.Frozen, false)
                    .SetProperty(y => y.IsDemoPeriod, false));
            
            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            var resourcesConfiguration =
                DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingServiceId == createCustomService.Id);

            var totalCost = resourcesConfiguration.Cost * 7;

            var invoice = generator.GenerateInvoice(createAccountCommand.AccountId, createAccountCommand.Inn,
                totalCost, InvoiceStatus.Processed, 7);

            DbLayer.InvoiceRepository.AddInvoice(invoice);
            DbLayer.Save();

            billingManager.NotifyBeforeLockServices();

            var anotherInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.Id != invoice.Id && w.AccountId == createAccountCommand.AccountId && w.Sum == totalCost);

            if (anotherInvoice == null)
                throw new InvalidOperationException ("Ошибка, не создался новый счет");
        }
    }
}
