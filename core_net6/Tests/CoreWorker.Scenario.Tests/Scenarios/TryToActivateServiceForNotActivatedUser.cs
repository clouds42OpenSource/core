﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    /// Сценарий подключения кастомного сервиса для заблокированного пользователя
    /// Блокируем пользователя test_user и подключаем ему кастомную услугу.
    /// Проверка: должна вернуться ошибка с кодом: CalculateBillingServiceTypeErrorCode.AccountUserIsNotActivated и описанием:
    /// Пользователь {createAccountCommand.AccountId} заблокирован. Подключение к сервисам недоступно.Разблокировать пользователя вы сможете в меню Пользователи.
    /// </summary>
    public class TryToActivateServiceForNotActivatedUser : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public TryToActivateServiceForNotActivatedUser()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _alphaNumeric42CloudsGenerator = TestContext.ServiceProvider.GetRequiredService<AlphaNumeric42CloudsGenerator>();
        }

        public override void Run()
        {
            var random = new Random().Next(1, 99999999);
            var createOwnerCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = $"Test account{random}",
                Login = $"TestLogin{random}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = $"TestEmail{random}@efsol.ru",
                FirstName = $"TestFirstName{random}",
                Inn = "1234567890",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto()
            });
            createOwnerCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var serviceId = Guid.NewGuid();
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Key = serviceId,
                Name = "Тестовый Сервис",
                AccountId = createOwnerCommand.AccountId,
                BillingServiceTypes = billingServiceTypes,
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, service.Id, false);

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(w => w.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);

            var accountUserActivateProvider = ServiceProvider.GetRequiredService<AccountUserActivateProvider>();
            accountUserActivateProvider.SetActivateStatusForUser(accountUser, false);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = true,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            var billingServiceManager2 = ServiceProvider.GetRequiredService<BillingServiceManager>();

            var data = billingServiceManager2.CalculateBillingServiceType(createAccountCommand.AccountId,
                service.Id, user, dataAccountUsers);

            var errors = data.Result.SelectMany(w => w.Errors).Select(c => c.Code).ToList();

            Assert.IsNotNull(data);
            Assert.IsTrue(errors.Contains(CalculateBillingServiceTypeErrorCode.AccountUserIsNotActivated), "Не выдало ошибку активности юзера");
            Assert.IsTrue(errors.Contains(CalculateBillingServiceTypeErrorCode.Rent1CIsNotActive), "Не выдало ошибку наличия аренды");

        }

    }
}
