﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.CloudServices;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость меньше чем старая стоимость и на счету достаточно денег.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем стоимость подписки
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис успешно разблокирован и продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsLessAndMoneyIsEnough: ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        public ProlongServiceWhenNewCostIsLessAndMoneyIsEnough()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            #region customservice

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });
            createCustomService.Run();

            #endregion customservice
            
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();
            
            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            DbLayer.RefreshAll();
            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status =false,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = createAccountCommand.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(createAccountCommand.AccountId, createCustomService.Id, false, null, dataAccountUsers);

            #region PaymenSumm

            var billingService =
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());
            
            var rent1CService = GetRent1CServiceOrThrowException();

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);
            
            var paymentTotalSum = resourceRent1C.Cost + billingServiceTypes[0].ServiceTypeCost - 100;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);

            #endregion PaymenSumm

            RefreshDbCashContext(DbLayer.ResourceConfigurationRepository.GetAll());

            resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, rent1CService.Id);

            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, false);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.AreEqual(billingServiceTypes[0].ServiceTypeCost - 100, billingAccount.Balance);
        }
    }
}
