﻿using System;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace CoreWorker.Scenario.Tests.Scenarios.CancelYookassaPendingPaymentsTest
{
    /// <summary>
    /// Сценарий теста отмены зависших платежей агрегатора ЮKassa,
    /// когда нет подходящих платежей для выборки
    ///
    /// 1) Создаем аккаунт, которому будут принадлежать платежи #CreateAccount
    ///
    /// 2) Создаем платежи агрегатора ЮKassa #CreatePayments
    ///
    /// 3) Выполняем отмену зависших платежей #CancelPendingPayments
    ///
    /// 4) Проверяем что нет отмененных платежей #Check 
    /// 
    /// </summary>
    public class ScenarioCancelYookassaPendingPaymentsWhenNoSuitablePaymentsTest : ScenarioBase
    {
        private readonly CreateYookassaAggregatorPaymentHelperTest _createYookassaAggregatorPaymentHelperTest;

        public ScenarioCancelYookassaPendingPaymentsWhenNoSuitablePaymentsTest()
        {
            _createYookassaAggregatorPaymentHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateYookassaAggregatorPaymentHelperTest>();

        }

        public override void Run()
        {
            var allowedPendingPaymentsDaysCount =
                CloudConfigurationProvider.YookassaAggregatorPayment.GetAllowedPendingPaymentsDaysCount();

            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            #endregion

            #region CreatePayments

            _createYookassaAggregatorPaymentHelperTest.CreateYookassaPayment(accountId,
                DateTime.Now.AddDays(-allowedPendingPaymentsDaysCount).Date.AddSeconds(-1));

            _createYookassaAggregatorPaymentHelperTest.CreateYookassaPayment(accountId,
                DateTime.Now.AddDays(-allowedPendingPaymentsDaysCount).Date.AddSeconds(1),
                status: PaymentStatus.Done);

            _createYookassaAggregatorPaymentHelperTest.CreateYookassaPayment(accountId,
                DateTime.Now.AddDays(-allowedPendingPaymentsDaysCount).Date.AddDays(1));

            #endregion

            #region Check
            
            RefreshDbCashContext(TestContext.DbLayer.PaymentRepository.GetAll());
            

            #endregion
        }
    }
}
