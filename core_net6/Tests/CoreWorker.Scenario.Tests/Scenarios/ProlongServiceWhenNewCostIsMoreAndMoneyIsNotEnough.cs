﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreWorker.Scenario.Tests.Scenarios
{
    /// <summary>
    ///     Сценарий покупки заблокированого сервиса когда его стоимость больше чем старая стоимость и на счету не достаточно денег.
    ///     Действия:
    ///         1) Создаем кастомный сервис
    ///         2) Блокируем его
    ///         3) Меняем стоимость подписки
    ///         3) Закидываем деньги
    ///         4) Пытаемся продлить его
    ///         5) Проверяем что сервис не разблокирован и не продлен
    /// </summary>
    public class ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnough : ScenarioBase
    {
        private readonly IRateProvider _rateProvider;


        private readonly BillingServiceManager _billingServiceManager;
        public ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnough()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
        }

        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var myEntUser =DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);

            var usrCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var usrWebCost = _rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var billingService = GetRent1CServiceOrThrowException();

            var paymentTotalSum = usrCost.Cost + usrWebCost.Cost + 2 * billingServiceTypes[0].ServiceTypeCost;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentTotalSum);

            var resConfExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, true));

            var resources = DbLayer.ResourceConfigurationRepository.Where(w => w.BillingServiceId != createCustomService.Id);
            foreach (var resource in resources)
            {
                resource.ExpireDate = DateTime.Now.AddDays(-1);
                resource.Frozen = true;
                TestContext.DbLayer.ResourceConfigurationRepository.Update(resource);
            }

            UpdateResourceConfigurationMakeCustomServiceNotDemo(createCustomService.Id);
            
            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();

            var user = new CalculateBillingServiceTypeDto
            {
                Subject = createAccountCommand.AccountAdminId,
                Status = true,
                BillingServiceTypeId = billingServiceTypes.FirstOrDefault().Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                {
                    I = false,
                    Me = false,
                    Label = billingAccount.Account.AccountCaption
                }
            };
            dataAccountUsers.Add(user);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id,
                false, null, dataAccountUsers);
            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, billingService.Id);
            
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(resourceRent1C.FrozenValue, true);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, DateTime.Now.AddDays(-1).Date);
        }
    }
}
