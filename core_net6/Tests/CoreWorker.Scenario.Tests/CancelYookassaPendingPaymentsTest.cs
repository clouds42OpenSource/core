﻿using Clouds42.Test.Runner;
using CoreWorker.Scenario.Tests.Scenarios.CancelYookassaPendingPaymentsTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests
{
    /// <summary>
    /// Тест для проверки отмены зависших платежей агрегатора ЮKassa
    /// </summary>
    [TestClass]
    public class CancelYookassaPendingPaymentsTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Сценарий теста отмены зависших платежей агрегатора ЮKassa,
        /// когда нет подходящих платежей для выборки
        /// </summary>
        [TestMethod]
        public void ScenarioCancelYookassaPendingPaymentsWhenNoSuitablePaymentsTest()
        {
            _testRunner.Run<ScenarioCancelYookassaPendingPaymentsWhenNoSuitablePaymentsTest>();
        }

        /// <summary>
        /// Сценарий теста отмены зависших платежей агрегатора ЮKassa
        /// </summary>
        [TestMethod]
        public void ScenarioCancelYookassaPendingPaymentsTest()
        {
            _testRunner.Run<ScenarioCancelYookassaPendingPaymentsTest>();
        }
    }
}
