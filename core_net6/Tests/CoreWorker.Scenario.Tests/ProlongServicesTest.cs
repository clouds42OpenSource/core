﻿using System.Threading.Tasks;
using Clouds42.Test.Runner;
using CoreWorker.Scenario.Tests.Scenarios;
using CoreWorker.Scenario.Tests.Scenarios.Rent1CProlongingOrBlocking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoreWorker.Scenario.Tests
{
    [TestClass]
    public class ProlongServicesTest
    {
        private readonly TestRunner _testRunner = new();

        /// <summary>
        /// Тест проверки продления просроченного сервиса если есть денеги.
        /// </summary>
        [TestMethod]
        public void ProlongExpiredServiceTest()
        {
            _testRunner.Run<ProlongExpiredServiceTest>();
        }

        /// <summary>
        /// Тест проверки продления просроченного сервиса если есть деньги и есть свободные платные ресурсы.
        /// </summary>
        [TestMethod]
        public void ProlongExpiredServiceWithEmptyResourcesTest()
        {
            _testRunner.Run<ProlongExpiredServiceWithEmptyResourcesTest>();
        }

        /// <summary>
        /// Тест проверки блокирования просроченного сервиса если нет денег.
        /// </summary>
        [TestMethod]
        public void LockExpiredServiceTest()
        {
            _testRunner.Run<LockExpiredServiceTest>();
        }

        /// <summary>
        /// Обработка просроченных обещанных платежей если есть деньги
        /// </summary>
        [TestMethod]
        public async Task ProcessExpiredPromisePaymentsWithMoneyTest()
        {
            await _testRunner.RunAsync<ProcessExpiredPromisePaymentsWithMoneyTest>();
        }

        /// <summary>
        /// Тест проверки уведомления о скорой блокиоровки сервиса.
        /// </summary>
        [TestMethod]
        public void BeforeLockServiceNotificationTest()
        {
            _testRunner.Run<BeforeLockServiceNotificationTest>();
        }

        /// <summary>
        /// Тест пересчёта бонусов в уже существующем счёте
        /// </summary>
        [TestMethod]
        public void ScenarioRecalculateBonusRewardForExistingInvoiceTest()
        {
            _testRunner.Run<ScenarioRecalculateBonusRewardForExistingInvoiceTest>();
        }

        /// <summary>
        /// Тест пересчёта бонусов в уже существующем счёте для випов
        /// </summary>
        [TestMethod]
        public void ScenarioRecalculateBonusRewardInExistingInvoiceForVipTest()
        {
            _testRunner.Run<ScenarioRecalculateBonusRewardInExistingInvoiceForVipTest>();
        }


        /// <summary>
        /// Сценарий теста проверки по периоду при уведомления о скорой блокировки сервиса
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPeriodAtLockServiceNotificationTest()
        {
            _testRunner.Run<ScenarioCheckPeriodAtLockServiceNotificationTest>();
        }

        /// <summary>
        /// Сценарий теста проверки по периоду при уведомления о скорой блокировки сервиса
        /// и зависимых от него сервисов
        /// </summary>
        [TestMethod]
        public void ScenarioCheckPeriodAtLockServiceAndDependentServicesNotificationTest()
        {
            _testRunner.Run<ScenarioCheckPeriodAtLockServiceAndDependentServicesNotificationTest>();
        }

        /// <summary>
        /// Сценарий теста проверки уведомления о скорой блокировки сервиса Аренды 1С
        /// </summary>
        [TestMethod]
        public async Task ScenarioCheckLockServiceNotificationForMyEntServiceTest()
        {
            await _testRunner.RunAsync<ScenarioCheckLockServiceNotificationForMyEntServiceTest>();
        }

        /// <summary>
        /// Сценарий теста проверки уведомления о скорой блокировки сервиса Аренды 1С
        /// и зависимых сервисов от Аренды 1С
        /// </summary>
        [TestMethod]
        public void ScenarioCheckLockServiceNotificationForMyEntServiceAndDependentServices()
        {
            _testRunner.Run<ScenarioCheckLockServiceNotificationForMyEntServiceAndDependentServices>();
        }

        /// <summary>
        /// Сценарий теста проверки уведомлени¤ о скорой блокировки сервиса Мой диск
        /// </summary>
        [TestMethod]
        public async Task ScenarioCheckLockServiceNotificationForRent1CAndMyDiskServiceTest()
        {
            await _testRunner.RunAsync<ScenarioCheckLockServiceNotificationForRent1CAndMyDiskServiceTest>();
		}

        /// <summary>
        /// Сценарий теста проверки уведомлени о скорой блокировки сервиса
        /// когда есть недавние не оплаченные счета
        /// </summary>
        [TestMethod]
        public void ScenarioCheckLockServiceNotificationWhenThereIsAnotherUnpaidInvoiceTest()
        {
            _testRunner.Run<ScenarioCheckLockServiceNotificationWhenThereIsAnotherUnpaidInvoiceTest>();
        }

        /// <summary>
        /// Обработка просроченного обещанного платежа если нет денег
        /// </summary>
        [TestMethod]
        public async Task ProcessExpiredPromisePaymentsWithoutMoneyTest()
        {
            await _testRunner.RunAsync<ProcessExpiredPromisePaymentsWithoutMoneyTest>();
        }

        /// <summary>
        /// Проверка продления сервиса при отключенной аренде
        /// </summary>
        [TestMethod]
        public void ProlongSponsoredServicesWithoutRentAndEmptyResTest()
        {
            _testRunner.Run<ProlongSponsoredServicesWithoutRentTest>();
        }

        /// <summary>
        /// Проверка на отсутствие продления сервиса при отключенной аренде
        /// </summary>
        [TestMethod]
        public void TryToUnlockBlockedServiceWithZeroCostTest()
        {
            _testRunner.Run<TryToUnlockBlockedServiceWithZeroCostTest>();
        }

        /// <summary>
        /// Проверка на продления сервиса при активном сервисе
        /// </summary>
        [TestMethod]
        public void ProlongSponsoredServicesWithActiveRentTest()
        {
            _testRunner.Run<ProlongSponsoredServiceWithActiveRentTest>();
        }

        /// <summary>
        /// Блокировка сервиса при активной аренде
        /// </summary>
        [TestMethod]
        public void BlockActiveRentWithoutResourcesTest()
        {
            _testRunner.Run<BlockActiveRentWithoutResourcesTest>();
        }

        /// <summary>
        /// Проверка на продление при заблокированном сервисе
        /// </summary>
        [TestMethod]
        public void UnlockExpiredServiceWithMoneyTest()
        {
            _testRunner.Run<UnlockExpiredServiceWithMoneyTest>();
        }

        /// <summary>
        /// Проверка на блокировку сервиса, без подключенных пользователей 
        /// </summary>
        [TestMethod]
        public void BlockActiveRentInSponsoredAccountTest()
        {
            _testRunner.Run<BlockActiveRentInSponsoredAccountTest>();
        }

        /// <summary>
        /// Проверка на продление (один подключенный пользователь)
        /// </summary>
        [TestMethod]
        public void ProlongForPersonalFreeTariffTest()
        {
            _testRunner.Run<ProlongForPersonalFreeTariffTest>();
        }

        /// <summary>
        /// Проверка блокирования просроченного сервиса если нет денег
        /// </summary>
        [TestMethod]
        public void LockExpiredServiceWithDependenciesTest()
        {
            _testRunner.Run<LockExpiredServiceWithDependenciesTest>();
        }

        /// <summary>
        /// Проверка продления просроченного сервиса если есть денеги
        /// </summary>
        [TestMethod]
        public void ProlongExpiredServiceWithDependenciesTest()
        {
            _testRunner.Run<ProlongExpiredServiceWithDependenciesTest>();
        }

        /// <summary>
        /// Блокирование сервиса с отключенными пользователями и деньгами на счету
        /// </summary>
        [TestMethod]
        public void LockNotUsedServiceTest()
        {
            _testRunner.Run<LockNotUsedServiceTest>();
        }

        /// <summary>
        /// Проверка продления заблокированного сервиса если есть денеги
        /// </summary>
        [TestMethod]
        public void ProlongLockedServiceTest()
        {
            _testRunner.Run<ProlongLockedServiceTest>();
        }

        /// <summary>
        /// Тест проверки продления заблокированного сервиса с зависимыми сервисами,
        /// при условии что есть денги на счету.
        /// </summary>
        [TestMethod]
        public void ProlongLockedServiceWithDependenciesTest()
        {
            _testRunner.Run<ProlongLockedServiceWithDependenciesTest>();
        }

        /// <summary>
        /// Проверка продления заблокированного сервиса, который спонсируется других активным аккаунтом
        /// </summary>
        [TestMethod]
        public void UnlockSponsoredServicesTest()
        {
            _testRunner.Run<UnlockSponsoredServicesTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки что стоимость подписки больше
        /// чем старая стоимость и на счету достаточно денег
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsMoreAndMoneyIsEnough()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsMoreAndMoneyIsEnough>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки больше
        ///     чем старая стоимость, но на счету не достаточно денег 
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnough()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnough>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки меньше
        ///     чем старая стоимость и на счету достаточно денег
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsLessAndMoneyIsEnough()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsLessAndMoneyIsEnough>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки меньше
        ///     чем старая стоимость, но на счету не достаточно денег
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsLessAndMoneyIsNotEnough()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsLessAndMoneyIsNotEnough>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки больше
        ///     чем старая стоимость и на счету достаточно денег,
        ///     но есть обещанный платеж
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsMoreAndMoneyIsEnoughWithPromisePayment()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsMoreAndMoneyIsEnoughWithPromisePayment>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки больше
        ///     чем старая стоимость, но на счету не достаточно денег
        ///     и есть обещанный платеж
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnoughWithPromisePayment()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsMoreAndMoneyIsNotEnoughWithPromisePayment>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки меньше
        ///     чем старая стоимость и на счету достаточно денег,
        ///     но есть обещанный платеж
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsLessAndMoneyIsEnoughWithPromisePayment()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsLessAndMoneyIsEnoughWithPromisePayment>();
        }

        /// <summary>
        ///     Сценарий теста для проверки что стоимость подписки меньше
        ///     чем старая стоимость но на счету не достаточно денег 
        ///     и есть обещанный платеж
        /// </summary>
        [TestMethod]
        public void ProlongServiceWhenNewCostIsLessThenOldCostAndMoneyIsNotEnoughAndThereIsPromisePayment()
        {
            _testRunner.Run<ProlongServiceWhenNewCostIsLessAndMoneyIsNotEnoughWithPromisePayment>();
        }

        /// <summary>
        /// Проверка блокировки сервисов с просроченным демо периодом
        /// </summary>
        [TestMethod]
        public void LockExpiredDemoServicesTest()
        {
            _testRunner.Run<LockExpiredDemoServicesTest>();
        }

        /// <summary>
        /// Проверка что ресурс возвращается спонсору когда отключается спонсируемый аккаунт
        /// </summary>
        [TestMethod]
        public void ScenarioCheckResourceIsReturnWhenDisableSponsoredAccount()
        {
            _testRunner.Run<ScenarioCheckResourceIsReturnWhenDisableSponsoredAccount>();
        }

        /// <summary>
        /// Проверка подключения кастомного сервиса для заблокированного пользователя
        /// </summary>
        [TestMethod]
        public void TryToActivateServiceForNotActivatedUser()
        {
            _testRunner.Run<TryToActivateServiceForNotActivatedUser>();
        }

        /// <summary>
        /// Продление заблокированной платной услуги "Мой Диск" после освобождения места
        /// </summary>
        [TestMethod]
        public void ScenarioProlongLockedAfterOverflowNotFreeMyDiskTest()
        {
            _testRunner.Run<ScenarioProlongLockedAfterOverflowNotFreeMyDiskTest>();
        }

        /// <summary>
        /// Проверка невозможности блокировки Аренды у Сервисного аккаунта
        /// </summary>
        [TestMethod]
        public void TryToBlockServiceAccountRent()
        {
            _testRunner.Run<TryToBlockServiceAccountRent>();
        }
        
        /// <summary>
        /// Сценарий теста выборки сервисов, которые можно заблокировать
        /// через n дней
        /// </summary>
        [TestMethod]
        public void ScenarioGetSoonExpiredMainServicesTest()
        {
            _testRunner.Run<ScenarioGetSoonExpiredMainServicesTest>();
        }

        /// <summary>
        /// Пролонгация сервиса за деньги и бонусы
        /// </summary>
        [TestMethod]
        public void ScenarioProlongRent1CWithMoneyAndBonuses()
        {
            _testRunner.Run<ProlongRent1CWithMoneyAndBonuses>();
        }

        /// <summary>
        /// Тест проверки продления заблокированного сервиса с зависимыми сервисами,
        /// при условии что есть деньги на счету и у аккаунта есть реферальный аккаунт.
        /// Должно произойти начисление вознаграждения
        /// </summary>
        [TestMethod]
        public void ProlongLockedServiceWithDependenciesAndAccrualAgentRewardTest()
        {
            _testRunner.Run<ProlongLockedServiceWithDependenciesAndAccrualAgentRewardTest>();
        }

        /// <summary>
        /// Сценарий теста для проверки создания счета с сервисами
        /// имеющими демо период
        /// </summary>
        [TestMethod]
        public void ScenarioCheckCreatingInvoiceWithDemoServicesTest()
        {
            _testRunner.Run<ScenarioCheckCreatingInvoiceWithDemoServicesTest>();
        }

        /// <summary>
        /// Сценарий теста с проверкой пролонгации Аренды 1С при переполненном диске
        /// </summary>
        [TestMethod]
        public void ScenarioProlongOfRent1CInCaseOfFullDiskDriveTest()
        {
            _testRunner.Run<ScenarioProlongOfRent1CInCaseOfFullDiskDriveTest>();
        }
    }
}
