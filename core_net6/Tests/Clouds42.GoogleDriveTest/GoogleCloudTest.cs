﻿using System;
using System.IO;
using Clouds42.Configurations;
using Clouds42.GoogleCloud;
using Clouds42.Logger.Serilog;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.GoogleCloudTests
{
    [TestClass]
    public class GoogleCloudTest
    {
        public GoogleCloudTest()
        {
            var confBuilder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddGcpKeyValueSecrets()
                .Build();
            
            ConfigurationHelper.SetConfiguration(confBuilder);
        }

        [TestMethod]
        public void TestGoogleDriveProvider()
        {
            var provider = new GoogleCloudProvider(new SerilogLogger42());
            var filePath = CreateLocalFile();
            var fileId = provider.UploadFile(filePath, "tests", true, string.Empty);

            if (string.IsNullOrEmpty(fileId))
                throw new InvalidOperationException($"Ошибка 300520181903. Ожидалось что будет получен Id загруженного файла. Метод {nameof(provider.UploadFile)}");

            var objectById = provider.GetFileById(fileId);
            if (string.IsNullOrEmpty(fileId))
                throw new InvalidOperationException($"Ошибка 300520181904. Ожидалось что будет получен объект загруженного файла по Id '{fileId}'. " +
                                    $"Метод {nameof(provider.GetFileById)}");
            
            if (objectById.Size == 0)
                throw new InvalidOperationException($"Ошибка 3005201819041. Ожидалось что будет получен объект c ненулевым размером. Метод {nameof(provider.GetFileById)}");
            
            File.Delete(filePath);

            var downloadFile = provider.DownloadFile(fileId, filePath, true);
            if (downloadFile == false)
                throw new InvalidOperationException($"Ошибка 300520181909. Не сработала загрузка файла. Метод {nameof(provider.DownloadFile)}");

            var deleteFile = provider.DeleteFile(fileId);
            if (deleteFile == false)
                throw new InvalidOperationException($"Ошибка 300520181910. Не сработало удаление файла. Метод {nameof(provider.DeleteFile)}");
            
            File.Delete(filePath);
        }

        public static string BaseDirectory => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Test");

        public string CreateLocalFile()
        {
            var filePath = Path.Combine(BaseDirectory, "test.txt");

            if (!Directory.Exists(BaseDirectory))
                Directory.CreateDirectory(BaseDirectory);

            File.WriteAllText(filePath, "Test text");

            return filePath;
        }
    }
}
