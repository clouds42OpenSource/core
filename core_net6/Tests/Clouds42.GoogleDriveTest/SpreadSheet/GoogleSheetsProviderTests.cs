﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.GoogleCloud;
using Clouds42.GoogleCloud.SpreadSheet;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.GoogleCloud.SpreadSheet.Models;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using FluentAssertions;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Sheets.v4.Data;
using Moq;
using NUnit.Framework;

namespace Clouds42.GoogleCloudTests.SpreadSheet;

[TestFixture]
    public class GoogleSheetsProviderTests
    {
        private Mock<ILogger42> _loggerMock;
        private Mock<IGoogleDriveWithAccessProvider> _driveServiceMock;
        private Mock<ISheetsServiceWrapper> _sheetsServiceWrapperMock;
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private Mock<IHandlerException> _handlerMock;
        private GoogleSheetsProvider _googleSheetsProvider;
        
        private const string TestEmail = "test@example.com";
        private const string ExistingSpreadsheetId = "existing-spreadsheet-id";

        private static readonly GoogleSheetDto TestGoogleSheetDto = new()
        {
            AccountId = Guid.NewGuid(),
            PeriodFrom = DateTime.Now.AddDays(-1),
            PeriodTo = DateTime.Now.AddDays(11),
            SheetType = SheetType.PartnerClients
        };
            
        [SetUp]
        public void Setup()
        {
            _loggerMock = new Mock<ILogger42>();
            _driveServiceMock = new Mock<IGoogleDriveWithAccessProvider>();
            _sheetsServiceWrapperMock = new Mock<ISheetsServiceWrapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _handlerMock = new Mock<IHandlerException>();

            _googleSheetsProvider = new GoogleSheetsProvider(
                _loggerMock.Object,
                _driveServiceMock.Object,
                _unitOfWorkMock.Object,
                Mock.Of<IAccessProvider>(),
                _handlerMock.Object,
                _sheetsServiceWrapperMock.Object);
        }
        
        [Test]
        public async Task GetOrCreateGoogleSheetAsync_ShouldCreateNewSpreadsheet_WhenSpreadsheetNotExists()
        {
            // Arrange
            _unitOfWorkMock
                .Setup(uow => uow.GoogleSheetRepository
                    .FirstOrDefaultAsync(It.IsAny<Expression<Func<GoogleSheet, bool>>>()))
                .ReturnsAsync((GoogleSheet)null);
        
            _sheetsServiceWrapperMock.Setup(sw => sw.CreateSpreadsheetAsync(It.IsAny<Spreadsheet>()))
                .ReturnsAsync(new Spreadsheet { SpreadsheetId = "new-spreadsheet-id" });
        
            _driveServiceMock.Setup(ds => ds.GrantAccessToUser(It.IsAny<string>(), It.IsAny<PermissionData>()))
                .Returns(Task.FromResult<Permission?>(null));
        
            _unitOfWorkMock.Setup(uow => uow.GoogleSheetRepository.Insert(It.IsAny<GoogleSheet>()));
            _unitOfWorkMock.Setup(uow => uow.SaveAsync()).Returns(Task.CompletedTask);
        
            // Act
            var result = await _googleSheetsProvider.GetOrCreateGoogleSheetAsync(TestEmail, TestGoogleSheetDto);
        
            // Assert
            result.Error.Should().BeFalse();
            result.Result.Should().Be("new-spreadsheet-id");
            _loggerMock.Verify(logger => logger.Info(It.Is<string>(msg => msg.Contains("created with ID"))), Times.Once);
        }
        
        [Test]
        public async Task UpdateSpreadsheetAsync_ShouldClearAndWriteToSheet()
        {
            // Arrange
            var documentId = "test-document-id";
            var data = new List<object> { new { Column1 = "Value1", Column2 = "Value2" } };
            var columnNames = new List<string> { "Column1", "Column2" };

            _sheetsServiceWrapperMock.Setup(sw => sw.ClearValuesAsync(It.IsAny<ClearValuesRequest>(), documentId, It.IsAny<string>()))
                .ReturnsAsync(new ClearValuesResponse());

            _sheetsServiceWrapperMock.Setup(sw => sw.UpdateValuesAsync(It.IsAny<ValueRange>(), documentId, It.IsAny<string>()))
                .ReturnsAsync(new UpdateValuesResponse());

            _sheetsServiceWrapperMock.Setup(sw => sw.BatchUpdateAsync(It.IsAny<BatchUpdateSpreadsheetRequest>(), documentId))
                .ReturnsAsync(new BatchUpdateSpreadsheetResponse());

            // Act
            await _googleSheetsProvider.UpdateSpreadsheetAsync(data, documentId, columnNames);

            // Assert
            _sheetsServiceWrapperMock.Verify(sw => sw.ClearValuesAsync(It.IsAny<ClearValuesRequest>(), documentId, It.IsAny<string>()), Times.Once);
            _sheetsServiceWrapperMock.Verify(sw => sw.UpdateValuesAsync(It.IsAny<ValueRange>(), documentId, It.IsAny<string>()), Times.Once);
            _sheetsServiceWrapperMock.Verify(sw => sw.BatchUpdateAsync(It.IsAny<BatchUpdateSpreadsheetRequest>(), documentId), Times.Once);
        }
        
        
        [Test]
        public async Task GetOrCreateGoogleSheetAsync_ShouldReturnExistingSpreadsheet_WhenSpreadsheetExists()
        {
            // Arrange

            var existingSpreadsheet = new GoogleSheet
            {
                AccountId = TestGoogleSheetDto.AccountId,
                DocumentId = ExistingSpreadsheetId,
                PeriodFrom = TestGoogleSheetDto.PeriodFrom,
                PeriodTo = TestGoogleSheetDto.PeriodTo
            };

            _unitOfWorkMock
                .Setup(uow => uow.GoogleSheetRepository
                    .FirstOrDefaultAsync(It.IsAny<Expression<Func<GoogleSheet, bool>>>()))
                .ReturnsAsync(existingSpreadsheet);

            // Act
            var result = await _googleSheetsProvider.GetOrCreateGoogleSheetAsync(TestEmail, TestGoogleSheetDto);

            // Assert
            result.Error.Should().BeFalse();
            result.Result.Should().Be("existing-spreadsheet-id");
            _loggerMock.Verify(logger => logger.Info(It.Is<string>(msg => msg.Contains("found with ID"))), Times.Once);
        }
        
    }




