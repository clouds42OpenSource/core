﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Scenarios.ADTests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using NUnit.Framework.Legacy;

namespace Clouds42.Scenarios.ADTests.Libs.Scenarios
{
    /// <summary>
    /// Тест создания нового пользователя аккаунта и проверка проставления у него групп после покупки Аренды
    /// Сценарий:
    ///         1. Создаем аккаунт, добавляем еще одного пользователя
    ///         2. Проверяем, что пользователь создан в АД и состоит в необходимых группах
    ///         3. Докупаем ему Аренду, проеряем, что проставились необходимые группы
    /// </summary>
    public class CheckAdGroupsAndOfNewCreatedUserTest : ScenarioBase
    {
        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountUserDc = new AccountUserDto
            {
                Password = ",qwerty_Qwerty_1",
                AccountId = createAccountCommand.AccountId,
                Login = GetLogin(),
                Email = $"{GetLogin()}@posv.sqw",
                AccountCaption = createAccountCommand.Account.AccountCaption,
                Roles = [AccountUserGroup.AccountUser]
            };

            var newAccountUserId = AddUserToAccount(accountUserDc);

            var newUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == newAccountUserId);
            ClassicAssert.IsNotNull(newUser);

            var companyGroupName = DomainCoreGroups.CompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var clientsMkGroupName = DomainCoreGroups.ClientsMk;
            var remoteDesktopAccessGroup = DomainCoreGroups.RemoteDesktopAccess;

            newUser.CheckUserExistInAdGroup(companyGroupName);
            newUser.CheckUserExistInAdGroup(clientsMkGroupName);
            newUser.CheckUserExistInAdGroup(webGroupName, false);
            newUser.CheckUserExistInAdGroup(remoteDesktopAccessGroup, false);

            var billingService = GetRent1CServiceOrThrowException();
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var standardRent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == billingService.Id &&
                r.AccountType == "Standart").Sum(r => r.Cost);
            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, standardRent1CCost);

            TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>().ConfigureAccesses(
                createAccountCommand.AccountId, [
                    new() { StandartResource = true, AccountUserId = newAccountUserId }
                ]);

            newUser.CheckUserExistInAdGroup(webGroupName);
            newUser.CheckUserExistInAdGroup(remoteDesktopAccessGroup);
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
