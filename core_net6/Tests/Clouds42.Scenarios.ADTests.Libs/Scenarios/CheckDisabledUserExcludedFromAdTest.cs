﻿using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.ADTests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.ADTests.Libs.Scenarios
{
    /// <summary>
    /// Тест проверки исключения ползователя из групп в АД после отключения
    /// Сценарий:
    ///         1. Создаем аккаунт с активной Арендой
    ///         2. Проверяем, что пользователь состоит во всех необходимых группах
    ///         3. Отключаем пользователя, проверяем, что он исключен из групп:  RemoteDesktopAccess, company__web
    /// </summary>
    public class CheckDisabledUserExcludedFromAdTest : ScenarioBase
    {
        private readonly AccountUsersActivateManager _accountUsersActivateManager;

        public CheckDisabledUserExcludedFromAdTest()
        {
            _accountUsersActivateManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersActivateManager>();
        }
        
        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            var companyGroupName = DomainCoreGroups.CompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var clientsMkGroupName = DomainCoreGroups.ClientsMk;
            var remoteDesktopAccessGroup = DomainCoreGroups.RemoteDesktopAccess;

            var userExist = ActiveDirectoryUserFunctions.UsersExistsInDomain(accountUser.Login);
            Assert.IsTrue(userExist);

            accountUser.CheckUserExistInAdGroup(companyGroupName);
            accountUser.CheckUserExistInAdGroup(webGroupName);
            accountUser.CheckUserExistInAdGroup(clientsMkGroupName);
            accountUser.CheckUserExistInAdGroup(remoteDesktopAccessGroup);

            var managerResult = _accountUsersActivateManager.Activate(createAccountCommand.AccountAdminId, false);
            Assert.IsFalse(managerResult.Error);

            accountUser.CheckUserExistInAdGroup(webGroupName, false);
            accountUser.CheckUserExistInAdGroup(remoteDesktopAccessGroup, false);
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
