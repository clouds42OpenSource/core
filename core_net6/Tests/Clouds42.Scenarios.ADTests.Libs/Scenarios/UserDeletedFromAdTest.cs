﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.ADTests.Libs.Scenarios
{
    /// <summary>
    /// Тест удаления пользователя из АД
    /// Сценарий:
    ///         1. Создаем аккаунт, добавляем в него пользователя
    ///         2. Проверяем, что пользователь существует в АД
    ///         3. Удаляем пользователя, проверяем, что пользователь удален и из АД
    /// </summary>
    public class UserDeletedFromAdTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;

        public UserDeletedFromAdTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountUserDc = new AccountUserDto
            {
                Password = ",qwerty_Qwerty_08",
                AccountId = createAccountCommand.AccountId,
                Login = GetLogin(),
                Email = $"{GetLogin()}@posv.sqw",
                AccountCaption = createAccountCommand.Account.AccountCaption,
                Roles = [AccountUserGroup.AccountUser]
            };

            var newAccountUserId = AddUserToAccount(accountUserDc);

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.Id == newAccountUserId);
            Assert.IsNotNull(user);

            var userExist = ActiveDirectoryUserFunctions.UsersExistsInDomain(user.Login);
            Assert.IsTrue(userExist);

            _accountUsersProfileManager.Delete(newAccountUserId);

            user = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.Id == newAccountUserId);
            Assert.IsNotNull(user);
            Assert.IsNotNull(user.Removed);
            Assert.IsTrue(user.Removed.Value);

            userExist = ActiveDirectoryUserFunctions.UsersExistsInDomain(user.Login);
            Assert.IsFalse(userExist);
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
