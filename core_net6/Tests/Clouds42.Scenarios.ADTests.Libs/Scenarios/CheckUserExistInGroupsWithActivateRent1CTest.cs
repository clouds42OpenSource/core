﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.ADTests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.ADTests.Libs.Scenarios
{
    /// <summary>
    /// Тест регистрации с активацией Аренды 1С, проверяющий, что пользователь существует в АД и у него проставлены группы
    /// Сценарий:
    ///         1. Регистрируем новый аккаунт, с активацией Аренды 1С
    ///         2. Проверяем, что пользователь создан в АД
    ///         3. Проверяем, что он состоит в группах: Клиенты МК, company, company__web, RemoteDesktopAccess
    /// </summary>
    public class CheckUserExistInGroupsWithActivateRent1CTest : ScenarioBase
    {
        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.Id == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(user);

            var companyGroupName = DomainCoreGroups.CompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var clientsMkGroupName = DomainCoreGroups.ClientsMk;
            var remoteDesktopAccessGroup = DomainCoreGroups.RemoteDesktopAccess;

            var userExist = ActiveDirectoryUserFunctions.UsersExistsInDomain(user.Login);
            Assert.IsTrue(userExist);

            user.CheckUserExistInAdGroup(companyGroupName);
            user.CheckUserExistInAdGroup(clientsMkGroupName);
            user.CheckUserExistInAdGroup(webGroupName);
            user.CheckUserExistInAdGroup(remoteDesktopAccessGroup);
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
