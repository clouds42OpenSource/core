﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.ActiveDirectory.TaskProviders;
using TestSupportHelper;
using Clouds42.ActiveDirectory.Contracts.Functions;

namespace Clouds42.Scenarios.ADTests.Libs.Scenarios
{
    /// <summary>
    /// Тест джобы, редактирующей логин пользователя в АД
    /// Сценарий:
    ///         1. Создаем акканут и польщователя [CreteAccount]
    ///         2. Проверяем, что пользователь есть в АД [CheckUserExistInAd]
    ///         3. Редактируем логин пользователя в АД [EditUserLoginInAd]
    ///         4. Проверяем, что пользователь с новым логин существует в АД, а со старым - нет [CheckEditedUserExistInAd]
    /// </summary>
    public class ChangeUserLoginInAdJobTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreteAccount

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);

            var userOldLogin = accountUser.Login;

            #endregion

            #region CheckUserExistInAd

            AssertHelper.Execute(() => Assert.IsNotNull(accountUser, "Не удалось найти пользователя"));
            var userExists = ActiveDirectoryUserFunctions.UsersExistsInDomain(accountUser.Login);
            AssertHelper.Execute(() => Assert.IsTrue(userExists, "Пользователь не был создан в АД"));

            #endregion

            #region EditUserLoginInAd

            var newUserLogin = $"user_ad_test_{DateTime.Now:ssfff}";

            ActiveDirectoryUserFunctions.ChangeLogin(userOldLogin, newUserLogin, accountUser.Email);

            #endregion

            #region CheckEditedUserExistInAd

            userExists = ActiveDirectoryUserFunctions.UsersExistsInDomain(newUserLogin);
            AssertHelper.Execute(() => Assert.IsTrue(userExists, "Пользователь не был отредактирован в АД"));

            userExists = ActiveDirectoryUserFunctions.UsersExistsInDomain(userOldLogin);
            AssertHelper.Execute(() => Assert.IsFalse(userExists, "Пользователь не должен быть найден в АД"));

            #endregion
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}

