﻿using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Domain.DataModels;
using Xunit;

namespace Clouds42.Scenarios.ADTests.Libs.Helpers
{
    /// <summary>
    /// Хелпер проверки групп пользователя в АД
    /// </summary>
    public static class CheckUserInAdGroupsHelper
    {
        /// <summary>
        /// Проверить существование пользователя в группе АД
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <param name="groupName">Название группы</param>
        /// <param name="userShouldExist">Должен ли пользователь быть в группе (да/нет)</param>
        public static void CheckUserExistInAdGroup(this AccountUser accountUser, string groupName, bool userShouldExist = true)
        {
            var userExist = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(accountUser.Login, groupName);

            if (userShouldExist)
                Assert.True(userExist);
            else
                Assert.False(userExist);
        }
    }
}
