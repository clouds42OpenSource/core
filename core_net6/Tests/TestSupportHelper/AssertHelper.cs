﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AgileObjects.ReadableExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestSupportHelper
{
    /// <summary>
    /// Помощник класса Assert
    /// </summary>
    public static class AssertHelper
    {
        /// <summary>
        /// Выполнить выражение Assert(Более дитальное описание ошибки)
        /// </summary>
        /// <param name="expression">Выражение Assert</param>
        public static void Execute(Expression<Action> expression)
        {
            var act = expression.Compile();

            try
            {
                act();
            }
            catch (Exception ex)
            {
                throw new AssertFailedException(
                    expression.Body.ToReadableString() +
                    Environment.NewLine +
                    ex.Message);
            }
        }

        /// <summary>
        /// Сравнить 2 элемента
        /// </summary>
        /// <typeparam name="T">Тип элементов</typeparam>
        /// <param name="expected">Ожидаемое значение</param>
        /// <param name="actual">Действительное значение</param>
        public static void HasEqualFieldValues<T>(T expected, T actual)
        {
            var fields = typeof(T).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            var failures = (from field in fields
                            let v1 = field.GetValue(expected)
                            let v2 = field.GetValue(actual)
                            where v1 != null || v2 != null
                            where v1 != null && !v1.Equals(v2)
                            select $"{field.Name}: Expected:<{v1}> Actual:<{v2}>").ToList();

            if (failures.Any())
                Assert.Fail($"AssertHelper.HasEqualFieldValues failed. {Environment.NewLine} {string.Join(Environment.NewLine, failures)}");
        }

        /// <summary>
        /// Проверить условие на истину
        /// </summary>
        /// <param name="condition">условие</param>
        /// <param name="failAction">Действие в случае отрицательного результата</param>
        public static void IsTrue(bool condition, Action failAction)
        {
            try
            {
                Assert.IsTrue(condition);
            }
            catch (Exception)
            {
                failAction();
            }
        }
    }
}
