﻿using System;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.ActiveDirectory.Interface;


namespace Clouds42.Scenarios.Tests.Libs.TestManagers
{
    /// <summary>
    /// Фейковый успешный провайдер контроллера домена.
    /// </summary>
    public class ActiveDirectoryProviderFakeSuccess : IActiveDirectoryProvider
    {
        public void RegisterNewAccount(ICreateNewAccountModel model)
        {
            // заглушка
        }

        public void RegisterNewUser(ICreateNewUserModel userModel)
        {
            // заглушка
        }

        public void DeleteUser(Guid accountUserId, string username)
        {
            // заглушка
        }

        public virtual void DeleteUser(string username)
        {
            // заглушка
        }

        public void ChangeLogin(Guid accountUserId, string username, string newLogin, string email)
        {
            // заглушка
        }

        public void ChangePassword(Guid accountUserId, string username, string newPassword)
        {
            // заглушка
        }

        public virtual bool ExistUserAtGroup(string username, string groupName)
        {
            return true;
        }

        public void ChangeLogin(string username, string newLogin, string email)
        {
            // заглушка
        }

        public void ChangePassword(string username, string newPassword)
        {
            // заглушка
        }

        public void EditUser(Guid accountUserId, string userName, IPrincipalUserAdapterDto userModel)
        {
            // заглушка
        }

        public void EditUser(string userName, IPrincipalUserAdapterDto userModel)
        {
            // заглушка
        }

        public void EditUserGroupsAdList(EditUserGroupsAdListModel model)
        {
            // заглушка
        }

        public void AddToGroupIfNoExist(string username, string groupName)
        {
            // заглушка
        }

        public void RemoveFromGroup(string username, string groupName)
        {
            // заглушка
        }

        public void RemoveFromGroupIfExist(string username, string groupName)
        {
            // заглушка
        }

        public void SetDirectoryAcl(string groupName, string strPath)
        {
            // заглушка
        }

        public void SetDirectoryAclForUser(string userName, string strPath)
        {
            // заглушка
        }

        public void RemoveDirectoryAclForUser(string userName, string strPath)
        {
            // заглушка
        }

        public virtual bool UsersExistsInDomain(string login)
        {
            return true;
        }
    }

    public class ActiveDirectoryProviderUserNotExistFakeSuccess : ActiveDirectoryProviderFakeSuccess
    {
        public override bool UsersExistsInDomain(string login)
        {
            return false;
        }

        public override bool ExistUserAtGroup(string username, string groupName)
        {
            return false;
        }
    }

    public class ActiveDirectoryProviderUserNotDeleted : ActiveDirectoryProviderFakeSuccess
    {
        public override void DeleteUser(string username)
        {
            throw new InvalidOperationException("Имитация исключения");
        }
    }

}
