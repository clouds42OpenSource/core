﻿using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверки бонусного баланса
    /// в выборке списка аккаунтов для отчета
    /// 1) Создаем 4 аккаунта
    /// 2) Создаем для одного аккаунта биллинг аккаунт и определяем бонусный баланс
    /// 3) Делаем выборку
    /// </summary>
    public class ScenarioCheckBonusBalanceDataInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountReportDataProvider _accountReportDataProvider;
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioCheckBonusBalanceDataInSamplingAccountReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var accountsCount = 4;

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var accounts = _createAccountHelperTest.CreateByCount(accountsCount);
            var billingAccountOne = _testDataGenerator.GenerateBillingAccount(accounts[0].Id);

            billingAccountOne.BonusBalance = 200;
            DbLayer.BillingAccountRepository.Insert(billingAccountOne);
            DbLayer.Save();

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();
            
            Assert.AreEqual(managerResult.Count, accountsCount);

            var findAccount = managerResult.FirstOrDefault(r => r.AccountNumber == accounts[0].IndexNumber) ??
                              throw new NotFoundException("Не удалось найти аккаунт");

            Assert.AreEqual(billingAccountOne.BonusBalance, findAccount.BonusBalance);
        }
    }
}
