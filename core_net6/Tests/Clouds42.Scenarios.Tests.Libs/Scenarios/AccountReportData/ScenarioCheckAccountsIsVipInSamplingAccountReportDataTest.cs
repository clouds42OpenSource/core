﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверка соответствия статуса IsVip аккаунту пользователей в выборке списка аккаунтов для отчета
    ///     1) Создаем 50 пользователей
    ///     2) Делаем выборку
    ///     3) Проверяем соответсвуют ли поля IsVip у акаунтов и в аккаунтов выборки  
    /// </summary>
    public class ScenarioCheckAccountsIsVipInSamplingAccountReportDataTest :ScenarioBase
    {
        private readonly CreateAccountHelperTest _createAccountHelperTest;
        private readonly IAccountReportDataProvider _accountReportDataProvider;

        public ScenarioCheckAccountsIsVipInSamplingAccountReportDataTest()
        {
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
        }

        public override void Run()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            const int accountsCount = 50;
            _createAccountHelperTest.CreateByCount(accountsCount);

            var accounts = DbLayer.AccountsRepository.AsQueryableNoTracking().Include(account => account.AccountConfiguration).ToList();

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            if(accounts.Count != managerResult.Count)
                throw new InvalidOperationException("Выборка работает некорректно");
            
            managerResult.ForEach(w =>
            {
                var _ = accounts.FirstOrDefault(account => account.AccountConfiguration.IsVip == w.IsVip) ?? throw new InvalidOperationException("В выборке не найдено соответствий");
            });
        }
    }
}
