﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверка названий аккаунтов в выборке списка аккаунтов для отчета
    ///     1) Создаем аккаунт
    ///     2) Делаем выборку и проверяем соответствие названия аккаунта
    ///     3) Создаем еще 10 аккаунтов
    ///     4) Делаем выборку и проверяем соответствие названий аккаунтов
    /// </summary>
    public class ScenarioCheckAccountCaptionInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly CreateAccountHelperTest _createAccountHelperTest;
        private readonly IAccountReportDataProvider _accountReportDataProvider;
        public ScenarioCheckAccountCaptionInSamplingAccountReportDataTest()
        {
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            var account =
                managerResult.FirstOrDefault(w => w.AccountNumber == createAccountCommand.Account.IndexNumber) ?? throw new NullReferenceException();
            var accountName = account.CompanyName;

            if(accountName != createAccountCommand.Account.AccountCaption)
                throw new InvalidOperationException();

            _createAccountHelperTest.CreateByCount(10);
            managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            var accountsRepository = DbLayer.AccountsRepository.GetAll().ToList();
            
            if(managerResult.Count != accountsRepository.Count)
                throw new InvalidOperationException("Выборка работает некорректно");

            managerResult.ForEach(w =>
            {
                var _ = accountsRepository.FirstOrDefault(s => s.AccountCaption == w.CompanyName) ?? throw new InvalidOperationException("В выборке не найдено соответствий");
            });
        }
    }
}
