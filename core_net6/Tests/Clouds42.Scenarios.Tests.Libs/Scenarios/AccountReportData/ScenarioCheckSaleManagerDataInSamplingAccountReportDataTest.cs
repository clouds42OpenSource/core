﻿using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверки данных сейл менеджера
    /// в выборке списка аккаунтов для отчета
    /// 1) Создаем 5 аккаунтов
    /// 2) Пользователя из 2 аккаунтов будут сейл менеджерами пользователям двух других аккаунтов
    /// У пользователя другого аккаунта сейл менеджера не будет
    /// 3) Делаем выборку
    /// </summary>
    public class ScenarioCheckSaleManagerDataInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly CreateAccountHelperTest _createAccountHelperTest;
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountReportDataProvider _accountReportDataProvider;

        public ScenarioCheckSaleManagerDataInSamplingAccountReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            const int accountsCount = 5;

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var accounts = _createAccountHelperTest.CreateByCount(accountsCount);
            foreach (var accountUser in accounts.Select(account => _testDataGenerator.GenerateAccountUser(account.Id)))
            {
                DbLayer.AccountUsersRepository.Insert(accountUser);
            }

            var firstSaleManager = _testDataGenerator.GenerateAccountSaleManager(accounts[2].Id,
                accounts[0].AccountUsers.First().Id, "Отдел ИТЭР");

            var secondSaleManager = _testDataGenerator.GenerateAccountSaleManager(accounts[3].Id,
                accounts[1].AccountUsers.First().Id, "Отдел ОВР");

            DbLayer.AccountSaleManagerRepository.Insert(firstSaleManager);
            DbLayer.AccountSaleManagerRepository.Insert(secondSaleManager);
            DbLayer.Save();

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();
            var accountWithoutSaleManager = managerResult.Where(a =>
                string.IsNullOrEmpty(a.SaleManagerDivision) && string.IsNullOrEmpty(a.SaleManagerFullName) &&
                a.AccountNumber == accounts[4].IndexNumber);

            if (accountWithoutSaleManager.Count() != 1)
                throw new NotFoundException("Не удалось найти аккаунт за которым не закреплен сейл менеджер");

            var accountWithSaleManagers = managerResult.Where(a =>
                !string.IsNullOrEmpty(a.SaleManagerDivision) && !string.IsNullOrEmpty(a.SaleManagerFullName) &&
                (a.AccountNumber == accounts[2].IndexNumber || a.AccountNumber == accounts[3].IndexNumber));

            if (accountWithSaleManagers.Count() != 2)
                throw new NotFoundException("Не удалось найти аккаунты за которыми закреплены сейл менеджеры");
        }
    }
}
