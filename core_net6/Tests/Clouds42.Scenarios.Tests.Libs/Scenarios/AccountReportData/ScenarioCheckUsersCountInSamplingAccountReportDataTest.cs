﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверка количества пользователей в выборке списка аккаунтов для отчета
    ///     1) Создаем 10 аккаунтов
    ///     2) Каждому аккаунту добавляем по 10 пользователей
    ///     3) Делаем выборку
    ///     4) Проверяем количество пользователей у каждого аккаунта
    /// </summary>
    public class ScenarioCheckUsersCountInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountReportDataProvider _accountReportDataProvider;

        public ScenarioCheckUsersCountInSamplingAccountReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
        }

        public override void Run()
        {
            const int accountsCount = 10;
            const int accountsUsersCount = 10;

            var accounts = _testDataGenerator.GenerateListAccountsByCount(accountsCount);
            accounts.SaveAccounts(DbLayer);

            foreach (var accountUsers in accounts.Select(account => _testDataGenerator.GenerateListAccountUsersByCount(accountsUsersCount, account.Id)))
            {
                accountUsers.SaveAccountUsers(DbLayer);
            }
            
            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

         
            foreach (var reportDataDc in managerResult.Where(reportDataDc => reportDataDc.AccountUserCount != accountsUsersCount))
            {
                throw new InvalidOperationException(
                    $"Неверное количество пользователей у аккаунта {reportDataDc.CompanyName}, выборка работает некорректно");
            }
        }
    }
}
