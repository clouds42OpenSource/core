﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверки количества ВИП аккаунтов в выборке списка аккаунтов для отчета
    ///     1) Создаем 100 аккаунтов
    ///     2) Делаем выборку
    ///     3) Получаем количество всех ВИП аккаунтов
    ///     4) Сравниваем это количество с количеством ВИП аккаунтов в выборке
    /// </summary>
    public class ScenarioCheckAccountsIsVipCountInSamplingAccountReportDataTest : ScenarioBase
    {
       
        private readonly CreateAccountHelperTest _createAccountHelperTest;
        private readonly IAccountReportDataProvider _accountReportDataProvider;
        public ScenarioCheckAccountsIsVipCountInSamplingAccountReportDataTest()
        {
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            const int accountsCount = 100;

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();
            _createAccountHelperTest.CreateByCount(accountsCount);

            var accounts = DbLayer.AccountsRepository.AsQueryableNoTracking().Include(account => account.AccountConfiguration).ToList();
            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            if (accounts.Count != managerResult.Count)
                throw new InvalidOperationException("Выборка работает некорректно");

            var isVipAccountsCount = accounts.Count(account => account.AccountConfiguration.IsVip);
            var managerResultIsVipAccounts = managerResult.Count(reportDataDc => reportDataDc.IsVip);

            if (isVipAccountsCount != managerResultIsVipAccounts)
                throw new InvalidOperationException("Выборка работает некорректно");
        }
    }
}
