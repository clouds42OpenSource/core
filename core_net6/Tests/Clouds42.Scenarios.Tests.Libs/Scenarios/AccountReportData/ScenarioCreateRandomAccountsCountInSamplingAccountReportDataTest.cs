﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверка количества аккаунтов в выборке списка аккаунтов для отчета
    ///     1) Создаем случайное число аккаунтов 0-1000
    ///     2) Делаем выборку, проверяем количество созданнных аккаунтов и кличество аккаунтов в выборке
    /// </summary>
    public class ScenarioCreateRandomAccountsCountInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly IAccountReportDataProvider _accountReportDataProvider;
        public ScenarioCreateRandomAccountsCountInSamplingAccountReportDataTest()
        {
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
        }

        public override void Run()
        {
            var random = new Random();

            var createSegmentCommand = new CreateSegmentCommand(TestContext, true);
            createSegmentCommand.Run();

            ServiceProvider.GetRequiredService<CreateAccountHelperTest>().CreateByCount(random.Next(0, 1000));

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            var accountsRepository = DbLayer.AccountsRepository.GetAll().ToList();
            
            if (accountsRepository.Count != managerResult.Count)
                throw new InvalidOperationException("Некорректно работает выборка");
        }
    }
}
