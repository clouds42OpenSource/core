﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста проверки количества количества баз у аккаунта в выборке списка аккаунтов для отчета
    ///     1) Создаем аккаунт
    ///     2) Создаем базы
    ///     3) Делаем выборку и сравниваем количесво баз у аккаунта и в выборке
    /// </summary>
    public class ScenarioCheckAccountDatabasesInSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountReportDataProvider _accountReportDataProvider;

        public ScenarioCheckAccountDatabasesInSamplingAccountReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
        }
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var databases = _testDataGenerator.GenerateListAccountDatabasesByCount(5, createAccountCommand.AccountId, 20, 1234);
            databases.SaveAccountDatabases(DbLayer);
           
            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();
            
            if(managerResult.Count != 1)
                throw new InvalidOperationException("Выборка работает некорректно");

            var account = managerResult.FirstOrDefault();

            if (account != null && account.AccountDatabaseCount != createAccountCommand.Account.AccountDatabases.Count)
                throw new InvalidOperationException("Выборка работает некорректно");
        }
    }
}
