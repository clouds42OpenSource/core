﻿using System;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountReportData
{
    /// <summary>
    /// Сценарий теста выборки списка аккаунтов для отчета
    /// </summary>
    public class ScenarioSamplingAccountReportDataTest : ScenarioBase
    {
        private readonly IAccountReportDataProvider _accountReportDataProvider;
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioSamplingAccountReportDataTest()
        {
            _accountReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountReportDataProvider>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            _createAccountHelperTest.CreateByCount(10);

            var managerResult = _accountReportDataProvider.GetAccountReportDataDcs();

            if (managerResult.Count != 11)
                throw new InvalidOperationException("Не корректно работает выборка");
        }
    }
}
