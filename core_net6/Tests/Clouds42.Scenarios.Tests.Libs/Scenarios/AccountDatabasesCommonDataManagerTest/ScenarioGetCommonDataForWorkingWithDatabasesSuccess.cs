﻿using System;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabasesCommonDataManagerTest
{
    /// <summary>
    /// Сценарий для проверки успешного получения общих данных при создании инф. базы
    /// Действия:
    /// 1. Создадим аккаунт с настройкой регистрации системного сервиса Аренда 1С. [AccountCreation]
    /// 2. Проверим, что в общих данных будут корректно получены разрешения и информация о сервисе для пользователя Админа. [CheckCommonDataForAdminUser]
    /// 3. Проверим, что в общих данных будут корректно получены разрешения для обычного пользователя. [CheckCommonDataForCommonUser]
    /// 4. Проверим, что в общих данных будут корректно получены разрешения для пользователя Hotline. [CheckCommonDataForHotlineUser]
    /// 5. Проверим, что ошибка не будет выброшена при передачи невалидных Id. [CheckErrorWontThrowForNotValidIds]
    /// </summary>
    public class ScenarioGetCommonDataForWorkingWithDatabasesSuccess : ScenarioBase
    {
        private readonly AccountDatabasesCommonDataManager _accountDatabasesCommonDataManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;

        public ScenarioGetCommonDataForWorkingWithDatabasesSuccess()
        {
            _accountDatabasesCommonDataManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabasesCommonDataManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
        }

        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region CheckCommonDataForAdminUser

            var result =
                _accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(createAccountCommand.AccountId,
                    createAccountCommand.AccountAdminId);

            Assert.IsFalse(result.Error, result.Message);
            CheckCorrectnessOfPermissionsForAdmin(result.Result.PermissionsForWorkingWithDatabase);
            CheckCorrectnessOfMainStatusInfo(result.Result.MainServiceStatusInfo);

            #endregion

            #region CheckCommonDataForCommonUser

            var newUser = _accountUserTestHelper.GetOrCreateUserWithAccountUserRole(createAccountCommand.AccountId);

            result =
                _accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(createAccountCommand.AccountId,
                    newUser.Id);

            CheckCorrectnessOfPermissionsForCommonUser(result.Result.PermissionsForWorkingWithDatabase);

            #endregion

            #region CheckCommonDataForHotlineUser

            _accountUserTestHelper.ChangeUserRoles(newUser, AccountUserGroup.Hotline);

            result =
                _accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(createAccountCommand.AccountId,
                    newUser.Id);

            CheckCorrectnessOfPermissionsForHotlineUser(result.Result.PermissionsForWorkingWithDatabase);

            #endregion

            #region CheckErrorWontThrowForNotValidIds

            result =
                _accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(Guid.NewGuid(), 
                    newUser.Id);

            Assert.IsTrue(result.Error,
                "При неверном ID аккаунта должна вернуться ошибка, т.к не возможно работать с инф. базами не зная конфигурации аккаунта");

            result =
                _accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(createAccountCommand.AccountId,
                    Guid.NewGuid());

            Assert.IsFalse(result.Error, result.Message);

            #endregion
        }

        /// <summary>
        /// Проверить корректность получения разрешений для пользователя с ролью Админ
        /// </summary>
        /// <param name="permissions">Разрешения</param>
        private void CheckCorrectnessOfPermissionsForAdmin(PermissionsForWorkingWithDatabaseDto permissions)
        {
            Assert.IsTrue(permissions.HasPermissionForDisplayAutoUpdateButton, "Админу не должно обыть разрешено видеть кнопку АО");
            Assert.IsTrue(permissions.HasPermissionForRdp, "Админу должен иметь разрешение на РДП подключение");
            Assert.IsTrue(permissions.HasPermissionForWeb, "Админу должен иметь разрешение на WEB подключение");
        }

        /// <summary>
        /// Проверить корректность получения разрешений для обычного пользователя
        /// </summary>
        /// <param name="permissions">Разрешения</param>
        private void CheckCorrectnessOfPermissionsForCommonUser(PermissionsForWorkingWithDatabaseDto permissions)
        {
            Assert.IsFalse(permissions.HasPermissionForDisplayAutoUpdateButton, "Обычному пользователю не должно обыть разрешено видеть кнопку АО");
            Assert.IsFalse(permissions.HasPermissionForRdp, "Обычный пользователь не должен иметь разрешение на РДП подключение");
            Assert.IsFalse(permissions.HasPermissionForWeb, "Обычный пользователь не должен иметь разрешение на WEB подключение");
        }

        /// <summary>
        /// Проверить корректность получения разрешений для пользователя Hotline
        /// </summary>
        /// <param name="permissions">Разрешения</param>
        private void CheckCorrectnessOfPermissionsForHotlineUser(PermissionsForWorkingWithDatabaseDto permissions)
        {
            Assert.IsTrue(permissions.HasPermissionForDisplayAutoUpdateButton, "Пользователю Hotline должно обыть разрешено видеть кнопку АО");
            Assert.IsTrue(permissions.HasPermissionForRdp, "Пользователю Hotline должен иметь разрешение на РДП подключение");
            Assert.IsTrue(permissions.HasPermissionForWeb, "Пользователю Hotline должен иметь разрешение на WEB подключение");
        }

        /// <summary>
        /// Проверить корректность получения информации о сервисе
        /// </summary>
        /// <param name="model">Модель информации о сервисе</param>
        private void CheckCorrectnessOfMainStatusInfo(ServiceStatusModelDto model)
        {
            Assert.IsFalse(model.PromisedPaymentIsActive, "Активирован обещанный платеж, хотя активации не происходило");
            Assert.IsFalse(model.ServiceIsLocked, model.ServiceLockReason);
        }
    }
}
