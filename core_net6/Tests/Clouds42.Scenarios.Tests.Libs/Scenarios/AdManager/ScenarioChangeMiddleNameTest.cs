﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение отчества у пользователя в Active Directory
    public class ScenarioChangeMiddleNameTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            var user = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            ActiveDirectoryUserFunctions.ChangeMiddleName(_testaccount.Login, "middleName");
            var userChange = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            if (user.MiddleName == userChange.MiddleName)
                throw new InvalidOperationException ("Ошибка изменения отчества у пользователя домена");
        }

        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
