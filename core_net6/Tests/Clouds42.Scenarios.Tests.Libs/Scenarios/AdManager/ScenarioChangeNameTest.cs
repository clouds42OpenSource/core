﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение имени у пользователя в Active Directory
    public class ScenarioChangeNameTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            var user = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            ActiveDirectoryUserFunctions.ChangeName(_testaccount.Login, "firstName");
            var userChange = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            if (user.GivenName == userChange.GivenName)
                throw new InvalidOperationException ("Ошибка изменения имени у пользователя домена");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
