﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Удаление тестового пользователя в Active Directory
    public class ScenarioDeleteUserinAdTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            // удаление пользователя
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);

            // проверка есть ли такой юзер в Active Directory
            var user = ActiveDirectoryUserFunctions.UsersExistsInDomain(_testaccount.Login);
            if (user)
                throw new InvalidOperationException ("Тестовый аккаунт в AD найден");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
