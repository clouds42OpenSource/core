﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение фамилии у пользователя в Active Directory
    public class ScenarioChangeLastNameNameTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            var user = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            ActiveDirectoryUserFunctions.ChangeLastName(_testaccount.Login, "Lastname");
            var userChange = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            if (user.Surname == userChange.Surname)
                throw new InvalidOperationException ("Ошибка изменения фамилии у пользователя домена");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
