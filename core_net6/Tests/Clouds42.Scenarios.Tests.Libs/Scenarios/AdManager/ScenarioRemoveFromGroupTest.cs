﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Удаление пользователя из группы в Active Directory
    public class ScenarioRemoveFromGroupTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();
        private readonly string _groupName = "_testGroup";

        public override void Run()
        {
            var createAccountandGroupInActiveDirectoryCommand = new CreateAccountandGroupInActiveDirectoryCommand(accountData: _testaccount);
            createAccountandGroupInActiveDirectoryCommand.Run();

            ActiveDirectoryCompanyFunctions.AddToGroups(_testaccount.Login, _groupName);
            
            var existUserAtGroup = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(_testaccount.Login, _groupName);
            if (!existUserAtGroup)
                throw new InvalidOperationException ("Ошибка добавление пользователя в группу");

            ActiveDirectoryCompanyFunctions.RemoveFromGroups(_testaccount.Login, _groupName);
            
            existUserAtGroup = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(_testaccount.Login, _groupName);
            if (existUserAtGroup)
                throw new InvalidOperationException ("Ошибка удаления пользователя из группы");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryCompanyFunctions.DeleteGroups(_groupName);
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
