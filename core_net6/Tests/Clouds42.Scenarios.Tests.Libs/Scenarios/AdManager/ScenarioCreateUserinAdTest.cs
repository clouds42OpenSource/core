﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Добавление тестового пользователя в Active Directory
    public class ScenarioCreateUserinAdTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            //добавление пользователя в Active Directory
            ActiveDirectoryUserFunctions.CreateUser(_testaccount);
                
            // проверка есть ли такой юзер в Active Directory
            var user = ActiveDirectoryUserFunctions.UsersExistsInDomain(_testaccount.Login);
            if(!user)
                throw new InvalidOperationException ("Тестовый аккаунт в AD не найден");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
