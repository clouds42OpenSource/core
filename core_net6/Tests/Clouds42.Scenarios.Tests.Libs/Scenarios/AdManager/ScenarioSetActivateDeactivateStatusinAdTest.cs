﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Активация/деактивация для пользователя в Active Directory
    public class ScenarioSetActivateDeactivateStatusinAdTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();
        public override void Run()
        {
                var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
                createAccountInActiveDirectoryCommand.Run();
                ActiveDirectoryUserFunctions.SetActivateStatus(_testaccount.Login, true);
                

                var activate = ActiveDirectoryUserFunctions.UsersActivateInDomain(_testaccount.Login);
                if (!activate)
                    throw new InvalidOperationException ("Пользователь не активирован");

                ActiveDirectoryUserFunctions.SetActivateStatus(_testaccount.Login, false);

                activate = ActiveDirectoryUserFunctions.UsersActivateInDomain(_testaccount.Login);
                if (activate)
                    throw new InvalidOperationException ("Пользователь активирован");

        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
