﻿using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Проверка валидности пользователя в Active Directory
    public class ScenarioIsValidUserinAdTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();
        public override void Run()
        {
                var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
                createAccountInActiveDirectoryCommand.Run();
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
