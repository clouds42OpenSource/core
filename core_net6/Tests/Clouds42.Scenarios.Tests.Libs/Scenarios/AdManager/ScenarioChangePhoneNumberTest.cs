﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение номера телефона у пользователя в Active Directory
    public class ScenarioChangePhoneNumberTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            var user = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            ActiveDirectoryUserFunctions.ChangePhoneNumber(_testaccount.Login, $"{PhoneNumberGeneratorHelperTest.Generate()}");
            var userChange = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            if (user.VoiceTelephoneNumber == userChange.VoiceTelephoneNumber)
                throw new InvalidOperationException ("Ошибка изменения номера телефона у пользователя домена");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
