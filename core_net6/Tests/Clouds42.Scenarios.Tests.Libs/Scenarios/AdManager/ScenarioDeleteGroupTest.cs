﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Удаление группы в Active Directory
    public class ScenarioDeleteGroupTest : ScenarioBase
    {
        private readonly string _groupName = "testGroup";

        public override void Run()
        {
            ActiveDirectoryCompanyFunctions.CreateGroups(_groupName);
            
            var groupExists = ActiveDirectoryCompanyFunctions.GroupExists(_groupName);
            if (!groupExists)
                throw new InvalidOperationException ("Группы нет в домене");

            ActiveDirectoryCompanyFunctions.DeleteGroups(_groupName);
            
            groupExists = ActiveDirectoryCompanyFunctions.GroupExists(_groupName);
            if (groupExists)
                throw new InvalidOperationException ("Группы осталась в домене");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryCompanyFunctions.DeleteGroups(_groupName);
        }
    }
}
