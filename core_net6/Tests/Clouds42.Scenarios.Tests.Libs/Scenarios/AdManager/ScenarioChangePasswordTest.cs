﻿using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение пароля у пользователя в Active Directory
    public class ScenarioChangePasswordTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();
        private const string Newpassword = "Qwerty_123";

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            ActiveDirectoryUserFunctions.ChangePassword(_testaccount.Login, Newpassword);
        }

        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
