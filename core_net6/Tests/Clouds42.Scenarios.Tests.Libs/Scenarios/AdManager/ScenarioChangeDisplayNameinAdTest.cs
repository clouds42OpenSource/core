﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Изменение отображаемого имени у пользователя в Active Directory
    public class ScenarioChangeDisplayNameinAdTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();

        public override void Run()
        {
            var createAccountInActiveDirectoryCommand = new CreateAccountInActiveDirectoryCommand(accountData: _testaccount);
            createAccountInActiveDirectoryCommand.Run();
            var user = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            ActiveDirectoryUserFunctions.ChangeDisplayName(_testaccount.Login, _testaccount.GetDisplayName());
            var userChange = ActiveDirectoryUserFunctions.FindUser(_testaccount.Login);
            if (user.DisplayName == userChange.DisplayName)
                throw new InvalidOperationException ("Ошибка изменения отображаемого имени у пользователя домена");
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
