﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdManager
{
    //Добавление пользователя в группу если еще не входи в нее в Active Directory
    public class ScenarioAddToGroupIfNoExistTest : ScenarioBase
    {
        private readonly AccountRegistrationModelTest _testaccount = new();
        private readonly string _groupName = "_testGroup";

        public override void Run()
        {
            var createAccountandGroupInActiveDirectoryCommand = new CreateAccountandGroupInActiveDirectoryCommand(accountData: _testaccount);
            createAccountandGroupInActiveDirectoryCommand.Run();
            ActiveDirectoryCompanyFunctions.AddToGroupIfNoExist(_testaccount.Login, _groupName);
            var existUserAtGroup = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(_testaccount.Login, _groupName);
            if (!existUserAtGroup) 
                throw new InvalidOperationException ("Ошибка добавление пользователя в группу");
            ActiveDirectoryCompanyFunctions.AddToGroupIfNoExist(_testaccount.Login, _groupName);
        }
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryCompanyFunctions.DeleteGroups(_groupName);
            ActiveDirectoryUserFunctions.DeleteUser(_testaccount.Login);
        }
    }
}
