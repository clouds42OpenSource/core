﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.AccountDatabase.Publishes.Providers;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseWebAccessTests
{
    /// <summary>
    /// Тест предоставления доступа в информационную базу внешнего пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт и информационную базу
    ///         2. Создаем второй аккаунт и предоставляем доступ в базу админу второго аккаунта
    ///         3. Публикуем базу, проверяем, что в веб-конфиге базы доступы для пользователей обоих аккаунтов
    /// </summary>
    public class GrantDbAccessToExternalUserTest : ScenarioBase
    {
        private readonly CreateAccountManager _createAccountManager;
        private readonly IAcDbAccessHelper _acDbAccessHelper;

        public GrantDbAccessToExternalUserTest()
        {
            _createAccountManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountManager>();
            _acDbAccessHelper = TestContext.ServiceProvider.GetRequiredService<IAcDbAccessHelper>();
        }

        public override void Run()
        {
            Services.AddTransient<IDatabaseWebPublisher, DatabaseWebPublisher>();
            var sp = Services.BuildServiceProvider();

            CreateSegment();
            var account = CreateAccount();

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.AccountId == account.Id);

            var accountDetailsTest = new AccountDetailsTest
            {
                Account = account,
                AccountId = account.Id,
                AccountAdminId = accountUser.Id,
            };

            var createBaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetailsTest, new InfoDatabaseDomainModelTest(TestContext));
            createBaseCommand.Run();

            var createSecondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createSecondAccountCommand.Run();

            var secondAccountAdmin =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createSecondAccountCommand.AccountAdminId);

            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == createBaseCommand.AccountDatabaseId);

            _acDbAccessHelper.GrandExternalAccessToDb(database, secondAccountAdmin.Email);

            var publishHelper = sp.GetRequiredService<PublishHelper>();
            publishHelper.Publish(createBaseCommand.AccountDatabaseId);

            var accessGroupName = DomainCoreGroups.GetDatabaseGroupName(account.IndexNumber, database.DbNumber);
            Assert.IsTrue(ActiveDirectoryCompanyFunctions.ExistUserAtGroup(accountUser.Login, accessGroupName));
            Assert.IsTrue(ActiveDirectoryCompanyFunctions.ExistUserAtGroup(secondAccountAdmin.Login, accessGroupName));

            var accountdatabaseManger = sp.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.CancelPublishDatabaseWithWaiting(database.Id);

            DeleteDatabase(database.Id);
        }

        /// <summary>
        /// Создать сегмент
        /// </summary>
        private void CreateSegment()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext, true, publishNodeDtoTest: new PublishNodeDtoTest
            {
                Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer()
            });
            createSegmentCommand.Run();
        }

        /// <summary>
        /// Создать аккаунт
        /// </summary>
        /// <returns>Модель аккаунта</returns>
        private Account CreateAccount()
        {
            var managerResult = _createAccountManager.AddAccountAndCreateSessionAsync(new AccountRegistrationModelTest()).Result;

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == managerResult.Result.AccountId);
            Assert.IsNotNull(account);

            return account;
        }

        protected override bool NeedUseAd() => true;
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
