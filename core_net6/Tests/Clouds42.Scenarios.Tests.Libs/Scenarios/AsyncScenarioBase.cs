﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorker;
using Clouds42.DomainContext.Context;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders;
using Clouds42.TelegramBot;
using Clouds42.Test.Runner;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios
{

    public abstract class AsyncScenarioBase :  IAsyncScenario
    {
        protected readonly IConfiguration Configuration;
        protected readonly Clouds42DbContext Context;
        protected readonly IUnitOfWork DbLayer;
        protected readonly IServiceProvider ServiceProvider;
        protected readonly IServiceCollection Services;
        protected readonly ILogger42 Logger;
        protected readonly IHandlerException HandlerException;
        protected readonly IAccessMapping AccessMapping;

        protected AsyncScenarioBase()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddGcpKeyValueSecrets()
                .Build();

            Services = new ServiceCollection();
            Services.AddSingleton(Configuration);
            Services.AddScoped<IUnitOfWork, UnitOfWork>();
            Services.AddScoped<IAccessMapping, AccessMapping>();
            Services.AddScoped<IAccessProvider, ScenarioTestAccessProvider>();
            Services.AddSingleton<ILogger42, SerilogLogger42>();
            Services.ConfigureJobsCollection((jobsCollection) =>
            {
                jobsCollection.RegisterClouds42Jobs();
            });
            Services.AddScoped<IRequestContextTaskDispatcher>(_ => new RequestContextTaskDispatcher());
            Services.AddHttpClient<IisHttpClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    var login = Configuration.GetRequiredSection("IISConfiguration:Login")?.Value;
                    var password = Configuration.GetRequiredSection("IISConfiguration:Password")?.Value;
                    var domain = Configuration.GetRequiredSection("IISConfiguration:Domain")?.Value;

                    return new HttpClientHandler
                    {
                        UseDefaultCredentials = false,
                        ServerCertificateCustomValidationCallback =
                            HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                        Credentials = new NetworkCredential(login, password, domain),
                        PreAuthenticate = true
                    };
                });
            TestScenarioDependencyRegistrator.Register(Services, Configuration);
            RegisterTestServices(Services);
            Services.AddTelegramBots(Configuration);
            ServiceProvider = Services.BuildServiceProvider();
            DbLayer = ServiceProvider.GetRequiredService<IUnitOfWork>();
            Logger = ServiceProvider.GetRequiredService<ILogger42>();
            HandlerException = ServiceProvider.GetRequiredService<IHandlerException>();
            AccessMapping = ServiceProvider.GetRequiredService<IAccessMapping>();

            Logger42.Init(ServiceProvider);
        }

        Task IAsyncScenario.ClearGarbage()
        {
            return Task.Run(() => GarbageClearHelper.ClearGarbage(DbLayer));
        }

        public abstract Task Run();

        public void WaitAllBackgraundTasks()
        {
            var dispetcherHelper = ServiceProvider.GetRequiredService<TaskDispatcherHelper>();
            dispetcherHelper.WaitAllBackgroundTasks(true);
        }

        /// <summary>
        /// Нужно для кастомных регистраций сервисов в отдельных тестах
        /// </summary>
        /// <param name="serviceCollection"></param>
        protected virtual void RegisterTestServices(IServiceCollection serviceCollection) { }
        protected ITestContext TestContext => new TestContext(AccessProvider, DbLayer, ServiceProvider, Logger, Configuration, Context, AccessMapping, HandlerException);

        private IAccessProvider _accessProvider;
        protected IAccessProvider AccessProvider
        {
            get
            {
                if (_accessProvider != null)
                    return _accessProvider;
                return _accessProvider = new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            }
        }
    }
}
