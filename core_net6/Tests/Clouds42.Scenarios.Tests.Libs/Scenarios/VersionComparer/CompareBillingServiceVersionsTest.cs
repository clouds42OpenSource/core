﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Clouds42.Common.Helpers;
using TestSupportHelper;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.VersionComparer
{
    /// <summary>
    /// Тест провеки сравнения версий файлов разработки сервиса
    /// Сценарий:
    ///         1. Создаем список тестовых моделей версий файлов разработки
    ///         2. Выполняяем сравнение 
    ///         3. Проверяем, что результат сравнения правильный
    /// </summary>
    public class CompareBillingServiceVersionsTest : ScenarioBase
    {
        public override void Run()
        {
            var comparerTestModels = new List<ServiceVersionComparerTestModel>
            {
                new()
                {
                    OldVersion = "1",
                    NewVersion = "2",
                    Result = true
                },
                new()
                {
                    OldVersion = "1.0.0",
                    NewVersion = "1.1",
                    Result = true
                },
                new()
                {
                    OldVersion = "1.0.1",
                    NewVersion = "1.0.0.1",
                    Result = false
                },
                new()
                {
                    OldVersion = "1.0.0.0",
                    NewVersion = "75",
                    Result = true
                },
                new()
                {
                    OldVersion = "75",
                    NewVersion = "35.32.321.12",
                    Result = false
                }
            };

            foreach (var testModel in comparerTestModels)
            {
                AssertHelper.Execute(() =>
                    Assert.AreEqual(BillingServiceVersionComparer.Compare(testModel.OldVersion, testModel.NewVersion),
                        testModel.Result));
            }
        }
    }
}
