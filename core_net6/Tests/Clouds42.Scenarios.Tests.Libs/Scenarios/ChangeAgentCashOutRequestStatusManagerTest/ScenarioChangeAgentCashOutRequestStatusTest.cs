﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeAgentCashOutRequestStatusManagerTest
{
    /// <summary>
    /// Сценарий теста смены статуса заявки на вывод средств
    /// 1) Создаем аккаунт и заявку на вывод для аккаунта
    /// 2) Пытаемся сменить статус отправляя не валидные данные
    /// 3) Меняем статус с "Новая" на "в работе", деньги не должны списаться
    /// 4) Меняем статус с "в работе" на "Новая", деньги не должны списаться
    /// 5) Меняем статус с "Новая" на "Оплачена", деньги должны списаться.
    /// Проверяем что создалась транзакция на списание.
    /// 5) Меняем статус с "Оплачена" на "в работе", деньги должны вернуться на баланс аккаунта.
    /// Проверяем что создалась транзакция на возврат денежных средств.
    /// </summary>
    public class ScenarioChangeAgentCashOutRequestStatusTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultSum = 2000;

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();
            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();
            var changeAgentCashOutRequestStatusManager =
                TestContext.ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);
            Assert.IsNotNull(agentRequisitesInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            var agentCashOutRequestDto = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                Files = requisitesFiles,
                TotalSum = defaultSum,
                RequestStatus = AgentCashOutRequestStatusEnum.New,
                AgentRequisitesId = agentRequisitesId,
                PaySum = defaultSum
            };

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequestDto);

            var cashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault() ??
                                 throw new NotFoundException("Не удалось найти заявку на расходование средств");

            var changeAgentCashOutRequestStatusDto = new ChangeAgentCashOutRequestStatusDto
            {
                AgentCashOutRequestStatus =  AgentCashOutRequestStatusEnum.InProcess,
                Sum = cashOutRequest.RequestedSum,
                RequestNumber = "test"
            };

            var managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(null);
            Assert.IsTrue(managerResult.Error);

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsTrue(managerResult.Error);

            changeAgentCashOutRequestStatusDto.RequestNumber = cashOutRequest.RequestNumber;
            changeAgentCashOutRequestStatusDto.Sum = 10;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsTrue(managerResult.Error);

            changeAgentCashOutRequestStatusDto.Sum = cashOutRequest.RequestedSum;

            var agentWallet = GetAgentWalletForAccount(createAccountCommand.AccountId);
            Assert.AreEqual(agentWallet.AvailableSum, defaultSum);

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            agentWallet = GetAgentWalletForAccount(createAccountCommand.AccountId);
            Assert.AreEqual(agentWallet.AvailableSum, defaultSum);

            changeAgentCashOutRequestStatusDto.AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.New;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            agentWallet = GetAgentWalletForAccount(createAccountCommand.AccountId);
            Assert.AreEqual(agentWallet.AvailableSum, defaultSum);

            changeAgentCashOutRequestStatusDto.AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            agentWallet = GetAgentWalletForAccount(createAccountCommand.AccountId);
            Assert.AreEqual(0,agentWallet.AvailableSum);

            var outflowAgentPayment = DbLayer.AgentPaymentRepository.FirstOrDefault(ap =>
                ap.AccountOwnerId == createAccountCommand.AccountId && ap.PaymentType == PaymentType.Outflow &&
                ap.AgentPaymentSourceType == AgentPaymentSourceTypeEnum.AgentCashOutRequest);

            Assert.IsNotNull(outflowAgentPayment);

            changeAgentCashOutRequestStatusDto.AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.InProcess;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            agentWallet = GetAgentWalletForAccount(createAccountCommand.AccountId);
            Assert.AreEqual(agentWallet.AvailableSum, defaultSum);

            var inflowAgentPayment = DbLayer.AgentPaymentRepository.FirstOrDefault(ap =>
                ap.AccountOwnerId == createAccountCommand.AccountId && ap.PaymentType == PaymentType.Inflow &&
                ap.AgentPaymentSourceType == AgentPaymentSourceTypeEnum.AgentCashOutRequest);

            Assert.IsNotNull(inflowAgentPayment);
        }

        /// <summary>
        /// Получить кошелек для партнера
        /// </summary>
        /// <param name="accountId">Id партнера</param>
        /// <returns>Кошелек партнера</returns>
        private AgentWallet GetAgentWalletForAccount(Guid accountId) =>
            DbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == accountId) ??
            throw new NotFoundException("Не удалось получить кошелек для аккаунта");
    }
}
