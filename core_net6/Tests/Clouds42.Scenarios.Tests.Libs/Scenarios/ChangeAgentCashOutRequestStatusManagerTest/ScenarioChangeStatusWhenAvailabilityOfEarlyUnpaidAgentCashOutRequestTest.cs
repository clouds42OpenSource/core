﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeAgentCashOutRequestStatusManagerTest
{
    /// <summary>
    /// Сценарий теста смены статуса заявки на "Оплачено"
    /// если есть более ранние не оплаченные заявки
    ///
    /// 1) Создаем 2 заявки на вывод и делаем им статусы "Новая"
    /// 2) Пытаемся поменять статус второй заявке на "Оплачено" и ожидаем ошибку,
    /// так как есть более ранняя не оплаченная заявка
    /// </summary>
    public class ScenarioChangeStatusWhenAvailabilityOfEarlyUnpaidAgentCashOutRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var defaultSum = 2000;

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();
            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();
            var changeAgentCashOutRequestStatusManager =
                TestContext.ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);
            Assert.IsNotNull(agentRequisitesInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            var agentCashOutRequestDto = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                Files = requisitesFiles,
                TotalSum = defaultSum,
                RequestStatus = AgentCashOutRequestStatusEnum.New,
                AgentRequisitesId = agentRequisitesId,
                PaySum = defaultSum
            };

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequestDto);

            var cashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault() ??
                                 throw new NotFoundException("Не удалось найти заявку на расходование средств");

            var changeAgentCashOutRequestStatusDto = new ChangeAgentCashOutRequestStatusDto
            {
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid,
                Sum = cashOutRequest.RequestedSum,
                RequestNumber = cashOutRequest.RequestNumber
            };

            var managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            var creationResult = createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequestDto);

            var secondCashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(acr => acr.Id == creationResult.Result) ??
                                 throw new NotFoundException("Не удалось найти заявку на расходование средств");

            changeAgentCashOutRequestStatusDto.AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.New;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            changeAgentCashOutRequestStatusDto.AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid;
            changeAgentCashOutRequestStatusDto.RequestNumber = secondCashOutRequest.RequestNumber;

            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatusDto);
            Assert.IsTrue(managerResult.Error, "Cуществует более ранняя заявка, которая еще не оплаченная.");
        }
    }
}
