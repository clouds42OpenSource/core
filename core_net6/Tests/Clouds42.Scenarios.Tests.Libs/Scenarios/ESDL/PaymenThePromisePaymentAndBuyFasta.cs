﻿using System;
using System.Threading.Tasks;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.FastaContext.Queries;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ESDL
{
    /// <summary>
    /// Входящие параметры: аккаунт зарегестрирован и взят обещанный платеж
    /// 
    /// Действия: погашение обещанного платежа и покупка фасты
    /// 
    /// Ожидание: фаста куплена и доступна к использованию в Лк, разблокирована.
    /// </summary>
    public class PaymenThePromisePaymentAndBuyFasta : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var promisePaymenSum = 10;
            var licenseCount = ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // взятие оп
            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(
                createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
                {
                    SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = promisePaymenSum }
                }).Result;

            Assert.IsTrue(promisePayment);

            var billingAccount = await DbLayer.BillingAccountRepository.FirstOrDefaultAsync(ba => ba.Id == createAccountCommand.AccountId);
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<IBillingServiceDataProvider>();
            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.Esdl);


            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-10);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            await DbLayer.SaveAsync();

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            
            //оплата оп
            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Погашение обещанного платежа",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = promisePaymenSum,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Money
            });

            RefreshDbCashContext(DbLayer.BillingAccountRepository.GetAll());
            billingAccount = await DbLayer.BillingAccountRepository.FirstOrDefaultAsync(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(billingAccount.PromisePaymentSum, 0, "ОП должен был погасится");


            //покупка фасты
            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 18000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var res = resourcesManager.BuyEsdlAndRec42(1000, 1);

            if (string.IsNullOrEmpty(res.Comment) && !res.Complete)
                throw new InvalidOperationException("Произошел сбой при покупке лицензий.");

            if (!string.IsNullOrEmpty(res.Comment) && !res.Complete)
                throw new InvalidOperationException($"{res.Comment}");

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            var esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));

            Assert.AreEqual(1000, esdl.Result.CurrentPagesTariff);
            Assert.AreEqual(esdl.Result.PagesRemain, 1000 + licenseCount);
            Assert.AreEqual(esdl.Result.ExpireDate.Date, DateTime.Now.AddYears(1).AddDays(demoDaysCount).Date);
            Assert.IsFalse(esdl.Result.IsFrozen);

        }
    }
}
