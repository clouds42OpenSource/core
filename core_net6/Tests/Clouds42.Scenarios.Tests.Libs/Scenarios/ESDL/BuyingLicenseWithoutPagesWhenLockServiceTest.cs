﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Internal;
using Clouds42.CloudServices;
using Clouds42.Resources.Contracts.Interfaces;
using Core42.Application.Features.FastaContext.Queries;
using System.Threading.Tasks;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ESDL
{
	/// <summary>
    /// Входящие параметры: аккаунт зарегестрирован и ему было предоставлено в пользование на 1 месяц 10 страниц
    ///						ресурса Загрузка документа, страницы не использовал, но прошел 1 месяц и сервис заблокирован
	/// Действия: покупка 1 год лицензии, без покупки страниц
	/// Ожидание: дата ресурса изменится на + 1 год и сервис разблокируется, а страниц останется 10
	/// </summary>
	public class BuyingLicenseWithoutPagesWhenLockServiceTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();
            var licenseCount = ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");

			var recalculateResourcesConfigurationCostProvider = ServiceProvider.GetRequiredService<IRecalculateResourcesConfigurationCostProvider>();
			ResourcesService resourcesService = new ResourcesService(DbLayer, Logger, recalculateResourcesConfigurationCostProvider, HandlerException);

			var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            var esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));

            Assert.AreEqual(esdl.Result.CurrentPagesTariff, licenseCount);
            Assert.AreEqual(esdl.Result.PagesRemain, licenseCount);
            Assert.AreEqual(esdl.Result.ExpireDate.Date, DateTime.Now.AddDays(demoDaysCount).Date);

			var service = resourcesService.GetResourceConfig(createAccountCommand.AccountId, Clouds42Service.Esdl);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
	        {
		        Account = createAccountCommand.AccountId,
		        Date = DateTime.Now,
		        Description = "Задаем начальный баланс",
		        BillingServiceId = billingService.Id,
		        System = PaymentSystem.ControlPanel,
		        Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
		        Total = 12000,
		        OriginDetails = PaymentSystem.ControlPanel.ToString()
	        });

			//блокируем сервис 
			using (var dbScope = TestContext.DbLayer.SmartTransaction.Get())
	        {
		        try
		        {
			        service.Frozen = true;
			        service.ExpireDate = DateTime.Now.AddMonths(-1);
                    TestContext.DbLayer.ResourceConfigurationRepository.Update(service);
                    await TestContext.DbLayer.SaveAsync();
			        dbScope.Commit();
		        }
		        catch (Exception ex)
		        {
			        dbScope.Rollback();
			        throw new InvalidOperationException ($"Ошибка блокировки ресурсов {ex}");
		        }
	        }
			var res = resourcesManager.BuyEsdlAndRec42(0, 1);

			if (string.IsNullOrEmpty(res.Comment) && !res.Complete)
                throw new InvalidOperationException("Произошел сбой при покупке лицензий.");

	        if (!string.IsNullOrEmpty(res.Comment) && !res.Complete)
		        throw new InvalidOperationException($"{res.Comment}");


            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));

            Assert.AreEqual(esdl.Result.CurrentPagesTariff, licenseCount);
			Assert.AreEqual(esdl.Result.PagesRemain, licenseCount);
			Assert.AreEqual(esdl.Result.ExpireDate.Date, DateTime.Now.AddYears(1).Date);
			Assert.IsFalse(esdl.Result.IsFrozen);
		}
	}
}
