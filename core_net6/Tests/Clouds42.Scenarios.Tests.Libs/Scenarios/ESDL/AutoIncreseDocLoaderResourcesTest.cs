﻿using System;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ESDL
{
    /// <summary>
    /// Входящие параметры: аккаунт имеет 10 лицензий ЗД сроком на 5 дней и активную аренду 1С которая просрочилась и нуждается в пролонгации (деньги для пролонгации есть).
    /// Действия: запускаем таску пролонгации сервисов.
    /// Ожидание: доначисленно лицензий ЗД в размере 990 и стало 1000, срок ЗД = дата окончаеия аренды 1С.
    /// </summary>
    public class AutoIncreseDocLoaderResourcesTest : ScenarioBase
    {
        private readonly IBillingPaymentsProvider _billingPaymentsProvider;

        public AutoIncreseDocLoaderResourcesTest()
        {
            _billingPaymentsProvider = TestContext.ServiceProvider.GetRequiredService<IBillingPaymentsProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            _billingPaymentsProvider.CreatePayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = GetRent1CServiceOrThrowException().Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 10000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var rcRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsNotNull(rcRent1C, "Не удалось получить запись о сервисе Аренда 1С");

            rcRent1C.ExpireDate = DateTime.Now;
            DbLayer.ResourceConfigurationRepository.Update(rcRent1C);
            DbLayer.Save();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            var prolongServicesProvider = ServiceProvider.GetRequiredService<IProlongServicesProvider>();

            prolongServicesProvider.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            rcRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsNotNull(rcRent1C, "Не удалось получить запись о сервисе Аренда 1С");

            if (rcRent1C.ExpireDateValue.Date != DateTime.Now.AddMonths(1).Date)
                throw new InvalidOperationException ("Сервис аренда 1С не продлен на месяц.");
            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);
            
            var scopeValueResources = ServiceProvider.GetRequiredService<IFlowResourcesScopeDataProvider>().GetScopeValue(createAccountCommand.AccountId, CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId()).Result;

            if (scopeValueResources != 1000)
                throw new InvalidOperationException ("При пролонгации сервиса, не доначислило 850 страниц рекогнишен42.");

            var esdlResConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.Esdl);

            Assert.IsNotNull(esdlResConf, $"Не удалось получить запись о сервисе {NomenclaturesNameConstants.Esdl}");

            if (esdlResConf.ExpireDateValue.Date != rcRent1C.ExpireDateValue.Date)
                throw new InvalidOperationException ("При пролонгации сервиса, дата окончания сервиса ЗД отличается от даты окончания сервиса Аренда 1С.");
        }
    }
}
