﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Core42.Application.Features.FastaContext.Queries;
using System.Threading.Tasks;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ESDL
{
    /// <summary>
    /// Входящие параметры: аккаунт зарегестрирован и ему предоставлено в пользование на 1 месяц 10 страниц
    ///						ресурса Загрузка документа и не хватает денег для покупки
    /// Действия: покупка 3000 страниц, без покупки лицензии на один год
    /// Ожидание: выдаст сообщение с количеством недостающих денег и сумму недостоющих средст
    /// </summary>
    public class BuyingPagesWithoutLicenseWhenNoMoneyTes : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();
            var licenseCount = ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");

			var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            var esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));
            Assert.AreEqual(esdl.Result.CurrentPagesTariff, licenseCount);
            Assert.AreEqual(esdl.Result.PagesRemain, licenseCount);
            Assert.AreEqual(esdl.Result.ExpireDate.Date, DateTime.Now.AddDays(demoDaysCount).Date);

			var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
	        {
		        Account = createAccountCommand.AccountId,
		        Date = DateTime.Now,
		        Description = "Задаем начальный баланс",
		        BillingServiceId = billingService.Id,
		        System = PaymentSystem.ControlPanel,
		        Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
		        Total = 6000,
		        OriginDetails = PaymentSystem.ControlPanel.ToString()
	        });

			var res = resourcesManager.BuyEsdlAndRec42(3000, 0);

			if (string.IsNullOrEmpty(res.Comment) )
                throw new InvalidOperationException("Произошел сбой при покупке лицензий с недостоющим балансом.");

	        if (!res.NeedModey.HasValue)
		        throw new InvalidOperationException("Нет недостоющей стоимости");

	        Assert.AreEqual(15000,res.NeedModey.Value);

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));
            Assert.AreEqual(esdl.Result.CurrentPagesTariff, licenseCount);
            Assert.AreEqual(esdl.Result.PagesRemain, licenseCount);
            Assert.AreEqual(esdl.Result.ExpireDate.Date, DateTime.Now.AddDays(demoDaysCount).Date);
            Assert.IsFalse(esdl.Result.IsFrozen);
		}
	}
}
