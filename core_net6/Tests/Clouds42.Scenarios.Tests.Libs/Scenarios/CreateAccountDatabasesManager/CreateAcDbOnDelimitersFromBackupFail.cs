﻿using System;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для проверки неуспешного создания базы на разделителях из бэкапа
    /// 1. Создадим аккаунт и необходимые сущности. [EntitiesCreation]
    /// 2. Проверим, что база не будет создана при условиях: [NotValidConditionsForCreationDbFromBackup]
    ///     2.1 Неактивна Аренда 1С у аккаунта.
    ///     2.2 Передан несуществующий Id аккаунта.
    ///     2.3 Передан Id несуществующего бэкапа базы.
    ///     2.4 Не заполнено название базы.
    ///
    /// Проверки остальных полей в классе ScenarioCreateAccountDatabaseFromTemplateFail, так как используется одинаковый механизм.
    /// </summary>
    public class CreateAcDbOnDelimitersFromBackupFail : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly Cloud42ServiceFactory _cloud42ServiceFactory;
        private readonly TestDataGenerator _testDataGenerator;

        public CreateAcDbOnDelimitersFromBackupFail()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _cloud42ServiceFactory = (Cloud42ServiceFactory)TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            var dbCaption = "Test Data Base Name";

            #endregion

            #region NotValidConditionsForCreationDbFromBackup

            var createAcDbOnDelimitersFromBackupDto = _testDataGenerator.GenerateCreateAcDbOnDelimitersFromBackupDto(
                account.Id, dbTemplate.Id, uploadedFileId, createAccountCommand.AccountAdminId, "Test Data Base Name",
                true, backupId);

            var resourcesConf =
                DbLayer.ResourceConfigurationRepository.Where(resConf => resConf.AccountId == account.Id);
            DbLayer.ResourceConfigurationRepository.DeleteRange(resourcesConf);
            DbLayer.Save();

            var result = _createAccountDatabasesManager.CreateAcDbOnDelimitersFromBackup(account.Id, createAcDbOnDelimitersFromBackupDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу из бекапа при неактивной Аренде", account.Id,
                dbCaption);

            var cloudService =
                _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, createAccountCommand.AccountId);
            cloudService.ActivateService();

            result = _createAccountDatabasesManager.CreateAcDbOnDelimitersFromBackup(Guid.NewGuid(), createAcDbOnDelimitersFromBackupDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при несуществующем Id аккаунта", account.Id,
                dbCaption);

            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.AccountDatabaseBackupId = Guid.NewGuid();
            result = _createAccountDatabasesManager.CreateAcDbOnDelimitersFromBackup(account.Id, createAcDbOnDelimitersFromBackupDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при несуществующем Id бэкапа инф. базы", account.Id,
                dbCaption);

            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.AccountDatabaseBackupId = backupId;
           createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.DataBaseName = string.Empty;

           result = _createAccountDatabasesManager.CreateAcDbOnDelimitersFromBackup(account.Id, createAcDbOnDelimitersFromBackupDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при пустом названии базы", account.Id,
                dbCaption);

            #endregion

        }

        /// <summary>
        /// Проверить, что база не создана по условию
        /// </summary>
        /// <param name="result">Результат создания базы</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbCaption">Название базы при создании</param>
        private void CheckDbWasNotCreatedByCondition(ManagerResult<CreateAccountDatabasesResultDto> result,
            string errorMessage, Guid accountId, string dbCaption)
        {
            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseNotCreated(dbCaption, accountId);
            Assert.IsTrue(result.Error || result.Result == null || !result.Result.IsComplete, errorMessage);
        }
    }
}
