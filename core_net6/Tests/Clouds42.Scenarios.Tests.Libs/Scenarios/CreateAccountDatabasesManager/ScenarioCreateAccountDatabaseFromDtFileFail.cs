﻿using System;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Clouds42.CloudServices.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для неуспешного создания инф. базы из dt
    /// 1. Создадим необходимые сущности. [EntitiesCreation]
    /// 2. Проверим, что база не будет создана при отсутствии записи ResourceConfiguration.[CheckCreationWithNoResourceConfiguration]
    /// 3. Проверим, что база не будет создана при невалидном Id загруженного файла. [CheckCreationWithEmptyUploadedFileId]
    /// 4. Проверим, что база не будет создана при передачи пустой модели создания. [CheckCreationWithEmptyModel]
    /// 5. Проверим, что база не будет создана при невалидном Id аккаунта. [CheckCreationWithEmptyAccountId]
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromDtFileFail : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;

        public ScenarioCreateAccountDatabaseFromDtFileFail()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var createAcDbFromDtDto = new CreateAcDbFromDtDto
            {
                DbCaption = "Test Db Caption",
                NeedUsePromisePayment = false,
                UploadedFileId = uploadedFileId,
                UsersIdForAddAccess = [createAccountCommand.AccountAdminId]
            };

            #endregion

            #region CheckCreationWithNoResourceConfiguration

            var result =
                _createAccountDatabasesManager.CreateAccountDatabaseFromDtFile(createAccountCommand.AccountId,
                    createAcDbFromDtDto);

            Assert.IsTrue(result.Error, "Успешно создали базу при отсутствии ResourceConfiguration с BillingService = MyEnterprise");
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(ProcessesNameConstantsTest.CreateDatabaseFromDtJobName);

            #endregion

            #region CheckCreationWithEmptyModel

            createAcDbFromDtDto.UploadedFileId = uploadedFileId;

            result =
                _createAccountDatabasesManager.CreateAccountDatabaseFromDtFile(createAccountCommand.AccountId,
                    new CreateAcDbFromDtDto());

            Assert.IsTrue(result.Error, "Успешно создали базу при пустой модели создания инф. базы из файла dt");
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(ProcessesNameConstantsTest.CreateDatabaseFromDtJobName);

            #endregion

            #region CheckCreationWithEmptyAccountId

            result =
                _createAccountDatabasesManager.CreateAccountDatabaseFromDtFile(Guid.NewGuid(), 
                    new CreateAcDbFromDtDto());

            Assert.IsTrue(result.Error, "Успешно создали базу при несуществующем Id аккаунта");
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(ProcessesNameConstantsTest.CreateDatabaseFromDtJobName);

            #endregion
        }
    }
}
