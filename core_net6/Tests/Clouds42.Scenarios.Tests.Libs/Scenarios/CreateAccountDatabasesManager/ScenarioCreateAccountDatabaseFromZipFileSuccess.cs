﻿using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для успешного создания инф. базы из zip
    /// 1. Создадим необходимые сущности и зависимости. [EntitiesCreation]
    /// 2. Создадим первую базу и проверим, что она создалась. [CreateFirstDbAndCheck]
    /// 3. Создадим вторую базу и проверим, что она создалась. [CreateSecondDbAndCheck]
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromZipFileSuccess : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioCreateAccountDatabaseFromZipFileSuccess()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            #endregion

            #region CreateFirstDbAndCheck

            var createAcDbFromZipDto = _testDataGenerator.GenerateCreateAcDbFromZipDto("Test Db Caption from Zip", uploadedFileId,
                dbTemplate.Id, createAccountCommand.AccountAdminId);

            var result = _createAccountDatabasesManager.CreateAccountDatabaseFromZip(account.Id, createAcDbFromZipDto);
            Assert.IsFalse(result.Error, result.Message);

            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseCreatedLkAndDelimsByCaption(createAcDbFromZipDto.DbCaption, account.Id);

            #endregion

            #region CreateSecondDbAndCheck

            createAcDbFromZipDto.NeedUsePromisePayment = false;
            createAcDbFromZipDto.DbCaption = "Test Db Caption from Zip 2";

            _createAccountDatabasesManager.CreateAccountDatabaseFromZip(account.Id, createAcDbFromZipDto);
            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseCreatedLkAndDelimsByCaption(createAcDbFromZipDto.DbCaption, account.Id);

            #endregion
        }
    }
}
