﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager.ServerAccountDatabasesTests
{
    /// <summary>
    /// Тест проверки создания заявки на смену режима инф базы
    /// 1. Создание аккаунта базы и пополнение баланса
    /// 2. Создать и обработать заявку на смену режима базы
    /// 3. проверить создалась ли задача, списались ли деньги и начислились лицензии  
    /// </summary>
    public class RequstToChangeAccountDatabaseTypeTest : ScenarioBase
    {
        private readonly AccountDatabaseEditManager _accountDatabaseEditManager;
        readonly IMyDatabasesResourceDataProvider _myDatabasesResourceDataProvider;

        public RequstToChangeAccountDatabaseTypeTest()
        {
            _accountDatabaseEditManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            _myDatabasesResourceDataProvider = TestContext.ServiceProvider.GetRequiredService<IMyDatabasesResourceDataProvider>();
        }

        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            var database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);
            var account = DbLayer.AccountsRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDetails.AccountId);
            var paidLicenseBeforePurchase = _myDatabasesResourceDataProvider
                .GetMyDatabasesResourceOrThrowException(account.Id, ResourceType.ServerDatabasePlacement).PaidDatabasesCount;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.Admin,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            //проверяем что база изначально файловая(true)
            Assert.AreEqual(true, database.IsFile);

            var requestToChangeAccDbTypeDto = new RequestToChangeAccDbTypeDto
            {
                AccountDatabseId = database.Id,
                DatabaseAdminLogin = "",
                DatabaseAdminPassword = "",
                AdminPhoneNumber = "",
                ChangeTypeDate = DateTime.Now.AddDays(1),
            };

            var result = _accountDatabaseEditManager.ChangeAccDbTypeRequest(requestToChangeAccDbTypeDto);
            Assert.IsTrue(!result.Error);

            var requestTask = DbLayer.CoreWorkerTasksQueueRepository.OrderByDescending(x => x.CreateDate).FirstOrDefault();
            Assert.AreEqual(requestTask.CoreWorkerTask.TaskName, CoreWorkerTaskType.HandleRequestToChangeAccDbTypeJob.ToString());

            var paidLicenseAfterPurchase = _myDatabasesResourceDataProvider
                .GetMyDatabasesResourceOrThrowException(account.Id, ResourceType.ServerDatabasePlacement).PaidDatabasesCount;
            Assert.AreEqual(paidLicenseBeforePurchase + 1, paidLicenseAfterPurchase);
        }
    }
}
