﻿using System;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для неуспешного создания инф. базы из шаблона
    /// 1. Создадим аккаунт и необходимые сущности для успешного создания инф. базы из шаблона. [EntitiesCreation]
    /// 2. Проверим, что инф. база не будет создана из шаблона при условиях: [NotValidConditionsForDbCreationFromTemplate]
    ///     2.1 Проверим, что инф. база не будет создана, если для аккаунта не активирована Аренда 1С.
    ///     2.2 Проверим, что инф. база не будет создана для несуществующего Id аккаунта.
    ///     2.3 Проверим, что инф. база не будет создана для несуществующего Id файла.
    ///     2.4 Проверим, что инф. база не будет создана для пустого списка доступов пользователей.
    ///     2.5 Проверим, что инф. база не будет создана для пустого названия инф. базы.
    ///     2.6 Проверим, что инф. база не будет создана если указана настройка, что шаблон на разделителях, а шаблона на разделителях нет.
    ///     2.7 Проверим, что инф. база не будет создана для пустого списка пользователей из zip.
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromTemplateFail : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioCreateAccountDatabaseFromTemplateFail()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var dbCaption = "Test Data Base Name";

            #endregion

            #region NotValidConditionsForDbCreationFromTemplate

            var createAccountDatabasesFromTemplateDto =
                _testDataGenerator.GenerateCreateAccountDatabasesFromTemplateDto(account.Id, dbTemplate.Id,
                    uploadedFileId, createAccountCommand.AccountAdminId, "Test Data Base Name", true);

            var result = _createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(account.Id, createAccountDatabasesFromTemplateDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при неактивной Аренде", account.Id,
                dbCaption);

            var cloudService =
                _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, createAccountCommand.AccountId);
            cloudService.ActivateService();

            result = _createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(Guid.NewGuid(), createAccountDatabasesFromTemplateDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при несуществующем Id аккаунта", account.Id,
                dbCaption);


            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].UsersToGrantAccess =
                [createAccountCommand.AccountAdminId];
            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].DataBaseName = string.Empty;

            result = _createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(account.Id, createAccountDatabasesFromTemplateDto);
            CheckDbWasNotCreatedByCondition(result, "Удалось создать базу при пустом названии базы", account.Id,
                dbCaption);

            #endregion
        }

        /// <summary>
        /// Проверить, что база не создана по условию
        /// </summary>
        /// <param name="result">Результат создания базы</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbCaption">Название базы при создании</param>
        private void CheckDbWasNotCreatedByCondition(ManagerResult<CreateAccountDatabasesResultDto> result,
            string errorMessage, Guid accountId, string dbCaption)
        {
            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseNotCreated(dbCaption, accountId);
            Assert.IsTrue(result.Error || result.Result == null || !result.Result.IsComplete, errorMessage);
        }
    }
}
