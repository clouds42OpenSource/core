﻿using System;
using System.Linq;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для успешной и неуспешной активации Аренды 1С при создании базы
    /// из шаблона и кастомной выгрузки
    /// Действия:
    /// 1. Создадим аккаунт и активируем у него Аренду 1С. Проверим, что активация прошла успешно. [ActivateRentForAccountAndCheckCorrectness]
    ///    Проверим, что создалась ресурс конфигурация и появились записи об активированных сервисах
    ///    (Esdl, Recognition, MyEnterprise)
    /// 2. Проверим, что при активации Аренды 1С у аккаунта, у которого уже активна Аренда 1С не произойдет повторная активация. [TryToActivateRentForAccountWithAlreadyActivatedRent]
    /// 3. Проверим, что Аренда 1С не будет активирована для несуществующего аккаунта. [TryToActivateRentForNotExistingAccount]
    /// </summary>
    public class ActivateRent1CIfNeededSuccessAndFail : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;

        public ActivateRent1CIfNeededSuccessAndFail()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
        }
        public override void Run()
        {
            #region ActivateRentForAccountAndCheckCorrectness

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.Account.Id;

            var result = _createAccountDatabasesManager.ActivateRent1CIfNeeded(accountId);
            Assert.IsFalse(result.Error, result.Message);

            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == Clouds42Service.MyEnterprise);

            var resourceConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingServiceId == billingService.Id);

            Assert.IsNotNull(resourceConf, "Не создался ресурс конфигурации при активации Аренды 1С");

            var providedServices = DbLayer.ProvidedServiceRepository.Where(ps => ps.AccountId == accountId);

            Assert.AreEqual(3, providedServices.Count(), "Не созадились записи о сервисах при актвивации Аренды 1С");

            #endregion

            #region TryToActivateRentForAccountWithAlreadyActivatedRent

            _createAccountDatabasesManager.ActivateRent1CIfNeeded(accountId);

            providedServices = DbLayer.ProvidedServiceRepository.Where(ps => ps.AccountId == accountId);
            Assert.AreEqual(3, providedServices.Count(), "Создались записи о сервисах при повторной активации Аренды 1С");

            var resourceConfs = DbLayer.ResourceConfigurationRepository.Where(rc =>
                rc.AccountId == accountId && rc.BillingServiceId == billingService.Id);
            Assert.AreEqual(1, resourceConfs.Count(), "Создалась запись о ресурс конфигурации при повторной активации Аренды 1С");

            #endregion

            #region TryToActivateRentForNotExistingAccount

            result = _createAccountDatabasesManager.ActivateRent1CIfNeeded(Guid.NewGuid());
            Assert.IsTrue(result.Error, "Не произошла ошибка при активации Аренды 1С у несуществующего аккаунта");

            #endregion

        }
    }
}
