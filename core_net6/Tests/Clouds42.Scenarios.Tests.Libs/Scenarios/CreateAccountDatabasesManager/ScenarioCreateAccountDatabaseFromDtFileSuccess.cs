﻿using System;
using System.Linq;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий успешного создания инф. базы из dt
    /// 1. Создадим необходимые сущности:
    ///     1.1 Аккаунт
    ///     1.2 Активируем сервис "Аренда 1С" для аккаунта
    ///     1.3 Запись о загруженном файле.
    ///     1.4 Обычного пользователя аккаунта.
    /// 2. Создадим инф. базу из dt и проверим результат выполнения:
    ///     2.1 Запись о инф. базе создана
    ///     2.2 Создана задача на создание инф. базы.
    ///     2.3 Доступы пользователям еще не выданы.
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromDtFileSuccess : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;

        public ScenarioCreateAccountDatabaseFromDtFileSuccess()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);

        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var cloudService =
                _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, createAccountCommand.AccountId);
            cloudService.ActivateService();

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var notAdminUserId =_accountUserTestHelper.CreateAccountUser(account.Id);

            #endregion

            #region DatabaseCreationAndResultCheck

            var createAcDbFromDtDto = new CreateAcDbFromDtDto
            {
                DbCaption = "Test Db Caption",
                NeedUsePromisePayment = false,
                UploadedFileId = uploadedFileId,
                UsersIdForAddAccess = [createAccountCommand.AccountAdminId, notAdminUserId]
            };

            var result =
                _createAccountDatabasesManager.CreateAccountDatabaseFromDtFile(createAccountCommand.AccountId,
                    createAcDbFromDtDto);

            Assert.IsFalse(result.Error, result.Message);

            var createdDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(db => db.Caption == createAcDbFromDtDto.DbCaption) ??
                throw new NotFoundException(
                    $"Не удалось найти созданную инф. базу по названию {createAcDbFromDtDto.DbCaption}");

            _databaseProcessTestHelper.CheckProcessOfDatabaseStarted(createdDatabase.Id, ProcessesNameConstantsTest.CreateDatabaseFromDtJobName, false);

            var usersAccesses = DbLayer.AcDbAccessesRepository.Where(ac => ac.AccountID == account.Id);
            if(usersAccesses.Count() != 0)
                throw new InvalidOperationException("Доступы пользователям добавились, хотя на данном шаге еще не должны");

            #endregion
        }
    }
}
