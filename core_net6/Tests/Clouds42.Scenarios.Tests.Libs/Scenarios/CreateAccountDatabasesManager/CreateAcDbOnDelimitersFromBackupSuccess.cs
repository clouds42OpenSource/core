﻿using System;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий успешного создания базы на разделителях из бэкапа
    /// 1. Создадим аккаунт и необходимые сущности. [EntitiesCreation]
    /// 2. Создадим базу из бэкапа с заданными настройками и проверим, что база создалась корректно. [FirstDbCreationFromBackup]
    /// 3. Изменим настройки создания и создадим базу, проверив, что она также создалась корректно. [SecondDbFromBackupCreation]
    /// </summary>
    public class CreateAcDbOnDelimitersFromBackupSuccess : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public CreateAcDbOnDelimitersFromBackupSuccess()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId =_accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            #endregion

            #region FirstDbCreationFromBackup

            var createAcDbOnDelimitersFromBackupDto = _testDataGenerator.GenerateCreateAcDbOnDelimitersFromBackupDto(
                account.Id, dbTemplate.Id, uploadedFileId, createAccountCommand.AccountAdminId, "Test Data Base Name",
                true, backupId);

            CreateDbFromBackupAndCheckCorrectnessOfCreation(createAcDbOnDelimitersFromBackupDto, account.Id,
                dbTemplate.Id);

            #endregion

            #region SecondDbFromBackupCreation

            var notAdminUserId = _accountUserTestHelper.CreateAccountUser(account.Id);
            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.UsersToGrantAccess.Add(notAdminUserId);
            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.DataBaseName = "Test Data Base Name 2";

            var dbTemplateWithNoDelimTemplate = _createDbTemplateTestHelper.CreateDbTemplate();

            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.TemplateId = dbTemplateWithNoDelimTemplate.Id;
            createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.DbTemplateDelimiters = false;

            CreateDbFromBackupAndCheckCorrectnessOfCreation(createAcDbOnDelimitersFromBackupDto, account.Id,
                dbTemplateWithNoDelimTemplate.Id);

            #endregion
        }

        /// <summary>
        /// Создать базу из бэкапа и проверить правильность создания
        /// </summary>
        /// <param name="createAcDbOnDelimitersFromBackupDto">Модель создания из бэкапа</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbTemplateId">Id шаблона</param>
        private void CreateDbFromBackupAndCheckCorrectnessOfCreation(
            CreateAcDbOnDelimitersFromBackupDto createAcDbOnDelimitersFromBackupDto, Guid accountId, Guid dbTemplateId)
        {
            var result = _createAccountDatabasesManager.CreateAcDbOnDelimitersFromBackup(accountId, createAcDbOnDelimitersFromBackupDto);
            Assert.IsFalse(result.Error, result.Message);
            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseCreated(
                createAcDbOnDelimitersFromBackupDto.CreateAcDbModel.DataBaseName, accountId, out var fromBackupDbId);

            var fromBackupDb = DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == fromBackupDbId && db.AccountId == accountId);
            Assert.AreEqual(fromBackupDb.StateEnum, DatabaseState.NewItem);
            Assert.AreEqual(fromBackupDb.TemplateId, dbTemplateId);
        }
    }
}
