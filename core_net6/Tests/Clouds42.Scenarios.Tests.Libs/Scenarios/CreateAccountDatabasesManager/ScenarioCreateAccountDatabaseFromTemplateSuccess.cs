﻿using System;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для успешного создания инф. базы из шаблона
    /// 1. Создадим аккаунт и необходимые сущности. [EntitiesCreation]
    /// 2. Создадим базу из шаблона с настройками, для создания базы на разделителях. [DbCreationWithOptionsForDelimiters]
    /// 3. Создадим базу из шаблона с настройками, чтобы база на разделителях не была создана. [DbCreationWithNoOptionsForDelimiters]
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromTemplateSuccess : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioCreateAccountDatabaseFromTemplateSuccess()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            #endregion

            #region DbCreationWithOptionsForDelimiters

            var createAccountDatabasesFromTemplateDto =
                _testDataGenerator.GenerateCreateAccountDatabasesFromTemplateDto(account.Id, dbTemplate.Id,
                    uploadedFileId, createAccountCommand.AccountAdminId, "Test Data Base Name", true);

            var result = _createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(account.Id, createAccountDatabasesFromTemplateDto);
            if(result.Error || result.Result == null || !result.Result.IsComplete)
                throw new InvalidOperationException("Не удалось создать инф. базу из шаблона");

            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseCreatedLkAndDelimsByCaption(
                createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].DataBaseName, account.Id);

            #endregion

            #region DbCreationWithNoOptionsForDelimiters

            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].DataBaseName = "Test Data Base Name 2";
            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].DbTemplateDelimiters = false;

            var notAdminUserId = _accountUserTestHelper.CreateAccountUser(account.Id);
            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].ListInfoAboutUserFromZipPackage.Add(new InfoAboutUserFromZipPackageDto
            {
                AccountUserId = notAdminUserId,
                AccountDatabaseZipUserId = Guid.NewGuid(),
            });
            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].UsersToGrantAccess.Add(notAdminUserId);

            var dbTemplateOnlyInLk = _createDbTemplateTestHelper.CreateDbTemplate();
            createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].TemplateId = dbTemplateOnlyInLk.Id;

            result = _createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(account.Id, createAccountDatabasesFromTemplateDto);
            if (result.Error || result.Result == null || !result.Result.IsComplete)
                throw new InvalidOperationException("Не удалось создать инф. базу из шаблона");

            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseCreated(
                createAccountDatabasesFromTemplateDto.CreateDatabasesModelDc[0].DataBaseName, account.Id, out var databaseId);

            var databaseOnDelim = DbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(db => db.AccountDatabaseId == databaseId);
            if (databaseOnDelim != null)
                throw new InvalidOperationException($"Создалась база на разделителях с Id {databaseId}");

            #endregion
        }
    }
}
