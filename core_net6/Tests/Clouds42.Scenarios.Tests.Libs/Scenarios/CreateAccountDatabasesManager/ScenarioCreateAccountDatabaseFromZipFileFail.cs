﻿using System;
using Clouds42.Billing;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Managers = Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabasesManager
{
    /// <summary>
    /// Сценарий для проверки неуспешного создания инф. баз из zip
    /// 1. Создадим необходимые сущности и зависимости. [EntitiesCreation]
    /// 2. Выполним проверки, при которых инф. база не должна быть создана [CheckNotValidData]
    ///     2.1 Не существующий Id загруженного файла.
    ///     2.2 Не существующий Id шаблона.
    ///     2.3 Шаблон существует, но не существует шаблон на разделителях.
    ///     2.4 Не существует пользователь ЛК.
    ///     2.5 Не существует Id аккаунта.
    ///     2.6 Передана пустая модель создания.
    ///     2.7 Пустое название инф. базы.
    /// </summary>
    public class ScenarioCreateAccountDatabaseFromZipFileFail : ScenarioBase
    {
        private readonly Managers.CreateAccountDatabasesManager _createAccountDatabasesManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabaseCheckerTestHelper _accountDatabaseCheckerTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioCreateAccountDatabaseFromZipFileFail()
        {
            _createAccountDatabasesManager = TestContext.ServiceProvider.GetRequiredService<Managers.CreateAccountDatabasesManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabaseCheckerTestHelper = new AccountDatabaseCheckerTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var uploadedFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var createAcDbFromZipDto = _testDataGenerator.GenerateCreateAcDbFromZipDto("Test Db Caption from Zip", Guid.NewGuid(),
                dbTemplate.Id, createAccountCommand.AccountAdminId);

            #endregion

            #region CheckNotValidData


            createAcDbFromZipDto.UploadedFileId = uploadedFileId;
            createAcDbFromZipDto.TemplateId = Guid.NewGuid();

            TryToCreateDbFromZip(createAcDbFromZipDto, account.Id,
                "Результат не вернул ошибку при невалидном Id шаблона");

            createAcDbFromZipDto.UserFromZipPackageList[0].AccountUserId = createAccountCommand.AccountAdminId;

            TryToCreateDbFromZip(createAcDbFromZipDto, Guid.NewGuid(), 
                "Результат не вернул ошибку при том, что Аккаунт не сущестует");

            TryToCreateDbFromZip(new CreateAcDbFromZipDto(), account.Id,
                "Результат не вернул ошибку при пустой модели создания");

            createAcDbFromZipDto.DbCaption = string.Empty;

            TryToCreateDbFromZip(new CreateAcDbFromZipDto(), account.Id,
                "Результат не вернул ошибку при пустом названии инф. базы");

            #endregion
        }

        /// <summary>
        /// Попробовать создать базу из zip
        /// </summary>
        /// <param name="createAcDbFromZipDto">Модель создания базы из zip</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="errorMessage">Описание ошибки</param>
        private void TryToCreateDbFromZip(CreateAcDbFromZipDto createAcDbFromZipDto, Guid accountId,
            string errorMessage)
        {
            var result = _createAccountDatabasesManager.CreateAccountDatabaseFromZip(accountId, createAcDbFromZipDto);

            if(result.Result != null)
                Assert.IsFalse(result.Result.IsComplete, errorMessage);
            _accountDatabaseCheckerTestHelper.CheckAccountDatabaseNotCreated(createAcDbFromZipDto.DbCaption, accountId);
        }
    }
}
