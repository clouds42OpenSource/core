﻿using System;
using Clouds42.Common.ManagersResults;
using Clouds42.Segment.CoreHosting.Managers;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesSegment.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Hosting
{
    /// <summary>
    /// Проверка удаления хостинга
    /// Действия:
    /// 1) Создаём аккаунт
    /// 2) Создаём хостинг
    /// 3) Создаём сегмент с указанием нового хостинга
    /// 4) Удаляем хостинг
    /// 5) Удаляем сегмент
    /// 6) Удаляем хостинг
    /// Проверяем: пока есть сегмент, использующий рассматриваемый хостинг,
    /// этот хостинг удалить нельзя
    /// </summary>
    public class ScenarioDeleteUsedHostingTest: ScenarioBase
    {
        private readonly CreateAndEditCoreHostingManager _createAndEditCoreHostingManager;
        private readonly CoreHostingManager _coreHostingManager;
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public ScenarioDeleteUsedHostingTest()
        {
            _createAndEditCoreHostingManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
            _coreHostingManager = TestContext.ServiceProvider.GetRequiredService<CoreHostingManager>();
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            var CoreHostingDto = new CoreHostingDto
            {
                Id = Guid.NewGuid(),
                Name = "Тестовый хостинг",
                UploadFilesPath = "Начальный путь",
                UploadApiUrl = "Начальный путь"
            };

            _createAndEditCoreHostingManager.CreateCoreHosting(CoreHostingDto);
            RefreshDbCashContext(TestContext.Context.CoreHosting);
            var hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);
            Assert.IsNotNull(hosting, "Ошибка создания хостинга");

            var createSegmentCommand = new CreateSegmentCommand(TestContext, defaultSegment: false, CoreHostingDto: CoreHostingDto);
            createSegmentCommand.Run();

            var deleteResult = _coreHostingManager.DeleteCoreHosting(hosting.Id);

            Assert.IsTrue(deleteResult.Error,"Удаление должно вернуть ошибку");
            Assert.AreEqual(deleteResult.State,ManagerResultState.PreconditionFailed,"Неверный тип ошибки");

            _cloudSegmentReferenceManager.DeleteSegment(createSegmentCommand.Id);
            RefreshDbCashContext(TestContext.Context.CloudServicesSegmentStorages);
            var segment =
                DbLayer.CloudServicesSegmentRepository.FirstOrDefault(a => a.ID == createSegmentCommand.Id);

            if (segment != null) 
                throw new InvalidOperationException("Сегмент не удалился.");
            deleteResult = _coreHostingManager.DeleteCoreHosting(hosting.Id);
            Assert.IsFalse(deleteResult.Error, "Удаление не должно вернуть ошибку");
            Assert.AreEqual(deleteResult.State, ManagerResultState.Ok, "Неверный тип результата");
            hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);
            Assert.IsNull(hosting,"Ошибка удаления");

        }
    }
}
