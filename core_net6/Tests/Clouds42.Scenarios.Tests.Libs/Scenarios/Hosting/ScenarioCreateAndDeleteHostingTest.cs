﻿using System;
using Clouds42.Segment.CoreHosting.Managers;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Hosting
{
    /// <summary>
    /// Проверка удаления хостинга
    /// Действия:
    /// 1) создаём аккаунт
    /// 2) создаём второй тестовый хостинг
    /// 3) удаляем его
    /// Проверяем базу на наличие изменений
    /// </summary>
    public class ScenarioCreateAndDeleteHostingTest: ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            CoreHostingDto CoreHostingDto = new CoreHostingDto
            {
                Id = Guid.NewGuid(),
                Name = "Тестовый хостинг",
                UploadFilesPath = "Начальный путь",
                UploadApiUrl = "Начальный путь"
            };

            var createAndEditCoreHostingManager = ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
            createAndEditCoreHostingManager.CreateCoreHosting(CoreHostingDto);
            RefreshDbCashContext(TestContext.Context.CoreHosting);
            var hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);
            Assert.IsNotNull(hosting, "Ошибка создания хостинга");

            var coreHostingManager = ServiceProvider.GetRequiredService<CoreHostingManager>();
            coreHostingManager.DeleteCoreHosting(hosting.Id);
            hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);
            Assert.IsNull(hosting, "Ошибка удаления хостинга");

        }
    }
}
