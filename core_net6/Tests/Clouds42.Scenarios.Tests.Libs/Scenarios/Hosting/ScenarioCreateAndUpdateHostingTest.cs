﻿using System;
using Clouds42.Segment.CoreHosting.Managers;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Hosting
{
    /// <summary>
    /// Проверка редактирования хостинга
    /// Действия:
    /// 1) создаём аккаунт
    /// 2) создаём второй тестовый хостинг
    /// 3) редактируем его
    /// Проверяем базу на наличие изменений
    /// </summary>
    public class ScenarioCreateAndUpdateHostingTest: ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            CoreHostingDto CoreHostingDto = new CoreHostingDto
            {
                Id = Guid.NewGuid(),
                Name = "Тестовый хостинг",
                UploadFilesPath = "Начальный путь",
                UploadApiUrl = "Начальный путь"
            };

            var testHostName = "тестовое имя";
            var testPath = "тестовый путь";
            var testUrl = "тестовый адрес";

            var createAndEditCoreHostingManager = ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
            createAndEditCoreHostingManager.CreateCoreHosting(CoreHostingDto);

            RefreshDbCashContext(TestContext.Context.CoreHosting);
            var hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);
            Assert.IsNotNull(hosting,"Ошибка создания хостинга");
            CoreHostingDto.Name = testHostName;
            CoreHostingDto.UploadFilesPath = testPath;
            CoreHostingDto.UploadApiUrl = testUrl;

            createAndEditCoreHostingManager.EditCoreHosting(CoreHostingDto);
            RefreshDbCashContext(TestContext.Context.CoreHosting);
            hosting = DbLayer.CoreHostingRepository.FirstOrDefault(h => h.Id == CoreHostingDto.Id);

            Assert.AreEqual(hosting.Name,testHostName,"Ошибка переименования хостинга");
            Assert.AreEqual(hosting.UploadFilesPath,testPath, "Ошибка редактирования пути для загрузки");
            Assert.AreEqual(hosting.UploadApiUrl,testUrl, "Ошибка редактирования адреса API для загрузки");

        }
    }
}
