﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Segment.CoreHosting.Managers;
using Core42.Application.Features.CoreHostingContext.Queries;
using Core42.Application.Features.CoreHostingContext.Queries.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreHosting
{
    /// <summary>
    /// Тест получения отфильтрованных хостингов
    /// </summary>
    public class GetFilteredHostingsTest : ScenarioBase
    {
        private readonly CreateAndEditCoreHostingManager _createAndEditCoreHostingManager;

        public GetFilteredHostingsTest()
        {
            _createAndEditCoreHostingManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
        }

        public override async Task RunAsync()
        {
            var coreHosting = await DbLayer.CoreHostingRepository
                .AsQueryableNoTracking()
                .ToListAsync();

            var hostingsCount = 20 + coreHosting.Count;

            CreateHostingsByCount(hostingsCount);
            await GetFilteredByNameHostings();
            await GetFilteredByUploadApiUrlHostings();
            await GetFilteredByUploadFilesPathHostings();
        }

        public override void Run()
        {
        
        }

        /// <summary>
        /// Получить хостинги отсортированные
        /// по названию хостинга
        /// </summary>
        private async Task GetFilteredByNameHostings()
        {
            var coreHostings = DbLayer.CoreHostingRepository
                .AsQueryableNoTracking()
                .Count(ch => ch.Name.StartsWith("HostingName_1"));

            var managerResult = await Mediator.Send(new GetFilteredCoreHostingQuery{ Filter = new CoreHostingFilter{ Name = "HostingName_1" }, PageNumber = 1});

            Assert.IsFalse(managerResult.Error, managerResult.Message);
            Assert.AreEqual(coreHostings, managerResult.Result.Records.Count);
        }

        /// <summary>
        /// Получить хостинги отсортированные
        /// по адресу API для загрузки файловм
        /// </summary>
        private async Task GetFilteredByUploadApiUrlHostings()
        {
            var managerResult = await Mediator.Send(new GetFilteredCoreHostingQuery { Filter = new CoreHostingFilter { UploadApiUrl = "HostingUploadApiUrl_1" }, PageNumber = 1 });

            var coreHostings = DbLayer.CoreHostingRepository
                .AsQueryableNoTracking()
                .Count(ch => ch.UploadApiUrl.StartsWith("HostingUploadApiUrl_1"));
            
            Assert.IsFalse(managerResult.Error, managerResult.Message);
            Assert.AreEqual(coreHostings, managerResult.Result.Records.Count);
        }

        /// <summary>
        /// Получить хостинги отсортированные
        /// по пути для загружаемых файлов
        /// </summary>
        private async Task GetFilteredByUploadFilesPathHostings()
        {
            var managerResult = await Mediator.Send(new GetFilteredCoreHostingQuery { Filter = new CoreHostingFilter { UploadFilesPath = "HostingUploadFilesPath_1" }, PageNumber = 1 });

            var coreHostings = DbLayer.CoreHostingRepository
                .AsQueryableNoTracking()
                .Count(ch => ch.UploadFilesPath.StartsWith("HostingUploadFilesPath_1"));
            
            Assert.IsFalse(managerResult.Error, managerResult.Message);
            Assert.AreEqual(coreHostings, managerResult.Result.Records.Count);
        }

        /// <summary>
        /// Создать хостинги по кличеству
        /// </summary>
        /// <param name="hostingsCount">Количество, которое необходимо создать</param>
        private void CreateHostingsByCount(int hostingsCount)
        {
            for (int i = 0; i < hostingsCount; i++)
            {
                var coreHostingDto = new CoreHostingDto
                {
                    Name = $"HostingName_{i}",
                    UploadApiUrl = $"HostingUploadApiUrl_{i}",
                    UploadFilesPath = $"HostingUploadFilesPath_{i}"
                };
                var managerResult = _createAndEditCoreHostingManager.CreateCoreHosting(coreHostingDto);
                Assert.IsFalse(managerResult.Error, managerResult.Message);
            }

        }
    }
}
