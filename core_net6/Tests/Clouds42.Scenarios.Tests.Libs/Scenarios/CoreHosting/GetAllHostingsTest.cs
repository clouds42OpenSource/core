﻿using System.Threading.Tasks;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Segment.CoreHosting.Managers;
using Core42.Application.Features.CoreHostingContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreHosting
{
    /// <summary>
    /// Тест получения всех хостингов
    /// </summary>
    public class GetAllHostingsTest : ScenarioBase
    {
        private readonly CreateAndEditCoreHostingManager _createAndEditCoreHostingManager;

        public GetAllHostingsTest()
        {
            _createAndEditCoreHostingManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
        }

        public override async Task RunAsync()
        {
            var coreHosting = await DbLayer.CoreHostingRepository.AsQueryableNoTracking()
                .CountAsync();

            const int hostingsCount = 20;

            CreateHostingsByCount(hostingsCount);

            var coreHostings = await Mediator.Send(new GetFilteredCoreHostingQuery { PageNumber = 1, PageSize = hostingsCount  + coreHosting });

            Assert.IsFalse(coreHostings.Error, coreHostings.Message);
            Assert.AreEqual(hostingsCount + coreHosting, coreHostings.Result.Records.Count);
        }

        public override void Run()
        {
          
        }

        /// <summary>
        /// Создать хостинги по количеству
        /// </summary>
        /// <param name="hostingsCount">Количество хостингов</param>
        private void CreateHostingsByCount(int hostingsCount)
        {
            for (int i = 0; i < hostingsCount; i++)
            {
                var coreHostingDto = new CoreHostingDto
                {
                    Name = $"HostingName_{i}",
                    UploadApiUrl = $"HostingUploadApiIrl_{i}",
                    UploadFilesPath = $"HostingUploadFilesPath_{i}"
                };
                var managerResult = _createAndEditCoreHostingManager.CreateCoreHosting(coreHostingDto);
                Assert.IsFalse(managerResult.Error, managerResult.Message);
            }
           
        }
    }
}
