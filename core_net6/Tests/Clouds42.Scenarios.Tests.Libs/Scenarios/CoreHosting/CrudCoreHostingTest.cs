﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CoreHosting.Managers;
using Clouds42.DataContracts.Service.CoreHosting;
using Core42.Application.Features.CoreHostingContext.Queries;
using System.Threading.Tasks;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreHosting
{
    /// <summary>
    /// Тест создания/удаления/редактирования хостинга
    /// Сценарий:
    ///         1) Создаем хостинг, проверяем, что создание прошло без ошибок
    ///         2) Редактируем созданный хостинг, проверяем, редактирование прошло без ошибок
    ///         3) Удаляем хостинг, проверяем, что хостинг удален
    /// </summary>
    public class CrudCoreHostingTest : ScenarioBase
    {
        private readonly CoreHostingManager _coreHostingManager;
        private readonly CreateAndEditCoreHostingManager _createAndEditCoreHostingManager;

        public CrudCoreHostingTest()
        {
            _createAndEditCoreHostingManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditCoreHostingManager>();
            _coreHostingManager = TestContext.ServiceProvider.GetRequiredService<CoreHostingManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var hostingId = CreateCoreHosting();
            await EditCoreHostingAsync(hostingId);
            await DeleteCoreHosting(hostingId);
        }

        /// <summary>
        /// Создать хостинг
        /// </summary>
        /// <returns>Id хостинга</returns>
        private Guid CreateCoreHosting()
        {
            var coreHostingDto = new CoreHostingDto
            {
                Name = "Test_CoreHosting_Name",
                UploadApiUrl = "Test_CoreHosting_UploadApiUrl",
                UploadFilesPath = "Test_CoreHosting_UploadApiUrl"
            };

            var managerResult = _createAndEditCoreHostingManager.CreateCoreHosting(coreHostingDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var coreHosting = DbLayer.CoreHostingRepository.FirstOrDefault(ch => ch.Name == coreHostingDto.Name);
            Assert.IsNotNull(coreHosting, "Не удалось найти хостинг");

            return coreHosting.Id;
        }

        /// <summary>
        /// Редактировать хостинг
        /// </summary>
        /// <param name="hostingId">Id хостинга</param>
        private async Task EditCoreHostingAsync(Guid hostingId)
        {
            var coreHosting = await Mediator.Send(new GetCoreHostingByIdQuery { Id = hostingId });
            Assert.IsFalse(coreHosting.Error, coreHosting.Message);

            var editedCoreHostingDc = new CoreHostingDto
            {
                Id = hostingId,
                Name = "New_CoreHosting_Name",
                UploadApiUrl = "New_CoreHosting_UploadApiUrl",
                UploadFilesPath = "New_CoreHosting_UploadFilesPath"
            };

            var managerResult = _createAndEditCoreHostingManager.EditCoreHosting(editedCoreHostingDc);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var editedCoreHosting = await Mediator.Send(new GetCoreHostingByIdQuery { Id = hostingId });
            Assert.IsFalse(editedCoreHosting.Error, editedCoreHosting.Message);

            CompareCoreHostings(editedCoreHostingDc, editedCoreHosting.Result);

        }

        /// <summary>
        /// Сравнить модели хостинга
        /// </summary>
        /// <param name="expectedDc">Ожидаемое значение</param>
        /// <param name="actualDc">Действительное значение</param>
        private void CompareCoreHostings(CoreHostingDto expectedDc, Core42.Application.Features.CoreHostingContext.Dtos.CoreHostingDto actualDc)
        {
            Assert.AreEqual(expectedDc.Name, actualDc.Name, $"Поле название должно быть {expectedDc.Name}");
            Assert.AreEqual(expectedDc.UploadApiUrl, actualDc.UploadApiUrl, $"Поле fдрес API для загрузки файлов должно быть {expectedDc.Name}");
            Assert.AreEqual(expectedDc.UploadFilesPath, actualDc.UploadFilesPath, $"Поле путь для загружаемых файлов должно быть {expectedDc.Name}");
        }

        /// <summary>
        /// Удалить хостинг
        /// </summary>
        /// <param name="hostingId">Id хостинга</param>
        private async Task DeleteCoreHosting(Guid hostingId)
        {
            var deleteCoreHosting = _coreHostingManager.DeleteCoreHosting(hostingId);
            Assert.IsFalse(deleteCoreHosting.Error, deleteCoreHosting.Message);

            var coreHostingById = await Mediator.Send(new GetCoreHostingByIdQuery { Id = hostingId });
            Assert.IsTrue(coreHostingById.Error, "Должна быть ошибка тк хостинг был удалено");
        }
    }
}
