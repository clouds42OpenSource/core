﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Locales.Extensions;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Localization
{
    /// <summary>
    /// Тест проверки локализованного значения сервиса Аренда 1С
    /// Сценарий:
    ///         1. Создаем 2 аккаунта Ru и Ua локалей
    ///         2. Получаем локализованные значения сервиса Аренда 1С для них
    ///         3. Сравниваем их и проверяем, что значения отличаются
    /// </summary>
    public class BillingServiceNameLocalizationTest : ScenarioBase
    {
        public override void Run()
        {
            var rent1C = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>().GetSystemService(Clouds42Service.MyEnterprise);
            

            var accountCommand = new CreateAccountCommand(TestContext);
            accountCommand.Run();

            var ukrAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Ukraine
            });
            ukrAccountCommand.Run();

            Assert.AreNotEqual(rent1C.GetNameBasedOnLocalization(CloudLocalizer, accountCommand.AccountId),
                rent1C.GetNameBasedOnLocalization(CloudLocalizer, ukrAccountCommand.AccountId));
        }
    }
}
