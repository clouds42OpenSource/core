﻿using Clouds42.Scenarios.Tests.Libs.Helpers.Locale;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Locales.Managers;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Localization
{
    /// <summary>
    /// Тест редактивроания конфигураций локалей
    /// Сценарий:
    ///         1. Создаем сегмент
    ///         2. Создаем конфигурацию локали
    ///         3. Редактируем конфигурацию локали, передаем в качестве аргумента, Id несуществующего сегмента, проверяем, что редактирование не выполнено
    ///         4. Редактируем конфигурацию локали, передаем в качестве аргумента, Id несуществующей локали, проверяем, что редактирование не выполнено
    ///         5. Редактируем конфигурацию локали, передаем все корректные аргументы, проверяем, что редактирование выполнено успешно
    /// </summary>
    public class EditLocaleConfigurationTest : ScenarioBase
    {
        private readonly LocaleManager _localeManager;
        private readonly LocaleTestHelper _localeTestHelper;

        public EditLocaleConfigurationTest()
        {
            _localeManager = TestContext.ServiceProvider.GetRequiredService<LocaleManager>();
            _localeTestHelper = new LocaleTestHelper(TestContext);
        }

        public override void Run()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var locale = DbLayer.LocaleRepository.FirstOrDefault();

            var localeConfiguration =
                _localeTestHelper.CreateLocaleConfiguration(createSegmentCommand.Id, locale.ID);
            
            var managerResult = _localeManager.GetById(localeConfiguration.LocaleId);

            AssertHelper.Execute(() => Assert.IsFalse(managerResult.Error));

            var newUrl = "test_pro.url";

            EditLocale(localeConfiguration.LocaleId, newUrl, Guid.Empty, false);
            EditLocale(Guid.NewGuid(), newUrl, Guid.Empty, false);
            EditLocale(localeConfiguration.LocaleId, newUrl, createSegmentCommand.Id);

            var editedLocaleConfiguration = DbLayer.GetGenericRepository<LocaleConfiguration>()
                .FirstOrDefault(loc => loc.LocaleId == localeConfiguration.LocaleId);

            AssertHelper.Execute(() => Assert.IsNotNull(editedLocaleConfiguration));
            Assert.AreEqual(localeConfiguration.CpSiteUrl, newUrl);
        }

        /// <summary>
        /// Отредактировать конфигурацию локали
        /// </summary>
        /// <param name="id">Id локали</param>
        /// <param name="newUrl">Новый URL</param>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="isSuccess">Ожидается успешная операция(да/нет)</param>
        private void EditLocale(Guid id, string newUrl, Guid segmentId, bool isSuccess = true)
        {
            var editResult =_localeManager.Edit(new EditLocaleDto
            {
                Id = id,
                CpSiteUrl = newUrl,
                DefaultSegmentId = segmentId
            });

            if (isSuccess)
            {
                AssertHelper.Execute(() => Assert.IsFalse(editResult.Error));
                return;
            }
            
            AssertHelper.Execute(() => Assert.IsTrue(editResult.Error));
        }
    }
}
