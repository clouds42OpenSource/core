﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.TaskProviders;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Localization
{
    /// <summary>
    /// Тест механизма локализации
    /// Сценарий:
    ///         1. Создаем аккаунт, получаем значение локализованной фразы
    ///         2. Меняем локаль аккаунта, получаем значение локализованной фразы.
    ///         3. Сравниваем две локализованные фразы, проверяем, что они одинаковые
    ///         4. Обновляем данные класса-локализатора
    ///         5. Получаем значение локализованной фразы
    ///         6. Сравниваем значение с полученным ранее и проверяем, что значения отличаются
    /// </summary>
    public class CompareLocalizedPhrasesBeforeAndAfterPerformingLocalizationTest : ScenarioBase
    {
        public override void Run()
        {
            var accountCommand = new CreateAccountCommand(TestContext);
            accountCommand.Run();

            CloudLocalizer.InitializeAccountLocales();

            var ukrLocale = DbLayer.LocaleRepository.FirstOrDefault(locale => locale.Name == LocaleConst.Ukraine);

            AssertHelper.Execute(() => Assert.IsNotNull(ukrLocale));

            var value1 = CloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.UserIsAnAdministratorInThe1CDatabase,
                    accountCommand.AccountId);

            var resolve = TestContext.ServiceProvider.GetRequiredService<ChangeLocaleProvider>();
            resolve.ChangeLocaleForAccount(accountCommand.Account, ukrLocale.ID);

            AssertHelper.Execute(() => Assert.AreEqual(ukrLocale.ID, GetAccountLocaleId(accountCommand.AccountId)));
           
            var value2 = CloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.UserIsAnAdministratorInThe1CDatabase, accountCommand.AccountId);
            AssertHelper.Execute(() => Assert.AreEqual(value1, value2));

            CloudLocalizer.InitializeAccountLocales();
            
            var value3 = CloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.UserIsAnAdministratorInThe1CDatabase,
                accountCommand.AccountId);

            AssertHelper.Execute(() => Assert.AreNotEqual(value1, value3));
        }
           
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            serviceCollection.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
        }
    }
}
