﻿using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers.Locale;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Locales.Managers;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Localization
{
    /// <summary>
    /// Тест получения конфигураций локалей по фильтру
    /// Сценарий:
    ///         1. Создаем сегмент и конфигурации локалей для всех локалей
    ///         2. Получаем конфигурации локалей по названию локали, проверяем, что вернулось правильное количество записей
    ///         3. Получаем конфигурации локалей по валюте, проверяем, что вернулось правильное количество записей
    ///         4. Получаем конфигурации локалей по названию страны, проверяем, что вернулось правильное количество записей
    /// </summary>
    public class GetFilteredLocaleConfigurationsTest : ScenarioBase
    {
        private readonly LocaleManager _localeManager;
        private readonly LocaleTestHelper _localeTestHelper;

        public GetFilteredLocaleConfigurationsTest()
        {
            _localeManager = TestContext.ServiceProvider.GetRequiredService<LocaleManager>();
            _localeTestHelper = new LocaleTestHelper(TestContext);
        }

        public override void Run()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            _localeTestHelper.CreateLocaleConfigurationForAllLocales(createSegmentCommand.Id);

            var recordsByLocaleName = GetFilteredData(LocaleConst.Kazakhstan);
            Assert.AreEqual(1, recordsByLocaleName.Records.Length);

            var recordsByCurrencyCode = GetFilteredData(LocaleCurrencyConst.Griven);
            Assert.AreEqual(1, recordsByCurrencyCode.Records.Length);

            var recordsByCountryName = GetFilteredData("Россия");
            Assert.AreEqual(1, recordsByCountryName.Records.Length);

            var recordsData  = GetFilteredData("");
            Assert.AreEqual(4, recordsData.Records.Length);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        private LocalesDataDto GetFilteredData(string searchString)
        {
            var managerResult =  _localeManager.GetByFilter(new LocalesFilterDto
            {
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Desc,
                    FieldName = "Name"
                },
                Filter = new LocalesFilterDataDto
                {
                    SearchString = searchString
                },
                PageNumber = 1
            });

            AssertHelper.Execute(() => Assert.IsFalse(managerResult.Error));
            return managerResult.Result;
        }
    }
}
