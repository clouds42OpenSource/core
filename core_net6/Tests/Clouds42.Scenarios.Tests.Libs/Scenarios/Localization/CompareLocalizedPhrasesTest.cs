﻿using System;
using System.Linq;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Localization
{
    /// <summary>
    /// Тест проверки локализованных фраз
    /// Сценарий:
    ///         1. Создаем 2 аккаунта Ru и Ua локалей
    ///         2. Получаем список всех значений enum локализованных фраз
    ///         3. Сопоставляем значения локализованных фраз для каждого аккаунта, проверяем, что они отличаются
    /// </summary>
    public class CompareLocalizedPhrasesTest : ScenarioBase
    {
        public override void Run()
        {
            var accountCommand = new CreateAccountCommand(TestContext);
            accountCommand.Run();

            var ukrAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Ukraine
            });
            ukrAccountCommand.Run();


            var values = Enum.GetValues(typeof(CloudLocalizationKeyEnum)).Cast<CloudLocalizationKeyEnum>();

            foreach (var cloudLocalizationKeyEnum in values)
            {
                Assert.IsFalse(CloudLocalizer.GetValueByAccount(cloudLocalizationKeyEnum, accountCommand.AccountId) ==
                               CloudLocalizer.GetValueByAccount(cloudLocalizationKeyEnum, ukrAccountCommand.AccountId));
            }
        }
    }
}
