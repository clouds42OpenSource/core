﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.DataContracts.Account.AccountsService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceAccounts
{
    /// <summary>
    /// Тест создания/удаления служебного аккаунта
    /// Сценарий:
    ///         1. Создаем аккаунт. Делаем аккаунт служебным
    ///         2. Проверяем, что аккаунт является служебным
    ///         3. Удаляем служебный аккаунт и проверяем, что он удален
    /// </summary>
    public class CreateAndDeleteServiceAccountTest : ScenarioBase
    {
        private readonly ServiceAccountManager _serviceAccountManager;

        public CreateAndDeleteServiceAccountTest()
        {
            _serviceAccountManager = TestContext.ServiceProvider.GetRequiredService<ServiceAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var addServiceAccountDto = new AddServiceAccountDto
            {
                AccountId = createAccountCommand.AccountId
            };
            
            AddServiceAccount(addServiceAccountDto);

            var serviceAccount =
                DbLayer.ServiceAccountRepository.FirstOrDefault(sa => sa.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(serviceAccount);

            DeleteServiceAccount(createAccountCommand.AccountId);

            serviceAccount =
                DbLayer.ServiceAccountRepository.FirstOrDefault(sa => sa.Id == createAccountCommand.AccountId);
            Assert.IsNull(serviceAccount);
        }

        /// <summary>
        /// Добавить служебный аккаунт
        /// </summary>
        /// <param name="addServiceAccountDto">Модель создания служебного аккаунта</param>
        private void AddServiceAccount(AddServiceAccountDto addServiceAccountDto)
        {
            var addServiceAccount = _serviceAccountManager.AddServiceAccountItem(addServiceAccountDto);
            Assert.IsFalse(addServiceAccount.Error);
        }

        /// <summary>
        /// Удалить служебный аккаунт
        /// </summary>
        /// <param name="accountId"></param>
        private void DeleteServiceAccount(Guid accountId)
        {
            var addServiceAccount = _serviceAccountManager.DeleteServiceAccount(accountId);
            Assert.IsFalse(addServiceAccount.Error);
        }
    }
}
