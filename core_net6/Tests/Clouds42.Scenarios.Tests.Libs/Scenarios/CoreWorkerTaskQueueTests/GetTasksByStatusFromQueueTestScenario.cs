﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests
{
    /// <summary>
    /// Тест проверки метода АПИ, возвращающего Id задач, по статусу
    /// Сценарий:
    ///         1. Создаем задачи с разными статусами
    ///         2. Вызываем метод, возвращающий Id задач, по статусу
    ///         3. Проверяем, что возвращается правильное количество задач
    /// </summary>
    public class GetTasksByStatusFromQueueTestScenario : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;

        public GetTasksByStatusFromQueueTestScenario()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
        }

        public override void Run()
        {
            const int newTaskQueueCount = 6;
            const int capturedTaskQueueCount = 5;
            const int processingTaskQueueCount = 3;
            const int readyTaskCount = 5;
            
            CreateCoreWorkerTaskQueueByCount(newTaskQueueCount);

            var capturedTasks = CreateCoreWorkerTaskQueueByCount(capturedTaskQueueCount);
            UpdateTaskQueueStatus("Captured", capturedTasks);

            var processingTasks = CreateCoreWorkerTaskQueueByCount(processingTaskQueueCount);
            UpdateTaskQueueStatus("Processing", processingTasks);

            var readyTasks = CreateCoreWorkerTaskQueueByCount(readyTaskCount);
            UpdateTaskQueueStatus("Ready", readyTasks);

            var newTasksResult = _coreWorkerTasksQueueManager.GetIds("New", newTaskQueueCount);
            Assert.AreEqual(newTasksResult.Result.Count(), newTaskQueueCount);

            var capturedTasksResult = _coreWorkerTasksQueueManager.GetIds("Captured", capturedTaskQueueCount);
            Assert.AreEqual(capturedTasksResult.Result.Count(), capturedTaskQueueCount);

            var processingTasksResult = _coreWorkerTasksQueueManager.GetIds("Processing", processingTaskQueueCount);
            Assert.AreEqual(processingTasksResult.Result.Count(), processingTaskQueueCount);

            var readyTasksResult = _coreWorkerTasksQueueManager.GetIds("Ready", readyTaskCount);
            Assert.AreEqual(readyTasksResult.Result.Count(), readyTaskCount);

            var newTasksResultSecond = _coreWorkerTasksQueueManager.GetIds("New", newTaskQueueCount + 5);
            Assert.AreEqual(newTasksResultSecond.Result.Count(), newTaskQueueCount);

        }

        /// <summary>
        /// Создать задачи по количеству
        /// </summary>
        /// <param name="taskCount">Количетво задач</param>
        private List<Guid> CreateCoreWorkerTaskQueueByCount(int taskCount)
        {
            var ids = new List<Guid>();
            var counter = 0;
            var coreWorkerTasks = DbLayer.CoreWorkerTaskRepository.All().ToList();

            foreach (var coreWorkerTask in coreWorkerTasks)
            {
                if (counter == taskCount)
                    break;

                var coreWorkerTasksDto = new CoreWorkerTasksDto
                {
                    CloudTaskId = coreWorkerTask.ID
                };

                var managerResult = _coreWorkerTasksQueueManager.Add(coreWorkerTasksDto);
                Assert.IsFalse(managerResult.Error);
                ids.Add(managerResult.Result);
                counter++;
            }

            return ids;
        }

        /// <summary>
        /// Изменить статус задач
        /// </summary>
        /// <param name="status">Статус задачи</param>
        /// <param name="taskQueueIds">Список id задач</param>
        private void UpdateTaskQueueStatus(string status, IEnumerable<Guid> taskQueueIds)
        {
            foreach (var taskQueueId in taskQueueIds)
            {
                var taskQueue = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(cwtq => cwtq.Id == taskQueueId);
                Assert.IsNotNull(taskQueue);

                taskQueue.Status = status;

                DbLayer.CoreWorkerTasksQueueRepository.Update(taskQueue);
            }

            DbLayer.Save();
        }
    }
}
