﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests
{
    /// <summary>
    /// Тест проверки удаления задач из очереди
    /// </summary>
    public class CheckDeleteTasksFromTaskQueueLiveTestScenario : ScenarioCaseBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;

        public CheckDeleteTasksFromTaskQueueLiveTestScenario()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
        }

        /// <summary>
        /// Сценарий:
        ///     1. Проверить что удалились все взаимосвязанные задачи, если одна была отменена
        ///     2. Проверить что удалились одна задача из двух
        /// </summary>
        protected override IEnumerable<Action> GetTestCaseScenarioActions()
        {
            return new Action[]
            {
                CheckDeleteTwoTasksWhenOneCanceledFromTaskQueueTestScenarioCase,
                CheckDeleteOneTaskFromTwoWhenOneCanceledFromTaskQueueTestScenarioCase
            };
        }

        /// <summary>
        /// Проверить что удалились все взаимосвязанные задачи, если одна была отменена
        /// </summary>
        private void CheckDeleteTwoTasksWhenOneCanceledFromTaskQueueTestScenarioCase()
        {
            var coreWorkerTaskId = DbLayer.CoreWorkerTaskRepository.AsQueryableNoTracking().First().ID;
            var coreWorkerId = DbLayer.CoreWorkerRepository.AsQueryableNoTracking().First().CoreWorkerId;

            var taskQueue1 = CreateCoreWorkerTasksQueueObject(coreWorkerTaskId, coreWorkerId, CloudTaskQueueStatus.Processing);
            var taskQueue2 = CreateCoreWorkerTasksQueueObject(coreWorkerTaskId, null, CloudTaskQueueStatus.New);

            DbLayer.CoreWorkerTasksQueueRepository.Insert(taskQueue1);
            DbLayer.CoreWorkerTasksQueueRepository.Insert(taskQueue2);

            DbLayer.CoreWorkerTasksQueueLiveRepository.Insert(CreateCoreWorkerTasksQueueLiveObject(taskQueue1, null));
            DbLayer.CoreWorkerTasksQueueLiveRepository.Insert(CreateCoreWorkerTasksQueueLiveObject(taskQueue2, taskQueue1.Id));
            DbLayer.Save();

             _ = _coreWorkerTasksQueueManager.CancelCoreWorkerTasksQueue(taskQueue1.Id, string.Empty).Result;

            var any = DbLayer.CoreWorkerTasksQueueLiveRepository.Any(x => x.CoreWorkerTasksQueueId == taskQueue1.Id || x.DependTaskId == taskQueue1.Id);

            Assert.AreEqual(false, any);
        }


        /// <summary>
        /// Проверить что удалились одна задача из двух
        /// </summary>
        private void CheckDeleteOneTaskFromTwoWhenOneCanceledFromTaskQueueTestScenarioCase()
        {
            var coreWorkerTaskId = DbLayer.CoreWorkerTaskRepository.AsQueryableNoTracking().First().ID;
            var coreWorkerId = DbLayer.CoreWorkerRepository.AsQueryableNoTracking().First().CoreWorkerId;

            var taskQueue1 = CreateCoreWorkerTasksQueueObject(coreWorkerTaskId, coreWorkerId, CloudTaskQueueStatus.Processing);
            var taskQueue2 = CreateCoreWorkerTasksQueueObject(coreWorkerTaskId, null, CloudTaskQueueStatus.New);

            DbLayer.CoreWorkerTasksQueueRepository.Insert(taskQueue1);
            DbLayer.CoreWorkerTasksQueueRepository.Insert(taskQueue2);

            DbLayer.CoreWorkerTasksQueueLiveRepository.Insert(CreateCoreWorkerTasksQueueLiveObject(taskQueue1, null));
            DbLayer.CoreWorkerTasksQueueLiveRepository.Insert(CreateCoreWorkerTasksQueueLiveObject(taskQueue2, null));
            DbLayer.Save();

            var _ = _coreWorkerTasksQueueManager.CancelCoreWorkerTasksQueue(taskQueue1.Id, string.Empty).Result;

            var any = DbLayer.CoreWorkerTasksQueueLiveRepository.Any(x => x.CoreWorkerTasksQueueId == taskQueue2.Id);

            Assert.AreEqual(true, any);
        }

        /// <summary>
        /// Создать объект задачи в очереди
        /// </summary>
        /// <param name="coreWorkerTaskId">ID задачи</param>
        /// <returns>Объект задачи в очереди</returns>
        private static CoreWorkerTasksQueue CreateCoreWorkerTasksQueueObject(Guid coreWorkerTaskId, short? сapturedWorkerId, CloudTaskQueueStatus status)
            => new()
            {
                Id = Guid.NewGuid(),
                CoreWorkerTaskId = coreWorkerTaskId,
                CreateDate = DateTime.Now,
                Status = status.ToString(),
                CapturedWorkerId = сapturedWorkerId
            };



        /// <summary>
        /// Создать активный объект задачи в очереди
        /// </summary>
        /// <param name="task">От какой задачи в очереди создать</param>
        /// <param name="dependTaskId">ID зависимой задачи</param>
        /// <returns>Активный объект задачи в очереди</returns>
        private static CoreWorkerTasksQueueLive CreateCoreWorkerTasksQueueLiveObject(CoreWorkerTasksQueue task, Guid? dependTaskId)
            => new()
            {
                CoreWorkerTasksQueueId = task.Id,
                CoreWorkerTaskId = task.CoreWorkerTaskId,
                CreateDate = task.CreateDate,
                Status = task.QueueStatus,
                DependTaskId = dependTaskId
            };
    }
}
