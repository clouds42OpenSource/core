﻿using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.Domain.Enums.CoreWorker;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests
{
    /// <summary>
    /// Добавить задачу воркера
    /// Сценарий:
    ///         1. Создаем задачу
    ///         2. Проверяем, что она успешно создана и добавилась в CoreWorkerTasksQueueLive
    /// </summary>
    public class AddCoreWorkerTaskQueueTestScenario : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;

        public AddCoreWorkerTaskQueueTestScenario()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
        }

        public override void Run()
        {
            var managerResult = _coreWorkerTasksQueueManager.Add(CoreWorkerTaskType.TemplatesUpdater, "");
            var coreWorkerTaskQueueId = managerResult.Result;
            var coreWorkerTaskQueue = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(cwtq => cwtq.Id == coreWorkerTaskQueueId);
            var coreWorkerTaskQueueLive = DbLayer.CoreWorkerTasksQueueLiveRepository.GetById(coreWorkerTaskQueue.Id);

            Assert.IsFalse(managerResult.Error);
            Assert.IsNotNull(coreWorkerTaskQueueLive);
            Assert.IsNotNull(coreWorkerTaskQueue);
            Assert.IsTrue(string.IsNullOrEmpty(coreWorkerTaskQueue.Comment));
            Assert.AreEqual(coreWorkerTaskQueue.CoreWorkerTaskId, coreWorkerTaskQueueLive.CoreWorkerTaskId);
        }
    }
}
