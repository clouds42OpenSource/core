﻿using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Enums.CoreWorker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests
{
    /// <summary>
    /// Установить коммент к задаче в очереди
    /// Сценарий:
    ///         1. Создаем задачу
    ///         2. Меняем этой задаче коммент
    ///         3. Проверяем, что он успешно изменен
    /// </summary>
    public class SetCommentForCoreWorkerTaskQueueScenario : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;

        public SetCommentForCoreWorkerTaskQueueScenario()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
        }

        public override void Run()
        {
            var addTaskManagerResult = _coreWorkerTasksQueueManager.Add(CoreWorkerTaskType.TemplatesUpdater, "");

            var queueSetCommentDto = new CoreWorkerTasksQueueSetCommentDto
            {
                Id = addTaskManagerResult.Result,
                Comment = "Additional information"
            };
            var managerResult = _coreWorkerTasksQueueManager.SetComment(queueSetCommentDto);
            var coreWorkerTaskQueue = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(x => x.Id == queueSetCommentDto.Id);

            Assert.IsFalse(managerResult.Error);
            Assert.IsNotNull(coreWorkerTaskQueue);
            Assert.AreEqual(coreWorkerTaskQueue.Comment, queueSetCommentDto.Comment);
        }
    }
}
