﻿using System;
using System.Threading.Tasks;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Enums.CoreWorker;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTaskQueueTests
{
    /// <summary>
    /// Зазват задачи
    /// Сценарий:
    ///         1. Создаем задачу
    ///         2. Захватываем задачу
    ///         3. Проверяем что активная задача в очереди сменила статус
    /// </summary>
    public class CaptureCoreWorkerTaskQueueTestScenario : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;

        public CaptureCoreWorkerTaskQueueTestScenario()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var coreWorkerId = (await DbLayer.CoreWorkerRepository.AsQueryableNoTracking().FirstOrDefaultAsync())?.CoreWorkerId ?? throw new InvalidOperationException ("Не удалось найти идентификатор воркера в таблице CoreWorkers");

            var managerResult = _coreWorkerTasksQueueManager.Add(CoreWorkerTaskType.TemplatesUpdater, "");
            var coreWorkerTaskQueueId = managerResult.Result;

            var coreWorkerTaskQueue = await DbLayer.CoreWorkerTasksQueueRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == coreWorkerTaskQueueId);

            var id = (await _coreWorkerTasksQueueManager.CaptureTaskFromQueueAsync(new CaptureTaskRequestDto
            {
                CoreWorkerId = coreWorkerId,
                InternalTasksQueue = new InternalTasksQueueDto { List = [] }
            })).Result;

            RefreshDbCashContext(Context.CoreWorkerTasksQueuesLive);

            var taskLive = await DbLayer.CoreWorkerTasksQueueLiveRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.CoreWorkerTasksQueueId == id);

            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(coreWorkerTaskQueue?.Id, id);
            Assert.AreEqual(CloudTaskQueueStatus.Captured, taskLive?.Status ?? CloudTaskQueueStatus.Captured);
        }
    }
}
