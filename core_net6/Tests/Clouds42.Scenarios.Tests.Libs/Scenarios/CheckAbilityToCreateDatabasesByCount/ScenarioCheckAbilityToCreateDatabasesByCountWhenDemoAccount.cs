﻿using System;
using Clouds42.Billing;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CheckAbilityToCreateDatabasesByCount
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз по количеству, когда демо аккаунт
    /// </summary>
    public class ScenarioCheckAbilityToCreateDatabasesByCountWhenDemoAccount : ScenarioBase
    {
        private readonly TestDataGenerator _generator;
        private readonly int _maxCountAccountDbForDemoAccount;

        public ScenarioCheckAbilityToCreateDatabasesByCountWhenDemoAccount()
        {
            _generator = new TestDataGenerator(Configuration, DbLayer);
            _maxCountAccountDbForDemoAccount = CloudConfigurationProvider.AccountDatabase.Support.MaxCountAccountDbForDemoAccount();
        }

        public override void Run()
        {
            ScenarioWhenUserHasNoRightsToCreateDb();
            ClearGarbage();
            ScenarioWhenUserHasRightsToCreateDb();
        }

        /// <summary>
        /// Сценарий теста когда у пользователя нет особых прав на создание базы
        /// </summary>
        private void ScenarioWhenUserHasNoRightsToCreateDb()
        {
            var account = _generator.GenerateAccount();
            var accountUser = _generator.GenerateAccountUser(account.Id);
            var accountUserRole = _generator.GenerateAccountUserRole(accountUser.Id, AccountUserGroup.AccountUser);
            var accountUserSession = _generator.GenerateAccountUserSession(accountUser.Id);
            DbLayer.AccountUsersRepository.Insert(accountUser);
            DbLayer.AccountUserRoleRepository.Insert(accountUserRole);
            DbLayer.AccountUserSessionsRepository.Insert(accountUserSession);
            DbLayer.AccountsRepository.Insert(account);
            var accountDatabases = _generator.GenerateListAccountDatabasesByCount(_maxCountAccountDbForDemoAccount - 1, account.Id, 1, account.IndexNumber);
            DbLayer.DatabasesRepository.InsertRange(accountDatabases);
            DbLayer.Save();

            var result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 2);


            if (result.Complete && string.IsNullOrEmpty(result.Comment))
                throw new InvalidOperationException ($"Ошибка. количество допустимых баз для демо аккаунта = {_maxCountAccountDbForDemoAccount}");

            result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>()
                .CheckAbilityToCreateDatabasesByLimit(account, 1);

            if (!result.Complete)
                throw new InvalidOperationException ($"Ошибка. Для демо аккаунта разрешено создать {_maxCountAccountDbForDemoAccount} баз.");
        }

        /// <summary>
        /// Сценарий теста когда у пользователя имеются особые права на создание базы
        /// </summary>
        private void ScenarioWhenUserHasRightsToCreateDb()
        {
            var account = _generator.GenerateAccount();
            var accountUser = _generator.GenerateAccountUser(account.Id);
            var accountUserRole = _generator.GenerateAccountUserRole(accountUser.Id, AccountUserGroup.CloudAdmin);
            var accountUserSession = _generator.GenerateAccountUserSession(accountUser.Id);
            DbLayer.AccountUsersRepository.Insert(accountUser);
            DbLayer.AccountUserRoleRepository.Insert(accountUserRole);
            DbLayer.AccountUserSessionsRepository.Insert(accountUserSession);
            DbLayer.AccountsRepository.Insert(account);
            var accountDatabases = _generator.GenerateListAccountDatabasesByCount(_maxCountAccountDbForDemoAccount - 1, account.Id, 1, account.IndexNumber);
            DbLayer.DatabasesRepository.InsertRange(accountDatabases);
            DbLayer.Save();

            var result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 2);
            if (!result.Complete)
                throw new InvalidOperationException ("Ошибка. Для пользователя с правами CloudAdmin лимит не имеет значения");

            accountUserRole.AccountUserGroup = AccountUserGroup.AccountSaleManager;
            DbLayer.AccountUserRoleRepository.Update(accountUserRole);
            DbLayer.Save();

            result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 2);
            if (!result.Complete)
                throw new InvalidOperationException ("Ошибка. Для пользователя с правами AccountSaleManager лимит не имеет значения");

            accountUserRole.AccountUserGroup = AccountUserGroup.Hotline;
            DbLayer.AccountUserRoleRepository.Update(accountUserRole);
            DbLayer.Save();

            result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 2);
            if (!result.Complete)
                throw new InvalidOperationException ("Ошибка. Для пользователя с правами Hotline лимит не имеет значения");
        }
    }
}
