﻿using System;
using Clouds42.Billing;
using Clouds42.Accounts.Contracts.CheckAccountData.Interfaces;
using Clouds42.Configurations.Configurations;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CheckAbilityToCreateDatabasesByCount
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз по количеству, когда не демо аккаунт
    /// </summary>
    public class ScenarioCheckAbilityToCreateDatabasesByCountWhenNotDemoAccount : ScenarioBase
    {

        private readonly TestDataGenerator _generator;
        private readonly int _maxCountAccountDbForDemoAccount;

        public ScenarioCheckAbilityToCreateDatabasesByCountWhenNotDemoAccount()
        {
            _generator = new TestDataGenerator(Configuration, DbLayer);
            _maxCountAccountDbForDemoAccount = CloudConfigurationProvider.AccountDatabase.Support.MaxCountAccountDbForDemoAccount();
        }

        public override void Run()
        {
            ScenarioWhenThereIsPaymentAccount();
            ClearGarbage();
            ScenarioWhenThereIsSponsorAccount();
        }

        /// <summary>
        /// Сценарий когда есть платежи у аккаунта
        /// </summary>
        private void ScenarioWhenThereIsPaymentAccount()
        {
            var account = _generator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(account);
            var accountUser = _generator.GenerateAccountUser(account.Id);
            var accountUserRole = _generator.GenerateAccountUserRole(accountUser.Id, AccountUserGroup.AccountUser);
            var accountUserSession = _generator.GenerateAccountUserSession(accountUser.Id);
            DbLayer.AccountUsersRepository.Insert(accountUser);
            DbLayer.AccountUserRoleRepository.Insert(accountUserRole);
            DbLayer.AccountUserSessionsRepository.Insert(accountUserSession);
            var accountDatabases = _generator.GenerateListAccountDatabasesByCount(_maxCountAccountDbForDemoAccount, account.Id, 1, account.IndexNumber);
            DbLayer.DatabasesRepository.InsertRange(accountDatabases);
            var payment = _generator.GeneratePayment(account.Id);
            DbLayer.PaymentRepository.Insert(payment);
            DbLayer.Save();

            var result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 5);

            if (!result.Complete)
                throw new InvalidOperationException ("Ошибка, для данного аккаунта нет ограничений. По данному аккаунту есть платежи.");
        }

        /// <summary>
        /// Сценарий когда есть спонсор у аккаунта
        /// </summary>
        private void ScenarioWhenThereIsSponsorAccount()
        {
            var account = _generator.GenerateAccount();
            var accountUser = _generator.GenerateAccountUser(account.Id);
            var accountUserRole = _generator.GenerateAccountUserRole(accountUser.Id, AccountUserGroup.AccountUser);
            var accountUserSession = _generator.GenerateAccountUserSession(accountUser.Id);
            DbLayer.AccountUsersRepository.Insert(accountUser);
            DbLayer.AccountUserRoleRepository.Insert(accountUserRole);
            DbLayer.AccountUserSessionsRepository.Insert(accountUserSession);
            var accountSponsor = _generator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(account);
            DbLayer.AccountsRepository.Insert(accountSponsor);
            var accountDatabases = _generator.GenerateListAccountDatabasesByCount(_maxCountAccountDbForDemoAccount, account.Id, 1, account.IndexNumber);
            DbLayer.DatabasesRepository.InsertRange(accountDatabases);
            var billingAccount = _generator.GenerateBillingAccount(account.Id);
            var billingAccountForSponsor = _generator.GenerateBillingAccount(accountSponsor.Id);
            DbLayer.BillingAccountRepository.Insert(billingAccount);
            DbLayer.BillingAccountRepository.Insert(billingAccountForSponsor);            

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var resourcesConfiguration = _generator.GenerateResourcesConfiguration(accountSponsor.Id, billingService);
            var resource =
                _generator.GenerateResource(account.Id, ResourceType.MyEntUser, account.Id, accountSponsor.Id, 500);
            DbLayer.ResourceRepository.Insert(resource);

            DbLayer.ResourceConfigurationRepository.Insert(resourcesConfiguration);

            DbLayer.Save();

            var result = ServiceProvider.GetRequiredService<ICheckAccountDataProvider>().CheckAbilityToCreateDatabasesByLimit(account, 5);

            if (!result.Complete)
                throw new InvalidOperationException ("Ошибка, для данного аккаунта нет ограничений. По данному аккаунту есть спонсор.");
        }
    }
}
