﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Billing;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingPathsDatabasesAuditTest
{
    /// <summary>
    /// Сценарий теста проверки аудита путей публикации баз
    /// </summary>
    public class ScenarioCheckPublishingPathsDatabasesAudit : ScenarioBase
    {
        public override void Run()
        {
            var publicationPathHelper = new FormationOfPublicationPathHelper(DbLayer, ServiceProvider.GetRequiredService<IisHttpClient>(), CloudLocalizer);
            var generator = new TestDataGenerator(Configuration, DbLayer);

           var accountDatabaseAndAccountCommandByFileDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommandByFileDb.Run();

            var databaseByFileDb = DbLayer.DatabasesRepository.AsQueryable().Include(w => w.Account).FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);
            databaseByFileDb.PublishStateEnum = PublishState.Published;
            databaseByFileDb.IsFile = true;
            DbLayer.DatabasesRepository.Update(databaseByFileDb);
            DbLayer.Save();

            var publishNode = DbLayer.PublishNodeReferenceRepository.FirstOrDefault();
            Assert.IsNotNull(publishNode);
            publishNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
            DbLayer.PublishNodeReferenceRepository.Update(publishNode);
            DbLayer.Save();
            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();
            var pathHelper = new AccountDatabasePathHelper(DbLayer, Logger, segmentHelper, HandlerException);
            var path = pathHelper.GetPath(databaseByFileDb);
            var ibPath = $"File=&quot;{path}&quot;;";
            var publicationPaths = publicationPathHelper.GetPublicationPaths(databaseByFileDb);
            var link = segmentHelper.GetWindowsThinkClientDownloadLink(databaseByFileDb.Account, databaseByFileDb);
            var vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);

            var accountDbOne = generator.GenerateAccountDatabase(databaseByFileDb.AccountId, 2, 8, null, null,
                PublishState.Published);

            var accountDbSecond = generator.GenerateAccountDatabase(databaseByFileDb.AccountId, 2, 8);
            DbLayer.DatabasesRepository.Insert(accountDbOne);
            DbLayer.DatabasesRepository.Insert(accountDbSecond);
            DbLayer.Save();

            try
            {
                var audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabasesAudit>();
                audit.ProcessAudit();
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Выполнение аудита завершилось ошибкой");
            }
        }
    }
}
