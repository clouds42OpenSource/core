﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Billing;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingPathsDatabasesAuditTest
{
    /// <summary>
    /// Сценарий теста проверки аудита путей публикации файловых информационных баз
    /// </summary>
    public class ScenarioCheckPublishingPathsFileDatabasesAuditTest : ScenarioBase
    {
        public override void Run()
        {
            var publicationPathHelper = new FormationOfPublicationPathHelper(DbLayer, ServiceProvider.GetRequiredService<IisHttpClient>(), CloudLocalizer);
            var generator = new TestDataGenerator(Configuration, DbLayer);
            var accountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommand.Run();
            
            var accountDatabase =
                DbLayer.DatabasesRepository.AsQueryable().Include(w => w.Account).FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommand.AccountDatabaseId);

            accountDatabase.PublishStateEnum = PublishState.Published;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();

            var publishNode = DbLayer.PublishNodeReferenceRepository.FirstOrDefault();
            Assert.IsNotNull(publishNode);
            publishNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
            DbLayer.PublishNodeReferenceRepository.Update(publishNode);
            DbLayer.Save();

            var platform = DbLayer.PlatformVersionReferencesRepository.Where(w => w.Version.StartsWith("8.3") && w.PathToPlatfromX64 != null).Last();
            platform.WindowsThinClientDownloadLink = "https://drive.google.com/file/d/1PS26aytJzTE9jVjfMv1Ra4IXrmd3u1Vw/view?usp=sharing";
            DbLayer.PlatformVersionReferencesRepository.Update(platform);
            DbLayer.Save();

            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();
            var pathHelper = new AccountDatabasePathHelper(DbLayer, Logger, segmentHelper, HandlerException);
            var path = pathHelper.GetPath(accountDatabase);
            var ibPath = $"File=&quot;{path}&quot;;";
            var publicationPaths = publicationPathHelper.GetPublicationPaths(accountDatabase);
            var link = segmentHelper.GetWindowsThinkClientDownloadLink(accountDatabase.Account, accountDatabase);
            var vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);

            var audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (!audit.Success)
                throw new InvalidOperationException ("Результат аудита должен быть успешным. Файл валиден и ссылки на публикацию совпадают");

            var filePath = Path.Combine(publicationPaths.BasePath, "default.vrd");
            File.Delete(filePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл в директории нет");

            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link).Replace("point", "pointt1");
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден, нет атрибута point");

            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link).Replace("ib=", "ibу=");
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден, нет атрибута ib");

            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link).Replace("File=", "Filer=");
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден, нет значения пути");

            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link).Replace("pubdst=", "pubdstss=");
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден, нет значения публикации");

            path = pathHelper.GetPath(accountDatabase) + "error";
            ibPath = $"File=&quot;{path}&quot;;";
            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Пути публикации не совпадают");
        }
    }
}
