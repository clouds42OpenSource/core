﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Billing;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingPathsDatabasesAuditTest
{
    /// <summary>
    /// Сценарий теста проверки аудита путей публикации серверных информационных баз
    /// </summary>
    public class ScenarioCheckPublishingPathsServerDatabasesAuditTest : ScenarioBase
    {
        public override void Run()
        {

            var publicationPathHelper = new FormationOfPublicationPathHelper(DbLayer, ServiceProvider.GetRequiredService<IisHttpClient>(), CloudLocalizer);
            var generator = new TestDataGenerator(Configuration, DbLayer);
            
            var accountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommand.Run();
            
            var accountDatabase =
                DbLayer.DatabasesRepository.AsQueryable().Include(w => w.Account).FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommand.AccountDatabaseId);

            accountDatabase.PublishStateEnum = PublishState.Published;
            accountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();

            var publishNode = DbLayer.PublishNodeReferenceRepository.FirstOrDefault();
            Assert.IsNotNull(publishNode);
            publishNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
            DbLayer.PublishNodeReferenceRepository.Update(publishNode);
            DbLayer.Save();

            var platform = DbLayer.PlatformVersionReferencesRepository.Where(w => w.Version.StartsWith("8.3") && w.PathToPlatfromX64 != null).Last();
            platform.WindowsThinClientDownloadLink = "https://drive.google.com/file/d/1PS26aytJzTE9jVjfMv1Ra4IXrmd3u1Vw/view?usp=sharing";
            DbLayer.PlatformVersionReferencesRepository.Update(platform);
            DbLayer.Save();

            var segmentHelper = new SegmentHelper(DbLayer, CloudLocalizer);
            var curPlatform = string.IsNullOrEmpty(accountDatabase.ApplicationName) ? "8.2" : accountDatabase.ApplicationName.Trim();
            var server1C = curPlatform == "8.3"
                ? segmentHelper.GetEnterpriseServer83(accountDatabase.Account)
                : segmentHelper.GetEnterpriseServer82(accountDatabase.Account);
            var ibPath = $"Srvr=&quot;{server1C}&quot;;Ref=&quot;{accountDatabase.V82Name}&quot;;";

            var publicationPaths = publicationPathHelper.GetPublicationPaths(accountDatabase);
            var link = segmentHelper.GetWindowsThinkClientDownloadLink(accountDatabase.Account, accountDatabase);
            var vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);

            var audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (!audit.Success)
                throw new InvalidOperationException ("Результат аудита должен быть успешным. Файл валиден и ссылки на публикацию совпадают");

            ibPath = $"Srvrr=&quot;{server1C}&quot;;Ref=&quot;{accountDatabase.V82Name}&quot;;";
            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден.");

            ibPath = $"Srvr=&quot;{server1C}&quot;;Refs=&quot;{accountDatabase.V82Name}&quot;;";
            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Файл не валиден.");

            ibPath = $"Srvr=&quot;{server1C + "v2"}&quot;;Ref=&quot;{accountDatabase.V82Name}&quot;;";
            vrdFile = generator.GenerateDefaultVrdFile(ibPath, publicationPaths.BaseAddress, link);
            vrdFile.SaveVrd(publicationPaths.BasePath);
            audit = ServiceProvider.GetRequiredService<IPublishingPathsDatabaseAuditProvider>().AccountDatabaseAudit(accountDatabase);
            if (audit.Success)
                throw new InvalidOperationException ("Результат аудита не может быть успешным. Не совпадают пути публикации.");
        }
    }
}
