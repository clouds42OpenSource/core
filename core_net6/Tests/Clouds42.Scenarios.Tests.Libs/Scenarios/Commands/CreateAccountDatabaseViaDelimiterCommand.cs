﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountDatabaseViaDelimiterCommand : CommandBase, IAccountDatabseDetails
    {        
        private readonly IAccountDetails _account;
        private readonly IAccessProvider _accessProvider;

		private readonly InfoDatabaseDomainModelTest _databaseData;

        public CreateAccountDatabaseViaDelimiterCommand(ITestContext testContext,
            IAccountDetails account,
            InfoDatabaseDomainModelTest databaseData = null,
            IAccessProvider accessProvider = null
            ) : base(testContext)
        {
            _account = account;
            _accessProvider = accessProvider ?? new FakeAccessProvider(_account.AccountAdminId, TestContext.DbLayer, TestContext.Logger,
                TestContext.Configurations, TestContext.HandlerException, TestContext.AccessMapping);
            _databaseData = databaseData ?? new InfoDatabaseDomainModelTest(TestContext);
        }

        public override void Run()
        {            
            var createDatabaseResult =
				TestContext.ServiceProvider.GetRequiredService<CreateDatabaseOnDelimitersTestCreator>().CreateDatabases(
		            _accessProvider.ContextAccountId, [_databaseData]);

			if (!createDatabaseResult.Complete || !createDatabaseResult.Ids.Any())
                throw new InvalidOperationException ($"Не удалось создать базу данных '{createDatabaseResult.Comment}'");            

            AccountDatabaseId = createDatabaseResult.Ids.Single();
            AccountDetails = _account;

            Task.Delay(1000).Wait();
        }

        public Guid AccountDatabaseId { get; private set; }
        public IAccountDetails AccountDetails { get; private set; }

    }
}
