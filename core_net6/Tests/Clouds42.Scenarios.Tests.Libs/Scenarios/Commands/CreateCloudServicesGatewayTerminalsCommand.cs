﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesGatewayTerminal.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания бэкап хранилища
    /// </summary>
    public class CreateCloudServicesGatewayTerminalsCommand(
        ITestContext testContext,
        CreateCloudGatewayTerminalModelTest data = null)
        : CommandBase(testContext), IId
    {
        private readonly CreateCloudGatewayTerminalModelTest _data = data ?? new CreateCloudGatewayTerminalModelTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudGatewayTerminalManager>();
            var managerResult = manager.CreateCloudGatewayTerminal(_data);
            Assert.IsFalse(managerResult.Error);

            Id = managerResult.Result;
        }

        public Guid Id { get; private set;}

    }
}