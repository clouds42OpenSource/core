﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesTerminalFarm.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания Фермы ТС
    /// </summary>
    public class CreateCloudServicesTerminalFarmCommand(
        ITestContext testContext,
        CloudElementViewModelTest data = null)
        : CommandBase(testContext), IId
    {
        private readonly CloudElementViewModelTest _data = data ?? new CloudElementViewModelTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<TerminalFarmManager>();
            var result = manager.AddNewTerminalFarm(_data);
            Assert.IsFalse(result.Error, result.Message);

            Id = result.Result;
        }

        public Guid Id { get; private set; }
    }
}