﻿using System;
using System.Linq;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountDatabaseCommand : CommandBase, IAccountDatabseDetails
    {        
        private readonly IAccountDetails _account;
        private readonly IAccessProvider _accessProvider;
		private readonly InfoDatabaseDomainModelTest _databaseData;

        public CreateAccountDatabaseCommand(ITestContext testContext,
            IAccountDetails account,
            InfoDatabaseDomainModelTest databaseData = null,
            IAccessProvider accessProvider = null
            ) : base(testContext)
        {
            _account = account;
            _accessProvider = accessProvider ?? new FakeAccessProvider(_account.AccountAdminId, 
                TestContext.DbLayer, TestContext.Logger, TestContext.Configurations, TestContext.HandlerException, TestContext.AccessMapping);
            _databaseData = databaseData ?? new InfoDatabaseDomainModelTest(TestContext);
        }

        public override void Run()
        {
            if (_account != null)
                _databaseData.UsersToGrantAccess.Add(_account.AccountAdminId);

            var createDatabaseResult =
				TestContext.ServiceProvider.GetRequiredService<CreateDatabaseFromTemplateTestAdapter>().CreateDatabases(
		            _accessProvider.ContextAccountId, [_databaseData]);

			if (!createDatabaseResult.Complete || !createDatabaseResult.Ids.Any())
                throw new InvalidOperationException ($"Не удалось создать базу данных '{createDatabaseResult.Comment}'");            

            AccountDatabaseId = createDatabaseResult.Ids.Single();
            AccountDetails = _account;
        }

        public Guid AccountDatabaseId { get; private set; }
        public IAccountDetails AccountDetails { get; private set; }

    }
}
