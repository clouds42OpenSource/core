﻿using System;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Комманда по созданию платежа агрегатора ЮKassa 
    /// </summary>
    public class CreateYookassaPaymentCommand(
        ITestContext testContext,
        Guid accountId,
        PaymentStatus paymentStatus = PaymentStatus.Waiting,
        YookassaPaymentObjectTestDto yookassaPaymentObjectTest = null,
        YookassaPaymentMethodTestDto yookassaPaymentMethodTest = null)
        : CommandBase(testContext)
    {
        private readonly YookassaPaymentObjectTestDto _yookassaPaymentObjectTest = yookassaPaymentObjectTest ?? new YookassaPaymentObjectTestDto();
        private readonly YookassaPaymentMethodTestDto _yookassaPaymentMethodTest = yookassaPaymentMethodTest ?? new YookassaPaymentMethodTestDto();
        public Payment Payment { get; private set; }

        public override void Run()
        {
          
            var yookassaPaymentDataHelperTest = TestContext.ServiceProvider.GetRequiredService<YookassaPaymentDataHelperTest>();
            var yookassaPaymentProvider = TestContext.ServiceProvider.GetRequiredService<IPayYookassaAggregatorPaymentProvider>();
            var createModel = new PrepareYookassaAggregatorPaymentDto
            {
                AccountId = accountId,
                PaymentSum = _yookassaPaymentObjectTest.PaymentAmountData.Amount,
                PaymentMethodId = null
            };
            var createPaymentResult = yookassaPaymentProvider.PreparePaymentOrPayWithSavedPaymentMethod(createModel);

            if (paymentStatus != PaymentStatus.Waiting)
                TestContext.ServiceProvider.GetRequiredService<IYookassaAggregatorPaymentFinalizerProvider>().FinalizePayment(
                    new YookassaFinalPaymentObjectTestDto(_yookassaPaymentObjectTest.Id,
                        _yookassaPaymentObjectTest.IsPaid,
                        _yookassaPaymentObjectTest.PaymentAmountData.Amount,
                        paymentStatus,
                        _yookassaPaymentMethodTest));

            Payment = yookassaPaymentDataHelperTest.GetPayment(createPaymentResult.PaymentId);
        }
    }
}
