﻿using System;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Segment.PlatformVersion.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreatePlatformVersionReferenceCommand : CommandBase
    {
        private readonly PlatformVersion1CDto _modelTest;
        private static readonly Random Random = new();

        public CreatePlatformVersionReferenceCommand(ITestContext testContext,
            PlatformVersion1CDto modelTest = null) : base(testContext)
        {
            var stable83Version = $"8.3.{Random.Next(0, 9999)}.{Random.Next(0, 9999)}";
            var stable83Path = $@"C:\Program Files (x86)\1cv8\{stable83Version}\bin\";
            _modelTest = modelTest ??
                         new PlatformVersion1CDto { PathToPlatform = stable83Path, Version = stable83Version };
        }

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<PlatformVersionManager>();
            var result = manager.AddNewPlatformVersionReference(_modelTest);

            if (result.Error)
                throw new InvalidOperationException($"Не удалось создать платформу 1C. Exception: {result.Message}");

            Version = _modelTest.Version;
        }

        public string Version { get; private set; }
    }
}
