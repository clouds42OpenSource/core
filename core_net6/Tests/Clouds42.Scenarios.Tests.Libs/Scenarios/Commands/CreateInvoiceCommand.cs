﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateInvoiceCommand(
        ITestContext testContext,
        IAccountDetails account,
        decimal sum = 1000)
        : CommandBase(testContext), IInvoiceDetails
    {
        public override void Run()
        {
            var invoiceId = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreateInvoiceForSpecifiedInvoiceAmount(account.AccountId, sum).Result;

            var invoice = TestContext.DbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == invoiceId);
            if (invoice == null)
                throw new InvalidOperationException ("Не удалось создать счет на оплату");

            InvoiceId = invoice.Id;
            UniqKey = invoice.Uniq;
            Account = account;
            AccessProvider = TestContext.AccessProvider;
        }

        public Guid InvoiceId { get; private set; }
        public long UniqKey { get; private set; }
        public IAccountDetails Account { get; private set; }
        public IAccessProvider AccessProvider { get; private set; }
    }
}
