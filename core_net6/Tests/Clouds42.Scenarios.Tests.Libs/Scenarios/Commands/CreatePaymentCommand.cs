﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда для создания платежа
    /// </summary>
    public class CreatePaymentCommand : CommandBase
    {
        private readonly PaymentDefinitionModelTest _paymentDefinition;
        private readonly CreatePaymentManager _createPaymentManager;

        public CreatePaymentCommand(ITestContext testContext, PaymentDefinitionModelTest PaymentDefinitionDto) : base(testContext)
        {
            _paymentDefinition = PaymentDefinitionDto;
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
        }

        public override void Run()
        {
            var managerResult = _createPaymentManager.AddPayment(_paymentDefinition);

            if (managerResult.Result != PaymentOperationResult.Ok)
                throw new InvalidOperationException();

            var payment = TestContext.DbLayer.PaymentRepository.FindPayment(_paymentDefinition.Id);

            if (payment == null)
                throw new NotFoundException("Платеж не был найден");

            PaymentId = payment.Id;
            PaymentAccountId = payment.AccountId;
            PaymentSum = payment.Sum;
            PaymentNumber = payment.Number;
        }

        /// <summary>
        /// Id платежа
        /// </summary>
        public Guid PaymentId { get; private set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid PaymentAccountId { get; private set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; private set; }

        /// <summary>
        /// Номер платежа
        /// </summary>
        public int PaymentNumber { get; private set; }

    }
}
