﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudEnterpriseServer.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания сервер 1С
    /// </summary>
    public class CreateCloudServicesEnterpriseServerCommand(
        ITestContext testContext,
        CloudEnterpriseElementViewModelTest data = null)
        : CommandBase(testContext), IId
    {
        private readonly CloudEnterpriseElementViewModelTest _data = data ?? new CloudEnterpriseElementViewModelTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<EnterpriseServersManager>();
            var result = manager.AddNewEnterpriseServer(_data);
            Assert.IsFalse(result.Error, result.Message);

            Id = result.Result;
        }

        public Guid Id { get; private set;}

    }
}