﻿using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    //Добавление двух аккаунтов и добавление им информационных баз
    public class CreateAccountdatabaseAndTwoAccountCommand : CommandBase, IAccountDatabseDetails
    {
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;
        private readonly InfoDatabaseDomainModelTest _databaseData;

        public CreateAccountdatabaseAndTwoAccountCommand(ITestContext testContext,
            InfoDatabaseDomainModelTest databaseData = null) : base(testContext)
        {
            _databaseData = databaseData ?? new InfoDatabaseDomainModelTest(TestContext);
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(
                TestContext,
                new AccountRegistrationModelTest());
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, _databaseData);
            createAccountDatabaseCommand.Run();

            AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId;
            AccountDetails = createAccountCommand;

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto()
            };
            createAccountCommand = new CreateAccountCommand(
                TestContext,
                secondAccount);
            createAccountCommand.Run();

            createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, _databaseData);
            createAccountDatabaseCommand.Run();

            SecondAccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId;
            SecondAccountDetails = createAccountCommand;
        }

        public Guid AccountDatabaseId { get; private set; }
        public IAccountDetails AccountDetails { get; private set; }
        public Guid SecondAccountDatabaseId { get; private set; }
        public IAccountDetails SecondAccountDetails { get; private set; }

    }
}