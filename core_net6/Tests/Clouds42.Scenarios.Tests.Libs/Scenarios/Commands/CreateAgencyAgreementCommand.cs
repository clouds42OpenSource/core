﻿using System;
using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания агентского соглашения
    /// </summary>
    public class CreateAgencyAgreementCommand : CommandBase
    {
        private readonly CreateAgencyAgreementManager _createAgencyAgreementManager;
        private readonly CreateAgencyAgreementDtoTest _agreementDtoTest;
        public CreateAgencyAgreementCommand(ITestContext testContext,
            CreateAgencyAgreementDtoTest agreementDtoTest = null) : base(testContext)
        {
            _createAgencyAgreementManager = TestContext.ServiceProvider.GetRequiredService<CreateAgencyAgreementManager>();
            _agreementDtoTest = agreementDtoTest ?? new CreateAgencyAgreementDtoTest(TestContext.DbLayer);
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public override void Run()
        {
            var managerResult = _createAgencyAgreementManager.AddAgencyAgreementItem(_agreementDtoTest);

            if (managerResult.Error)
                throw new InvalidOperationException("Не удалось создать Агентское соглашение");

            Id = managerResult.Result;
            Name = _agreementDtoTest.Name;
            EffectiveDate = _agreementDtoTest.EffectiveDate;
            ServiceOwnerRewardPercent = _agreementDtoTest.ServiceOwnerRewardPercent;
            Rent1CRewardPercent = _agreementDtoTest.Rent1CRewardPercent;
            MyDiskRewardPercent = _agreementDtoTest.MyDiskRewardPercent;
        }

        /// <summary>
        /// Id агентского соглашения
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название агентского соглашения
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата вступления в силу агентского соглашения
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Вознаграждение за сервис разработчика
        /// (проценты)
        /// </summary>
        public decimal ServiceOwnerRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Аренда 1С"
        /// (проценты)
        /// </summary>
        public decimal Rent1CRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Мой диск"
        /// (проценты)
        /// </summary>
        public decimal MyDiskRewardPercent { get; set; }

    }
}
