﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountAndInvoiceCommand(
        ITestContext testContext,
        AccountRegistrationModelTest accountData = null,
        decimal sum = 1000)
        : CommandBase(testContext), IInvoiceDetails
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, accountData);
            createAccountCommand.Run();

            var createInvoiceCommand = new CreateInvoiceCommand(TestContext, createAccountCommand, sum);
            createInvoiceCommand.Run();

            InvoiceId = createInvoiceCommand.InvoiceId;
            UniqKey = createInvoiceCommand.UniqKey;
            Account = createAccountCommand;
            AccessProvider = createAccountCommand.AccessProvider;
        }

        public Guid InvoiceId { get; private set; }
        public long UniqKey { get; private set; }
        public IAccountDetails Account { get; private set; }
        public IAccessProvider AccessProvider { get; private set; }
    }
}