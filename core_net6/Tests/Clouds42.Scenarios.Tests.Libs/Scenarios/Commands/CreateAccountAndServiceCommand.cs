﻿using System;
using System.Linq;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountAndServiceCommand(
        ITestContext testContext,
        AccountRegistrationModelTest accountData = null)
        : CommandBase(testContext), IAccountDetails
    {
        private readonly AccountRegistrationModelTest _accountData = accountData ?? new AccountRegistrationModelTest();

        public override void Run()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext, defaultSegment: true);
            createSegmentCommand.Run();

            var accountsManager =  TestContext.ServiceProvider.GetRequiredService<CreateAccountManager>();
            var managerResult = accountsManager.AddAccountAndCreateSessionAsync(_accountData).Result;
            var authInfo = managerResult.Result;

            if (authInfo?.AccountId == null)
                throw new InvalidOperationException ($"Не удалось создать тестовый аккаунт {managerResult.Message}");

            AccountId = authInfo.AccountId.Value;

            var accountAdmin = TestContext.DbLayer.AccountUsersRepository.FirstOrDefault(u =>
                u.AccountId == AccountId &&
                u.AccountUserRoles.Any(a => a.AccountUserGroup == AccountUserGroup.AccountAdmin));

            if (accountAdmin == null)
                throw new InvalidOperationException ("Не удалось создать аккаунт админа");

            AccountAdminId = accountAdmin.Id;

            if (authInfo.Token == null )
                throw new InvalidOperationException ("Не удалось получить токен авторизации");

            var account = TestContext.DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);

            Account = account;

            if (account == null)
                throw new InvalidOperationException ("Не удалось создать аккаунт");

            var cloudServiceMgr = TestContext.ServiceProvider.GetRequiredService<ICloudServiceManager>();

            // Добавляем пользователя в список служб
            var csModel = new AddCloudServiceModelDto
            {
                CloudServiceId = accountAdmin.Login,
                ServiceCaption = "SomeTestServiceCaption"
            };

            cloudServiceMgr.Add(csModel);

            TestContext.DbLayer.AccountUserSessionsRepository.AsQueryable()
                .Where(x => x.AccountUserId == AccountAdminId)
                .ExecuteDelete();

            accountAdmin.AuthToken = authInfo.Token.Value;

            TestContext.DbLayer.AccountUsersRepository.Update(accountAdmin);
            TestContext.DbLayer.Save();

            Token = authInfo.Token.Value;
            Inn = account.AccountRequisites.Inn;
            Segment = createSegmentCommand;
            AccessProvider = new ScenarioTestAccessProvider(this, TestContext.DbLayer, TestContext.Logger, TestContext.Configurations, TestContext.AccessMapping, TestContext.HandlerException);
        }

        public Account Account { get; private set; }
        public Guid AccountId { get; private set; }
        public Guid AccountAdminId { get; private set; }
        public Guid Token { get; private set; }
        public string Inn { get; private set; }
        public ISegmentDetails Segment { get; private set; }
        public IAccessProvider AccessProvider { get; private set; }
    }
}
