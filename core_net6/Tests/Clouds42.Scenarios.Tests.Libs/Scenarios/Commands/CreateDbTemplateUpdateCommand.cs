﻿using System;
using System.IO;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.DbTemplates.Extensions;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Комманда для создания обновления шаблона
    /// </summary>
    public class CreateDbTemplateUpdateCommand : CommandBase
    {
        public Guid TemplateId;
        public Guid TemplateUpdateId;
        private readonly DbTemplateUpdate _dbTemplateUpdate;
        private readonly IUnitOfWork _dbLayer;
        private readonly TestDataGenerator _testDataGenerator;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        private const string TemplateUpdateVersion = "1.0.0.1";

        public CreateDbTemplateUpdateCommand(ITestContext testContext, DbTemplateUpdate dbTemplateUpdate = null) : base(testContext)
        {
            _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
            _testDataGenerator = new TestDataGenerator(testContext.Configurations, testContext.DbLayer);
            _createDbTemplateTestHelper = testContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _dbTemplateUpdate = dbTemplateUpdate ?? CreateDbTemplateUpdateModel();
        }

        public override void Run()
        {
            _dbLayer.DbTemplateUpdateRepository.Insert(_dbTemplateUpdate);
            _dbLayer.Save();

            DbTemplate template =
                _dbLayer.DbTemplateRepository.FirstOrDefault(templ => templ.Id == _dbTemplateUpdate.TemplateId) ??
                throw new NotFoundException($"Не удалось найти шаблон по Id {_dbTemplateUpdate.TemplateId}");

            if (!Directory.Exists(_dbTemplateUpdate.UpdateTemplatePath))
                Directory.CreateDirectory(_dbTemplateUpdate.UpdateTemplatePath);

            File.Copy(template.GetTemplateFullPath(), $"{_dbTemplateUpdate.UpdateTemplatePath}/{template.Name}.1CD", true);

            TemplateId = _dbTemplateUpdate.TemplateId;
            TemplateUpdateId = _dbTemplateUpdate.Id;
        }

        /// <summary>
        /// Создать модель обновления шаблона
        /// </summary>
        /// <returns>Модель обновления шаблона</returns>
        private DbTemplateUpdate CreateDbTemplateUpdateModel() =>
            _testDataGenerator.GenerateDbTemplateUpdateModel(_createDbTemplateTestHelper.CreateDbTemplate().Id,
                TemplateUpdateVersion);

    }
}
