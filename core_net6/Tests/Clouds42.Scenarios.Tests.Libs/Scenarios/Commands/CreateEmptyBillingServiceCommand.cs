﻿using Clouds42.Scenarios.Tests.Libs.Contexts;
using System;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания сервиса биллинга
    /// </summary>
    public class CreateEmptyBillingServiceCommand(
        ITestContext testContext,
        CreateBillingServiceTest model = null,
        bool needActivateService = true)
        : CommandBase(testContext)
    {
        public CreateBillingServiceTest CreateBillingServiceTest = model ?? new CreateBillingServiceTest(testContext);
        public ManagerResult Result { get; private set; }
        public Guid Id { get; set; }

        /// <summary>
        /// Результат обработки модерации сервиса
        /// </summary>
        public ManagerResult ProcessModerationResult { get; private set; } = new()
        {
            Message = $@"Указан true для параметра {nameof(needActivateService)} при запуске комманды
                            или создание сервиса завершилось ошибкой.",
            State = ManagerResultState.PreconditionFailed
        };

        /// <summary>
        /// Выполнить комманду
        /// </summary>
        public override void Run()
        {
            var createServiceManager = TestContext.ServiceProvider.GetRequiredService<CreateBillingServiceManager>();
            var createServiceResult = createServiceManager.CreateBillingService(CreateBillingServiceTest);

            Result = createServiceResult;
            Id = createServiceResult.Result;

            ActivateService(createServiceResult);
        }

        /// <summary>
        /// Выполнить активацию сервиса
        /// </summary>
        /// <param name="serviceCreationResult">Результат создания сервиса</param>
        private void ActivateService(ManagerResult<Guid> serviceCreationResult)
        {
            if (!needActivateService || serviceCreationResult.Error ||
                CreateBillingServiceTest.BillingServiceStatus == BillingServiceStatusEnum.Draft)
                return;

            var changeServiceRequest =
                TestContext.DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == serviceCreationResult.Result);

            ProcessModerationResult = TestContext.ServiceProvider.GetRequiredService<ModerationResultManager>()
                .ProcessModerationResult(
                    new ChangeServiceRequestModerationResultDto
                    {
                        Id = changeServiceRequest.Id,
                        ModeratorComment = "Comment",
                        Status = ChangeRequestStatusEnum.Implemented
                    });
        }
    }
}
