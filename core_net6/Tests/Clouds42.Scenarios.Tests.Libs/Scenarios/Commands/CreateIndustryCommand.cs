﻿using System;
using Clouds42.AgencyAgreement.Industry;
using Clouds42.Domain.IDataModels;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Комманда создания отрасли
    /// </summary>
    public class CreateIndustryCommand(
        ITestContext testContext,
        IndustryModelTest industry = null)
        : CommandBase(testContext), IIndustry
    {
        private readonly IndustryModelTest _industry = industry ?? new IndustryModelTest();

        public override void Run()
        {
            var industryManager = TestContext.ServiceProvider.GetRequiredService<IndustryManager>();
            var createIndustryResult = industryManager.AddNewIndustry(_industry);

            if (createIndustryResult.Error)
                throw new InvalidOperationException($"Не удалось создать отрасль '{createIndustryResult.Message}'");

            var industry = TestContext.DbLayer.IndustryRepository.FirstOrDefault(w => w.Name == _industry.Name);

            if (industry == null)
                throw new InvalidOperationException("Создание отрасли завершилось ошибкой");

            Id = industry.Id;
            Name = _industry.Name;
            Description = _industry.Description;
        }

        /// <summary>
        /// Id отрасли
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название отрасли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание отрасли
        /// </summary>
        public string Description { get; set; }
    }
}
