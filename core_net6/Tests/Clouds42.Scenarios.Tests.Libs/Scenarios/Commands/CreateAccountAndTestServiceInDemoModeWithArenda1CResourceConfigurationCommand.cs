﻿using Clouds42.Billing.Billing.Providers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using System;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда для регистрации тестового сервиса в демо режиме и Аренды 1С в ResourceConfigurations
    /// </summary>
    public class CreateAccountAndTestServiceInDemoModeWithArenda1CResourceConfigurationCommand(
        ITestContext testContext,
        decimal testServiceCost,
        DateTime testServiceExpiredDate,
        bool isAutoSubscriptionEnabled)
        : CommandBase(testContext)
    {
        public Guid AccountId { get;private set; }
        public Guid TestServiceId { get; private set; }

        public override void Run()
        {
            AccountId = CreateAccount();
            TestServiceId = CreateService();
            RegisterServiceConfigurations(AccountId, TestServiceId);
        }

        /// <summary>
        /// Зарегестрировать тестовый сервис
        /// </summary>
        /// <returns>ID сервиса</returns>
        private Guid CreateService()
        {
            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommand.Run();
            return createBillingServiceCommand.Id;
        }


        /// <summary>
        /// Зарегестрировать аккаунт
        /// </summary>
        /// <returns>ID аккаунта</returns>
        private Guid CreateAccount()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            return createAccountDatabaseAndAccountCommand.AccountId;
        }

        /// <summary>
        /// Регистрация русурсов конфигурации
        /// </summary>
        /// <param name="accountId">На какой аккаунт</param>
        /// <param name="testServiceId">На какой сервис</param>
        private void RegisterServiceConfigurations(Guid accountId, Guid testServiceId)
        {
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var myEnterpriseResourceConfiguration = new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                BillingServiceId = billingService.Id,
                Cost = 5000,
                ExpireDate = DateTime.Now.AddDays(30),
                Frozen = false
            };

            var testServiceResourceConfiguration = new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Cost = testServiceCost,
                IsDemoPeriod = true,
                ExpireDate = testServiceExpiredDate,
                CreateDate = DateTime.Now,
                Frozen = false,
                IsAutoSubscriptionEnabled = isAutoSubscriptionEnabled,
                BillingServiceId = testServiceId,
            };

            TestContext.DbLayer.ResourceConfigurationRepository.InsertRange(new[] { testServiceResourceConfiguration, myEnterpriseResourceConfiguration });

            TestContext.DbLayer.Save();
        }
    }
}
