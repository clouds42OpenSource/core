﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания терминального сервера
    /// </summary>
    public class CreateCloudTerminalServersCommand(
        ITestContext testContext,
        CreateCloudTerminalServerModelTest data = null)
        : CommandBase(testContext), IId
    {
        private readonly CreateCloudTerminalServerModelTest _data = data ?? new CreateCloudTerminalServerModelTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudTerminalServerManager>();
            var managerResult = manager.CreateCloudTerminalServer(_data);
            Assert.IsFalse(managerResult.Error);

            Id = managerResult.Result;
        }

        public Guid Id { get; private set;}

    }
}