﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountandGroupInActiveDirectoryCommand(
        AccountRegistrationModelTest accountData = null,
        string groupName = null)
    {
        private readonly AccountRegistrationModelTest _accountData = accountData ?? new AccountRegistrationModelTest();
        private readonly string _groupName = groupName ?? "_testGroup";

        public void Run()
        {
            ActiveDirectoryUserFunctions.CreateUser(_accountData);
            
            // проверка есть ли такой юзер в Active Directory
            var user = ActiveDirectoryUserFunctions.UsersExistsInDomain(_accountData.Login);
            if (!user)
                throw new InvalidOperationException ("Тестовый аккаунт в AD не найден");

            ActiveDirectoryCompanyFunctions.CreateGroups(_groupName);
            
            var groupExists = ActiveDirectoryCompanyFunctions.GroupExists(_groupName);
            if (!groupExists)
                throw new InvalidOperationException ("Группы нет в домене");

        }

    }
}
