﻿using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountDatabaseAndAccountCommand : CommandBase, IAccountDatabseDetails
    {
        private InfoDatabaseDomainModelTest _databaseData;
        private readonly AccountRegistrationModelTest _accountModel;
        private readonly bool _dbOnDelimiters;


        public CreateAccountDatabaseAndAccountCommand(ITestContext testContext,
            InfoDatabaseDomainModelTest databaseData = null, AccountRegistrationModelTest accountModel = null,
            bool dbOnDelimiter = false) : base(testContext)
        {
            _databaseData = databaseData ?? new InfoDatabaseDomainModelTest(TestContext);
            _accountModel = accountModel ?? new AccountRegistrationModelTest();
            _dbOnDelimiters = dbOnDelimiter;
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, _accountModel);
            createAccountCommand.Run();

            if (_dbOnDelimiters)
            {
                var dbTemplateModelTest = new DbTemplateModelTest(TestContext.DbLayer)
                {
                    Name = DbTemplatesNameConstants.BpName,
                    DefaultCaption = "Бухгалтерия предприятия 3.0"
                };
                
                var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext);
                sourceAccountDatabaseCommand.Run();

                _databaseData = new InfoDatabaseDomainModelTest(TestContext, dbTemplateModelTest)
                {
                    DbTemplateDelimiters = true,
                    DataBaseName = "Тестовая база на разделителях"
                };

                var createBaseCommand = new CreateAccountDatabaseViaDelimiterCommand(TestContext, createAccountCommand, _databaseData);
                createBaseCommand.Run();

                AccountDatabaseId = createBaseCommand.AccountDatabaseId;
                AccountDetails = createAccountCommand;
            }
            else
            {
                var createBaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, _databaseData);
                createBaseCommand.Run();
                AccountDatabaseId = createBaseCommand.AccountDatabaseId;
                AccountDetails = createAccountCommand;
            }
        }

        public Guid AccountDatabaseId { get; private set; }
        public IAccountDetails AccountDetails { get; private set; }

    }
}