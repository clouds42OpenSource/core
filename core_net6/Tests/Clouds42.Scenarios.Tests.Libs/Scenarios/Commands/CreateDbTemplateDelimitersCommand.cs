﻿using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.DataContracts.Account.Registration;
using System;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания шаблона инф. базы на разделителях
    /// </summary>
    public class CreateDbTemplateDelimitersCommand : CommandBase, IAccountDatabseDetails
    {
        private readonly DbTemplateDelimitersModelTest _dbTemplateDelimiters;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public CreateDbTemplateDelimitersCommand(ITestContext testContext,
            DbTemplateDelimitersModelTest dbTemplateDelimiters = null) : base(testContext)
        {
            _dbTemplateDelimiters = dbTemplateDelimiters ?? new DbTemplateDelimitersModelTest(TestContext);
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        /// <summary>
        /// Выполнить команду
        /// </summary>
        public override void Run()
        {
            var sourceAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Source Test account",
                Login = $"SourceTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SourceTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto { CloudService = Clouds42Service.MyEnterprise }
            };
            var createAccountCommand = new CreateAccountCommand(
                TestContext,
                sourceAccount);
            createAccountCommand.Run();

            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<DbTemplatesManager>()
                .GetDbTemplateById(_dbTemplateDelimiters.TemplateId).Result;

            if (dbTemplate == null)
            {
                dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(TestContext.DbLayer)
                {
                    Name = DbTemplatesNameConstants.BpName,
                    DefaultCaption = "Бухгалтерия предприятия 3.0"
                });

                _dbTemplateDelimiters.TemplateId = dbTemplate.Id;
            }
            
            var databaseDomainModelTest = new InfoDatabaseDomainModelTest(TestContext, dbTemplate);

            var sourceAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, databaseDomainModelTest);
            sourceAccountDatabaseCommand.Run();

            //добавляем в справочник баз на разделителях
            var dbTemplateDelimiters = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            dbTemplateDelimiters.AddDbTemplateDelimiterItem(_dbTemplateDelimiters);

            AccountDatabaseId = sourceAccountDatabaseCommand.AccountDatabaseId;
            AccountDetails = sourceAccountDatabaseCommand.AccountDetails;
        }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; private set; }

        /// <summary>
        /// Данные об аккаунте
        /// </summary>
        public IAccountDetails AccountDetails { get; private set; }
    }
}