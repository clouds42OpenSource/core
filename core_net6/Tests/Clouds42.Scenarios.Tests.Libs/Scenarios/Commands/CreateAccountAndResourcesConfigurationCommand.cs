﻿using System;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using CommonLib.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Создаёт тестовый аккаунт с сервисами для аккаунта (ResourceConfiguration и Resource)
    /// </summary>
    public class CreateAccountAndResourcesConfigurationCommand(ITestContext testContext, bool isFrozenService = false)
        : CommandBase(testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.DbLayer;

        public Guid AccountId;
        public CalculationOfInvoiceRequestModelTest InvoiceRequest;

        public override void Run()
        {
            InvoiceRequest =
                new CalculationOfInvoiceRequestModelTest();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            AccountId = createAccountCommand.AccountId;

            var daysToAdd = isFrozenService ? -5 : 5;

            var expireDate = DateTime.Today.AddDays(daysToAdd);

            var billingService = new BillingServiceDataProvider(_dbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var resourcesConfig = new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = createAccountCommand.AccountId,
                BillingServiceId = billingService.Id,
                Cost = 5000,
                ExpireDate = expireDate,
                Frozen = isFrozenService
            };

            var serviceType = new BillingServiceDataProvider(_dbLayer).GetSystemServiceTypeOrThrowException(ResourceType.MyEntUser);
            var resource = new Resource
            {
                Id = Guid.NewGuid(),                
                Cost = 5000,
                AccountId = AccountId,
                BillingServiceTypeId = serviceType.Id,
            };

            _dbLayer.ResourceRepository.Insert(resource);
            _dbLayer.ResourceConfigurationRepository.Insert(resourcesConfig);
            _dbLayer.Save();
        }
    }
}
