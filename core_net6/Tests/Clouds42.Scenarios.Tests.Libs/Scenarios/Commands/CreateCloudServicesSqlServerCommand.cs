﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesSQLServer.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания SQL хранилища
    /// </summary>
    public class CreateCloudServicesSqlServerCommand(
        ITestContext testContext,
        CreateCloudSqlServerModelTest data = null)
        : CommandBase(testContext), IId
    {
        private readonly CreateCloudSqlServerModelTest _data = data ?? new CreateCloudSqlServerModelTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudSqlServerManager>();
            var managerResult = manager.CreateCloudSqlServer(_data);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            Id = managerResult.Result;
        }

        public Guid Id { get; private set;}

    }
}