﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppCommon.Access;
using AppCommon.DataManagers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Repositories;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateCloudSegmentCommand : CommandBase, ICreateCloudSegmentDetails
    {
        private readonly CreateCloudSegmentModelTest _segment;

        public CreateCloudSegmentCommand(ITestContext testContext, 
            CreateCloudSegmentModelTest segmentModel = null) : base(testContext)
        {
            _segment = segmentModel ?? new CreateCloudSegmentModelTest();
        }

        public override void Run()
        {
            var manager = new CloudSegmentManager(TestContext.AccessProvider, TestContext.DbLayer);

            manager.AddSegment(_segment);

            var segmentDb = TestContext.DbLayer.CloudServicesSegmentRepository.FirstOrDefault(x => x.ID == _segment.ID);

            if (segmentDb == null)
                throw new Exception("Fail to create the segment");

            Id = segmentDb.ID;
            Name = segmentDb.Name;
            Description = segmentDb.Description;
            ContentServerId = segmentDb.ContentServerID;
            ServicesTerminalFarmId = segmentDb.ServicesTerminalFarmID;
            EnterpriseServer82Id = segmentDb.EnterpriseServer82ID;
            EnterpriseServer83Id = segmentDb.EnterpriseServer83ID;
            FileStorageServersId = segmentDb.FileStorageServersID.Value;
            SqlServerId = segmentDb.SQLServerID;
            BackupStorageId = segmentDb.BackupStorageID;
        }

        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public Guid ContentServerId { get; private set; }
        public Guid ServicesTerminalFarmId { get; private set; }
        public Guid EnterpriseServer82Id { get; private set; }
        public Guid EnterpriseServer83Id { get; private set; }
        public Guid FileStorageServersId { get; private set; }
        public Guid SqlServerId { get; private set; }
        public Guid BackupStorageId { get; private set; }
    }
}
