﻿using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.DataContracts.Account.Registration;
using System;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Common.Encrypt;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания пустого сервиса биллинга
    /// </summary>
    public class CreateBillingServiceCommand : CommandBase, IId
    {
        private readonly bool _needActivateService;
        public Lazy<CreateBillingServiceTest> CreateBillingServiceTest;

        public ManagerResult Result { get; private set; }

        /// <summary>
        /// Результат обработки модерации сервиса
        /// </summary>
        public ManagerResult ProcessModerationResult { get; private set; }

        public CreateBillingServiceCommand(ITestContext testContext,
            Func<Guid, CreateBillingServiceTest> modelCreator = null, bool needActivateService = true) : base(
            testContext)
        {
            _needActivateService = needActivateService;
            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());

            CreateBillingServiceTest = new Lazy<CreateBillingServiceTest>(() =>
            {
                var createIndustry = new CreateIndustryCommand(testContext);
                createIndustry.Run();

                var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(TestContext.DbLayer)
                {
                    Name = DbTemplatesNameConstants.BpName,
                    DefaultCaption = "Бухгалтерия предприятия 3.0"
                });

                var dbTemplateDelimiterManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
                dbTemplateDelimiterManager.AddDbTemplateDelimiterItem(new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id));

                var random = new Random().Next(1, 99999999);
                var serviceOwnerAccount = new CreateAccountCommand(testContext, new AccountRegistrationModelTest
                {
                    AccountCaption = $"Test account{random}",
                    Login = $"TestLogin{random}",
                    Password = alphaNumeric42CloudsGenerator.GeneratePassword(),
                    Email = $"TestEmail{random}@efsol.ru",
                    FirstName = $"TestFirstName{random}",
                    Inn = "1234567890",
                    FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                    RegistrationConfig = new RegistrationConfigDomainModelDto()
                });
                serviceOwnerAccount.Run();

                var model = modelCreator != null ? modelCreator(serviceOwnerAccount.AccountId) : new CreateBillingServiceTest(testContext, serviceOwnerAccount.AccountId);
                model.AccountId = serviceOwnerAccount.AccountId;
                model.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

                return model;
            });
        }

        /// <summary>
        /// Выполнить комманду
        /// </summary>
        public override void Run()
        {
            var createEmptyBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext, CreateBillingServiceTest.Value, _needActivateService);
            createEmptyBillingServiceCommand.Run();

            Id = createEmptyBillingServiceCommand.Id;
            Result = createEmptyBillingServiceCommand.Result;
            ProcessModerationResult = createEmptyBillingServiceCommand.ProcessModerationResult;
        }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; private set; }
    }
}
