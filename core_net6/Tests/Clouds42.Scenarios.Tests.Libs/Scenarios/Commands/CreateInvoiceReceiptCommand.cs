﻿using System;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateInvoiceReceiptCommand(ITestContext testContext, Guid invoiceId) : CommandBase(testContext), IId
    {
        public Guid Id { get; } = invoiceId;

        public override void Run()
        {
            var invoiceReceipt = new Domain.DataModels.billing.InvoiceReceipt
            {
                InvoiceId = Id,
                DocumentDateTime = DateTime.Now,
                FiscalSign = "3837082083",
                FiscalSerial = "9286000100051786",
                FiscalNumber = "7",
                QrCodeFormat = "Jpg",
                ReceiptInfo = "КАССОВЫЙ ЧЕК/ПРИХОД\t16-05-18 10:25\n" +
                              "ООО \"УЧЕТОНЛАЙН\"\n" +
                              "Россия, город Москва, улица Шарикоподшипниковская, дом 11, строение 9\n" +
                              "МЕСТО РАСЧЕТОВ\thttps://42clouds.com\n" +
                              "КАССИР: КАССИР 1\tИНН: 7721643307\n" +
                              "ЧЕК: 1\tСМЕНА: 2\n" +
                              "СНО\tУСН ДОХОД\n" +
                              "Предоставление права для использования лицензионного обеспечения\n" +
                              "ПРЕДОПЛАТА 100%\t ~5.00\n" +
                              "##BIG##ИТОГ\t~5.00\n" +
                              "ВСЕГО ПОЛУЧЕНО\t~5.00\n" +
                              "ЭЛЕКТРОННЫМИ\t~5.00\n" +
                              "СУММА БЕЗ НДС\t~5.00\n" +
                              "ККТ ДЛЯ ИНТЕРНЕТ\n" +
                              "САЙТ ФНС\twww.nalog.ru\n" +
                              "ЭЛ.АДР.ПОКУПАТЕЛЯ\tafinder@bk.ru\n" +
                              "РН ККТ: 0001802922062565\tФН: 9286000100051786\n" +
                              "ФД: 7\tФП: 3837082083",
                QrCodeData = "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHB"
            };

            TestContext.DbLayer.InvoiceReceiptRepository.Insert(invoiceReceipt);
            TestContext.DbLayer.Save();
        }
    }
}