﻿using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountCommand(
        ITestContext testContext,
        AccountRegistrationModelTest accountData = null)
        : CommandBase(testContext), IAccountDetails
    {
        private readonly AccountRegistrationModelTest _accountData = accountData ?? new AccountRegistrationModelTest();

        public override void Run()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext, defaultSegment: true);
            createSegmentCommand.Run();

            _accountData.SkipOpenIdCreating = true;
            var accountsManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountManager>();
            var managerResult = accountsManager.AddAccountAndCreateSessionAsync(_accountData).Result;
            var authInfo = managerResult.Result;

            if (authInfo?.AccountId == null)
                throw new InvalidOperationException ($"Не удалось создать тестовый аккаунт {managerResult.Message}");

            AccountId = authInfo.AccountId.Value;

            var accountAdmin = TestContext.DbLayer.AccountUsersRepository.FirstOrDefault(u =>
                u.AccountId == AccountId &&
                u.AccountUserRoles.Any(a => a.AccountUserGroup == AccountUserGroup.AccountAdmin)) ?? throw new InvalidOperationException ("Не удалось создать аккаунт админа");
            
            AccountAdminId = accountAdmin.Id;

            if (authInfo.Token == null)
                throw new InvalidOperationException ("Не удалось получить токен авторизации");

            var account = TestContext.DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == AccountId);

            Account = account;

            var accountSegmentHelper = TestContext.ServiceProvider.GetRequiredService<AccountSegmentHelper>();

            var accountFileStoragePath = accountSegmentHelper.GetAccountFileStoragePath(Account);
            CreateFolder(accountFileStoragePath);

            var fullClientFileStoragePath = accountSegmentHelper.GetFullClientFileStoragePath(AccountId);
            CreateFolder(fullClientFileStoragePath);

            if (account == null)
                throw new InvalidOperationException ("Не удалось создать аккаунт");
            Token = authInfo.Token.Value;
            Inn = account.AccountRequisites.Inn;
            Segment = createSegmentCommand;
            AccessProvider = new AccountTestAccessProvider(this, TestContext.DbLayer, TestContext.Logger,
                TestContext.Configurations, TestContext.AccessMapping,TestContext.HandlerException);
        }

        public Account Account { get; private set; }
        public Guid AccountId { get; private set; }
        public Guid AccountAdminId { get; private set; }
        public Guid Token { get; private set; }
        public string Inn { get; private set; }
        public ISegmentDetails Segment { get; private set; }
        public IAccessProvider AccessProvider { get; private set; }
        public List<AccountUserGroup> Groups { get; set; } = [AccountUserGroup.AccountAdmin];

        /// <summary>
        /// Создать директорию.
        /// </summary>
        /// <param name="path">Путь директории.</param>
        private static void CreateFolder(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}
