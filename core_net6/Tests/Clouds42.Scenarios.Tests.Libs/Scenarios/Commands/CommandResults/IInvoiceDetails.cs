﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults
{
    public interface IInvoiceDetails
    {
        Guid InvoiceId { get; }
        long UniqKey { get; }
        IAccountDetails Account { get; }
        IAccessProvider AccessProvider { get; }
    }
}