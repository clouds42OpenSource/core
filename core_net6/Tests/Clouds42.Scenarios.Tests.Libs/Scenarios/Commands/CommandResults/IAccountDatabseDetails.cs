﻿using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults
{
    public interface IAccountDatabseDetails
    {
        Guid AccountDatabaseId { get; }
        IAccountDetails AccountDetails { get; }
    }
}