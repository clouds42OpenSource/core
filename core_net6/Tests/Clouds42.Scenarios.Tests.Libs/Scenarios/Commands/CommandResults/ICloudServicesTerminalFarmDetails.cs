﻿using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults
{
    public interface IId
    {
        Guid Id { get; }
    }

    public interface ISegmentDetails : IId
    {

    }

}