﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults
{
    public interface IAccountDetails
    {
        Guid AccountId { get; }
        Guid AccountAdminId { get; }
        Guid Token { get; }
        string Inn { get; }

        Account Account { get; }

        ISegmentDetails Segment { get; }

        IAccessProvider AccessProvider { get; }
    }
}