﻿using System;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Scenarios.Tests.Libs.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountInActiveDirectoryCommand(AccountRegistrationModelTest accountData = null)
    {
        private readonly AccountRegistrationModelTest _accountData = accountData ?? new AccountRegistrationModelTest();

        public void Run()
        {
            ActiveDirectoryUserFunctions.CreateUser(_accountData);
            
            // проверка есть ли такой юзер в Active Directory
            var user = ActiveDirectoryUserFunctions.UsersExistsInDomain(_accountData.Login);
            if (!user)
                throw new InvalidOperationException ("Тестовый аккаунт в AD не найден");
        }

    }
}
