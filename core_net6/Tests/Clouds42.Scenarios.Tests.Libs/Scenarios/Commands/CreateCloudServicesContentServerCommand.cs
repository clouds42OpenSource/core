﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesContentServer.Managers;
using Clouds42.Segment.PublishNode.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания сервера публикаций
    /// </summary>
    public class CreateCloudServicesContentServerCommand(
        ITestContext testContext,
        CloudContentServerElementViewModelTest data = null,
        PublishNodeDtoTest publishNodeDtoTest = null)
        : CommandBase(testContext), IId
    {
        private readonly CloudContentServerElementViewModelTest _data = data ?? new CloudContentServerElementViewModelTest();
        private readonly PublishNodeDtoTest _nodeReference = publishNodeDtoTest ?? new PublishNodeDtoTest();

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudContentServerManager>();
            var publishNodesManager = TestContext.ServiceProvider.GetRequiredService<PublishNodesManager>();
            
            var result = publishNodesManager.AddNewPublishNode(_nodeReference);
            Assert.IsFalse(result.Error, result.Message);

            var publishNodeReference =
                TestContext.DbLayer.PublishNodeReferenceRepository.FirstOrDefault(pnr => pnr.ID == result.Result);
            
            _data.PublishNodes =
            [
                new PublishNodeDto
                {
                    Id = publishNodeReference.ID,
                    Address = publishNodeReference.Address,
                    Description = publishNodeReference.Description
                }
            ];
            
            var managerResult = manager.CreateCloudContentServer(_data);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            Id = managerResult.Result;
        }

        public Guid Id { get; private set;}

    }
}
