﻿using System;
using System.IO;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания файлового хранилища
    /// </summary>
    public class CreateCloudServicesFileStorageServersCommand : CommandBase, IId
    {
        private readonly CloudFilestorageElementViewModelTest _data;

        public CreateCloudServicesFileStorageServersCommand(ITestContext testContext,
            CloudFilestorageElementViewModelTest data = null) : base(testContext)
        {
            var defaultStorage = Path.Combine(testContext.TestDataPath, "DefaultStorage");

            if (!Directory.Exists(defaultStorage))
                Directory.CreateDirectory(defaultStorage);

            _data = data ?? new CloudFilestorageElementViewModelTest(defaultStorage);
        }

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<ICloudFileStorageServerManager>();
            var result = manager.CreateCloudFileStorageServer(_data);
            Assert.IsFalse(result.Error, result.Message);

            if (result.Error)
                throw new InvalidOperationException (result.Message);

            Id = result.Result;
        }

        public Guid Id { get; private set;}

    }
}
