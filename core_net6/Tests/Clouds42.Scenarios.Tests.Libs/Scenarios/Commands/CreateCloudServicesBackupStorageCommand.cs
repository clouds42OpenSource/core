﻿using System;
using System.IO;
using Clouds42.Segment.CloudServicesBackupStorage.Managers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания бэкап хранилища
    /// </summary>
    public class CreateCloudServicesBackupStorageCommand : CommandBase, IId
    {
        private readonly CloudBackupElementViewModelTest _data;

        public CreateCloudServicesBackupStorageCommand(ITestContext testContext,
            CloudBackupElementViewModelTest data = null) : base(testContext)
        {
            _data = data ?? new CloudBackupElementViewModelTest
            {
                ConnectionAddress = Path.Combine(TestContext.TestDataPath, "BackupStorage")
            };
        }

        public override void Run()
        {
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudBackupStorageManager>();
            var managerResult = manager.CreateCloudBackupStorage(_data);
            Assert.IsFalse(managerResult.Error);

            Id = managerResult.Result;
        }

        public Guid Id { get; private set;}

    }
}