﻿using System.Threading.Tasks;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Test.Runner;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public abstract class CommandBase(ITestContext testContext) : ICommand
    {
        public abstract void Run();

        public virtual async Task RunAsync()
        {
        }

        protected ITestContext TestContext = testContext;
    }
}
