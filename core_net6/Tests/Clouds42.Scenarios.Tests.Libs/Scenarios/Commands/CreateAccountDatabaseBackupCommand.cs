﻿using System;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    public class CreateAccountDatabaseBackupCommand(
        ITestContext testContext,
        IAccountDatabseDetails accountDatabseDetails)
        : CommandBase(testContext), IId
    {
        public override void Run()
        {
            var segmentHelper = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>();
            var backUpManager =  new AccountDatabaseLocalBackupProvider(TestContext.DbLayer, segmentHelper, TestContext.Logger, TestContext.HandlerException);

            var accountDatabase =
                TestContext.DbLayer.DatabasesRepository.FirstOrDefault(d =>
                    d.Id == accountDatabseDetails.AccountDatabaseId);

            if (accountDatabase == null)
                throw  new Exception($"Не удалось прлучить данные о базе по номеру {accountDatabseDetails.AccountDatabaseId} ");

            Id = backUpManager.CreateBackupAccountDatabase(accountDatabase, CreateBackupAccountDatabaseTrigger.FromApi,
                TestContext.AccessProvider.GetUser().Id);
        }

        public Guid Id { get; private set; }
    }
}
