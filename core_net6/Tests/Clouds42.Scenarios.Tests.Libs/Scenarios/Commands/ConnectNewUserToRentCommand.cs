﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{

    /// <summary>
    /// Команда подключения пользователя к аренде.
    /// </summary>
    public class ConnectNewUserToRentCommand : CommandBase
    {
        private readonly IAccountDetails _account;
        private readonly AccountUserDcTest _userInfo;

        public ConnectNewUserToRentCommand(ITestContext testContext,
            IAccountDetails account, AccountUserDcTest userInfo = null) : base(testContext)
        {
            _account = account;
            _userInfo = userInfo ?? new AccountUserDcTest();
            _userInfo.AccountId = account.AccountId;
        }

        public override void Run()
        {
            var accountUserId = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>()
                .AddToAccount(_userInfo).Result;

            var rent1CAccessHelper = TestContext.ServiceProvider.GetRequiredService<IRent1CAccessHelper>();
            var resourcesConfiguration = TestContext.ServiceProvider.GetRequiredService<IResourcesService>().GetResourceConfig(_userInfo.AccountId, Clouds42Service.MyEnterprise);
            var ren1CInfo = rent1CAccessHelper.GetAccountBillingDataForBuyRent1C(_account.Account, resourcesConfiguration.ExpireDateValue);
            var billingService = new Billing.Billing.Providers.BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = _account.AccountId,
                Date = DateTime.Now,
                Description = "Подключение аренды",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = ren1CInfo.PartialCostOfStandartLicense,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                Id = Guid.NewGuid()
            });

            var rent1CService = TestContext.ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(_account.AccountId);
            rent1CService.ConfigureAccesses([
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);
        }
    }
}