﻿using System;
using System.Linq;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Commands
{
    /// <summary>
    /// Команда создания сегмента
    /// </summary>
    public class CreateSegmentCommand(
        ITestContext testContext,
        bool defaultSegment = false,
        CloudFilestorageElementViewModelTest fileStorageData = null,
        CoreHostingDto CoreHostingDto = null,
        string v83PlatformVersion = null,
        PublishNodeDtoTest publishNodeDtoTest = null)
        : CommandBase(testContext), ISegmentDetails
    {
        private readonly Random _random = new();

        public override void Run()
        {
            var cloudEnterpriseElementViewModelTest = new CloudEnterpriseElementViewModelTest
            {
                Version = PlatformType.V82.ToString(),
                Name = $"ent1c82-prod-1-{DateTime.Now:mm-ss-fff}-{_random.Next(0,10000)}",
                ConnectionAddress = $"ent1c82-prod--{DateTime.Now:mm-ss-fff}-{_random.Next(0,1000)}"
            };

            var dbLayer = TestContext.DbLayer;

            var create82VersionCommand = dbLayer.PlatformVersionReferencesRepository.Where(w => w.Version.StartsWith("8.2")).Last();

            var create83VersionCommand = dbLayer.PlatformVersionReferencesRepository.Where(w => w.Version.StartsWith("8.3") && w.PathToPlatfromX64 != null).Last();
            
            var alpha83Version = dbLayer.PlatformVersionReferencesRepository.Where(w => w.Version.StartsWith("8.3") && w.PathToPlatfromX64 != null).Last();

            var createCloudServicesTerminalFarmCommand = new CreateCloudServicesTerminalFarmCommand(TestContext);
            createCloudServicesTerminalFarmCommand.Run();

            var createCloudServicesEnterpriseServer82Command = new CreateCloudServicesEnterpriseServerCommand(TestContext, cloudEnterpriseElementViewModelTest);
            createCloudServicesEnterpriseServer82Command.Run();

            var createCloudServicesEnterpriseServer83Command = new CreateCloudServicesEnterpriseServerCommand(TestContext);
            createCloudServicesEnterpriseServer83Command.Run();

            var createCloudServicesContentServerCommand =
                new CreateCloudServicesContentServerCommand(TestContext, publishNodeDtoTest: publishNodeDtoTest);
            createCloudServicesContentServerCommand.Run();

            var createCloudServicesFileStorageServersCommand = new CreateCloudServicesFileStorageServersCommand(TestContext, fileStorageData);
            createCloudServicesFileStorageServersCommand.Run();

            var createCloudServicesSqlServerCommand = new CreateCloudServicesSqlServerCommand(TestContext);
            createCloudServicesSqlServerCommand.Run();

            var createCloudServicesBackupStorageCommand = new CreateCloudServicesBackupStorageCommand(TestContext);
            createCloudServicesBackupStorageCommand.Run();

            var createCloudServicesGatewayTerminalsCommand = new CreateCloudServicesGatewayTerminalsCommand(TestContext);
            createCloudServicesGatewayTerminalsCommand.Run();

            var createCloudTerminalServersCommand = new CreateCloudTerminalServersCommand(TestContext);
            createCloudTerminalServersCommand.Run();

            var createHelper = new CreateAccountTestHelper(TestContext.DbLayer, TestContext);

            var model = new CreateCloudServicesSegmentDto
            {
                Name = $"Test segment{Guid.NewGuid():N}",
                Description = "Test segment",
                EnterpriseServer82ID = createCloudServicesEnterpriseServer82Command.Id,
                EnterpriseServer83ID = createCloudServicesEnterpriseServer83Command.Id,
                SQLServerID = createCloudServicesSqlServerCommand.Id,
                ServicesTerminalFarmID = createCloudServicesTerminalFarmCommand.Id,
                BackupStorageID = createCloudServicesBackupStorageCommand.Id,
                FileStorageServersID = createCloudServicesFileStorageServersCommand.Id,
                ContentServerID = createCloudServicesContentServerCommand.Id,
                GatewayTerminalsID = createCloudServicesGatewayTerminalsCommand.Id,
                IsDefault = true,
                Stable82VersionId = create82VersionCommand.Version,
                Stable83VersionId = v83PlatformVersion ?? create83VersionCommand.Version,
                CoreHostingId =CoreHostingDto?.Id ?? createHelper.GetDefaultCoreHosting().Id,
                Alpha83VersionId = alpha83Version.Version
            };
            var manager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();

            var result = manager.AddNewSegment(model);
            if (result.Error)
                throw new InvalidOperationException (result.Message);

            var segment = TestContext.DbLayer.CloudServicesSegmentRepository.FirstOrDefault(s => s.Name == model.Name);

            if (segment == null)
                throw new InvalidOperationException ("Не удалось создать сегмент");

            segment.IsDefault = defaultSegment;
            TestContext.DbLayer.CloudServicesSegmentRepository.Update(segment);
            TestContext.DbLayer.Save();

            Id = segment.ID;
        }

        public Guid Id { get; private set; }
    }
}
