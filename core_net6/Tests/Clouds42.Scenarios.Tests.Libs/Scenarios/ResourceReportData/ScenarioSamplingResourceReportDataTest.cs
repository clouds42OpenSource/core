﻿using Clouds42.Billing;
using Clouds42.Resources.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourceReportData
{
    /// <summary>
    /// Сценарий теста выборки списка ресурсов для отчета
    /// Действия:
    ///     1) Создаем записи ресурсов
    ///     2) Делаем выборку
    ///     3) Сравниваем количество данных
    /// </summary>
    public class ScenarioSamplingResourceReportDataTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        private readonly ResourceReportDataManager _resourceReportDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioSamplingResourceReportDataTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            _resourceReportDataManager = TestContext.ServiceProvider.GetRequiredService<ResourceReportDataManager>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createAccountCommand.AccountId);
            var accountAdminId = createAccountCommand.AccountAdminId;
            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var saleManagerAccount = _testDataGenerator.GenerateAccount();
            var saleManagerUser = _testDataGenerator.GenerateAccountUser(saleManagerAccount.Id);

            var saleManager =
                _testDataGenerator.GenerateAccountSaleManager(createAccountCommand.AccountId, saleManagerUser.Id,
                    "ОВР");

            DbLayer.AccountsRepository.Insert(saleManagerAccount);
            DbLayer.AccountUsersRepository.Insert(saleManagerUser);
            DbLayer.AccountSaleManagerRepository.Insert(saleManager);
            DbLayer.Save();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(serv => serv.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id, accountAdminId);

            var accountResources = DbLayer.ResourceRepository
                .Where(res => res.AccountId == account.Id &&
                              (res.Subject != null && res.Cost > 0 || res.BillingServiceType.Service.SystemService ==
                                  Clouds42Service.Recognition ||
                              res.BillingServiceType.Service.SystemService == Clouds42Service.Esdl)).ToList();
            Assert.IsNotNull(accountResources);

            var resourceReport = _resourceReportDataManager.GetResourceReportDataDcs();
            Assert.IsNotNull(resourceReport);

            Assert.AreEqual(accountResources.Count, resourceReport.Result.Count);

            var reportWebResources = resourceReport.Result.Where(w => w.LicenseName == "WEB").ToList();
            var reportStandartResources = resourceReport.Result.Where(w => w.LicenseName == "Стандарт").ToList();

            const int rent1CResourcesCount = 1;
            Assert.AreEqual(reportWebResources.Count, rent1CResourcesCount);
            Assert.AreEqual(reportStandartResources.Count, rent1CResourcesCount);
            
            var resourcesConfigurations = DbLayer.ResourceConfigurationRepository.Where(rc => rc.BillingServiceId == service.Id);

            foreach (var resConf in resourcesConfigurations)
            {
                resConf.ExpireDate = DateTime.Now.AddYears(-3);
                DbLayer.ResourceConfigurationRepository.Update(resConf);
            }
            DbLayer.Save();
            accountResources = DbLayer.ResourceRepository.Where(resource => resource.AccountId == account.Id && 
                                                                            resource.BillingServiceType.Service.SystemService != null &&
                                                                            (resource.Subject != null && 
                                                                                resource.Cost > 0 || 
                                                                                resource.BillingServiceType.Service.SystemService == Clouds42Service.Recognition ||  
                                                                                resource.BillingServiceType.Service.SystemService == Clouds42Service.Esdl)).ToList();
            Assert.IsNotNull(accountResources);

            resourceReport = _resourceReportDataManager.GetResourceReportDataDcs();
            Assert.IsNotNull(resourceReport);

            Assert.AreEqual(accountResources.Count, resourceReport.Result.Count);

            if (resourceReport.Result.Any(rr => string.IsNullOrEmpty(rr.SaleManagerFullName) || string.IsNullOrEmpty(rr.SaleManagerDivision)))
                throw new InvalidOperationException("В записях ресурсов у аккаунта должен быть сейл менеджер");
        }
    }
}
