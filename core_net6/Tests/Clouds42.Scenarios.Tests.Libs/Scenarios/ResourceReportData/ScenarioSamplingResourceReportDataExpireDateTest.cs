﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Resources.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourceReportData
{
    /// <summary>
    /// Сценарий теста выборки списка ресурсов для отчета по датам
    /// Действия:
    ///     1) Создаем записи ресурсов
    ///     2) Делаем выборку
    ///     3) Сравниваем количество данных
    ///     4) Изменеяем ExpireDate у ресурса конфигурации на null
    ///     5) Делаем выборку с сравниваем количество данных
    ///     6) Изменяем ExpireDate у ресурса конфигурации на истекшую
    ///     7) Делаем выборку с сравниваем количество данных
    /// </summary>
    public class ScenarioSamplingResourceReportDataExpireDateTest : ScenarioBase
    {
        private readonly ResourceReportDataManager _resourceReportDataManager;

        public ScenarioSamplingResourceReportDataExpireDateTest()
        {
            _resourceReportDataManager = TestContext.ServiceProvider.GetRequiredService<ResourceReportDataManager>();
        }

        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });
            createBillingServiceCommand.Run();

            var secondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestLogin{DateTime.Now:mmss}",
                Email = "testemail22@efsol.ru",
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1),
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createBillingServiceCommand.Id
                }
            });
            secondAccountCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == createBillingServiceCommand.Id);

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == secondAccountCommand.AccountAdminId);

            var resourceReport = _resourceReportDataManager.GetResourceReportDataDcs();
            Assert.IsTrue(resourceReport.Result.Any(r => r.BillingServiceName == service.Name && r.AccountUserLogin == accountUser.Login));

            var resconfs = DbLayer.ResourceConfigurationRepository.Where(rc =>
                rc.AccountId == secondAccountCommand.AccountId);

            foreach (var resconf in resconfs)
            {
                resconf.ExpireDate = null;
                DbLayer.ResourceConfigurationRepository.Update(resconf);
            }
            DbLayer.Save();

            resourceReport = _resourceReportDataManager.GetResourceReportDataDcs();
            Assert.IsTrue(resourceReport.Result.Any(r => r.BillingServiceName == service.Name && r.AccountUserLogin == accountUser.Login));

            resconfs = DbLayer.ResourceConfigurationRepository.Where(rc =>
                rc.AccountId == secondAccountCommand.AccountId);

            foreach (var resconf in resconfs)
            {
                resconf.ExpireDate = DateTime.Now.AddYears(-3);
                DbLayer.ResourceConfigurationRepository.Update(resconf);
            }
            DbLayer.Save();

            resourceReport = _resourceReportDataManager.GetResourceReportDataDcs();
            Assert.IsFalse(resourceReport.Result.Any(r => r.BillingServiceName == service.Name && r.AccountUserLogin == accountUser.Login));
        }
    }
}