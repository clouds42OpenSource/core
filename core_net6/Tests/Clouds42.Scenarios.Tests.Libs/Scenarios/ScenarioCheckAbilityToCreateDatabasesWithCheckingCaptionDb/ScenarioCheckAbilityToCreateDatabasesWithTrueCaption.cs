﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioCheckAbilityToCreateDatabasesWithCheckingCaptionDb
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз с верным названием
    /// </summary>
    public class ScenarioCheckAbilityToCreateDatabasesWithTrueCaption : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            
            var createDatabaseResult =
                TestContext.ServiceProvider.GetRequiredService<CreateDatabaseFromTemplateTestAdapter>().CreateDatabases(
                    createAccountCommand.AccountId,
                    [new InfoDatabaseDomainModelTest(TestContext) { DataBaseName = "Моя база есть же_-10" }]);

            if (!createDatabaseResult.Complete)
                throw new InvalidOperationException ("Название базы не содержит не допустимые символы. Создание базы возможно!");
        }
    }
}
