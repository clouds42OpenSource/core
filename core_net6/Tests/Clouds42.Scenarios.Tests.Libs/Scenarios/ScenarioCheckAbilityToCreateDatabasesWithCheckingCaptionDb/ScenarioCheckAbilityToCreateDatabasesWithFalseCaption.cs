﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioCheckAbilityToCreateDatabasesWithCheckingCaptionDb
{
    /// <summary>
    /// Сценарий теста на проверку возможности создания баз с ложным\не верным названием
    /// </summary>
    public class ScenarioCheckAbilityToCreateDatabasesWithFalseCaption : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            char[] unacceptableSymbols =
            [
                '!', '@', '\'', '\"', '<', '>', '@', '{', '}', '[', ']', '=', '+', '$', '#', '$', '^', '?', ';'
            ];

            foreach (var symbol in unacceptableSymbols)
            {
                var createDatabaseResult =
                    TestContext.ServiceProvider.GetRequiredService<CreateDatabaseFromTemplateTestAdapter>().CreateDatabases(
                        createAccountCommand.AccountId,
                        [new InfoDatabaseDomainModelTest(TestContext) { DataBaseName = "Моя база есть же" + symbol }]);

                if (createDatabaseResult.Complete)
                    throw new InvalidOperationException ("Название базы содержит не допустимый символ" + symbol + ". Создание базы не возможно!");
            }
        }
    }
}
