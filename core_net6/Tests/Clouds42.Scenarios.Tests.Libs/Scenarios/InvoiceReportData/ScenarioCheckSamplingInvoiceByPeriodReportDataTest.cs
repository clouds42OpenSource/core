﻿using System;
using Clouds42.Billing;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReportData
{
    /// <summary>
    /// Сценарий теста проверки выборки счетов для отчета по периоду
    /// 1) Создаем 3 счета и одному делаем дату на 3 года старше
    /// В выборку должны попасть только 2 счета
    /// </summary>
    public class ScenarioCheckSamplingInvoiceByPeriodReportDataTest : ScenarioBase
    {

        private readonly TestDataGenerator _testDataGenerator;
        private readonly IInvoiceReportDataManager _invoiceReportDataManager;

        public ScenarioCheckSamplingInvoiceByPeriodReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _invoiceReportDataManager = TestContext.ServiceProvider.GetRequiredService<IInvoiceReportDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var invoiceOne = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 500, InvoiceStatus.Processed, 5, DateTime.Now.AddYears(-3));

            var invoiceTwo = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 400, InvoiceStatus.Canceled, 5, DateTime.Now.AddDays(-5));

            var invoiceThree = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 100, InvoiceStatus.Processing, null, DateTime.Now.AddDays(-5));

            DbLayer.InvoiceRepository.Insert(invoiceOne);
            DbLayer.InvoiceRepository.Insert(invoiceTwo);
            DbLayer.InvoiceRepository.Insert(invoiceThree);
            DbLayer.Save();


            var managerResult = _invoiceReportDataManager.GetInvoiceReportDataDcs();

            if (managerResult.Error)
                throw new InvalidOperationException(managerResult.Message);

            if (managerResult.Result.Count != 2)
                throw new InvalidOperationException("Не корректно работает выборка счетов.Счета должны получаться только за последние 2 года");
        }
    }
}
