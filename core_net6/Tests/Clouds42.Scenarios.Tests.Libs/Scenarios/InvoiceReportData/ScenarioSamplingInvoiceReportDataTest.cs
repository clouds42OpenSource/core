﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReportData
{
    /// <summary>
    /// Сценарий теста выборки списка счетов для отчета
    /// Создаем 3 разных счета, получаем выборку и сравниваем счета
    /// </summary>
    public class ScenarioSamplingInvoiceReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IInvoiceReportDataManager _invoiceReportDataManager;

        public ScenarioSamplingInvoiceReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _invoiceReportDataManager = TestContext.ServiceProvider.GetRequiredService<IInvoiceReportDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var prefix = CloudConfigurationProvider.Invoices.UniqPrefix();

            var payment = _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, PaymentType.Inflow, 5000);

            var invoiceOne = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 500, InvoiceStatus.Processed, 5, DateTime.Now);

            var invoiceTwo = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 400, InvoiceStatus.Canceled, 5, DateTime.Now.AddDays(-5));

            var invoiceThree = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Inn, 100, InvoiceStatus.Processing, null, DateTime.Now.AddDays(-5), payment.Id);

            DbLayer.PaymentRepository.Insert(payment);
            DbLayer.InvoiceRepository.Insert(invoiceOne);
            DbLayer.InvoiceRepository.Insert(invoiceTwo);
            DbLayer.InvoiceRepository.Insert(invoiceThree);
            DbLayer.Save();

            var managerResult = _invoiceReportDataManager.GetInvoiceReportDataDcs();

            if (managerResult.Error)
                throw new InvalidOperationException(managerResult.Message);

            if (managerResult.Result.Count != 3)
                throw new InvalidOperationException("Не корректно работает выборка счетов");

            var findInvoice = managerResult.Result.FirstOrDefault(w => w.InvoiceNumber == $"{prefix}{invoiceOne.Uniq}");

            if (findInvoice == null)
                throw new InvalidOperationException("Не найден счет в выборке");

            CompareTwoInvoice(invoiceOne, findInvoice, createAccountCommand.Account.IndexNumber);


            findInvoice = managerResult.Result.FirstOrDefault(w => w.InvoiceNumber == $"{prefix}{invoiceTwo.Uniq}");

            if (findInvoice == null)
                throw new InvalidOperationException("Не найден счет в выборке");

            CompareTwoInvoice(invoiceTwo, findInvoice, createAccountCommand.Account.IndexNumber);

            findInvoice = managerResult.Result.FirstOrDefault(w =>
                w.InvoiceNumber == $"{prefix}{invoiceThree.Uniq}" && w.PaymentSystem == payment.PaymentSystem);

            if (findInvoice == null)
                throw new InvalidOperationException("Не найден счет в выборке");

            CompareTwoInvoice(invoiceThree, findInvoice, createAccountCommand.Account.IndexNumber);
        }

        /// <summary>
        /// Сравнить два счета
        /// </summary>
        /// <param name="invoice">Созданый счет</param>
        /// <param name="invoiceReportDataDc">Счет из выборки</param>
        /// <param name="accountNumber">Номер аккаунта</param>
        private void CompareTwoInvoice(Invoice invoice, InvoiceReportDataDto invoiceReportDataDc, int accountNumber)
        {
            if  (invoiceReportDataDc.AccountNumber != accountNumber)
                throw new InvalidOperationException("Номер аккаунта не совпадает");

            if (invoiceReportDataDc.DateOfCreation.Date == invoice.Date && invoiceReportDataDc.DateOfCreation.Hour == invoice.Date.Hour && invoiceReportDataDc.DateOfCreation.Minute == invoice.Date.Minute && invoiceReportDataDc.DateOfCreation.Second == invoice.Date.Second)
                throw new InvalidOperationException("Дата не совпадает");

            if (invoiceReportDataDc.Period != invoice.Period)
                throw new InvalidOperationException("Период не совпадает");

            if (invoiceReportDataDc.StatusString != invoice.State)
                throw new InvalidOperationException("Статус не совпадает");

            if (invoiceReportDataDc.Sum != invoice.Sum)
                throw new InvalidOperationException("Суммы не совпадают");

        }

    }
}
