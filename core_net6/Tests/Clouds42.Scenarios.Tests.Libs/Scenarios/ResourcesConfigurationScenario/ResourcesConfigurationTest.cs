﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Resources.Managers;
using System.Threading.Tasks;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Access;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesConfigurationScenario
{
    /// <summary>
    /// Проверка работы метода GetResourceConfigurationExpireDate
    /// </summary>
    public class ResourcesConfigurationTest : ScenarioBase
    {
        public override void Run()
        {
            var fakeAccessProvider = new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();

            const int allowableTimeDifferenceInSec = 60;

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var resConfController = new ResourcesConfigurationManager(fakeAccessProvider, DbLayer, HandlerException);
            var serviceName = @"Esdl";

            ManagerResult<DateTime> result = null;

            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(createAccountCommand.AccountId, serviceName);
            }).Wait();

            if (result.State != ManagerResultState.Ok)
            {
                throw new InvalidOperationException($"Для аккаунта с Id={createAccountCommand.AccountId} и сервиса \"{serviceName}\" должно возвращаться Ok");
            }

            var nowDate = DateTime.Now;

            var expireDate = nowDate.AddDays(demoDaysCount);

            var absoluteDateDiffInSec = (result.Result - expireDate).Duration().TotalSeconds;

            if (absoluteDateDiffInSec > allowableTimeDifferenceInSec)
            {
                throw new InvalidOperationException($"Для аккаунта с Id={createAccountCommand.AccountId} и сервиса \"{serviceName}\" разница между ожидаемым временем {expireDate} и возвращённым методом GetServiceExpireDate {result.Result} равно {absoluteDateDiffInSec} секунд, что превышает {allowableTimeDifferenceInSec} сек.");
            }

            // Следующий тест проверяет работу метода при заблокированном сервисе
            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(item => item.AccountId == createAccountCommand.AccountId && item.BillingService.SystemService == Clouds42Service.Esdl);
            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(createAccountCommand.AccountId, serviceName);
            }).Wait();

            absoluteDateDiffInSec = (result.Result - expireDate).Duration().TotalSeconds;


            if (absoluteDateDiffInSec > allowableTimeDifferenceInSec)
                throw new InvalidOperationException($"Для аккаунта с Id ={ createAccountCommand.AccountId } и сервиса \"{serviceName}\" разница между ожидаемым временем {allowableTimeDifferenceInSec} и возвращённым методом GetServiceExpireDate {result.Result} равно {absoluteDateDiffInSec} секунд, что превышает {allowableTimeDifferenceInSec} сек.");

            // Проверяем значение при неверном AccountId.
            var fakeAccoundId = Guid.NewGuid();

            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(fakeAccoundId, serviceName);
            }).Wait();

            if (result.State != ManagerResultState.NotFound)
            {
                throw new InvalidOperationException($"Для сгенерированного случайного Id={fakeAccoundId} и сервиса \"{serviceName}\" должно возвращаться NotFound");
            }

            // Проверяем значение при неверном имени сервиса.
            serviceName = @"SomeInvalidServiceName";

            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(createAccountCommand.AccountId, serviceName);
            }).Wait();

            if (result.State != ManagerResultState.PreconditionFailed)
            {
                throw new InvalidOperationException($"Для аккаунта с Id={createAccountCommand.AccountId} и сервиса \"{serviceName}\" должно возвращаться PreconditionFailed");
            }

            // Проверяем значение при неверном AccountId, но верном имени сервиса
            serviceName = @"Esdl";
            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(fakeAccoundId, serviceName);
            }).Wait();

            if (result.State != ManagerResultState.NotFound)
            {
                throw new InvalidOperationException($"Для сгенерированного случайного Id={fakeAccoundId} и сервиса \"{serviceName}\" должно возвращаться NotFound");
            }

            // Наконец удаляем соответствующую запись из таблицы.
            DbLayer.ResourceConfigurationRepository.Delete(resourcesConfiguration.Id);
            DbLayer.Save();

            Task.Run(async () =>
            {
                result = await resConfController.GetServiceExpireDate(createAccountCommand.AccountId, serviceName);
            }).Wait();

            if (result.State != ManagerResultState.NotFound)
                throw new InvalidOperationException($"GetServiceExpireDate должен вернуть ManagerResultState.NotFound для сервиса {serviceName} поскольку соответствующая запись была только что удалена, но фактически возвращено {result.State.ToString()} ");
        }
    }
}
