﻿using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios
{
    /// <summary>
    /// Фейк провайдера валидации пользователя в АД
    /// </summary>
    public class ValidateLoginInActiveDirectoryProviderFake : IValidateLoginInActiveDirectoryProvider
    {
        /// <summary>
        /// /Выполнить валидацию пользователя в АД
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        public void Validate(string login)
        {
            //Заглушка
        }
    }
}
