﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodIsComingToEndLetter
{
    /// <summary>
    /// Тест на проверку вызова письма о том, что демо период заканчивается
    /// </summary>
    public class DemoPeriodIsComingToEndLetterTest : ScenarioBase
    {
        public DemoPeriodIsComingToEndLetterTest()
            : base()
        {
            Services.AddTransient<DemoPeriodIsComingToEndLetterMultiUnityContextRunnerTest>();
        }

        public override void Run()
        {
            var testServiceCost = 100;
            var testServiceExpireDate = DateTime.Now.AddDays(IDemoPeriodIsComingToEndNotifacotionProcessor.RemainingNumberOfDays);
            var isAutoSubscriptionEnabled = true;
            
            var command = new CreateAccountAndTestServiceInDemoModeWithArenda1CResourceConfigurationCommand(TestContext,
                testServiceCost, testServiceExpireDate, isAutoSubscriptionEnabled);
            command.Run();

            var prolongServicesProvider = ServiceProvider.GetRequiredService<IProlongServicesProvider>();
            prolongServicesProvider.NotifyServiceDemoPeriodIsComingToEnd();

            //Assert.IsTrue(DemoPeriodIsComingToEndLetterMultiUnityContextRunnerTest.IsSuccess);
        }
    }
}
