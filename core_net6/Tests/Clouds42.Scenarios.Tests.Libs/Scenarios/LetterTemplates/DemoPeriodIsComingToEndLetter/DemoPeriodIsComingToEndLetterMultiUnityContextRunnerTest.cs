﻿using Clouds42.BLL.Common;
using Clouds42.RuntimeContext;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodIsComingToEndLetter
{
    /// <summary>
    /// MultiUnityContext с регистрацией мока вызова письма о том, что демо период заканчивается 
    /// </summary>
    public class DemoPeriodIsComingToEndLetterMultiUnityContextRunnerTest 
    {
        /// <summary>
        /// Индикатор того, что письмо было вызвано.
        /// </summary>
        public static bool IsSuccess { get; private set; }

        public DemoPeriodIsComingToEndLetterMultiUnityContextRunnerTest(IRequestContextTaskDispatcher requestContextTaskDispatcher, ICoreExecutionContext coreExecutionContext)
            
        {
        }

        //protected override IServiceCollection CreateRegisteredContainer()
        //{
        //    var container = new ServiceCollection();
        //    var ServiceProvider = container.BuildServiceProvider();
        //    var t = container.AddTransient<ILetterNotificationProcessor>();
        //    t.AddTransient(x => x.GetRequiredService<DemoPeriodIsComingToEndLetterNotification, DemoPeriodIsComingToEndLetterModelDto>()).
        //        .Returns(true).Callback<DemoPeriodIsComingToEndLetterModelDto>((model) =>
        //        {
        //            IsSuccess = model != null;
        //        });
        //    container.RegisterInstance(t.Object);

        //    return container;
        //}
    }
}
