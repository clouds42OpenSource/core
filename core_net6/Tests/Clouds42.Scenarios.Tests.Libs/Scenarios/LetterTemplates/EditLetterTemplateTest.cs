﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.DataModels.Mailing;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.LetterNotification.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates
{
    /// <summary>
    /// Тест редактирования шаблона письма
    /// Сценарий:
    ///         1. Получаем существующий шаблон
    ///         2. Редактируем его заголовок
    ///         3. Проверяем, что редактирование прошло успешно
    /// </summary>
    public class EditLetterTemplateTest : ScenarioBase
    {
        private readonly LetterTemplateManager _letterTemplateManager;
        public EditLetterTemplateTest()
        {
            _letterTemplateManager = TestContext.ServiceProvider.GetRequiredService<LetterTemplateManager>();
        }

        public override void Run()
        {
            var letterTemplate = DbLayer.GetGenericRepository<LetterTemplate>().FirstOrDefault();
            Assert.IsNotNull(letterTemplate);

            var letterTemplateDto = _letterTemplateManager.GetLetterTemplateDto(letterTemplate.Id);
            Assert.IsFalse(letterTemplateDto.Error);
            Assert.AreEqual(letterTemplateDto.Result.Body, letterTemplate.Body);
            Assert.AreEqual(letterTemplateDto.Result.Header, letterTemplate.Header);
            Assert.AreEqual(letterTemplateDto.Result.SendGridTemplateId, letterTemplate.SendGridTemplateId);

            var newHeader = $"My header {Guid.NewGuid()}";

            var editLetterTemplateDto = letterTemplateDto.Result;
            editLetterTemplateDto.Header = newHeader;

            var editLetterTemplate = _letterTemplateManager.EditLetterTemplate(editLetterTemplateDto);
            Assert.IsFalse(editLetterTemplate.Error);

            letterTemplateDto = _letterTemplateManager.GetLetterTemplateDto(letterTemplate.Id);
            Assert.IsFalse(letterTemplateDto.Error);
            Assert.AreEqual(letterTemplateDto.Result.Header, newHeader);
        }
    }
}
