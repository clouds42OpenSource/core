﻿using Clouds42.BLL.Common;
using Clouds42.RuntimeContext;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodEndedPaymentRequiredLetter
{
    /// <summary>
    /// MultiUnityContext с регистрацией мока вызова письма о том, что демо период окончен, нужна оплата 
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredLetterMultiUnityContextRunnerTest 
    {
        /// <summary>
        /// Индикатор того, что письмо было вызвано.
        /// </summary>
        public static bool IsSuccess { get; private set; }

        public DemoPeriodEndedPaymentRequiredLetterMultiUnityContextRunnerTest(IRequestContextTaskDispatcher requestContextTaskDispatcher, ICoreExecutionContext coreExecutionContext)
        {
        }

        //protected override IUnityContainer CreateRegisteredContainer()
        //{
        //    var container = base.CreateRegisteredContainer();

        //    var t = new Mock<ILetterNotificationProcessor>();
        //    t.Setup(x => x.TryNotify<DemoPeriodEndedPaymentRequiredLetterNotification, DemoPeriodEndedPaymentRequiredLetterModel>(It.IsAny<DemoPeriodEndedPaymentRequiredLetterModel>()))
        //        .Returns(true).Callback<DemoPeriodEndedPaymentRequiredLetterModel>((model) =>
        //        {
        //            IsSuccess = model != null;
        //        });
        //    container.RegisterInstance(t.Object);

        //    return container;
        //}
    }
}
