﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.TestCommands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.LetterTemplates.DemoPeriodEndedPaymentRequiredLetter
{
    /// <summary>
    /// Тест на проверку вызова письма о том, что демо период закончился, нужна оплата
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredLetterTest : ScenarioBase
    {

        public override void Run()
        {
            var testServiceCost = 100;
            var testServiceExpireDate = DateTime.Now;
            var isAutoSubscriptionEnabled = true;
            Services.AddTransient<DemoPeriodEndedPaymentRequiredLetterMultiUnityContextRunnerTest>();
            Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>();
            var sp = Services.BuildServiceProvider();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            var command = new CreateAccountAndTestServiceInDemoModeWithArenda1CResourceConfigurationCommand(TestContext,
                testServiceCost, testServiceExpireDate, isAutoSubscriptionEnabled);
            command.Run();

            var prolongServicesProvider = sp.GetRequiredService<IProlongServicesProvider>();
            prolongServicesProvider.LockExpiredDemoServices();

            Assert.IsTrue(IsTestServiceFrozen(command.AccountId, command.TestServiceId), "Сервис должен быть заморожен.");
        }

        /// <summary>
        /// Получить значение заморожен ли сервис или нет
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <param name="serviceId">Для какого сервиса</param>
        /// <returns></returns>
        private bool IsTestServiceFrozen(Guid accountId, Guid serviceId)
        {
            var found = DbLayer.ResourceConfigurationRepository.FirstOrDefault(item =>
            item.AccountId == accountId && item.BillingServiceId == serviceId);
            DbLayer.ResourceConfigurationRepository.Reload(found);

            return found?.Frozen == true;
        }
    }
}
