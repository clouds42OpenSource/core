﻿using Clouds42.Billing;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioPartnerBillingServicesDataManager
{
    /// <summary>
    /// Получить список подключенных услуг сервиса у клиента(По Id клиента)
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис, создаем воторой аккаунт и подключаем ему этот сервис
    ///         2) Включаем услугу для пользователя второго аккаунта
    ///         3) Выполняем запрос, проверяем, что вернулись верные данные
    /// </summary>
    public class ScenarioGetServiceTypesStateForUser : ScenarioBase
    {
        private readonly PartnerBillingServicesDataManager _billingServicesDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioGetServiceTypesStateForUser()
        {
            _billingServicesDataManager = TestContext.ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var serviceCommand = new CreateBillingServiceCommand(TestContext);
            serviceCommand.Run();

            var service = dbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == serviceCommand.Id);
            Assert.IsNotNull(service);

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au =>
                au.AccountId == serviceCommand.CreateBillingServiceTest.Value.AccountId);

            var secondAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = service.Id
                }
            });
            secondAccount.Run();

            var secondAccountUser = _testDataGenerator.GenerateAccountUser(secondAccount.AccountId);
            dbLayer.AccountUsersRepository.InsertAccountUser(secondAccountUser);
            dbLayer.Save();
            
            var resConfig =
               dbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);

            var managerResult = _billingServicesDataManager.GetServiceTypesStateForUserByKey(service.Id, secondAccount.AccountAdminId);
            Assert.IsTrue(managerResult.Result.ServiceIsActive);
            Assert.IsTrue(managerResult.Result.ServiceExpiredDate.Date == resConfig.ExpireDateValue.Date);
            
            managerResult = _billingServicesDataManager.GetServiceTypesStateForUserByKey(service.Id, accountUser.Id);
            Assert.IsFalse(managerResult.Result.ServiceIsActive);
        }
    }
}
