﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioPartnerBillingServicesDataManager
{
    /// <summary>
    /// Проверить статус указанной услуги сервиса у клиента
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис, создаем воторой аккаунт и подключаем ему этот сервис
    ///         2) Включаем услугу для пользователя второго аккаунта
    ///         3) Выполняем запрос, проверяем, что пользователю подключена услуга
    /// </summary>
    public class ScenarioCheckServiceTypeStatusForUserByLogin : ScenarioBase
    {
        private readonly PartnerBillingServicesDataManager _billingServicesDataManager;

        public ScenarioCheckServiceTypeStatusForUserByLogin()
        {
            _billingServicesDataManager = TestContext.ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
        }
        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var service = new CreateBillingServiceCommand(TestContext);
            service.Run();

            var billingService = dbLayer.BillingServiceRepository.FirstOrDefault(a => a.Id == service.Id);

            if (billingService == null)
                throw new NotFoundException();

            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == billingService.AccountOwnerId);
            if (account == null)
                throw new NotFoundException();

            var secondAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = service.Id
                }
            });
            secondAccount.Run();

            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.AccountId == account.Id);
            var secondAccountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.AccountId == secondAccount.AccountId);
            
            var serviceTypeStatus = CheckServiceTypeStatusForUser(service.Id, accountUser.Login);
            Assert.IsFalse(serviceTypeStatus);

            serviceTypeStatus = CheckServiceTypeStatusForUser(service.Id, secondAccountUser.Login);
            Assert.IsTrue(serviceTypeStatus);
        }

        /// <summary>
        /// Получить статус улуги для пользователя (подключена или нет)
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>Статус услуг сервиса для пользователя</returns>
        private bool CheckServiceTypeStatusForUser(Guid billingServiceId, string userLogin)
        {
            var serviceTypes = DbLayer.BillingServiceTypeRepository
                .AsQueryable()
                .Where(bst => bst.ServiceId == billingServiceId)
                .ToList();

            var status = false;

            foreach (var checkServiceTypeStatusForUser in serviceTypes.Select(serviceType => _billingServicesDataManager.CheckServiceTypeStatusForUser(serviceType.Key, userLogin)))
            {
                if (checkServiceTypeStatusForUser.Error)
                    throw new InvalidOperationException($"Не удалось получить статус услуги для пользователя {userLogin}");

                status = checkServiceTypeStatusForUser.Result.Result;
            }

            return status;
        }


    }
}
