﻿using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioPartnerBillingServicesDataManager
{
    /// <summary>
    /// Получить детальную информацию о услуге сервиса
    /// Сценарий:
    ///         1) Создаем сервис
    ///         2) Выполняем метод менеджера, для каждой услиги сервиса
    ///         3) Прверяем, что возвращаются корректные данные
    /// </summary>
    public class ScenarioGetServiceTypeInfo : ScenarioBase
    {
        private readonly PartnerBillingServicesDataManager _billingServicesDataManager;

        public ScenarioGetServiceTypeInfo()
        {
            _billingServicesDataManager = TestContext.ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
        }

        public override void Run()
        {
            var service = new CreateBillingServiceCommand(TestContext);
            service.Run();

            foreach (var billingServiceType in service.CreateBillingServiceTest.Value.BillingServiceTypes)
            {
                var managerResult = _billingServicesDataManager.GetServiceTypeInfo(billingServiceType.Id);
                Assert.IsFalse(managerResult.Error);
                Assert.AreEqual(billingServiceType.Name, managerResult.Result.Name, $"Название услуги должно быть {billingServiceType.Name}");
                Assert.AreEqual(billingServiceType.BillingType, managerResult.Result.BillingType, $"Тип работы биллинга должен быть {billingServiceType.Name}");
                Assert.AreEqual(billingServiceType.ServiceTypeCost, managerResult.Result.Cost, $"Стоимость услуги должна быть {billingServiceType.ServiceTypeCost}");
            }
        }
    }
}