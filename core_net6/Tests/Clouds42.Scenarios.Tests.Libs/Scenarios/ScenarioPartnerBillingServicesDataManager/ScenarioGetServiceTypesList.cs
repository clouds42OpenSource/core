﻿using System.Linq;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ScenarioPartnerBillingServicesDataManager
{
    /// <summary>
    /// Получить список услуг партнерского сервиса.
    /// Сценарий:
    ///         1) Создаем сервис, выполняем метод менеджера
    ///         2) Проверяем, что менеджер вернул правильное количество
    ///            услуг сервиса
    /// </summary>
    public class ScenarioGetServiceTypesList : ScenarioBase
    {
        private readonly PartnerBillingServicesDataManager _billingServicesDataManager;

        public ScenarioGetServiceTypesList()
        {
            _billingServicesDataManager = TestContext.ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
        }

        public override void Run()
        {
            var service = new CreateBillingServiceCommand(TestContext);
            service.Run();

            var serviceTypesIds = service.CreateBillingServiceTest.Value.BillingServiceTypes.Select(s => s.Id).ToList();
            var serviceTypesCount = service.CreateBillingServiceTest.Value.BillingServiceTypes.Count;

            var managerResult = _billingServicesDataManager.GetServiceTypesList(service.Id);
            Assert.IsFalse(managerResult.Error);
            Assert.IsTrue(serviceTypesCount == managerResult.Result.List.Count, $"Количество услуг сервиса должно быть равно {serviceTypesCount}");

            foreach (var serviceType in managerResult.Result.List)
            {
                Assert.IsTrue(serviceTypesIds.Contains(serviceType), $"В списке должна быть услуга {serviceType}");
            }
        }
    }
}
