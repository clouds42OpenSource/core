﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Backup.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BackupManagerTest
{
    /// <summary>
    /// Сценарий для проверки успешного восстановления инф. базы из бэкапа
    /// Действия:
    /// 1. Создадим аккаунт и все необходимые сущности. [EntitiesCreation]
    /// 2. Проверим, что процесс восстановления из бэкапа будет запущен и статус базы изменится. [CheckProccessStartedAndDbStateChanged]
    /// 3. Проверим, что процесс восстановления из бэкапа будет запущен и статус базы не будет изменен, так как это восстановление в новую. [CheckProccessStartedAndDbStateDidntChange]
    /// </summary>
    public class ScenarioRestoreAccountDatabaseSuccess : ScenarioBase
    {
        private readonly BackupManager _backupManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly IMyDatabasesServiceTypeDataProvider _myDatabasesServiceTypeDataProvider;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioRestoreAccountDatabaseSuccess()
        {
            _backupManager = TestContext.ServiceProvider.GetRequiredService<BackupManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _myDatabasesServiceTypeDataProvider = TestContext.ServiceProvider.GetRequiredService<IMyDatabasesServiceTypeDataProvider>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();

        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var databaseIds = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 2);

            var databaseIdOne = databaseIds[0]; 
            var databaseIdTwo = databaseIds[1];

            var backupIdOne = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseIdOne, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));
            var backupIdTwo = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseIdTwo, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(2));

            var myDatabasesServiceTypeId =
                _myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDatabaseId(databaseIdOne) ??
                throw new NotFoundException("Не удалось найти сервис Мои инф. базы");

            var uploadFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            #endregion

            #region CheckProccessStartedAndDbStateChanged

            var restoreAcDbFromBackupDto = _testDataGenerator.GenerateRestoreAcDbFromBackupDto(uploadFileId,
                backupIdOne, createAccountCommand.AccountAdminId, myDatabasesServiceTypeId);

            var result = _backupManager.RestoreAccountDatabase(restoreAcDbFromBackupDto);
            Assert.IsFalse(result.Error);
            Assert.IsTrue(result.Result.IsComplete);

            _databaseProcessTestHelper.CheckProcessOfDatabaseStartedByAdditionalParams(databaseIdOne, backupIdOne,
                ProcessesNameConstantsTest.RestoreAccountDatabaseFromTombJobName);

            #endregion

            #region CheckProccessStartedAndDbStateDidntChange

            restoreAcDbFromBackupDto.RestoreType = AccountDatabaseBackupRestoreType.ToNew;
            restoreAcDbFromBackupDto.AccountDatabaseBackupId = backupIdTwo;

            result = _backupManager.RestoreAccountDatabase(restoreAcDbFromBackupDto);
            Assert.IsFalse(result.Error);
            Assert.IsTrue(result.Result.IsComplete);

            _databaseProcessTestHelper.CheckProcessOfDatabaseStartedByAdditionalParams(databaseIdTwo, backupIdTwo,
                ProcessesNameConstantsTest.RestoreAccountDatabaseFromTombJobName, false);

            #endregion
        }
    }
}
