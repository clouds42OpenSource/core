﻿using Clouds42.AccountDatabase.Backup.Helpers;
using Clouds42.AccountDatabase.Backup.Providers;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BackupManagerTest
{
    /// <summary>
    /// Тест на калькулятор для рассчета времени восстановления базы
    /// </summary>
    public class BackupRestoringTimeCalculatorTest : ScenarioBase
    {
        public override void Run()
        {
            var backupRestoringTimeCalculator = new BackupRestoringTimeCalculator();

            CompareValues(backupRestoringTimeCalculator.Calculate(0), 0);
            CompareValues(backupRestoringTimeCalculator.Calculate(592),
                RestoreBackupTimeConst.FirstGapResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(5000),
                RestoreBackupTimeConst.FirstGapResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(7892),
                RestoreBackupTimeConst.SecondGapResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(11000),
                RestoreBackupTimeConst.ThirdGapResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(17892),
                RestoreBackupTimeConst.FourthGapResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(100000),
                RestoreBackupTimeConst.MaxGapValueResult.IncreaseValueToThirtyPercents());
            CompareValues(backupRestoringTimeCalculator.Calculate(1000),
                RestoreBackupTimeConst.MaxGapValueResult.IncreaseValueToThirtyPercents(), false);
            CompareValues(backupRestoringTimeCalculator.Calculate(12000),
                RestoreBackupTimeConst.FirstGapResult.IncreaseValueToThirtyPercents(), false);
        }

        /// <summary>
        /// Сравнить значения
        /// </summary>
        /// /// <param name="actualValue">Действительное значение</param>
        /// <param name="expectedValue">Ожидаемое значение</param>
        /// <param name="shouldBeEqual">Должны ли значения быть равны</param>
        private void CompareValues(int actualValue, int expectedValue, bool shouldBeEqual = true)
        {
            if (shouldBeEqual)
            {
                AssertHelper.Execute(() => Assert.AreEqual(expectedValue, actualValue));
                return;
            }

            AssertHelper.Execute(() => Assert.AreNotEqual(expectedValue, actualValue));
        }
    }
}