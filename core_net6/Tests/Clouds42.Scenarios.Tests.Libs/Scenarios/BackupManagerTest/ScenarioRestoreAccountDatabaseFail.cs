﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Backup.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BackupManagerTest
{
    /// <summary>
    /// Сценарий для проверки неуспешного восстановления инф. базы из бэкапа
    /// Действия:
    /// 1. Создадим аккаунт и все необходимые сущности. [EntitiesCreation]
    /// 2. Проверим, что будет получена ошибка при указанини несуществующего Id бэкапа. [CheckGettingErrorIfBackupGuidDoesntExist]
    /// 3. Проверим, что будет получена ошибка при указании несуществующего Id сервиса "Моф инф. базы". [CheckGettingErrorIfMyDatabasesServiceTypeIdDoesntExist]
    /// 4. Проверим, что ошибка не будет получена при указаннии остальных несуществующих параметров. [CheckThatErrorWontBeGot]
    /// </summary>
    public class ScenarioRestoreAccountDatabaseFail : ScenarioBase
    {
        private readonly BackupManager _backupManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly IMyDatabasesServiceTypeDataProvider _myDatabasesServiceTypeDataProvider;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioRestoreAccountDatabaseFail()
        {
            _backupManager = TestContext.ServiceProvider.GetRequiredService<BackupManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _myDatabasesServiceTypeDataProvider = TestContext.ServiceProvider.GetRequiredService<IMyDatabasesServiceTypeDataProvider>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            var myDatabasesServiceTypeId =
                _myDatabasesServiceTypeDataProvider.GetServiceTypeIdByDatabaseId(databaseId) ??
                throw new NotFoundException("Не удалось найти сервис Мои инф. базы");

            var uploadFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            #endregion

            #region CheckGettingErrorIfBackupGuidDoesntExist

            var restoreAcDbFromBackupDto = _testDataGenerator.GenerateRestoreAcDbFromBackupDto(uploadFileId,
                backupId, createAccountCommand.AccountAdminId, myDatabasesServiceTypeId);

            restoreAcDbFromBackupDto.AccountDatabaseBackupId = Guid.NewGuid();

            var result = _backupManager.RestoreAccountDatabase(restoreAcDbFromBackupDto);
            Assert.IsTrue(result.Error, "Не получили ошибку при парадче несуществующего Id бэкапа");

            #endregion

            #region CheckGettingErrorIfMyDatabasesServiceTypeIdDoesntExist

            restoreAcDbFromBackupDto.AccountDatabaseBackupId = backupId;
            restoreAcDbFromBackupDto.MyDatabasesServiceTypeId = Guid.NewGuid();

            Assert.IsTrue(result.Error, "Не получили ошибку при парадче несуществующего Id сервиса");

            #endregion

            #region CheckThatErrorWontBeGot

            restoreAcDbFromBackupDto.MyDatabasesServiceTypeId = myDatabasesServiceTypeId;
            restoreAcDbFromBackupDto.AccountDatabaseName = string.Empty;
            restoreAcDbFromBackupDto.UsersIdForAddAccess = [];
            restoreAcDbFromBackupDto.UploadedFileId = Guid.NewGuid();

            result = _backupManager.RestoreAccountDatabase(restoreAcDbFromBackupDto);
            Assert.IsFalse(result.Error, result.Message);
            Assert.IsTrue(result.Result.IsComplete);

            _databaseProcessTestHelper.CheckProcessOfDatabaseStartedByAdditionalParams(databaseId, backupId,
                ProcessesNameConstantsTest.RestoreAccountDatabaseFromTombJobName);

            #endregion
        }
    }
}
