﻿using System;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ActivityServices
{
    /// <summary>
    /// Сценарий теста проверки активности сервиса
    /// при использовании менеджера по работе с ресурсами
    ///
    /// 1) Созхдаем аккаунт с активной Арендой 1С
    /// 2) Сдвигаем дату окончания сервиса и проверяем что сервис
    /// не считается заблокированным
    /// 3) Выставляем Frozen true и повторяем проверку,
    /// ожидая что теперь сервис будет считаться заблокированным
    /// </summary>
    public class ScenarioCheckIsActiveServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var accountCsResourceValuesManager = ServiceProvider.GetRequiredService<IAccountCsResourceValuesManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resConf.ExpireDate = DateTime.Now.AddDays(-5);
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            var managerResult = accountCsResourceValuesManager.IsActiveService(createAccountCommand.AccountId,
                createAccountCommand.AccountAdminId, Clouds42Service.MyEnterprise.ToString());

            if (managerResult.Error || !managerResult.Result)
                throw new InvalidOperationException("Операция выполнилась с ошибкой");

            resConf.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            managerResult = accountCsResourceValuesManager.IsActiveService(createAccountCommand.AccountId,
                createAccountCommand.AccountAdminId, Clouds42Service.MyEnterprise.ToString());

            if (managerResult.Error || managerResult.Result)
                throw new InvalidOperationException("Операция выполнилась с ошибкой");
        }
    }
}
