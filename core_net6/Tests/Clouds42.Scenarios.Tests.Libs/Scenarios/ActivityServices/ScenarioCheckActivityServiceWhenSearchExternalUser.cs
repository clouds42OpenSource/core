﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ActivityServices
{
    /// <summary>
    /// Сценарий теста проверки на активность сервиса
    /// при поиске пользователя для спонсирования
    ///
    /// 1) Создаем два аккаунта с активной Арендой 1С
    /// 2) Сдвигаем дату окончания сервиса и проверяем что сервис
    /// не считается заблокированным при поиске по почте
    /// 3) Выставляем Frozen true и повторяем проверку,
    /// ожидая что теперь сервис будет считаться заблокированным
    /// при поиске по почте
    /// </summary>
    public class ScenarioCheckActivityServiceWhenSearchExternalUser : ScenarioBase
    {
        public override void Run()
        {
            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var emailForSearching = DbLayer.AccountUsersRepository.FirstOrDefault(w =>
                w.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId).Email;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 900,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resConf.ExpireDate = DateTime.Now.AddDays(-5);
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            var result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (result.Error|| result.Result.User == null)
                throw new InvalidOperationException("Ошибка. Спонсирование разрешено, сервис Аренда 1С не заблокирован.");

            resConf.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (result.Result.User != null)
                throw new InvalidOperationException("Ошибка. Спонсирование запрещено, сервис Аренда 1С заблокирован.");
        }
    }
}
