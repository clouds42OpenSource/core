﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Internal.ModelProcessors;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ActivityServices
{
    /// <summary>
    /// Сценарий теста проверки активности сервиса
    /// при получении набора опций инф. базы
    ///
    /// 1) Создаем аккаунт и подключенным сервисом Аренда 1С
    /// 2) Сдвигаем дату окончания сервиса и проверяем что сервис
    /// не считается заблокированным
    /// 3) Выставляем Frozen true и повторяем проверку,
    /// ожидая что теперь сервис будет считаться заблокированным
    /// </summary>
    public class ScenarioCheckDbControlOptionsDomainModelProcessorTest : ScenarioBase
    {
        public override void Run()
        {
            var processor = ServiceProvider.GetRequiredService<DbControlOptionsDomainModelProcessor>();
            var testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var database = testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 0,
                createAccountCommand.Account.IndexNumber);

            DbLayer.DatabasesRepository.Insert(database);

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resConf.ExpireDate = DateTime.Now.AddDays(-5);
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            var model = processor.GetModel(createAccountCommand.AccountId, database.Id);

            if (!model.IsEnable)
                throw new InvalidOperationException("Сервис не заблокирован. Не верный статус.");

            resConf.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            model = processor.GetModel(createAccountCommand.AccountId, database.Id);

            if (model.IsEnable)
                throw new InvalidOperationException("Сервис заблокирован. Не верный статус.");
        }
    }
}
