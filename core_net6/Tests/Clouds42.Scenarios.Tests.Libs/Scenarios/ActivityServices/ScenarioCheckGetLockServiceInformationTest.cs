﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ActivityServices
{
    /// <summary>
    /// Сценарий теста проверки получение информации
    /// о заблокированном сервисе для аккаунта
    /// 1) Создаем аккаунт с Арендой 1С
    /// 2) Сдвигаем дату окончания сервиса и првоеряем что сервис
    /// не считается заблокированным
    /// 3) Выставляем Frozen true и повторяем проверку,
    /// ожидая что теперь сервис будет считаться заблокированным
    /// </summary>
    public class ScenarioCheckGetLockServiceInformationTest : ScenarioBase
    {

        public override void Run()
        {
            var accountUsersManager = ServiceProvider.GetRequiredService<IAccountUsersManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resConf.ExpireDate = DateTime.Now.AddDays(-5);
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            var managerResult = accountUsersManager.GetLockServiceInformation(createAccountCommand.AccountId);

            if (managerResult.Error)
                throw new InvalidOperationException("Операция завершилась ошибкой");

            if (!string.IsNullOrEmpty(managerResult.Result))
                throw new InvalidOperationException("Сервис не заблокирован");

            resConf.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();

            managerResult = accountUsersManager.GetLockServiceInformation(createAccountCommand.AccountId);

            if (managerResult.Error)
                throw new InvalidOperationException("Операция завершилась ошибкой");

            if (managerResult.Result != Clouds42Service.MyEnterprise.GetDisplayName())
                throw new InvalidOperationException("Сервис заблокирован");
        }
    }
}
