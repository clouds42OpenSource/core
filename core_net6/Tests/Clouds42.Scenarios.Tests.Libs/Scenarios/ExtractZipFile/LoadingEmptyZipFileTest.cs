﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.AccountDatabase.Create.Helpers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ExtractZipFile
{
    /// <summary>
    /// Распаковка пустого зип файла
    /// Предусловия: пользователем загружен пустой зип файл для баз на разделителях
    /// Действия: проверяем зип файл на наличие в нем файла DumpInfo.xml
    /// Проверка: обьект должен быть равен null
    /// </summary>
    public class LoadingEmptyZipFileTest : ScenarioBase
    {
        private readonly IAgentFileStorageProvider _agentFileStorageProvider;

        public LoadingEmptyZipFileTest()
        {
            _agentFileStorageProvider = TestContext.ServiceProvider.GetRequiredService<IAgentFileStorageProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var sourseFilePath = _agentFileStorageProvider.GetPathToFileStorageAgent(createAccountCommand.AccountId);

            var createZipFileHelper = ServiceProvider.GetRequiredService<CreateZipFileHelper>();
            createZipFileHelper.CreateEmptyZipFile(sourseFilePath, "empty");

            try
            {
                var dataObject = ExtractZipFileHelper.ExtractDumpInfoFromZipFile(sourseFilePath + @"\empty.zip");
                Assert.IsNull(dataObject?.DumpInfo, "При загрузке пустого зип файла был дессериализован объект");

            }
            catch (Exception)
            {
                // так и должно быть
            }
            createZipFileHelper.DeleteTestFile(sourseFilePath + @"\empty.zip");
        }
    }
}
