﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Configurations.Configurations;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ExtractZipFile
{
    //Распаковка не корректного зип файла
    ///     Предусловия: пользователем загружен не правильный зип файла для баз на разделителях
    ///                 Полный путь к файлу: FileUploadPath что в базе + userLogin + zipFileName
    ///     Действия: проверяем зип файл на наличие в нем файла DumpInfo.xml, достаем его и дессереализуем
    /// 
    ///     Проверка: при дессериализации выдаст ошибку
    public class LoadingIncorrectZipFileTest : ScenarioBase
    {
        private readonly string userLogin = "TestLoad";
        private readonly string zipFileName = "trade_error.zip";

        public override void Run()
        {
            var sourseFilePath = Path.Combine(CloudConfigurationProvider.Files.FileUploadPath(), userLogin, zipFileName);
            try
            {
                var dataObject = ExtractZipFileHelper.ExtractDumpInfoFromZipFile(sourseFilePath);
                if (dataObject?.DumpInfo != null)
                    throw new InvalidOperationException ("При закгрузке не корректного зип файла, объект дессериализован");
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
