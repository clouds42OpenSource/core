﻿using System;
using System.IO;
using System.Linq;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Helpers;
using Clouds42.Configurations;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ExtractZipFile
{
    ///Предусловия: Проверка удаления загруженных файлов
    ///
    ///Действия: 
    ///     1) проверяем что есть файлы на удаление (дата создания больше вчерашней даты)
    ///     2) считаем количество файлов на удаление
    ///     3) удаляем файлы
    ///     4) проверяем что нужное количество файлов было удалено
    public class RemoveUploadFileTest : ScenarioBase
    {
        public override void Run()
        {
             string sourseFilePath = ConfigurationHelper.GetConfigurationValue("FileUploadPath");
             string userLogin = "TestLoad";

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var user = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == createAccountCommand.AccountAdminId);
            user.Login = userLogin;
            DbLayer.AccountUsersRepository.Update(user);
            DbLayer.Save();

            GenereateFilesInDirectory(sourseFilePath);

            //проверка сколько файлов в основной директории 
            var filesInDirectoryCount = 0;
            //метод SearchDirectoryFromWhichDeletingDownloadedFiles удаляет файлы лишь
            //если им более суток
            foreach (var file in Directory.EnumerateFiles(sourseFilePath + @"\TestLoad", "*", SearchOption.AllDirectories))
            {
                File.SetCreationTime(file, new DateTime(1912, 3, 16));
                filesInDirectoryCount++;
            }

            //удаляем файлы из директории
            SearchDirectoryFromWhichDeletingDownloadedFiles(sourseFilePath);

            //проверка сколько файлов в основной директории после удаления 
            var filesInDirectoryAfterRemovalCount = Directory.EnumerateFiles(sourseFilePath + @"\TestLoad", "*", SearchOption.AllDirectories).Count();

            if (filesInDirectoryCount == filesInDirectoryAfterRemovalCount)
                throw new InvalidOperationException ($"Ошибка при удалении файлов из директории было '{filesInDirectoryCount}' стало {filesInDirectoryAfterRemovalCount}");

        }

        private void GenereateFilesInDirectory(string storagePath)
        {
            var storage = new DirectoryInfo(storagePath + @"\TestLoad");
            var startPath = storagePath + @"\TestLoad\empty";
            storage.CreateSubdirectory("empty");
            
            for (int i = 1; i <= 4; i++)
            {
                try
                {
                    ZipHelper.SafelyCreateZipFromDirectory(startPath, storagePath + @"\TestLoad\empty" + i + ".zip");
                }
                catch (Exception)
                {
                    
                }
            }
        }

        /// <summary>
        /// Поиск в каталоге, из которого удаляются загруженные файлы
        /// </summary>
        /// <returns></returns>
        public void SearchDirectoryFromWhichDeletingDownloadedFiles(string fileUploadPath)
        {
            //получить список всех пользователей
            var accountUserLoginList =
                DbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery().Select(l => l.Login).ToList();

            //проверить для каждого пользователя наличие папки и содержания в ней загруженных файлов
            foreach (var login in accountUserLoginList)
            {
                var sourcePath = Path.Combine(fileUploadPath, login);
                DirectoryHelper.ClearOldFiles(sourcePath, TimeSpan.FromSeconds(2), Logger);
            }

            //удаляем файлы в корневом каталоге
            DirectoryHelper.ClearOldFiles(fileUploadPath, TimeSpan.FromSeconds(2), Logger);
        }
    }
}
