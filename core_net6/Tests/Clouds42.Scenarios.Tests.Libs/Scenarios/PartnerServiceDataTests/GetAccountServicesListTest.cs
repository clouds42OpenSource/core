﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerServiceDataTests
{
    /// <summary>
    /// Получить список созданных сервисов
    ///     Сценарий:
    ///         1) Создаём сервис c пользователем
    ///         2) Получаем id сервиса
    ///         3) Пытаемся получить, список сервисов с помощью GetAccountServicesList для владельца сервиса
    ///             если список не пустой, то метод работает верно
    /// </summary>
    public class GetAccountServicesListTest : ScenarioBase
    {
        private readonly PartnerBillingServicesDataManager _partnerBillingServicesDataManager;
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public GetAccountServicesListTest()
        {
            _partnerBillingServicesDataManager = TestContext.ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = [serviceTypeDto]
                });
            createCustomService.Run();
            var billingService = DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createCustomService.Id);

            var result = _partnerBillingServicesDataManager.GetAccountServicesList(billingService.AccountOwner.Id);

            Assert.IsTrue(result.Result.List.Contains(createCustomService.Id), $"Сервис {createCustomService.Id} не пренадлежит аккаунту {billingService.AccountOwner.Id}");
        }
    }
}

