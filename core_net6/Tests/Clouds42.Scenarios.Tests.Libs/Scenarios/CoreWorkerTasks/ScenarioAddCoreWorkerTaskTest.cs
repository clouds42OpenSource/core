﻿using System.Linq;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks
{
    /// <summary>
    /// Сценарий добавления задачи воркера в очередь
    /// Действия:
    ///     1) Пытаемся добавить задачу в очередь
    ///     2) Проверяем что задача добавлена
    /// </summary>
    public class ScenarioAddCoreWorkerTaskTest : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;
        private readonly ICoreWorkerTasksQueueDataManager _coreWorkerTasksQueueDataManager;

        public ScenarioAddCoreWorkerTaskTest()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
            _coreWorkerTasksQueueDataManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueDataManager>();
        }

        public override void Run()
        {
            var processMailingJob =
                DbLayer.CoreWorkerTaskRepository.FirstOrDefault(w => w.TaskName == "AutoPayBeforeLock");

            _coreWorkerTasksQueueManager.AddTaskQueue(new CoreWorkerTasksDto
            {
                CloudTaskId = processMailingJob.ID,
                Comment = nameof(Test)
            });

            var tasksViewModel = _coreWorkerTasksQueueDataManager.GetCoreWorkerTasksQueueData(new CoreWorkerTasksQueueFilterParamsDto
            {
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Desc,
                    FieldName = "CreateDateTime"
                },
                Filter = new CoreWorkerTasksQueueFilterDto(),
                PageNumber = 1
            });

            Assert.IsTrue(tasksViewModel.Result.Records.Any());
        }
    }
}
