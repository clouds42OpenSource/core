﻿using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks
{
    /// <summary>
    /// Сценарий выборки задач
    /// Действия:
    ///     1) Вытаскиваем задачу 
    ///     2) Проверяем, что выборка задачи прошла успешна
    ///     3) Проверяем что запись вытащена
    ///     4) Проверяем что вытаскиваемая задача вытащена
    /// </summary>
    public class ScenarioSelectCoreWorkerTasksTest : ScenarioBase
    {
        private readonly ICoreWorkerTaskManager _coreWorkerTaskManager;

        public ScenarioSelectCoreWorkerTasksTest()
        {
            _coreWorkerTaskManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTaskManager>();
        }

        public override void Run()
        {
            #region Вытаскиваем задачу

            var processMailingJob =
                DbLayer.CoreWorkerTaskRepository.FirstOrDefault(w => w.TaskName == "AutoPayBeforeLock");

            var taskViewModel = _coreWorkerTaskManager.GetCoreWorkerTasks(new CoreWorkerTaskFilterDto
            {
                Filter = new CoreWorkerTaskFilterInfoDto
                {
                    TaskId = processMailingJob.ID
                },
                PageNumber = 1
            });

            #endregion
            

            #region Проверяем, что выборка задачи прошла успешна

            Assert.IsTrue(taskViewModel.State == ManagerResultState.Ok, taskViewModel.Message);

            #endregion


            #region Проверяем что запись вытащена

            Assert.IsTrue(taskViewModel.Result.Records.Length == 1);

            #endregion


            #region Проверяем что вытаскиваемая задача вытащена

            Assert.AreEqual(taskViewModel.Result.Records[0].Id, processMailingJob.ID);

            #endregion
        }
    }
}
