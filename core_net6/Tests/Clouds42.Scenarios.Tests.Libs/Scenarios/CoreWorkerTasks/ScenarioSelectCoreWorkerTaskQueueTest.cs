﻿using System.Linq;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks
{
    /// <summary>
    /// Сценарий выборки задачи в очереди
    /// Действия:
    ///     1) Пытаемся добавить задачу в очередь
    ///     2) Проверяем, что задача добавлена
    ///     3) Вытаскиваем записи
    ///     4) Проверяем, что выборка задач прошла успешна
    ///     5) Проверяем что записи вытащены
    ///     6) Проверяем что добавленная задача вытащена
    /// </summary>
    public class ScenarioSelectCoreWorkerTaskQueueTest : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;
        private readonly ICoreWorkerTasksQueueDataManager _coreWorkerTasksQueueDataManager;

        public ScenarioSelectCoreWorkerTaskQueueTest()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
            _coreWorkerTasksQueueDataManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueDataManager>();
        }

        public override void Run()
        {
            #region Пытаемся добавить задачу в очередь

            var processMailingJob =
                DbLayer.CoreWorkerTaskRepository.FirstOrDefault(w => w.TaskName == "AutoPayBeforeLock");

            var newTaskQueueId = _coreWorkerTasksQueueManager.AddTaskQueue(new CoreWorkerTasksDto
            {
                CloudTaskId = processMailingJob.ID,
                Comment = nameof(Test)
            });

            #endregion


            #region Проверяем, что задача добавлена

            Assert.IsTrue(newTaskQueueId.State == ManagerResultState.Ok, newTaskQueueId.Message);

            #endregion


            #region Вытаскиваем записи 
            
            var tasksViewModel = _coreWorkerTasksQueueDataManager.GetCoreWorkerTasksQueueData(new CoreWorkerTasksQueueFilterParamsDto
            {
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Desc,
                    FieldName = "CreateDateTime"
                },
                Filter = new CoreWorkerTasksQueueFilterDto(),
                PageNumber = 1
            });

            #endregion


            #region Проверяем, что выборка задач прошла успешна

            Assert.IsTrue(tasksViewModel.State == ManagerResultState.Ok, tasksViewModel.Message);

            #endregion


            #region Проверяем что записи вытащены

            Assert.IsTrue(tasksViewModel.Result.Records.Any());

            #endregion


            #region Проверяем что добавленная задача вытащена

            Assert.AreEqual(tasksViewModel.Result.Records[0].Id, newTaskQueueId.Result);

            #endregion
        }
    }
}
