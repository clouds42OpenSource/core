﻿using System.Linq;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.DataContracts.BaseModel;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks
{
    /// <summary>
    /// Сценарий добавления задачи воркера в очередь
    /// Действия:
    ///     1) Пытаемся добавить задачу EditUserInAdJob в очередь
    ///     2) Проверяем что задача добавлена
    ///     3) Проверяем что при получении списка задач у этой задачи нет параметров
    /// </summary>
    public class ScenarioCheckCoreWorkerTasksQueueViewModelTest : ScenarioBase
    {
        private readonly ICoreWorkerTasksQueueManager _coreWorkerTasksQueueManager;
        private readonly ICoreWorkerTasksQueueDataManager _coreWorkerTasksQueueDataManager;

        public ScenarioCheckCoreWorkerTasksQueueViewModelTest()
        {
            _coreWorkerTasksQueueManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueManager>();
            _coreWorkerTasksQueueDataManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTasksQueueDataManager>();
        }

        public override void Run()
        {
            var editUserInAdJob =
                DbLayer.CoreWorkerTaskRepository.FirstOrDefault(w => w.TaskName == nameof(EditUserInAdJob));

            _coreWorkerTasksQueueManager.AddTaskQueue(new CoreWorkerTasksDto
            {
                CloudTaskId = editUserInAdJob.ID,
                Comment = "Редактирование пользователя 'Test' в домене.",
                TaskParams = @"
                                {
                                    'AccountUserId':'f2a93778-4e41-424f-b2a1-9a874b8ebf6f',
                                    'OldLogin':'Test',
                                    'UserName':'Test',
                                    'Password':'123',
                                    'FirstName':'Имя',
                                    'LastName':'Фамилия',
                                    'MiddleName':'Отчество',
                                    'PhoneNumber':null
                                }"
            });

            var tasksViewModel = _coreWorkerTasksQueueDataManager.GetCoreWorkerTasksQueueData(new CoreWorkerTasksQueueFilterParamsDto
            {
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Desc,
                    FieldName = "CreateDateTime"
                },
                Filter = new CoreWorkerTasksQueueFilterDto(),
                PageNumber = 1
            });
            Assert.IsTrue(tasksViewModel.Result.Records.Any());

            var taskInQueue = tasksViewModel.Result.Records.FirstOrDefault();
            Assert.IsNotNull(taskInQueue);
            Assert.AreEqual(taskInQueue.Name, editUserInAdJob.TaskName);

            var taskInQueueItemInfoResult = _coreWorkerTasksQueueDataManager.GetTaskInQueueItemInfo(taskInQueue.Id);
            Assert.IsFalse(taskInQueueItemInfoResult.Error);
            Assert.IsFalse(string.IsNullOrEmpty(taskInQueueItemInfoResult.Result.TaskParams));
        }
    }
}
