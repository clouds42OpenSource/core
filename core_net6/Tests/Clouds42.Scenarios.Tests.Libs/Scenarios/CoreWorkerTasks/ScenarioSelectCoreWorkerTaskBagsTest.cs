﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CoreWorkerTasks
{
    /// <summary>
    /// Сценарий выборки задач связанных с воркером
    /// Действия:
    ///     1) Вытаскиваем задачи из задач воркера
    ///     2) Проверяем, что выборка задач прошла успешна
    ///     3) Проверяем что записи вытащены
    /// </summary>
    public class ScenarioSelectCoreWorkerTaskBagsTest : ScenarioBase
    {
        private readonly ICoreWorkerTaskManager _coreWorkerTaskManager;

        public ScenarioSelectCoreWorkerTaskBagsTest()
        {
            _coreWorkerTaskManager = TestContext.ServiceProvider.GetRequiredService<ICoreWorkerTaskManager>();
        }

        public override void Run()
        {
            #region Вытаскиваем задачи из задач воркера

            var processMailingJob =
                DbLayer.CoreWorkerTaskRepository.FirstOrDefault(w => w.TaskName == "AutoPayBeforeLock");
            AddProcessMailingJobToWorkerIfNotExist(processMailingJob.ID);

            var taskBagViewModel = _coreWorkerTaskManager.GetCoreWorkerAvailableTasksBag(new CoreWorkersAvailableTasksBagsFilterDto
            {
                SortingData = new SortingDataDto
                {
                    FieldName = "Priority",
                    SortKind = SortType.Asc
                },
                Filter = new CoreWorkersAvailableTasksBagsFilterDataDto
                {
                    TaskId = processMailingJob.ID
                },
                PageNumber = 1
            });

            #endregion
            

            #region Проверяем, что выборка задач прошла успешна

            Assert.IsTrue(taskBagViewModel.State == ManagerResultState.Ok, taskBagViewModel.Message);

            #endregion


            #region Проверяем что записи вытащены

            Assert.IsTrue(taskBagViewModel.Result.Records.Any());

            #endregion
          
        }

        /// <summary>
        /// Добавить задачу отправки писем к воркеру если ее нет
        /// </summary>
        /// <param name="processMailingJobId">ID задачи</param>
        private void AddProcessMailingJobToWorkerIfNotExist(Guid processMailingJobId)
        {
            var coreWorker = DbLayer.CoreWorkerRepository.FirstOrDefault() ??
                             throw new NotFoundException("Не найдено ни одного воркера");
            var coreWorkerAvailableTasksBags = DbLayer.GetGenericRepository<CoreWorkerAvailableTasksBag>()
                .Where(task => task.CoreWorkerId == coreWorker.CoreWorkerId).Select(task => task.CoreWorkerTaskId)
                .ToList();

            if (coreWorkerAvailableTasksBags.Any(taskId => taskId.Equals(processMailingJobId)))
                return;

            coreWorkerAvailableTasksBags.Add(processMailingJobId);

            _coreWorkerTaskManager.ChangeWorkerAvailableTasksBags(new ChangeWorkerAvailableTasksBagsDto
            {
                CoreWorkerId = coreWorker.CoreWorkerId,
                WorkerTasksBagsIds = coreWorkerAvailableTasksBags
            });
        }
    }
}
