﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Тест получаения данных кальлкулятора для подписочных сервисов для обычного аккаунта
    /// </summary>
    public class ScenarioGetInvoiceCalculatorForSubscriptionServicesForNormalAccount : ScenarioBase
    {
        private readonly IInvoiceCalculatorManager _manager;

        public ScenarioGetInvoiceCalculatorForSubscriptionServicesForNormalAccount()
        {
            _manager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCalculatorManager>();
        }

        /// <inheritdoc/>
        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var managerResult = _manager.GetInvoiceCalculatorForAccount(account.AccountId, InvoicePurposeType.BaseBillingServices);

            Assert.IsFalse(managerResult.Error);
            var calculator = managerResult.Result;
            Assert.IsNotNull(calculator);

            ///Проверяем что в продуктах есть записи
            Assert.IsTrue(calculator.Products.Any());

            ///Проверяем что в продуктах нет записией для сервисов Fasta (Esdl, Recongition) или дополнительных ресурсов (доступны только VIP аккаунтам)
            Assert.IsFalse(calculator.Products.Any(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.Esdl));
            Assert.IsFalse(calculator.Products.Any(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.Recognition));
            Assert.IsFalse(calculator.Products.Any(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.AdditionalResources));
        }
    }
}

