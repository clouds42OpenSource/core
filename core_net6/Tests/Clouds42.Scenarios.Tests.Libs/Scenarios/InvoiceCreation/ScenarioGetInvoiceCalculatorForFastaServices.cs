﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Тест получения данных калькулятора для доплнительных сервисов (Fasta и Recognition)
    /// </summary>
    public class ScenarioGetInvoiceCalculatorForFastaServices : ScenarioBase
    {
        private readonly IInvoiceCalculatorManager _manager;

        public ScenarioGetInvoiceCalculatorForFastaServices()
        {
            _manager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCalculatorManager>();
        }

        /// <inheritdoc/>
        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var managerResult = _manager.GetInvoiceCalculatorForAccount(account.AccountId, InvoicePurposeType.AdditionalBillingServices);
            Assert.IsFalse(managerResult.Error);
            var calculator = managerResult.Result;
            Assert.IsNotNull(calculator);
            ///Проверяем что в продуктах только две записи (Esdl и Recognition)
            Assert.IsTrue(calculator.Products.Count == 2);
            Assert.IsTrue(calculator.Products.Where(x => ParseIdentifier(x).ServiceCalculatorType == ServiceCalculatorType.Esdl).Count() == 1);
            Assert.IsTrue(calculator.Products.Where(x => ParseIdentifier(x).ServiceCalculatorType == ServiceCalculatorType.Recognition).Count() == 1);
        }

        private ServiceCalculatorIdentifier ParseIdentifier(InvoiceProductCalculatorDc product)
        {
            return ServiceCalculatorIdentifier.Parse(product.Identifier);
        }
    }
}

