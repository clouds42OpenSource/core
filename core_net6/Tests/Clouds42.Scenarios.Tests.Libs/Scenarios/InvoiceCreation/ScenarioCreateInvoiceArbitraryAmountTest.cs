﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Тест на создания инвойса на основании калькуляции
    /// </summary>
    public class ScenarioCreateInvoiceArbitraryAmountTest : ScenarioBase
    {
        readonly IInvoiceCreationManager _invoiceCreationManager;

        public ScenarioCreateInvoiceArbitraryAmountTest()
        {

            _invoiceCreationManager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCreationManager>();
        }

        /// <inheritdoc/>
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.Account;

            if (account == null)
                throw new InvalidOperationException("Аккаунт не найден или не был создан");

            var model = new CreateArbitraryAmountInvoiceDto
            {
                AccountId = account.Id,
                Amount = 100,
                Description = "Custom Description"
            };

            var invoiceCreateResult = _invoiceCreationManager.CreateArbitraryAmountInvoice(model);
            Assert.IsFalse(invoiceCreateResult.Error, invoiceCreateResult.Message);
            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(x => x.Id == invoiceCreateResult.Result);
            Assert.IsNotNull(invoice);
        }
    }
}

