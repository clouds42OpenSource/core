﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Тест получаения данных кальлкулятора для подписочных сервисво
    /// </summary>
    public class ScenarioGetInvoiceCalculatorForSubscriptionServicesForVipAccount : ScenarioBase
    {
        private readonly IInvoiceCalculatorManager _manager;

        public ScenarioGetInvoiceCalculatorForSubscriptionServicesForVipAccount()
        {
            _manager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCalculatorManager>();
        }

        /// <inheritdoc/>
        public override void Run()
        {
            var additionalResourceCost = 900;
            var additionalResourceName = "My Additional Resource";

            var dbLayer = TestContext.DbLayer;
            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(a => a.Id == account.AccountId);            

            TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
                .UpdateSignVipAccount(account.AccountId, true);

            billingAccount.AdditionalResourceName = additionalResourceName;
            billingAccount.AdditionalResourceCost = additionalResourceCost;
            dbLayer.BillingAccountRepository.Update(billingAccount);
            dbLayer.Save();


            var managerResult = _manager.GetInvoiceCalculatorForAccount(account.AccountId, InvoicePurposeType.BaseBillingServices);
            var calculator = managerResult?.Result;


            Assert.IsNotNull(calculator);

            ///Проверяем что в продуктах есть записи
            Assert.IsTrue(calculator.Products.Any());

            ///Проверяем что в продуктах нет записией для сервисов Fasta (Esdl, Recongition)
            Assert.IsFalse(calculator.Products.Any(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.Esdl));
            Assert.IsFalse(calculator.Products.Any(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.Recognition));

            var addiitonalResourcesProducts = calculator
                .Products
                .Where(x => x.ParseIdentifier().ServiceCalculatorType == ServiceCalculatorType.AdditionalResources)
                .ToList();
            Assert.IsTrue(addiitonalResourcesProducts.Count == 1);
            var additionalResourceProduct = addiitonalResourcesProducts.First();
            Assert.AreEqual(additionalResourceProduct.Description, additionalResourceName);
            Assert.AreEqual(additionalResourceProduct.Rate, additionalResourceCost);
        }
    }
}

