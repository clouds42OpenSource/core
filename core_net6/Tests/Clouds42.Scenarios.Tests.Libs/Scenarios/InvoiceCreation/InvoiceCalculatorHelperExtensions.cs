﻿using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Helper расширения для тестов калькулятора инвойсов
    /// </summary>
    public static class InvoiceCalculatorHelperExtensions
    {
        /// <summary>
        /// Расарсить идентификатор продукта инвойса
        /// </summary>
        /// <param name="product">Модель продукта</param>
        /// <returns>Идентификатор продукта</returns>
        public static ServiceCalculatorIdentifier ParseIdentifier(this InvoiceProductCalculatorDc product)
        {
            return ServiceCalculatorIdentifier.Parse(product.Identifier);
        }
    }
}

