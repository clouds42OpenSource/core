﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceCreation
{
    /// <summary>
    /// Тест на создания инвойса на основании калькуляции
    /// </summary>
    public class ScenarioCreateInvoiceFromCalculationTest : ScenarioBase
    {
        readonly IInvoiceCreationManager _invoiceCreationManager;

        public ScenarioCreateInvoiceFromCalculationTest()
        {
            _invoiceCreationManager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCreationManager>();
        }

        /// <inheritdoc/>
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.Account;

            if (account == null)
                throw new InvalidOperationException("Аккаунт не найден или не был создан");

            var esdlRate = TestContext.DbLayer.RateRepository
                 .FirstOrDefault(r =>
                     r.BillingServiceType.Service.SystemService == Clouds42Service.Esdl &&
                     r.BillingServiceType.SystemServiceType.HasValue);

            var rec1000Rate = TestContext.DbLayer.RateRepository
                .Where(r =>
                    r.BillingServiceType.Service.SystemService == Clouds42Service.Recognition &&
                    r.BillingServiceType.SystemServiceType == ResourceType.Rec1000)
                .First();

            var model = new InvoiceCalculationDto
            {
                AccountId = account.Id,
                InvoicePurposeType = InvoicePurposeType.AdditionalBillingServices,
                Products =
                [
                    new()
                    {
                        Identifier = $"{(int)ServiceCalculatorType.Recognition}:",
                        Quantity = 1000,
                        Rate = rec1000Rate.Cost / 1000,
                        TotalPrice = rec1000Rate.Cost
                    },

                    new()
                    {
                        Identifier = $"{(int)ServiceCalculatorType.Esdl}:",
                        Quantity = 1,
                        Rate = esdlRate.Cost,
                        TotalPrice = esdlRate.Cost,
                        TotalBonus = 0
                    }
                ],
                TotalAfterVAT = rec1000Rate.Cost + esdlRate.Cost,
                TotalBeforeVAT = rec1000Rate.Cost + esdlRate.Cost,
                TotalBonus = 0,
                VAT = 0
            };

            var invoiceCreateResult = _invoiceCreationManager.CreateInvoiceFromCalculation(model);
            Assert.IsFalse(invoiceCreateResult.Error, invoiceCreateResult.Message);
            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(x => x.Id == invoiceCreateResult.Result);
            Assert.IsNotNull(invoice);
        }
    }
}

