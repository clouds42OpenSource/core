﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentsManagerTests
{
    /// <summary>
    /// Тест подготовки и получения модели платежа Robokassa
    /// Сценарий:
    /// 
    /// 1) Создаем аккаунт [CreateAccountAndPrepareData]
    /// 
    /// 2) Вызываем метод подготовки и получения модели платежа Robokassa [GetPaymentModel]
    /// 
    /// 3) Проверяем, что был создан платеж в статусе "Ожидает оплаты" и сумма платежа правильная [CheckPaymentObject]
    /// 
    /// 4) Проверяем, что метод возвращает корректную модель [CheckPaymentModel]
    /// </summary>
    public class GetRobokassaPaymentModelTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;

        public GetRobokassaPaymentModelTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
        }

        public override void Run()
        {
            #region CreateAccountAndPrepareData

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var amount = 100;

            #endregion

            #region GetPaymentModel

            var model = _paymentsManager.GetPaymentModel(createAccountCommand.AccountId, amount,
                GetAccountLocale(createAccountCommand.AccountId).Name);

            #endregion

            #region CheckPaymentObject

            var payment = DbLayer.PaymentRepository.FirstOrDefault();

            Assert.IsNotNull(payment);
            Assert.AreEqual(payment.Sum, amount);
            Assert.AreEqual(payment.OperationTypeEnum, PaymentType.Inflow);
            Assert.AreEqual(payment.Status, PaymentStatus.Waiting.ToString());
            Assert.AreEqual(payment.PaymentSystem, PaymentSystem.Robokassa.ToString());

            #endregion

            #region CheckPaymentModel

            Assert.IsNull(model.ModelForUkrPays);
            Assert.IsFalse(string.IsNullOrEmpty(model.OtherPaymentModels));
            Assert.IsTrue(model.OtherPaymentModels.Contains("robokassa"));
            Assert.IsTrue(
                model.OtherPaymentModels.Contains($"OutSum={amount.ToString("F", CultureInfo.InvariantCulture)}"));
            Assert.IsTrue(model.OtherPaymentModels.Contains($"InvId={payment.Number}"));

            #endregion
        }
    }
}