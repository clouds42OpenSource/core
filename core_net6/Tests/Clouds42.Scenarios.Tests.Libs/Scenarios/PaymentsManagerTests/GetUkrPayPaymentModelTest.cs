﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentsManagerTests
{
    /// <summary>
    /// Тест подготовки и получения модели платежа UkrPay
    /// Сценарий:
    /// 
    /// 1) Создаем аккаунт [CreateAccountAndPrepareData]
    /// 
    /// 2) Вызываем метод подготовки и получения модели платежа UkrPay,
    ///    Проверяем, что метод возвращает корректную модель [GetAndCheckPaymentModel]
    /// 
    /// 3) Проверяем, что был создан платеж в статусе "Ожидает оплаты" и сумма платежа правильная [CheckPaymentObject]
    /// 
    /// </summary>
    public class GetUkrPayPaymentModelTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;

        public GetUkrPayPaymentModelTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
        }

        public override void Run()
        {
            #region CreateAccountAndPrepareData

            var amount = 100;

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = "ru-ua"
            });
            createAccountCommand.Run();

            #endregion

            #region GetAndCheckPaymentModel

            var model = _paymentsManager.GetPaymentModel(createAccountCommand.AccountId, amount,
                GetAccountLocale(createAccountCommand.AccountId).Name);

            Assert.IsNull(model.OtherPaymentModels);
            Assert.IsNotNull(model.ModelForUkrPays.ServiceId);
            Assert.AreEqual(model.ModelForUkrPays.Amount, amount);
            Assert.AreEqual(model.ModelForUkrPays.Encoding.ToLower(), Encoding.UTF8.BodyName);
            Assert.IsNotNull(model.ModelForUkrPays.Order);
            Assert.IsNotNull(model.ModelForUkrPays.RequestUrl);
            Assert.IsNotNull(model.ModelForUkrPays.Successurl);

            #endregion

            #region CheckPaymentObject

            var payment = DbLayer.PaymentRepository.FirstOrDefault();

            Assert.IsNotNull(payment);
            Assert.AreEqual(payment.Sum, model.ModelForUkrPays.Amount);
            Assert.AreEqual(payment.OperationTypeEnum, PaymentType.Inflow);
            Assert.AreEqual(payment.Status, PaymentStatus.Waiting.ToString());
            Assert.AreEqual(payment.PaymentSystem, PaymentSystem.UkrPays.ToString());

            #endregion
        }
    }
}