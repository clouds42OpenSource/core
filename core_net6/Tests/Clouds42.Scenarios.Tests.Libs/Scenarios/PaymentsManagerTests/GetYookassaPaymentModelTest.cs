﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentsManagerTests
{
    /// <summary>
    /// Тест подготовки и получения модели платежа Yookassa
    /// Сценарий:
    /// 
    /// 1) Создаем аккаунт, создаем платежного агрегатора Yookassa для русской локали [CreateAccountAndPrepareData]
    /// 
    /// 2) Вызываем метод подготовки и получения модели платежа Robokassa [GetPaymentModel]
    /// 
    /// 3) Проверяем, что не был создан платеж  [CheckPaymentObject]
    /// 
    /// 4) Проверяем, что метод возвращает корректную модель [CheckPaymentModel]
    /// </summary>
    public class GetYookassaPaymentModelTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;

        public GetYookassaPaymentModelTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
        }

        public override void Run()
        {
            #region CreateAccountAndPrepareData

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var cloudCore = new CloudCore
            {
                HourseDifferenceOfUkraineAndMoscow = null,
                MainPageNotification = nameof(Test),
                PrimaryKey = Guid.NewGuid(),
                RussianLocalePaymentAggregator = RussianLocalePaymentAggregatorEnum.Yookassa
            };

            DbLayer.CloudCoreRepository.Insert(cloudCore);
            DbLayer.Save();

            var amount = 100;

            #endregion

            #region GetPaymentModel

            var model = _paymentsManager.GetPaymentModel(createAccountCommand.AccountId, amount,
                GetAccountLocale(createAccountCommand.AccountId).Name);

            #endregion

            #region CheckPaymentObject

            var payment = DbLayer.PaymentRepository.FirstOrDefault();
            Assert.IsNull(payment);

            #endregion

            #region CheckPaymentModel

            Assert.IsNull(model.ModelForUkrPays);
            Assert.IsFalse(string.IsNullOrEmpty(model.OtherPaymentModels));
            Assert.IsTrue(model.OtherPaymentModels.Contains("PayViaYookassa"));
            Assert.IsTrue(model.OtherPaymentModels.Contains($"totalSum={amount}"));

            #endregion
        }
    }
}