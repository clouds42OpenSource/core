﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentsManagerTests
{
    /// <summary>
    /// Тест подготовки и получения модели платежа Paybox
    /// Сценарий:
    /// 
    /// 1) Создаем аккаунт с казахской локалью [CreateAccountAndPrepareData]
    /// 
    /// 2) Вызываем метод подготовки и получения модели платежа Paybox [GetPaymentModel]
    /// 
    /// 3) Проверяем, что был создан платеж в статусе "Ожидает оплаты" и сумма платежа правильная [CheckPaymentObject]
    /// 
    /// 4) Проверяем, что метод возвращает корректную модель [CheckPaymentModel]
    /// </summary>
    public class GetPayboxPaymentModelTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;

        public GetPayboxPaymentModelTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
        }

        public override void Run()
        {
            #region CreateAccountAndPrepareData

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = "ru-kz"
            });
            createAccountCommand.Run();
            var amount = 100;

            #endregion

            #region GetPaymentModel

            var model = _paymentsManager.GetPaymentModel(createAccountCommand.AccountId, amount,
                GetAccountLocale(createAccountCommand.AccountId).Name);

            #endregion

            #region CheckPaymentObject

            var payment = DbLayer.PaymentRepository.FirstOrDefault();
            Assert.IsNotNull(payment);

            #endregion

            #region CheckPaymentModel

            Assert.IsFalse(string.IsNullOrEmpty(model.OtherPaymentModels));
            Assert.IsNull(model.ModelForUkrPays);
            Assert.IsTrue(model.OtherPaymentModels.Contains("paybox"));
            Assert.IsTrue(model.OtherPaymentModels.Contains($"pg_amount={amount}"));
            Assert.IsTrue(model.OtherPaymentModels.Contains($"pg_order_id={payment.Number}"));

            #endregion
        }
    }
}