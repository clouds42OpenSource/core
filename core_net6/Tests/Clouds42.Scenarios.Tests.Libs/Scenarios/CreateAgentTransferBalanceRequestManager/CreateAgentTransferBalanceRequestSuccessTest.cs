﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAgentTransferBalanceRequestManager
{
    /// <summary>
    /// Сценарий теста проверки успешного создания заявки
    /// на перевод баланса агента
    ///
    /// Создаем аккаунт и начинаем проверку создания заявки на перевод баланса агента,
    /// выполняя кейсы которые должны завершится успешно
    ///
    /// 1) Когда нет заявок на расходование средств
    /// 2) Когда есть заявка на расходование средств, но она оплачена
    /// </summary>
    public class CreateAgentTransferBalanceRequestSuccessTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId) ??
                throw new NotFoundException(
                    $"Не удалось найти пользователя по Id= {createAccountCommand.AccountAdminId}");

            var createAgentTransferBalanceRequestManager =
                ServiceProvider.GetRequiredService<AgencyAgreement.AgentTransferBalanceRequest.Managers.CreateAgentTransferBalanceRequestManager>();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();

            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var createAgentTransferBalanceRequestDto = new CreateAgentTransferBalanseRequestDto
            {
                ToAccountId = createAccountCommand.AccountId,
                FromAccountId = createAccountCommand.AccountId,
                InitiatorId = accountUser.Id,
                Sum = 5
            };

            DbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(createAccountCommand.AccountId);

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = 5,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = 5
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var managerResult = createAgentTransferBalanceRequestManager.Create(createAgentTransferBalanceRequestDto);

            Assert.IsFalse(managerResult.Error, managerResult.Message);


            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = 40000,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = 5
            });

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };
            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(
                new CreateAgentCashOutRequestDto
                {
                    AccountId = createAccountCommand.AccountId,
                    Files = requisitesFiles,
                    TotalSum = 5,
                    RequestStatus = AgentCashOutRequestStatusEnum.New,
                    AgentRequisitesId = agentRequisitesId,
                    PaySum = 5
                });

            var cashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault() ??
                                 throw new InvalidOperationException ("Не удалось найти заявку на расходование средств");

            RefreshDbCashContext(TestContext.Context.AgentWallets);
            var changeAgentCashOutRequestStatusManager = ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();
            changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(new ChangeAgentCashOutRequestStatusDto
            {
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid,
                RequestNumber =  cashOutRequest.RequestNumber,
                Sum = cashOutRequest.RequestedSum
            });

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = 100,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = 5
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);
            managerResult = createAgentTransferBalanceRequestManager.Create(createAgentTransferBalanceRequestDto);

            Assert.IsFalse(managerResult.Error, managerResult.Message);
        }
    }
}
