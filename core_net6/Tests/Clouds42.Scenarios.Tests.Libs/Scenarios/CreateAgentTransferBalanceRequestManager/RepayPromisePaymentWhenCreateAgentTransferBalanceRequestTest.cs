﻿using System;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAgentTransferBalanceRequestManager
{
    /// <summary>
    /// Сценарий теста проверки погашения ОП
    /// при создании заявки на перевод баланса агента
    ///
    /// 1) Создаем аккаунт и создаем агентский платеж
    /// 2) Берем обещанный платеж для аккаунта на сумму агентского платежа
    /// 3) Переводим деньги на баланс аккаунта и проверяем что обещанный платеж погасился и баланс равен 0.
    /// </summary>
    public class RepayPromisePaymentWhenCreateAgentTransferBalanceRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId) ??
                throw new NotFoundException(
                    $"Не удалось найти пользователя по Id= {createAccountCommand.AccountAdminId}");

            var createAgentTransferBalanceRequestManager =
                ServiceProvider.GetRequiredService<AgencyAgreement.AgentTransferBalanceRequest.Managers.CreateAgentTransferBalanceRequestManager>();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            DbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(createAccountCommand.AccountId);

            var defaultSum = 7000;
            
            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentSum = defaultSum;
            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-5);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            var managerResult = createAgentTransferBalanceRequestManager.Create(new CreateAgentTransferBalanseRequestDto
            {
                ToAccountId = createAccountCommand.AccountId,
                FromAccountId = createAccountCommand.AccountId,
                InitiatorId = accountUser.Id,
                Sum = defaultSum
            });

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(Context.BillingAccounts);

            billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.AreEqual(0, billingAccount.PromisePaymentSum);
            Assert.AreEqual(0, billingAccount.Balance);
        }
    }
}
