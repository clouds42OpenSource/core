﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAgentTransferBalanceRequestManager
{
    /// <summary>
    /// Сценарий теста проверки покупки/разблокировки заблокированных сервисов аккаунта
    /// при создании заявки на перевод баланса агента
    ///
    /// 1) Создаем сервис, который зависит от Аренды Стандарт.
    /// 2) Создаем аккаунт и подключаем ему этот сервис
    /// 3) Убираем демо период сервисам и блокируем Анренду 1С
    /// 4) Создаем агентский платеж на сумму сервисов
    /// 5) Переводим деньги на баланс аккаунта и проверяем что сервисы разблокированны и баланс равен 0
    /// 6) Проверяем что начисленны лицензии в рамках активного сервиса Аренда 1С по Recognition
    /// </summary>
    public class UnlockServicesWhenCreateAgentTransferBalanceRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            serviceTypeDtos[0].DependServiceTypeId = GetStandardServiceTypeOrThrowException().Id;

            var createAgentTransferBalanceRequestManager =
                ServiceProvider.GetRequiredService<AgencyAgreement.AgentTransferBalanceRequest.Managers.CreateAgentTransferBalanceRequestManager>();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });

            createCustomService.Run();

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });

            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>{
                new()
                {
                    Subject = createAccountCommand.AccountAdminId,
                    Status = true,
                    BillingServiceTypeId = serviceTypeDtos[0].Id,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        I = false,
                        Me = false,
                        Label = billingAccount.Account.AccountCaption
                    }
                }};
            
            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, createCustomService.Id,
                false, null, dataAccountUsers);

            var resConf1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise && rc.AccountId == createAccountCommand.AccountId);

            var previousDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId != createCustomService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, previousDate).SetProperty(y => y.Frozen, true));
            Context.ChangeTracker.Clear();

            var customBillingService =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                    w => w.BillingServiceId == createCustomService.Id);
            customBillingService.IsDemoPeriod = false;
            customBillingService.ExpireDate = null;
            customBillingService.Frozen = null;
            DbLayer.ResourceConfigurationRepository.Update(customBillingService);
            DbLayer.Save();

            DbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(createAccountCommand.AccountId);

            var servicesCost = resConf1C.Cost + serviceTypeDtos[0].ServiceTypeCost;

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = servicesCost,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = servicesCost
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var managerResult = createAgentTransferBalanceRequestManager.Create(new CreateAgentTransferBalanseRequestDto
            {
                ToAccountId = createAccountCommand.AccountId,
                FromAccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                Sum = servicesCost
            });

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(Context.BillingAccounts);
            RefreshDbCashContext(Context.ResourcesConfigurations);
            RefreshDbCashContext(Context.FlowResourcesScope);

            billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            resConf1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise && rc.AccountId == createAccountCommand.AccountId);

            var esdlConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.Esdl && rc.AccountId == createAccountCommand.AccountId);

            var rec42ResourcesCount = ServiceProvider.GetRequiredService<IFlowResourcesScopeDataProvider>()
                .GetScopeValue(createAccountCommand.AccountId,
                    CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId()).Result;

            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resConf1C.FrozenValue);
            Assert.IsNotNull(resConf1C.ExpireDate);
            Assert.IsNotNull(esdlConf.ExpireDate);
            Assert.AreEqual(resConf1C.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date);
            Assert.AreEqual(esdlConf.ExpireDate.Value.Date, resConf1C.ExpireDate.Value.Date);
            Assert.AreEqual(rec42ResourcesCount,
                CloudConfigurationProvider.BillingServices.GetOptimalLimitRc42ResourceForActiveRent1C()/* +
                ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount")*/); //TODO: Разобраться почему сумма лицензий активной аренды и демо, DocLoaderByRent1CProlongationProcessor.Rec42Process добавляет разницу между рекомендуемой и текущим значением, если было 20 а добавляем рекомендуемые 1000 то будет 980 + текущие 20 но никак не 1020.
        }
    }
}
