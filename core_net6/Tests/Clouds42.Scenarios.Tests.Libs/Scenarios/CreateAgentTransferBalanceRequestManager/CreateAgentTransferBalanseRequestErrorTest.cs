﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAgentTransferBalanceRequestManager
{
    /// <summary>
    /// Сценарий теста проверки ошибочного создания заявки
    /// на перевод баланса агента
    ///
    /// Создаем аккаунт и начинаем проверку создания заявки на перевод баланса агента,
    /// выполняя кейсы которые должны завершится с ошибкой
    ///
    /// 1) Отправляем модель, не указывая Id аккаунта
    /// 2) Для агента не создан кошелек
    /// 3) У агента не достаточно средств для списания баланса
    /// 4) Не указан инициатор перевода
    /// 5) Когда есть заявка на расходование средств
    /// </summary>
    public class CreateAgentTransferBalanseRequestErrorTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId) ??
                throw new NotFoundException(
                    $"Не удалось найти пользователя по Id= {createAccountCommand.AccountAdminId}");

            var createAgentTransferBalanseRequestManager =
                ServiceProvider.GetRequiredService<AgencyAgreement.AgentTransferBalanceRequest.Managers.CreateAgentTransferBalanceRequestManager>();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();

            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var createAgentTransferBalanseRequestDto = new CreateAgentTransferBalanseRequestDto();

            var managerResult = createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanseRequestDto);

            Assert.IsTrue(managerResult.Error, "Не заполнина таблица создания заявки");
            Assert.IsFalse(CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest());

            createAgentTransferBalanseRequestDto.ToAccountId = createAccountCommand.AccountId;
            createAgentTransferBalanseRequestDto.FromAccountId = createAccountCommand.AccountId;
            createAgentTransferBalanseRequestDto.InitiatorId = accountUser.Id;
            createAgentTransferBalanseRequestDto.Sum = 5;

            managerResult = createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanseRequestDto);

            Assert.IsTrue(managerResult.Error, "У агента нет кошелька, проверки баланса");
            Assert.IsFalse(CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest());

            DbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(createAccountCommand.AccountId);

            managerResult = createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanseRequestDto);

            Assert.IsTrue(managerResult.Error, "У агента не достаточно средств для списания баланса.");
            Assert.IsFalse(CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest());

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = 5,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = 5
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);
            createAgentTransferBalanseRequestDto.InitiatorId = Guid.Empty;

            managerResult = createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanseRequestDto);

            Assert.IsTrue(managerResult.Error, "Не указан инициатор перевода");
            Assert.IsFalse(CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest());

            createAgentTransferBalanseRequestDto.InitiatorId = accountUser.Id;

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };
            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(
                new CreateAgentCashOutRequestDto
                {
                    AccountId = createAccountCommand.AccountId,
                    Files = requisitesFiles,
                    TotalSum = 5,
                    RequestStatus = AgentCashOutRequestStatusEnum.New,
                    AgentRequisitesId = agentRequisitesId,
                    PaySum = 5
                });


            managerResult = createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanseRequestDto);

            Assert.IsTrue(managerResult.Error, "Нельзя создавать заявку на перевод, когда есть заявка на расходование средств.");
            Assert.IsFalse(CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest());
        }

        /// <summary>
        /// Проверка объектов в базе
        /// при попытке создания заявки на перевод баланса агента
        /// </summary>
        /// <returns>Наличие одного из объектов</returns>
        private bool CheckDbObjWhenTryingToCreateAgentTransferBalanseRequest() =>
            DbLayer.PaymentRepository.FirstOrDefault() != null ||
            DbLayer.AgentPaymentRepository.FirstOrDefault(ap => ap.PaymentType != PaymentType.Inflow) != null ||
            DbLayer.AgentTransferBalanceRequestRepository.FirstOrDefault() != null;
    }
}
