﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Создаем аккаунт 1 с 1 пользователем у которого просроченная арендой 1С.
    /// Создаем аккаунт 2 с деньгами на счету.
    /// Спонсируем аккаунтом 2 аккаунт 1.
    /// Убеждаемся что аренда у аккаунта 1 разблокируется.
    /// </summary>
    public class ActiveRentBySponsorLicenseTest : ScenarioBase
    {
        public override void Run()
        {
            var firstAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1)
            });
            firstAccountCommand.Run();

            CreateInflowPayment(firstAccountCommand, 1);

            var cloud42ServiceFactory = ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            var rent1CService = (MyEnterprise42CloudService)cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, firstAccountCommand.AccountId);

            rent1CService.ActivateService();

            DisableRentForUser(firstAccountCommand.AccountAdminId);
            
            var rent1CResourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == firstAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsNotNull(rent1CResourceConfig, "Не активировался сервис Аренда 1С");

            rent1CResourceConfig.ExpireDate = DateTime.Now.AddDays(-1);
            rent1CResourceConfig.Frozen = true;
            rent1CResourceConfig.Cost = 0;

            DbLayer.ResourceConfigurationRepository.Update(rent1CResourceConfig);
            DbLayer.Save();

            var secondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestLogin{DateTime.Now:mmss}",
                Email = "TestEmail22@efsol.ru",
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1)
            });
            secondAccountCommand.Run();

            rent1CService = (MyEnterprise42CloudService)cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, secondAccountCommand.AccountId);
            rent1CService.ActivateService();

            var billingAccount =
                TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(a =>
                    a.Id == secondAccountCommand.AccountId);

            var rateProvider = ServiceProvider.GetRequiredService<IRateProvider>(); 
            var myEntUserWeb = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
            var cost = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id).Cost;

            CreateInflowPayment(secondAccountCommand, cost);

            rent1CService.ConfigureAccesses([
                new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = firstAccountCommand.AccountAdminId,
                    WebResource = true,
                    SponsorAccountUser = true
                }
            ]);
            
            rent1CResourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == firstAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.AreEqual(false,rent1CResourceConfig.FrozenValue, "Бесплатный сервис спонсируемого пользователя должен быть разблокирован.");
        }

        private void CreateInflowPayment(CreateAccountCommand account, decimal cost)
        {            

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<BillingServiceDataProvider>();

            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.AccountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }

        private void DisableRentForUser(Guid accountAdminId)
        {
            var resources = DbLayer.ResourceRepository.Where(r => r.Subject == accountAdminId).ToList().Select(x =>
            {
                x.Subject = null;
                return x;
            }).ToList();

            DbLayer.BulkUpdate(resources);
            DbLayer.Save();
        }
    }
}
