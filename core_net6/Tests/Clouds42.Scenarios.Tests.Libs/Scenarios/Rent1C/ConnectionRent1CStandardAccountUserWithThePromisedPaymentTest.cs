﻿using Clouds42.AccountUsers.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий подключения нового пользователя при недостаточном балансе при помощи ОП
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем второго пользователя и пытаемся подключить его к Аренде(Стандарт)
    /// 
    ///     Проверка: пользователь должен быть подключен, сумма списана с баланса, сервис подорожал
    ///     но дата остается прежней, пользователь состоит в группах АД, есть ОП
    /// </summary>
    public class ConnectionRent1CStandardAccountUserWithThePromisedPaymentTest : ScenarioBase
	{
		public override void Run()
		{
			var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
			{
				RegistrationConfig = new RegistrationConfigDomainModelDto
				{
					CloudService = Clouds42Service.MyEnterprise
				}
			});
			createAccountCommand.Run();

            var billingService = GetRent1CServiceOrThrowException();

			TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountCommand.AccountId, new Rent1CServiceManagerDto
			{
				ExpireDate = DateTime.Now.AddDays(-1)
			});

            var paymentSum = 1500;
			CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, paymentSum);
			
			var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

			resourcesConfiguration.ExpireDate = dateValue;
			resourcesConfiguration.Frozen = false;
			DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
			DbLayer.Save();

			var billingAccount =
				DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

			Assert.IsNotNull(billingAccount);

			var newUserModel = new AccountUserDcTest
			{
				AccountCaption = "Test caption",
				AccountId = createAccountCommand.AccountId,
				Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
				Password = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric()).GeneratePassword(),
				FirstName = nameof(Test),
				LastName = nameof(Test),
				Email = $"{Guid.NewGuid():N}@efsol.ru",
				PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
			};

			var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;
			var accessHelper = TestContext.ServiceProvider.GetRequiredService<IRent1CAccessHelper>();

			var costForUser = accessHelper.GetAccountBillingDataForBuyRent1C(accountUserId);
			var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
			var result = rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
			billingAccount =
				DbLayer.BillingAccountRepository.FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
			
			Assert.IsTrue(result.Result);
			Assert.IsNotNull(billingAccount);
			Assert.IsNotNull(resourceRent1C);
			Assert.IsNotNull(billingAccount.PromisePaymentSum);
			Assert.AreEqual(costForUser.PartialCostOfStandartLicense, billingAccount.PromisePaymentSum);
			Assert.AreEqual(0, billingAccount.Balance);
			Assert.AreEqual(false, resourceRent1C.FrozenValue);
			Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
			Assert.AreEqual(3000, resourceRent1C.Cost);
        }
	}
}