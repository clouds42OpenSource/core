﻿using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий блокировки сервиса Аренда 1С и проверки того
    /// что выводится сообщение о блокировке
    ///  
    ///     Предусловия: для аккаунта test_account подключен пользователь test_user_1
    /// по тарифу Стандарт; баланс 0 руб; дата окончания сервиса (+7) дней от тек.даты,
    /// взят ОП который истекает сегодня.
    /// 
    ///     Действия: проверяем что сервис заблокирован и флаг о блокировке ресурса
    /// в статусе true
    /// 
    /// </summary>
    public class LockRent1CWithPromisePaymentTest : ScenarioBase
    {
        private const int TariffSize = 11;

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountCommand.AccountId, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-1)
            });
            
            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();

            var costOfTariff = resourcesManager.TryChangeMyDiskTariff(createAccountCommand.AccountId, TariffSize).Result.CostOftariff;

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(
                createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
                {
                    SuggestedPayment = new SuggestedPaymentModelDto {PaymentSum = costOfTariff}
                }).Result;

            Assert.IsTrue(promisePayment);

            // Тратим деньги
            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = TariffSize, ByPromisedPayment = false };
            var result = resourcesManager.ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsTrue(result.Result);

            // Устанавливаем дату ОП
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-10);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            // Блочим сервис так как ОП не погашен
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProcessExpiredPromisePayments();

            Assert.IsFalse(processRes.Error);

            // Проверяем статус блокировки который передается на View
            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();

            var resultService = rent1CManager.GetRent1CInfo(createAccountCommand.AccountId);

            RefreshDbCashContext(Context.ResourcesConfigurations);

            var resourceRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsFalse(resultService.Error);
            Assert.IsNotNull(resultService.Result);
            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsTrue(resultService.Result.ServiceStatus.ServiceIsLocked);
            Assert.IsTrue(resultService.Result.ServiceStatus.PromisedPaymentIsActive);
        }
    }
}
