﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Проверка работы продления дэмо-периода.
    /// Действия: создаём аккаунт,подключаем к аренде,
    /// делаем недемо. Пытаемся продлить дэмо-период
    /// Проверяем: в продлении отказано
    /// </summary>
    public class ScenarioRent1CDemoProlongationButTooLate : ScenarioBase
    {
        private readonly ResourcesManager _resourcesManager;

        public ScenarioRent1CDemoProlongationButTooLate()
        {
            _resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountCommand.AccountId, createAccountCommand.AccountAdminId);

            if (result.Error || !string.IsNullOrEmpty(result.Message))
                throw new InvalidOperationException("Подключение аренды не возможно. ");


            var newPayment = new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                OriginDetails = "Name",
                Id = Guid.NewGuid(),
                OperationType = PaymentType.Inflow,
                System = PaymentSystem.Admin,
                Status = PaymentStatus.Done,
                Description = "",
                Total = 10000
            };
            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var paymentState = paymentsManager.AddPayment(newPayment);
            if (paymentState.Error || paymentState.Result != PaymentOperationResult.Ok)
                throw new InvalidOperationException ("Ошибка внесения платежа платежа");

            var futureExpDate = DateTime.Now.AddDays(7);
            var rentModelAcc = _resourcesManager.GetRent1DemoPeriodInfo(createAccountCommand.AccountId);
            rentModelAcc.Result.ExpireDate = futureExpDate;
            var resultAcc = _resourcesManager.ChangeRent1CDemoPeriod(createAccountCommand.AccountId, rentModelAcc.Result);

            Assert.IsTrue(resultAcc.Error, "Продление для недемо аккаунта должно вернуть ошибку");
            Assert.IsFalse(string.IsNullOrEmpty(resultAcc.Message), "Продление для недемо аккаунта должно вернуть ошибку");

        }
    }
}
