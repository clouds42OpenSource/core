﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppCommon.Access;
using AppCommon.DataManagers;
using AppCommon.DataManagers.AccountDatabases.Internal;
using AppCommon.DataManagers.AccountUsers;
using AppCommon.DataManagers.Resources;
using AppCommon.Helpers;
using AppCommon.Models.ControlPanelModels.Rent1CServicesModel;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Encrypt.Hashes;
using DataContracts.Models.AcDbAccessesModels;
using DataContracts.Models.RegistrationModels;
using Repositories;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С стандарт для спонсируемого аккаунта
    /// </summary>
    public class ConnectionRent1CStandartAccountUserTest : ScenarioBase
    {
        public const string RemoteDesktopAccess = "Remote Desktop Access";
        public override void Run()
        {
            var fakeAccessProvider = new FakeAccessProvider();
            var testContext = new TestContext(fakeAccessProvider, DbLayer);
            //добавление аккаунтa с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(testContext);
            createAccountDatabaseAndAccountCommand.Run();
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId)
            };
            var res = new AccountUsersManager(fakeAccessProvider, DbLayer).AddToAccount(secondUser);
            if(res== null)
                throw new Exception("Ошибка создания нового пользователя");
            var resourcesManager = new ResourcesManager(fakeAccessProvider, DbLayer);

            var accessRent1C = new List<UpdaterAccessRent1CRequestDm>
            {
                new UpdaterAccessRent1CRequestDm
                {
                    AccountUserId = res.Result,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            var dbAccess =  new  AcDbAccessPostAddModelDTO
                {
                    AccountDatabaseID = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                    AccountID = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                    LocalUserID = Guid.Empty,
                    AccountUserID = res.Result,
                    SendNotification = true
                };
            var billing =
                DbLayer.BillingAccountRepository.GetBillingAccount(createAccountDatabaseAndAccountCommand.AccountDetails
                    .AccountId);
            //есть деньги на счету
            billing.Balance = 400;
            DbLayer.BillingAccountRepository.Update(billing);
            DbLayer.Save();
            //подключение аренды 1С для аккаунта
            resourcesManager.TryPayAccessByPromisePaymentForRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            //Добавить доступ к базе
            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.Id == res.Result);
            var result = new AcDbAccessesManager(fakeAccessProvider, DbLayer).Add(dbAccess);
            if (result.Error || result.State != ManagerResultState.Ok)
                throw new Exception("Ошибка предоставления доступа к базе");

            //проверка 
            var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                where access.AccountUserID == res.Result
             select access).ToList();

            if (accessList.Any(t => t.IsLock))
                throw new Exception("Ошибка спонсирования аренды 1С");

            var groupName = $"company_{accountUser.Account.IndexNumber}_web";
            var adManager = new AppCommon.Helpers.AdManager();

            var userExist = adManager.ExistUserAtGroup(accountUser.Login, groupName);
            if (userExist == false)
                throw new Exception($"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");
        }
    }
}
