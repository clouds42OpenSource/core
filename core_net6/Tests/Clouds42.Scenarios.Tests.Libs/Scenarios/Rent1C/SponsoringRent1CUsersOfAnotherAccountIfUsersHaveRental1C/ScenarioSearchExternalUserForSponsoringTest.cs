﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1C
{
    /// <summary>
    /// Сценарий теста на поиск внешнего пользователя для спонсирования
    /// Создаем 2 аккаунта с пользователями, выполняем метод поиска внешнего пользователя
    /// для спонсирования. При наличии лицензий у пользователя, должен вернуться пользователь.
    /// Если у пользователя лицензии спонсорские должен вернуться алерт о том что пользователь уже проспонсирован
    /// </summary>
    public class ScenarioSearchExternalUserForSponsoringTest : ScenarioBase
    {
        private readonly TestDataGenerator _generator;

        public ScenarioSearchExternalUserForSponsoringTest()
        {
            _generator = new TestDataGenerator(Configuration, DbLayer);
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс, что бы аккаунт не считался демо",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 900,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var emailForSearching = DbLayer.AccountUsersRepository.FirstOrDefault(w =>
                w.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId).Email;


            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();

            var result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (!string.IsNullOrEmpty(result.Result.ErrorMessage) || result.Result.User == null)
                throw new InvalidOperationException("Пользователя можно проспонсировать, так как у него нет спонсорских лицензий");

            var accountSponsor = _generator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(accountSponsor);
            DbLayer.Save();

            var resourcesUserFirstAccount = DbLayer.ResourceRepository.Where(w =>
                w.Subject == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId).ToList();

           var forUpdate = resourcesUserFirstAccount.Select(w =>
            {
                w.AccountSponsorId = accountSponsor.Id;
                return w;
            }).ToList();

            DbLayer.BulkUpdate(forUpdate);
            DbLayer.Save();

            result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (string.IsNullOrEmpty(result.Result.ErrorMessage) || result.Result.User != null)
                throw new InvalidOperationException("Нельзя проспонсировать пользователя, так как у пользователя уже имеются спонсорские лицензии");
        }
    }
}
