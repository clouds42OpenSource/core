﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Providers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1C
{
    /// <summary>
    /// Сценарий теста спонсирования арендой 1С пользователей другого аккаунта
    /// при наличии у пользователей аренды 1С
    /// Создаем 2 аккаунта и выполняем спонсирование пользователю, у которого активна аренда
    /// Те лицензии, которые использует спонсируемый должно освободиться а спонсорские должны привязаться к пользователю
    /// </summary>
    public class ScenarioSponsoringRent1CUsersOfAnotherAccountIfUsersHaveRental1CTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 900,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = true
                }
            };

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var result = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            if (!result.Result.Complete)
                throw new InvalidOperationException ("Ошибка. Спонсирование может произойти, даже если аренда у пользователя подключена");

            var resourcesUserFirstAccount = DbLayer.ResourceRepository.Where(w =>
                w.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId &&
                w.Subject == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId &&
                (w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb || w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser) &&
                w.AccountSponsorId == createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId).ToList();

            if (!resourcesUserFirstAccount.Any() || resourcesUserFirstAccount.Count != 2)
                throw new InvalidOperationException("Ошибка. У пользователя должны быть только спонсорские ресурсы");
        }
    }
}
