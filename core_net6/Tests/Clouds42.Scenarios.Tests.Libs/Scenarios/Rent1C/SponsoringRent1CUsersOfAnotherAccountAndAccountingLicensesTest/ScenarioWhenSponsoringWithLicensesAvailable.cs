﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountAndAccountingLicensesTest
{
    /// <summary>
    /// Сценарий теста спонсирования при наличии свободных лицензий
    /// </summary>
    public class ScenarioWhenSponsoringWithLicensesAvailable : ScenarioBase
    {
        public override void Run()
        {
            //добавление двух аккаунтов с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                }
            };

            // отключаем аренду первому аккаунту
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            var resourses = DbLayer.ResourceRepository.Where(w =>
                w.AccountId == createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId &&
                (w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser || w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb) && w.Subject == null).ToList();

            accessRent1C.First().StandartResource = true;
            accessRent1C.First().RdpResourceId = resourses.FirstOrDefault(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser).Id;
            accessRent1C.First().WebResourceId = resourses.FirstOrDefault(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb).Id;

            //подключение аренды 1С для спонсируемого аккаунта
            var res = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            if (!res.Result.Complete)
                throw new InvalidOperationException ("Ошибка. Для спонсирования имеются свободные ресурсы");
        }
    }
}
