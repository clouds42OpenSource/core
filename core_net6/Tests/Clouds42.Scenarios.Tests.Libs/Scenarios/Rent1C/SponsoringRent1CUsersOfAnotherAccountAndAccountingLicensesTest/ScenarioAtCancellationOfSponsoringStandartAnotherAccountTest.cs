﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Billing.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Providers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.SponsoringRent1CUsersOfAnotherAccountAndAccountingLicensesTest
{
    /// <summary>
    /// Сценарий теста при отмене спонсирования "Стандарта" у другого аккаунта
    /// Создаем 2 аккаунта и выполняем спонсирование и отмену спонсирования
    /// сначала отменяет спонсор, потом отменяет спонсируемый, лицензии должны вернуться в любом случаи
    /// </summary>
    public class ScenarioAtCancellationOfSponsoringStandartAnotherAccountTest : ScenarioBase
    {
        private readonly Rent1CConfigurationAccessManager _accessManager;
        private readonly Rent1CConfigurationAccessManager _rent1CConfigurationAccessManager;

        public ScenarioAtCancellationOfSponsoringStandartAnotherAccountTest()
        {
            _accessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
        }

        public override void Run()
        {
            var resourcesHelper = new ResourcesHelper(DbLayer);

            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 1500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                }
            };

            _accessManager.TurnOnRent1C(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            var freeRes = DbLayer.ResourceRepository.Where(r =>
                r.Subject == null && (r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
                                      r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser));

            DbLayer.ResourceRepository.DeleteRange(freeRes);
            DbLayer.Save();


            _rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            accessRent1C.First().StandartResource = true;
            
            _rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);
                
            accessRent1C.First().StandartResource = false;

            _rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            var resourceAtFirstAccount = resourcesHelper.GetListResourcesRental1CForAccount(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            var resourceAtSecondAccount =
                resourcesHelper.GetListResourcesRental1CForAccount(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, null, true);

            resourceAtSecondAccount = resourceAtSecondAccount.ToList();

            if (resourceAtFirstAccount.Any() || !resourceAtSecondAccount.Any())
                throw new InvalidOperationException("Ошибка. При отмене спонсором спонсирования, лицензии должны вернуться спонсору");
            
            accessRent1C.First().StandartResource = true;
            accessRent1C.First().RdpResourceId = resourceAtSecondAccount
                .FirstOrDefault(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)?.Id;
            accessRent1C.First().WebResourceId = resourceAtSecondAccount
                .FirstOrDefault(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)?.Id;

            _rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            resourcesHelper.GetListResourcesRental1CForAccount(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            accessRent1C.First().StandartResource = false;
            accessRent1C.First().WebResourceId = null;
            accessRent1C.First().RdpResourceId = null;
            
            _rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            resourceAtFirstAccount = resourcesHelper.GetListResourcesRental1CForAccount(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            resourceAtSecondAccount =
                resourcesHelper.GetListResourcesRental1CForAccount(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, null, true);

            resourceAtSecondAccount = resourceAtSecondAccount.ToList();

            if (resourceAtFirstAccount.Any() || !resourceAtSecondAccount.Any())
                throw new InvalidOperationException("Ошибка. При отмене проспонсированинном пользователе спонсирования, лицензии должны вернуться спонсору");
        }
    }
}
