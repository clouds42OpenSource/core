﻿using System;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий продления аренды 1С при наличии средств и ОП 
    /// 
    ///  
    ///     Предусловия: для аккаунта test_account подключен пользователь test_user_1
    /// по тарифу Стандарт; баланс 1500 руб; дата окончания сервиса - сейчас,
    /// взят ОП  на сумму 1500. Дата окончания периода ОП совпадает с
    /// датой окончания сервиса 1С.
    /// 
    ///     Действия: проверяем что сервис продлён, оп не погашен, баланс 0 и флаг о блокировке ресурса
    /// в статусе false
    /// 
    /// </summary>
    public class LockRent1CWithPromisePaymentAndExpiredRent1CwithNoMoney : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();

            var promisePayment = accountManager.CreatePromisePayment(createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
            {
                SuggestedPayment = new SuggestedPaymentModelDto
                {
                    PaymentSum = 1500
                }
            });

            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            //устанавливаем дату окончания Аренды 1С
            var rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAccountCommand.AccountId && r.BillingService.SystemService==Clouds42Service.MyEnterprise);
            rent1C.ExpireDate = DateTime.Now.AddHours(-1);

            // Устанавливаем дату ОП
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount, $"Не удалось найти аккаунт биллинга по Id = {createAccountCommand.AccountId}");

            billingAccount.PromisePaymentDate = rent1C.ExpireDate;
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            //симулируем таску Cloud42ServicePrologationJob
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processResProm = billingManager.ProcessExpiredPromisePayments();
            var processResServ = billingManager.ProlongOrLockExpiredServices();

            Assert.IsFalse(processResProm.Error, processResProm.Message);
            Assert.IsFalse(processResServ.Error, processResServ.Message);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);

            var resultService = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().GetRent1CInfo(createAccountCommand.AccountId);

            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);


            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsFalse(resultService.Error, resultService.Message);
            Assert.IsNotNull(resultService.Result);
            Assert.IsNotNull(resourceRent1C, $"Не удалось найти ресурсы Аренды 1С для аккаунта {createAccountCommand.AccountId}");
            Assert.IsNotNull(billingAcc.PromisePaymentSum, "Сумма обещанного платежа не должна быть null");
            Assert.AreEqual(0, billingAcc.Balance, "Баланс должнен быть 0");
            Assert.AreEqual(1500, billingAcc.PromisePaymentSum, "Баланс должнен быть 1500");
            Assert.IsFalse(resourceRent1C.FrozenValue, "Ресурсы Аренды 1С не должны быть заблокированы");
            Assert.IsFalse(resultService.Result.ServiceStatus.ServiceIsLocked, "Сервис Аренда 1С должен быть разблокирован");
            Assert.IsTrue(resultService.Result.ServiceStatus.PromisedPaymentIsActive, "Обещанный платеж доложен быть активным");
        }
    }
}
