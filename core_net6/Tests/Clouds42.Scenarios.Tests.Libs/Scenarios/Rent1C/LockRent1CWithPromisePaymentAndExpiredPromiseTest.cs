﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий блокировки сервиса Аренда 1С и проверки того
    /// что выводится сообщение о блокировке
    ///  
    ///     Предусловия: для аккаунта test_account подключен пользователь test_user_1
    /// по тарифу Стандарт; баланс 4500 руб; дата окончания сервиса(+10) от текущего дня,
    /// взят ОП  на сумму 4500 который истекает сегодня.
    /// 
    ///     Действия: проверяем что сервис заблокирован, оп погашен, баланс 0 и флаг о блокировке ресурса
    /// в статусе true
    /// 
    /// </summary>
    public class LockRent1CWithPromisePaymentAndExpiredPromiseTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(
                createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
                {
                    SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = 4500 }
                });

            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            // Устанавливаем дату ОП
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-8);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            // Блочим сервис так как ОП не погашен
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProcessExpiredPromisePayments();

            Assert.IsFalse(processRes.Error);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);

            var resultService = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().GetRent1CInfo(createAccountCommand.AccountId);

            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);


            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsFalse(resultService.Error);
            Assert.IsNotNull(resultService.Result);
            Assert.IsNotNull(resourceRent1C);
            Assert.IsNotNull(billingAcc.PromisePaymentSum);
            Assert.AreEqual(0, billingAcc.Balance);
            Assert.AreEqual(0, billingAcc.PromisePaymentSum);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.IsFalse(resultService.Result.ServiceStatus.ServiceIsLocked);
            Assert.IsFalse(resultService.Result.ServiceStatus.PromisedPaymentIsActive);
        }
    }
}
