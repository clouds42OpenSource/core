﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С  для спонсируемого аккаунта
    /// </summary>
    public class ConnectionRent1CWebSponsorAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 1500;

            CreateInflowPayment(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, billingService.Id, paymentSum);
            
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>()
                .ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);
            
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            accessRent1C.First().WebResource = true;

            var res = rent1CConfigurationAccessManager.ConfigureAccesses(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

            var resourceRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);
            
	        var resourceSecondRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, Clouds42Service.MyEnterprise);
            
	        var billingAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                where access.AccountUserID == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId
                select access).ToList();

            if (accessList.Any(t => t.IsLock))
                throw new InvalidOperationException("Ошибка спонсирования аренды 1С");

            Assert.IsNull(res.Result.ErrorMessage);
	        Assert.IsNotNull(billingAccount);
	        Assert.IsNotNull(resourceRent1C);
	        Assert.IsNotNull(resourceSecondRent1C);
	        Assert.AreEqual(0, billingAccount.Balance);
	        Assert.AreEqual(false, resourceRent1C.FrozenValue);
	        Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(0, resourceRent1C.Cost);
	        Assert.AreEqual(false, resourceSecondRent1C.FrozenValue);
	        Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceSecondRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(2200, resourceSecondRent1C.Cost);
		}
    }
}
