﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Сценарий:
    ///     Создаём пользователя с подключённой арендой
    ///     добавляем к нему нового пользователя и блокируем его
    ///     проверяем, состоит ли пользователь в группах
    /// </summary>
    public class CheckNotActivateUserInADGroupsTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = createAccountCommand.AccountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = Guid.NewGuid().ToString(),
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid().ToString("N")}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;
            TestContext.ServiceProvider.GetRequiredService<AccountUsersActivateManager>().Activate(accountUserId, false);
            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            var result = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>().ConfigureAccesses(billingAccount.Id,
            [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            Assert.IsFalse(result.Error);

            var companyGroupName = DomainCoreGroups.CompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var remoteDesktopGroupName = DomainCoreGroups.RemoteDesktopAccess;
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(createAccountCommand.Account.IndexNumber);

            var userExist = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, remoteDesktopGroupName);
            Assert.IsFalse(userExist, $"Пользователь не должен быть в группе {remoteDesktopGroupName}");

            userExist = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, $"Пользователь не должен быть в группе {webGroupName}");
            Assert.IsFalse(userExist);

            userExist = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, companyGroupName);
            Assert.IsFalse(userExist, $"Пользователь не должен быть в группе {companyGroupName}");
        }
    }
}
