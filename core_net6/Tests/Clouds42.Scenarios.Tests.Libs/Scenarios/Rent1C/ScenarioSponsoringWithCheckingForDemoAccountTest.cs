﻿using System;
using System.Collections.Generic;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Сценарий теста на спонсирование с проверкой на демо аккаунт
    /// Создаем 2 аккаунта и пытаемся выполнить поиск внешнего пользователя если аккаунт-спонсор демо и если не демо
    /// </summary>
    public class ScenarioSponsoringWithCheckingForDemoAccountTest : ScenarioBase
    {

        public override void Run()
        {
            //добавление двух аккаунтов с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                }
            };

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };

            //блокировка аренды 1С
            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);
            //отключение аренды 1С
            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            var emailForSearching = DbLayer.AccountUsersRepository.FirstOrDefault(w =>
                w.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId).Email;

            var result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (string.IsNullOrEmpty(result.Result.ErrorMessage) || result.Result.User != null)
                throw new InvalidOperationException("Ошибка. Спонсирование запрещено для демо аккаунтов"); 

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 900,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            result =
                rent1CManager.SearchExternalUserRent1C(
                    createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, emailForSearching);

            if (!string.IsNullOrEmpty(result.Result.ErrorMessage) || result.Result.User == null)
                throw new InvalidOperationException("Ошибка. Спонсирование разрешено. Аккаунт не является демо, так как есть входящий платеж.");
        }
    }
}
