﻿using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий подключения нового пользователя при недостаточном балансе при помощи ОП
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем второго аккаунт  и пытаемся подключить его к Аренде(Стандарт) при помощи спонсирования
    /// 
    ///     Проверка: пользователь должен быть подключен, сумма списана с баланса, сервис подорожал
    ///     но дата остается прежней, пользователь состоит в группах АД, есть ОП
    /// </summary>
    public class ConnectionRent1CStandardSponsorAccountUserWithThePromisedPaymentTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

	        var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);

	        resourcesConfiguration.ExpireDate = dateValue;
	        resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba =>
                ba.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
	        Assert.IsNotNull(billingAccount);

	        var balance = 100m;
            var billingService = GetRent1CServiceOrThrowException();

			CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, balance);

            var resource = DbLayer.ResourceRepository.Where(r =>
                r.Subject == null && r.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            TestContext.DbLayer.ResourceRepository.DeleteRange(resource);
            TestContext.DbLayer.Save();
            
	        var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
	        {
		        new()
                {
			        AccountUserId = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountAdminId,
		        }
	        };
	        var blockRent1C = new Rent1CServiceManagerDto
	        {
		        ExpireDate = DateTime.Now.AddMonths(-1)
	        };

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, blockRent1C);
            
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);

	        accessRent1C.First().StandartResource = true;

	        var result = rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            var resourceRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);
			
	        var resourceSecondRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, Clouds42Service.MyEnterprise);
			
			billingAccount =DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

	        Assert.IsTrue(result.Result);
	        Assert.IsNotNull(billingAccount);
	        Assert.IsNotNull(resourceRent1C);
	        Assert.IsNotNull(resourceSecondRent1C);
			Assert.IsNotNull(billingAccount.PromisePaymentSum);
	        Assert.AreEqual(1400, billingAccount.PromisePaymentSum);
	        Assert.AreEqual(0, billingAccount.Balance);
	        Assert.AreEqual(false, resourceRent1C.FrozenValue);
	        Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(3000, resourceRent1C.Cost);
	        Assert.AreEqual(false, resourceSecondRent1C.FrozenValue);
	        Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceSecondRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(0, resourceSecondRent1C.Cost);
		}
	}
}
