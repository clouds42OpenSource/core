﻿using Clouds42.Billing.Billing.Providers;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С стандарт для спонсируемого аккаунта
    /// Действия:
    /// 1) Создаём два аккаунта
    /// 2) Второму кладём на счёт средства, а первому блокируем аренду через просрочку
    /// 3) Первый аккаунт отлючает у себя услуги аренды
    /// 4) Второй - покупает админу первого аккаунта Стандарт
    /// Проверяем: аренда у первого аккаунта продлилась
    /// </summary>
    public class ConnectionRent1CStandartSponsorAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            //добавление двух аккаунтов с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var secondAcc = createAccountDatabaseAndAccountCommand.SecondAccountDetails.Account;

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            
            var paymentSum = 1500;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, billingService.Id, paymentSum);

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId == secondAcc.Id && x.BillingServiceId == billingService.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, DateTime.Now.AddDays(-2)));


            accessRent1C.First().StandartResource = true;

            var res = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);
            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            var resourceRent1C =
		        DbLayer.ResourceConfigurationRepository.FirstOrDefault(
				        r =>
					        r.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId &&
					        r.BillingService.SystemService == Clouds42Service.MyEnterprise);

	        var resourceSecondRent1C =
		        DbLayer.ResourceConfigurationRepository.FirstOrDefault(
				        r =>
					        r.AccountId == createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId &&
					        r.BillingService.SystemService == Clouds42Service.MyEnterprise);

	        var billingAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

			
			var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                where access.AccountUserID == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId
                select access).ToList();

            if (accessList.Any(t => t.IsLock))
                throw new InvalidOperationException("Ошибка спонсирования аренды 1С");

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId);

            var groupName = $"company_{accountUser.Account.IndexNumber}_web";
            var adManager = ServiceProvider.GetRequiredService<ActiveDirectoryProviderFakeSuccess>();

            var userExist = adManager.ExistUserAtGroup(accountUser.Login, groupName);

            RefreshDbCashContext(TestContext.Context.ProvidedServices);
            #region rentCheck
            Assert.IsTrue(userExist, $"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");
            Assert.IsNull(res.Result.ErrorMessage);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.IsNotNull(resourceSecondRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(0, resourceRent1C.Cost);
            Assert.AreEqual(false, resourceSecondRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceSecondRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(3000, resourceSecondRent1C.Cost);
            #endregion rentCheck

            #region provSerCheck
            var provRent = DbLayer.ProvidedServiceRepository.Where(p => p.Service == Clouds42Service.MyEnterprise.ToString() && p.AccountId== secondAcc.Id);
            Assert.IsTrue(provRent.Count() == 2, "После покупки должно быть две услуги аренды(1-себе и 1-спонсирование)");
            var sponsRent = provRent.FirstOrDefault(r => r.AccountId== secondAcc.Id && r.SponsoredAccountId!=null);
            Assert.IsNotNull(sponsRent, "Ошибка записи спонсирования");
            Assert.AreEqual(1500,sponsRent.Rate,"Запись с неверной суммой. Подключали Standart(1500)");
            Assert.AreEqual(sponsRent.DateTo.Value.Date,DateTime.Now.AddDays(7).Date,"Дата окончания через 7 дней так как спонсор считается дэмо");
            #endregion provSerCheck

        }
    }
}
