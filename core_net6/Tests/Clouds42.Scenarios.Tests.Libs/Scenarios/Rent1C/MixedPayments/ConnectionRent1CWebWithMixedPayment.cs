﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.MixedPayments
{
    /// <summary>
    /// Покупка веба за бонусы
    /// Действия:
    /// 1)Создаём аккаунт с подключённой арендой
    /// 2) Кладём ему денег на бонусный счёт равный стоимости веба.
    /// 3) Создаём второго пользователя.
    /// 4) Подключаем ему веб.
    /// 5) Проверяем, что операция прошла успешно, счёт не ушёл в минус,
    ///     а средства были сняты с бонусов.
    /// </summary>
    public class ConnectionRent1CWebWithMixedPayment: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var rent = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent.ExpireDate = DateTime.Now.AddMonths(1);
            DbLayer.ResourceConfigurationRepository.Update(rent);
            DbLayer.Save();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();
            var myEntUser = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
            
            var rdpMonthlyPerUser = rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id)?.Cost;
            var webMonthlyPerUser = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id)?.Cost;

            billingAccount.BonusBalance = webMonthlyPerUser ?? 0;
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            #region money
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный бонусный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = webMonthlyPerUser ?? 0,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Bonus
            });
            #endregion money

            #region secondUser
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin", 
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(), 
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();

            #endregion secondUser

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = res.Result, WebResource = true }
            ]);
            Assert.IsFalse(result.Error, "Операция выдачи аренды вернула ошибку");

            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            var webServiceType = GetWebServiceTypeOrThrowException();
            rent = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.AreEqual(rent.Cost,rdpMonthlyPerUser+webMonthlyPerUser*2);
            var resource = DbLayer.ResourceRepository.FirstOrDefault(r =>r.Subject == res.Result && r.BillingServiceTypeId == webServiceType.Id);
            Assert.IsNotNull(resource,"Не создался ресурс");

            billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsTrue(billingAccount.Balance==0,$"Основной баланс должен быть равен 0, а не {billingAccount.Balance}");
            Assert.IsTrue(billingAccount.BonusBalance ==0,$"Бонусы должны быть списаны полностью, а не {billingAccount.BonusBalance}");
        }
    }
}
