﻿using System;
using System.Linq;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Проверка работы продления дэмо-периода.
    /// Действия: создаём аккаунт,подключаем к аренде,
    /// удаляем свободные ресурсы аренды. Пытаемся продлить дэмо-период
    /// Проверяем: аренду продлило и добавил свободных лицензий аренды
    /// </summary>
    public class ScenarioRent1CDemoProlongation : ScenarioBase
    {
        private readonly ResourcesManager _resourcesManager;

        public ScenarioRent1CDemoProlongation()
        {
            _resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
        }

        public override void Run()
        {
            #region acc
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountCommand.AccountId, createAccountCommand.AccountAdminId);

            Assert.IsTrue(string.IsNullOrEmpty(result.Message), "Подключение аренды не возможно.");

            #endregion acc

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.ProlongOrLockExpiredServices();

            var futureExpDate = DateTime.Now.AddDays(7);

            var rentModelAcc = _resourcesManager.GetRent1DemoPeriodInfo(createAccountCommand.AccountId);
            rentModelAcc.Result.ExpireDate = futureExpDate;
            var resultAcc = _resourcesManager.ChangeRent1CDemoPeriod(createAccountCommand.AccountId, rentModelAcc.Result);

            Assert.IsFalse(resultAcc.Error, "Продление для демо аккаунта должно вернуть true");
            Assert.IsTrue( string.IsNullOrEmpty(resultAcc.Message), "Продление для демо аккаунта должно вернуть true");

            var freeResourceCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();
            var rentLicensesCount = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise).Count();

            Assert.AreEqual(freeResourceCount*2, rentLicensesCount,"Неверное количество лицензий аренды после продления дэмо.");

            var resConfExpDate = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise).ExpireDate;

            Assert.AreEqual(resConfExpDate,futureExpDate,"Не изменилась дата окончания");

        }
    }
}
