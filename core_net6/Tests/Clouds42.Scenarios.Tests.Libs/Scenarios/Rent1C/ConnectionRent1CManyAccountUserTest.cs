﻿using System;
using System.Collections.Generic;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountDatabase.Managers;
using System.Linq;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С стандарт для многих аккаунтов
    /// </summary>
    public class ConnectionRent1CManyAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            //добавление аккаунтa с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            var listUser = new List<AccountUserRegistrationToAccountTest>();
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>();
            for (var i = 0; i < 5; i++)
            {
                var user = new AccountUserRegistrationToAccountTest
                {
                    AccountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                    AccountIdString = SimpleIdHash.GetHashById(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId),
                    Login = $"UserTest_{i}_{i}",
                    Email = $"usertest{i}@mail.ru",
                    FirstName = $"UserTest{i}_{i}",
                    FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(i)
                };
                listUser.Add(user);

            }
            foreach (var user in listUser)
            {
                var res = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(user).Result;
                if (res == null)
                    throw new InvalidOperationException ("Ошибка создания нового пользователя");
                var access = new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = res.Result,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                };
                accessRent1C.Add(access);
            }


            var dbAccess = new AcDbAccessPostAddAllUsersModelDto
            {
                AccountDatabaseID = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                AccountID = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                LocalUserID = Guid.Empty
            };

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 2000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            //подключение аренды 1С для аккаунта
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var resRent1C = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            //Добавить доступ к базе
            var result = TestContext.ServiceProvider.GetRequiredService<AcDbAccessManager>().AddAccessesForAllAccountsUsers(dbAccess);
            if (result.Error || result.State != ManagerResultState.Ok)
                throw new InvalidOperationException ("Ошибка предоставления доступа к базе");

	        // assert
	        var resourceRent1C =
		        DbLayer
			        .ResourceConfigurationRepository.FirstOrDefault(
				        r =>
					        r.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId &&
					        r.BillingService.SystemService == Clouds42Service.MyEnterprise);


	        var billingAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

			foreach (var user in accessRent1C)
            {
                var accessList =
                (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                 where access.AccountUserID == user.AccountUserId
                    select access).ToList();

                if (accessList.Any(t => t.IsLock))
                    throw new InvalidOperationException ("Ошибка спонсирования аренды 1С");
                var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.Id == user.AccountUserId);
                var groupName = $"company_{accountUser.Account.IndexNumber}_web";
                var adManager = ServiceProvider.GetRequiredService<ActiveDirectoryProviderFakeSuccess>();

                var userExist = adManager.ExistUserAtGroup(accountUser.Login, groupName);
                if (!userExist)
                    throw new InvalidOperationException(
                        $"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");
            }

	        Assert.IsNull(resRent1C.Result.ErrorMessage);

	        Assert.IsNotNull(billingAccount);

	        Assert.IsNotNull(resourceRent1C);

	        Assert.AreEqual(0, billingAccount.Balance);

			Assert.AreEqual(false, resourceRent1C.FrozenValue);

	        Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());

	        Assert.AreEqual(9000, resourceRent1C.Cost);

		}
    }
}
