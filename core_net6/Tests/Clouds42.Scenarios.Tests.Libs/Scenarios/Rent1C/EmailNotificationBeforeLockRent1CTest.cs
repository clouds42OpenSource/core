﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Processors;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///Проверка на отправку сообщения о блокировке Аренды за пять дней
    /// Сценарий:
    /// 1)Создаём аккаунт
    /// 2) Запускаем процесс уведомления
    /// 3) Проверяем, что добавилась нужная запись в буфер
    /// 4) Делаем 2 входящие транзакции бонусную и денежную, чтобы
    ///     их суммы хватало для продления Аренды двух юзеров
    /// 5) Запускаем процесс уведомления
    /// 6) Проверяем, что при достаточном суммарном балансе новое уведомление
    ///     в буфер не добавляется
    /// </summary>
    public class EmailNotificationBeforeLockRent1CTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billing = createAccountDatabaseAndAccountCommand.AccountDetails.Account.BillingAccount;
            var cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            var service = cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, billing.Account.Id);

            TestContext.ServiceProvider.GetRequiredService<Before5DayNotificationProcessor>().Process(service);

            var firstPaymentSum = 400;
            var secondPaymentSum = 1100;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, null, firstPaymentSum);
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, null, secondPaymentSum, TransactionType.Bonus);

            TestContext.ServiceProvider.GetRequiredService<Before5DayNotificationProcessor>().Process(service);
        }
    }
}
