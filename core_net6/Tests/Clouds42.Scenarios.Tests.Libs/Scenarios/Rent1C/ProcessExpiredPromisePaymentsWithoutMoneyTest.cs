﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.MyDiskContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Сценарий отключения пользователя от Стандарт-а с заблокированным
    /// сервисом из за просроченного ОП,
    ///
    /// Предусловия: для аккаунта test_account подключено два пользователя test_user_1 и
    /// test_user_2 по тарифу Стандарт; баланс 0 руб; взят ОП дата окончания сегодня
    ///
    /// Действия: вносим сумму денег только за одного пользователя и пробуем отключить второго
    ///
    /// Проверка: пользователь отключен от Аренды(Стандарт) и не состоит в группах АД,
    /// цена за сервис изменилась, сервис не заблокировался
    /// </summary>
    public class ProcessExpiredPromisePaymentsWithoutMoneyTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            DeleteServiceTypeResourcesByAccountUser();

            await TestContext.DbLayer.RefreshAllAsync();
            //RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var resConf =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
           
            var promisePaymentSum = resConf.Cost;
            var dateValue = DateTime.Now.AddDays(-2);
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
                
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = true;

            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            await DbLayer.SaveAsync();

            await DbLayer.BillingAccountRepository
                .AsQueryable()
                .Where(x => x.Id == createAccountCommand.AccountId)
                .ExecuteUpdateAsync(x => x.SetProperty(y => y.PromisePaymentDate, DateTime.Now.AddDays(-8))
                    .SetProperty(y => y.PromisePaymentSum, promisePaymentSum));

            await TestContext.DbLayer.RefreshAllAsync();

            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.ProcessExpiredPromisePayments();

            const decimal balance = 1500m;
            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, balance);
            
            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var rent1CInfo = rent1CManager.GetRent1CInfo(createAccountCommand.AccountId);
            var myDiskInfo = await Mediator.Send(new GetMyDiskDataQuery { AccountId = createAccountCommand.AccountId });

            var resourceRent1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
           
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(true,resourceRent1C.FrozenValue);

            var billingAccount = await 
                DbLayer.BillingAccountRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(billingAccount.PromisePaymentDate);
            Assert.AreEqual(billingAccount.Balance, balance);
            Assert.AreEqual(billingAccount.PromisePaymentSum, promisePaymentSum);
            Assert.IsTrue(rent1CInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsTrue(myDiskInfo.Result.ServiceStatus.PromisedPaymentIsActive);
            Assert.IsTrue(rent1CInfo.Result.ServiceStatus.ServiceIsLocked);
            Assert.IsTrue(myDiskInfo.Result.ServiceStatus.ServiceIsLocked);

            var accountUser = await DbLayer.AccountUsersRepository.FirstOrDefaultAsync(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id != createAccountCommand.AccountAdminId);

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUser.Id, StandartResource = false, WebResource = false }
            ]);

            var emptyResources = await DbLayer.ResourceRepository.AsQueryableNoTracking().Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !res.Subject.HasValue).ToListAsync();

            billingAccount = await DbLayer.BillingAccountRepository.AsQueryableNoTracking()
                .FirstOrDefaultAsync(acc => acc.Id == createAccountCommand.AccountId);

            resourceRent1C = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(emptyResources);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(1500, billingAccount.Balance);
            Assert.AreEqual(true, resourceRent1C.FrozenValue);
            Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов Аренды не должно оставатся!!");
        }
    }
}
