﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///Сценарий подключения спонсируемого аккаунта за ОП при недостаточном балансе
    ///
    /// Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 1 руб; дата окончания сервиса  +10 дней от тек.даты.
    ///
    /// Действия: создаем второго пользователя и спонсируемого и пытаемся подключить их к Аренде(Web)
    ///
    /// Проверка: цена при проверке возможности проспонсировать и по факту спонсирования одиноковая,
    /// пользователь подключен, наличие ОП на сумму равную стоимости спонсируемого аккаунта
    /// </summary>
    public class ConnectionRent1CWebSponsorAccountUserWithThePromisedPaymentTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var sponsorAccountUser = DbLayer.AccountUsersRepository.FirstOrDefault(u =>
	            u.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId);

			var primaryAccount = DbLayer.AccountsRepository.FirstOrDefault(u =>
				u.Id == createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            var rent1CService = GetRent1CServiceOrThrowException();
            var paymentSum = 1;
            CreateInflowPayment(primaryAccount.Id, rent1CService.Id, paymentSum);
            
            var dateValue = DateTime.Now.AddDays(10);
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(primaryAccount.Id, Clouds42Service.MyEnterprise);
            
            resourcesConfiguration.ExpireDate = dateValue;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = sponsorAccountUser.Id,
                    StandartResource = false,
                    WebResource = false
                }
            };

            var rent1CManager = ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(sponsorAccountUser.AccountId, accessRent1C);

            var result = rent1CManager.SearchExternalUserRent1C(primaryAccount.Id, sponsorAccountUser.Email);
            accessRent1C.First().WebResource = true;

            var res = rent1CConfigurationAccessManager.ConfigureAccesses(primaryAccount.Id, accessRent1C);
            if (res.Result.Complete)
	            throw new InvalidOperationException($"Пользователь {sponsorAccountUser.Login} подключен при недостаточном балансе");

            Assert.AreEqual(result.Result.User.PartialUserCostWeb, res.Result.CostForPay);
            Assert.AreEqual(result.Result.User.PartialUserCostWeb-paymentSum, res.Result.EnoughMoney);
            Assert.IsTrue(res.Result.CanGetPromisePayment);

            var resOnPromisePayment = rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(primaryAccount.Id, accessRent1C);
            if (resOnPromisePayment.Error)
	            throw new InvalidOperationException($"Пользователь {sponsorAccountUser.Login} не подключен при помощи ОП");
            
            var resourceRent1C = GetResourcesConfigurationOrThrowException(sponsorAccountUser.AccountId, Clouds42Service.MyEnterprise);
            
            var billingAccount = DbLayer.BillingAccountRepository
	            .FirstOrDefault(acc => acc.Id == sponsorAccountUser.AccountId);

            var resourcePrimaryRent1C = GetResourcesConfigurationOrThrowException(primaryAccount.Id, Clouds42Service.MyEnterprise);
          
	        var billingPrimaryAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == primaryAccount.Id);

            var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                where access.AccountUserID == sponsorAccountUser.Id
                select access).ToList();

            if (accessList.Any(t => t.IsLock))
                throw new InvalidOperationException("Ошибка спонсирования аренды 1С");
            
            Assert.IsFalse(resOnPromisePayment.Error, resOnPromisePayment.Message);
	        Assert.IsTrue(resOnPromisePayment.Result);
	        Assert.IsNotNull(billingAccount);
	        Assert.IsNotNull(resourceRent1C);
	        Assert.IsNotNull(resourcePrimaryRent1C);
	        Assert.AreEqual(0, billingAccount.Balance);
	        Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(0, resourceRent1C.Cost);
            Assert.AreEqual(false, resourcePrimaryRent1C.Frozen);
	        Assert.AreEqual(dateValue.ToShortDateString(), resourcePrimaryRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(2200, resourcePrimaryRent1C.Cost);
	        Assert.AreEqual(0, billingPrimaryAccount.Balance);
            Assert.AreEqual(result.Result.User.PartialUserCostWeb - paymentSum, billingPrimaryAccount.PromisePaymentSum);
        }
    }
}
