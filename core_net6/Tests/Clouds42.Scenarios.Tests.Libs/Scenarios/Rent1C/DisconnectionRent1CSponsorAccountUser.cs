﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Отключение Аренды 1С стандарт для спонсируемого аккаунта
    /// Сценарий:
    ///         1. Создаем 2 аккаунта
    ///         2. Блокируем 1 аккаунт, отключаем все ресурсы
    ///         3. Создаем платеж для второго аккаунта, чтобы он мог спонсировать
    ///         4. Спонсируем первый аккаунт, проверяем, что аккаунт проспонсирован
    ///         5. Отключаем спонсирование, проверяем, что спонсирование отключено
    ///         6. Проверяем, что пользователь удален из групп АД
    /// </summary>S
    public class DisconnectionRent1CSponsorAccountUser : ScenarioBase
    {
        private readonly Rent1CConfigurationAccessManager _configurationAccessManager;

        public DisconnectionRent1CSponsorAccountUser()
        {
            _configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
        }

        public override void Run()
        {
            #region Создание аккаунтов

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createAccountCommand2 = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand2.Run();

            #endregion

            BlockRent1CForAccount(createAccountCommand.AccountId);

            #region Отключение ресурсов пользователю

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountCommand.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            ConfigurationAccess(createAccountCommand.AccountId, accessRent1C);

            var accountUserHasResources = AccountUserHasResources(createAccountCommand.AccountAdminId);
            Assert.IsFalse(accountUserHasResources);

            #endregion

            #region Спонсирование пользователя

            var accountResources = DbLayer.ResourceRepository.Where(r => r.AccountId == createAccountCommand2.AccountId)
                .ToList();
            Assert.IsNotNull(accountResources);

            var billingService = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);

            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);

            var standardRent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == billingService.Id &&
                r.AccountType == "Standart").Sum(r => r.Cost);

            PayRent1C(createAccountCommand2.AccountId, standardRent1CCost);

            var rdpResourceId = accountResources.First(w =>
                w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser && w.Subject == null).Id;
            var webResourceId = accountResources.First(w =>
                w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && w.Subject == null).Id;

            accessRent1C.First().RdpResourceId = rdpResourceId;
            accessRent1C.First().WebResourceId = webResourceId;
            accessRent1C.First().StandartResource = true;
            ConfigurationAccess(createAccountCommand2.AccountId, accessRent1C);

            accountUserHasResources = AccountUserHasResources(createAccountCommand.AccountAdminId);
            Assert.IsTrue(accountUserHasResources);

            var billingAccount2 =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand2.AccountId);
            Assert.IsNotNull(billingAccount2);
            Assert.AreEqual(1500, billingAccount2.Balance);

            #endregion

            #region Отключение спонсирования пользователю

            accessRent1C.First().StandartResource = false;
            accessRent1C.First().RdpResourceId = null;
            accessRent1C.First().WebResourceId = null;
            ConfigurationAccess(createAccountCommand2.AccountId, accessRent1C);

            accountUserHasResources = AccountUserHasResources(createAccountCommand.AccountAdminId);
            Assert.IsFalse(accountUserHasResources);

            #endregion
        }

        /// <summary>
        /// Подключение и отключение аренды для своих и спонсируемых пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accessList">Список моделей доступов пользователей для подключения/отключения аренды</param>
        private void ConfigurationAccess(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList)
        {
            var result = _configurationAccessManager.ConfigureAccesses(accountId, accessList);
            Assert.IsFalse(result.Error);
        }

        /// <summary>
        /// Оплатить сервис
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Цена</param>
        private void PayRent1C(Guid accountId, decimal cost)
        {
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Задаем начальный денежный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Money
            });
        }

        /// <summary>
        /// Проверка наличия ресурсов у пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Результат проверки</returns>
        private bool AccountUserHasResources(Guid accountUserId)
        {
            var resources = DbLayer.ResourceRepository.Where(r => r.Subject == accountUserId);
            if (!resources.Any())
                return false;

            return true;
        }
    }
}