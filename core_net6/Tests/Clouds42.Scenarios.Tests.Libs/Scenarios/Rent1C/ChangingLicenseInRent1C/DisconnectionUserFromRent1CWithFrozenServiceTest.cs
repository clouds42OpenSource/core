﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий отключения пользователя при неактивном сервисе + баланса достаточно
    /// для продления измененной подписки
    /// 
    ///     Предусловия:
    /// для аккаунта test_account подключено два пользователя test_user_1 и 
    /// test_user_2 по тарифу Стандарт; баланс 1500 руб (500 денег и 1000 бонусы);
    /// дата окончания сервиса (-10) дней от тек.даты.
    /// 
    ///     Действия: отключаем тариф Стандарт пользователю
    /// 
    ///     Проверка: пользователь отключен от Аренды(Стандарт) и не состоит в группах АД, сервис разблокирован и продлен,
    /// цена сервиса уменьшилась, деньги списались с баланса, пустые ресурсы отсутствуют
    /// </summary>
    public class DisconnectionUserFromRent1CWithFrozenServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var dateValue = DateTime.Now.AddDays(-10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
            
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var billingService = GetRent1CServiceOrThrowException();
            var firstPaymentSum = 500;
            var secondPaymentSum = 1000;

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, firstPaymentSum);
            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, secondPaymentSum, TransactionType.Bonus);
            
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id != createAccountCommand.AccountAdminId);

            RefreshDbCashContext(Context.BillingAccounts);
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUser.Id, StandartResource = false, WebResource = false }
            ]);

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
                
            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !res.Subject.HasValue);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
            
            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(emptyResources);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(1500, resourceRent1C.Cost);
            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов Аренды не должно оставатся!!");
        }
    }
}
