﻿using System;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий подключения нового пользователя при недостаточном балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем второго пользователя и пытаемся подключить его к Аренде(Стандарт)
    /// 
    ///     Проверка: пользователь не подключен, сравниваем цену подключения, сервис не затрагивается
    /// </summary>
    public class ConnectionUserToRent1CWithZeroBallanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = createAccountCommand.AccountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "Qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid().ToString("N")}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;

            var accessHelper = ServiceProvider.GetRequiredService<IRent1CAccessHelper>();

            var costForUser = accessHelper.GetAccountBillingDataForBuyRent1C(accountUserId).PartialCostOfStandartLicense;

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            var resourceRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsFalse(result.Result.Complete);
            Assert.AreEqual(costForUser, result.Result.EnoughMoney);
            Assert.AreEqual(costForUser, result.Result.CostForPay);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(false,resourceRent1C.FrozenValue);
            Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(1500, resourceRent1C.Cost);
        }
    }
}
