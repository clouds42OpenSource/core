﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий подключения нового пользователя при недостаточном балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем второго пользователя и спонсируемого и пытаемся подключить их к Аренде(Стандарт)
    /// 
    ///     Проверка: пользователи не подключены, сравниваем цену подключения, сервис не затрагивается
    /// </summary>
    public class ConnectionUserAndSponsorUserToRent1CWithZeroBalanceTest : ScenarioBase
    {
        private readonly Rent1CConfigurationAccessManager _configurationAccessManager;

        public ConnectionUserAndSponsorUserToRent1CWithZeroBalanceTest()
        {
            _configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createAccountCommandSecond = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommandSecond.Run();

            BlockRent1CForAccount(createAccountCommandSecond.AccountId);

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountCommandSecond.AccountAdminId,
                    StandartResource = false,
                    SponsorAccountUser = false,
                    WebResource = false
                }
            };
            ConfigurationAccess(createAccountCommandSecond.AccountId, accessRent1C);

            var accountResources = DbLayer.ResourceRepository.Where(r => r.AccountId == createAccountCommand.AccountId).ToList();
            Assert.IsNotNull(accountResources);

            var rdpResourceId = accountResources.First(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser && w.Subject == null).Id;
            var webResourceId = accountResources.First(w => w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && w.Subject == null).Id;

            accessRent1C.First().AccountUserId = createAccountCommandSecond.AccountAdminId;
            accessRent1C.First().RdpResourceId = rdpResourceId;
            accessRent1C.First().WebResourceId = webResourceId;

            ConfigurationAccess(createAccountCommand.AccountId, accessRent1C);
            GetAccessRent1CResultDm(createAccountCommand.Account, accessRent1C);

            RefreshDbCashContext(Context.ResourcesConfigurations);

            var resourceRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var resourceSecondRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommandSecond.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var accountUserHasResources = AccountUserHasResources(createAccountCommandSecond.AccountAdminId);

            Assert.IsFalse(accountUserHasResources);
            Assert.IsNotNull(resourceRent1C);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.AreEqual(1500, resourceRent1C.Cost);
            Assert.IsNotNull(resourceSecondRent1C);
            Assert.IsTrue(resourceSecondRent1C.FrozenValue);
            Assert.AreEqual(0, resourceSecondRent1C.Cost);

        }

        /// <summary>
        /// Получить модель описывающую недостаток суммы для оплаты аренды
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="updaterAccessRent1CRequestDm">Модель параметров для смены ресурсов арнеды</param>
        /// <returns>Модель, описывающая недостаток суммы для оплаты аренды</returns>
        private void GetAccessRent1CResultDm(Account account,
            List<UpdaterAccessRent1CRequestDto> updaterAccessRent1CRequestDm)
        {
            var accessHelper = ServiceProvider.GetRequiredService<IRent1CAccessHelper>();
            var resourcesService = ServiceProvider.GetRequiredService<IResourcesService>(); 
            var resourceConfig = resourcesService.GetResourceConfig(account.Id, Clouds42Service.MyEnterprise);
            
            var billingModels = new List<ChangeRent1CAccessDto>();
            updaterAccessRent1CRequestDm.ForEach(access =>
            {
                billingModels.Add(new ChangeRent1CAccessDto
                {
                    BuyRent1CData = access.SponsorAccountUser
                        ? accessHelper.GetAccountBillingDataForBuyRent1C(access.AccountUserId)
                        : accessHelper.GetAccountBillingDataForBuyRent1C(account, resourceConfig.ExpireDateValue.Date),
                    AccessData = access
                });
            });
            accessHelper.CanPayRent1CAccesses(account.Id, billingModels, out _);
        }

        /// <summary>
        /// Проверка наличия ресурсов у пользователя
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Результат проверки</returns>
        private bool AccountUserHasResources(Guid accountUserId)
            => DbLayer.ResourceRepository.Where(r => r.Subject == accountUserId).Any();

        /// <summary>
        /// Подключение и отключение аренды для своих и спонсируемых пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accessList">Список моделей доступов пользователей для подключения/отключения аренды</param>
        private void ConfigurationAccess(Guid accountId, List<UpdaterAccessRent1CRequestDto> accessList)
        {
            var result = _configurationAccessManager.ConfigureAccesses(accountId, accessList);
            Assert.IsFalse(result.Error);
        }
    }
}
