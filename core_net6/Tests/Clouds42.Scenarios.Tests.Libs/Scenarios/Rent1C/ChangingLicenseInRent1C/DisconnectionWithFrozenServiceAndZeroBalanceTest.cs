﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий отключения пользователей от Стандарт-а при заблокированом сервисе,
    /// без денег на балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключено два пользователя test_user_1 и
    /// test_user_2 по тарифу Стандарт; баланс 0 руб; дата окончания сервиса (-10) дней от тек.даты.
    /// 
    ///     Действия: поочерёдно отключаем пользователей от Стандарт-а
    /// 
    ///     Проверка: пользователи отключены от Аренды(Стандарт) и не состоят в группах АД,
    /// цена за сервис изменилась на 0, сервис остался заблокированным
    /// </summary>
    public class DisconnectionWithFrozenServiceAndZeroBalanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var dateValue = DateTime.Now.AddDays(-10);

            var resourcesConfiguration =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id != createAccountCommand.AccountAdminId);

            var accountUserAdmin = DbLayer.AccountUsersRepository.FirstOrDefault(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id == createAccountCommand.AccountAdminId);

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUser.Id, StandartResource = false, WebResource = false }
            ]);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            #region 1check

            billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
            
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(true, resourceRent1C.FrozenValue);
            Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(1500, resourceRent1C.Cost);

            #endregion 1check

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserAdmin.Id, StandartResource = false, WebResource = false }
            ]);

            #region 2check

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
            
            Assert.IsNull(result.Result.ErrorMessage);
            Assert.AreEqual(0, resourceRent1C.Cost);
            Assert.IsNotNull(resourceRent1C.ExpireDate);
            Assert.AreEqual(resourceRent1C.ExpireDate.Value.Date, dateValue.Date,"Неверная дата окончания сервиса");
            Assert.AreEqual(true, resourceRent1C.FrozenValue, "Сервис должен оставатся замороженым");

            #endregion 2check
        }
    }
}
