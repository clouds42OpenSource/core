﻿using System;
using System.Linq;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    /// Сценарий отключения пользователя от Аренды(Стандарт)
    /// при неактивном сервисе + баланса достаточно для продления измененной подписки
    /// 
    /// Предусловия: для аккаунта test_account подключено 2 пользовател test_user_1 и test_user_2
    /// по тарифу Стандарт; взят обещанный платеж на сумму 1500 руб; дата окончания сервиса (+3) часа от тек.даты.
    /// 
    /// Действия: отключаем одного пользователя от тарифа Стандарт
    /// 
    /// Проверка: 1 пользователь подключен к Аренде(Стандарт) состоит в группах АД, сервис разблокирован и продлен,
    /// второй пользователь отключен, не состоит в группах АД, цена сервиса уменьшилась, деньги списались с баланса, пустые ресурсы отсутствуют
    /// </summary>
    public class DisconnectionToRentWithFrozenServiceAndMoney : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var dateValue = DateTime.Now.AddHours(+3);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(Context.ResourcesConfigurations);

            resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsTrue(resourcesConfiguration.FrozenValue);
            Assert.IsTrue((bool)resourcesConfiguration.Frozen);

            var balance = 1500m;

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(
                createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
                {
                    SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = balance }
                });

            Assert.IsFalse(promisePayment.Error, promisePayment.Message);
            
            resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsTrue(resourcesConfiguration.FrozenValue);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id != createAccountCommand.AccountAdminId);


            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var modelRent1C = rent1CManager.GetRent1CInfo(createAccountCommand.AccountId).Result;

            var rent1Cres = modelRent1C.UserList.FirstOrDefault(u => u.Login == accountUser.Login);

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountCommand.AccountId,
            [
                new()
                {
                    AccountUserId = accountUser.Id,
                    StandartResource = false,
                    WebResource = false,
                    RdpResourceId = rent1Cres.RdpResourceId,
                    WebResourceId = rent1Cres.WebResourceId
                }
            ]);

            DbLayer.RefreshAll();
            var resourceRent1C =
                DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !res.Subject.HasValue);

            var billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
            
            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(emptyResources);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(1500, resourceRent1C.Cost);
            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов Аренды не должно оставатся!!");
        }
    }
}
