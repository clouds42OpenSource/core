﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий подключения нового пользователя к Аренде(Стандарт)
    /// при неактивном сервисе + баланса достаточно для продления измененной подписки
    /// 
    ///     Предусловия: для аккаунта test_account подключен пользовател test_user_1
    /// по тарифу Стандарт; баланс 3000 руб; дата окончания сервиса (-10) дней от тек.даты.
    /// 
    ///     Действия: создаем нового пользователя и подключаем ему
    /// тариф Стандарт
    /// 
    ///     Проверка: пользователь подключен к Аренде(Стандарт) состоит в группах АД, сервис разблокирован и продлен,
    /// цена сервиса увеличилась, деньги списались с баланса, пустые ресурсы отсутствуют
    /// </summary>
    public class ConnectionToRentWithFrozenServiceAndMoney : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            var balance = 3000m;
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, balance);
           
            var dateValue = DateTime.Now.AddDays(-10);
            var resourcesConfiguration =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
            
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = createAccountCommand.AccountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "Qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid():N}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
               
            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyEnterprise && !res.Subject.HasValue);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
            
            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(3000, resourceRent1C.Cost);
            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов Аренды не должно оставатся!!");
        }
    }
}
