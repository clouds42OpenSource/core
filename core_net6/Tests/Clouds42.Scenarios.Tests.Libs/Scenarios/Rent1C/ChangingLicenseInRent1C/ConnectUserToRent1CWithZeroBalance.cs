﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    /// Проверка подключения пользователя к Аренде, если на балансе не достаточно денег
    /// Сценарий:
    ///         1) Создаем аккаунт, завершаем демо период Аренды
    ///         2) Создаем пользователя пытаемся подключить ему Аренду
    ///         3) Проверяем что пользователю не была подключена Аренда
    /// </summary>
    public class ConnectUserToRent1CWithZeroBalance : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var defaultMonthsCount = -1;
            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(defaultMonthsCount)
            };

            var service = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);
            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountCommand.AccountId, blockRent1C);

            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);

            var rent1CStandartCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == service.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Платеж",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = rent1CStandartCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var accountUser = new AccountUserDcTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountCaption = "Test caption",
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid().ToString("N")}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };
            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(accountUser).Result;
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = accountUserId,
                    StandartResource = true
                }
            };
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountCommand.AccountId, accessRent1C);

            var resource = DbLayer.ResourceRepository.Where(r => r.Subject == accountUserId).ToList();
            Assert.IsTrue(resource.Count == 0, $"У пользователя {accountUserId} не должно быть ресурсов");
        }
    }
}