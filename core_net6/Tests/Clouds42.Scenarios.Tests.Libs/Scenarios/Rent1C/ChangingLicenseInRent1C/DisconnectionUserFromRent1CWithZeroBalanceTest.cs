﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий отключения пользователя от Стандарт-а с активным сервисом,
    /// без денег на балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключено два пользователя test_user_1 и
    /// test_user_2 по тарифу Стандарт; баланс 0 руб; дата окончания сервиса (+10) дней от тек.даты.
    /// 
    ///     Действия: отключаем пользователя от Стандарт-а
    /// 
    ///     Проверка: пользователь отключен от Аренды(Стандарт) и не состоит в группах АД,
    /// цена за сервис изменилась
    /// </summary>
    public class DisconnectionUserFromRent1CWithZeroBalanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, createAccountCommand);
            connectUserToRentCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
            
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(usrId =>
                usrId.AccountId == createAccountCommand.AccountId && usrId.Id != createAccountCommand.AccountAdminId);

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUser.Id, StandartResource = false, WebResource = false }
            ]);

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);

            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(1500, resourceRent1C.Cost);
        }
    }
}
