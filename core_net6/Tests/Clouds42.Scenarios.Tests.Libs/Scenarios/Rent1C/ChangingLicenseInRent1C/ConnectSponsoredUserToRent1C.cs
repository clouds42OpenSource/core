﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    /// Подключение к сервису Аренда 1С пользователя спонсируемого аккаунта
    /// Сценарий:
    ///         1) Создаем аккаунт и аккаунт, пользователя которого будем спонсировать
    ///         2) Подключаем пользователя спонсируемого аккаунта к Аренде
    ///         3) Проверяем, что пользователь состоит в группах АД
    /// </summary>
    public class ConnectSponsoredUserToRent1C : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var service = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CStandartCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == service.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);
            var rent1CCostForTwoUsers = rent1CStandartCost * 2;

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = rent1CCostForTwoUsers,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var sponsoredAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            sponsoredAccount.Run();

            var defaultDaysCount = -7;
            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(sponsoredAccount.AccountId,
                new Rent1CServiceManagerDto
                {
                    ExpireDate = DateTime.Now.AddDays(defaultDaysCount)
                });

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = sponsoredAccount.AccountAdminId,
                    StandartResource = true
                }
            };

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountCommand.AccountId, accessRent1C);

            var resource = DbLayer.ResourceRepository.FirstOrDefault(r => r.Subject == sponsoredAccount.AccountAdminId);
            Assert.AreEqual(resource.AccountSponsorId, createAccountCommand.AccountId,
                $"Аккаунт {sponsoredAccount.AccountId} не спонсируется");

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(rent1CStandartCost, billingAccount.Balance,
                $"На балансе должно остаться {rent1CStandartCost}");

            var adManager = ServiceProvider.GetRequiredService<ActiveDirectoryProviderFakeSuccess>();
            var accountUser = DbLayer.AccountUsersRepository.Where(au => au.AccountId == sponsoredAccount.AccountId)
                .First();

            var webGroup =
                adManager.ExistUserAtGroup(accountUser.Login, $"company_{accountUser.Account.IndexNumber}_web");
            if (!webGroup)
                throw new InvalidOperationException($"Пользователь {accountUser.Login} не добавлен в группу web");

            var rdpGroup = adManager.ExistUserAtGroup(accountUser.Login, "Remote Desktop Access");
            if (!rdpGroup)
                throw new InvalidOperationException(
                    $"Пользователь {accountUser.Login} не добавлен в группу Remote Desktop Access");
        }
    }
}