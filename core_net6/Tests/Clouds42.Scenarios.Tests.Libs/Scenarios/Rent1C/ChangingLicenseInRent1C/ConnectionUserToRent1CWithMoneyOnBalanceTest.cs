﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ChangingLicenseInRent1C
{
    /// <summary>
    ///     Сценарий подключения пользователя при достаточном кол-ве денег на балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 1000 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем и подключаем пользователя к Аренде(Стандарт)
    /// 
    ///     Проверка: пользователь должен быть подключен, сумма списана с баланса, сервис подорожал
    /// но дата остается прежней, пользователь состоит в группах АД
    /// </summary>
    public class ConnectionUserToRent1CWithMoneyOnBalanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
            
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var balance = 1000m;
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            CreateInflowPayment(createAccountCommand.AccountId, billingService.Id, balance);
            
            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = createAccountCommand.AccountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "Qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid():N}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;
            var accessHelper = ServiceProvider.GetRequiredService<IRent1CAccessHelper>();
            var costForUser = accessHelper.GetAccountBillingDataForBuyRent1C(accountUserId);
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var result = rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            var resourceRent1C =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);
                
            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);
            
            Assert.IsNull(result.Result.ErrorMessage);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceRent1C);
            Assert.AreEqual(balance - costForUser.PartialCostOfStandartLicense, billingAccount.Balance);
            Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(dateValue.ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
            Assert.AreEqual(3000, resourceRent1C.Cost);
        }
    }
}
