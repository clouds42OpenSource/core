﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С  для спонсируемого аккаунта
    /// </summary>
    public class ConnectionRent1CWebAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId),
                Login = $"TestLogin_{DateTime.Now:ffff}"
            };

            var res = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(secondUser).Result;
            if (res == null)
                throw new InvalidOperationException("Ошибка создания нового пользователя");

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = res.Result,
                    StandartResource = false,
                    WebResource = true,
                    SponsorAccountUser = false
                }
            };
            var dbAccess = new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                AccountID = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                LocalUserID = Guid.Empty,
                AccountUserID = res.Result,
                SendNotification = true
            };

            var billingService = GetRent1CServiceOrThrowException();
            var paymentSum = 200;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, paymentSum);
            
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var resRent1C =
                rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(
                    createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            
            var result = TestContext.ServiceProvider.GetRequiredService<AcDbAccessManager>().Add(dbAccess);

            if (result.Error || result.State != ManagerResultState.Ok)
                throw new InvalidOperationException("Ошибка предоставления доступа к базе");

            var resourceRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);
            
	        var billingAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            
			var accessList =
            (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                where access.AccountUserID == res.Result
             select access).ToList();

            if (accessList.Any(t => t.IsLock))
                throw new InvalidOperationException("Ошибка спонсирования аренды 1С");

	        Assert.IsTrue(resRent1C.Result);
	        Assert.IsNotNull(billingAccount);
	        Assert.IsNotNull(resourceRent1C);
	        Assert.AreEqual(0, billingAccount.Balance);
	        Assert.AreEqual(false, resourceRent1C.FrozenValue);
	        Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(2200, resourceRent1C.Cost);
		}
    }
}
