﻿using System;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C.ConnectingTrialVersionRental1CWhenAlienAccountTest
{
    /// <summary>
    /// Сценарий теста подключения пробной версии аренды 1С
    /// с указанием пользователя
    /// </summary>
    public class ScenarioConnectingTrialVersionRental1CIndicatingUserTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var resourcesHelper = ServiceProvider.GetRequiredService<ResourcesHelper>();
            var resourceConfigurationHelper = ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();

            var resourcesFirstAccount = resourcesHelper.GetResourcesRental1CForAccount(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            DbLayer.ResourceRepository.DeleteRange(resourcesFirstAccount);

            var resourcesConfigurationsFirstAccount =
                resourceConfigurationHelper.GetResourcesConfigurationsRental1CForAccount(
                    createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            DbLayer.ResourceConfigurationRepository.DeleteRange(resourcesConfigurationsFirstAccount);

            DbLayer.Save();

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountAdminId);

            if (!result.Error)
                throw new InvalidOperationException ($"Подключение аренды не возможно. Пользователь не принадлежит указанному аккаунту аккаунту :: {result.Message}");

            result = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId);

            if (result.Error)
                throw new InvalidOperationException ($"Подключение аренды возможно, так как пользователь принадлежит к указанному аккаунту :: {result.Message}");
        }
    }
}
