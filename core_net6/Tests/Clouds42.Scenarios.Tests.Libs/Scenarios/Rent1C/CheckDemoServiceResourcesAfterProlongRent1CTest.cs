﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.BillingService;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Тест проверяющий, что ресурсы активных демо сервисов не удаляются при пролонгации
    /// Сценарий:
    ///         1. Создаем аккаунт и сервис
    ///         2. Блокируем аккаунту Аренду, создаем платеж (избавляемся от демо периода)
    ///         3. Подключаем демо период созданного сервиса
    ///         4. Изменяем дату окончания Аренды, на дату меньше текушей
    ///         5. Выполняем пролонгацию, проверяем, что демо лицензии сервиса не удалились 
    /// </summary>
    public class CheckDemoServiceResourcesAfterProlongRent1CTest : ScenarioBase
    {
        private readonly Rent1CConfigurationAccessManager _rent1CConfigurationAccess;
        private readonly Rent1CManager _rent1CManager;
        private readonly CreatePaymentManager _createPaymentManager;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingManager _billingManager;

        public CheckDemoServiceResourcesAfterProlongRent1CTest()
        {
            _rent1CConfigurationAccess = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
            _activateServiceForRegistrationAccountProvider =
                TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingManager = (BillingManager)TestContext.ServiceProvider.GetRequiredService<IBillingManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var turnOnRent1C = _rent1CConfigurationAccess.TurnOnRent1C(createAccountCommand.AccountId);
            Assert.IsFalse(turnOnRent1C.Error);

            _rent1CManager.ManageRent1C(createAccountCommand.AccountId, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-1),
            });

            var rent1C = GetRent1CServiceOrThrowException();

            CreatePayment(createAccountCommand.Account, rent1C);

            var standardResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var billingServiceTypes =
                _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standardResourceTypeId);
            billingServiceTypes[0].BillingType = BillingTypeEnum.ForAccountUser;

            var billingService = CreateCustomService(billingServiceTypes);

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(billingService.Id,
                createAccountCommand.AccountAdminId);

            var resources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId).ToList();
            Assert.IsNotNull(resources);

            var demoLicensesCount = CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod();

            var rent1CResourcesCount = resources.Count(r => r.BillingServiceType.ServiceId == rent1C.Id);
            var customServiceTypesCount =
                resources.Count(r => r.BillingServiceTypeId == billingServiceTypes[0].Id);
            Assert.AreEqual(demoLicensesCount, customServiceTypesCount);
            Assert.AreEqual(2, rent1CResourcesCount);

            CreatePayment(createAccountCommand.Account, rent1C);

            var expireDate = DateTime.Now.AddHours(-1);
            ChangeRent1CExpireDate(createAccountCommand.AccountId, billingService.Id, expireDate);

            _billingManager.ProlongOrLockExpiredServices();

            rent1CResourcesCount = resources.Count(r => r.BillingServiceType.ServiceId == rent1C.Id);
            customServiceTypesCount =
                resources.Count(r => r.BillingServiceTypeId == billingServiceTypes[0].Id);
            Assert.AreEqual(demoLicensesCount, customServiceTypesCount);
            Assert.AreNotEqual(demoLicensesCount, rent1CResourcesCount);
        }

        /// <summary>
        /// Создать кастомный сервис
        /// </summary>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService CreateCustomService(
            List<BillingServiceTypeDto> billingServiceTypes)
        {
            var createIndustry = new CreateIndustryCommand(TestContext);
            createIndustry.Run();

            CreateDelimiterTemplate();

            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Key = Guid.NewGuid(),
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            Assert.IsFalse(createBillingServiceCommand.Result.Error, createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            Assert.IsNotNull(service, "Произошла ошибка в процессе создания. Сервис не найден.");

            return service;
        }

        /// <summary>
        /// Изменить дату окончания Аренды 1С
        /// </summary>
        /// <param name="accountId">Id frrfeynf</param>
        /// <param name="serviceId">Id cthdbcf</param>
        /// <param name="expireDate">Дата окончания Аренды</param>
        private void ChangeRent1CExpireDate(Guid accountId, Guid serviceId, DateTime expireDate)
        {
            var rent1CResConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingServiceId == serviceId);
            Assert.IsNotNull(rent1CResConfig);

            rent1CResConfig.ExpireDate = expireDate;
            DbLayer.ResourceConfigurationRepository.Update(rent1CResConfig);
            DbLayer.Save();

            rent1CResConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == accountId && rc.BillingServiceId == serviceId);
            Assert.AreEqual(expireDate, rent1CResConfig.ExpireDate);
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="account">Модель аккаунта</param>
        /// <param name="billingService">Модель сервиса</param>
        private void CreatePayment(Account account, IBillingService billingService)
        {
            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = GetServiceCost(account, billingService),
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }

        /// <summary>
        /// Получить стоимость сервиса
        /// </summary>
        /// <param name="account">Модель аккаунта</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Стоимость сервиса</returns>
        private decimal GetServiceCost(Account account, IBillingService billingService)
        {
            var localeId = GetAccountLocaleId(account.Id);

            var standardRent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == billingService.Id &&
                r.AccountType == "Standart").Sum(r => r.Cost);

            return standardRent1CCost;
        }
    }
}