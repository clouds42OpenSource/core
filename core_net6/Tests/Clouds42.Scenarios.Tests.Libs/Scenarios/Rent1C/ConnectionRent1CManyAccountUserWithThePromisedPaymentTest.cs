﻿using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountDatabase.Managers;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    /// Подключение Аренды 1С стандарт для многих аккаунтов при помощи ОП
    /// </summary>
    public class ConnectionRent1CManyAccountUserWithThePromisedPaymentTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            var listUser = new List<AccountUserRegistrationToAccountTest>();
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>();
            for (var i = 0; i < 5; i++)
            {
                var user = new AccountUserRegistrationToAccountTest
                {
                    AccountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                    AccountIdString = SimpleIdHash.GetHashById(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId),
                    Login = $"UserTest_{i}_{i}",
                    Email = $"usertest{i}@mail.ru",
                    FirstName = $"UserTest{i}_{i}",
                    FullPhoneNumber = $"7904532212{i}"
                };
                listUser.Add(user);

            }
            foreach (var user in listUser)
            {
                var res = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(user).Result;
                if (res == null)
                    throw new InvalidOperationException("Ошибка создания нового пользователя");

                var access = new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = res.Result,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                };
                accessRent1C.Add(access);
            }

            var dbAccess = new AcDbAccessPostAddAllUsersModelDto
            {
                AccountDatabaseID = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                AccountID = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                LocalUserID = Guid.Empty
            };

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 100;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id,
                paymentSum);

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var resRent1C = rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            
            var result = TestContext.ServiceProvider.GetRequiredService<AcDbAccessManager>().AddAccessesForAllAccountsUsers(dbAccess);
            if (result.Error || result.State != ManagerResultState.Ok)
                throw new InvalidOperationException("Ошибка предоставления доступа к базе");

            var resourceRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);
                
	        var billingAccount = DbLayer.BillingAccountRepository
		        .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            foreach (var user in accessRent1C)
            {
                var accessList =
                    (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                        where access.AccountUserID == user.AccountUserId
                        select access).ToList();

                if (accessList.Any(t => t.IsLock))
                    throw new InvalidOperationException("Ошибка спонсирования аренды 1С");
            }

            Assert.IsTrue(resRent1C.Result);
	        Assert.IsNotNull(billingAccount);
	        Assert.IsNotNull(resourceRent1C);
	        Assert.AreEqual(0, billingAccount.Balance);
	        Assert.AreEqual(1900, billingAccount.PromisePaymentSum);
			Assert.AreEqual(false, resourceRent1C.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());
	        Assert.AreEqual(9000, resourceRent1C.Cost);
		}
    }
}
