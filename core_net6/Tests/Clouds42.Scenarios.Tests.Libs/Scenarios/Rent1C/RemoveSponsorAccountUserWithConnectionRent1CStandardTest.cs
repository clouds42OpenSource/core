﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Rent1C
{
    /// <summary>
    ///     Сценарий подсчета итоговой стоимости аренды при удалении спонсируемого пользователя
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: создаем второй аккаунт  и подключаем его к Аренде(Стандарт) при помощи спонсирования
    ///                 удаляем созданный аккаунт, но аренду не отключаем
    /// 
    ///     Проверка: итоговая сумма за сервис Аренда 1С должна быть только за одиного пользователя
    /// </summary>
    public class RemoveSponsorAccountUserWithConnectionRent1CStandardTest : ScenarioBase
    {
        public override void Run()
        { 
            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 500;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, billingService.Id, paymentSum);

            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId,
                    StandartResource = false,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };
            
            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, blockRent1C);
            
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, accessRent1C);
            
            accessRent1C.First().StandartResource = true;

            var res = rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, accessRent1C);
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            Assert.IsTrue(accountUsersProfileManager.Delete(createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId));

            var resourceSecondRent1C = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId, Clouds42Service.MyEnterprise);
		        
            Assert.IsNull(res.Result.ErrorMessage);
	        Assert.AreEqual(1500, resourceSecondRent1C.Cost);
		}
    }
}
