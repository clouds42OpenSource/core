﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabaseDataManagerTest
{
    /// <summary>
    /// Сценарий для успешной проверки получения списка пользователей для предоставления доступа в базу восстанавливаемую из бэкапа
    /// Действия:
    /// 1. Создадим аккаунт, инф. базу, бэкап и остальные нужные сущности. [EntitiesCreation]
    /// 2. Проверим, что информация про админа аккаунта будет получена корректно. [CheckCorrectnessOfGettingAdminUser]
    /// 3. Проверим, что информация про обычного пользователя аккаунта будет получена корректно. [CheckCorrectnessOfGetiingCommonUser]
    /// 4. Проверим, что информация про удаленного пользователя не будет подключена. [CheckDeletedUserWontBeSelected]
    /// 5. Проверим, что информация будет получена про пользователя с неподключенной Арендой, но с доступом. [CheckGettingUserWithAcDbAccess]
    /// </summary>
    public class ScenarioGetAvailableUsersForGrantAccessToRestoringDbSuccess : ScenarioBase
    {
        private readonly CreateAccountDatabaseDataManager _createAccountDatabaseDataManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly UserInfoDataCheckTestHelper _userInfoDataCheckTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioGetAvailableUsersForGrantAccessToRestoringDbSuccess()
        {
            _createAccountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountDatabaseDataManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _userInfoDataCheckTestHelper = new UserInfoDataCheckTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            #endregion

            #region CheckCorrectnessOfGettingAdminUser

            var result =
                _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId, account.Id);

            Assert.IsFalse(result.Error, result.Message);
            Assert.AreNotEqual(0,result.Result.AvailableUsersForGrantAccess.Count);

            _userInfoDataCheckTestHelper.CheckCorrectnessOfUserForGrantAccess(
                result.Result.AvailableUsersForGrantAccess[0], createAccountCommand.AccountAdminId, false);

            #endregion

            #region CheckCorrectnessOfGetiingCommonUser

            var notAdminUserId = _accountUserTestHelper.CreateAccountUser(account.Id);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(notAdminUserId, account.Id);

            result =
                _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId, account.Id);

            Assert.IsFalse(result.Error, result.Message);

            var availableNotAdminUserInfo =
                GetUserInfoByUserIdOrThrowException(result.Result.AvailableUsersForGrantAccess, notAdminUserId);

            _userInfoDataCheckTestHelper.CheckCorrectnessOfUserForGrantAccess(
                availableNotAdminUserInfo, notAdminUserId, false);

            #endregion

            #region CheckDeletedUserWontBeSelected

            var notAdminUser = DbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == notAdminUserId);
            notAdminUser.CorpUserSyncStatus = "Deleted";
            DbLayer.AccountUsersRepository.Update(notAdminUser);
            DbLayer.Save();
            result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId,
                account.Id);

            Assert.IsFalse(result.Error, result.Message);

            Assert.AreEqual(1, result.Result.AvailableUsersForGrantAccess.Count,
                "Получили удаленного пользователя аккаунта для предоставления доступа");

            #endregion

            #region CheckGettingUserWithAcDbAccess

            var commonUserTwoId = _accountUserTestHelper.CreateAccountUser(account.Id);
            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, account.Id, Guid.NewGuid(), commonUserTwoId);

            result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId,
                account.Id);

            Assert.IsFalse(result.Error, result.Message);

            var commonUserTwoInfo = GetUserInfoByUserIdOrThrowException(result.Result.AvailableUsersForGrantAccess, commonUserTwoId);
            _userInfoDataCheckTestHelper.CheckCorrectnessOfUserForGrantAccess(
                commonUserTwoInfo, commonUserTwoId, true);

            #endregion
        }

        /// <summary>
        /// Выбрать информацию пользователя из всей полученной информации
        /// </summary>
        /// <param name="infos">Список моделей информации о пользователе</param>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Модель информации о пользователе</returns>
        private UserInfoDataDto GetUserInfoByUserIdOrThrowException(List<UserInfoDataDto> infos, Guid userId) =>
            infos.FirstOrDefault(usrInfo => usrInfo.UserId == userId) ??
            throw new InvalidOperationException($"Не получили пользователя с Id {userId}");
    }
}
