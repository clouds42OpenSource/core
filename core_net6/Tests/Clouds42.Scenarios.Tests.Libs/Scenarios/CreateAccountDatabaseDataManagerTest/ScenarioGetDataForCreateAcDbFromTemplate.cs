﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabaseDataManagerTest
{
    /// <summary>
    /// Сценарий для получения шаблонов инф. баз при их создании для аккаунта
    /// Действия:
    /// 1. Создадим аккаунт и проверим, что он создался с указанной локалью. [AccountCreation]
    /// 2. Получим шаблоны и проверим, что они корректно получились. Также проверим пользователей, которым можно выдать доступ. [GetTemplatesAndCheckCorrectness]
    /// 3. Создадим пользователя аккаунта с нужными ресурсами и проверим, что он будет получен как пользователь, которому можно выдать доступ. [CheckGettingUserForGrantAccess]
    /// 4. Создадим свой шаблон и проверим, что он будет получен. [CheckGettingOwnTemplate]
    /// 5. Проверим, что шаблоны не будут получены для несуществующего аккаунта. [CheckNotValidAccountId]
    /// </summary>
    public class ScenarioGetDataForCreateAcDbFromTemplate : ScenarioBase
    {
        private readonly CreateAccountDatabaseDataManager _createAccountDatabaseDataManager;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;

        public ScenarioGetDataForCreateAcDbFromTemplate()
        {
            _createAccountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountDatabaseDataManager>();
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
        }

        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);

            #endregion

            #region GetTemplatesAndCheckCorrectness

            var result = _createAccountDatabaseDataManager.GetDataForCreateAcDbFromTemplate(account.Id).Result;
            Assert.IsFalse(result.Error, result.Message);

            Assert.AreEqual(GetDbTemplatesCountForAccountByLocale(localeId), result.Result.AvailableTemplates.Count,
                "Неверно выбрались шаблоны для аккаунт для создания инф. баз");

            Assert.AreEqual(0, result.Result.AvailableUsersForGrantAccess.Count,
                "Ошибочно выбрались пользователи для предоставления доступа");

            #endregion

            #region CheckGettingUserForGrantAccess

            var userOneId = _accountUserTestHelper.CreateAccountUser(account.Id);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userOneId, account.Id);

            result = _createAccountDatabaseDataManager.GetDataForCreateAcDbFromTemplate(account.Id).Result;
            Assert.IsFalse(result.Error, result.Message);

            if (result.Result.AvailableUsersForGrantAccess.Count == 0 ||
                result.Result.AvailableUsersForGrantAccess[0].UserId != userOneId)
                throw new InvalidOperationException(
                    "Не выбрался пользователь, которому можно выдать доступ при создании из шаблона");

            #endregion

            #region CheckGettingOwnTemplate

            _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                LocaleId = localeId
            });

            result = _createAccountDatabaseDataManager.GetDataForCreateAcDbFromTemplate(account.Id).Result;
            Assert.IsFalse(result.Error, result.Message);

            Assert.AreEqual(GetDbTemplatesCountForAccountByLocale(localeId), result.Result.AvailableTemplates.Count,
                "Неверно выбрались шаблоны для аккаунт без указанной локали для создания инф. баз");

            #endregion

            #region CheckNotValidAccountId

            result = _createAccountDatabaseDataManager.GetDataForCreateAcDbFromTemplate(Guid.NewGuid()).Result;
            Assert.IsTrue(result.Error, "Получили шаблоны для несуществующего аккаунта");

            #endregion
        }

        /// <summary>
        /// Получить количество шаблонов инф. базы для аккаунта по локале
        /// </summary>
        /// <param name="localeId">Id локали</param>
        /// <returns>Количество шаблонов</returns>
        private int GetDbTemplatesCountForAccountByLocale(Guid localeId) => DbLayer.DbTemplateRepository
            .Where(tmp => tmp.Locale.ID == localeId && !tmp.Name.Contains("demo")).Count();
    }
}
