﻿using System;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabaseDataManagerTest
{
    /// <summary>
    /// Сценарий для неуспешного получения списка пользователей для предоставления доступа в базу восстанавливаемую из бэкапа
    /// Действия:
    /// 1. Создадим аккаунт и все необходимые сущности.
    /// 2. Проверим, что инофрмация о пользователе, у которого не подключена Аренда и нет доступа, не будет получена.
    /// 3. Проверим, что получим ошибку при передаче несуществующего Id бэкапа.
    /// 4. Проверим, что получим информацию о пользователях при передаче несуществуюещго Id аккаунта.
    /// </summary>
    public class ScenarioGetAvailableUsersForGrantAccessToRestoringDbFail : ScenarioBase
    {
        private readonly CreateAccountDatabaseDataManager _createAccountDatabaseDataManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;

        public ScenarioGetAvailableUsersForGrantAccessToRestoringDbFail()
        {
            _createAccountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountDatabaseDataManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate();

            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(dbTemplate.Name, ConfigurationsIdTestConstants.Unf);

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            #endregion

            #region CheckUserWithoutRentAndAcDbAccess

            _accountUserTestHelper.CreateAccountUser(account.Id);

            var result =
                _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId, account.Id);

            Assert.IsFalse(result.Error, result.Message);
            Assert.AreEqual(1, result.Result.AvailableUsersForGrantAccess.Count, 
                "Выбрался пользователь с неактивной арендой и без доступа");

            #endregion

            #region CheckNotValidBackupId

            result =
                _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(Guid.NewGuid(), account.Id);

            Assert.IsTrue(result.Error, "Не получили ошибку при передачи несуществуюещго Id бэкапа");

            #endregion

            #region CheckNotValidAccountId

            result =
                _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccessToRestoringDb(backupId, Guid.NewGuid());

            Assert.IsFalse(result.Error, result.Message);
            Assert.AreEqual(1,result.Result.AvailableUsersForGrantAccess.Count,
                "Не выбрались пользователи, хоть и передан несуществующий Id аккаунта");

            #endregion

        }
    }
}
