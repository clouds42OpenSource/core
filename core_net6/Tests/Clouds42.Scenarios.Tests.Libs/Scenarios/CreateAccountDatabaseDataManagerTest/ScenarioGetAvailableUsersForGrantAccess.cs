﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateAccountDatabaseDataManagerTest
{
    /// <summary>
    /// Сценарий для проверки получения списка пользователей для предоставления доступа
    /// Действия:
    /// 1. Создадим аккаунт и добавим ресурс MyEnterprise для админа аккаунта. [AccountCreation]
    /// 2. Проверим, что данные про админа аккаунта будут получены корректно. [CheckGettingInfoAboutAdminUser]
    /// 3. Создадим обычного пользователя аккаунта, добавим ему ресурс MyEnterprise и проверим, что данные про него получены корректно. [CheckGettingInfoAboutCommonUser]
    /// 4. Проверим, что удаленный пользователь не будет получен. [CheckDeletedUser]
    /// 5. Проверим, что не будут получены данные для несуществующего аккаунта. [CheckNotValidAccountId]
    /// </summary>
    public class ScenarioGetAvailableUsersForGrantAccess : ScenarioBase
    {
        private readonly CreateAccountDatabaseDataManager _createAccountDatabaseDataManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly UserInfoDataCheckTestHelper _userInfoDataCheckTestHelper;

        public ScenarioGetAvailableUsersForGrantAccess()
        {
            _createAccountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountDatabaseDataManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _userInfoDataCheckTestHelper = new UserInfoDataCheckTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(createAccountCommand.AccountAdminId, account.Id);

            #endregion

            #region CheckGettingInfoAboutAdminUser

            var result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccess(account.Id);
            Assert.IsFalse(result.Error, result.Message);
            Assert.AreNotEqual(0, result.Result.Count);

            _userInfoDataCheckTestHelper.CheckCorrectnessOfUserForGrantAccess(result.Result[0],
                createAccountCommand.AccountAdminId, true);

            #endregion

            #region CheckGettingInfoAboutCommonUser

            var notAdminUserId = _accountUserTestHelper.CreateAccountUser(account.Id);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(notAdminUserId, account.Id);

            result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccess(account.Id);
            Assert.IsFalse(result.Error, result.Message);

            var commonUserInfo = result.Result.FirstOrDefault(usrInfo => usrInfo.UserId == notAdminUserId) ??
                               throw new InvalidOperationException($"Не получили пользователя с Id {notAdminUserId}");

            _userInfoDataCheckTestHelper.CheckCorrectnessOfUserForGrantAccess(commonUserInfo, commonUserInfo.UserId, false);

            #endregion

            #region CheckDeletedUser

            var notAdminUser = DbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == notAdminUserId);
            notAdminUser.CorpUserSyncStatus = "Deleted";
            DbLayer.AccountUsersRepository.Update(notAdminUser);
            DbLayer.Save();

            result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccess(account.Id);
            Assert.IsFalse(result.Error, result.Message);

            Assert.AreEqual(1, result.Result.Count,"Получили удаленного пользователя аккаунта для предоставления доступа");

            #endregion

            #region CheckNotValidAccountId

            result = _createAccountDatabaseDataManager.GetAvailableUsersForGrantAccess(Guid.NewGuid());
            Assert.AreEqual(0, result.Result.Count,"Получили пользователей для несуществующего Id аккаунта");

            #endregion

        }
    }
}
