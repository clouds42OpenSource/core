﻿using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods
{
    /// <summary>
    /// 1) Сценарий теста для удаления существующего способа оплаты для аккаунта
    /// 
    /// 2) Добавляем способ оплаты для аккаунта
    /// 
    /// 3) Пытаемся получить и проверить что он не равен null
    /// 
    /// 4) Пытаемся удалить сохраненный способ
    /// 
    /// 5) Проверяем на успех
    /// </summary>
    public class ScenarioDeleteSavedPaymentMethodTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            #endregion

            #region GetSavedPaymentMethods

            AddSavedPaymentMethodWithBankCardInfoForAccount(createAccountCommand.AccountId, PaymentSystem.Yookassa);

            var savedPaymentMethod = GetFirstSavedPaymentMethodByAccountId(createAccountCommand.AccountId);

            Assert.IsNotNull(savedPaymentMethod);

            var managerResult = ServiceProvider.GetRequiredService<SavedPaymentMethodsManager>()
                .DeleteSavedPaymentMethodAsync(savedPaymentMethod.Id)
                .GetAwaiter()
                .GetResult();

            Assert.IsFalse(managerResult.Error);
            Assert.IsTrue(managerResult.Result);

            #endregion
        }
    }
}
