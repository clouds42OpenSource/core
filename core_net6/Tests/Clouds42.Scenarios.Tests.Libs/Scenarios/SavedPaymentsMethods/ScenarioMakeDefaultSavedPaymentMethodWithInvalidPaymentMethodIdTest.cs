﻿using System;
using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods
{
    /// <summary>
    /// Сценарий теста установки способа оплаты по умолчанию для автоплатежей с невалидным идентификатором способа оплаты
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Создаем способ оплаты
    ///
    /// 3) Готовим невалидую модель с неверным идентификатором способа оплаты
    ///
    /// 4) Пытаемся установить значение по умолчанию MakeDefaultAsync
    /// 
    /// 5) Проверяем результат на ошибку
    /// </summary>
    public class ScenarioMakeDefaultSavedPaymentMethodWithInvalidPaymentMethodIdTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region TryMakeDefaultForAutoPayPaymentMethoIdForAccount

            AddSavedPaymentMethodWithBankCardInfoForAccount(createAccountCommand.AccountId, PaymentSystem.Yookassa);

            var model = new AccountPaymentMethodDto
            {
                AccountId = createAccountCommand.AccountId,
                PaymentMethodId = Guid.NewGuid()
            };

            var managerResult = ServiceProvider.GetRequiredService<SavedPaymentMethodsManager>()
                .MakeDefault(model);

            Assert.IsTrue(managerResult.Error);
            Assert.IsFalse(managerResult.Result);

            #endregion
        }
    }
}
