﻿using System;
using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods
{
    /// <summary>
    /// Сценарий теста установки способа оплаты по умолчанию для автоплатежей с невалидным идентификатором аккаунта
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Создаем способ оплаты
    ///
    /// 3) Проверяем, что способ оплаты создался
    /// 
    /// 4) Готовим невалидую модель с неверным идентификатором аккаунта
    ///
    /// 5) Пытаемся установить значение по умолчанию MakeDefaultAsync
    /// 
    /// 6) Проверяем результат на ошибку
    /// </summary>
    public class ScenarioMakeDefaultSavedPaymentMethodWithInvalidAccountIdTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region TryMakeDefaultForAutoPayPaymentMethoIdForAccount

            AddSavedPaymentMethodWithBankCardInfoForAccount(createAccountCommand.AccountId, PaymentSystem.Yookassa);

            var savedPaymentMethod = GetFirstSavedPaymentMethodByAccountId(createAccountCommand.AccountId);

            Assert.IsNotNull(savedPaymentMethod);

            var model = new AccountPaymentMethodDto
            {
                AccountId = Guid.NewGuid(),
                PaymentMethodId = savedPaymentMethod.Id
            };

            var managerResult = ServiceProvider.GetRequiredService<SavedPaymentMethodsManager>()
                .MakeDefault(model);

            Assert.IsTrue(managerResult.Error);
            Assert.IsFalse(managerResult.Result);

            #endregion
        }
    }
}
