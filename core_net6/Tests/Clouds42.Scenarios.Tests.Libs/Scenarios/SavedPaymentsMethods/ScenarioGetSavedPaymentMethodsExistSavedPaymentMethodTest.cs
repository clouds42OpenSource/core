﻿using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods
{
    /// <summary>
    /// Сценарий теста для получения сохраненных способов оплат у аккаунта с существующим сохраненным способом оплаты
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Добавялем пользователю сохраненный способ оплаты
    ///
    /// 3) Пытаемся получить сохраненные способы оплаты
    ///
    /// 4) Проверяем результат на успех
    /// 
    /// 6) Проверяем результат на не null
    /// </summary>
    public class ScenarioGetSavedPaymentMethodsExistSavedPaymentMethodTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region GetSavedPaymentMethods

            AddSavedPaymentMethodWithBankCardInfoForAccount(createAccountCommand.AccountId, PaymentSystem.Yookassa);

            var managerResultAfterAddPaymentMethod = ServiceProvider.GetRequiredService<SavedPaymentMethodsManager>()
                .GetAllSavedPaymentMethodsAsync(createAccountCommand.AccountId).GetAwaiter().GetResult();

            Assert.IsFalse(managerResultAfterAddPaymentMethod.Error);
            Assert.IsNotNull(managerResultAfterAddPaymentMethod.Result);
            Assert.AreEqual(1, managerResultAfterAddPaymentMethod.Result.Count);

            #endregion
        }
    }
}
