﻿using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SavedPaymentsMethods
{
    /// <summary>
    /// Сценарий теста для получения сохраненный способов оплат у аккаунта без сохраненных способов оплаты
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Пытаемся получить сохраненные способы оплаты
    ///
    /// 3) Проверяем результат на успех
    ///
    /// 4) Проверяем что результат вернул null
    /// </summary>
    public class ScenarioGetSavedPaymentMethodsTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region GetSavedPaymentMethods

            var managerResult = ServiceProvider.GetRequiredService<SavedPaymentMethodsManager>()
                .GetAllSavedPaymentMethodsAsync(createAccountCommand.AccountId)
                .GetAwaiter()
                .GetResult();

            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(0, managerResult.Result.Count);

            #endregion
        }
    }
}
