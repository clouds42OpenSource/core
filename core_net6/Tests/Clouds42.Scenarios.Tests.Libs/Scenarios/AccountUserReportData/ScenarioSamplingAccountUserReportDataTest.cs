﻿using System;
using System.Linq;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserReportData
{
    /// <summary>
    /// Сценарий теста выборки списка пользователей для отчета
    /// </summary>
    public class ScenarioSamplingAccountUserReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountUserReportDataProvider _accountUserReportDataProvider;

        public ScenarioSamplingAccountUserReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountUserReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountUserReportDataProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var secondAccount = _testDataGenerator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(secondAccount);
            DbLayer.Save();

            var firstAccountUser = _testDataGenerator.GenerateAccountUser(createAccountCommand.AccountId);
            var secondAccountUser = _testDataGenerator.GenerateAccountUser(createAccountCommand.AccountId);

            var thirdAccountUser = _testDataGenerator.GenerateAccountUser(secondAccount.Id);
            var fourthAccountUser = _testDataGenerator.GenerateAccountUser(secondAccount.Id);

            DbLayer.AccountUsersRepository.Insert(firstAccountUser);
            DbLayer.AccountUsersRepository.Insert(secondAccountUser);
            DbLayer.AccountUsersRepository.Insert(thirdAccountUser);
            DbLayer.AccountUsersRepository.Insert(fourthAccountUser);
            DbLayer.Save();

            var dbOne = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 1, 1);
            var dbTwo = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 2, 2);
            
            DbLayer.DatabasesRepository.Insert(dbOne);
            DbLayer.DatabasesRepository.Insert(dbTwo);
            DbLayer.Save();

            var acDbAccessesOne =
                _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId, firstAccountUser.Id, dbOne.Id);

            var acDbAccessesTwo =
                _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId, secondAccountUser.Id, dbTwo.Id);

            var acDbAccessesThird =
                _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId, thirdAccountUser.Id, dbOne.Id);

            var acDbAccessesFourth =
                _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId, fourthAccountUser.Id, dbTwo.Id);

            var acDbAccessesFive =
                _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId, fourthAccountUser.Id, dbOne.Id);

            DbLayer.AcDbAccessesRepository.Insert(acDbAccessesOne);
            DbLayer.AcDbAccessesRepository.Insert(acDbAccessesTwo);
            DbLayer.AcDbAccessesRepository.Insert(acDbAccessesThird);
            DbLayer.AcDbAccessesRepository.Insert(acDbAccessesFourth);
            DbLayer.AcDbAccessesRepository.Insert(acDbAccessesFive);

            DbLayer.Save();

            var managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();
            

            Assert.AreEqual(5,managerResult.Count);

            var findAccountUser = managerResult.FirstOrDefault(w => w.Login == firstAccountUser.Login);

            if (findAccountUser.CountInternalAccessesToDatabases != 1 || findAccountUser.CountExternalAccessesToDatabases > 0)
                throw new InvalidOperationException("У пользователя есть только 1 доступ к внутренней базе");

            CompareTwoInvoice(firstAccountUser, findAccountUser, createAccountCommand.Account.IndexNumber);

            findAccountUser = managerResult.FirstOrDefault(w => w.Login == secondAccountUser.Login);

            if (findAccountUser.CountInternalAccessesToDatabases != 1 || findAccountUser.CountExternalAccessesToDatabases > 0)
                throw new InvalidOperationException("У пользователя есть только 1 доступ к внутренней базе");

            CompareTwoInvoice(secondAccountUser, findAccountUser, createAccountCommand.Account.IndexNumber);

            findAccountUser = managerResult.FirstOrDefault(w => w.Login == thirdAccountUser.Login);

            if (findAccountUser.CountExternalAccessesToDatabases != 1 || findAccountUser.CountInternalAccessesToDatabases > 0)
                throw new InvalidOperationException("У пользователя есть только 1 доступ к внешней базе базе");

            CompareTwoInvoice(thirdAccountUser, findAccountUser, secondAccount.IndexNumber);

            findAccountUser = managerResult.FirstOrDefault(w => w.Login == fourthAccountUser.Login);

            if (findAccountUser.CountExternalAccessesToDatabases != 2 || findAccountUser.CountInternalAccessesToDatabases > 0)
                throw new InvalidOperationException("У пользователя есть только 2 доступ к внешней базе базе");

            CompareTwoInvoice(fourthAccountUser, findAccountUser, secondAccount.IndexNumber);
        }


        /// <summary>
        /// Сравнить два объекта
        /// </summary>
        /// <param name="accountUser">Созданный пользователь</param>
        /// <param name="accountUserReportDataDc">Полученный пользователь</param>
        /// <param name="accountNumber">Номер аккаунта</param>
        private void CompareTwoInvoice(AccountUser accountUser, AccountUserReportDataDto accountUserReportDataDc, int accountNumber)
        {
            if (accountUserReportDataDc.AccountNumber != accountNumber)
                throw new InvalidOperationException("Не совпадает номер аккаунта");

            if (accountUserReportDataDc.Email != accountUser.Email)
                throw new InvalidOperationException("Не совпадает почта");

            if (accountUserReportDataDc.FullName != accountUser.FullName)
                throw new InvalidOperationException("Не совпадает ФИО");

            if (accountUserReportDataDc.PhoneNumber != accountUser.PhoneNumber)
                throw new InvalidOperationException("Не совпадает Номер телефона");
        }
    }
}
