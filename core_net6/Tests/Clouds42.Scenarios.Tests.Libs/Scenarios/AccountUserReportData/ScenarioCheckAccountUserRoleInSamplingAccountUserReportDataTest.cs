﻿using System;
using System.Linq;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserReportData
{
    /// <summary>
    /// Сценарий проверки роли пользователя в выборке списка пользователей для отчета
    ///     1) Создаем аккаунт с пользователем
    ///     2) Делаем выборку, проверяем роль пользователя в выборке
    ///     3) Изменяем пользователю роль
    ///     4) Делаем выборку, проверяем изменилась ли роль в выборке
    /// </summary>
    public class ScenarioCheckAccountUserRoleInSamplingAccountUserReportDataTest : ScenarioBase
    {
        private readonly IAccountUserReportDataProvider _accountUserReportDataProvider;

        public ScenarioCheckAccountUserRoleInSamplingAccountUserReportDataTest()
        {
            _accountUserReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountUserReportDataProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser = createAccountCommand.Account.AccountUsers.FirstOrDefault() ?? throw new NullReferenceException();
            var managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();

            if(managerResult.Count != createAccountCommand.Account.AccountUsers.Count)
                throw new InvalidOperationException("Выборка работает некорректно");

            var reportDataDc = managerResult.FirstOrDefault() ?? throw new NotFoundException();
            if (reportDataDc.AccountUserRoles != AccountUserGroup.AccountAdmin.Description())
                throw new InvalidOperationException($"Некорректно работает выборка, роль пользователя должна быть {AccountUserGroup.AccountAdmin}");

            var accountUserRepository =
                DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);

            var userRole = accountUserRepository.AccountUserRoles.FirstOrDefault() ?? throw new NotFoundException();
            ChangeUserRole(userRole, AccountUserGroup.AccountUser);

            managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();

            reportDataDc = managerResult.FirstOrDefault();

            if (reportDataDc != null && reportDataDc.AccountUserRoles != AccountUserGroup.AccountUser.Description())
                throw new InvalidOperationException("Выборка работает некорректно");
        }

        /// <summary>
        /// Изменить роль пользователя
        /// </summary>
        /// <param name="accountUserRole">Текущая роль пользователя</param>
        /// <param name="newRole">Новая роль пользователя</param>
        private void ChangeUserRole(AccountUserRole accountUserRole, AccountUserGroup newRole)
        {
            accountUserRole.AccountUserGroup = newRole;
            DbLayer.AccountUserRoleRepository.Update(accountUserRole);
            DbLayer.Save();
        }
    }
}
