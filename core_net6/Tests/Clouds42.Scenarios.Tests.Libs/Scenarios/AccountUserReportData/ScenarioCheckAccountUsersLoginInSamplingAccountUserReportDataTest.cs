﻿using System;
using System.Linq;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserReportData
{
    /// <summary>
    /// Сценарий проверки логина пользователя в выборке списка пользователей для отчета
    ///     1) Создаем аккаунт с пользователем
    ///     2) Делаем выборку, поверяем, что поле Login в выборке не null
    ///     3) Изменяем логин пользователю
    ///     4) Делаем выборку, проверяем изменился ли логин в выборке
    /// </summary>
    public class ScenarioCheckAccountUsersLoginInSamplingAccountUserReportDataTest : ScenarioBase
    {
        private readonly IAccountUserReportDataProvider _accountUserReportDataProvider;

        public ScenarioCheckAccountUsersLoginInSamplingAccountUserReportDataTest()
        {
            _accountUserReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountUserReportDataProvider>();
        }

        public override void Run()
        {
            const string accountLogin = "Earl";

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();
            
            var accountUserInSampling = managerResult.FirstOrDefault() ?? throw new NullReferenceException();
            if (accountUserInSampling.Login == null)
                throw new NullReferenceException("Выборка работает некорректно");

            var accountUser = createAccountCommand.Account.AccountUsers.FirstOrDefault();

            if (accountUser != null)
                ChangeAccountUserLogin(accountUser, accountLogin);

            var accountUsersRepository = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);

            if(accountUsersRepository.Login != accountLogin)
                throw new InvalidOperationException("Не логин не были изменен");

            managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();

            var accountUserFromSampling = managerResult.FirstOrDefault() ?? throw new NullReferenceException("Выборка работает некорректно");
            if (accountUsersRepository.Login != accountUserFromSampling.Login)
                throw new InvalidOperationException($"Некорректно работает выборка, не найден пользователь с логином: {accountLogin}");
        }

        /// <summary>
        /// Изменить логин пользователя аккаунта
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <param name="login">Новый логин</param>
        private void ChangeAccountUserLogin(AccountUser accountUser, string login)
        {
            accountUser.Login = login;
            DbLayer.AccountUsersRepository.Update(accountUser);
            DbLayer.Save();
        }
    }
}
