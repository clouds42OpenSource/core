﻿using System;
using System.Linq;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Billing;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserReportData
{
    /// <summary>
    /// Сценарий теста выборки списка пользователей для отчета
    ///     1) Создаем аккаунт с пользователем 
    ///     2) Делаем выборку и сравниваем кличество активных пользователей в выборке и у аккаунта
    ///     3) Блокируем пользователя
    ///     4) Делаем выборку, проверяем количество заблокированных пользователей в выборке
    ///     5) Создаем 10 пользователей
    ///     6) Делаем выборку и проверяем соответствует ли количество активных пользователей в выборке и у аккаунта
    /// </summary>
    public class ScenarioCheckAccountUserIsActiveInSamplingAccountUserReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly IAccountUserReportDataProvider _accountUserReportDataProvider;

        public ScenarioCheckAccountUserIsActiveInSamplingAccountUserReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountUserReportDataProvider = TestContext.ServiceProvider.GetRequiredService<IAccountUserReportDataProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();
            

            if(managerResult.Count != 1)
                throw new InvalidOperationException("Выборка работает некорректно");

            var activatedAccountUsers = managerResult.Where(w => w.Status == "Активен").ToList();

            if(activatedAccountUsers.Count != 1)
                throw new InvalidOperationException("Выборка работает некорректно");
            
            var accountUser =
                createAccountCommand.Account.AccountUsers.FirstOrDefault(w =>
                    w.AccountId == createAccountCommand.AccountId);

            BlockAccountUser(accountUser);

            managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();

            var notActivatedAccountUsers = managerResult.Where(w => w.Status == "Заблокирован").ToList();

            if(notActivatedAccountUsers.Count != 1)
                throw new InvalidOperationException("Выборка работает некорректно");

            var accountUsers = _testDataGenerator.GenerateListAccountUsersByCount(10, createAccountCommand.AccountId);
            accountUsers.SaveAccountUsers(DbLayer);

            managerResult = _accountUserReportDataProvider.GetAccountUserReportDataDcs();

            var accountUsersRepository = DbLayer.AccountUsersRepository.All().ToList();

            if(managerResult.Count != accountUsersRepository.Count)
                throw new InvalidOperationException("Выборка работает некорректно");

            var activatedUsersInSampling = managerResult.Count(w => w.Status == "Активен");

            var activatedAccountUsersCount = accountUsersRepository.Count(w => w.Activated);

            if (activatedAccountUsersCount != activatedUsersInSampling)
                throw new InvalidOperationException("Выборка работает некорректно");
        }

        /// <summary>
        /// Заблокировать пользователя аккаунта
        /// </summary>
        /// <param name="accountUser">Пользователь, которого необходимо заблокировать</param>
        private void BlockAccountUser(AccountUser accountUser)
        {
            accountUser.Activated = false;
            DbLayer.AccountUsersRepository.Update(accountUser);
            DbLayer.Save();
        }
    }
}
