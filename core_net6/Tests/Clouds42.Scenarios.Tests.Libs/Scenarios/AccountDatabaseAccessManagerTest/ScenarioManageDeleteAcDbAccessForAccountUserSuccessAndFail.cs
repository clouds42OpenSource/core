﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseAccessManagerTest
{
    /// <summary>
    /// Сценарий для успешной и неуспешной проверки обработки процесса удаления доступа пользователю в инф. базу
    /// Действия:
    /// 1. Создадим аккаунт, инф. базу, пользователя аккаунта , записи про доступ для пользователя. [EntitiesCreation]
    /// 2. Проверим, что процесс удаления доступа выполнится без ошибок для пользователя. [CreateAcDbAccessOnDelimiters]
    /// 3. Проверим, что невалидные параметры будут корректно обработаны и ошибка не будет выдана. [CheckNotValidParams]
    /// </summary>
    public class ScenarioManageDeleteAcDbAccessForAccountUserSuccessAndFail : ScenarioBase
    {
        private readonly IDeleteAcDbAccessForAccountUserProvider _deleteAcDbAccessForAccountUserProvider;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioManageDeleteAcDbAccessForAccountUserSuccessAndFail()
        {
            _deleteAcDbAccessForAccountUserProvider = TestContext.ServiceProvider.GetRequiredService<IDeleteAcDbAccessForAccountUserProvider>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 2)[0];
            var userOneId = _accountUserTestHelper.CreateAccountUser(accountId);

            var acDbAccess = _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userOneId);
            _acDbAccessTestHelper.CreateDbOnDelimitersAccess(acDbAccess.ID, AccountDatabaseAccessState.ProcessingGrant);

            #endregion

            #region CheckDeletionAccesses

            var acDbAccessParamsDc = new ManageAcDbAccessParamsDto
            {
                AccountDatabaseId = databaseId,
                AccountUserId = userOneId,
                ActionType = UpdateAcDbAccessActionType.Delete
            };

            var result = _deleteAcDbAccessForAccountUserProvider.DeleteAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Done || _acDbAccessTestHelper.CheckDbOnDelimitersAccessWasDeleted(acDbAccess.ID))
                _acDbAccessTestHelper.CheckAcDbAccessDeleted(acDbAccessParamsDc.AccountUserId,
                    acDbAccessParamsDc.AccountDatabaseId, accountId);

            #endregion

            #region CheckNotValidParams

            acDbAccessParamsDc.AccountDatabaseId = Guid.NewGuid();
            result = _deleteAcDbAccessForAccountUserProvider.DeleteAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Done)
                throw new InvalidOperationException("Неверно обработалась ситуация с несуществующей базой");

            acDbAccessParamsDc.AccountDatabaseId = databaseId;
            acDbAccessParamsDc.AccountUserId = Guid.NewGuid();
            result = _deleteAcDbAccessForAccountUserProvider.DeleteAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Done)
                throw new InvalidOperationException("Неверно обработалась ситуация с несуществующим пользователем");

            #endregion
        }
    }
}
