﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseAccessManagerTest
{
    /// <summary>
    /// Сценарий для успешной и неуспешной проверки обработки процесса выдачи доступа пользователю в инф. базу
    /// Действия:
    /// 1. Создадим аккаунт, инф. базу, пользователей аккаунта с нужными ресурсами, записи про доступ для первого пользователя. [EntitiesCreation]
    /// 2. Проверим, что процесс выдачи доступа выполнится без ошибок для первого пользователя. [CreateAcDbAccessOnDelimiters]
    /// 3. Проверим, что невалидные параметры будут корректно обработаны и ошибка не будет выдана. [CheckNotValidParams]
    /// 4. Проверим, что доступы не будут будут пересозданы при условиях что доступ не требуется. [CheckAcDbAccessWontRecreated]
    /// </summary>
    public class ScenarioManageGrantAcDbAccessForAccountUserSuccessAndFail : ScenarioBase
    {
        private readonly IGrantAcDbAccessForAccountUserProvider _grantAcDbAccessForAccountUserProvider;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioManageGrantAcDbAccessForAccountUserSuccessAndFail()
        {
            _grantAcDbAccessForAccountUserProvider = TestContext.ServiceProvider.GetRequiredService<IGrantAcDbAccessForAccountUserProvider>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;
            
            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 2)[0];
            var userOneId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userTwoId = _accountUserTestHelper.CreateAccountUser(accountId);

            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userOneId, accountId);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userTwoId, accountId);
            
           
            var acDbAccess = _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userOneId);
            _acDbAccessTestHelper.CreateDbOnDelimitersAccess(acDbAccess.ID, AccountDatabaseAccessState.ProcessingGrant);

            #endregion

            #region CreateAcDbAccessOnDelimiters

            var acDbAccessParamsDc = new ManageAcDbAccessParamsDto
            {
                AccountDatabaseId = databaseId,
                AccountUserId = userOneId,
                ActionType = UpdateAcDbAccessActionType.Grant
            };

            var result = _grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Done || _acDbAccessTestHelper.CheckDbOnDelimitersAccessWasDeleted(acDbAccess.ID))
                throw new InvalidOperationException(result.ErrorMessage);

            #endregion

            #region CheckNotValidParams

            acDbAccessParamsDc.AccountDatabaseId = Guid.NewGuid();
            result = _grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Error)
                throw new InvalidOperationException("Неверно обработалась ситуация с несуществующей базой");

            acDbAccessParamsDc.AccountDatabaseId = databaseId;
            acDbAccessParamsDc.AccountUserId = Guid.NewGuid();
            result = _grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Error)
                throw new InvalidOperationException("Неверно обработалась ситуация с несуществующим пользователем");

            #endregion

            #region CheckAcDbAccessWontRecreated

            acDbAccessParamsDc.AccountUserId = userTwoId;
            result = _grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState != AccountDatabaseAccessState.Error || _acDbAccessTestHelper.CheckDbOnDelimitersAccessWasDeleted(acDbAccess.ID))
                throw new InvalidOperationException(
                    "Неверно обработалась ситуация, когда доступ в AcDbAccessRepository не существует");

            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userTwoId);
            result = _grantAcDbAccessForAccountUserProvider.GrantAcDbAccess(acDbAccessParamsDc);
            if (result.AccessState ==AccountDatabaseAccessState.Error || _acDbAccessTestHelper.CheckDbOnDelimitersAccessWasDeleted(acDbAccess.ID))
                throw new InvalidOperationException(
                    "Неверно обработалась ситуация, когда доступ уже был предоставлен и это повторная обработка");

            #endregion
        }
    }
}