﻿using System.Threading.Tasks;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Segment.CloudServicesBackupStorage.Managers;
using Core42.Application.Features.BackupStorageContext.Queries;
using Core42.Application.Features.BackupStorageContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudBackupStorage
{
    /// <summary>
    /// Тест проверки фильтра при получении хранилищ бекапов
    /// Сценарий:
    ///         Сценарий:
    ///         1. Получаем все хранилища бекапов, проверяем, что количесво 0
    ///         2. Создаем 4 хранилища бекапов
    ///         3. Получаем все хранилища бекапов, проверяем, что количесво верное
    ///         4. Получаем хранилища бекапов, по описанию, проверяем, что количесво верное
    ///         5. Получаем хранилища бекапов, по названию, проверяем, что количесво верное
    ///         6. Получаем хранилища бекапов, по адресу подключения, проверяем, что количесво верное
    ///         7. Получаем хранилища бекапов, по всем полям, проверяем, что количесво верное
    /// </summary>
    public class GetFilteredCloudBackupStorageTest : ScenarioBase
    {
        private readonly CloudBackupStorageManager _backupStorageManager;

        public GetFilteredCloudBackupStorageTest()
        {
            _backupStorageManager = TestContext.ServiceProvider.GetRequiredService<CloudBackupStorageManager>();
        }

        public override void Run()
        {
        }

        public override async Task RunAsync()
        {
            var filter = new GetFilteredBackupStoragesQuery { PageNumber = 1 };
            Assert.AreEqual(0, await GetFilteredCloudBackupStoragesCount(filter));

            var backupStorageDto = new CreateCloudBackupStorageDto
            {
                Name = "Name",
                Description = "Desc",
                ConnectionAddress = "1234",
                PhysicalPath = "C:\\Path"
            };
            CreateCloudBackupStorage(backupStorageDto);

            var backupStorageDto2 = new CreateCloudBackupStorageDto
            {
                Name = "Storage",
                Description = "Description",
                ConnectionAddress = "1234",
                PhysicalPath = "C:\\Path2"
            };
            CreateCloudBackupStorage(backupStorageDto2);

            var backupStorageDto3 = new CreateCloudBackupStorageDto
            {
                Name = "BackupName",
                Description = "Storage",
                ConnectionAddress = "9999",
                PhysicalPath = "D:\\Folder"
            };
            CreateCloudBackupStorage(backupStorageDto3);

            Assert.AreEqual(3, await GetFilteredCloudBackupStoragesCount(filter));

            filter.Filter = new BackupStorageFilter { ConnectionAddress = "9999" };


            Assert.AreEqual(1, await GetFilteredCloudBackupStoragesCount(filter));

            filter.Filter.ConnectionAddress = null;
            filter.Filter.Name = "Name";
            Assert.AreEqual(2, await GetFilteredCloudBackupStoragesCount(filter));

            Assert.AreEqual(2, await GetFilteredCloudBackupStoragesCount(filter));

            filter.Filter.Name = backupStorageDto.Name;
            filter.Filter.Description = backupStorageDto.Description;
            filter.Filter.ConnectionAddress = backupStorageDto.ConnectionAddress;

            Assert.AreEqual(1, await GetFilteredCloudBackupStoragesCount(filter));
        }

        /// <summary>
        /// Создать хранилище бэкапов
        /// </summary>
        /// <returns>Id хранилища бэкапов</returns>
        private void  CreateCloudBackupStorage(CreateCloudBackupStorageDto backupStorageDto)
        {
            var cloudBackupStorageCreateResult = _backupStorageManager.CreateCloudBackupStorage(backupStorageDto);
            Assert.IsFalse(cloudBackupStorageCreateResult.Error, cloudBackupStorageCreateResult.Message);
        }

        /// <summary>
        /// Получить хранилища бекапов по фильтру
        /// </summary>
        /// <returns>Количество записей о хранилищах по фильтру</returns>
        private async Task<int> GetFilteredCloudBackupStoragesCount(GetFilteredBackupStoragesQuery filter)
        {
            var cloudBackupStorages = await Mediator.Send(filter);
            Assert.IsFalse(cloudBackupStorages.Error, cloudBackupStorages.Message);

            return cloudBackupStorages.Result.Records.Count;
        }
    }
}
