﻿using Clouds42.Segment.CloudServicesBackupStorage.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudBackupStorage
{
    /// <summary>
    /// Тест проверки создания/удаления/редактирования хранилища бэкапов
    /// Сценарий:
    ///         1. Создаем хранилище бэкапов, проверям, что оно успешно создано
    ///         2. Редактируем хранилище бэкапов, проверям, что оно успешно отредактировано
    ///         3. Удаляем хранилище бэкапов, проверям, что оно успешно удалено
    /// </summary>
    public class CrudCloudBackupStorageTest : ScenarioBase
    {
        private readonly CloudBackupStorageManager _backupStorageManager;

        public CrudCloudBackupStorageTest()
        {
            _backupStorageManager = TestContext.ServiceProvider.GetRequiredService<CloudBackupStorageManager>();
        }

        public override void Run()
        {
            var cloudBackupStorageId = CreateCloudBackupStorage();
            EditCloudBackupStorage(cloudBackupStorageId);
            DeleteCloudBackupStorage(cloudBackupStorageId);

        }

        /// <summary>
        /// Создать хранилище бэкапов
        /// </summary>
        /// <returns>Id хранилища бэкапов</returns>
        private Guid CreateCloudBackupStorage()
        {
            var backupStorageDto = new CreateCloudBackupStorageDto
            {
                Name = $"Storage{Guid.NewGuid():N}",
                Description = "Description",
                ConnectionAddress = $"{DateTime.Now:mmssfff}",
                PhysicalPath = $"c:\\{Guid.NewGuid()}"

            };
            var cloudBackupStorageCreateResult =_backupStorageManager.CreateCloudBackupStorage(backupStorageDto);
            Assert.IsFalse(cloudBackupStorageCreateResult.Error, cloudBackupStorageCreateResult.Message);

            return cloudBackupStorageCreateResult.Result;
        }

        /// <summary>
        /// Редактировать хранилище бэкапов
        /// </summary>
        /// <param name="cloudBackupStorageId">Id хранилища бэкапов</param>
        private void EditCloudBackupStorage(Guid cloudBackupStorageId)
        {
            var backupStorageDto = _backupStorageManager.GetCloudBackupStorageDto(cloudBackupStorageId);
            Assert.IsFalse(backupStorageDto.Error, backupStorageDto.Message);

            var editBackupStorageDto = new CloudBackupStorageDto
            {
                Id = backupStorageDto.Result.Id,
                Name = $"NewName{Guid.NewGuid():N}",
                Description = "Desc",
                PhysicalPath = $"C:\\NewPath\\{Guid.NewGuid():N}",
                ConnectionAddress = $"{DateTime.Now:mmssfff}",
            };

            var editCloudBackupStorageResult =_backupStorageManager.EditCloudBackupStorage(editBackupStorageDto);
            Assert.IsFalse(editCloudBackupStorageResult.Error, editCloudBackupStorageResult.Message);

            var editedCloudBackupStorage = DbLayer.CloudServicesBackupStorageRepository.FirstOrDefault(csbs => csbs.ID == cloudBackupStorageId);
            Assert.IsNotNull(editedCloudBackupStorage);
            Assert.AreEqual(editBackupStorageDto.Name, editedCloudBackupStorage.Name);
            Assert.AreEqual(editBackupStorageDto.Description, editedCloudBackupStorage.Description);
            Assert.AreNotEqual(editBackupStorageDto.PhysicalPath, editedCloudBackupStorage.PhysicalPath);
            Assert.AreEqual(editBackupStorageDto.ConnectionAddress, editedCloudBackupStorage.ConnectionAddress);

        }

        /// <summary>
        /// Удалить хранилище бэкапов
        /// </summary>
        /// <param name="cloudBackupStorageId">Id хранилища бэкапов</param>
        private void DeleteCloudBackupStorage(Guid cloudBackupStorageId)
        {
            var backupStorageDto = _backupStorageManager.GetCloudBackupStorageDto(cloudBackupStorageId);
            Assert.IsFalse(backupStorageDto.Error, backupStorageDto.Message);

            var deleteCloudBackupStorageResult = _backupStorageManager.DeleteCloudBackupStorage(cloudBackupStorageId);
            Assert.IsFalse(deleteCloudBackupStorageResult.Error, deleteCloudBackupStorageResult.Message);
        }
    }
}
