﻿using System;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.EditBillingService
{
    /// <summary>
    /// Сценарий теста ошибочного редактирования сервиса
    /// 1) Сценарий проверки полей сервиса при изменении сервиса
    /// 2) Сценарий проверки статуса сервиса
    /// при изменении сервиса
    /// 3) Сценарий проверки внутреннего облачного сервиса
    /// при изменении сервиса
    /// </summary>
    public class ScenarioForErrorServiceEditingTest : ScenarioBase
    {
        private readonly EditBillingServiceManager _editServiceManager;
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;

        public ScenarioForErrorServiceEditingTest()
        {
            _editServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            ScenarioCheckServiceFieldsWithEditingServiceTest();
            ScenarioCheckServiceStatusWithEditingServiceTest();
            ScenarioCheckInternalCloudServiceWithEditingServiceTest();
        }

        /// <summary>
        /// Сценарий проверки полей сервиса при изменении сервиса
        /// </summary>
        private void ScenarioCheckServiceFieldsWithEditingServiceTest()
        {
            var serviceName = "Дублированое имя";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext,
                    new CreateBillingServiceTest(TestContext) {BillingServiceStatus = BillingServiceStatusEnum.Draft});
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.Name = serviceName;
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var editResult = _editServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsTrue(editResult.Error, "Название сервиса должно быть уникальным");

            editBillingServiceDto.Name = null;
            editResult = _editServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsTrue(editResult.Error, "Название сервиса обязательное поле");

        }

        /// <summary>
        /// Сценарий проверки статуса сервиса
        /// при изменении сервиса
        /// </summary>
        private void ScenarioCheckServiceStatusWithEditingServiceTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Новое названиеNew",
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();


            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceControlManager = ServiceProvider.GetRequiredService<BillingServiceControlManager>();

            var managerResult = billingServiceControlManager.Manage(new ManageBillingServiceDto
            {
                Id = service.Id,
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            });

            Assert.IsTrue(managerResult.Error, "Нельзя менять статус сервиса на черновик");
        }

        /// <summary>
        /// Сценарий проверки внутреннего облачного сервиса
        /// при изменении сервиса
        /// </summary>
        private void ScenarioCheckInternalCloudServiceWithEditingServiceTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Новое названиеNew2",
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);
            service.InternalCloudService = InternalCloudServiceEnum.Delans;
            DbLayer.BillingServiceRepository.Update(service);
            DbLayer.Save();

            var createCustomService =
                new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext))
                {
                    CreateBillingServiceTest = {Name = "CustomServiceTwo"}
                };

            createCustomService.Run();

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createCustomService.CreateBillingServiceTest.Name);

            var billingServiceControlManager = ServiceProvider.GetRequiredService<BillingServiceControlManager>();

            var managerResult = billingServiceControlManager.Manage(new ManageBillingServiceDto
            {
                Id = service.Id,
                InternalCloudService = InternalCloudServiceEnum.Delans
            });

            if (!managerResult.Error)
                throw new InvalidOperationException("Внутренний облачный сервис Деланс уже существует");
        }
    }
}
