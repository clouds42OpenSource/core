﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.EditBillingService
{
    /// <summary>
    /// Сценарий теста успешного редактирования сервиса
    /// 1) Сценарий редактирования сервиса с полным изменением сервиса
    /// 2) Сценарий редактирования сервиса с частичным изменением сервиса
    /// </summary>
    public class ScenarioForSuccessfulServiceEditingTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;
        private readonly EditBillingServiceManager _editServiceManager;
        private readonly ModerationResultManager _moderationResultManager;

        public ScenarioForSuccessfulServiceEditingTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            _editServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            _moderationResultManager = TestContext.ServiceProvider.GetRequiredService<ModerationResultManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            ScenarioEditingServiceWithFullChangeService();
            ScenarioCheckServiceStatusWithEditingServiceTest();
        }

        /// <summary>
        /// Сценарий редактирования сервиса с полным изменением сервиса
        /// </summary>
        private void ScenarioEditingServiceWithFullChangeService()
        {
            var cpSite = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);
            Assert.IsNotNull(service);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var registrationUrl = $"ReferralAccountId={service.AccountOwnerId}&CloudServiceId={service.Id}";
            var linkToActivateDemoPeriod = $"{cpSite}/signup?{registrationUrl}";

            if (string.IsNullOrEmpty(billingServiceCardDto.Result.LinkToActivateDemoPeriod))
                throw new InvalidOperationException("Отсутсвует ссылка на демо период");

            if (billingServiceCardDto.Result.LinkToActivateDemoPeriod != linkToActivateDemoPeriod)
                throw new InvalidOperationException("Ссылки на демо период не совпадают");

            var createIndustryCommand = new CreateIndustryCommand(TestContext, new IndustryModelTest { Name = "Новая отрасль", Description = "TEST" });
            createIndustryCommand.Run();

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewService";

            var editBillingServiceManagerResult = _editServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == service.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = _moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == editBillingServiceDto.Name);

            if (service == null)
                throw new InvalidOperationException("Изменение сервиса не произошло");

            var resultOfComparing =
                _createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingModelAndService(
                    editBillingServiceDto, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);

            if (!service.ServiceActivationDate.HasValue)
                throw new InvalidOperationException("Не проставилась дата активации");
        }

        /// <summary>
        /// Сценарий проверки статуса сервиса
        /// при изменении сервиса
        /// </summary>
        private void ScenarioCheckServiceStatusWithEditingServiceTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Новое названиеNew",
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest, false);
            createBillingServiceCommand.Run();
            
            var billingServiceControlManager = ServiceProvider.GetRequiredService<BillingServiceControlManager>();

            billingServiceControlManager.Manage(new ManageBillingServiceDto
            {
                Id = createBillingServiceCommand.Id,
                BillingServiceStatus = BillingServiceStatusEnum.IsActive
            });

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);


            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.IsActive);
            Assert.IsNotNull(service.StatusDateTime);
        }
    }
}
