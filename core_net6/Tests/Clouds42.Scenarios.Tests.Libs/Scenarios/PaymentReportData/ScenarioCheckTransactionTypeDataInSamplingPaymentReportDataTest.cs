﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Payment.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentReportData
{
    /// <summary>
    /// Сценарий теста проверки типа транзакции
    /// в выборке списка платежей для отчета
    /// 1) Создаем 2 платежа с разными типами транзакций
    /// 2) Делаем выборку и проверяем тип транзакции
    /// </summary>
    public class ScenarioCheckTransactionTypeDataInSamplingPaymentReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly PaymentReportDataManager _paymentReportDataManager;
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioCheckTransactionTypeDataInSamplingPaymentReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _paymentReportDataManager = TestContext.ServiceProvider.GetRequiredService<PaymentReportDataManager>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var objsCount = 2;

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();
            var accounts = _createAccountHelperTest.CreateByCount(objsCount);

            var payment = _testDataGenerator.GeneratePayment(accounts[0].Id, transactionType: TransactionType.Bonus);
            var paymentTwo = _testDataGenerator.GeneratePayment(accounts[1].Id);

            DbLayer.PaymentRepository.Insert(paymentTwo);
            DbLayer.PaymentRepository.Insert(payment);
            DbLayer.Save();

            var managerResult = _paymentReportDataManager.GetPaymentReportDataDcs();

            if (managerResult.Error)
                throw new InvalidOperationException("Не работает выборка платежей");

            var result = managerResult.Result;

            Assert.AreEqual(result.Count, objsCount);

            var findPayment = result.FirstOrDefault(r => r.TransactionType == TransactionType.Money);
            Assert.IsNotNull(findPayment, "Не удалось найти денежную транзакцию");

            findPayment = result.FirstOrDefault(r => r.TransactionType == TransactionType.Bonus);
            Assert.IsNotNull(findPayment, "Не удалось найти бонусную транзакцию");
        }
    }
}
