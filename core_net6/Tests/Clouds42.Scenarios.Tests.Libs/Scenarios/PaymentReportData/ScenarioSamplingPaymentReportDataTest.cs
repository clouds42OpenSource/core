﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.Billing.Payment.Managers;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PaymentReportData
{
    /// <summary>
    /// Сценарий теста выборки списка платежей для отчета
    /// </summary>
    public class ScenarioSamplingPaymentReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly PaymentReportDataManager _paymentReportDataManager;
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioSamplingPaymentReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _paymentReportDataManager = TestContext.ServiceProvider.GetRequiredService<PaymentReportDataManager>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var payment = _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, PaymentType.Inflow, 5000);
            var paymentTwo = _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, date:DateTime.Now.AddYears(-3));

            var saleManagerAccount = _createAccountHelperTest.Create();
            var saleManagerUser = _testDataGenerator.GenerateAccountUser(saleManagerAccount.Id);

            var saleManager =
                _testDataGenerator.GenerateAccountSaleManager(createAccountCommand.AccountId, saleManagerUser.Id,
                    "ОВР");

            DbLayer.AccountUsersRepository.Insert(saleManagerUser);
            DbLayer.AccountSaleManagerRepository.Insert(saleManager);
            DbLayer.PaymentRepository.Insert(payment);
            DbLayer.PaymentRepository.Insert(paymentTwo);
            DbLayer.Save();

            var managerResult = _paymentReportDataManager.GetPaymentReportDataDcs();

            if (managerResult.Error)
                throw new InvalidOperationException("Не работает выборка платежей");

            if (managerResult.Result.Count != 1)
                throw new InvalidOperationException("Количество платежей не совпадает. Платежи должные выбираться только за последние 2 года");

            var findPayment = managerResult.Result.FirstOrDefault();

            if (findPayment.AccountNumber != createAccountCommand.Account.IndexNumber)
                throw new InvalidOperationException("Не соответсвует номер аккаунта");

            if (findPayment.Sum != payment.Sum)
                throw new InvalidOperationException("Не соответсвует сумма платежа");

            if (findPayment.Description != payment.Description)
                throw new InvalidOperationException("Не соответсвует описание платежа");

            if (findPayment.PaymentStatus != payment.StatusEnum.Description())
                throw new InvalidOperationException("Не соответсвует статус платежа");

            if (findPayment.PaymentType != payment.OperationTypeEnum.Description())
                throw new InvalidOperationException("Не соответсвует тип платежа");

            if (string.IsNullOrEmpty(findPayment.SaleManagerFullName) || string.IsNullOrEmpty(findPayment.SaleManagerDivision))
                throw new InvalidOperationException("К аккаунту прикреплен сейл менеджер");
        }
    }
}
