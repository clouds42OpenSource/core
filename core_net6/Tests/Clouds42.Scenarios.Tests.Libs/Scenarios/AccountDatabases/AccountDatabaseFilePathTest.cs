﻿using System.IO;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки на нахождение базы по указанному пути
    /// </summary>
    public class AccountDatabaseFilePathTest : ScenarioBase
    {
        public override void Run()
        {            

            var createAccountCommand = new CreateAccountCommand(TestContext);

            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();


            var dbInform = DbLayer.DatabasesRepository.GetAccountDatabase(createAccountDatabaseCommand.AccountDatabaseId);

            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();

            var accountDatabasesPathHelper = new AccountDatabasePathHelper(DbLayer, Logger, segmentHelper, HandlerException);

            var fullPath = accountDatabasesPathHelper.GetPath(dbInform);

            var file = Directory.GetFiles(fullPath);

            if (file.Length == 0)
                throw new NotFoundException("Файл по данному пути отсутствует");
        }
    }
}
