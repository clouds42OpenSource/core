﻿using Clouds42.AccountDatabase.Activity.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Тест фиксации последней активности запуска инф. базы или RDP
    /// Сценарий:
    ///         1. Создаем аккаунт и базу
    ///         2. Фиксруем запуск базы
    ///         3. Проверяем, что запуск зафиксирован
    /// </summary>
    public class FixationDatabaseLaunchActivityTest : ScenarioBase
    {
        private readonly IFixationAccountDatabaseActivityManager _accountDatabaseActivityManager;

        public FixationDatabaseLaunchActivityTest()
        {
            _accountDatabaseActivityManager = TestContext.ServiceProvider.GetRequiredService<IFixationAccountDatabaseActivityManager>();
        }

        public override void Run()
        {
            var createDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createDatabaseAndAccountCommand.Run();

            var accountAdmin = DbLayer.AccountUsersRepository.FirstOrDefault(au =>
                au.Id == createDatabaseAndAccountCommand.AccountDetails.AccountAdminId);

            var actionInfoDto = new LinkLaunchActionInfoDto
            {
                AccountId = createDatabaseAndAccountCommand.AccountDetails.AccountId,
                Login = accountAdmin.Login,
                Action = LinkActionType.LaunchDatabase,
                LinkAppVersion = "1.23.55",
                PlatformVersion1C = "8.3.16.1684",
                LaunchType = LaunchType.WebClient,
                LinkAppType = AppModes.ClientApp
            };

            var postRequestSetActivityDateDto = new PostRequestSetActivityDateDto
            {
                AccountDatabaseID = createDatabaseAndAccountCommand.AccountDatabaseId,
                LinkLaunchActionInfo = actionInfoDto
            };

            var fixLaunchActivity = _accountDatabaseActivityManager.FixLaunchActivity(postRequestSetActivityDateDto, "127.0.0.1");
            Assert.IsFalse(fixLaunchActivity.Error);
            var databaseLaunchRdpStartHistories = DbLayer
                    .GetGenericRepository<DatabaseLaunchRdpStartHistory>()
                    .WhereLazy(dl => dl.Login == accountAdmin.Login)
                    .ToList();

            Assert.IsNotNull(databaseLaunchRdpStartHistories);
            Assert.AreEqual(1, databaseLaunchRdpStartHistories.Count);
            var databaseLaunchRdpStartHistory = databaseLaunchRdpStartHistories.First();
            Assert.AreEqual(databaseLaunchRdpStartHistory.Action, actionInfoDto.Action);
            Assert.AreEqual(databaseLaunchRdpStartHistory.LaunchType, actionInfoDto.LaunchType);
            Assert.AreEqual(databaseLaunchRdpStartHistory.LinkAppType, actionInfoDto.LinkAppType);
            Assert.AreEqual(databaseLaunchRdpStartHistory.LinkAppVersion, actionInfoDto.LinkAppVersion);
        }
    }
}
