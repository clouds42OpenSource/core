﻿using System.Threading.Tasks;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    ///     Смена типа дистрибутива если есть 
    /// указана альфа версия в сегменте
    /// </summary>
    public class ChangeDbDistributionTypeTest : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;

        public ChangeDbDistributionTypeTest()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
            _accountDatabaseCardManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
           
        }

        public override async Task RunAsync()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var createPlatform = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatform.Run();

            var accDbEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();

            var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createAccAndDb.AccountDetails.Segment.Id });

            Assert.IsNotNull(segmentManagerResult);
            Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

            var segmentViewModel = segmentManagerResult.Result;
            var database = DbLayer.DatabasesRepository.GetAccountDatabase(createAccAndDb.AccountDatabaseId);

            var dbManagerResult = _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name,
                createAccAndDb.AccountDetails.AccountId).Result;

            var existedDatabase = await 
                DbLayer.DatabasesRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefaultAsync(db => db.Id == createAccAndDb.AccountDatabaseId);

            Assert.IsNotNull(existedDatabase);
            Assert.AreEqual(PublishState.Unpublished, existedDatabase.PublishStateEnum);
            Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);

            Assert.IsNotNull(dbManagerResult);

            segmentViewModel.BaseData.Alpha83VersionId = createPlatform.Version;

            var managerResult = _cloudSegmentReferenceManager.EditSegment(new EditCloudServicesSegmentDto
            {
                FileStorageServersID = segmentViewModel.BaseData.FileStorageServersId,
                Name = segmentViewModel.BaseData.Name,
                ContentServerID = segmentViewModel.BaseData.ContentServerId,
                Description = segmentViewModel.BaseData.Description,
                BackupStorageID = segmentViewModel.BaseData.BackupStorageId,
                GatewayTerminalsID = segmentViewModel.BaseData.GatewayTerminalsId,
                SQLServerID = segmentViewModel.BaseData.SqlServerId,
                EnterpriseServer82ID = segmentViewModel.BaseData.EnterpriseServer82Id,
                ServicesTerminalFarmID = segmentViewModel.BaseData.ServicesTerminalFarmId,
                EnterpriseServer83ID = segmentViewModel.BaseData.EnterpriseServer83Id,
                DefaultSegmentStorageId = segmentViewModel.BaseData.DefaultSegmentStorageId,
                Stable82VersionId = segmentViewModel.BaseData.Stable82VersionId,
                Stable83VersionId = segmentViewModel.BaseData.Stable83VersionId,
                Alpha83VersionId = createPlatform.Version,
                CustomFileStoragePath = segmentViewModel.BaseData.CustomFileStoragePath,
                DelimiterDatabaseMustUseWebService = segmentViewModel.BaseData.DelimiterDatabaseMustUseWebService,
                Id = segmentViewModel.Id
            });

            Assert.IsNotNull(managerResult);
            Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании сегмента. Текст ошибки: {managerResult.Message}");

            dbManagerResult = _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name,
                createAccAndDb.AccountDetails.AccountId).Result;

            Assert.IsNotNull(dbManagerResult);
            Assert.IsNotNull(dbManagerResult.Database.Alpha83Version);

            var platformResult = accDbEditManager.ChangeAccountDatabasePlatformType(createAccAndDb.AccountDatabaseId,
                PlatformType.V83,
                DistributionType.Alpha);

            Assert.IsNotNull(platformResult);
            Assert.IsFalse(platformResult.Error,
                $"При смене дистрибутива у базы произошла ошибка. Текст ошибки: {platformResult.Message}");

            var existedSegment = await 
                DbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().FirstOrDefaultAsync(s => s.ID == segmentViewModel.Id);

            existedDatabase = await 
                DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(db => db.Id == createAccAndDb.AccountDatabaseId);

            Assert.IsNotNull(existedSegment);
            Assert.IsNotNull(existedDatabase);
            Assert.IsNotNull(existedSegment.Alpha83PlatformVersionReference);
            Assert.AreEqual(segmentViewModel.BaseData.Alpha83VersionId, existedSegment.Alpha83PlatformVersionReference.Version);
            Assert.AreEqual(PublishState.Unpublished, existedDatabase.PublishStateEnum);
            Assert.AreEqual(DistributionType.Alpha, existedDatabase.DistributionTypeEnum);

            platformResult = accDbEditManager.ChangeAccountDatabasePlatformType(createAccAndDb.AccountDatabaseId,
                PlatformType.V83);

            Assert.IsNotNull(platformResult);
            Assert.IsFalse(platformResult.Error,
                $"При смене дистрибутива у базы произошла ошибка. Текст ошибки: {platformResult.Message}");

            existedDatabase = await DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

            Assert.IsNotNull(existedDatabase);
            Assert.AreEqual(PublishState.Unpublished, existedDatabase.PublishStateEnum);
            Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);
        }
    }
}
