﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки смены модели восстановления для инф. базы на полную
    ///     Действия:
    ///         1) Создаем аккаунт и инф. базу
    ///         2) Меняем тип модели восстановления (указываем "Полная")
    ///         3) Проверяем что установилась полная модель восстановления 
    /// </summary>
    public class ScenarioChangeAccountDatabaseRestoreModelTypeToFull : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта и инф.базы

            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var accountDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);
            Assert.AreEqual(true, accountDatabase.IsFile);

            #endregion

            #region Смена модели восстановления в SQL сервере сегмента

            var acDbSqlServer = GetAccountSegment(accountDatabase.AccountId).CloudServicesSQLServer;
            acDbSqlServer.RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Mixed;
            DbLayer.CloudServicesSqlServerRepository.Update(acDbSqlServer);
            DbLayer.Save();

            #endregion

            #region Смена типа инф. базы на серверную

            var accountDatabaseEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();

            var result = accountDatabaseEditManager.ChangeAccountDatabaseType
                (new ChangeAccountDatabaseTypeDto { DatabaseId = createAccAndDb.AccountDatabaseId, RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full });

            #endregion

            #region Проверка результата работы метода

            DbLayer.DatabasesRepository.Reload(accountDatabase);

            Assert.IsNotNull(result);
            Assert.IsTrue(accountDatabase.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Full);

            #endregion
        }
    }
}
