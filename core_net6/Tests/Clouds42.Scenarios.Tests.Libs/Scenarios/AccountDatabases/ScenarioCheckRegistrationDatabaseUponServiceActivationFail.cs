﻿using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Clouds42.Common.ManagersResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки менеджера "RegisterAccountDatabaseByTemplateManager"
    /// База не будет создана при передаче невалидных данных
    /// 
    /// 1. Создаем Аккаунт, Шаблон БД и Сервис. [CreationOfEntries]
    /// 2. Проверим, что база не создается при пустой модели RegisterDatabaseUponServiceActivationDto. [EmptyRegisterDatabaseUponServiceActivationDto]
    /// 3. Проверим, что база не создается при несущестующем Id шаблона. [FakeTemplateId]
    /// 4. Проверим, что база не создается при несуществующем Id аккаунта. [FakeAccountId]
    /// 5. Проверим, что база не создается при несуществующем Id сервиса. [FakeServiceId]
    /// </summary>
    public class ScenarioCheckRegistrationDatabaseUponServiceActivationFail : ScenarioBase
    {
        private readonly RegisterAccountDatabaseByTemplateManager _registerAccountDatabaseByTemplateManager;

        public ScenarioCheckRegistrationDatabaseUponServiceActivationFail()
        {
            _registerAccountDatabaseByTemplateManager = TestContext.ServiceProvider.GetRequiredService<RegisterAccountDatabaseByTemplateManager>();
        }

        public override void Run()
        {
            #region CreationOfEntries

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountId = account.Id;

           var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });

            var dbTemplateDelimitersModelTest = new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id);
            var dbTemplateDelimiters = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            dbTemplateDelimiters.AddDbTemplateDelimiterItem(dbTemplateDelimitersModelTest);

            var templateId = dbTemplateDelimitersModelTest.TemplateId;

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest = { AccountId = account.Id }
            };
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            var serviceId = createBillingServiceCommand.Id;

            #endregion

            #region  EmptyRegisterDatabaseUponServiceActivationDto

            var result = _registerAccountDatabaseByTemplateManager.RegisterDatabaseUponServiceActivation(
                new RegisterDatabaseUponServiceActivationDto());

            CheckDbWasNotCreated(result, "Пустая модель регистрации базы", accountId, templateId);

            #endregion

            #region FakeTemplateId

            var registerDatabaseUponServiceActivationDto = new RegisterDatabaseUponServiceActivationDto
            {
                AccountId = accountId,
                ServiceId = serviceId,
                TemplateId = Guid.NewGuid()
            };

            result = _registerAccountDatabaseByTemplateManager.RegisterDatabaseUponServiceActivation(
                new RegisterDatabaseUponServiceActivationDto());

            CheckDbWasNotCreated(result, "Несуществующий Id шаблона базы", accountId, templateId);

            #endregion

            #region FakeAccountId

            registerDatabaseUponServiceActivationDto.TemplateId = templateId;
            registerDatabaseUponServiceActivationDto.AccountId = Guid.NewGuid();

            result = _registerAccountDatabaseByTemplateManager.RegisterDatabaseUponServiceActivation(
                new RegisterDatabaseUponServiceActivationDto());

            CheckDbWasNotCreated(result, "Несуществующий Id аккаунта", accountId, templateId);

            #endregion

            #region FakeServiceId

            registerDatabaseUponServiceActivationDto.ServiceId = Guid.NewGuid();
            registerDatabaseUponServiceActivationDto.AccountId = accountId;

            result = _registerAccountDatabaseByTemplateManager.RegisterDatabaseUponServiceActivation(
                new RegisterDatabaseUponServiceActivationDto());

            CheckDbWasNotCreated(result, "Несуществующий Id сервиса", accountId, templateId);

            #endregion
        }

        /// <summary>
        /// Проверить, что база не была создана
        /// </summary>
        /// <param name="managerResult">Результат раоты менеджера</param>
        /// <param name="testCondition">Условие запуска теста</param>
        private void CheckDbWasNotCreated(ManagerResult managerResult, string testCondition, Guid accountId, Guid templateId)
        {
            var dbsAfterRegistrationDb =
                DbLayer.DatabasesRepository.Where(db => db.AccountId == accountId && db.TemplateId == templateId);

            if (dbsAfterRegistrationDb.Any())
                throw new InvalidOperationException("База была создана при условиях невозможности создания базы. " +
                                                    $"Тест был успешно пройден при условии: {testCondition}." +
                                                    $"Ошибка из менеджера: {managerResult.Message}");
        }
    }
}
