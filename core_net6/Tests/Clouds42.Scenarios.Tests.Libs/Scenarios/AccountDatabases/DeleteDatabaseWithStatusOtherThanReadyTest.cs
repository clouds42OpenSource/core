﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий отправки на удаления ИБ у которой статус отличается от статуса Ready
    ///  
    /// Предусловия: для аккаунта test_account создаем ИБ и изменяем статус на "ошибка создания",
    /// отправляем базу на удаление
    /// 
    /// Действия: проверяем что статус базы изменился на удалена и таска
    /// на удаление не запустилась
    /// </summary>
    public class DeleteDatabaseWithStatusOtherThanReadyTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var dbInform =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var task = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(t =>
                t.TaskName == "DeleteAccountDatabaseToTombJob");

            dbInform.StateEnum = DatabaseState.ErrorDtFormat;

            DbLayer.DatabasesRepository.Update(dbInform);
            DbLayer.Save();

            var accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseTombManager>();

            var result =
                accountDatabaseManager.DeleteAccountDatabaseToTomb(createAccountDatabaseCommand.AccountDatabaseId,
                    false);

            Assert.IsTrue(result.Result, "Метод удаления должен был вернуть true");

            RefreshDbCashContext(TestContext.Context.AccountDatabases);

            var db = DbLayer.DatabasesRepository.FirstOrDefault(x =>
                x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.AreEqual(db.StateEnum, DatabaseState.DeletedToTomb, "База не изменила статус");

            var taskQueue = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(d => d.CoreWorkerTaskId == task.ID);

            Assert.IsNull(taskQueue, "Таска на удаление все равно была запущена");

        }
    }
}
