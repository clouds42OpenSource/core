﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения строки подключения к инф. базе на разделителях
    ///     1) Создаем аккаунт с инф. базой на разделителях
    ///     2) Имитируем открытие карточки базы
    ///     3) Проверяем что модель получена и сервер предприятия в строке подключения у базы правильный
    /// </summary>
    public class ScenarioGetDatabaseOnDelimitersConnectionPathTest : ScenarioBase
    {
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;

        public ScenarioGetDatabaseOnDelimitersConnectionPathTest()
        {
            _accountDatabaseCardManager = ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();

            var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(createAccAndDb.AccountDatabaseId);
            Assert.IsNotNull(accountDatabase);

            accountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();
            DbLayer.DatabasesRepository.Reload(accountDatabase);

            var accountDatabaseDataResult =
                _accountDatabaseCardManager.GetAccountDatabaseInfo(accountDatabase.V82Name,
                    createAccAndDb.AccountDetails.AccountId);

            var sourceDatabaseAccount = accountDatabase.GetSourceAccount();
            var actualAcDbEnterpriseServer =
                TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>().GetEnterpriseServer83((Account)sourceDatabaseAccount);

            Assert.IsFalse(accountDatabaseDataResult.Error);
            Assert.IsNotNull(accountDatabaseDataResult.Result);
            Assert.IsTrue(accountDatabaseDataResult.Result.Database.DatabaseConnectionPath.Contains(actualAcDbEnterpriseServer));
        }
    }
}
