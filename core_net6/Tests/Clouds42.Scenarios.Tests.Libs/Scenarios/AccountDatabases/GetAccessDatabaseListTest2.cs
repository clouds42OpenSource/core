﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
    /// </summary>
    /// <remarks>
    /// тест 2)
    /// есть 2 базы в компании Комп0
    /// и 1 база в компании Комп1
    /// ко всем базам есть доступ у пользователя test0 из компании Комп0 
    /// Убедиться что получаем список из 3 баз для пользователя test0.
    /// </remarks>
    public class GetAccessDatabaseListTest2 : ScenarioBase
    {
        private readonly StartProcessToWorkDatabaseManager _startProcessToWorkDatabaseManager;

        public GetAccessDatabaseListTest2()
        {
            _startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
        }

        public override void Run()
        {
            var createAccountCommandList = new CreateAccountCommand[2];

            var accountData = new AccountRegistrationModelTest();

            for (var i = 0; i < createAccountCommandList.Length; i++)
            {
                accountData.Login = $"TestLogin{i}{DateTime.Now:mmss}";
                accountData.Email = $"{accountData.Login}@test.ru";
                accountData.AccountCaption = $"Комп{i}";
                accountData.FullPhoneNumber = $"+{PhoneNumberGeneratorHelperTest.Generate()}";

                createAccountCommandList[i] = new CreateAccountCommand(TestContext, accountData);
                createAccountCommandList[i].Run();
            }

            // имеем две компании Комп0 и Комп1 и двух пользователей test0 и test1.
            var createAccountDatabaseCommandList = new CreateAccountDatabaseCommand[3];

            var infoDatabaseModel = new InfoDatabaseDomainModelTest(TestContext);

            // Есть две базы в компании Комп0
            for (var i = 0; i < 2; i++)
            {
                infoDatabaseModel.DataBaseName = $"Тестовая база {i}";
                createAccountDatabaseCommandList[i] = new CreateAccountDatabaseCommand(TestContext, createAccountCommandList[0], infoDatabaseModel, TestContext.AccessProvider);
                createAccountDatabaseCommandList[i].Run();

                AddAccessesForAllAccountsUsers(createAccountDatabaseCommandList[i].AccountDatabaseId,
                    createAccountCommandList[0].AccountAdminId);
            }

            // И одна база в компании Комп1
            infoDatabaseModel.DataBaseName = $"Тестовая база {2}";
            createAccountDatabaseCommandList[2] = new CreateAccountDatabaseCommand(TestContext, createAccountCommandList[1], infoDatabaseModel, TestContext.AccessProvider);
            createAccountDatabaseCommandList[2].Run();

            AddAccessesForAllAccountsUsers(createAccountDatabaseCommandList[2].AccountDatabaseId,
                createAccountCommandList[1].AccountAdminId);

            AddAccessesForAllAccountsUsers(createAccountDatabaseCommandList[2].AccountDatabaseId,
                createAccountCommandList[0].AccountAdminId);

            var accountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();
            var result = accountDatabaseDataManager.GetAccessDatabaseListAsync(createAccountCommandList[0].AccountAdminId).Result;

            Assert.AreEqual(3, result.Result.Count);
        }

        /// <summary>
        /// Предоставить доступы в базу для всех пользователей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="accountUserIds">Список Id пользователей</param>
        private void AddAccessesForAllAccountsUsers(Guid accountDatabaseId, params Guid[] accountUserIds)
        {
            _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(new StartProcessesOfManageAccessesToDbDto
            {
                DatabaseId = accountDatabaseId,
                UsersId = accountUserIds.ToList()
            });
        }
    }
}
