﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки менеджера "RegisterAccountDatabaseByTemplateManager"
    /// База будет создана при передаче валидных данных
    /// 
    /// 1. Создаем Аккаунт, Шаблон БД и Сервис. [CreationOfEntries]
    /// 2. Регистрируем базу и определяем шаблон для ее создания. [DbRegistration]
    /// 3. Проверяем что база добавилась в БД. [CheckDbCreated]
    /// </summary>
    public class ScenarioCheckRegistrationDatabaseUponServiceActivationSuccess : ScenarioBase
    {
        private readonly RegisterAccountDatabaseByTemplateManager _registerAccountDatabaseByTemplateManager;

        public ScenarioCheckRegistrationDatabaseUponServiceActivationSuccess()
        {
            _registerAccountDatabaseByTemplateManager = TestContext.ServiceProvider.GetRequiredService<RegisterAccountDatabaseByTemplateManager>();
        }

        public override void Run()
        {
            #region CreationOfEntries

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();
                        
            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });

            var dbTemplateDelimitersModelTest = new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id);

            var dbTemplateDelimiters = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            dbTemplateDelimiters.AddDbTemplateDelimiterItem(dbTemplateDelimitersModelTest);

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest = { AccountId = account.Id }
            };
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            #endregion

            #region DbRegistration

            var registerDatabaseUponServiceActivationDto = new RegisterDatabaseUponServiceActivationDto
            {
                AccountId = account.Id,
                ServiceId = createBillingServiceCommand.Id,
                TemplateId = dbTemplateDelimitersModelTest.TemplateId
            };

            var result = _registerAccountDatabaseByTemplateManager.RegisterDatabaseUponServiceActivation(
                 registerDatabaseUponServiceActivationDto);

            if (result.Error)
                throw new InvalidOperationException(result.Message);

            #endregion

            #region CheckDbCreated

            var dbCountAfterRegistrationDb =
               DbLayer.DatabasesRepository.Where(db => db.AccountId == account.Id && db.TemplateId == dbTemplateDelimitersModelTest.TemplateId).Count();

            if (dbCountAfterRegistrationDb != 1)
                throw new InvalidOperationException("База не была создана, или было создано несколько баз при одной регистрации");

            #endregion
        }
    }
}
