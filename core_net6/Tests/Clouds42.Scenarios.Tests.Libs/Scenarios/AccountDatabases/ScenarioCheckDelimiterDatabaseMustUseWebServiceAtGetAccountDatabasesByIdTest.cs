﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки поля "База на разделителях должна использовать вэб сервис"
    /// при получении данных по инф. базе
    ///
    /// 1) Создаем аккаунт с базой на разделителях
    /// 2) Получаем информацию по инф. базе и проверяем поле DelimiterDatabaseMustUseWebService (DelimiterDatabaseMustUseWebService опционально - ожидаемый результат false)
    /// 3) Меняем в базе поле DelimiterDatabaseMustUseWebService на true и повторяем проверку (ожидаемый результат true)
    /// </summary>
    public class ScenarioCheckDelimiterDatabaseMustUseWebServiceAtGetAccountDatabasesByIdTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();

            var accountDatabaseDataManager = ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();

            var managerResult = accountDatabaseDataManager
                .GetAccountDatabasesByIdAsync(createAccAndDb.AccountDatabaseId).Result;

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            if (managerResult.Result.DelimiterInfo.DelimiterDatabaseMustUseWebService)
                throw new InvalidOperationException("База на разделителях не должна использовать веб сервис");

            var segment = GetAccountSegmentForDatabaseOnDelimeter(createAccAndDb.AccountDatabaseId);
            segment.DelimiterDatabaseMustUseWebService = true;
            DbLayer.CloudServicesSegmentRepository.Update(segment);
            DbLayer.Save();

            managerResult = accountDatabaseDataManager
                .GetAccountDatabasesByIdAsync(createAccAndDb.AccountDatabaseId).Result;

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            if (!managerResult.Result.DelimiterInfo.DelimiterDatabaseMustUseWebService)
                throw new InvalidOperationException("База на разделителях должна использовать веб сервис");
        }
    }
}
