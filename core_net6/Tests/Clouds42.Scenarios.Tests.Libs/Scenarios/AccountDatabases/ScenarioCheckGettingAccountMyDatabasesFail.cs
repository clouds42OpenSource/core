﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий для неуспешного получения своих инф. баз аккаунта
    /// Действия:
    /// 1. Создадим аккаунт. [AccountCreation]
    /// 2. Проверим, что свои инф. базы не будут получены для созданного аккаунта. [CheckNoDatabaseSelected]
    /// 3. Создадим тестовые базы. Проверим, что свои инф. базы не будут получены для несуществующего аккаунта. [CheckWithEmptyAccountId]
    /// 4. Проверим, что свои инф. базы не будут получены для несуществующего пользователя аккаунта. [CheckWithEmptyAccountUserId]
    /// 5. Проверим, что свои инф. базы не будут получены для пустой строки поиска. [CheckWithEmptySearchString]
    /// </summary>
    public class ScenarioCheckGettingAccountMyDatabasesFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _createAccountDatabasesTestHelper;

        public ScenarioCheckGettingAccountMyDatabasesFail()
        {
            _createAccountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            Assert.IsNotNull(createAccountCommand.Account);
            var account = createAccountCommand.Account;
            var accountAdmin = account.AccountUsers.FirstOrDefault() ?? throw new NotFoundException("Не создался админ аккаунта");

            #endregion

            #region CheckNoDatabaseSelected

            var errorMessage = "Были выбраны свои базы аккаунта при невалидном фильтре";

            var accountDatabasesFilterParamsDto = new AccountDatabasesFilterParamsDto
            {
                Filter = new AccountDatabasesFilterDto
                {
                    AccountDatabaseAffiliationType = AccountDatabaseAffiliationTypeEnum.My,
                    AccountId = account.Id,
                    AccountUserId = accountAdmin.Id,
                    PageSize = 1,
                    SearchString = AccountDatabasesTestHelper.DbNamePattern,
                    IsServerDatabaseOnly = false
                },
                PageNumber = 1,
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Asc,
                    FieldName = "Caption"
                }
            };

            _createAccountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto, errorMessage, 0);

            #endregion

            #region CheckWithEmptyAccountId

            _createAccountDatabasesTestHelper.CreateAccountDatabases(account.Id, 3);

            accountDatabasesFilterParamsDto.Filter.AccountId = Guid.NewGuid();

            _createAccountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto, errorMessage, 0);

            #endregion

            #region CheckWithEmptyAccountUserId

            accountDatabasesFilterParamsDto.Filter.AccountId = account.Id;
            accountDatabasesFilterParamsDto.Filter.AccountUserId = Guid.NewGuid();

            _createAccountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto, errorMessage, 0);

            #endregion

            #region CheckWithEmptySearchString

            accountDatabasesFilterParamsDto.Filter.AccountUserId = accountAdmin.AccountId;
            accountDatabasesFilterParamsDto.Filter.SearchString = "";

            _createAccountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto, errorMessage, 0);

            #endregion
        }
    }
}
