﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    ///     Сценарий создания таски для удаления ролей у пользователей МЦОБ
    /// 
    ///     Предусловия: аккаунт с подключенной базой и предоставленной к ней доступом и имеет длинный список
    ///		ролей в таблице AcDbAccRolesJHO
    /// 
    ///     Действия: создаем запрос на удаление ролей
    /// 
    /// 
    ///     Проверка: таска на удаления ролей у пользователей МЦОБ создана
    /// </summary>
    public class NewMethodForMcobRemoveUserRolesTest : ScenarioBase
    {

        public override void Run()
        {
            var accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseMcobManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var accountDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(a => a.AccountId == accountDatabase.AccountId);

            var acDbAccesses = DbLayer.AcDbAccessesRepository.FirstOrDefault(a =>
                a.AccountID == user.AccountId &&
                a.AccountDatabaseID == accountDatabase.Id);
            var roles =
                "АдминистрированиеДополнительныхФормИОбработок,АдминистрированиеСохраненныхНастроек,БанковскиеОперации,БухгалтерМСФО,БухгалтерУСО," +
                "Бюджетирование,ВводЗаявокНаРасходованиеСредств,ВыплатаЗарплаты,ВыполнениеРегламентныхОперацийРегл,ГлавныйИнженер,ДенежныеДокументы," +
                "ДиспетчерАТиСМиМ,ДолевоеСтроительство,ЗавершениеСмены,ИспользованиеТорговогоОборудования,КадровикРегламентированныхДанных," +
                "КадровикУправленческихДанных,Кассир,Кладовщик,МастерСмены,МенеджерПоЗаазам,МенеджерПоЗакупкам,МенеджерПоНаборуПерсонала," +
                "МенеджерПоПродажам,МеханикАТиСМиМ,НастройкаНСИ,НастройкаНСИЗатрат,НастройкаНСИРегл,НастройкаТорговогоОборудования," +
                "НачальникАТиСМиМ,ОбменДаннымиСПрограммамиКлиентБанк,ОператорККМ,ОтражениеВРегламентированномУчете,Планирование,ПодотчетныеЛица," +
                "Пользователь,ПравоВнешнегоПодключения,ПравоВыводаИнформации,ПравоЗапускаВнешнихОтчетовИОбработок,ПравоИспользованияЭлектроннойПочты," +
                "ПравоНаЗащищенныйДокументооборотСКонтролирующимиОрганами,ПрорабскийУчасток,ПросмотрДанныхОперативногоУчетаПроизводства," +
                "ПросмотрДанныхРегламентированногоУчета,ПросмотрДвиженийДокументов,ПросмотрСтруктурыПодчиненности,РасчетчикРегламентированнойЗарплаты," +
                "РасчетчикУправленческойЗарплаты,РегламентированнаяОтчетность,РозничныеПродажи,Сертификация,СогласованиеЗаявокНаРасходованиеДС,Технолог," +
                "УстановкаДатыЗапретаИзмененияДанных,УчастокПТО,УчетЗатрат,УчетНДС,УчетОСиНМА,УчетРБП,УчетСпецодежды,Финансист,Ценообразование,ЭкономистЦеха";

            DbLayer.AcDbAccRolesJhoRepository.Insert(new AcDbAccRolesJho
            {
                AcDbAccessesID = acDbAccesses.ID,
                RoleJHO = roles
            });
            DbLayer.Save();

            var result = accountDatabaseManager.McobRemoveUserRolesToDatabase(accountDatabase.V82Name, user.Login, roles);

            if (result.Error)
                throw new InvalidOperationException($"Ошибка выполнения метода McobRemoveUserRolesToDatabase {result.Message}");

            var queueItem = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.CoreWorkerTask.TaskName == CoreWorkerTaskType.McobEditUserRolesOfDatabase.ToString());

            if (queueItem == null)
                throw new InvalidOperationException("Ошибка создания задачи по удалению ролей у пользователей МЦОБ");
        }
    }
}
