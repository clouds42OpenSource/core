﻿using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Queries.Filters;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий получения инф. баз
    ///     1) Создаём две базы
    ///     2) Проверяем что записи появились в базе данных
    ///     3) Проверяем что названия баз индетичны для первой и второй базы
    ///     4) Проверяем что индексы баз индетичны для первой и второй базы
    ///     5) Проверяем что номера баз разные для первой и второй базы
    ///     6) Ищем базы по названию, должно быть две
    ///     7) Ищем базы по индексу базы, должно быть две
    ///     8) Ищем базы по номеру базы, должна быть одна
    /// </summary>
    public class ScenarioGetDatabaseListTest : ScenarioBase
    {
        private readonly ISender _mediator;

        public ScenarioGetDatabaseListTest()
        {
            _mediator = ServiceProvider.GetRequiredService<ISender>();
        }

        public override void Run()
        {
            #region Создаём две базы

            var createAccAndDb1 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb1.Run();

            var createAccAndDb2 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb2.Run();

            #endregion

            #region Проверяем что записи появились в базе данных

            var firstDb = DbLayer.DatabasesRepository.OrderBy(item=>item.DbNumber).FirstOrDefault(item => item.Id == createAccAndDb1.AccountDatabaseId);
            var secondDb = DbLayer.DatabasesRepository.OrderBy(item => item.DbNumber).FirstOrDefault(item => item.Id == createAccAndDb2.AccountDatabaseId);

            Assert.IsNotNull(firstDb, "База не создалась!");
            Assert.IsNotNull(secondDb, "База не создалась!");

            #endregion

            #region Проверяем что названия баз индетичны для первой и второй базы

            Assert.AreEqual(firstDb.Caption, secondDb.Caption);

            #endregion

            #region Проверяем что индексы баз индетичны для первой и второй базы

            Assert.AreEqual(firstDb.DbNumber, secondDb.DbNumber);

            #endregion

            #region Проверяем что номера баз разные для первой и второй базы

            Assert.AreNotEqual(firstDb.V82Name, secondDb.V82Name);

            #endregion


            #region Ищем базы по названию, должно быть две

            var foundDbs = _mediator.Send(new GetAccountDatabasesQuery
            {
                Filter = new AccountDatabaseItemsFilter
                {
                    SearchLine = firstDb.Caption
                }
            }).Result;
            Assert.IsFalse(foundDbs.Error);
            Assert.AreEqual(2, foundDbs.Result.Records.Count);

            #endregion


            #region Ищем базы по индексу базы, должно быть две

            foundDbs = _mediator.Send(new GetAccountDatabasesQuery
            {
                Filter = new AccountDatabaseItemsFilter
                {
                    SearchLine = firstDb.DbNumber.ToString()
                }
            }).Result;
            Assert.IsFalse(foundDbs.Error);
            Assert.AreEqual(2, foundDbs.Result.Records.Count);

            #endregion


            #region Ищем базы по номеру базы, должна быть одна

            #region 1 база

            foundDbs = _mediator.Send(new GetAccountDatabasesQuery
            {
                Filter = new AccountDatabaseItemsFilter
                {
                    SearchLine = firstDb.V82Name
                }
            }).Result;
            Assert.IsFalse(foundDbs.Error);
            Assert.AreEqual(1, foundDbs.Result.Records.Count);

            #endregion

            #region 2 база

            foundDbs = _mediator.Send(new GetAccountDatabasesQuery
            {
                Filter = new AccountDatabaseItemsFilter
                {
                    SearchLine = secondDb.V82Name
                }
            }).Result;
            Assert.IsFalse(foundDbs.Error);
            Assert.AreEqual(1, foundDbs.Result.Records.Count);

            #endregion

            #endregion
        }
    }
}
