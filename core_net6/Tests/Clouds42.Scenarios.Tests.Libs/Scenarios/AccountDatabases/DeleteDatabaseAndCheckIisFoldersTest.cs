﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки на остаток каталогов после удаления опубликованой базы
    /// </summary>
    public class DeleteDatabaseAndCheckIisFoldersTest : ScenarioBase
    {
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;

        public DeleteDatabaseAndCheckIisFoldersTest()
        {
            _accountDatabaseCardManager = ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);

            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData);
            createSegmentCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var model = new PublishDto
            {
                AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                AccountUsersId = createAccountCommand.AccountAdminId
            };
            
            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var tombPreparationProvider = ServiceProvider.GetRequiredService<TombPreparationProvider>();

            var basePath = publishManager.PublishDatabase(model).Result;
            var database =
                DbLayer.DatabasesRepository.GetAccountDatabase(createAccountDatabaseCommand.AccountDatabaseId);

            try
            {
                if (string.IsNullOrEmpty(basePath))
                    throw new NotFoundException("Не удалось получить путь");

                var dbInform = _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name, createAccountCommand.AccountId).Result;
                
                TestContext.ServiceProvider.GetRequiredService<AccountDatabaseTombManager>().DeleteAccountDatabaseToTomb(createAccountDatabaseCommand.AccountDatabaseId, false);

                var db = DbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.Id == createAccountDatabaseCommand.AccountDatabaseId);

                if (db.State != DatabaseState.DelitingToTomb.ToString())
                    throw new InvalidOperationException("База не удалилась");

                var canDelete = tombPreparationProvider.CanDelete(db, out var errorMessage);

                if (!canDelete)
                    throw new InvalidOperationException(errorMessage);

                tombPreparationProvider.PreparationAccountDatabaseBeforeDeleteToTomb(db,
                    CreateBackupAccountDatabaseTrigger.ManualRemoval, createAccountCommand.AccountAdminId);

                var encodeCode = createAccountCommand.AccountId.GetEncodeGuid();

                var pathToPublish = GetPublishServerHelper.PathToPublishServer();

                var fullPath = Path.Combine(pathToPublish, encodeCode, dbInform.Database.V82Name);

                if (Directory.Exists(fullPath))
                    throw new InvalidOperationException("Отмена публикации завершилась с ошибкой, осталась папка на сервере");
            }
            finally
            {
                publishManager.CancelPublishDatabaseWithWaiting(databaseId: createAccountDatabaseCommand.AccountDatabaseId);
            }
        }
    }
}
