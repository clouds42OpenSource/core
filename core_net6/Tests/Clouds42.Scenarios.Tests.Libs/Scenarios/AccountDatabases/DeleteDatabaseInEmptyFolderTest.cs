﻿using System;
using System.IO;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.AccountDatabase.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий отправки на удаления ИБ у которой папка пустая и отсутствует файл 1CD
    ///  
    /// Предусловия: для аккаунта test_account создаем ИБ и удаляем содержимое папки
    /// отправляем базу на удаление
    /// 
    /// Действия: проверяем что статус базы изменился на удалена и таска
    /// на удаление не запустилась
    /// </summary>
    public class DeleteDatabaseInEmptyFolderTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var dbInform =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var task = DbLayer.CoreWorkerTaskRepository.FirstOrDefault(t =>
                t.TaskName == "DeleteAccountDatabaseToTombJob");

            var accountDatabasesPathHelper = ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();

            var fullPath = accountDatabasesPathHelper.GetPath(dbInform);

            if (!Directory.Exists(fullPath))
                throw new InvalidOperationException("После создания базы папки не существует");

            //удаляем файл из папки
            var di = new DirectoryInfo(fullPath);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
            var accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseTombManager>();

            var result =
                accountDatabaseManager.DeleteAccountDatabaseToTomb(createAccountDatabaseCommand.AccountDatabaseId,
                    false);

            if (!result.Result)
                throw new InvalidOperationException(result.Message);

            var db = DbLayer.DatabasesRepository.FirstOrDefault(x =>
                x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            if (db.StateEnum != DatabaseState.DeletedToTomb)
                throw new InvalidOperationException("База не изменила статус");

            var taskQueue = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(d => d.CoreWorkerTaskId == task.ID);

            if (taskQueue != null)
                throw new InvalidOperationException("Таска на удаление все равно была запущена");
        }
    }
}
