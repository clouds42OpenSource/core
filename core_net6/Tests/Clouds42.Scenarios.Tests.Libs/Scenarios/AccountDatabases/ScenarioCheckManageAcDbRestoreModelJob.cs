﻿using System;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.AccountDatabase.Restore.Managers;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки работы джобы аудита и корректировки модели восстановления инф. баз
    ///     Действия:
    ///         1) Создаем аккаунт и 2 серверные инф. базы
    ///         2) Для одной базы меняем тип восстановления на "полная"
    ///         3) Имитируем запуск джобы аудита и корректировки модели восстановления
    ///         4) Проверям что у всех баз модель восстановления соответствует SQL серверу
    /// </summary>
    public class ScenarioCheckManageAcDbRestoreModelJob : ScenarioBase
    {
        private readonly AccountDatabaseRestoreModelManager _accountDatabaseRestoreModelManager;

        public ScenarioCheckManageAcDbRestoreModelJob()
        {
            Services.AddTransient<IAccountDatabaseRestoreModelHelper, AccountDatabaseRestoreModelHelperTest>();
            _accountDatabaseRestoreModelManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseRestoreModelManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта и инф.базы
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccAndDb.AccountDetails);
            createAccountDatabaseCommand.Run();

            var firstAccountDatabase = GetAccountDatabase(createAccAndDb.AccountDatabaseId);
            var secondAccountDatabase = GetAccountDatabase(createAccountDatabaseCommand.AccountDatabaseId);

            #endregion

            #region Смена типа и модели восстановления

            firstAccountDatabase.IsFile = false;
            secondAccountDatabase.IsFile = false;
            firstAccountDatabase.RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Simple;
            secondAccountDatabase.RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full;

            DbLayer.DatabasesRepository.Update(firstAccountDatabase);
            DbLayer.DatabasesRepository.Update(secondAccountDatabase);

            DbLayer.Save();

            #endregion

            #region Имитация запуска джобы аудита и корректировки модели восстановления

            var result = _accountDatabaseRestoreModelManager.ManageAccountDatabaseRestoreModel();
            Assert.IsFalse(result.Error, result.Message);

            #endregion

            #region Проверка моделей восстановления инф. баз

            DbLayer.DatabasesRepository.Reload(firstAccountDatabase);
            DbLayer.DatabasesRepository.Reload(secondAccountDatabase);

            Assert.IsTrue(firstAccountDatabase.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Simple);
            Assert.IsTrue(secondAccountDatabase.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Full);

            #endregion
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IAccountDatabaseRestoreModelHelper, AccountDatabaseRestoreModelHelperTest>();
        }


        /// <summary>
        /// Получить инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
            => DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == accountDatabaseId);
    }
}
