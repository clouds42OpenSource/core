﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий для успешного получения расшаренных инф. баз аккаунта
    /// Действия:
    /// 1. Создадим первый аккаунт и создадим 3 его базы. [AccountOneCreation]
    /// 2. Создадим второй аккаунт и создадим обычного пользователя этого аккаунта. [AccountTwoCreation]
    /// 3. Добавим доступы обычному пользователю второго аккаунта к двум базам первого аккаунта и зададим фильтр. [AcDbAccessAndFilterCreation]
    /// 4. Проверим, что будет получено 2-е инф. базы для пользователя, которому предоставили доступ. [CheckGettingDatabasesForAccountUser]
    /// 5. Проверим, что база в состоянии отличном от Ready не будет получена. [CheckNotReadyDatabase]
    /// 6. Проверим, что для админа аккаунта будут получены все расшаренные базы для аккаунта. [CheckGettingDatabasesByAccountAdmin]
    /// </summary>
    public class ScenarioCheckGettingSharedAccountDatabasesSuccess : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioCheckGettingSharedAccountDatabasesSuccess()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }
        public override void Run()
        {
            #region AccountOneCreation

            var createAccountCommandOne = new CreateAccountCommand(TestContext);
            createAccountCommandOne.Run();

            var accountOne = createAccountCommandOne.Account;
            var databaseIdsAccountOne = _accountDatabasesTestHelper.CreateAccountDatabases(accountOne.Id, 3);

            #endregion

            #region AccountTwoCreation

            var createAccountCommandTwo = new CreateAccountCommand(TestContext);
            createAccountCommandTwo.Run();

            var accountTwo = createAccountCommandTwo.Account;
            var accountTwoAdmin = accountTwo.AccountUsers.FirstOrDefault() ??
                                  throw new NotFoundException($"Не создался админ аккаунта {accountTwo.Id}");

            _accountUserTestHelper.CreateAccountUser(accountTwo.Id);
            var accountTwoUser = _accountUserTestHelper.GetOrCreateUserWithAccountUserRole(accountTwo.Id);

            #endregion

            #region AcDbAccessAndFilterCreation

            _acDbAccessTestHelper.CreateAcDbAccess(databaseIdsAccountOne[0], accountTwo.Id, Guid.NewGuid(), accountTwoUser.Id);
            _acDbAccessTestHelper.CreateAcDbAccess(databaseIdsAccountOne[1], accountTwo.Id, Guid.NewGuid(), accountTwoUser.Id);

            var filter = new AccountDatabasesFilterParamsDto
            {
                Filter = new AccountDatabasesFilterDto
                {
                    AccountDatabaseAffiliationType = AccountDatabaseAffiliationTypeEnum.Shared,
                    AccountId = accountTwo.Id,
                    AccountUserId = accountTwoUser.Id,
                    PageSize = 5,
                    SearchString = AccountDatabasesTestHelper.DbNamePattern,
                    IsServerDatabaseOnly = false
                },
                PageNumber = 1,
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Asc,
                    FieldName = "Caption"
                }
            };

            #endregion

            #region CheckGettingDatabasesForAccountUser

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter( filter, "Получили некорректное количество расшаренных баз", 2);

            #endregion

            #region CheckNotReadyDatabase

            _accountDatabasesTestHelper.ChangeDatabasesState([databaseIdsAccountOne[0]], DatabaseState.DeletedToTomb);
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(filter, "Получили базу в статусе отличном от Ready", 1);

            #endregion

            #region CheckGettingDatabasesByAccountAdmin

            _acDbAccessTestHelper.CreateAcDbAccess(databaseIdsAccountOne[2], accountTwo.Id, Guid.NewGuid(),
                _accountUserTestHelper.CreateAccountUser(accountTwo.Id));

            filter.Filter.AccountUserId = accountTwoAdmin.Id;
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(filter, "Не найдены все базы для админа аккаунта", 2);

            #endregion
        }
    }
}
