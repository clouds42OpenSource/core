﻿using System;
using System.Collections.Generic;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Тест проверяет редактирование номер БД V82Name
    /// </summary>
    public class ChangeAccountDatabaseV82NameTest : ScenarioBase
    {

        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var accountDatabaseEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();

            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            var longName = new string('a', 40);
            var bigName = new string('A', 20);
            const string wrongChar = "42_5664_378''//";
            const string emptyName = "";
            var coolName = new string('a', 20);

            var v82Names = new List<string> { emptyName, wrongChar, longName, bigName };
            v82Names.ForEach(v82Name =>
            {
                var result = accountDatabaseEditManager.SetV82Name(database.Id, v82Name);
                if (!result.Error)
                    throw new InvalidOperationException("Сбой валидации.");
            });

            var goodResult = accountDatabaseEditManager.SetV82Name(database.Id, coolName);
            if (goodResult.Error)
                throw new InvalidOperationException("Ошибка метода при валидных данных.");

            database = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);
            Assert.AreEqual(database.V82Name, coolName, "Ошибка смены номера в базе");
        }
    }
}
