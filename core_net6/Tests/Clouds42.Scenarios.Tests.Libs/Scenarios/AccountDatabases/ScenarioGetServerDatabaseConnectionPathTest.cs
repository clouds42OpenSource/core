﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения строки подключения к серверной инф. базе
    ///     1) Создаем аккаунт с серверной инф. базой
    ///     2) Имитируем открытие карточки базы
    ///     3) Проверяем что модель получена, путь подключения к базе правильный
    /// </summary>
    public class ScenarioGetServerDatabaseConnectionPathTest : ScenarioBase
    {
        private readonly AccountDatabaseCardManager _accountDatabaseDataManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public ScenarioGetServerDatabaseConnectionPathTest()
        {
            _accountDatabasePathHelper = ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _accountDatabaseDataManager = ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(createAccAndDb.AccountDatabaseId);
            Assert.IsNotNull(accountDatabase);

            accountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();
            DbLayer.DatabasesRepository.Reload(accountDatabase);

            var accountDatabaseDataResult =
                _accountDatabaseDataManager.GetAccountDatabaseInfo(accountDatabase.V82Name,
                    createAccAndDb.AccountDetails.AccountId);

            var actualAcDbConnectionPath = _accountDatabasePathHelper.GetServerDatabaseConnectionPath(accountDatabase);

            Assert.IsFalse(accountDatabaseDataResult.Error);
            Assert.IsNotNull(accountDatabaseDataResult.Result);
            Assert.AreEqual(accountDatabaseDataResult.Result.Database.DatabaseConnectionPath, actualAcDbConnectionPath);
        }
    }
}
