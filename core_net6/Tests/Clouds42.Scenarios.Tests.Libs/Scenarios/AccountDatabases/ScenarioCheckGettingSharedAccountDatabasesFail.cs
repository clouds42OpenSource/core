﻿using System;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий для неуспешного получения расшаренных инф. баз аккаунта
    /// Действия:
    /// 1. Создадим аккаунт и его базы. [AccountOneCreation]
    /// 2. Создадим второй аккаунт и дадим доступ пользователю доступ к двум базам первого аккаунта. [AccountTwoCreation]
    /// 3. Проверим, что свои инф. базы не будут получены для несуществующего аккаунта. [CheckEmptyAccountId]
    /// 4. Проверим, что свои инф. базы не будут получены для несуществующего пользователя аккаунта. [CheckEmptyAccountUserId]
    /// 5. Проверим, что свои инф. базы не будут получены для пустой строки поиска. [CheckEmptySearchString ]
    /// </summary>
    public class ScenarioCheckGettingSharedAccountDatabasesFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioCheckGettingSharedAccountDatabasesFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            #region AccountOneCreation

            var createAccountCommandOne = new CreateAccountCommand(TestContext);
            createAccountCommandOne.Run();

            var accountOne = createAccountCommandOne.Account;
            var databaseIdsAccountOne = _accountDatabasesTestHelper.CreateAccountDatabases(accountOne.Id, 3);

            #endregion

            #region AccountTwoCreation

            var createAccountCommandTwo = new CreateAccountCommand(TestContext);
            createAccountCommandTwo.Run();

            var accountTwo = createAccountCommandTwo.Account;
            var accountTwoUser = _accountUserTestHelper.GetOrCreateUserWithAccountUserRole(accountTwo.Id);

            _acDbAccessTestHelper.CreateAcDbAccess(databaseIdsAccountOne[0], accountTwo.Id, Guid.NewGuid(), accountTwoUser.Id);
            _acDbAccessTestHelper.CreateAcDbAccess(databaseIdsAccountOne[1], accountTwo.Id, Guid.NewGuid(), accountTwoUser.Id);

            #endregion

            #region CheckEmptyAccountId

            var databasesFilterParamsDto = new AccountDatabasesFilterParamsDto
            {
                Filter = new AccountDatabasesFilterDto
                {
                    AccountDatabaseAffiliationType = AccountDatabaseAffiliationTypeEnum.Shared,
                    AccountId = Guid.NewGuid(),
                    AccountUserId = accountTwoUser.Id,
                    PageSize = 5,
                    SearchString = AccountDatabasesTestHelper.DbNamePattern,
                    IsServerDatabaseOnly = false
                },
                PageNumber = 1,
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Asc,
                    FieldName = "Caption"
                }
            };

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(databasesFilterParamsDto,
                "Получили расшаренные базы при невалидном значении Id аккаунта в фильтре", 0);

            #endregion

            #region CheckEmptyAccountUserId

            databasesFilterParamsDto.Filter.AccountUserId = Guid.NewGuid();
            databasesFilterParamsDto.Filter.AccountId = accountTwo.Id;

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(databasesFilterParamsDto,
                "Получили расшаренные базы при невалидном значении AccountUserId в фильтре", 0);

            #endregion

            #region CheckEmptySearchString

            databasesFilterParamsDto.Filter.SearchString = "";
            databasesFilterParamsDto.Filter.AccountUserId = accountTwoUser.Id;

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(databasesFilterParamsDto, "Не получили инф. базы при пустой строке поиска в  фильтре", 2);

            #endregion
        }
    }
}
