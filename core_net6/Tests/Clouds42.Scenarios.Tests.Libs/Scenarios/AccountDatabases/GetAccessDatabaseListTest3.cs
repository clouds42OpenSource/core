﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
    /// </summary>
    /// <remarks>
    /// тест 3)
    /// есть 2 базы в статусе Ready,
    /// 1 в статусе "Удалена в склеп" и
    /// 1 в статусе "Заархивирована в склеп"
    /// у пользователя есть доступ к 2 базам к удаленной и заархивированной
    /// убедиться, что получим список из 1 базы
    /// </remarks>
    public class GetAccessDatabaseListTest3 : ScenarioBase
    {
        public override void Run()
        {            

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommandList = new CreateAccountDatabaseCommand[2];

            var infoDatabaseModel = new InfoDatabaseDomainModelTest(TestContext);

            var acDbAccessesManager = TestContext.ServiceProvider.GetRequiredService<AcDbAccessManager>();

            var acDbAccessesPostAddAllUsersModel = new AcDbAccessPostAddAllUsersModelDto
            {
                AccountID = createAccountCommand.AccountId,
                LocalUserID = createAccountCommand.AccountAdminId
            };

            // Есть две базы в компании Комп0
            for (var i = 0; i < 2; i++)
            {
                infoDatabaseModel.DataBaseName = $"Тестовая база {i}";
                createAccountDatabaseCommandList[i] = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, infoDatabaseModel, TestContext.AccessProvider);
                createAccountDatabaseCommandList[i].Run();

                // У пользователя test0 есть доступ ко всем своим базам
                acDbAccessesPostAddAllUsersModel.AccountDatabaseID = createAccountDatabaseCommandList[i].AccountDatabaseId;
                acDbAccessesManager.AddAccessesForAllAccountsUsers(acDbAccessesPostAddAllUsersModel);
            }

            var accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            accountDatabaseManager.ChangeDatabaseState(createAccountDatabaseCommandList[0].AccountDatabaseId,
                DatabaseState.DeletedToTomb);
            accountDatabaseManager.ChangeDatabaseState(createAccountDatabaseCommandList[1].AccountDatabaseId,
                DatabaseState.DetachedToTomb);

            var accountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();
            var result =  accountDatabaseDataManager.GetAccessDatabaseListAsync(createAccountCommand.AccountAdminId).Result;            

            Assert.AreEqual(0,result.Result.Count);
        }
    }
}
