﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения строки подключения к файловой инф. базе
    ///     1) Создаем аккаунт с файловой инф. базой
    ///     2) Имитируем открытие карточки базы
    ///     3) Проверяем что модель получена, путь подключения к базе правильный
    /// </summary>
    public class ScenarioGetFileDatabaseConnectionPathTest : ScenarioBase
    {
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public ScenarioGetFileDatabaseConnectionPathTest()
        {
            _accountDatabasePathHelper = ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _accountDatabaseCardManager = ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var database = DbLayer.DatabasesRepository.GetAccountDatabase(createAccAndDb.AccountDatabaseId);

            var accountDatabaseDataResult =
                _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name,
                    createAccAndDb.AccountDetails.AccountId);

            var actualAcDbConnectionPath = _accountDatabasePathHelper.GetPath(createAccAndDb.AccountDatabaseId);

            Assert.IsFalse(accountDatabaseDataResult.Error);
            Assert.IsNotNull(accountDatabaseDataResult.Result);
            Assert.AreEqual(accountDatabaseDataResult.Result.Database.DatabaseConnectionPath, actualAcDbConnectionPath);
        }
    }
}
