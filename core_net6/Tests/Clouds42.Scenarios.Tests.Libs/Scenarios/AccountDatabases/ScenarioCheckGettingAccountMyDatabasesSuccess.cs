﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий для успешного получения своих инф. баз аккаунта
    /// Действия:
    /// 1. Создадим аккаунт, его инф. базы и фильтр для получения инф. баз. [AccountAndDependentEntitiesCreation]
    /// 2. Проверим, что получим все созданные инф. базы по фильтру для админа аккаунта. [CheckGettingDatabases]
    /// 3. Проверим, что не получим инф. базу в статусе Undefined для админа аккаунта. [CheckGettingNoUndefinedDatabases]
    /// 4. Проверим, что не получим инф. базу в статусе DeletedFromCloud для админа аккаунта. [CheckGettingNoDeletedFromCloudDatabases]
    /// 5. Проверим, что не получим инф. базу в статусе DelitingToTomb для админа аккаунта. [CheckGettingNoDelitingToTombDatabases]
    /// 6. Создадим пользователя аккаунта с ролью Hotline. Проверим, что будут получены базы в статусе DelitingToTomb, DeletedToTomb, DetachingToTomb для пользователя с ролью Hotline. [CheckGettingDatabasesForHotlineUser]
    /// 7. Изменим строку поиска фильтра и проверим что корректно ищутся базы по строке поиска. [CheckGettingDatabasesBySearchStringInFilter]
    /// </summary>
    public class ScenarioCheckGettingAccountMyDatabasesSuccess : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;

        public ScenarioCheckGettingAccountMyDatabasesSuccess()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountAndDependentEntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            
            Assert.IsNotNull(createAccountCommand.Account);
            var account = createAccountCommand.Account;
            var accountAdmin = account.AccountUsers.FirstOrDefault() ??
                               throw new NotFoundException("Не создался админ аккаунта");

            var databaseToCreateCount = 5;

            var databaseIds = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, databaseToCreateCount);

            var accountDatabasesFilterParamsDto = new AccountDatabasesFilterParamsDto
            {
                Filter = new AccountDatabasesFilterDto
                {
                    AccountDatabaseAffiliationType = AccountDatabaseAffiliationTypeEnum.My,
                    AccountId = account.Id,
                    AccountUserId = accountAdmin.Id,
                    PageSize = databaseToCreateCount,
                    SearchString = AccountDatabasesTestHelper.DbNamePattern,
                    IsServerDatabaseOnly = false
                },
                PageNumber = 1,
                SortingData = new SortingDataDto
                {
                    SortKind = SortType.Asc,
                    FieldName = "Caption"
                }
            };

            var currentDbIndex = 0;
            var dbWrongCount = 0;

            #endregion

            #region CheckGettingDatabases

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto, "Получили меньше баз, чем было создано", databaseToCreateCount- dbWrongCount);

            #endregion

            #region CheckGettingNoUndefinedDatabases

            ChangeDatabaseStatus(databaseIds[currentDbIndex], DatabaseState.Undefined, ref dbWrongCount, ref currentDbIndex);
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto,
                "Выбралась база со статусом Undefined", databaseToCreateCount - dbWrongCount);

            #endregion

            #region CheckGettingNoDeletedFromCloudDatabases

            ChangeDatabaseStatus(databaseIds[currentDbIndex], DatabaseState.DeletedFromCloud, ref dbWrongCount, ref currentDbIndex);
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto,
                "Выбралась база со статусом DeletedFromCloud", databaseToCreateCount - dbWrongCount);

            #endregion

            #region CheckGettingNoDeltingToTombDatabases

            ChangeDatabaseStatus(databaseIds[currentDbIndex], DatabaseState.DelitingToTomb, ref dbWrongCount, ref currentDbIndex);
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto,
                "Выбралась база со статусом DelitingToTomb", databaseToCreateCount - dbWrongCount);

            #endregion

            #region CheckGettingDatabasesForHotlineUser

            var newUser = _accountUserTestHelper.GetOrCreateUserWithAccountUserRole(account.Id);

            _accountUserTestHelper.ChangeUserRoles(newUser, AccountUserGroup.Hotline);

            accountDatabasesFilterParamsDto.Filter.AccountUserId = newUser.Id;

            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto,
                "Не выбрались базы, доступные для пользователя с правами Hotline",
                3);

            #endregion

            #region CheckGettingDatabasesBySearchStringInFilter

            accountDatabasesFilterParamsDto.Filter.SearchString = $" {databaseToCreateCount}";
            _accountDatabasesTestHelper.GetAndCheckDatabasesDataByFilter(accountDatabasesFilterParamsDto,
                "Не выбрались базы по строке поиске для пользователя с правами Hotline",
                1);

            #endregion

        }

        /// <summary>
        /// Изменить статус инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="newState">Новый статус</param>
        /// <param name="dbWrongCount">Количество неподходящих баз под выборку</param>
        /// <param name="currentDbIndex">Текущий индекс инф. базы в выборке</param>
        private void ChangeDatabaseStatus(Guid databaseId, DatabaseState newState, ref int dbWrongCount, ref int currentDbIndex)
        {
            var successedChanged =_accountDatabasesTestHelper.ChangeDatabasesState([databaseId], newState);
            if (!successedChanged)
                throw new InvalidOperationException($"Не удалось изменить статус базы {databaseId} на {newState}");

            dbWrongCount++;
            currentDbIndex++;
        }
    }
}
