﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий правильной проверки пересчета размера базы аккаунта
    /// 1) Создаем аккаунт и базу
    /// 2) Отправляем базу на пересчет размера
    /// 3) Проверяем что после пересчета размер остался равен 0
    /// </summary>
    public class ScenarioCheckSizeOfCorrectAccountDb : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            if (account == null)
                throw new InvalidOperationException("Аккаунт не найден или не был создан");

            var accountDbId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;
            var segment = GetAccountSegment(account.Id);
            var sqlServer = DbLayer.CloudServicesSqlServerRepository.FirstOrDefault(x => x.ID == segment.SQLServerID);

            sqlServer.ConnectionAddress = "cl-db-core-ha";
            DbLayer.CloudServicesSqlServerRepository.Update(sqlServer);

            var accountDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDbId);
            accountDb.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDb);
            DbLayer.Save();

            var accountDbManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();

            accountDbManager.RecalculateSizeOfAccountDatabase(accountDbId);

            var accDbAfterRecalculateSize = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDbId);

            Assert.AreEqual(0,accDbAfterRecalculateSize.SizeInMB);

        }
    }
}
