﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Clouds42.AccountDatabase.Internal;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки строки подключения к базе
    /// </summary>
    public class AccountDatabaseConnectionStringTest : ScenarioBase
    {
        public override void Run()
        {            

            var createAccountCommand = new CreateAccountCommand(TestContext);

            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();


            var dbInform = DbLayer.DatabasesRepository.GetAccountDatabase(createAccountDatabaseCommand.AccountDatabaseId);

            new Run1CEpfMcobCommand(dbInform.V82Name, false, string.Empty, string.Empty, PlatformType.V82);
            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();
            var run1CEpfMcobCommandHandler = new Run1CEpfMcobCommandHandler(DbLayer,segmentHelper,Logger);
            var expectedConnectionString = run1CEpfMcobCommandHandler.GetConnectionString(segmentHelper.GetEnterpriseServer82(createAccountCommand.Account),dbInform.V82Name, false, string.Empty);

            var segment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault(x => x.ID == createAccountCommand.Segment.Id);
            var item = DbLayer.CloudServicesEnterpriseServerRepository.FirstOrDefault(x => x.ID == segment.EnterpriseServer82ID);
            var actualConnectionString = $"/S\"{item.ConnectionAddress}\\{dbInform.V82Name}\"";

            if (!string.Equals(expectedConnectionString, actualConnectionString))
                throw new InvalidOperationException($"Фактически полученная строка \"{actualConnectionString}\" подключения к базе \"{dbInform.V82Name}\" не совпадает с ожидаемой строкой {expectedConnectionString}");
        }
    }
}
