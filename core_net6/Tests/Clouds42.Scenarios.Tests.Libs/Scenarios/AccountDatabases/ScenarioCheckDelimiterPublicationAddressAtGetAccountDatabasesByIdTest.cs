﻿using System.Linq;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки поля "Адрес публикации"
    /// при получении данных базе на разделителях
    ///
    /// 1) Создаем аккаунт и материнскую базу
    /// 2) Выполняем запрос на получения данных и смотрим что корректно достается адрес публикации
    /// </summary>
    public class ScenarioCheckDelimiterPublicationAddressAtGetAccountDatabasesByIdTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();

            var createDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<CreateDelimiterSourceAccountDatabaseManager>();

            var databaseOnDelimiter =
                DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == createAccAndDb.AccountDatabaseId)
                    ?.AccountDatabaseOnDelimiter ??
                throw new NotFoundException("Не удалось найти базу на разделителях");

            databaseOnDelimiter.SourceAccountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(databaseOnDelimiter.SourceAccountDatabase);
            DbLayer.Save();

            var delimiterSourceAccountDatabaseDto = new DelimiterSourceAccountDatabaseDto
            {
                AccountDatabaseId = databaseOnDelimiter.SourceAccountDatabase.Id,
                DbTemplateDelimiterCode = databaseOnDelimiter.DbTemplateDelimiterCode,
                DatabaseOnDelimitersPublicationAddress = "s"
            };

            var managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var findDatabase = DbLayer.DatabasesRepository.GetAccountDatabasesById(createAccAndDb.AccountDatabaseId).Result;

            Assert.IsNotNull(findDatabase);

            Assert.AreEqual(findDatabase.SiteName, delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress);
            Assert.AreEqual(findDatabase.DemoSiteName, databaseOnDelimiter.DbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress);

            databaseOnDelimiter.Zone = 5;
            DbLayer.AccountDatabaseDelimitersRepository.Update(databaseOnDelimiter);
            DbLayer.Save();

            var findDatabaseTwo =
                DbLayer.DatabasesRepository.GetDelimitersDbsForAccUserAsync(
                    createAccAndDb.AccountDetails.AccountAdminId).Result;

            Assert.IsNotNull(findDatabaseTwo);

            Assert.AreEqual(findDatabaseTwo.First().SiteName, delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress);
        }
    }
}
