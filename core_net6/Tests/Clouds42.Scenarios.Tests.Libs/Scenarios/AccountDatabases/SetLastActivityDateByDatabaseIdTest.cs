﻿using Clouds42.AccountDatabase.Activity.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Тест установки даты последней активности в информационной базе
    /// Сценарий:
    ///         1. Создаем аккаунт и базу
    ///         2. Устанавливаем дату последней активности
    ///         3. Проверяем, что дата активности изменилась и предыдущая дата меньше
    /// </summary>
    public class SetLastActivityDateByDatabaseIdTest : ScenarioBase
    {
        private readonly IFixationAccountDatabaseActivityManager _accountDatabaseActivityManager;

        public SetLastActivityDateByDatabaseIdTest()
        {
            _accountDatabaseActivityManager = TestContext.ServiceProvider.GetRequiredService<IFixationAccountDatabaseActivityManager>();
        }

        public override void Run()
        {
            var createDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createDatabaseAndAccountCommand.Run();
            
            var database = GetAccountDatabase(createDatabaseAndAccountCommand.AccountDatabaseId);

            var lastActivityDate = database.LastActivityDate;

            var fixLastActivity = _accountDatabaseActivityManager.FixLastActivity(database.Id);
            Assert.IsFalse(fixLastActivity.Error);

            var dbAfterFixationActivityDate = GetAccountDatabase(createDatabaseAndAccountCommand.AccountDatabaseId);
            Assert.IsTrue(lastActivityDate < dbAfterFixationActivityDate.LastActivityDate);
            Assert.AreNotEqual(dbAfterFixationActivityDate.LastActivityDate, lastActivityDate);
        }

        /// <summary>
        /// Получить модель информационной базы
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        /// <returns>Модель информационной базы</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid databaseId)
        {
            var database = DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == databaseId);
            Assert.IsNotNull(database);

            return database;
        }
    }
}
