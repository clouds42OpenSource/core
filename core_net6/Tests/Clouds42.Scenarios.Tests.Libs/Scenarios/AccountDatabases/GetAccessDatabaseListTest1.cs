﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
    /// </summary>
    /// <remarks>
    /// тест 1: Есть 5 баз в статусе ready.
    /// ко всем базам предоставлен доступ пользователю test0 из компании Комп0
    /// и к 1 базе предоставлен доступ пользователю test1 из компании Комп2
    /// и к 3 базам предоставлен доступ пользователю test2 из компании Комп0
    /// Убедиться что в результате получаем список из 5 баз для первого пользователя.
    /// </remarks>
    public class GetAccessDatabaseListTest1 : ScenarioBase
    {
        private readonly StartProcessToWorkDatabaseManager _startProcessToWorkDatabaseManager;

        public GetAccessDatabaseListTest1()
        {
            _startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
        }
        public override void Run()
        {
            var createAccountCommandList = new CreateAccountCommand[2];

            var accountData = new AccountRegistrationModelTest();

            for (var i = 0; i < createAccountCommandList.Length; i++)
            {
                accountData.Login = $"TestLogin{i}{DateTime.Now:mmss}";
                accountData.Email = $"{accountData.Login}@test.ru";
                accountData.AccountCaption = $"Комп{i}";
                accountData.FullPhoneNumber = $"+{PhoneNumberGeneratorHelperTest.Generate()}";

                createAccountCommandList[i] = new CreateAccountCommand(TestContext, accountData);
                createAccountCommandList[i].Run();
            }

            // имеем две компании Комп0 и Комп1 и двух пользователей test0 и test1.
            // добавляем третьего пользователя в аккаунт Комп1
            var accountUserManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var accountRegistrationModel = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommandList[0].AccountId,
                Login = $"TestLogin{DateTime.Now:mmss}",
                Email = "test2@test.ru"
            };

            var test2AccountUserId = accountUserManager.AddToAccount(accountRegistrationModel).Result.Result;

            var createAccountDatabaseCommandList = new CreateAccountDatabaseCommand[5];

            var infoDatabaseModel = new InfoDatabaseDomainModelTest(TestContext);

            // ко всем базам предоставлен доступ пользователю test из компании Комп1
            for (var i = 0; i < createAccountDatabaseCommandList.Length; i++)
            {
                infoDatabaseModel.DataBaseName = $"Тестовая база {i}";
                createAccountDatabaseCommandList[i] = new CreateAccountDatabaseCommand(TestContext, createAccountCommandList[0], infoDatabaseModel, TestContext.AccessProvider);
                createAccountDatabaseCommandList[i].Run();

                AddAccessesForAllAccountsUsers(createAccountDatabaseCommandList[i].AccountDatabaseId,
                    createAccountCommandList[0].AccountAdminId);
            }

            //к 3 базам предоставлен доступ пользователю test2 из компании Комп0
            for (var i = createAccountDatabaseCommandList.Length - 3; i < createAccountDatabaseCommandList.Length; i++)
                AddAccessesForAllAccountsUsers(createAccountDatabaseCommandList[i].AccountDatabaseId,
                    test2AccountUserId);

            var accountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();
            var result = accountDatabaseDataManager.GetAccessDatabaseListAsync(createAccountCommandList[0].AccountAdminId).Result;

            Assert.AreEqual(5, result.Result.Count);
        }

        /// <summary>
        /// Предоставить доступы в базу для всех пользователей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="accountUserIds">Список Id пользователей</param>
        private void AddAccessesForAllAccountsUsers(Guid accountDatabaseId, params Guid[] accountUserIds)
        {
            _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(new StartProcessesOfManageAccessesToDbDto
            {
                DatabaseId = accountDatabaseId,
                UsersId = accountUserIds.ToList()
            });
        }
    }
}
