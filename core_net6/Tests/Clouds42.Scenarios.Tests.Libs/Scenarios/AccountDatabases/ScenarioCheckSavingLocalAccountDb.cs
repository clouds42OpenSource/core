﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Проверка метода сохранения локальной БД аккаунта
    ///     Действия: 
    ///         1) Создаем аккаунт с базой
    ///         2) Вызываем метод сохранения локальной БД аккаунта
    ///         3) Проверяем что изменения сохранились
    ///         4) Снова вызываем метод, но передаем объект с пустым ID
    ///         5) Проверяем что запись создалась
    /// </summary>
    public class ScenarioCheckSavingLocalAccountDb : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var accountDb =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var defaultCaption = accountDb.Caption;

            var accountDatabaseManager = ServiceProvider.GetRequiredService<IAccountDatabaseManager>();

            var saveLocalAccDb = new SaveLocalAccountDatabasesDto
            {
                ID = accountDb.Id,
                AccountID = createAccountCommand.AccountId,
                Caption = "ChangeCaption",
                SqlName = accountDb.SqlName,
                IsFile = (bool) accountDb.IsFile,
                ServerName = accountDb.V82Server
            };

            var result = accountDatabaseManager.SaveLocalAccountDatabases(saveLocalAccDb);

            Assert.IsNotNull(result.Result);

            var dbAfterChange = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == result.Result);

            Assert.IsNotNull(dbAfterChange);

            Assert.AreNotEqual(dbAfterChange.Caption, defaultCaption);

            var emptyLocalAccDb = new SaveLocalAccountDatabasesDto
            {
                ID = Guid.Empty,
                Caption = "empty",
                AccountID = createAccountCommand.AccountId,
                FilePath = "empty",
                SqlName = "empty",
                Platform = PlatformType.V83,
                IsFile = true,
                ServerName = "empty"
            };

            var emptyResult = accountDatabaseManager.SaveLocalAccountDatabases(emptyLocalAccDb);
            Assert.IsNotNull(emptyResult.Result);

            var emptyRecord = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == emptyLocalAccDb.ID);
            Assert.IsNull(emptyRecord);
        }
    }
}
