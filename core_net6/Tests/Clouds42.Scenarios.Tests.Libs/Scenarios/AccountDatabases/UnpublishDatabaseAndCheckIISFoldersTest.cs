﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки на остаток каталогов после отмены публикации базы
    /// </summary>
    public class UnpublishDatabaseAndCheckIISFoldersTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData);
            createSegmentCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var model = new PublishDto
            {
                AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                AccountUsersId = createAccountCommand.AccountAdminId
            };

            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();

            var basePath = publishManager.PublishDatabase(model).Result;

            if (string.IsNullOrEmpty(basePath))
                throw new InvalidOperationException("Не удалось получить путь");

            publishManager.CancelPublishDatabaseWithWaiting(createAccountDatabaseCommand.AccountDatabaseId);

            var db = DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var encodeCode = createAccountCommand.AccountId.GetEncodeGuid();

            var pathToPublish = GetPublishServerHelper.PathToPublishServer();

            var fullPath = Path.Combine(pathToPublish, encodeCode, db.V82Name);

            if (db.PublishStateEnum != PublishState.Unpublished)
                throw new InvalidOperationException("Отмена публикации не прошла успешно");

            if (Directory.Exists(fullPath))
                throw new InvalidOperationException("Отмена публикации завершилась с ошибкой, осталась папка на сервере");

        }
    }
}
