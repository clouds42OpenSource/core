﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки получения списка баз, к которым у пользователя есть доступ
    /// </summary>
    /// <remarks>
    /// Проверяем адекватность поля JHO.
    /// </remarks>>
    public class GetAccessDatabaseListTestJho : ScenarioBase
    {
        public override void Run()
        {            

            var createAccountCommand = new CreateAccountCommand(TestContext);

            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();
            
            // Шаблон для реализации теста получения списка баз, к которым у пользователя есть доступ.
            // Пока проверка будет всегда валидной, до получения необходимых спецификаций метода.
        }
    }
}
