﻿using System;
using System.Threading.Tasks;
using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Проверка чистки таблицы AcDbAccesses после удаление пользователя
    /// Сценарий : создаём аккаунт с двумя пользователями и базой. Удаляем пользователя
    /// у которого есть доступ к этой базе. Проверяем что его запись в AcDbAccesses удалена.
    /// </summary>
    public class ScenarioDeleteUserAndCheckAcDbAccesses : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly AccountDatabaseUserAccessManager _accessToAcDbOnDelimitersManager;

        public ScenarioDeleteUserAndCheckAcDbAccesses()
        {
            _accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            #region createAccount
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            var accountDetails = createAccountDatabaseAndAccountCommand.AccountDetails;
            var databaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;
            var accountAdminId = accountDetails.AccountAdminId;

            var connectUserToRentCommand = new ConnectNewUserToRentCommand(TestContext, accountDetails);
            connectUserToRentCommand.Run();

            var secondUser = await DbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(u =>  u.AccountId == accountDetails.AccountId && u.Id != accountAdminId);
            #endregion createAccount

            #region getAccess
            var result = _accessToAcDbOnDelimitersManager.GrandInternalAccessToDb(databaseId, secondUser.Id);

            if (!result.Success || !string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе  для пользователя завершился с ошибкой {result.Message}!!!");

            var dbAccess = await DbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(a => a.AccountDatabaseID == databaseId && a.AccountUserID == secondUser.Id);

            Assert.IsNotNull(dbAccess, "Доступ к базе  для пользователя  не предоставлен!!!");
            #endregion getAccess

            #region deleteAccess
            var user = new AccountUserIdModel { AccountUserID = secondUser.Id };

            var deleteResult = _accountUsersProfileManager.Delete(user);
            Assert.IsFalse(deleteResult.Error, "Ошибка при удалении пользователя");

            var existsAccesses = await DbLayer.AcDbAccessesRepository
                .AsQueryableNoTracking()
                .AnyAsync(a => a.AccountDatabaseID == databaseId && a.AccountUserID == secondUser.Id);

            Assert.IsFalse(existsAccesses, "Доступов для удалённого пользователя быть не должно.");
            #endregion deleteAccess
        }
    }
}
