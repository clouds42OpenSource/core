﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Смена типа информационной базы с 
    /// серверной на файловую и обратно 
    /// </summary>
    public class ChangeAccountDatabaseTypeTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var accountDatabaseEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            //проверяем что база изначально файловая(true)
            Assert.AreEqual(true, database.IsFile);

            //меняем тип информационной базы с файловой на серверную
            var dbManagerResult = accountDatabaseEditManager.ChangeAccountDatabaseType(new ChangeAccountDatabaseTypeDto { DatabaseId =  createAccAndDb.AccountDatabaseId });

            //проверяем что метод отработал без ошибок
            Assert.IsNotNull(dbManagerResult);
            if (dbManagerResult.Error)
                throw new InvalidOperationException($"Метод изменения типа инфорцационной базы вернул ошибку: {dbManagerResult.Message}");

            var changeDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            //проверяем что тип ИБ изменился на серверную(false)
            Assert.AreEqual(false, changeDatabase.IsFile);

            //меняем тип информационной базы с серверной на файловую
            dbManagerResult = accountDatabaseEditManager.ChangeAccountDatabaseType(new ChangeAccountDatabaseTypeDto { DatabaseId = createAccAndDb.AccountDatabaseId });

            //проверяем что метод отработал без ошибок
            Assert.IsNotNull(dbManagerResult);
            if (dbManagerResult.Error)
                throw new InvalidOperationException($"Метод изменения типа инфорцационной базы вернул ошибку: {dbManagerResult.Message}");

            changeDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            //проверяем что тип ИБ изменился и стал снова файловым(true)
            Assert.AreEqual(true, changeDatabase.IsFile);

        }
    }
}
