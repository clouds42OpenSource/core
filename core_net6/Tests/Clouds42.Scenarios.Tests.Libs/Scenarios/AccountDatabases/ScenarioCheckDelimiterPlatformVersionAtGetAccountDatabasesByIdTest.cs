﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки поля "Версия платформы на разделителях"
    /// при получении данных по инф. базе
    ///
    /// 1) Создаем Аккаунт и базу на разделителях
    /// 2) Получаем информацию по инф. базе и проверяем поле "Версия платформы на разделителях".
    /// Версия должна совпадать с версией из сегмента материнской базы
    /// </summary>
    public class ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();

            var accountDatabaseDataManager = ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();

            var managerResult = accountDatabaseDataManager
                .GetAccountDatabasesByIdAsync(createAccAndDb.AccountDatabaseId).Result;

            Assert.IsFalse(managerResult.Error, managerResult.Message);
            var segment = GetAccountSegmentForDatabaseOnDelimeter(createAccAndDb.AccountDatabaseId);

            if (managerResult.Result.DelimiterInfo.PlatformVersion != segment.Stable83Version)
                throw new InvalidOperationException("База на разделителях должна использовать веб сервис");
        }
    }
}
