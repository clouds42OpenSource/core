﻿using System.IO;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Publishes.Providers;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    ///     Смена дистрибутива для опубликованной базы 
    /// при условии что указана альфа версия в сегменте
    /// </summary>
    public class ChangeDistributionTypeForPublishedDbTest : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;

        public ChangeDistributionTypeForPublishedDbTest()
        {
            
            _cloudSegmentReferenceManager = ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
            _accountDatabaseCardManager = ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var createPlatform = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatform.Run();

            UpdateCloudContentServerNodes();
            var accDbEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var database = DbLayer.DatabasesRepository.GetAccountDatabase(createAccAndDb.AccountDatabaseId);

            try
            {
                publishManager.PublishDatabase(
                    new PublishDto { AccountDatabaseId = createAccAndDb.AccountDatabaseId });

                var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createAccAndDb.AccountDetails.Segment.Id });

                Assert.IsNotNull(segmentManagerResult);
                Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

                var segmentViewModel = segmentManagerResult.Result;

                var dbManagerResult = _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name,
                    createAccAndDb.AccountDetails.AccountId).Result;

                var existedDatabase = await
                    DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(accDb => accDb.Id == createAccAndDb.AccountDatabaseId);

                Assert.IsNotNull(existedDatabase);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);

                Assert.IsNotNull(dbManagerResult);
                segmentViewModel.BaseData.Alpha83VersionId = createPlatform.Version;

                var managerResult = _cloudSegmentReferenceManager.EditSegment(segmentViewModel.MapToEditCloudServicesSegmentDto());

                Assert.IsNotNull(managerResult);
                Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании сегмента. Текст ошибки: {managerResult.Message}");

                dbManagerResult = _accountDatabaseCardManager.GetAccountDatabaseInfo(database.V82Name,
                    createAccAndDb.AccountDetails.AccountId).Result;

                Assert.IsNotNull(dbManagerResult);
                Assert.IsNotNull(dbManagerResult.Database.Alpha83Version);

                var platformResult = accDbEditManager.ChangeAccountDatabasePlatformType(createAccAndDb.AccountDatabaseId,
                    PlatformType.V83,
                    DistributionType.Alpha);

                Assert.IsNotNull(platformResult);
                Assert.IsFalse(platformResult.Error,
                    $"При смене дистрибутива у базы произошла ошибка. Текст ошибки: {platformResult.Message}");

                var existedSegment = await
                        DbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().FirstOrDefaultAsync(s => s.ID == segmentViewModel.Id);
                existedDatabase = await
                    DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(acDb => acDb.Id == createAccAndDb.AccountDatabaseId);

                Assert.IsNotNull(existedSegment);
                Assert.IsNotNull(existedDatabase);
                Assert.IsNotNull(existedSegment.Alpha83PlatformVersionReference);
                Assert.AreEqual(segmentViewModel.BaseData.Alpha83VersionId, existedSegment.Alpha83PlatformVersionReference.Version);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(DistributionType.Alpha, existedDatabase.DistributionTypeEnum);

                var partOfPath =
                    $@"{createAccAndDb.AccountDetails.AccountId.GetEncodeGuid()}\{existedDatabase.V82Name}";

                var pathToPublish = GetPublishServerHelper.PathToPublishServer();

                var fullPathToFolder = Path.Combine(pathToPublish, partOfPath).Remove(0, 2);

                var scriptProcessorValue = TestWebConfigHelper.Get1CScriptProcessorValue(fullPathToFolder);
                var expectedScriptProcessorValue = string.Format(CloudConfigurationProvider.Segment.V83ModulePath(),
                    existedSegment.Alpha83PlatformVersionReference.Version);

                Assert.IsFalse(string.IsNullOrWhiteSpace(scriptProcessorValue));
                Assert.AreEqual(expectedScriptProcessorValue, scriptProcessorValue);


                platformResult = accDbEditManager.ChangeAccountDatabasePlatformType(createAccAndDb.AccountDatabaseId,
                    PlatformType.V83);

                Assert.IsNotNull(platformResult);
                Assert.IsFalse(platformResult.Error,
                    $"При смене дистрибутива у базы произошла ошибка. Текст ошибки: {platformResult.Message}");

                existedSegment = await
                    DbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.ID == segmentViewModel.Id);

                existedDatabase = await
                    DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

                Assert.IsNotNull(existedDatabase);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);

                partOfPath =
                    $@"{createAccAndDb.AccountDetails.AccountId.GetEncodeGuid()}\{existedDatabase.V82Name}";

                pathToPublish = GetPublishServerHelper.PathToPublishServer();

                fullPathToFolder = Path.Combine(pathToPublish, partOfPath).Remove(0, 2);

                scriptProcessorValue = TestWebConfigHelper.Get1CScriptProcessorValue(fullPathToFolder);
                expectedScriptProcessorValue = string.Format(CloudConfigurationProvider.Segment.V83ModulePath(),
                    existedSegment.Stable83PlatformVersionReference.Version);

                Assert.IsFalse(string.IsNullOrWhiteSpace(scriptProcessorValue));
                Assert.AreEqual(expectedScriptProcessorValue, scriptProcessorValue);
            }
            finally
            {
                publishManager.CancelPublishDatabaseWithWaiting(createAccAndDb.AccountDatabaseId);
            }
        }

        /// <summary>
        /// Обновить адреса нод публикаций
        /// </summary>
        private void UpdateCloudContentServerNodes()
        {
            var contentServerNodes = DbLayer.PublishNodeReferenceRepository.All();

            foreach (var serverNode in contentServerNodes)
            {
                serverNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
                DbLayer.PublishNodeReferenceRepository.Update(serverNode);
            }

            DbLayer.Save();
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDatabaseWebPublisher, DatabaseWebPublisher>();
        }
    }
}
