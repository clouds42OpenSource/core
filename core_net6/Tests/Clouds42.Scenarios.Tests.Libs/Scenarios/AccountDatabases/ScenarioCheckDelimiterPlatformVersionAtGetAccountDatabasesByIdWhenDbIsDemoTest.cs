﻿using System;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки поля "Версия платформы на разделителях"
    /// при получении данных по демо базе на разделителях
    ///
    /// 1) Создаем аккаунт и базу на разделителях демо
    /// 2) Создаем новый сегмент и служебный аккаунт для
    /// демо баз на разделителях
    /// 3) Проверяем что версия платформы для базы определеяется
    /// по сегменту служебного аккаунта для демо баз на разделителях
    /// </summary>
    public class ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdWhenDbIsDemoTest : ScenarioBase
    {
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioCheckDelimiterPlatformVersionAtGetAccountDatabasesByIdWhenDbIsDemoTest()
        {
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var accountDatabaseDataManager = ServiceProvider.GetRequiredService<IAccountDatabaseDataManager>();

            var databasePlatformVersionHelper = ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var accountDb = DbLayer.DatabasesRepository.FirstOrDefault(db =>
                db.Id == createAccountDatabaseAndAccountCommand.AccountDatabaseId);

            var segment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault(s => s.ID == createSegmentCommand.Id) ??
                          throw new NotFoundException($"Не удалось найти сегмент по ID {createSegmentCommand.Id}");

            var serviceAccountForDemoDelimiterDatabases =
                _createAccountHelperTest.Create();

            accountDb.AccountDatabaseOnDelimiter.IsDemo = true;
            DbLayer.AccountDatabaseDelimitersRepository.Update(accountDb.AccountDatabaseOnDelimiter);
            DbLayer.Save();

            UpdateServiceAccountForDemoDelimiterDatabases(serviceAccountForDemoDelimiterDatabases.Id);

            var managerResult = accountDatabaseDataManager
                .GetAccountDatabasesByIdAsync(accountDb.Id).Result;

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb, serviceAccountForDemoDelimiterDatabases.Id);

            Assert.AreEqual(segment.Stable83Version, receivedVersionPlatform.Version);
        }


        /// <summary>
        /// Обновить служебный аккаунт для
        /// демо баз на разделителях
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public void UpdateServiceAccountForDemoDelimiterDatabases(Guid accountId)
        {
            var cloudConfig = DbLayer.CloudConfigurationRepository.FirstOrDefault(cc =>
                                  cc.Key == "DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases") ??
                              throw new NotFoundException("Не удалось получить конфигурацию облака");

            cloudConfig.Value = accountId.ToString();
            DbLayer.CloudConfigurationRepository.Update(cloudConfig);
            DbLayer.Save();
        }
    }
}
