﻿using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabases
{
    /// <summary>
    /// Сценарий проверки смены модели восстановления для инф. базы
    ///     Действия:
    ///         1) Создаем аккаунт и инф. базу
    ///         2) Меняем тип модели восстановления (указываем "Полная")
    ///         3) Проверяем что установилась простая модель восстановления, так как SQL сервер не позволяет устанавливать другие модели
    /// </summary>
    public class ScenarioChangeAccountDatabaseRestoreModelType : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта и инф.базы
            
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            #endregion

            #region Смена типа инф. базы на серверную

            var accountDatabaseEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
            var accountDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            Assert.AreEqual(true, accountDatabase.IsFile);

            var result = accountDatabaseEditManager.ChangeAccountDatabaseType(new ChangeAccountDatabaseTypeDto 
            { DatabaseId = createAccAndDb.AccountDatabaseId, RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full});

            #endregion

            #region Проверка результата работы метода
            DbLayer.DatabasesRepository.Reload(accountDatabase);

            Assert.IsNotNull(result);
            Assert.IsTrue(accountDatabase.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Full);

            #endregion
        }
    }
}
