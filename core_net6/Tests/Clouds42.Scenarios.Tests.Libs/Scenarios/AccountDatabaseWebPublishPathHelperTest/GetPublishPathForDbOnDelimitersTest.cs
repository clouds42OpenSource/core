﻿using System;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseWebPublishPathHelperTest
{
    /// <summary>
    /// Сценарий теста проверки получения пути публикации
    /// для базы на разделителях
    ///
    /// 1) Создаем аккаунт и базу на разделителях
    /// 2) Получаем адрес публикации и проверям на соответствие
    /// </summary>
    public class GetPublishPathForDbOnDelimitersTest : ScenarioBase
    {
        public override void Run()
        {
            var accountDatabaseWebPublishPathHelper =
                TestContext.ServiceProvider.GetRequiredService<AccountDatabaseWebPublishPathHelper>();

            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();

            var createDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<CreateDelimiterSourceAccountDatabaseManager>();

            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == createAccAndDb.AccountDatabaseId) ??
                throw new NotFoundException("Не удалось найти базу");

            database.AccountDatabaseOnDelimiter.SourceAccountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(database.AccountDatabaseOnDelimiter.SourceAccountDatabase);
            DbLayer.Save();

            var delimiterSourceAccountDatabaseDto = new DelimiterSourceAccountDatabaseDto
            {
                AccountDatabaseId = database.AccountDatabaseOnDelimiter.SourceAccountDatabase.Id,
                DbTemplateDelimiterCode = database.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode,
                DatabaseOnDelimitersPublicationAddress = "i44"
            };

            var managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var webPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(database);

           if (!webPublishPath.Contains(delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress))
               throw new InvalidOperationException("Не верно получен адрес публикации");

            database.AccountDatabaseOnDelimiter.IsDemo = true;
            DbLayer.AccountDatabaseDelimitersRepository.Update(database.AccountDatabaseOnDelimiter);
            DbLayer.Save();

            webPublishPath = accountDatabaseWebPublishPathHelper.GetWebPublishPath(database);

            if (webPublishPath != database.AccountDatabaseOnDelimiter.DbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress)
                throw new InvalidOperationException("Не верно получен адрес публикации");
        }
    }
}
