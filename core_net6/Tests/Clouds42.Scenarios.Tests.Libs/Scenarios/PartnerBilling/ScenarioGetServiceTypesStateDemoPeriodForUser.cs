﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: 
    ///     1) у аккаунта активен демо период и метод возвращает true. 
    ///     2) у аккаунта не активнен демо период и метод возвращает false
    /// </summary>
    public class ScenarioGetServiceTypesStateDemoPeriodForUser : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;

        public ScenarioGetServiceTypesStateDemoPeriodForUser()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var account = DbLayer.AccountsRepository.GetAccount(TestContext.AccessProvider.ContextAccountId);

            var serviceRent1C = GetRent1CServiceOrThrowException();
            _resourceConfigurationHelper.InsertServiceDemoResourceConfiguration(account.Id, service.Id);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, serviceRent1C.Id, false);

            var resConfig =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == service.Id && rc.AccountId == account.Id);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Ошибка. У созданного аккаунта '{account.IndexNumber}' не подключен сервис.");

            var partnerClientDataProvider = ServiceProvider.GetRequiredService<IPartnersClientDataProvider>();

            var partnerClientsList = partnerClientDataProvider.GetPartnerClients(new PartnersClientFilterDto{AccountId = account.Id,  PageNumber= 1 });
            var serviceTypeStatus = partnerClientsList.ChunkDataOfPagination
                .FirstOrDefault(a => a.AccountIndexNumber == account.IndexNumber)?.ServiceIsActiveForClient;
            Assert.IsTrue(serviceTypeStatus.HasValue && serviceTypeStatus.Value);

            var resourcesConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(w => w.BillingServiceId == service.Id);

            resourcesConfiguration.Frozen = true;
            resourcesConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();
            
            partnerClientDataProvider = ServiceProvider.GetRequiredService<IPartnersClientDataProvider>();
            partnerClientsList = partnerClientDataProvider.GetPartnerClients(new PartnersClientFilterDto { AccountId = account.Id, PageNumber = 1 });
            serviceTypeStatus = partnerClientsList.ChunkDataOfPagination
                .FirstOrDefault(a => a.AccountIndexNumber == account.IndexNumber)?.ServiceIsActiveForClient;
            Assert.IsFalse(serviceTypeStatus.HasValue && serviceTypeStatus.Value);
        }
    }
}
