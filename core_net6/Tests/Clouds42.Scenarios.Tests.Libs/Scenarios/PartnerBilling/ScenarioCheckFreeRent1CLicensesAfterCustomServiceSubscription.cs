﻿using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Цель: проверяем, что после подключения кастомного сервиса в случае если
    /// у клиента Аренда 1С просрочена и заблокирована мы активируем ему аренду на 7 дней
    /// и выдаём 20 лицензий.
    /// Сценарий : создаём аккаунт с подключённой арендой. Блокируем аренду. Создаём кастомный
    /// сервис с услугой. Подключаем аккаунту новый сервис. Смотрим состояние аренды и ресурсов.
    /// </summary>
    public class ScenarioCheckFreeRent1CLicensesAfterCustomServiceSubscription : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public ScenarioCheckFreeRent1CLicensesAfterCustomServiceSubscription()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;
            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            #region serviceCreation

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            #endregion serviceCreation

            var rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAccountCommand.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent1C.ExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.Save();

            Services.AddTransient<IProlongServicesProvider, BillingServicesProvider>();

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processResServ = billingManager.ProlongOrLockExpiredServices();

            Assert.IsFalse(processResServ.Error);

            rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAccountCommand.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsTrue(rent1C.FrozenValue);

            var webResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb).Count();

            var rdpResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser).Count();

            var usersCount = DbLayer.AccountUsersRepository.Where(u => u.AccountId == createAccountCommand.AccountId).Count();

            Assert.AreEqual(webResources, usersCount,"Неверное количество ресурсов");
            Assert.AreEqual(rdpResources, usersCount, "Неверное количество ресурсов");

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id,accountAdminId);

            rent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAccountCommand.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsFalse(rent1C.FrozenValue);

            Assert.IsNotNull(rent1C.ExpireDate);
            if ((rent1C.ExpireDate.Value.Date - DateTime.Now.Date).Days != Clouds42SystemConstants.MyEnterpriseDemoPeriodDays)
                throw new InvalidOperationException("Неверная дата окончания аренды.");

            webResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb).Count();

            rdpResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser).Count();

            Assert.AreEqual(webResources, CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod(), "Неверное количество лицензий");
            Assert.AreEqual(rdpResources, CloudConfigurationProvider.ConfigRental1C.FreeResourceCountAtDemoPeriod(), "Неверное количество лицензий");
        }
    }
}
