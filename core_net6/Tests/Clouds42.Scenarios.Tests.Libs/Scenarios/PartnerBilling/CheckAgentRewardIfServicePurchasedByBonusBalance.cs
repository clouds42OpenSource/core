﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.BillingServices.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверка агентского вознаграждения при оплате кастомного сервиса бонусами
    /// Сценарий:
    ///         1) Создаем аккаунт с сервисом
    ///         2) Создаем аккаунт реферал первого
    ///         3) Меняем дату окончания Аренды и кастомного сервиса на прошедшую дату для реферала
    ///         4) Создаем бонусный платеж на сумму равную стоимости Аренды и стоимости кастомного сервиса
    ///         5) Проверяем, что агенту начислилось вознаграждение только за Аренду
    ///         6) Проверяем, что у реферала разблокированы Аренда и кастомный сервис
    /// </summary>
    public class CheckAgentRewardIfServicePurchasedByBonusBalance : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public CheckAgentRewardIfServicePurchasedByBonusBalance()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
            _activateServiceForRegistrationAccountProvider =
                TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = [serviceTypeDto]
                });
            createCustomService.Run();

            var referrerAccount =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createCustomService.Id).AccountOwner;

            var createReferralAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = referrerAccount.Id
                }
            });
            createReferralAccountCommand.Run();

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(createCustomService.Id,
                createReferralAccountCommand.AccountAdminId);

            var defaultDaysCount = -1;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createReferralAccountCommand.AccountId,
                new Rent1CServiceManagerDto
                {
                    ExpireDate = DateTime.Now.AddDays(defaultDaysCount)
                });

            var localeId = GetAccountLocaleId(referrerAccount.Id);
            var rent1CServiceType = DbLayer.RateRepository.FirstOrDefault(rate =>
                rate.BillingServiceTypeId == serviceTypeDto.DependServiceTypeId &&
                rate.LocaleId == localeId);
            var rent1CAndCustomServiceCost = serviceTypeDto.ServiceTypeCost + rent1CServiceType.Cost;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createReferralAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = rent1CAndCustomServiceCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Bonus
            });

            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == createCustomService.Id);
            resourceConfiguration.ExpireDate = DateTime.Now.AddDays(defaultDaysCount);
            resourceConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            DbLayer.Save();
            RefreshDbCashContext(Context.ResourcesConfigurations);

            var actualAgencyAgreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();
            var agentRewardForCustomService = GetAgentRewardForCustomService(createCustomService.Id,
                createReferralAccountCommand.AccountId, actualAgencyAgreement.ServiceOwnerRewardPercent);

            _billingServiceManager.ApplyDemoServiceSubscribe(createCustomService.Id,
                createReferralAccountCommand.AccountId, false);

            RefreshDbCashContext(Context.AgentWallets);
            var agentWallet =
                DbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == referrerAccount.Id);

            var customServiceResourceConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingServiceId == createCustomService.Id &&
                rc.AccountId == createReferralAccountCommand.AccountId);

            Assert.AreEqual(agentRewardForCustomService, agentWallet.AvailableSum,
                $"Сумма агентского вознаграждения должна быть {agentRewardForCustomService}");
            Assert.IsFalse(customServiceResourceConfiguration.FrozenValue, "");
        }

        /// <summary>
        /// Получить сумму агентского вознаграждения для кастомного сервиса
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceOwnerRewardPercent">Вознаграждение за сервис разработчика</param>
        /// <returns>Сумма агентского вознаграждения для кастомного сервиса</returns>
        private decimal GetAgentRewardForCustomService(Guid billingServiceId, Guid accountId,
            decimal serviceOwnerRewardPercent)
        {
            var billingServiceInfo = TestContext.ServiceProvider.GetRequiredService<BillingServiceInfoProvider>()
                .GetBillingService(billingServiceId, accountId);

            var serviceAmountData = billingServiceInfo.AmountData;
            var serviceCost = serviceAmountData.ServicePartialAmount ?? serviceAmountData.ServiceAmount;

            return (serviceOwnerRewardPercent / 100) * serviceCost;
        }
    }
}