﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверка типа продления кастомного сервиса
    /// Действия :
    /// 1) Создаём аккаунт с подключённой арендой
    /// 2) Создаём кастомный сервис
    /// 3) Подключаем его.
    /// Проверка:
    /// 1) Когда дэмо заканчивается раньше аренда, то ServicePartialAmount >0
    /// 2) Если совпадают ServicePartialAmount=0
    /// </summary>
    public class ScenarionCheckCustomServicePayOrSubscribe : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        public ScenarionCheckCustomServicePayOrSubscribe()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider =
                TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }
        public override void Run()
        {
            #region startsettings

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var standartResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var billingServiceTypes =
                _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standartResourceTypeId);
            billingServiceTypes[0].BillingType = BillingTypeEnum.ForAccountUser;

            var service = CreateCustomService(billingServiceTypes);
            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id,
                createAccountCommand.AccountAdminId);

            #endregion startsettings

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();

            var result = billingServiceManager.GetBillingService(service.Id, createAccountCommand.AccountId);
            Assert.IsTrue(result.Result.AmountData.ServicePartialAmount == 0, "Тут должно быть подтверждение подписки");

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.BillingServiceId == billingService.Id && x.AccountId == createAccountCommand.AccountId)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, DateTime.Now.AddDays(-7).AddMonths(1)));

           Context.ChangeTracker.Clear();

            result = billingServiceManager.GetBillingService(service.Id, createAccountCommand.AccountId);
            Assert.IsTrue(result.Result.AmountData.ServicePartialAmount > 0, "Тут должна быть оплата");

        }

        /// <summary>
        /// Создать кастомный сервис
        /// </summary>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService CreateCustomService(List<BillingServiceTypeDto> billingServiceTypes)
        {
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            return service;
        }


    }
}
