﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Цель: убедиться, что кастомный сервис в с статусе "Дэмо"
    /// не оплачивается и не продливается при продлении службы "Аренда 1С"
    /// Сценарий: создаём аккаунт с подключённой арендой. Для этого аккаунта
    /// создаём и подключаем сервис. Делаем аренду просроченной, кладём деньги на счёт,
    /// проверяем статусы сервисов.
    /// </summary>
    public class ScenarioCheckServiceStatusAfterRentProlongation: ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public ScenarioCheckServiceStatusAfterRentProlongation()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            var accountAdminId = createAccountCommand.AccountAdminId;

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            #region serviceCreation

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id, accountAdminId);

            RefreshDbCashContext(Context.ResourcesConfigurations);

            var custResource =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            Assert.AreEqual(true,custResource.IsDemoPeriod,"при подключении сервис должен быть дэмо");

            #endregion serviceCreation

            #region rent

            var rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAccountCommand.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent1C.ExpireDate = DateTime.Now.AddDays(-1);
            custResource.ExpireDate = DateTime.Now.AddDays(-1);

            var startedExpDateService = custResource.ExpireDate;
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.Save();
            DbLayer.ResourceConfigurationRepository.Update(custResource);
            DbLayer.Save();

            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processResServ = billingManager.ProlongOrLockExpiredServices();
            Assert.IsFalse(processResServ.Error);

            RefreshDbCashContext(Context.ResourcesConfigurations);

            rent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsTrue(rent1C.FrozenValue);

            var webResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId &&
                r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb).Count();

            var rdpResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createAccountCommand.AccountId && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser).Count();

            var usersCount = DbLayer.AccountUsersRepository.Where(u => u.AccountId == createAccountCommand.AccountId).Count();

            Assert.AreEqual(webResources, usersCount, "Неверное количество ресурсов");
            Assert.AreEqual(rdpResources, usersCount, "Неверное количество ресурсов");

            #endregion rent

            RefreshDbCashContext(Context.ResourcesConfigurations);
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 100000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            RefreshDbCashContext(Context.ResourcesConfigurations);
            RefreshDbCashContext(Context.Payments);

            processResServ = billingManager.ProlongOrLockExpiredServices();
            Assert.IsFalse(processResServ.Error);

            var outflowString = PaymentType.Outflow.ToString();
            var payments = DbLayer.PaymentRepository.Where(p =>
                p.AccountId == createAccountCommand.AccountId && p.OperationType == outflowString && p.Sum >0).ToList();
            Assert.AreEqual(1,payments.Count,"Должно быть лишь одна оплата");

            var myEnterpriseId = DbLayer.BillingServiceRepository
                .FirstOrDefault(s => s.SystemService == Clouds42Service.MyEnterprise).Id;
            Assert.AreEqual(payments[0].BillingServiceId, myEnterpriseId,"Оплачен должен быть с. аренды");

            custResource =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            Assert.IsTrue(custResource.IsDemoPeriod,"Сервис должен остаться демо");
            Assert.IsNotNull(custResource.ExpireDate);
            Assert.AreEqual(custResource.ExpireDate.Value.Date,startedExpDateService.Value.Date,"Дата окончания не должна измениться");
        }
    }
}
