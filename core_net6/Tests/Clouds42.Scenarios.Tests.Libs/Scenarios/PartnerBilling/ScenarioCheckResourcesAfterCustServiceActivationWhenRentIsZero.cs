﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Цель:   проверить корректность активации лицензий по пользователям
    /// при подключении пользовательского сервиса
    /// Сценарий:   создаём аккаунт , у пользователя есть лицензия
    /// "Веб". Создаём сервис с услугой, требующей "Стандарт".
    /// Подключаем его и проверяем чтобы все лицензии остались свободными.
    /// </summary>
    public class ScenarioCheckResourcesAfterCustServiceActivationWhenRentIsZero : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public ScenarioCheckResourcesAfterCustServiceActivationWhenRentIsZero()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            CreateDelimiterTemplate();

            var standartResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var webResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUserWeb).Id;
            
            var unnecessaryStandartResources = DbLayer.ResourceRepository
                .Where(r => r.AccountId == createAccountCommand.AccountId && r.BillingServiceTypeId==standartResourceTypeId);
            var unnecessaryWebResources = DbLayer.ResourceRepository
                .Where(r => r.AccountId == createAccountCommand.AccountId && r.BillingServiceTypeId == webResourceTypeId && r.Subject == null);

            DbLayer.ResourceRepository.DeleteRange(unnecessaryStandartResources);
            DbLayer.ResourceRepository.DeleteRange(unnecessaryWebResources);
            DbLayer.Save();
            
            var rentService =
                DbLayer.BillingServiceRepository.FirstOrDefault(r => r.SystemService == Clouds42Service.MyEnterprise);
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = rentService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 1,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var adminId = createAccountCommand.AccountAdminId;

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            #region serviceCreation

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standartResourceTypeId);
            billingServiceTypes[0].BillingType = BillingTypeEnum.ForAccountUser;
            
            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            #endregion serviceCreation
            
            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id, adminId);
            
            var custServiceResConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            
            Assert.AreEqual(true,custServiceResConf.IsDemoPeriod,"Новый сервис должен быть дэмо");

            if ((custServiceResConf.ExpireDate.Value.Date - DateTime.Now.Date).Days != Clouds42SystemConstants.MyEnterpriseDemoPeriodDays)
            {
                throw new InvalidOperationException ("Неверная дата окончания дэмо сервиса.");
            }

            var custServiceTypeId = billingServiceTypes[0].Id;
            var custResources = DbLayer.ResourceRepository.Where(r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingServiceTypeId == custServiceTypeId)
                .ToList();

            foreach(var custResource in custResources)
            {
                if (custResource.Subject!=null)
                {
                    throw new InvalidOperationException ("Все ресурсы кастомного сервиса в этом тесте должны быть свободными");
                }
            }
        }
    }
}
