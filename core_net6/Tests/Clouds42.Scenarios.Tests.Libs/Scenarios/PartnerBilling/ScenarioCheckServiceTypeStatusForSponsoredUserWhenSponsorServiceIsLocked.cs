﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса услуг сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: аккаунт спосируется другим аккаунтом, у которого сервис заблокирован
    /// </summary>
    public class ScenarioCheckServiceTypeStatusForSponsoredUserWhenSponsorServiceIsLocked : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioCheckServiceTypeStatusForSponsoredUserWhenSponsorServiceIsLocked()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var serviceRent1C = GetRent1CServiceOrThrowException();
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto()
            };
            var createSecondAccountCommand = new CreateAccountCommand(
                TestContext,
                secondAccount);
            createSecondAccountCommand.Run();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, service.Id, true);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, serviceRent1C.Id, true);
            _resourceConfigurationHelper.InsertServiceTypeResourcesWithSponsorship(createSecondAccountCommand.Account.Id, createSecondAccountCommand.Account.Id,
                billingServiceTypes, 1, createAccountCommand.AccountId);

            var accountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createSecondAccountCommand.Account.Id).Login;

            foreach (var billingServiceType in billingServiceTypes)
            {
                var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

                if (billingServiceType.Key.IsNullOrEmpty())
                    continue;

                var serviceTypeStatus = partnerBillingServiceDataManager.CheckServiceTypeStatusForUser(billingServiceType.Key,
                    accountUserLogin);

                Assert.IsFalse(serviceTypeStatus.Result.Result);
            }
        }
    }
}
