﻿using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Тест привязки аккаунта к партнёру
    /// Сценарий:
    /// 1) Создаём партнёра и его сервис
    /// 2) Выдаём партнёру права клаудадмина
    /// 3) Создаём клиента на РУ локали
    /// 4) Привязываем клиента к партнёру
    /// 5) Проверки
    /// </summary>
    public class ScenarioSuccessBindingAccountToPartnerTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public ScenarioSuccessBindingAccountToPartnerTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }
        public override void Run()
        {
            var createPartnerCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createPartnerCommand.Run();
            var createClientCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createClientCommand.Run();

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createPartnerCommand.AccountId);
            var accountAdminId = createPartnerCommand.AccountAdminId;

            var partnerRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == accountAdminId);
            partnerRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(partnerRole);
            DbLayer.Save();

            var client = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createClientCommand.AccountId);

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            #region serviceCreation

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            Assert.IsNotNull(service, "Произошла ошибка в процессе создания. Сервис не найден.");

            #endregion serviceCreation

            var partnerClientsManager = TestContext.ServiceProvider.GetRequiredService<PartnerClientsManager>();
            var accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = account.Id,
                ClientAccountId = client.Id
            };

            var result = partnerClientsManager.BindAccountToPartner(accountsBinding, account.Id);
            Assert.IsFalse(result.Error,"Произошла ошибка привязки");

            RefreshDbCashContext(TestContext.Context.Accounts);
            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            client = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createClientCommand.AccountId);
            Assert.AreEqual(client.ReferralAccountID,account.Id,"Клиенту не выставился реферал.");

        }
    }
}
