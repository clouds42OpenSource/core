﻿using System;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса услуг сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: у аккаунта заблокирован сервис. Метод вернет исключение что сервис заблокирован
    /// </summary>
    public class ScenarioCheckingServiceTypeStatusForUserWhenServiceIsFrozen : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;

        public ScenarioCheckingServiceTypeStatusForUserWhenServiceIsFrozen()
        {
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }
        
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            //Формируем связи услуг
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
                BillingServiceStatus = BillingServiceStatusEnum.IsActive
            };

            // Создание сервиса c указанием связей услуг
            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var account = DbLayer.AccountsRepository.GetAccount(TestContext.AccessProvider.ContextAccountId);
            var serviceRent1C = GetRent1CServiceOrThrowException();
            //блокируем сервис для созданного аккаунта
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id,service.Id, true);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, serviceRent1C.Id, true);

            var resConfig =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == service.Id && rc.AccountId == account.Id);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Ошибка. У созданного аккаунта '{account.IndexNumber}' не подключен сервис.");

            //активируем услуги сервиса для аккаунта
            _resourceConfigurationHelper.InsertServiceTypeResources(account.Id, account.Id,  billingServiceTypes, 1);

            var accountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == account.Id).Login;
            
            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
            foreach (var billingServiceType in billingServiceTypes)
            {
                if (billingServiceType.Key.IsNullOrEmpty())
                    continue;
                

                var serviceTypeStatus = partnerBillingServiceDataManager.CheckServiceTypeStatusForUser(billingServiceType.Key,
                    accountUserLogin);

                Assert.IsFalse(serviceTypeStatus.Result.Result);
            }
        }
    }
}