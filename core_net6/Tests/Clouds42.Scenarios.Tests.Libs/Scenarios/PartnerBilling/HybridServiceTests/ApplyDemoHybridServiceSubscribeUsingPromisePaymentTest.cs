﻿using System;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling.HybridServiceTests
{
    public class ApplyDemoHybridServiceSubscribeUsingPromisePaymentTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ReactivateServiceManager _reactivateServiceManager;
        private readonly BillingServiceManager _billingServiceManager;
        public ApplyDemoHybridServiceSubscribeUsingPromisePaymentTest()
        {
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyDisk
                }
            });
            account.Run();

            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = [serviceTypeDto],
                IsHybridService = true
            });
            billingService.Run();


            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            var rent1C =
            DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
            r.AccountId == account.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsNull(rent1C);

            //активируем сервис
            _reactivateServiceManager.ActivateServiceForExistingAccount(billingService.Id, account.AccountAdminId);
            
            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            resourceConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            DbLayer.Save();

            var managerResult = _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, true);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            Assert.IsFalse(resourceConfiguration.IsDemoPeriod, "Демо период должен был завершиться");
            Assert.IsNotNull(resourceConfiguration.ExpireDate, "Дата окончания подписки сервиса должна быть не null");

            var payment = DbLayer.PaymentRepository.FirstOrDefault(p => p.AccountId == account.AccountId);
            Assert.IsNotNull(payment, "Платеж не был создан");
        }
    }
}
