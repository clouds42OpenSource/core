﻿using System.Linq;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling.HybridServiceTests
{
    public class ActivateHybridServiceTest : ScenarioBase
    {

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountAdmin = createAccountCommand.Account.AccountUsers.FirstOrDefault();
            Assert.IsNotNull(accountAdmin, "Не создался админ аккаунта");

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest =
                {
                    AccountId = createAccountCommand.Account.Id,
                    IsHybridService = true,
                }
            };
            createBillingServiceCommand.Run();

            var serviceProvider = Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>().BuildServiceProvider();
            var activateServiceForRegistrationAccountProvider = serviceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccountWithoutServiceInstallation(createBillingServiceCommand.Id, createAccountCommand.AccountAdminId);

            var serviceResConfig = GetResourcesConfigurationOrThrowException(createAccountCommand.Account.Id, createBillingServiceCommand.Id);
            Assert.IsTrue(serviceResConfig.IsDemoPeriod);
        }
    }
}
