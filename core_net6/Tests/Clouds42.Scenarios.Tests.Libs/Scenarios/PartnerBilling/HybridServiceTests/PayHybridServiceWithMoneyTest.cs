﻿using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling.HybridServiceTests
{
    public class PayHybridServiceWithMoneyTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ReactivateServiceManager _reactivateServiceManager;
        private readonly BillingServiceManager _billingServiceManager;
        /// <summary>
        /// Сценарий покупки гибридного сервиса при наличии денег на счету
        ///
        /// Предусловия: для аккаунта test_account подключено гибридный сервис и продлен демопериод 
        /// баланс 1000 руб; дата окончания сервиса сегодня
        ///
        /// Действия: запускаем таску пролонгации
        ///
        /// Проверка: сервис не заблокировался, дата продлилась на 1 месяц
        /// </summary>
        public PayHybridServiceWithMoneyTest()
        {
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            //создаем аккаунта без аренды
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyDisk
                }
            });
            account.Run();

            //создаем сервис гибридный
            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes =
                [
                    serviceTypeDto
                ],
                IsHybridService = true,
            });
            billingService.Run();

            //начисляем баланс
            CreateInflowPayment(account.AccountId, billingService.Id, 800);

            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            var rent1C =
            DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
            r.AccountId == account.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsNull(rent1C);

            //активируем сервис
            _reactivateServiceManager.ActivateServiceForExistingAccount(billingService.Id, account.AccountAdminId);

            //продляем демо период
            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, false);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == account.AccountId && rc.BillingServiceId == service.Id);
            rent1C =
            DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
            r.AccountId == account.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsNull(rent1C);
            Assert.IsNotNull(resourcesConfiguration);
            Assert.IsFalse(resourcesConfiguration.IsDemoPeriod, "У сервиса не должно быть демо периода");
            Assert.IsNotNull(resourcesConfiguration.ExpireDate, "Дата окончания подписки должна быть не null");
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourcesConfiguration.ExpireDateValue.ToShortDateString());

            //изменяем дату сервиса на месяц назад и запускаем таску пролонгации
            resourcesConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            //симулируем таску Cloud42ServicePrologationJob
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processResServ = billingManager.ProlongOrLockExpiredServices();
            var processResProm = billingManager.ProlongOrLockHybridExpiredServices();

            Assert.IsFalse(processResProm.Error, processResProm.Message);
            Assert.IsFalse(processResServ.Error, processResServ.Message);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);


            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == account.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            var resourcesConfigurationHyb = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == account.AccountId && rc.BillingServiceId == service.Id);

            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == account.AccountId);
            Assert.IsNull(resourceRent1C);
            Assert.AreEqual(0, billingAcc.Balance, "Баланс должнен быть 0");
            Assert.IsFalse(resourcesConfigurationHyb.FrozenValue, "Ресурсы гибридного сервиса не должны быть заблокированы");
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourcesConfigurationHyb.ExpireDateValue.ToShortDateString());

        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="cost">Сумма платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        protected void CreateInflowPayment(Guid accountId, Guid? serviceId, decimal cost,
            TransactionType transactionType = TransactionType.Money)
        {
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = serviceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = transactionType
            });
        }
    }
}
