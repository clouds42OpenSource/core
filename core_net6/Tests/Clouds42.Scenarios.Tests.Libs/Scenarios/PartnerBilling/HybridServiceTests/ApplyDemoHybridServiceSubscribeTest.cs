﻿using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling.HybridServiceTests
{
    public class ApplyDemoHybridServiceSubscribeTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ReactivateServiceManager _reactivateServiceManager;
        private readonly BillingServiceManager _billingServiceManager;

        public ApplyDemoHybridServiceSubscribeTest()
        {
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyDisk
                }
            });
            account.Run();

            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes =
                [
                    serviceTypeDto
                ],
                IsHybridService = true,
            });
            billingService.Run();

            //начисляем баланс
            CreateInflowPayment(account.AccountId, billingService.Id, 400);

            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            var rent1C =
            DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
            r.AccountId == account.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsNull(rent1C);

            //активируем сервис
            _reactivateServiceManager.ActivateServiceForExistingAccount(billingService.Id, account.AccountAdminId);

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, false);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == account.AccountId && rc.BillingServiceId == service.Id);

            Assert.IsNotNull(resourcesConfiguration);
            Assert.IsFalse(resourcesConfiguration.IsDemoPeriod, "У сервиса не должно быть демо периода");
            Assert.IsNotNull(resourcesConfiguration.ExpireDate, "Дата окончания подписки должна быть не null");
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="cost">Сумма платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        protected void CreateInflowPayment(Guid accountId, Guid? serviceId, decimal cost,
            TransactionType transactionType = TransactionType.Money)
        {
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = serviceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = transactionType
            });
        }
    }
}
