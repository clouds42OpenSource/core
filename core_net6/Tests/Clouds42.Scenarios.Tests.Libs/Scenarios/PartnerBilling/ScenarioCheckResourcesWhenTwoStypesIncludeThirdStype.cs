﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Тестирование корректности ресурсов когда две услуги содержат в себе третью
    /// Действия:
    /// 1) Создаём аккаунт с арендой
    /// 2) Для этого аккаунта создаём кастомный сервис с тремя услугами.
    ///     При этом первые две включают в себя третью.
    /// 3) Подключаем аккаунту первые две услуги
    /// 4) Проверяем, что ресурс третьей услуги выдался в единственном екземпляре.
    /// </summary>
    public class ScenarioCheckResourcesWhenTwoStypesIncludeThirdStype: ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public ScenarioCheckResourcesWhenTwoStypesIncludeThirdStype()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;
            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            #region serviceCreation
            //Формируем связи услуг
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);

            billingServiceTypes[0].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            RefreshDbCashContext(TestContext.Context.ServiceTypes);
            var littleStypeName = billingServiceTypes[2].Name;
            var littleStypeId = DbLayer.BillingServiceTypeRepository.FirstOrDefault(s=>s.Name== littleStypeName).Id;

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            Assert.IsNotNull(service, "Произошла ошибка в процессе создания. Сервис не найден.");

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id, accountAdminId);

            RefreshDbCashContext(TestContext.Context.Resources);

            var resource = DbLayer.ResourceRepository.Where(r => r.BillingServiceTypeId == littleStypeId);
            Assert.AreEqual(20,resource.Count(), "Всего должно быть 20 лицензий");
            resource = DbLayer.ResourceRepository.Where(r => r.BillingServiceTypeId == littleStypeId && r.Subject!=null);
            Assert.AreEqual(1,resource.Count(), "Для одного пользователя ресурс услуги не должен дублироватся");

            #endregion serviceCreation

        }
    }
}
