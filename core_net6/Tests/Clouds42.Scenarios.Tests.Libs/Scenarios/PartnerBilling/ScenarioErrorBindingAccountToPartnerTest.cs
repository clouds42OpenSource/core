﻿using System;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверка валидатора привязки клиента к аккаунту
    /// Сценарий:
    /// 1) Создаём партнёра и клиента на укр локали
    /// 2) Обоим выдаём права клаудадмина(для проверки круговой зависимости)
    /// 3) Проверка привязки самого к себе
    /// 4) Проверка привязки укр клиента
    /// 5) Меняем локали и проверяем привязку к укр парнёру(на будущее)
    /// 6) Проверяем круговую привязку
    /// </summary>
    public class ScenarioErrorBindingAccountToPartnerTest : ScenarioBase
    {
        private readonly Guid _localeUkrId, _localeRusId;
        public Account Account, Client;
        private readonly IUpdateAccountConfigurationProvider _updateAccountConfigurationProvider;

        public ScenarioErrorBindingAccountToPartnerTest()
        {
            _localeUkrId = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;
            _localeRusId = GetLocaleByNameOrThrowException(LocaleConst.Russia).ID;
            _updateAccountConfigurationProvider = TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>();
        }

        public override void Run()
        {
            var createPartnerCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createPartnerCommand.Run();

            var createClientCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createClientCommand.Run();

            Account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createPartnerCommand.AccountId);
            Client = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createClientCommand.AccountId);
            var accountAdminId = createClientCommand.AccountAdminId;
            var clientAdminId = createClientCommand.AccountAdminId;

            #region roles

            var partnerRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == accountAdminId);
            var clientRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == clientAdminId);
            partnerRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            clientRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(partnerRole);
            DbLayer.AccountUserRoleRepository.Update(clientRole);
            DbLayer.Save();

            #endregion roles

            HimselfBinding();
            UkrClientBinding();
            UkrPartnerBinding();
            CircleBinding();
        }

        private void HimselfBinding()
        {
            var accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = Account.Id,
                ClientAccountId = Account.Id
            };
            var partnerClientsManager = TestContext.ServiceProvider.GetRequiredService<PartnerClientsManager>();
            var result = partnerClientsManager.BindAccountToPartner(accountsBinding, Account.Id);
            Assert.IsTrue(result.Error, "Валидатор пропустил привязку к самому себе");
        }

        private void UkrClientBinding()
        {
            _updateAccountConfigurationProvider.UpdateLocaleId(Client.Id, _localeUkrId);

            var accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = Account.Id,
                ClientAccountId = Client.Id
            };
            RefreshDbCashContext(TestContext.Context.Accounts);
            var partnerClientsManager = TestContext.ServiceProvider.GetRequiredService<PartnerClientsManager>();
            var result = partnerClientsManager.BindAccountToPartner(accountsBinding, Account.Id);
            Assert.IsTrue(result.Error, "Валидатор пропустил привязку украинского клиента");
        }

        private void UkrPartnerBinding()
        {
            _updateAccountConfigurationProvider.UpdateLocaleId(Client.Id, _localeRusId);
            _updateAccountConfigurationProvider.UpdateLocaleId(Account.Id, _localeUkrId);

            var accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = Account.Id,
                ClientAccountId = Client.Id
            };
            RefreshDbCashContext(TestContext.Context.Accounts);
            var partnerClientsManager = TestContext.ServiceProvider.GetRequiredService<PartnerClientsManager>();
            var result = partnerClientsManager.BindAccountToPartner(accountsBinding, Account.Id);
            Assert.IsTrue(result.Error, "Валидатор пропустил привязку к украинскому партнёру");
        }

        private void CircleBinding()
        {
            _updateAccountConfigurationProvider.UpdateLocaleId(Account.Id, _localeRusId);

            var accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = Account.Id,
                ClientAccountId = Client.Id
            };
            RefreshDbCashContext(TestContext.Context.Accounts);
            var partnerClientsManager = TestContext.ServiceProvider.GetRequiredService<PartnerClientsManager>();
            var result = partnerClientsManager.BindAccountToPartner(accountsBinding, Account.Id);
            Assert.IsFalse(result.Error, "Ошибка в привязке с корректными входными данными");

            accountsBinding = new AccountsBindingDto
            {
                PartnerAccountId = Client.Id,
                ClientAccountId = Account.Id
            };
            RefreshDbCashContext(TestContext.Context.Accounts);
            result = partnerClientsManager.BindAccountToPartner(accountsBinding, Client.Id);
            Assert.IsTrue(result.Error, "Валидаор пропустил круговую привязку");
        }
    }
}