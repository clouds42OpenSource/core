﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Подтверждение подписки демо сервиса
    /// Сценарий:
    ///         1) Создаем аккаунт, создаем сервис
    ///         2) Подключаем сервис для аккаунта
    ///         3) Подтверждаем подписку демо сервиса
    ///         4) Проверяем, что подписка подтверждена и сервис больше не демо
    /// </summary>
    public class ApplyDemoServiceSubscribeTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly Rent1CConfigurationAccessManager _configurationAccessManager;

        public ApplyDemoServiceSubscribeTest()
        {
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            account.Run();

            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

           CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = [serviceTypeDto]
            });
            billingService.Run();

            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            _configurationAccessManager.TurnOnRent1C(account.AccountId);

            _resourceConfigurationHelper.InsertServiceDemoResourceConfiguration(account.AccountId, service.Id);
            _resourceConfigurationHelper.InsertResourcesWithResConfigChanging(account.AccountId, account.AccountAdminId, billingService.CreateBillingServiceTest.BillingServiceTypes, service.Id, 0);

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, false);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.AccountId == account.AccountId && rc.BillingServiceId == service.Id);

            Assert.IsNotNull(resourcesConfiguration);
            Assert.IsFalse(resourcesConfiguration.IsDemoPeriod, "У сервиса не должно быть демо периода");
            Assert.IsNull(resourcesConfiguration.ExpireDate, "Дата окончания подписки должна быть null");
        }
    }
}