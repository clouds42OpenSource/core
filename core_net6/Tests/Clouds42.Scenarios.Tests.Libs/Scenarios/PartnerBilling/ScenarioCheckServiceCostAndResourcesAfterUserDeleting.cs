﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.DataContracts.BillingService;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{

    /// <summary>
    /// Проверка пересчёта ресурсов сервиса поcле удаления юзера кастомного сервиса
    /// Сценарий: создаём сервис с услугами, создаём аккаунт с двумя юзерами с подключённым
    ///         сервисом. Подключаем сервис второму юзеру. Удаляем его.
    /// Проверка: должны высвободится услуги , subject = null, общая стоимость кастомного сервиса
    ///         равно стоимости услуг, подключённых первому юзеру. 
    /// </summary>
    public class ScenarioCheckServiceCostAndResourcesAfterUserDeleting : ScenarioBase
    {
         private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
         private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
         private readonly AccountUserActivateProvider _accountUserActivateProvider;

         public ScenarioCheckServiceCostAndResourcesAfterUserDeleting()
         {
             _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
             _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
             _accountUserActivateProvider = TestContext.ServiceProvider.GetRequiredService<AccountUserActivateProvider>();
        }
        public override void Run()
        {
            #region account
            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdminId = createAccountCommand.AccountAdminId;

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = account.Id,
                AccountIdString = SimpleIdHash.GetHashById(account.Id),
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            var secondUserObject = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == secondUser.Login);
            var activeUsersCount = DbLayer.AccountUsersRepository.Where(u => u.AccountId == account.Id && u.Activated).Count();
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, 
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]", 
                ClientIPAddress = AccessProvider.GetUserHostAddress(), 
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();

            #endregion account

            #region customservice

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var billingServiceTypeCost = billingServiceTypes[0].ServiceTypeCost;
            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id, accountAdminId);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var custResource =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            Assert.AreEqual(true,custResource.IsDemoPeriod, "при подключении сервис должен быть дэмо");

            #endregion customservice

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            var rateProvider = ServiceProvider.GetRequiredService<IRateProvider>(); 
            var myEntUser = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUser);
            var myEntUserWeb = DbLayer.BillingServiceTypeRepository.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);
            var usrCost = rateProvider.GetOptimalRate(billingAccount.Id,myEntUser.Id);
            var usrWebCost = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();
            var rentForSecondUser = new CalculateBillingServiceTypeDto
            {
                Subject = res.Result,
                Status = true,
                BillingServiceTypeId = myEntUserWeb.Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                { I = false,Me = false,Label = ""}
            };
            var rentService =
                DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == Clouds42Service.MyEnterprise);
            dataAccountUsers.Add(rentForSecondUser);

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, rentService.Id, false, null, dataAccountUsers);

            var customServiceData = new List<CalculateBillingServiceTypeDto>();
            var customForFirstUser = new CalculateBillingServiceTypeDto
            {
                Subject = accountAdminId,
                Status = true,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    { I = false, Me = false, Label = "" }
            };

            var customForSecondUser = new CalculateBillingServiceTypeDto
            {
                Subject = res.Result,
                Status = true,
                BillingServiceTypeId = billingServiceTypes[0].Id,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    { I = false, Me = false, Label = "" }
            };
            customServiceData.AddRange(new List<CalculateBillingServiceTypeDto>{ customForFirstUser , customForSecondUser });
            billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(billingAccount.Id, service.Id, false, null, customServiceData);
            
            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);
            var rentCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r=>r.BillingService.SystemService == Clouds42Service.MyEnterprise).Cost;
            Assert.AreEqual(rentCost, usrCost.Cost + 2*usrWebCost.Cost,"Неверный расчёт стоимости аренды");
            
            var customServiceCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingService.Id == service.Id).Cost;
            Assert.AreEqual(customServiceCost, activeUsersCount * billingServiceTypeCost, "Неверный расчёт стоимости кастомного сервиса.");
           
            _accountUserActivateProvider.SetActivateStatusForUser(secondUserObject,false);

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            activeUsersCount = DbLayer.AccountUsersRepository.Where(u => u.AccountId == account.Id && u.Activated).Count();
            Assert.AreEqual(1,activeUsersCount,"Не отработала деактивация юзера.");

            customServiceCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingService.Id == service.Id).Cost;
            Assert.AreEqual(customServiceCost, activeUsersCount * billingServiceTypeCost, "Неверный расчёт стоимости кастомного сервиса.");

            rentCost = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingService.SystemService == Clouds42Service.MyEnterprise).Cost;
            Assert.AreEqual(rentCost, usrCost.Cost +  usrWebCost.Cost, "Неверный расчёт стоимости аренды");

            var secondUserResources = DbLayer.ResourceRepository.Where(r => r.AccountId == account.Id).ToList();

            foreach (var secondUserResource in secondUserResources)
                if (secondUserResource.Subject == secondUserObject.Id)
                    throw new InvalidOperationException ("У второго юзера лицензий быть не должно.");

        }
    }
}
