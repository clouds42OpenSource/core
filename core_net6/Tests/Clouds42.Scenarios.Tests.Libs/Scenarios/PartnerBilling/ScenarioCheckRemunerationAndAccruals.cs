﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверка корректности отображения информации.
    /// Сценарий: создаём аккаунт и его кастомный сервис. Регистрируем по
    /// его реферальной ссылке второй аккаунт.
    /// Проверка : поле ежемесячное начисление равно 0 до момента
    /// выхода каст. сервиса из демо режима, после - полная стоимость.
    /// Ежемесячное вознаграждение сумма за кастомный сервис и аренду.
    /// </summary>
    public class ScenarioCheckRemunerationAndAccruals : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IPartnersClientDataProvider _partnersClientDataProvider;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        private readonly IPartnerDataProvider _partnerDataProvider;
        public ScenarioCheckRemunerationAndAccruals()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _partnersClientDataProvider = TestContext.ServiceProvider.GetRequiredService<IPartnersClientDataProvider>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            _partnerDataProvider = TestContext.ServiceProvider.GetRequiredService<IPartnerDataProvider>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();
            
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            const string serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                AccountId = createAccountCommand.AccountId,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = await DbLayer.BillingServiceRepository.FirstOrDefaultAsync(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");
            
            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = createBillingServiceCommand.CreateBillingServiceTest.AccountId
                }
            };
            var createSecondAccountCommand = new CreateAccountCommand(TestContext, secondAccount);
            createSecondAccountCommand.Run();
            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id,
                createSecondAccountCommand.AccountAdminId);
            var monthlyCharge = await _partnerDataProvider.GetMonthlyCharge(createAccountCommand.AccountId);
         
            var filter = new PartnersClientFilterDto
            {
                AccountId = createAccountCommand.AccountId,
                PageNumber = 1
            };
           
            var myClientsData = _partnersClientDataProvider.GetPartnerClients(filter);
            var chunkData = myClientsData.ChunkDataOfPagination;

            var rentBonus = chunkData.First(a => a.ServiceName == "Аренда 1С").MonthlyBonus;
            var serviceBonus = chunkData.First(a => a.ServiceName == "Тестовый Сервис").MonthlyBonus;
            Assert.AreEqual(monthlyCharge.Result, rentBonus, "До активации сервиса вознаграждение лишь за аренду");

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, createSecondAccountCommand.AccountId, false);

            monthlyCharge = await _partnerDataProvider.GetMonthlyCharge(createAccountCommand.AccountId);

            var agencyAgreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();
            var rent1CBonusByAgencyAgreement = 1500 * agencyAgreement.Rent1CRewardPercent / 100;
            var serviceBonusByAgencyAgreement =
                billingServiceTypes[0].ServiceTypeCost * agencyAgreement.ServiceOwnerRewardPercent / 100;

            var actualTotalRewardValue = rent1CBonusByAgencyAgreement + serviceBonusByAgencyAgreement;

            Assert.AreEqual(rentBonus + serviceBonus, actualTotalRewardValue, "Неверная сумма.");
            Assert.AreEqual(monthlyCharge.Result, rentBonus + serviceBonus,
                "После активации сервиса вознаграждение равно полной стоимости сервиса и частично аренды");
        }
    }
}
