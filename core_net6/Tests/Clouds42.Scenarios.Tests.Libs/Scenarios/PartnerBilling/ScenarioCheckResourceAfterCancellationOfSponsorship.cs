﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.BillingService.BillingServiceGet;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Дано: есть два аккаунта с активированным кастомным сервисом.
    /// Спонсор после спонсирования отзывает лицензию, отдаёт опять
    /// и потом спонсируемый отказывается от спонсирования.
    /// Проверка: в обеих случаях лицензия должна возвращаться спонсору.
    /// </summary>
    public class ScenarioCheckResourceAfterCancellationOfSponsorship : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly BillingServiceTypeAccessHelper _billingServiceTypeAccessHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioCheckResourceAfterCancellationOfSponsorship()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _billingServiceTypeAccessHelper = TestContext.ServiceProvider.GetRequiredService<BillingServiceTypeAccessHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }
        public override void Run()
        {
            var serviceRent1C = GetRent1CServiceOrThrowException();
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto()
            };
            var createSecondAccountCommand = new CreateAccountCommand(TestContext,secondAccount);
            createSecondAccountCommand.Run();

            #region service

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingService = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext, createBillingService);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден."); 

            #endregion service

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, service.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, serviceRent1C.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createSecondAccountCommand.AccountId, service.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createSecondAccountCommand.AccountId, serviceRent1C.Id, false);

            var serviceTypeDto = new BillingServiceTypeDto {Id = billingServiceTypes[1].Id, ServiceTypeCost = 800};

            _resourceConfigurationHelper.InsertServiceTypeResources(createAccountCommand.AccountId, createAccountCommand.AccountAdminId,
                [serviceTypeDto], 0);
            _resourceConfigurationHelper.InsertServiceTypeResources(createSecondAccountCommand.AccountId, createSecondAccountCommand.AccountAdminId,
                [serviceTypeDto], 0);

            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

            var firstAccountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.Account.Id).Login;
            var secondAccountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createSecondAccountCommand.Account.Id).Login;

            var serviceTypeStatusForFirstUser = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(service.Id, firstAccountUserLogin);
            var serviceTypeStatusForSecondUser = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(service.Id, secondAccountUserLogin);

            Assert.IsTrue(serviceTypeStatusForFirstUser.Result.ServiceIsActive, "Тестовый сервис не подключился первому аккаунту");
            Assert.IsTrue(serviceTypeStatusForSecondUser.Result.ServiceIsActive, "Тестовый сервис не подключился второму аккаунту");

            var serviceTypeResultAhead = new CalculateBillingServiceTypeResultDto
            {
                BillingServiceTypeId = billingServiceTypes.First().Id,
                Subject = createSecondAccountCommand.AccountAdminId,
                Status = true,
                Cost = billingServiceTypes.First().ServiceTypeCost,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto { I = true, Me = false, Label = createSecondAccountCommand.Account.AccountCaption}
            };

            var billingServiceTypeId = billingServiceTypes.First().Id;

            _billingServiceTypeAccessHelper.ApplyResources(createAccountCommand.Account,new List<CalculateBillingServiceTypeResultDto> { serviceTypeResultAhead }) ;

            var testResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createSecondAccountCommand.AccountId
                && r.AccountSponsorId == createAccountCommand.AccountId
                && r.BillingServiceTypeId == billingServiceTypeId).ToList();

            if (testResources==null)
            { throw new InvalidOperationException ("Ошибка спонсирования при создании ресурса"); }

            Assert.AreEqual(1, testResources.Count,"Создалось неверное количество ресурсов");

            var testResource = testResources.First();

            Assert.AreEqual(testResource.Cost, billingServiceTypes.First().ServiceTypeCost,"Неверная цена ресурса");
            Assert.AreEqual(testResource.Subject, createSecondAccountCommand.AccountAdminId,"Неверный сабджект");

            var serviceTypeResultBack = new CalculateBillingServiceTypeResultDto
            {
                BillingServiceTypeId = billingServiceTypes.First().Id,
                Subject = createSecondAccountCommand.AccountAdminId,
                Status = false,
                Cost = billingServiceTypes.First().ServiceTypeCost,
                Sponsorship = null
            };
            _billingServiceTypeAccessHelper.ApplyResources(createAccountCommand.Account, new List<CalculateBillingServiceTypeResultDto> { serviceTypeResultBack });
            testResource = DbLayer.ResourceRepository.FirstOrDefault(r=>r.AccountId== createAccountCommand.AccountId &&
                                                                         r.BillingServiceTypeId== billingServiceTypeId);
            Assert.IsNotNull(testResource,"Ошибка отзыва лицензии.");
            Assert.IsNull(testResource.Subject, "Ошибка отзыва лицензии, поле сабжект должно быть null.");
            Assert.AreEqual(testResource.Cost, billingServiceTypes.First().ServiceTypeCost,"Неверная цена.");

            serviceTypeResultAhead.Cost = 0;
            _billingServiceTypeAccessHelper.ApplyResources(createAccountCommand.Account, new List<CalculateBillingServiceTypeResultDto> {serviceTypeResultAhead});

            testResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createSecondAccountCommand.AccountId
                && r.AccountSponsorId == createAccountCommand.AccountId
                && r.BillingServiceTypeId == billingServiceTypeId).ToList();

            if (testResources == null)
                throw new InvalidOperationException ("Ошибка спонсирования при создании ресурса"); 

            Assert.AreEqual(1,testResources.Count, "Создалось неверное количество ресурсов");

            _billingServiceTypeAccessHelper.ApplyResources(createSecondAccountCommand.Account, new List<CalculateBillingServiceTypeResultDto> { serviceTypeResultBack });
            testResource = DbLayer.ResourceRepository.FirstOrDefault(r => r.AccountId == createAccountCommand.AccountId &&
                                                                          r.BillingServiceTypeId == billingServiceTypeId);

            Assert.IsNotNull(testResource, "Ошибка отзыва лицензии.");
            Assert.IsNull(testResource.Subject, "Ошибка отзыва лицензии, поле сабжект должно быть null.");

        }
    }
}
