﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Тест создания клиента из раздела партнёрки
    /// Сценарий:
    /// 1) Создаём аккаунт-реферал и его сервис
    /// 2) С-под его учётки регистрируем новый аккаунт в разделе партнёрки
    /// Проверяем:
    /// 1) Аккаунт создался и у него есть реферал
    /// 2) Ему не подключены сервисы реферала
    /// </summary>
    public class ScenarioCreateNewClientInAdminPanelTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public ScenarioCreateNewClientInAdminPanelTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var serviceName = "ReferralCustomService";
            
            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Сервис не найден.");

            var createReferralAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = service.AccountOwnerId
                }
            });
            createReferralAccountCommand.Run();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createReferralAccountCommand.AccountId && r.BillingServiceId == service.Id);

            Assert.IsNull(resConf, "Новому аккаунту подключился кастомный сервис реферала");
            Assert.AreEqual(createReferralAccountCommand.Account.ReferralAccountID, service.AccountOwnerId, "Новому аккаунту не выставился реферал!");

            var billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var filter = new PartnersClientFilterDto { PageNumber = 1, PageSize = 50 };

            var data = billingServiceManager.GetPartnerClients(filter);
            Assert.IsFalse(data.Error, "Ошибка получения списка клиентов");
            Assert.IsTrue(data.Result.TotalCount == 2, "Должно быть 2 записи: для аренды и диска.");

            var services = data.Result.ChunkDataOfPagination.Select(s => s.ServiceName).ToList();
            Assert.IsTrue(services.Contains(Clouds42Service.MyEnterprise.GetDisplayName()), "Не нашло аренду");
            Assert.IsTrue(services.Contains("Мой диск"), "Не нашло мой диск");
        }
    }
}
