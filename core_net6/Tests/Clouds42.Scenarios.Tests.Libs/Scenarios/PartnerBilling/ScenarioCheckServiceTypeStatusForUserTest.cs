﻿using Clouds42.DataContracts.BillingService;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса услуг сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: у аккаунта активен сервис. Первая услуга сервиса не активна у аккаунта, остальные активны
    /// </summary>
    public class ScenarioCheckServiceTypeStatusForUserTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;

        public ScenarioCheckServiceTypeStatusForUserTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
        }
        
        public override void Run()
        {
            var serviceRent1C = GetRent1CServiceOrThrowException();
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var managerResult = billingServiceCardDataManager.GetData(service.Id);
            var account = DbLayer.AccountsRepository.GetAccount(TestContext.AccessProvider.ContextAccountId);

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, service.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, serviceRent1C.Id, false);

            var resConfig =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == service.Id && rc.AccountId == account.Id);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Ошибка. У созданного аккаунта '{account.IndexNumber}' не подключен сервис.");

            _resourceConfigurationHelper.InsertServiceTypeResources(account.Id, account.Id, managerResult.Result.BillingServiceTypes, 1);

            var accountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == account.Id).Login;
            ScenarioCheckServiceTypeStatusForUserWhenServiceTypeIsNotActive(accountUserLogin, managerResult.Result.BillingServiceTypes.FirstOrDefault());
            ScenarioCheckServiceTypeForUserWhenServiceTypeIsActive(accountUserLogin, managerResult.Result.BillingServiceTypes.Skip(1).ToList());
        }

        /// <summary>
        ///     Предусловия: у аккаунта активен сервис и часть услуг этого сервиса(передаем в метод заведомо неактивную услугу)
        ///     Действия: проверяем что услуга сервиса не активна у данного аккаунта
        ///     Ожидание: услуга сервиса не активна у аккаунта      
        /// </summary>
        /// <param name="accountUserLogin">Логин тестового пользователя</param>
        /// <param name="notActivatedServiceType">Не активная услуга сервиса</param>
        private void ScenarioCheckServiceTypeStatusForUserWhenServiceTypeIsNotActive(string accountUserLogin, BillingServiceTypeDto notActivatedServiceType)
        {
            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

            if (notActivatedServiceType.Key.IsNullOrEmpty())
                throw new ArgumentException("У услуги не указан ключ");

            var serviceTypeStatus = partnerBillingServiceDataManager.CheckServiceTypeStatusForUser(notActivatedServiceType.Key,
                accountUserLogin);
            
            Assert.IsFalse(serviceTypeStatus.Result.Result);
        }

        /// <summary>
        ///     Предусловия: у аккаунта активен сервис и часть услуг этого сервиса
        ///     Действия: проверяем что услуги сервиса активны у данного аккаунта
        ///     Ожидание: услуги сервиса активны у аккаунта     
        /// </summary>
        /// <param name="accountUserLogin">Логин тестового пользователя</param>
        /// <param name="activatedServiceTypes"></param>
        private void ScenarioCheckServiceTypeForUserWhenServiceTypeIsActive(string accountUserLogin, List<BillingServiceTypeDto> activatedServiceTypes)
        {
            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();
            
            foreach (var serviceType in activatedServiceTypes)
            {
                if (serviceType.Key.IsNullOrEmpty())
                    throw new ArgumentException("У услуги не указан ключ");

                var serviceTypeStatus = partnerBillingServiceDataManager.CheckServiceTypeStatusForUser(serviceType.Key,
                    accountUserLogin);

                Assert.IsTrue(serviceTypeStatus.Result.Result);
            }
        }
    }
}
