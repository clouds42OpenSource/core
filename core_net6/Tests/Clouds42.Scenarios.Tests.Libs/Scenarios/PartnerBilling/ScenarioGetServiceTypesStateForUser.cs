﻿using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Common.Exceptions;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: 
    ///     1) у аккаунта активен сервис и метод возвращает true. 
    ///     2) у аккаунта не активнен сервис и метод возвращает false
    /// </summary>
    public class ScenarioGetServiceTypesStateForUser : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;

        public ScenarioGetServiceTypesStateForUser()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var account = DbLayer.AccountsRepository.GetAccount(TestContext.AccessProvider.ContextAccountId);
            var serviceRent1C = GetRent1CServiceOrThrowException();

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, service.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(account.Id, serviceRent1C.Id, false);

            var resConfig =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.BillingServiceId == service.Id && rc.AccountId == account.Id);

            if (resConfig == null)
                throw new NotFoundException(
                    $"Ошибка. У созданного аккаунта '{account.IndexNumber}' не подключен сервис.");

            _resourceConfigurationHelper.InsertServiceTypeResources(account.Id, account.Id, billingServiceTypes, 1);

            var accountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == account.Id).Login;
            
            ScenarioGetServiceTypesStateForUserWhenServiceIsActive(accountUserLogin, service.Id);

            var resourcesConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(w => w.BillingServiceId == serviceRent1C.Id);

            resourcesConfiguration.Frozen = true;
            resourcesConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            ScenarioGetServiceTypesStateForUserWhenServiceIsLocked(accountUserLogin, service.Id);
        }

        /// <summary>
        ///     Проверка: метод должен вернуть false, так так передаем заведомо не активный сервис
        /// </summary>
        /// <param name="accountUserLogin">Логин пользователя</param>
        /// <param name="serviceId">Номер сервиса</param>
        private void ScenarioGetServiceTypesStateForUserWhenServiceIsLocked(string accountUserLogin, Guid serviceId)
        {
            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

            var serviceTypeStatus = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(serviceId, accountUserLogin);

            Assert.IsFalse(serviceTypeStatus.Result.ServiceIsActive);
        }

        /// <summary>
        ///     Проверка: метод должен вернуть true, так так передаем заведомо активный сервис
        /// </summary>
        /// <param name="accountUserLogin">Логин пользователя</param>
        /// <param name="serviceId">Номер сервиса</param>
        private void ScenarioGetServiceTypesStateForUserWhenServiceIsActive(string accountUserLogin, Guid serviceId)
        {
            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

            var serviceTypeStatus = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(serviceId, accountUserLogin);

            Assert.IsTrue(serviceTypeStatus.Result.ServiceIsActive);
        }
    }
}
