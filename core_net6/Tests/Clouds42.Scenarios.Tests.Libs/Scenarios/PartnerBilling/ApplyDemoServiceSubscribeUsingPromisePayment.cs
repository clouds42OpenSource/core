﻿using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Подтверждение подписки демо сервиса обещанным платежом
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис
    ///         2) Подключаем сервис аккаунту
    ///         3) Подтверждаем подписку сервиса (устанавливаем флаг "IsPromisePayment")
    ///         4) Проверяем, что подписка подтверждена и создан платеж
    /// </summary>
    public class ApplyDemoServiceSubscribeUsingPromisePayment : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly Rent1CConfigurationAccessManager _configurationAccessManager;
        public ApplyDemoServiceSubscribeUsingPromisePayment()
        {
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            account.Run();

            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = [serviceTypeDto]
            });
            billingService.Run();

            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            _configurationAccessManager.TurnOnRent1C(account.AccountId);

            _resourceConfigurationHelper.InsertServiceDemoResourceConfiguration(account.AccountId, service.Id);
            _resourceConfigurationHelper.InsertResourcesWithResConfigChanging(account.AccountId, account.AccountAdminId, billingService.CreateBillingServiceTest.BillingServiceTypes, service.Id, 0);
            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            resourceConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            DbLayer.Save();

            var managerResult = _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, true);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            Assert.IsFalse(resourceConfiguration.IsDemoPeriod, "Демо период должен был завершиться");
            Assert.IsNull(resourceConfiguration.ExpireDate, "Дата окончания подписки сервиса должна быть null");

            var payment = DbLayer.PaymentRepository.FirstOrDefault(p => p.AccountId == account.AccountId);
            Assert.IsNotNull(payment, "Платеж не был создан");
        }
    }
}
