﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Common.Constants;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий проверки активации сервиса пользователем
    ///     1) Создаем сервис
    ///     2) Создаем пользователя с ролью AccountUser
    ///     3) Пытаемся активировать сервис
    ///     4) Проверяем что при попытке вернулась ошибка (но не выпало исключение)
    /// </summary>
    public class ScenarioCheckCustomServiceActivationForAccountUser : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ReactivateServiceManager _reactivateServiceManager;

        public ScenarioCheckCustomServiceActivationForAccountUser()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
        }

        public override void Run()
        {
            var createOwnerAccountCommand = new CreateAccountCommand(TestContext);
            createOwnerAccountCommand.Run();

            var createCloudServicesEnterpriseServer83Command = new CreateCloudServicesEnterpriseServerCommand(TestContext);
            createCloudServicesEnterpriseServer83Command.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceType = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Тестовый Сервис",
                AccountId = createOwnerAccountCommand.AccountId,
                BillingServiceTypes = billingServiceType
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);
            Assert.IsNotNull(service);

            var createClientAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = "Test account2",
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                Email = "TestEmail2@efsol.ru",
                LocaleName = LocaleConst.Russia,
                FullPhoneNumber = "+380680506712",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createClientAccountCommand.Run();

            var accountAdminId = createClientAccountCommand.AccountAdminId;
            var accountUsers = DbLayer.AccountUsersRepository.All().ToList();
            foreach (var accountUser in accountUsers)
            {
                DbLayer.AccountUserRoleRepository.DeleteRange(accountUser.AccountUserRoles);
                DbLayer.Save();
            }

            var clientAccountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == createClientAccountCommand.AccountAdminId);
            Assert.IsNotNull(clientAccountUser, "Клиент не создался");
            
            var newUserRole = new AccountUserRole
            {
                AccountUserId = accountAdminId,
                AccountUserGroup = AccountUserGroup.AccountUser
            };
            DbLayer.AccountUserRoleRepository.Insert(newUserRole);
            DbLayer.Save();

            var result = _reactivateServiceManager.ActivateServiceForExistingAccount(service.Id, accountAdminId);
            Assert.IsTrue(result.Error, "Должна быть ошибка.");
            Assert.AreEqual(result.State, ManagerResultState.PreconditionFailed, "Неверный тип результата");

            var resource = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            Assert.IsNull(resource, "Не должно быть записи кастомного сервиса");
        }
    }
}
