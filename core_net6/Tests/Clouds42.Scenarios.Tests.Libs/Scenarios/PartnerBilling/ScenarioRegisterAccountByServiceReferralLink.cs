﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий регистрации пользователя по ссылке сервиса зависимого от Аренды Стандарт
    ///     Действия:
    ///         1) Создаем сервис
    ///         2) Регистрируем аккаунт по ссылке сервиса
    ///         3) Проверяем что у пользователя активированы ресурсы стандарт, веб и услуги сервиса
    /// </summary>
    public class ScenarioRegisterAccountByServiceReferralLink : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioRegisterAccountByServiceReferralLink()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var webServiceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(
                st => st.SystemServiceType == ResourceType.MyEntUserWeb);

            var standartServiceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(
                st => st.SystemServiceType == ResourceType.MyEntUser);

            var billingServiceTypes = new List<BillingServiceTypeDto>
            {
                _createOrEditBillingServiceHelper.GenerateBillingServiceType(dependServiceTypeId: standartServiceType.Id),
                _createOrEditBillingServiceHelper.GenerateBillingServiceType(dependServiceTypeId: webServiceType.Id)
            };

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Кастомный сервис",
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();


            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"TestLogin{DateTime.Now:hhmmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = createAccountCommand.AccountId,
                    CloudServiceId = createBillingServiceCommand.Id
                }
            };

            var createSecondAccountCommand = new CreateAccountCommand(TestContext, secondAccount);
            createSecondAccountCommand.Run();

            var accountUserResources =
                DbLayer.ResourceRepository.AsQueryableNoTracking().Include(res => res.BillingServiceType)
                    .Where(res => res.AccountId == createSecondAccountCommand.AccountId).ToList();

            Assert.IsTrue(accountUserResources.Any());
            Assert.IsTrue(accountUserResources.Any(res =>
                res.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && 
                res.Subject != null));

            Assert.IsTrue(accountUserResources.Any(res =>
                res.BillingServiceType.SystemServiceType == ResourceType.MyEntUser &&
                res.Subject != null));

            Assert.IsTrue(accountUserResources.Any(res =>
                res.BillingServiceType.ServiceId == createBillingServiceCommand.Id && res.Subject != null));

        }
    }
}
