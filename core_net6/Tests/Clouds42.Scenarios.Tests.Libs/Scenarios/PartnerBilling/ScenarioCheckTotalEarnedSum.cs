﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Partner;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий для проверки получения суммы "Всего заработано" в партнерке
    /// 1. Создадим аккаунт и зададим ожидаемую сумму "Всего заработано".
    /// 2. Проверим, что начальная сумма равна 0.
    /// 3. Проверим, что будет получена сумма для транзакции с ручным вводом.
    /// 4. Проверим, что не будет получена сумма для исходящей транзакции.
    /// 5. Проверим, что не будет получена сумма для транзакции с неручным вводом.
    /// </summary>
    public class ScenarioCheckTotalEarnedSum : ScenarioBase
    {
        private readonly PartnerManager _partnerManager;

        public ScenarioCheckTotalEarnedSum()
        {
            _partnerManager = TestContext.ServiceProvider.GetRequiredService<PartnerManager>();
        }

        public override void Run()
        {
            #region EntityCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;
            var totalEarned = 0.0m;
            var amountOfPayment = 1000;

            #endregion

            #region CheckTotalEarnedIsZero

            var result = _partnerManager.GetSummaryInformationForPartner(accountId);
            CheckResultOfGettingTotalEarned(result.Error, totalEarned, result.Result.TotalEarned);

            #endregion

            #region CheckManualInputTransaction

            totalEarned += InsertAgentPayment(accountId, amountOfPayment, PaymentType.Inflow,
                AgentPaymentSourceTypeEnum.ManualInput, null);

            result = _partnerManager.GetSummaryInformationForPartner(accountId);
            CheckResultOfGettingTotalEarned(result.Error, totalEarned, result.Result.TotalEarned);

            #endregion

            #region CheckManualInputOutflowTransaction

            totalEarned += InsertAgentPayment(accountId, amountOfPayment, PaymentType.Outflow,
                AgentPaymentSourceTypeEnum.ManualInput, null);

            result = _partnerManager.GetSummaryInformationForPartner(accountId);
            CheckResultOfGettingTotalEarned(result.Error, totalEarned, result.Result.TotalEarned);

            #endregion

            #region CheckNotManualInputTransaction

            totalEarned += InsertAgentPayment(accountId, amountOfPayment, PaymentType.Inflow,
                AgentPaymentSourceTypeEnum.AgentCashOutRequest, null);

            result = _partnerManager.GetSummaryInformationForPartner(accountId);
            CheckResultOfGettingTotalEarned(result.Error, totalEarned, result.Result.TotalEarned);

            #endregion
        }

        /// <summary>
        /// Вставить платеж клиента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="amount">Сумма платежа</param>
        /// <param name="paymentType">Тип платежа</param>
        /// <param name="agentPaymentSourceType">Источник платежа</param>
        /// <param name="clientAccountId">Id аккаунта клиента</param>
        private decimal InsertAgentPayment(Guid accountId, decimal amount,
            PaymentType paymentType, AgentPaymentSourceTypeEnum agentPaymentSourceType, Guid? clientAccountId)
        {
            DbLayer.AgentPaymentRepository.InsertAgentPaymentAndUpdateAgentWallet(
                accountId, amount,
                paymentType: paymentType, date: DateTime.Now,
                comment: "Test comment",
                agentPaymentSourceType: agentPaymentSourceType,
                clientAccountId: clientAccountId,
                clientPaymentSum: amount);

            return paymentType == PaymentType.Inflow &&
                   (agentPaymentSourceType == AgentPaymentSourceTypeEnum.ManualInput || clientAccountId != null)
                ? amount
                : 0;
        }

        /// <summary>
        /// Проверить результат получения суммы "всего заработано"
        /// </summary>
        /// <param name="isError">Признак ошибка с менеджера</param>
        /// <param name="expectedSum">Ожидающаяся сумма</param>
        /// <param name="actualSum">Фактическая сумма</param>
        private void CheckResultOfGettingTotalEarned(bool isError, decimal expectedSum, decimal actualSum)
        {
            AssertHelper.Execute(() => Assert.IsFalse(isError));
            AssertHelper.HasEqualFieldValues(expectedSum, actualSum);
        }
    }
}
