﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.BillingService;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Цель: убериться , что при покупке не остаються лишние free лицензии
    /// </summary>
    public class ScenarioCheckFreeResourcesAfterPurchaseLargerLicense : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        private readonly BillingServiceManager _billingServiceManager;
        
        public ScenarioCheckFreeResourcesAfterPurchaseLargerLicense()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            #region Rent
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createAccountCommand.AccountId);
            var rent = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == account.Id && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent.ExpireDate = rent.CreateDate.Value.AddMonths(1);
            DbLayer.ResourceConfigurationRepository.Update(rent);
            
            //вносим оплату
            CreateInflowPayment(account.Id, null, 10000);

            var accountAdminId = createAccountCommand.AccountAdminId;
            var secondUserId = AddUserToAccount(account.Id, accountAdminId);

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();
            #endregion Rent

            #region serviceCreation
            var webResourceType =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUserWeb);
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2, webResourceType.Id);

            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[0].Id);
            var serviceName = "Тестовый Сервис";

            CreateDelimiterTemplate();

            var billingServiceId = CreateBillingService(serviceName, billingServiceTypes);
            
            var firstBillingServiceTypeId = billingServiceTypes[0].Id;
            var secondBillingServiceTypeId = billingServiceTypes[1].Id;

            //подключаем сервис
            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(billingServiceId, accountAdminId);
            //активируем сервис
            _billingServiceManager.ApplyDemoServiceSubscribe(billingServiceId, account.Id, false);
            #endregion serviceCreation
            
            //сотрём ненужные ресурсы
            var hoboRes = DbLayer.ResourceRepository.Where(r => r.Subject == null);
            DbLayer.ResourceRepository.DeleteRange(hoboRes);
            DbLayer.Save();

            RefreshDbCashContext(Context.Resources);

            //покупка второму пользователю меньшей лицензии
            var serviceTypeResultFirst = CreateCalculateBillingServiceTypeResultDtoObj(firstBillingServiceTypeId,
                secondUserId, true,
                billingServiceTypes.First().ServiceTypeCost);

            var serviceTypeResultFirstOf = CreateCalculateBillingServiceTypeResultDtoObj(firstBillingServiceTypeId,
                secondUserId, false,
                billingServiceTypes.First().ServiceTypeCost);

            var serviceTypeResultSecond = CreateCalculateBillingServiceTypeResultDtoObj(secondBillingServiceTypeId,
                secondUserId, true,
                billingServiceTypes[1].ServiceTypeCost);

            var firstPurchase = new List<CalculateBillingServiceTypeDto>();
            var secondPurchase = new List<CalculateBillingServiceTypeDto>();

            firstPurchase.Add(serviceTypeResultFirst);

            secondPurchase.Add(serviceTypeResultFirstOf);//отключаем малую
            secondPurchase.Add(serviceTypeResultSecond);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(account.Id, billingServiceId,
                false, null, firstPurchase);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(account.Id, billingServiceId,
                false, null, secondPurchase);

            RefreshDbCashContext(Context.Resources);
            CheckResourcesAfterPurchase(firstBillingServiceTypeId, secondBillingServiceTypeId, account.Id);
        }
        
        /// <summary>
        /// Добавить пользователя в аккаунт
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountAdminId">ID админа аккаунта</param>
        /// <returns>ID нового пользователя</returns>
        private Guid AddUserToAccount(Guid accountId, Guid accountAdminId)
        {
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = accountId,
                AccountIdString = SimpleIdHash.GetHashById(accountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };

            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            if (res.Error)
                throw new InvalidOperationException("Ошибка создания нового пользователя. " + res.Message);

            CreateUserSession(res.Result);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == secondUser.Login) 
                              ?? throw new NotFoundException($"Пользователь по логину {secondUser.Login} не найден");
            return accountUser.Id;
        }

        /// <summary>
        /// Создать сессию пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        private void CreateUserSession(Guid accountUserId)
        {
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, // default - "dynamic" session token
                ClientDescription = "GetTokenByLogin", // client description
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]", // user agent
                ClientIPAddress = AccessProvider.GetUserHostAddress(), // user ip
                TokenCreationTime = DateTime.Now,
                AccountUserId = accountUserId
            };

            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();
        }

        /// <summary>
        /// Создать сервис биллинга
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="billingServiceTypes">Список услуг</param>
        private Guid CreateBillingService(string serviceName, List<BillingServiceTypeDto> billingServiceTypes)
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            var firstBillingServiceTypeId = billingServiceTypes[0].Id;
            var secondBillingServiceTypeId = billingServiceTypes[1].Id;

            var firstRate = GetServiceTypeRateOrThrowException(firstBillingServiceTypeId);
            var secondRate = GetServiceTypeRateOrThrowException(secondBillingServiceTypeId);

            firstRate.Cost = 333;
            secondRate.Cost = 555;

            DbLayer.RateRepository.Update(firstRate);
            DbLayer.RateRepository.Update(secondRate);
            DbLayer.Save();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            return service.Id;
        }

        /// <summary>
        /// Получить тариф услуги или выкинуть исключение
        /// </summary>
        /// <param name="serviceTypeId">ID услуги</param>
        /// <returns>Тариф услуги</returns>
        private Rate GetServiceTypeRateOrThrowException(Guid serviceTypeId)
            => DbLayer.RateRepository.FirstOrDefault(r => r.BillingServiceTypeId == serviceTypeId)
               ?? throw new NotFoundException($"Тариф услуги {serviceTypeId} не найден");

        /// <summary>
        /// Создать модель активации услуги сервиса
        /// </summary>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="status">Статус</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        /// <returns>Модель активации услуги сервиса</returns>
        private static CalculateBillingServiceTypeResultDto CreateCalculateBillingServiceTypeResultDtoObj(Guid serviceTypeId,
            Guid accountUserId, bool status, decimal serviceTypeCost)
            => new()
            {
                BillingServiceTypeId = serviceTypeId,
                Subject = accountUserId,
                Status = status,
                Cost = serviceTypeCost,
                Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto {I = false, Me = false, Label = "777"}
            };

        /// <summary>
        /// Проверить ресурсы после покупки
        /// </summary>
        /// <param name="firstBillingServiceTypeId">ID первой услуги сервиса</param>
        /// <param name="secondBillingServiceTypeId">ID второй услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        private void CheckResourcesAfterPurchase(Guid firstBillingServiceTypeId, Guid secondBillingServiceTypeId, Guid accountId)
        {
            var firstRate = GetServiceTypeRateOrThrowException(firstBillingServiceTypeId);
            var secondRate = GetServiceTypeRateOrThrowException(secondBillingServiceTypeId);

            var littleGroup = DbLayer.ResourceRepository.Where(r => r.BillingServiceTypeId == firstBillingServiceTypeId).ToList();
            var bigGroup = DbLayer.ResourceRepository.Where(r => r.BillingServiceTypeId == secondBillingServiceTypeId).ToList();

            Assert.AreEqual(2, littleGroup.Count, "два юзера - две лицензии!!!первая услуга.");
            Assert.AreEqual(2, bigGroup.Count, "два юзера - две лицензии!!!вторая услуга.");
            foreach (var littleRes in littleGroup)
            {
                if (littleRes.Subject == null) { throw new InvalidOperationException ("Свободных лицензий не должно быть. Первая услуга"); }
                if (littleRes.Cost != firstRate.Cost) { throw new InvalidOperationException ("Выставились неверные цены. Первая услуга"); }
            }

            foreach (var bigRes in bigGroup)
            {
                if (bigRes.Subject == null) { throw new InvalidOperationException ("Свободных лицензий не должно быть. Вторая услуга"); }
                if (bigRes.Cost != secondRate.Cost) { throw new InvalidOperationException ("Выставились неверные цены. Вторая услуга"); }
            }

            var outflowType = PaymentType.Outflow;
            var payments = DbLayer.PaymentRepository
                .Where(p => p.AccountId == accountId && p.OperationType == outflowType.ToString()).OrderBy(d => d.Date).ToList();

            Assert.AreEqual(payments[1].Sum, firstRate.Cost, "Списалась неверная сумма. Первая услуга");
            Assert.AreEqual(payments[2].Sum, secondRate.Cost, "Списалась неверная сумма. Вторая услуга");
        }
    }
}
