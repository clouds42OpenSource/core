﻿using System;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    ///     Сценарий проверки статуса сервиса у аккаунта
    ///     Действия: создание аккаунта, сервиса и услуг сервиса и проверка статуса услуг сервиса
    ///     Проверка: у аккаунта активен сервис и его спонсирют. 
    ///     В первом случае у спонсора аренда активна и метод возвращает true
    ///     Во втором случае у спонсора аренда не активна и метод возвращает false
    /// </summary>
    public class ScenarioGetServiceTypeStateForUserWithSponsorship : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioGetServiceTypeStateForUserWithSponsorship()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto()
            };
            var createSecondAccountCommand = new CreateAccountCommand(
                TestContext,
                secondAccount);
            createSecondAccountCommand.Run();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var serviceRent1C = GetRent1CServiceOrThrowException();

            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, service.Id, false);
            _resourceConfigurationHelper.InsertServiceResourceConfiguration(createAccountCommand.AccountId, serviceRent1C.Id, false);
            _resourceConfigurationHelper.InsertServiceTypeResourcesWithSponsorship(createSecondAccountCommand.Account.Id, createSecondAccountCommand.Account.Id,
                billingServiceTypes, 1, createAccountCommand.AccountId);

            var accountUserLogin = DbLayer.AccountUsersRepository.FirstOrDefault(w => w.AccountId == createSecondAccountCommand.Account.Id).Login;

            var partnerBillingServiceDataManager = ServiceProvider.GetRequiredService<PartnerBillingServicesDataManager>();

            var serviceTypeStatus = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(service.Id, accountUserLogin);

            Assert.IsTrue(serviceTypeStatus.Result.ServiceIsActive);

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(w => w.BillingServiceId == serviceRent1C.Id && w.AccountId == createAccountCommand.AccountId);
            resConf.ExpireDate = DateTime.Now.AddDays(-1);
            resConf.Frozen = true;

            DbLayer.ResourceConfigurationRepository.Update(resConf);
            DbLayer.Save();
            
            var serviceTypeStatusAfterBlock = partnerBillingServiceDataManager.GetServiceTypeStateForUserById(service.Id, accountUserLogin);

            Assert.IsFalse(serviceTypeStatusAfterBlock.Result.ServiceIsActive);
        }
    }
}
