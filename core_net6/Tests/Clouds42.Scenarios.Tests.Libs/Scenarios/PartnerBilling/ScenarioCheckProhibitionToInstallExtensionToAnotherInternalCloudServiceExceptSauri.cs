﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий для проверки запрета установки расширения для внутреннего облачного сервиса кроме Саюри
    /// Действия:
    ///     1) Создаем кастомный сервис и указываем для него внутренний облачный сервис отличный от Саюри
    ///     2) Создаем аккаунт с 2 пользователями
    ///     3) Активируем сервис для аккаунта
    ///     4) Проверяем что инф. база создана, но возможности установить расширение нет
    /// </summary>
    public class ScenarioCheckProhibitionToInstallExtensionToAnotherInternalCloudServiceExceptSauri : ScenarioBase
    {

        public override void Run()
        {
            #region Создание аккаунта с 2 пользователями и сервиса

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdmin = createAccountCommand.Account.AccountUsers.FirstOrDefault();
            Assert.IsNotNull(accountAdmin, "Не создался админ аккаунта");

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest =
                {
                    AccountId = account.Id
                }
            };
            createBillingServiceCommand.Run();
            ManageBillingServiceControl(new ManageBillingServiceDto
            {
                Id = createBillingServiceCommand.Id,
                InternalCloudService = InternalCloudServiceEnum.Sauri,
                BillingServiceStatus = BillingServiceStatusEnum.IsActive
            });

            var billingService = GetBillingServiceOrThrowException(createBillingServiceCommand.Id);
            Assert.IsNotNull(billingService);
            Assert.IsTrue(billingService.InternalCloudService == InternalCloudServiceEnum.Sauri);

            var secondUserId = AddUserToAccount(new AccountUserDcTest
            {
                AccountId = account.Id
            });

            var secondUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == secondUserId);
            Assert.IsNotNull(secondUser);

            #endregion

            #region Активация сервиса у аккаунта
            Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>();
            var sp = Services.BuildServiceProvider();
            var _billingServiceManager = sp.GetRequiredService<BillingServiceManager>();
            var activateServiceForRegistrationAccountProvider = sp.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp"]);
            activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(createBillingServiceCommand.Id, secondUser.Id);

            #endregion

            #region Проверка возможности установить расширение

            var accountDatabases = DbLayer.DatabasesRepository.Where(db => db.AccountId == account.Id);
            Assert.IsTrue(accountDatabases.Any(), "База для кастомного сервиса не создана");

            var billingServiceInfo = _billingServiceManager.GetBillingService(billingService.Id, createAccountCommand.AccountId);
            Assert.IsFalse(billingServiceInfo.Error);
            Assert.IsFalse(billingServiceInfo.Result.CanInstallExtension);

            #endregion
        }
    }
}
