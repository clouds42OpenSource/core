﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий проверки активации сервиса в режиме демо,
    /// в случае наличия спонсируемых лицензии аренды у аккаунт админа
    /// 
    /// 1) Создаем аккаунт и демо сервис
    /// 2) Добавляем спонсируемую лицензию аренды для админа
    /// 3) Активируем демо сервис для аккаунта
    /// 4) Проверяем что сервис активировался, но лицензии не выдались
    /// </summary>
    public class ScenarioActivateDemoServiceWithSponsoredRent : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public ScenarioActivateDemoServiceWithSponsoredRent()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _activateServiceForRegistrationAccountProvider =
                TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            CreateSponsorAccountAndEnableSponsoredRent1C(createAccountCommand.AccountAdminId);

            var sponsoredLicenses = DbLayer.ResourceRepository.Where(w =>
                w.AccountId == createAccountCommand.AccountId && w.AccountSponsorId.HasValue);

            if (!sponsoredLicenses.Any())
                throw new InvalidOperationException("Спонсируемые лицензии не активировались");

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var standartResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var billingServiceTypes =
                _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standartResourceTypeId);
            billingServiceTypes[0].BillingType = BillingTypeEnum.ForAccountUser;

            var service = CreateCustomService(billingServiceTypes);

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(service.Id,
                createAccountCommand.AccountAdminId);

            var custServiceTypeId = billingServiceTypes[0].Id;
            var custResources = DbLayer.ResourceRepository.Where(r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingServiceTypeId == custServiceTypeId)
                .ToList();

            foreach (var custResource in custResources)
            {
                Assert.IsNull(custResource.Subject);
            }
        }

        /// <summary>
        /// Создать входящий платеж
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="cost">Сумма</param>
        private void CreateInflowPayment(CreateAccountCommand account, decimal cost)
        {

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<BillingServiceDataProvider>();

            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.AccountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }

        /// <summary>
        /// Создать кастомный сервис
        /// </summary>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService CreateCustomService(List<BillingServiceTypeDto> billingServiceTypes)
        {
            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();
            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            return service;
        }

        /// <summary>
        /// Создать аккаунт-спонсор и активировать спонсируемую лицензию аренды
        /// </summary>
        /// <param name="sponsoredAccountUserId">Спонсируемый пользователь</param>
        private void CreateSponsorAccountAndEnableSponsoredRent1C(Guid sponsoredAccountUserId)
        {
            var secondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestLogin{DateTime.Now:mmss}",
                Email = "TestEmail22@efsol.ru",
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1)
            });
            secondAccountCommand.Run();

            var cloud42ServiceFactory = ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            var rent1CService = (MyEnterprise42CloudService)cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, secondAccountCommand.AccountId);
            rent1CService.ActivateService();

            var billingAccount =
                TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(a =>
                    a.Id == secondAccountCommand.AccountId);

            var rateProvider = ServiceProvider.GetRequiredService<IRateProvider>(); ;
            var myEntUserWeb = TestContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(x => x.SystemServiceType == ResourceType.MyEntUserWeb);
            var cost = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id).Cost;

            CreateInflowPayment(secondAccountCommand, cost);

            rent1CService.ConfigureAccesses([
                new() { AccountUserId = sponsoredAccountUserId, WebResource = true, SponsorAccountUser = true }
            ]);
        }
    }
}
