﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверка корректности выбора локали для клиента
    /// Сценарий : 1) создаём владельца сервиса на русской локали,
    /// 2) создаём его сервис. 3) По ссылке этого сервиса регистрируем новый аккаунт, указываем
    /// украинский номер телефона.
    /// Проверяем: новый аккаунт будет размещён на локали владельца сервиса(РФ)
    /// </summary>
    public class GetLocaleOfServiceOwnerOnRegistration : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public GetLocaleOfServiceOwnerOnRegistration()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var createOwnerAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Russia,
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createOwnerAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceType = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var billingServiceTypeId = billingServiceType[0].Id;

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Тестовый Сервис",
                AccountId = createOwnerAccountCommand.AccountId,
                BillingServiceTypes = billingServiceType
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var createClientAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = "Test account2",
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                Email = "TestEmail2@efsol.ru",
                LocaleName = LocaleConst.Ukraine,
                FullPhoneNumber = "+380680506712",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = service.Id,
                    Configuration1C = null,
                    ReferralAccountId = createOwnerAccountCommand.AccountId,
                    RegistrationSource = ExternalClient.Promo
                }
            });
            createClientAccountCommand.Run();

            var clientAcc =
                DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createClientAccountCommand.AccountId);
            var ruLocale = DbLayer.LocaleRepository.FirstOrDefault(l => l.CurrencyCode == 643);

            var customService = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == createClientAccountCommand.AccountId && r.BillingServiceId == service.Id);
            Assert.IsNotNull(customService, "Клиенту не подключился кастомный сервис");

            var customResources = DbLayer.ResourceRepository.Where(r =>
                r.AccountId == createClientAccountCommand.AccountId && r.BillingServiceTypeId == billingServiceTypeId);
            Assert.IsTrue(customResources.Any(), "Клиенту не выделелись лицензии кастомного сервиса");
            Assert.AreEqual(GetAccountLocale(clientAcc.Id).Name, ruLocale.Name,
                "Регистрация прошла на неверной локали.");
        }
    }
}