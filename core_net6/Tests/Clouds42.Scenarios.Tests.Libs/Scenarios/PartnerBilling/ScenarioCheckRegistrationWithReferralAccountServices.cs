﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Проверяем что после регистрации новому аккаунту будут подключены все активные
    /// кастомные сервисы регистрирующего.
    /// Сценарий:  создаём аккаунт, 5 сервисов(3 активных, на модерации и черновик) и
    /// услуги этих сервисов. Проверяем, что у нового аккаунта не подлючён ни один кастомный сервис.
    /// </summary>
    public class ScenarioCheckRegistrationWithReferralAccountServices : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioCheckRegistrationWithReferralAccountServices()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            #region service

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[2].ServiceTypeRelations.Add(billingServiceTypes[3].Id);

            var billingServiceTypes2 = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            var billingServiceTypes3 = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            var billingServiceTypes4 = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            var billingServiceTypes5 = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);

            var serviceName1 = "1 Сервис Активный";
            var serviceName2 = "2 Сервис Активный";
            var serviceName3 = "3 Сервис Активный";
            var serviceName4 = "4 Сервис Модерация";
            var serviceName5 = "5 Сервис Черновик";

            var createBillingServiceTest1 = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName1,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceTest2 = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName2,
                BillingServiceTypes = billingServiceTypes2
            };

            var createBillingServiceTest3 = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName3,
                BillingServiceTypes = billingServiceTypes3
            };

            var createBillingServiceTest4 = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName4,
                BillingServiceTypes = billingServiceTypes4,
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceTest5 = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName5,
                BillingServiceTypes = billingServiceTypes5,
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var createBillingServiceCommand1 =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest1);
            var createBillingServiceCommand2 =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest2);
            var createBillingServiceCommand3 =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest3);
            var createBillingServiceCommand4 =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest4, false);
            var createBillingServiceCommand5 =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest5, false);

            createBillingServiceCommand1.Run();
            createBillingServiceCommand2.Run();
            createBillingServiceCommand3.Run();
            createBillingServiceCommand4.Run();
            createBillingServiceCommand5.Run();

            if (createBillingServiceCommand1.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand1.Result.Message);
            if (createBillingServiceCommand2.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand2.Result.Message);
            if (createBillingServiceCommand3.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand3.Result.Message);
            if (createBillingServiceCommand4.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand3.Result.Message);
            if (createBillingServiceCommand5.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand3.Result.Message);

            var services = DbLayer.BillingServiceRepository.Where(w =>
                w.Name == serviceName1 || w.Name == serviceName2 || w.Name == serviceName3 || w.Name == serviceName4 || w.Name == serviceName5).ToList();

            Assert.IsTrue(services.Any(), "Произошла ошибка в процессе создания. Сервисы не найдены.");

            Assert.IsTrue(services.Count == 5, "Произошла ошибка в процессе создания. Неверное количество сервисов.");

            #endregion service

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"TestLogin{DateTime.Now:hhmmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondTestEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.Unknown,
                    ReferralAccountId = createAccountCommand.AccountId
                }
            };
            var createSecondAccountCommand = new CreateAccountCommand(TestContext, secondAccount);
            createSecondAccountCommand.Run();

            RefreshDbCashContext(Context.ResourcesConfigurations);

            var resConfList = DbLayer.ResourceConfigurationRepository.Where(r =>
                r.AccountId == createSecondAccountCommand.AccountId && r.BillingService.SystemService == null && r.BillingService.Name != "Дополнительные сеансы").ToList();

            Assert.AreEqual(0,resConfList.Count, "Не должно быть ни одного кастомного сервиса");

            var accountUserResources =
                DbLayer.ResourceRepository.AsQueryableNoTracking().Include(res => res.BillingServiceType)
                    .Where(res => res.AccountId == createSecondAccountCommand.AccountId).ToList();

            Assert.IsTrue(accountUserResources.Any(res =>
                res.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb &&
                res.Subject != null));
        }
    }
}
