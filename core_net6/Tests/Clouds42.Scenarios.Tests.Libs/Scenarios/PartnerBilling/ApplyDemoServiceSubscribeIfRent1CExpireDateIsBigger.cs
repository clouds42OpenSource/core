﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Потверждение подписки демо сервиса если дата окончания
    /// Аренды больше чем дата окончания сервиса
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис
    ///         2) Подключаем сервис аккаунту
    ///         3) Устанавливаем дату окончания подписки сервиса меньше чем дата окончания подпискаи сервиса Аренда 1С
    ///         4) Пытаемся подтвердить подписку, проверяем что не удалось подтвердить подписку тк нет денег на балансе
    ///         5) Создаем платеж, подтверждаем подписку, проверяем, что подписка подтверждена и с баланса списало деньги
    /// </summary>
    public class ApplyDemoServiceSubscribeIfRent1CExpireDateIsBigger : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly Rent1CConfigurationAccessManager _configurationAccessManager;
        public ApplyDemoServiceSubscribeIfRent1CExpireDateIsBigger()
        {
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            account.Run();

            var serviceTypeDto = _createOrEditBillingServiceHelper.GenerateBillingServiceType();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = [serviceTypeDto]
            });
            billingService.Run();

            var service =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs =>
                    bs.Name == billingService.CreateBillingServiceTest.Name);

            _configurationAccessManager.TurnOnRent1C(account.AccountId);
            _resourceConfigurationHelper.InsertServiceDemoResourceConfiguration(account.AccountId, service.Id);
            _resourceConfigurationHelper.InsertResourcesWithResConfigChanging(account.AccountId, account.AccountAdminId, billingService.CreateBillingServiceTest.BillingServiceTypes, service.Id, 0);

            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            resourceConfiguration.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(resourceConfiguration);
            DbLayer.Save();

            var applyDemoServiceSubscribe = _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, false);

            Assert.IsNotNull(applyDemoServiceSubscribe.Result.NeedMoney, "Частичная стоимость не должна быть null");

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = serviceTypeDto.ServiceTypeCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.AccountId, false);

            var resConfig =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            Assert.IsFalse(resConfig.IsDemoPeriod, "Сервис не должен быть на демо периоде");
            Assert.IsNull(resConfig.ExpireDate, "Дата окончания подписки сервиса должна быть null");

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == account.AccountId);
            Assert.AreEqual(serviceTypeDto.ServiceTypeCost - applyDemoServiceSubscribe.Result.NeedMoney, billingAccount.Balance, "Была списана неправильная сумма");
        }
    }
}