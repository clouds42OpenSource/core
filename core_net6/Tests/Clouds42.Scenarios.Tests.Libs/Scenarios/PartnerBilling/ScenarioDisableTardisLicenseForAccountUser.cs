﻿using Clouds42.Billing;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий отключения услуги Тардис для пользователя
    ///  Действия:
    ///         1) Создаем аккаунт с сервисом и инф. базой Тардис
    ///         2) Регистрируем второй аккаунт по реф. ссылке сервиса
    ///         3) Проверяем что второму аккаунту подлключен сервис и выдан доступ в инф. базу
    ///         4) Отключаем услугу сервиса для пользователя и проверяем что доступ к базе удален
    /// </summary>
    public class ScenarioDisableTardisLicenseForAccountUser : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;
        private readonly AccountDatabasePathHelper _accountDatabasesPathHelper;
        private readonly TestDataGenerator _testDataGenerator;
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioDisableTardisLicenseForAccountUser()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            _accountDatabasesPathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var standartServiceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(
                st => st.SystemServiceType == ResourceType.MyEntUser);

            var billingServiceTypes = new List<BillingServiceTypeDto>
            {
                _createOrEditBillingServiceHelper.GenerateBillingServiceType(
                    dependServiceTypeId: standartServiceType.Id),
            };

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Кастомный сервис",
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(serv => serv.Id == createBillingServiceCommand.Id);

            billingService.InternalCloudService = InternalCloudServiceEnum.Tardis;
            DbLayer.BillingServiceRepository.Update(billingService);
            DbLayer.Save();

            CreateTardisAccountDatabase(createAccountCommand);

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Test account",
                Login = $"TestLogin{DateTime.Now:hhmmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = $"TestEmail{DateTime.Now:hhmmss}@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = createAccountCommand.AccountId,
                    CloudServiceId = createBillingServiceCommand.Id
                }
            };

            var createSecondAccountCommand = new CreateAccountCommand(TestContext, secondAccount);
            createSecondAccountCommand.Run();

            CheckDataAfterRegisterAccount(createSecondAccountCommand.AccountId,
                createSecondAccountCommand.AccountAdminId, billingService.Id);

            var dataAccountUsers = new List<CalculateBillingServiceTypeDto>();

            billingServiceTypes.ForEach(serviceType =>
            {
                dataAccountUsers.Add(new CalculateBillingServiceTypeDto
                {
                    Subject = createSecondAccountCommand.AccountAdminId,
                    Status = false,
                    BillingServiceTypeId = serviceType.Id,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        I = false,
                        Me = false
                    }
                });
            });

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(createSecondAccountCommand.AccountId,
                billingService.Id, false, null, dataAccountUsers);

            CheckDataAfterDisableLicense(createSecondAccountCommand.AccountId,
                createSecondAccountCommand.AccountAdminId, billingService.Id);
        }

        /// <summary>
        /// Проверить данные после регистрации аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="billingServiceId">ID сервиса</param>
        private void CheckDataAfterRegisterAccount(Guid accountId, Guid accountUserId, Guid billingServiceId)
        {
            var accountUserResourcesData = GetAccountResourcesByBillingType(accountId);

            Assert.IsTrue(accountUserResourcesData.Any());
            Assert.IsTrue(accountUserResourcesData.Any(data =>
                data.ServiceType.SystemServiceType == ResourceType.MyEntUserWeb &&
                data.Resource.Subject != null));

            Assert.IsTrue(accountUserResourcesData.Any(data =>
                data.ServiceType.ServiceId == billingServiceId && data.Resource.Subject != null));

            var accountUserAccesses = DbLayer.AcDbAccessesRepository.Where(access =>
                access.AccountUserID == accountUserId).ToList();

            Assert.IsTrue(accountUserAccesses.Any());
            Assert.IsTrue(accountUserAccesses.Any(access =>
                access.AccountDatabaseID == CloudConfigurationProvider.Tardis.GetAccountDatabaseId()));
        }

        /// <summary>
        /// Проверить данные после отключения лицензий сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="billingServiceId">ID сервиса</param>
        private void CheckDataAfterDisableLicense(Guid accountId, Guid accountUserId, Guid billingServiceId)
        {
            var accountUserResources =
                DbLayer.ResourceRepository.Where(res => res.AccountId == accountId).ToList();

            Assert.IsTrue(accountUserResources.Any());
            Assert.IsTrue(accountUserResources.Any(res =>
                res.BillingServiceType.SystemServiceType == ResourceType.MyEntUser &&
                res.Subject != null));

            Assert.IsFalse(accountUserResources.Any(res =>
                res.BillingServiceType.ServiceId == billingServiceId && res.Subject != null));

            var accountUserAccesses = DbLayer.AcDbAccessesRepository.Where(access =>
                access.AccountUserID == accountUserId).ToList();

            Assert.IsFalse(accountUserAccesses.Any(access =>
                access.AccountDatabaseID == CloudConfigurationProvider.Tardis.GetAccountDatabaseId()));
        }

        /// <summary>
        /// Создать инф. базу Тардис
        /// </summary>
        /// <param name="accountDetails">Данные аккаунта</param>
        private void CreateTardisAccountDatabase(IAccountDetails accountDetails)
        {
            var tardisAccountDatabaseId = CloudConfigurationProvider.Tardis.GetAccountDatabaseId();

            var dbTemplate = DbLayer.DbTemplateRepository.FirstOrDefault();
            var accountDatabase = _testDataGenerator.GenerateAccountDatabase(accountDetails.AccountId, 1, 1,
                fileStorageId:
                GetAccountFileStorageId(accountDetails.AccountId));
            accountDatabase.Id = tardisAccountDatabaseId;
            accountDatabase.TemplateId = dbTemplate.Id;
            DbLayer.DatabasesRepository.Insert(accountDatabase);
            DbLayer.Save();

            var accountDatabasePath = _accountDatabasesPathHelper.GetPath(accountDatabase);
            if (!Directory.Exists(accountDatabasePath))
                Directory.CreateDirectory(accountDatabasePath);
        }
    }
}