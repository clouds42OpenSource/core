﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Тест проверки запрета подключения кастомных
    /// сервисов если ты не на русской локали
    /// Действия :
    /// 1) создаём на РУ владельца сервиса и сам сервис.
    /// 2) создаём второй аккаунт на украинской локали. Подключаем ему
    /// аренду.
    /// 3) пробуем подключить ему кастомный сервис.
    /// Проверяем:
    /// 1) должны получить ошибку
    /// 2) сервис не подключён.
    /// </summary>
    public class ScenarioCheckActivationCustomServiceForExistingUkrAccountTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ReactivateServiceManager _reactivateServiceManager;

        public ScenarioCheckActivationCustomServiceForExistingUkrAccountTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
        }

        public override void Run()
        {
            #region ownerAndService

            var createOwnerAccountCommand = new CreateAccountCommand(TestContext);
            createOwnerAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceType = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Тестовый Сервис",
                AccountId = createOwnerAccountCommand.AccountId,
                BillingServiceTypes = billingServiceType
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            #endregion ownerAndService

            #region regClient

            var createClientAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = "Test account2",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Email = "TestEmail2@efsol.ru",
                LocaleName = LocaleConst.Ukraine,
                FullPhoneNumber = "+380680506712",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createClientAccountCommand.Run();

            var accountAdminId = createClientAccountCommand.AccountAdminId;

            var clientAcc =
                DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createClientAccountCommand.AccountId);
            Assert.IsNotNull(clientAcc, "Клиент не создался");
            Assert.AreEqual(GetAccountLocale(clientAcc.Id).Name, LocaleConst.Ukraine, "Не та локаль.");

            #endregion regClient

            #region serviceActivation

            var result = _reactivateServiceManager.ActivateServiceForExistingAccount(service.Id, accountAdminId);
            Assert.IsTrue(result.Error, "Должна быть ошибка.");
            Assert.AreEqual(result.State, ManagerResultState.PreconditionFailed, "Неверный тип результата");

            var resource =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r => r.BillingServiceId == service.Id);
            Assert.IsNull(resource, "Не должно быть записи кастомного сервиса");

            #endregion serviceActivation
        }
    }
}