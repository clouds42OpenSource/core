﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AlphaNumericsSupport;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerBilling
{
    /// <summary>
    /// Сценарий теста проверки поля Демо период
    /// при выборке клиентов партнера
    /// 1) Создаем аккаунт и для него сервис
    /// </summary>
    public class ScenarioCheckDemoPeriodWhenFetchingPartnerClientsTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ScenarioCheckDemoPeriodWhenFetchingPartnerClientsTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

           CreateDelimiterTemplate();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var serviceName = "Тестовый Сервис";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                
                Name = serviceName,
                AccountId = createAccountCommand.AccountId,
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException (createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);

            if (service == null)
                throw new NotFoundException("Произошла ошибка в процессе создания. Сервис не найден.");

            var secondAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Second Account",
                Login = $"SecondTestLogin{DateTime.Now:mmss}",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "SecondEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = "79046185729",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = createBillingServiceCommand.CreateBillingServiceTest.AccountId
                }
            };

            var thirdAccount = new AccountRegistrationModelTest
            {
                AccountCaption = "Third Account",
                Login = "ThirdLogin",
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                Email = "ThirdEmail@efsol.ru",
                FirstName = "TestFirstName",
                Inn = "0123456789",
                FullPhoneNumber = "79046254178",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = createBillingServiceCommand.CreateBillingServiceTest.AccountId
                }
            };

            var createSecondAccountCommand = new CreateAccountCommand(TestContext, secondAccount);
            createSecondAccountCommand.Run();
            var createThirdAccountCommand = new CreateAccountCommand(TestContext, thirdAccount);
            createThirdAccountCommand.Run();

            var billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();

            var fetchingPartnerClients = billingServiceManager.GetPartnerClients(new PartnersClientFilterDto
            {
                PageNumber = 1
            });

            if (fetchingPartnerClients.Error)
                throw new NotFoundException(fetchingPartnerClients.Message);

            CheckDemoPeriod(fetchingPartnerClients.Result.ChunkDataOfPagination);

            var resConfigs =
                DbLayer.ResourceConfigurationRepository.Where(rc => rc.BillingServiceId == service.Id).ToList();

            resConfigs.ForEach(x =>
            {
                x.IsDemoPeriod = false;
                DbLayer.ResourceConfigurationRepository.Update(x);
                DbLayer.Save();
            });

            var paymentManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();

            paymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createSecondAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            paymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createThirdAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });


            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            fetchingPartnerClients = billingServiceManager.GetPartnerClients(new PartnersClientFilterDto
            {
                PageNumber = 1
            });

            if (fetchingPartnerClients.Error)
                throw new NotFoundException(fetchingPartnerClients.Message);

            CheckDemoPeriod(fetchingPartnerClients.Result.ChunkDataOfPagination, false);
        }

        /// <summary>
        /// Проверить демо период у клиентов партнера
        /// </summary>
        /// <param name="partnerClients">Клиента партнера</param>
        /// <param name="isDemo">Флаг указывающий на демо период</param>
        private static void CheckDemoPeriod(IEnumerable<PartnerClientDto> partnerClients, bool isDemo = true)
        {
            if (partnerClients.Any(x => x.IsDemoPeriod != isDemo))
            {
                throw new ArgumentException("Не верное значение поля Демо период");
            }
        }
    }
}
