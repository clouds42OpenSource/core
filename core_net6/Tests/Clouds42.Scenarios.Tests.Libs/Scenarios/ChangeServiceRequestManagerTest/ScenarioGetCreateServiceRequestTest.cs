﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestManagerTest
{
    /// <summary>
    /// Сценарий теста, по проверке получения
    /// заявки на создание сервиса
    ///
    /// 1) Создаем сервис и отправляем его на модерацию
    /// 2) Пытаемся получить модель заявки отправляя дефолтный Id(ожидаем ошибку)
    /// 3) Получаем модель заявки и проверяем поля в "Текущие данные сервиса"
    /// 4) Проверяем что "Новые данные сервиса" равны null
    /// </summary>
    public class ScenarioGetCreateServiceRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                }, false);

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);

            var createServiceRequest = DbLayer.GetGenericRepository<CreateServiceRequest>()
                .FirstOrDefault(esr => esr.BillingServiceId == service.Id);

            Assert.IsNotNull(createServiceRequest);
            Assert.AreEqual(createServiceRequest.ChangeRequestType, ChangeRequestTypeEnum.Creation);

            var changeServiceRequestManager = ServiceProvider.GetRequiredService<ChangeServiceRequestManager>();
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var changeServiceRequestManagerResult = changeServiceRequestManager.Get(Guid.NewGuid());
            Assert.IsTrue(changeServiceRequestManagerResult.Error);

            changeServiceRequestManagerResult = changeServiceRequestManager.Get(createServiceRequest.Id);
            Assert.IsFalse(changeServiceRequestManagerResult.Error);

            var changeServiceRequestDto = changeServiceRequestManagerResult.Result;

            Assert.AreEqual(createServiceRequest.ChangeRequestType, changeServiceRequestDto.ChangeRequestType);
            Assert.AreEqual(createServiceRequest.Status, changeServiceRequestDto.Status);
            Assert.AreEqual(createServiceRequest.ModeratorComment, changeServiceRequestDto.ModeratorComment);
            Assert.IsNull(changeServiceRequestDto.NewServiceData);

            var resultOfComparing =
                createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingModelAndService(
                    changeServiceRequestDto.CurrentServiceData.MapToEditBillingServiceDto(), service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }
    }
}
