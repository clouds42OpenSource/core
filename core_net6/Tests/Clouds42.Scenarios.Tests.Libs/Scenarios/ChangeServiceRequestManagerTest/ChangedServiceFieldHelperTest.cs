﻿using System;
using System.Collections.Generic;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestManagerTest
{
    /// <summary>
    /// Хелпер для работы с измененныеми полями сервиса
    /// </summary>
    public static class ChangedServiceFieldHelperTest
    {
        /// <summary>
        /// Проверить наличие в списке элемента
        /// </summary>
        /// <param name="list">Список</param>
        /// <param name="element">Элемент</param>
        public static void CheckAvailabilityInList(this List<string> list, string element)
        {
            if (!list.Contains(element))
                throw new ArgumentException($"Не удалось найти элемент '{element}' в списке");
        }
    }
}
