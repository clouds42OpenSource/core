﻿using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestManagerTest
{
    /// <summary>
    /// Сценарий теста, по проверке  полей услуг сервиса,
    /// при получении заявки на редактирование сервиса
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем услуги сервиса и отправляем на модерацию
    /// 3) Получаем модель заявки на редактирование сервиса и проверяем данные:
    ///     1. Проверяем что не измененные поля услуг не будут null, а будут заполненны старыми
    ///     2. Проверяем измененные поля услуг
    ///     3. Проверяем поля из "Текущие данные сервиса"
    ///     4. Проверяем что список измененных полей сервиса содержит измененные поля услуг
    /// </summary>
    public class ScenarioCheckServiceTypesAtGetEditServiceRequestTest: ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var changeServiceRequestManager = ServiceProvider.GetRequiredService<ChangeServiceRequestManager>();


            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);
            var serviceId = service.Id;

            var billingServiceCardDto = billingServiceCardDataManager.GetData(serviceId);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            var serviceTypeThree = editBillingServiceDto.BillingServiceTypes[2];
            serviceTypeOne.Name = "ChangedName";
            serviceTypeTwo.Description = "ChangedDescription";
            serviceTypeThree.ServiceTypeCost = 20;

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            var editServiceRequest = DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(esr =>
                esr.BillingServiceId == service.Id && esr.ChangeRequestType == ChangeRequestTypeEnum.Editing);

            Assert.IsNotNull(editServiceRequest);

            var changeServiceRequestManagerResult = changeServiceRequestManager.Get(editServiceRequest.Id);
            Assert.IsFalse(changeServiceRequestManagerResult.Error);

            Assert.IsNotNull(changeServiceRequestManagerResult.Result);
            Assert.IsNotNull(changeServiceRequestManagerResult.Result.NewServiceData);
            Assert.IsNotNull(changeServiceRequestManagerResult.Result.CurrentServiceData);

            var changedServiceTypes = changeServiceRequestManagerResult.Result.NewServiceData.BillingServiceTypes;
            Assert.AreEqual(3, changedServiceTypes.Count);

            var changedServiceFieldsCount = 3;

            var changedServiceType = changedServiceTypes.FirstOrDefault(cst => cst.ServiceTypeId == serviceTypeOne.Id);
            Assert.IsNotNull(changedServiceType);
            Assert.AreEqual(changedServiceType.Name, serviceTypeOne.Name);
            Assert.AreEqual(changedServiceType.Description, serviceTypeOne.Description);
            Assert.AreEqual(changedServiceType.ServiceTypeCost, serviceTypeOne.ServiceTypeCost);
            
            changedServiceType = changedServiceTypes.FirstOrDefault(cst => cst.ServiceTypeId == serviceTypeTwo.Id);
            Assert.IsNotNull(changedServiceType);
            Assert.AreEqual(changedServiceType.Name, serviceTypeTwo.Name);
            Assert.AreEqual(changedServiceType.Description, serviceTypeTwo.Description);
            Assert.AreEqual(changedServiceType.ServiceTypeCost, serviceTypeTwo.ServiceTypeCost);

            changedServiceType = changedServiceTypes.FirstOrDefault(cst => cst.ServiceTypeId == serviceTypeThree.Id);
            Assert.IsNotNull(changedServiceType);
            Assert.AreEqual(changedServiceType.Name, serviceTypeThree.Name);
            Assert.AreEqual(changedServiceType.Description, serviceTypeThree.Description);
            Assert.AreEqual(changedServiceType.ServiceTypeCost, serviceTypeThree.ServiceTypeCost);
        }
    }
}
