﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestManagerTest
{
    /// <summary>
    /// Сценарий теста, по проверке получения
    /// заявки на редактирование сервиса
    /// 
    /// 1) Создаем активный сервис
    /// 2) Редактируем Имя сервиса и отправляем на модерацию
    /// 3) Получаем модель заявки на редактирование сервиса и проверяем данные:
    ///     1. Проверяем что не измененные поля не будут null в "Новые данные сервиса",а будут заполненны старыми
    ///     2. Проверяем измененные поля в "Новые данные сервиса"
    ///     3. Проверяем поля из "Текущие данные сервиса".    
    /// </summary>
    public class ScenarioGetEditServiceRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var changeServiceRequestManager = ServiceProvider.GetRequiredService<ChangeServiceRequestManager>();

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);
            var serviceId = service.Id;

            var billingServiceCardDto = billingServiceCardDataManager.GetData(serviceId);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewServiceName";

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            var editServiceRequest = DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(esr =>
                esr.BillingServiceId == service.Id && esr.ChangeRequestType == ChangeRequestTypeEnum.Editing);

            Assert.IsNotNull(editServiceRequest);

            var changeServiceRequestManagerResult = changeServiceRequestManager.Get(editServiceRequest.Id);
            Assert.IsFalse(changeServiceRequestManagerResult.Error);

            Assert.IsNotNull(changeServiceRequestManagerResult.Result);
            Assert.IsNotNull(changeServiceRequestManagerResult.Result.NewServiceData);
            Assert.IsNotNull(changeServiceRequestManagerResult.Result.CurrentServiceData);

            var changeServiceRequestDto = changeServiceRequestManagerResult.Result;

            Assert.AreEqual(editServiceRequest.ChangeRequestType, changeServiceRequestDto.ChangeRequestType);
            Assert.AreEqual(editServiceRequest.Status, changeServiceRequestDto.Status);
            Assert.AreEqual(editServiceRequest.ModeratorComment, changeServiceRequestDto.ModeratorComment);

            Assert.AreEqual(service.ShortDescription, changeServiceRequestDto.NewServiceData.ShortDescription);
            Assert.AreEqual(service.Opportunities, changeServiceRequestDto.NewServiceData.Opportunities);

            var resultOfComparing =
                createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingModelAndService(
                    changeServiceRequestDto.CurrentServiceData.MapToEditBillingServiceDto(), service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }
    }
}
