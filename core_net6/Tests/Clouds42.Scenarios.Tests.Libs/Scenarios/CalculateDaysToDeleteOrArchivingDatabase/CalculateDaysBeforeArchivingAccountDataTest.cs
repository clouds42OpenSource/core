﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CalculateDaysToDeleteOrArchivingDatabase
{
    /// <summary>
    /// Проверка расчета количества дней до архивации данных неактивного аккаунта(не демо)
    /// Сценарий:
    ///         1) Создаем аккаунт с базой
    ///         2) Создаем платеж на произвольную сумму (избавляемся от статуса демо)
    ///         3) Изменяем дату окончания Аренды на -25 дней с текущей даты
    ///         4) Проверям, что количество дней до удаления рассчитываеится правильно
    /// </summary>
    public class CalculateDaysBeforeArchivingAccountDataTest : ScenarioBase
    {
        private readonly InactiveAccountNotificationDataHelper _inactiveAccountNotificationDataHelper;

        public CalculateDaysBeforeArchivingAccountDataTest()
        {
            _inactiveAccountNotificationDataHelper =
                TestContext.ServiceProvider.GetRequiredService<InactiveAccountNotificationDataHelper>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext),
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            var service = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>().GetSystemService(Clouds42Service.MyEnterprise);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Создаем платеж",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 100,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-25)
            });

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);

            if (resourcesConfiguration == null)
                throw new NotFoundException("Не удалось получить ресурс конфигурацию аккаунта");

            var beforeArchiveDbModel = new BeforeArchiveDbModelDto
            {
                AccountId = account.Id,
                Rent1CExpireDate = resourcesConfiguration.ExpireDate.Value
            };

            var archiveCount = _inactiveAccountNotificationDataHelper.GetDaysToArchiveCount(beforeArchiveDbModel, IsAccountDemo(account.Id));

            Assert.AreEqual(5, archiveCount, "Количество дней должно равняться 5");
        }

        private bool IsAccountDemo(Guid accountId)
        {
            var payments = DbLayer.PaymentRepository.Where(p =>
                p.AccountId == accountId).Count();
           
            return payments == 0;
        }
    }
}
