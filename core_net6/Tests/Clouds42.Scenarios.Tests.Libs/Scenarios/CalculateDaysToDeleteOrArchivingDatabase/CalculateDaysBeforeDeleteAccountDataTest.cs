﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CalculateDaysToDeleteOrArchivingDatabase
{
    /// <summary>
    /// Проверка рассчета количества дней до удаления данных неактивного демо аккаунта
    /// Сценарий:
    ///         1) Создаем аккаунт с базой
    ///         2) Изменяем дату окончания Аренды на -5 дней с текущей даты
    ///         3) Проверям, что количество дней до удаления рассчитываеится правильно
    /// </summary>
    public class CalculateDaysBeforeDeleteAccountDataTest : ScenarioBase
    {
        private readonly InactiveAccountNotificationDataHelper _inactiveAccountNotificationDataHelper;

        public CalculateDaysBeforeDeleteAccountDataTest()
        {
            _inactiveAccountNotificationDataHelper =
                TestContext.ServiceProvider.GetRequiredService<InactiveAccountNotificationDataHelper>();
        }

        public override void Run()
        {
            var accountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            accountDatabaseAndAccountCommand.Run();

            var account = accountDatabaseAndAccountCommand.AccountDetails.Account;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-5)
            });

            var service = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>().GetSystemService(Clouds42Service.MyEnterprise);
            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc => rc.BillingServiceId == service.Id);
            if (resourcesConfiguration == null)
                throw new NotFoundException("Не удалось получить ресурс конфигурацию аккаунта");

            var beforeArchiveDbModel = new BeforeArchiveDbModelDto
            {
                AccountId = account.Id,
                Rent1CExpireDate = resourcesConfiguration.ExpireDate.Value

            };

            var daysToDelete = _inactiveAccountNotificationDataHelper.GetDaysToArchiveCount(beforeArchiveDbModel, IsAccountDemo(account.Id));
            Assert.AreEqual(2, daysToDelete, "Количество дней должно равняться 2");
        }

        private bool IsAccountDemo(Guid accountId)
        {
            var payments = DbLayer.PaymentRepository.Where(p =>
                p.AccountId == accountId).Count();

            return payments == 0;
        }
    }
}
