﻿using System;
using System.Linq;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста когда надо получить аккаунт по счету
    /// </summary>
    public class ScenarioWhenYouNeedToGetInvoiceAccountTest : ScenarioBase
    {
        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            var firstCreateAccountCommand = new CreateAccountCommand(TestContext, createHelper.GenerateAccountRegistrationModel);
            firstCreateAccountCommand.Run();
            var secondCreateAccountCommand = new CreateAccountCommand(TestContext, createHelper.GenerateAccountRegistrationModel);
            secondCreateAccountCommand.Run();

            var createInvoiceCommand = new CreateInvoiceCommand(TestContext, firstCreateAccountCommand);
            createInvoiceCommand.Run();

            var invoicePref = CloudConfigurationProvider.Invoices.UniqPrefix();


            var result = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter
                {
                    OnlyMine = false,
                    SearchLine = $"{invoicePref + createInvoiceCommand.UniqKey}"
                }
            }).Result;

            if (result.Result.Records.Count != 1 && result.Result.Records.All(w => w.AccountId != firstCreateAccountCommand.AccountId))
                throw new InvalidOperationException("Выборка работает не корректно, выборка должна содержать только аккаунт по счету");
        }
    }
}
