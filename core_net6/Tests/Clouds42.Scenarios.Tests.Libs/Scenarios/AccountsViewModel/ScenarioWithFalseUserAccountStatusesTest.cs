﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Core42.Application.Features.AccountUsersContext.Queries;
using Core42.Application.Features.AccountUsersContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста c ложными статусами UserAccount при выборке
    /// </summary>
    public class ScenarioWithFalseUserAccountStatusesTest : ScenarioBase
    {
        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            var acc1 = createHelper.CreateAccountAndAccountUserOnConfigurationFields("Deleted");
            var acc2 = createHelper.CreateAccountAndAccountUserOnConfigurationFields("SyncDeleted");


            var result = Mediator.Send(new GetFilteredAccountUsersQuery{ Filter = new AccountUsersFilter{ AccountId = acc1.AccountId }}).Result;
            var result2 = Mediator.Send(new GetFilteredAccountUsersQuery{ Filter = new AccountUsersFilter{ AccountId = acc2.AccountId }}).Result;

            if (result.Result.Records.Count != 0) throw new InvalidOperationException("Нельзя включать в выборку userAccount со статусом Deleted и SyncDeleted");
            if (result2.Result.Records.Count != 0) throw new InvalidOperationException("Нельзя включать в выборку userAccount со статусом Deleted и SyncDeleted");
        }
    }
}
