﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста когда нужны только vip аккаунты
    /// </summary>
    public class ScenarioWhenNeededVipAccountsTest: ScenarioBase
    {
        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            createHelper.CreateAccountAndAccountUserOnConfigurationFields(null, true);
            createHelper.CreateAccountAndAccountUserOnConfigurationFields(null, false);
            createHelper.CreateAccountAndAccountUserOnConfigurationFields(null, false);

            var result = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter
                {
                    OnlyMine = false,
                    SearchLine = "VIP"
                }
            }).Result;

            if (result.Result.Records.Count != 1) throw new InvalidOperationException("Выбрка должна содержать только VIP аккаунты");
        }
    }
}
