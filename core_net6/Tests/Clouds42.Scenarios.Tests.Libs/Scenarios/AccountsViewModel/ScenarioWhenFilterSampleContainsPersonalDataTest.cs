﻿using System;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста когда фильтр выборки содержит персональные данные(Логин, Имя, Фамилия, Отчество, Почту, Номер телефона) 
    /// и делает выборку по этим данным
    /// </summary>
    public class ScenarioWhenFilterSampleContainsPersonalDataTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;

        public ScenarioWhenFilterSampleContainsPersonalDataTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            var login = $"testlog{DateTime.Now:hhmmssfff}";
            var email = $"na{Guid.NewGuid():N}@osmp.kg";

            var firstAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(login: login);
            var secondAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(firstName: "Никита");
            var thirdAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(lastName: "Авдейчик");
            var fourthAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(middleName: "Константинович");
            var fifthAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(email: email);
            var sixthAccount = createHelper.CreateAccountAndAccountUserOnAccUserDataFields(phoneNumber: "996554556080");
            var seventhAccount = createHelper.CreateAccountAndAccountUserOnAccountDataFields(null, "tp23084uh");
            var eighthAccount = createHelper.CreateAccountAndAccountUserOnAccountDataFields(null, null, "1234567890");
            var falseCreateAccountCommand = new CreateAccountCommand(TestContext, createHelper.GenerateAccountRegistrationModel);
            falseCreateAccountCommand.Run();
            
            GetAccountsPaginationListDtoAndCheckResult(login, firstAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("Никита", secondAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("Авдейчик", thirdAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("Константинович", fourthAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult(email, fifthAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("996554556080", sixthAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("tp23084uh", seventhAccount.AccountId);
            GetAccountsPaginationListDtoAndCheckResult("1234567890", eighthAccount.AccountId);

            AddUsersToAccount(firstAccount.AccountId);

            var result = Mediator.Send(new GetFilteredAccountsQuery { Filter = new AccountFilter { OnlyMine = false } }).Result;

            if ((from accountDetails in result.Result.Records let userId = DbLayer.AccountUsersRepository.GetAccountUserByLogin(accountDetails.AccountAdminLogin).Id let accountsAccountAdmins = DbLayer.AccountsRepository.GetAccountAdminIds(accountDetails.AccountId) where accountsAccountAdmins.Count > 0 && !accountsAccountAdmins.Contains(userId) select userId).Any())
            {
                throw new InvalidOperationException("Ошибка выборки. Метод GetAccontsInfoByFilter() упускает из виду пользователей AccountAdmin. ");
            }
        }

        /// <summary>
        /// Получить модель отображения аккаунтов и проверить результат
        /// </summary>
        /// <param name="searchQuery">Строка поиска</param>
        /// <param name="accountId">ID аккаунта</param>
        private void GetAccountsPaginationListDtoAndCheckResult(string searchQuery, Guid accountId)
        {
            var result = Mediator.Send(new GetFilteredAccountsQuery { Filter = new AccountFilter { OnlyMine = false, SearchLine = searchQuery} }).Result;
            if (result.Result.Records.Count != 1 &&
                result.Result.Records.All(acDet => acDet.AccountId != accountId))
                throw new InvalidOperationException("Ошибка выборки по фильтру. Выборка должна содержать только 1 аккаунт");
        }

        /// <summary>
        /// Добавить пользователей в аккаунт
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void AddUsersToAccount(Guid accountId)
        {
            for (var i = 0; i < 15; i++)
            {
                var result = _accountUsersProfileManager.AddToAccount(new AccountUserRegistrationToAccountTest
                {
                    AccountId = accountId,
                    AccountIdString = SimpleIdHash.GetHashById(DbLayer.AccountsRepository.FirstOrDefault().Id),
                    Login = "UserTestLogin009" + i,
                    Email = "UserTestEmail009" + i + "@efsol.ru",
                    FirstName = "UserTestName009"
                }).Result;

                if (result.Error)
                    throw new InvalidOperationException($"Ошибка создания нового пользователя. Причина: {result.Message}");
            }
        }
    }
}
