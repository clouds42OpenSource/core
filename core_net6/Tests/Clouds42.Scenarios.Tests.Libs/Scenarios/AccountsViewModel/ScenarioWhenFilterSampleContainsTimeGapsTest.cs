﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста когда фильтр выборки содержит временной интервал
    /// </summary>
    public class ScenarioWhenFilterSampleContainsTimeGapsTest : ScenarioBase
    {
        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            createHelper.CreateAccountAndAccountUserOnAccountDataFields(DateTime.Now.AddDays(-1));
            createHelper.CreateAccountAndAccountUserOnAccountDataFields(DateTime.Now.AddDays(-5));

            var result = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter
                {
                    OnlyMine = false, RegisteredFrom = DateTime.Now.AddDays(-2), RegisteredTo = DateTime.Now
                }
            }).Result;

            if (result.Result.Records.Count != 1) throw new InvalidOperationException("Результат выборки должен содержать 1 аккаунт");
        }
    }
}
