﻿using System;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountsViewModel
{
    /// <summary>
    /// Сценарий теста когда нужны аккаунты только для менеджера продаж
    /// </summary>
    public class ScenarioWhenYouNeedAccountsOnlyForSaleManagerTest : ScenarioBase
    {
        public override void Run()
        {
            var createHelper = new CreateAccountTestHelper(DbLayer, TestContext);

            var createAccountCommand = createHelper.CreateAccountAndAccountUserOnConfigurationFields(null, null, [AccountUserGroup.AccountSaleManager], true);

            DbLayer.AccountUserRoleRepository.Insert( new AccountUserRole
            {
                Id = Guid.NewGuid(),
                AccountUserId = createAccountCommand.AccountAdminId,
                AccountUserGroup = AccountUserGroup.AccountSaleManager
            });
            DbLayer.Save();

            createHelper.CreateAccountAndAccountUserOnConfigurationFields();

            var result = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter
                {
                    OnlyMine = true
                }
            }).Result;

            if (result.Result.Records.Count != 1)
                throw new InvalidOperationException($"Выбрка должна содержать только аккаунты менеджера продаж. Текущее количество аккаунтов для выборки 1, метод вернул {result.Result.Records.Count}");
        }
    }
}
