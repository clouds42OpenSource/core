﻿using System;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    ///     Попытка миграции с проверкой на доступность хранилища
    /// </summary>
    public class MigrateAccountWithCheckingPermissionTest : ScenarioBase
    {
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;

        public MigrateAccountWithCheckingPermissionTest()
        {
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData);
            createSegmentCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            if (!Directory.Exists(defaultStorage))
                Directory.CreateDirectory(defaultStorage);

            DirectoryPermissionHelperTest.AddDirectorySecurity(defaultStorage, @"Администратор",
                FileSystemRights.FullControl, AccessControlType.Deny);

            try
            {
                _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
                {
                    AccountId = createAccountCommand.AccountId,
                    InitiatorId = createAccountCommand.AccountAdminId,
                    SegmentToId = createSegmentCommand.Id,
                    SegmentFromId = createAccountCommand.Segment.Id,
                    MigrationStartDateTime = DateTime.Now
                });
            }
            catch (Exception e)
            {
                if (e is UnauthorizedAccessException)
                    return;

                throw;
            }

            DirectoryPermissionHelperTest.RemoveDirectorySecurity(defaultStorage, @"Администратор",
                FileSystemRights.FullControl, AccessControlType.Deny);

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            RefreshDbCashContext(TestContext.Context.AccountConfigurations.ToList());

            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            if (account == null || GetAccountSegmentId(account.Id) != createSegmentCommand.Id)
                throw new InvalidOperationException("Не удалось смигрировать аккаунт на заданый сегмент");

            if (account.AccountDatabases.ToList().Any(db =>
                !DirectoryHelper.DirectoryHas1CDatabaseFile(_accountDatabasePathHelper.GetPath(db))))
                throw new InvalidOperationException("При миграции физически пропал файл базы");

            if (Directory.Exists(defaultStorage))
                Directory.Delete(defaultStorage, true);
        }
    }
}
