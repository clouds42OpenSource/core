﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    /// Тестовый сценарий миграции одной базы
    /// Сценарий:
    /// 1) Создаём аккаунт и базу данных
    /// 2) Добавляем в сегмент файловое хранилище №2
    /// 3) Мигрируем базу и хр. №1 в хр. №2
    /// 4) Проверяем, что база переместилась и она в статусе Ready
    /// 5) На прежнем месте базы создаём одноимённую папку со случайными файлами
    /// 6) Пытаемся переместить базу с хр. №2 в хр. №1
    /// 7) Убеждаемся, что перемещение не прошло, все файлы в прежнем состоянии,
    ///     база в статусе Ready
    /// </summary>
    public class MigrateAccountDatabaseTest : ScenarioBase
    {
        private readonly IAccountDatabaseMigrationManager _accountDatabaseMigrationManager;
        
        public MigrateAccountDatabaseTest()
        {
            _accountDatabaseMigrationManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseMigrationManager>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;
            
            var cloudFilestorageElementViewModelTest = new CloudFilestorageElementViewModelTest("")
            {
                Name = "Хранилище №2",
                Description = "New New Desc",
                ConnectionAddress = "Адрес"
            };
            
            var cloudServicesFileStorageServersCommand = new CreateCloudServicesFileStorageServersCommand(TestContext, cloudFilestorageElementViewModelTest);
            cloudServicesFileStorageServersCommand.Run();
            
            var cloudServicesSegmentStorage = new CloudServicesSegmentStorage
            {
                ID = Guid.NewGuid(),
                SegmentID = createAccountDatabaseAndAccountCommand.AccountDetails.Segment.Id,
                FileStorageID = cloudServicesFileStorageServersCommand.Id,
                IsDefault = false
            };
            
            DbLayer.CloudServicesSegmentStorageRepository.Insert(cloudServicesSegmentStorage);
            DbLayer.Save();

            var databasesIds = new List<Guid>
            {
                accountDatabaseId
            };

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(a =>
                a.AccountId == account.Id);
            
            var migrationResult = _accountDatabaseMigrationManager.MigrateAccountDatabases(databasesIds,
                cloudServicesFileStorageServersCommand.Id, accountUser.Id);

            Assert.IsFalse(migrationResult.Error, migrationResult.Message);

            var database = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
            
            Assert.AreEqual(cloudServicesFileStorageServersCommand.Id, database.FileStorageID,
                $"Id Файлового хранилища базы должен быть {cloudServicesFileStorageServersCommand.Id}");

            database = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
            Assert.AreEqual("Ready", database.State, "Статус базы должен быть Ready");
        }
    }
}


