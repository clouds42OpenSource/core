﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    ///     Сценарий миграции аккаунта с серверными базами (1 опубликована, вторая нет)
    /// 
    ///     Предусловия: для аккаунта test_account создаем 2 серверные базы 
    ///     одну опубликовывае
    /// 
    ///     Действия: мигрируем аккаунт на другой сегмент
    /// 
    ///     Проверка: аккаунт и база  должены смигрировать на новый сегмент,
    ///     не опубликованная серверная база должна остаться не опубликованной
    /// </summary>
    public class MigrateAccountWithPublishServerAccountDatabases : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;

        public MigrateAccountWithPublishServerAccountDatabases()
        {
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var platform = DbLayer.PlatformVersionReferencesRepository.Where(pv => pv.Version.StartsWith("8.3")).Last();

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData,
                v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            var createServerPublishDbCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createServerPublishDbCommand.Run();
            var createServerDbCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createServerDbCommand.Run();

            if (!Directory.Exists(defaultStorage))
                Directory.CreateDirectory(defaultStorage);

            var databases = DbLayer.DatabasesRepository.Where(db => db.AccountId == createAccountCommand.AccountId)
                .ToList();

            if (databases.Count < 2)
                throw new InvalidOperationException("Ошибка создания двух баз");

            var serverPublish = databases.FirstOrDefault(d => d.Id == createServerPublishDbCommand.AccountDatabaseId);
            if (serverPublish == null)
                throw new InvalidOperationException("Ошибка создания базы");
            serverPublish.IsFile = false;
            serverPublish.PublishStateEnum = PublishState.Published;
            DbLayer.DatabasesRepository.Update(serverPublish);
            DbLayer.Save();

            ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            publishManager.PublishDatabase(
                new PublishDto {AccountDatabaseId = serverPublish.Id});

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            RefreshDbCashContext(Context.AccountConfigurations.ToList());
            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            if (account == null || GetAccountSegmentId(account.Id) != createSegmentCommand.Id)
                throw new InvalidOperationException("Не удалось смигрировать аккаунт на заданый сегмент");

            var serverPublishDb =
                DbLayer.DatabasesRepository.FirstOrDefault(i => i.Id == createServerPublishDbCommand.AccountDatabaseId);

            Assert.IsNotNull(serverPublishDb);
            Assert.AreEqual(serverPublishDb.PublishStateEnum, PublishState.Published);

            var serverDb =
                DbLayer.DatabasesRepository.FirstOrDefault(i => i.Id == createServerDbCommand.AccountDatabaseId);

            Assert.IsNotNull(serverDb);
            Assert.AreEqual(serverDb.PublishStateEnum, PublishState.Unpublished);

            if (Directory.Exists(defaultStorage))
                Directory.Delete(defaultStorage, true);
        }
    }
}
