﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    /// Сценарий миграции аккаунта в сегмент с другим сервером публикаций
    /// Действия:
    ///     1) Создаем аккаунт с опубликованными базами
    ///     2) Создаем сегмент с другим сервером публикаций
    ///     3) Мигрируем аккаунт в новый сегмент
    ///     4) Проверяем что миграция выполнена. Базы переехали в новое хранилище и опубликованы.
    /// </summary>
    public class ScenarioMigrateAccountToSegmentWithDifferentContentServer : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly string _defaultStoragePath;

        public ScenarioMigrateAccountToSegmentWithDifferentContentServer()
        {
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _defaultStoragePath = Path.Combine(TestContext.TestDataPath, "NewStorage");
        }

        public override void Run()
        {
            #region Создание аккаунта и опубликованных баз

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            CreatePublishedAccountDatabases(createAccountCommand);

            #endregion

            #region Создание сегмента с другим сервером публикаций

            var segment = CreateSegment();

            #endregion

            #region Смена сегмента для аккаунта

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = segment.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            #endregion

            #region Проверка после смены сегмента аккаунта

            RefreshDbCashContext(Context.AccountConfigurations.ToList());
            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(account);
            Assert.IsTrue(GetAccountSegmentId(account.Id) == segment.Id,
                "Не удалось смигрировать аккаунт на заданый сегмент");

            var accountDatabases = GetAccountDatabases(createAccountCommand.AccountId);
            Assert.IsTrue(accountDatabases.Any());
            accountDatabases.ForEach(accountDatabase =>
            {
                var accountDatabasePath = _accountDatabasePathHelper.GetPath(accountDatabase);

                Assert.IsTrue(DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath));
                Assert.IsTrue(accountDatabase.PublishStateEnum == PublishState.Published);
            });

            if (Directory.Exists(_defaultStoragePath))
                Directory.Delete(_defaultStoragePath, true);

            #endregion
        }

        /// <summary>
        /// Создать сегмент
        /// </summary>
        /// <returns>Созданный сегмент</returns>
        private CreateSegmentCommand CreateSegment()
        {
            var platform = DbLayer.PlatformVersionReferencesRepository.Where(pv => pv.Version.StartsWith("8.3")).Last();

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(_defaultStoragePath);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData,
                v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            if (!Directory.Exists(_defaultStoragePath))
                Directory.CreateDirectory(_defaultStoragePath);

            return createSegmentCommand;
        }

        /// <summary>
        /// Создать опубликованные базы аккаунта
        /// </summary>
        /// <param name="accountDetails">Данные аккаунта</param>
        private void CreatePublishedAccountDatabases(IAccountDetails accountDetails)
        {
            var createFirstAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetails);
            createFirstAccountDatabaseCommand.Run();
            var createSecondAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetails);
            createSecondAccountDatabaseCommand.Run();

            var accountDatabases = GetAccountDatabases(accountDetails.AccountId);
            Assert.IsFalse(accountDatabases.Count < 2, "Ошибка создания двух баз");

            ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            accountDatabases.ForEach(accountDatabase =>
            {
                publishManager.PublishDatabase(
                    new PublishDto {AccountDatabaseId = accountDatabase.Id});
            });
        }

        /// <summary>
        /// Получить инф. базы аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список инф. баз аккаунта</returns>
        private List<Domain.DataModels.AccountDatabase> GetAccountDatabases(Guid accountId)
            => DbLayer.DatabasesRepository.Where(db => db.AccountId == accountId).ToList();
    }
}
