﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Clouds42.Configurations.Configurations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Segment;
using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    public class IncorrectDeletionOfDirectoryTest : ScenarioBase
    {
        /// <summary>
        /// Сценарий удаления директории с открытым файлом
        /// 
        /// Предусловия: создаем директорию, добавляем файл который используется
        /// 
        /// Действия: удаляем директорию
        /// 
        /// Проверка: директория не удалилась после 200 попыток и выдало сообщение
        /// </summary>
        private readonly string nameDirectory = "TestDirectory";
        public override void Run()
        {
            var migrationTasksAggregator = ServiceProvider.GetRequiredService<IMigrationTasksAggregator>();

            //создаем директорию которую в дальнейшем надо будет удалить
            var clientFileFolder = Path.Combine(CloudConfigurationProvider.Files.FileUploadPath(), nameDirectory);
            var resFileFullPath = Path.Combine(clientFileFolder, "TestFile.txt");
            if (!Directory.Exists(clientFileFolder))
                Directory.CreateDirectory(clientFileFolder);

            if (!File.Exists(clientFileFolder))
                File.WriteAllText(resFileFullPath, "Тестирование");

            File.OpenWrite(resFileFullPath);
            //создаем таски на удаление
            var rollBack = new List<RollbackDataDto>();
            var accountDatabase = new List<Domain.DataModels.AccountDatabase>();
            var tasks = migrationTasksAggregator.CreateDeletionOfDirectoryTasks(rollBack, accountDatabase, clientFileFolder,
                true);
            //ждем выполнение таски
            try
            {
                // Wait for all the tasks to finish.
                Task.WaitAll(tasks.ToArray());
                throw new ArgumentException($"Сценарий удаления директории выполнился без ошибки, директория удалена {clientFileFolder}");
            }
            catch (AggregateException)
            {
                
                //ignore
            }


            Assert.IsNotNull(tasks);
            Assert.AreEqual(1, tasks.Count);


            if (!Directory.Exists(clientFileFolder))
                throw new ArgumentException($"Сценарий удаления директории выполнился без ошибки, директория удалена {clientFileFolder}");
        }
    }
}
