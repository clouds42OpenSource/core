﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.DataContracts.Account;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    /// Сценарий миграции аккаунта в сегмент с меньшей версией платформы
    /// Действия:
    ///     1) Создаем аккаунт с файловыми инф. базами
    ///     2) Создаем сегмент с меньшей версией платформы
    ///     3) Мигрируем аккаунт в новый сегмент
    ///     4) Проверяем что сегмент не сменился
    /// </summary>
    public class MigrateAccountToSegmentWithLowerPlatformVersion : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;

        public MigrateAccountToSegmentWithLowerPlatformVersion()
        {
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();

            var currentAccountSegment =
                DbLayer.CloudServicesSegmentRepository.FirstOrDefault(
                    segm => segm.ID == createAccountCommand.Segment.Id);

            Assert.IsNotNull(currentAccountSegment);

            var platform = DbLayer.PlatformVersionReferencesRepository.Where(pv => pv.Version.StartsWith("8.3"))
                .First();

            var createSegmentCommand = new CreateSegmentCommand(TestContext, v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(account);
            Assert.AreEqual(GetAccountSegmentId(account.Id), currentAccountSegment.ID);
        }
    }
}
