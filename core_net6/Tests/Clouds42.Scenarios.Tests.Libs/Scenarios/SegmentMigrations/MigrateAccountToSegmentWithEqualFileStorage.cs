﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    /// Сценарий миграции аккаунта в сегмент с одинаковыми файловыми хранилищами
    ///     Действия:
    ///     1) Создаем аккаунт с файловыми инф. базами
    ///     2) Создаем сегмент с одинаковыми файловыми хранилищами
    ///     3) Мигрируем аккаунт в новый сегмент
    ///     4) Проверяем что сегмент сменился, а путь к базам не изменился
    /// </summary>
    public class MigrateAccountToSegmentWithEqualFileStorage : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public MigrateAccountToSegmentWithEqualFileStorage()
        {
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            
        }

        public override async Task RunAsync()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();

            var currentAccountSegment = GetAccountSegment(createAccountCommand.AccountId);

            Assert.IsNotNull(currentAccountSegment);

            var platform = DbLayer.PlatformVersionReferencesRepository
                .AsQueryableNoTracking() 
                .Where(x => x.Version.Contains("8.3"))
                .OrderByDescending(x => x.Version)
                .FirstOrDefault();

            var createSegmentCommand = new CreateSegmentCommand(TestContext,
                v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            var segmentManager = ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();

            var managerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createSegmentCommand.Id });
            Assert.IsFalse(managerResult.Error);
            Assert.IsNotNull(managerResult.Result);

            var editSegmentModel = managerResult.Result.MapToEditCloudServicesSegmentDto();
            editSegmentModel.SegmentFileStorageServers.Add(new SegmentElementDto
            {
                Id = currentAccountSegment.CloudServicesFileStorageServer.ID,
                Name = currentAccountSegment.CloudServicesFileStorageServer.Name
            });

            var editSegmentResult = segmentManager.EditSegment(editSegmentModel);
            Assert.IsFalse(editSegmentResult.Error);

            var accountDatabases = await DbLayer.DatabasesRepository
                .AsQueryableNoTracking()
                .Where(db => db.AccountId == createAccountCommand.AccountId)
                .ToListAsync();

            var initialAcDbPathList = GetAccountDatabasesPathList(accountDatabases);

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = currentAccountSegment.ID,
                MigrationStartDateTime = DateTime.Now
            });

            RefreshDbCashContext(Context.AccountConfigurations);
            currentAccountSegment = GetAccountSegment(createAccountCommand.AccountId);

            Assert.IsNotNull(currentAccountSegment);
            Assert.AreEqual(currentAccountSegment.ID, createSegmentCommand.Id);

            var acDbPathListAfterMigration = GetAccountDatabasesPathList(accountDatabases);
            acDbPathListAfterMigration.ToList().ForEach(acDb =>
            {
                var oldPath = initialAcDbPathList.FirstOrDefault(initialState => initialState.Key == acDb.Key);

                Assert.IsTrue(
                    DirectoryHelper.DirectoryHas1CDatabaseFile(
                        _accountDatabasePathHelper.GetPath(acDb.Key)));

                Assert.AreEqual(oldPath.Value, acDb.Value);
            });
        }

        /// <summary>
        /// Получить список путей к инф. базам аккаунта
        /// </summary>
        /// <param name="accountDatabases">Список инф. баз аккаунта</param>
        /// <returns>Список путей к инф. базам аккаунта</returns>
        private Dictionary<Guid, string> GetAccountDatabasesPathList(List<Domain.DataModels.AccountDatabase> accountDatabases)
        {
            var databasesPathList = new Dictionary<Guid, string>();

            accountDatabases.ForEach(accountDatabase =>
            {
                var accountDatabasePath = _accountDatabasePathHelper.GetPath(accountDatabase);
                databasesPathList.Add(accountDatabase.Id, accountDatabasePath);
            });

            return databasesPathList;
        }
    }
}
