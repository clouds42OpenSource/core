﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    ///     Попытка миграции аккаунта с проверкой на доступность хранилища
    /// </summary>
    /// <summary>
    ///     Сценарий миграции аккаунта с базами в разных статусах
    /// 
    ///     Предусловия: для аккаунта test_account создаем базы с разными статусами
    ///     (Ready, ErrorCreate, TransferArchive)
    /// 
    ///     Действия: мигрируем аккаунт на другой сегмент
    /// 
    ///     Проверка: аккаунт и база в статусе Ready должны смигрировать на новый сегмент,
    ///     оставшиеся базы статус не должны поменять
    /// </summary>
    public class MigrateAccountWithAccountDatabasesTest : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public MigrateAccountWithAccountDatabasesTest()
        {
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var platformVersion = "8.4.14.228";
            var platform =
                DbLayer.PlatformVersionReferencesRepository.FirstOrDefault(w => w.Version == platformVersion);

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData,
                v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();
            createAccountDatabaseCommand.Run();

            if (!Directory.Exists(defaultStorage))
                Directory.CreateDirectory(defaultStorage);

            var databases = DbLayer.DatabasesRepository.Where(db => db.AccountId == createAccountCommand.AccountId)
                .ToList();

            if (databases.Count < 3)
                throw new InvalidOperationException("Ошибка создания трех баз");

            databases[0].StateEnum = DatabaseState.ErrorCreate;
            DbLayer.DatabasesRepository.Update(databases[0]);
            databases[1].StateEnum = DatabaseState.TransferArchive;
            DbLayer.DatabasesRepository.Update(databases[1]);

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            RefreshDbCashContext(Context.AccountConfigurations.ToList());
            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            if (account == null || GetAccountSegmentId(account.Id) != createSegmentCommand.Id)
                throw new InvalidOperationException("Не удалось смигрировать аккаунт на заданый сегмент");

            foreach (var db in account.AccountDatabases.ToList())
            {
                if (db.StateEnum == DatabaseState.Ready)
                {
                    if (!DirectoryHelper.DirectoryHas1CDatabaseFile(_accountDatabasePathHelper.GetPath(db)))
                        throw new InvalidOperationException("При миграции физически пропал файл базы");

                    continue;
                }

                if (db.StateEnum != DatabaseState.ErrorCreate && db.StateEnum != DatabaseState.TransferArchive)
                    throw new InvalidOperationException("При миграции базы в не в статусе Ready поменяли свой статус");
            }

            if (Directory.Exists(defaultStorage))
                Directory.Delete(defaultStorage, true);
        }
    }
}
