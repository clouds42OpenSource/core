﻿using System;
using System.Collections.Generic;
using System.IO;
using Clouds42.Configurations.Configurations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Segment;
using Clouds42.AccountDatabase.Contracts.Migration.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    public class CorrectDeletionOfDirectoryTest : ScenarioBase
    {
        /// <summary>
        /// Сценарий удаления директории 
        /// 
        /// Предусловия: создаем директорию пустую
        /// 
        /// Действия: удаляем директорию
        /// 
        /// Проверка: директория удалилась
        /// </summary>
        private readonly string userLogin = "TestDirectory";
        public override void Run()
        {
            var migrationTasksAggregator = ServiceProvider.GetRequiredService<IMigrationTasksAggregator>();

            //создаем директорию которую в дальнейшем надо будет удалить
            var clientFileFolder = Path.Combine(CloudConfigurationProvider.Files.FileUploadPath(), userLogin);

            if (!Directory.Exists(clientFileFolder))
                Directory.CreateDirectory(clientFileFolder);

            //создаем таски на удаление
            var rollBack = new List<RollbackDataDto>();
            var accountDatabase = new List<Domain.DataModels.AccountDatabase>();
            var tasks = migrationTasksAggregator.CreateDeletionOfDirectoryTasks(rollBack, accountDatabase, clientFileFolder,
                true);

            //ждем выполнение таски
            migrationTasksAggregator.WaitForCompletionOfTasks(tasks, out var messageText);


            Assert.IsNotNull(tasks);
            Assert.AreEqual(1, tasks.Count);
            Assert.IsNull(messageText);

            if (Directory.Exists(clientFileFolder))
                throw new ArgumentException($"Сценарий удаления директории выполнился с ошибкой, директория не удалена {clientFileFolder}");
        }
    }
}
