﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SegmentMigrations
{
    /// <summary>
    ///     Сценарий миграции аккаунта без серверных баз 
    ///     Действия: мигрируем аккаунт на другой сегмент
    /// 
    ///     Проверка: аккаунт и база  должены смигрировать на новый сегмент     
    /// </summary>
    public class MigrateAccountWithoutUsingDbTest : ScenarioBase
    {
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;

        public MigrateAccountWithoutUsingDbTest()
        {
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");

            var platformVersion = "8.4.14.228";

            var platform =
                DbLayer.PlatformVersionReferencesRepository.FirstOrDefault(w => w.Version == platformVersion);

            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData,
                v83PlatformVersion: platform.Version);
            createSegmentCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();


            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = createAccountCommand.AccountId,
                InitiatorId = createAccountCommand.AccountAdminId,
                SegmentToId = createSegmentCommand.Id,
                SegmentFromId = createAccountCommand.Segment.Id,
                MigrationStartDateTime = DateTime.Now
            });

            RefreshDbCashContext(TestContext.Context.AccountConfigurations.ToList());

            var account = DbLayer.AccountsRepository
                .FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            if (account == null || GetAccountSegmentId(account.Id) != createSegmentCommand.Id)
                throw new InvalidOperationException ("Не удалось смигрировать аккаунт на заданый сегмент");

            if (account.AccountDatabases.ToList().Any(db =>
                !DirectoryHelper.DirectoryHas1CDatabaseFile(_accountDatabasePathHelper.GetPath(db))))
                throw new InvalidOperationException("При миграции физически пропал файл базы");
        }
    }
}
