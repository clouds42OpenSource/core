﻿using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке отклонения заявки на редактирование сервиса,
    /// при редактировании услуг сервиса
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем услуги сервиса и отправляем заявку на модерацию
    /// 3) Отклоняем заявку и проверяем статус сервиса и заявки
    /// 4) Проверяем что изменения не применились
    /// </summary>
    public class ScenarioCheckRejectEditServiceRequestWhenEditingServiceTypesTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(createCustomService.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);
            Assert.AreEqual(billingServiceCardDto.Result.BillingServiceStatus, BillingServiceStatusEnum.IsActive);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            var serviceTypeThree = editBillingServiceDto.BillingServiceTypes[2];
            serviceTypeOne.Name = "NewName";
            serviceTypeTwo.Description = "NewDescription";
            serviceTypeThree.ServiceTypeCost = 10;

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);

            Assert.IsFalse(editBillingServiceManagerResult.Error);

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);

            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Rejected
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.Rejected);

            Assert.IsNotNull(service);
            Assert.IsNotNull(changeServiceRequest);

            var findServiceType = billingServiceTypes.FirstOrDefault(st => st.Id == serviceTypeOne.Id);
            Assert.IsNotNull(findServiceType);
            Assert.AreNotEqual(findServiceType.Name, serviceTypeOne.Name);

            findServiceType = billingServiceTypes.FirstOrDefault(st => st.Id == serviceTypeTwo.Id);
            Assert.IsNotNull(findServiceType);
            Assert.AreNotEqual(findServiceType.Description, serviceTypeTwo.Description);

            findServiceType = billingServiceTypes.FirstOrDefault(st => st.Id == serviceTypeThree.Id);
            Assert.IsNotNull(findServiceType);
            Assert.AreNotEqual(GetRateForServiceTypeOrThrowException(findServiceType.Id).Cost,
                serviceTypeThree.ServiceTypeCost);
        }
    }
}
