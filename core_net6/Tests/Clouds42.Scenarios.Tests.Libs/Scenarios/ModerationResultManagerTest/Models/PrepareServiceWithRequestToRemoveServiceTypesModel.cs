﻿using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Models
{
    /// <summary>
    /// Модель для подготовки сервиса с завякой на удаление услуги
    /// </summary>
    public class PrepareServiceWithRequestToRemoveServiceTypeModel
    {
        /// <summary>
        /// Id севриса
        /// </summary>
        public Guid ServcieId { get; set; }

        /// <summary>
        /// Id удаленной услуги
        /// </summary>
        public Guid DeletedServiceTypeId { get; set; }
    }
}
