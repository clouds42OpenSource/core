﻿using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
    /// при редактировании услуг сервиса (Название, Описание)
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем услуги сервиса(Название, Описание) и отправляем заявку на модерацию
    /// 3) Подтверждаем заявку и проверяем поля услуг сервиса
    /// 4) Проверяем что по стоимости не было никаких изменений
    /// </summary>
    public class ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypesTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.IsActive);
            Assert.AreEqual(changeServiceRequest.Status, ChangeRequestStatusEnum.Implemented);

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);
            Assert.AreEqual(billingServiceCardDto.Result.BillingServiceStatus, BillingServiceStatusEnum.IsActive);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            serviceTypeOne.Name = "TestNewName";
            serviceTypeTwo.Description = "TestNewDescription";

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.Implemented);
            Assert.IsNotNull(changeServiceRequest);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);
            Assert.IsNotNull(service);

            var serviceTypes = service.BillingServiceTypes.ToList();

            serviceTypes.ToList().ForEach(st =>
            {
                Assert.IsNull(DbLayer.AccountRateRepository.FirstOrDefault(ar => ar.BillingServiceTypeId == st.Id));
            });

            Assert.IsNull(DbLayer.GetGenericRepository<RecalculationServiceCostAfterChange>().FirstOrDefault());
            Assert.IsNotNull(serviceTypes.FirstOrDefault(st =>
                st.Id == serviceTypeOne.Id && st.Name == serviceTypeOne.Name &&
                st.Description == serviceTypeOne.Description));

            Assert.IsNotNull(serviceTypes.FirstOrDefault(st =>
                st.Id == serviceTypeTwo.Id && st.Name == serviceTypeTwo.Name &&
                st.Description == serviceTypeTwo.Description));
        }
    }
}
