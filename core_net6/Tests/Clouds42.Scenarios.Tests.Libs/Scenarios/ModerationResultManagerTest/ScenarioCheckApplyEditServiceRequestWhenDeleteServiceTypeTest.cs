﻿using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.CoreWorker.AccountJobs.BillingServiceTypes;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
    /// при удалении услуги сервиса
    ///
    /// 1) Создаем сервис и услуги #CreateService
    /// 2) Удаляем услугу севриса #RemoveServiceType
    /// 3) Выполняем модерацию сервиса #Moderation
    /// 4) Проверяем что после модерации выставляется флаг IsDeleted
    /// и создается задача на обработку удаленных услуг #CheckEditServiceRequest
    /// </summary>
    public class ScenarioCheckApplyEditServiceRequestWhenDeleteServiceTypeTest : ScenarioBase
    {
        public override void Run()
        {
            var serviceTypeHelper =
                TestContext.ServiceProvider.GetRequiredService<PrepareServiceWithRequestToRemoveServiceTypeHelperTest>();

            #region CreateService
            var prepareServiceModel = serviceTypeHelper.CreateService(TestContext);
            #endregion

            #region RemoveServiceType
           serviceTypeHelper.RemoveServiceType(prepareServiceModel);
            #endregion

            #region Moderation
            var editServiceRequest = serviceTypeHelper.ProcessModerationResult(TestContext, prepareServiceModel);
            #endregion

            #region CheckEditServiceRequest
            Assert.IsFalse(editServiceRequest.BillingService.BillingServiceTypes.Any(st =>
                st.IsDeleted && st.Key == prepareServiceModel.DeletedServiceTypeId));

            Assert.IsFalse(DbLayer.BillingServiceTypeRelationRepository.Any(st =>
                st.IsDeleted && st.MainServiceTypeId == prepareServiceModel.DeletedServiceTypeId));

            var jobTypeName = typeof(ProcessDeletedServiceTypesForAccountsJob).Name;

            var coreWorkerTask =
                DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(task =>
                    task.CoreWorkerTask.TaskName == jobTypeName);

            var taskParams = coreWorkerTask.TaskParameter.TaskParams
                .DeserializeFromJson<ProcessDeletedServiceTypesForAccountsDto>();

            Assert.AreEqual(taskParams.ServiceId, editServiceRequest.BillingServiceId);
            Assert.AreEqual(taskParams.BillingServiceChangesId, editServiceRequest.BillingServiceChangesId);
            Assert.AreEqual(taskParams.ServiceTypeIds.First(), prepareServiceModel.DeletedServiceTypeId);
            #endregion
        }
    }
}
