﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста проверки процесса по отклонению заявки
    /// на создание сервиса
    /// 1) Создаем сервис со статусом "На модерации" и отправляем заявку на создание сервиса
    /// 2) Отклоняем заявку и проверяем поля
    /// Ожидаем что сервис будет со статусом "Черновик"
    /// </summary>
    public class ScenarioRejectCreateServiceRequestTest : ScenarioBase
    {
        public override void Run()
        {
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                }, false);

            createCustomService.Run();

            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Rejected
            };

            moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.Draft);
            Assert.AreEqual(changeServiceRequest.Status, ChangeRequestStatusEnum.Rejected);
            Assert.IsNotNull(changeServiceRequest.ModeratorId);
            Assert.IsNotNull(service.StatusDateTime);
        }
    }
}
