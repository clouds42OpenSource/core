﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста процесса по отклонению заявки
    /// на изменения сервиса
    /// 1) Создаем сервис со статусом "На модерации" и отправляем заявку
    /// 2) Не указываем "Комментарий модератора", отклоняем заявку и ожидаем ошибку
    /// 3) Отклоняем заявку и проверяем поля
    /// 4) Повторно отклоняем заявку и ожидаем ошибку
    /// 5) Обновляем статус заявки в базе на "На модерации", подтверждаем заявку и проверяем поля
    /// 6) Редактируем сервис и создаем новую заявку "На модерации"
    /// 7) Отклоняем заявку и проверяем поля
    /// </summary>
    public class ScenarioRejectChangeServiceRequestProcessTest : ScenarioBase
    {
        public override void Run()
        {
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                }, false);

            createCustomService.Run();

            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();
            var editBillingServiceManager = ServiceProvider.GetRequiredService<EditBillingServiceManager>();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                Status = ChangeRequestStatusEnum.Rejected
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsTrue(managerResult.Error);

            changeServiceRequestModerationResultDto.ModeratorComment = "тестирование";
            moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.Draft);
            Assert.AreEqual(changeServiceRequest.Status, ChangeRequestStatusEnum.Rejected);

            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsTrue(managerResult.Error);

            changeServiceRequest.Status = ChangeRequestStatusEnum.OnModeration;
            DbLayer.GetGenericRepository<ChangeServiceRequest>().Update(changeServiceRequest);
            DbLayer.Save();

            changeServiceRequestModerationResultDto.Status = ChangeRequestStatusEnum.Implemented;
            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);
            Assert.AreEqual(billingServiceCardDto.Result.BillingServiceStatus, BillingServiceStatusEnum.IsActive);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            var oldName = editBillingServiceDto.Name;
            editBillingServiceDto.Name = "NewName";
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            
            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);

            Assert.IsFalse(editBillingServiceManagerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);


            changeServiceRequestModerationResultDto.Id = changeServiceRequest.Id;
            changeServiceRequestModerationResultDto.Status = ChangeRequestStatusEnum.Rejected;
            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.Rejected);

            Assert.IsNotNull(service);
            Assert.IsNotNull(changeServiceRequest);
            Assert.AreEqual(service.Name, oldName);
        }
    }
}
