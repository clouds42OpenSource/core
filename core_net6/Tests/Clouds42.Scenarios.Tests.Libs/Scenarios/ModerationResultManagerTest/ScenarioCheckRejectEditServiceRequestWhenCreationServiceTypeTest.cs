﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке отклонения заявки на редактирование сервиса,
    /// при создании новой услуги сервиса
    ///
    /// 1) Создаем кастомный сервис м проверяем что сервис создался и активен #CreateService
    /// 2) Редактируем сервис добавляя новую услугу #EditService
    /// 3) Выполняем модерацию заявки #Moderation
    /// 4) Проверяем что изменения не записались в базу #CheckServiceType
    /// </summary>
    public class ScenarioCheckRejectEditServiceRequestWhenCreationServiceTypeTest :ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            #region CreateService
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(createCustomService.Id);
            Assert.IsNotNull(billingServiceCardDto);
            #endregion

            #region EditService
            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var newServiceType = createOrEditBillingServiceHelper.GenerateBillingServiceType("NewServiceTypeName");
            newServiceType.IsNew = true;
            editBillingServiceDto.BillingServiceTypes.Add(newServiceType);

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);
            #endregion

            #region Moderation
            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Rejected
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);
            #endregion

            #region CheckServiceType
            Assert.IsNull(DbLayer.BillingServiceTypeRepository.FirstOrDefault(st => st.Id == newServiceType.Id));
            #endregion
        }
    }
}
