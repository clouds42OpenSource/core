﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста процесса
    /// по принятию заявки на создание сервиса
    /// 1) Создаем сервис со статусом "На модерации" и отправляем заявку
    /// 2) Принимаем заявку и проверяем поля
    /// 3) Повторно принимаем заявку и ожидаем ошибку
    /// </summary>
    public class ScenarioApplyCreateServiceRequestProcessTest : ScenarioBase
    {
        public override void Run()
        {
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                }, false);

            createCustomService.Run();

            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.OnModeration);
            Assert.AreEqual(changeServiceRequest.Status, ChangeRequestStatusEnum.OnModeration);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsTrue(managerResult.Error);

            changeServiceRequestModerationResultDto.ModeratorComment = "тестирование";
            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id && csr.Status == ChangeRequestStatusEnum.Implemented);
            service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);

            Assert.IsNotNull(changeServiceRequest);
            Assert.IsNotNull(service);

            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsTrue(managerResult.Error);
        }
    }
}
