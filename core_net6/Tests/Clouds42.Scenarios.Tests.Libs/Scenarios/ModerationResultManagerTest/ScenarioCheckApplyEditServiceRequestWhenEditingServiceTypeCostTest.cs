﻿using System;
using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
    /// при редактировании стоимости услуг сервиса
    ///
    /// 1) Создаем активный сервис
    /// 2) Создаем два аккаунта и подключаем им сервис. Для одного аккаунта делаем Аренду просроченной.
    /// 3) Редактируем стоимость услуг сервиса и отправляем заявку на модерацию
    /// 4) Подтверждаем заявку и проверяем:
    ///     1) Тариф по измененным услугам изменен
    ///     2) Для аккаунта у которого активна Аренда 1С по измененным услугам создался тариф на аккаунт(старая стоимость)
    ///     3) Проверяем что создалось 2 записи "Пересчет стоимости сервиса".  
    /// </summary>
    public class ScenarioCheckApplyEditServiceRequestWhenEditingServiceTypeCostTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = changeServiceRequest.BillingServiceId
                    }
                });
            createAccountCommand.Run();

            var createSecondAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = changeServiceRequest.BillingServiceId
                    }
                });
            createSecondAccountCommand.Run();

            var resConfRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(resconf =>
                resconf.AccountId == createSecondAccountCommand.AccountId &&
                resconf.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resConfRent1C.ExpireDate = DateTime.Now.AddDays(-30);
            DbLayer.ResourceConfigurationRepository.Update(resConfRent1C);
            DbLayer.Save();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(createCustomService.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();

            var oldCostServiceTypes = editBillingServiceDto.BillingServiceTypes.Sum(st => st.ServiceTypeCost);

            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            var serviceTypeThree = editBillingServiceDto.BillingServiceTypes[2];

            var serviceTypeOneOldCost = serviceTypeOne.ServiceTypeCost;
            var serviceTypeTwoOldCost = serviceTypeTwo.ServiceTypeCost;
            serviceTypeOne.ServiceTypeCost = 20;
            serviceTypeTwo.ServiceTypeCost = 10;

            var newCostServiceTypes = editBillingServiceDto.BillingServiceTypes.Sum(st => st.ServiceTypeCost);

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            var editServiceRequest =
                DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.Implemented);
            Assert.IsNotNull(editServiceRequest);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);
            Assert.IsNotNull(service);

            var serviceTypes = service.BillingServiceTypes.ToList();

            var findServiceType = serviceTypes.FirstOrDefault(st => st.Id == serviceTypeOne.Id);
            Assert.IsNotNull(findServiceType);
            var rate = GetRateForServiceTypeOrThrowException(serviceTypeOne.Id);
            Assert.AreEqual(rate.Cost, serviceTypeOne.ServiceTypeCost);
            var firstAccountRate = GetAccountRate(serviceTypeOne.Id, createAccountCommand.AccountId);
            var secondAccountRate = GetAccountRate(serviceTypeOne.Id, createSecondAccountCommand.AccountId);
            Assert.IsNotNull(firstAccountRate);
            Assert.IsNull(secondAccountRate);
            Assert.AreEqual(firstAccountRate.Cost, serviceTypeOneOldCost);

            findServiceType = serviceTypes.FirstOrDefault(st => st.Id == serviceTypeTwo.Id);
            Assert.IsNotNull(findServiceType);
            rate = GetRateForServiceTypeOrThrowException(serviceTypeTwo.Id);
            Assert.AreEqual(rate.Cost, serviceTypeTwo.ServiceTypeCost);
            firstAccountRate = GetAccountRate(serviceTypeTwo.Id, createAccountCommand.AccountId);
            secondAccountRate = GetAccountRate(serviceTypeTwo.Id, createSecondAccountCommand.AccountId);
            Assert.IsNotNull(firstAccountRate);
            Assert.IsNull(secondAccountRate);
            Assert.AreEqual(firstAccountRate.Cost, serviceTypeTwoOldCost);

            findServiceType = serviceTypes.FirstOrDefault(st => st.Id == serviceTypeThree.Id);
            Assert.IsNotNull(findServiceType);
            rate = GetRateForServiceTypeOrThrowException(serviceTypeThree.Id);
            Assert.AreEqual(rate.Cost, serviceTypeThree.ServiceTypeCost);
            Assert.IsNull(
                DbLayer.AccountRateRepository.FirstOrDefault(ar => ar.BillingServiceTypeId == serviceTypeThree.Id));

            var recalculationServiceCostAfterChanges = DbLayer
                .GetGenericRepository<RecalculationServiceCostAfterChange>().Where(rs =>
                    rs.BillingServiceChangesId == editServiceRequest.BillingServiceChangesId).ToList();

            Assert.AreEqual(2, recalculationServiceCostAfterChanges.Count);

            var recalculationServiceCostAfterChange = recalculationServiceCostAfterChanges.FirstOrDefault(rsc =>
                rsc.CoreWorkerTasksQueue.DateTimeDelayOperation?.Month == DateTime.Now.Month);

            Assert.IsNotNull(recalculationServiceCostAfterChange);
            Assert.AreEqual(recalculationServiceCostAfterChange.OldServiceCost, oldCostServiceTypes);
            Assert.AreEqual(recalculationServiceCostAfterChange.NewServiceCost, newCostServiceTypes);
        }
    }
}
