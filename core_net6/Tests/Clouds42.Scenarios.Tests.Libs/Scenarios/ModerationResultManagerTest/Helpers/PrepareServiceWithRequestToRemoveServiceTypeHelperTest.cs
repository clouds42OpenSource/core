﻿using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers
{
    /// <summary>
    /// Хелпер для подготовки сервиса с заявкой на удаление услуг(Для тестов)
    /// </summary>
    public class PrepareServiceWithRequestToRemoveServiceTypeHelperTest(
        CreateOrEditBillingServiceHelper createOrEditBillingServiceHelper,
        BillingServiceCardDataManager billingServiceCardDataManager,
        EditBillingServiceManager editBillingServiceManager,
        ModerationResultManager moderationResultManager)
    {
        /// <summary>
        /// Создать сервис и выбрать услугу для удаления
        /// </summary>
        /// <param name="testContext">Тестовый контекст</param>
        /// <returns>Модель для подготовки сервиса с завякой на удаление услуги</returns>
        public PrepareServiceWithRequestToRemoveServiceTypeModel CreateService(ITestContext testContext)
        {
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var serviceTypeToDelete = billingServiceTypes[0];
            serviceTypeToDelete.ServiceTypeRelations.Add(billingServiceTypes[0].Id);

            var createCustomService = new CreateBillingServiceCommand(testContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(testContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var service = testContext.DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);

            return new PrepareServiceWithRequestToRemoveServiceTypeModel
            {
                ServcieId = service.Id,
                DeletedServiceTypeId = serviceTypeToDelete.Key
            };
        }

        /// <summary>
        /// Удалить услугу
        /// </summary>
        /// <param name="model">Модель для подготовки сервиса с завякой на удаление услуги</param>
        public void RemoveServiceType(PrepareServiceWithRequestToRemoveServiceTypeModel model)
        {
            var billingServiceCardDto = billingServiceCardDataManager.GetData(model.ServcieId);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var serviceType =
                editBillingServiceDto.BillingServiceTypes.FirstOrDefault(st => st.Key == model.DeletedServiceTypeId);
            editBillingServiceDto.BillingServiceTypes.Remove(serviceType);

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);
        }

        /// <summary>
        /// Выполнить модерацию сервиса
        /// </summary>
        /// <param name="testContext">Тестовый контекст</param>
        /// <param name="model">Модель для подготовки сервиса с завякой на удаление услуги</param>
        /// <returns>Заявка редактирования сервиса на модерацию</returns>
        public EditServiceRequest ProcessModerationResult(ITestContext testContext,
            PrepareServiceWithRequestToRemoveServiceTypeModel model)
        {
            var changeServiceRequest =
                testContext.DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == model.ServcieId &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            var editServiceRequest =
                testContext.DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == model.ServcieId &&
                    csr.Status == ChangeRequestStatusEnum.Implemented);
            Assert.IsNotNull(editServiceRequest);

            return editServiceRequest;
        }


    }
}
