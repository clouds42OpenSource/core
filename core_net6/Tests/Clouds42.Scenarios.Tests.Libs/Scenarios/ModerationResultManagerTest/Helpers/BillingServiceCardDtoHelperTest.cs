﻿using System;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers
{
    /// <summary>
    /// Класс хелпер для работы с моделью
    /// карточки сервиса биллинга
    /// </summary>
    public static class BillingServiceCardDtoHelperTest
    {
        /// <summary>
        /// Выполнить маппинг
        /// к модели редактирования сервиса
        /// </summary>
        /// <param name="billingServiceCardDto">Модель карточки сервиса биллинга</param>
        /// <returns>Модель редактирования сервиса</returns>
        public static EditBillingServiceDto
            MapToEditBillingServiceDto(this BillingServiceCardDto billingServiceCardDto)
        {
            return new EditBillingServiceDto
            {
                Id = billingServiceCardDto.Id,
                AccountId = billingServiceCardDto.AccountId,
                Name = billingServiceCardDto.Name,
                Key = billingServiceCardDto.Key,
                BillingServiceTypes = billingServiceCardDto.BillingServiceTypes,
                BillingServiceStatus = billingServiceCardDto.BillingServiceStatus,
                CreateServiceRequestId = Guid.NewGuid()
            };
        }

        /// <summary>
        /// Выполнить маппинг к модели EditBillingServiceDraftDto
        /// </summary>
        /// <param name="billingServiceCardDto"> Модель карточки сервиса биллинга </param>
        /// <returns>Модель редактирования сервиса в статусе Draft</returns>
        public static EditBillingServiceDraftDto MapToBillingServiceDraftDto(this BillingServiceCardDto billingServiceCardDto)
        {
            return new EditBillingServiceDraftDto
            {
                Id = billingServiceCardDto.Id,
                AccountId = billingServiceCardDto.AccountId,
                Name = billingServiceCardDto.Name,
                Key = billingServiceCardDto.Key,
                BillingServiceTypes = billingServiceCardDto.BillingServiceTypes,
                BillingServiceStatus = billingServiceCardDto.BillingServiceStatus
            };
        }

        /// <summary>
        /// Выполнить маппинг
        /// к модели создания сервиса
        /// </summary>
        /// <param name="billingServiceCardDto">Модель карточки сервиса биллинга</param>
        /// <returns>Модель создания сервиса</returns>
        public static CreateBillingServiceDto MapToCreateBillingServiceDto(
            this BillingServiceCardDto billingServiceCardDto)
        {
            return new CreateBillingServiceDto
            {
                AccountId = billingServiceCardDto.AccountId,
                Name = billingServiceCardDto.Name,
                Key = billingServiceCardDto.Key,
                BillingServiceTypes = billingServiceCardDto.BillingServiceTypes,
                BillingServiceStatus = billingServiceCardDto.BillingServiceStatus
            };
        }
    }
}
