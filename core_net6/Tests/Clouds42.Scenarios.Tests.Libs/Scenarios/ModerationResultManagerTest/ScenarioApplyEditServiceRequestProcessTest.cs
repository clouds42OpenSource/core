﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста процесса
    /// по принятию заявки на редактирование сервиса
    /// 1) Создаем активный сервис
    /// 2) Редактируем сервис и отправляем заявку на модерацию
    /// 3) Подтверждаем заявку и проверяем поля отредактированного сервиса
    /// 4) Повторно подтверждаем заявку и ожидаем ошибку
    /// </summary>
    public class ScenarioApplyEditServiceRequestProcessTest : ScenarioBase
    {
        public override void Run()
        {
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();
            var editBillingServiceManager = ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
            TestContext.ServiceProvider.GetRequiredService<IAgentFileStorageProvider>();

            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == createCustomService.Id);
            Assert.IsNotNull(changeServiceRequest);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.IsActive);
            Assert.AreEqual(changeServiceRequest.Status, ChangeRequestStatusEnum.Implemented);

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);
            Assert.AreEqual(billingServiceCardDto.Result.BillingServiceStatus, BillingServiceStatusEnum.IsActive);

            var dbTemplates = DbLayer.DbTemplateRepository.WhereLazy().Select(w => w.Id).ToList();

            if (!dbTemplates.Any())
                throw new InvalidOperationException ("Не заполнен справочник шаблоны");

            var dbTemplateDelimitersOne =
                testDataGenerator.GenerateDbTemplateDelimiters(dbTemplates[0], "ut");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersOne);

            DbLayer.Save();

            var createIndustryCommand = new CreateIndustryCommand(TestContext,
                new IndustryModelTest {Name = "Новая отрасльNew", Description = "TEST"});
            createIndustryCommand.Run();

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewName";

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(changeServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.Implemented);
            Assert.IsNotNull(changeServiceRequest);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);
            Assert.IsNotNull(service);

            var resultOfComparing =
                createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingModelAndService(editBillingServiceDto, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);

            managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsTrue(managerResult.Error);
        }
    }
}
