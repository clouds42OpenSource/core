﻿using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest
{
    /// <summary>
    /// Сценарий теста по проверке принятия заявки на редактирование сервиса,
    /// при создании новой услуги сервиса
    ///
    /// 1) Создаем кастомный сервис м проверяем что сервис создался и активен #CreateService
    /// 2) Редактируем сервис добавляя новую услугу #EditService
    /// 3) Проверяем что создалась заявка и новая услуга в таблице изменений услуг #CheckBillingServiceTypeChange
    /// 4) Выполняем модерацию заявки #Moderation
    /// 5) Проверяем что новая услуга и тариф для услуги добавлены в базу #CheckServiceType
    /// </summary>
    public class ScenarioCheckApplyEditServiceRequestWhenCreationServiceTypeTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            #region CreateService
            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(createCustomService.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);
            Assert.AreEqual(billingServiceCardDto.Result.BillingServiceStatus, BillingServiceStatusEnum.IsActive);
            #endregion

            #region EditService
            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var newServiceType = createOrEditBillingServiceHelper.GenerateBillingServiceType("NewServiceTypeName");
            newServiceType.IsNew = true;
            editBillingServiceDto.BillingServiceTypes.Add(newServiceType);

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);
            #endregion

            #region CheckBillingServiceTypeChange
            var changeServiceRequest =
                DbLayer.GetGenericRepository<ChangeServiceRequest>().FirstOrDefault(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);

            Assert.IsNotNull(changeServiceRequest);

            var billingServiceTypeChange = DbLayer.GetGenericRepository<BillingServiceTypeChange>()
                .FirstOrDefault(st => st.Name == newServiceType.Name);

            Assert.IsNotNull(billingServiceTypeChange);
            #endregion

            #region Moderation
            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = changeServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);
            #endregion

            #region CheckServiceType
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                s.SystemService == null && s.BillingServiceStatus == BillingServiceStatusEnum.IsActive);

            Assert.IsNotNull(service);

            var findServiceType = service.BillingServiceTypes.FirstOrDefault(st => st.Id == billingServiceTypeChange.Id);
            Assert.IsNotNull(findServiceType);

            var rate = GetRateForServiceTypeOrThrowException(findServiceType.Id);

            Assert.AreEqual(rate.Cost,
                newServiceType.ServiceTypeCost);
            #endregion
        }
    }
}
