﻿using System;
using System.Collections.Generic;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Billing.Providers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProvidedServices
{
    /// <summary>
    /// Проверка редактирования даты окончания услуги при редактировании
    /// даты окончания Аренды
    /// Сценарий:
    /// 1) Создаём акаунт без услуг аренды
    /// 2) Покупаем ему Web
    /// 3) Сдвигаем окончания аренды на неделю вперёд
    /// Проверяем : дата окончания в таблице оказаных услуг
    /// также изменилась соответственно аренде 
    /// </summary>
    public class ScenarioChangingProvidedServiceExpireDate : ScenarioBase
    {
        public override void Run()
        {
            var createAcc  = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyDisk
                }
            });

            createAcc.Run();
            var account = createAcc.Account;
            var adminId = createAcc.AccountAdminId;

            Rent1CConfigurationAccessManager rentManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var nowDate = DateTime.Now;

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAcc.AccountId,
                Date = nowDate,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 700,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            DbLayer.ResourceConfigurationRepository.Insert(new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = account.Id,
                ExpireDate = nowDate.AddMonths(1),
                CreateDate = nowDate,
                BillingServiceId = billingService.Id
            });
            DbLayer.Save();

            var accessList = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = adminId,
                    StandartResource = false,
                    WebResource = true,
                    SponsorAccountUser = false,
                }
            };

            rentManager.ConfigureAccesses(account.Id, accessList);
            RefreshDbCashContext(TestContext.Context.ProvidedServices);

            var providedService =
                DbLayer.ProvidedServiceRepository.FirstOrDefault(p => p.Service == Clouds42Service.MyEnterprise.ToString());

            Assert.AreEqual(providedService.DateTo.Value.Date, nowDate.AddMonths(1).Date,"Неверная дата окончания услуги");

            var futureExpDate = nowDate.AddMonths(1).AddDays(7);
            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();
            var newRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = futureExpDate
            };

            rent1CManager.ManageRent1C(account.Id, newRent1C);
            RefreshDbCashContext(TestContext.Context.ProvidedServices);

            providedService =
                DbLayer.ProvidedServiceRepository.FirstOrDefault(p => p.Service == Clouds42Service.MyEnterprise.ToString());

            Assert.AreEqual(providedService.DateTo.Value.Date, futureExpDate.Date, "Неверная дата окончания услуги");
        }
    }
}
