﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProvidedServices
{
    ///<summary>
    /// Действия:
    /// Создаём аккаунт и докупаем ему год лицензии и 1000 страниц
    /// Проверяем:
    /// 1) Создались записи в таблице ProvidedServices
    /// 2) Проверяем поля записей на корректность
    /// </summary>
    public class ScenarioCheckProvidedServicesAfterBuyingPagesWithLicenseTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);

            var account = createAccountCommand.Account;
            new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);

            var resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 18000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var res = resourcesManager.BuyEsdlAndRec42(1000, 1);

            if (string.IsNullOrEmpty(res.Comment) && !res.Complete)
                throw new InvalidOperationException("Произошел сбой при покупке лицензий.");

            if (!string.IsNullOrEmpty(res.Comment) && !res.Complete)
                throw new InvalidOperationException($"{res.Comment}");

            RefreshDbCashContext(TestContext.Context.FlowResourcesScope);
            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var recog = DbLayer.ProvidedServiceRepository.Where(p => p.Service == Clouds42Service.Recognition.ToString());
            Assert.IsTrue(recog.Count()==2,"После покупки должно быть две услуги рекогнишена");
            var secondRecog = recog.FirstOrDefault(r => r.Rate > 0);

            var recSer = TestContext.ServiceProvider.GetRequiredService<IRecognition42CloudService>().SetAccountId(account.Id);
            var recCost = recSer.GetCost(1000);
            
            Assert.AreEqual(secondRecog.Rate, recCost, "Ошибка в стоимости");
            Assert.IsNull(secondRecog.DateTo, "Для рекогнишена не проставляеться дата");

            var esdlSer = TestContext.ServiceProvider.GetRequiredService<IEsdlCloud42Service>().SetAccountId(account.Id); 
            var esdlCost = esdlSer.GetCost(1);
            var esdl = DbLayer.ProvidedServiceRepository.Where(p => p.Service == Clouds42Service.Esdl.ToString());
            Assert.IsTrue(esdl.Count() == 2, "После покупки должно быть две услуги esdl");
            var secondEsdl = esdl.FirstOrDefault(r => r.Rate > 0);
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();

            Assert.AreEqual(secondEsdl.Rate, esdlCost, "Ошибка в стоимости");
            Assert.AreEqual(secondEsdl.DateTo.Value.Date, DateTime.Now.AddDays(demoDaysCount).AddMonths(12).Date,
                "Неверная дата, надо сейчас +13мес");

        }
    }
}
