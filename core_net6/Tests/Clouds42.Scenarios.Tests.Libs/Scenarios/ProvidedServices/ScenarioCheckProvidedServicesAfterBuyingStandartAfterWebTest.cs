﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProvidedServices
{
    /// <summary>
    /// Проверка обновления записи оказанной услуги WEB на Standart
    /// после покупки RDP
    /// Сценарий:
    /// 1) Создаём аккаунт и второго пользователя
    /// 2) Кладём деньги и покупаем второму WEB
    /// 3) Проверяем ProvidedServices
    /// 4) Докупаем стандарт
    /// 5) Проверяем ProvidedServices. 
    /// </summary>
    public class ScenarioCheckProvidedServicesAfterBuyingStandartAfterWebTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();
            var account = createAccountCommand.Account;


            #region secondUser
            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            var secondUserId = res.Result;
            RefreshDbCashContext(TestContext.Context.AccountUsers);

            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, 
                ClientDescription = "GetTokenByLogin", 
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]", 
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();
            
            #endregion secondUser
            #region lave
            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 1500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            #endregion lave

            Rent1CConfigurationAccessManager rentManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var accessList = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = secondUserId,
                    StandartResource = false,
                    WebResource = true,
                    SponsorAccountUser = false
                }
            };

            rentManager.ConfigureAccesses(account.Id, accessList);

            RefreshDbCashContext(TestContext.Context.ProvidedServices);
            RefreshDbCashContext(TestContext.Context.Resources);

            var webProvServise =
                DbLayer.ProvidedServiceRepository.FirstOrDefault(p => p.ServiceType == ResourceType.MyEntUserWeb.ToString());
            var webResource = DbLayer.ResourceRepository.FirstOrDefault(w =>
                w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb);

            Assert.AreEqual(700,webProvServise.Rate, "Неверная стоимость");
            Assert.AreEqual(webProvServise.DateTo.Value.Date, DateTime.Now.AddDays(7).Date, "Неверная дата окончания.");

            accessList =
            [
                new()
                {
                    AccountUserId = secondUserId,
                    StandartResource = true,
                    WebResource = false,
                    WebResourceId = webResource.Id,
                    SponsorAccountUser = false
                }
            ];
            rentManager.ConfigureAccesses(account.Id, accessList);
            RefreshDbCashContext(TestContext.Context.ProvidedServices);

            var rentProvServises =
                DbLayer.ProvidedServiceRepository.Where(p => p.Service == Clouds42Service.MyEnterprise.ToString());
            Assert.IsTrue(rentProvServises.Count() == 3, "Должно создастся 3 записи");
            var rentProvSer = rentProvServises.Where(p => p.ServiceType== ResourceType.MyEntUser.ToString()).ToList();
            Assert.IsTrue(rentProvSer.Count == 2, "Должно создастся 2 записи");
            var webProvSer = rentProvServises.Where(p => p.ServiceType == ResourceType.MyEntUserWeb.ToString());
            Assert.IsTrue(webProvSer.Count() == 1, "Должна создастся 1 запись");
            Assert.IsNotNull(rentProvSer,"Нет услуги стандарта");
        }
    }
}
