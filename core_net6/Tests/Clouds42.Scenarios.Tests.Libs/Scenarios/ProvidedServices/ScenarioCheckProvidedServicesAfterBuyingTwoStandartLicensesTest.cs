﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProvidedServices
{
    /// <summary>
    /// Проверка корректности записи в таблицу ProvidedServices
    /// когда мы подключаем одновременно двух юзеров к аренде стандарт
    /// Сценарий:
    /// 1) Создаём аккаунт без аренды
    /// 2) Создаём второго юзера
    /// 3) Подключаем обеих юзеров к стандарту
    /// Проверяем корректность записи
    /// </summary>
    public class ScenarioCheckProvidedServicesAfterBuyingTwoStandartLicensesTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyDisk
                }
            });

            createAccountCommand.Run();
            var account = createAccountCommand.Account;
            var adminId = createAccountCommand.AccountAdminId;
            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = account.Id,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            var secondUserId = res.Result;
            RefreshDbCashContext(TestContext.Context.AccountUsers);
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin", 
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 3000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            Rent1CConfigurationAccessManager rentManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var accessList = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = adminId,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                },
                new()
                {
                    AccountUserId = secondUserId,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                }
            };
            DbLayer.ResourceConfigurationRepository.Insert(new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = account.Id,
                ExpireDate = DateTime.Now.AddMonths(1),
                CreateDate = DateTime.Now,
                BillingServiceId = billingService.Id
            });
            DbLayer.Save();

            rentManager.ConfigureAccesses(account.Id, accessList);

            RefreshDbCashContext(TestContext.Context.ProvidedServices);

            var rentProvServises =
                DbLayer.ProvidedServiceRepository.Where(p => p.Service == Clouds42Service.MyEnterprise.ToString());

            Assert.IsTrue(rentProvServises.Count()==1, "Должна создастся одна запись");
            var rentProvSer = rentProvServises.FirstOrDefault(p => p.Rate > 0);

            Assert.AreEqual(1500,rentProvSer.Rate, "Неверная стоимость");
            Assert.AreEqual(rentProvSer.ServiceType, ResourceType.MyEntUser.ToString(),"Указан неверный тип услуги.");
            Assert.AreEqual(2,rentProvSer.Count,"Ошибка количества");
            Assert.AreEqual(rentProvSer.DateFrom.Value.Date,DateTime.Now.Date,"Ошибка даты с");
            Assert.AreEqual(rentProvSer.DateTo.Value.Date,DateTime.Now.AddMonths(1).Date,"Ошибка даты по");

        }
    }
}
