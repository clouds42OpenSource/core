﻿using System;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CheckYookassaAgregatorPaymentStatusManagerTest
{
    /// <summary>
    /// Сценарий теста для проверки изменения статуса платежа,
    /// для успешного платежа
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Создаем платеж Юкассы #CreateYookassaPayment
    ///
    /// 3) Регистрируем фейковый провайдер который возвращает объект платежа ЮKassa
    /// со статусом Success #RegisterGetSuccessYookassaPaymentObjectInfoProviderFake
    ///
    /// 4) Проверяем изменение статуса платежа #CheckPaymentStatusChange
    /// 
    /// 5) Проверяем, что способ оплаты сохранился
    /// 
    /// </summary>
    public class ScenarioCheckYookassaPaymentStatusChangeForSuccessPaymentAndSavedPaymentMethodTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            

            var createYookassaPaymentCommand =
                new CreateYookassaPaymentCommand(TestContext, createAccountCommand.AccountId);
            createYookassaPaymentCommand.Run();
            
            var savedPaymentMethod = GetFirstSavedPaymentMethodByAccountId(createAccountCommand.AccountId);

            Assert.IsNull(savedPaymentMethod);

            var managerResult = ServiceProvider.GetRequiredService<IFinalizeYookassaAggregatorPaymentManager>().FinalizePayment(
                new Guid(createYookassaPaymentCommand.Payment.PaymentSystemTransactionId));
            Assert.IsFalse(managerResult.Error);

            savedPaymentMethod = GetFirstSavedPaymentMethodByAccountId(createAccountCommand.AccountId);
            Assert.IsNotNull(savedPaymentMethod);

            #endregion
        }
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IGetYookassaPaymentObjectInfoProvider, GetSuccessYookassaPaymentObjectInfoProviderFake>();
            serviceCollection.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProviderFake>();
        }

    }
}
