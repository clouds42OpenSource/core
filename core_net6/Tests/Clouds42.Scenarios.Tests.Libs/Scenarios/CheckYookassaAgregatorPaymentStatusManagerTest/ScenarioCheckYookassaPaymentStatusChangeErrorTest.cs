﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CheckYookassaAgregatorPaymentStatusManagerTest
{
    /// <summary>
    /// Сценарий теста для не успешной проверки
    /// изменения статуса платежа агрегатора ЮKassa(не успешные кейсы)
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Создаем платеж Юкассы #CreateYookassaPayment
    ///
    /// 3) Проверяем изменение статуса платежа указывая не валидный Id платежа (ожидаем ошибку) #CheckPaymentStatusChangeWithInvalidPaymentId
    ///
    /// 4) Регистрируем фейковый провайдер который возвращает объект платежа ЮKassa
    /// со статусом Pending #RegisterGetPendingYookassaPaymentObjectInfoProviderFake
    ///
    /// 5) Проверяем изменение статуса платежа (ожидаем ошибку) #CheckPaymentStatusChangeForPendingPayment
    /// </summary>
    public class ScenarioCheckYookassaPaymentStatusChangeErrorTest : ScenarioBase
    {
        public override void Run()
        {

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var createYookassaAgregatorPaymentCommand =
                new CreateYookassaPaymentCommand(TestContext, createAccountCommand.AccountId);
            createYookassaAgregatorPaymentCommand.Run();
            
            var managerResult = ServiceProvider.GetRequiredService<IFinalizeYookassaAggregatorPaymentManager>()
                .FinalizePayment(Guid.NewGuid());
            Assert.IsTrue(managerResult.Error);
            
            managerResult = ServiceProvider.GetRequiredService<IFinalizeYookassaAggregatorPaymentManager>().FinalizePayment(
                new Guid(createYookassaAgregatorPaymentCommand.Payment.PaymentSystemTransactionId));
            Assert.IsTrue(managerResult.Error);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IGetYookassaPaymentObjectInfoProvider, GetPendingYookassaPaymentObjectInfoProviderFake>();
            serviceCollection.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProviderFake>();
        }
    }
}
