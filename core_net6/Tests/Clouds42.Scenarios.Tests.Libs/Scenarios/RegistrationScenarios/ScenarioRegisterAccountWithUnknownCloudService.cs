﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    /// Сценарий регистрации аккаунта когда пользователь выбрал "Перейти в личный кабинет"
    /// Действия:
    ///     1) Имитируем регистрацию аккаунта без активации аренды
    ///     2) Проверяем что пользователь попал только в 2 основные группы: Клиенты МК, company_number
    /// </summary>
    public class ScenarioRegisterAccountWithUnknownCloudService : ScenarioBase
    {
        public override void Run()
        {
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.Unknown
                }
            };

            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountUser = DbLayer.AccountUsersRepository.GetAccountUser(createAccountCommand.AccountAdminId);

            Assert.IsNotNull(account);
            Assert.IsNotNull(accountUser);
        }
    }
}
