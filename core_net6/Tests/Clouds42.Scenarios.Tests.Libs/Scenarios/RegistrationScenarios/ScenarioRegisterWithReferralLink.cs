﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    /// Сценарий для тестирования регистрации нового пользователя
    /// по реферральной ссылке, которая содержит в себе информацию
    /// о сервисе, у которого будет активирован демо период у нового пользователя.
    /// Тестируется запись в логировании о активированном демо периоде.
    /// 
    /// Действия:
    /// 1. CreateService - Зарегистрировать сервис
    /// 2. CreateRegistrationConfig - Сформировать модель регистрации
    /// 3. CreateAccountAndActivateService - Зарегистрировать аккаунт и активировать демопериод созданного на шаге 1 сервиса
    /// 4. TestServiceRegistration - Проверить запись зарегистрированного по реф.ссылке сервиса
    /// 5. TestAccountRegistration - Проверить регистрацию пользователя по сформированной модели регситрации на шаге 1
    /// 5. TestLogEntry - Проверить запись об активации демо периода в логировании
    /// 6. TestServiceConfiguration - Проверить конфигурацию ресурса
    /// </summary>
    public class ScenarioRegisterWithReferralLink : ScenarioBase
    {
        public override void Run()
        {
            #region CreateService
            Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>();
            Services.BuildServiceProvider();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp"]);
            var billingService = CreateService();

            #endregion

            #region CreateRegistrationConfig

            var registrationConfig = CreateRegistrationConfig(billingService.Id, billingService.AccountOwnerId);

            #endregion

            #region CreateAccountAndActivateService

            var createdAccount = CreateAccount(registrationConfig);

            #endregion

            #region GetDataFromDataBase

            var billingServiceInDataBase =
                TestContext.DbLayer.BillingServiceRepository.FirstOrDefault(service => service.Id == billingService.Id );
            
            var accountInDatabase =
                TestContext.DbLayer.AccountsRepository.FirstOrDefault(account => account.Id == createdAccount.Id);

            var logEntryInDatabase =
                (
                    from log in TestContext.DbLayer.CloudChangesRepository.WhereLazy()
                    join logAction in TestContext.DbLayer.CloudChangesActionRepository.WhereLazy() on log.Action equals logAction.Id
                    where log.Account.Id == accountInDatabase.Id &&
                          logAction.Index == (int)LogActions.ActivateServiceForExistingAccount
                    select log
                ).FirstOrDefault();

            var serviceConfigurationInDataBase =
                TestContext.DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                    config.BillingServiceId == billingServiceInDataBase.Id);

            #endregion

            #region TestServiceRegistration

            Assert.IsNotNull(billingServiceInDataBase, 
                "Не найден сервис в базе данных, сервис не создан");

            #endregion

            #region TestAccountRegistration

            Assert.IsNotNull(accountInDatabase,
                "Не найден аккаунт в базе данных, аккаунт не создан");

            #endregion

            #region TestServiceConfiguration

            Assert.IsNotNull(serviceConfigurationInDataBase,
                "Не найдена конфигурация сервиса в базе данных, сервис не создан");

            #endregion

            #region TestLogEntry

            Assert.IsNotNull(logEntryInDatabase,
                "Не найден лог активации демо периода, сервис не активирован");

            #endregion
        }

        #region CreateService

        /// <summary>
        /// Создать сервис биллинга
        /// </summary>
        /// <returns> Пустой сервис биллинга </returns>
        private BillingService CreateService()
        {
            var accountServiceOwner = new CreateAccountCommand(TestContext);
            accountServiceOwner.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var customService = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest =
                {
                    AccountId = accountServiceOwner.AccountId
                }
            };
            customService.Run();

            if (customService.Result.Error)
                throw new InvalidOperationException(customService.Result.Message);

            return TestContext.DbLayer.BillingServiceRepository.FirstOrThrowException(service => 
                service.Id == customService.Id,
                "Не найден сервис в базе данных, сервис не создан");
        }

        #endregion

        #region CreateRegistrationConfig

        /// <summary>
        /// Создать конфигурацию регистрации
        /// </summary>
        /// <param name="serviceId"> Id сервиса в реф.ссылке </param>
        /// <param name="referralAccountId"> Id владельца сервиса </param>
        /// <returns> Конфигурация регистрации </returns>
        private AccountRegistrationModelTest CreateRegistrationConfig(Guid? serviceId, Guid? referralAccountId)
        {
            return new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = serviceId,
                    ReferralAccountId = referralAccountId
                }
            };
        }

        #endregion

        #region CreateAccount

        /// <summary>
        /// Создать аккаунт
        /// </summary>
        /// <param name="config"> Конфигурация регистрации </param>
        /// <returns> Аккаунт </returns>
        private Account CreateAccount(AccountRegistrationModelTest config)
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, config);
            createAccountCommand.Run();

            return createAccountCommand.Account;
        }

        #endregion


    }
}
