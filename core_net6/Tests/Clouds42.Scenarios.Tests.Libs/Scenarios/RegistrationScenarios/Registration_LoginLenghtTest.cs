﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    ///     Сценарий регистрации аккаунта с различной длинной логина
    /// 
    ///     Минимальная длинна логина должна быть 3 символа, максимальная - 35
    /// </summary>
    public class RegistrationLoginLenghtTest : ScenarioBase
    {
        public override void Run()
        {
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                Login = $"{Guid.NewGuid().ToString().Replace("-", string.Empty)}{Guid.NewGuid().ToString().Replace("-", string.Empty)}"
            };

            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);
            var exceptionMessage = string.Empty;

            try
            {
                createAccountCommand.Run();
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }

            Assert.IsFalse(string.IsNullOrWhiteSpace(exceptionMessage));

            testRegistrationModel.Login = "bb";
            createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);
            exceptionMessage = null;

            try
            {
                createAccountCommand.Run();
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }

            Assert.IsFalse(string.IsNullOrWhiteSpace(exceptionMessage));

            testRegistrationModel.Login = Guid.NewGuid().ToString().Replace("-", string.Empty)[..20];
            testRegistrationModel.FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
            testRegistrationModel.Email = "suepasd@dasds.cas";

            createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();

            testRegistrationModel.Login = Guid.NewGuid().ToString().Replace("-", string.Empty)[..3];
            testRegistrationModel.FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
            testRegistrationModel.Email = "adasdas@dds.cas";

            createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();
        }
    }
}
