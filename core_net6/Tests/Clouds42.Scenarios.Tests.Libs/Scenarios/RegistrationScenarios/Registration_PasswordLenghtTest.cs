﻿using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    ///     Сценарий регистрации аккаунта с различной длинной пароля 
    ///     Минимальная длинна пароля должна быть 7 символов, максимальная - 25
    /// </summary>
    public class RegistrationPasswordLengthTest : ScenarioBase
    {
        private readonly IAlphaNumericsGenerator _alphaNumericsGenerator = new AlphaNumericsGenerator(new CryptoRandomGeneric());
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator = new(new CryptoRandomGeneric());

        public override void Run()
        {
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                Password = _alphaNumericsGenerator
                    .GetRandomString(50, 
                        AlphaNumericsTypes.LowerCaseAlpha, 
                        AlphaNumericsTypes.Numerics, 
                        AlphaNumericsTypes.SpecSymbols)
            };

            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);
            var exceptionMessage = string.Empty;

            try
            {
                createAccountCommand.Run();
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }

            Assert.IsNotNull(exceptionMessage);

            exceptionMessage = null;

            testRegistrationModel.Password =
                _alphaNumericsGenerator.GetRandomString(5, AlphaNumericsTypes.Numerics, AlphaNumericsTypes.SpecSymbols);

            createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);
            try
            {
                createAccountCommand.Run();
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }
            Assert.IsNotNull(exceptionMessage);

            testRegistrationModel.Password = _alphaNumeric42CloudsGenerator.GeneratePassword();
            createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);
            createAccountCommand.Run();

            Assert.IsNotNull(createAccountCommand.Account);
            Assert.IsNotNull(createAccountCommand.AccountAdminId);
        }
    }
}
