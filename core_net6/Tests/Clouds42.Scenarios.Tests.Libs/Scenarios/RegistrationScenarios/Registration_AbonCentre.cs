﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    ///     Сценарий регистрации аккаунта по внешней ссылке (Абонцентр)
    /// 
    ///     Предусловия: вызываем метод АПИ регистрации нового аккаунта. 
    ///  
    ///     Действия: имитирование действий регистрации
    /// 
    ///     Проверка: попытка регистрации успешна, тип у аккаунта соответствует -
    /// Абонцентр
    /// </summary>
    public class RegistrationAbonCenterTest : ScenarioBase
    {
        public override void Run()
        {
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    RegistrationSource = ExternalClient.AbonCenter
                }
            };

            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            Assert.IsNotNull(account);
            Assert.AreEqual(ExternalClient.AbonCenter.ToString(), GetAccountType(account.Id));
        }
    }
}