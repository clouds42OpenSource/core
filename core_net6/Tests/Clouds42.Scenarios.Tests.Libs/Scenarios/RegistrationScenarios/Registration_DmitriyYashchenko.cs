﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    ///     Сценарий регистрации аккаунта по внешней ссылке (DmitriyYashchenko)
    /// 
    ///     Предусловия: пользователь должен перейти
    /// по ссылке https://betacp.42clouds.com/signup?RegistrationSource=DmitriyYashchenko. 
    ///  
    ///     Действия: имитирование действий регистрации
    /// 
    ///     Проверка: попытка регистрации успешна, тип у аккаунта соответствует -
    /// ProfitAccount
    /// </summary>
    public class RegistrationDmitriyYashchenkoTest : ScenarioBase
    {
        public override void Run()
        {
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    RegistrationSource = ExternalClient.Yashchenko
                }
            };

            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            Assert.IsNotNull(account);
            Assert.AreEqual(ExternalClient.Yashchenko.ToString(), GetAccountType(account.Id));
        }
    }
}
