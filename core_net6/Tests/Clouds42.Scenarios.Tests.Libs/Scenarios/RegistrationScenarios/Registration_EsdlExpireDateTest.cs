﻿using System;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Core42.Application.Features.FastaContext.Queries;
using System.Threading.Tasks;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RegistrationScenarios
{
    /// <summary>
    ///     Сценарий регистрации аккаунта для проверки правильности периода демо лицензии для ЗД
    /// 
    ///     Действия: имитирование действий регистрации
    /// 
    ///     Проверка: попытка регистрации успешна, период действия демо лицензии для ЗД - 1 месяц
    /// </summary>
    public class RegistrationEsdlExpireDateTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();
            var testRegistrationModel = new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    RegistrationSource = ExternalClient.Promo
                }
            };

            Services.AddTransient<CreateAccountCommand>();
            var createAccountCommand = new CreateAccountCommand(TestContext, testRegistrationModel);

            createAccountCommand.Run();
            
            var account = createAccountCommand.Account;
            Assert.IsNotNull(account);

            var esdl = await Mediator.Send(new GetFastaInfoQuery(createAccountCommand.AccountId));
            if (esdl.Result.ExpireDate.Date != DateTime.Now.AddDays(demoDaysCount).Date)
                throw new InvalidOperationException($"Произошел сбой при регистрации и предоставлен не {demoDaysCount} дней демо периода.");
        }
    }
}
