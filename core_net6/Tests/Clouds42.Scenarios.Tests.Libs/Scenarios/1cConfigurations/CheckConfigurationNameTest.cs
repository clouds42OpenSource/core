﻿using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase._1C.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios._1cConfigurations
{
    /// <summary>
    /// Тест валидации названия конфигурации, чтобы не было повторений в базе
    /// Сценарий:
    ///         1. Выбираем названия всех конфигураций, проверяем, что валидация возвращает правильный результат
    ///         2. Создаем список несуществующих названий конфигураций, проверяем, что валидация возвращает правильный результат
    /// </summary>
    public class CheckConfigurationNameTest : ScenarioBase
    {
        private readonly Configurations1CDataManager _configurations1CDataManager;

        public CheckConfigurationNameTest()
        {
            _configurations1CDataManager = TestContext.ServiceProvider.GetRequiredService<Configurations1CDataManager>();
        }
        public override void Run()
        {
            var configurationsNames = DbLayer.Configurations1CRepository.Select(c => c.Name).ToList();

            CheckNamesOfConfigurations(configurationsNames, false);

            var notExistingConfigurations = new List<string>
            {
                "Бухгалтерия для Кыргызстана базовая, редакция 3",
                "Розница для Балтии",
                "Управление компанией для Балтии",
                "Управление торговлей для стран Балтии, редакция 11.0"
            };
            CheckNamesOfConfigurations(notExistingConfigurations, true);
        }

        /// <summary>
        /// Проверить существует 
        /// </summary>
        /// <param name="configurationsNames">Списк названий конфигураций</param>
        /// <param name="isConfigurationsNotExist">Флаг существует ли конфигурация</param>
        private void CheckNamesOfConfigurations(IEnumerable<string> configurationsNames, bool isConfigurationsNotExist)
        {
            foreach (var configurationsName in configurationsNames)
            {
                var managerResult = _configurations1CDataManager.CheckConfigurationNameNotExist(configurationsName);
                Assert.IsFalse(managerResult.Error);
                Assert.AreEqual(isConfigurationsNotExist, managerResult.Result);
            }
        }
    }
}
