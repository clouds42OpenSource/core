﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.AccountDatabase._1C.Managers;
using Clouds42.DataContracts.Configurations1c;
using Core42.Application.Features.Configuration1CContext.Command;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework.Legacy;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios._1cConfigurations
{
    /// <summary>
    /// Тест добавления/удаления/редактирования новой конфигурации 1С
    /// Сценарий:
    ///         1. Создаем новую конфигурацию 1С, проверяем, что создание прошло успешно
    ///         2. Редактируем конфигурацию 1С, проверяем, что редактирование прошло успешно
    ///         3. Удаляем конфигурацию 1С, проверяем, что удаление прошло успешно
    /// </summary>
    public class CrudConfigurations1CTest : ScenarioBase
    {
        private readonly Configurations1CDataManager _configurations1CDataManager;

        public CrudConfigurations1CTest()
        {
            _configurations1CDataManager = TestContext.ServiceProvider.GetRequiredService<Configurations1CDataManager>();
        }

        public override async Task RunAsync()
        {
            var configurationsVariations = new List<string>
            {
                "Бухгалтерия для Молдовы",
                "Бухгалтерия для Молдовы, редакция"
            };

            var configurations1C = new AddOrUpdateConfiguration1CDataItemDto
            {
                Name = "Бухгалтерия для Молдовы, редакция 3",
                ShortCode = "am",
                UpdateCatalogUrl = "http://downloads.v8.1c.ru/tmplts/",
                RedactionCatalogs = "3.0",
                PlatformCatalog = "8.3",
                ConfigurationCatalog = "AccountingMd",
                NeedCheckUpdates = true,
                UseComConnectionForApplyUpdates = true,
                ConfigurationVariations = configurationsVariations,
                ItsAuthorizationDataId = GetAnyItsAuthorizationRecordIdOrThrowException()
            };

            CreateNewConfiguration(configurations1C);

            await EditConfiguration1C(configurations1C.Name);
            await DbLayer.RefreshAllAsync();
            var configuration = _configurations1CDataManager.GetConfiguration(configurations1C.Name);
            ClassicAssert.IsFalse(configuration.Error);

            var editedConfiguration = configuration.Result;
            ClassicAssert.AreEqual(editedConfiguration.Configuration.Name, configurations1C.Name);
            ClassicAssert.AreNotEqual(editedConfiguration.Configuration.ShortCode, configurations1C.ShortCode);
            ClassicAssert.AreNotEqual(editedConfiguration.Configuration.NeedCheckUpdates, configurations1C.NeedCheckUpdates);
            ClassicAssert.AreNotEqual(editedConfiguration.Configuration.UseComConnectionForApplyUpdates, configurations1C.UseComConnectionForApplyUpdates);
            ClassicAssert.AreNotEqual(editedConfiguration.ConfigurationNameVariations.Count, configurations1C.ConfigurationVariations.Count);

            RemoveConfiguration1C(configurations1C.Name);

            var deletedConfiguration = _configurations1CDataManager.GetConfiguration(configurations1C.Name);
            ClassicAssert.IsTrue(deletedConfiguration.Error);
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Создать новую конфигурацию 1С
        /// </summary>
        /// <param name="configurations1C">Конфигурация инф. базы</param>
        private void CreateNewConfiguration(AddOrUpdateConfiguration1CDataItemDto configurations1C)
        {
            var addNewConfiguration = _configurations1CDataManager.AddConfiguration1C(configurations1C);
            if (addNewConfiguration.Error)
            {
                throw new NotImplementedException($"Ошибка при создании конфигурации {addNewConfiguration.Message}");
            }
            ClassicAssert.IsFalse(addNewConfiguration.Error);
        }

        /// <summary>
        /// Отредактировать конфигурацию 1С
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        private async Task EditConfiguration1C(string configurationName)
        {
            var configuration = _configurations1CDataManager.GetConfiguration(configurationName);
            ClassicAssert.IsFalse(configuration.Error);

            var resultConfiguration = configuration.Result;
            configuration.Result.ConfigurationNameVariations.Add("Бухгалтерия для Молдовы редакция");

            var editConfiguration = new EditConfiguration1CCommand
            {
                Name = resultConfiguration.Configuration.Name,
                ShortCode = "rgb",
                UseComConnectionForApplyUpdates = false,
                NeedCheckUpdates = false,
                PlatformCatalog = resultConfiguration.Configuration.PlatformCatalog,
                RedactionCatalogs = resultConfiguration.Configuration.RedactionCatalogs,
                UpdateCatalogUrl = resultConfiguration.Configuration.UpdateCatalogUrl,
                ConfigurationCatalog = resultConfiguration.Configuration.ConfigurationCatalog,
                ConfigurationVariations = ["Бухгалтерия для Молдовы редакция"],
                ItsAuthorizationDataId = resultConfiguration.Configuration.ItsAuthorizationDataId
            };

            var editExistingConfiguration = await Mediator.Send(editConfiguration);

            ClassicAssert.IsFalse(editExistingConfiguration.Error);
        }
    }
}
