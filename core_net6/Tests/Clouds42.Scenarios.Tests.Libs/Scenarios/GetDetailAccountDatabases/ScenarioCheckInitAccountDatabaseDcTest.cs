﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.GetDetailAccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки инициализации
    /// модели отображения информационной базы
    /// Создаем 6 разных баз(на разделителях, удаленные, созданные итд..)
    /// и получаем по ним модели отображения.
    /// Далее проверяем все ли поля заполнены верно
    /// </summary>
    public class ScenarioCheckInitAccountDatabaseDcTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly AccountDatabaseDcHelper _accountDatabaseDcHelper;

        public ScenarioCheckInitAccountDatabaseDcTest()
        {
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountDatabaseDcHelper = ServiceProvider.GetRequiredService<AccountDatabaseDcHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountCommand2 = new CreateAccountCommand(TestContext);
            createAccountCommand2.Run();
            var account = DbLayer.AccountsRepository.FirstOrDefault(acc => acc.Id == createAccountCommand2.AccountId);
            
            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var dbTemlate = DbLayer.DbTemplateRepository.FirstOrDefault();
            var configuration1C = DbLayer.Configurations1CRepository.FirstOrDefault();

            var accountDatabases = new List<Domain.DataModels.AccountDatabase>
            {
                GetAccountDatabase(createDbTemplateDelimitersCommand.AccountDatabaseId)
            };

            var fileStorageId = GetAccountFileStorageId(createAccountCommand.AccountId);

            var dbTwo = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 2,
                createAccountCommand.Account.IndexNumber, dbTemlate.Id, fileStorageId, PublishState.Published,
                DatabaseState.DeletedToTomb, "Все ровно");
            DbLayer.DatabasesRepository.Insert(dbTwo);
            accountDatabases.Add(dbTwo);

            var dbThree = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 3,
                createAccountCommand.Account.IndexNumber, null, fileStorageId, PublishState.Unpublished);
            DbLayer.DatabasesRepository.Insert(dbThree);
            accountDatabases.Add(dbThree);

            var dbFour = _testDataGenerator.GenerateAccountDatabase(account.Id, 1,
                account.IndexNumber, null, fileStorageId, PublishState.Published);
            DbLayer.DatabasesRepository.Insert(dbFour);
            accountDatabases.Add(dbFour);

            var dbFive = _testDataGenerator.GenerateAccountDatabase(account.Id, 2,
                account.IndexNumber, null, fileStorageId, PublishState.Published);
            DbLayer.DatabasesRepository.Insert(dbFive);
            accountDatabases.Add(dbFive);

            var acDbSupport = _testDataGenerator.GenerateAcDbSupport(dbFive.Id, configuration1C.Name);
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);

            var dbSix = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 4,
                createAccountCommand.Account.IndexNumber, null, fileStorageId, PublishState.Published);
            DbLayer.DatabasesRepository.Insert(dbSix);
            accountDatabases.Add(dbSix);

            var acDbSupportTwo = _testDataGenerator.GenerateAcDbSupport(dbSix.Id, configuration1C.Name, false, false);
            DbLayer.AcDbSupportRepository.Insert(acDbSupportTwo);
            DbLayer.Save();

            var accountDatabaseDataManager = ServiceProvider.GetRequiredService<CreateAccountDatabaseDataManager>();
            var accountDatabasesDc = accountDatabaseDataManager.GetAccountDatabaseStates(accountDatabases.Select(db => db.Id).ToArray()).Result;

            if (accountDatabasesDc.Count != accountDatabases.Count)
                throw new InvalidOperationException("Не на все информационные базы получены модели отображения");

            _accountDatabaseDcHelper.CheckCorrectnessOfAccountDatabasesDc(accountDatabases, accountDatabasesDc);
        }

        /// <summary>
        /// Получить базу по Id
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>База</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId) =>
            DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
    }
}
