﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.GetDetailAccountDatabases
{
    /// <summary>
    /// Сценарий теста проверки таймера, проверки статуса быза
    /// Создаем 1 базу на разделителях и получаем по ним модели отображения.
    /// Далее проверяем таймер
    /// </summary>
    public class ScenarioCheckTimerNewTimeGreenTest : ScenarioBase
    {
        public override void Run()
        {
            //создаем аккаунт с подключенной арендой
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });

            //создаем второй аккаут который добавим в справочник баз на разделители
            var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext,
                new DbTemplateDelimitersModelTest(TestContext, dbTemplate.Id));
            sourceAccountDatabaseCommand.Run();

            //создаем обычную базу из шаблонона
            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            //создаем базу на разделителях
            var dbOnDelimter = new InfoDatabaseDomainModelTest(TestContext, dbTemplate) { DbTemplateDelimiters = true, DataBaseName = "Тестовая база на разделителях" };
            var createDbOnDelimiterCommand = new CreateAccountDatabaseViaDelimiterCommand(TestContext, createAccountCommand, dbOnDelimter);
            createDbOnDelimiterCommand.Run();

            RefreshDbCashContext(TestContext.Context.AccountDatabases.ToList());
            RefreshDbCashContext(TestContext.Context.AccountDatabaseDelimiters.ToList());
            var accountDatabases = new List<Domain.DataModels.AccountDatabase>();

            //база на разделителях
            var database = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == createDbOnDelimiterCommand.AccountDatabaseId);

            accountDatabases.Add(database);
            if (database == null)
                throw new InvalidOperationException("Созданная ИБ на разделителях не найдена");

            var newItemState = DatabaseState.NewItem.ToString();

            DbLayer.DatabasesRepository.AsQueryable().Where(x => x.Id == database.Id)
                .ExecuteUpdate(x => x.SetProperty(y => y.State, newItemState));

            RefreshDbCashContext(TestContext.Context.AccountDatabases.ToList());
            RefreshDbCashContext(TestContext.Context.AccountDatabaseDelimiters.ToList());
         
            database = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == createDbOnDelimiterCommand.AccountDatabaseId);
            if (database.StateEnum != DatabaseState.NewItem)
                throw new InvalidOperationException("Созданная ИБ на разделителях не в статусе NewItem");
        }

    }
}
