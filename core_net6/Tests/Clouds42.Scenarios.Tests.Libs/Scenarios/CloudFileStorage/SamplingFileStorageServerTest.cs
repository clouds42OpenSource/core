﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Core42.Application.Features.FileStorageServerContext.Queries;
using Core42.Application.Features.FileStorageServerContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudFileStorage
{
    /// <summary>
    /// Тест выборки файловых хранилищ по фильтру
    /// Сценарий:
    ///         1. Создаем 3 файловых хранилища
    ///         2. Делаем выборки по фильтрам, проверяем, что выборки корректные
    /// </summary>
    public class SamplingFileStorageServerTest : ScenarioBase
    {
        private readonly ICloudFileStorageServerManager _cloudFileStorageServerManager;

        public SamplingFileStorageServerTest()
        {
            _cloudFileStorageServerManager = TestContext.ServiceProvider.GetRequiredService<ICloudFileStorageServerManager>();
        }
        public override void Run()
        {
            var firstStorageServerDto = new CreateCloudFileStorageServerDto
            {
                Name = "Name",
                Description = "Desc",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                DnsName = $"Server_{Guid.NewGuid()}"
            };
            CreateFileStorageServer(firstStorageServerDto);

            var secondStorageServerDto = new CreateCloudFileStorageServerDto
            {
                Name = "NameTest",
                Description = "Description",
                ConnectionAddress = $"address_{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                DnsName = $"Server_{Guid.NewGuid()}"
            };
            CreateFileStorageServer(secondStorageServerDto);

            var thirdStorageServerDto = new CreateCloudFileStorageServerDto
            {
                Name = "Storage",
                Description = "Engine",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                DnsName = $"Server_{Guid.NewGuid()}"
            };
            CreateFileStorageServer(thirdStorageServerDto);


            var filter = new GetFilteredFileStorageServersQuery
            {
                Filter = new FileStorageServerFilter
                {
                    Name = "Name"
                },
                PageNumber = 1
            };
            GetFilteredFileStorageServers(2, filter, GetData);

            filter.Filter.Name = null;
            filter.Filter.Description = "Eng";
            GetFilteredFileStorageServers(1, filter, GetData);

            filter.Filter.Description = null;
            filter.Filter.Name = "Storage";
            GetFilteredFileStorageServers(1, filter, GetData);
        }

        /// <summary>
        /// Создать файловое хранилище
        /// </summary>
        /// <returns>Id созданного файлового хранилища</returns>
        public void CreateFileStorageServer(CreateCloudFileStorageServerDto cloudFileStorageServerDto)
        {
            var cloudFileStorageServer =
                _cloudFileStorageServerManager.CreateCloudFileStorageServer(cloudFileStorageServerDto);

            AssertHelper.Execute(() => Assert.IsFalse(cloudFileStorageServer.Error, cloudFileStorageServer.Message));
        }

        /// <summary>
        /// Получить файловые хранилища по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество файловых хранилищ</param>
        /// <param name="filter">Фильтр</param>
        /// <param name="action">Действие, для выбора и проверки выборки файловых хранилищ</param>
        private void GetFilteredFileStorageServers(int expectedCount, GetFilteredFileStorageServersQuery filter,
            Action<GetFilteredFileStorageServersQuery, int> action)
        {
            action(filter, expectedCount);
        }

        /// <summary>
        /// Получить файловые хранилища по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="query">Фильтр</param>
        /// <param name="expectedCount">Ожидаемое количество файловых хранилищ</param>
        private void GetData(GetFilteredFileStorageServersQuery query, int expectedCount)
        {
            var enterpriseServers = Mediator.Send(query).Result;
            AssertHelper.Execute(() => Assert.IsFalse(enterpriseServers.Error));
            AssertHelper.Execute(() => Assert.AreEqual(expectedCount, enterpriseServers.Result.Records.Count));
        }
    }
}
