﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudFileStorage
{
    /// <summary>
    /// Тест создания/удаления/редактирования файлового хранилища
    /// Сценарий:
    ///         1) Создаем файловое хранилище, проверяем, что создание прошло без ошибок
    ///         2) Редактируем созданное файловое хранилище, проверяем, редактирование прошло без ошибок
    ///         3) Удаляем файловое хранилище, проверяем, что файловое хранилище удалено
    /// </summary>
    public class CrudFileStorageServerTest : ScenarioBase
    {
        private readonly ICloudFileStorageServerManager _cloudFileStorageServerManager;

        public CrudFileStorageServerTest()
        {
            _cloudFileStorageServerManager = TestContext.ServiceProvider.GetRequiredService<ICloudFileStorageServerManager>();
        }

        public override void Run()
        {
            var fileStorageServerId = CreateFileStorageServer();
            EditFileStorageServer(fileStorageServerId);
            DeleteFileStorageServer(fileStorageServerId);

        }

        /// <summary>
        /// Создать файловое хранилище
        /// </summary>
        /// <returns>Id созданного файлового хранилища</returns>
        public Guid CreateFileStorageServer()
        {
            var cloudFileStorageServerDto = new CreateCloudFileStorageServerDto
            {
                Name = "TestFileName",
                Description = "Test Description",
                ConnectionAddress = "Test connection Address",
                DnsName = $"Server_{Guid.NewGuid()}"
            };

            var cloudFileStorageServer =
                _cloudFileStorageServerManager.CreateCloudFileStorageServer(cloudFileStorageServerDto);

            AssertHelper.Execute(() => Assert.IsFalse(cloudFileStorageServer.Error, cloudFileStorageServer.Message));

            return cloudFileStorageServer.Result;
        }

        /// <summary>
        /// Редактировать файловое хранилище 
        /// </summary>
        /// <param name="cloudFileStorageServerId">Id файлового хранилища</param>
        private void EditFileStorageServer(Guid cloudFileStorageServerId)
        {
            var cloudFileStorageServerDto =
                _cloudFileStorageServerManager.GetCloudFileStorageServerDto(cloudFileStorageServerId);

            Assert.IsFalse(cloudFileStorageServerDto.Error, cloudFileStorageServerDto.Message);

            var editedCloudFileStorageServerDto = new CloudFileStorageServerDto
            {
                Id = cloudFileStorageServerDto.Result.Id,
                ConnectionAddress = "NewConnectionAddress",
                Description = "NewDescription",
                Name = "NewName",
                DnsName = $"Server_{Guid.NewGuid()}"
            };

            _cloudFileStorageServerManager.EditCloudFileStorageServer(editedCloudFileStorageServerDto);

            var cloudFileStorageServerDtoAfterEdit =
                _cloudFileStorageServerManager.GetCloudFileStorageServerDto(cloudFileStorageServerId);

            AssertHelper.Execute(() =>
                Assert.IsFalse(cloudFileStorageServerDtoAfterEdit.Error, cloudFileStorageServerDtoAfterEdit.Message));

            CompareFileStorageServerDtos(editedCloudFileStorageServerDto, cloudFileStorageServerDtoAfterEdit.Result);
        }

        /// <summary>
        /// Сравнить модели файлового хранилища
        /// </summary>
        /// <param name="expectedDto">Ожидаемая модель файлового хранилища</param>
        /// <param name="actualDto">Действительная модель файлового хранилища</param>
        private void CompareFileStorageServerDtos(CreateCloudFileStorageServerDto expectedDto, CreateCloudFileStorageServerDto actualDto)
        {
            AssertHelper.Execute(() =>
                Assert.AreEqual(expectedDto.Name, actualDto.Name, $"Поле название должно быть {actualDto.Name}"));

            AssertHelper.Execute(() => Assert.AreEqual(expectedDto.Description, actualDto.Description,
                $"Поле описание должно быть {actualDto.Description}"));

            AssertHelper.Execute(() => Assert.AreEqual(expectedDto.ConnectionAddress, actualDto.ConnectionAddress,
                $"Поле адрес подключения должно быть {actualDto.ConnectionAddress}"));
        }

        /// <summary>
        /// Удалить файловое хранилище
        /// </summary>
        /// <param name="cloudFileStorageServerId">Id файлового хранилища</param>
        private void DeleteFileStorageServer(Guid cloudFileStorageServerId)
        {
            var deleteCloudFileStorageServer = _cloudFileStorageServerManager.DeleteCloudFileStorageServer(cloudFileStorageServerId);
            AssertHelper.Execute(() =>
                Assert.IsFalse(deleteCloudFileStorageServer.Error, deleteCloudFileStorageServer.Message));

            var cloudFileStorageServerDto = _cloudFileStorageServerManager.GetCloudFileStorageServerDto(cloudFileStorageServerId);
            AssertHelper.Execute(() =>
                Assert.IsTrue(cloudFileStorageServerDto.Error, "Должна быть ошибка тк хранилище было удалено"));
        }
    }
}
