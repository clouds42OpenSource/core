﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.Billing;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MeasuringTimeToGetData
{
    /// <summary>
    /// Сценарий теста при замере времени на получение аккаунтов и пользователей аккаунтов
    /// </summary>
    public class ScenarioWithMeasuringTimeToGetAccountAndAccountUserTest : ScenarioBase
    {
        private readonly TestDataGenerator _generator;
        private readonly Stopwatch _stopwatch;
        private readonly CreateTestDataHelper _createHelper;

        public ScenarioWithMeasuringTimeToGetAccountAndAccountUserTest()
        {
            _generator = new TestDataGenerator(Configuration, DbLayer);
            _stopwatch = new Stopwatch();
            _createHelper = new CreateTestDataHelper(DbLayer);
        }

        public override void Run()
        {
            var accounts = _generator.GenerateListAccountsByCount(100);
            var accountUsers = new List<AccountUser>();

            foreach (var account in accounts)
                accountUsers.AddRange(_generator.GenerateListAccountUsersByCount(100, account.Id));

            accounts.SaveAccounts(DbLayer);
            accountUsers.SaveAccountUsers(DbLayer);

            var currentAccountUser = accountUsers.FirstOrDefault();
            var fakeAccessProvider = new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            _createHelper.CreateSession(currentAccountUser.Id, ExternalClient.Efsol);
            _createHelper.CreateAccountUserRole(currentAccountUser.Id);

            Services.AddTransient<IAccessProvider>();

            _stopwatch.Start();
            Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter
                {
                    OnlyMine = false
                }
            });
            _stopwatch.Stop();

            if (_stopwatch.ElapsedMilliseconds > 2000)
                throw new InvalidOperationException ("Метод AccountsManager.GetAccountsPaginationListDto выполняется более 2 секунд");

            var accountUsersSearchHelper = new AccountUsersSearchHelper(fakeAccessProvider, DbLayer);

            _stopwatch.Restart();
            accountUsersSearchHelper.GetAccountUsersSeachResult(currentAccountUser.AccountId, new AccountUsersFilterModel());
            _stopwatch.Stop();

            if (_stopwatch.ElapsedMilliseconds > 2000)
                throw new InvalidOperationException ("Метод AccountUsersSearchHelper.GetAccountUsersSearchResult выполняется более 2 секунд");
        }
    }
}
