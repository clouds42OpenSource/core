﻿using System;
using System.Diagnostics;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MeasuringTimeToGetData
{
    /// <summary>
    /// Сценарий теста при замере времени на получение информационных баз
    /// </summary>
    public class ScenarioWithMeasuringTimeToGetAccountDatabasesTest : ScenarioBase
    {
        private readonly TestDataGenerator _generator;
        private readonly CreateTestDataHelper _createHelper;
        private readonly IAccountDatabaseManager _accountDatabaseManager;

        public ScenarioWithMeasuringTimeToGetAccountDatabasesTest()
        {
            _generator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _createHelper = TestContext.ServiceProvider.GetRequiredService<CreateTestDataHelper>();
            _accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();
        }

        public override void Run()
        {
            var stopwatch = new Stopwatch();
            var account = _generator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(account);
            DbLayer.Save();

            var billingAccount = _generator.GenerateBillingAccount(account.Id);
            DbLayer.BillingAccountRepository.Insert(billingAccount);

            var accontUser = _generator.GenerateAccountUser(account.Id);
            DbLayer.AccountUsersRepository.Insert(accontUser);
            DbLayer.Save();

            _createHelper.CreateSession(accontUser.Id, ExternalClient.Efsol);
            _createHelper.CreateAccountUserRole(accontUser.Id);

            var accountDatabases = _generator.GenerateListAccountDatabasesByCount(1000, account.Id, 1, account.IndexNumber);
            accountDatabases.SaveAccountDatabases(DbLayer);

            var acDbAccesses = _generator.GenerateListAcDbAccess(accontUser, accountDatabases);
            acDbAccesses.SaveAcDbAccesses(DbLayer);

            stopwatch.Start();
            _accountDatabaseManager.GetAccountDatabasesViewModel(1, "");
            stopwatch.Stop();

            if (stopwatch.ElapsedMilliseconds > 2000)
                throw new InvalidOperationException("Метод AccountDatabaseManager.GetAccountDatabasesViewModel выполняется более 2 секунд");

        }
    }
}
