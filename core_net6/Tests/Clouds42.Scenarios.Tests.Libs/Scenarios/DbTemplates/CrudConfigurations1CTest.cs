﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplates
{
    /// <summary>
    /// Тест добавления/удаления/редактирования нового шаблона
    /// Сценарий:
    ///         1. Создаем новый шаблон, проверяем, что создание прошло успешно
    ///         2. Редактируем новый шаблон, проверяем, что редактирование прошло успешно
    ///         3. Удаляем новый шаблон, проверяем, что удаление прошло успешно
    /// </summary>
    public class CrudDbTemplatesTest : ScenarioBase
    {
        private readonly DbTemplatesManager _dbTemplatesManager;

        public CrudDbTemplatesTest()
        {
            _dbTemplatesManager = TestContext.ServiceProvider.GetRequiredService<DbTemplatesManager>();
        }

        public override void Run()
        {
            var dbTemplate = new DbTemplateItemDto
            {
                Id = Guid.Empty,
                Name = Guid.NewGuid().ToString(),
                DefaultCaption = Guid.NewGuid().ToString(),
                Configuration1CName = "Бухгалтерия предприятия, редакция 3.0"
        };

            CreateNewDbTemplate(ref dbTemplate);
            Assert.IsTrue(!Guid.Empty.Equals(dbTemplate.Id));
            UpdateDbTemplatePlatform(dbTemplate);
            DeleteDbTemplate(dbTemplate.Id);
        }

        /// <summary>
        /// Создать новый шаблон
        /// 1. Проверка, что шаблона не существует
        /// 2. Добавляем новый шаблон
        /// 3. Проверка что новая запись добавлена
        /// 4. Назначаем созданный ID в переданную модель
        /// </summary>
        /// <param name="item">Данные для дабавления шаблона</param>
        private void CreateNewDbTemplate(ref DbTemplateItemDto item)
        {
            #region Проверка, что шаблона не существует

            var foundItem = _dbTemplatesManager.GetDbTemplateByName(item.Name);
            Assert.IsTrue(foundItem.Error);

            #endregion


            #region Добавляем новый шаблон

            var addNewDbTemplate = _dbTemplatesManager.AddDbTemplateItem(item);
            Assert.IsFalse(addNewDbTemplate.Error);

            #endregion

            
            #region Проверка что новая запись добавлена

            foundItem = _dbTemplatesManager.GetDbTemplateByName(item.Name);
            Assert.IsFalse(foundItem.Error);

            #endregion

            
            #region Назначаем созданный ID в переданную модель

            item.Id = addNewDbTemplate.Result.DbTemplateId;

            #endregion

        }

        /// <summary>
        /// Обновить шаблон
        /// 1. Проверка, что шаблон существует
        /// 2. Обновляем платформу для шаблона
        /// 3. Проверка что запись обнавлена
        /// </summary>
        /// <param name="item">Шаблон для обновления</param>
        private void UpdateDbTemplatePlatform(DbTemplateItemDto item)
        {
            #region Проверка, что шаблон существует

            var foundItemResult = _dbTemplatesManager.GetDbTemplateByName(item.Name);
            
            Assert.IsFalse(foundItemResult.Error);

            #endregion


            #region Обновляем найденный шаблон

            var oldPlatform = item.Platform;

            var newPlatform = oldPlatform == PlatformType.V82 ? PlatformType.V83 : PlatformType.V82;

            item.Platform = newPlatform;
            
            var updateNewDbTemplate = _dbTemplatesManager.UpdateDbTemplateItem(item);
            
            Assert.IsFalse(updateNewDbTemplate.Error);

            #endregion


            #region Проверка что запись обнавлена

            foundItemResult = _dbTemplatesManager.GetDbTemplateByName(item.Name);
            
            Assert.IsFalse(foundItemResult.Error);
            
            var foundItem = foundItemResult.Result;

            Assert.IsTrue(oldPlatform != foundItem.Platform);

            #endregion
        }


        /// <summary>
        /// Обновить шаблон
        /// 1. Проверка, что шаблон существует
        /// 2. Обновляем платформу для шаблона
        /// 3. Проверка что запись обнавлена
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона для обновления</param>
        private void DeleteDbTemplate(Guid dbTemplateId)
        {
            #region Удаляем шаблон

            var deleteDbTemplateResult = _dbTemplatesManager.DeleteDbTemplateItem(new DeleteDbTemplateDataItemDto
            {
                DbTemplateId = dbTemplateId
            });

            Assert.IsFalse(deleteDbTemplateResult.Error);

            #endregion


            #region Проверка что запись удалена

            var foundItemResult = _dbTemplatesManager.GetDbTemplateById(dbTemplateId);

            Assert.IsTrue(foundItemResult.Error);

            #endregion
        }
    }
}
