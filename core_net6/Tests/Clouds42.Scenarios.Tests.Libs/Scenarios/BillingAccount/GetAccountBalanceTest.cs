﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест получения баланса аккаунта
    /// Сценарий:
    ///         1. Создаем аккаунт, пополняем баланс, проверяем, что баланс правильный
    ///         2. Пополняем баланс еще раз, проверяем, что баланс изменился и баланс правильный
    ///         3. Пополняем бонусный баланс, проверяем, что основной баланс не изменился
    /// </summary>
    public class GetAccountBalanceTest : ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;

        public GetAccountBalanceTest()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var firstPaymentSum = 500;
            CreateInflowPayment(createAccountCommand.AccountId, null, firstPaymentSum);

            GetAccountBalance(createAccountCommand.AccountId, firstPaymentSum);

            var secondPaymentSum = 500;
            CreateInflowPayment(createAccountCommand.AccountId, null, secondPaymentSum);
            GetAccountBalance(createAccountCommand.AccountId, firstPaymentSum + secondPaymentSum);

            var bonusPaymentSum = 500;
            CreateInflowPayment(createAccountCommand.AccountId, null, bonusPaymentSum, TransactionType.Bonus);
            GetAccountBalance(createAccountCommand.AccountId, firstPaymentSum + secondPaymentSum);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="sum"></param>
        private void GetAccountBalance(Guid accountId, int sum)
        {
            var getAccountBalance = _billingAccountManager.GetAccountBalance(accountId);
            Assert.IsFalse(getAccountBalance.Error);
            Assert.AreEqual(getAccountBalance.Result, sum);
        }
    }
}
