﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест преждeвременного погашения обещанного платежа
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Блокируем Арнеду и оплачиваем ее(чтобы была возможность создать обещанный платеж)
    ///         3. Берем обещанный платеж, проверяем, что ОП предоставлен
    ///         4. Погашаем преждевеменно ОП, проверяем, что ОП погашен
    /// </summary>
    public class PromisePaymentEarlyRepayTest : ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;

        public PromisePaymentEarlyRepayTest()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            BlockRent1CForAccount(createAccountCommand.AccountId);

            var rent1C = GetRent1CServiceOrThrowException();

            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CStandardCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == rent1C.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            var isRentBlock = IsRent1CBlock(createAccountCommand.AccountId, rent1C.Id);
            Assert.IsTrue(isRentBlock);

            CreateInflowPayment(createAccountCommand.AccountId, null, rent1CStandardCost);

            var promisePaymentSum = rent1CStandardCost;

            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto {PaymentSum = promisePaymentSum},
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment =
                _billingAccountManager.CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);
            Assert.IsFalse(promisePayment.Error);

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(billingAccount.Balance, rent1CStandardCost);
            Assert.AreEqual(billingAccount.Balance, rent1CStandardCost);

            var promisePaymentEarlyRepay = _billingAccountManager.PromisePaymentEarlyRepay(billingAccount);
            Assert.IsFalse(promisePaymentEarlyRepay.Error);

            billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(0, billingAccount.PromisePaymentSum);
        }

        /// <summary>
        /// Проверить состояние блокировки Аренды
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id Аренды 1С</param>
        /// <returns>Состояние блокироваки Аренды</returns>
        private bool IsRent1CBlock(Guid accountId, Guid serviceId)
        {
            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.AccountId == accountId && rc.BillingServiceId == serviceId);
            Assert.IsNotNull(resourceConfiguration);

            return resourceConfiguration.FrozenValue;
        }
    }
}