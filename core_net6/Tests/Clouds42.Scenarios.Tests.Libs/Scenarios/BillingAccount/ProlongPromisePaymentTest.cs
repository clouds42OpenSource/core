﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест продления обещанного платёжа
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Блокируем Арнеду и оплачиваем ее(чтобы была возможность создать обещанный платеж)
    ///         3. Продлеваем обещанный платеж, проверяем, что ОП продлен на 1 день
    /// </summary>
    public class ProlongPromisePaymentTest : ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;

        public ProlongPromisePaymentTest()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            BlockRent1CForAccount(createAccountCommand.AccountId);

            var rent1C = GetRent1CServiceOrThrowException();
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CStandardCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == rent1C.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            CreateInflowPayment(createAccountCommand.AccountId, null, rent1CStandardCost);

            var promisePaymentSum = rent1CStandardCost;

            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto {PaymentSum = promisePaymentSum},
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment =
                _billingAccountManager.CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);
            Assert.IsFalse(promisePayment.Error);

            var bas = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(bas.PromisePaymentSum, rent1CStandardCost);
            Assert.IsNotNull(bas.PromisePaymentDate);

            var promisePaymentExpireDate = bas.PromisePaymentDate.Value;
            Assert.AreEqual(promisePaymentExpireDate.Date, DateTime.Now.Date);

            var prolongPromisePayment = _billingAccountManager.ProlongPromisePayment(bas);
            Assert.IsFalse(prolongPromisePayment.Error);

            bas = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(bas.PromisePaymentDate);
            Assert.AreEqual(bas.PromisePaymentDate.Value.Date, DateTime.Now.AddDays(1).Date);
        }
    }
}