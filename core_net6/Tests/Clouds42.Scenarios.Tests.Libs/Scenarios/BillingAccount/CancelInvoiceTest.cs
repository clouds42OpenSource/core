﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест создания счёта для аккаунта на указанную сумму
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Создаем счет на указанную сумму
    ///         3. Проверяем, что создан счет
    ///         4. Отменяем счет
    ///         5.Проверяем что отменен
    /// </summary>
    public class CancelInvoiceTest : ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;
        public CancelInvoiceTest()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var invoiceSum = 900;

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);
            Assert.IsNull(invoice);

            var invoiceForSpecifiedInvoiceAmount =
                _billingAccountManager.CreateInvoiceForSpecifiedInvoiceAmount(createAccountCommand.AccountId, invoiceSum);
            Assert.IsFalse(invoiceForSpecifiedInvoiceAmount.Error, invoiceForSpecifiedInvoiceAmount.Message);

            invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);
            Assert.AreEqual(invoice.Sum, invoiceSum);

            var cancelResult =
                _billingAccountManager.CancelInvoice(invoice.Id);

            Assert.IsFalse(cancelResult.Error);

            invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);

            Assert.AreEqual(invoice.State, InvoiceStatus.Canceled.ToString());
        }
    }
}
