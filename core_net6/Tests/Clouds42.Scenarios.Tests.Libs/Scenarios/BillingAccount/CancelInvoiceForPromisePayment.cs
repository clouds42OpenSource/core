﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;
using System.Linq;
using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест создания счёта для аккаунта на указанную сумму
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Берем ОП
    ///         3. Проверяем, что создан счет
    ///         4. Отменяем счет
    ///         5. Проверяем что счет отменен
    /// </summary>
    public class CancelInvoiceForPromisePayment : ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;
        public CancelInvoiceForPromisePayment()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            BlockRent1CForAccount(createAccountCommand.AccountId);

            var rent1C = GetRent1CServiceOrThrowException();

            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CStandardCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == rent1C.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            var isRentBlock = IsRent1CBlock(createAccountCommand.AccountId, rent1C.Id);
            Assert.IsTrue(isRentBlock);

            CreateInflowPayment(createAccountCommand.AccountId, null, rent1CStandardCost);

            var promisePaymentSum = rent1CStandardCost;

            var invoiceRequest = new CalculationOfInvoiceRequestModel
            {
                SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = promisePaymentSum },
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 10
            };

            var promisePayment =
                _billingAccountManager.CreatePromisePayment(createAccountCommand.AccountId, invoiceRequest);
            Assert.IsFalse(promisePayment.Error);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);
            Assert.AreEqual(invoice.Sum, promisePaymentSum);

            var cancelResult =
                _billingAccountManager.CancelInvoice(invoice.Id);

            Assert.IsFalse(cancelResult.Error);

            invoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.AccountId == createAccountCommand.AccountId);

            Assert.AreEqual(invoice.State, InvoiceStatus.Canceled.ToString());
        }


        /// <summary>
        /// Проверить состояние блокировки Аренды
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id Аренды 1С</param>
        /// <returns>Состояние блокироваки Аренды</returns>
        private bool IsRent1CBlock(Guid accountId, Guid serviceId)
        {
            var resourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                    rc.AccountId == accountId && rc.BillingServiceId == serviceId);
            Assert.IsNotNull(resourceConfiguration);

            return resourceConfiguration.FrozenValue;
        }
    }
}
