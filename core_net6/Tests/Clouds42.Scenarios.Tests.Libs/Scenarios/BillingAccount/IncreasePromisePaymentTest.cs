﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Core42.Models.Invoices.Creation;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    /// <summary>
    /// Тест увеличения обещанного платёжа
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Блокируем Арнеду и оплачиваем ее(чтобы была возможность создать обещанный платеж)
    ///         3. Увеличиваем обещанный платеж, проверяем, что ОП увеличен
    /// </summary>
    public class IncreasePromisePaymentTest : ScenarioBase
    {
        readonly IInvoiceCreationManager _invoiceCreationManager;
        readonly IPromisedPaymentActivationManager _promisedPaymentActivationManager;
        
        public IncreasePromisePaymentTest()
        {
            _invoiceCreationManager = TestContext.ServiceProvider.GetRequiredService<IInvoiceCreationManager>();
            _promisedPaymentActivationManager = TestContext.ServiceProvider.GetRequiredService<IPromisedPaymentActivationManager>();
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            BlockRent1CForAccount(createAccountCommand.AccountId);

            var rent1C = GetRent1CServiceOrThrowException();
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CStandardCost = await DbLayer.RateRepository
                .AsQueryableNoTracking()
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == rent1C.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .SumAsync(rc => rc.Cost);

            CreateInflowPayment(createAccountCommand.AccountId, null, rent1CStandardCost);

            var request = new CreateInvoiceFromArbitraryAmountApiModel
            {
                Amount = rent1CStandardCost,
                NotifyAccountUserId = createAccountCommand.AccountAdminId,
                Description = "ОП на сумму аренды"
            };

            var invoice = _invoiceCreationManager.CreateArbitraryAmountInvoice
                (request.ToCreateArbitraryAmountInvoiceDc(createAccountCommand.AccountId), request.NotifyAccountUserId);

            Assert.IsFalse(invoice.Error);
            Assert.IsNotNull(invoice.Result);


            var promisePayment = await _promisedPaymentActivationManager.ActivatePromisedPaymentAsync
                (createAccountCommand.AccountId, invoice.Result);
            Assert.IsFalse(promisePayment.Error);

            var bas = await DbLayer.BillingAccountRepository.AsQueryableNoTracking().FirstOrDefaultAsync(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(bas.PromisePaymentSum, rent1CStandardCost);
            Assert.IsNotNull(bas.PromisePaymentDate);
            var promisePaymentExpireDate = bas.PromisePaymentDate.Value;

            request = new CreateInvoiceFromArbitraryAmountApiModel
            {
                Amount = 10,
                NotifyAccountUserId = createAccountCommand.AccountAdminId,
                Description = "ОП на аренду"
            };

            invoice = _invoiceCreationManager.CreateArbitraryAmountInvoice
                (request.ToCreateArbitraryAmountInvoiceDc(createAccountCommand.AccountId), request.NotifyAccountUserId);

            promisePayment = _promisedPaymentActivationManager.
                IncreasePromisedPaymentAsync(createAccountCommand.AccountId, invoice.Result).Result;
            Assert.IsFalse(promisePayment.Error);

            bas = await DbLayer.BillingAccountRepository.AsQueryableNoTracking().FirstOrDefaultAsync(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(bas.PromisePaymentSum, rent1CStandardCost + 10);
            Assert.IsNotNull(bas.PromisePaymentDate);
            Assert.AreEqual(promisePaymentExpireDate, bas.PromisePaymentDate);
        }

        public override void Run()
        {
            throw new NotImplementedException();

        }
    }
}
