﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Clouds42.Billing.Billing.Models;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingAccount
{
    public class CheckPayboxInputModelTest : ScenarioBase
    {
        public override void Run()
        {
            var inputJsonData = @"{
            ""pg_order_id"": ""759457"",
            ""pg_payment_id"": 642293555,
            ""pg_amount"": 13000,
            ""pg_currency"": ""KZT"",
            ""pg_net_amount"": 12623,
            ""pg_ps_amount"": 13000,
            ""pg_ps_full_amount"": 13000,
            ""pg_ps_currency"": ""KZT"",
            ""pg_description"": ""Id компании = 21039"",
            ""pg_result"": 1,
            ""pg_payment_date"": ""2022-10-21 14:16:16"",
            ""pg_can_reject"": 1,
            ""pg_user_phone"": ""7702334130"",
            ""pg_need_phone_notification"": 0,
            ""pg_user_contact_email"": ""mamedova.y.h@gmail.com"",
            ""pg_need_email_notification"": 1,
            ""pg_payment_method"": ""bankcard"",
            ""pg_captured"": 1,
            ""pg_card_pan"": ""4400-43XX-XXXX-8604"",
            ""pg_card_exp"": ""10/24"",
            ""pg_card_owner"": ""ILDYZ MAMEDOVA"",
            ""pg_card_brand"": ""VI"",
            ""pg_salt"": ""BN8lgDthYXhYuF1x"",
            ""pg_sig"": ""494254e78cb4bbbc4c2f2b906bb267c9""
            }";

            var resultModel = JsonConvert.DeserializeObject<PayboxImportResultModel>(inputJsonData);

            resultModel?.SetSourceDataCollection(RequestSourceDataCollectionHelperTest.GetSourceDataCollectionFromJson(inputJsonData));
            var modelProps = typeof(PayboxImportResultModel).GetProperties();
            Assert.IsTrue(modelProps.Length >= resultModel?._sourceDataCollection.Count());
            var modelPropNames = modelProps.Where(x => x.GetValue(resultModel) != null).Select(x => x.Name);
            var sourceDataCollectionNames = resultModel._sourceDataCollection.Select(x => x.Key);
            Assert.IsTrue(modelPropNames.OrderBy(x => x).SequenceEqual(sourceDataCollectionNames.OrderBy(x => x)));

            var url = "PayboxImportResult";

            var allProperties = resultModel._sourceDataCollection
                .Where(x => x.Key != "pg_sig")
                .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                .OrderBy(x => x.Key)
                .Aggregate(
                    new StringBuilder(url).Append(";"),
                    (sb, kv) => sb.Append(kv.Value).Append(";"))  + "kof5uDOj9W0JGs8q";


            using var md5Hash = new MD5CryptoServiceProvider();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(allProperties));
            var signature = new StringBuilder();
            foreach (byte b in data)
            {
                signature.Append(b.ToString("x2"));
            }

            Assert.IsTrue(signature.Equals(resultModel.pg_sig));
        }
    }
}
