﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Constants;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Cloud42ServiceTest
{
    /// <summary>
    /// Тест рассчета частичной стоимости сервиса 
    /// </summary>
    public class ServicePartialCostCalculatorTest : ScenarioBase
    {
        public override void Run()
        {
            CalculatePartialCost(0, 1200, DateTime.Now.AddDays(-1).Date, null);
            CalculatePartialCost(1200, 1200, DateTime.Now.AddMonths(1), null);
            CalculatePartialCost(-1200, -1200, DateTime.Now.AddMonths(1), null);
            CalculatePartialCost(0, 1200, DateTime.Now.AddDays(-1), LocaleConst.Ukraine);
            CalculatePartialCost(40, 1200, DateTime.Now.AddDays(1).Date, LocaleConst.Ukraine);
            CalculatePartialCost(530, 530, DateTime.Now.AddMonths(1).Date, LocaleConst.Ukraine);
            CalculatePartialCost(0, 1200, DateTime.Now.AddDays(-1), LocaleConst.Kazakhstan);
            CalculatePartialCost(1200, 1200, DateTime.Now.AddMonths(1), LocaleConst.Kazakhstan);
            CalculatePartialCost(0, 1200, DateTime.Now.Date, LocaleConst.Kazakhstan);
            CalculatePartialCost(40, 1200, DateTime.Now.AddDays(1).Date, LocaleConst.Ukraine);
        }

        /// <summary>
        /// Рассчитать частичную стоимость сервиса
        /// </summary>
        /// <param name="expectedValue">Ожидаемое значение</param>
        /// <param name="sum">Сумма</param>
        /// <param name="expiredDate">Дата окончания сервиса</param>
        /// <param name="localeName">Локаль</param>
        private void CalculatePartialCost(int expectedValue, int sum, DateTime expiredDate, string localeName)
        {
            Assert.AreEqual(expectedValue,
                BillingServicePartialCostCalculator.CalculateUntilExpireDate(sum, expiredDate, localeName));
        }

    }
}
