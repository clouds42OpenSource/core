﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingDatabasesTest
{
    /// <summary>
    /// Сценарий проверки публикации базы когда одна из нод сервера публикаций недоступна
    ///     1) Создаем тестовый сегмент
    ///     2) Создаем аккаунт и базу
    ///     3) Меняем созданному аккаунту сегмент на новый
    ///     4) Пытаемся опубликовать базу
    ///     5) Проверяем что база опубликована хотя одна из нод недоступна
    /// </summary>
    public class ScenarioPublishingDatabaseWhenOneNodeIsUnavailable : ScenarioBase
    {
        public override void Run()
        {
            var accountDatabaseAndAccountCommandByFileDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommandByFileDb.Run();

            var accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w =>
                w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);
            
            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException(
                    $"При создании базы статус должен быть Unpublished, а он {accountDatabasebyFileDb.PublishState}");

            var testSegment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault(w =>
                w.ID == accountDatabaseAndAccountCommandByFileDb.AccountDetails.Segment.Id);

            if (testSegment == null)
                throw new NotFoundException("Тестовый сегмент не найден");

            var faildePublishNode = new PublishNodeReference
            {
                Address = "1.2.3.4",
                ID = Guid.NewGuid(),
                Description = "FailedTestNode"
            };

            var contentServerNode = DbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w =>
                w.ContentServerId == testSegment.ContentServerID);
            
            var contentServerNodeFailed = new CloudServicesContentServerNode
            {
                ContentServerId = testSegment.ContentServerID,
                NodeReferenceId = faildePublishNode.ID
            };

            DbLayer.CloudServicesContentServerNodeRepository.Delete(contentServerNode);
            DbLayer.Save();

            DbLayer.PublishNodeReferenceRepository.Insert(faildePublishNode);
            DbLayer.CloudServicesContentServerNodeRepository.Insert(contentServerNodeFailed);
            DbLayer.CloudServicesContentServerNodeRepository.Insert(contentServerNode);
            DbLayer.Save();

            var accountdatabaseManger = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var result = accountdatabaseManger.PublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w =>
                w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum == PublishState.Unpublished)
                throw new ArgumentException(
                    $"При попытке опубликовать базу статус базы завис в {accountDatabasebyFileDb.PublishState}");

            //снимаем публикацию базы
            accountdatabaseManger.CancelPublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            Assert.IsFalse(result.Error);
        }
    }
}
