﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingDatabasesTest
{
    /// <summary>
    /// Сценарий проверки статуса публикации ИБ при переопубликации
    ///
    /// Предусловия: для аккаунта test_account создана база и опубликована
    ///
    /// Действия: Пытаемся переопубликовать базу и вовремя переопубликации
    /// происходит ошибка
    /// 
    /// Проверка: проверить что статус базы остался прежний опубликована
    /// </summary>
    public class ScenarioErrorRePublishingDatabase : ScenarioBase
    {
        public override void Run()
        {
            var accountDatabaseAndAccountCommandByFileDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommandByFileDb.Run();

            var accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException($"При создании базы статус должен быть Unpublished, а он {accountDatabasebyFileDb.PublishState}");

            var accountdatabaseManger = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.PublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Published)
                throw new ArgumentException($"При неудачной попытке опубликовать базу статус базы завис в {accountDatabasebyFileDb.PublishState}");

            Services.AddTransient<IDatabaseWebPublisher, DatabaseErrorWebPublisherTest>();
            var sp = Services.BuildServiceProvider();
            accountdatabaseManger = sp.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.RepublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);
            DbLayer.DatabasesRepository.Reload(accountDatabasebyFileDb);
            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException($"При неудачной попытке отменить публикацию базы статус базы завис в {accountDatabasebyFileDb.PublishState}");
        }
    }
}
