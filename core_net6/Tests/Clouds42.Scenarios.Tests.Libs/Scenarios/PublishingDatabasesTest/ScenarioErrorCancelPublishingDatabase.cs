﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingDatabasesTest
{
    /// <summary>
    /// Сценарий проверки статуса публикации ИБ после отмены публикации
    ///
    /// Предусловия: для аккаунта test_account создана база и опубликована
    ///
    /// Действия: Пытаемся отменить публикацию и вовремя отмены
    /// происходит ошибка
    /// 
    /// Проверка: проверить что статус базы остался прежний опубликована
    /// </summary>
    public class ScenarioErrorCancelPublishingDatabase : ScenarioBase
    {
        public override void Run()
        {
            var accountDatabaseAndAccountCommandByFileDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommandByFileDb.Run();

            var accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if(accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException($"При создании базы статус должен быть Unpublished, а он {accountDatabasebyFileDb.PublishState}");

            var accountdatabaseManger = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.PublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Published)
                throw new ArgumentException($"При неудачной попытке опубликовать базу статус базы завис в {accountDatabasebyFileDb.PublishState}");


            Services.AddTransient<IDatabaseWebPublisher, DatabaseErrorWebPublisherTest>();
            var sp = Services.BuildServiceProvider();
            accountdatabaseManger = sp.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.CancelPublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Published)
                throw new ArgumentException($"При неудачной попытке отменить публикацию базы статус базы завис в {accountDatabasebyFileDb.PublishState}");
        }
    }
}
