﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishingDatabasesTest
{
    /// <summary>
    /// Сценарий проверки статуса публикации ИБ
    ///
    /// Предусловия: для аккаунта test_account создана база
    ///
    /// Действия: Пытаемся ее опубликовать и вовремя
    /// публикации происходит ошибка
    ///
    /// Проверка: проверить что статус базы остался прежний не опубликована
    /// </summary>
    public class ScenarioErrorPublishingDatabases : ScenarioBase
    {
        public override void Run()
        {
            var accountDatabaseAndAccountCommandByFileDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommandByFileDb.Run();

            var accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if(accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException($"При создании базы статус должен быть Unpublished, а он {accountDatabasebyFileDb.PublishState}");

            Services.AddTransient<IDatabaseWebPublisher, DatabaseErrorWebPublisherTest>();
            var sp = Services.BuildServiceProvider();
            var accountdatabaseManger = sp.GetRequiredService<AccountDatabasePublishManager>();
            accountdatabaseManger.PublishDatabaseWithWaiting(accountDatabasebyFileDb.Id);

            accountDatabasebyFileDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseAndAccountCommandByFileDb.AccountDatabaseId);

            if (accountDatabasebyFileDb.PublishStateEnum != PublishState.Unpublished)
                throw new ArgumentException($"При неудачной попытке опубликовать базу статус базы завис в {accountDatabasebyFileDb.PublishState}");
        }
    }
}
