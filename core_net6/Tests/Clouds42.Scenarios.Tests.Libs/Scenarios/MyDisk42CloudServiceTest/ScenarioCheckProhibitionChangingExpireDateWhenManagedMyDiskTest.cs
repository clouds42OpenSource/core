﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk42CloudServiceTest
{
    /// <summary>
    /// Сценарий теста проверки запрета изменения даты пролонгации
    /// при управлении сервисом мой Диск
    /// </summary>
    public class ScenarioCheckProhibitionChangingExpireDateWhenManagedMyDiskTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var resourcesProvider = ServiceProvider.GetRequiredService<IResourcesProvider>();

            var serviceMyDiskManagerDc = resourcesProvider.GetServiceMyDiskManagerInfo(createAccountCommand.AccountId);

            serviceMyDiskManagerDc.ExpireDate = DateTime.Now.AddMonths(10);

            ServiceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(createAccountCommand.AccountId).ManagedMyDisk(serviceMyDiskManagerDc);

            var updateServiceMyDiskManagerDc = resourcesProvider.GetServiceMyDiskManagerInfo(createAccountCommand.AccountId);

            if (serviceMyDiskManagerDc.ExpireDate == updateServiceMyDiskManagerDc.ExpireDate)
                throw new InvalidOperationException(
                    $"Нельзя именить дату пролонгации для сервиса {Clouds42Service.MyDisk.GetDisplayName()}, так как сервис зависит от {Clouds42Service.MyEnterprise.GetDisplayName()}.");
        }
    }
}
