﻿using System;
using System.Linq;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference
{
    /// <summary>
    ///     Тест на получения объекта или списка объектов с
    /// справочника Платформа 1С
    /// </summary>
    public class GetPlatformTest : ScenarioBase
    {
        public override void Run()
        {
            var createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatformVersionCommand.Run();

            createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatformVersionCommand.Run();

            var random = new Random();
            var stable82Version = $"8.2.{random.Next(0, 9999)}.{random.Next(0, 9999)}";
            var stable82Path = $@"C:\V82Path\{stable82Version}";
            createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext,
                new PlatformVersion1CDto{Version = stable82Version, PathToPlatform = stable82Path});
            createPlatformVersionCommand.Run();

            var platformVersionProvider = ServiceProvider.GetRequiredService<IPlatformVersionProvider>();

            var stable82Versions = DbLayer.PlatformVersionReferencesRepository.GetStable82Versions();
            var stable83Versions = DbLayer.PlatformVersionReferencesRepository.GetStable83Versions();
            var alpha83Versions = DbLayer.PlatformVersionReferencesRepository.GetAlpha83Versions();

            Assert.IsNotNull(stable82Versions);
            Assert.IsNotNull(stable83Versions);
            Assert.IsNotNull(alpha83Versions);
           
            Assert.IsTrue(stable82Versions.Any(), "Не обнаружено елементов в стабильных версиях 8.2");
            Assert.IsTrue(stable83Versions.Any(), "Не обнаружено елементов в стабильных версиях 8.3");
            Assert.IsTrue(alpha83Versions.Any(), "Не обнаружено елементов в альфа версиях 8.3");

            var listOfAllPlatforms = DbLayer.PlatformVersionReferencesRepository.AsQueryableNoTracking();

            Assert.IsNotNull(listOfAllPlatforms);
            Assert.IsTrue(listOfAllPlatforms.Any(), "Не обнаружено елементов в общем списке платформ");

            var singlePlatform = platformVersionProvider.GetPlatformVersion(createPlatformVersionCommand.Version);

            Assert.IsNotNull(singlePlatform);
            Assert.AreEqual(createPlatformVersionCommand.Version, singlePlatform.Version);
            Assert.AreEqual(stable82Path, singlePlatform.PathToPlatform);
        }
    }
}
