﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.PlatformVersion.Managers;
using Microsoft.Extensions.DependencyInjection;
using TestContext = Clouds42.Scenarios.Tests.Libs.Contexts.TestContext;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference
{
    /// <summary>
    ///     Сценарный тест проверки доступа к "Платформа 1С"
    /// </summary>
    public class CheckAccessToPlatformReferenceTest : ScenarioBase
    {
        public override void Run()
        {

            var testBaseAccessProvider = new TestBaseAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            var testContext = new TestContext(testBaseAccessProvider, DbLayer, ServiceProvider, Logger, Configuration, Context, AccessMapping, HandlerException);

            var createAccountCommand = new CreateAccountCommand(testContext);
            createAccountCommand.Run();
            Services.AddTransient<IAccessProvider, TestBaseAccessProvider>();
            var sp = Services.BuildServiceProvider();
            var platformManager = sp.GetRequiredService<PlatformVersionManager>();

            var listNotAccessibleRoles = new List<AccountUserGroup>
            {
                AccountUserGroup.Undefined,
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.AccountUser,
                AccountUserGroup.Anonymous,
                AccountUserGroup.Cloud42Service,
                AccountUserGroup.ProcOperator,
                AccountUserGroup.Hotline
            };

            var listOfAccessibleRoles = new List<AccountUserGroup>
            {
                AccountUserGroup.CloudSE
            };

            InsertNewUserRoles(createAccountCommand.AccountAdminId, listNotAccessibleRoles);
            

            InsertNewUserRoles(createAccountCommand.AccountAdminId, listOfAccessibleRoles);
            
            listOfAccessibleRoles = [AccountUserGroup.CloudAdmin];

            InsertNewUserRoles(createAccountCommand.AccountAdminId, listOfAccessibleRoles);
        }
        

        private void InsertNewUserRoles(Guid accountUserId, List<AccountUserGroup> accountUserGroups)
        {
            if (accountUserGroups == null || !accountUserGroups.Any())
                throw new InvalidOperationException("Нужно задать роли");
            
            var listOfNewRoles = accountUserGroups.Select(userGroup => new AccountUserRole
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUserId,
                AccountUserGroup = userGroup
            });
            DbLayer.AccountUserRoleRepository.InsertRange(listOfNewRoles);
            DbLayer.Save();
        }
    }
}
