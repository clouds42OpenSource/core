﻿using System;
using System.Collections.Generic;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.PlatformVersion.Managers;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference
{
    /// <summary>
    ///     Тест на удаление объектов из справочника Платформа 1С
    /// </summary>
    public class DeletePlatformTest : ScenarioBase
    {
        public override void Run()
        {
            var listOfVersions = new List<string>();
            var createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatformVersionCommand.Run();
            listOfVersions.Add(createPlatformVersionCommand.Version);

            createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatformVersionCommand.Run();
            listOfVersions.Add(createPlatformVersionCommand.Version);

            var random = new Random();
            var stable82Version = $"8.2.{random.Next(0, 9999)}.{random.Next(0, 9999)}";
            var stable82Path = $@"C:\V82Path\{stable82Version}";
            createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext,
                new PlatformVersion1CDto{Version = stable82Version, PathToPlatform = stable82Path});
            createPlatformVersionCommand.Run();
            listOfVersions.Add(createPlatformVersionCommand.Version);

            var platformManager = ServiceProvider.GetRequiredService<PlatformVersionManager>();

            foreach (var version in listOfVersions)
            {
                var managerResult = platformManager.DeletePlatformVersionReference(version);

                Assert.IsNotNull(managerResult);
                Assert.IsFalse(managerResult.Error, $"Ошибка при удалении платформы. Текст: {managerResult.Message}");
                Assert.IsTrue(managerResult.Result, "Произошла ошибка при удалении платформы");
            }
        }
    }
}
