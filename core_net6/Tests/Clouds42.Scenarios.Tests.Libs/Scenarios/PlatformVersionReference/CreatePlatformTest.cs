﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.PlatformVersion.Managers;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference
{
    /// <summary>
    ///     Тест на создание объекта в справочник Платформа 1С
    /// </summary>
    public class CreatePlatformTest : ScenarioBase
    {
        public override void Run()
        {
            var model = new PlatformVersion1CDto {PathToPlatform = "asda", Version = "asda" };
            var platformManager = ServiceProvider.GetRequiredService<PlatformVersionManager>();
            var managerResult = platformManager.AddNewPlatformVersionReference(model);

            Assert.IsNotNull(managerResult);
            Assert.IsTrue(managerResult.Error);
            Assert.IsNotNull(managerResult.Message);

            var random = new Random();

            model.Version = $"10.3.{random.Next(0, 99999)}";
            model.PathToPlatform = $@"c:\Test\{model.Version}\test.exe";

            managerResult = platformManager.AddNewPlatformVersionReference(model);

            Assert.IsNotNull(managerResult);
            Assert.IsFalse(managerResult.Error, $"При добавлении произошла ошибка {managerResult.Message}");
            Assert.IsNull(managerResult.Message);

            model.Version = $"{random.Next(0, 9)}";
            model.PathToPlatform = nameof(Test);

            managerResult = platformManager.AddNewPlatformVersionReference(model);

            Assert.IsNotNull(managerResult);
            Assert.IsFalse(managerResult.Error, $"При добавлении произошла ошибка {managerResult.Message}");
            Assert.IsNull(managerResult.Message);
        }
    }
}
