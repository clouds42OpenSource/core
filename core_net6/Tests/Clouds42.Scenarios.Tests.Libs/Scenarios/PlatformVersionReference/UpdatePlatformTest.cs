﻿using System;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.Contracts.PlatformVersion.Interfaces;
using Clouds42.Segment.PlatformVersion.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PlatformVersionReference
{
    /// <summary>
    ///     Тест на редактирование объекта в справочнике Платформа 1С
    /// </summary>
    public class UpdatePlatformTest : ScenarioBase
    {
        public override void Run()
        {
            var random = new Random();
            var stable82Version = $"8.2.{random.Next(0, 9999)}.{random.Next(0, 9999)}";
            var stable82Path = $@"C:\V82Path\{stable82Version}";
            var createPlatformVersionCommand = new CreatePlatformVersionReferenceCommand(TestContext,
                new PlatformVersion1CDto {Version = stable82Version, PathToPlatform = stable82Path});
            createPlatformVersionCommand.Run();

            var platformVersionProvider = ServiceProvider.GetRequiredService<IPlatformVersionProvider>();
            var platformVersionManager = ServiceProvider.GetRequiredService<PlatformVersionManager>();
            
            var v82Platform = platformVersionProvider.GetPlatformVersion(createPlatformVersionCommand.Version);

            Assert.IsNotNull(v82Platform);
            Assert.AreEqual(createPlatformVersionCommand.Version, v82Platform.Version);
            Assert.AreEqual(stable82Path, v82Platform.PathToPlatform);

            var updatedModel =
                new PlatformVersion1CDto{Version = v82Platform.Version, PathToPlatform = v82Platform.PathToPlatform };

            var newPath = $@"d:\best\{random.Next(0, 9999)}\test";
            updatedModel.PathToPlatform = newPath;

            var managerResult = platformVersionManager.EditExistedPlatformVersionReference(updatedModel);

            var existedPlatform = DbLayer.PlatformVersionReferencesRepository.FirstOrDefault(
                    x => x.Version == updatedModel.Version);

            Assert.IsNotNull(managerResult);
            Assert.IsNotNull(existedPlatform);
            Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании платформы {updatedModel.Version}. Текст ошибки: {managerResult.Message}");
            Assert.AreEqual(v82Platform.Version, existedPlatform.Version);
            Assert.AreEqual(updatedModel.Version, existedPlatform.Version);
            Assert.AreEqual(updatedModel.PathToPlatform, existedPlatform.PathToPlatform);
        }
    }
}
