﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseCardManagerTest
{
    /// <summary>
    /// Сценарий для проверки получения данных о тех. поддержке инф. базы
    /// Действия:
    /// 1. Создадим аккаунт и инф. базу. [AccountAndDbCreation]
    /// 2. Проверим, что полученная модель поддержки инф. базы валидна относительно созданной. [CheckGettingAcDbSupportData]
    /// 3. Создадим записи истории поддержки инф. базы и проверим, что они будут корректно получены. [CheckGettingAcDbSupportHistory]
    /// 4. Очистим записи о тех. поддержке инф. базы и проверим, что будет получена модель при отстутсвии данных о поддержке. [CheckNewAcDbSupportWillBeCreated]
    /// 5. Проверим, что будет получена модель данных о поддержке при передаче несуществующего Id инф. базы. [CheckEmptyDbGuid]
    /// </summary>
    public class ScenarioGetDatabaseCardAcDbSupportDataSuccessAndFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;
        private readonly AcDbSupportTestHelper _acDbSupportTestHelper;

        public ScenarioGetDatabaseCardAcDbSupportDataSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseCardManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
            _acDbSupportTestHelper = new AcDbSupportTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountAndDbCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;
            
            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 1, "Бухгалтерия предприятия 3.0")[0];

            #endregion

            #region CheckGettingAcDbSupportData

            var acDbSupport =
                DbLayer.AcDbSupportRepository.FirstOrDefault(acDb => acDb.AccountDatabasesID == databaseId) ??
                throw new NotFoundException($"Не удалось найти созданную тех поддержку инф базы {databaseId}");

            var result = _accountDatabaseCardManager.GetDatabaseCardAcDbSupportData(databaseId);
            if (!CheckAcDbSupportDataIsValid(acDbSupport, result.Result, out var errorMessage) || result.Error)
                throw new InvalidOperationException(errorMessage);

            #endregion

            #region CheckGettingAcDbSupportHistory

            _acDbSupportTestHelper.CreateAcDbSupportHistory(databaseId, DatabaseSupportOperation.AuthToDb);
            _acDbSupportTestHelper.CreateAcDbSupportHistory(databaseId, DatabaseSupportOperation.PlanTehSupport);

            var acDbSupportHistory =
                DbLayer.AcDbSupportHistoryRepository.Where(acDbHistory => acDbHistory.AccountDatabaseId == databaseId);

            result = _accountDatabaseCardManager.GetDatabaseCardAcDbSupportData(databaseId);
            if (result.Result.AcDbSupportHistories.Count != acDbSupportHistory.Count())
                throw new InvalidOperationException("Не получена вся история тех поддержки инф. базы");

            #endregion

            #region CheckNewAcDbSupportWillBeCreated

            ClearAcDbSupportRecords(databaseId);
            result = _accountDatabaseCardManager.GetDatabaseCardAcDbSupportData(databaseId);
            CheckSupportDataModelWithAbsenceSupportReceived(databaseId, result.Result);

            #endregion

            #region CheckEmptyDbGuid

            result = _accountDatabaseCardManager.GetDatabaseCardAcDbSupportData(databaseId);
            CheckSupportDataModelWithAbsenceSupportReceived(Guid.NewGuid(), result.Result);

            #endregion
        }

        /// <summary>
        /// Проверить полученную информацию тех. поддержки на валидность
        /// </summary>
        /// <param name="acDbSupport">Поддержка инф. базы</param>
        /// <param name="acDbSupportDataDto">Модель данных поддержки инф. базы</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Признак успешной валидации</returns>
        private bool CheckAcDbSupportDataIsValid(AcDbSupport acDbSupport,
            DatabaseCardAcDbSupportDataDto acDbSupportDataDto, out string errorMessage)
        {
            errorMessage = "";

            if (acDbSupportDataDto.DatabaseId != acDbSupport.AccountDatabasesID)
                errorMessage = "Не совпадают DatabaseId у полученной тех поддержки из бд и из менеджера";

            if (acDbSupportDataDto.Login != acDbSupport.Login)
                errorMessage = "Не совпадают Login у полученной тех поддержки из бд и из менеджера";

            if (acDbSupportDataDto.Password != acDbSupport.Password)
                errorMessage = "Не совпадают Password у полученной тех поддержки из бд и из менеджера";

            if ((int)acDbSupportDataDto.SupportState != acDbSupport.State)
                errorMessage = "Не совпадают State у полученной тех поддержки из бд и из менеджера";

            return string.IsNullOrEmpty(errorMessage);
        }

        /// <summary>
        /// Очистить записи о тех. поддержке инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        private void ClearAcDbSupportRecords(Guid databaseId)
        {
            var allAcDbSupportRecords = DbLayer.AcDbSupportRepository.Where(acDb => acDb.AccountDatabasesID == databaseId);
            DbLayer.AcDbSupportRepository.DeleteRange(allAcDbSupportRecords);
            DbLayer.Save();
        }

        /// <summary>
        /// Проверить модель данных поддержки при отсутствии полученной поддержки
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="acDbSupportDataDto">Модель данных поддержки инф. базы</param>
        private void CheckSupportDataModelWithAbsenceSupportReceived(Guid databaseId, DatabaseCardAcDbSupportDataDto acDbSupportDataDto)
        {
            var acDbSupport =
                DbLayer.AcDbSupportRepository.FirstOrDefault(acDb => acDb.AccountDatabasesID == databaseId);
            if (acDbSupport != null)
                throw new InvalidOperationException("Были созданы записи о тех поддержке при получении модели");

            if (acDbSupportDataDto.SupportState != SupportState.NotAutorized)
                throw new InvalidOperationException("Неверно получен статус тех поддержки модели");
        }
    }
}
