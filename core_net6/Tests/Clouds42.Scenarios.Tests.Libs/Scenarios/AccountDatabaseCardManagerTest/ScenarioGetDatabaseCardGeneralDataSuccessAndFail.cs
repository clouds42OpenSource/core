﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Data.Constants;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseCardManagerTest
{
    /// <summary>
    /// Сценарий для проверки получения данных карточки инф. базы
    /// Действия:
    /// 1. Создадим аккаунт и инф. базу. [AccountAndDbCreation]
    /// 2. Получим данные карточки инф. базы и данные для сравнения. [GetResultData]
    /// 3. Проверим, что получены верны данные. [CheckDataCorrectness]
    /// 4. Проверим, что данные будут получены для невалидного Id аккаунта и не будут получены для невалидного Id инф. базы. [CheckNotValidData]
    /// </summary>
    public class ScenarioGetDatabaseCardGeneralDataSuccessAndFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;
        private readonly DatabasePlatformVersionHelper _databasePlatformVersionHelper;
        private readonly AccountDatabaseDataHelper _accountDatabaseDataHelper;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public ScenarioGetDatabaseCardGeneralDataSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseCardManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
            _databasePlatformVersionHelper = (DatabasePlatformVersionHelper)TestContext.ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();
            _accountDatabaseDataHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseDataHelper>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
        }

        public override void Run()
        {
            #region AccountAndDbCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountId = createAccountCommand.AccountId;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 1)[0];

            _accountDatabasesTestHelper.CreateDbOnDelimiters(databaseId, "unf");

            #endregion

            #region GetResultData

            var database = DbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == databaseId);
            var segment = GetAccountSegment(account.Id);
            var accDbSupport = DbLayer.AcDbSupportRepository.FirstOrDefault(sup => sup.AccountDatabasesID == databaseId);
            var dbOnDelimiters =
                DbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(dbDelim =>
                    dbDelim.AccountDatabaseId == databaseId);

            var result = _accountDatabaseCardManager.GetDatabaseCardGeneralData(databaseId, accountId);
            if (result.Error)
                throw new InvalidOperationException(result.Message);

            var data = result.Result;

            #endregion

            #region CheckDataCorrectness

            CompareReceivedFieldWithNecessary(data.TemplatePlatform, database.DbTemplate.PlatformType,
                "TemplatePlatform");

            CompareReceivedFieldWithNecessary(data.V82Name, database.V82Name,
                "V82Name");

            CompareReceivedFieldWithNecessary(data.PlatformType, database.PlatformType,
                "PlatformType");

            CompareReceivedFieldWithNecessary(data.DbNumber, database.DbNumber,
                "DbNumber");

            CompareReceivedFieldWithNecessary(data.V82Server, segment.CloudServicesEnterpriseServer83.ConnectionAddress,
                "V82Server");

            CompareReceivedFieldWithNecessary(data.SqlServer, segment.CloudServicesSQLServer.ConnectionAddress,
                nameof(SqlServer));

            CompareReceivedFieldWithNecessary(data.DbTemplate, database.DbTemplate.Id.ToString(),
                "DbTemplate");

            CompareReceivedFieldWithNecessary(data.DatabaseState, database.StateEnum,
                "DatabaseState");

            CompareReceivedFieldWithNecessary(data.FileStorageId, segment.FileStorageServersID,
                "FileStorageId");

            CompareReceivedFieldWithNecessary(data.IsDbOnDelimiters, database.IsDelimiter(),
                "IsDbOnDelimiters");

            CompareReceivedFieldWithNecessary(data.DistributionType, database.DistributionTypeEnum,
                "DistributionType");

            CompareReceivedFieldWithNecessary(data.Alpha83Version, _databasePlatformVersionHelper.GetAlpha83VersionFromSegment(accountId),
                "Alpha83Version");

            CompareReceivedFieldWithNecessary(data.Stable83Version, _databasePlatformVersionHelper.GetStable83VersionFromSegment(database),
                "Stable83Version");

            CompareReceivedFieldWithNecessary(data.Stable82Version, _databasePlatformVersionHelper.GetStable82VersionFromSegment(accountId),
                "Stable82Version");

            CompareReceivedFieldWithNecessary(data.ZoneNumber, dbOnDelimiters.Zone,
                "ZoneNumber");

            CompareReceivedFieldWithNecessary(data.RestoreModelType, database.RestoreModelType,
                "RestoreModelType");

            CompareReceivedFieldWithNecessary(data.PublishState, database.PublishStateEnum,
                "PublishState");

            CompareReceivedFieldWithNecessary(data.Id, databaseId,
                "Id");

            CompareReceivedFieldWithNecessary(data.Caption, database.Caption,
                "Caption");

            if (accDbSupport != null)
                CompareReceivedFieldWithNecessary(data.Version, accDbSupport.CurrentVersion,
                    nameof(Version));

            CompareReceivedFieldWithNecessary(data.ConfigurationName, AccountDatabaseDataProviderConstants.ClearTemplateConfigurationName,
                "ConfigurationName");

            CompareReceivedFieldWithNecessary(data.TemplateName, dbOnDelimiters.DbTemplateDelimiter.Name,
                "TemplateName");

            CompareReceivedFieldWithNecessary(data.FileStorageName, segment.CloudServicesFileStorageServer.Name,
                "FileStorageName");

            CompareReceivedFieldWithNecessary(data.DatabaseConnectionPath, _accountDatabaseDataHelper.GetDatabaseConnectionPath(database),
                "DatabaseConnectionPath");

            CompareReceivedFieldWithNecessary(data.FilePath, _accountDatabasePathHelper.GetPath(database),
                "FilePath");

            #endregion

            #region CheckNotValidData

            result = _accountDatabaseCardManager.GetDatabaseCardGeneralData(Guid.NewGuid(), accountId);
            if (!result.Error && result.Result != null)
                throw new InvalidOperationException("Получили данные карточки при невалидном Id инф. базы");

            result = _accountDatabaseCardManager.GetDatabaseCardGeneralData(databaseId, Guid.NewGuid());
            if (result.Error || result.Result == null)
                throw new InvalidOperationException("Не получили данные карточки при невалидном Id аккаунта");

            #endregion
        }

        /// <summary>
        /// Сравнить значение полученного поля и данных поля, которое должно было выбраться
        /// </summary>
        /// <typeparam name="T">Тип поля</typeparam>
        /// <param name="receivedFieldData">Полученные данные</param>
        /// <param name="necessaryFieldData">Нужные данные</param>
        /// <param name="fieldName">Название поля</param>
        private void CompareReceivedFieldWithNecessary<T>(T receivedFieldData, T necessaryFieldData, string fieldName)
        {
            if(!receivedFieldData.Equals(necessaryFieldData))
                GenerateMessageAndThrowException(fieldName);
        }

        /// <summary>
        /// Сгенерировать сообщение об ошибке и выбросить исключение
        /// </summary>
        /// <param name="fildName">Название поля</param>
        private void GenerateMessageAndThrowException(string fildName) =>
            throw new InvalidOperationException($"Неправильно выбралось поле {fildName}");
    }
}
