﻿using System;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseCardManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного получения бэкапов инф. баз
    /// Действия:
    /// 1. Создадим аккаунт, инф. базу и бэкапы данной базы. [AccountDbAndBackupsCreation]
    /// 2. Проверим, что бэкапы инф. баз будут корректно получены для админа аккаунта. [GetBackupsForAdminUser]
    /// 3. Проверим, что бэкапы инф. баз не будут получены для невалидного Id инф. базы. [CheckEmptyDbId]
    /// 4. Проверим, что бэкапы инф. баз не будут получены для невалидных дат в фильтре. [CheckNotValidDate]
    /// 5. Проверим, что бэкапы инф. баз не будут получены для обычного пользователя аккаунта. [CheckGettingBackupsForNotAdminUser]
    /// </summary>
    public class ScenarioGetDatabaseCardDbBackupsDataByFilterSuccessAndFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountDatabaseCardManager _accountDatabaseCardManager;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;

        public ScenarioGetDatabaseCardDbBackupsDataByFilterSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountDatabaseCardManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
        }

        public override void Run()
        {
            #region AccountDbAndBackupsCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;
            
            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 1)[0];

            _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId, CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));
            _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId, CreateBackupAccountDatabaseTrigger.FromApi, DateTime.Now.AddMinutes(2));
            _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId, CreateBackupAccountDatabaseTrigger.TehSupportAccountDatabase, DateTime.Now.AddMinutes(3));


            #endregion

            #region GetBackupsForAdminUser

            var filter = new DatabaseCardDbBackupsFilterDto
            {
                CreationDateFrom = DateTime.Today,
                CreationDateTo = DateTime.Now.AddHours(1),
                DatabaseId = databaseId
            };

            var result = _accountDatabaseCardManager.GetDatabaseCardDbBackupsDataByFilter(filter);

            if (result.Result.DbBackupsCount != 2)
                throw new InvalidOperationException("Не получили нужное количество бэкапов для админа аккаунта");

            #endregion

            #region CheckEmptyDbId

            filter.DatabaseId = Guid.NewGuid();

            result = _accountDatabaseCardManager.GetDatabaseCardDbBackupsDataByFilter(filter);
            if (result.Result.DbBackupsCount != 0)
                throw new InvalidOperationException("Получили бэкапы инф. баз для невалидного Id базы");

            #endregion

            #region CheckNotValidDate

            filter.CreationDateFrom = DateTime.Now;
            filter.CreationDateTo = DateTime.Now.AddDays(-1);
            filter.DatabaseId = databaseId;

            result = _accountDatabaseCardManager.GetDatabaseCardDbBackupsDataByFilter(filter);
            if (result.Result.DbBackupsCount != 0)
                throw new InvalidOperationException("Получили бэкапы инф. баз для невалидных дат");

            #endregion

            #region CheckGettingBackupsForNotAdminUser

            filter.CreationDateFrom = DateTime.Today;
            filter.CreationDateTo = DateTime.Now.AddHours(1);

            var notAdminAccountUser = _accountUserTestHelper.GetOrCreateUserWithAccountUserRole(accountId);
            _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, notAdminAccountUser.Id, CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase, DateTime.Now.AddMinutes(4));
            _accountUserTestHelper.DeleteAccountUser(createAccountCommand.AccountAdminId);

            result = _accountDatabaseCardManager.GetDatabaseCardDbBackupsDataByFilter(filter);
            if (result.Result.DbBackupsCount != 0)
                throw new InvalidOperationException("Получили бэкапы инф. баз для обычного пользователя аккаунта");

            #endregion
        }
    }
}
