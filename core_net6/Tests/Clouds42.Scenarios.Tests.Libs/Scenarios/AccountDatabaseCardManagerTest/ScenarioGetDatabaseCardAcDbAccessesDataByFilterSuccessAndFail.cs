﻿using System;
using System.Threading.Tasks;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseCardManagerTest
{
    /// <summary>
    /// Сценарий для проверки успешного и неуспешного получения доступов к инф. базам
    /// Действия:
    /// 1. Создадим аккаунт, пользователя с доступом и инф. базу. [AccountAndDbCreation]
    /// 2. Проверим, что корректно будут получены данные доступов для внутренних пользователям по разным фильтрам. [CheckGettingAcDbAccessForInternalUsers]
    /// 3. Создадим второй аккаунт с доступом пользователя к базе первого аккаунта. [AccountTwoCreation]
    /// 4. Проверим, что корректно будут получены данные доступов для внешних пользователей. [CheckGettingAcDbAccessForExternalUsers]
    /// 5. Проверим, что данные доступов не будут получены для невалидных полей фильтра. [CheckNotValidFilter]
    /// </summary>
    public class ScenarioGetDatabaseCardAcDbAccessesDataByFilterSuccessAndFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioGetDatabaseCardAcDbAccessesDataByFilterSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            #region AccountAndDbCreation

            var createAccountCommandOne = new CreateAccountCommand(TestContext);
            createAccountCommandOne.Run();

            var accountOneId = createAccountCommandOne.AccountId;
            
            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountOneId, 1)[0];
            
            var accountOneUserId = _accountUserTestHelper.CreateAccountUser(accountOneId);

            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountOneId, Guid.NewGuid(), accountOneUserId);

            #endregion

            #region CheckGettingAcDbAccessForInternalUsers

            var query = new GetAccountDatabaseUsersAccessQuery
            {
                DatabaseId = databaseId,
                SearchString = AccountUserTestHelper.LoginPattern,
                UsersByAccessFilterType = UsersByAccessFilterTypeEnum.All
            };

            var result = await Mediator.Send(query);

            if (result.Result.DatabaseAccesses.Count != 1 && result.Result.DatabaseAccesses[0].UserId != accountOneUserId)
                throw new InvalidOperationException("Неверно выбрался доступ для инф. базы");

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.WithAccess;
            
            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 1 && result.Result.DatabaseAccesses[0].UserId != accountOneUserId)
                throw new InvalidOperationException("Неверно выбрался доступ для инф. базы для пользователей с доступом");

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.WithoutAccess;
            
            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 0)
                throw new InvalidOperationException("Выбрались доступы для пользователей без доступа");

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.ExternalUsers;

            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 0)
                throw new InvalidOperationException("Выбрались доступы для внешних пользователей, которые не были созданы");

            #endregion

            #region AccountTwoCreation

            var createAccountCommandTwo = new CreateAccountCommand(TestContext);
            createAccountCommandTwo.Run();

            var accountTwoId = createAccountCommandTwo.AccountId;
            var accountTwoUserId = _accountUserTestHelper.CreateAccountUser(accountTwoId);

            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountTwoId, Guid.NewGuid(),
                accountTwoUserId);

            #endregion

            #region CheckGettingAcDbAccessForExternalUsers

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.All;

            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 2)
                throw new InvalidOperationException("Неверно выбрались доступы для внешнего и внутреннего пользователя");

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.ExternalUsers;

            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 1)
                throw new InvalidOperationException("Неверно выбрались доступы для внешнего пользователя");

            #endregion

            #region CheckNotValidFilter

            query.UsersByAccessFilterType = UsersByAccessFilterTypeEnum.All;
            query.DatabaseId = Guid.NewGuid();

            result = await Mediator.Send(query);
            if (result.Result != null)
                throw new InvalidOperationException("Выбрались доступы для несуществующей инф. базы");

            query.DatabaseId = databaseId;
            query.SearchString = "aaaa";

            result = await Mediator.Send(query);
            if (result.Result.DatabaseAccesses.Count != 0)
                throw new InvalidOperationException("Выбрались доступы для невалидной строки поиска");

            #endregion
        }
    }
}
