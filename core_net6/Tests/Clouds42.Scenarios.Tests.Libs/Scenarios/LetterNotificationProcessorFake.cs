﻿using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.LetterNotification.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios
{
    /// <summary>
    /// Фейк процессор для работы с уведомлениями по почте
    /// </summary>
    public class LetterNotificationProcessorFake : ILetterNotificationProcessor
    {
        /// <summary>
        /// Попытаться выполнить уведомление по почте
        /// </summary>
        /// <typeparam name="TNotification">Тип уведомления</typeparam>
        /// <typeparam name="TModel">Входящая модель для уведомления</typeparam>
        /// <param name="model">Входящая модель</param>
        /// <returns>Результат выполнения уведомления</returns>
        /// <remarks>Заглушка. Метод всегда возвращает true</remarks>
        public bool TryNotify<TNotification, TModel>(TModel model) where TNotification : ILetterNotificationBase<TModel>
            where TModel : ILetterTemplateModel
        {
            return true;
        }
    }
}
