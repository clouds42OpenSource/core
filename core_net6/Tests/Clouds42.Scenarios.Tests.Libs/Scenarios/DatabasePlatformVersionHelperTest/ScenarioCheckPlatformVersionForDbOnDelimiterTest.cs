﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DatabasePlatformVersionHelperTest
{
    /// <summary>
    /// Сценарий теста проверки получения
    /// версии платформы для инф. базы на разделителях
    ///
    /// 1) Создаем аккаунт и базу на разделителях
    /// 2) Проверяем что версия платформы для базы определеяется по сегменту материнской базы
    /// </summary>
    public class ScenarioCheckPlatformVersionForDbOnDelimiterTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand =
                new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var databasePlatformVersionHelper = ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();
            var accountDb = DbLayer.DatabasesRepository.FirstOrDefault(db =>
                db.Id == createAccountDatabaseAndAccountCommand.AccountDatabaseId);

            var platformVersion = GetAccountSegmentForDatabaseOnDelimeter(accountDb)
                                      ?.Stable83Version ??
                                  throw new NotFoundException("Не удалось получить версию платформы для инф. базы");

            var receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb);

            Assert.AreEqual(platformVersion, receivedVersionPlatform.Version);
        }
    }
}