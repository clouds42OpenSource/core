﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DatabasePlatformVersionHelperTest
{
    /// <summary>
    /// Сценарий теста проверки получения
    /// версии платформы для демо базы на разделителях
    /// 
    /// 1) Создаем аккаунт и базу на разделителях демо
    /// 2) Создаем новый сегмент и служебный аккаунт для
    /// демо баз на разделителях
    /// 3) Проверяем что версия платформы для базы определеяется
    /// по сегменту служебного аккаунта для демо баз на разделителях
    /// </summary>
    public class ScenarioCheckPlatformVersionForDemoDbOnDelimiterTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);

            createAccountDatabaseAndAccountCommand.Run();

            var createAccountHelperTest = ServiceProvider.GetRequiredService<CreateAccountHelperTest>();

            var databasePlatformVersionHelper = ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var accountDb = DbLayer.DatabasesRepository.FirstOrDefault(db =>
                db.Id == createAccountDatabaseAndAccountCommand.AccountDatabaseId);

            var segment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault(s => s.ID == createSegmentCommand.Id) ??
                          throw new NotFoundException($"Не удалось найти сегмент по ID {createSegmentCommand.Id}");

            accountDb.AccountDatabaseOnDelimiter.IsDemo = true;
            var serviceAccountForDemoDelimiterDatabases =
                createAccountHelperTest.Create();

            DbLayer.AccountDatabaseDelimitersRepository.Update(accountDb.AccountDatabaseOnDelimiter);
            DbLayer.Save();
            
            var receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb, serviceAccountForDemoDelimiterDatabases.Id);

            Assert.AreEqual(segment.Stable83Version, receivedVersionPlatform.Version);
        }
    }
}
