﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DatabasePlatformVersionHelperTest
{
    /// <summary>
    /// Сценарий теста проверки получения
    /// версии платформы для файловой инф. базы
    ///
    /// 1) Создаем аккаунт и файловую инф. базу
    /// 2) Проверяем версию платформы для инф. базы меняя тип платформы
    /// </summary>
    public class ScenarioCheckPlatformVersionForFileDbTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext)
                {
                    IsFile = true
                });

            createAccountDatabaseAndAccountCommand.Run();
            var databasePlatformVersionHelper = ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();

            var accountDb =
                DbLayer.DatabasesRepository.FirstOrDefault(db =>
                    db.Id == createAccountDatabaseAndAccountCommand.AccountDatabaseId) ??
                throw new NotFoundException("Не удалось найти инф. базу");

            accountDb.ApplicationName = "8.2";
            DbLayer.DatabasesRepository.Update(accountDb);
            DbLayer.Save();

            var receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb);
            var segment = GetAccountSegment(accountDb.Account.Id);

            Assert.AreEqual(segment.Stable82PlatformVersionReference.Version, receivedVersionPlatform.Version);

            accountDb.ApplicationName = "8.3";
            DbLayer.DatabasesRepository.Update(accountDb);
            DbLayer.Save();

            receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb);

            Assert.AreEqual(segment.Stable83PlatformVersionReference.Version, receivedVersionPlatform.Version);
        }
    }
}