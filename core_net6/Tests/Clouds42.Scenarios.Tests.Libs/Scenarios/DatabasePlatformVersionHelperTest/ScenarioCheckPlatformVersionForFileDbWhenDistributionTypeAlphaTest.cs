﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DatabasePlatformVersionHelperTest
{
    /// <summary>
    /// Сценарий теста проверки получения
    /// версии платформы для файловой инф. базы
    /// у которой тип распространения "Альфа версия"
    ///
    /// 1) Создаем аккаунт и файловую инф. базу с типом распространения "Альфа версия"
    /// 2) Проверяем версию платформы для инф. базы
    /// </summary>
    public class ScenarioCheckPlatformVersionForFileDbWhenDistributionTypeAlphaTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, new InfoDatabaseDomainModelTest(TestContext)
            {
                IsFile = true
            });
            createAccountDatabaseAndAccountCommand.Run();
            var databasePlatformVersionHelper = ServiceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();

            var accountDb =
                DbLayer.DatabasesRepository.FirstOrDefault(db =>
                    db.Id == createAccountDatabaseAndAccountCommand.AccountDatabaseId) ??
                throw new NotFoundException("Не удалось найти инф. базу");

            accountDb.DistributionTypeEnum = DistributionType.Alpha;

            var alpha83PlatformVersionReference = DbLayer.PlatformVersionReferencesRepository.FirstOrDefault();
            var segment = GetAccountSegment(accountDb.AccountId);
            segment.Alpha83PlatformVersionReference = alpha83PlatformVersionReference;
            DbLayer.DatabasesRepository.Update(accountDb);
            DbLayer.Save();

            var receivedVersionPlatform = databasePlatformVersionHelper.GetPlatformVersion(accountDb);

            Assert.AreEqual(alpha83PlatformVersionReference.Version, receivedVersionPlatform.Version);
        }
    }
}
