﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseReportData
{
    /// <summary>
    /// Сценарий теста выборки списка инф. баз для отчета
    /// </summary>
    public class ScenarioSamplingAccountDbReportDataTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly AccountDatabaseReportDataManager _accountDatabaseReportDataManager;

        public ScenarioSamplingAccountDbReportDataTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            _accountDatabaseReportDataManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseReportDataManager>();
        }

        public override void Run()
        {
            var configuration1C = DbLayer.Configurations1CRepository.FirstOrDefault();
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var anotherAccount = _testDataGenerator.GenerateAccount();
            DbLayer.AccountsRepository.Insert(anotherAccount);
            DbLayer.Save();

            var accountUsers = _testDataGenerator.GenerateListAccountUsersByCount(5, anotherAccount.Id);
            accountUsers.SaveAccountUsers(DbLayer);

            var accountDatabases = _testDataGenerator.GenerateListAccountDatabasesByCount(10,
                createAccountCommand.AccountId, 1, createAccountCommand.Account.IndexNumber);

            accountDatabases[0].IsFile = null;
            accountDatabases[1].PublishStateEnum = PublishState.PendingUnpublication;

            accountDatabases.SaveAccountDatabases(DbLayer);

            var acdbAccessOne = _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId,
                createAccountCommand.AccountAdminId, accountDatabases[2].Id);

            var acdbAccessTwo = _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId,
                accountUsers[0].Id, accountDatabases[2].Id);

            var acdbAccessThree = _testDataGenerator.GenerateAcDbAccess(createAccountCommand.AccountId,
                accountUsers[1].Id, accountDatabases[2].Id);

            DbLayer.AcDbAccessesRepository.Insert(acdbAccessOne);
            DbLayer.AcDbAccessesRepository.Insert(acdbAccessTwo);
            DbLayer.AcDbAccessesRepository.Insert(acdbAccessThree);
            DbLayer.Save();

            var acDbSupportOne = _testDataGenerator.GenerateAcDbSupport(accountDatabases[0].Id, configuration1C.Name,
                supportState: SupportState.AutorizationFail);
            var acDbSupportTwo = _testDataGenerator.GenerateAcDbSupport(accountDatabases[1].Id, configuration1C.Name, false);
            DbLayer.AcDbSupportRepository.Insert(acDbSupportOne);
            DbLayer.AcDbSupportRepository.Insert(acDbSupportTwo);
            DbLayer.Save();

            var managerResult = _accountDatabaseReportDataManager.GetAccountDatabaseReportDataDcs();

            if (managerResult.Error)
                throw new InvalidOperationException(managerResult.Message);

            if (managerResult.Result.Count != 10)
                throw new InvalidOperationException("Не верно работает выборка инф. баз");

            var findDb = managerResult.Result.FirstOrDefault(w => w.DatabaseNumber == accountDatabases[0].V82Name) ??
                         throw new InvalidOperationException("Не найдена база по номеру");

            if (findDb.DatabaseType != "Серверная база" || findDb.HasAutoUpdate || findDb.HasSupport)
                throw new InvalidOperationException("Не корректно определяется тип базы или поля поддержки базы"); 

            findDb = managerResult.Result.FirstOrDefault(w => w.DatabaseNumber == accountDatabases[1].V82Name) ??
            throw new InvalidOperationException("Не найдена база по номеру");

            if (findDb.IsPublished || findDb.HasAutoUpdate || !findDb.HasSupport)
                throw new InvalidOperationException("Не корректно определяется флаг публикации или флаги поддержки базы");

            findDb = managerResult.Result.FirstOrDefault(w => w.DatabaseNumber == accountDatabases[2].V82Name) ??
                     throw new InvalidOperationException("Не найдена база по номеру");

            if (findDb.ExternalAccessesToDatabaseCount != 2 || findDb.InternalAccessesToDatabaseCount != 1)
                throw new InvalidOperationException("Не корректно определяются доступы к инф. базе");
        }
    }
}
