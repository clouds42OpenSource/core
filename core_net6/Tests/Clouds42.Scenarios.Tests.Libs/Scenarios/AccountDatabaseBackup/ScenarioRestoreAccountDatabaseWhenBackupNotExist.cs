﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий проверки восстановления базы, когда бэкапа физически не существует
    ///     Действия:
    ///         1) Создаем аккаунт и файловую базу
    ///         2) Создаем бэкап инф. базы
    ///         3) Удаляем файл бэкапа
    ///         4) Проверяем что база не восстановлена и рабочий процесс не запустился
    /// </summary>
    public class ScenarioRestoreAccountDatabaseWhenBackupNotExist : ScenarioBase
    {
        private readonly IRestoreAccountDatabaseBackupManager _restoreAccountDatabaseBackupManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;

        public ScenarioRestoreAccountDatabaseWhenBackupNotExist()
        {
            _restoreAccountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<IRestoreAccountDatabaseBackupManager>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var accountDatabasePath = _accountDatabasePathHelper.GetPath(createAccountDatabaseCommand.AccountDatabaseId);

            if (!DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath))
                throw new NotFoundException("Файл базы не найден на хранилище.");

            if (accountDatabasePath.ToDirectoryInfo().GetSize() == 0)
                throw new InvalidOperationException($"Папка с базой пустая {accountDatabasePath}");

            var createAccountDatabaseBackupCommand =
                new CreateAccountDatabaseBackupCommand(TestContext, createAccountDatabaseCommand);
            createAccountDatabaseBackupCommand.Run();

            var accountDatabaseBackup = DbLayer.AccountDatabaseBackupRepository
                .FirstOrDefault(w => w.AccountDatabaseId == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(accountDatabaseBackup, "Не найден бэкап для базы, поведение не верно");

            File.Delete(accountDatabaseBackup.BackupPath);
            Assert.IsFalse(File.Exists(accountDatabaseBackup.BackupPath));

            var restoreFromTomb = _restoreAccountDatabaseBackupManager.Restore(new RestoreAccountDatabaseFromTombWorkerTaskParam
            {
                RestoreType = AccountDatabaseBackupRestoreType.ToCurrent,
                AccountDatabaseBackupId = accountDatabaseBackup.Id,
                SendMailAboutError = true,
                NeedChangeState = true
            });

            Assert.IsTrue(restoreFromTomb.Error, "Файл бэкапа удален. Должна была вернуться ошибка");

            var processFlows = DbLayer.ProcessFlowRepository.All();
            Assert.IsFalse(processFlows.Any(), "Был запущен рабочий процесс. Поведение системы не верно");
        }
    }
}
