﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Backup;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий для проверки обновления уже существующей записи о бэкапе
    /// Действия
    ///     1) Создаем запись о бэкапе
    ///     2) Имитируем обращение к методу АПИ для регистрации бэкапа
    ///     3) Проверяем что в существующей записи о бэкапе поменялся путь
    /// </summary>
    public class ScenarioUpdateExistingBackupRecord : ScenarioBase
    {
        private readonly AccountDatabaseBackupManager _accountDatabaseBackupManager;

        public ScenarioUpdateExistingBackupRecord()
        {
            _accountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseBackupManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта, инф. базы и бэкапа

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var createAccountDatabaseBackupCommand =
                new CreateAccountDatabaseBackupCommand(TestContext, createAccountDatabaseCommand);
            createAccountDatabaseBackupCommand.Run();

            #endregion

            #region Имитация обращения на АПИ

            var accountDatabaseBackup = GetAccountDatabaseBackup(createAccountDatabaseCommand.AccountDatabaseId);
            Assert.IsNotNull(accountDatabaseBackup, "Не найден бэкап для базы, поведение не верно");

            var smBackupId = CreateSmBackupRecord(accountDatabaseBackup.Id);
            var backupPath = CreateBackupFile(createAccountCommand.Account);

            var result = _accountDatabaseBackupManager.RegisterBackup(new RegisterAccountDatabaseBackupRequestDto
            {
                ServiceManagerAcDbBackupId = smBackupId,
                AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                CreationBackupDateTime = DateTime.Now,
                State = AccountDatabaseBackupHistoryState.Done,
                Trigger = CreateBackupAccountDatabaseTrigger.RegulationBackup,
                BackupFullPath = backupPath
            });

            Assert.IsFalse(result.Error);

            #endregion

            var acDbBackupAfterUpdateAction = GetAccountDatabaseBackup(createAccountDatabaseCommand.AccountDatabaseId);
            Assert.IsNotNull(acDbBackupAfterUpdateAction);
            Assert.AreEqual(acDbBackupAfterUpdateAction.BackupPath, backupPath, "Бэкап инф. базы не был изменен, поведение не верно");
        }

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        private Domain.DataModels.AccountDatabaseBackup GetAccountDatabaseBackup(Guid accountDatabaseId)
            => DbLayer.AccountDatabaseBackupRepository
                .FirstOrDefault(w => w.AccountDatabaseId == accountDatabaseId);

        /// <summary>
        /// Создать запись о связи бэкапа инф. базы с бэкапом МС
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>ID бэкапа инф. базы от МС</returns>
        private Guid CreateSmBackupRecord(Guid accountDatabaseBackupId)
        {
            var smBackupId = Guid.NewGuid();

            var smBackupRecord = new ServiceManagerAcDbBackup
            {
                AccountDatabaseBackupId = accountDatabaseBackupId,
                ServiceManagerAcDbBackupId = smBackupId
            };

            DbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().Insert(smBackupRecord);
            DbLayer.Save();

            return smBackupId;
        }

        /// <summary>
        /// Создать файл бэкапа
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Путь к файлу бэкапа</returns>
        private string CreateBackupFile(IAccount account)
        {
            var backupStorage = TestContext.ServiceProvider.GetRequiredService<ISegmentHelper>().GetBackupStorage((Account)account);
            var backupDirectory = Path.Combine(backupStorage, "tomb", $"company_{account.IndexNumber}");
            var backupPath = Path.Combine(backupDirectory, $"{account.IndexNumber}_{DateTime.Now:yyyy-MM-dd-HH-mm}.zip");

            ServiceProvider.GetRequiredService<CreateZipFileHelper>().CreateZipFileWithDumpInfo(backupPath);

            return backupPath;
        }
    }
}
