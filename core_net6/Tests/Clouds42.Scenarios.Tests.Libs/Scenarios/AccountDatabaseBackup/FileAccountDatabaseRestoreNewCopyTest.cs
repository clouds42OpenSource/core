﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CoreWorker.Jobs;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий проверки восстановления ИБ в новую копию с файлового архива.
    /// </summary>
    public class FileAccountDatabaseRestoreNewCopyTest : ScenarioBase
    {
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;

        public FileAccountDatabaseRestoreNewCopyTest()
        {
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var сreateAccountDatabaseBackupCommand =
                new CreateAccountDatabaseBackupCommand(TestContext, createAccountDatabaseCommand);
            сreateAccountDatabaseBackupCommand.Run();

            var uploadedFileId =
                _createUploadedFileTestHelper.CreateUploadedFileDbRecord(createAccountCommand.AccountId);

            _createUploadedFileTestHelper.CreateUploadedFilePhysicallyInDbStorage(uploadedFileId, createAccountDatabaseCommand.AccountDatabaseId);

            var tombManager = TestContext.ServiceProvider.GetRequiredService<TombTaskCreatorManager>();

            tombManager.CreateTaskForRestoreAccountDatabaseFromTomb(new RestoreAccountDatabaseFromTombWorkerTaskParam
            {
                AccountDatabaseBackupId = сreateAccountDatabaseBackupCommand.Id,
                RestoreType = AccountDatabaseBackupRestoreType.ToNew,
                NeedChangeState = false,
                UploadedFileId = uploadedFileId
            });

            var queueItem = DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(t => t.CoreWorkerTask.TaskName == CoreWorkerTaskType.RestoreAccountDatabaseFromTombJob.ToString());

            if (queueItem == null)
                throw new InvalidOperationException("Ошибка создания задачи по разархивации бэкапа");

            new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            Services.AddTransient<IAccessProvider>();
            Services.AddTransient<IUnitOfWork>();

            var restoreAccountDatabaseFromTombJob = ServiceProvider.GetRequiredService<RestoreAccountDatabaseFromTombJob>();
            restoreAccountDatabaseFromTombJob.Execute(Guid.Empty, queueItem.Id);

            var databases = DbLayer.DatabasesRepository
                .Where(d => d.AccountId == createAccountDatabaseCommand.AccountDetails.AccountId);

            var newRestoredDb = databases.FirstOrDefault(d => d.Id != createAccountDatabaseCommand.AccountDatabaseId);

            if (newRestoredDb == null)
                throw new InvalidOperationException("Ошибка восстановления базы в новую копию. Записи о базе отсутсвует");
            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();
            
            var dbPath = new AccountDatabasePathHelper(DbLayer, Logger, segmentHelper, HandlerException).GetPath(newRestoredDb.Id);

            if (!DirectoryHelper.DirectoryHas1CDatabaseFile(dbPath))
                throw new InvalidOperationException("Ошибка восстановления базы в новую копию. Файл восстановленной базы не найден на хранилище.");
        }
    }
}
