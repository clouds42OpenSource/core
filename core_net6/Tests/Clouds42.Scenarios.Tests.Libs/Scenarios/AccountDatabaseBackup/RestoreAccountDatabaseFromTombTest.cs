﻿using System;
using Clouds42.AccountDatabase.Archiving.Managers;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.Contracts.Archiving.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий проверки восстановления базы из склепа
    ///     Действия:
    ///         1) Создаем аккаунт и файловую базу
    ///         2) Отправляем базу в архив
    ///         3) Проверяем что бэкап создался и уехал в склеп
    ///         4) Восстанавливаем базу из бэкапа
    ///         5) Проверяем что база восстановлена и размер директории совпадает с размером до архивации
    /// </summary>
    public class RestoreAccountDatabaseFromTombTest : ScenarioBase
    {
        private readonly IRestoreAccountDatabaseBackupManager _restoreAccountDatabaseBackupManager;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly ArchiveAccountDatabaseToTombManager _archiveAccountDatabaseToTombManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;

        public RestoreAccountDatabaseFromTombTest()
        {
            _restoreAccountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<IRestoreAccountDatabaseBackupManager>();
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _archiveAccountDatabaseToTombManager = (ArchiveAccountDatabaseToTombManager)TestContext.ServiceProvider.GetRequiredService<IArchiveAccountDatabaseToTombManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
        }

        public override void Run()
        {        
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var accountDatabasePath = _accountDatabasePathHelper.GetPath(createAccountDatabaseCommand.AccountDatabaseId);

            ValidateAccountDatabasePath(accountDatabasePath);
            var beforeSize = accountDatabasePath.ToDirectoryInfo().GetSize();

            var uploadedFileId =
                _createUploadedFileTestHelper.CreateUploadedFileDbRecord(createAccountCommand.AccountId);

            _createUploadedFileTestHelper.CreateUploadedFilePhysicallyInDbStorage(uploadedFileId, createAccountDatabaseCommand.AccountDatabaseId);

            var taskParams = new DeleteAccountDatabaseToTombJobParamsDto
            {
                AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                InitiatorId = createAccountCommand.AccountAdminId,
                ForceUpload = true,
                Trigger = CreateBackupAccountDatabaseTrigger.ManualRemoval
            };

            var sendToTomb = _archiveAccountDatabaseToTombManager.DetachAccountDatabaseToTomb(taskParams);
            Assert.IsFalse(sendToTomb.Error, sendToTomb.Message);

            RefreshDbCashContext(Context.AccountDatabaseBackups);

            var accountDatabaseBackup = DbLayer.AccountDatabaseBackupRepository
                .FirstOrDefault(w => w.AccountDatabaseId == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(accountDatabaseBackup, "Не найден бэкап для базы, поведение не верно");

            if (DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath))
                throw new NotFoundException("Файл базы найден на хранилище.");

            if (accountDatabasePath.ToDirectoryInfo().GetSize() != 0)
                throw new InvalidOperationException($"Папка с базой не пустая после архивации базы в склепе. {accountDatabasePath}");
            
            var restoreFromTomb =
                _restoreAccountDatabaseBackupManager.Restore(new RestoreAccountDatabaseFromTombWorkerTaskParam
                {
                    RestoreType = AccountDatabaseBackupRestoreType.ToCurrent,
                    AccountDatabaseBackupId = accountDatabaseBackup.Id,
                    SendMailAboutError = true,
                    NeedChangeState = true,
                    UploadedFileId = uploadedFileId
                });
            Assert.IsFalse(restoreFromTomb.Error, restoreFromTomb.Message);

            ValidateAccountDatabasePath(accountDatabasePath);

            var afterSize = accountDatabasePath.ToDirectoryInfo().GetSize();

            Assert.AreEqual(beforeSize, afterSize,
                $"Размер восстановленой базы отличается от размера перед сохранением в склепе. До:{beforeSize}, После:{afterSize}");
        }

        /// <summary>
        /// Провалидировать путь к инф. базе
        /// </summary>
        /// <param name="accountDatabasePath">Путь к инф. базе</param>
        private static void ValidateAccountDatabasePath(string accountDatabasePath)
        {
            if (!DirectoryHelper.DirectoryHas1CDatabaseFile(accountDatabasePath))
                throw new InvalidOperationException("Файл базы не найден на хранилище.");

            var size = accountDatabasePath.ToDirectoryInfo().GetSize();
            if (size == 0)
                throw new InvalidOperationException($"Папка с базой пустая. {accountDatabasePath}");
        }
    }
}