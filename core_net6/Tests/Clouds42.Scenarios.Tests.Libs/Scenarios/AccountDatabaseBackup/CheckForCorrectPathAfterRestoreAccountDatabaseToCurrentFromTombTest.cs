﻿using System;
using System.IO;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.CoreWorker.Jobs;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий проверки на правильный путь после восстановления базы в тоже место из склепа
    /// </summary>
    public class CheckForCorrectPathAfterRestoreAccountDatabaseToCurrentFromTombTest : ScenarioBase
    {
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;

        public CheckForCorrectPathAfterRestoreAccountDatabaseToCurrentFromTombTest()
        {
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);    
        }

        public override void Run()
        {
            
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountSegmentId = GetAccountSegmentId(createAccountCommand.AccountId);

            Assert.IsNotNull(accountSegmentId, "Ошибка создания аккаунта.");

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "NewStorage");
            var cloudFileStorageData = new CloudFilestorageElementViewModelTest(defaultStorage);
            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFileStorageData);
            createSegmentCommand.Run();

            var storage = await DbLayer.CloudServicesFileStorageServerRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(s => s.ConnectionAddress == defaultStorage);
            var segmentManager = ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();

            var managerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = accountSegmentId });
            Assert.IsFalse(managerResult.Error);
            Assert.IsNotNull(managerResult.Result);

            var editSegmentModel = managerResult.Result.MapToEditCloudServicesSegmentDto();
            editSegmentModel.SegmentFileStorageServers.Add(new SegmentElementDto
            {
                Id = storage.ID,
                Name = storage.Name
            });

            var editSegmentResult = segmentManager.EditSegment(editSegmentModel);
            Assert.IsFalse(editSegmentResult.Error);

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var createAccountDatabaseBackupCommand =
                         new CreateAccountDatabaseBackupCommand(TestContext, createAccountDatabaseCommand);
            createAccountDatabaseBackupCommand.Run();

            var db = await DbLayer.DatabasesRepository.AsQueryable()
             .FirstOrDefaultAsync(d => d.AccountId == createAccountDatabaseCommand.AccountDetails.AccountId);

            var uploadedFileId =
                _createUploadedFileTestHelper.CreateUploadedFileDbRecord(createAccountCommand.AccountId);

            _createUploadedFileTestHelper.CreateUploadedFilePhysicallyInDbStorage(uploadedFileId, db.Id);

            db.FileStorageID = storage.ID;

            DbLayer.DatabasesRepository.Update(db);
            await DbLayer.SaveAsync();

            var tombManager = TestContext.ServiceProvider.GetRequiredService<TombTaskCreatorManager>();
            tombManager.CreateTaskForRestoreAccountDatabaseFromTomb(new RestoreAccountDatabaseFromTombWorkerTaskParam
            {
                AccountDatabaseBackupId = createAccountDatabaseBackupCommand.Id,
                RestoreType = AccountDatabaseBackupRestoreType.ToCurrent,
                NeedChangeState = false,
                UploadedFileId = uploadedFileId
            });

            RefreshDbCashContext(TestContext.Context.CoreWorkerTasksQueues);

            var queueItem = await DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefaultAsync(t => t.CoreWorkerTask.TaskName == CoreWorkerTaskType.RestoreAccountDatabaseFromTombJob.ToString());
            Assert.IsNotNull(queueItem, "Ошибка создания задачи по разархивации бэкапа");

            new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            ServiceProvider.GetRequiredService<IAccessProvider>();

            var restoreAccountDatabaseFromTombJob = ServiceProvider.GetRequiredService<RestoreAccountDatabaseFromTombJob>();

            restoreAccountDatabaseFromTombJob.Execute(Guid.Empty, queueItem.Id);

            var segmentHelper = ServiceProvider.GetRequiredService<ISegmentHelper>();

            var dbPath = new AccountDatabasePathHelper(DbLayer, Logger, segmentHelper, HandlerException).GetPath(createAccountDatabaseCommand.AccountDatabaseId);

            if (!DirectoryHelper.DirectoryHas1CDatabaseFile(dbPath))
                throw new InvalidOperationException("Ошибка восстановления базы в новую копию. Файл восстановленной базы не найден на хранилище.");
        }
    }
}
