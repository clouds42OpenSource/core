﻿using System;
using Clouds42.AccountDatabase.Backup;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Сценарий удаления бэкапа инф. базы через АПИ когда указан не верный ID бэкапа от МС
    /// Действия:
    ///     1) Создаем инф. базу и бэкап
    ///     2) Имитируем удаление бэкапа через АПИ передвая заведомо не существующий ID бэкапа от МС
    ///     3) Проверяем что метод отработал, но бэкап не был удален
    /// </summary>
    public class ScenarioDeleteAcDbBackupThroughApiWithIncorrectSmId : ScenarioBase
    {
        private readonly AccountDatabaseBackupManager _accountDatabaseBackupManager;

        public ScenarioDeleteAcDbBackupThroughApiWithIncorrectSmId()
        {
            _accountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseBackupManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var createAccountDatabaseBackupCommand =
                new CreateAccountDatabaseBackupCommand(TestContext, createAccountDatabaseCommand);
            createAccountDatabaseBackupCommand.Run();

            var accountDatabaseBackup = GetAccountDatabaseBackup(createAccountDatabaseCommand.AccountDatabaseId);
            Assert.IsNotNull(accountDatabaseBackup, "Не найден бэкап для базы, поведение не верно");

            CreateSmBackupRecord(accountDatabaseBackup.Id);

            var result = _accountDatabaseBackupManager.DeleteBackup(new DeleteAccountDatabaseBackupRequestDto
            {
                ServiceManagerAcDbBackupId = Guid.NewGuid()
            });

            Assert.IsFalse(result.Error);

            var acDbBackupAfterDeleteAction = GetAccountDatabaseBackup(createAccountDatabaseCommand.AccountDatabaseId);
            Assert.IsNotNull(acDbBackupAfterDeleteAction, "Бэкап инф. базы не должен быть удален, поведение не верно");
        }

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        private Domain.DataModels.AccountDatabaseBackup GetAccountDatabaseBackup(Guid accountDatabaseId)
            => DbLayer.AccountDatabaseBackupRepository
                .FirstOrDefault(w => w.AccountDatabaseId == accountDatabaseId);

        /// <summary>
        /// Создать запись о связи бэкапа инф. базы с бэкапом МС
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>ID бэкапа инф. базы от МС</returns>
        private void CreateSmBackupRecord(Guid accountDatabaseBackupId)
        {
            var smBackupId = Guid.NewGuid();

            var smBackupRecord = new ServiceManagerAcDbBackup
            {
                AccountDatabaseBackupId = accountDatabaseBackupId,
                ServiceManagerAcDbBackupId = smBackupId
            };

            DbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().Insert(smBackupRecord);
            DbLayer.Save();
        }
    }
}
