﻿using Clouds42.AccountDatabase.Backup;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDatabaseBackup
{
    /// <summary>
    /// Тест регистрации бекапа
    /// Сценарий:
    ///         1. Создаем аккаунт с информационной базой
    ///         2. Создаем зип файл
    ///         3. Регистрируем его в качестве бекапа
    ///         4. Проверяем, что регистрация прошла успешно 
    /// </summary>
    public class AccountDatabaseRegisterBackupTest : ScenarioBase
    {
        private readonly AccountDatabaseBackupManager _accountDatabaseBackupManager;
        private readonly CreateZipFileHelper _createZipFileHelper;

        public AccountDatabaseRegisterBackupTest()
        {
            _accountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseBackupManager>();
            _createZipFileHelper = new CreateZipFileHelper();
        }

        public override void Run()
        {
            var accountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            accountDatabaseAndAccountCommand.Run();

            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(d =>
                    d.Id == accountDatabaseAndAccountCommand.AccountDatabaseId);

            Assert.IsNotNull(database);

            var serviceManagerBackupId = Guid.Parse("ED2499DD-7687-4763-B9A3-5786A3315705");
            var backupName = "SuperBackup";
            var backupPath = $"C:\\temp\\DbBackup\\{database.Id:N}\\";
            
            _createZipFileHelper.CreateEmptyZipFile(backupPath, backupName);

            var backupFullPath = Path.Combine(backupPath, backupName + ".zip");

            var accountDatabaseBackupRequestDto = new RegisterAccountDatabaseBackupRequestDto
            {
                Trigger = CreateBackupAccountDatabaseTrigger.FromApi,
                AccountDatabaseId = database.Id,
                Comment = "YourBackup",
                CreationBackupDateTime = DateTime.Now,
                State = AccountDatabaseBackupHistoryState.Done,
                ServiceManagerAcDbBackupId = serviceManagerBackupId,
                ServiceManagerAcDbBackupIdString = serviceManagerBackupId.ToString(),
                BackupFullPath = backupFullPath
            };

            var registerBackup = _accountDatabaseBackupManager.RegisterBackup(accountDatabaseBackupRequestDto);
            Assert.IsFalse(registerBackup.Error);

            var backup =
                DbLayer.AccountDatabaseBackupRepository.FirstOrDefault(adb => adb.AccountDatabaseId == database.Id);
            Assert.IsNotNull(backup);
            Assert.AreEqual(accountDatabaseBackupRequestDto.BackupFullPath, backup.BackupPath);

            DeleteZip(backupFullPath);
        }

        /// <summary>
        /// Удалить зип файл
        /// </summary>
        /// <param name="filePath">Путь до зип файла</param>
        private void DeleteZip(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            File.Delete(filePath);
        }
    }
}
