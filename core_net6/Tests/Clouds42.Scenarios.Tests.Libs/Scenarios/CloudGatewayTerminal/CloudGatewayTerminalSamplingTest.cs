﻿using System;
using System.Threading.Tasks;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Segment.CloudsServicesGatewayTerminal.Managers;
using Core42.Application.Features.GatewayTerminalContext.Queries;
using Core42.Application.Features.GatewayTerminalContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudGatewayTerminal
{
    /// <summary>
    /// Тест выборки терминальных шлюзов по фильтру
    /// Сценарий:
    ///         1. Создаем 3 терминальных шлюза
    ///         2. Делаем выборку по фильтрам, проверяем, что возвращается корректное количество записей
    /// </summary>
    public class CloudGatewayTerminalSamplingTest : ScenarioBase
    {
        private readonly CloudGatewayTerminalManager _cloudGatewayTerminalManager;

        public CloudGatewayTerminalSamplingTest()
        {
            _cloudGatewayTerminalManager = TestContext.ServiceProvider.GetRequiredService<CloudGatewayTerminalManager>();
        }

        public override void Run()
        {
           
        }

        public override async Task RunAsync()
        {
            var firstCloudGatewayTerminalDto = new CreateCloudGatewayTerminalDto
            {
                Name = "Terminal",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                Description = "Desc"
            };

            CreateCloudGatewayTerminal(firstCloudGatewayTerminalDto);

            var secondCloudGatewayTerminalDto = new CreateCloudGatewayTerminalDto
            {
                Name = "Name",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                Description = nameof(DateTime)
            };
            CreateCloudGatewayTerminal(secondCloudGatewayTerminalDto);

            var thirdCloudGatewayTerminalDto = new CreateCloudGatewayTerminalDto
            {
                Name = "DtosName",
                ConnectionAddress = $"adr_{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                Description = "Description"
            };
            CreateCloudGatewayTerminal(thirdCloudGatewayTerminalDto);



            var query = new GetFilteredGatewayTerminalsQuery
            {
                Filter = new GatewayTerminalFilter { Name = "Name" },
                PageNumber = 1
            };

            await GetFilteredCloudGatewayTerminals(2, query, GetData);

            query.Filter.Name = null;
            query.Filter.Description = "Desc";
            await GetFilteredCloudGatewayTerminals(2, query, GetData);

            query.Filter.Description = "Date";
            await GetFilteredCloudGatewayTerminals(1, query, GetData);

            query.Filter.Description = null;
            query.Filter.ConnectionAddress = "adr_";
            await GetFilteredCloudGatewayTerminals(1, query, GetData);
        }

        /// <summary>
        /// Создать терминальный шлюз
        /// </summary>
        /// <returns>Id терминального шлюза</returns>
        private void CreateCloudGatewayTerminal(CreateCloudGatewayTerminalDto cloudGatewayTerminalDto)
        {
            var createResult = _cloudGatewayTerminalManager.CreateCloudGatewayTerminal(cloudGatewayTerminalDto);
            Assert.IsFalse(createResult.Error, createResult.Message);
        }

        /// <summary>
        /// Получить данные терминальных шлюзов по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество терминальных шлюзов</param>
        /// <param name="filter">Фильтр</param>
        /// <param name="action">Действие, для выбора и проверки выборки терминальных шлюзов</param>
        private async Task GetFilteredCloudGatewayTerminals(int expectedCount, GetFilteredGatewayTerminalsQuery filter,
            Func<GetFilteredGatewayTerminalsQuery, int, Task> action)
        {
            await action(filter, expectedCount);
        }

        /// <summary>
        /// Получить данные терминальных шлюзов по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <param name="expectedCount">Ожидаемое количество терминальных шлюзов<</param>
        private async Task GetData(GetFilteredGatewayTerminalsQuery filter, int expectedCount)
        {
            var enterpriseServers = await Mediator.Send(filter);
            Assert.IsFalse(enterpriseServers.Error);
            Assert.AreEqual(expectedCount, enterpriseServers.Result.Records.Count);
        }
    }
}
