﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesGatewayTerminal.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudGatewayTerminal
{
    /// <summary>
    /// Тест создания/удаления/редактирования терминального шлюза
    /// Сценарий:
    ///         1. Создаем терминальный шлюз, проверяем, что создание прошло без ошибок 
    ///         2. Редактируем терминальный шлюз, проверяем, что редактирование прошло без ошибок 
    ///         3. Удаляем терминальный шлюз, проверяем, что удаление прошло без ошибок 
    /// </summary>
    public class CrudCloudGatewayTerminalTest : ScenarioBase
    {
        private readonly CloudGatewayTerminalManager _cloudGatewayTerminalManager;

        public CrudCloudGatewayTerminalTest()
        {
            _cloudGatewayTerminalManager = TestContext.ServiceProvider.GetRequiredService<CloudGatewayTerminalManager>();
        }

        public override void Run()
        {
            var cloudGatewayTerminalId = CreateCloudGatewayTerminal();
            EditCloudGatewayTerminal(cloudGatewayTerminalId);
            DeleteCloudGatewayTerminal(cloudGatewayTerminalId);
        }

        /// <summary>
        /// Создать терминальный шлюз
        /// </summary>
        /// <returns>Id терминального шлюза</returns>
        private Guid CreateCloudGatewayTerminal()
        {
            var cloudGatewayTerminalDto = new CreateCloudGatewayTerminalDto
            {
                Name = "Name",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}",
                Description = "Desc"
            };
            var createResult = _cloudGatewayTerminalManager.CreateCloudGatewayTerminal(cloudGatewayTerminalDto);
            Assert.IsFalse(createResult.Error, createResult.Message);

            return createResult.Result;
        }

        /// <summary>
        /// Редактировать терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalId">Id терминального шлюза</param>
        private void EditCloudGatewayTerminal(Guid cloudGatewayTerminalId)
        {
            var cloudContentServer = _cloudGatewayTerminalManager.GetCloudGatewayTerminalDto(cloudGatewayTerminalId);
            Assert.IsFalse(cloudContentServer.Error, cloudContentServer.Message);

            var cloudGatewayTerminalDto = cloudContentServer.Result;

            cloudGatewayTerminalDto.Name = "New Name";
            cloudGatewayTerminalDto.Description = "Description";
            cloudGatewayTerminalDto.ConnectionAddress =
                $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}";

            var editCloudGatewayTerminal = _cloudGatewayTerminalManager.EditCloudGatewayTerminal(cloudGatewayTerminalDto);
            Assert.IsFalse(editCloudGatewayTerminal.Error, editCloudGatewayTerminal.Message);

            var editedCloudGatewayTerminal = _cloudGatewayTerminalManager.GetCloudGatewayTerminalDto(cloudGatewayTerminalId);
            Assert.IsFalse(editedCloudGatewayTerminal.Error, editedCloudGatewayTerminal.Message);

            var editedCloudGatewayTerminalDto = editedCloudGatewayTerminal.Result;
            Assert.AreEqual(cloudGatewayTerminalDto.Name, editedCloudGatewayTerminalDto.Name);
            Assert.AreEqual(cloudGatewayTerminalDto.Description, editedCloudGatewayTerminalDto.Description);
            Assert.AreEqual(cloudGatewayTerminalDto.ConnectionAddress, editedCloudGatewayTerminalDto.ConnectionAddress);
        }

        /// <summary>
        /// Удалить терминальный шлюз
        /// </summary>
        /// <param name="cloudGatewayTerminalId">Id терминального шлюза</param>
        private void DeleteCloudGatewayTerminal(Guid cloudGatewayTerminalId)
        {
            var deleteCloudGatewayTerminal = _cloudGatewayTerminalManager.DeleteCloudGatewayTerminal(cloudGatewayTerminalId);
            Assert.IsFalse(deleteCloudGatewayTerminal.Error, deleteCloudGatewayTerminal.Message);
        }
    }
}
