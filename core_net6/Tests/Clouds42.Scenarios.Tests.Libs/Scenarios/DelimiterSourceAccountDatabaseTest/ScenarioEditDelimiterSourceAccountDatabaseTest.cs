﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DelimiterSourceAccountDatabaseTest
{
    /// <summary>
    /// Сценарий теста проверки редактирования материнской базы разделителей
    ///
    /// 1) Создаем материнскую базу и аккаунт
    /// 2) Пытаемся получить мат. базу указывая не верные параметры(ожидаем ошибку)
    /// 3) Получаем мат. базу и начинаем редактировать подставляя не корректные параметры(ожидаем ошибку)
    /// 4) Успешное редактирование мат. базы
    /// </summary>
    public class ScenarioEditDelimiterSourceAccountDatabaseTest : ScenarioBase
    {
        public override async Task RunAsync()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var dbTemplateOnDelimiters = await DbLayer.DbTemplateDelimitersReferencesRepository
                .AsQueryable()
                .ToListAsync();

            var editDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<EditDelimiterSourceAccountDatabaseManager>();
            var createDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<CreateDelimiterSourceAccountDatabaseManager>();
            var testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();

            var database = testDataGenerator.GenerateAccountDatabase(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, 0,
                createAccountDatabaseAndAccountCommand.AccountDetails.Account.IndexNumber);

            database.IsFile = false;

            DbLayer.DatabasesRepository.Insert(database);
            await DbLayer.SaveAsync();

            var delimiterSourceAccountDatabaseDto = new DelimiterSourceAccountDatabaseDto
            {
                AccountDatabaseId = database.Id,
                DbTemplateDelimiterCode = dbTemplateOnDelimiters[0].ConfigurationId,
                DatabaseOnDelimitersPublicationAddress = "s"
            };

            var managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var data = await Mediator.Send(new GetFilteredDelimiterSourceAccountDatabaseQuery
            {
                Filter = new DelimiterSourceAccountDatabaseFilter
                {
                    AccountDatabaseId = Guid.NewGuid(),
                    SearchLine = dbTemplateOnDelimiters[0].ConfigurationId
                }
            });
            Assert.IsTrue(!data.Result.Records.Any(), "Не корректно указан ID базы");

            data = await Mediator.Send(new GetFilteredDelimiterSourceAccountDatabaseQuery
            {
                Filter = new DelimiterSourceAccountDatabaseFilter
                {
                    AccountDatabaseId = database.Id,
                    SearchLine = "dd"
                }
            });
            Assert.IsTrue(!data.Result.Records.Any(), "Не корректно указан код конфигурации разделителей");

            data = await Mediator.Send(new GetFilteredDelimiterSourceAccountDatabaseQuery
            {
                Filter = new DelimiterSourceAccountDatabaseFilter
                {
                    AccountDatabaseId = database.Id,
                    SearchLine = dbTemplateOnDelimiters[0].ConfigurationId
                }
            });

            Assert.IsFalse(!data.Result.Records.Any(), managerResult.Message);

            delimiterSourceAccountDatabaseDto.AccountDatabaseId = Guid.NewGuid();

            managerResult = editDelimiterSourceAccountDatabaseManager.Edit(delimiterSourceAccountDatabaseDto);
            Assert.IsTrue(managerResult.Error, "Не корректно указан ID базы");

            delimiterSourceAccountDatabaseDto.AccountDatabaseId = database.Id;
            delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode = "1";

            managerResult = editDelimiterSourceAccountDatabaseManager.Edit(delimiterSourceAccountDatabaseDto);
            Assert.IsTrue(managerResult.Error, "Не корректно указан код конфигурации разделителей");

            delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode = dbTemplateOnDelimiters[0].ConfigurationId;

            delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress = "new";

            managerResult = editDelimiterSourceAccountDatabaseManager.Edit(delimiterSourceAccountDatabaseDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            data = await Mediator.Send(new GetFilteredDelimiterSourceAccountDatabaseQuery
            {
                Filter = new DelimiterSourceAccountDatabaseFilter
                {
                    AccountDatabaseId = delimiterSourceAccountDatabaseDto.AccountDatabaseId,
                    SearchLine = delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode
                }
            });

            Assert.IsFalse(!data.Result.Records.Any(), data.Message);

            Assert.AreEqual(data.Result.Records.FirstOrDefault().DatabaseOnDelimitersPublicationAddress, delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress);
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }
    }
}
