﻿using System.Linq;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DelimiterSourceAccountDatabaseTest
{
    /// <summary>
    /// Сценарий теста проверки создания материнской базы разделителей
    ///
    /// 1) Создаем аккаунт, базу на разделителях и обычную базу
    /// 2) Пробуем создат мат. базу указывая базу на разделителях(ошибочное создание)
    /// 3) Пробуем создат мат. базу указывая базу файловую(ошибочное создание)
    /// 4) Пробуем создат мат. базу указывая базу не в статусе "Готова к использованию"(ошибочное создание)
    /// 5) Пробуем создат мат. базу указывая не корректный шаблон разделителей(ошибочное создание)
    /// 6) Создаем мат. базу указывая серверную базу и верный шаблон(успешное создание)
    /// 7) Пробуем создат мат. базу указывая параметры мат. базы существующей (ошибочное создание)
    /// </summary>
    public class ScenarioCreateDelimiterSourceAccountDatabaseTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var dbTemlateOnDelimiters = DbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy().ToList();

            var createDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<CreateDelimiterSourceAccountDatabaseManager>();
            var testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();

            var delimiterSourceAccountDatabaseDto = new DelimiterSourceAccountDatabaseDto
            {
                AccountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                DbTemplateDelimiterCode = dbTemlateOnDelimiters[0].ConfigurationId,
                DatabaseOnDelimitersPublicationAddress = "test"
            };

            var managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, "Нельзя создать материнскую базу, база является базой на разделителях");
            
            var database = testDataGenerator.GenerateAccountDatabase(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, 0,
                createAccountDatabaseAndAccountCommand.AccountDetails.Account.IndexNumber);

            DbLayer.DatabasesRepository.Insert(database);
            DbLayer.Save();

            delimiterSourceAccountDatabaseDto.AccountDatabaseId = database.Id;

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, "Нельзя создать материнскую базу, база является файловой");

            database.IsFile = false;
            database.StateEnum = DatabaseState.ErrorCreate;

            DbLayer.DatabasesRepository.Update(database);
            DbLayer.Save();

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, $"Нельзя создать материнскую базу, статус базы должен быть {DatabaseState.Ready}");

            database.StateEnum = DatabaseState.Ready;
            DbLayer.DatabasesRepository.Update(database);
            DbLayer.Save();

            delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode = "ss";

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, "Не корректно указана конфигурация разделителей");

            delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode = dbTemlateOnDelimiters[0].ConfigurationId;
            delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress = "";

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, "Адрес публикации обязательное поле");

            delimiterSourceAccountDatabaseDto.DatabaseOnDelimitersPublicationAddress = "rr";

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);

            Assert.IsTrue(managerResult.Error, "Такая база на разделителях уже существует");
        }
    }
}
