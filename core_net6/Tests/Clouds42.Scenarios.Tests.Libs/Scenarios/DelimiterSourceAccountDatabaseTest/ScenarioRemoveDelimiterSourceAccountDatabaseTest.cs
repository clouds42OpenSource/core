﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DelimiterSourceAccountDatabaseTest
{
    /// <summary>
    /// Сценарий теста проверки удаления материнской базы разделителей
    ///
    /// 1) Создаем аккаунт и мат. базу
    /// 2) Пытаемся удалить базу указывая не корректные параметры (ошибочное удаление)
    /// 3) Успешное удаление
    /// </summary>
    public class ScenarioRemoveDelimiterSourceAccountDatabaseTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var dbTemlateOnDelimiters = DbLayer.DbTemplateDelimitersReferencesRepository.WhereLazy().ToList();

            var removeDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<RemoveDelimiterSourceAccountDatabaseManager>();
            var createDelimiterSourceAccountDatabaseManager = ServiceProvider.GetRequiredService<CreateDelimiterSourceAccountDatabaseManager>();
            var testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();

            var database = testDataGenerator.GenerateAccountDatabase(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, 0,
                createAccountDatabaseAndAccountCommand.AccountDetails.Account.IndexNumber);

            database.IsFile = false;

            DbLayer.DatabasesRepository.Insert(database);
            DbLayer.Save();

            var delimiterSourceAccountDatabaseDto = new DelimiterSourceAccountDatabaseDto
            {
                AccountDatabaseId = database.Id,
                DbTemplateDelimiterCode = dbTemlateOnDelimiters[0].ConfigurationId,
                DatabaseOnDelimitersPublicationAddress = "s"
            };

            var managerResult = createDelimiterSourceAccountDatabaseManager.Create(delimiterSourceAccountDatabaseDto);
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            managerResult = removeDelimiterSourceAccountDatabaseManager.Remove(Guid.NewGuid(), delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode);
            Assert.IsTrue(managerResult.Error, "Не корректно указан ID базы");

            managerResult = removeDelimiterSourceAccountDatabaseManager.Remove(delimiterSourceAccountDatabaseDto.AccountDatabaseId, "11");
            Assert.IsTrue(managerResult.Error, "Не корректно указан код конфигурации разделителей");

            managerResult = removeDelimiterSourceAccountDatabaseManager.Remove(delimiterSourceAccountDatabaseDto.AccountDatabaseId, delimiterSourceAccountDatabaseDto.DbTemplateDelimiterCode);
            Assert.IsFalse(managerResult.Error, managerResult.Message);
        }
    }
}
