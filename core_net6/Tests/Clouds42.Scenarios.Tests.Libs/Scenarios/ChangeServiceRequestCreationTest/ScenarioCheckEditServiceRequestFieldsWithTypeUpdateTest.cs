﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста, по проверке полей заявки на редактирование,
    /// с типом 'Editing'
    ///
    /// 1) Создаем активный сервис
    /// 2) Изменяем свойство Name и отправляем его на модерацию
    /// 3) Получаем заявку на редактирование сервиса и проверяем ее поля
    /// 4) Проверяем изменения сервиса у заявки
    /// </summary>
    public class ScenarioCheckEditServiceRequestFieldsWithTypeUpdateTest : ScenarioBase
    {
        public override void Run()
        {
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            editBillingServiceDto.Name = "NewName";

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            var editServiceRequest = DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefault(esr =>
                                         esr.BillingServiceId == service.Id && esr.ChangeRequestType == ChangeRequestTypeEnum.Editing) ??
                                     throw new NotFoundException("Не удалось найти заявку для сервиса");

            Assert.AreEqual(editServiceRequest.Status, ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(editServiceRequest.BillingServiceChanges);

            var billingServiceChanges = editServiceRequest.BillingServiceChanges;

            Assert.IsNull(billingServiceChanges.Opportunities);
            Assert.IsNull(billingServiceChanges.ShortDescription);
            Assert.IsNull(billingServiceChanges.InstructionServiceCloudFile);
            Assert.IsNull(billingServiceChanges.IconCloudFile);

        }
    }
}
