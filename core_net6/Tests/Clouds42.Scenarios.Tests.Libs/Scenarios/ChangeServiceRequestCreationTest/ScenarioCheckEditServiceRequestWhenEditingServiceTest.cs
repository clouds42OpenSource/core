﻿using Clouds42.Billing;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста по проверке создания заявки на редактирование сервиса,
    /// при редактировании сервиса
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем поля сервиса и отправляем на модерацию
    /// 3) Проверяем что поля сервиса не меняются, пока не будет принята заявка
    /// </summary>
    public class ScenarioCheckEditServiceRequestWhenEditingServiceTest : ScenarioBase
    {
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        public ScenarioCheckEditServiceRequestWhenEditingServiceTest()
        {
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);
            var serviceId = service.Id;

            var billingServiceCardDto = billingServiceCardDataManager.GetData(serviceId);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var createIndustryCommand = new CreateIndustryCommand(TestContext, new IndustryModelTest { Name = "Новая отрасль", Description = "TEST" });
            createIndustryCommand.Run();
            
            var utTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.UtName,
                DefaultCaption = "Управление торговлей"
            });

            var kaTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.KaName,
                DefaultCaption = "Комплексная автоматизация"
            });

            var dbTemplateDelimitersOne =
                testDataGenerator.GenerateDbTemplateDelimiters(utTemplate.Id, "ut");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersOne);

            var dbTemplateDelimitersTwo =
                testDataGenerator.GenerateDbTemplateDelimiters(kaTemplate.Id, "ka");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersTwo);

            DbLayer.Save();

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewService";

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Id == serviceId);
            Assert.IsNotNull(service);

            Assert.AreNotEqual(service.Name, editBillingServiceDto.Name);
        }
    }
}
