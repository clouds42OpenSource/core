﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста проверки создания заявки сервиса на модерацию,
    /// при отправке черновика сервиса на модерацию
    /// 1) Создаем сервис со статусом "Черновик"
    /// 2) Редактируем черновик и оптравляем на модерацию
    /// 3) Проверяем поля сервиса и заявки на создание сервиса
    /// 4) Проверяем что данные применились
    /// </summary>
    public class ScenarioCheckSendingServiceDraftToModerationTest: ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.Draft

                });
            createBillingServiceCommand.Run();

            var createBillingServiceTest = createBillingServiceCommand.CreateBillingServiceTest;

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceTest.Name);
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.Draft);

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDraftDto = billingServiceCardDto.Result.MapToBillingServiceDraftDto();

            editBillingServiceDraftDto.Name = "UpdateNameTest";
            editBillingServiceDraftDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingServiceDraft(editBillingServiceDraftDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == editBillingServiceDraftDto.Name);
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.OnModeration);

            var createServiceRequest =
                DbLayer.GetGenericRepository<CreateServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == service.Id);
            Assert.IsNotNull(createServiceRequest);
            Assert.AreEqual(createServiceRequest.Status, ChangeRequestStatusEnum.OnModeration);
            Assert.AreEqual(createServiceRequest.ChangeRequestType, ChangeRequestTypeEnum.Creation);

            var resultOfComparing =
                createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingDraftModelAndService(
                    editBillingServiceDraftDto, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }
    }
}
