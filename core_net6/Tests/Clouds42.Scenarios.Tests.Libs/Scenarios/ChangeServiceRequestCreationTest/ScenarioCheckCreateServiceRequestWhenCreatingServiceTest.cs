﻿using System;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста проверки создания заявки сервиса на модерацию,
    /// при создании сервиса
    /// 1) Создаем сервис и отправляем заявку на создание сервиса, на модерацию
    /// 2) Проверяем статус у заявки и у сервиса, проверяем тип заявки
    /// 3) Сверяем данные сервиса и данные модели создания
    /// </summary>
    public class ScenarioCheckCreateServiceRequestWhenCreatingServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                }, false);

            createCustomService.Run();

            var createBillingServiceTest = createCustomService.CreateBillingServiceTest.Value;

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.SystemService == null && s.Name != "Дополнительные сеансы");
            Assert.IsNotNull(service);
            Assert.AreEqual(service.BillingServiceStatus, BillingServiceStatusEnum.OnModeration);

            var createServiceRequest =
                DbLayer.GetGenericRepository<CreateServiceRequest>().FirstOrDefault(csr => csr.BillingServiceId == service.Id);
            Assert.IsNotNull(createServiceRequest);
            Assert.AreEqual(createServiceRequest.Status, ChangeRequestStatusEnum.OnModeration);
            Assert.AreEqual(createServiceRequest.ChangeRequestType, ChangeRequestTypeEnum.Creation);

            var resultOfComparing =
                createOrEditBillingServiceHelper.CompareSimilarityOfServiceCreationModelAndService(
                    createBillingServiceTest, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }
    }
}
