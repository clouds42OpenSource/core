﻿using System.Linq;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста по проверке создания заявки на редактирование сервиса,
    /// при удалении услуг сервиса
    ///
    /// 1) Создаем сервис и услуги #CreateService
    /// 2) Удаляем услугу севриса #RemoveServiceType
    /// 3) Проверяем что ничего не изменилось, пока не будет принята заявка #CheckService
    /// </summary>
    public class ScenarioCheckEditServiceRequestWhenDeleteServiceTypesTest : ScenarioBase
    {
        public override void Run()
        {
            var serviceTypeHelper =
                TestContext.ServiceProvider.GetRequiredService<PrepareServiceWithRequestToRemoveServiceTypeHelperTest>();

            #region CreateService
            var prepareServiceModel = serviceTypeHelper.CreateService(TestContext);
            #endregion

            #region RemoveServiceType
            serviceTypeHelper.RemoveServiceType(prepareServiceModel);
            #endregion

            #region CheckService
            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Id == prepareServiceModel.ServcieId);
            Assert.IsNotNull(service);

            Assert.IsFalse(service.BillingServiceTypes.Any(st => st.IsDeleted));
            Assert.IsFalse(DbLayer.BillingServiceTypeRelationRepository.Any(st => st.IsDeleted));
            Assert.AreEqual(2, DbLayer.GetGenericRepository<BillingServiceTypeChange>().Count());
            #endregion
        }
    }
}
