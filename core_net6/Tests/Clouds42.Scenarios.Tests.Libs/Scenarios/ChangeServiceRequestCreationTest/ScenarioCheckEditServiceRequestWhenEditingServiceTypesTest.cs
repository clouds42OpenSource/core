﻿using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста по проверке создания заявки на редактирование сервиса,
    /// при редактировании услуг сервиса
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем поля услуг сервиса и отправляем на модерацию
    /// 3) Проверяем что поля сервиса не меняются, пока не будет принята заявка
    /// 4) проверяем поля заявки
    /// </summary>
    public class ScenarioCheckEditServiceRequestWhenEditingServiceTypesTest : ScenarioBase
    {
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);
            var serviceId = service.Id;

            var billingServiceCardDto = billingServiceCardDataManager.GetData(serviceId);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            var serviceTypeThree = editBillingServiceDto.BillingServiceTypes[2];
            serviceTypeOne.Name = "NewName";
            serviceTypeTwo.Description = "NewDescription";
            serviceTypeThree.ServiceTypeCost = 10;

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Id == serviceId);
            Assert.IsNotNull(service);

            Assert.IsNull(service.BillingServiceTypes.FirstOrDefault(st => st.Name == serviceTypeOne.Name));
            Assert.IsNull(service.BillingServiceTypes.FirstOrDefault(st => st.Description == serviceTypeTwo.Description));
            Assert.IsNull(DbLayer.RateRepository.FirstOrDefault(r =>
                r.BillingServiceTypeId == serviceTypeThree.Id && r.Cost == serviceTypeThree.ServiceTypeCost));

            var editServiceRequest = DbLayer.GetGenericRepository<EditServiceRequest>()
                .FirstOrDefault(esr => esr.BillingServiceId == serviceId);

            var billingServiceTypeChanges = editServiceRequest.BillingServiceChanges.BillingServiceTypeChanges;

            Assert.IsNotNull(editServiceRequest);
            Assert.AreEqual(3, billingServiceTypeChanges.Count);

            Assert.IsNotNull(billingServiceTypeChanges.FirstOrDefault(stc =>
                stc.BillingServiceTypeId == serviceTypeOne.Id && stc.Name == serviceTypeOne.Name &&
                stc.Description == null && stc.Cost == null));

            Assert.IsNotNull(billingServiceTypeChanges.FirstOrDefault(stc =>
                stc.BillingServiceTypeId == serviceTypeTwo.Id && stc.Description == serviceTypeTwo.Description &&
                stc.Name == null && stc.Cost == null));

            Assert.IsNotNull(billingServiceTypeChanges.FirstOrDefault(stc =>
                stc.BillingServiceTypeId == serviceTypeThree.Id && stc.Cost == serviceTypeThree.ServiceTypeCost &&
                stc.Name == null && stc.Description == null));
        }
    }
}
