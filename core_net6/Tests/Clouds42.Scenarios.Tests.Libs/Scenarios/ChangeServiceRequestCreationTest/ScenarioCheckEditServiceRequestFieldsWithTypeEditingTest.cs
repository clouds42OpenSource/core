﻿using Clouds42.Billing;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestCreationTest
{
    /// <summary>
    /// Сценарий теста, по проверке полей заявки на редактирование,
    /// с типом 'Редактирование'
    ///
    /// 1) Создаем активный сервис
    /// 2) Редактируем сервис и отправляем его на модерацию
    /// 3) Получаем заявку на редактирование сервиса и проверяем ее поля
    /// 4) Проверяем изменения сервиса у заявки
    /// </summary>
    public class ScenarioCheckEditServiceRequestFieldsWithTypeEditingTest : ScenarioBase
    {
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioCheckEditServiceRequestFieldsWithTypeEditingTest()
        {
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }
        public override void Run()
        {
            TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var createIndustryCommand = new CreateIndustryCommand(TestContext, new IndustryModelTest { Name = "Новая отрасль", Description = "TEST" });
            createIndustryCommand.Run();

            var dbTemplates = DbLayer.DbTemplateRepository.WhereLazy().Select(w => w.Id).ToList();

            if (!dbTemplates.Any())
                throw new ArgumentException("Не заполнен справочник шаблоны");

            var utTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.UtName,
                DefaultCaption = "Управление торговлей"
            });

            var kaTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.KaName,
                DefaultCaption = "Комплексная автоматизация"
            });

            var dbTemplateDelimitersOne =
                testDataGenerator.GenerateDbTemplateDelimiters(utTemplate.Id, "ut");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersOne);

            var dbTemplateDelimitersTwo =
                testDataGenerator.GenerateDbTemplateDelimiters(kaTemplate.Id, "ka");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersTwo);

            DbLayer.Save();

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewService";

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            var editServiceRequest = DbLayer.GetGenericRepository<EditServiceRequest>()
                                         .AsQueryableNoTracking()
                                         .Include(x => x.BillingServiceChanges)
                                         .FirstOrDefault(esr =>
                                  esr.BillingServiceId == service.Id && esr.ChangeRequestType == ChangeRequestTypeEnum.Editing) ??
                              throw new NotFoundException("Не удалось найти заявку для сервиса");

            Assert.AreEqual(editServiceRequest.Status, ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(editServiceRequest.BillingServiceChanges);

            Assert.AreEqual(editServiceRequest.BillingServiceChanges.Name, editBillingServiceDto.Name);
        }
    }
}
