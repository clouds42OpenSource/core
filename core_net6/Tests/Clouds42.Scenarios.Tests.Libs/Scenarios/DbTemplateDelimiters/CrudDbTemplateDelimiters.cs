﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimiters
{
    /// <summary>
    /// Тест добавления/удаления/редактирования новой базы на разделителях
    /// Сценарий:
    ///         1. Создаём новый шаблон для проверки создания базы на разделителях 
    ///         2. Создаем новую базу на разделителях, проверяем, что создание прошло успешно
    ///         3. Редактируем базу на разделителях, проверяем, что редактирование прошло успешно
    ///         4. Удаляем базу на разделителях, проверяем, что удаление прошло успешно
    ///         5. Удаляем созданный шаблон 
    /// </summary>
    public class CrudDbTemplateDelimiters : ScenarioBase
    {
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;
        private readonly DbTemplatesManager _dbTemplatesManager;

        public CrudDbTemplateDelimiters()
        {
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            _dbTemplatesManager = TestContext.ServiceProvider.GetRequiredService<DbTemplatesManager>();
        }

        public override void Run()
        {
            var dbTemplateDelimiter = new DbTemplateDelimeterItemDto
            {
                ConfigurationId = Guid.NewGuid().ToString("D").Split('-')[0],
                Name = Guid.NewGuid().ToString(),
                ShortName = Guid.NewGuid().ToString(),
                DemoDatabaseOnDelimitersPublicationAddress = Guid.NewGuid().ToString(),
                MinReleaseVersion = "1",
                ConfigurationReleaseVersion = "2",
                TemplateId = CreateDbTemplateId()
            };

            CreateNewDbTemplateDelimiter(ref dbTemplateDelimiter);
            UpdateDbTemplateDemoDatabaseOnDelimitersPublicationAddress(dbTemplateDelimiter);
            DeleteDbTemplateDelimiter(dbTemplateDelimiter.ConfigurationId);
            DeleteDbTemplateId(dbTemplateDelimiter.TemplateId);
        }

        /// <summary>
        /// Создать шаблон
        /// </summary>
        /// <returns>ID созданного шаблона</returns>
        private Guid CreateDbTemplateId()
        {
            var dbTemplate = new DbTemplateItemDto
            {
                Id = Guid.Empty,
                Name = Guid.NewGuid().ToString(),
                DefaultCaption = Guid.NewGuid().ToString(),
                Configuration1CName = "Бухгалтерия предприятия, редакция 3.0"
            };
            var addResult = _dbTemplatesManager.AddDbTemplateItem(dbTemplate);

            Assert.IsFalse(addResult.Error);
            
            return addResult.Result.DbTemplateId;
        }

        /// <summary>
        /// Удалить шаблон
        /// </summary>
        /// <param name="dbTemplateId">ID шаблона для удаления</param>
        private void DeleteDbTemplateId(Guid dbTemplateId)
        {
            var deleteResult = _dbTemplatesManager.DeleteDbTemplateItem(new DeleteDbTemplateDataItemDto
            {
                DbTemplateId = dbTemplateId
            });

            Assert.IsFalse(deleteResult.Error);
        }


        /// <summary>
        /// Создать новую базу на разделителях
        /// 1. Проверка, что базы на разделителях не существует
        /// 2. Добавляем новую базу на разделителях
        /// 3. Проверка что новая запись добавлена
        /// </summary>
        /// <param name="item">Данные для дабавления шаблона</param>
        private void CreateNewDbTemplateDelimiter(ref DbTemplateDelimeterItemDto item)
        {
            #region Проверка, что базы на разделителях не существует

            var foundItem = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(item.ConfigurationId);
            Assert.IsTrue(foundItem.Error);

            #endregion


            #region Добавляем новую базу на разделителях

            var addNewDbTemplateDelimiter = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(item);
            Assert.IsFalse(addNewDbTemplateDelimiter.Error);

            #endregion

            
            #region Проверка что новая запись добавлена

            foundItem = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(item.ConfigurationId);
            Assert.IsFalse(foundItem.Error);

            #endregion
        }

        /// <summary>
        /// Обновить базу на разделителях
        /// 1. Проверка, что база на разделителях существует
        /// 2. Обновляем поле DemoDatabaseOnDelimitersPublicationAddress для базы на разделителях
        /// 3. Проверка что запись обнавлена
        /// </summary>
        /// <param name="item">База на разделителях для обновления</param>
        private void UpdateDbTemplateDemoDatabaseOnDelimitersPublicationAddress(DbTemplateDelimeterItemDto item)
        {
            #region Проверка, что база на разделителях существует

            var foundItemResult = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(item.ConfigurationId);
            
            Assert.IsFalse(foundItemResult.Error);

            #endregion


            #region Обновляем поле DemoDatabaseOnDelimitersPublicationAddress для базы на разделителях

            var oldValue = item.DemoDatabaseOnDelimitersPublicationAddress;

            item.DemoDatabaseOnDelimitersPublicationAddress = Guid.NewGuid().ToString();
            
            var updateNewDbTemplateDelimiter = _dbTemplateDelimitersManager.UpdateDbTemplateDelimiterItem(item);
            
            Assert.IsFalse(updateNewDbTemplateDelimiter.Error);

            #endregion


            #region Проверка что запись обнавлена

            foundItemResult = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(item.ConfigurationId);
            
            Assert.IsFalse(foundItemResult.Error);
            
            var foundItem = foundItemResult.Result;

            Assert.IsTrue(oldValue != foundItem.DemoDatabaseOnDelimitersPublicationAddress);

            #endregion
        }


        /// <summary>
        /// Удалить базу на разделителях
        /// 1. Удаляем базу на разделителях
        /// 2. Проверка что запись удалена
        /// </summary>
        /// <param name="configurationId">ID конфигурации для удаления базы на разделителях</param>
        private void DeleteDbTemplateDelimiter(string configurationId)
        {
            #region Удаляем базу на разделителях

            var deleteDbTemplateDelimiterResult = _dbTemplateDelimitersManager.DeleteDbTemplateDelimiterItem(new DeleteDbTemplateDelimiterDto
            {
                ConfigurationId = configurationId
            });

            Assert.IsFalse(deleteDbTemplateDelimiterResult.Error);

            #endregion


            #region Проверка что запись удалена

            var foundItemResult = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId);

            Assert.IsTrue(foundItemResult.Error);

            #endregion
        }
    }
}
