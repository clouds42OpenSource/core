﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudEnterpriseServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;
using Core42.Application.Features.EnterpriseServerContext.Queries;
using Core42.Application.Features.EnterpriseServerContext.Queries.Filters;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CrudEnterpriseServer
{
    /// <summary>
    /// Тест получения серверов предприятия по фильтру
    /// Сценарий:
    ///         1. Создаем 3 Sql сервера предприятия
    ///         2. Получаем сервера, отфильтрованные по названию
    ///         3. Получаем сервера, отфильтрованные по версии платформы
    ///         4. Получаем сервера, отфильтрованные по описанию
    ///         5. Получаем сервера, отфильтрованные по описанию, названию и версии платформы
    /// </summary>
    public class GetFilteredEnterpriseServersTest : ScenarioBase
    {
        private readonly EnterpriseServersManager _enterpriseServersManager;

        public GetFilteredEnterpriseServersTest()
        {
            _enterpriseServersManager = TestContext.ServiceProvider.GetRequiredService<EnterpriseServersManager>();
        }

        public override async Task RunAsync()
        {
            var firstEnterpriseServerDto = new EnterpriseServerDto
            {
                Name = $"NewName{DateTime.Now:mmssfff}",
                Description = "Desc",
                AdminName = "Admin",
                AdminPassword = "Password",
                ConnectionAddress = $"10.35.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                Version = PlatformType.V83.ToString()
            };
            CreateEnterpriseServer(firstEnterpriseServerDto);

            var secondEnterpriseServerDto = new EnterpriseServerDto
            {
                Name = $"NewName{DateTime.Now:mmssfff}Server",
                Description = "Desc",
                AdminName = "Admin",
                AdminPassword = "Password",
                ConnectionAddress = $"10.35.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                Version = PlatformType.V83.ToString()
            };
            CreateEnterpriseServer(secondEnterpriseServerDto);

            var thirdEnterpriseServerDto = new EnterpriseServerDto
            {
                Name = $"NewName{DateTime.Now:mmssfff}",
                Description = "Description",
                AdminName = "Admin",
                AdminPassword = "Password",
                ConnectionAddress = $"10.35.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                Version = PlatformType.V82.ToString()
            };
            CreateEnterpriseServer(thirdEnterpriseServerDto);

            #region Получение серверов предприятия по названию

            var enterpriseServerFilter = new GetFilteredEnterpriseServersQuery
            {
                Filter = new EnterpriseServerFilter { Name = "Name" },
                PageNumber = 1
            };
            await GetFilteredEnterpriseServersAsync(3, enterpriseServerFilter, GetData);

            enterpriseServerFilter.Filter.Name = "Server";
            await GetFilteredEnterpriseServersAsync(1, enterpriseServerFilter, GetData);

            #endregion

            #region Получение серверов предприятия по версии платформы
            
            enterpriseServerFilter.Filter.Name = null;
            enterpriseServerFilter.Filter.Version = PlatformType.V82.ToString();
            enterpriseServerFilter.PageNumber = 1;

            await GetFilteredEnterpriseServersAsync(1, enterpriseServerFilter, GetData);

            enterpriseServerFilter.Filter.Version = PlatformType.V83.ToString();
            await GetFilteredEnterpriseServersAsync(2, enterpriseServerFilter, GetData);

            #endregion

            #region Получение серверов предприятия по описанию
            
            enterpriseServerFilter.Filter.Name = null;
            enterpriseServerFilter.Filter.Version = null;
            enterpriseServerFilter.Filter.Description = "Description";

            await GetFilteredEnterpriseServersAsync(1, enterpriseServerFilter, GetData);

            enterpriseServerFilter.Filter.Description = "Desc";

            await GetFilteredEnterpriseServersAsync(3, enterpriseServerFilter, GetData);

            #endregion

            #region Получение серверов предприятия по описанию, названиюи версии

            
            enterpriseServerFilter.Filter.Name = "Server";
            enterpriseServerFilter.Filter.Description = "Desc";
            enterpriseServerFilter.Filter.Version = PlatformType.V83.ToString();
            enterpriseServerFilter.PageNumber = 1;

            await GetFilteredEnterpriseServersAsync(1, enterpriseServerFilter, GetData);

            #endregion
        }

        public override void Run()
        {
           

        }

        /// <summary>
        /// Содать новый сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Модель сервер 1С:Предприятие</param>
        private void CreateEnterpriseServer(EnterpriseServerDto enterpriseServerDto)
        {
            var createEnterpriseServerResult = _enterpriseServersManager.AddNewEnterpriseServer(enterpriseServerDto);
            Assert.IsFalse(createEnterpriseServerResult.Error, createEnterpriseServerResult.Message);
        }

        /// <summary>
        /// Получить сервера предприятия по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество серверов предприятия</param>
        /// <param name="query">Фильтр</param>
        /// <param name="action">Действие, для выбора и проверки выборки северов</param>
        private async Task GetFilteredEnterpriseServersAsync(int expectedCount, GetFilteredEnterpriseServersQuery query,
            Func<GetFilteredEnterpriseServersQuery, int, Task> action)
        { 
            await action(query, expectedCount);
        }

        /// <summary>
        /// ДесПолучить сервера предприятия по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="query">Фильтр</param>
        /// <param name="expectedCount">Ожидаемое количество серверов предприятия<</param>
        private async Task GetData(GetFilteredEnterpriseServersQuery query, int expectedCount)
        {
            var enterpriseServers = await Mediator.Send(query);
            Assert.IsFalse(enterpriseServers.Error);
            Assert.AreEqual(expectedCount, enterpriseServers.Result.Records.Count);
        }
    }
}
