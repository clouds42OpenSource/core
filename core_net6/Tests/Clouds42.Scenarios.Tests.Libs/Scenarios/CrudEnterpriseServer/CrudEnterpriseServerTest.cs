﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;
using Clouds42.Segment.CloudEnterpriseServer.Managers;
using Core42.Application.Features.EnterpriseServerContext.Queries;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CrudEnterpriseServer
{
    /// <summary>
    /// Тест проверки создания/удаления/редактированрия сервера 1С:Предприятие
    /// Сценарий:
    ///         1. Создаем сервер 1С:Предприятие, проверяем, что создание прошло без ошибок
    ///         2. Удаляем сервер 1С:Предприятие, проверяем, что удаленик прошло без ошибок
    ///         3. Редактируем сервер 1С:Предприятие, проверяем, что редактирование прошло без ошибок
    /// </summary>
    public class CrudEnterpriseServerTest : ScenarioBase
    {
        private readonly EnterpriseServersManager _enterpriseServersManager;

        public CrudEnterpriseServerTest()
        {
            _enterpriseServersManager = TestContext.ServiceProvider.GetRequiredService<EnterpriseServersManager>();
        }
        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var enterpriseServerId = CreateEnterpriseServer();
            await EditEnterpriseServer(enterpriseServerId);
            DeleteEnterpriseServer(enterpriseServerId);
        }

        /// <summary>
        /// Добавить новый сервер 1С:Предприятие
        /// </summary>
        /// <returns>ID сервера 1С:Предприятие</returns>
        private Guid CreateEnterpriseServer()
        {
            var enterpriseServerDto = new EnterpriseServerDto
            {
                Name = $"NewName{DateTime.Now:mmssfff}",
                Description = "Desc",
                AdminName = "Admin",
                AdminPassword = "Password",
                ConnectionAddress = $"10.35.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                Version = PlatformType.V83.ToString()
            };

            var createEnterpriseServerResult = _enterpriseServersManager.AddNewEnterpriseServer(enterpriseServerDto);
            Assert.IsFalse(createEnterpriseServerResult.Error, createEnterpriseServerResult.Message);

            return createEnterpriseServerResult.Result;
        }

        /// <summary>
        /// Изменить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        private async Task EditEnterpriseServer(Guid enterpriseServerId)
        {
            var enterpriseServerDto = await Mediator.Send(new GetEnterpriseServerByIdQuery { Id = enterpriseServerId });
            Assert.IsFalse(enterpriseServerDto.Error, enterpriseServerDto.Message);

            var enterpriseServer = enterpriseServerDto.Result;

            enterpriseServer.Name = "NotName";
            enterpriseServer.AdminName = "NotAdmin";
            enterpriseServer.AdminPassword = "NotPassword";
            enterpriseServer.ConnectionAddress = $"10.30.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}";
            enterpriseServer.Description = "NotDesc";

            var editEnterpriseServerResult = _enterpriseServersManager.EditEnterpriseServer(new EnterpriseServerDto
            {
                Id = enterpriseServer.Id,
                ClusterSettingsPath = enterpriseServer.ClusterSettingsPath,
                Version = enterpriseServer.Version,
                AdminName = enterpriseServer.AdminName,
                AdminPassword = enterpriseServer.AdminPassword,
                Name = enterpriseServer.Name,
                ConnectionAddress = enterpriseServer.ConnectionAddress,
                Description = enterpriseServer.Description
            });

            Assert.IsFalse(editEnterpriseServerResult.Error, editEnterpriseServerResult.Message);

            var editedModel = await Mediator.Send(new GetEnterpriseServerByIdQuery { Id = enterpriseServerId });
            Assert.IsFalse(editedModel.Error, editedModel.Message);

            Assert.AreEqual(enterpriseServer.Name, editedModel.Result.Name);
            Assert.AreEqual(enterpriseServer.Description, editedModel.Result.Description);
            Assert.AreEqual(enterpriseServer.AdminName, editedModel.Result.AdminName);
            Assert.AreEqual(enterpriseServer.AdminPassword, editedModel.Result.AdminPassword);
            Assert.AreEqual(enterpriseServer.ConnectionAddress, editedModel.Result.ConnectionAddress);
        }

        /// <summary>
        /// Удалить сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerId">ID сервера 1С:Предприятие</param>
        private void DeleteEnterpriseServer(Guid enterpriseServerId)
        {
            var deletingResult = _enterpriseServersManager.DeleteEnterpriseServer(enterpriseServerId);
            Assert.IsFalse(deletingResult.Error, deletingResult.Message);
        }
    }
}
