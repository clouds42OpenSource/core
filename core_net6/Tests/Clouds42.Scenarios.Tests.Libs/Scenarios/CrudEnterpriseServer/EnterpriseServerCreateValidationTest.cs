﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudEnterpriseServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CrudEnterpriseServer
{
    /// <summary>
    /// Тест проверки валидации при создании сервера 1С:Предприятие
    /// Сценарий:
    ///         1. Создаем новый сервер 1С:Предприятие с незаполненными обязятельными полями
    ///         2. Проверяем, что во всех случаях создание завершилось с ошибкой
    ///         3. Заполяем модель полностью, проверяем, что сервер создан
    ///         4. Создаем сервер с уже существующим названием и адресом подключения
    ///         5. Прверяем, что сервер не создан
    /// </summary>
    public class EnterpriseServerCreateValidationTest : ScenarioBase
    {
        private readonly EnterpriseServersManager _enterpriseServersManager;

        public EnterpriseServerCreateValidationTest()
        {
            _enterpriseServersManager = TestContext.ServiceProvider.GetRequiredService<EnterpriseServersManager>();
        }

        public override void Run()
        {
            var enterpriseServerDto = new EnterpriseServerDto
            {
                AdminName = "AdminName",
                AdminPassword = "Password",
                Description = "Desc"
            };
            CreateEnterpriseServer(enterpriseServerDto);

            enterpriseServerDto.Version = PlatformType.V83.ToString();
            CreateEnterpriseServer(enterpriseServerDto);

            enterpriseServerDto.ConnectionAddress = $"10.35.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}";
            CreateEnterpriseServer(enterpriseServerDto);

            enterpriseServerDto.Name = "EnterpriseName";
            CreateEnterpriseServer(enterpriseServerDto, false);

            var secondEnterpriseServerDto = new EnterpriseServerDto
            {
                AdminName = "AdminName",
                AdminPassword = "Password",
                Description = "Desc",
                Name = "EnterpriseName",
                ConnectionAddress = enterpriseServerDto.ConnectionAddress,
                Version = PlatformType.V83.ToString()
            };
            CreateEnterpriseServer(secondEnterpriseServerDto);

            secondEnterpriseServerDto.Name = "SomeName";
            CreateEnterpriseServer(secondEnterpriseServerDto);

            secondEnterpriseServerDto.ConnectionAddress = $"10.37.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}";
            CreateEnterpriseServer(secondEnterpriseServerDto, false);

        }

        /// <summary>
        /// Создать новый сервер 1С:Предприятие
        /// </summary>
        /// <param name="enterpriseServerDto">Модель сервер 1С:Предприятие</param>
        /// <param name="isErrorCreate">Флаг ошибка создания</param>
        private void CreateEnterpriseServer(EnterpriseServerDto enterpriseServerDto, bool isErrorCreate = true)
        {
            var createEnterpriseServerResult = _enterpriseServersManager.AddNewEnterpriseServer(enterpriseServerDto);

            if (isErrorCreate)
                Assert.IsTrue(createEnterpriseServerResult.Error);
            else
                Assert.IsFalse(createEnterpriseServerResult.Error, createEnterpriseServerResult.Message);
        }
    }
}
