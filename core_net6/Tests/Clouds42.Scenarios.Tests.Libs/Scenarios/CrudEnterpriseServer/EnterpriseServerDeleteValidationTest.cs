﻿using System;
using System.Threading.Tasks;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudEnterpriseServer.Managers;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CrudEnterpriseServer
{
    /// <summary>
    /// Тест проверки валидации при удалении сервера 1С:Предприятие
    /// Сценарий:
    ///         1. Создаем сегмент, пытаемся удалить его сервер предприятия
    ///         2. Проверяем, что вернулась ошибка
    ///         3. Создаем новый сервер
    ///         4. Меняем сервер предприятия сегмента на новый
    ///         5. Удаляем первый сервер предприятия, проверяем, что удаление прошло без ошибок
    /// </summary>
    public class EnterpriseServerDeleteValidationTest : ScenarioBase
    {
        private readonly EnterpriseServersManager _enterpriseServersManager;
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public EnterpriseServerDeleteValidationTest()
        {
            _enterpriseServersManager = TestContext.ServiceProvider.GetRequiredService<EnterpriseServersManager>();
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var cloudSegmentDto = await Mediator.Send(new GetSegmentByIdQuery { Id = createSegmentCommand.Id }); ;
            Assert.IsFalse(cloudSegmentDto.Error, cloudSegmentDto.Message);

            var enterpriseServer83Id = cloudSegmentDto.Result.BaseData.EnterpriseServer83Id;

            var deleteEnterpriseServerResult = _enterpriseServersManager.DeleteEnterpriseServer(enterpriseServer83Id);
            Assert.IsTrue(deleteEnterpriseServerResult.Error);

            var enterpriseServerDto = new EnterpriseServerDto
            {
                AdminName = "AdminName",
                AdminPassword = "Password",
                Description = "Desc",
                Name = "EnterpriseName",
                ConnectionAddress = $"17.85.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                Version = PlatformType.V83.ToString()
            };

            var addNewEnterpriseServer = _enterpriseServersManager.AddNewEnterpriseServer(enterpriseServerDto);
            Assert.IsFalse(addNewEnterpriseServer.Error, addNewEnterpriseServer.Message);

            cloudSegmentDto.Result.BaseData.EnterpriseServer83Id = addNewEnterpriseServer.Result;

            var editSegmentResult = _cloudSegmentReferenceManager.EditSegment(cloudSegmentDto.Result.MapToEditCloudServicesSegmentDto());
            Assert.IsFalse(editSegmentResult.Error, editSegmentResult.Message);

            deleteEnterpriseServerResult = _enterpriseServersManager.DeleteEnterpriseServer(enterpriseServer83Id);
            Assert.IsFalse(deleteEnterpriseServerResult.Error, deleteEnterpriseServerResult.Message);
        }
    }
}
