﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Clouds42.Billing.BillingServices.Validators;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    /// Создание сервиса с невалидными полями Name и ShortDescription
    ///     1. Создаем сервис и все необходимые зависимости
    ///     2. Заполняем модель сервиса невалидными значениями полей Name и ShortDescription.
    ///     3. Записываем объект сервиса.
    ///     4. Проверяем, что сервис не запишется с сообщением об ошибке валидации.
    /// </summary>
    public class ScenarioValidatingServiceNameOnCreatingFail : ScenarioBase
    {
        public override void Run()
        {
            var createBillingServiceCommandFail = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandFail.CreateBillingServiceTest.Value.Name = new string('t', BillingServiceValidator.MaxLengthServiceName + 1);
            
            createBillingServiceCommandFail.Run();

            if (!createBillingServiceCommandFail.Result.Error)
                throw new InvalidOperationException("Сервис был успешно создан с невалидными полями Name и ShortDescription");
        }
    }
}
