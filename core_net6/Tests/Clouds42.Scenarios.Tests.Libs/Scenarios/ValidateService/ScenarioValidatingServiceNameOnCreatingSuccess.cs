﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    /// Создание сервиса с валидными полями
    ///     1. Создаем сервис и все необходимые зависимости
    ///     2. Заполняем модель сервиса валидными значениями полей Name и ShortDescription. (при создании сразу валидные)
    ///     3. Записываем объект сервиса.
    ///     4. Проверяем, что сервис запишется без ошибок.
    /// </summary>
    public class ScenarioValidatingServiceNameOnCreatingSuccess : ScenarioBase
    {
        public override void Run()
        {
            var createBillingServiceCommandSuccess = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandSuccess.Run();

            if (createBillingServiceCommandSuccess.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommandSuccess.Result.Message);
        }
    }
}
