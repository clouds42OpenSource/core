﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    ///  Создание сервиса с валидными полями файла разработки 1С
    ///     1. Создаем и записываем сервис и все необходимые зависимости.
    ///     2. Проверяем, что сервис запишется без ошибок валидации.
    /// </summary>
    public class ScenarioValidatingService1CFilesOnCreatingSuccess : ScenarioBase
    {
        public override void Run()
        {
            var createBillingServiceCommandSuccess = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandSuccess.Run();

            if (createBillingServiceCommandSuccess.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommandSuccess.Result.Message);
        }
    }
}
