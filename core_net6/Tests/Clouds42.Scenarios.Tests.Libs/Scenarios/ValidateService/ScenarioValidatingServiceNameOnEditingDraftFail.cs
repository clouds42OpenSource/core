﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    /// Редактирование сервиса в статусе "черновик" с невалидными полями Name и ShortDescription
    ///     1. Создаем и записываем сервис в статусе черновик и все необходимые зависимости.
    ///     2. Получаем и заполняем модель сервиса невалидными значениями полей Name и ShortDescription.
    ///     3. Записываем объект сервиса.
    ///     4. Проверяем, что сервис не запишется с сообщением об ошибке валидации.
    /// </summary>
    public class ScenarioValidatingServiceNameOnEditingDraftFail : ScenarioBase
    {
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;
        private readonly EditBillingServiceManager _editServiceManager;

        public ScenarioValidatingServiceNameOnEditingDraftFail()
        {
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            _editServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
        }

        public override void Run()
        {
            var createBillingServiceCommandSuccess = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandSuccess.CreateBillingServiceTest.Value.BillingServiceStatus =
                BillingServiceStatusEnum.Draft;
            createBillingServiceCommandSuccess.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == createBillingServiceCommandSuccess.Id);
            Assert.IsNotNull(service);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id);
            var editBillingServiceDraftDto = billingServiceCardDto.Result.MapToBillingServiceDraftDto();

            editBillingServiceDraftDto.Name = new string('a', BillingServiceValidator.MaxLengthServiceName + 1);
            editBillingServiceDraftDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var editBillingServiceManagerResult = _editServiceManager.EditBillingServiceDraft(editBillingServiceDraftDto);
            if (!editBillingServiceManagerResult.Error)
                throw new InvalidOperationException("Сервис был луспешно отредактирован с невалидными значениями Name и ShortDescription");
        }
    }
}
