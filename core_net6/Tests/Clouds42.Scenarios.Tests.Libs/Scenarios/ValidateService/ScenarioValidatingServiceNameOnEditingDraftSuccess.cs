﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    /// Создание сервиса в статусе "черновик" с валидными полями Name и ShortDescription
    ///     1. Создаем и записываем сервис в статусе черновик и все необходимые зависимости.
    ///     2. Получаем и заполняем модель сервиса валидными значениями полей Name и ShortDescription. (при создании сразу валидные)
    ///     3. Записываем объект сервиса.
    ///     4. Проверяем, что сервис запишется без ошибок валидации.
    /// </summary>
    public class ScenarioValidatingServiceNameOnEditingDraftSuccess : ScenarioBase
    {
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;
        private readonly BillingServiceValidator _billingServiceValidator;

        public ScenarioValidatingServiceNameOnEditingDraftSuccess()
        {
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            _billingServiceValidator = TestContext.ServiceProvider.GetRequiredService<BillingServiceValidator>();
        }

        public override void Run()
        {
            var createBillingServiceCommandSuccess = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandSuccess.CreateBillingServiceTest.Value.BillingServiceStatus =
                BillingServiceStatusEnum.Draft;
            createBillingServiceCommandSuccess.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == createBillingServiceCommandSuccess.Id);
            Assert.IsNotNull(service);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id);
            var editBillingServiceDraftDto = billingServiceCardDto.Result.MapToBillingServiceDraftDto();

            editBillingServiceDraftDto.Name = new string('a', BillingServiceValidator.MaxLengthServiceName - 1);
            editBillingServiceDraftDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var res = _billingServiceValidator.ValidateEditServiceDraft(editBillingServiceDraftDto);
            if (!res.Success)
                throw new InvalidOperationException(res.Message);
        }
    }
}
