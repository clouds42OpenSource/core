﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.BillingServices.Validators;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateService
{
    /// <summary>
    ///  Редактирование черновика с валидными полями файла 1С
    ///     1. Создаем и записываем сервис в статусе черновик и все необходимые зависимости.
    ///     2. Получаем модель редактирования сервиса.
    ///     3. Меняем значения полей файла 1С на валидные.
    ///     4. Записываем сервис и не видим ошибку.
    /// </summary>
    public class ScenarioValidatingService1CFilesOnEditingDraftSuccess : ScenarioBase
    {
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;
        private readonly BillingServiceValidator _billingServiceValidator;

        public ScenarioValidatingService1CFilesOnEditingDraftSuccess()
        {
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            _billingServiceValidator = TestContext.ServiceProvider.GetRequiredService<BillingServiceValidator>();
        }

        public override void Run()
        {
            var createBillingServiceCommandSuccess = new CreateBillingServiceCommand(TestContext);
            createBillingServiceCommandSuccess.CreateBillingServiceTest.Value.BillingServiceStatus =
                BillingServiceStatusEnum.Draft;
            createBillingServiceCommandSuccess.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == createBillingServiceCommandSuccess.Id);
            Assert.IsNotNull(service);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id);
            var editBillingServiceDtoDraft = billingServiceCardDto.Result.MapToBillingServiceDraftDto();

            var res = _billingServiceValidator.ValidateEditServiceDraft(editBillingServiceDtoDraft);
            if (!res.Success)
                throw new InvalidOperationException(res.Message);
        }
    }
}
