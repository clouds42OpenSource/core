﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ArchivDatabases
{
    /// <summary>
    /// Сценрации провеки выборки аккаунтов для архивации баз и файлов
    /// </summary>
    public class ArchivDatabaseJobSamplesTest : ScenarioBase
    {
        private readonly IAccountDatabaseManager _accountDatabaseManager;

        public ArchivDatabaseJobSamplesTest()
        {
            _accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();
        }

        public override void Run()
        {
            CheckArchivateDatabasesListIfAccountIsDemoAndRent1CExpired7DaysAgo();
            CheckArchivateDatabasesListIfAccountIsDemoAndRent1CExpired30DaysAgo();
            CheckArchivateDatabasesListIfAccountNotDemo();
            CheckArchivateDatabasesListIfAccountNotDemoAndRent1CExpired7DaysAgo();
        }

        /// <summary>
        /// Проверка, что в выборку не попадают демо аккаунты, у которых дата аренды истекла 7 дней назад
        /// Сценарий:
        ///         1) Создаем аккаунт, делаем дату окончания Аренды -7 дней с сегодняшнего дня
        ///         2) Выполняем выборку аккаунтов для архивации баз
        ///         3) Проверяем, что аккаунт не попал в выбоку и выборка пустая
        /// </summary>
        private void CheckArchivateDatabasesListIfAccountIsDemoAndRent1CExpired7DaysAgo()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            var daysAfterServiceExpired = 7;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-daysAfterServiceExpired)
            });

            var archiveDatabaseList = _accountDatabaseManager.GetArchiveDatabaseList();
            Assert.IsFalse(archiveDatabaseList.Error, archiveDatabaseList.Message);

            var archiveDatabaseModels = archiveDatabaseList.Result;
            Assert.IsTrue(archiveDatabaseModels.Count == 0, "В списке не должно быть баз, т.к. аккаунт демо");
            ClearGarbage();
        }

        /// <summary>
        /// Проверка, что в выборку не попадают демо аккаунты, у которых дата аренды истекла 30 дней назад
        /// Сценарий:
        ///         1) Создаем аккаунт, делаем дату окончания Аренды -30 дней с сегодняшнего дня
        ///         2) Выполняем выборку аккаунтов для архивации баз
        ///         3) Проверяем, что аккаунт не попал в выбоку и выборка пустая
        /// </summary>
        private void CheckArchivateDatabasesListIfAccountIsDemoAndRent1CExpired30DaysAgo()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            var daysAfterServiceExpired = 30;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-daysAfterServiceExpired)
            });

            var archiveDatabaseList = _accountDatabaseManager.GetArchiveDatabaseList();
            Assert.IsFalse(archiveDatabaseList.Error, archiveDatabaseList.Message);

            var archiveDatabaseModels = archiveDatabaseList.Result;
            Assert.IsTrue(archiveDatabaseModels.Count == 0, "В списке не должно быть баз, т.к. аккаунт демо");
            ClearGarbage();
        }

        /// <summary>
        /// Проверка попадпния в выборку аккаунта (не демо) однажды продливавшим аренду
        /// Сценарий:
        ///         1) Создаем аккаунт, делаем дату окончания Аренды -1 день с сегодняшнего дня
        ///         2) Покумаем Аренду, делаем дату окончания Аренды -30 дней с сегодняшнего дня
        ///         3) Выполняем выборку аккаунтов для архивации баз
        ///         4) Проверяем, что аккаунт попал в выборку
        /// </summary>
        private void CheckArchivateDatabasesListIfAccountNotDemo()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            var service = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);

            var daysAfterServiceExpired = 30;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-1)
            });

            var localeId = GetAccountLocaleId(account.Id);
            var rent1CStandartCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == service.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = rent1CStandartCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-daysAfterServiceExpired)
            });

            var archiveDatabasesList = _accountDatabaseManager.GetArchiveDatabaseList();
            Assert.IsFalse(archiveDatabasesList.Error, archiveDatabasesList.Message);

            var archiveDatabaseModels = archiveDatabasesList.Result.First();
            Assert.IsNotNull(archiveDatabasesList);
            Assert.AreEqual(account.Id, archiveDatabaseModels.AccountId);
            Assert.AreEqual(createAccountDatabaseAndAccountCommand.AccountDatabaseId, archiveDatabaseModels.DataBaseId);
            ClearGarbage();
        }

        /// <summary>
        /// Проверка, что в выборку не попадают не демо аккаунты, у которых дата аренды истекла 7 дней назад
        /// Сценарий:
        ///         1) Создаем аккаунт, создаем платеж на произвольную сумму
        ///         2) Делаем дату окончания Аренды -7 дней с сегодняшнего дня
        ///         3) Выполняем выборку аккаунтов для архивации баз
        ///         4) Проверяем, что аккаунт не попал в выбоку и выборка пустая
        /// </summary>
        private void CheckArchivateDatabasesListIfAccountNotDemoAndRent1CExpired7DaysAgo()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            var service = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>()
                .GetSystemService(Clouds42Service.MyEnterprise);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 100,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var archiveDatabasesList = _accountDatabaseManager.GetArchiveDatabaseList();
            Assert.IsFalse(archiveDatabasesList.Error, archiveDatabasesList.Message);
            Assert.IsTrue(archiveDatabasesList.Result.Count == 0, "Выборка должна быть пустой");
            ClearGarbage();
        }
    }
}