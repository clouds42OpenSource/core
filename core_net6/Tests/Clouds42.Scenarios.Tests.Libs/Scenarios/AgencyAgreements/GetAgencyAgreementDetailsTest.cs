﻿using System;
using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Получить детальную информацию об агентском соглашении
    /// Сценарий:
    ///         1) Создам агентское соглашение
    ///         2) Получаем детальную информацию по агентскому соглашению
    ///         3) Проверяем, что информация верна 
    /// </summary>
    public class GetAgencyAgreementDetailsTest : ScenarioBase
    {
        private readonly AgencyAgreementDataManager _agencyAgreementDataManager;

        public GetAgencyAgreementDetailsTest()
        {
            _agencyAgreementDataManager = TestContext.ServiceProvider.GetRequiredService<AgencyAgreementDataManager>();
        }

        public override void Run()
        {
            var agencyAgreement = new CreateAgencyAgreementCommand(TestContext);
            agencyAgreement.Run();

            var effectiveDate = DateTime.Now.AddDays(5);

            var agencyAgreement2 = new CreateAgencyAgreementCommand(TestContext, new CreateAgencyAgreementDtoTest(DbLayer)
            {
                EffectiveDate = effectiveDate
            });
            agencyAgreement2.Run();

            var res = _agencyAgreementDataManager.GetAgencyAgreementDetails(agencyAgreement.Id);

            Assert.IsFalse(res.Error);
            Assert.AreEqual(agencyAgreement.Name, res.Result.Name);
            
            var res2 = _agencyAgreementDataManager.GetAgencyAgreementDetails(agencyAgreement2.Id);

            Assert.IsFalse(res2.Error);
            Assert.AreEqual(agencyAgreement2.Name, res2.Result.Name);

        }
    }
}
