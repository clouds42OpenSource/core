﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Получить следующее агентское соглашение
    /// которое вступит в силу(если такое соглашение существует)
    /// Сценарий:
    ///         1) Получаем следующее агентское соглашение, проверяем, что такого нет
    ///         2) Создаем агентское соглашение
    ///         3) Получаем следующее агентское соглашение, проверяем, что агентское соглашение получено
    /// </summary>
    public class TryGetNextEffectiveAgencyAgreementTest : ScenarioBase
    {
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;
        public TryGetNextEffectiveAgencyAgreementTest()
        {
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
        }

        public override void Run()
        {
             var effectiveDate = DateTime.Now.AddDays(10);

            var agencyAgreement = new CreateAgencyAgreementCommand(TestContext, new CreateAgencyAgreementDtoTest(DbLayer)
            {
                EffectiveDate = effectiveDate
            });
            agencyAgreement.Run();

            var agreement = _agencyAgreementDataProvider.TryGetNextEffectiveAgencyAgreement();

            Assert.IsNotNull(agreement);
            Assert.AreEqual(agencyAgreement.Id, agreement.Id);
        }
    }
}
