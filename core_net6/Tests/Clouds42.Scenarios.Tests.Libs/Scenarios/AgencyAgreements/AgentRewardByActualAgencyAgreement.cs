﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Helpers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Расчет агентского вознаграждения за Аренду, Диск и Кастомный сервис
    /// Сценарий:
    ///         1)Создаем агентское соглашение
    ///         2) Создаем сервис с аккаунтом и создаем реферала для этого аккаунта
    ///         3) Отключаем все сервисы
    ///         4) Покупаем сервисы и докупаем обхем диска
    ///         5) Проверяем, что вознагражлдение агенту начислилось согласно агентскому соглашению
    /// </summary>
    public class AgentRewardByActualAgencyAgreement : ScenarioBase
    {
        private readonly CreatePaymentManager _createPaymentManager;
        private readonly BillingServiceDataProvider _billingServiceDataProvider;
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly Rent1CManageHelper _rent1CManager;
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        private readonly MyDiskManager _myDiskManager;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public AgentRewardByActualAgencyAgreement()
        {
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
            _cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _rent1CManager = (Rent1CManageHelper)TestContext.ServiceProvider.GetRequiredService<IRent1CManageHelper>();
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
            _myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var agreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();
            Assert.IsNotNull(agreement);

            var rent1CPercent = agreement.Rent1CRewardPercent;
            var myDiskPercent = agreement.MyDiskRewardPercent;
            var ownServicePercent = agreement.ServiceOwnerRewardPercent;

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });

            createBillingServiceCommand.Run();

            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);

            var account =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == billingService.AccountOwnerId);

            var referral = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = account.Id
                }
            });
            referral.Run();

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(billingService.Id, referral.AccountAdminId);

            var rent1CService = _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, referral.AccountId);
            var myDiscService = _cloud42ServiceFactory.Get(Clouds42Service.MyDisk, referral.AccountId);
            rent1CService.ActivateService();
            myDiscService.ActivateService();

            TurnOffRent1CForAccount(referral.AccountId);

            var billingAccount =
                dbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == referral.AccountId);

            TurnOffCustomService(createBillingServiceCommand.Id);

            const int paymentSum = 4000;
            const int discSize = 50;

            var rent1CServiceId = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise).Id;

            var serviceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();

            var billingServiceTypeDtos = new List<CalculateBillingServiceTypeDto>
            {
                new()
                {
                    Subject = referral.AccountAdminId,
                    Status = true,
                    BillingServiceTypeId = billingService.BillingServiceTypes.First().Id,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        I = false,
                        Me = false,
                        Label = billingAccount.Account.AccountCaption
                    }
                }
            };

            CreateInflowPayment(referral.AccountId, paymentSum, rent1CServiceId);

            serviceManager.ApplyOrPayForAllChangesAccountServiceTypes(referral.AccountId, createBillingServiceCommand.Id, false,
                null, billingServiceTypeDtos);

            var changeSizeOfServicePreview =
                _cloud42ServiceHelper.ChangeSizeOfServicePreview(referral.AccountId, discSize);

            var myDiskChangeTariffDto = new MyDiskChangeTariffDto { AccountId = referral.AccountId, SizeGb = discSize, ByPromisedPayment = false };
            var result = _myDiskManager.ChangeMyDiskTariff(myDiskChangeTariffDto);
            Assert.IsTrue(result.Result);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var wallet = dbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == account.Id);

            var resConfRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsNotNull(resConfRent1C);

            var rent1CReward = RewardForEachService(resConfRent1C.Cost, rent1CPercent);
            var myDiskReward = RewardForEachService(changeSizeOfServicePreview.Cost, myDiskPercent);
            var ownServiceReward = RewardForEachService(serviceTypeDtos[0].ServiceTypeCost, ownServicePercent);

            var totalReward = rent1CReward + myDiskReward + ownServiceReward;

            Assert.AreEqual(totalReward, wallet.AvailableSum);
        }

        /// <summary>
        /// Отключить Арнду 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id акаунта</param>
        private void TurnOffRent1CForAccount(Guid accountId)
        {
            _rent1CManager.ManageRent1C(new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-10),
            }, accountId);
        }

        /// <summary>
        /// Создать входящий платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Сумма платежа</param>
        private void CreateInflowPayment(Guid accountId, decimal cost, Guid billingServiceId)
        {
            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingServiceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }

        /// <summary>
        /// Расчитать агентское вознаграждение для каждого сервиса
        /// </summary>
        /// <param name="sum">Стоимость сервиса</param>
        /// <param name="rewardPercent">Доля агентского вознаграждения</param>
        /// <returns></returns>
        private decimal RewardForEachService(decimal sum, decimal rewardPercent)
        {
            return sum * rewardPercent / 100;
        }

        /// <summary>
        /// Отключить кастомный сервис
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        private void TurnOffCustomService(Guid billingServiceId)
        {
            var customBillingService =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                    w => w.BillingServiceId == billingServiceId);
            customBillingService.IsDemoPeriod = false;
            customBillingService.ExpireDate = null;
            customBillingService.Frozen = null;
            DbLayer.ResourceConfigurationRepository.Update(customBillingService);
            DbLayer.Save();
        }
    }
}
