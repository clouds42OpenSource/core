﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Получить актуальное агентское соглашение
    /// Сценарий: Создаем два агентских соглашения,
    ///           одно вступает в силу сейчас, а другое через 10 дней,
    ///           получаем актуальное агентское соглашение, проверяем,
    ///           что получили то, которое вступаетв силу сейчас 
    /// </summary>
    public class GetActualAgencyAgreementTest : ScenarioBase
    {
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;

        public GetActualAgencyAgreementTest()
        {
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var agencyAgreement = dbLayer.AgencyAgreementRepository.FirstOrDefault();

            Assert.IsNotNull(agencyAgreement);

            var effectiveDate = DateTime.Now.AddDays(10);

            var agencyAgreementCommand2 = new CreateAgencyAgreementCommand(TestContext, new CreateAgencyAgreementDtoTest(DbLayer)
            {
                EffectiveDate = effectiveDate
            });
            agencyAgreementCommand2.Run();
             
            var actualAgencyAgreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();

            Assert.IsNotNull(actualAgencyAgreement, "Не удалось найти агентское соглашение");
            Assert.AreEqual(agencyAgreement.Name, actualAgencyAgreement.Name);
        }
    }
}
