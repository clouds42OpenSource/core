﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Получить данные агентских соглашений
    /// Сценарий:
    ///         1) Создаем агентские соглашения
    ///         2) Получаем данные агнетских соглашений
    ///         3) Проверяем количество соглашений в результате работы менеджера
    /// </summary>
    public class GetAgencyAgreementsDataTest : ScenarioBase
    {
        private readonly AgencyAgreementDataManager _agencyAgreementDataManager;

        public GetAgencyAgreementsDataTest()
        {
            _agencyAgreementDataManager = TestContext.ServiceProvider.GetRequiredService<AgencyAgreementDataManager>();
        }

        public override void Run()
        {
            var agencyAgreementCommand = new CreateAgencyAgreementCommand(TestContext);
            agencyAgreementCommand.Run();

            var agencyAgreementCommand2 = new CreateAgencyAgreementCommand(TestContext,new CreateAgencyAgreementDtoTest(DbLayer)
            {
                EffectiveDate = DateTime.Now.AddDays(10)
            });
            agencyAgreementCommand2.Run();

            var managerResult = _agencyAgreementDataManager.GetAgencyAgreementsData();

            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(3, managerResult.Result.AgencyAgreementInfos.Count);
        }
    }
}
