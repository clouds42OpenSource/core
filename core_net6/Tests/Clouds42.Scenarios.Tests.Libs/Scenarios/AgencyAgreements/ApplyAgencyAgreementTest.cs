﻿using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Partner;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgencyAgreements
{
    /// <summary>
    /// Принять агентское соглашение
    /// Сценарий:
    ///         1) Создаем пользоваетля, получаем сводную информацию для партнера
    ///         2) Проверяем, что соглашение еще не принято
    ///         3) Принимаем соглашение и снова получаем сводную информацию для партнера
    ///            и проверяем, что соглашение принято
    /// </summary>
    public class ApplyAgencyAgreementTest : ScenarioBase
    {
        private readonly PartnerManager _partnerManager;
        private readonly ApplyAgencyAgreementManager _applyAgencyAgreementManager;

        public ApplyAgencyAgreementTest()
        {
            _partnerManager = TestContext.ServiceProvider.GetRequiredService<PartnerManager>();
            _applyAgencyAgreementManager = TestContext.ServiceProvider.GetRequiredService<ApplyAgencyAgreementManager>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext);
            account.Run();
            
            var result = _partnerManager.GetSummaryInformationForPartner(account.AccountId);
            Assert.IsTrue(result.Result.NeedApplyAgencyAgreement, "Агенское соглашение еще не принято");

            var managerResult = _applyAgencyAgreementManager.Apply(account.AccountId);
            Assert.IsFalse(managerResult.Error, "Ошибка акцепта агентского соглашения");

            result = _partnerManager.GetSummaryInformationForPartner(account.AccountId);
            Assert.IsFalse(result.Result.NeedApplyAgencyAgreement, "Агентское соглашение должно быть принято");
        }
    }
}
