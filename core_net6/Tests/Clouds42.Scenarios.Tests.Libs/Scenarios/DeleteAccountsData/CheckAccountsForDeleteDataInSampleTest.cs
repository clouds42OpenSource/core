﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DeleteAccountsData
{
    /// <summary>
    /// Проверка выборки аккаунтов для удаления данных
    /// </summary>
    public class CheckAccountsForDeleteDataInSampleTest : ScenarioBase
    {
        private readonly InactiveAccountDataHelper _inactiveAccountDataHelper;

        public CheckAccountsForDeleteDataInSampleTest()
        {
            _inactiveAccountDataHelper = TestContext.ServiceProvider.GetRequiredService<InactiveAccountDataHelper>();
        }

        public override void Run()
        {
            CheckDemoAccountsForDeleteDataInSample();
        }

        /// <summary>
        /// Проверка попадает ли в выборку для удаления данных демо аккаунт,
        /// Аренда 1С у которого истекла 7 дней назад
        /// Сценарий: 
        ///         1) Создаем аккаунт, изменяем дату окончания Аренды 1С
        ///         2) Делаем выборку, проверяем, что аккаунит попал в выборку 
        /// </summary>
        private void CheckDemoAccountsForDeleteDataInSample()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext), new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            const int daysAfterServiceExpired = 7;

            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(account.Id, new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-daysAfterServiceExpired)
            });

            var accountsForDeleteData = _inactiveAccountDataHelper.GetAccountsForDeleteData();
            Assert.IsTrue(accountsForDeleteData.Contains(account.Id), $"Акаунт {account.Id} должен быть в выборке");
        }
    }
}
