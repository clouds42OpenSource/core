﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Delete.Helpers;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DeleteAccountsData
{
    /// <summary>
    /// Выборка аккаунтов для удаления данных
    /// Сценарий:
    ///         1) Создаем аккаунты, с базами, без баз, демо
    ///         2) Выполняем выборку
    ///         3) Проверяем, что в выборку попали аккаунты, соответствующие условиям выборки
    /// </summary>
    public class GetAccountsForDeleteDataTest : ScenarioBase
    {
        private readonly InactiveAccountDataHelper _inactiveAccountDataHelper;
        private readonly CreatePaymentManager _paymentsManager;

        public GetAccountsForDeleteDataTest()
        {
            _inactiveAccountDataHelper = TestContext.ServiceProvider.GetRequiredService<InactiveAccountDataHelper>();
            _paymentsManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
        }
        public override void Run()
        {
            var accountWithDb = new CreateAccountDatabaseAndAccountCommand(TestContext, accountModel: new AccountRegistrationModelTest
            {
                Login = $"TestOne{DateTime.Now:mmss}",
                Email = $"TestOne{DateTime.Now:mmss}@mail.ru"
            });
            accountWithDb.Run();

            var accountDemoWithDb = new CreateAccountDatabaseAndAccountCommand(TestContext, accountModel: new AccountRegistrationModelTest
            {
                Login = $"TestTwo{DateTime.Now:mmss}",
                Email = $"TestTwo{DateTime.Now:mmss}@mail.ru"
            });
            accountDemoWithDb.Run();

            var accountDemo = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestThree{DateTime.Now:mmss}",
                Email = $"TestThree{DateTime.Now:mmss}@mail.ru"
            });
            accountDemo.Run();

            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestFour{DateTime.Now:mmss}",
                Email = $"TestFour{DateTime.Now:mmss}@mail.ru"
            });
            account.Run();

            var activeAccount = new CreateAccountCommand(TestContext);
            activeAccount.Run();

            _paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.AccountId,
                Description = "Пополнение счета",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                TransactionType = TransactionType.Money,
                Total = 1500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            _paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = accountWithDb.AccountDetails.AccountId,
                Description = "Пополнение счета",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                TransactionType = TransactionType.Money,
                Total = 1500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var expireDate = DateTime.Now.AddDays(-90);

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId != activeAccount.AccountId)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, expireDate).SetProperty(y => y.Frozen, true)); ;

            var accountsForDeleteData = _inactiveAccountDataHelper.GetAccountsForDeleteData();
            
            Assert.IsFalse(accountsForDeleteData.Contains(account.AccountId), "Аккаунт без баз не должнен попадать в выборку");
            Assert.IsFalse(accountsForDeleteData.Contains(accountDemo.AccountId), "Демо аккаунт без баз не должен попасть в выборку");
            Assert.IsTrue(accountsForDeleteData.Contains(accountWithDb.AccountDetails.AccountId), "Аккаунт должен быть в выборке, тк есть базы");
            Assert.IsTrue(accountsForDeleteData.Contains(accountDemoWithDb.AccountDetails.AccountId), "Демо аккаунт должен быть в выборке, тк есть базы");
            Assert.IsFalse(accountsForDeleteData.Contains(activeAccount.AccountId), "Аккаунт не должен попасть в выборку, тк он активен");

        }
    }
}
