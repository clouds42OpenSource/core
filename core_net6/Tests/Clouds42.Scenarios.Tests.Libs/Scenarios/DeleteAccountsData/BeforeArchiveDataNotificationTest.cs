﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.AccountDatabase.Delete.Managers;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DeleteAccountsData
{
    /// <summary>
    /// Уведомление о скорой архивации баз и файлов аккаунта
    /// Сценарий:
    ///         1) Создаем два аккаунт, один из которых демо
    ///         2) Изменяем ExpireDate Аренды на -5 дней, с текущего момента, для демо аккаунта
    ///            и на -25 дней для второго аккаунта
    ///         3) Запускаем рассылку уведомлений, проверяем, что письма были отправлены
    /// </summary>
    public class BeforeArchiveDataNotificationTest : ScenarioBase
    {
        private readonly BeforeArchiveDataNotificationManager _beforeArchiveDataNotificationManager;
        private readonly BillingServiceDataProvider _billingServiceDataProvider;

        public BeforeArchiveDataNotificationTest()
        {
            _beforeArchiveDataNotificationManager = TestContext.ServiceProvider.GetRequiredService<BeforeArchiveDataNotificationManager>();
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account1 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            account1.Run();

            var account2 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            account2.Run();

            var billingService = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 1500;
            CreateInflowPayment(account1.AccountDetails.AccountId, billingService.Id, paymentSum);

            var demoAccountExpireDate = DateTime.Now.AddDays(-5);
            var accountExpireDate = DateTime.Now.AddDays(-25);

            ChangeRet1CExpireDate(account2.AccountDetails.AccountId, demoAccountExpireDate);
            ChangeRet1CExpireDate(account1.AccountDetails.AccountId, accountExpireDate);

            var managerResult = _beforeArchiveDataNotificationManager.SendNotifications();
            Assert.IsFalse(managerResult.Error, "Уведомление аккаунтов о скорой архивации баз и файлов завершилось с ошибкой");

            var notificationBuffer2 =
                dbLayer.NotificationBufferRepository.FirstOrDefault(nb => nb.Account == account2.AccountDetails.AccountId);
            Assert.IsNotNull(notificationBuffer2, $"Менеджеру аккаунта {account2.AccountDetails.Account.AccountCaption} не было отправлено письмо");

            var notificationBuffer1 =
                dbLayer.NotificationBufferRepository.FirstOrDefault(nb => nb.Account == account1.AccountDetails.AccountId);
            Assert.IsNotNull(notificationBuffer1,
                $"Менеджеру аккаунта {account1.AccountDetails.Account.AccountCaption} не было отправлено письмо");
        }

        /// <summary>
        /// Отключить Арнду 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id акаунта</param>
        /// <param name="expireDate">Дата окончания Аренды 1С</param>
        private void ChangeRet1CExpireDate(Guid accountId, DateTime expireDate)
        {
            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .Where(x => x.AccountId == accountId)
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, expireDate));
        }
    }
}
