﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    /// Проверка удаления доступа к базе на разделителях когда тот находится в оброботке
    /// Действия:
    /// 1) Создаём сервисный аккаунт с базой
    /// 2) Создаём второй аккаунт и базу на разделителях (доступ выдаётся сразу)
    /// 3) Создаём запись в таблице ManageDbOnDelimitersAccesses
    /// 4) Проверяем работу отбора доступа к базе на разделителях
    /// </summary>
    public class ScenarioWrongDeletingAccessToDbOnDelimitersTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();
            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            var accountAdminId = createAccAndDb.AccountDetails.AccountAdminId;

            #region 1check
            Assert.IsNotNull(database);
            Assert.AreEqual(false, database.IsFile);
            Assert.IsNotNull(database.AccountDatabaseOnDelimiter, "Созданная база не на разделителях.");

            var templateDelimiters = DbLayer.DbTemplateDelimitersReferencesRepository.All().ToList();

            Assert.IsTrue(templateDelimiters.Count > 0, "Не создался шаблон для базы на разделителях.");
            #endregion #region 1check

            var acDbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.AccountUserID == accountAdminId);

            var manageAccess = new ManageDbOnDelimitersAccess
            {
                Id = acDbAccess.ID,
                AccessState = AccountDatabaseAccessState.ProcessingDelete,
                StateUpdateDateTime = DateTime.Now
            };

            DbLayer.ManageDbOnDelimitersAccessRepository.Insert(manageAccess);
            DbLayer.Save();

            var acDbDelimitersManager = TestContext.ServiceProvider.GetRequiredService<IAcDbDelimitersManager>();

            var result = acDbDelimitersManager.DeleteAccessFromDelimiterDatabase(database, accountAdminId);

            Assert.IsNotNull(result.State);

        }
    }
}
