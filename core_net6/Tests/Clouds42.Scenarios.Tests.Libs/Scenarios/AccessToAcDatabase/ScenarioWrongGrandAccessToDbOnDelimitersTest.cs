﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    /// Проверка добавления доступа к базе на разделителях когда доступ уже есть
    /// 1) Создаём сервисный аккаунт с базой
    /// 2) Создаём второй аккаунт и базу на разделителях (доступ выдаётся сразу)
    /// 4) Проверяем работу выдачи доступа к базе на разделителях когда запись уже есть
    /// </summary>
    public class ScenarioWrongGrandAccessToDbOnDelimitersTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccAndDb.Run();
            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

            var account = createAccAndDb.AccountDetails.Account;
            var accountAdminId = createAccAndDb.AccountDetails.AccountAdminId;

            #region 1check
            Assert.IsNotNull(database);
            Assert.AreEqual(false, database.IsFile);
            Assert.IsNotNull(database.AccountDatabaseOnDelimiter, "Созданная база не на разделителях.");

            var templateDelimiters = DbLayer.DbTemplateDelimitersReferencesRepository.All().ToList();

            Assert.IsTrue(templateDelimiters.Count > 0, "Не создался шаблон для базы на разделителях.");
            #endregion #region 1check

            var acDbDelimitersManager = TestContext.ServiceProvider.GetRequiredService<IAcDbDelimitersManager>();

            var acDbAccessPostAddModelDTO =
            new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = database.Id,
                LocalUserID = Guid.Empty,
                AccountID = account.Id,
                AccountUserID = accountAdminId
            };

            var result = acDbDelimitersManager.AddAccessToDbOnDelimiters(acDbAccessPostAddModelDTO, database);

            Assert.AreEqual(result.State, ManagerResultState.PreconditionFailed, "Метод должен вернуть ошибку");
            Assert.IsTrue(result.Error, "Метод должен вернуть ошибку");

        }
    }
}

