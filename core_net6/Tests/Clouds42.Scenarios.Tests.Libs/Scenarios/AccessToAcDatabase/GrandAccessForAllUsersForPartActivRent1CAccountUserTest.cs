﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    /// Сценарий предоставления доступа к ИБ пользователям с включенной арендой у всех
    ///
    /// Предусловия: для аккаунта test_account подключены 3 пользователеля
    /// по тарифу Стандарт и 2 пользователя не подключены;
    ///
    /// Действия: Предоставляем доступ к базе всем
    ///
    /// Проверка: доступ предоставлен 3 пользователелям, и при повторном запросе вернет сообщение что для остальных надо подключить Аренду
    /// </summary>
    public class GrandAccessForAllUsersForPartActivRent1CAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;

            var listUser = new List<AccountUserRegistrationToAccountTest>();
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>();
            for (int i = 0; i < 5; i++)
            {
                var user = new AccountUserRegistrationToAccountTest
                {
                    AccountId = accountId,
                    AccountIdString = SimpleIdHash.GetHashById(accountId),
                    Login = $"UserTest_{i}_{i}",
                    Email = $"usertest{i}@mail.ru",
                    FirstName = $"UserTest{i}_{i}",
                    FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(i)
                };
                listUser.Add(user);

            }
            foreach (var user in listUser)
            {
                var res = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(user).Result;
                if (res == null)
                    throw new InvalidOperationException("Ошибка создания нового пользователя");
                var access = new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = res.Result,
                    StandartResource = true,
                    WebResource = false,
                    SponsorAccountUser = false
                };
                accessRent1C.Add(access);

            }
            accessRent1C.RemoveAt(1);
            accessRent1C.RemoveAt(2);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 20000;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, paymentSum);

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == accountId);
            rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, accessRent1C);
            
            var result = accessToAcDbOnDelimitersManager.GrandAccessForAllUsersOfAccount(accountDatabaseId);

            if(!result.Success)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для всех пользователей завершился с ошибкой {result.Message}!!!");

            if (result.Message != "Доступ не у всех")
                throw new InvalidOperationException("Не вернуло сообщение о том что доступ предоставлен всем, кроме тех у кого не подключена аренда!!!");

            var usersQuery =
                from user in DbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery().Where(au => au.AccountId == accountId)
                join access in
                    DbLayer.AcDbAccessesRepository.WhereLazy(
                        a =>
                            a.AccountDatabaseID == accountDatabaseId &&
                            a.AccountUserID != null &&
                            a.AccountID == accountId)
                    on user.Id equals access.AccountUserID into joinedAccess
                from access in joinedAccess.DefaultIfEmpty()
                where access == null
                select user;

            var accountUsersList = usersQuery.Distinct().ToList();
            Assert.AreEqual(2, accountUsersList.Count);

            result = accessToAcDbOnDelimitersManager.GrandAccessForAllUsersOfAccount(accountDatabaseId);

            if (result.Success)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для всех пользователей завершился с ошибкой {result.Message}!!!");

            if (result.Message != "Доступ не у всех")
                throw new InvalidOperationException("Не вернуло сообщение о том что доступ предоставлен всем, кроме тех у кого не подключена аренда!!!");

            usersQuery =
                from user in DbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery().Where(au => au.AccountId == accountId)
                join access in
                    DbLayer.AcDbAccessesRepository.WhereLazy(
                        a =>
                            a.AccountDatabaseID == accountDatabaseId &&
                            a.AccountUserID != null &&
                            a.AccountID == accountId)
                    on user.Id equals access.AccountUserID into joinedAccess
                from access in joinedAccess.DefaultIfEmpty()
                where access == null
                select user;

             accountUsersList = usersQuery.Distinct().ToList();
            Assert.AreEqual(2, accountUsersList.Count);
        }
    }
}
