﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Common.ManagersResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    ///     Сценарий предоставления доступа к ИБ внешнему пользователю с включенной арендой
    ///
    ///     Предусловия: для внешнего аккаунта подключена аренда 1С
    ///     по тарифу Стандарт;
    ///
    ///     Действия: Предоставляем доступ к базе, "открываем" базу внешнего аккаунта
    ///
    ///     Проверка: доступ предоставлен, сообщений никаких нет, серверная часть открытия
    ///     карточки базы отрабатывает
    /// </summary>
    public class GrandExternalAccessToDbForActivRent1CAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(createAccountDatabaseAndAccountCommand.AccountDatabaseId);
            var externalUserId = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountAdminId;

            var externalUser = DbLayer.AccountUsersRepository.GetAccountUser(externalUserId);
           
            var result = accessToAcDbOnDelimitersManager.GrandExternalAccessToDb(accountDatabase.Id, externalUser.Email);

            if(!result.Success || !string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе {accountDatabase.Id} для пользователя {externalUserId} завершился с ошибкой {result.Message}!!!");

            var dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.AccountDatabaseID == accountDatabase.Id && a.AccountUserID == externalUserId);

            if(dbAccess == null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabase.Id} для пользователя {externalUserId} не предоставлен!!!");
            
            var accountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();

            var managerResult = accountDatabaseDataManager.GetAccountDatabaseInfo(accountDatabase.V82Name, createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId);

            Assert.IsFalse(managerResult.Error, managerResult.Message);
            Assert.IsNotNull(managerResult.Result, "Ошибка получения модели базы");
            Assert.AreEqual(managerResult.State, ManagerResultState.Ok, "Ошибка получения модели.");
        }
    }
}
