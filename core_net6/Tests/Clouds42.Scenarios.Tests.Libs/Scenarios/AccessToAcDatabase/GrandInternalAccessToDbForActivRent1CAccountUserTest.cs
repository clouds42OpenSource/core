﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    ///     Сценарий предоставления доступа к ИБ пользователю с включенной арендой
    ///
    ///     Предусловия: для аккаунта test_account подключен пользовател test_user_1
    /// по тарифу Стандарт;
    ///
    ///     Действия: Предоставляем доступ к базе
    ///
    ///     Проверка: доступ предоставлен, сообщений никаких нет
    /// </summary>
    public class GrandInternalAccessToDbForActivRent1CAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = accountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "Qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid():N}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };
            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 3000;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, paymentSum);
           
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == accountId);
            rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            var result = accessToAcDbOnDelimitersManager.GrandInternalAccessToDb(accountDatabaseId, accountUserId);

            if(!result.Success || !string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} завершился с ошибкой {result.Message}!!!");

            var dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a =>
                a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == accountUserId);

            if(dbAccess == null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} не предоставлен!!!");
        }
    }
}
