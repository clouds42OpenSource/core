﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    ///     Сценарий предоставления доступа к ИБ пользователю с отключенной арендой
    ///
    ///     Предусловия: для аккаунта test_account подключен пользовател test_user_1
    ///     но аренда не подключена;
    ///
    ///     Действия: Предоставляем доступ к базе
    ///
    ///     Проверка: доступ не предоставлен, вернет сообщение
    /// </summary>
    public class GrandInternalAccessToDbForNoActivRent1CAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = accountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = "Qwerty_123",
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid():N}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };
            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;

            var result = accessToAcDbOnDelimitersManager.GrandInternalAccessToDb(accountDatabaseId, accountUserId);

            if(result.Success || string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} предоставлен и сообщения нет!!!");

            var dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == accountUserId && a.AccountID == accountId);

            if(dbAccess != null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} предоставлен!!!");
        }
    }
}
