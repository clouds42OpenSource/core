﻿using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Accounts.Account.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Common.ManagersResults;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    /// Тест на корректность получения признака необходимости отображать
    /// ссылку и кнопку запуска БД
    /// Действия:
    /// 1) Создаём аккаунт и базу
    /// 2) Проверяем, что признак доступности кнопки запуска = true
    /// 3) Отбираем доступ админа к базе
    /// 4) Проверяем, что признак доступности кнопки запуска = flase
    /// </summary>
    public class ScenarioCheckNeedToRunButtonShowingTest: ScenarioBase
    {
        private readonly IAccountDatabasesDataManager _accountDatabasesDataManager;

        public ScenarioCheckNeedToRunButtonShowingTest()
        {
            _accountDatabasesDataManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabasesDataManager>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;
            var accountAdminId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId;
            var database = DbLayer.DatabasesRepository.GetAccountDatabase(createAccountDatabaseAndAccountCommand.AccountDatabaseId);

            var accountDatabaseDataManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseCardManager>();
            var databaseDataItemDto = GetDatabasesDataByFilter(account.Id, accountAdminId).FirstOrDefault(w => w.Id == database.Id);

            var result = accountDatabaseDataManager.GetAccountDatabaseInfo(database.V82Name, account.Id);

            var needShow = databaseDataItemDto.NeedShowWebLink;
            Assert.IsTrue(needShow, "Кнопка и ссылка должны быть доступны.");

            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var accessResult = accessToAcDbOnDelimitersManager.DeleteAccessFromDb(database.Id, accountAdminId);
            if (!accessResult.Success || !string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException(
                    $"Доступ к базе {database.Id} для пользователя {accountAdminId} предоставлен и завершился с ошибкой {accessResult.Message}!!!");

            RefreshDbCashContext(Context.AcDbAccesses);

            var dataByFilter = GetDatabasesDataByFilter(account.Id, accountAdminId).FirstOrDefault(db => db.Id == database.Id);
            Assert.IsFalse(dataByFilter.NeedShowWebLink, "Кнопка и ссылка должны быть доступны.");
        }

        /// <summary>
        /// Получить данные информационных баз
        /// по фильтру
        /// </summary>
        /// <param name="args">Модель фильтра для выбора баз</param>
        /// <returns>Данные информационных баз</returns>
        private AccountDatabaseDataItemDto[] GetDatabasesDataByFilter(Guid accountId, Guid accountAdminId)
        {
            var dataByFilter = _accountDatabasesDataManager.GetDatabasesDataByFilter(new AccountDatabasesFilterParamsDto
            {
                PageNumber = 1,
                Filter = new AccountDatabasesFilterDto
                {
                    AccountId = accountId,
                    AccountUserId = accountAdminId,
                    PageSize = 10,

                }
            });
            Assert.IsFalse(dataByFilter.Error, "Ошибка получения модели");
            Assert.AreEqual(dataByFilter.State, ManagerResultState.Ok, "Ошибка получения модели");
            return dataByFilter.Result.Records;
        }
    }
}
