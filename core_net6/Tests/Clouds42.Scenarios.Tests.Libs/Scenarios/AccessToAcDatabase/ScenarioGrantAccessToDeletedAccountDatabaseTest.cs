﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    /// Сценарий выдачи доступа к удаленной инф. базе
    /// Действия:
    ///     1) Создаем аккаунт с активной арендой и инф. базой
    ///     2) Имитируем удаление инф. базы
    ///     3) Пытаемся выдать доступ
    ///     4) Проверяем что доступ не выдан и вернулось предупреждение
    /// </summary>
    public class ScenarioGrantAccessToDeletedAccountDatabaseTest : ScenarioBase
    {
        private readonly Rent1CConfigurationAccessManager _rent1CConfigurationAccessManager;
        private readonly AccountDatabaseUserAccessManager _accountDatabaseUserAccessManager;

        public ScenarioGrantAccessToDeletedAccountDatabaseTest()
        {
            _rent1CConfigurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _accountDatabaseUserAccessManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта и инф. базы

            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;

            var newUserModel = new AccountUserDcTest { AccountId = accountId };
            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;

            #endregion

            #region Активация аренды

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            var paymentSum = 3000;
            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, paymentSum);

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == accountId);
            _rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = accountUserId, StandartResource = true }
            ]);

            #endregion

            #region Имитация удаления базы

            var accountDatabase = DbLayer.DatabasesRepository.GetAccountDatabase(accountDatabaseId);
            Assert.IsNotNull(accountDatabase);

            accountDatabase.StateEnum = DatabaseState.DeletedFromCloud;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();

            #endregion

            #region Попытка выдачи доступа

            var result = _accountDatabaseUserAccessManager.GrandInternalAccessToDb(accountDatabaseId, accountUserId);
            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Message);

            var acDbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a =>
                a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == accountUserId);
            Assert.IsNull(acDbAccess);

            #endregion
        }
    }
}
