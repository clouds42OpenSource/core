﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    ///     Сценарий удаления доступа к ИБ пользователю
    ///
    ///     Предусловия: для аккаунта test_account подключена Аренда 1С по тарифу Стандарт;
    ///
    ///     Действия: Удаление доступа к базе
    ///
    ///     Проверка: доступ удален, сообщений никаких нет
    /// </summary>
    public class DeleteAccessFromDbForAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;
            var accountUserId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId;

            var dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == accountUserId);

            if (dbAccess == null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} не предоставлен!!!");

            var result = accessToAcDbOnDelimitersManager.DeleteAccessFromDb(accountDatabaseId, accountUserId);

            if (!result.Success || !string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} предоставлен и завершился с ошибкой {result.Message}!!!");

            dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == accountUserId);

            if(dbAccess != null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {accountUserId} предоставлен!!!");
        }
    }
}
