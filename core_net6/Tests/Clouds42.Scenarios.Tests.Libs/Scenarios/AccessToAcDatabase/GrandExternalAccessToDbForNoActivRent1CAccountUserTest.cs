﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessToAcDatabase
{
    /// <summary>
    ///     Сценарий предоставления доступа к ИБ внешнему пользователю с отключенной арендой
    ///
    ///     Предусловия: для внешнего аккаунта не подключена аренда 1С
    ///
    ///     Действия: Предоставляем доступ к базе
    ///
    ///     Проверка: доступ не предоставлен, вернет сообщение
    /// </summary>
    public class GrandExternalAccessToDbForNoActivRent1CAccountUserTest : ScenarioBase
    {
        public override void Run()
        {
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var accessToAcDbOnDelimitersManager = ServiceProvider.GetRequiredService<AccountDatabaseUserAccessManager>();

            var createAccountDatabaseAndAccountCommand = new CreateAccountdatabaseAndTwoAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountDatabaseId = createAccountDatabaseAndAccountCommand.AccountDatabaseId;
            var externalUserId = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountAdminId;
            var externalAccountId = createAccountDatabaseAndAccountCommand.SecondAccountDetails.AccountId;

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == externalAccountId);
            rent1CConfigurationAccessManager.ConfigureAccesses(billingAccount.Id, [
                new() { AccountUserId = externalUserId, StandartResource = false }
            ]);
            var externalUser = DbLayer.AccountUsersRepository.GetAccountUser(externalUserId);
            
            var result = accessToAcDbOnDelimitersManager.GrandExternalAccessToDb(accountDatabaseId, externalUser.Email);

            if (result.Success || string.IsNullOrWhiteSpace(result.Message))
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {externalUserId} предоставлен и сообщения нет!!!");

            var dbAccess = DbLayer.AcDbAccessesRepository.FirstOrDefault(a =>
                a.AccountDatabaseID == accountDatabaseId && a.AccountUserID == externalUserId &&
                a.AccountID == accountId);

            if (dbAccess != null)
                throw new InvalidOperationException($"Доступ к базе {accountDatabaseId} для пользователя {externalUserId} предоставлен!!!");
        }
    }
}
