﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта когда у счета нет продуктов счета.
    /// Действия:
    /// 1. Создаем аккаунт. [AccountCreation]
    /// 2. Создаем счет на указанную сумму. [InvoiceCreation]
    /// 3. Проверяем, что нельзя изменить локаль если есть счета без продуктов счета. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenInvoiceHasNoInvoiceProductsTest : ScenarioBase
    {
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;

        public ScenarioChangeLocaleAccountWhenInvoiceHasNoInvoiceProductsTest()
        {
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            #endregion

            #region InvoiceCreation

            var invoiceProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceProvider>();
            invoiceProvider.CreateInvoiceForSpecifiedInvoiceAmount(createAccountCommand.AccountId, 9000);

            #endregion

            #region ChangeAccountLocale

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);

            editAccountDto.LocaleId = localeUkr;
            var result = _editAccountManager.UpdateAccount(editAccountDto);

            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}
