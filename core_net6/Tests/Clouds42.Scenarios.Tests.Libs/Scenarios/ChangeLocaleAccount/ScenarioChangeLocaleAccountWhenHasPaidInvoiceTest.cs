﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта когда есть оплаченный счет.
    /// 1. Создаем аккаунт. [AccountCreation]
    /// 2. Создаем счет и меняем статус счета на оплаченный. [InvoiceCreation]
    /// 3. Проверим, что изменить локаль нельзя если есть оплаченный счет. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenHasPaidInvoiceTest : ScenarioBase
    {
        private readonly IInvoiceProvider _invoiceProvider;
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;

        public ScenarioChangeLocaleAccountWhenHasPaidInvoiceTest()
        {
            _invoiceProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceProvider>();
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            #endregion

            #region InvoiceCreation

            _invoiceProvider.CreateInvoiceBasedOnCalculationInvoiceModel(createAccountCommand.AccountId,
                new CalculationOfInvoiceRequestModel
                    { PayPeriod = PayPeriod.Month1, MyDiskSize = 150});

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.AccountId == createAccountCommand.AccountId);
            invoice.State = InvoiceStatus.Processed.ToString();
            DbLayer.InvoiceRepository.Update(invoice);
            DbLayer.Save();

            #endregion

            #region ChangeAccountLocale

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);
            editAccountDto.LocaleId = localeUkr;

            var result = _editAccountManager.UpdateAccount(editAccountDto);
            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}
