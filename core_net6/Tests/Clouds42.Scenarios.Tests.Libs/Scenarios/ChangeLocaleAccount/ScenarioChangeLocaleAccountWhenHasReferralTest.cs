﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта когда у него есть реферал.
    /// Действия:
    /// 1) Создаём аккаунт-реферал. [AccountReferalCreation]
    /// 2) Создаём второй аккаунт по фереральной ссылке. [AccountByReferralLinkCreation]
    /// 3) Проверяем, что нельзя изменить локаль у аккаунта, зарегистрированного по реф ссылке. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenHasReferralTest : ScenarioBase
    {
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;
        public ScenarioChangeLocaleAccountWhenHasReferralTest()
        {
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
        }
        public override void Run()
        {
            #region AccountReferalCreation

            var createRefAccountCommand = new CreateAccountCommand(TestContext);
            createRefAccountCommand.Run();

            var referralAcc = createRefAccountCommand.Account;

            #endregion

            #region AccountByReferralLinkCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = referralAcc.Id
                }
            });
            createAccountCommand.Run();

            #endregion

            #region ChangeAccountLocale

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            editAccountDto.LocaleId = localeUkr;
            var result = _editAccountManager.UpdateAccount(editAccountDto);

            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}