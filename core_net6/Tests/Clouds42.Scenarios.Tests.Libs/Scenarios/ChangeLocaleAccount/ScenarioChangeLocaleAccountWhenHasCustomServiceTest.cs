﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта, когда есть не системный кастомный сервис.
    /// Действия:
    /// 1. Создаем сервис и аккаунт с этим сервисом. [AccountAndServiceCreation]
    /// 2. Проверим, что нельзя изменить локаль если есть не системный кастомный сервис. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenHasCustomServiceTest : ScenarioBase
    {
        private readonly AccountMapperTest _accountMapperTest;
        private readonly EditAccountManager _editAccountManager;

        public ScenarioChangeLocaleAccountWhenHasCustomServiceTest()
        {
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
        }
        public override void Run()
        {
            #region AccountAndServiceCreation

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var webRent1CServiceType = GetWebServiceTypeOrThrowException();
            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(2, webRent1CServiceType.Id);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });

            createAccountCommand.Run();

            #endregion

            #region ChangeAccountLocale

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);
            editAccountDto.LocaleId = localeUkr;

            var result = _editAccountManager.UpdateAccount(editAccountDto);

            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}
