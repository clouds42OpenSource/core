﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Common.Constants;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунта когда есть счет на произвольную сумму.
    /// Действия:
    /// 1. Создаем аккаунт. [AccountCreation]
    /// 2. Создаем счет на произвольную сумму. [InvoiceCreation]
    /// 3. Проверяем, что изменить локаль нельзя если есть счет на произвольную сумму. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenInvoiceOnSuggestedSumTest : ScenarioBase
    {

        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;
        private readonly IInvoiceProvider _invoiceProvider;

        public ScenarioChangeLocaleAccountWhenInvoiceOnSuggestedSumTest()
        {
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
            _invoiceProvider = ServiceProvider.GetRequiredService<IInvoiceProvider>();
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            #endregion

            #region InvoiceCreation

            _invoiceProvider.CreateInvoiceBasedOnCalculationInvoiceModel(createAccountCommand.AccountId,
                new CalculationOfInvoiceRequestModel
                    { SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = 500, ServiceTypeContent = "500" } });

            #endregion

            #region ChangeAccountLocale

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            editAccountDto.LocaleId = localeUkr;
            var result = _editAccountManager.UpdateAccount(editAccountDto);

            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}
