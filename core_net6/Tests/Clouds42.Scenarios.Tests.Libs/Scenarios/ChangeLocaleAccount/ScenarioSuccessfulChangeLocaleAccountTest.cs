﻿using System;
using System.Linq;
using Clouds42.Accounts.Account.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста успешного изменения локали аккаунта.
    /// Действия:
    /// 1. Создаем аккаунт и счет. [AccountAndInvoiceCreation]
    /// 2. Выполняем смену локали аккаунта. [ChangeAccountLocale]
    /// 3. Проверяем сменилась ли локаль. [CheckLocaleWasChanged]
    /// 4. Проверяем пересчитался ли счет. [CheckInvoiceWasRecounted]
    /// 5. Проверяем пересчитались ли продукты счета. [CheckInvoiceWasRecounted]
    /// 6. Проверяем пересчитались ли ресурсы. [CheckResourcesWereRecounted]
    /// 7. Проверяем пересчиталась ли стоимость сервиса. [CheckResourscesConfigurationWasRecounted]
    /// </summary>
    public class ScenarioSuccessfulChangeLocaleAccountTest : ScenarioBase
    {
        private readonly IInvoiceProvider _invoiceProvider;
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;
        private readonly CalculationOfInvoiceProvider _calculationOfInvoiceProvider;

        public ScenarioSuccessfulChangeLocaleAccountTest()
        {
            _invoiceProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceProvider>();
            _editAccountManager = TestContext.ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = TestContext.ServiceProvider.GetRequiredService<AccountMapperTest>();
            _calculationOfInvoiceProvider = TestContext.ServiceProvider.GetRequiredService<CalculationOfInvoiceProvider>();
            TestContext.ServiceProvider.GetRequiredService<IAccessProvider>();
        }

        public override void Run()
        {
            #region AccountAndInvoiceCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var resourcesCountBeforeLocaleChanged = DbLayer.ResourceRepository
                .Where(res => res.AccountId == createAccountCommand.AccountId).Count();

            var calculationOfInvoiceRequestModel = new CalculationOfInvoiceRequestModel
            {
                PayPeriod = PayPeriod.Month1,
                MyDiskSize = 50
            };

            var invoice = _invoiceProvider.CreateInvoiceBasedOnCalculationInvoiceModel(createAccountCommand.AccountId,
                calculationOfInvoiceRequestModel);

            #endregion

            #region ChangeAccountLocale

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);
            editAccountDto.LocaleId = localeUkr;

            var result = _editAccountManager.UpdateAccount(editAccountDto);
            if (result.Error)
                throw new InvalidOperationException(result.Message);

            #endregion

            #region CheckLocaleWasChanged

            var account = DbLayer.AccountsRepository.FirstOrDefault(acc => acc.Id == editAccountDto.Id);
            Assert.IsNotNull(account);

            if (GetAccountLocaleId(createAccountCommand.AccountId) != localeUkr)
                throw new InvalidOperationException("Локаль аккаунта не изменилась");

            #endregion

            #region CheckInvoiceWasRecounted

            CheckInvoiceWasRecounted(createAccountCommand.AccountId, invoice, calculationOfInvoiceRequestModel);

            #endregion

            #region CheckResourcesWereRecounted

            var resourcesCountAfterLocaleChanged = DbLayer.ResourceRepository
                .Where(res => res.AccountId == createAccountCommand.AccountId).Count();

            if (resourcesCountBeforeLocaleChanged != resourcesCountAfterLocaleChanged)
                throw new InvalidOperationException("Изменилось количество ресурсов после смены локали");

            CheckResourcesWereRecounted(createAccountCommand.Account.BillingAccount, createAccountCommand.AccountId);

            #endregion

            #region CheckResourscesConfigurationWasRecounted

            CheckResourcesConfigurationWasRecounted(account);

            #endregion
        }

        /// <summary>
        /// Проверить, что ресурсы аккаунта пересчитались
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        private void CheckResourcesWereRecounted(Domain.DataModels.billing.BillingAccount billingAccount, Guid accountId)
        {
            RefreshDbCashContext(Context.Accounts);
            var billingService = GetBillingServiceRent1C();
            Assert.IsNotNull(billingService);

            var myEntUser =
                billingService.BillingServiceTypes.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUser);
            Assert.IsNotNull(myEntUser);

            var myEntUserWeb =
                billingService.BillingServiceTypes.FirstOrDefault(x =>
                    x.SystemServiceType == ResourceType.MyEntUserWeb);
            Assert.IsNotNull(myEntUserWeb);

            var rateProvider = ServiceProvider.GetRequiredService<IRateProvider>();

            var myEntRate = rateProvider.GetOptimalRate(billingAccount.Id, myEntUser.Id);
            var myEntWebRate = rateProvider.GetOptimalRate(billingAccount.Id, myEntUserWeb.Id);

            var resourceEnt = DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.AccountId == accountId && res.BillingServiceTypeId == myEntUser.Id);
            Assert.IsNotNull(resourceEnt);

            if (myEntRate.Cost != resourceEnt.Cost)
                throw new InvalidOperationException("Неверно пересчитался ресурс для Аренда 1С - Стандарт");

            var resourceEntWeb = DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.AccountId == accountId && res.BillingServiceTypeId == myEntUserWeb.Id);
            Assert.IsNotNull(resourceEntWeb);

            if (myEntWebRate.Cost != resourceEntWeb.Cost)
                throw new InvalidOperationException("Неверно пересчитался ресурс для Аренда 1С - WEB");
        }

        /// <summary>
        /// Проверить, что счет пересчитался
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="invoice">Счет</param>
        /// <param name="calculationOfInvoiceRequestModel">Модель калькуляций счета</param>
        private void CheckInvoiceWasRecounted(Guid accountId, Invoice invoice,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequestModel)
        {
            var updatedInvoice = DbLayer.InvoiceRepository.FirstOrDefault(w => w.AccountId == accountId);
            Assert.IsNotNull(updatedInvoice);

            if (invoice.InvoiceProducts.Count != updatedInvoice.InvoiceProducts.Count)
                throw new InvalidOperationException ("При смене локали пропали продукты счета.");

            var calculationOfInvoiceResult =
                _calculationOfInvoiceProvider.GetCalculationsOfInvoiceResult(accountId,
                    calculationOfInvoiceRequestModel);

            if (updatedInvoice.Sum != calculationOfInvoiceResult.TotalSum)
                throw new InvalidOperationException("Не пересчитался счет на оплату при смене локали");

            if (updatedInvoice.InvoiceProducts
                    .FirstOrDefault(w => w.ServiceType.SystemServiceType == ResourceType.DiskSpace)
                    ?.ServiceSum != calculationOfInvoiceResult.MyDiskCost)
                throw new InvalidOperationException("Не пересчитался продукт счета Мой диск");
        }

        /// <summary>
        /// Проверить, что пересчиталась стоимость сервиса для аккаунта 
        /// </summary>
        /// <param name="account">Аккаунт</param>
        private void CheckResourcesConfigurationWasRecounted(Account account)
        {
            var accountResourcesConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(resConf =>
                    resConf.AccountId == account.Id &&
                    resConf.BillingService.SystemService == Clouds42Service.MyEnterprise);
            Assert.IsNotNull(accountResourcesConfiguration);

            var billingServiceRent1C = GetBillingServiceRent1C();

            var sumResources =
                DbLayer.ResourceRepository.Where(res =>
                        (res.AccountId == accountResourcesConfiguration.AccountId && res.AccountSponsorId == null) &&
                        res.BillingServiceType.ServiceId == billingServiceRent1C.Id ||
                        res.AccountSponsorId == accountResourcesConfiguration.AccountId)
                    .Where(res => res.Subject.HasValue)
                    .Where(res =>
                        !res.DemoExpirePeriod.HasValue || (res.DemoExpirePeriod.Value -  DateTime.Now).Days == 0)
                    .Sum(res => (decimal?) res.Cost);

            var cost = sumResources ?? 0;
            if (billingServiceRent1C.SystemService == Clouds42Service.MyEnterprise && IsVipAccount(account.Id))
            {
                cost += account.BillingAccount.AdditionalResourceCost ?? 0;
            }

            if (accountResourcesConfiguration.Cost != cost)
                throw new InvalidOperationException("Неверно пересчиталась стоимость сервиса для аккаунта");
        }

        /// <summary>
        /// Получить сервис биллинга Аренда 1С
        /// </summary>
        /// <returns>Сервис биллинга Аренда 1С</returns>
        private BillingService GetBillingServiceRent1C() =>
            DbLayer.BillingServiceRepository.FirstOrDefault(bl => bl.SystemService == Clouds42Service.MyEnterprise)
            ?? throw new NotFoundException("Не найден сервис Аренда 1С");
    }
}
