﻿using System;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeLocaleAccount
{
    /// <summary>
    /// Сценарий теста изменения локали аккаунт когда есть платеж.
    /// Действия:
    /// 1. Создаем аккаунт. [AccountCreation]
    /// 2. Создаем платеж. [PaymentAddition]
    /// 3. Проверяем, что изменить локаль нельзя если хотя бы один платеж. [ChangeAccountLocale]
    /// </summary>
    public class ScenarioChangeLocaleAccountWhenHasPaymentTest : ScenarioBase
    {
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;
        private readonly CreatePaymentManager _createPaymentManager;

        public ScenarioChangeLocaleAccountWhenHasPaymentTest()
        {
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
            _createPaymentManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
        }

        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            #endregion

            #region PaymentAddition

            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 500,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            #endregion

            #region ChangeAccountLocale

            var localeUkr = GetLocaleByNameOrThrowException(LocaleConst.Ukraine).ID;

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);
            editAccountDto.LocaleId = localeUkr;

            var result = _editAccountManager.UpdateAccount(editAccountDto);
            Assert.IsFalse(result.Result);

            #endregion
        }
    }
}
