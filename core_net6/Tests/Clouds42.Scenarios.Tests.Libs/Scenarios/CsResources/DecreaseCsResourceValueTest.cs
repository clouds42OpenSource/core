﻿using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CsResources
{
    /// <summary>
    /// Тест списания ресурсов
    /// Сценарий:
    ///         1. Создаем аккаунту
    ///         2. Списываем ресурс ЕСДЛ, проверяем, что списание прошло усешно
    ///         3. Списываем ресурс Рекогнишен, проверяем, что списание прошло усешно
    /// </summary>
    public class DecreaseCsResourceValueTest : ScenarioBase
    {
        private readonly IAccountCsResourceValuesDataManager _accountCsResourceValuesData;

        public DecreaseCsResourceValueTest()
        {
            _accountCsResourceValuesData = TestContext.ServiceProvider.GetRequiredService<IAccountCsResourceValuesDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.Esdl
                }
            });
            createAccountCommand.Run();

            DecreaseEsdlResource(createAccountCommand.AccountId);
            IncreaseRecognitionResource(createAccountCommand.AccountId);
        }

        /// <summary>
        /// Списать ресурс ЕСДЛ
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void DecreaseEsdlResource(Guid accountId)
        {
            var resources = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.Esdl);
            Assert.IsNotNull(resources);

            var expireDate = resources.ExpireDateValue;
            var esdlId = CloudConfigurationProvider.BillingServices.GetEsdl42ServiceId();
            var decreaseValue = 5;

            var increaseDecreaseValueModel = new IncreaseDecreaseValueModelDto
            {
                AccountId = accountId,
                CSResourceId = esdlId,
                ModifyResourceComment = "Modify",
                ModifyResourceValue = decreaseValue
            };

            var decreaseResource = _accountCsResourceValuesData.DecreaseResource(increaseDecreaseValueModel);
            Assert.IsFalse(decreaseResource.Error);

            DbLayer.RefreshAll();

            resources = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.Esdl);
            Assert.AreEqual(resources.ExpireDateValue.Date, expireDate.AddDays(-decreaseValue).Date);
        }

        /// <summary>
        /// Списать ресурс Рекогнишен
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void IncreaseRecognitionResource(Guid accountId)
        {
            var recognition42Service = CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId();

            RefreshDbCashContext(Context.FlowResourcesScope);

            var flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(frs =>
                frs.AccountId == accountId && frs.FlowResourceId ==
                recognition42Service);
            Assert.IsNotNull(flowResourcesScope);

            var scopeValue = flowResourcesScope.ScopeValue;
            var decreaseValue = 5;

            var increaseDecreaseValueModel = new IncreaseDecreaseValueModelDto
            {
                AccountId = accountId,
                ModifyResourceValue = decreaseValue,
                CSResourceId = recognition42Service,
                ModifyResourceComment = "Mod"
            };
            var decreaseResource = _accountCsResourceValuesData.DecreaseResource(increaseDecreaseValueModel);
            Assert.IsFalse(decreaseResource.Error);

            DbLayer.RefreshAll();

            flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(frs =>
                frs.AccountId == accountId && frs.FlowResourceId ==
                recognition42Service);
            Assert.AreEqual(flowResourcesScope.ScopeValue, scopeValue - decreaseValue);
        }
    }
}
