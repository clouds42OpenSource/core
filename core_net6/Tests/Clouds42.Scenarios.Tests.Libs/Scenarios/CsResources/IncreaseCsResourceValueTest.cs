﻿using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CsResources
{
    /// <summary>
    /// Тест начисления ресурсов
    /// Сценарий:
    ///         1. Создаем аккаунту
    ///         2. Начисляем ресурс ЕСДЛ, проверяем, что начисление прошло усешно
    ///         3. Начисляем ресурс Рекогнишен, проверяем, что начисление прошло усешно
    /// </summary>
    public class IncreaseCsResourceValueTest : ScenarioBase
    {
        private readonly IAccountCsResourceValuesDataManager _accountCsResourceValuesData;

        public IncreaseCsResourceValueTest()
        {
            _accountCsResourceValuesData = TestContext.ServiceProvider.GetRequiredService<IAccountCsResourceValuesDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.Esdl
                }
            });
            createAccountCommand.Run();

            IncreaseEsdlResource(createAccountCommand.AccountId);
            IncreaseRecognitionResource(createAccountCommand.AccountId);
        }

        /// <summary>
        /// Начислить ресурс ЕСДЛ
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void IncreaseEsdlResource(Guid accountId)
        {
            var resources = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.Esdl);
            Assert.IsNotNull(resources);
            var expireDate = resources.ExpireDateValue;

            var esdlId = CloudConfigurationProvider.BillingServices.GetEsdl42ServiceId();

            var increaseDecreaseValueModel = new IncreaseDecreaseValueModelDto
            {
                AccountId = accountId,
                CSResourceId = esdlId,
                ModifyResourceComment = "Modify",
                ModifyResourceValue = 5
            };

            var managerResult = _accountCsResourceValuesData.IncreaseResource(increaseDecreaseValueModel);
            Assert.IsFalse(managerResult.Error);

            DbLayer.RefreshAll();

            resources = DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.Esdl);

            Assert.AreEqual(resources.ExpireDateValue.Date, expireDate.AddDays(5).Date);
        }

        /// <summary>
        /// Начислить ресурс Рекогнишен
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void IncreaseRecognitionResource(Guid accountId)
        {
            var recognition42Service = CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId();

            DbLayer.RefreshAll();

            var flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(frs =>
                frs.AccountId == accountId && frs.FlowResourceId ==
                recognition42Service);
            Assert.IsNotNull(flowResourcesScope);

            var scopeValue = flowResourcesScope.ScopeValue;


            var sd = new IncreaseDecreaseValueModelDto
            {
                AccountId = accountId,
                ModifyResourceValue = 400,
                CSResourceId = recognition42Service,
                ModifyResourceComment = "Mod"
            };
            var managerResult = _accountCsResourceValuesData.IncreaseResource(sd);
            Assert.IsFalse(managerResult.Error);

            DbLayer.RefreshAll();   

            flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(frs =>
                frs.AccountId == accountId && frs.FlowResourceId ==
                recognition42Service);
            Assert.AreEqual(flowResourcesScope.ScopeValue, scopeValue + 400);
        }
    }
}
