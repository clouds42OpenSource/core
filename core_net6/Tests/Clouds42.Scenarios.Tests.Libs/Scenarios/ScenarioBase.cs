﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations;
using Clouds42.CoreWorker;
using Clouds42.CoreWorkerTask;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.TelegramBot;
using Clouds42.Test.Runner;
using CommonLib.Enums;
using GCP.Extensions.Configuration.SecretManager;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repositories;
using billing = Clouds42.Domain.DataModels.billing;
using TestContext = Clouds42.Scenarios.Tests.Libs.Contexts.TestContext;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios
{
    /// <summary>
    /// Базовый класс сценарных тестов
    /// </summary>
    public abstract class ScenarioBase : IScenario
    {
        protected readonly IUnitOfWork DbLayer;
        protected readonly IServiceProvider ServiceProvider;
        protected readonly IServiceCollection Services;
        protected readonly ILogger42 Logger;
        protected readonly IHandlerException HandlerException;
        protected readonly IAccessMapping AccessMapping;
        protected readonly IConfiguration Configuration;
        protected readonly ICloudLocalizer CloudLocalizer;
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider;
        protected readonly Clouds42DbContext Context;
        protected ISender Mediator;

        protected ScenarioBase()
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddGcpKeyValueSecrets()
                .Build();

            var connectionString = Configuration.GetConnectionString("DefaultConnection");


            Services = new ServiceCollection();
            Services.AddSingleton(Configuration);
            Services.AddClouds42DbContext(Configuration);
            Services.AddScoped<IUnitOfWork, UnitOfWork>();
            Services.AddScoped<IAccessMapping, AccessMapping>();
            Services.AddScoped<IAccessProvider, ScenarioTestAccessProvider>();
            Services.AddSingleton<ILogger42, SerilogLogger42>();
            Services.AddScoped<ICloudLocalizer, CloudLocalizer>();
            Services.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            Services.AddDistributedIsolatedExecutor(new DbObjectLockProvider(connectionString, Configuration.GetSection("ConnectionStrings:DatabaseType").Value));
            Services.ConfigureJobsCollection((jobsCollection) =>
            {
                jobsCollection.RegisterClouds42Jobs();
            });

            Services.AddHttpClient<IisHttpClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    var login = Configuration.GetRequiredSection("IISConfiguration:Login")?.Value;
                    var password = Configuration.GetRequiredSection("IISConfiguration:Password")?.Value;
                    var domain = Configuration.GetRequiredSection("IISConfiguration:Domain")?.Value;

                    return new HttpClientHandler
                    {
                        UseDefaultCredentials = false,
                        ServerCertificateCustomValidationCallback =
                            HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                        Credentials = new NetworkCredential(login, password, domain),
                        PreAuthenticate = true
                    };
                });

            Services.AddScoped<IRequestContextTaskDispatcher>(_ => new RequestContextTaskDispatcher());
            TestScenarioDependencyRegistrator.Register(Services, Configuration);
            RegisterTestServices(Services);
            Services.AddTelegramBots(Configuration);
            ServiceProvider = Services.BuildServiceProvider();
            DbLayer = ServiceProvider.GetRequiredService<IUnitOfWork>();
            Context = ServiceProvider.GetRequiredService<Clouds42DbContext>();
            _accountConfigurationDataProvider = ServiceProvider.GetRequiredService<IAccountConfigurationDataProvider>();
            Logger = ServiceProvider.GetRequiredService<ILogger42>();
            CloudLocalizer = ServiceProvider.GetRequiredService<ICloudLocalizer>();
            HandlerException = ServiceProvider.GetRequiredService<IHandlerException>();
            AccessMapping = ServiceProvider.GetRequiredService<IAccessMapping>();
            HandlerException42.Init(ServiceProvider);
            ConfigurationHelper.SetConfiguration(Configuration);
            Logger42.Init(ServiceProvider);

            Mediator = ServiceProvider.GetRequiredService<ISender>();
        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected virtual bool NeedUseAd() => false;

        /// <summary>
        /// Нужно для кастомных регистраций сервисов в отдельных тестах
        /// </summary>
        /// <param name="serviceCollection"></param>
        protected virtual void RegisterTestServices(IServiceCollection serviceCollection){}

        /// <summary>
        /// Выполнить чистку мусора
        /// </summary>
        public virtual void ClearGarbage()
        {
            GarbageClearHelper.ClearGarbage(DbLayer, NeedUseAd());
        }

        /// <summary>
        /// Запустить тест
        /// </summary>
        public abstract void Run();

        public virtual async Task RunAsync() { }

        /// <summary>
        /// Дождаться выполнения фоновых задач
        /// </summary>
        public void WaitAllBackgroundTasks()
        {
            var dispatcherHelper = ServiceProvider.GetRequiredService<TaskDispatcherHelper>();
            dispatcherHelper.WaitAllBackgroundTasks(true);
        }

        protected ITestContext TestContext => new TestContext(AccessProvider, DbLayer, ServiceProvider, Logger, Configuration, Context, AccessMapping, HandlerException);

        private IAccessProvider _accessProvider;

        protected IAccessProvider AccessProvider
        {
            get
            {
                if (_accessProvider != null)
                    return _accessProvider;

                return _accessProvider = new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            }
        }

        /// <summary>
        /// Частично обновить контекст
        /// </summary>
        /// <param name="collection">Коллекция объектов</param>
        protected void RefreshDbCashContext(IEnumerable collection)
        {
            try
            {
                foreach (var entity in collection)
                {
                    Context.Entry(entity).Reload();
                }
          
            }
            catch (Exception ex)
            {
                Logger.Warn(ex, "Ошибка обновления котекста");
            }
        }

        /// <summary>
        /// Полностью обновить контекст 
        /// </summary>
        protected void RefreshFullDbContext()
        {
            try
            {
                var entities = Context.ChangeTracker.Entries().Select(x => x.Entity).ToList();

                foreach (var entity in entities)
                {
                    Context.Entry(entity).Reload();
                }
            }

            catch (Exception ex)
            {
                Logger.Warn(ex, "Ошибка обновления котекста");
            }
        }

        /// <summary>
        /// Сбросить все кэшированны данные, и несохраненные изменения контекста
        /// </summary>
        protected void DropDbContextCache()
        {
            foreach (var entry in ((UnitOfWork)DbLayer).Context.ChangeTracker.Entries())
            {
                entry.State = EntityState.Detached;
            }
        }

        /// <summary>
        /// Получить сохраненный способ оплаты
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <returns></returns>
        protected SavedPaymentMethod GetFirstSavedPaymentMethodByAccountId(Guid accountId)
            => DbLayer.SavedPaymentMethodRepository.FirstOrDefault(spm => spm.AccountId == accountId);

        /// <summary>
        /// Получить сервис Аренды 1С
        /// или вызвать исключение
        /// </summary>
        /// <returns>Сервис Аренды 1С</returns>
        protected IBillingService GetRent1CServiceOrThrowException()
            => DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.SystemService == Clouds42Service.MyEnterprise) ?? throw new InvalidOperationException (
                   "Не заполнен справочник billing.Services не указан сервис Аренда 1С");

        /// <summary>
        /// Получить услугу Стандарт-Аренда 1С
        /// или вызвать исключение
        /// </summary>
        /// <returns>Услуга Стандарт-Аренда 1С</returns>
        protected IBillingServiceType GetStandardServiceTypeOrThrowException()
            => DbLayer.BillingServiceTypeRepository.FirstOrDefault(st =>
                   st.SystemServiceType == ResourceType.MyEntUser) ?? throw new InvalidOperationException (
                   "Не заполнен справочник billing.ServiceTypes не указана рдп услуга сервиса Аренда 1С");

        /// <summary>
        /// Получить услугу ВЕБ-Аренда 1С
        /// или вызвать исключение
        /// </summary>
        /// <returns>Услуга ВЕБ-Аренда 1С</returns>
        protected IBillingServiceType GetWebServiceTypeOrThrowException()
            => DbLayer.BillingServiceTypeRepository.FirstOrDefault(st =>
                   st.SystemServiceType == ResourceType.MyEntUserWeb) ?? throw new InvalidOperationException (
                   "Не заполнен справочник billing.ServiceTypes не указана web услуга сервиса Аренда 1С");

        /// <summary>
        /// Получить локаль по имени
        /// или вызвать исключение
        /// </summary>
        /// <param name="localeName">Имя локали</param>
        /// <returns>Локаль</returns>
        protected ILocale GetLocaleByNameOrThrowException(string localeName) =>
            DbLayer.LocaleRepository.FirstOrDefault(l => l.Name == localeName) ??
            throw new InvalidOperationException ($"Не заполнен справочник локалей, не удалось найти локаль по имени {localeName}");

        /// <summary>
        /// Получить сервис или вызвать исключение
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        protected BillingService GetBillingServiceOrThrowException(Guid serviceId) =>
            DbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == serviceId) ??
            throw new NotFoundException($"Не удалось получить сервис по Id {serviceId}");
        /// <summary>
        /// Получить тариф для услуги или выкинуть мсключение
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф для услуги</returns>
        protected Rate GetRateForServiceTypeOrThrowException(Guid serviceTypeId) =>
            DbLayer.RateRepository.FirstOrDefault(r => r.BillingServiceTypeId == serviceTypeId) ??
            throw new NotFoundException($"Не удалось получить тариф для услуги {serviceTypeId}");

        /// <summary>
        /// Получить тариф на аккаунт
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Тариф на аккаунт</returns>
        protected AccountRate GetAccountRate(Guid serviceTypeId, Guid accountId) =>
            DbLayer.AccountRateRepository.FirstOrDefault(ar =>
                ar.BillingServiceTypeId == serviceTypeId && ar.AccountId == accountId);

        /// <summary>
        /// Получить запущенную задачу воркера
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <returns>Запущенная задача воркера</returns>
        protected CoreWorkerTasksQueue GetCoreWorkerTasksQueue(string taskName) =>
            DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(tq =>
                tq.CoreWorkerTask.TaskName == taskName);

        /// <summary>
        /// Получить параметры запущенной задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueueId">Id запущенной задачи</param>
        /// <returns>Модель параметров запущенной задачи</returns>
        protected CoreWorkerTaskParameter GetCoreWorkerTaskParameterOrThrowException(Guid coreWorkerTasksQueueId) =>
           DbLayer.GetGenericRepository<CoreWorkerTaskParameter>()
                .FirstOrDefault(tp => tp.TaskId == coreWorkerTasksQueueId) ??
            throw new NotFoundException($"Не удалось получить параметры задачи '{coreWorkerTasksQueueId}'");

        /// <summary>
        /// Получить модель параметров запущенной задачи
        /// </summary>
        /// <typeparam name="T">Тип модели</typeparam>
        /// <param name="coreWorkerTasksQueueId">Id запущенной задачи</param>
        /// <returns>Модель параметров запущенной задачи</returns>
        protected T GetCoreWorkerTaskParametersModel<T>(Guid coreWorkerTasksQueueId) =>
            GetCoreWorkerTaskParameterOrThrowException(coreWorkerTasksQueueId).TaskParams
                .DeserializeFromJson<T>();

        /// <summary>
        /// Получить конфигурацию ресурса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Конфигурацию ресурса</returns>
        protected ResourcesConfiguration GetResourcesConfigurationOrThrowException(Guid accountId, Guid serviceId) =>
             TestContext.DbLayer.ResourceConfigurationRepository.AsQueryable().FirstOrDefault(rc =>
                rc.BillingServiceId == serviceId && rc.AccountId == accountId) ??
            throw new NotFoundException("Не удалось получить конфигурацию ресурса");

        /// <summary>
        /// Получить конфигурацию ресурса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="clouds42Service">Сервис облака</param>
        /// <returns>Конфигурацию ресурса</returns>
        protected ResourcesConfiguration
            GetResourcesConfigurationOrThrowException(Guid accountId, Clouds42Service clouds42Service) =>
            DbLayer.ResourceConfigurationRepository.AsQueryable().FirstOrDefault(rc =>
                rc.BillingService.SystemService == clouds42Service && rc.AccountId == accountId) ??
            throw new InvalidOperationException(
                "Не удалось получить конфигурацию ресурса");

        /// <summary>
        /// Получить сервис Мой диск
        /// или вызвать исключение
        /// </summary>
        /// <returns>Сервис Мой диск</returns>
        protected IBillingService GetMyDiskServiceOrThrowException()
            => DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.SystemService == Clouds42Service.MyDisk) ?? throw new InvalidOperationException (
                   "Не заполнен справочник billing.Services не указан сервис Мой диск");

        /// <summary>
        /// Получить ресурсы для услуги
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Список ресурсов</returns>
        public List<Resource> GetResources(Guid accountId, Guid serviceTypeId) => Context.Resources
            .Where(resource => resource.AccountId == accountId && resource.BillingServiceTypeId == serviceTypeId)
            .ToList();


        /// <summary>
        /// Установить заблокированную ресурс конфигурацию для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="expireDate">Дата пролонгации</param>
        protected void SetFrozenResourceConfigurationForAccount(Guid accountId, Guid serviceId, DateTime expireDate)
        {
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(accountId, serviceId);
            SetFrozenResourceConfiguration(resourcesConfiguration, expireDate);
        }

        /// <summary>
        /// Установить заблокированную ресурс конфигурацию для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="clouds42Service">Сервис облака</param>
        /// <param name="expireDate">Дата пролонгации</param>
        protected void SetFrozenResourceConfigurationForAccount(Guid accountId, Clouds42Service clouds42Service,
            DateTime expireDate)
        {
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(accountId, clouds42Service);
            SetFrozenResourceConfiguration(resourcesConfiguration, expireDate);
        }

        protected void AddSavedPaymentMethodWithBankCardInfoForAccount(Guid accountId, PaymentSystem paymentSystem)
        {
            var savedPaymentMethod = new SavedPaymentMethod
            {
                AccountId = accountId,
                CreatedOn = DateTime.Now,
                PaymentSystem = paymentSystem.ToString(),
                Token = Guid.NewGuid().ToString(),
                Title = "Visa 4111 11** **** 1111",
                Type = SavedPaymentMethodType.BankCard.Description()
            };

            DbLayer.SavedPaymentMethodRepository.Insert(savedPaymentMethod);
            DbLayer.Save();

            var bankCard = new SavedPaymentMethodBankCard
            {
                CardType = "Visa",
                ExpiryMonth = DateTime.Now.AddMonths(-1).Month.ToString(),
                ExpiryYear = DateTime.Now.Year.ToString(),
                FirstSixDigits = "411111",
                LastFourDigits = "1111",
                SavedPaymentMethodId = savedPaymentMethod.Id
            };

            DbLayer.SavedPaymentMethodBankCardRepository.Insert(bankCard);
            DbLayer.Save();
        }

        /// <summary>
        /// Установить заблокированную ресурс конфигурацию
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="expireDate">Дата пролонгации</param>
        private void SetFrozenResourceConfiguration(ResourcesConfiguration resourcesConfiguration, DateTime expireDate)
        {
            resourcesConfiguration.Frozen = true;
            resourcesConfiguration.ExpireDate = expireDate;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();
        }

        /// <summary>
        /// Заблокировать Аренду 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        protected void BlockRent1CForAccount(Guid accountId)
        {
            var rent1CManager = TestContext.ServiceProvider.GetRequiredService<Rent1CManager>();

            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddMonths(-1)
            };
            rent1CManager.ManageRent1C(accountId, blockRent1C);
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="cost">Сумма платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        protected void CreateInflowPayment(Guid accountId, Guid? serviceId, decimal cost,
            TransactionType transactionType = TransactionType.Money)
        {
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = serviceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = transactionType
            });
        }

        /// <summary>
        /// Сделать кастомный сервис не Демо
        /// </summary>
        /// <param name="customServiceId">Id кастомного сервиса</param>
        protected void UpdateResourceConfigurationMakeCustomServiceNotDemo(Guid customServiceId)
        {
            var customBillingService =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(w => w.BillingServiceId == customServiceId) ??
                throw new NotFoundException();

            customBillingService.IsDemoPeriod = false;
            customBillingService.ExpireDate = null;
            customBillingService.Frozen = null;

            DbLayer.ResourceConfigurationRepository.Update(customBillingService);
            DbLayer.Save();
        }

        /// <summary>
        /// Изменить дату окончания ресурс конфигурации
        /// </summary>
        /// <param name="resConfExpireDate">Дата окончания ресурс конфигурацию</param>
        /// /// <param name="serviceId">Дата окончания ресурс конфигурацию</param>
        /// <remarks></remarks>
        protected void ChangeResourceConfigurationExpireDate(DateTime resConfExpireDate, Guid? serviceId = null)
        {
            if (serviceId.HasValue)
            {
                DbLayer.ResourceConfigurationRepository
                    .AsQueryableNoTracking()
                    .Where(x => x.BillingServiceId == serviceId.Value)
                    .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                        .SetProperty(y => y.Frozen, false));

                Context.ChangeTracker.Clear();

                return;
            }

            DbLayer.ResourceConfigurationRepository
                .AsQueryableNoTracking()
                .ExecuteUpdate(x => x.SetProperty(y => y.ExpireDate, resConfExpireDate)
                    .SetProperty(y => y.Frozen, false));

            Context.ChangeTracker.Clear();
        }

        /// <summary>
        /// Добавить пользователя к аккаунту
        /// </summary>
        /// <param name="accountUserDc">Модель пользователя</param>
        /// <returns>Id пользователя</returns>
        protected Guid AddUserToAccount(AccountUserDto accountUserDc)
        {
            var managerResult = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(accountUserDc);
            if (managerResult.Error)
                throw new InvalidOperationException(managerResult.Message);

            return managerResult.Result;
        }

        /// <summary>
        /// Получить логин пользователя
        /// </summary>
        /// <returns></returns>
        protected string GetLogin()
        {
            var guid = Guid.NewGuid();
            var guidParts = guid.ToString().Split('-');
            return $"user{guidParts[0]}{guidParts[1]}";
        }

        /// <summary>
        /// Получить аккаунт биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт биллинга</returns>
        protected billing.BillingAccount GetBillingAccount(Guid accountId)
            =>DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == accountId);

        /// <summary>
        /// Подключить ресурсы Аренды 1С пользователю
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="userId">Id пользователя</param>
        /// <param name="webResourceId">Id ВЕБ ресурса</param>
        /// <param name="standardResourceId">Id РДП ресурса</param>
        protected void ConfigureAccessToRent1C(Guid accountId, Guid userId, Guid webResourceId,
            Guid? standardResourceId = null)
        {
            var configurationAccessManager = TestContext.ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = configurationAccessManager.ConfigureAccesses(accountId,
            [
                new UpdaterAccessRent1CRequestDto
                {
                    AccountUserId = userId,
                    WebResource = true,
                    StandartResource = standardResourceId != null,
                    WebResourceId = webResourceId,
                    RdpResourceId = standardResourceId
                }
            ]);
            Assert.IsFalse(result.Error);
        }

        /// <summary>
        /// Удалить информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        protected void DeleteDatabase(Guid accountDatabaseId)
        {
            var startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            var deleteAccountDatabase =
                startProcessToWorkDatabaseManager.StartProcessOfRemoveDatabase(accountDatabaseId);
            if (deleteAccountDatabase.Error)
                throw new InvalidOperationException();
        }

        /// <summary>
        /// Выполнить управление сервисом
        /// </summary>
        /// <param name="manageBillingService">Модель управления сервисом </param>
        protected void ManageBillingServiceControl(ManageBillingServiceDto manageBillingService)
        {
            var billingServiceControlManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceControlManager>();
            billingServiceControlManager.Manage(manageBillingService);
        }

        /// <summary>
        /// Удалить ресурсы аккаунта
        /// </summary>
        protected void DeleteAccountResources()
        {
            DbLayer.GetGenericRepository<MyDatabasesResource>().AsQueryableNoTracking().ExecuteDelete();
            DbLayer.ResourceRepository.AsQueryableNoTracking().ExecuteDelete();
        }

        /// <summary>
        /// Удалить ресурсы услуг по пользователю
        /// </summary>
        protected void DeleteServiceTypeResourcesByAccountUser()
        {
            DbLayer.ResourceRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingServiceType.BillingType == BillingTypeEnum.ForAccountUser)
                .ExecuteDelete();
        }

        /// <summary>
        /// Удалить конфигурацию 1С
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        protected void RemoveConfiguration1C(string configurationName)
        {
            using var transaction = DbLayer.SmartTransaction.Get();
            try
            {
                var configuration1C = DbLayer.Configurations1CRepository.FirstOrDefault(c => c.Name == configurationName);
                if (configuration1C == null)
                {
                    transaction.Commit();
                    return;
                }

                var configurationNameVariations = DbLayer.ConfigurationNameVariationRepository
                    .Where(variation => variation.Configuration1CName == configurationName).ToList();

                if (configurationNameVariations.Any())
                    DbLayer.ConfigurationNameVariationRepository.DeleteRange(configurationNameVariations);

                var configurationServiceTypeRelation = DbLayer
                    .GetGenericRepository<ConfigurationServiceTypeRelation>()
                    .FirstOrDefault(rel => rel.Configuration1CName == configurationName);
                if (configurationServiceTypeRelation == null)
                {
                    transaction.Commit();
                    return;
                }

                DbLayer.GetGenericRepository<ConfigurationServiceTypeRelation>()
                    .Delete(configurationServiceTypeRelation);

                var serviceType =
                    GetBillingServiceTypeOrThrowException(configurationServiceTypeRelation.ServiceTypeId);

                var serviceTypeRate = GetRateForServiceTypeOrThrowException(serviceType.Id);
                DbLayer.RateRepository.Delete(serviceTypeRate);

                DbLayer.BillingServiceTypeRepository.Delete(serviceType);
                DbLayer.Configurations1CRepository.Delete(configuration1C);

                DbLayer.Save();
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить ID любой записи из справочника "Доступ к ИТС" или выкинуть исключение
        /// </summary>
        /// <returns>ID любой записи из справочника "Доступ к ИТС"</returns>
        protected Guid GetAnyItsAuthorizationRecordIdOrThrowException()
            => DbLayer.GetGenericRepository<ItsAuthorizationData>().FirstOrDefault()?.Id
               ?? throw new InvalidOperationException("Не заполнен справочник доступа к ИТС");

        /// <summary>
        /// Получить услугу сервиса или выкинуть исключение
        /// </summary>
        /// <param name="serviceTypeId">ID услуги</param>
        /// <returns>Услуга сервиса</returns>
        protected BillingServiceType GetBillingServiceTypeOrThrowException(Guid serviceTypeId)
            => DbLayer.BillingServiceTypeRepository.FirstOrDefault(st =>
                   st.Id == serviceTypeId) ??
               throw new NotFoundException($"Услуга сервиса по ID {serviceTypeId} не найдена.");

        /// <summary>
        /// Получить ресурсы аккаунта по услуге
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Список ресурсов аккаунта по услугам</returns>
        protected List<AccountResourcesByBillingTypeModelTest> GetAccountResourcesByBillingType(Guid accountId) =>
            (from resource in DbLayer.ResourceRepository.WhereLazy(r =>
                    r.AccountId == accountId)
             join serviceType in DbLayer.BillingServiceTypeRepository.WhereLazy() on resource
                     .BillingServiceTypeId
                 equals serviceType.Id
             select new AccountResourcesByBillingTypeModelTest { ServiceType = serviceType, Resource = resource })
            .ToList();


        /// <summary>
        /// Создать шаблон базы на разделителях
        /// </summary>
        /// <param name="dbTemplateModelTest">Модель шаблона</param>
        /// <param name="dbConfigId">Id конфигурации</param>
        protected void CreateDelimiterTemplate(DbTemplateModelTest dbTemplateModelTest = null, string dbConfigId = null)
        {
            var model = dbTemplateModelTest ?? new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            };

            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(model);

            TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>()
                .AddDbTemplateDelimiterItem(new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id,
                    configId: dbConfigId));
        }

        /// <summary>
        /// Создать аккаунт или выкинуть исключение
        /// </summary>
        /// <param name="registrationModel">Модель регистрации</param>
        /// <returns>Аккаунт</returns>
        protected Account CreateAccountOrThrowException(AccountRegistrationModelTest registrationModel)
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, registrationModel);
            createAccountCommand.Run();

            if (createAccountCommand.Account == null)
                throw new InvalidOperationException("Аккаунт не найден или не был создан");

            return createAccountCommand.Account;
        }
        /// <summary>
        /// Установить размер файлов аккаунта для сервиса "Мой диск"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="filesSizeInMb">Размер файлов на диске</param>
        protected void SetMyDiskFilesSizeInMbForAccount(Guid accountId, long filesSizeInMb)
        {
            DbLayer
                .GetGenericRepository<MyDiskPropertiesByAccount>()
                .AsQueryableNoTracking()
                .Where(x => x.AccountId == accountId)
                .ExecuteUpdate(x => x.SetProperty(y => y.MyDiskFilesSizeInMb, filesSizeInMb));
        }

        /// <summary>
        /// Получить данные конфигурации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель конфигурации аккаунта</returns>
        protected AccountConfigurationDto GetAccountConfigurationData(Guid accountId) =>
            _accountConfigurationDataProvider.GetAccountConfigurationDc(accountId);

        /// <summary>
        /// Получить Id сегмента аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id сегмента аккаунта</returns>
        protected Guid GetAccountSegmentId(Guid accountId) => GetAccountConfigurationData(accountId).SegmentId;

        /// <summary>
        /// Получить сегмент аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Сегмент аккаунта</returns>
        protected CloudServicesSegment GetAccountSegment(Guid accountId) =>
            (CloudServicesSegment)(_accountConfigurationDataProvider.GetAccountConfigurationsDataBySegment()
                .FirstOrDefault(accountConfig => accountConfig.Account.Id == accountId)?.Segment ??
            throw new NotFoundException($"Не удалось определить сегмент по Id аккаунту '{accountId}'"));

        /// <summary>
        /// Получить сегмент аккаунта для базы на разделителях
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Сегмент аккаунта для базы на разделителях</returns>
        protected CloudServicesSegment GetAccountSegmentForDatabaseOnDelimeter(Guid databaseId) =>
            GetAccountSegmentForDatabaseOnDelimeter(DbLayer.DatabasesRepository
                .FirstOrDefault(db => db.Id == databaseId));

        /// <summary>
        /// Получить сегмент аккаунта для базы на разделителях
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Сегмент аккаунта для базы на разделителях</returns>
        protected CloudServicesSegment GetAccountSegmentForDatabaseOnDelimeter(Domain.DataModels.AccountDatabase database)
        {
            var sourceAccountDatabase = database
                                            ?.AccountDatabaseOnDelimiter?.SourceAccountDatabase ??
                                        throw new NotFoundException("Не удалось определить материнскую базу");

            return GetAccountSegment(sourceAccountDatabase.AccountId);
        }

        /// <summary>
        /// Проверить что аккаунт является vip
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак указывающий что аккаунт VIP</returns>
        protected bool IsVipAccount(Guid accountId) => _accountConfigurationDataProvider.IsVipAccount(accountId);

        /// <summary>
        /// Получить тип аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Тип аккаунта</returns>
        protected string GetAccountType(Guid accountId) => GetAccountConfigurationData(accountId).Type;

        /// <summary>
        /// Получить Id файлового хранилища аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id файлового хранилища аккаунта</returns>
        protected Guid GetAccountFileStorageId(Guid accountId) =>
            _accountConfigurationDataProvider.GetAccountFileStorageId(accountId);

        /// <summary>
        /// Получить Id локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id локали аккаунта</returns>
        protected Guid GetAccountLocaleId(Guid accountId) =>
            _accountConfigurationDataProvider.GetAccountLocaleId(accountId);

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Локаль аккаунта</returns>
        protected Locale GetAccountLocale(Guid accountId) =>
            _accountConfigurationDataProvider.GetAccountLocale(accountId);
    }


    /// <summary>
    /// Базовый класс сценарных тестов
    /// </summary>
    public abstract class ScenarioCaseBase : ScenarioBase
    {
        private IEnumerable<Action> _testCaseScenario;

        protected IEnumerable<Action> TestCaseScenario =>
            _testCaseScenario ??= GetTestCaseScenarioActions();

        /// <summary>
        /// Запустить все тест-case-ы данного теста
        /// </summary>
        private void RunAllTestCases()
        {
            foreach (var testCaseScenarioAction in TestCaseScenario)
            {
                ClearGarbage();
                testCaseScenarioAction();
            }
        }

        protected abstract IEnumerable<Action> GetTestCaseScenarioActions();

        public sealed override void Run()
        {
            RunAllTestCases();
        }
    }
}
