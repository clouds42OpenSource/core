﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StateMachine
{
    /// <summary>
    /// Сценарий теста на проверку удаления пользователя
    /// Действия:
    ///     1) Создаем аккаунт
    ///     2) Пытаемся удалить его, вызывая при этом ошибки в действиях
    /// </summary>
    public class ScenarioDeleteUserWithErrors : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.AccountId == createAccountCommand.AccountId);
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            
            Services.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProviderUserNotDeleted>();
            try
            {
                accountUsersProfileManager.Delete(accountUser.Id);
            }
            catch (Exception)
            {
                var userAfterDeletion = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accountUser.Id);

                Assert.AreEqual(userAfterDeletion.Email, accountUser.Email);
                Assert.AreEqual(userAfterDeletion.FirstName, accountUser.FirstName);
                Assert.AreEqual(userAfterDeletion.PhoneNumber, accountUser.PhoneNumber);
                Assert.AreNotEqual(userAfterDeletion.Removed, true);
                Assert.AreNotEqual(userAfterDeletion.CreatedInAd, false);
            }
        }
    }
}
