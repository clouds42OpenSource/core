﻿using System;
using System.Linq;
using Clouds42.Billing;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RestoreAccountDatabaseBackupManagerTest
{
    /// <summary>
    /// Сценарий для успешного восстановления инф. базы из бэкапа
    /// Действия:
    /// 1. Создадим, аккаунт, инф. базу и файл бэкапа. [EntitiesCreation]
    /// 2. Проверим, что при восстановлении в текущую не будет создана новая запись о инф. базе. [CheckNewDbRecordWontBeAdded]
    /// 3. Проверим, что при восстановлении в новую будет создана запись о инф. базе. [CheckNewDbRecordGonnaBeAdded]
    /// </summary>
    public class ScenarioRestoreDbFromBackupSuccess : ScenarioBase
    {
        private readonly IRestoreAccountDatabaseBackupManager _accountDatabaseBackupManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioRestoreDbFromBackupSuccess()
        {
            _accountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<IRestoreAccountDatabaseBackupManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            var uploadFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);
            _createUploadedFileTestHelper.CreateUploadedFilePhysicallyInDbStorage(uploadFileId, databaseId);

            var restoreParams =
                _testDataGenerator.GenerateRestoreAccountDatabaseFromTombWorkerTaskParam(uploadFileId, backupId,
                    createAccountCommand.AccountAdminId);

            #endregion

            #region CheckNewDbRecordWontBeAdded

            var result = _accountDatabaseBackupManager.Restore(restoreParams);
            Assert.IsFalse(result.Error, result.Message);

            var dbCount = DbLayer.DatabasesRepository.All().Count();
            Assert.AreEqual(1, dbCount, "Создалась лишняя база из бэкапа");

            #endregion

            #region CheckNewDbRecordGonnaBeAdded

            restoreParams.RestoreType = AccountDatabaseBackupRestoreType.ToNew;

            _accountDatabaseBackupManager.Restore(restoreParams);

            var createdDbFromBackup =
                DbLayer.DatabasesRepository.FirstOrDefault(db => db.Caption == restoreParams.AccountDatabaseName);
            Assert.IsNotNull(createdDbFromBackup, "Не создалась база из бэкапа");

            #endregion
        }
    }
}
