﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RestoreAccountDatabaseBackupManagerTest
{
    /// <summary>
    /// Сценарий для проверки неуспешного восстановления инф. базы из бэкапа
    /// Действия:
    /// 1. Создадим аккаунт и необходимые сущности для восстановления. [EntitiesCreation]
    /// 2. Проверим невалидные условия для создания инф. базы из бэкапа: [CheckNotValidConditions]
    ///     2.1 Проверим, что будет получена ошибка при отсутствии физического загруженного файла.
    ///     2.2 Проверим, что будет получена ошибка при указании несуществующего Id загруженного файла.
    ///     2.3 Проверим, что будет получена ошибка при указании несуществующего Id бэкапа.
    ///     2.4 Проверим, что ошибка не будет получена при указании пустого имени инф. базы и списка пользователей для предоставления доступа.
    /// </summary>
    public class ScenarioRestoreDbFromBackupFail : ScenarioBase
    {
        private readonly IRestoreAccountDatabaseBackupManager _accountDatabaseBackupManager;
        private readonly CreateUploadedFileTestHelper _createUploadedFileTestHelper;
        private readonly AccountDatabaseBackupTestHelper _accountDatabaseBackupTestHelper;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioRestoreDbFromBackupFail()
        {
            _accountDatabaseBackupManager = TestContext.ServiceProvider.GetRequiredService<IRestoreAccountDatabaseBackupManager>();
            _createUploadedFileTestHelper = new CreateUploadedFileTestHelper(TestContext);
            _accountDatabaseBackupTestHelper = new AccountDatabaseBackupTestHelper(TestContext);
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(account.Id, 1)[0];

            var backupId = _accountDatabaseBackupTestHelper.CreateDbBackup(databaseId, createAccountCommand.AccountAdminId,
                CreateBackupAccountDatabaseTrigger.OverdueRent, DateTime.Now.AddMinutes(1));

            var uploadFileId = _createUploadedFileTestHelper.CreateUploadedFileDbRecord(account.Id);

            var restoreParams =
                _testDataGenerator.GenerateRestoreAccountDatabaseFromTombWorkerTaskParam(uploadFileId, backupId,
                    createAccountCommand.AccountAdminId);

            #endregion

            #region CheckNotValidConditions

            var result = _accountDatabaseBackupManager.Restore(restoreParams);
            Assert.IsTrue(result.Error, "Не получили ошибку при отсутствии физически загруженного файла");

            _createUploadedFileTestHelper.CreateUploadedFilePhysicallyInDbStorage(uploadFileId, databaseId);

            restoreParams.UploadedFileId = Guid.NewGuid();

            result = _accountDatabaseBackupManager.Restore(restoreParams);
            Assert.IsTrue(result.Error, "Не получили ошибку при несуществующем Id файла загрузки");

            restoreParams.UploadedFileId = uploadFileId;
            restoreParams.AccountDatabaseBackupId = Guid.NewGuid();

            result = _accountDatabaseBackupManager.Restore(restoreParams);
            Assert.IsTrue(result.Error, "Не получили ошибку при несуществующем Id бэкапа");

            restoreParams.AccountDatabaseBackupId = backupId;
            restoreParams.AccountDatabaseName = string.Empty;
            restoreParams.UsersIdForAddAccess = [];

            result = _accountDatabaseBackupManager.Restore(restoreParams);
            Assert.IsFalse(result.Error, result.Message);

            #endregion
        }
    }
}
