﻿using System;
using Clouds42.DataContracts.BillingService;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Провайдер для уведомления о скором
    /// удалении услуг сервиса
    /// </summary>
    public class NotifyBeforeDeleteServiceTypesProviderFake : INotifyBeforeDeleteServiceTypesProvider
    {
        /// <summary>
        /// Уведомить аккаунты о скором удалении
        /// услуг сервиса
        /// </summary>
        /// <param name="model">Модель отложенного удаления услуг сервиса для аккаунтов</param>
        public void NotifyAccounts(DelayedServiceTypesDeletionForAccountsDto model)
        {
            if (model.AccountsForSetAvailabilityDateTime == null || model.ServiceTypeIds == null)
                throw new InvalidOperationException("Не заданы основные параметры");
        }
    }
}
