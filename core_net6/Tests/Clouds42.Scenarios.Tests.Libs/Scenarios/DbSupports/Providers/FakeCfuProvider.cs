﻿using System.Collections.Generic;
using System.IO;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Провайдер пакетов обновления 1С.
    /// </summary>
    public class FakeCfuProvider : ICfuProvider
    {
        /// <summary>
        /// Получить путь до cfu пакета обновлений 1С.
        /// </summary>                
        public string GetCfuFullPath(string configurationCode, string release)
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetCfuStoragePath(), configurationCode);

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>        
        public void DownloadCfu(IConfigurationsCfu cfu)
        {
            // заглушка
        }

        /// <summary>
        /// Скачать пакет обновления.
        /// </summary>    
        public void DownloadCfu(ICfuSource cfu, IConfigurations1C configuration)
        {
            // заглушка
        }

        /// <summary>
        /// Пакет обновлений уже скачан.
        /// </summary>  
        public bool CfuExist(ICfuSource cfu, IConfigurations1C configuration)
            => true;

        /// <summary>
        /// Получить максимально возможную версию обновления пакета.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Максимально возможная версия обновления пакета</returns>   
        public IConfigurationsCfu GetLastUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion)
            => new ConfigurationsCfu
            {
                Platform1CVersion = segmentPlatformVersion,
                Version = $"{version}.1"
            };

        /// <summary>
        /// Получить следующую версию пакета обновления.
        /// </summary>
        /// <param name="configuration">Конфигурация инф. базы</param>
        /// <param name="version">Версия релиза</param>
        /// <param name="segmentPlatformVersion">Версия платформы в сегменте</param>
        /// <returns>Следующая версия пакета обновления</returns>
        public IConfigurationsCfu GetNextUpdateVersion(IConfigurations1C configuration, string version, string segmentPlatformVersion)
            => new ConfigurationsCfu
            {
                Platform1CVersion = segmentPlatformVersion,
                Version = $"{version}.1"
            };

        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                    
        /// <param name="configuration">Конфигурация 1С.</param>                    
        /// <returns>Полный путь до пакета обновлений.</returns>
        public string GetCfuFullPath(ICfuSource cfu, IConfigurations1C configuration)
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetCfuStoragePath(), configuration.Name);

        /// <summary>
        /// Получить полный путь до пакета обновлений на диске.
        /// </summary>
        /// <param name="cfu">Пакет обновлений</param>                            
        /// <returns>Полный путь до пакета обновлений.</returns>
        public string GetCfuFullPath(IConfigurationsCfu cfu)
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetCfuStoragePath(), cfu.ConfigurationName);

        /// <summary>
        /// Получить полный путь к архиву с информацией по обновлениям конфигурации.
        /// </summary>
        /// <param name="configuration">Конфигурация 1С</param>
        /// <returns>Список путей  по редакциям</returns>
        public List<ConfigurationRedactionModelDto> GetConfigurationRedactionInfo(IConfigurations1C configuration)
            => [];
    }
}
