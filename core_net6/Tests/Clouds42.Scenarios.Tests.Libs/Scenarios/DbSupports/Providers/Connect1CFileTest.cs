﻿using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Tools1C.Connectors1C.DbConnector;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    public class Connect1CFileTest(string version, string synonym) : IDbConnector1C
    {
        public bool CanConnect()
            => true;

        public bool TryGetMetadataInfo(out MetadataResultDto result)
        {
            result = new MetadataResultDto(version, synonym);
            return true;
        }

        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo()
        {
            return new ConnectorResultDto<MetadataResultDto>
            {
                Result = new MetadataResultDto(version, synonym) 
            };
        }

        public void SetAdminRolesForUser(string userName)
        {       
            // заглушка
        }

        public void ApplyUpdates()
        {
            // заглушка
        }

        public bool TryApplyLegalUpdate()
        {
            return true;
        }
    }
}