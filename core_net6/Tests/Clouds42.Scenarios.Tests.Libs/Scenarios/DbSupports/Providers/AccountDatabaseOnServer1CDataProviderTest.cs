﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.RasClient.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Провайдер для получения инф. баз на сервере предприятия
    /// </summary>
    public class AccountDatabaseOnServer1CDataProviderTest : IAccountDatabaseOnServer1CDataProvider
    {
        /// <summary>
        /// Получить список инф. баз
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список инф. баз</returns>
        public List<ClusterAccountDatabaseInfoDto> GetAccountDatabases(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var testModel = (AccountDatabaseEnterpriseModelTest)accountDatabaseEnterprise;

            var accountDatabasesList = new List<ClusterAccountDatabaseInfoDto>();

            if (testModel.ExistOnServer)
                accountDatabasesList.Add(
                    new ClusterAccountDatabaseInfoDto
                    {
                        ClusterId = Guid.NewGuid().ToString(),
                        AccountDatabaseV82Name = accountDatabaseEnterprise.AccountDatabase.V82Name,
                        AccountDatabaseId = accountDatabaseEnterprise.AccountDatabase.Id.ToString()
                    }
                );

            return accountDatabasesList;
        }
    }
}
