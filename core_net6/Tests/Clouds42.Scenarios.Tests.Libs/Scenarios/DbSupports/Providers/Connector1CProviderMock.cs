﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Провайдер заглушка ком коннектора 1С
    /// </summary>
    public class Connector1CProviderMock(IUnitOfWork dbLayer) : IConnector1CProvider
    {
        /// <summary>
        /// Версия конфигурации
        /// </summary>
        public string Version { get; set; }
        
        /// <summary>
        /// Синоним конфигурации
        /// </summary>
        public string Synonym { get; set; }

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>
        /// <returns>Метаданные информационной базы</returns>    
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo(IUpdateDatabaseDto database)
        {
            var config1C =
                dbLayer.Configurations1CRepository.FirstOrDefault(w =>
                    w.Name == "Бухгалтерия предприятия, редакция 3.0");

            var acDbSupport =
                dbLayer.AcDbSupportRepository.FirstOrDefault(sup =>
                    sup.AccountDatabase.V82Name == database.UpdateDatabaseName);

            return new ConnectorResultDto<MetadataResultDto>
            {
                ConnectCode = ConnectCodeDto.Success,
                Result = new MetadataResultDto(acDbSupport?.CurrentVersion ?? "3.0.73.50", acDbSupport?.ConfigurationName ?? config1C.Name)
            };
        }

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>
        /// <param name="database">Модель данных обновления базы</param>
        public void ApplyUpdates(IUpdateDatabaseDto database)
        {    
            // заглушка
        }
    }
}