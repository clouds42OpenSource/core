﻿using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Tools1C.Connectors1C.DbConnector;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    public class DbConnector1CServerTest(string version, string synonym) : IDbConnector1C
    {
        public bool TryGetMetadataInfo(out MetadataResultDto result)
        {
            result = new MetadataResultDto(version, synonym);
            return true;
        }

        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo()
        {
            return new ConnectorResultDto<MetadataResultDto> {
                ConnectCode = ConnectCodeDto.Success,
                Result = new MetadataResultDto(version, synonym)
            };
        }

        public void SetAdminRolesForUser(string userName)
        {     
            // заглушка
        }

        public void ApplyUpdates()
        {
            // заглушка
        }

        public bool TryApplyLegalUpdate()
        {
            return true;
        }
    }
}