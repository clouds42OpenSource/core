﻿using Clouds42.Cluster1CProviders;
using Clouds42.DataContracts.AccountDatabase.Interface;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    public class Cluster1CProviderTest : ICluster1CProvider
    {
        
        public bool AnyDbSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            return false;
        }

        public void DropSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            // заглушка
        }

        public bool TryDropSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, out string message)
        {
            message = null;
            return true;
        }

        public void DropDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            // заглушка
        }

        public void CreateDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            // заглушка
        }

        public void SetLockStatusDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, bool lockValue)
        {
            // заглушка
        }
    }
}