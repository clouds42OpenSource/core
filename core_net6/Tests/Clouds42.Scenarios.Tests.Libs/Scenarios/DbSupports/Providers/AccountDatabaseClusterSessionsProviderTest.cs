﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.RasClient.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Провайдер сессий инф. баз на сервере 1С для тестов
    /// </summary>
    public class AccountDatabaseClusterSessionsProviderTest : IAccountDatabaseClusterSessionsProvider
    {
        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabase">Инф. база на кластере</param>
        /// <returns>Список сессий инф. базы</returns>
        public List<ClusterAccountDatabaseSessionDto> GetAccountDatabaseSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            ClusterAccountDatabaseInfoDto clusterAccountDatabase)
        {
            var testModel = (AccountDatabaseEnterpriseModelTest) accountDatabaseEnterprise;

            var sessionsList = new List<ClusterAccountDatabaseSessionDto>();

            if (testModel.HasSessions)
                sessionsList.Add(
                    new ClusterAccountDatabaseSessionDto
                    {
                        AccountDatabaseId = clusterAccountDatabase.AccountDatabaseId,
                        LastActivity = DateTime.Now,
                        StartDateTime = DateTime.Now,
                        SessionId = Guid.NewGuid().ToString()
                    }
                );

            return sessionsList;
        }

        /// <summary>
        /// Закрыть сессию инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="accountDatabaseSession">Сессия инф. базы</param>
        public void DropAccountDatabaseSession(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            ClusterAccountDatabaseSessionDto accountDatabaseSession)
        {
            // заглушка
        }
    }
}
