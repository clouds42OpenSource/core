﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers
{
    /// <summary>
    /// Стартер 1С для тестов.
    /// </summary>
    public class Starter1CSuccesesTest(IUnitOfWork dbLayer) : IStarter1C
    {
        private IUpdateDatabaseDto _updateDatabase;

        public bool StartApp(string param, bool isEnterprise, bool logOut = false)
        {
            return true;
        }

        /// <summary>
        /// Обновить информационную базу.
        /// </summary>
        /// <param name="cfuPath">Путь к пакету обновления</param>
        /// <param name="logResult">Результат выполнения из лога 1С</param>
        /// <returns>Результат выполнения</returns>
        public bool UpdateVersion(string cfuPath, out LogUpdate1CParser.Result logResult)
        {
            logResult = new LogUpdate1CParser.Result
            {
                LogState = LogUpdate1CParser.LogState.Success
            };
            return true;
        }

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool ApplyUpdate()
            => true;

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        public ConnectorResultDto<MetadataResultDto> GetMetadata()
        {
            var config1C =
                dbLayer.Configurations1CRepository.FirstOrDefault(w =>
                    w.Name == "Бухгалтерия предприятия, редакция 3.0");

            return new ConnectorResultDto<MetadataResultDto>
            {
                ConnectCode = ConnectCodeDto.Success,
                Result = new MetadataResultDto("3.0.73.50", config1C.Name)
            };
        }

        public IStarter1C SetUpdateDatabase(IUpdateDatabaseDto updateDatabase)
        {
            _updateDatabase = updateDatabase;
            return this;
        }
    }
}
