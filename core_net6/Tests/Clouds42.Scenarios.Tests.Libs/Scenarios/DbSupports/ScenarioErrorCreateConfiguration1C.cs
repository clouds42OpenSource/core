﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase._1C.Managers;
using Clouds42.DataContracts.Configurations1c;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий ошибочного создания конфигурации 1С
    ///     Действия:
    ///         1) Создаем аккаунт
    ///         2) Создаем конфигурацию 1С
    ///         3) Проверяем что конфигурация не создалась
    /// </summary>
    public class ScenarioErrorCreateConfiguration1C : ScenarioBase
    {
        private readonly Configurations1CDataManager _configurations1CDataManager;

        public ScenarioErrorCreateConfiguration1C()
        {
            _configurations1CDataManager = TestContext.ServiceProvider.GetRequiredService<Configurations1CDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var configName = "Конфигурация 1С";

            var createResult = _configurations1CDataManager.AddConfiguration1C(new AddOrUpdateConfiguration1CDataItemDto
            {
                UseComConnectionForApplyUpdates = false,
                Name = configName,
                ConfigurationCatalog = "Config",
                PlatformCatalog = "8.6",
                RedactionCatalogs = "0.0",
                UpdateCatalogUrl = "http://1cConfigs.ru/tmplts/",
                ConfigurationVariations = [configName]
            });

            Assert.IsNotNull(createResult);
            Assert.IsTrue(createResult.Error);

            var configuration = DbLayer.Configurations1CRepository.FirstOrDefault(conf => conf.Name == configName);
            Assert.IsNull(configuration);
        }
    }
}
