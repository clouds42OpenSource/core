﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Starter1C.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Providers;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий получения метаданных из информационной базы через 1С Предприятие
    /// Действия:
    ///     1) Создаем инф. базу
    ///     2) Пытаемся получить метаданные
    ///     3) Проверяем что метаданные получены 
    /// </summary>
    public class ScenarioGetAcDbMetadataThrough1CEnterprise : ScenarioBase
    {
        private readonly AccountDatabaseSupportModelCreator _accountDatabaseSupportModelCreator;

        public ScenarioGetAcDbMetadataThrough1CEnterprise()
        {
            _accountDatabaseSupportModelCreator = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseSupportModelCreator>();
        }

        public override void Run()
        {
            #region Создание инф. базы и модели поддержки

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand, new InfoDatabaseDomainModelTest(TestContext));
            createAccountDatabaseCommand.Run();

            var platformVersionReference =
                DbLayer.PlatformVersionReferencesRepository.FirstOrDefault(w => w.Version == "8.3.13.1644");

            var cloudServicesSegment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault();
            cloudServicesSegment.Stable83Version = platformVersionReference.Version;
            DbLayer.CloudServicesSegmentRepository.Update(cloudServicesSegment);
            DbLayer.Save();

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = string.Empty,
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            #endregion

            #region Получение метаданных через 1С Предприятие

            var databaseSupportModel = _accountDatabaseSupportModelCreator.CreateModel(acDbSupport.AccountDatabasesID);
            var result = ServiceProvider.GetRequiredService<IStarter1C>().SetUpdateDatabase(databaseSupportModel).GetMetadata();

            Assert.AreEqual(result.ConnectCode, ConnectCodeDto.Success);

            #endregion
        }
    }
}
