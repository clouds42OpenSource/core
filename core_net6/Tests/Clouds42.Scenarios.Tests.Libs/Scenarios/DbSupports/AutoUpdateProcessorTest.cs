﻿using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Тест выполнения АО
    /// Сценарий:
    ///         1) Создаем аккаунт и базу
    ///         2) Подключаем базу к АО
    ///         3) Проводим АО для базы, проверяем, что выполнилось без ошибок
    /// </summary>
    public class AutoUpdateProcessorTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();


            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");

            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();
            
            var updateProcessor = ServiceProvider.GetRequiredService<IProcessAutoUpdateAccountDatabasesProvider>();

            var processResult = updateProcessor.ProcessAccountDatabase(acDbSupport);

            if (processResult == null)
                throw new InvalidOperationException("Ошибка проведения АО.");

            Assert.IsNotNull(processResult.NewVersion);
            Assert.AreEqual(processResult.SupportProcessorState, AccountDatabaseSupportProcessorState.Success);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IStarter1C, Starter1CSuccesesTest>();
            serviceCollection.AddTransient<IStarter1CProvider, Starter1CProviderTest>();
            serviceCollection.AddTransient<IConnector1CProvider, Connector1CProviderMock>();
            serviceCollection.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapperTest>();
            serviceCollection.AddTransient<ICluster1CProvider, Cluster1CProviderTest>();
            serviceCollection.AddTransient<IAccessProvider, FakeAccessProvider>();
            serviceCollection.AddTransient<ICfuProvider, FakeCfuProvider>();
        }
    }
}
