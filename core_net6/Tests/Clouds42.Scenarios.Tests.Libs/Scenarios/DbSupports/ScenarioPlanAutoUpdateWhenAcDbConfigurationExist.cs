﻿using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий проверки планирования инф. базы,
    /// в случае если в таблице AcDbSupport заполнены поля конфигурации и синонима конфигурации
    /// Действия:
    ///     1) Создаем аккаунт и инф. базу
    ///     2) Заполняем AcDbSupport для этой базы
    ///     3) Запускаем планирование
    ///     4) Проверяем что база попала в план и конфигурация не отличается от созданной руками
    /// </summary>
    public class ScenarioPlanAutoUpdateWhenAcDbConfigurationExist : ScenarioBase
    {
        private readonly IPlanUpdateVersionAccountDatabaseProvider _planUpdateVersionAccountDatabaseProvider;

        public ScenarioPlanAutoUpdateWhenAcDbConfigurationExist()
        {
            _planUpdateVersionAccountDatabaseProvider = ServiceProvider.GetRequiredService<IPlanUpdateVersionAccountDatabaseProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name,
                SynonymConfiguration = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            var result = _planUpdateVersionAccountDatabaseProvider.PlanUpdateVersionAccountDatabase(acDbSupport);

            Assert.IsTrue(result);

            var acDbSupportFromDb = DbLayer.AcDbSupportRepository.FirstOrDefault(sup =>
                sup.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(acDbSupportFromDb);
            Assert.IsTrue(acDbSupportFromDb.AcDbSupportHistories.Any());
            Assert.AreEqual(acDbSupport.CurrentVersion, acDbSupportFromDb.CurrentVersion);
            Assert.AreEqual(acDbSupport.ConfigurationName, acDbSupportFromDb.ConfigurationName);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICfuProvider, FakeCfuProvider>();
        }
    }
}
