﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий подключения инф. базы к ТиИ
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базой
    ///         2) Подключаем базу к ТиИ
    ///         3) Проверяем что база подключена 
    /// </summary>
    public class ScenarioConnectAccountDatabaseToSupport : ScenarioBase
    {
        private readonly CreateTestDataHelper _createTestDataHelper;
        private readonly IAccountDatabaseManager _accountDatabaseManager;

        public ScenarioConnectAccountDatabaseToSupport()
        {
            _createTestDataHelper = TestContext.ServiceProvider.GetRequiredService<CreateTestDataHelper>();
            _accountDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var createPaymentCommand = new CreatePaymentCommand(TestContext, new PaymentDefinitionModelTest(createAccountCommand.AccountId));
            createPaymentCommand.Run();

            _createTestDataHelper.CreateAcDbSupport(createAccountDatabaseCommand.AccountDatabaseId, "Бухгалтерия предприятия, редакция 3.0");

            var changeResult = _accountDatabaseManager.SaveSupportDataAndAuthorize(new SupportDataDto
            {
                DatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                HasSupport = true,
                Login = "",
                Password= ""
            });

            Assert.IsTrue(changeResult.Result);

            var acDbSupport = DbLayer.AcDbSupportRepository.FirstOrDefault(sup =>
                sup.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(acDbSupport);
            Assert.IsTrue(acDbSupport.HasSupport);
        }
    }
}
