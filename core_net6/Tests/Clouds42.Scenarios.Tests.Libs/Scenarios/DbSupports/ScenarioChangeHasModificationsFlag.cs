﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий проверки смены флага наличия доработок для инф. базы
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базой
    ///         2) Подключаем базу к АО
    ///         3) Имитируем переключение тумблера "База содержит доработки" на true
    ///         4) Проверяем что поле изменило значение на true и база снята с АО
    /// </summary>
    public class ScenarioChangeHasModificationsFlag : ScenarioBase
    {
        private readonly CreateTestDataHelper _createTestDataHelper;
        private readonly AccountDatabaseManager _accountDatabaseManager;
        private readonly AccountDatabaseEditManager _accountDatabaseEditManager;

        public ScenarioChangeHasModificationsFlag()
        {
            _createTestDataHelper = TestContext.ServiceProvider.GetRequiredService<CreateTestDataHelper>();
            _accountDatabaseManager = (AccountDatabaseManager)TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseManager>();
            _accountDatabaseEditManager = TestContext.ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта, инф. базы и подключение базы к АО

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var createPaymentCommand = new CreatePaymentCommand(TestContext, new PaymentDefinitionModelTest(createAccountCommand.AccountId));
            createPaymentCommand.Run();

            _createTestDataHelper.CreateAcDbSupport(createAccountDatabaseCommand.AccountDatabaseId, "Бухгалтерия предприятия, редакция 3.0");
            _createTestDataHelper.CreateAcDbCfu("Бухгалтерия предприятия, редакция 3.0");
            var changeResult = _accountDatabaseManager.SaveSupportDataAndAuthorize(new SupportDataDto
            {
                DatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true
            });
            Assert.IsTrue(changeResult.Result);

            #endregion

            #region Проверка наличия АО

            var acDbSupport = DbLayer.AcDbSupportRepository.FirstOrDefault(sup =>
                sup.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(acDbSupport);
            Assert.IsTrue(acDbSupport.HasAutoUpdate);

            #endregion

            #region Смена флага "База содержит доработки" на true

            var editResult =
                _accountDatabaseEditManager.ChangeDatabaseHasModificationsFlag(acDbSupport.AccountDatabasesID, true);

            Assert.IsFalse(editResult.Error);

            DbLayer.AcDbSupportRepository.Reload(acDbSupport);
            Assert.IsTrue(acDbSupport.DatabaseHasModifications);
            Assert.IsFalse(acDbSupport.HasAutoUpdate);
            Assert.IsFalse(acDbSupport.PrepearedForUpdate);

            #endregion

        }
    }
}
