﻿using System;
using System.IO;
using Clouds42.Cluster1CProviders;
using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.DataContracts.Configurations1c.CfuPackage;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий проверки удаления временно директории после АО инф. базы
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базой
    ///         2) Запускаем АО для инф. базы
    ///         3) После выполнения проверяем что создалась задача на удаление временной директории с CFU пакетами
    /// </summary>
    public class ScenarioCheckDeletionTempDirectoryAfterAutoUpdate : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");

            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            acDbSupport = DbLayer.AcDbSupportRepository.FirstOrDefault(w => w.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            Services.AddTransient<IStarter1C, Starter1CSuccesesTest>();
            Services.AddTransient<IStarter1CProvider, Starter1CProvider>();
            Services.AddTransient<IConnector1CProvider, Connector1CProviderMock>();
            Services.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapperTest>();
            Services.AddTransient<ICfuProvider, FakeCfuProvider>();
            Services.AddTransient<ICluster1CProvider, Cluster1CProviderTest>();
            var sp = Services.BuildServiceProvider();
            CreateNextUpdateCfuVersion(config1C, acDbSupport, sp);
            CreateFileInTempCfuStorage(acDbSupport.AccountDatabase, sp);

            var updateProcessor = sp.GetRequiredService<IProcessAutoUpdateAccountDatabasesProvider>();

            var processResult = updateProcessor.ProcessAccountDatabase(acDbSupport);

            if (processResult == null)
                throw new InvalidOperationException("Ошибка проведения АО.");

            RefreshDbCashContext(Context.CoreWorkerTasksQueues);

            var coreWorkerTasksQueue =
                DbLayer.CoreWorkerTasksQueueRepository.FirstOrDefault(task =>
                    task.CoreWorkerTask.TaskName == CoreWorkerTaskType.RemoveDirectoryJob.ToString());

            var taskParams = coreWorkerTasksQueue.TaskParameter.TaskParams.DeserializeFromJson<RemoveDirectoryJobParams>();
            Assert.AreEqual(taskParams.DerectoryPath, GetCfuTemporaryStoragePath(acDbSupport.AccountDatabase));
        }

        /// <summary>
        /// Создать следующую версию пакета CFU для обновления
        /// </summary>
        /// <param name="configurations1C">Конфигурация 1С</param>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        private void CreateNextUpdateCfuVersion(Domain.DataModels.Configurations1C configurations1C, AcDbSupport acDbSupport, IServiceProvider serviceProvider)
        {
            var cfuProvider = ServiceProvider.GetRequiredService<ICfuProvider>();
            var dbPlatformVersion = serviceProvider.GetRequiredService<IDatabasePlatformVersionHelper>()
                .GetPlatformVersion(acDbSupport.AccountDatabase);

            var nextUpdateVersion =
                cfuProvider.GetNextUpdateVersion(configurations1C, acDbSupport.CurrentVersion, dbPlatformVersion.Version);

            if (nextUpdateVersion != null)
                return;

            SaveNewCfuToDb(new CfuConfigurations
            {
                VersionCfu = new VersionCfu { Platform1C = dbPlatformVersion.Version, VersionValue = $"{acDbSupport.CurrentVersion}1"},
                FilePath = "test",
                TargetList = [acDbSupport.CurrentVersion]
            }, configurations1C);
        }

        /// <summary>
        /// Сохранить новый CFU пакет в базу данных
        /// </summary>
        /// <param name="cfuPackage">CFU пакет</param>
        /// <param name="configurations1C">Конфигурация 1С</param>
        private void SaveNewCfuToDb(CfuConfigurations cfuPackage, Domain.DataModels.Configurations1C configurations1C)
        {
            using var dbScope = DbLayer.SmartTransaction.Get();
            try
            {
                var cfuId = Guid.NewGuid();
                DbLayer.ConfigurationsCfuRepository.Insert(new ConfigurationsCfu
                {
                    Id = cfuId,
                    ConfigurationName = configurations1C.Name,
                    DownloadDate = DateTime.Now,
                    Version = cfuPackage.Version,
                    Platform1CVersion = cfuPackage.VersionCfu.Platform1C,
                    FilePath = cfuPackage.FilePath,
                    ValidateState = true
                });
                DbLayer.Save();

                foreach (var targetVersion in cfuPackage.TargetList)
                {
                    DbLayer.AccountDatabaseUpdateVersionMappingRepository.Insert(new AccountDatabaseUpdateVersionMapping
                    {
                        CfuId = cfuId,
                        ConfigurationName = configurations1C.Name,
                        TargetVersion = targetVersion
                    });
                    DbLayer.Save();

                }

                DbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Создать файл во временном хранилище CFU пакетов
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        private void CreateFileInTempCfuStorage(Domain.DataModels.AccountDatabase accountDatabase, IServiceProvider serviceProvider)
        {
            var tempCfuStorage = GetCfuTemporaryStoragePath(accountDatabase);

            if (!Directory.Exists(tempCfuStorage))
                Directory.CreateDirectory(tempCfuStorage);

            serviceProvider.GetRequiredService<CreateTextFileHelper>()
                .CreateTextFileForTest(Path.Combine(tempCfuStorage, $"{DateTime.Now:ddmmhhss}.txt"));
        }

        /// <summary>
        /// Получить путь для временного хранилища CFU пакетов
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Путь для временного хранилища CFU пакетов</returns>
        private string GetCfuTemporaryStoragePath(Domain.DataModels.AccountDatabase accountDatabase)
            => Path.Combine(CloudConfigurationProvider.Enterprise1C.GetTemporaryStoragePath(), "Cfus",
                accountDatabase.V82Name);
    }
}
