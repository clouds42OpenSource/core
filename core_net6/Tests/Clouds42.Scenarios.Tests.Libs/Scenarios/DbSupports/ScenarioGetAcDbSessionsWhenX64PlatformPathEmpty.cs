﻿using System;
using Clouds42.Cluster1CProviders;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий получения списка активных сессий инф. базы когда путь к платформе x64 не указан
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базой
    ///         2) Имитируем что для платформы не указан путь к х64
    ///         3) Пытаемся получить список активных сеансов
    ///         4) Проверяем что упала ошибка
    /// </summary>
    public class ScenarioGetAcDbSessionsWhenX64PlatformPathEmpty : ScenarioBase
    {
        private readonly IAccountDatabaseModelCreator _accountDatabaseModelCreator;
        private readonly ICluster1CProvider _cluster1CProvider;

        public ScenarioGetAcDbSessionsWhenX64PlatformPathEmpty()
        {
            _accountDatabaseModelCreator = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseModelCreator>();
            _cluster1CProvider = TestContext.ServiceProvider.GetRequiredService<ICluster1CProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = string.Empty,
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            var accountDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(acDb =>
                    acDb.Id == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(accountDatabase);

            accountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();

            var supportModel = _accountDatabaseModelCreator.CreateModel(accountDatabase);

            var supportModelTest = new AccountDatabaseEnterpriseModelTest
            {
                AccountDatabase = supportModel.AccountDatabase,
                EnterpriseServer = supportModel.EnterpriseServer,
                PlatformPathX64 = string.Empty,
                Segment = supportModel.Segment,
                SqlServer = supportModel.SqlServer,
                HasSessions = true,
                ExistOnServer = true
            };

            try
            {
                var result = _cluster1CProvider.AnyDbSessions(supportModelTest);
                Assert.IsNull(result, "Не верное поведение системы. Должна упасть ошибка!");
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
