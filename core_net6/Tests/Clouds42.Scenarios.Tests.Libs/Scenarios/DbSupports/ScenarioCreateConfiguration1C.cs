﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase._1C.Managers;
using Clouds42.DataContracts.Configurations1c;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий создания конфигурации 1С
    ///     Действия:
    ///         1) Создаем аккаунт
    ///         2) Создаем конфигурацию 1С
    ///         3) Проверяем что конфигурация создалась
    /// </summary>
    public class ScenarioCreateConfiguration1C : ScenarioBase
    {
        private readonly Configurations1CDataManager _configurations1CDataManager;

        public ScenarioCreateConfiguration1C()
        {
            _configurations1CDataManager = TestContext.ServiceProvider.GetRequiredService<Configurations1CDataManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var configName = $"БП 3.0_{Guid.NewGuid()}";

            try
            {
                var createResult = _configurations1CDataManager.AddConfiguration1C(new AddOrUpdateConfiguration1CDataItemDto
                {
                    UseComConnectionForApplyUpdates = false,
                    Name = configName,
                    ConfigurationCatalog = "Accounting",
                    PlatformCatalog = "8.3",
                    RedactionCatalogs = "3.0",
                    UpdateCatalogUrl = "http://downloads.v8.1c.ru/tmplts/",
                    ConfigurationVariations = [configName],
                    ItsAuthorizationDataId = GetAnyItsAuthorizationRecordIdOrThrowException()
                });

                Assert.IsNotNull(createResult);
                Assert.IsFalse(createResult.Error, createResult.Message);

                var configuration = DbLayer.Configurations1CRepository.FirstOrDefault(conf => conf.Name == configName);
                Assert.IsNotNull(configuration);
            }
            finally
            {
               RemoveConfiguration1C(configName);
            }
        }
    }
}
