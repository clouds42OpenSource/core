﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Clouds42.Starter1C.Helpers.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий получения метаданных из базы для проверки регистрации КОМ объекта на лету
    /// Действия:
    ///     1) Создаем инф. базу
    ///     2) Пытаемся получить метаданные
    ///     3) Проверяем что метаданные получены 
    /// </summary>
    public class ScenarioGetMetadataFromInfoBaseTest : ScenarioBase
    {
        private readonly ISupportAuthorizationProvider _supportAuthorizationProvider;

        public ScenarioGetMetadataFromInfoBaseTest()
        {
            _supportAuthorizationProvider = ServiceProvider.GetRequiredService<ISupportAuthorizationProvider>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var platformVersionReference =
                DbLayer.PlatformVersionReferencesRepository.FirstOrDefault(w => w.Version == "8.3.13.1644");

            var cloudServicesSegment = DbLayer.CloudServicesSegmentRepository.FirstOrDefault();
            cloudServicesSegment.Stable83Version = platformVersionReference.Version;
            DbLayer.CloudServicesSegmentRepository.Update(cloudServicesSegment);
            DbLayer.Save();

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = string.Empty,
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            var result = _supportAuthorizationProvider.GetMetadataInfoAccountDatabase(acDbSupport);

            Assert.AreEqual(result.ConnectCode, ConnectCodeDto.Success);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IStarter1C, Starter1CSuccesesTest>();
        }
    }
}
