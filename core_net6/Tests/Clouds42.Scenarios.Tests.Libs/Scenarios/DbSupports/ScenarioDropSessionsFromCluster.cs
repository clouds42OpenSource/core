﻿using System;
using Clouds42.Cluster1CProviders;
using Clouds42.RasClient.Providers.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Helpers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий проверки завершения сессий на кластере
    ///     Действия:
    ///         1) Создаем аккаунт с инф. базой
    ///         2) Вызываем метод завершения сессий
    ///         3) Проверяем что операция прошла успешно
    /// </summary>
    public class ScenarioDropSessionsFromCluster : ScenarioBase
    {
        private readonly ICluster1CProvider _cluster1CProvider;
        private readonly IAccountDatabaseModelCreator _accountDatabaseModelCreator;
        private readonly CreateTestDataHelper _createTestDataHelper;
        public ScenarioDropSessionsFromCluster()
        {
            Services.AddTransient<IAccountDatabaseOnServer1CDataProvider, AccountDatabaseOnServer1CDataProviderTest>();
            Services.AddTransient<IAccountDatabaseClusterSessionsProvider, AccountDatabaseClusterSessionsProviderTest>();
            _createTestDataHelper = TestContext.ServiceProvider.GetRequiredService<CreateTestDataHelper>();
            _cluster1CProvider = TestContext.ServiceProvider.GetRequiredService<ICluster1CProvider>();
            _accountDatabaseModelCreator = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseModelCreator>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();



            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = string.Empty,
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();
            _createTestDataHelper.CreateAcDbCfu("Бухгалтерия предприятия, редакция 3.0");
            var accountDatabase =
                DbLayer.DatabasesRepository.FirstOrDefault(acDb =>
                    acDb.Id == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(accountDatabase);

            accountDatabase.IsFile = false;
            DbLayer.DatabasesRepository.Update(accountDatabase);
            DbLayer.Save();

            var supportModel = _accountDatabaseModelCreator.CreateModel(accountDatabase);
            var supportModelTest = new AccountDatabaseEnterpriseModelTest
            {
                AccountDatabase = supportModel.AccountDatabase,
                EnterpriseServer = supportModel.EnterpriseServer,
                PlatformPathX64 = supportModel.PlatformPathX64,
                Segment = supportModel.Segment,
                SqlServer = supportModel.SqlServer,
                HasSessions = false,
                ExistOnServer = true
            };

            try
            {
                _cluster1CProvider.DropSessions(supportModelTest);
            }
            catch (Exception)
            {
                throw new InvalidOperationException("Не верное поведение системы. Операция должна пройти успешно");
            }
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IAccountDatabaseOnServer1CDataProvider, AccountDatabaseOnServer1CDataProviderTest>();
        }
    }
}
