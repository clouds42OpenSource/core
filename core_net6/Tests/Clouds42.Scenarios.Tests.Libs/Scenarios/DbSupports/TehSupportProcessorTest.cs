﻿using System;
using System.Linq;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{

    /// <summary>
    /// Сценарий проверки выполения ТиИ подключенной базы.
    /// Убедись, что в случае виртуалки значение ключа '_1C.TemporaryStoragePath' указывает на диск 'C'
    /// и прописана джоб  RemoveDirectoryJob
    /// </summary>
    public class TehSupportProcessorTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var acDbSupport = new AcDbSupport
            {                
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasSupport = true,
                Login = "admin",
                Password = string.Empty,
                State = (int) SupportState.AutorizationSuccess,
                PreparedForTehSupport = true
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            Services.AddTransient<IStarter1C, Starter1CSuccesesTest>();            
            Services.AddTransient<IStarter1CProvider, Starter1CProvider>();            
            Services.AddTransient<IConnector1CProvider, Connector1CProviderMock>();
            Services.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapperTest>();
            Services.AddTransient<ICluster1CProvider, Cluster1CProviderTest>();
            new FakeAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            Services.AddTransient<IAccessProvider, FakeAccessProvider>();
            var sp = Services.BuildServiceProvider();

            var tehSupportProcessor = sp.GetRequiredService<IProcessTehSupportAccountDatabasesProvider>();

            var processResult = tehSupportProcessor.Process([acDbSupport]);

            if (!processResult)
                throw new InvalidOperationException ("Ошибка проведения ТиИ.");

            var lastTehSupportHistory = DbLayer.AcDbSupportHistoryRepository
                .WhereLazy(h => h.AccountDatabaseId == acDbSupport.AccountDatabasesID).OrderByDescending(o => o.EditDate).FirstOrDefault();

            Assert.IsNotNull(lastTehSupportHistory, "Не удалось получить запись о проведение ТиИ.");

            if (lastTehSupportHistory.State != SupportHistoryState.Success)
                throw new InvalidOperationException ("Ошибка проведения ТиИ.");            

        }
    }
}
