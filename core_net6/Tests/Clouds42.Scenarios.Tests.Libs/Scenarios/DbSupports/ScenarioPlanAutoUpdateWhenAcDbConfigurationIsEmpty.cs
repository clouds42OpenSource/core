﻿using System.Linq;
using Clouds42.Cluster1CProviders;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.Providers;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers;
using Clouds42.Starter1C.Providers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.PlanSupport.Interfaces;
using Clouds42.AccountDatabase.Contracts._1C.Interfaces;
using Clouds42.AccountDatabase.Contracts.CfuPackages.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Сценарий проверки планирования инф. базы,
    /// в случае если в таблице AcDbSupport не заполнены поля конфигурации и синонима конфигурации
    /// Действия:
    ///     1) Создаем аккаунт и инф. базу
    ///     2) Заполняем AcDbSupport для этой базы
    ///     3) Запускаем планирование
    ///     4) Проверяем что база попала в план и в AcDbSupport заполнена конфигурация
    /// </summary>
    public class ScenarioPlanAutoUpdateWhenAcDbConfigurationIsEmpty : ScenarioBase
    {
        private readonly IPlanUpdateVersionAccountDatabaseProvider _planUpdateVersionAccountDatabaseProvider;

        public ScenarioPlanAutoUpdateWhenAcDbConfigurationIsEmpty()
        {
            _planUpdateVersionAccountDatabaseProvider = ServiceProvider.GetRequiredService<IPlanUpdateVersionAccountDatabaseProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var acDb = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == createAccountDatabaseCommand.AccountDatabaseId);
            acDb.IsFile = false;

            dbLayer.DatabasesRepository.Update(acDb);
            dbLayer.Save();

            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess
            };
            dbLayer.AcDbSupportRepository.Insert(acDbSupport);
            dbLayer.Save();

            var result = _planUpdateVersionAccountDatabaseProvider.PlanUpdateVersionAccountDatabase(acDbSupport);

            Assert.IsTrue(result);

            var acDbSupportFromDb = DbLayer.AcDbSupportRepository.FirstOrDefault(sup =>
                sup.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            Assert.IsNotNull(acDbSupportFromDb);
            Assert.IsTrue(acDbSupportFromDb.AcDbSupportHistories.Any());
            Assert.IsNotNull(acDbSupportFromDb.CurrentVersion);
            Assert.IsNotNull(acDbSupportFromDb.ConfigurationName);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IStarter1C, Starter1CSuccesesTest>();
            serviceCollection.AddTransient<IStarter1CProvider, Starter1CProvider>();
            serviceCollection.AddTransient<IConnector1CProvider, Connector1CProviderMock>();
            serviceCollection.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapperTest>();
            serviceCollection.AddTransient<ICluster1CProvider, Cluster1CProviderTest>();
            serviceCollection.AddTransient<ICfuProvider, FakeCfuProvider>();
        }
    }
}
