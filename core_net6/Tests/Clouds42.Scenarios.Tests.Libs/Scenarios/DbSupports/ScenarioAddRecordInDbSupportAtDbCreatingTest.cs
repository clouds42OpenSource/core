﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports
{
    /// <summary>
    /// Проверка создания/не создания сописи в таблицу AcDbSupport
    /// при создании базы с шаблона
    /// Сценарий:
    /// 1) Создаём Аккаунт
    /// 2) Создаём базу с шаблона, в котором не указана параметр CurrentVersion
    /// 3) Проверяем, что запись в AcDbSupport не создалась.
    /// 4) Меняем шаблон, создаём ещё одну базу
    /// 5) Проверяем, что запись в базе создалась 
    /// </summary>
    public class ScenarioAddRecordInDbSupportAtDbCreatingTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.MyEnterprise
                    }
                });

            createAccountCommand.Run();

            TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate();

            var template = DbLayer.DbTemplateRepository.FirstOrDefault(t => t.Name == "db_1c_my_shablon");
            Assert.IsNotNull(template, "Шаблон не найден");
            Assert.IsNull(template.CurrentVersion, "На этом этапе значение должно быть null");

            var accountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            accountDatabaseCommand.Run();

            var databaseId = accountDatabaseCommand.AccountDatabaseId;

            RefreshDbCashContext(TestContext.Context.AcDbSupports);

            var acDbSup = DbLayer.AcDbSupportRepository.Where(s => s.AccountDatabasesID == databaseId);
            Assert.IsFalse(acDbSup.Any(), "Таблица должна быть пуста");

            const string testVersion = "1";
            template.DefaultCaption = "wtf";
            DbLayer.DbTemplateRepository.Update(template);
            DbLayer.Save();
            var accountDatabaseCommand2 = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            accountDatabaseCommand2.Run();

            RefreshDbCashContext(TestContext.Context.AcDbSupports);

            var acDbSup2 = DbLayer.AcDbSupportRepository.FirstOrDefault(s => s.AccountDatabasesID == accountDatabaseCommand2.AccountDatabaseId);

            Assert.IsNotNull(acDbSup2, "Должна быть запись");
            Assert.AreEqual(2, acDbSup2.State, "Неверный статус, должен быть 0");
        }
    }

}
