﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.NewSupportTest
{
    public class PlugUnknowConfigurationToAUTest : ScenarioBase
    {
        public override void Run()
        {
            var _accountDatabaseManager = ServiceProvider.GetRequiredService<IAccountDatabaseManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();

            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = false,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.NotAutorized,
                PrepearedForUpdate = false,
                OldVersion = "3.0.73.40",
                CurrentVersion = "3.0.73.50"
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            var accountDatabaseSupport = DbLayer.AcDbSupportRepository.FirstOrDefault(d => d.AccountDatabasesID == createAccountDatabaseCommand.AccountDatabaseId);

            _accountDatabaseManager.SaveSupportDataAndAuthorize(new SupportDataDto 
            {
                DatabaseId = accountDatabaseSupport.AccountDatabasesID, 
                HasAutoUpdate = true 
            });

            Assert.IsFalse(accountDatabaseSupport.HasAutoUpdate);
        }
    }
}
