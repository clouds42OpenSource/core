﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Models;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbSupports.NewSupportTest
{
    /// <summary>
    /// Тест выполнения АО
    /// Сценарий:
    ///         1) Создаем аккаунт и базу
    ///         2) Cоздаем ноду автообновления
    ///         3) имитируем импорт файлов
    ///         4) убеждаемся что файл создался
    /// </summary>
    public class ImportToObnovlyatorTest : ScenarioBase
    {

        public override void Run()
        {
            var autoUpdateNodeProvider = ServiceProvider.GetRequiredService<IAutoUpdateNodeProvider>();
            var autoUpdateAccountDatabasesProvider = ServiceProvider.GetRequiredService<IAutoUpdateAccountDatabasesProvider>();

            //создаем  аккаунт и базу

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
            createAccountDatabaseCommand.Run();


            //создаем ноду автообновления
            var testAUNode = new AutoUpdateNode
            {
                Id = Guid.NewGuid(),
                NodeAddress = "Test Node",
                AutoUpdateObnovlyatorPath = "C:\\Test"
            };

            if (!Directory.Exists(testAUNode.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath))
                Directory.CreateDirectory(testAUNode.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath);

            autoUpdateNodeProvider.Create(testAUNode);

            var config1C = DbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == "Бухгалтерия предприятия, редакция 3.0");
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = createAccountDatabaseCommand.AccountDatabaseId,
                HasAutoUpdate = true,
                Login = "admin",
                Password = string.Empty,
                State = (int)SupportState.AutorizationSuccess,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            DbLayer.AcDbSupportRepository.Insert(acDbSupport);
            DbLayer.Save();

            var accountDatabasesSupports = DbLayer.AcDbSupportRepository
                .Where(s => s.HasAutoUpdate && s.PrepearedForUpdate &&
                            s.AccountDatabase.State == DatabaseState.Ready.ToString())
                .ToList();

            autoUpdateAccountDatabasesProvider.ProcessAutoUpdate(accountDatabasesSupports);

            var testimportFile = File.ReadAllText(testAUNode.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath + $"/{acDbSupport.AccountDatabase.V82Name}.json");

            Assert.IsNotNull(testimportFile);

            if (Directory.Exists(testAUNode.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath))
                Directory.Delete(testAUNode.AutoUpdateObnovlyatorPath + ObnovlyatorConsts.ImportFolderPath, true);
        }
    }
}
