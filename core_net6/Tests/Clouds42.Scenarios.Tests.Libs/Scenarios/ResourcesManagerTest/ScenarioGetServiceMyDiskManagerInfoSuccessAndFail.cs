﻿using System;
using Clouds42.CloudServices.Contracts;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Core42.Application.Features.MyDiskContext.Queries;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesManagerTest
{
    /// <summary>
    /// Сценарий успешного и неуспешного получения модели управления сервисом "Мой диск"
    /// 1. Создадим аккаунт и активируем Аренду 1С для аккаунта. [AccountCreationAndMyEnterpriseActivation]
    /// 2. Получим модель управления сервиса и проверим корректно были ли получены данные. [GetAndCheckMyDiskManagerDc]
    /// 3. Проверим, что данные не будут получены при несуществующем Id аккаунта. [CheckNotValidAccountId]
    /// </summary>
    public class ScenarioGetServiceMyDiskManagerInfoSuccessAndFail : ScenarioBase
    {
        private readonly ICloud42ServiceFactory _cloud42ServiceFactory;
        private readonly ICloudServiceAdapter _cloudServiceAdapter;

        public ScenarioGetServiceMyDiskManagerInfoSuccessAndFail()
        {
            _cloud42ServiceFactory = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _cloudServiceAdapter = TestContext.ServiceProvider.GetRequiredService<ICloudServiceAdapter>();
        }

        public override void Run()
        {
            #region AccountCreationAndMyEnterpriseActivation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;

            var cloudService =
                _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, createAccountCommand.AccountId);
            cloudService.ActivateService();

            #endregion

            #region GetAndCheckMyDiskManagerDc

            var result = Mediator.Send(new GetMyDiskManagmentDataQuery(createAccountCommand.AccountId)).Result;

            Assert.IsFalse(result.Error, result.Message);

            var serviceMyDiskManagerDc = result.Result;
            var locale = GetAccountLocale(account.Id);
            Assert.AreEqual(serviceMyDiskManagerDc.AccountIsVip, IsVipAccount(account.Id));
            Assert.AreEqual(locale.Name, account.AccountConfiguration.Locale.Name);
            Assert.AreEqual(locale.Currency, account.AccountConfiguration.Locale.Currency);
            Assert.AreEqual(serviceMyDiskManagerDc.Tariff,
                CloudConfigurationProvider.BillingServices.MyDisk.GetMyDiskFreeTariffSize());

            var resourceConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == account.Id && config.BillingService.SystemService == Clouds42Service.MyDisk);

            Assert.AreEqual(serviceMyDiskManagerDc.ExpireDate,
                _cloudServiceAdapter.GetMainConfigurationOrThrowException(resourceConf).ExpireDate);
            Assert.AreEqual(serviceMyDiskManagerDc.Cost, resourceConf.Cost);
            Assert.AreEqual(serviceMyDiskManagerDc.DiscountGroup, resourceConf.DiscountGroup);

            #endregion

            #region CheckNotValidAccountId

            result = Mediator.Send(new GetMyDiskManagmentDataQuery(Guid.NewGuid())).Result;
            Assert.IsTrue(result.Error, "Получили модель при невалидном Id аккаунта");

            #endregion
        }
    }
}
