﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesManagerTest
{
    /// <summary>
    /// Сценарий успешного и неуспешного редактирования ресурса
    /// 1. Создадим аккаунт, получим созданный ресурс и установим ему Subject как Id пользователя аккаунта. [AccountCreationAndGettingExistingResource]
    /// 2. Зададим модель управления ресурсом, отредактируем ресурс и проверим что цены были пересчитаны верно. [EditingResourceAndCheckCorrectness]
    /// 3. Проверим, что будет возвращена ошибка при невалидных данных при редактировании ресурса. [CheckEditingResourceWithNotValidData]
    /// </summary>
    public class ScenarioEditResourceCostSuccessAndFail : ScenarioBase
    {
        private readonly ResourcesManager _resourcesManager;
        private readonly IResourceDataProvider _resourceDataProvider;

        public ScenarioEditResourceCostSuccessAndFail()
        {
            _resourcesManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
            _resourceDataProvider = TestContext.ServiceProvider.GetRequiredService<IResourceDataProvider>();
        }
        public override void Run()
        {
            #region AccountCreationAndGettingExistingResource

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdmin =
                DbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == createAccountCommand.AccountAdminId);

            var resource = DbLayer.ResourceRepository.AsQueryable().Include(res => res.BillingServiceType).FirstOrDefault(res =>
                res.Subject == null && res.AccountId == createAccountCommand.AccountId);

            Assert.IsNotNull(resource, "Не создался ресурс при создании аккаунта");

            resource.Subject = createAccountCommand.AccountAdminId;
            DbLayer.ResourceRepository.Update(resource);
            DbLayer.Save();

            #endregion

            #region EditingResourceAndCheckCorrectness

            var newCost = 222.2M;

            var rent1CUserManagerDc = new Rent1CUserManagerDto
            {
                Cost = newCost,
                Name = "Test name",
                Login = accountAdmin.Login,
                ResourceId = resource.Id,
                ServiceTypeName = "Test service type name"
            };

            var result = _resourcesManager.EditResourceCost(rent1CUserManagerDc, account.Id);
            Assert.IsFalse(result.Error, result.Message);

            var updatedResource =
                DbLayer.ResourceRepository.FirstOrDefault(res => res.Id == rent1CUserManagerDc.ResourceId);
            Assert.IsNotNull(updatedResource, "Пропал ресурс после его редактирования");

            Assert.AreEqual(updatedResource.Cost, rent1CUserManagerDc.Cost, "Не отредактировался ресурс");

            var resourceConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == account.Id && config.BillingServiceId == updatedResource.BillingServiceType.Service.Id);

            Assert.IsNotNull(resourceConf);

            var newResourceConfSum = _resourceDataProvider.GetUsedPaidResourcesCostByService(updatedResource.BillingServiceType.Service.Id, account.Id);
            Assert.AreEqual(resourceConf.Cost, newResourceConfSum, "Не пересчиталась стоимость при редактировании ресурса");

            #endregion

            #region CheckEditingResourceWithNotValidData

            result = _resourcesManager.EditResourceCost(new Rent1CUserManagerDto(), account.Id);
            Assert.IsTrue(result.Error, "Получилось отредактировать ресурс при пустой модели управления ресурсом");

            rent1CUserManagerDc.ResourceId = Guid.NewGuid();

            result = _resourcesManager.EditResourceCost(rent1CUserManagerDc, account.Id);
            Assert.IsTrue(result.Error, "Получилось отредактировать ресурс при Id ресурса");

            rent1CUserManagerDc.ResourceId = resource.Id;
            rent1CUserManagerDc.Cost = -1;

            result = _resourcesManager.EditResourceCost(rent1CUserManagerDc, account.Id);
            Assert.IsTrue(result.Error, "Получилось отредактировать ресурс при цене -1");

            rent1CUserManagerDc.Cost = newCost;

            result = _resourcesManager.EditResourceCost(rent1CUserManagerDc, Guid.NewGuid());
            Assert.IsTrue(result.Error, "Получилось отредактировать ресурс для несуществующего аккаунта");

            #endregion
        }
    }
}
