﻿using System;
using System.Linq;
using Clouds42.CloudServices;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResourcesManagerTest
{
    /// <summary>
    /// Сценарий для успешной и неуспешной проверки получения модели управления ресурсом
    /// 1. Проверим, что корректно будет получена модель или выброшена ошибка при невалидных параметрах. [CheckNotValidData]
    /// 2. Проверим, что модель будет получена при валидных данных. [CheckWithValidDataAndNoSubjectInResource]
    /// 3. Проверим, что модель будет получена корректно, если в ресурсе Subject заполнен Id пользователя. [CheckGettingResourceWithUserSubject]
    /// </summary>
    public class ScenarioGetRent1CUserManagerDcSuccessAndFail : ScenarioBase
    {
        private readonly ResourcesManager _resourceManager;

        public ScenarioGetRent1CUserManagerDcSuccessAndFail()
        {
            _resourceManager = TestContext.ServiceProvider.GetRequiredService<ResourcesManager>();
        }
        public override void Run()
        {
            #region CheckNotValidData

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var result = _resourceManager.GetRent1CUserManagerDc(Guid.NewGuid(), createAccountCommand.AccountId);
            Assert.IsTrue(result.Error, "Корректно получили данные при невалидном Id ресурса");

            var resource = DbLayer.ResourceRepository.AsQueryable().Include(res => res.BillingServiceType).FirstOrDefault(res =>
                res.Subject == null && res.AccountId == createAccountCommand.AccountId);
            Assert.IsNotNull(resource, "Не удалось найти созданных ресурс для аккаунта без указанного Subject");

            result = _resourceManager.GetRent1CUserManagerDc(resource.Id, Guid.NewGuid());
            Assert.IsFalse(result.Error, "Некорректно получили данные при невалидном Id аккаунта");

            #endregion

            #region CheckWithValidDataAndNoSubjectInResource

            result = _resourceManager.GetRent1CUserManagerDc(resource.Id, createAccountCommand.AccountId);
            CompareDcModelWithResource(resource, result.Result);

            #endregion

            #region CheckGettingResourceWithUserSubject

            resource.Subject = createAccountCommand.AccountAdminId;
            DbLayer.ResourceRepository.Update(resource);
            DbLayer.Save();

            result = _resourceManager.GetRent1CUserManagerDc(resource.Id, createAccountCommand.AccountId);

            CompareDcModelWithResource(resource, result.Result, true);

            #endregion

        }

        /// <summary>
        /// Сравнить модель управления ресурсом и ресурс
        /// </summary>
        /// <param name="resource">Ресурс</param>
        /// <param name="rent1CUserManagerDc">Модель управления</param>
        /// <param name="checkSubject">Признак нужно ли проверять свойства связанные с Subject ресурса</param>
        private void CompareDcModelWithResource(Resource resource, Rent1CUserManagerDto rent1CUserManagerDc, bool checkSubject = false)
        {
            Assert.AreEqual(rent1CUserManagerDc.Cost, resource.Cost);
            Assert.AreEqual(rent1CUserManagerDc.ResourceId, resource.Id);
            Assert.AreEqual(rent1CUserManagerDc.ServiceTypeName, resource.BillingServiceType.Name);

            if (!checkSubject)
                return;

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == resource.Subject);

            Assert.AreEqual(rent1CUserManagerDc.Login, user.Login);
            Assert.AreEqual(rent1CUserManagerDc.Name, user.Name);
        }
    }
}
