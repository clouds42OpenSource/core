﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountCSResourceValuesManager
{
	/// <summary>
	/// Проверка работы метода класса AccountCSResourceValuesManager GetValueAsync
	/// </summary>
	public class AccountCsResourceValuesManagerGetValueAsyncTest : ScenarioBase
    {
        private readonly IAccountCsResourceValuesManager _accountCsResourceValuesManager;
        private readonly Random _random = new();

        private readonly IDictionary<Times, Func<Clouds42Service, IAccountDetails, Guid, int, Task>> _mapTimeValueToCheckAction;

        public AccountCsResourceValuesManagerGetValueAsyncTest()
        {
            _accountCsResourceValuesManager = TestContext.ServiceProvider.GetRequiredService<IAccountCsResourceValuesManager>();

            _mapTimeValueToCheckAction = new Dictionary<Times, Func<Clouds42Service, IAccountDetails, Guid, int, Task>>
            {
                {Times.Past, CheckGetCsResourcesForPastTime},
                {Times.Now, CheckGetCsResourcesForNowTime},
                {Times.FutureBeforeExpiration, CheckGetCsResourcesForFutureTimeBeforeExpiration},
                {Times.FutureAfterExpiration, CheckGetCsResourcesForFutureTimeAfteExpiration},
            };
        }

        private enum Times { Past, Now, FutureBeforeExpiration, FutureAfterExpiration }

		public override void Run()
		{
            // добавление аккаунтa и базовой конфигурации
			var createAccountCommand = new CreateAccountCommand(TestContext);
			createAccountCommand.Run();

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Recognition);
            InsertResourceConfiguration(createAccountCommand.AccountId, billingService.Id);

            var demoDaysCount = CloudConfigurationProvider.FastaService.GetDemoDaysCount();
            var licenseCount = ConfigurationHelper.GetConfigurationValue<int>("Recognition42DemoLicenseCount");

			var defaultQtyOfResourcesForServices = new Dictionary<Clouds42Service, int> {
				{ Clouds42Service.Esdl, demoDaysCount},
				{ Clouds42Service.Recognition, licenseCount}
			};

			var resourceIds = new Dictionary<Clouds42Service, Guid>
			{
				{ Clouds42Service.Esdl, ConfigurationHelper.GetConfigurationValue<Guid>("Esdl42DaysResourceId") },
				{ Clouds42Service.Recognition, ConfigurationHelper.GetConfigurationValue<Guid>("Recognition42LicenseResourceId") }
			};

			var services = new[] { Clouds42Service.Esdl, Clouds42Service.Recognition };

            foreach (var service in services)
			{
				var resourceId = resourceIds[service];
				var defaultQtyOfResources = defaultQtyOfResourcesForServices[service];

                CheckGetCsResWithDiffAcIdAndResIdCombinations(createAccountCommand.AccountId,
                    service, resourceId, defaultQtyOfResources).GetAwaiter().GetResult();
                CheckGetCsResWithDiffAcIdAndResIdAndAcUsIdCombinations(createAccountCommand.AccountId,
                    createAccountCommand.AccountAdminId,
                    service, resourceId, defaultQtyOfResources).GetAwaiter().GetResult();

                // 3. Проверяем при различных значениях времени:
				foreach (Times time in Enum.GetValues(typeof(Times)))
                {
                    if (!_mapTimeValueToCheckAction.ContainsKey(time))
                        continue;
                    _mapTimeValueToCheckAction[time](service, createAccountCommand, resourceId,
                        defaultQtyOfResources).GetAwaiter().GetResult();
				}
            }
		}

        /// <summary>
        /// Проверить получение ресурсов для прошедшего времени
        /// </summary>
        /// <param name="service">Облачный сервис</param>
        /// <param name="accountDetails">Данные аккаунта</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResourcesForPastTime(Clouds42Service service, IAccountDetails accountDetails, Guid resourceId, int defaultQtyOfResources)
        {
            var positiveRandDays = _random.Next(1, defaultQtyOfResources);
            var someDateInThePast = DateTime.Now.AddDays(-positiveRandDays);

            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountDetails.AccountId, resourceId, accountDetails.AccountAdminId, someDateInThePast);
            var expectedResourcesCount = service == Clouds42Service.Esdl
                ? defaultQtyOfResources + positiveRandDays
                : 0;

            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, expectedResourcesCount, service);
        }

        /// <summary>
        /// Проверить получение ресурсов для текущего времени
        /// </summary>
        /// <param name="service">Облачный сервис</param>
        /// <param name="accountDetails">Данные аккаунта</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResourcesForNowTime(Clouds42Service service, IAccountDetails accountDetails, Guid resourceId, int defaultQtyOfResources)
        {
            var now = DateTime.Now;
            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(
                accountDetails.AccountId, resourceId, accountDetails.AccountAdminId,
                now);

            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok,
                service == Clouds42Service.Esdl ? defaultQtyOfResources : 0, service);
        }

        /// <summary>
        /// Проверить получение ресурсов для будущего времени перед истечением срока жизни ресурса
        /// </summary>
        /// <param name="service">Облачный сервис</param>
        /// <param name="accountDetails">Данные аккаунта</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResourcesForFutureTimeBeforeExpiration(Clouds42Service service, IAccountDetails accountDetails, Guid resourceId, int defaultQtyOfResources)
        {
            var positiveRandDays = _random.Next(1, defaultQtyOfResources - 1);
            var someDateInTheFuture = DateTime.Now.AddDays(positiveRandDays);

            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountDetails.AccountId, resourceId, accountDetails.AccountAdminId, someDateInTheFuture);
            var expectedResourcesCount = service == Clouds42Service.Esdl
                ? defaultQtyOfResources - positiveRandDays
                : 0;

            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, expectedResourcesCount, service);
        }

        /// <summary>
        /// Проверить получение ресурсов для будущего времени после истечения срока жизни ресурса
        /// </summary>
        /// <param name="service">Облачный сервис</param>
        /// <param name="accountDetails">Данные аккаунта</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResourcesForFutureTimeAfteExpiration(Clouds42Service service, IAccountDetails accountDetails, Guid resourceId, int defaultQtyOfResources)
        {
            var positiveRandDays = _random.Next(defaultQtyOfResources + 1, defaultQtyOfResources + 1 + 365);
            var someDateInTheFuture = DateTime.Now.AddDays(positiveRandDays);

            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountDetails.AccountId, resourceId, accountDetails.AccountAdminId, someDateInTheFuture);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, 0, service);
        }

        /// <summary>
        /// Проверить работу метода получения суммарного значения ресурсов облачного сервиса
        /// при разных сочетаниях AccountId и resourceId
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="service">Облачный сервис</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResWithDiffAcIdAndResIdCombinations(Guid accountId, Clouds42Service service, Guid resourceId, int defaultQtyOfResources)
        {
            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(Guid.NewGuid(), Guid.NewGuid());
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.PreconditionFailed, 0, service);

            // 1.2 Проверяем работу метода, если AccountId верный, но resourceId не соответствует реальному ресурсу (01).
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, Guid.NewGuid());
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.PreconditionFailed, 0, service);

            // 1.3 Проверяем работу метода, если AccountId не соответствует ни одному реальному аккаунту, но resourceId - верный. (10)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(Guid.NewGuid(), resourceId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, 0, service);

            // 1.4 Проверяем работу метода, если AccountId и resourceId корректные (11)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, resourceId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, defaultQtyOfResources, service);
        }

        /// <summary>
        /// Проверить работу метода получения суммарного значения ресурсов облачного сервиса
        /// при разных сочетаниях AccountId, ResourceId и AccountUsedId
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="service">Облачный сервис</param>
        /// <param name="resourceId">ID ресурса</param>
        /// <param name="defaultQtyOfResources">Кол-во ресурсов по умолчанию</param>
        private async Task CheckGetCsResWithDiffAcIdAndResIdAndAcUsIdCombinations(Guid accountId,
            Guid accountUserId, Clouds42Service service, Guid resourceId, int defaultQtyOfResources)
        {
            var resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, resourceId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, defaultQtyOfResources, service);

            // 2.2 (001)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(Guid.NewGuid(), Guid.NewGuid(), accountUserId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.PreconditionFailed, 0, service);

            // 2.3 (010)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(Guid.NewGuid(), resourceId, Guid.NewGuid());
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, 0, service);

            // 2.4 (011)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(Guid.NewGuid(), resourceId, accountUserId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok, 0, service);

            // 2.5 (100)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, Guid.NewGuid(), Guid.NewGuid());
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.PreconditionFailed, 0, service);

            // 2.6 (101)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, Guid.NewGuid(), accountUserId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.PreconditionFailed, 0, service);

            // 2.7 (110)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, resourceId, Guid.NewGuid());
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok,
                service == Clouds42Service.Esdl ? defaultQtyOfResources : 0, service);

            // 2.8 (111)
            resourceValue = await _accountCsResourceValuesManager.GetValueAsync(accountId, resourceId, accountUserId);
            CheckGetCsResourcesResult(resourceValue, ManagerResultState.Ok,
                service == Clouds42Service.Esdl ? defaultQtyOfResources : 0, service);
        }

        /// <summary>
        /// Вставить запись ресурс конфигурации в БД
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса</param>
        private void InsertResourceConfiguration(Guid accountId, Guid billingServiceId)
        {
            DbLayer.ResourceConfigurationRepository.Insert(new ResourcesConfiguration
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Cost = 0,
                ExpireDate = DateTime.Now.AddDays(20),
                CreateDate = DateTime.Now,
                BillingServiceId = billingServiceId,
                Frozen = false
            });

            DbLayer.Save();
		}

        /// <summary>
        /// Проверить результат получения суммарного значения ресурсов облачного сервиса 
        /// </summary>
        /// <param name="getResourcesResult">Результат получения ресурсов</param>
        /// <param name="expectedResultState">Ожидаемое состояние результата</param>
        /// <param name="expectedResourcesCount">Ожидаемое кол-во ресурсов</param>
        /// <param name="service">Облачный сервис</param>
        private static void CheckGetCsResourcesResult(ManagerResult<int> getResourcesResult, ManagerResultState expectedResultState, int expectedResourcesCount, Clouds42Service service)
        {
			Assert.IsTrue(getResourcesResult.State == expectedResultState && getResourcesResult.Result == expectedResourcesCount, 
                GenerateErrorMessage(expectedResultState, service, expectedResourcesCount, getResourcesResult.Result));
        }

		/// <summary>
		/// Сгенерировать сообщение об ошибке
		/// </summary>
		/// <param name="expectedResultState">Ожидаемое состояние результата (ManagerResult)</param>
		/// <param name="service">Системеный сервис</param>
		/// <param name="expectedResourcesCount">Ожидаемое кол-во ресурсов</param>
		/// <param name="actualResourcesCount">Актуальное кол-во ресурсов</param>
		/// <returns>Сообщение об ошибке</returns>
		private static string GenerateErrorMessage(ManagerResultState expectedResultState, Clouds42Service service, int expectedResourcesCount, int actualResourcesCount)
            => $"На пустой базе для вновь созданного аккаунта возвращаемое значение ожидается равным {expectedResultState}" +
               $" и величина ресурса {service.GetDisplayName()} должна быть равна {expectedResourcesCount}, фактически же она равна {actualResourcesCount}";
    }
}
