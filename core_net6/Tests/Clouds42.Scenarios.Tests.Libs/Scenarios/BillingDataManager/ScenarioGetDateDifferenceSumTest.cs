﻿using System;
using System.Linq;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.BillingAccountContext.Queries;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager
{

    /// <summary>
    /// Тестирование получения разницы суммы бонусного и денежного счета при изменении фильтра по дате
    /// 1) Создаем аккаунт
    /// 2) Создаем входящие бонусные и денежные транзакции
    /// 3) Задаем фильтр с измененным значением даты
    /// 4) Просчитываем разницу бонусной и денежной суммы для фильтра
    /// 5) Сравнивыем просчитанную разницу сумм с полученной
    /// </summary>
    public class ScenarioGetDateDifferenceSumTest : ScenarioBase
    {

        private readonly IBillingPaymentsProvider _billingPaymentsProvider;

        public ScenarioGetDateDifferenceSumTest()
        {
            _billingPaymentsProvider = TestContext.ServiceProvider.GetRequiredService<IBillingPaymentsProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var rand = new Random();

            const int transactionsCount = 30;

            for (var transaction = 1; transaction < transactionsCount; transaction++)
            {
                _billingPaymentsProvider.CreatePayment(new PaymentDefinitionDto
                {
                    Account = createAccountCommand.AccountId,
                    Date = DateTime.Now.AddDays(-transaction),
                    Description = "Транзакция для теста",
                    BillingServiceId = GetRent1CServiceOrThrowException().Id,
                    System = PaymentSystem.ControlPanel,
                    Status = PaymentStatus.Done,
                    OperationType = (transaction % 2 == 0) ? PaymentType.Inflow : PaymentType.Outflow,
                    Total = rand.Next(1, 10000),
                    OriginDetails = PaymentSystem.ControlPanel.ToString(),
                    TransactionType = (transaction % 3 == 0) ? TransactionType.Bonus : TransactionType.Money
                });
            }

            var dateFrom = DateTime.Now.AddDays(-9).AddDays(1);

            var transactions = dbLayer.PaymentRepository
                .WhereLazy(p => p.AccountId == createAccountCommand.AccountId).Where(o => dateFrom <= o.Date);

            var moneyDiff = TransactionsSumDifferenceTestHelper.GetSumDifference(transactions, TransactionType.Money);

            var bonusDiff = TransactionsSumDifferenceTestHelper.GetSumDifference(transactions, TransactionType.Bonus);

            var billingAccount =
                dbLayer.BillingAccountRepository.FirstOrDefault(bar => bar.Id == createAccountCommand.AccountId);

            var moneyDifferenceForPage = billingAccount.Balance + moneyDiff;
            var bonusDifferenceForPage = billingAccount.BonusBalance + bonusDiff;

            var managerResult =
                Mediator.Send(new GetBillingAccountBalanceQuery { AccountId = createAccountCommand.AccountId }).Result;

            var transactionInfo = managerResult.Result;

            Assert.AreEqual(moneyDifferenceForPage, transactionInfo.Balance + moneyDiff);
            Assert.AreEqual(bonusDifferenceForPage, transactionInfo.BonusBalance + bonusDiff);
        }
    }
}
