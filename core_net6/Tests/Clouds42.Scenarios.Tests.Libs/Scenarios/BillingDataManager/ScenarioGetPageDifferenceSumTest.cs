﻿using System;
using System.Linq;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.BillingAccountContext.Queries;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager
{

    /// <summary>
    /// Тестирование получения разницы суммы бонусного и денежного баланса для страницы
    /// Сценарий:
    /// 1) Создаем аккаунт
    /// 2) Создаем входящие бонусные и денежные транзакции
    /// 3) Задаем фильтр с измененным значением страницы
    /// 4) Просчитываем разницу бонусной и денежной суммы для страницы
    /// 5) Сравнивыем просчитанную разницу сумм с полученной
    /// </summary>
    public class ScenarioGetPageDifferenceSumTest : ScenarioBase
    {
        private readonly IBillingPaymentsProvider _billingPaymentsProvider;

        public ScenarioGetPageDifferenceSumTest()
        {
            _billingPaymentsProvider = TestContext.ServiceProvider.GetRequiredService<IBillingPaymentsProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var rand = new Random();

            var transactionsCount = 30;

            for (var transaction = 1; transaction < transactionsCount; transaction++)
            {
                _billingPaymentsProvider.CreatePayment(new PaymentDefinitionDto
                {
                    Account = createAccountCommand.AccountId,
                    Date = DateTime.Now.AddDays(-transaction),
                    Description = "Транзакция для теста",
                    BillingServiceId = GetRent1CServiceOrThrowException().Id,
                    System = PaymentSystem.ControlPanel,
                    Status = PaymentStatus.Done,
                    OperationType = (transaction % 2 == 0) ? PaymentType.Inflow : PaymentType.Outflow,
                    Total = rand.Next(1, 10000),
                    OriginDetails = PaymentSystem.ControlPanel.ToString(),
                    TransactionType = (transaction % 3 == 0) ? TransactionType.Bonus : TransactionType.Money
                });
            }

            var pageSize = 10;

            var filter = new ProvidedServiceAndTransactionsFilterDto
            {
                DateFrom = DateTime.Now.AddDays(-transactionsCount - 1),
                DateTo = DateTime.Now,
                PageNumber = 2
            };

            var transactionsForPage = dbLayer.PaymentRepository
                .WhereLazy(p => p.AccountId == createAccountCommand.AccountId).OrderByDescending(o => o.Date)
                .Take(pageSize * (filter.PageNumber - 1));

            var moneyDiff = TransactionsSumDifferenceTestHelper.GetSumDifference(transactionsForPage, TransactionType.Money);

            var bonusDiff = TransactionsSumDifferenceTestHelper.GetSumDifference(transactionsForPage, TransactionType.Bonus);

            var billingAccount =
                dbLayer.BillingAccountRepository.FirstOrDefault(bar => bar.Id == createAccountCommand.AccountId);

            var moneyDifferenceForPage = billingAccount.Balance + moneyDiff;
            var bonusDifferenceForPage = billingAccount.BonusBalance + bonusDiff;

            var managerResult = Mediator.Send(new GetBillingAccountBalanceQuery { AccountId = createAccountCommand.AccountId }).Result;

            var transactionInfo = managerResult.Result;

            Assert.AreEqual(moneyDifferenceForPage, transactionInfo.Balance + moneyDiff);
            Assert.AreEqual(bonusDifferenceForPage, transactionInfo.BonusBalance + bonusDiff);
        }
    }
}
