﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using billing = Clouds42.Billing.BillingOperations.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager
{
    /// <summary>
    /// Тест на метод получения данных для формы пополнения баланса
    /// Сценарий:
    ///         1) Создаем аккаунт, делаем его ВИПом, добавляем доп. ресурсы
    ///         2) Получаем данные для формы пополнения баланса
    ///         3) Проверяем, что модель доп.ресурсов не пустая, что стоимость и название доп. ресурса соответствует доп. ресурсам аккаунта
    /// </summary>
    public class ScenarioGetDataForReplenishBalanceForVipAccountTest : ScenarioBase
    {
        private readonly billing.BillingDataManager _billingDataManager;

        public ScenarioGetDataForReplenishBalanceForVipAccountTest()
        {
            _billingDataManager = TestContext.ServiceProvider.GetRequiredService<billing.BillingDataManager>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var additionalResourceCost = 900;
            var additionalResourceName = "My Additional Resource";

             var account = new CreateAccountCommand(TestContext);
            account.Run();
            
            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(a => a.Id == account.AccountId);

            TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
                .UpdateSignVipAccount(account.AccountId, true);

            billingAccount.AdditionalResourceName = additionalResourceName;
            billingAccount.AdditionalResourceCost = additionalResourceCost;
            dbLayer.BillingAccountRepository.Update(billingAccount);
            dbLayer.Save();
            
            var managerResult = _billingDataManager.GetDataForReplenishBalance(false, new SuggestedPaymentModelDto());

            var additionalResourcesForRent1C = managerResult.Result.AdditionalResourcesForRent1C;
            
            Assert.IsNotNull(additionalResourcesForRent1C);
            Assert.AreEqual(additionalResourceCost, additionalResourcesForRent1C.CostPerOneLicense);
            Assert.AreEqual(additionalResourceName, additionalResourcesForRent1C.ServiceTypeName);
        }
    }
}

