﻿using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingDataManager.Helpers
{
    /// <summary>
    /// Хелпер для вычисления разницы суммы для транзакций
    /// </summary>
    public static class TransactionsSumDifferenceTestHelper
    {
        /// <summary>
        /// Получить разницу суммы для транзакций
        /// </summary>
        /// <param name="transactions">Список транзакций</param>
        /// <param name="transactionType">Тип транзакции</param>
        /// <returns>Разница суммы</returns>
        public static decimal GetSumDifference(IEnumerable<Payment> transactions, TransactionType transactionType) => transactions
            .Sum(x => x.TransactionType == transactionType
                ? x.Sum * (x.OperationType == PaymentType.Inflow.ToString() ? -1 : 1) : 0);
    }
}
