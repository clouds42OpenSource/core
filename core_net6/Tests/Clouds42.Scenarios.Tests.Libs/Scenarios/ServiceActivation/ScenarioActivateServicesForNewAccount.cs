﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation
{
    /// <summary>
    /// Сценарий теста для проверки создания базы
    /// после активации сервиса для нового аккаунта в случае если у него только одна совместимая конфигурация
    /// Действия:
    ///     1) Создаем сервис биллинга
    ///     2) Имитируем регистрацию второго аккаунта по реф. ссылке сервиса
    ///     3) Проверяем что сервис активирован и база создана
    /// </summary>
    public class ChecDatabaseCreationAfterServiceActivationForNewAccount : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта и сервиса

            var accountServiceOwner = new CreateAccountCommand(TestContext);
            accountServiceOwner.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest =
                {
                    AccountId = accountServiceOwner.AccountId
                }
            };
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            #endregion

            #region Регистрация нового аккаунта по реф ссылке сервиса

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto { CloudServiceId = createBillingServiceCommand.Id }
            });
            createAccountCommand.Run();

            #endregion

            #region Проверка активации сервиса и наличия баз

            var serviceResConfig =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, createBillingServiceCommand.Id);

            Assert.IsTrue(serviceResConfig.IsDemoPeriod);

            #endregion
        }
    }
}
