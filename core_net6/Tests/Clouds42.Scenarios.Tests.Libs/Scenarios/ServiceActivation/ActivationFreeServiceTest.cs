﻿using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Constants;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation
{
    /// <summary>
    /// Сценарий теста для проверки активации бесплатного сервиса 
    /// Действия:
    ///     1) Создаем аккаунт и сервис
    ///     2) Активируем сервис у аккаунта
    ///     3) Проверяем что сервис активирован без демо периода, но база не создана 
    /// </summary>
    public class ActivationFreeServiceTest : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта с 2 пользователями и сервиса

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdmin = createAccountCommand.Account.AccountUsers.FirstOrDefault();
            Assert.IsNotNull(accountAdmin, "Не создался админ аккаунта");


            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext, CreateModelForCreateBillingSevice(account.Id));
            createBillingServiceCommand.Run();

            #endregion

            #region Активация сервиса у аккаунта

            var activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(createBillingServiceCommand.Id, accountAdmin.Id);

            #endregion

            #region Проверка активации сервиса и наличия баз

            var serviceResConfig =
                GetResourcesConfigurationOrThrowException(account.Id, createBillingServiceCommand.Id);

            Assert.IsTrue(!serviceResConfig.IsDemoPeriod);
            Assert.IsTrue(serviceResConfig.ExpireDate is null);

            #endregion
        }

        /// <summary>
        /// Создать модель для создания сервиса биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Модель для создания сервиса биллинга</returns>
        private CreateBillingServiceTest CreateModelForCreateBillingSevice(Guid accountId)
            => new(TestContext, freeService: true)
            {
                AccountId = accountId,

            };

        /// <summary>
        /// Создать шаблоны баз на разделителях
        /// </summary>
        private void CreateDbTemplateDelimiters()
        {
            CreateDelimiterTemplate();

            CreateDelimiterTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.KaName,
                DefaultCaption = DbTemplateCaptionConstants.KaCaption
            }, "ka");
        }
    }
}
