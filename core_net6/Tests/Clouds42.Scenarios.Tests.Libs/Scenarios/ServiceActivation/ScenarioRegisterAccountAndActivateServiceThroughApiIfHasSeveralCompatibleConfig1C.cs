﻿using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation
{
    /// <summary>
    /// Сценарий проверки регистрации аккаунта и активации сервиса через апи если у него несколько совместимых конфигураций
    /// Действия:
    ///     1) Создаем сервис биллинга
    ///     2) Имитируем регистрацию второго аккаунта по реф. ссылке сервиса через АПИ
    ///     3) Проверяем что сервис активирован и база создана
    /// </summary>
    public class ScenarioRegisterAccountAndActivateServiceThroughApiIfHasSeveralCompatibleConfig1C : ScenarioBase
    {
        private readonly CreateAccountManager _createAccountManager;

        public ScenarioRegisterAccountAndActivateServiceThroughApiIfHasSeveralCompatibleConfig1C()
        {
            _createAccountManager = TestContext.ServiceProvider.GetRequiredService<CreateAccountManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта и сервиса

            var accountServiceOwner = new CreateAccountCommand(TestContext);
            accountServiceOwner.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();
            
            CreateDbTemplateDelimiters();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext,
                CreateModelForCreateBillingSevice(accountServiceOwner.AccountId));
            
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            #endregion

            #region Регистрация аккаунта через АПИ

            var createAccountResult = _createAccountManager.AddAccountAndCreateSessionAsync(new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createBillingServiceCommand.Id,
                    CloudServiceCompatibleConfig1CForCreateDatabase = "ka"
                }
            }).Result;

            Assert.IsFalse(createAccountResult.Error);
            Assert.IsNotNull(createAccountResult.Result.AccountId);
            Assert.IsFalse(createAccountResult.Result.AccountId.Equals(Guid.Empty));

            var account = DbLayer.AccountsRepository.GetAccount(createAccountResult.Result.AccountId.Value);

            #endregion

            #region Проверка активации сервиса и наличия баз

            var serviceResConfig =
                GetResourcesConfigurationOrThrowException(account.Id, createBillingServiceCommand.Id);

            Assert.IsTrue(serviceResConfig.IsDemoPeriod);

            #endregion
        }

        /// <summary>
        /// Создать модель для создания сервиса биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Модель для создания сервиса биллинга</returns>
        private CreateBillingServiceTest CreateModelForCreateBillingSevice(Guid accountId)
            => new(TestContext)
            {
                AccountId = accountId,
            };

        /// <summary>
        /// Создать шаблоны баз на разделителях
        /// </summary>
        private void CreateDbTemplateDelimiters()
        {
            CreateDelimiterTemplate();

            CreateDelimiterTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.KaName,
                DefaultCaption = DbTemplateCaptionConstants.KaCaption
            }, "ka");
        }
    }
}
