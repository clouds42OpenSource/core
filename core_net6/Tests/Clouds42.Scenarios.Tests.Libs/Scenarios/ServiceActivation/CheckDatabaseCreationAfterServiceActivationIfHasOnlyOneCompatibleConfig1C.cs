﻿using System.Linq;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation
{
    /// <summary>
    /// Сценарий теста для проверки создания базы
    /// после активации сервиса в случае если у него только одна совместимая конфигурация
    /// Действия:
    ///     1) Создаем аккаунт с 2 пользователями и сервис биллинга с одной совместимой конфигурацией
    ///     2) Активируем сервис у аккаунта
    ///     3) Проверяем что сервис активирован и база создана 
    /// </summary>
    public class CheckDatabaseCreationAfterServiceActivationIfHasOnlyOneCompatibleConfig1C : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта с 2 пользователями и сервиса

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdmin = createAccountCommand.Account.AccountUsers.FirstOrDefault();
            Assert.IsNotNull(accountAdmin, "Не создался админ аккаунта");

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

           CreateDelimiterTemplate();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext)
            {
                CreateBillingServiceTest =
                {
                    AccountId = account.Id
                }
            };
            createBillingServiceCommand.Run();

            var secondUserId = AddUserToAccount(new AccountUserDcTest
            {
                AccountId = account.Id
            });

            var secondUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == secondUserId);
            Assert.IsNotNull(secondUser);

            #endregion

            #region Активация сервиса у аккаунта
            Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>();
            var sp = Services.BuildServiceProvider();
            var activateServiceForRegistrationAccountProvider = sp.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp"]);
            activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(createBillingServiceCommand.Id, secondUser.Id);

            #endregion

            #region Проверка активации сервиса и наличия баз

            var serviceResConfig =
                GetResourcesConfigurationOrThrowException(account.Id, createBillingServiceCommand.Id);

            Assert.IsTrue(serviceResConfig.IsDemoPeriod);

            var accountDatabases = DbLayer.DatabasesRepository.Where(db => db.AccountId == account.Id);
            Assert.IsTrue(accountDatabases.Any(), "База не создана для сервиса с одной совместимой конфигурацией");

            #endregion
        }
    }
}
