﻿using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Scenarios.Tests.Libs.TestCommands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ServiceActivation
{
    /// <summary>
    /// Сценарий теста для проверки активации сервиса в случае если у него несколько совместимых конфигураций 
    /// Действия:
    ///     1) Создаем аккаунт с 2 пользователями и сервис биллинга с несколькими совместимыми конфигурациями
    ///     2) Активируем сервис у аккаунта
    ///     3) Проверяем что сервис активирован, но база не создана 
    /// </summary>
    public class CheckNoDatabaseCreatedAndExtensionInstalledForExistingUserIfHasSeveralCompatibleConfig1C : ScenarioBase
    {
        public override void Run()
        {
            #region Создание аккаунта с 2 пользователями и сервиса

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = createAccountCommand.Account;
            var accountAdmin = createAccountCommand.Account.AccountUsers.FirstOrDefault();
            Assert.IsNotNull(accountAdmin, "Не создался админ аккаунта");

            var dbCountBeforeActivation = account.AccountDatabases == null ? 0 : account.AccountDatabases.Count;

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();
            
            CreateDbTemplateDelimiters();

            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext, CreateModelForCreateBillingSevice(account.Id));
            createBillingServiceCommand.Run();

            var secondUserId = AddUserToAccount(new AccountUserDcTest
            {
                AccountId = account.Id
            });

            var secondUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == secondUserId);
            Assert.IsNotNull(secondUser);

            #endregion

            #region Активация сервиса у аккаунта

            var activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp", "ka"]);
            activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(createBillingServiceCommand.Id, secondUser.Id);

            #endregion

            #region Проверка активации сервиса и наличия баз

            var serviceResConfig =
                GetResourcesConfigurationOrThrowException(account.Id, createBillingServiceCommand.Id);

            Assert.IsTrue(serviceResConfig.IsDemoPeriod);

            var dbCountAfterActivation = DbLayer.DatabasesRepository.Where(db => db.AccountId == account.Id).Count();

            if (dbCountAfterActivation > dbCountBeforeActivation)
                throw new InvalidOperationException("Создалась база после активации сервиса");

            #endregion
        }

        /// <summary>
        /// Создать модель для создания сервиса биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Модель для создания сервиса биллинга</returns>
        private CreateBillingServiceTest CreateModelForCreateBillingSevice(Guid accountId)
            => new(TestContext)
            {
                AccountId = accountId,
                
            };

        /// <summary>
        /// Создать шаблоны баз на разделителях
        /// </summary>
        private void CreateDbTemplateDelimiters()
        {
            CreateDelimiterTemplate();

            CreateDelimiterTemplate(new DbTemplateModelTest(DbLayer)
                {
                    Name = DbTemplatesNameConstants.KaName,
                    DefaultCaption = DbTemplateCaptionConstants.KaCaption
            }, "ka");
        }
    }
}
