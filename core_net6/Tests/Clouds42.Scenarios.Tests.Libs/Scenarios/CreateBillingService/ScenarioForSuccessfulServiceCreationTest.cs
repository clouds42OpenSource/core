﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.PartnersContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateBillingService
{
    /// <summary>
    /// Сценарий теста успешного создания сервиса
    /// 1) Создаем обычный тест путем комманды
    /// 2) Создаем сервис-черновик(у черновика валидации нет)
    /// 3) Создаем сервис и услуги со связями
    /// 4) Проверяем, что исправно создаётся модель для отображения сервиса в списке сервисов
    /// </summary>
    public class ScenarioForSuccessfulServiceCreationTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public ScenarioForSuccessfulServiceCreationTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }
        
        public override void Run()
        {
        }

        public override async Task RunAsync()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            await ScenarioCheckOfCreatingServiceWithoutServiceTypeRelationTest();
            await ScenarioCheckOfCreatingServiceWithServiceTypeRelationTest();
        }

        /// <summary>
        /// Сценарий теста проверки создания сервиса без зависимых услуг
        /// </summary>
        private async Task ScenarioCheckOfCreatingServiceWithoutServiceTypeRelationTest()
        {
            var createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext);
            createBillingServiceCommand.Run();

            var service = await DbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Include(x => x.BillingServiceTypes)
                .ThenInclude(x => x.ChildRelations)
                .FirstOrDefaultAsync(w => w.SystemService == null && w.Name != "Дополнительные сеансы");

            if (service == null)
                throw new InvalidOperationException("Создание сервиса не произошло");

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            var resultOfComparing =
                _createOrEditBillingServiceHelper.CompareSimilarityOfServiceCreationModelAndService(
                    createBillingServiceCommand.CreateBillingServiceTest, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = null,
                BillingServiceTypes = [],
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var account = await DbLayer.AccountsRepository.GetAccountAsync(TestContext.AccessProvider.ContextAccountId);
            var countBillingServices = DbLayer.AccountsRepository.GetCountBillingServicesForAccount(account.Id);
            var defaultBillingServiceName = CreateBillingServiceHelper.GetDefaultBillingServiceName(countBillingServices, account.IndexNumber);
            
            createBillingServiceCommand = new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            service = await DbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Include(x => x.BillingServiceTypes)
                .ThenInclude(x => x.ChildRelations)
                .FirstOrDefaultAsync(w => w.Name == defaultBillingServiceName);

            if (service == null)
                throw new InvalidOperationException("Создание сервиса произошло не корректно");

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("При указании статуса Черновик, валидация не должна осуществляться");

            createBillingServiceTest.Name = defaultBillingServiceName;

            resultOfComparing =
                _createOrEditBillingServiceHelper.CompareSimilarityOfServiceCreationModelAndService(
                    createBillingServiceCommand.CreateBillingServiceTest, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }

        /// <summary>
        /// Сценарий теста проверки создания сервиса c зависимыми услугами
        /// </summary>
        private async Task ScenarioCheckOfCreatingServiceWithServiceTypeRelationTest()
        {
            var webServiceType = GetWebServiceTypeOrThrowException();

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[0].ServiceTypeRelations.Add(billingServiceTypes[1].Id);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[0].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[1].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[2].Id);

            billingServiceTypes[0].DependServiceTypeId = webServiceType.Id;

            const string serviceName = "Тестовый Сервис 2";
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
                BillingServiceStatus = BillingServiceStatusEnum.OnModeration
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException(createBillingServiceCommand.Result.Message);

            var service = await DbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Include(x => x.BillingServiceTypes)
                .ThenInclude(x => x.ChildRelations)
                .FirstOrDefaultAsync(w => w.Name == serviceName);

            var resultOfComparing =
                _createOrEditBillingServiceHelper.CompareSimilarityOfServiceCreationModelAndService(
                    createBillingServiceCommand.CreateBillingServiceTest, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);

            var servicesModel = await Mediator.Send(new GetPartnerServicesQuery{PageNumber = 1, PageSize = 50});
            Assert.IsFalse(servicesModel.Error,"Ошибка получения модели");
            Assert.IsTrue(servicesModel.Result.ChunkDataOfPagination.Count() ==3, "Мы имеем 3 кастомных сервиса.");

        }
    }
}
