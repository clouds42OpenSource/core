﻿using System;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateBillingService
{
    /// <summary>
    /// Сценарий теста ошибочного создания сервиса
    /// Выполняем создание сервиса с не валидными полями
    /// </summary>
    public class ScenarioForErrorServiceCreationTest: ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _billingServiceHelper;

        public ScenarioForErrorServiceCreationTest()
        {
            _billingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            ScenarioCreationBillingServiceWithValidationOfServiceFields();
            ScenarioCreationBillingServiceWithValidationOfServiceTypeFields();
            ScenarioCreationBillingServiceWithValidationOfServiceTypeRelations();
        }

        /// <summary>
        /// Сценарий создания сервиса с проверкой полей сервиса
        /// </summary>
        private void ScenarioCreationBillingServiceWithValidationOfServiceFields()
        {
            ScenarioCheckServiceNameWithCreatingServiceTest();
            ScenarioCheckServiceShortDescriptionWithCreatingServiceTest();
        }

        /// <summary>
        /// Сценарий создания сервиса с проверкой полей услуги
        /// </summary>
        private void ScenarioCreationBillingServiceWithValidationOfServiceTypeFields()
        {
            var webServiceType = GetWebServiceTypeOrThrowException();
            var serviceType = _billingServiceHelper.GenerateBillingServiceType(dependServiceTypeId: webServiceType.Id);
            serviceType.Name = null;
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = [serviceType]
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Название услуги сервиса обязательное поле");

            createBillingServiceTest.BillingServiceTypes[0].Name = "test";
            createBillingServiceTest.BillingServiceTypes[0].ServiceTypeCost = -99;

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Стоимость услуги должна быть не меньше 0");

            createBillingServiceTest.BillingServiceTypes[0].Description = null;
            createBillingServiceTest.BillingServiceTypes[0].ServiceTypeCost = 55;

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Описание услуги сервиса обязательное поле");

            createBillingServiceTest.BillingServiceTypes[0].Description = "test";
            createBillingServiceTest.BillingServiceTypes[0].DependServiceTypeId = null;

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Сервис должен иметь хотя бы одну услугу с типом подключения к информационной базе 1С");

            createBillingServiceTest.BillingServiceTypes[0].DependServiceTypeId = webServiceType.Id;
            createBillingServiceTest.Key = Guid.Empty;

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Ключ сервиса обязательное поле");

            createBillingServiceTest.Key = Guid.NewGuid();
            createBillingServiceTest.BillingServiceTypes[0].Key = Guid.Empty;

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Ключ услуги обязательное поле");
        }

        /// <summary>
        /// Сценарий создания сервиса с проверкой связей услуг
        /// </summary>
        private void ScenarioCreationBillingServiceWithValidationOfServiceTypeRelations()
        {
            var billingServiceTypes = _billingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[0].ServiceTypeRelations.Add(billingServiceTypes[1].Id);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[0].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[0].Id);
            billingServiceTypes[0].ServiceTypeRelations.Add(billingServiceTypes[3].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes = billingServiceTypes
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Нельзя создать сервис при некорректном комбинировании услуг");
        }

        /// <summary>
        /// Сценарий проверки названия сервиса при создании сервиса
        /// </summary>
        private void ScenarioCheckServiceNameWithCreatingServiceTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = null
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Название сервиса обязательное поле");

            createBillingServiceTest.Name = nameof(Test);
            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Нельзя создать сервис с двумя одинаковыми названиями");
        }

        /// <summary>
        /// Сценарий проверки описания сервиса при создании сервиса
        /// </summary>
        private void ScenarioCheckServiceShortDescriptionWithCreatingServiceTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext);
            createBillingServiceTest.BillingServiceTypes.ForEach(t => t.Description = null);
            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            if (!createBillingServiceCommand.Result.Error)
                throw new InvalidOperationException("Описание сервиса обязательное поле");
        }
    }
}
