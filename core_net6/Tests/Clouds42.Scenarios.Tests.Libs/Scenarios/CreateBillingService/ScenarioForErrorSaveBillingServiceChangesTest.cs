﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateBillingService
{
    /// <summary>
    /// Сценарий теста ошибочного сохранения изменений сервиса
    /// 1) Сценарий проверки файлов сервиса при сохранении изменений сервиса
    /// 2) Сценарий проверки полей сервиса при сохранении изменений сервиса
    /// 3) Сценарий проверки полей сервиса услуг при сохранении изменений сервиса
    /// </summary>
    public class ScenarioForErrorSaveBillingServiceChangesTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand =
                new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            ScenarioCheckFilesServiceWithSaveBillingServiceChangesTest();
            ScenarioCheckBillingServiceFieldsWithSaveBillingServiceChangesTest();
            ScenarioCheckBillingServiceTypesFieldsWithSaveBillingServiceChangesTest();
        }

        /// <summary>
        /// Сценарий проверки файлов сервиса при сохранении изменений сервиса
        /// </summary>
        private void ScenarioCheckFilesServiceWithSaveBillingServiceChangesTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest, false);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var managerResult = billingServiceCardDataManager.GetData(service.Id);
            var createBillingServiceDto = managerResult.Result.MapToCreateBillingServiceDto();
            createBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;

            var createServiceManager = TestContext.ServiceProvider.GetRequiredService<CreateBillingServiceManager>();
            var editResult = createServiceManager.CreateBillingService(createBillingServiceDto);

            if (!editResult.Error)
                throw new InvalidOperationException("Нельзя сохранить изменения сервиса, файлы отсутсвуют");
        }

        /// <summary>
        /// Сценарий проверки полей сервиса при сохранении изменений сервиса
        /// </summary>
        private void ScenarioCheckBillingServiceFieldsWithSaveBillingServiceChangesTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.Draft, Name = "newTest"
                };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest, false);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id).Result;
            var createBillingServiceDto = billingServiceCardDto.MapToCreateBillingServiceDto();

            createBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            createBillingServiceDto.Name = null;
            createBillingServiceDto.BillingServiceTypes = null;

            var createServiceManager = TestContext.ServiceProvider.GetRequiredService<CreateBillingServiceManager>();
            var editResult = createServiceManager.CreateBillingService(createBillingServiceDto);

            if (!editResult.Error)
                throw new InvalidOperationException("Нельзя сохранить изменения сервиса, так как некоторые поля сервиса заполненны не корректно");
        }

        /// <summary>
        /// Сценарий проверки полей сервиса услуг при сохранении изменений сервиса
        /// </summary>
        private void ScenarioCheckBillingServiceTypesFieldsWithSaveBillingServiceChangesTest()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.Draft, Name = "newTest3"
                };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest ,false);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDataManager = ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id).Result;
            var createBillingServiceDto = billingServiceCardDto.MapToCreateBillingServiceDto();
            createBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            createBillingServiceDto.BillingServiceTypes[0].Name = null;
            createBillingServiceDto.BillingServiceTypes[0].ServiceTypeCost = -1;

            var createServiceManager = TestContext.ServiceProvider.GetRequiredService<CreateBillingServiceManager>();
            var editResult = createServiceManager.CreateBillingService(createBillingServiceDto);

            if (!editResult.Error)
                throw new InvalidOperationException("Нельзя сохранить изменения сервиса, так как некоторые поля сервиса заполненны не корректно");

            billingServiceCardDto.BillingServiceTypes[0].Name = "Проверка";
            billingServiceCardDto.BillingServiceTypes[0].ServiceTypeCost = 551;
            billingServiceCardDto.BillingServiceTypes[0].DependServiceTypeId = null;
            billingServiceCardDto.BillingServiceTypes[1].DependServiceTypeId = null;

            editResult = createServiceManager.CreateBillingService(createBillingServiceDto);

            if (!editResult.Error)
                throw new InvalidOperationException("Сервис должен иметь хотя бы одну услугу с типом подключения к информационной базе 1С");
        }
    }
}
