﻿using System;
using Clouds42.AgencyAgreement.Contracts.AgentFileStorage;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CreateBillingService
{
    /// <summary>
    /// Сценарий теста успешного сохранения изменений сервиса
    /// 1) Сценарий проверки полного изменения сервиса
    /// 2) Сценарий проверки сохранения изменений сервиса когда файлы уже существуют у сервиса
    ///    и при изменении новых нет
    /// 3) Сценарий проверки сохранения изменений сервиса когда сервис черновик
    /// </summary>
    public class ScenarioForSuccessSaveBillingServiceChangesTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly IAgentFileStorageProvider _agentFileStorageProvider;
        private readonly BillingServiceCardDataManager _billingServiceCardDataManager;
        private readonly EditBillingServiceManager _editBillingServiceManager;

        public ScenarioForSuccessSaveBillingServiceChangesTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _agentFileStorageProvider = TestContext.ServiceProvider.GetRequiredService<IAgentFileStorageProvider>();
            _billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            _editBillingServiceManager = ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createDbTemplateDelimitersCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            createDbTemplateDelimitersCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            ScenarioCheckFullChangeBillingServiceTest();
            ScenarioCheckOfSaveBillingServiceChangesWhenFilesAlreadyExist();
            ScenarioCheckOfSaveBillingServiceChangesWhenServiceIsDraft();
        }

        /// <summary>
        /// Сценарий проверки полного изменения сервиса
        /// </summary>
        private void ScenarioCheckFullChangeBillingServiceTest()
        {
            var webServiceType = GetWebServiceTypeOrThrowException();
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id).Result;

            if (billingServiceCardDto == null)
                throw new InvalidOperationException("Не удалоcь получить черновик для редактирования");

            // Формируем связи услуг
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(4);
            billingServiceTypes[0].ServiceTypeRelations.Add(billingServiceTypes[1].Id);
            billingServiceTypes[1].ServiceTypeRelations.Add(billingServiceTypes[2].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[0].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[1].Id);
            billingServiceTypes[3].ServiceTypeRelations.Add(billingServiceTypes[2].Id);

            billingServiceTypes[0].DependServiceTypeId = webServiceType.Id;

            var createIndustryCommand = new CreateIndustryCommand(TestContext, new IndustryModelTest { Name = "Новая отрасль", Description = "TEST" });
            createIndustryCommand.Run();

            var editBillingServiceDraftDto = billingServiceCardDto.MapToBillingServiceDraftDto();

            var editResult = _editBillingServiceManager.EditBillingServiceDraft(editBillingServiceDraftDto);

            if (editResult.Error)
                throw new InvalidOperationException("Не корректно работает сохранение изменений черновика");

            service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == editBillingServiceDraftDto.Name);

            var resultOfComparing =
                _createOrEditBillingServiceHelper.CompareSimilarityOfServiceEditingDraftModelAndService(
                    editBillingServiceDraftDto, service);

            if (!resultOfComparing.Similarity)
                throw new InvalidOperationException(resultOfComparing.Message);
        }

        /// <summary>
        /// Сценарий проверки сохранения изменений сервиса когда файлы уже существуют у сервиса
        /// и при изменении новых нет
        /// </summary>
        private void ScenarioCheckOfSaveBillingServiceChangesWhenFilesAlreadyExist()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext) { BillingServiceStatus = BillingServiceStatusEnum.Draft };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id).Result;
            var editBillingServiceDraftDto = billingServiceCardDto.MapToBillingServiceDraftDto();
            editBillingServiceDraftDto.BillingServiceStatus = BillingServiceStatusEnum.Draft;

            var editResult = _editBillingServiceManager.EditBillingServiceDraft(editBillingServiceDraftDto);

            if (editResult.Error)
                throw new InvalidOperationException("Не корректно работает сохранение изменений черновика");
        }

        /// <summary>
        /// Сценарий проверки сохранения изменений сервиса когда сервис черновик
        /// </summary>
        private void ScenarioCheckOfSaveBillingServiceChangesWhenServiceIsDraft()
        {
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                BillingServiceStatus = BillingServiceStatusEnum.Draft
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createBillingServiceCommand.CreateBillingServiceTest.Name);

            var billingServiceCardDto = _billingServiceCardDataManager.GetData(service.Id).Result;

            var editBillingServiceDraftDto = billingServiceCardDto.MapToBillingServiceDraftDto();
            editBillingServiceDraftDto.BillingServiceStatus = BillingServiceStatusEnum.Draft;

            var editResult = _editBillingServiceManager.EditBillingServiceDraft(editBillingServiceDraftDto);

            if (editResult.Error)
                throw new InvalidOperationException("Не корректно работает сохранение изменений черновика");
        }
    }
}
