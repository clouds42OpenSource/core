﻿using System;
using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProcessDeletedServiceTypesForAccountsTest
{
    /// <summary>
    /// Сценарий теста по проверке обработки удаленной услуги с моментальным пересчетом
    ///
    /// 1) Создаем сервис и услуги #CreateService
    /// 2) Создаем 2 аккаута для этого сервиса # CreateAccounts
    /// 3) Удаляем услугу севриса #RemoveServiceType
    /// 4) Выполняем модерацию сервиса #Moderation
    /// 5) Блокируем сервисы. Для первого аккаунта блокируем демо период. Для второго аккаунта блокируем Аренду 1С. #LockServices
    /// 6) Выполняем обработку удаленной услуги для аккаунтов #ProcessDeleted
    /// 7) Проверяем резултат обработки. Ожидаем моментальный пересчет стоимости, удаление ресурсов,
    /// метки что услуга удалена и что не создалась запись для отложенного удаления #CheckProcessDeleted
    /// </summary>
    public class ScenarioProcessDeletedServiceTypesWithInstantRecalculationTest : ScenarioBase
    {
        public override void Run()
        {
            var serviceTypeHelper =
                TestContext.ServiceProvider.GetRequiredService<PrepareServiceWithRequestToRemoveServiceTypeHelperTest>();

            var processDeletedServiceTypesForAccountsProvider =
                TestContext.ServiceProvider.GetRequiredService<IProcessDeletedServiceTypesForAccountsProvider>();

            #region CreateService
            var prepareServiceModel = serviceTypeHelper.CreateService(TestContext);
            #endregion

            #region CreateAccounts
            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = prepareServiceModel.ServcieId
                    }
                });
            createAccountCommand.Run();

            var createSecondAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = prepareServiceModel.ServcieId
                    }
                });
            createSecondAccountCommand.Run();

            var serviceCostForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, prepareServiceModel.ServcieId)
                    .Cost;

            var serviceCostForSecondAccount =
                GetResourcesConfigurationOrThrowException(createSecondAccountCommand.AccountId, prepareServiceModel.ServcieId)
                    .Cost;
            #endregion

            #region RemoveServiceType
            serviceTypeHelper.RemoveServiceType(prepareServiceModel);
            #endregion

            #region Moderation
            var editServiceRequest = serviceTypeHelper.ProcessModerationResult(TestContext, prepareServiceModel);
            #endregion

            #region LockServices
            SetFrozenResourceConfigurationForAccount(createAccountCommand.AccountId, prepareServiceModel.ServcieId,
                DateTime.Now.AddMonths(-2));

            SetFrozenResourceConfigurationForAccount(createSecondAccountCommand.AccountId, Clouds42Service.MyEnterprise,
                DateTime.Now.AddMonths(-2));

            #endregion

            #region ProcessDeleted
            processDeletedServiceTypesForAccountsProvider.Process(new ProcessDeletedServiceTypesForAccountsDto
            {
                ServiceId = prepareServiceModel.ServcieId,
                BillingServiceChangesId = editServiceRequest.BillingServiceChangesId.Value,
                ServiceTypeIds = [prepareServiceModel.DeletedServiceTypeId]
            });
            #endregion

            #region CheckProcessDeleted
            var resConfForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, prepareServiceModel.ServcieId);

            var resConfForSecondAccount =
                GetResourcesConfigurationOrThrowException(createSecondAccountCommand.AccountId, prepareServiceModel.ServcieId);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == prepareServiceModel.ServcieId);

            Assert.IsNotNull(service);
            Assert.AreNotEqual(resConfForFirstAccount.Cost, serviceCostForFirstAccount);
            Assert.AreNotEqual(resConfForSecondAccount.Cost, serviceCostForSecondAccount);
            Assert.AreEqual(0, DbLayer.BillingServiceTypeActivityForAccountRepository.Count());

            Assert.AreEqual(0, DbLayer.ResourceRepository
                .WhereLazy(r => r.BillingServiceTypeId == prepareServiceModel.DeletedServiceTypeId).Count());

            Assert.IsTrue(editServiceRequest.BillingService.BillingServiceTypes.Any(st =>
                st.IsDeleted && st.Key == prepareServiceModel.DeletedServiceTypeId));

            Assert.IsTrue(DbLayer.BillingServiceTypeRelationRepository.Any(st =>
                st.IsDeleted && st.MainServiceTypeId == prepareServiceModel.DeletedServiceTypeId));
            #endregion
        }
    }
}
