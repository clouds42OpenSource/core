﻿using System;
using System.Linq;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ProcessDeletedServiceTypesForAccountsTest
{
    /// <summary>
    /// Сценарий теста по проверке обработки удаленной услуги с отложенным пересчетом
    ///
    /// 1) Создаем сервис и услуги #CreateService
    /// 2) Создаем 2 аккаута для этого сервиса # CreateAccounts
    /// 3) Удаляем услугу севриса #RemoveServiceType
    /// 4) Выполняем модерацию сервиса #Moderation
    /// 5) Выполняем обработку удаленной услуги для аккаунтов #ProcessDeleted
    /// 6) Проверяем резултат обработки. Ожидаем что  создалась запись для отложенного удаления и пересчет стоимости не произошел #CheckProcessDeleted
    /// 7) Подготавливаем данные для пролонгации #PrepareDataForProlong
    /// 8) Выполняем пролонгацию и проверяем данные. Сервис Аренда 1С должен продлиться,
    /// сумма кастомного сервиса должна пересчитаться. Запись об отложенном удалении должна быть удалена #ProlongAndCheckData
    /// </summary>
    public class ScenarioProcessDeletedServiceTypesWithDelayRecalculationTest : ScenarioBase
    {
        public override void Run()
        {
            var serviceTypeHelper =
                TestContext.ServiceProvider.GetRequiredService<PrepareServiceWithRequestToRemoveServiceTypeHelperTest>();

            var processDeletedServiceTypesForAccountsProvider =
                TestContext.ServiceProvider.GetRequiredService<IProcessDeletedServiceTypesForAccountsProvider>();

            #region CreateService
            var prepareServiceModel = serviceTypeHelper.CreateService(TestContext);
            #endregion

            #region CreateAccounts
            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = prepareServiceModel.ServcieId
                    }
                });
            createAccountCommand.Run();

            var createSecondAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = prepareServiceModel.ServcieId
                    }
                });
            createSecondAccountCommand.Run();

            var serviceCostForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, prepareServiceModel.ServcieId)
                    .Cost;

            var serviceCostForSecondAccount =
                GetResourcesConfigurationOrThrowException(createSecondAccountCommand.AccountId, prepareServiceModel.ServcieId)
                    .Cost;

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var result = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountCommand.AccountId, createAccountCommand.AccountAdminId);
            Assert.IsFalse(result.Error);
            result = rent1CConfigurationAccessManager.TurnOnRent1C(createSecondAccountCommand.AccountId, createSecondAccountCommand.AccountAdminId);
            Assert.IsFalse(result.Error);
            #endregion

            #region RemoveServiceType
            serviceTypeHelper.RemoveServiceType(prepareServiceModel);
            #endregion

            #region Moderation
            var editServiceRequest = serviceTypeHelper.ProcessModerationResult(TestContext, prepareServiceModel);
            #endregion

            #region ProcessDeleted
            processDeletedServiceTypesForAccountsProvider.Process(new ProcessDeletedServiceTypesForAccountsDto
            {
                ServiceId = prepareServiceModel.ServcieId,
                BillingServiceChangesId = editServiceRequest.BillingServiceChangesId.Value,
                ServiceTypeIds = [prepareServiceModel.DeletedServiceTypeId]
            });
            #endregion

            #region CheckProcessDeleted
            var resConfForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, prepareServiceModel.ServcieId);

            var resConfForSecondAccount =
                GetResourcesConfigurationOrThrowException(createSecondAccountCommand.AccountId, prepareServiceModel.ServcieId);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == prepareServiceModel.ServcieId);

            Assert.IsNotNull(service);
            Assert.AreEqual(resConfForFirstAccount.Cost, serviceCostForFirstAccount);
            Assert.AreEqual(resConfForSecondAccount.Cost, serviceCostForSecondAccount);

            Assert.AreEqual(
                DbLayer.BillingServiceTypeActivityForAccountRepository.FirstOrDefault().BillingServiceTypeId,
                prepareServiceModel.DeletedServiceTypeId);

            Assert.IsTrue(
                DbLayer.ResourceRepository
                    .WhereLazy(r => r.BillingServiceTypeId == prepareServiceModel.DeletedServiceTypeId).Any());

            Assert.IsTrue(editServiceRequest.BillingService.BillingServiceTypes.Any(st =>
                st.IsDeleted && st.Key == prepareServiceModel.DeletedServiceTypeId));

            Assert.IsTrue(DbLayer.BillingServiceTypeRelationRepository.Any(st =>
                st.IsDeleted && st.MainServiceTypeId == prepareServiceModel.DeletedServiceTypeId));
            #endregion

            #region PrepareDataForProlong
            var serviceTypeActivityForFirstAccount =
                DbLayer.BillingServiceTypeActivityForAccountRepository.FirstOrDefault(st =>
                    st.BillingServiceTypeId == prepareServiceModel.DeletedServiceTypeId &&
                    st.AccountId == createAccountCommand.AccountId);

            var resConfRent1CForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            serviceTypeActivityForFirstAccount.AvailabilityDateTime = DateTime.Now.AddDays(-1);
            resConfRent1CForFirstAccount.ExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.BillingServiceTypeActivityForAccountRepository.Update(serviceTypeActivityForFirstAccount);
            DbLayer.ResourceConfigurationRepository.Update(resConfRent1CForFirstAccount);
            DbLayer.Save();

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = prepareServiceModel.ServcieId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = resConfRent1CForFirstAccount.Cost + resConfForFirstAccount.Cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            #endregion

            #region ProlongAndCheckData
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();

            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);

            resConfRent1CForFirstAccount =
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            Assert.IsFalse(resConfRent1CForFirstAccount.FrozenValue);

            Assert.IsFalse(DbLayer.ResourceRepository.Any(r =>
                r.BillingServiceTypeId == prepareServiceModel.DeletedServiceTypeId &&
                r.AccountId == createAccountCommand.AccountId));

            Assert.AreNotEqual(
                GetResourcesConfigurationOrThrowException(createAccountCommand.AccountId,
                    prepareServiceModel.ServcieId).Cost, serviceCostForFirstAccount);

            serviceTypeActivityForFirstAccount =
                DbLayer.BillingServiceTypeActivityForAccountRepository.FirstOrDefault(st =>
                    st.BillingServiceTypeId == prepareServiceModel.DeletedServiceTypeId &&
                    st.AccountId == createAccountCommand.AccountId);

            Assert.IsNull(serviceTypeActivityForFirstAccount);
            #endregion
        }
    }
}
