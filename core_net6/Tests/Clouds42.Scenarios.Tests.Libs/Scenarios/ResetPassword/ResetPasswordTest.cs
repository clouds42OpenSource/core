﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.AccountUser;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResetPassword
{
    /// <summary>
    /// Тест сброса пароля для пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Устанавливаем пароль, и вызываем функцию восстановления пароля
    ///         3. Проверяем, что токен создан и отличается от предыдущего
    ///         4. Устанавливаем пароль
    ///         5. Проверяем, что пароль был установлен и токен поменялся
    /// </summary>
    public class ResetPasswordTest : ScenarioBase
    {
        private readonly AccountUserResetPasswordNotificationManager _accountUsersNotificationManager;
        private readonly AccountUserManager _accountUserManager;
        private readonly AccountUsersProfileManager _accountUsersProfileManager;

        public ResetPasswordTest()
        {
            _accountUserManager = TestContext.ServiceProvider.GetRequiredService<AccountUserManager>();
            _accountUsersNotificationManager = TestContext.ServiceProvider.GetRequiredService<AccountUserResetPasswordNotificationManager>();
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {
            var oldPassword = "Password_001";
            var newPassword = "Qwerty_123";
            
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Password = oldPassword,
            });
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            var oldResetCode = accountUser.ResetCode;
            Assert.IsNotNull(accountUser);
            Assert.IsNotNull(oldResetCode);

            //вызов сброса пароля и отправка письма
             _accountUsersNotificationManager.SendPasswordChangeRequest(accountUser.Email);
            var resetCode = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId).ResetCode;
            Assert.IsNotNull(resetCode);
            Assert.AreNotEqual(oldResetCode, resetCode);

            //сброс пароля на новый
            _accountUsersProfileManager.SetNewUserPassword(accountUser.Login, newPassword);
 
            //проверка что пароль изменен
            var checkCurrentPasswordAfterChanging =
                _accountUserManager.CheckCurrentPassword(createAccountCommand.AccountAdminId, newPassword);
            Assert.IsFalse(checkCurrentPasswordAfterChanging.Error);
            Assert.IsTrue(checkCurrentPasswordAfterChanging.Result);

            //проверка что токен поменялся
            var newResetCode = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId).ResetCode;
            Assert.IsNull(newResetCode);
            Assert.AreNotEqual(newResetCode, resetCode);
        }
    }
}
