﻿using System;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResetPassword
{
    /// <summary>
    /// Тест установки логина для пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт, пытаемся установить для админа невалидный логин
    ///         2. Проверяем, что логин не был установлен
    ///         3. Устанавливаем валидный логин
    ///         4. ПРоверяем, что логин установлен
    /// </summary>
    public class SetLoginTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;

        public SetLoginTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {
            var wrongLogin = "@MyNewLogin)";
            var newLogin = $"MyNewLogin{DateTime.Now:ddmmssfff}";

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUsersLoginModel = new AccountUsersLoginModelDto
            {
                AccountUserID = createAccountCommand.AccountAdminId,
                Login = wrongLogin
            };
            var setLogin = _accountUsersProfileManager.SetLogin(accountUsersLoginModel);
            Assert.IsTrue(setLogin.Error);

            accountUsersLoginModel.Login = newLogin;

            setLogin = _accountUsersProfileManager.SetLogin(accountUsersLoginModel);
            Assert.IsFalse(setLogin.Error);
            
            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);

            Assert.IsNotNull(accountUser);
            Assert.AreEqual(newLogin, accountUser.Login);
        }
    }
}
