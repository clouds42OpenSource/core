﻿using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResetPassword
{
    /// <summary>
    /// Тест установуи пароля для пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Устанавливаем пароль, не удовлеворяющий условиям
    ///         3. Проверяем, что пароль не изменился
    ///         4. Устанавливаем пароль
    ///         5. Проверяем, что пароль был установлен
    /// </summary>
    public class SetPasswordTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly AccountUserManager _accountUserManager;

        public SetPasswordTest()
        {
            _accountUserManager = TestContext.ServiceProvider.GetRequiredService<AccountUserManager>();
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {
            var wrongPassword = "123456";
            var oldPassword = "Password_001";
            var newPassword = "Qwerty_123";
            
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Password = oldPassword
            });
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(accountUser);

            var changingModel = new PasswordChangingModelDto
            {
                AccountUserID = accountUser.Id,
                NewPassword = wrongPassword,
                OldPassword = oldPassword
            };

            _accountUsersProfileManager.SetPassword(changingModel);
            var checkCurrentPassword = _accountUserManager.CheckCurrentPassword(accountUser.Id, oldPassword);
            Assert.IsFalse(checkCurrentPassword.Error);
            Assert.IsTrue(checkCurrentPassword.Result);

            changingModel.NewPassword = newPassword;

            var setPassword = _accountUsersProfileManager.SetPassword(changingModel);
            Assert.IsFalse(setPassword.Error);

            var checkCurrentPasswordAfterChanging =
                _accountUserManager.CheckCurrentPassword(createAccountCommand.AccountAdminId, newPassword);
            Assert.IsFalse(checkCurrentPasswordAfterChanging.Error);
            Assert.IsTrue(checkCurrentPasswordAfterChanging.Result);
        }
    }
}
