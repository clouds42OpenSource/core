﻿using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ResetPassword
{
    /// <summary>
    /// Тест установки нового пароля для пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Устанавливаем новый пароль для адина аккаунта
    ///         3. ПРоверяем, что пароль был изменен
    /// </summary>
    public class SetNewPasswordTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly AccountUserManager _accountUserManager;

        public SetNewPasswordTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _accountUserManager = TestContext.ServiceProvider.GetRequiredService<AccountUserManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(accountUser);

            var userNewPassword = "Qwerty_123";

            _accountUsersProfileManager.SetNewUserPassword(accountUser.Login, userNewPassword);

            var checkPassword = _accountUserManager.CheckCurrentPassword(accountUser.Id, userNewPassword);
            Assert.IsFalse(checkPassword.Error);
            Assert.IsTrue(checkPassword.Result);
        }
    }
}
