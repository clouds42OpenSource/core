﻿using System;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Business;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Models.Transport;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt.Providers
{
    /// <summary>
    ///     Эмулятор запрос-ответов ChekOnline кассы
    /// </summary>
    public class ChequeOnlineRequestProviderTest : IChequeOnlineRequestProvider
    {
        private ChekOnlineRequestTestMode _testMode;

        public ChequeOnlineRequestProviderTest SetTestMode(ChekOnlineRequestTestMode testMode)
        {
            _testMode = testMode;
            return this;
        }

        public ChequeOnlineResponseTransport SendRequest(ChequeOnlineAuth authData, ChequeOnlineRequestTransport requestData)
        {
            return _testMode switch
            {
                ChekOnlineRequestTestMode.AnswerSuccess => AnswerSuccess(),
                ChekOnlineRequestTestMode.AnswerNull => AnswerNull(),
                ChekOnlineRequestTestMode.AnswerErrorCode => AnswerErrorCode(),
                ChekOnlineRequestTestMode.AnswerPathNull => AnswerPathNull(),
                ChekOnlineRequestTestMode.AnswerQrCodeNull => AnswerQrCodeNull(),
                ChekOnlineRequestTestMode.AnswerReceiptTextNull => AnswerReceiptTextNull(),
                ChekOnlineRequestTestMode.AnswerResponseGlobalNull => AnswerResponseGlobalNull(),
                ChekOnlineRequestTestMode.AnswerResponseArrayNull => AnswerResponseArrayNull(),
                ChekOnlineRequestTestMode.AnswerError => AnswerError(),
                _ => null
            };
        }

        private ChequeOnlineResponseTransport AnswerSuccess()
        {
            return new ChequeOnlineResponseTransport
            {
                DocNumber = 4222555,
                Path = "/fr/api/v2/BaseDocument",
                DocumentType = 1,
                ClientId = "NULL",
                Date = new ChequeOnlineResponseTransportDateTime
                {
                    Date = new ChequeOnlineResponseTransportDate
                    {
                        Year = DateTime.Now.Year,
                        Month = DateTime.Now.Month,
                        Day = DateTime.Now.Day
                    },
                    Time = new ChequeOnlineResponseTransportTime
                    {
                        Hour = DateTime.Now.Hour,
                        Minute = DateTime.Now.Minute,
                        Second = DateTime.Now.Second
                    }
                },
                Device = new ChequeOnlineResponseTransportDevice
                {
                    Name = "UNIT_TEST",
                    Address = "42clouds.com"
                },
                DeviceRegistrationNumber = "4222500",
                DeviceSerialNumber = "4222501",
                FiscalDocNumber = 4222502,
                FiscalSign = 4222503,
                FnSerialNumber = "4222504",
                GrandTotal = 100,
                QrCode = "https://betacp.42clouds.com/BillingAccounts/Main",
                RequestId = "",
                Response = new ChequeOnlineResponseTransportResponse
                {
                    Error = 0
                },
                Responses =
                [
                    new ChequeOnlineResponseTransportResponses
                    {
                        Path = "/fr/api/v2/CloseDocument",
                        Response = new ChequeOnlineResponseTransportResponse
                        {
                            Error = 0,
                            ReceiptFullText = "Тестовый чек"
                        }
                    },
                    new ChequeOnlineResponseTransportResponses
                    {
                        Path = "/fr/api/v2/OpenDocument"
                    }
                ]
            };
        }

        private ChequeOnlineResponseTransport AnswerNull()
        {
            return null;
        }

        private ChequeOnlineResponseTransport AnswerErrorCode()
        {
            return new ChequeOnlineResponseTransport
            {
                Response = new ChequeOnlineResponseTransportResponse
                {
                    Error = 300,
                    ErrorMessages = ["Fake error"]
                }
            };
        }

        private ChequeOnlineResponseTransport AnswerError()
        {
            return new ChequeOnlineResponseTransport { RequestError = "Fake error request" };
        }

        private ChequeOnlineResponseTransport AnswerPathNull()
        {
            return new ChequeOnlineResponseTransport
            {
                Response = new ChequeOnlineResponseTransportResponse { Error = 0 },
                Responses =
                [
                    new ChequeOnlineResponseTransportResponses
                {
                    Path = "fake_path"
                }
                ]
            };
        }

        private ChequeOnlineResponseTransport AnswerQrCodeNull()
        {
            return new ChequeOnlineResponseTransport
            {
                Response = new ChequeOnlineResponseTransportResponse { Error = 0 },
                Responses =
                [
                    new ChequeOnlineResponseTransportResponses
                    {
                        Path = "/fr/api/v2/CloseDocument",
                        Response = new ChequeOnlineResponseTransportResponse
                        {
                            ReceiptFullText = "Fake_receipt"
                        }
                    }
                ],
                QrCode = null
            };
        }

        private ChequeOnlineResponseTransport AnswerReceiptTextNull()
        {
            return new ChequeOnlineResponseTransport
            {
                Response = new ChequeOnlineResponseTransportResponse { Error = 0 },
                Responses =
                [
                    new ChequeOnlineResponseTransportResponses
                    {
                        Path = "/fr/api/v2/CloseDocument",
                        Response = new ChequeOnlineResponseTransportResponse
                        {
                            ReceiptFullText = null
                        }
                    }
                ]
            };
        }

        private ChequeOnlineResponseTransport AnswerResponseGlobalNull()
        {
            return new ChequeOnlineResponseTransport { Response = null };
        }

        private ChequeOnlineResponseTransport AnswerResponseArrayNull()
        {
            return new ChequeOnlineResponseTransport
            {
                Response = new ChequeOnlineResponseTransportResponse { Error = 0 },
                Responses =
                [
                    new ChequeOnlineResponseTransportResponses
                {
                    Path = "/fr/api/v2/CloseDocument",
                    Response = null
                }
                ]
            };
        }
    }
}
