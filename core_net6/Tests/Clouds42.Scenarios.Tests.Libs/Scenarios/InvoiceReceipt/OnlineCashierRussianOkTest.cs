﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt.Providers;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Factories.InvoiceReceiptFactoryData.ReceiptBuilders;
using Clouds42.Common.ManagersResults;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt
{
    /// <summary>
    ///     Тест для онлайн кассира (Россия) [ Тест на успешный ответ сервиса ]
    /// </summary>
    public class OnlineCashierRussianOkTest : ScenarioBase
    {
        public override void Run()
        {            

            var invoiceCommand = new CreateAccountAndInvoiceCommand(TestContext);
            invoiceCommand.Run();
            Services.AddTransient<IInvoiceReceiptProvider, InvoiceReceiptProvider>();
            Services.AddTransient<IInvoiceReceiptBuilder, RussiaReceiptBuilder>();
            Services.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            TestBySuccess(invoiceCommand);
        }

        /// <summary>
        ///     Получить результат создания фискального чека
        /// </summary>
        private ManagerResult<bool> GetFiscalReceiptResult(IServiceProvider serviceProvider, IInvoiceDetails invoiceCommand) 
            => serviceProvider.GetRequiredService<PaymentReceiptManager>().CreateFiscalReceipt(invoiceCommand.InvoiceId);

        /// <summary>
        ///     Проверить как отработает при успешном ответе
        /// </summary>
        private void TestBySuccess(IInvoiceDetails invoiceCommand)
        {
            Services.AddTransient<IChequeOnlineRequestProvider, ChequeOnlineRequestProviderTest>();
            var sp = Services.BuildServiceProvider();
            sp.GetRequiredService<ChequeOnlineRequestProviderTest>().SetTestMode(ChekOnlineRequestTestMode.AnswerSuccess);
            var result = GetFiscalReceiptResult(sp, invoiceCommand);
            
            Assert.IsTrue(result.Result, result.Message);

            if (result.Error)
                throw new InvalidOperationException(result.Message);

            var invoiceReceipt = DbLayer.InvoiceReceiptRepository.FirstOrDefault(w => w.InvoiceId == invoiceCommand.InvoiceId);
            if (invoiceReceipt == null)
                throw new InvalidOperationException("Фискальный чек не был создан при успешном ответе от сервиса");

            if (invoiceReceipt.DocumentDateTime == null)
                throw new InvalidOperationException("Не обнаружен DocumentDateTime");

            if (string.IsNullOrEmpty(invoiceReceipt.FiscalNumber))
                throw new InvalidOperationException("Не обнаружен FiscalNumber");

            if (string.IsNullOrEmpty(invoiceReceipt.FiscalSerial))
                throw new InvalidOperationException("Не обнаружен FiscalSerial");

            if (string.IsNullOrEmpty(invoiceReceipt.FiscalSign))
                throw new InvalidOperationException("Не обнаружен FiscalSign");

            if (string.IsNullOrEmpty(invoiceReceipt.QrCodeData))
                throw new InvalidOperationException("Не обнаружен QrCodeData");

            if (string.IsNullOrEmpty(invoiceReceipt.QrCodeFormat))
                throw new InvalidOperationException("Не обнаружен QrCodeFormat");

            if (string.IsNullOrEmpty(invoiceReceipt.ReceiptInfo))
                throw new InvalidOperationException("Не обнаружен ReceiptInfo");
        }
    }
}
