﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt.Providers;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt
{
    /// <summary>
    ///     Тест для онлайн кассира (Россия) [ Тест на неуспешные ответы сервиса ]
    /// </summary>
    public class OnlineCashierRussianFailTest : ScenarioBase
    {
        public override void Run()
        {            

            var invoiceCommand = new CreateAccountAndInvoiceCommand(TestContext);
            invoiceCommand.Run();

            Services.AddTransient<IInvoiceReceiptProvider>();
            Services.AddTransient<IInvoiceReceiptBuilder>();
            Services.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            Services.AddTransient<IAccessProvider>();
            Services.AddTransient<IUnitOfWork>();

            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerNull);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerError);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerResponseGlobalNull);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerResponseArrayNull);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerErrorCode);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerPathNull);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerQrCodeNull);
            TestByError(invoiceCommand, ChekOnlineRequestTestMode.AnswerReceiptTextNull);
        }

        /// <summary>
        ///     Получить результат создания фискального чека
        /// </summary>
        private ManagerResult<bool> GetFiscalReceiptResult(IInvoiceDetails invoiceCommand) 
            => ServiceProvider.GetRequiredService<PaymentReceiptManager>().CreateFiscalReceipt(invoiceCommand.InvoiceId);

        /// <summary>
        ///     Проверить как отработает тест при неуспешном ответе от сервиса
        /// </summary>
        private void TestByError(IInvoiceDetails invoiceCommand, ChekOnlineRequestTestMode testMode)
        {
            Services.AddTransient<IChequeOnlineRequestProvider, ChequeOnlineRequestProviderTest>();

            var result = GetFiscalReceiptResult(invoiceCommand);

            Assert.IsFalse(result.Result, result.Message);

            var invoiceReceipt = DbLayer.InvoiceReceiptRepository.FirstOrDefault(w => w.InvoiceId == invoiceCommand.InvoiceId);
            if (invoiceReceipt != null)
                throw new InvalidOperationException("Фискальный чек был создан, это неверно при неуспешном ответе от сервиса");

            if (!result.Error)
                throw new InvalidOperationException(result.Message);
        }
    }
}
