﻿namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt
{
    public enum ChekOnlineRequestTestMode
    {
        AnswerSuccess,
        AnswerNull,
        AnswerError,
        AnswerErrorCode,
        AnswerPathNull,
        AnswerQrCodeNull,
        AnswerReceiptTextNull,
        AnswerResponseGlobalNull,
        AnswerResponseArrayNull
    }
}