﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt.Providers;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.Billing.Factories.InvoiceReceiptFactoryData.ReceiptBuilders;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt
{
    /// <summary>
    /// Сценарий теста проверки создания повторяющегося фиксального чека
    /// </summary>
    public class ScenarioCheckCreateRepetitiveFiscalReceiptTest : ScenarioBase
    {
        public override void Run()
        {
            var invoiceCommand = new CreateAccountAndInvoiceCommand(TestContext);
            invoiceCommand.Run();

            Services.AddTransient<IInvoiceReceiptProvider, InvoiceReceiptProvider>();
            Services.AddTransient<IInvoiceReceiptBuilder, RussiaReceiptBuilder>();
            Services.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            Services.AddTransient<IChequeOnlineRequestProvider, ChequeOnlineRequestProviderTest>();
            var sp = Services.BuildServiceProvider();
            sp.GetRequiredService<ChequeOnlineRequestProviderTest>().SetTestMode(ChekOnlineRequestTestMode.AnswerSuccess);
            var paymentReceiptManager = sp.GetRequiredService<PaymentReceiptManager>();

            var result = paymentReceiptManager.CreateFiscalReceipt(invoiceCommand.InvoiceId);
            Assert.IsFalse(result.Error);

            paymentReceiptManager.CreateFiscalReceipt(invoiceCommand.InvoiceId);
            Assert.IsFalse(result.Error);
        }
    }
}
