﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt.Providers;
using Clouds42.WebKit;
using Clouds42.WebKit.RequestProviders;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Billing.Contracts.Factories;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.OnlineCashier.Builders.ChequeOnline.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.InvoiceReceipt
{
    /// <summary>
    ///     Тест для онлайн кассира (Россия) [ Тест на генерацию PDF документа ]
    /// </summary>
    public class ReceiptDocumentBuilderRussianTest : ScenarioBase
    {
        public override void Run()
        {            

            var invoiceCommand = new CreateAccountAndInvoiceCommand(TestContext);
            invoiceCommand.Run();

            var invoiceReceiptCommand = new CreateInvoiceReceiptCommand(TestContext, invoiceCommand.InvoiceId);
            invoiceReceiptCommand.Run();

            Services.AddTransient<IInvoiceReceiptProvider>();
            Services.AddTransient<IInvoiceReceiptBuilder>();
            Services.AddTransient<IHttpWebRequestProvider, HttpWebRequestProvider>();
            Services.AddTransient<IChequeOnlineRequestProvider, ChequeOnlineRequestProviderTest>();
            Services.AddTransient<IAccessProvider>();
            Services.AddTransient<IUnitOfWork>();

            var paymentReceiptManager = ServiceProvider.GetRequiredService<PaymentReceiptManager>();

            var builderResult = paymentReceiptManager.BuildInvoiceReceiptPdfDocument(invoiceCommand.InvoiceId);

            if (!builderResult.Bytes.Any())
                throw new InvalidOperationException("Данные PDF файла пустые!");

            if (string.IsNullOrEmpty(builderResult.FileName))
                throw new InvalidOperationException("Имя файла PDF отсутсвует!");
        }
    }
}
