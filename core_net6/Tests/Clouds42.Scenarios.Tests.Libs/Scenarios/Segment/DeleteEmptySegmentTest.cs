﻿using Clouds42.Segment.CloudsServicesSegment.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    //Удаление пустого сегмента
    public class DeleteEmptySegmentTest : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public DeleteEmptySegmentTest()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var segmentByAccount = GetAccountSegment(createAccountCommand.AccountId);
            var deleteSegment = _cloudSegmentReferenceManager.DeleteSegment(segmentByAccount.ID);
            Assert.IsTrue(deleteSegment.Error, "Удаление сегмента должно завершиться ошибкой");

            var deleteSegmentSecond = _cloudSegmentReferenceManager.DeleteSegment(createSegmentCommand.Id);
            Assert.IsFalse(deleteSegmentSecond.Error, deleteSegmentSecond.Message);

            var segment =
                DbLayer.CloudServicesSegmentRepository.FirstOrDefault(css => css.ID == createSegmentCommand.Id);
            Assert.IsNull(segment, "Сегмент должен быть удален");
        }
    }
}