﻿using System.Threading.Tasks;
using Clouds42.AccountDatabase.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    ///     Тест на проверку смены альфа версии
    /// </summary>
    public class ChangeAlphaPlatformVersionTest : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public ChangeAlphaPlatformVersionTest()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
           
        }

        public override async Task RunAsync()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var createPlatform = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatform.Run();

            var accDbEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();

            var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createAccAndDb.AccountDetails.Segment.Id });

            Assert.IsNotNull(segmentManagerResult);
            Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

            var segmentViewModel = segmentManagerResult.Result;

            var existedDatabase = await
                DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

            Assert.IsNotNull(existedDatabase);
            Assert.AreEqual(PublishState.Unpublished, existedDatabase.PublishStateEnum);
            Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);

            var managerResult = _cloudSegmentReferenceManager.EditSegment(new EditCloudServicesSegmentDto
            {
                FileStorageServersID = segmentViewModel.BaseData.FileStorageServersId,
                Name = segmentViewModel.BaseData.Name,
                ContentServerID = segmentViewModel.BaseData.ContentServerId,
                Description = segmentViewModel.BaseData.Description,
                BackupStorageID = segmentViewModel.BaseData.BackupStorageId,
                GatewayTerminalsID = segmentViewModel.BaseData.GatewayTerminalsId,
                SQLServerID = segmentViewModel.BaseData.SqlServerId,
                EnterpriseServer82ID = segmentViewModel.BaseData.EnterpriseServer82Id,
                ServicesTerminalFarmID = segmentViewModel.BaseData.ServicesTerminalFarmId,
                EnterpriseServer83ID = segmentViewModel.BaseData.EnterpriseServer83Id,
                DefaultSegmentStorageId = segmentViewModel.BaseData.DefaultSegmentStorageId,
                Stable82VersionId = segmentViewModel.BaseData.Stable82VersionId,
                Stable83VersionId = segmentViewModel.BaseData.Stable83VersionId,
                Alpha83VersionId = createPlatform.Version,
                CustomFileStoragePath = segmentViewModel.BaseData.CustomFileStoragePath,
                DelimiterDatabaseMustUseWebService = segmentViewModel.BaseData.DelimiterDatabaseMustUseWebService,
                Id = segmentViewModel.Id
            });

            Assert.IsNotNull(managerResult);
            Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании сегмента. Текст ошибки: {managerResult.Message}");

            var platformResult = accDbEditManager.ChangeAccountDatabasePlatformType(createAccAndDb.AccountDatabaseId, PlatformType.V83,
                DistributionType.Alpha);

            Assert.IsNotNull(platformResult);
            Assert.IsFalse(platformResult.Error,
                $"При смене дистрибутива у базы произошла ошибка. Текст ошибки: {platformResult.Message}");

            var existedSegment = await
                DbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.ID == segmentViewModel.Id);

            existedDatabase = await
                DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

            Assert.IsNotNull(existedSegment);
            Assert.IsNotNull(existedDatabase);
            Assert.IsNotNull(existedSegment.Alpha83PlatformVersionReference);
            Assert.AreEqual(createPlatform.Version, existedSegment.Alpha83PlatformVersionReference.Version);
            Assert.AreEqual(PublishState.Unpublished, existedDatabase.PublishStateEnum);
            Assert.AreEqual(DistributionType.Alpha, existedDatabase.DistributionTypeEnum);

            segmentViewModel.BaseData.Alpha83VersionId = null;

            managerResult = _cloudSegmentReferenceManager.EditSegment(new EditCloudServicesSegmentDto
            {
                FileStorageServersID = segmentViewModel.BaseData.FileStorageServersId,
                Name = segmentViewModel.BaseData.Name,
                ContentServerID = segmentViewModel.BaseData.ContentServerId,
                Description = segmentViewModel.BaseData.Description,
                BackupStorageID = segmentViewModel.BaseData.BackupStorageId,
                GatewayTerminalsID = segmentViewModel.BaseData.GatewayTerminalsId,
                SQLServerID = segmentViewModel.BaseData.SqlServerId,
                EnterpriseServer82ID = segmentViewModel.BaseData.EnterpriseServer82Id,
                ServicesTerminalFarmID = segmentViewModel.BaseData.ServicesTerminalFarmId,
                EnterpriseServer83ID = segmentViewModel.BaseData.EnterpriseServer83Id,
                DefaultSegmentStorageId = segmentViewModel.BaseData.DefaultSegmentStorageId,
                Stable82VersionId = segmentViewModel.BaseData.Stable82VersionId,
                Stable83VersionId = segmentViewModel.BaseData.Stable83VersionId,
                Alpha83VersionId = segmentViewModel.BaseData.Alpha83VersionId,
                CustomFileStoragePath = segmentViewModel.BaseData.CustomFileStoragePath,
                DelimiterDatabaseMustUseWebService = segmentViewModel.BaseData.DelimiterDatabaseMustUseWebService,
                Id = segmentViewModel.Id
            });

            Assert.IsNotNull(managerResult);
            Assert.IsTrue(managerResult.Error);
        }
    }
}
