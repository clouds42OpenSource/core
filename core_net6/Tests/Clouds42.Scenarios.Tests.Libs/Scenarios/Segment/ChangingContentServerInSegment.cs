﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Core42.Application.Features.SegmentContext.Queries;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    /// Тест на проверку смены сервера публикации в сегменте.
    /// Сценарий: создаём аккаунт с базой данных на разделителях,меняем сервер публикации
    /// на сегменте аккаунта. Потом добавляем файловую базу , публикуем её. Ещё
    /// раз меняем сервер публикации.
    /// Проверяем чтобы во втором случае вернулась ошибка.
    /// </summary>
    public class ChangingContentServerInSegment : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public ChangingContentServerInSegment()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext, dbOnDelimiter: true);
            createAccountDatabaseAndAccountCommand.Run();

            var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createAccountDatabaseAndAccountCommand.AccountDetails.Segment.Id });
            Assert.AreEqual(false, segmentManagerResult.Error, "Ошибка получания сегмента");
            var segmentViewModel = segmentManagerResult.Result;

            var contentServers = segmentViewModel.AdditionalData.ContentServers.ToList();
            foreach (var contentServer in contentServers.Where(contentServer => segmentViewModel.BaseData.ContentServerId != contentServer.Key))
            {
                segmentViewModel.BaseData.ContentServerId = contentServer.Key;
                break;
            }

            var managerResult = _cloudSegmentReferenceManager.EditSegment(segmentViewModel.MapToEditCloudServicesSegmentDto());

            Assert.AreEqual(false, managerResult.Error, "Смена сервера публикации должна пройти при налиции лишь БД на разделителях");
            Assert.AreEqual(managerResult.State, ManagerResultState.Ok, "Неверный статус результата операции");

            var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountDatabaseAndAccountCommand.AccountDetails);
            createAccountDatabaseCommand.Run();
            var model = new PublishDto
            {
                AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
                AccountUsersId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountAdminId
            };

            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            publishManager.PublishDatabase(model);

            foreach (var contentServer in contentServers.Where(contentServer => segmentViewModel.BaseData.ContentServerId != contentServer.Key))
            {
                segmentViewModel.BaseData.ContentServerId = contentServer.Key;
                break;
            }

            managerResult = _cloudSegmentReferenceManager.EditSegment(segmentViewModel.MapToEditCloudServicesSegmentDto());

            Assert.AreEqual(true, managerResult.Error,
                "Смена сервера публикации должна выдать ошибку при налиции опубликованых файловых баз");
            Assert.AreEqual(managerResult.State, ManagerResultState.PreconditionFailed, "Неверный статус результата операции");
        }
    }
}
