﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    ///     Тест смены версий платформы в сегменте
    /// </summary>
    public class ChangePlatformVersionsTest : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public ChangePlatformVersionsTest()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
        }

        public override async Task RunAsync()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var new83Version = DbLayer.PlatformVersionReferencesRepository
                .AsQueryableNoTracking()
                .Where(x => x.Version.Contains("8.3") && x.PathToPlatfromX64 != null)
                .OrderByDescending(x => x.Version)
                .FirstOrDefault();

            var new82Version = DbLayer.PlatformVersionReferencesRepository
                .AsQueryableNoTracking()
                .Where(x => x.Version.Contains("8.2"))
                .OrderByDescending(x => x.Version)
                .FirstOrDefault();

            var segmentManagerResult = await
                Mediator.Send(new GetSegmentByIdQuery { Id = createAccAndDb.AccountDetails.Segment.Id });

            Assert.IsNotNull(segmentManagerResult);
            Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

            var segmentViewModel = segmentManagerResult.Result;

            var managerResult = _cloudSegmentReferenceManager.EditSegment(new EditCloudServicesSegmentDto
            {
                FileStorageServersID = segmentViewModel.BaseData.FileStorageServersId,
                Name = segmentViewModel.BaseData.Name,
                ContentServerID = segmentViewModel.BaseData.ContentServerId,
                Description = segmentViewModel.BaseData.Description,
                BackupStorageID = segmentViewModel.BaseData.BackupStorageId,
                GatewayTerminalsID = segmentViewModel.BaseData.GatewayTerminalsId,
                SQLServerID = segmentViewModel.BaseData.SqlServerId,
                EnterpriseServer82ID = segmentViewModel.BaseData.EnterpriseServer82Id,
                ServicesTerminalFarmID = segmentViewModel.BaseData.ServicesTerminalFarmId,
                EnterpriseServer83ID = segmentViewModel.BaseData.EnterpriseServer83Id,
                DefaultSegmentStorageId = segmentViewModel.BaseData.DefaultSegmentStorageId,
                Stable82VersionId = new82Version.Version,
                Stable83VersionId = new83Version.Version,
                Alpha83VersionId = new83Version.Version,
                CustomFileStoragePath = segmentViewModel.BaseData.CustomFileStoragePath,
                DelimiterDatabaseMustUseWebService = segmentViewModel.BaseData.DelimiterDatabaseMustUseWebService,
                Id = segmentViewModel.Id
            });

            Assert.IsNotNull(managerResult);
            Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании сегмента. Текст ошибки: {managerResult.Message}");


            var existedSegment = DbLayer.CloudServicesSegmentRepository.GetSegment(segmentViewModel.Id);

            Assert.IsNotNull(existedSegment);
            Assert.IsNotNull(existedSegment.Alpha83PlatformVersionReference);
            Assert.IsNotNull(existedSegment.Stable82PlatformVersionReference);
            Assert.IsNotNull(existedSegment.Stable83PlatformVersionReference);
            Assert.AreEqual(new83Version.Version, existedSegment.Alpha83Version);
            Assert.AreEqual(new82Version.Version, existedSegment.Stable82Version);
            Assert.AreEqual(new83Version.Version, existedSegment.Stable83Version);
        }
    }
}
