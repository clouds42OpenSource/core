﻿using System.IO;
using System.Threading.Tasks;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Publishes.Providers;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    ///     Тест на переопубликацию списка баз в сегменте
    /// при смене версии платформы
    /// </summary>
    public class RepublishSegmentDatabasesTest : ScenarioBase
    {
        private IRepublishSegmentDatabasesProvider _republishSegmentDatabasesProvider;
        private CloudSegmentReferenceManager _cloudSegmentReferenceManager;

        public override void Run()
        {
           
        }

        public override async Task RunAsync()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var createPlatform = new CreatePlatformVersionReferenceCommand(TestContext);
            createPlatform.Run();

            await UpdateCloudContentServerNodes();
            _republishSegmentDatabasesProvider = ServiceProvider.GetRequiredService<IRepublishSegmentDatabasesProvider>();
            _cloudSegmentReferenceManager = ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = createAccAndDb.AccountDetails.Segment.Id });

            Assert.IsNotNull(segmentManagerResult);
            Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

            var segmentViewModel = segmentManagerResult.Result;
            try
            {
                publishManager.PublishDatabase(
                        new PublishDto { AccountDatabaseId = createAccAndDb.AccountDatabaseId });

                var existedDatabase = await
                    DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

                Assert.IsNotNull(existedDatabase);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);

                segmentViewModel.BaseData.Stable83VersionId = createPlatform.Version;

                var managerResult = _cloudSegmentReferenceManager.EditSegment(segmentViewModel.MapToEditCloudServicesSegmentDto());

                _republishSegmentDatabasesProvider.RepublishSegmentDatabases(segmentViewModel.Id,
                    PlatformType.V83, DistributionType.Stable);

                Assert.IsNotNull(managerResult);
                Assert.IsFalse(managerResult.Error, $"Произошла ошибка при редактировании сегмента. Текст ошибки: {managerResult.Message}");

                RefreshDbCashContext(Context.CloudServicesSegments);
                RefreshDbCashContext(Context.AccountDatabases);

                var existedSegment = await
                    DbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.ID == segmentViewModel.Id);

                RefreshDbCashContext(Context.AccountDatabases);

                existedDatabase = await 
                    DbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == createAccAndDb.AccountDatabaseId);

                var basePath = GetPublishServerHelper.PathToPublishServer();

                var partOfPath =
                    $@"{createAccAndDb.AccountDetails.AccountId.GetEncodeGuid()}\{existedDatabase.V82Name}";

                var fullPathToFolder = Path.Combine(basePath, partOfPath).Remove(0, 2);

                Assert.IsNotNull(existedSegment);
                Assert.IsNotNull(existedDatabase);
                Assert.IsNotNull(existedSegment.Stable83PlatformVersionReference);
                Assert.AreEqual(segmentViewModel.BaseData.Stable83VersionId, existedSegment.Stable83PlatformVersionReference.Version);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(DistributionType.Stable, existedDatabase.DistributionTypeEnum);
                Assert.IsTrue(Directory.GetFiles(fullPathToFolder).Length > 0);

                var scriptProcessorValue = TestWebConfigHelper.Get1CScriptProcessorValue(fullPathToFolder);
                var expectedScriptProcessorValue = string.Format(CloudConfigurationProvider.Segment.V83ModulePath(),
                    existedSegment.Stable83PlatformVersionReference.Version);

                Assert.IsFalse(string.IsNullOrWhiteSpace(scriptProcessorValue));
                Assert.AreEqual(expectedScriptProcessorValue, scriptProcessorValue);
            }
            finally
            {
                publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
                publishManager.CancelPublishDatabaseWithWaiting(createAccAndDb.AccountDatabaseId);
            }
        }

        /// <summary>
        /// Обновить адреса нод публикаций
        /// </summary>
        private async Task UpdateCloudContentServerNodes()
        {
            var contentServerNodes = DbLayer.PublishNodeReferenceRepository.All();

            foreach (var serverNode in contentServerNodes)
            {
                serverNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
                DbLayer.PublishNodeReferenceRepository.Update(serverNode);
            }

            await DbLayer.SaveAsync();
            RefreshDbCashContext(Context.PublishNodeReference);
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDatabaseWebPublisher, DatabaseWebPublisher>();

        }
    }
}
