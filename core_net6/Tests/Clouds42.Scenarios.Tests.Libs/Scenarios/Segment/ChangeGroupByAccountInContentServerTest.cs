﻿using System;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesContentServer.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    ///     Смена флага GroupByAccount с последующей публикацией
    /// базы и проверкой генерируемого пути
    /// </summary>
    public class ChangeGroupByAccountInContentServerTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb.Run();

            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();

            var segment = DbLayer.CloudServicesSegmentRepository.GetSegment(createAccAndDb.AccountDetails.Segment.Id);

            var contentServerManager = TestContext.ServiceProvider.GetRequiredService<CloudContentServerManager>();

            var contentManagerResult = contentServerManager.GetCloudContentServerDto(segment.ContentServerID);

            Assert.IsNotNull(contentManagerResult);
            Assert.IsFalse(contentManagerResult.Error, contentManagerResult.Message);

            var contentViewModel = contentManagerResult.Result;

            Assert.IsNotNull(contentViewModel, "Не удалось преобразовать модель");

            contentViewModel.GroupByAccount = false;

            var managerResult = contentServerManager.EditCloudContentServer(contentViewModel);

            var existedContentServer =
                DbLayer.CloudServicesContentServerRepository.FirstOrDefault(x => x.ID == contentViewModel.Id);

            Assert.IsNotNull(managerResult);
            Assert.IsNotNull(existedContentServer);

            Assert.AreEqual(contentViewModel.GroupByAccount, existedContentServer.GroupByAccount);

            try
            {
                var publishResult =
                    publishManager.PublishDatabase(new PublishDto { AccountDatabaseId = createAccAndDb.AccountDatabaseId });

                var existedDatabase =
                    DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccAndDb.AccountDatabaseId);

                var expectedPublishPath = new UriBuilder
                {
                    Host = contentViewModel.PublishSiteName,
                    Path = existedDatabase.V82Name,
                    Scheme = Uri.UriSchemeHttps
                }.Uri.AbsoluteUri;

                Assert.IsNotNull(publishResult);
                Assert.IsFalse(publishResult.Error,
                    $"Произошла ошибка при публикаци базы. Текст ошибки: {publishResult.Message}");
                Assert.IsNotNull(publishResult.Result);
                Assert.AreEqual(PublishState.Published, existedDatabase.PublishStateEnum);
                Assert.AreEqual(expectedPublishPath, publishResult.Result);
            }
            finally
            {
                publishManager.CancelPublishDatabaseWithWaiting(createAccAndDb.AccountDatabaseId);
            }
        }
    }
}
