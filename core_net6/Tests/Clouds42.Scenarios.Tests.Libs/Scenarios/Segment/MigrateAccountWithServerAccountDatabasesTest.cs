﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using System.IO;
using Clouds42.AccountDatabase.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers;
using Clouds42.Accounts.SegmentMigration.Managers;
using Clouds42.DataContracts.Account;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{

    /// <summary>
    /// При миграции аккаунта в новый сегмент для серверных баз меняем платформу на Stable.
    /// План: создаём аккаунт с  ИБ. Делаем ИБ серверной. Выставляем ей платформу Alpha.
    ///         Создаём второй сегмент и переносим туда аккаунт.
    /// </summary>
    public class MigrateAccountWithServerAccountDatabasesTest : ScenarioBase
    {
        private readonly ChangeAccountSegmentManager _changeAccountSegmentManager;

        public MigrateAccountWithServerAccountDatabasesTest()
        {
            _changeAccountSegmentManager = TestContext.ServiceProvider.GetRequiredService<ChangeAccountSegmentManager>();
        }

        public override void Run()
        {
            var createAccAndDatabase = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDatabase.Run();

            var database =
                DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == createAccAndDatabase.AccountDatabaseId);
            var startSegmentId = createAccAndDatabase.AccountDetails.Segment.Id;

            var accountDatabaseEditManager = ServiceProvider.GetRequiredService<AccountDatabaseEditManager>();

            Assert.AreEqual(true, database.IsFile);
            var dbManagerResult = accountDatabaseEditManager.ChangeAccountDatabaseType
                (new ChangeAccountDatabaseTypeDto { DatabaseId = createAccAndDatabase.AccountDatabaseId });

            if (dbManagerResult.Error)
                throw new InvalidOperationException($"Метод изменения типа инфорцационной базы вернул ошибку: {dbManagerResult.Message}");

            var databaseNew = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == createAccAndDatabase.AccountDatabaseId);
            Assert.AreEqual(false, databaseNew.IsFile);

            databaseNew.DistributionType = DistributionType.Alpha.ToString();
            databaseNew.DistributionTypeEnum = DistributionType.Alpha;
            DbLayer.DatabasesRepository.Update(databaseNew);
            DbLayer.Save();
            var account = createAccAndDatabase.AccountDetails;

            RefreshDbCashContext(Context.AccountDatabases);

            var defaultStorage = Path.Combine(TestContext.TestDataPath, "TestSegmentStorage2");
            var cloudFilestorageData = new CloudFilestorageElementViewModelTest(defaultStorage)
            {
                Name = "SecondStorage",
                Description = "УдалитьПоСлеТестов"
            };

            var createSegmentCommand = new CreateSegmentCommand(TestContext, fileStorageData: cloudFilestorageData);
            createSegmentCommand.Run();

            Services.AddTransient<IDatabaseWebPublisher, DatabaseWebPublisherTest>();
            Services.AddTransient<IExecuteDatabasePublishTasksProvider, TestExecuteDatabasePublishTasksProvider>();

            var targetSegmentId = createSegmentCommand.Id;

            _changeAccountSegmentManager.ChangeAccountSegment(new ChangeAccountSegmentParamsDto
            {
                AccountId = account.AccountId,
                SegmentFromId = startSegmentId,
                SegmentToId = targetSegmentId,
                InitiatorId = account.AccountAdminId,
                MigrationStartDateTime = DateTime.Now
            });


            database = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == createAccAndDatabase.AccountDatabaseId);
            Assert.IsNotNull(database);
            Assert.AreEqual(database.DistributionType, DistributionType.Stable.ToString());
        }
    }
}
