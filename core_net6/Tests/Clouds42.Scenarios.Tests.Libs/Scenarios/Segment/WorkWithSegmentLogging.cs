﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Extensions;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Core42.Application.Features.SegmentContext.Queries;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    /// Проверка логирования изменений в сегменте
    /// </summary>
    public class WorkWithSegmentLogging : ScenarioBase
    {
        private readonly CloudSegmentReferenceManager _cloudSegmentReferenceManager;
        private const string ErrorLogEventRecordSubstring = "ошибк";

        public WorkWithSegmentLogging()
        {
            _cloudSegmentReferenceManager = TestContext.ServiceProvider.GetRequiredService<CloudSegmentReferenceManager>();
        }

        public override void Run()
        {
            
        }

        public override async Task RunAsync()
        {
            var createAccount = new CreateAccountCommand(TestContext);
            createAccount.Run();

            RefreshDbCashContext(Context.CloudServicesSegments);

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var segmentCreationLogs = await DbLayer.CloudChangesRepository
                .AsQueryableNoTracking()
                .Where(c => c.AccountId == createAccount.AccountId)
                .ToListAsync();

            if (segmentCreationLogs.All(scl => scl.CloudChangesAction.IndexEnum != LogActions.AddSegmentOrElement))
                throw new InvalidOperationException($"Все логи должны относиться к действию \"{LogActions.AddSegmentOrElement.Description()}\"");

            if (segmentCreationLogs.Any(cloudChanges => HasErrorRecord(cloudChanges.Description)))
                throw new InvalidOperationException("Все логи дожны быть успешны");

            await SegmentEditingLogs(createSegmentCommand.Id, createAccount.AccountId);
            SegmentDeletingLogs(createSegmentCommand.Id, createAccount.AccountId);
            SegmentDeletingLogs(Guid.Empty, createAccount.AccountId, false);
        }

        /// <summary>
        /// Проверка логирования при редактировании сегмента
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="accountId">Id аккаунта</param>
        private async Task SegmentEditingLogs(Guid segmentId, Guid accountId)
        {
            var segmentManagerResult = await Mediator.Send(new GetSegmentByIdQuery { Id = segmentId });

            Assert.IsFalse(segmentManagerResult.Error, segmentManagerResult.Message);

            var segmentViewModel = segmentManagerResult.Result;

            var createCloudServicesSqlServerCommand = new CreateCloudServicesSqlServerCommand(TestContext);
            createCloudServicesSqlServerCommand.Run();

            var managerResult = _cloudSegmentReferenceManager.EditSegment(new EditCloudServicesSegmentDto
            {
                FileStorageServersID = segmentViewModel.BaseData.FileStorageServersId,
                Name = segmentViewModel.BaseData.Name,
                ContentServerID = segmentViewModel.BaseData.ContentServerId,
                Description = segmentViewModel.BaseData.Description,
                BackupStorageID = segmentViewModel.BaseData.BackupStorageId,
                GatewayTerminalsID = segmentViewModel.BaseData.GatewayTerminalsId,
                SQLServerID = createCloudServicesSqlServerCommand.Id,
                EnterpriseServer82ID = segmentViewModel.BaseData.EnterpriseServer82Id,
                ServicesTerminalFarmID = segmentViewModel.BaseData.ServicesTerminalFarmId,
                EnterpriseServer83ID = segmentViewModel.BaseData.EnterpriseServer83Id,
                DefaultSegmentStorageId = segmentViewModel.BaseData.DefaultSegmentStorageId,
                Stable82VersionId = segmentViewModel.BaseData.Stable82VersionId,
                Stable83VersionId = segmentViewModel.BaseData.Stable83VersionId,
                Alpha83VersionId = segmentViewModel.BaseData.Alpha83VersionId,
                CustomFileStoragePath = segmentViewModel.BaseData.CustomFileStoragePath,
                DelimiterDatabaseMustUseWebService = segmentViewModel.BaseData.DelimiterDatabaseMustUseWebService,
                Id = segmentViewModel.Id
            });

            Assert.IsFalse(managerResult.Error, $"Ошибка редактирования сегмента {managerResult.Message}");
            
            var editLogs = GetCloudChangesByLoggingAction(accountId, LogActions.EditSegmentOrElement);

            Assert.IsNotNull(editLogs, "Не записались логи редактирования сегмента");

            if (editLogs.Any(cloudChanges => HasErrorRecord(cloudChanges.Description)))
                throw new InvalidOperationException("Все логи дожны быть успешны");
        }

        /// <summary>
        /// Проверка логирования при удалении сегмента
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="isSuccessEdit">Флаг успешное/ошибочное удаление</param>
        private void SegmentDeletingLogs(Guid segmentId, Guid accountId, bool isSuccessEdit = true)
        {
            var deleteSegment = _cloudSegmentReferenceManager.DeleteSegment(segmentId);

            if (!isSuccessEdit)
                Assert.IsTrue(deleteSegment.Error, "Удаление должно было завершиться ошибкой");
            else
                Assert.IsFalse(deleteSegment.Error, deleteSegment.Message);

            var deleteLogs = GetCloudChangesByLoggingAction(accountId, LogActions.DeleteSegmentOrElement);
            Assert.IsNotNull(deleteLogs, "Не были записаны логи удаления сегмента");
        }

        /// <summary>
        /// Получить список объектов CloudChanges по действию логирования и аккаунту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="logActions">Действие логирования</param>
        /// <returns>Список CloudChanges</returns>
        private List<CloudChanges> GetCloudChangesByLoggingAction(Guid accountId, LogActions logActions)
        {
            var logAction = DbLayer.CloudChangesActionRepository.FirstOrDefault(ac => ac.Index == (short)logActions);

            var cloudChanges = DbLayer.CloudChangesRepository
                .Where(c => c.AccountId == accountId && c.Action == logAction.Id).ToList();

            return cloudChanges;
        }

        /// <summary>
        /// Признак что описание лога содержит ошибку
        /// </summary>
        /// <param name="logEventDescription">Описание записи лога</param>
        /// <returns>true - если строка содержит подстроку <see cref="ErrorLogEventRecordSubstring"/></returns>
        private static bool HasErrorRecord(string logEventDescription)
            => logEventDescription.ToLower(CultureInfo.InvariantCulture).Contains(ErrorLogEventRecordSubstring);
    }
}
