﻿using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.Segment.CloudServicesContentServer.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Segment
{
    /// <summary>
    /// Тестовый сценарий смены GroupByAccount 
    /// с уже опубликованной базой на этом сегменте. В оригинале проверяется изменение
    /// сегмента, но сейчас это невозможно с опубликованой БД . Переделал проверку
    /// чтобы тест показывал эту невозможность.
    /// </summary>
    public class ChangeGroupByAccountWithPublishedDbTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccAndDb = new CreateAccountDatabaseAndAccountCommand(TestContext,
                new InfoDatabaseDomainModelTest(TestContext)
                {
                    Publish = true
                });

            createAccAndDb.Run();

            var database = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == createAccAndDb.AccountDatabaseId);

            var publishDto = new PublishDto
            {
                AccountDatabaseId = database.Id,
                AccountUsersId = database.AcDbAccesses.FirstOrDefault().ID
            };

            var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var manager = publishManager.PublishDatabase(publishDto);
            Assert.IsFalse(manager.Error, manager.Message);

            try
            {
                var cloudContentServerManager = ServiceProvider.GetRequiredService<CloudContentServerManager>();
                var segment = GetAccountSegment(createAccAndDb.AccountDetails.AccountId);
                var managerResult = cloudContentServerManager.GetCloudContentServerDto(segment.ContentServerID);

                Assert.IsNotNull(managerResult);
                Assert.IsFalse(managerResult.Error, managerResult.Message);

                var segmentViewModel = managerResult.Result;

                segmentViewModel.GroupByAccount = false;

                var segmentResult = cloudContentServerManager.EditCloudContentServer(segmentViewModel);
                Assert.IsFalse(segmentResult.Error, segmentResult.Message);
            }
            finally
            {
                publishManager.CancelPublishDatabaseWithWaiting(createAccAndDb.AccountDatabaseId);
            }
        }
    }
}