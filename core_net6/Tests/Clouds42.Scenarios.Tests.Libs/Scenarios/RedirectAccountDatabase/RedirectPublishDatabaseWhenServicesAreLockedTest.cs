﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.AccountDatabase.Publishes.Providers;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Extensions;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RedirectAccountDatabase
{
	/// <summary>
	/// Тест на редирект и анридерект опубликованной базы при блокированнии сервиса.
	/// Входные параметры: аккаунт с созданой и опубликованной базой
	/// 1.Этап: Блокируем сервис. На выходе web.config должен измениться
	/// 2.Этап: Разблокируем сервис. На выходе web.config должен вернуться в первоначальный вид
	/// </summary>
	public class RedirectPublishDatabaseWhenServicesAreLockedTest : ScenarioBase
	{
		public override void Run()
		{

            //добавление аккаунтa с включеной арендой 1С и базой
			var createAccountCommand = new CreateAccountCommand(TestContext);
			createAccountCommand.Run();

			var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, createAccountCommand);
			createAccountDatabaseCommand.Run();
			
			var accountId = createAccountCommand.AccountId;
			var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);
			var enterprise42CloudService = ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountUser.AccountId);

            var publishNode = DbLayer.PublishNodeReferenceRepository.FirstOrDefault();
            Assert.IsNotNull(publishNode);
            publishNode.Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer();
            DbLayer.PublishNodeReferenceRepository.Update(publishNode);
            DbLayer.Save();

			var model = new PublishDto
			{
				AccountDatabaseId = createAccountDatabaseCommand.AccountDatabaseId,
				AccountUsersId = createAccountCommand.AccountAdminId
			};
            
			var publishManager = ServiceProvider.GetRequiredService<AccountDatabasePublishManager>();
            var basePath = publishManager.PublishDatabase(model).Result;

			if (string.IsNullOrEmpty(basePath))
				throw new InvalidOperationException ("Не удалось получить путь");

            var db = DbLayer.DatabasesRepository.FirstOrDefault(x => x.Id == createAccountDatabaseCommand.AccountDatabaseId);

            var encodeCode = createAccountCommand.AccountId.GetEncodeGuid();

            var pathToPublish = GetPublishServerHelper.PathToPublishServer();

            var path = Path.Combine(pathToPublish, encodeCode, db.V82Name).Remove(0, 2);

            if (!Directory.Exists(path))
                throw new InvalidOperationException ("Публикации завершилась с ошибкой, нет папки на сервере");

            var filename = $"{path}\\web.config";
            byte[] hashFileOld;
            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(filename))
                hashFileOld = md5.ComputeHash(stream);

            //блокируем ресурсы в базе
            using (var dbScope = DbLayer.SmartTransaction.Get())
			{
				try
				{
					var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
						.Where(x => x.AccountId == accountId).ToList();
					foreach (var resources in resourcesConfigurations)
					{
						resources.Frozen = true;
						resources.ExpireDate = DateTime.Now.AddDays(-5);
						DbLayer.ResourceConfigurationRepository.Update(resources);
					}

				    DbLayer.Save();
				    dbScope.Commit();
                }
				catch (Exception ex)
				{
					dbScope.Rollback();
					throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
				}
			}

			// блокирование сервиса
			enterprise42CloudService.Lock();

            byte[] hashFileNew;
            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(filename))
                hashFileNew = md5.ComputeHash(stream);

            if (BitConverter.ToInt64(hashFileOld, 0) != BitConverter.ToInt64(hashFileNew, 0))
                throw new InvalidOperationException("Установка редиректа завершилось с ошибкой, файл не заменен");

            //разблокируем ресурсы в базе
            using (var dbScope = DbLayer.SmartTransaction.Get())
			{
				try
				{
					var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
						.Where(x => x.AccountId == accountId).ToList();
					foreach (var resources in resourcesConfigurations)
					{
						resources.Frozen = false;
						resources.ExpireDate = DateTime.Now.AddDays(10);
						DbLayer.ResourceConfigurationRepository.Update(resources);
					}
				    DbLayer.Save();
				    dbScope.Commit();
                }
				catch (Exception ex)
				{
					dbScope.Rollback();
					throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
				}
			}

			// блокирование сервиса
			enterprise42CloudService.UnLock();

            using (var md5 = MD5.Create())
            using (var stream = File.OpenRead(filename))
                hashFileNew = md5.ComputeHash(stream);

            if (BitConverter.ToInt64(hashFileOld, 0) != BitConverter.ToInt64(hashFileNew, 0))
                throw new InvalidOperationException("Снятие редиректа завершилось с ошибкой, файл не вернулся в первоначальный вид");
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDatabaseWebPublisher, DatabaseWebPublisher>();
        }
    }
}
