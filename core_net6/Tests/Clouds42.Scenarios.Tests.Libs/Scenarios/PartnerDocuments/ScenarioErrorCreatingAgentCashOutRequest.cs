﻿using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий ошибочного создания заявки на вывод средств
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Пытаемся создать заявку на вывод средств
    ///     3) Проверяем что вернулась ошибка создания
    /// </summary>
    public class ScenarioErrorCreatingAgentCashOutRequest : ScenarioBase
    {
        private readonly CreateAgentCashOutRequestManager _createAgentCashOutRequestManager;

        public ScenarioErrorCreatingAgentCashOutRequest()
        {
            _createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = 100500,
                PaySum = 100500
            };

            var result = _createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);

            Assert.IsTrue(result.Error);
        }
    }
}
