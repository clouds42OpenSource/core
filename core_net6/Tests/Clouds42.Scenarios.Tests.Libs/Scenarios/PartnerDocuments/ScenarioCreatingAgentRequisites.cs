﻿using System.Linq;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий создания реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Пытаемся создать реквизиты агента (имитируя полностью запоненную форму)
    ///     3) Проверяем что реквизиты были успешно созданы
    /// </summary>
    public class ScenarioCreatingAgentRequisites : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;

        public ScenarioCreatingAgentRequisites()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var agentRequisitesDto = _fakeAgencyDocumentFileHelper.GenerateAgentRequisites(createAccountCommand.AccountId,
                AgentRequisitesTypeEnum.LegalPersonRequisites, AgentRequisitesStatusEnum.OnCheck);

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;

            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            var legalPersonRequisites =
                DbLayer.LegalPersonRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == agentRequisitesId);

            Assert.IsNotNull(legalPersonRequisites);

            var filter = new PartnersAgentRequisitesFilterDto {PageNumber = 1, PageSize = 50};
            var agentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<AgentRequisitesManager>();

            var data = agentRequisitesManager.GetAgentRequisites(filter);

            Assert.IsFalse(data.Error, "Ошибка получения модели отображения реквизитов");
            Assert.IsTrue(data.Result.TotalCount==1,"Неверное количество записей");
            Assert.IsTrue(data.Result.ChunkDataOfPagination.Count() == 1, "Неверное количество записей");
            Assert.AreEqual(data.Result.ChunkDataOfPagination.FirstOrDefault()?.AgentRequisitesStatus,AgentRequisitesStatusEnum.OnCheck,"Неверный статус");
            Assert.AreEqual(data.Result.ChunkDataOfPagination.FirstOrDefault()?.PartnerAccountId, createAccountCommand.AccountId,"Неверный Id акаунта");
        }
    }
}
