﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий ошибочной смены статуса реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем реквизиты агента в режиме "Черновик" (с заведомо не заполненными полями)
    ///     3) Пытаемся сменить их статус на "На модерации"
    ///     4) Проверяем что статус не сменен и реквизиты не отредактированы
    /// </summary>
    public class ScenarioErrorChangeAgentRequisitesStatus : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly EditAgentRequisitesManager _editAgentRequisitesManager;

        public ScenarioErrorChangeAgentRequisitesStatus()
        {
            _editAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // имитируем не заполненную форму
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321"
                }
            };

            var result = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto);

            Assert.IsFalse(result.Error);
            Assert.IsNotNull(result.Result);

            var editAgentRequisitesDto = new EditAgentRequisitesDto
            {
                LegalPersonRequisites = agentRequisitesDto.LegalPersonRequisites,
                AccountId = agentRequisitesDto.AccountId,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                Id = result.Result,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck
            };

            var editResult = _editAgentRequisitesManager.EditAgentRequisites(editAgentRequisitesDto);

            Assert.IsTrue(editResult.Error);
        }
    }
}
