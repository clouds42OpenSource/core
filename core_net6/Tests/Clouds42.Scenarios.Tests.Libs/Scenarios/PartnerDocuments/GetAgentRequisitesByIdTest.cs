﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Тест получения модели реквизитов агента для редактирования
    /// Сценарий:
    ///         1. Создаем аккаунт, создаем реквизиты
    ///         2. Получаем реквизиты агента по Id
    ///         3. Проверяем, что правильная модель реквизитов
    /// </summary>
    public class GetAgentRequisitesByIdTest : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly AgentRequisitesManager _agentRequisitesManager;

        public GetAgentRequisitesByIdTest()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _agentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<AgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var agentRequisitesDto = _fakeAgencyDocumentFileHelper.GenerateAgentRequisites(createAccountCommand.AccountId,
                AgentRequisitesTypeEnum.LegalPersonRequisites, AgentRequisitesStatusEnum.OnCheck);

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesById = _agentRequisitesManager.GetAgentRequisitesById(agentRequisitesId);

            Assert.IsNotNull(agentRequisitesById);
            Assert.IsFalse(agentRequisitesById.Error);
            Assert.AreEqual(agentRequisitesById.Result.Id, agentRequisitesId);
            Assert.AreEqual(agentRequisitesById.Result.AccountId, createAccountCommand.AccountId);

            var agentRequisitesByNotExistingId = _agentRequisitesManager.GetAgentRequisitesById(Guid.NewGuid());

            Assert.IsTrue(agentRequisitesByNotExistingId.Error);
            Assert.IsNull(agentRequisitesByNotExistingId.Result);
            
            agentRequisitesById = _agentRequisitesManager.GetAgentRequisitesById(Guid.NewGuid());

            Assert.IsTrue(agentRequisitesById.Error);
            Assert.IsNull(agentRequisitesById.Result);
        }
    }
}
