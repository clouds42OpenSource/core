﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий смены статуса реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем реквизиты агента в режиме "На модерации"
    ///     3) Меняем их статус на "Активен"
    ///     4) Проверяем что статус успешно сменен
    /// </summary>
    public class ScenarioChangeAgentRequisitesStatus : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly EditAgentRequisitesManager _editAgentRequisitesManager;

        public ScenarioChangeAgentRequisitesStatus()
        {
            _editAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var contractFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = contractFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;

            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            var legalPersonRequisites =
                DbLayer.LegalPersonRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == agentRequisitesId);

            Assert.IsNotNull(legalPersonRequisites);

            var editAgentRequisitesDto = new EditAgentRequisitesDto
            {
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified,
                Id = agentRequisitesId,
                AccountId = agentRequisitesDto.AccountId
            };

            var result = _editAgentRequisitesManager.ChangeAgentRequisitesStatus(editAgentRequisitesDto);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.Error);

            var agentRequisitesAfterChange =
                DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == agentRequisitesId);

            Assert.AreEqual(agentRequisitesAfterChange.AgentRequisitesStatus, AgentRequisitesStatusEnum.Verified);
        }
    }
}
