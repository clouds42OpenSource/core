﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий ошибочного создания реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Пытаемся создать реквизиты агента (имитируя не заполненную форму)
    ///     3) Проверяем что реквизиты не были созданы и вернулась ошибка
    /// </summary>
    public class ScenarioErrorCreatingAgentRequisites : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;

        public ScenarioErrorCreatingAgentRequisites()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // имитируем не заполненную форму
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321"
                }
            };

            var result = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto);

            Assert.IsTrue(result.Error);
        }
    }
}
