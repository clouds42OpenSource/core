﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий смены статуса заявки на вывод средств
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем активные реквизиты
    ///     3) Добавляем денег на баланс агента
    ///     4) Cоздаем заявку на вывод средств
    ///     5) Пытаемся сменить ее статус на "Оплачена"
    ///     6) Проверяем что статус изменен и с баланса кошелька списана нужная сумма
    /// </summary>
    public class ScenarioChangeAgentCashOutRequestStatus : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly CreateAgentCashOutRequestManager _createAgentCashOutRequestManager;

        public ScenarioChangeAgentCashOutRequestStatus()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var requisitesFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;

            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            var cashOutRequestFile =
                _fakeAgencyDocumentFileHelper.GenerateFileData("Очень важный отчет агента.png", "image");

            var files = new List<CloudFileDataDto<byte[]>>();
            files.Add(cashOutRequestFile);

            var agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = 100500,
                PaySum = 100500,
                Files = files,
                AgentRequisitesId = agentRequisitesId
            };

            var result = _createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);

            Assert.IsFalse(result.Error);

            var agentCashOutRequestInDb = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);

            Assert.IsNotNull(agentCashOutRequestInDb);
            
            var changeAgentCashOutRequestStatusManager = ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();
            var managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(new ChangeAgentCashOutRequestStatusDto
            {
                RequestNumber = agentCashOutRequestInDb.RequestNumber,
                Sum = agentCashOutRequestInDb.RequestedSum,
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid
            });
            
            Assert.IsFalse(managerResult.Error);

            var agentCashOutRequestAfterEditAction = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);

            Assert.AreEqual(agentCashOutRequestAfterEditAction.RequestStatus, AgentCashOutRequestStatusEnum.Paid);
            RefreshDbCashContext(TestContext.Context.AgentWallets);
            var agentWalletAfterChanges =
                DbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.AreEqual(-100500, agentWalletAfterChanges.AvailableSum);
        }
    }
}
