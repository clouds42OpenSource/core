﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий смены типа для реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем реквизиты агента в режиме черновика
    ///     3) Меняем тип реквизитов
    ///     4) Проверяем что тип был успешно изменен
    /// </summary>
    public class ScenarioChangeAgentRequisitesType : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly EditAgentRequisitesManager _editAgentRequisitesManager;

        public ScenarioChangeAgentRequisitesType()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _editAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // имитируем не заполненную форму
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321"
                }
            };

            var result = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto);

            Assert.IsFalse(result.Error);
            Assert.IsNotNull(result.Result);

            var editAgentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = result.Result,
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.PhysicalPersonRequisites,
                PhysicalPersonRequisites = new PhysicalPersonRequisitesDto
                {
                    FullName = "Тестов Тест Тестович",
                    DateOfBirth = DateTime.Now.AddYears(-20)
                }
            };

            var editResult = _editAgentRequisitesManager.EditAgentRequisites(editAgentRequisitesDto);

            Assert.IsFalse(editResult.Error);

            var requisitesAfterEditAction = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == result.Result);
            var physicalPersonRequisites =
                DbLayer.PhysicalPersonRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == result.Result);

            Assert.IsNotNull(requisitesAfterEditAction);
            Assert.IsNotNull(physicalPersonRequisites);
            Assert.IsNotNull(requisitesAfterEditAction.PhysicalPersonRequisites);

            editAgentRequisitesDto.AgentRequisitesType = AgentRequisitesTypeEnum.SoleProprietorRequisites;
            editResult = _editAgentRequisitesManager.EditAgentRequisites(editAgentRequisitesDto);
            Assert.IsFalse(editResult.Error);

            requisitesAfterEditAction = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == result.Result);
            var soleProprietorRequisites =
                DbLayer.SoleProprietorRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == result.Result);

            Assert.IsNotNull(requisitesAfterEditAction);
            Assert.IsNotNull(soleProprietorRequisites);
            Assert.IsNotNull(requisitesAfterEditAction.SoleProprietorRequisites);

            editAgentRequisitesDto.AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites;
            editResult = _editAgentRequisitesManager.EditAgentRequisites(editAgentRequisitesDto);
            Assert.IsFalse(editResult.Error);

            requisitesAfterEditAction = DbLayer.AgentRequisitesRepository.FirstOrDefault(w => w.Id == result.Result);
            var legalPersonRequisites =
                DbLayer.LegalPersonRequisitesRepository.FirstOrDefault(w => w.AgentRequisitesId == result.Result);

            Assert.IsNotNull(requisitesAfterEditAction);
            Assert.IsNotNull(legalPersonRequisites);
            Assert.IsNotNull(requisitesAfterEditAction.LegalPersonRequisites);
        }
    }
}
