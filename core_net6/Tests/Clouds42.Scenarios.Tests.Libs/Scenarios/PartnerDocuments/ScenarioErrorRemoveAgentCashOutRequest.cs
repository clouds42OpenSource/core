﻿using System.Collections.Generic;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий ошибочного удаления заявки на вывод средств
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем активный договор
    ///     3) Добавляем денег на баланс агента
    ///     4) Cоздаем заявку на вывод средств
    ///     5) Пытаемся ее удалить
    ///     6) Проверяем что заявка не удалена
    /// </summary>
    public class ScenarioErrorRemoveAgentCashOutRequest : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly CreateAgentCashOutRequestManager _createAgentCashOutRequestManager;
        private readonly AgentCashOutRequestManager _agentCashOutRequestManager;

        public ScenarioErrorRemoveAgentCashOutRequest()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            _agentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<AgentCashOutRequestManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var agentWallet = DbLayer.AgentWalletRepository.GetAgentWalletOrCreateNew(createAccountCommand.AccountId);
            agentWallet.AvailableSum = 200000;
            DbLayer.AgentWalletRepository.Update(agentWallet);
            DbLayer.Save();

            var contractFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = contractFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;

            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            var cashOutRequestFile =
                _fakeAgencyDocumentFileHelper.GenerateFileData("Очень важный отчет агента.png", "image");

            var files = new List<CloudFileDataDto<byte[]>>();
            files.Add(cashOutRequestFile);

            var agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = 100500,
                PaySum = 100500,
                Files = files,
                AgentRequisitesId = agentRequisitesId
            };

            var result = _createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);

            Assert.IsFalse(result.Error);

            var agentCashOutRequestInDb = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);

            Assert.IsNotNull(agentCashOutRequestInDb);

            var removeResult = _agentCashOutRequestManager.RemoveAgentCashOutRequest(result.Result);

            Assert.IsTrue(removeResult.Error);

            var agentCashOutRequestAfterRemoveAction = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);

            Assert.IsNotNull(agentCashOutRequestAfterRemoveAction);
        }
    }
}
