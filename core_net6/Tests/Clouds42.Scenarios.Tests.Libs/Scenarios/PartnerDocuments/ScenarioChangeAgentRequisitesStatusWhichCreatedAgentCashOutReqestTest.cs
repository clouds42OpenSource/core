﻿using System;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий теста смены статуса реквизитам
    /// по которым создана заявка на расходование средств
    ///
    /// 1) Создаем аккаунт, платеж и реквизиты
    /// 2) Создаем заявку на вывод и пытаемся поменять статус реквизитам(сменить нельзя, так как, заявка не оплачена)
    /// 3) Меняем статус заявки на "Оплачена" и повторяем попытку сменить статус реквизитам(смена статуса успешна)
    /// </summary>
    public class ScenarioChangeAgentRequisitesStatusWhichCreatedAgentCashOutReqestTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();

            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();
            var editAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = 5,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = 5
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };
            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);

            Assert.IsNotNull(agentRequisitesInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(
                new CreateAgentCashOutRequestDto
                {
                    AccountId = createAccountCommand.AccountId,
                    Files = requisitesFiles,
                    TotalSum = 5,
                    RequestStatus = AgentCashOutRequestStatusEnum.New,
                    AgentRequisitesId = agentRequisitesId,
                    PaySum = 5
                });

            var cashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault() ??
                                 throw new InvalidOperationException ("Не удалось найти заявку на расходование средств");

            var editAgentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = agentRequisitesId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft
            };

            var managerResult = editAgentRequisitesManager.ChangeAgentRequisitesStatus(editAgentRequisitesDto);

            Assert.IsTrue(managerResult.Error, "Нельзя сменить статус реквизитам, когда есть не оплаченная заявка");

            cashOutRequest.RequestStatus = AgentCashOutRequestStatusEnum.Paid;
            DbLayer.AgentCashOutRequestRepository.Update(cashOutRequest);
            DbLayer.Save();

            managerResult = editAgentRequisitesManager.ChangeAgentRequisitesStatus(editAgentRequisitesDto);

            Assert.IsFalse(managerResult.Error, managerResult.Message);
        }
    }
}
