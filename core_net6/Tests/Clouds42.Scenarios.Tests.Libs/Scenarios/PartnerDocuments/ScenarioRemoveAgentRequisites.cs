﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PartnerDocuments
{
    /// <summary>
    /// Сценарий удаления реквизитов агента
    /// Действия:
    ///     1) Создаем аккаунт с пользователем
    ///     2) Создаем реквизиты агента в режиме черновика
    ///     3) Удаляем реквизиты
    ///     4) Проверяем что реквизиты были успешно удалены
    /// </summary>
    public class ScenarioRemoveAgentRequisites : ScenarioBase
    {
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly RemoveAgentRequisitesManager _removeAgentRequisitesManager;

        public ScenarioRemoveAgentRequisites()
        {
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _removeAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<RemoveAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // имитируем не заполненную форму
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321"
                }
            };

            var result = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto);

            Assert.IsFalse(result.Error);
            Assert.IsNotNull(result.Result);

            var removeResult = _removeAgentRequisitesManager.RemoveAgentRequisites(result.Result);

            Assert.IsFalse(removeResult.Error);
        }
    }
}
