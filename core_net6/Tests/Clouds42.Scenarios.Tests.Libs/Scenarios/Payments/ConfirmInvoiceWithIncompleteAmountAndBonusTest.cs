﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Проверка того, что при оплате счёта неполной суммой бонусы не начисляются
    /// Сценарий:
    /// 1) Создаём аккаунт
    /// 2) Создаём счёт на оплату, указываем в нём бонус
    /// 3) Имитируем подтверждение счета через КОРП, но с суммой, меньшей чем сумма в счёте
    /// 4) Проверяем отсутствие бонусов
    /// </summary>
    public class ConfirmInvoiceWithIncompleteAmountAndBonusTest: ScenarioBase
    {
        private readonly BillingAccountManager _billingAccountManager;
        private readonly IInvoiceConfirmationProvider _invoiceConfirmationProvider;

        public ConfirmInvoiceWithIncompleteAmountAndBonusTest()
        {
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
            _invoiceConfirmationProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceConfirmationProvider>();
        }

        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();
            var accountId = createAcc.AccountId;

            var model = new CalculationOfInvoiceRequestModelTest();

            var managerResult = _billingAccountManager.CreateInvoiceBasedOnCalculationInvoiceModel(accountId, model);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == managerResult.Result);
            Assert.IsNotNull(invoice);

            invoice.BonusReward = 1000;
            DbLayer.InvoiceRepository.Update(invoice);
            DbLayer.Save();

            _invoiceConfirmationProvider.ConfirmInvoice(new ConfirmationInvoiceDto
            {
                sum = invoice.Sum-1,
                ID = invoice.Id,
                AccountID = invoice.AccountId,
                System = PaymentSystem.Corp.ToString(),
                Description = "Имитация подтверждения счета на оплату через КОРП"
            });

            var billingAccount = DbLayer.BillingAccountRepository.GetBillingAccount(accountId);
            RefreshDbCashContext(Context.Payments);
            var payment = DbLayer.PaymentRepository.FirstOrDefault(p => p.AccountId == accountId);
            Assert.IsNotNull(payment,"Оплата должна пройти");

            Assert.AreEqual(0,billingAccount.BonusBalance,"Бонусы не должны были начислится");
            invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == managerResult.Result);
            Assert.IsNull(invoice.BonusReward, "Значение бонусов в счёте должно быть изменено на null");

        }
    }
}
