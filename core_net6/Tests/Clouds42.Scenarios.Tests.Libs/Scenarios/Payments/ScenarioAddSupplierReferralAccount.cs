﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Suppliers.Contracts.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Привязываем к поставщику аккаунт-реферал
    /// Действия: создаём аккаунт. Выбираем из базы поставщика, привязываем.
    /// Проверка: в таблице SupplierReferralAccounts появилась соответствующая запись
    /// </summary>
    public class ScenarioAddSupplierReferralAccount : ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            var newAcc = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createAcc.AccountId);
            Assert.IsNotNull(newAcc, "Ошибка создания аккаунта");
            var rulocale = DbLayer.LocaleRepository.FirstOrDefault(l => l.CurrencyCode == 643);

            var supplier = DbLayer.SupplierRepository.GetFirst(s => s.LocaleId == rulocale.ID);
            Assert.IsNotNull(supplier,"Ошибка выборки поставщика");
            var supplierReferralProvider = ServiceProvider.GetRequiredService<ISupplierReferralProvider>();
            var result = supplierReferralProvider.AddReferralAccount(supplier.Id, createAcc.AccountId);

            Assert.IsNotNull(result,"Ошибка привязки реферала");

            var supplierReferral = DbLayer.SupplierReferralAccountRepository.FirstOrDefault(s =>
                s.SupplierId == supplier.Id && s.ReferralAccountId == createAcc.AccountId);

            Assert.IsNotNull(supplierReferral , "Ошибка привязки реферала. Нет записи в базе");

        }
    }
}
