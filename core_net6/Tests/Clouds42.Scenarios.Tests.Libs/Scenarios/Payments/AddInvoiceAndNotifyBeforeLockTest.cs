﻿using System;
using System.Linq;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Сценарий проверки создания/не создания нового счёта
    /// перед уведомлением о скорой блокировки сервиса
    /// Действия: 
    /// 1) Создаём аккаунт, меняем дату окончания на +5 дней
    /// 2) Пополняем счёт на произвольную сумму
    /// 3) Выполняем таску NotifyBeforeLockServices()
    /// 4) Ещё раз выполняем таску NotifyBeforeLockServices()
    /// Проверяем: после первого запуска таски должен добавится счёт на оплату аренды,
    /// а после второго запуска новые счета не добавляются.
    /// </summary>
    public class AddInvoiceAndNotifyBeforeLockTest : ScenarioBase
    {
        public override void Run()
        {
            var createAcc = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAcc.Run();

            var account = createAcc.Account;
            var model = new CalculationOfInvoiceRequestModelTest();
            var managerResult = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreateInvoiceBasedOnCalculationInvoiceModel(account.Id, model);
            
            Assert.IsFalse(managerResult.Error, managerResult.Message);

            RefreshDbCashContext(TestContext.Context.Invoices);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == managerResult.Result);
            Assert.IsNotNull(invoice);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный денежный баланс",
                BillingServiceId = null,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 5,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Money
            });

            var rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == createAcc.AccountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent1C.ExpireDate = rent1C.CreateDate.Value.Date.AddDays(5);  
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.Save();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.NotifyBeforeLockServices();

            RefreshDbCashContext(TestContext.Context.Invoices);
            var rentInvoice = DbLayer.InvoiceRepository.FirstOrDefault(i => i.Period == 3);
            Assert.IsNotNull(rentInvoice, "не создался счёт на аренду");

            billingManager.NotifyBeforeLockServices();
            RefreshDbCashContext(TestContext.Context.Invoices);

            var invoicesId = DbLayer.InvoiceRepository.All().Select(i=>i.Id).ToList();
            if (!invoicesId.Contains(rentInvoice.Id) || !invoicesId.Contains(managerResult.Result))
            { throw new InvalidOperationException("Счета заменялись");}

        }
    }
}
