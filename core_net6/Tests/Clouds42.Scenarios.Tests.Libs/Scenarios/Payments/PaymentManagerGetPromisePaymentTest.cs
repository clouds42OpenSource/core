﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест на взятие ОП при активных сервисах. Проверяем создался ли ОП.
    /// </summary>
    public class PaymentManagerGetPromisePaymentTest : ScenarioBase
    {
        public override void Run()
        {
            var adManager = TestContext.ServiceProvider.GetRequiredService<ActiveDirectoryProviderFakeSuccess>();

            //добавление аккаунтa с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var invoiceRequest = new CalculationOfInvoiceRequestModelTest();
            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);
            var groupName = $"company_{accountUser.Account.IndexNumber}_web";

            //создаем обещанный платеж
            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(accountId, invoiceRequest);

            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            //проверка что вернулись права
            var userExist = adManager.ExistUserAtGroup(accountUser.Login, groupName);
            if (!userExist)
                throw new InvalidOperationException ($"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");

            userExist = adManager.ExistUserAtGroup(accountUser.Login, "Remote Desktop Access");
            if (!userExist)
                throw new InvalidOperationException ($"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");

            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);
            var resourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(y => y.AccountId == accountId && y.Cost > 0);

            if (billingAcc.PromisePaymentSum == null)
                throw new InvalidOperationException ("Promise payment has not been created");
            if (resourceConfig.FrozenValue)
                throw new InvalidOperationException ("Services are still locked");
            if (resourceConfig.ExpireDate == DateTime.Now.AddMonths(-1))
                throw new InvalidOperationException ("Services are still locked");
        }
    }
}
