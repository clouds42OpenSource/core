﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Сценарий теста на проверку подтверждения счета на оплату с бонусным вознаграждением
    /// Действия:
    ///     1) Создаем счет на оплату
    ///     2) Указываем сумму бонусного вознаграждения
    ///     3) Имитируем подтверждение счета через КОРП
    ///     4) Проверяем что сумма бонусного баланса аккаунта равна сумме бонусного вознаграждения в счете на оплату
    /// </summary>
    public class ConfirmInvoiceWithBonusRewardTest : ScenarioBase
    {
        private readonly IInvoiceConfirmationProvider _invoiceConfirmationProvider;
        private readonly BillingAccountManager _billingAccountManager;

        public ConfirmInvoiceWithBonusRewardTest()
        {
            _invoiceConfirmationProvider = TestContext.ServiceProvider.GetRequiredService<IInvoiceConfirmationProvider>();
            _billingAccountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();
        }

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account ??
                          throw new InvalidOperationException("Аккаунт не найден или не был создан");

            var model = new CalculationOfInvoiceRequestModelTest();

            var managerResult = _billingAccountManager.CreateInvoiceBasedOnCalculationInvoiceModel(account.Id, model);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var invoiceId = managerResult.Result;

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == invoiceId);
            Assert.IsNotNull(invoice);

            invoice.BonusReward = 1000;
            DbLayer.InvoiceRepository.Update(invoice);
            DbLayer.Save();

            _invoiceConfirmationProvider.ConfirmInvoice(new ConfirmationInvoiceDto
            {
                sum = invoice.Sum,
                ID = invoice.Id,
                AccountID = invoice.AccountId,
                System = PaymentSystem.Corp.ToString(),
                Description = "Имитация подтверждения счета на оплату через КОРП"
            });

            var billingAccount = DbLayer.BillingAccountRepository.GetBillingAccount(account.Id);

            Assert.AreEqual(billingAccount.BonusBalance, invoice.BonusReward);
        }
    }
}
