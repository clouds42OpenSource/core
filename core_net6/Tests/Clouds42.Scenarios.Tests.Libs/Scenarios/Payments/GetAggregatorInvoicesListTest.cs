﻿using System;
using System.Collections.Generic;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить список платежей агрегаторов
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем платеж и счет
    ///         3)Получаем список платежей агрегаторов
    ///         4)Проверяем, что метод возвращает не пустое значение
    ///         5)Проверяем, что количество объектов, которые вернул метод верное
    /// </summary>
    public class GetAggregatorInvoicesListTest : ScenarioBase
    {
        private readonly IAggregatorInvoicesDataManager _aggregatorInvoicesDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public GetAggregatorInvoicesListTest()
        {
            _aggregatorInvoicesDataManager = ServiceProvider.GetRequiredService<IAggregatorInvoicesDataManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;
            var payments = new List<Payment>();
            var invoices = new List<Invoice>();
            var sum = 1000;

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var ukrPaysPayment =
                _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, PaymentSystem.UkrPays, sum);
            var robokassaPayment =
                _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, PaymentSystem.Robokassa, sum);

            payments.Add(ukrPaysPayment);
            payments.Add(robokassaPayment);

            dbLayer.PaymentRepository.InsertRange(payments);

            var firstInvoice = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Account.AccountRequisites.Inn, sum);
            firstInvoice.PaymentID = ukrPaysPayment.Id;

            var secondInvoice = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Account.AccountRequisites.Inn, sum);
            secondInvoice.PaymentID = robokassaPayment.Id;

            invoices.Add(firstInvoice);
            invoices.Add(secondInvoice);

            dbLayer.InvoiceRepository.InsertRange(invoices);
            dbLayer.Save();

            var dateFrom = DateTime.Now.AddDays(-1);
            var dateTo = DateTime.Now;

            var aggregatorInvoices = _aggregatorInvoicesDataManager.GetAggregatorInvoicesData(dateFrom, dateTo);

            Assert.IsNotNull(aggregatorInvoices);
            Assert.AreEqual(2, aggregatorInvoices.Result.Count);
        }
    }
}