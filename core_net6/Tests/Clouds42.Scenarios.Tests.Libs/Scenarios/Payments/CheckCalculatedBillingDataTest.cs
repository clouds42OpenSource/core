﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    public class CheckCalculatedBillingDataTest : ScenarioBase
    {
        /// <summary>
        /// Тест для проверки правильности расчета данных на вкладке баланс
        ///     1) Создаем аккаунт с включенной арендой
        ///     2) Подключаем тестовый демо сервис
        ///     3) Достаем вью модель для вкладки баланс
        ///     4) Проверяем что сумма ежемесячного платежа равна только стоимости аренды
        ///     5) Проверяем что в самом калькуляторе нет возможности купить демо лицензии
        /// </summary>
        public override void Run()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            var serviceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypes
                });

            createCustomService.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = createCustomService.Id
                }
            });

            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            var viewModel = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().GetBillingData(accountId);
            
            var paymentSum = viewModel.Result.BillingData.RegularPayment;

            var demoServiceResConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(w =>
                w.AccountId == createAccountCommand.AccountId && w.BillingService.Id == createCustomService.Id);

            Assert.IsTrue(demoServiceResConfig.IsDemoPeriod);

            var resConf1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.AreEqual(resConf1C.Cost, paymentSum);
        }
    }
}
