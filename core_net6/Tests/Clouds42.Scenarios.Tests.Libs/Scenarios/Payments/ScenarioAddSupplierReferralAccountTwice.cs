﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Suppliers.Contracts.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Попытка сделать аккаунт реферальным для двух поставщиков
    /// Действия: создаём аккаунт, выбираем двух поставщиков,
    /// привязываем к обеим созданый аккаунт как реферал(ко второму дважды)
    /// Проверяем : для первого и второго поставщика привязка отработает, но вернёт
    /// ошибку при попытке повторно привязать ко второму поставщику тот же реферал
    /// </summary>
    public class ScenarioAddSupplierReferralAccountTwice : ScenarioBase
    {
        public override void Run()
        {
            #region account
            var createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();
            var newAcc = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createAcc.AccountId);
            Assert.IsNotNull(newAcc, "Ошибка создания аккаунта");
            var rulocale = DbLayer.LocaleRepository.FirstOrDefault(l => l.CurrencyCode == 643);
            #endregion account

            #region firstMove
            var supplierFirst = DbLayer.SupplierRepository.GetFirst(s => s.LocaleId == rulocale.ID);
            Assert.IsNotNull(supplierFirst, "Ошибка выборки поставщика");
            var supplierReferralProvider = ServiceProvider.GetRequiredService<ISupplierReferralProvider>();
            var result = supplierReferralProvider.AddReferralAccount(supplierFirst.Id, createAcc.AccountId);

            Assert.IsNotNull(result, "Ошибка привязки реферала");

            var supplierReferral = DbLayer.SupplierReferralAccountRepository.FirstOrDefault(s =>
                s.SupplierId == supplierFirst.Id && s.ReferralAccountId == createAcc.AccountId);

            Assert.IsNotNull(supplierReferral, "Ошибка привязки реферала. Нет записи в базе");
            #endregion firstMove

            #region secondMove
            var supplierSecond =
                DbLayer.SupplierRepository.FirstOrDefault(s => s.LocaleId == rulocale.ID && s.Id != supplierFirst.Id);
            Assert.IsNotNull(supplierSecond, "Ошибка выборки поставщика");
            result = supplierReferralProvider.AddReferralAccount(supplierSecond.Id, createAcc.AccountId);
            Assert.IsNotNull(result, "Ошибка привязки реферала!");

            supplierReferral = DbLayer.SupplierReferralAccountRepository.FirstOrDefault(s =>
                s.SupplierId == supplierSecond.Id && s.ReferralAccountId == createAcc.AccountId);

            Assert.IsNotNull(supplierReferral, "Ошибка привязки реферала. Нет записи в базе");

            try
            {
                var addResult = supplierReferralProvider.AddReferralAccount(supplierSecond.Id, createAcc.AccountId);
                Assert.IsNull(addResult, "Метод должен был выдать ошибку");
            }
            catch(Exception ex)
            {
                Assert.IsNotNull(ex.Message);
            }
            # endregion secondMove

        }
    }
}
