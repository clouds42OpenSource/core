﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тестирование работы вкладки Баланс/Транзакции, фильтра
    /// Сценарий:
    /// 1) Создаём аккаунт с арендой
    /// 2) Наполняем billing.Payments тестовыми данными за разные
    ///     периоды и разные сервисы
    /// 3) Делаем выборки с разными фильтрами
    /// </summary>
    public class WorkWithTransactionsPageTest : ScenarioBase
    {
        private readonly CreatePaymentManager _createPaymentManager;
        private readonly IBillingServiceDataProvider _billingServiceDataProvider;
        private readonly Billing.BillingOperations.Managers.BillingDataManager _billingDataManager;

        public WorkWithTransactionsPageTest()
        {
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>();
            _billingDataManager = TestContext.ServiceProvider.GetRequiredService<Billing.BillingOperations.Managers.BillingDataManager>();
        }

        public override void Run()
        {
            var createAccCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccCommand.Run();
            var accId = createAccCommand.AccountId;

            var rentService = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var diskService = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyDisk);
            
            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = accId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс, что бы аккаунт не считался демо",
                BillingServiceId = rentService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 1000000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            AddPayment(accId, rentService, DateTime.Now);
            AddPayment(accId, rentService, DateTime.Now.AddDays(-24));

            AddPayment(accId, diskService, DateTime.Now.AddMonths(-2));
            AddPayment(accId, diskService, DateTime.Now.AddMonths(-2));

            AddPayment(accId, rentService, DateTime.Now.AddMonths(-5));
            AddPayment(accId, diskService, DateTime.Now.AddMonths(-5));

            AddPayment(accId, diskService, DateTime.Now.AddMonths(-9));
            
            AddPayment(accId, rentService, DateTime.Now.AddMonths(-13));

            var filter = new SelectDataCommonDto<PaymentsFilterDto>
            {
                Filter = new PaymentsFilterDto
                {
                    DateFrom = DateTime.Now.Date,
                    DateTo = DateTime.Now.Date,
                    Service = Clouds42Service.MyEnterprise
                }
            };

            var result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 2, "Сегодня 2 транзакция.");


            filter.Filter.DateFrom = DateTime.Now.AddDays(-29).Date;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 3, "За 4 недели было 3 транзакции.");


            filter.Filter.DateFrom = DateTime.Now.AddMonths(-2).Date;
            filter.Filter.DateTo = DateTime.Now.AddDays(-29).Date;
            filter.Filter.Service = Clouds42Service.MyDisk;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 2, "В этот период было 2 транзакции по диску.");

            filter.Filter.Service = Clouds42Service.MyEnterprise;
            filter.Filter.DateFrom = DateTime.Now.AddMonths(-5).Date;
            filter.Filter.DateTo = DateTime.Now.AddMonths(-2).AddDays(1).Date;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 1, "В этот период была 1 транзакция по аренде.");

            filter.Filter.Service = Clouds42Service.MyDisk;
            filter.Filter.DateFrom = DateTime.Now.AddMonths(-9).Date;
            filter.Filter.DateTo = DateTime.Now.AddMonths(-2).Date;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 4, "В этот период было 4 транзакции по диску.");


            filter.Filter.Service = Clouds42Service.MyEnterprise;
            filter.Filter.DateFrom = DateTime.Now.AddMonths(-13).Date;
            filter.Filter.DateTo = DateTime.Now.Date;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 5, "В этот период было 5 транзакций по аренде.");


            filter.Filter.DateFrom = null;
            filter.Filter.DateTo = null;
            result = _billingDataManager.GetPaginatedTransactionsForAccount(accId, filter);
            Assert.IsTrue(result.Result.Records.Length == 5, "Всего было 5 транзакций аренды.");
        }

        /// <summary>
        /// Добавить платеж
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="payDate">Дата платежа</param>
        private void AddPayment(Guid accountId, IBillingService billingService, DateTime payDate)
        {
            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = payDate,
                Description = $"{payDate:dd-MM-yyyy}, {billingService.Name}",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Outflow,
                Total = 1,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }
    }
}
