﻿using System.Collections.Generic;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест метода для отмены ППВх
    /// Сценарий:
    ///         1)Создаем оплаченный счет, бонусный и денежный платеж
    ///         2)Отменяем счет, проверяем, что он в статусе Processing,
    ///          денежный платеж в статусе Canceled, создался бонусный исходящий платеж
    /// </summary>
    public class CancelInvoiceTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;
        private readonly TestDataGenerator _testDataGenerator;

        public CancelInvoiceTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var sum = 1000;
            var bonus = 100;

            var payments = new List<Payment>();

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == account.AccountId);
            billingAccount.BonusBalance = bonus;
            billingAccount.Balance = sum;

            dbLayer.BillingAccountRepository.Update(billingAccount);

            var bonusPayment = _testDataGenerator.GeneratePayment(account.AccountId, PaymentSystem.Corp, bonus);
            bonusPayment.TransactionType = TransactionType.Bonus;

            var moneyPayment = _testDataGenerator.GeneratePayment(account.AccountId, PaymentSystem.Corp, sum);

            payments.Add(bonusPayment);
            payments.Add(moneyPayment);

            dbLayer.PaymentRepository.InsertRange(payments);

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, sum);
            invoice.BonusReward = bonus;
            invoice.PaymentID = moneyPayment.Id;

            dbLayer.InvoiceRepository.Insert(invoice);
            dbLayer.Save();

            var aggregatorInvoices = _paymentsManager.CancelInvoice(invoice.Uniq.ToString(), account.AccountId);

            Assert.IsFalse(aggregatorInvoices.Error);

            var accountBalanceAfterCancel = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == account.AccountId).BillingAccount;
            var invoiceAfterCancel = dbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == invoice.Id);

            Assert.AreEqual(InvoiceStatus.Processing.ToString(), invoiceAfterCancel.State);

            var payment = dbLayer.PaymentRepository.FindPayment(moneyPayment.Number);

            Assert.AreEqual(PaymentStatus.Canceled.ToString(), payment.Status);
            Assert.AreEqual(0, accountBalanceAfterCancel.Balance, "Деньги не списался");
            Assert.AreEqual(0, accountBalanceAfterCancel.BonusBalance, "Бонусы не списались");
        }
    }
}