﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить или создать новый счет на оплату по заданным параметрам.
    /// Сценарий:
    ///         1)Создаем аккаунт и сцет на оплату в статусе Processing
    ///         2)Получаем счет на оплату, проверяем что модель не пустая, Id счета равен Id счета в результате выполнения менеджера
    ///         3)Изменяем статус счета на оплату на Processed
    ///         4)Пытаемся снова получить этот счет, проверяем, что был создан ноывй счет на ту же сумму
    /// </summary>
    public class GetProcessingOrCreateNewInvoiceTest : ScenarioBase
    {
        private readonly InvoiceDataManager _invoiceDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public GetProcessingOrCreateNewInvoiceTest()
        {
            _invoiceDataManager = ServiceProvider.GetRequiredService<InvoiceDataManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, 999);
            invoice.State = InvoiceStatus.Processing.ToString();

            dbLayer.InvoiceRepository.Insert(invoice);
            dbLayer.Save();

            var invoiceDataManager = _invoiceDataManager.GetProcessingOrCreateNewInvoiceAsync($"42{invoice.Uniq}", 999);

            invoiceDataManager.Wait();

            Assert.IsFalse(invoiceDataManager.Result.Error);
            Assert.AreEqual(invoice.Id, invoiceDataManager.Result.Result.Id);

            invoice.State = InvoiceStatus.Processed.ToString();

            dbLayer.InvoiceRepository.Update(invoice);
            dbLayer.Save();
            
            var newInvoiceDataManager = _invoiceDataManager.GetProcessingOrCreateNewInvoiceAsync($"42{invoice.Uniq}", 999);

            newInvoiceDataManager.Wait();

            Assert.IsFalse(newInvoiceDataManager.Result.Error);
            Assert.AreNotEqual(invoiceDataManager.Result.Result.Id, newInvoiceDataManager.Result.Result.Id);
            Assert.AreNotEqual(invoiceDataManager.Result.Result.Uniq, newInvoiceDataManager.Result.Result.Uniq);

            var invoiceRepository =
                dbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == newInvoiceDataManager.Result.Result.Id);

            Assert.IsNotNull(invoiceRepository);

        }
    }
}