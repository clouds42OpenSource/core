﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    ///     Сценарий разблокирования сервисов при добавлении платежа,
    /// по регламенту разблокироватся должны только Аренда и МойДиск.
    /// 
    ///     Предусловия: для аккаунта test_account подключен один пользователь
    /// test_user_1 по тарифу Стандарт; баланс 0 руб; дата окончания
    /// сервиса +10 дней от тек.даты.
    /// 
    ///     Действия: делаем все сервисы платными, блокируем их,
    /// вносим деньги на баланс чтобы разблокировать сервисы
    /// 
    ///     Проверка: сервисы 'Аренда 1С' и 'МойДиск' должны быть
    /// разблокированны, остальные остаться в прежнем состоянии
    /// </summary>
    public class AddPaymentAndUnlockMyEnterpriseAndMyDisk : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Recognition);

            var resConfig = new ResourcesConfiguration
            {
                AccountId = accountId,
                Cost = 1500,
                CostIsFixed = true,
                DiscountGroup = 0,
                ExpireDate = DateTime.Today.AddMonths(1),
                Id = Guid.NewGuid(),
                BillingServiceId = billingService.Id,
                Frozen = false,
                CreateDate = DateTime.Now
            };

            TestContext.DbLayer.ResourceConfigurationRepository.Insert(resConfig);
            TestContext.DbLayer.Save();

            var enterprise42CloudService = TestContext.ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountUser.AccountId);

            var myDiskService = TestContext.ServiceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountUser.AccountId);
            var esdlService = TestContext.ServiceProvider.GetRequiredService<IEsdlCloud42Service>().SetAccountId(accountUser.AccountId);
            var recognitionService = TestContext.ServiceProvider.GetRequiredService<IRecognition42CloudService>().SetAccountId(accountUser.AccountId);

            using (var dbScope = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
                        .Where(x => x.AccountId == accountId).ToList();
                    foreach (var configuration in resourcesConfigurations)
                    {
                        configuration.Frozen = true;
                        configuration.Cost = 1500;
                        configuration.ExpireDate = DateTime.Now.AddDays(-5);
                        DbLayer.ResourceConfigurationRepository.Update(configuration);
                    }

                    enterprise42CloudService.Lock();
                    myDiskService.Lock();
                    esdlService.Lock();
                    recognitionService.Lock();

                    DbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    dbScope.Rollback();
                    throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
                }
            }

            var paymentSum = 999999;
            CreateInflowPayment(accountUser.AccountId, null, paymentSum);
            
            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);

            if (billingAcc.Balance >= paymentSum)
                throw new InvalidOperationException("Деньги не списались за подключение Аренды 1С");

            var mainServices = new[]
            {
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise).Id,
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyDisk).Id
            };

            var mainConfigurations = DbLayer.ResourceConfigurationRepository.Where(res =>
                res.AccountId == accountId
                && res.Cost > 0
                && mainServices.Contains(res.BillingServiceId));

            foreach (var configuration in mainConfigurations)
            {
                Assert.IsFalse(configuration.FrozenValue,
                    $"Сервис '{configuration.BillingService.Name}' остался заблокированным");
                Assert.AreEqual(configuration.ExpireDate?.Date, DateTime.Now.AddMonths(1).Date,
                    $"Срок сервиса '{configuration.BillingService.Name}' не был продлен!!");
            }

            var otherServices = new[]
            {
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Esdl).Id,
                new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.Recognition).Id
            };

            var otherConfigurations = DbLayer.ResourceConfigurationRepository.Where(res =>
                res.AccountId == accountId
                && res.Cost > 0
                && otherServices.Contains(res.BillingServiceId));

            foreach (var configuration in otherConfigurations)
            {
                Assert.IsTrue(configuration.FrozenValue,
                    $"Сервис '{configuration.BillingService.Name}' не должен быть разблокированным!");

                if (configuration.BillingService.SystemService == Clouds42Service.Esdl)
                    Assert.AreEqual(configuration.ExpireDate?.Date, DateTime.Now.AddMonths(1).Date,
                        "Срок сервиса ESDL должен продлиться на месяц");
                else
                    Assert.AreNotEqual(configuration.ExpireDate?.Date, DateTime.Now.AddMonths(1).Date,
                        $"Срок сервиса '{configuration.BillingService.Name}' должен остатся прежним!!");
            }
        }
    }
}