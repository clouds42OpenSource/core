﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Установить номер акта для счета на оплату
    /// Сценарий:
    ///         1)Создаем аккаунт, создаем счет на оплату
    ///         2)Устанавливаем номер акта для счета на оплату
    ///         3)Проверяем, что значение поля ActId, было установлено
    /// </summary>
    public class SetStatementTest : ScenarioBase
    {
        private readonly InvoiceActManagerTestAdapter _invoiceActManager;
        private readonly TestDataGenerator _testDataGenerator;
        
        public SetStatementTest()
        {
            _invoiceActManager = ServiceProvider.GetRequiredService<InvoiceActManagerTestAdapter>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, 1000);
            dbLayer.InvoiceRepository.Insert(invoice);

            dbLayer.Save();

            var actId = Guid.NewGuid();

            var act = _invoiceActManager.SetAct(invoice.Id, actId, "Description", false);
            act.Wait();
            
            Assert.IsFalse(act.Result.Error, "Не удалось установить номер акта для счета на оплату");

            RefreshDbCashContext(TestContext.Context.Invoices);

            var invoiceRepository = dbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == invoice.Id);

            Assert.IsNotNull(invoiceRepository.ActID);
            Assert.AreEqual(actId, invoiceRepository.ActID);
        }
    }
}
