﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    ///     Создаем счет, но не отправляем уведомление пользователю, проверяем наличие уведомления в базе
    /// </summary>
    public class AddInvoiceWithoutNotifyTest : ScenarioBase
    {
        private readonly string _email = $"random{new Random().Next(0, 99999)}mail@random.org";

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            if (account == null)
                throw new InvalidOperationException ("Аккаунт не найден или не был создан");

            var model = new CalculationOfInvoiceRequestModelTest();
            var accountAdmin =
                TestContext.DbLayer.AccountUsersRepository.GetAccountUser(createAccountDatabaseAndAccountCommand
                    .AccountDetails.AccountAdminId);

            model.NeedSendEmail = false;
            model.AccountUserIdForSendNotify = accountAdmin.Id;

            AddInvoiceAndCheck(model, account);

            accountAdmin.Email = _email;

            TestContext.DbLayer.AccountUsersRepository.Update(accountAdmin);
            TestContext.DbLayer.Save();

            AddInvoiceAndCheck(model, account);
        }

        /// <summary>
        /// Создать счет и проверить наличие уведомления в базе
        /// </summary>
        /// <param name="model">Модель создания счета</param>
        /// <param name="account">Аккаунт облака</param>
        private void AddInvoiceAndCheck(CalculationOfInvoiceRequestModelTest model, Account account)
        {
            var managerResult = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreateInvoiceBasedOnCalculationInvoiceModel(account.Id, model);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == managerResult.Result);

            Assert.IsNotNull(invoice);

            var notifyKey = $"SendInvoiceNotificationToClient=>InvoiceId=>{invoice.Id}";
            var buffer = DbLayer.NotificationBufferRepository.FirstOrDefault(item =>
                item.Account == invoice.AccountId &&
                item.NotifyKey == notifyKey);

            Assert.IsNull(buffer);
        }
    }
}
