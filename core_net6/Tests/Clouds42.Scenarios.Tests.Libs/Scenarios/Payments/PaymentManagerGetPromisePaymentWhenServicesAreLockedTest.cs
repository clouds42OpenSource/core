﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест на взятие ОП с заблокированными сервисами. На выходе сервисы должны быть разблокированные сервисы и деньги должны быть списаны.
    /// </summary>
    public class PaymentManagerGetPromisePaymentWhenServicesAreLockedTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var invoiceRequest = new CalculationOfInvoiceRequestModelTest();
            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);
            var enterprise42CloudService = TestContext.ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountUser.AccountId);
            
            using (var dbScope = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
                        .Where(x => x.AccountId == accountId).ToList();
                    foreach (var resources in resourcesConfigurations)
                    {
                        resources.Frozen = true;
                        resources.ExpireDate = DateTime.Now.AddDays(-5);
                        if (resources.BillingService.SystemService == Clouds42Service.MyDisk)
                            resources.Cost = 2505;

                        DbLayer.ResourceConfigurationRepository.Update(resources);
                    }

                    DbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    dbScope.Rollback();
                    throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
                }
            }
            enterprise42CloudService.Lock();
            
            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(accountId, invoiceRequest);

            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);

            if (billingAcc.PromisePaymentSum == null)
                throw new InvalidOperationException("Promise payment has not been created");
            if (billingAcc.Balance == invoiceRequest.SuggestedPayment.PaymentSum)
                throw new InvalidOperationException("Деньги не списались за подключение Аренды 1С");

            var resourceConfigList = DbLayer.ResourceConfigurationRepository.Where(y => y.AccountId == accountId && y.Cost > 0);

            foreach (var resourceConfig in resourceConfigList)
            {
                if (resourceConfig.FrozenValue)
                    throw new InvalidOperationException("Services are still locked");
                if (resourceConfig.ExpireDate.HasValue && resourceConfig.ExpireDate.Value.Date != DateTime.Now.AddMonths(1).Date)
                    throw new InvalidOperationException("Services are still locked");
            }
        }
    }
}
