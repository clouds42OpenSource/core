﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест пополнения баланса на произвольную сумму.
    /// Проверяем чтобы период был null
    /// </summary>
    public class AddInvoiceWithoutPeriodForSuggestedPaymentSumTest : ScenarioBase
    {
        public override void Run()
        {
            CreateAccountCommand createAcc = new CreateAccountCommand(TestContext);
            createAcc.Run();

            var account = createAcc.Account;

            if (account == null)
                throw new InvalidOperationException ("Аккаунт не найден или не был создан");

            var model = new CalculationOfInvoiceRequestModelTest();

            var managerResult = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreateInvoiceBasedOnCalculationInvoiceModel(account.Id, model);

            Assert.IsFalse(managerResult.Error, managerResult.Message);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == managerResult.Result);
            Assert.IsNotNull(invoice);
            Assert.IsNull(invoice.Period);
        }
    }
}
