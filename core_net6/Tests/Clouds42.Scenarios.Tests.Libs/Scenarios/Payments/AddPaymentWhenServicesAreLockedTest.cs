﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест на внесение оплаты с заблокированными сервисами. На выходе сервисы должны быть разблокированные сервисы.
    /// </summary>
    public class AddPaymentWhenServicesAreLockedTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            
            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);
            var enterprise42CloudService = TestContext.ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountUser.AccountId);
            
            using (var dbScope = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
                        .Where(x => x.AccountId == accountId).ToList();
                    foreach (var resources in resourcesConfigurations)
                    {
                        resources.Frozen = true;
                        resources.ExpireDate = DateTime.Now.AddDays(-5);
                        DbLayer.ResourceConfigurationRepository.Update(resources);
                    }

                    DbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    dbScope.Rollback();
                    throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
                }
            }
            enterprise42CloudService.Lock();

            var paymentSum = 10000;
            CreateInflowPayment(accountUser.AccountId, null, paymentSum);
            
            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);
            var resourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(y => y.AccountId == accountId && y.Cost>0);

            if (billingAcc.Balance >= paymentSum)
                throw new InvalidOperationException("Деньги не списались за подключение Аренды 1С");
            if (resourceConfig.FrozenValue)
                throw new InvalidOperationException("Services are still locked");
            if (resourceConfig.ExpireDate.HasValue && resourceConfig.ExpireDate.Value.Date != DateTime.Now.AddMonths(1).Date)
                throw new InvalidOperationException("Services are still locked");
        }
    }
}
