﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест на внесение оплаты с просроченным ОП и заблокированными сервисами.
    /// На выходе сервисы должны быть разблокированные и дата не должна быть изменена.
    /// </summary>
    public class AddPaymentWhenPromisePaymentAreLockedTest : ScenarioBase
    {
        private const decimal SumOfPromisedPayment = 20000;

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;
            var invoiceRequest = new CalculationOfInvoiceRequestModelTest
            {
                SuggestedPayment = new SuggestedPaymentModelDto { PaymentSum = SumOfPromisedPayment }
            };

            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            var resConfig = new ResourcesConfiguration
            {
                AccountId = accountId,
                Cost = 1500,
                CostIsFixed = true,
                DiscountGroup = 0,
                ExpireDate = DateTime.Today.AddMonths(1),
                Id = Guid.NewGuid(),
                BillingServiceId = billingService.Id,
                Frozen = false,
                CreateDate = DateTime.Now
            };

            TestContext.DbLayer.ResourceConfigurationRepository.Insert(resConfig);
            TestContext.DbLayer.Save();

            var enterprise42CloudService = TestContext.ServiceProvider.GetRequiredService<IMyEnterprise42CloudService>().SetAccountId(accountUser.AccountId); 

            var myDiskService = TestContext.ServiceProvider.GetRequiredService<IMyDisk42CloudService>().SetAccountId(accountUser.AccountId);
            var esdlService = TestContext.ServiceProvider.GetRequiredService<IEsdlCloud42Service>().SetAccountId(accountUser.AccountId); 
            var recognitionService = TestContext.ServiceProvider.GetRequiredService<IRecognition42CloudService>().SetAccountId(accountUser.AccountId); 

            var expireDate = DateTime.Now.AddDays(17);

            using (var dbScope = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var resourcesConfigurations = DbLayer.ResourceConfigurationRepository.Where(x => x.AccountId == accountId).ToList();

                    foreach (var configuration in resourcesConfigurations)
                    {
                        configuration.Frozen = false;
                        configuration.Cost = 1500;
                        configuration.ExpireDate = expireDate;
                        DbLayer.ResourceConfigurationRepository.Update(configuration);
                    }

                    DbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    dbScope.Rollback();
                    throw new InvalidOperationException($"Ошибка при обновлении ресурсов {ex}");
                }
            }

            var promisePayment = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(accountId, invoiceRequest);
            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            using (var dbScope = DbLayer.SmartTransaction.Get())
            {
                try
                {
                    var resourcesConfigurations = DbLayer.ResourceConfigurationRepository
                        .Where(x => x.AccountId == accountId).ToList();
                    var billing = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);

                    foreach (var resources in resourcesConfigurations)
                    {
                        resources.Frozen = true;
                        DbLayer.ResourceConfigurationRepository.Update(resources);
                        DbLayer.Save();
                    }

                    billing.PromisePaymentDate = DateTime.Now.AddDays(-10);
                    DbLayer.BillingAccountRepository.Update(billing);
                    DbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    dbScope.Rollback();
                    throw new InvalidOperationException($"Ошибка блокировки ресурсов {ex}");
                }
            }
            enterprise42CloudService.Lock();
            myDiskService.Lock();
            esdlService.Lock();
            recognitionService.Lock();
            
            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);

            Assert.IsNotNull(billingAcc.PromisePaymentSum, "Обещанный платеж не был активирован");

            var allPaidConfigurations = DbLayer.ResourceConfigurationRepository.GetAllPaidActiveMainServices(accountId);

            foreach (var configuration in allPaidConfigurations)
            {
                Assert.IsTrue(configuration.FrozenValue,
                    $"Сервис '{configuration.BillingService.Name}' не должен быть разблокированным!");
                Assert.AreEqual(configuration.ExpireDate?.Date, expireDate.Date,
                    $"Срок сервиса '{configuration.BillingService.Name}' должен остатся прежним!!");
            }

            var paymentSum = 10;
            CreateInflowPayment(accountUser.AccountId, null, paymentSum);
            
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);

            Assert.IsFalse(billingAccount.Balance >= SumOfPromisedPayment,
                "Деньги на балансе не списались за ОП");
            Assert.IsTrue(billingAccount.PromisePaymentSum == 0, "ОП не был погашен!!");

            allPaidConfigurations = DbLayer.ResourceConfigurationRepository.GetAllPaidActiveMainServices(accountId);

            foreach (var configuration in allPaidConfigurations)
            {
                Assert.IsFalse(configuration.FrozenValue,
                    $"Сервис '{configuration.BillingService.Name}' остался заблокированным");
                Assert.AreEqual(configuration.ExpireDate?.Date, expireDate.Date,
                    $"Срок сервиса '{configuration.BillingService.Name}' должен остатся прежним!!");
            }
        }
    }
}