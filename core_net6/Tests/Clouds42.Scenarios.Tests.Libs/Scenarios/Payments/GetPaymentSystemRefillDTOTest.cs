﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить информацию о ПС
    /// Сценарий:
    ///         1)Создаем три аккаунта разных локалей
    ///         2)Получаем информацию о ПС
    ///         3)Проверяем, что каждой локали соответствует ПС для этой локали
    /// </summary>
    public class GetPaymentSystemRefillDTOTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;

        public GetPaymentSystemRefillDTOTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var sum = 5000;

            var accountRu = new CreateAccountCommand(TestContext);
            accountRu.Run();

            var createAccountHelperTest = ServiceProvider.GetRequiredService<CreateAccountHelperTest>();

            var accountKz = createAccountHelperTest.Create(accountLocaleId: dbLayer.LocaleRepository
                .FirstOrDefault(l => l.Name == LocaleConst.Kazakhstan).ID);

            var accountUa = createAccountHelperTest.Create(accountLocaleId: dbLayer.LocaleRepository
                .FirstOrDefault(l => l.Name == LocaleConst.Ukraine).ID);

            GetPaymentSystemRefillDTOForRu(accountRu.AccountId, sum);
            GetPaymentSystemRefillDTOForKz(accountKz.Id, sum);
            GetPaymentSystemRefillDTOForUa(accountUa.Id, sum);
        }

        /// <summary>
        /// Получить информаию о ПС Казахской локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sum">Сумма платежа</param>
        private void GetPaymentSystemRefillDTOForKz(Guid accountId, int sum)
        {
            var paymentSystem = _paymentsManager.GetPaymentSystem(accountId, sum);

            Assert.IsFalse(paymentSystem.Error);
            Assert.IsNotNull(paymentSystem.Result);
            Assert.AreEqual(PaymentSystem.Paybox, paymentSystem.Result.PaymentSystem);
        }

        /// <summary>
        /// Получить информаию о ПС Российской локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sum">Сумма платежа</param>
        private void GetPaymentSystemRefillDTOForRu(Guid accountId, int sum)
        {
            var paymentSystem = _paymentsManager.GetPaymentSystem(accountId, sum);

            Assert.IsFalse(paymentSystem.Error);
            Assert.IsNotNull(paymentSystem.Result);
            Assert.AreEqual(PaymentSystem.Robokassa, paymentSystem.Result.PaymentSystem);
        }

        /// <summary>
        /// Получить информаию о ПС Укр локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sum">Сумма платежа</param>
        private void GetPaymentSystemRefillDTOForUa(Guid accountId, int sum)
        {
            var paymentSystem = _paymentsManager.GetPaymentSystem(accountId, sum);

            Assert.IsFalse(paymentSystem.Error);
            Assert.IsNotNull(paymentSystem.Result);
            Assert.AreEqual(PaymentSystem.UkrPays, paymentSystem.Result.PaymentSystem);
        }
    }
}