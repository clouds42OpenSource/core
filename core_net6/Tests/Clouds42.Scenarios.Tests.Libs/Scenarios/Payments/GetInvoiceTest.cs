﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить инфойс или создать новый
    /// Сценарий:
    ///         1)Создаем аккаунт и инвойс
    ///         2)Получаем инвойс на существующую сумму, проверяем, что модель не пустая
    ///         3)Получаем инвойс на другую сумму (создание - false), проверяем, что вернулась ошибка
    ///         4)Меняем ForceCreate на true, пытаемся снова получить инвойс, проверяем пустая ли модель, проверяем создался ли новый иновйс
    /// </summary>
    public class GetInvoiceTest : ScenarioBase
    {
        private readonly InvoiceDataManager _invoiceDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public GetInvoiceTest()
        {
            _invoiceDataManager = ServiceProvider.GetRequiredService<InvoiceDataManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var sum = 500;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, sum);

            dbLayer.InvoiceRepository.Insert(invoice);
            dbLayer.Save();

            var invoiceDataManager =
                _invoiceDataManager.GetOrCreateInvoiceAsync(invoice.Uniq.ToString(), false, account.Inn, sum);

            invoiceDataManager.Wait();

            Assert.IsFalse(invoiceDataManager.Result.Error);
            Assert.IsNotNull(invoiceDataManager.Result.Result);

            var newSum = 777;

            invoiceDataManager =
                _invoiceDataManager.GetOrCreateInvoiceAsync(invoice.Uniq.ToString(), false, account.Inn, newSum);

            invoiceDataManager.Wait();

            Assert.IsNull(invoiceDataManager.Result.Result);

            invoiceDataManager =
                _invoiceDataManager.GetOrCreateInvoiceAsync(invoice.Uniq.ToString(), true, account.Inn, newSum);

            invoiceDataManager.Wait();

            Assert.IsFalse(invoiceDataManager.Result.Error);
            Assert.IsNotNull(invoiceDataManager.Result.Result);

            var invoiceRepository =
                dbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == invoiceDataManager.Result.Result.Id);

            Assert.IsNotNull(invoiceRepository);
            Assert.AreEqual(newSum, invoiceRepository.Sum);
        }
    }
}