﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.DataModels;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    ///     Создаем счет и отправляем уведомление на почту, проверяем отослалось сообщение или нет
    /// </summary>
    public class AddInvoiceAndNotifyTest : ScenarioBase
    {
        private readonly string _email = $"random{new Random().Next(0, 99999)}mail@random.org";

        public override void Run()
        {
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var account = createAccountDatabaseAndAccountCommand.AccountDetails.Account;

            if (account == null)
                throw new InvalidOperationException("Аккаунт не найден или не был создан");

            var model = new CalculationOfInvoiceRequestModelTest();

            var accountAdmin =
                TestContext.DbLayer.AccountUsersRepository.GetAccountUser(createAccountDatabaseAndAccountCommand
                    .AccountDetails.AccountAdminId);

            model.NeedSendEmail = true;
            model.AccountUserIdForSendNotify = accountAdmin.Id;

            AddInvoiceAndCheck(model, account);
            
            accountAdmin.Email = _email;

            TestContext.DbLayer.AccountUsersRepository.Update(accountAdmin);
            TestContext.DbLayer.Save();
            
            AddInvoiceAndCheck(model, account);
        }

        private void AddInvoiceAndCheck(CalculationOfInvoiceRequestModelTest model, Account account)
        {
            var invoiceResult = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreateInvoiceBasedOnCalculationInvoiceModel(account.Id, model);

            Assert.IsFalse(invoiceResult.Error, invoiceResult.Message);

            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(inv => inv.Id == invoiceResult.Result);

            Assert.IsNotNull(invoice);
        }
    }
}

