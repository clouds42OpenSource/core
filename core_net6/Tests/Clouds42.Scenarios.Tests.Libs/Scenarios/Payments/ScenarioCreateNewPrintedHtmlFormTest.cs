﻿using System;
using System.Linq;
using Clouds42.AgencyAgreement.PrintedHtmlForm.Managers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Проверяем создание печатной формы
    /// Действия: получаем пустой шаблон, заполняем поля,
    /// проверяем, что изменения сохранились в базе. Удаляем.
    /// </summary>
    public class ScenarioCreateNewPrintedHtmlFormTest: ScenarioBase
    {
        private readonly PrintedHtmlFormManager _printedHtmlFormManager;
        private readonly CreateAndEditPrintedHtmlFormManager _createAndEditPrintedHtmlFormManager;

        public ScenarioCreateNewPrintedHtmlFormTest()
        {
            _printedHtmlFormManager = TestContext.ServiceProvider.GetRequiredService<PrintedHtmlFormManager>();
            _createAndEditPrintedHtmlFormManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditPrintedHtmlFormManager>();
        }

        public override void Run()
        {
            var createAccount = new CreateAccountCommand(TestContext);
            createAccount.Run();

            var formResult = _printedHtmlFormManager.GetPrintedHtmlFormModelForCreating();
            var fileTestName = $"Тестовое название файла - New {DateTime.Now:yyyy-MM-dd-hhmmssfff}";
            var form = formResult.Result;
            Assert.IsNotNull(form,"Ошибка генерации пустой формы.");
            form.Name = $"Тестовая форма - New {DateTime.Now:yyyy-MM-dd-hhmmssfff}";

            form.HtmlData =
                "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <meta http-equiv=\"content-type\" " +
                "content=\"text/html; charset=utf-8\"/>\r\n</head>\r\n<body>\r\n</body>\r\n</html>";
            form.Files=
            [
                new()
                {
                    FileName = fileTestName,
                    ContentType = "image",
                    Bytes = [],
                    Base64 = Convert.ToBase64String(new byte[] { }),
                }
            ];

            var invoiceDc = new InvoiceDc();
            form.ModelType = invoiceDc.GetType().FullName;

            try
            {
                _createAndEditPrintedHtmlFormManager.CreateOrChangePrintedHtmlForm(formResult.Result);
            }
            finally
            {
                var formForDelete = DbLayer.PrintedHtmlFormRepository.FirstOrDefault(f => f.Name == form.Name);
                var formFilesForDelete = DbLayer.PrintedHtmlFormFileRepository.Where(f => f.PrintedHtmlFormId == formForDelete.Id).ToList();
                var cloudFilesForDelete =
                    DbLayer.CloudFileRepository.FirstOrDefault(f => f.FileName == fileTestName);

                DbLayer.PrintedHtmlFormFileRepository.DeleteRange(formFilesForDelete);
                DbLayer.PrintedHtmlFormRepository.Delete(formForDelete);
                DbLayer.CloudFileRepository.Delete(cloudFilesForDelete);
                DbLayer.Save();

                Assert.IsNotNull(formForDelete.HtmlData,"У формы пустая разметка.");
                Assert.IsNotNull(formForDelete, "Форма не создалась.");
                Assert.IsTrue(formFilesForDelete.Any(), "Файлы формы не создались.");
                Assert.IsNotNull(cloudFilesForDelete, "Файлы формы не создались.");
            }
        }
    }
}
