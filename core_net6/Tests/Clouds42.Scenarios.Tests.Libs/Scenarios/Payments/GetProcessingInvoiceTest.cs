﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить необработанный инвойс на заданную сумму
    /// Сценарий:
    ///         1)Создаем аккаунт и необработанный инвойс
    ///         2)Получаем необработанный инвойс на сумму счета, проверям, что модель не пустая
    ///         3)Получаем инвойс на другую сумму счета, проверям, что модель пустая
    ///         4)Изменяем статус инвойса на Processed, получаем необработанный инвойс на сумму счета, проверяем, что модель пустая
    /// </summary>
    public class GetProcessingInvoiceTest : ScenarioBase
    {
        private readonly InvoiceDataManager _invoiceDataManager;
        private readonly TestDataGenerator _testDataGenerator;
        public GetProcessingInvoiceTest()
        {
            _invoiceDataManager = ServiceProvider.GetRequiredService<InvoiceDataManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;
            var sum = 2000;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, sum);
            invoice.State = InvoiceStatus.Processing.ToString();

            dbLayer.InvoiceRepository.Insert(invoice);
            dbLayer.Save();

            var processingInvoice = _invoiceDataManager.GetProcessingInvoice(account.Inn, sum);

            processingInvoice.Wait();

            Assert.IsNotNull(processingInvoice.Result.Result);

            var anotherSum = 900;

            processingInvoice = _invoiceDataManager.GetProcessingInvoice(account.Inn, anotherSum);

            processingInvoice.Wait();

            Assert.IsNull(processingInvoice.Result.Result);

            invoice.State = InvoiceStatus.Processed.ToString();

            dbLayer.InvoiceRepository.Update(invoice);
            dbLayer.Save();

            processingInvoice = _invoiceDataManager.GetProcessingInvoice(account.Inn, sum);

            processingInvoice.Wait();

            Assert.IsNull(processingInvoice.Result.Result);

        }
    }
}