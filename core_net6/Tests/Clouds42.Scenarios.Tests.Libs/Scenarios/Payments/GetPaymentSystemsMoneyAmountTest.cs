﻿using System;
using System.Collections.Generic;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing;
using Clouds42.Common.Constants;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Получить сумму платежей в разрере платежных систем
    /// Сценарий:
    ///         1)Создаем аккаунты(одинг в укр. локали)
    ///         2)Содаем этим аккаунтам платежи в платежных системах: Корп, УкрПэй, Робокасса
    ///         3)Получаем все платежи, убеждаемся, что сумма платежей по каждой системе верная
    /// </summary>
    public class GetPaymentSystemsMoneyAmountTest : ScenarioBase
    {
        private readonly PaymentsManager _paymentsManager;
        private readonly TestDataGenerator _testDataGenerator;

        public GetPaymentSystemsMoneyAmountTest()
        {
            _paymentsManager = ServiceProvider.GetRequiredService<PaymentsManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var accountsCount = 5;
            var ukrAccountsCount = 5;

            var createSegmentCommand = new CreateSegmentCommand(TestContext, true);
            createSegmentCommand.Run();

            var createAccountHelperTest = ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
            var locale = dbLayer.LocaleRepository.FirstOrDefault(i => i.Name == LocaleConst.Ukraine);
            var ukrAccounts = createAccountHelperTest.CreateByCount(ukrAccountsCount, locale.ID);

            var accounts = createAccountHelperTest.CreateByCount(accountsCount);
            accounts.AddRange(ukrAccounts);

            var paymentSum = 500;
            var paymentsCount = 5;
            var payments = new List<Payment>();

            foreach (var account in accounts)
            {
                var corPayments = _testDataGenerator.GenerateManyPayments(account.Id, PaymentSystem.Corp, paymentSum, paymentsCount);
                var robokassaPayments = _testDataGenerator.GenerateManyPayments(account.Id, PaymentSystem.Robokassa, paymentSum, paymentsCount);
                var ukrPaysPayments = _testDataGenerator.GenerateManyPayments(account.Id, PaymentSystem.UkrPays, paymentSum, paymentsCount);
                payments.AddRange(corPayments);
                payments.AddRange(robokassaPayments);
                payments.AddRange(ukrPaysPayments);
            }

            dbLayer.PaymentRepository.InsertRange(payments);
            dbLayer.Save();

            var dateFrom = DateTime.Now.AddDays(-1);
            var dateTo = DateTime.Now;

            var paymentSystemsMoneyAmount = _paymentsManager.GetPaymentSystemsMoneyAmount(dateFrom, dateTo);

            Assert.AreEqual(GetPaymentsSum(accountsCount, paymentsCount, paymentSum), paymentSystemsMoneyAmount.Result.CorpRUMoneyAmount);
            Assert.AreEqual(GetPaymentsSum(accounts.Count, paymentsCount, paymentSum), paymentSystemsMoneyAmount.Result.RobokassaMoneyAmount);
            Assert.AreEqual(GetPaymentsSum(accounts.Count, paymentsCount, paymentSum), paymentSystemsMoneyAmount.Result.UkrPaysMoneyAmount);
            Assert.AreEqual(GetPaymentsSum(ukrAccountsCount, paymentsCount, paymentSum), paymentSystemsMoneyAmount.Result.CorpUAMoneyAmount);

        }

        /// <summary>
        /// Получить сумму всех платежей
        /// </summary>
        /// <param name="accountsCount">Количество аккаунтов</param>
        /// <param name="paymentsCount">Количество платежей</param>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <returns>Сумма платежей</returns>
        private int GetPaymentsSum(int accountsCount, int paymentsCount, int paymentSum)
        {
            return accountsCount * paymentsCount * paymentSum;
        }
    }
}