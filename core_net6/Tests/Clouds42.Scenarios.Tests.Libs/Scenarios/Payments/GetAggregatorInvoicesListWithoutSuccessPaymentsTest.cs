﻿using System;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест на проверку получения списка платежей агрегаторов если платеж не успешен
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем платеж и счет (платеж в процессе)
    ///         3)Получаем список платежей агрегаторов
    ///         4)Проверяем, что метод возвращает не пустое значение
    ///         5)Проверяем, что количество объектов, которые вернул метод верное
    /// </summary>
    public class GetAggregatorInvoicesListWithoutSuccessPaymentsTest : ScenarioBase
    {
        private const int PaymentSum = 100500;

        private readonly IAggregatorInvoicesDataManager _aggregatorInvoicesDataManager;
        private readonly TestDataGenerator _testDataGenerator;

        public GetAggregatorInvoicesListWithoutSuccessPaymentsTest()
        {
            _aggregatorInvoicesDataManager = ServiceProvider.GetRequiredService<IAggregatorInvoicesDataManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var robokassaPayment =
                _testDataGenerator.GeneratePayment(createAccountCommand.AccountId, PaymentSystem.Robokassa, PaymentSum);
            DbLayer.PaymentRepository.Insert(robokassaPayment);

            var invoice = _testDataGenerator.GenerateInvoice(createAccountCommand.AccountId,
                createAccountCommand.Account.AccountRequisites.Inn, PaymentSum);

            invoice.PaymentID = robokassaPayment.Id;

            DbLayer.InvoiceRepository.Insert(invoice);
            DbLayer.Save();

            CheckAggregatorInvoicesCount(1);

            robokassaPayment.StatusEnum = PaymentStatus.Canceled;
            DbLayer.PaymentRepository.Update(robokassaPayment);
            DbLayer.Save();
            DbLayer.PaymentRepository.Reload(robokassaPayment);

            CheckAggregatorInvoicesCount(0);
        }

        /// <summary>
        /// Проверить количество платежей агрегатора
        /// </summary>
        /// <param name="expectedPaymentsCount">Ожидаемое количество платежей</param>
        private void CheckAggregatorInvoicesCount(int expectedPaymentsCount)
        {
            var dateFrom = DateTime.Now.AddDays(-1);
            var dateTo = DateTime.Now;

            var aggregatorInvoices = _aggregatorInvoicesDataManager.GetAggregatorInvoicesData(dateFrom, dateTo);
            Assert.IsNotNull(aggregatorInvoices);
            Assert.AreEqual(expectedPaymentsCount, aggregatorInvoices.Result.Count);
        }
    }
}