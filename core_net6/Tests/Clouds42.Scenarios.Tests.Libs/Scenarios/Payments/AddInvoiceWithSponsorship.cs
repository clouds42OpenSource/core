﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Сценарий проверки  корректности списка услуг в счёте
    /// Действия: создаём первый аккаунт-спонсор и второй, с двумя пользователями.
    /// Второму пользователю второго аккауунта аренду купит спонсор.
    /// Проверка: при формировании вторым аккаунтом счёта на оплату
    /// спонсированый пользователь учитываться не будет
    /// </summary>
    public class AddInvoiceWithSponsorship : ScenarioBase
    {
        private readonly ResourceConfigurationHelper _resourceConfigurationHelper;

        public AddInvoiceWithSponsorship()
        {
            _resourceConfigurationHelper = TestContext.ServiceProvider.GetRequiredService<ResourceConfigurationHelper>();
        }

        public override void Run()
        {
            #region accounts

            var createSponsorAccountCommand = new CreateAccountCommand(TestContext);
            createSponsorAccountCommand.Run();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == createAccountCommand.AccountAdminId);
            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"SecondUser{DateTime.Now:mmssffff}",
                Email = $"SecondUser{DateTime.Now:mmssffff}@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            var secondUserId = res.Result;

            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = "",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result
            };

            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);

            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var result = rent1CConfigurationAccessManager.TurnOnRent1C(createSponsorAccountCommand.AccountId,
                createSponsorAccountCommand.AccountAdminId);
            Assert.IsTrue(string.IsNullOrEmpty(result.Message), "Подключение аренды не возможно.");

            var result2 = rent1CConfigurationAccessManager.TurnOnRent1C(createAccountCommand.AccountId,
                createAccountCommand.AccountAdminId);
            Assert.IsTrue(string.IsNullOrEmpty(result2.Message), "Подключение аренды не возможно.");

            DbLayer.Save();

            Assert.IsFalse(res.Error, "Ошибка создания нового пользователя. " + res.Message);

            #endregion accounts

            var webResourceType =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUserWeb);

            var serviceDto = new List<BillingServiceTypeDto>
            {
                new()
                {
                    Id = webResourceType.Id,
                    Name = "вэб",
                    Description = "вэб",
                    BillingType = BillingTypeEnum.ForAccountUser,
                    ServiceTypeCost = 700,
                    DependServiceTypeId = null
                }
            };

            _resourceConfigurationHelper.InsertServiceTypeResourcesWithSponsorship(createAccountCommand.Account.Id,
                secondUserId,
                serviceDto, 0, createSponsorAccountCommand.Account.Id);

            var billingDataProvider = TestContext.ServiceProvider.GetRequiredService<IBillingDataProvider>();

            var billingInfo =
                billingDataProvider.GetDataForReplenishBalance(createAccountCommand.AccountId, false, null);

            var rent1CService = GetRent1CServiceOrThrowException();
            Assert.IsNotNull(rent1CService);
            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);
            var rent1CRate = GetRent1CRate(localeId, rent1CService.Id);
            Assert.AreEqual(rent1CRate, billingInfo.BaseBillingServices.TotalAmount, "Неверная общая сумма счёта");

            var webLicenses = billingInfo.BaseBillingServices.BillingServiceTypeResources
                                  .FirstOrDefault(s => s.Id == webResourceType.Id)?.CountLicenses ?? 0;
            Assert.AreEqual(0, webLicenses, "Кол-во WEB лицензий должно быть 0");
        }

        /// <summary>
        /// Получить цену Аренды 1С
        /// </summary>
        /// <param name="localeId">Id локали</param>
        /// <param name="rent1CId">Id Аренды 1С</param>
        /// <returns></returns>
        private decimal GetRent1CRate(Guid localeId, Guid rent1CId)
        {
            var sum = DbLayer.RateRepository
                .Where(r => r.BillingServiceType.ServiceId == rent1CId && r.AccountType == "Standart" &&
                            r.LocaleId == localeId).Sum(r => r.Cost);
            Assert.IsNotNull(sum);

            return sum;
        }
    }
}
