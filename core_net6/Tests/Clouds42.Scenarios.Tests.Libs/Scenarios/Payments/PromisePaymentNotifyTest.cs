﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    ///     Берем обещанный платеж и проверяем отправилось ли уведомление
    /// </summary>
    public class PromisePaymentNotifyTest : ScenarioBase
    {
        public override void Run()
        {
            var resourcesConfigurationCommand = new CreateAccountAndResourcesConfigurationCommand(TestContext);

            resourcesConfigurationCommand.Run();

            var accountId = resourcesConfigurationCommand.AccountId;

            var result = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>()
                .CreatePromisePayment(accountId, resourcesConfigurationCommand.InvoiceRequest);

            Assert.IsFalse(result.Error, result.Message);

            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);
            var resourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(y => y.AccountId == accountId);

            if (billingAcc.PromisePaymentSum == null)
                throw new InvalidOperationException("Promise payment has not been created");
            if (resourceConfig.FrozenValue)
                throw new InvalidOperationException("Services are still locked");
            
            var invoice = DbLayer.InvoiceRepository.FirstOrDefault(item => item.AccountId == accountId);
            Assert.IsNotNull(invoice);
        }
    }
}
