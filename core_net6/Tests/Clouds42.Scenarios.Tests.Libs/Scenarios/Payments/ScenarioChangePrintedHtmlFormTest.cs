﻿using System;
using System.Linq;
using Clouds42.AgencyAgreement.PrintedHtmlForm.Managers;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Тест редактирования печатной формы
    /// Действия: создаём печатную форму, а потом редактируем её.
    /// Проверяем ,что поля изменились
    /// </summary>
    public class ScenarioChangePrintedHtmlFormTest: ScenarioBase
    {
        private readonly PrintedHtmlFormManager _printedHtmlFormManager;
        private readonly CreateAndEditPrintedHtmlFormManager _createAndEditPrintedHtmlFormManager;

        public ScenarioChangePrintedHtmlFormTest()
        {
            _printedHtmlFormManager = TestContext.ServiceProvider.GetRequiredService<PrintedHtmlFormManager>();
            _createAndEditPrintedHtmlFormManager = TestContext.ServiceProvider.GetRequiredService<CreateAndEditPrintedHtmlFormManager>();
        }

        public override void Run()
        {
            var createAccount = new CreateAccountCommand(TestContext);
            createAccount.Run();

            var formResult = _printedHtmlFormManager.GetPrintedHtmlFormModelForCreating();
            var fileTestName = $"Тестовое название файла - Edit {DateTime.Now:yyyy-MM-dd-hhmmssfff}";
            var form = formResult.Result;
            var formNext = form;
            Assert.IsNotNull(form, "Ошибка генерации пустой формы.");
            form.Name = $"Тестовая форма - Edit {DateTime.Now:yyyy-MM-dd-hhmmssfff}";

            form.HtmlData =
                "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n    <meta http-equiv=\"content-type\" " +
                "content=\"text/html; charset=utf-8\"/>\r\n</head>\r\n<body>\r\n</body>\r\n</html>";
            form.Files =
            [
                new()
                {
                    FileName = fileTestName,
                    ContentType = "image",
                    Bytes = [],
                    Base64 = Convert.ToBase64String(new byte[] { }),
                }
            ];

            var invoiceDc = new InvoiceDc();
            var invoiceReceiptDocumentBuilderModel = new InvoiceReceiptDocumentBuilderModel();
            form.ModelType = invoiceDc.GetType().FullName;

           try
            {
                var result = _createAndEditPrintedHtmlFormManager.CreateOrChangePrintedHtmlForm(formResult.Result);
                formNext.Id = result.Result;
                formNext.Files = [];
                formNext.ModelType = invoiceReceiptDocumentBuilderModel.GetType().FullName;
                _createAndEditPrintedHtmlFormManager.CreateOrChangePrintedHtmlForm(formNext);
            }
            finally
            {
                var formForDelete = DbLayer.PrintedHtmlFormRepository.FirstOrDefault(f => f.Name == form.Name);
                var formFilesForDelete = DbLayer.PrintedHtmlFormFileRepository
                    .Where(f => f.PrintedHtmlFormId == formForDelete.Id).ToList();
                var cloudFilesForDelete =
                    DbLayer.CloudFileRepository.FirstOrDefault(f => f.FileName == fileTestName);

                DbLayer.PrintedHtmlFormFileRepository.DeleteRange(formFilesForDelete);
                DbLayer.PrintedHtmlFormRepository.Delete(formForDelete);
                DbLayer.CloudFileRepository.Delete(cloudFilesForDelete);
                DbLayer.Save();

                Assert.IsNotNull(formForDelete.HtmlData, "У формы пустая разметка.");
                Assert.IsNotNull(formForDelete, "Форма не создалась.");
                Assert.AreEqual(0,formFilesForDelete.Count, "Файлов быть не должно.");
                Assert.IsNull(cloudFilesForDelete, "Файлов быть не должно.");
            }
        }
    }
}
