﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Создает аккаунт с влюченной арендой.
    /// Задается просроченный обещанный платеж.
    /// Производится пополнение на баланс всей суммы долга.
    /// В результате обещанный платеж должен быть погашен.
    /// </summary>
    public class RepayPromisePaymentTest : ScenarioBase
    {
                
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();            

            var billingAccount =
                DbLayer
                    .BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-7);

            const int  sum = 1500;

            billingAccount.PromisePaymentSum = sum;
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var systemServiceDataProvider = ServiceProvider.GetRequiredService<IBillingServiceDataProvider>();

            var billingService = systemServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            paymentsManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = sum,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.AreEqual(0.0000M,billingAccount.PromisePaymentSum);

            Assert.AreEqual(0.0000M,billingAccount.Balance);

        }

    }
}