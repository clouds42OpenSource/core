﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Провести платеж путем принятия счета на оплату
    /// Сценарий:
    ///         1)Создаем аккаунт, и счет на оплату
    ///         2)Выполняем метод подтверждения счета на оплату
    ///         3)Проверяем, что счет в статусе Processed, а баланс аккаунта увеличился на сумму счета
    /// </summary>
    public class ConfirmationInvoiceTest : ScenarioBase
    {
        private readonly InvoiceConfirmationManagerTestAdapter _confirmationManager;
        private readonly TestDataGenerator _testDataGenerator;
        public ConfirmationInvoiceTest()
        {
            _confirmationManager = ServiceProvider.GetRequiredService<InvoiceConfirmationManagerTestAdapter>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();

        }
        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var bonus = 500;
            var sum = 1500;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var billingAccount = dbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == account.AccountId);

            if (billingAccount == null)
                throw new InvalidOperationException();

            billingAccount.Balance = 0;
            billingAccount.BonusBalance = 0;

            dbLayer.BillingAccountRepository.Update(billingAccount);

            var invoice = _testDataGenerator.GenerateInvoice(account.AccountId, account.Inn, sum);
            invoice.BonusReward = bonus;
            invoice.State = InvoiceStatus.Processing.ToString();

            dbLayer.InvoiceRepository.Insert(invoice);
            dbLayer.Save();

            var managerResult = _confirmationManager.ConfirmInvoiceViaNewThread(GetConfirmationInvoiceDto(invoice, account.Account));

            Assert.IsFalse(managerResult.Error);

            var invoiceRepository = dbLayer.InvoiceRepository.FirstOrDefault(i => i.Id == invoice.Id);
            var accountRepository = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == account.AccountId);
            var payment = dbLayer.PaymentRepository.All().Where(w => w.AccountId == account.AccountId).ToList();

            Assert.IsNotNull(payment);

            var balance = accountRepository.BillingAccount.Balance;
            var bonusBalance = accountRepository.BillingAccount.BonusBalance;

            Assert.AreEqual(InvoiceStatus.Processed.ToString(), invoiceRepository.State);
            Assert.AreEqual(bonus, bonusBalance);
            Assert.AreEqual(balance, sum);
        }

        /// <summary>
        /// Получить модель подтверждения счета
        /// </summary>
        /// <param name="invoice">Модель счета</param>
        /// <param name="account">Модель аккаунта</param>
        /// <returns>Модель подтверждения счета</returns>
        private ConfirmationInvoiceDto GetConfirmationInvoiceDto(Invoice invoice, Account account)
        {
            return new ConfirmationInvoiceDto
            {
                ID = invoice.Id,
                AccountID = account.Id,
                System = PaymentSystem.Corp.ToString(),
                Date = DateTime.Now,
                sum = invoice.Sum,
                Description = nameof(Test),
                OriginDetails = "КОРП"
            };
        }
    }
}