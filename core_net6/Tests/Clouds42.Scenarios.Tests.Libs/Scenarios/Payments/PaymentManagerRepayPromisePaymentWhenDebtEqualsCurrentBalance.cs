﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Payments
{
    /// <summary>
    /// Проверяем возможность сразу же погасить ОП, если текущий баланс равен сумме задолженности по ОП
    /// </summary>
    public class PaymentManagerRepayPromisePaymentWhenDebtEqualsCurrentBalance : ScenarioBase
    {
        public override void Run()
        {
            var adManager = TestContext.ServiceProvider.GetRequiredService<ActiveDirectoryProviderFakeSuccess>();

            //добавление аккаунтa с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var accountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId;

            var invoiceRequest = new CalculationOfInvoiceRequestModelTest();
            TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().CreatePromisePayment(accountId, invoiceRequest);

            var billingAcc = DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == accountId);
            var resourceConfig = DbLayer.ResourceConfigurationRepository.FirstOrDefault(y => y.AccountId == accountId && y.Cost > 0);

            Assert.IsNotNull(billingAcc.PromisePaymentSum, "Promise payment has not been created");
            Assert.AreEqual(billingAcc.Balance, invoiceRequest.SuggestedPayment.PaymentSum,
                "Счёт пополнился на не верную сумму или произошли списания");

            Assert.IsFalse(resourceConfig.FrozenValue, "Сервис должен быть активным");

            if(resourceConfig.ExpireDate.HasValue && resourceConfig.ExpireDate.Value.Date == DateTime.Now.AddMonths(1).Date)
                throw new InvalidOperationException("Дата сместилась при взятии обещанного платежа");

            var viewModel = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>().GetBillingData(accountId);
            Assert.IsTrue(viewModel.Result.BillingData.CanEarlyPromisePaymentRepay,"Должна быть возможность погасить ОП");

            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.AccountId == accountId);

            var groupName = $"company_{accountUser.Account.IndexNumber}_web";
            //проверка что есть права
            var userExist = adManager.ExistUserAtGroup(accountUser.Login, groupName);

            Assert.IsNotNull(userExist, $"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");

            userExist = adManager.ExistUserAtGroup(accountUser.Login, "Remote Desktop Access");
            Assert.IsNotNull(userExist, $"Пользователь {accountUser.Login} не добавлен в группу Remote Desktop Access или группы нет");
        }
    }
}
