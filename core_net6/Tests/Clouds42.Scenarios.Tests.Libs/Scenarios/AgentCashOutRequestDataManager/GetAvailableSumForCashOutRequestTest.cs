﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Helpers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices.Contracts;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.CloudServices;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgentCashOutRequestDataManager
{
    /// <summary>
    /// Получить сумму вознаграждения агента, доступную для выплаты
    /// Сценарий:
    ///         1. Создаем аккаунт и сервис
    ///         2. Создаем рефреала этого аккаунта
    ///         3. Отключаем Аренду и сервис рефералу
    ///         4. Создаем платеж
    ///         5. Получаем актуальное агентское вознаграждение///         
    ///         6. Создаем реквизиты агента (нужно для рачета вознаграждения)
    ///         7. Получаем сумму агентского вознаграждения 
    /// </summary>
    public class GetAvailableSumForCashOutRequestTest : ScenarioBase
    {
        private readonly CreatePaymentManager _createPaymentManager;
        private readonly CreateAgentCashOutRequestManager _agentCashOutRequestManager;
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly EditAgentRequisitesManager _editAgentRequisites;
        private readonly Rent1CManageHelper _rent1CManager;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;
        private readonly Cloud42ServiceFactory _cloud42ServiceFactory;
        private readonly Billing.Billing.Providers.BillingServiceDataProvider _billingServiceDataProvider;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;

        public GetAvailableSumForCashOutRequestTest()
        {
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
            _agentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _editAgentRequisites = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
            _rent1CManager = (Rent1CManageHelper)TestContext.ServiceProvider.GetRequiredService<IRent1CManageHelper>();
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
            _cloud42ServiceFactory = (Cloud42ServiceFactory)TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceFactory>();
            _billingServiceDataProvider = (Billing.Billing.Providers.BillingServiceDataProvider)TestContext.ServiceProvider.GetRequiredService<IBillingServiceDataProvider>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _activateServiceForRegistrationAccountProvider =
                TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var standardResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var serviceTypeCost = 1000;

            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var serviceTypeDtos =
                createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standardResourceTypeId);
            serviceTypeDtos[0].ServiceTypeCost = serviceTypeCost;
            serviceTypeDtos[0].BillingType = BillingTypeEnum.ForAccountUser;

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceTypes = serviceTypeDtos
                });

            createBillingServiceCommand.Run();

            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);

            var account =
                dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == billingService.AccountOwnerId);

            var referral = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto{ReferralAccountId = account.Id}
            });
            referral.Run();
            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(billingService.Id,
                referral.AccountAdminId);

            var rent1CService = _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, referral.AccountId);
            rent1CService.ActivateService();

            TurnOffRent1CForAccount(referral.AccountId);
            TurnOffCustomService(createBillingServiceCommand.Id);

            const int paymentSum = 4000;

            var rent1CServiceId = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise).Id;

            var billingServiceTypeDtos =
                GetCalculateBillingServiceTypeDtos(referral.AccountAdminId, billingService);

            CreateInflowPayment(referral.AccountId, paymentSum, rent1CServiceId);

            _billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(referral.AccountId,
                createBillingServiceCommand.Id, false,
                null, billingServiceTypeDtos);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();
            Assert.IsNotNull(agreement);

            var rent1C = GetRent1CServiceOrThrowException();
            var rent1CCost = GetServiceCost(referral.Account, rent1C);
            var rent1CAgentReward = RewardForEachService(rent1CCost, agreement.Rent1CRewardPercent);
            var customServiceAgentReward = RewardForEachService(serviceTypeCost, agreement.ServiceOwnerRewardPercent);

            var agentRequisitesId = CreateAgentRequisites(account.Id);
            VerifyAgentRequisites(agentRequisitesId);

            var sum = _agentCashOutRequestManager.GetAvailableSumForCashOutRequest(account.Id);
            Assert.AreEqual(sum.Result, rent1CAgentReward + customServiceAgentReward);
        }

        /// <summary>
        /// Получить стоимость сервиса
        /// </summary>
        /// <param name="account">Модель аккаунта</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Стоимость сервиса</returns>
        private decimal GetServiceCost(Account account, IBillingService billingService)
        {
            var localeId = GetAccountLocaleId(account.Id);
            var standardRent1CCost = DbLayer.RateRepository.Where(r =>
                r.LocaleId == localeId &&
                r.BillingServiceType.ServiceId == billingService.Id &&
                r.AccountType == "Standart").Sum(r => r.Cost);

            return standardRent1CCost;
        }

        /// <summary>
        /// Отключить Арнду 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id акаунта</param>
        private void TurnOffRent1CForAccount(Guid accountId)
        {
            _rent1CManager.ManageRent1C(new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-10),
            }, accountId);
        }

        /// <summary>
        /// Создать входящий платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Сумма платежа</param>
        private void CreateInflowPayment(Guid accountId, decimal cost, Guid billingServiceId)
        {
            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingServiceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
        }

        /// <summary>
        /// Расчитать агентское вознаграждение для каждого сервиса
        /// </summary>
        /// <param name="sum">Стоимость сервиса</param>
        /// <param name="rewardPercent">Доля агентского вознаграждения</param>
        /// <returns></returns>
        private decimal RewardForEachService(decimal sum, decimal rewardPercent)
        {
            return sum * rewardPercent / 100;
        }

        /// <summary>
        /// Отключить кастомный сервис
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        private void TurnOffCustomService(Guid billingServiceId)
        {
            var customBillingService =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                    w => w.BillingServiceId == billingServiceId);
            customBillingService.IsDemoPeriod = false;
            customBillingService.ExpireDate = null;
            customBillingService.Frozen = null;
            DbLayer.ResourceConfigurationRepository.Update(customBillingService);
            DbLayer.Save();
        }

        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id агентских реквизитов</returns>
        private Guid CreateAgentRequisites(Guid accountId)
        {
            var requisitesFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = accountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г.Тест, ул.Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            return agentRequisitesId;
        }

        /// <summary>
        /// Подтвердить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId"> Id реквизитов агента</param>
        private void VerifyAgentRequisites(Guid agentRequisitesId)
        {
            var agentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = agentRequisitesId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified
            };

            var managerResult = _editAgentRequisites.ChangeAgentRequisitesStatus(agentRequisitesDto);
            Assert.IsNotNull(managerResult.Error);
        }

        /// <summary>
        /// Получить список моделей для переключения
        /// </summary>
        /// <param name="accountAdminId">Id админа аккаунта</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Список моделей для переключения</returns>
        private List<CalculateBillingServiceTypeDto> GetCalculateBillingServiceTypeDtos(Guid accountAdminId,
            BillingService billingService)
        {
            var account = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountAdminId).Account;

            return
            [
                new()
                {
                    Subject = accountAdminId,
                    Status = true,
                    BillingServiceTypeId = billingService.BillingServiceTypes.First().Id,
                    Sponsorship = new AccountUserBillingServiceTypeSponsorshipDto
                    {
                        I = false, Me = false, Label = account.AccountCaption
                    }
                }
            ];
        }
    }
}
