﻿using System;
using System.Linq;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgentCashOutRequestDataManager
{
    /// <summary>
    /// Сценарий теста получения заявок на расходование средств в статусе "Новая"
    /// 1) Создаем два аккаунта и для них платежи и реквизиты
    /// 2) Выполняем выборку и ожидаем что в выборке не будет данных, так как нет не одной заявки
    /// 3) Создаем первому аккаунту заявку и выполняем выборку получая 1 запись.Сравниваем данные
    /// 4) Создаем второму аккаунту завяку и выполняем выборку получая 2 записи
    /// 5) Первому аккаунту меняем статус заявки на "Проведена" и выполняем выборку получая одну запись. Сравниваем данные
    /// </summary>
    public class ScenarioGetAgentCashOutRequestsInStatusNewTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var secondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = $"TestLogin{DateTime.Now:mmss}",
                Email = "TestEmail22@efsol.ru",
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1)
            });
            secondAccountCommand.Run();

            var defaultSum = 2000;
            
            var agentCashOutRequestDataManager =
                ServiceProvider.GetRequiredService<AgencyAgreement.Partner.Managers.PartnerServices.AgentCashOutRequestDataManager>();
            
            var createAgentCashOutRequestManager = ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var managerResult = agentCashOutRequestDataManager.GetAgentCashOutRequestsInStatusNew();
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(0,managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.Count);

            var agentPaymentManager = ServiceProvider.GetRequiredService<AgentPaymentManager>();

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = createAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = createAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            agentPaymentManager.CreateAgentPayment(new AgencyPaymentCreationDto
            {
                PaymentType = PaymentType.Inflow,
                Sum = defaultSum,
                AccountNumber = secondAccountCommand.Account.IndexNumber,
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                Comment = "тест",
                Date = DateTime.Now,
                AgentAccountId = secondAccountCommand.AccountId,
                ClientPaymentSum = defaultSum
            });

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = createAccountCommand.AccountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            agentRequisitesDto.AccountId = secondAccountCommand.AccountId;
            var agentRequisitesForSecondAccountId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesForSecondAccountId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == createAccountCommand.AccountId);
            Assert.IsNotNull(agentRequisitesInDb);

            var agentRequisitesForSecondAccountInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesForSecondAccountId && w.AccountOwnerId == secondAccountCommand.AccountId);
            Assert.IsNotNull(agentRequisitesForSecondAccountInDb);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            agentRequisitesForSecondAccountInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesForSecondAccountInDb);
            DbLayer.Save();

            var agentCashOutRequestDto = new CreateAgentCashOutRequestDto
            {
                AccountId = createAccountCommand.AccountId,
                Files = requisitesFiles,
                TotalSum = defaultSum,
                RequestStatus = AgentCashOutRequestStatusEnum.New,
                AgentRequisitesId = agentRequisitesId,
                PaySum = defaultSum
            };

            createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequestDto);

            var cashOutRequest = DbLayer.AgentCashOutRequestRepository.FirstOrDefault() ??
                                 throw new NotFoundException("Не удалось найти заявку на расходование средств");

            managerResult = agentCashOutRequestDataManager.GetAgentCashOutRequestsInStatusNew();
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(1,managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.Count);
            CompareAgentCashOutRequestData(cashOutRequest, cashOutRequest.AgentRequisites,
                managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.First());

            agentCashOutRequestDto.AccountId = secondAccountCommand.AccountId;
            agentCashOutRequestDto.AgentRequisitesId = agentRequisitesForSecondAccountInDb.Id;
            createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequestDto);

            var cashOutRequestForSecondAccount = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(cr => cr.AgentRequisites.AccountOwnerId == secondAccountCommand.AccountId) ??
                                 throw new NotFoundException("Не удалось найти заявку на расходование средств");

            managerResult = agentCashOutRequestDataManager.GetAgentCashOutRequestsInStatusNew();
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(2, managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.Count);

            cashOutRequest.RequestStatus = AgentCashOutRequestStatusEnum.Paid;
            DbLayer.AgentCashOutRequestRepository.Update(cashOutRequest);
            DbLayer.Save();

            managerResult = agentCashOutRequestDataManager.GetAgentCashOutRequestsInStatusNew();
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(1,managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.Count);

            CompareAgentCashOutRequestData(cashOutRequestForSecondAccount, cashOutRequestForSecondAccount.AgentRequisites,
                managerResult.Result.AgentCashOutRequestDatas.AgentCashOutRequestDataList.First());
        }

        /// <summary>
        /// Сравнить данные заявки
        /// </summary>
        /// <param name="agentCashOutRequest">Завяка на расходование средств</param>
        /// <param name="agentRequisites">Реквизиты по которым создана заявка</param>
        /// <param name="agentCashOutRequestData">Заявка из выборки со статусом "Новая"</param>
        private void CompareAgentCashOutRequestData(AgentCashOutRequest agentCashOutRequest, AgentRequisites agentRequisites,
            AgentCashOutRequestDataDto agentCashOutRequestData)
        {
            Assert.AreEqual(agentCashOutRequestData.AccountNumber, agentRequisites.AccountOwner.IndexNumber);
            Assert.AreEqual(agentCashOutRequestData.CreationDateTime, agentCashOutRequest.CreationDateTime);
            Assert.AreEqual(agentCashOutRequestData.RequestNumber, agentCashOutRequest.RequestNumber);

            Assert.AreEqual(agentCashOutRequestData.Sum, agentCashOutRequest.RequestedSum);
            Assert.AreEqual(agentCashOutRequestData.SupplierCode, agentRequisites.AccountOwner.AccountConfiguration.Supplier.Code);
            Assert.AreEqual(3,agentCashOutRequestData.AgentReportLinks.Links.Count);

            var agentRequisitesData = agentCashOutRequestData.AgentRequisitesForPayment;
            Assert.AreEqual(agentRequisitesData.BankName, agentRequisites.LegalPersonRequisites.BankName);
            Assert.AreEqual(agentRequisitesData.Inn, agentRequisites.LegalPersonRequisites.Inn);
            Assert.AreEqual(agentRequisitesData.Recipient, agentRequisites.LegalPersonRequisites.OrganizationName);
            Assert.AreEqual(agentRequisitesData.SettlementAccount, agentRequisites.LegalPersonRequisites.SettlementAccount);
        }
    }
}
