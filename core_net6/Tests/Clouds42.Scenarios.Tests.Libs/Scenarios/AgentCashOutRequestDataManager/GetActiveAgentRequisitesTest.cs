﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgentCashOutRequestDataManager
{
    /// <summary>
    /// Тест получения списка активных реквизитов агента
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Создаем реквизиты агента в статусе "На модерации" (юр и физ лицо)
    ///         3. Получаем количество активных реквизитов(должно быть 0)
    ///         4. Меняем статус реквизитов на "Проверено"
    ///         3. Получаем количество активных реквизитов(должно быть 2)
    /// </summary>
    public class GetActiveAgentRequisitesTest : ScenarioBase
    {
        private readonly CreateAgentCashOutRequestManager _agentCashOutRequestManager;
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly EditAgentRequisitesManager _editAgentRequisites;

        public GetActiveAgentRequisitesTest()
        {
            _agentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _editAgentRequisites = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            
            var legalRequisitesFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();
            var legalPersonRequisitesId = CreateAgentRequisites(createAccountCommand.AccountId, legalRequisitesFiles, AgentRequisitesTypeEnum.LegalPersonRequisites);


            var physicalRequisitesFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForPhysicalPersonRequisites();
            var physicalRequisitesId = CreateAgentRequisites(createAccountCommand.AccountId, physicalRequisitesFiles, AgentRequisitesTypeEnum.PhysicalPersonRequisites);

            Assert.AreEqual(0, GetActiveAgentRequisitesCount(createAccountCommand.AccountId));

            ChangeAgentRequisitesStatus(legalPersonRequisitesId, AgentRequisitesStatusEnum.Verified);
            Assert.AreEqual(1, GetActiveAgentRequisitesCount(createAccountCommand.AccountId));

            ChangeAgentRequisitesStatus(physicalRequisitesId, AgentRequisitesStatusEnum.Verified);
            Assert.AreEqual(2, GetActiveAgentRequisitesCount(createAccountCommand.AccountId));

            ChangeAgentRequisitesStatus(legalPersonRequisitesId, AgentRequisitesStatusEnum.Draft);
            Assert.AreEqual(1, GetActiveAgentRequisitesCount(createAccountCommand.AccountId));

        }

        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id агентских реквизитов</returns>
        private Guid CreateAgentRequisites(Guid accountId, List<CloudFileDataDto<byte[]>> requisitesFiles, AgentRequisitesTypeEnum agentRequisitesTypeEnum)
        {
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = accountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = agentRequisitesTypeEnum,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г.Тест, ул.Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            return agentRequisitesId;
        }

        /// <summary>
        /// Подтвердить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId"> Id реквизитов агента</param>
        private void ChangeAgentRequisitesStatus(Guid agentRequisitesId, AgentRequisitesStatusEnum requisitesStatus)
        {
            var agentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = agentRequisitesId,
                AgentRequisitesStatus = requisitesStatus
            };

            var managerResult = _editAgentRequisites.ChangeAgentRequisitesStatus(agentRequisitesDto);
            Assert.IsNotNull(managerResult.Error);
        }

        /// <summary>
        /// Получить количество активных реквизитов агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Количество активных реквизитов агента</returns>
        private int GetActiveAgentRequisitesCount(Guid accountId)
        {
            var activeAgentRequisites = _agentCashOutRequestManager.GetActiveAgentRequisites(accountId);
            Assert.IsFalse(activeAgentRequisites.Error);

            return activeAgentRequisites.Result.Count;
        }
    }
}
