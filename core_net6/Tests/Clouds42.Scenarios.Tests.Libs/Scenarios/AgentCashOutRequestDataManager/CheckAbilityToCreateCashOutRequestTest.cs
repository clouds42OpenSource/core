﻿using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AgentCashOutRequestDataManager
{
    /// <summary>
    /// Проверить возможность создания заявки на вывод
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Создаем реквизиты агента в статусе на модерации, проеряем возможность создания 
    ///         2. Добавляем 400 руб на кошелек агента, проеряем возможность создания
    ///         2. Меняем статус реквизитов агента на Проверено, проеряем возможность создания 
    ///         3. Добавляем 1500 руб на кошелек агента, проеряем возможность создания
    /// </summary>
    public class CheckAbilityToCreateCashOutRequestTest : ScenarioBase
    {
        private readonly CreateAgentCashOutRequestManager _agentCashOutRequestManager;
        private readonly CreateAgentRequisitesManager _createAgentRequisitesManager;
        private readonly FakeAgencyDocumentFileHelper _fakeAgencyDocumentFileHelper;
        private readonly EditAgentRequisitesManager _editAgentRequisites;
        public CheckAbilityToCreateCashOutRequestTest()
        {
            _agentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            _createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            _fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            _editAgentRequisites = TestContext.ServiceProvider.GetRequiredService<EditAgentRequisitesManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var agentRequisites = CreateAgentRequisites(createAccountCommand.AccountId);

            CheckAbilityToCreateCashOutRequest(createAccountCommand.AccountId, false);

            ChangeAgentWalletSum(createAccountCommand.AccountId, 400);

            CheckAbilityToCreateCashOutRequest(createAccountCommand.AccountId, false);
            
            VerifyAgentRequisites(agentRequisites);

            CheckAbilityToCreateCashOutRequest(createAccountCommand.AccountId, false);

            ChangeAgentWalletSum(createAccountCommand.AccountId, 1500);

            CheckAbilityToCreateCashOutRequest(createAccountCommand.AccountId, true);
        }

        /// <summary>
        /// Создать реквизиты агента
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id агентских реквизитов</returns>
        private Guid CreateAgentRequisites(Guid accountId)
        {
            var requisitesFiles = _fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = accountId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Draft,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г.Тест, ул.Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = _createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            return agentRequisitesId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="expectedResult"></param>
        private void CheckAbilityToCreateCashOutRequest(Guid accountId, bool expectedResult)
        {
            var managerResult = _agentCashOutRequestManager.CheckAbilityToCreateCashOutRequest(accountId);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(managerResult.Result, expectedResult);
        }

        /// <summary>
        /// Подтвердить реквизиты агента
        /// </summary>
        /// <param name="agentRequisitesId"> Id реквизитов агента</param>
        private void VerifyAgentRequisites(Guid agentRequisitesId)
        {
            var agentRequisitesDto = new EditAgentRequisitesDto
            {
                Id = agentRequisitesId,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified
            };

            var managerResult = _editAgentRequisites.ChangeAgentRequisitesStatus(agentRequisitesDto);
            Assert.IsNotNull(managerResult.Error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="sum"></param>
        private void ChangeAgentWalletSum(Guid accountId, decimal sum)
        {
            var agentWallet =
                DbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == accountId);
            Assert.IsNotNull(agentWallet);

            agentWallet.AvailableSum = sum;

            DbLayer.AgentWalletRepository.Update(agentWallet);
            DbLayer.Save();
        }
    }
}
