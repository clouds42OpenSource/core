﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.StateMachine.Actions.UpdateAccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий редактирования данных пользователя
    /// 1. Создаём аккаунт и пользователя с ролью AccountUser
    /// 2. Сохраняем пользователя без изменений и проверяем отсутствие записи об изменении
    /// 3. Сохраняем пользователя с изменением простых полей и проверяем наличие записи об
    ///     изменении а так же наличие сведений об изменённых полях
    /// 4. Сохраняем пользователя с изменением вложенного поля (роли) и проверяем наличие
    ///     записи об изменении а так же наличие сведений об изменённом поле
    /// </summary>

    public class AccountUserChangeLoggingTest : ScenarioBase
    {
        public override void Run()
        {
            var profileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var accountUserDataProvider = ServiceProvider.GetRequiredService<IAccountUserDataProvider>();
            var accountUserUpdateProvider = ServiceProvider.GetRequiredService<IAccountUserUpdateProvider>();
            var action = new EditAccountUserInDatabaseAction(DbLayer, AccessProvider, accountUserDataProvider, accountUserUpdateProvider, Logger, HandlerException);

            #region 1. Создаём аккаунт и пользователя с ролью AccountUser

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var randomSuffix = new Random().Next(100, 1000).ToString();
            var accountUserTest = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = $"TestLogin{randomSuffix}",
                Email = $"TestEmail{randomSuffix}@efsol.ru",
                FirstName = $"TestName{randomSuffix}",
            };
            var accountUserAddingResult = profileManager.AddToAccount(accountUserTest).Result;
            Assert.IsFalse(accountUserAddingResult.Error, "Не удалось добавить к аккаунту второго пользователя: " + accountUserAddingResult.Message);

            var accountUser = DbLayer.AccountUsersRepository
                .FirstOrThrowException(x => x.Id == accountUserAddingResult.Result, "Второй аккаунт отсутствует в БД");

            #endregion

            #region 2. Сохраняем пользователя без изменений и проверяем отсутствие записи об изменении

            var dataContract = new AccountUserDto
            {
                Id = accountUser.Id,
                AuthToken = accountUser.AuthToken,
                Email = accountUser.Email,
                PhoneNumber = accountUser.PhoneNumber,
                FirstName = accountUser.FirstName,
                MiddleName = accountUser.MiddleName,
                LastName = accountUser.LastName,
                Login = accountUser.Login,
                Roles = [AccountUserGroup.AccountUser]
            };
            action.Do(dataContract);

            var lastChange = DbLayer.CloudChangesRepository.OrderByDescending(x => x.Date).First();
            Assert.AreNotEqual(LogActions.EditUserCard, lastChange.CloudChangesAction.IndexEnum);

            #endregion

            #region 3. Сохраняем пользователя с изменением простых полей и проверяем наличие записи об изменении а так же наличие сведений об изменённых полях

            dataContract.Email += "123";
            dataContract.FirstName += "123";
            action.Do(dataContract);

            lastChange = DbLayer.CloudChangesRepository.OrderByDescending(x => x.Date).First();
            Assert.AreEqual(LogActions.EditUserCard, lastChange.CloudChangesAction.IndexEnum, "Нет записи об изменении пользователя");
            Assert.IsTrue(lastChange.Description.Contains("Изменена электронная почта"), "Нет сведений об изменении электронной почты");
            Assert.IsFalse(lastChange.Description.Contains("Изменен телефон"), "Ложные сведения об изменении телефона");
            Assert.IsTrue(lastChange.Description.Contains("Изменено имя"), "Нат сведений об изменении имени");
            Assert.IsFalse(lastChange.Description.Contains("Изменено отчество"), "Ложные сведения об изменении отчества");
            Assert.IsFalse(lastChange.Description.Contains("Изменена фамилия"), "Ложные сведения об изменении фамилии");
            Assert.IsFalse(lastChange.Description.Contains("Изменен логин"), "Ложные сведения об изменении логина");
            Assert.IsFalse(lastChange.Description.Contains("Изменены роли доступа"), "Ложные сведения об изменении ролей");

            #endregion

            #region 4. Сохраняем пользователя с изменением вложенного поля (роли) и проверяем наличие записи об изменении а так же наличие сведений об изменённом поле
            dataContract.Roles.Add(AccountUserGroup.AccountAdmin);
            action.Do(dataContract);

            var previousChangeId = lastChange.Id;
            lastChange = DbLayer.CloudChangesRepository.OrderByDescending(x => x.Date).First();
            Assert.AreNotEqual(previousChangeId, lastChange.Id, "Нет сведений о последующем изменении пользователя");
            Assert.IsFalse(lastChange.Description.Contains("Изменена электронная почта"), "Ложные сведения об изменении электронной почты");
            Assert.IsFalse(lastChange.Description.Contains("Изменен телефон"), "Ложные сведения об изменении телефона");
            Assert.IsFalse(lastChange.Description.Contains("Изменено имя"), "Ложные сведения об изменении имени");
            Assert.IsFalse(lastChange.Description.Contains("Изменено отчество"), "Ложные сведения об изменении отчества");
            Assert.IsFalse(lastChange.Description.Contains("Изменена фамилия"), "Ложные сведения об изменении фамилии");
            Assert.IsFalse(lastChange.Description.Contains("Изменен логин"), "Ложные сведения об изменении логина");
            Assert.IsTrue(lastChange.Description.Contains("Изменены роли доступа"), "Нет сведений об изменении ролей");

            #endregion
        }
    }
}
