﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий изменения данных на валидные данные
    /// Сценарий:
    ///         1) Создаем аккаунт
    ///         2) Редактируем пользователя этого аккаунта
    ///         3) Проверяем, что пользователь отредактирован
    ///         4) Проверяем, что пользователь состоит в группах АД
    /// </summary>
    public class SuccessEditAccountUser : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        public SuccessEditAccountUser()
        {
            _accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        private string Login { get; } = $"Test{new Random().Next(1, 1000)}InAdNum";

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            
            var updatedModel = new AccountUserDto
            {
                Id = createAccountCommand.AccountAdminId,
                AccountId = createAccountCommand.AccountId,
                AccountCaption = createAccountCommand.Account.AccountCaption,
                Login = Login,
                Password = "Qwerty_123",
                Email = "TestEmail@efsol.ru",
                FirstName = "TestFirstName",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };
            _accountUsersProfileManager.Update(updatedModel);

            if (updatedModel.Id == null)
                throw new NullReferenceException();

            var editedUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(editedUser, $"Не найден пользователь по Id {createAccountCommand.AccountAdminId}");
            Assert.AreEqual(updatedModel.Login, editedUser.Login, "Пользователь не отредактирован");


        }
    }
}
