﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    ///     Сценарий изменения данных другого пользователя на валидные данные
    /// пользователем с правами клауд админна 
    /// 
    ///     Входные параметры: создание пользователя, предоставление ему роли
    /// CloudAdmin, добавление пользователя с ролью AccountUser
    /// 
    ///     Действия: Изменяем данные добавленого пользователя, новая роль - AccountAdmin
    ///     
    ///     Проверка: Роль добавленого пользователя - AccountAdmin, из групп 
    /// не удалился, изменились данные только те которые меняли
    /// </summary>
    public class CloudAdminAddEditAndDeleteAccountUser : ScenarioBase
    {
        private string Login { get; } = $"Test{new Random().Next(1, 1000)}InAdNum";
        private string LoginSecondUser { get; } = $"LoginSecondUser{DateTime.Now:mmss}";

        private const AccountUserGroup NewUserRole = AccountUserGroup.AccountAdmin;
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        public CloudAdminAddEditAndDeleteAccountUser()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

	        var accountAdminId = createAccountCommand.AccountAdminId;

	        var adminUser = DbLayer.AccountUsersRepository.GetAccountUser(accountAdminId) 
                            ?? throw new NotFoundException($"Пользователь-администратор аккаунта c id={accountAdminId} не найден.");

            ChangeUserRoles(adminUser);

            RefreshDbCashContext(Context.AccountUsers);
            RefreshDbCashContext(Context.AccountUserRoles);

            var secondUser = new AccountUserRegistrationToAccountTest
	        {
	            AccountId = adminUser.Account.Id,
	            AccountIdString = SimpleIdHash.GetHashById(adminUser.Account.Id),
	            Login = LoginSecondUser,
	            Email = LoginSecondUser+"@efsol.ru",
	            FirstName = LoginSecondUser+"Name"
            };

			var result = _accountUsersProfileManager.AddToAccount(secondUser).Result;
            CreateUserSession(result.Result);

            if (result.Error)
		        throw new InvalidOperationException($"Ошибка создания нового пользователя. {result.Message}");

            RefreshDbCashContext(Context.AccountUsers);
            RefreshDbCashContext(Context.AccountUserRoles);
            ChangeAccountUserProperties(result.Result);

            _accountUsersProfileManager.Delete(result.Result);

            var editedUser = DbLayer.AccountUsersRepository.GetByLogin(Login);
            Assert.IsNull(editedUser, "Пользователь не был удален!!");

            var roles = DbLayer.AccountUserRoleRepository.Where(rol => rol.AccountUserId == result.Result);
            Assert.IsTrue(!roles.Any(), "Роли не должны оставатся после удаления пользователя!!");
        }

        /// <summary>
        /// Сменить данные пользователя
        /// </summary>
        /// <param name="accountAdminId">ID админа аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        private void ChangeAccountUserProperties(Guid accountUserId)
        {
            var updatedModel = new AccountUserDto
            {
                Id = accountUserId,
                AccountCaption = "Test account",
                Login = LoginSecondUser,
                Password = "Qwerty_123",
                Email = "TestEmail@efsol.ru",
                FirstName = "TestFirstName",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Roles = [NewUserRole]
            };

            _accountUsersProfileManager.Update(updatedModel);

            if (updatedModel.Id == null)
                throw new InvalidOperationException("Не указан ID пользователя");

            RefreshDbCashContext(Context.AccountUsers);
            RefreshDbCashContext(Context.AccountUserRoles);

            var editedUser = DbLayer.AccountUsersRepository.GetAccountUser(updatedModel.Id.Value);
            Assert.IsNotNull(editedUser);
            Assert.AreEqual(updatedModel.PhoneNumber, editedUser.PhoneNumber);
        }

        /// <summary>
        /// Создать сессию пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        private void CreateUserSession(Guid accountUserId)
        {
            var accountUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, 
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = accountUserId
            };

            DbLayer.AccountUserSessionsRepository.Insert(accountUserSession);
            DbLayer.Save();
        }

        /// <summary>
        /// Сменить роли для пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        private void ChangeUserRoles(AccountUser accountUser)
        {
            if (accountUser.AccountUserRoles.Count != 1)
                throw new InvalidOperationException($"У пользователя c id={accountUser.Id} существует более чем одна роль");

            var userFirstRole = accountUser.AccountUserRoles.FirstOrDefault()
                                ?? throw new InvalidOperationException($"У пользователя c id={accountUser.Id} не найдена по крайней мере одна роль");

            userFirstRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(userFirstRole);
            DbLayer.Save();
        }

        /// <summary>
        /// Очистить мусор
        /// </summary>
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(Login);
            ActiveDirectoryUserFunctions.DeleteUser(LoginSecondUser);
        }
    }
}
