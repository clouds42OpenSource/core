﻿#region Libr
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
#endregion Libr

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    public class AccountUserDeactivateTest : ScenarioBase
    {
        public override void Run()
        {
            //добавление аккаунтa с включеной арендой 1С и базой
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>();

            var accountUserModel = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                AccountIdString =
                    SimpleIdHash.GetHashById(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "usertest1@mail.ru",
                FirstName = "UserTest1",
                FullPhoneNumber = PhoneNumberGeneratorHelperTest.GenerateWithPrefixCode(1)
            };

            var res = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(accountUserModel).Result;
            if (res == null)
                throw new InvalidOperationException("Ошибка создания нового пользователя");
            accessRent1C.Add(new UpdaterAccessRent1CRequestDto
            {
                AccountUserId = res.Result,
                StandartResource = true
            });

            var dbAccess = new AcDbAccessPostAddAllUsersModelDto
            {
                AccountDatabaseID = createAccountDatabaseAndAccountCommand.AccountDatabaseId,
                AccountID = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                LocalUserID = Guid.Empty
            };
            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);
            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 2000,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });
            //подключение аренды 1С для аккаунта
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            var resRent1C =
                rent1CConfigurationAccessManager.ConfigureAccesses(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                    accessRent1C);
            //Добавить доступ к базе
            var result = TestContext.ServiceProvider.GetRequiredService<AcDbAccessManager>().AddAccessesForAllAccountsUsers(dbAccess);
            if (result.Error || result.State != ManagerResultState.Ok)
                throw new InvalidOperationException("Ошибка предоставления доступа к базе");

            var accountUser = DbLayer.AccountUsersRepository.GetFirst(au => au.Id == res.Result);

            //Деактивируем пользователя
            var accountUserActivateProvider = ServiceProvider.GetRequiredService<AccountUserActivateProvider>();
            accountUserActivateProvider.SetActivateStatusForUser(accountUser, false);

            // assert
            var resourceRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);


            var billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            var accessList =
                (from access in DbLayer.AcDbAccessesRepository.WhereLazy()
                 where access.AccountUserID == accountUser.Id
                 select access).ToList();

            if (accessList.Any(t => !t.IsLock))
                throw new InvalidOperationException("Ошибка спонсирования аренды 1С");

            var groupName = $"company_{accountUser.Account.IndexNumber}_web";

            var userExist = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(accountUser.Login, groupName);
            if (userExist)
                throw new NotFoundException(
                    $"Пользователь {accountUser.Login} не добавлен в группу {groupName} или группы нет");

            Assert.IsNull(resRent1C.Result.ErrorMessage);

            Assert.IsNotNull(billingAccount);

            Assert.IsNotNull(resourceRent1C);

            Assert.AreEqual(1600, billingAccount.Balance);

            Assert.AreEqual(false, resourceRent1C.Frozen);

            Assert.AreEqual(DateTime.Now.AddDays(7).ToShortDateString(),
                resourceRent1C.ExpireDateValue.ToShortDateString());

            Assert.AreEqual(1500, resourceRent1C.Cost);
        }
    }
}
