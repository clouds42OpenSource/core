﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Тест проверки текущего пароля пользователя
    /// Сценарий:
    ///         1. Создаем аккаунт и добавляем в него 3 пользователей
    ///         2. Проверяем пароли для первого и второго пользвателя указываем правильный пароль
    ///            для админа аккаунта и 3-го пользователя не правильный
    ///         3. ПРоверяем, что везеде вернулся правильный результат проверки
    /// </summary>
    public class CheckUserCurrentPasswordTest : ScenarioBase
    {
        private readonly AccountUserManager _accountUserManager;

        public CheckUserCurrentPasswordTest()
        {
            _accountUserManager = TestContext.ServiceProvider.GetRequiredService<AccountUserManager>();
        }

        public override void Run()
        {
            var wrongPassword = "!Qwerty_123";
            var password1 = "Qwerty_123";
            var password2 = "mkonji_09";

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountUser1 = GetAccountUserDc(createAccountCommand.AccountId, password1);
            var userId1 = AddUserToAccount(accountUser1);

            var accountUser2 = GetAccountUserDc(createAccountCommand.AccountId, password2);
            var userId2 = AddUserToAccount(accountUser2);

            var accountUser3 = GetAccountUserDc(createAccountCommand.AccountId, password1);
            var userId3 = AddUserToAccount(accountUser3);

            var checkPasswordResult = CheckCurrentPassword(createAccountCommand.AccountAdminId, wrongPassword);
            Assert.IsFalse(checkPasswordResult);

            var checkPasswordResult1 = CheckCurrentPassword(userId1, password1);
            Assert.IsTrue(checkPasswordResult1);

            var checkPasswordResult2 = CheckCurrentPassword(userId2, password2);
            Assert.IsTrue(checkPasswordResult2);

            var checkPasswordResult3 = CheckCurrentPassword(userId3, wrongPassword);
            Assert.IsFalse(checkPasswordResult3);
        }

        /// <summary>
        /// Проверить текущий пароль пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="password">Пароль</param>
        /// <returns>Результат проверки</returns>
        private bool CheckCurrentPassword(Guid userId, string password)
        {
            var checkCurrentPassword = _accountUserManager.CheckCurrentPassword(userId, password);
            Assert.IsFalse(checkCurrentPassword.Error);

            return checkCurrentPassword.Result;
        }

        /// <summary>
        /// Получить модель пользователя
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="password">Пароль</param>
        /// <returns>Модель пользователя</returns>
        private AccountUserDto GetAccountUserDc(Guid accountId, string password)
        {
            return new AccountUserDto
            {
                AccountId = accountId,
                Login = $"test_{DateTime.Now:mmssfff}",
                Password = password,
                Email = $"em_{Guid.NewGuid():N}@we.io",
                Roles = [AccountUserGroup.AccountUser]
            };
        }
    }
}
