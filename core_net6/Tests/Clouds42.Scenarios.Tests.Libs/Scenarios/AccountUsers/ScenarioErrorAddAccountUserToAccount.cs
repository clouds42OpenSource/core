﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий ошибочного добавления пользователя в аккаунт
    /// Действия:
    ///     1) Создаем аккаунт
    ///     2) Пытаемся добавить нового пользователя с заведомо невалидным паролем
    ///     3) Проверяем что пользователь не был создан
    /// </summary>
    public class ScenarioErrorAddAccountUserToAccount : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;

        public ScenarioErrorAddAccountUserToAccount()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var secondUserRegModel = new AccountUserRegistrationToAccountTest { Password = "11111111" };

            var secondUserAddResult = _accountUsersProfileManager.AddToAccount(secondUserRegModel).Result;
            
            Assert.IsNotNull(secondUserAddResult);
            Assert.IsTrue(secondUserAddResult.Error);
            Assert.AreEqual(secondUserAddResult.Result, Guid.Empty);
        }
    }
}
