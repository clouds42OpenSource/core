﻿using Clouds42.RuntimeContext;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Domain.Enums;

/// <summary>
/// Cценапий:
///     создаём аккаунт с подключенной арендой
///     подключаем пользователя к аккаунту и уаляем пользователя
///     проверяем состоит ли пользователь в группах AD
///     проверяем есть ли запись о пользователе в бд
/// </summary>

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    public class SuccesDeleteUserTest : ScenarioBase
    {
        private readonly ICoreExecutionContext _coreExecutionContext;
        public SuccesDeleteUserTest()
        {
            _coreExecutionContext = TestContext.ServiceProvider.GetRequiredService<ICoreExecutionContext>();
        }

        public override void Run()
        {
            TestContext.ServiceProvider.GetRequiredService<ICoreExecutionContext>();
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var newUserModel = new AccountUserDcTest
            {
                AccountCaption = "Test caption",
                AccountId = createAccountCommand.AccountId,
                Login = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Password = Guid.NewGuid().ToString(),
                FirstName = nameof(Test),
                LastName = nameof(Test),
                Email = $"{Guid.NewGuid().ToString("N")}@efsol.ru",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

            var accountUserId = ServiceProvider.GetRequiredService<AccountUsersProfileManager>().AddToAccount(newUserModel).Result;
            accountUsersProfileManager.Delete(accountUserId);

            var companyGroupName = DomainCoreGroups.CompanyGroupBuild(createAccountCommand.Account.IndexNumber);
            var remoteDesktopGroupName = DomainCoreGroups.RemoteDesktopAccess;
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(createAccountCommand.Account.IndexNumber);

            var userExistInAD = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, remoteDesktopGroupName);
            Assert.IsFalse(userExistInAD, $"Пользователь не должен быть в группе {remoteDesktopGroupName}");

            userExistInAD = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, $"Пользователь не должен быть в группе {webGroupName}");
            Assert.IsFalse(userExistInAD);

            userExistInAD = ActiveDirectoryCompanyFunctions.ExistUserAtGroup(newUserModel.Login, companyGroupName);
            Assert.IsFalse(userExistInAD, $"Пользователь не должен быть в группе {companyGroupName}");

            var userExistInDb = DbLayer.AccountUsersRepository.IsUserExist(newUserModel.Login);
            Assert.IsNull(userExistInDb, "Записи пользователя не должно быть в базе данных");
        }
    }
}
