﻿using System;
using System.Linq;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestManagers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    ///     Сценарий изменения своих данных пользователем с правами CloudAdmin
    /// на валидные данные и удаление пользователя
    ///
    ///     Входные параметры: создание пользователя, предоставление ему роли CloudAdmin
    ///
    ///     Действия: Изменяем данные
    ///
    ///     Проверка: Роль пользователя осталась прежняя, из групп не удалился,
    /// изменились данные только те которые меняли
    /// </summary>
    public class CloudAdminEditAndDeleteAccountUser : ScenarioBase
    {
        private string Login { get; } = $"Test{new Random().Next(1, 1000)}InAdNum";

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.GetAccountUser(accountAdminId);

            if (adminUser == null)
                throw new InvalidOperationException ($"Пользователь-администратор аккаунта c id={accountAdminId} не найден.");

            if (adminUser.AccountUserRoles.Count != 1)
                throw new InvalidOperationException ($"У пользователя c id={accountAdminId} существует более чем одна роль");

            var userFirstRole = adminUser.AccountUserRoles.FirstOrDefault();

            if (userFirstRole == null)
                throw new InvalidOperationException ($"У пользователя c id={accountAdminId} не найдена по крайней мере одна роль");

            userFirstRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(userFirstRole);
            DbLayer.Save();

            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var adManager = ServiceProvider.GetRequiredService<ActiveDirectoryProviderUserNotExistFakeSuccess>();
            var updatedModel = new AccountUserDto
            {
                Id = accountAdminId,
                AccountCaption = "Test account",
                Login = Login,
                Password = "Qwerty_123",
                Email = $"TestEmail{DateTime.Now}@efsol.ru",
                FirstName = "TestFirstName",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Roles = null
            };

            accountUsersProfileManager.Update(updatedModel);

            if (updatedModel.Id == null)
                throw new InvalidOperationException ("");

            var editedUser = DbLayer.AccountUsersRepository.GetAccountUser(updatedModel.Id.Value);

            Assert.IsNotNull(editedUser);
            Assert.AreEqual(updatedModel.PhoneNumber, editedUser.PhoneNumber);

            if (editedUser.AccountUserRoles.All(r => r.AccountUserGroup != userFirstRole.AccountUserGroup))
                throw new InvalidOperationException ("Роль пользователя не должна была изменится!!");

            accountUsersProfileManager.Delete(editedUser.Id);
            Assert.IsFalse(adManager.UsersExistsInDomain(editedUser.Login));

            editedUser = DbLayer.AccountUsersRepository.GetByLogin(Login);
            Assert.IsNull(editedUser, "Пользователь не был удален!!");

            var roles = DbLayer.AccountUserRoleRepository.Where(rol => rol.AccountUserId == updatedModel.Id.Value);
            Assert.IsTrue(!roles.Any(), "Роли не должны оставатся после удаления пользователя!!");
        }

        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(Login);
        }
    }
}
