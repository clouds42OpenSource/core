﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий изменения данных на невалидные данные, откат изменений и удаление пользователя
    /// </summary>
    public class RollbackEditAndDeleteAccountUser : ScenarioBase
    {
        private const string TestLogin = "TestLogin2ForTests";

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            
            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = createAccountCommand.AccountId,
                AccountIdString = SimpleIdHash.GetHashById(createAccountCommand.AccountId),
                Login = TestLogin,
                Email = "UserTestEmail009@efsol.ru",
                FirstName = "UserTestName009"

            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;

       
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };

            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();

            var updatedModel = new AccountUserDto
            {
                Id = res.Result,
                AccountCaption = "Test account",
                Login = TestLogin,
                Password = "1234567",
                Email = "TestEmail8@efsol.ru",
                FirstName = "TestFirstName111",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Roles =
                [
                    AccountUserGroup.AccountSaleManager,
                    AccountUserGroup.AccountUser,
                    AccountUserGroup.AccountAdmin,
                    AccountUserGroup.Hotline
                ]
            };

            try
            {
                accountUsersProfileManager.Update(updatedModel);
            }

            catch (Exception exception)
            {
                if (exception.Message!= "Не удалось сменить телефон")
                {
                    throw;
                }
            }
            
            var editedUser = DbLayer.AccountUsersRepository.GetAccountUser(updatedModel.Id.Value);

            Assert.IsNotNull(editedUser);
            Assert.AreEqual(TestLogin, editedUser.Login);
            accountUsersProfileManager.Delete(editedUser.Id);
            
            var action = DbLayer.CloudChangesActionRepository.FirstOrDefault(x => x.Index == (int)LogActions.RemoveUser);
            var cloudChange = DbLayer.CloudChangesRepository.FirstOrDefault(u =>
                u.AccountId == createAccountCommand.Account.Id &&
                u.Action == action.Id);
            Assert.IsNotNull(cloudChange);
        }

        /// <summary>
        /// Выполнить сборку мусора
        /// </summary>
        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(TestLogin);
        }
    }

}
