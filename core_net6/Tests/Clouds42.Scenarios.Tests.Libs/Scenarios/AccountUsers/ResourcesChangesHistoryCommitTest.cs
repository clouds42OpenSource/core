﻿using System;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.CloudServices.Contracts;
using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий фиксации изменений в подписке VIP аккаунта при отключении
    /// всех услуг пользователю с включенной услугой "Аренда 1С"
    /// 
    /// Действия:
    ///     1) Создаем аккаунт и пользователя
    ///     2) Подключаем пользователю услугу "Аренда 1С"
    ///     3) Отключаем пользователю сервисы
    ///     4) Проверяем, что факт изменения подписки у VIP аккаунта был зафиксирован
    /// </summary>
    public class ResourcesChangesHistoryCommitTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly Rent1CConfigurationAccessManager _rent1CConfigurationAccessManager;
        private readonly IMainServiceResourcesChangesHistoryProvider
            _mainServiceResourcesChangesHistoryProvider;
        private readonly AccountUserActivateProvider _accountUserActivateProvider;

        public ResourcesChangesHistoryCommitTest()
        {
            _accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();
            _mainServiceResourcesChangesHistoryProvider = ServiceProvider.GetRequiredService<IMainServiceResourcesChangesHistoryProvider>();
            _accountUserActivateProvider = ServiceProvider.GetRequiredService<AccountUserActivateProvider>();
        }

        public override void Run()
        {
            #region 1) Создаем аккаунт и пользователя

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var account = DbLayer.AccountsRepository
                .FirstOrThrowException(x => x.Id == createAccountCommand.AccountId);

            TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
                .UpdateSignVipAccount(account.Id, true);

            DbLayer.AccountsRepository.Update(account);

            var accountUserId = _accountUsersProfileManager
                .AddToAccount(new AccountUserRegistrationToAccountTest
                {
                    AccountId = account.Id
                })
                ?.Result.Result;

            Assert.IsNotNull(accountUserId, "Ошибка создания нового пользователя");

            #endregion

            #region 2) Подключаем пользователю услугу "Аренда 1С"

            _rent1CConfigurationAccessManager.TurnOnRent1C(account.Id, accountUserId);

            #endregion

            #region Предварительная проверка на отсутствие зафиксированных изменений на текущий момент

            var history = _mainServiceResourcesChangesHistoryProvider
                .GetMainServiceResourcesChangesHistory(account.Id, DateTime.Today);
            Assert.IsNull(history);

            #endregion

            #region 3) Отключаем пользователю сервисы

            var accountUser = DbLayer.AccountUsersRepository
                .FirstOrThrowException(x => x.Id == accountUserId);
            _accountUserActivateProvider.DisableAllBillingServices(accountUser);

            #endregion

            #region 4) Проверяем, что факт изменения подписки у VIP аккаунта был зафиксирован

            history = _mainServiceResourcesChangesHistoryProvider
                .GetMainServiceResourcesChangesHistory(account.Id, DateTime.Today);
            Assert.IsNotNull(history);
            Assert.IsTrue(history.DisabledResourcesCount > 0);

            #endregion
        }
    }
}
