﻿using Clouds42.AlphaNumericsSupport;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Common.Encrypt;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий смены пароля для пользователя абон центра
    /// Действия:
    ///     1) Создаем аккаунт и пользователя с источником регистрации АбонЦентр
    ///     2) Имитируем восстановление пароля (с заведомо невалидным паролем)
    ///     3) Проверяем что пароль не изменен
    ///     4) Имитируем восстановление пароля (с заведомо валидным паролем)
    ///     5) Проверяем что пароль изменен
    /// </summary>
    public class ChangePasswordForAbonCenterUserTest : ScenarioBase
    {
        private readonly string ErrorPassword = "1234567";

        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator;

        public ChangePasswordForAbonCenterUserTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    RegistrationSource = ExternalClient.AbonCenter
                }
            });
            createAccountCommand.Run();

            var accountUser = DbLayer.AccountUsersRepository.GetAccountUser(createAccountCommand.AccountAdminId);

            var hashProvider = new Md5HashProvider();
            var resetToken = hashProvider.ComputeHash(accountUser.ResetCode.ToString());

            var errorResult = _accountUsersProfileManager.SetPasswordWithResetToken(new ChangePasswordWithTokenModelDto
            {
                ResetToken = resetToken,
                NewPassword = ErrorPassword
            });

            Assert.IsNotNull(errorResult);
            Assert.IsTrue(errorResult.Error, $"{errorResult.Message}");
            Assert.AreEqual(ManagerResultState.PreconditionFailed, errorResult.State);

            var successResult = _accountUsersProfileManager.SetPasswordWithResetToken(new ChangePasswordWithTokenModelDto
            {
                ResetToken = resetToken,
                NewPassword = _alphaNumeric42CloudsGenerator.GeneratePassword()
            });

            Assert.IsNotNull(successResult);
            Assert.IsFalse(successResult.Error, $"{errorResult.Message}");
            Assert.AreEqual(ManagerResultState.Ok, successResult.State);

            RefreshDbCashContext(TestContext.Context.AccountUsers);
            accountUser = DbLayer.AccountUsersRepository
                .GetAccountUser(createAccountCommand.AccountAdminId);

            Assert.IsNull(accountUser.ResetCode);
        }
    }
}
