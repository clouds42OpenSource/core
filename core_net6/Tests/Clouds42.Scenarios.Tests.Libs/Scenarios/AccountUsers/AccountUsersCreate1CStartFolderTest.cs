﻿#region Libr
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Helpers.JobWrappers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

#endregion Libr

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    /// <summary>
    /// Сценарий проверки создания папки 1CeStart
    /// при создании пользователя и при изменении логина пользователя
    /// 
    /// Входные параметры: создание пользователя
    /// 
    /// Действия: изменяем логин
    /// 
    /// Проверка: При создании пользователя папка создается, 
    /// при редактировании  создается новая
    /// </summary>
    public class AccountUsersCreate1CStartFolderTest : ScenarioBase
    {
        private string Login { get; } = $"Test{new Random().Next(1, 1000)}InAdNum";
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.GetAccountUser(accountAdminId);
            
            if (adminUser == null)
                throw new InvalidOperationException ($"Пользователь аккаунта c id={accountAdminId} не найден.");

            var frPathToCreateAccount = AccountUserProfileDirectoryHelper.GetPathForCreate1CeStartDirectory(adminUser.Login);

            if (!Directory.Exists(frPathToCreateAccount))
                throw new InvalidOperationException (
                    $"При создании пользователя \"{adminUser.Login}\" директория  \"{frPathToCreateAccount}\" не создалась .");

            Services.AddTransient<ICreateDirectoryJobWrapper, TestCreateDirectoryJobWrapper>();
            var sp = Services.BuildServiceProvider();
            var accountUsersProfileManager = sp.GetRequiredService<AccountUsersProfileManager>();
            var updatedModel = new AccountUserDto
            {
                Id = accountAdminId,
                AccountCaption = "Test account",
                Login = Login,
                Password = "123456",
                Email = "TestEmail@efsol.ru",
                FirstName = "TestFirstName",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Roles = null
            };

            accountUsersProfileManager.Update(updatedModel);

            var frPathToUpdateAccount = AccountUserProfileDirectoryHelper.GetPathForCreate1CeStartDirectory(adminUser.Login);

            if (!Directory.Exists(frPathToUpdateAccount))
                throw new InvalidOperationException (
                    $"При редактировании пользователя \"{Login}\" директория  \"{frPathToUpdateAccount}\" не создалась .");
        }
    }
}
