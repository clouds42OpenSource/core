﻿#region Libr
using System;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.Billing;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

#endregion Libr

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    public class AccountUsersSortingTest : ScenarioBase
    {
        private AccountUser CreateUser()
        {
            //создание пользователя
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.GetAccountUser(accountAdminId);

            if (adminUser == null)
            {
                throw new InvalidOperationException ($"Пользователь-администратор аккаунта c id={accountAdminId} не найден.");
            }

            return adminUser;
        }
  
        public override void Run()
        {
           var accountAdmin =  CreateUser();

           var accountId = accountAdmin.Account.Id;

            TestDataGenerator tdGenerator = new TestDataGenerator(Configuration, DbLayer);
            
            var usersToCreate = tdGenerator.GenerateListAccountUsersByCount(7, accountId);
            usersToCreate.SaveAccountUsers(DbLayer);

            //список дополнительных пользователей, не аккаунтадмины
            var simpleUsersForSorting = DbLayer.AccountUsersRepository.Where(a => (a.AccountId == accountId)).ToList();
            //Обычный список имён
            string[] names = new string[simpleUsersForSorting.Count];


            if (simpleUsersForSorting.Count == 8)
            {
                simpleUsersForSorting[0].LastName = "";
                simpleUsersForSorting[0].FirstName = "";
                simpleUsersForSorting[0].MiddleName = "";

                simpleUsersForSorting[1].LastName = "Abramovich";
                simpleUsersForSorting[1].FirstName = "";
                simpleUsersForSorting[1].MiddleName = "";

                simpleUsersForSorting[2].LastName = "April";
                simpleUsersForSorting[2].FirstName = "FirstName";
                simpleUsersForSorting[2].MiddleName = "MiddleName";

                simpleUsersForSorting[3].LastName = "Абрамов";
                simpleUsersForSorting[3].FirstName = "Максим";
                simpleUsersForSorting[3].MiddleName = null;

                simpleUsersForSorting[4].LastName = null;
                simpleUsersForSorting[4].FirstName = null;
                simpleUsersForSorting[4].MiddleName = "Андреевич";

                simpleUsersForSorting[5].LastName = null;
                simpleUsersForSorting[5].FirstName = "Андрей";
                simpleUsersForSorting[5].MiddleName = "Викторович";

                simpleUsersForSorting[6].LastName = "Володин";
                simpleUsersForSorting[6].FirstName = "Максим";
                simpleUsersForSorting[6].MiddleName = "";

                simpleUsersForSorting[7].LastName = null;
                simpleUsersForSorting[7].FirstName = "";
                simpleUsersForSorting[7].MiddleName = "Ильич";

            }

            foreach (var simpleUserForSorting in simpleUsersForSorting)
            {
                DbLayer.AccountUsersRepository.Update(simpleUserForSorting);
                DbLayer.Save();
            }

            //список имён сортированый
            var sortedNamesAsc = new string[8]
            {
                "",
                "Abramovich",
                "April FirstName MiddleName",
                "Абрамов Максим",
                "Андреевич",
                "Андрей Викторович",
                "Володин Максим",
                "Ильич"
            };

            //список имён сортированый по убыванию
            var sortedNamesByDescending = new string[8]
            {
                "Ильич",
                "Володин Максим",
                "Андрей Викторович",
                "Андреевич",
                "Абрамов Максим",
                "April FirstName MiddleName",
                "Abramovich",
                ""
            };

            var filterModelWithoutDesc = new AccountUsersFilterModel
            {
                Page = 1,
                PageSize = 8,
                SearchQuery = null,
                SortColumn = nameof(AccountUser.FullName),
                DescendingFlag = false
            };

            var filterModelWithDesc = new AccountUsersFilterModel
            {
                Page = 1,
                PageSize = 8,
                SearchQuery = null,
                SortColumn = nameof(AccountUser.FullName),
                DescendingFlag = true
            };

            var access = new FakeAccessProvider(accountAdmin.Id, TestContext.DbLayer, TestContext.Logger,
                TestContext.Configurations, HandlerException, AccessMapping);
            var helper = new AccountUsersSearchHelper(access, DbLayer);
            AccountUsersSearchManager accountUsersSeachManager = new AccountUsersSearchManager(access, DbLayer, HandlerException, helper);

            var userListAsc = accountUsersSeachManager.GetAccountUsersSearchResult(accountId, filterModelWithoutDesc).Result.Users;

            if (userListAsc.Count!=names.Length)
            {
                throw new InvalidOperationException ("Ошибка. Получено неверное количество записей");
            }

            for (int i=0;i< userListAsc.Count;i++)
            {
                string name = $"{userListAsc[i].LastName} {userListAsc[i].FirstName} {userListAsc[i].MiddleName}";
                name = name.TrimEnd(' ').TrimStart(' ');
                var compareResult = string.Compare(name, sortedNamesAsc[i]);
                if (compareResult != 0 )
                {
                    throw new InvalidOperationException ($"Ошибка сортировки по имени. \"{name}\" не соответствует \"{sortedNamesAsc[i]}\" .");
                }
            }

            var userListDesc = accountUsersSeachManager.GetAccountUsersSearchResult(accountId, filterModelWithDesc).Result.Users;
            if ((userListDesc.Count != names.Length)|| (userListDesc.Count != userListAsc.Count))
            {
                throw new InvalidOperationException ("Ошибка. Получено неверное количество записей");
            }
            for (int i = 0; i < userListAsc.Count; i++)
            {
                string name = $"{userListDesc[i].LastName} {userListDesc[i].FirstName} {userListDesc[i].MiddleName}";
                name = name.TrimEnd(' ').TrimStart(' ');
                var compareResult = string.Compare(name, sortedNamesByDescending[i]);
                if (compareResult != 0)
                {
                    throw new InvalidOperationException ($"Ошибка сортировки по убыванию по имени. \"{name}\" не соответствует \"{sortedNamesByDescending[i]}\" .");
                }
            }
 

        }
    }
}
