﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser.Providers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsers
{
    public class ChangeAccountUserRolesTest : ScenarioBase
    {
        private string Login { get; } = $"Test{new Random().Next(1, 1000)}InAdNum";

        private readonly List<AccountUserGroup> _adminRole =
        [
            AccountUserGroup.AccountUser,
            AccountUserGroup.CloudAdmin,
            AccountUserGroup.Hotline
        ];

        private readonly List<AccountUserGroup> _hotlineRole =
        [
            AccountUserGroup.AccountUser,
            AccountUserGroup.Hotline
        ];


        private readonly List<AccountUserGroup> _adminGroupList =
        [
            AccountUserGroup.AccountUser,
            AccountUserGroup.AccountAdmin,
            AccountUserGroup.AccountSaleManager,
            AccountUserGroup.Cloud42Service,
            AccountUserGroup.Hotline,
            AccountUserGroup.CloudSE,
            AccountUserGroup.ProcOperator
        ];

        private readonly List<AccountUserGroup> _hotlineGroupList =
        [
            AccountUserGroup.AccountUser,
            AccountUserGroup.AccountSaleManager,
            AccountUserGroup.AccountAdmin
        ];

        /// <summary>
        /// Создание тестового аккаунта
        /// </summary>
        private AccountUser CreateUser()
        {
            //создание пользователя
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            // изменение роли на CloudAdmin
            var accountAdminId = createAccountCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.GetAccountUser(accountAdminId);

            if (adminUser == null)
                throw new InvalidOperationException ($"Пользователь-администратор аккаунта c id={accountAdminId} не найден.");


            return adminUser;
        }

        /// <summary>
        /// Преобразование списка ролей в список групп
        /// </summary>
        /// <param name="accountUserRoles">Список ролей </param>
        private List<AccountUserGroup> ConvertRoleToGroupList(List<AccountUserRole> accountUserRoles)
        {
            var accountUserGroupList = new List<AccountUserGroup>();
            foreach(var role in accountUserRoles)
            {
                accountUserGroupList.Add(role.AccountUserGroup);
            }

            return accountUserGroupList;
        }

        /// <summary>
        /// Сравнение двух списков групп
        /// </summary>
        /// <param name="first">Певый спиок</param>
        /// <param name="second">Второй список</param>
        /// <returns>Истина если одинаковые</returns>
        private bool CompareLists(List<AccountUserGroup> first, List<AccountUserGroup> second)
        {
            return first.OrderBy(t => t).SequenceEqual(second.OrderBy(t => t));
        }

        /// <summary>
        /// Получение списка групп пользователя
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        private List<AccountUserRole> GetAccountUserGroupsList(Guid accountUserId) {
            return DbLayer.AccountUserRoleRepository.Where(x => x.AccountUserId == accountUserId).ToList();
        }

        /// <summary>
        /// Удаление ролей
        /// </summary>
        /// <param name="rolesList">Список ролей для удаления</param>
        /// <param name="rolesProvider">Провайдер смены ролей</param>
        private void RemoveRoles(List<AccountUserRole> rolesList, AccountUserRolesProvider rolesProvider)
        {
            rolesProvider.RemoveAccountUserRoles(rolesList); //удаляем все текущие роли
        }

        /// <summary>
        /// Запись ролей Аадминистратором облака. Возвращает записанные роли в БД
        /// </summary>
        /// <param name="user">Аккаунт которому добавляь роли</param>
        /// <param name="groups">Список ролей для добавления</param>
        /// <param name="rolesProvider">Провайдер смены ролей</param>
        private List<AccountUserRole> AdminAddRoles(AccountUser user, List<AccountUserGroup> groups, AccountUserRolesProvider rolesProvider)
        {
            rolesProvider.SetAccountUserRoles(user, _adminRole, groups);
            return GetAccountUserGroupsList(user.Id);
        }

        /// <summary>
        /// Запись ролей Хотлайном. Возвращает записанные роли в БД
        /// </summary>
        /// <param name="user">Аккаунт которому добавляь роли</param>
        /// <param name="groups">Список ролей для добавления</param>
        /// <param name="rolesProvider">Провайдер смены ролей</param>
        private List<AccountUserRole> HotlineAddRoles(AccountUser user, List<AccountUserGroup> groups, AccountUserRolesProvider rolesProvider)
        {
            rolesProvider.SetAccountUserRoles(user, _hotlineRole, groups);
            return GetAccountUserGroupsList(user.Id);
        }


        public override void Run()
        {

            var adminUser = CreateUser();
            var rolesProvider = new AccountUserRolesProvider(DbLayer);

            var accountUsersProfileManager = ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = adminUser.Account.Id,
                AccountIdString = SimpleIdHash.GetHashById(adminUser.Account.Id),
                Login = Login,
                Email = "UserTestEmail009@efsol.ru",
                FirstName = "UserTestName009"
            };
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            if (res.Error)
                throw new InvalidOperationException ("Ошибка создания нового пользователя. " + res.Message);

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == res.Result);
            var oldUserRoles = ConvertRoleToGroupList(user.AccountUserRoles.ToList());



            var editedRolesByAdmin = AdminAddRoles(user, _adminGroupList, rolesProvider);
            Assert.IsFalse(CompareLists(oldUserRoles, ConvertRoleToGroupList(editedRolesByAdmin)), "Роли не сменились");
            Assert.IsTrue(CompareLists(_adminGroupList, ConvertRoleToGroupList(editedRolesByAdmin)), "Список ролей не соответствует ожидаемому списку после изменения Клауд админом");

            var editedRolesByHotline = HotlineAddRoles(user, _hotlineGroupList, rolesProvider);
            Assert.IsTrue(CompareLists(ConvertRoleToGroupList(editedRolesByHotline), ConvertRoleToGroupList(editedRolesByAdmin)), "Хотлайн убрал роли добавленные Клауд админом");

            RemoveRoles(editedRolesByHotline, rolesProvider);

            var newRolesList = new List<AccountUserGroup> { AccountUserGroup.Hotline };
            editedRolesByAdmin = AdminAddRoles(user, newRolesList, rolesProvider);
            Assert.IsTrue(CompareLists(newRolesList, ConvertRoleToGroupList(editedRolesByAdmin)), "Список ролей содержит лишние элементы после добавления Клауд админом");

            newRolesList.Add(AccountUserGroup.AccountAdmin);
            editedRolesByAdmin = AdminAddRoles(user, newRolesList, rolesProvider);
            Assert.IsTrue(CompareLists(newRolesList, ConvertRoleToGroupList(editedRolesByAdmin)), "Список ролей не соответствует ожидаемому списку после изменения Клауд админом");

            RemoveRoles(editedRolesByAdmin, rolesProvider);

            editedRolesByHotline = HotlineAddRoles(user, newRolesList, rolesProvider);
            Assert.IsFalse(CompareLists(newRolesList, ConvertRoleToGroupList(editedRolesByHotline)), "Хотлайн добавил недоступные ему роли");

        }

        public override void ClearGarbage()
        {
            base.ClearGarbage();
            ActiveDirectoryUserFunctions.DeleteUser(Login);
        }
    }
}
