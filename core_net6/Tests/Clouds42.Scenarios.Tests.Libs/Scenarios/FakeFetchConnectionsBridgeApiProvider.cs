﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios
{
    public class FakeFetchConnectionsBridgeApiProvider : IFetchConnectionsBridgeApiProvider
    {
        /// <summary>
        /// Обаботать команду обновления базы в облаке.
        /// </summary>
        public void UpdateDatabaseHandle(Guid accountDatabaseId)
        {
            // заглушка
        }

        /// <summary>
        /// Обаботать команду содание новой базы в облаке.
        /// </summary>
        public void CreateDatabaseHandle(Guid accountDatabaseId)
        {
            // заглушка
        }

        /// <summary>
        /// Обаботать команду удаления базы с облака.
        /// </summary>
        public void DeleteDatabaseHandle(Guid accountDatabaseId)
        {
            // заглушка
        }

        /// <summary>
        /// Обаботать команду предоставления доступа к базе.
        /// </summary>
        public void GrandAccessDatabaseHandle(Guid accountDatabaseId)
        {
            // заглушка
        }

        /// <summary>
        /// Обаботать команду удаления пользовательского доступа у базы.
        /// </summary>
        public void DeleteAccessDatabaseHandle(Guid accountDatabaseId, Guid accountUserId)
        {
            // заглушка
        }

        /// <summary>
        /// Выполнить обработку ассинхронно.
        /// </summary>
        public IFetchConnectionsBridgeApiProvider ExecuteInBackgroundThread()
        {
            return this;
        }
    }
}
