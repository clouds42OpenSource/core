﻿using System;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateVersionInLoadZipFile
{
    ///Распаковка не корректного зип файла
    ///Предусловия: пользователем загружен зип файл c версией ниже поддерживаемой для баз на разделителях.
    ///             Полный путь к файлу: FileUploadPath что в базе + userLogin + zipFileName
    ///                
    ///Действия: проверяем зип файл на наличие в нем файла DumpInfo.xml, проверяем в ней название конфигурации и версию
    ///
    ///Проверка: файл должен быть не валидным 
    public class LoadingMinVersionFileTest : ScenarioBase
    {
        private readonly string userLogin = "TestLoad";
        private readonly string zipFileName = "tradeMinVersion.zip";
        private readonly IAccountDatabaseParseZipFileProvider _accountDatabaseParseZipFileProvider;

        public LoadingMinVersionFileTest()
        {
            _accountDatabaseParseZipFileProvider = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseParseZipFileProvider>();
        }

        public override void Run()
        {
            try
            {
                var createAccountCommand = new CreateAccountCommand(TestContext);
                createAccountCommand.Run();
                var user = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == createAccountCommand.AccountAdminId);
                user.Login = userLogin;
                DbLayer.AccountUsersRepository.Update(user);
                DbLayer.DbTemplateDelimitersReferencesRepository.Insert(new DbTemplateDelimitersModelTest(TestContext));
                DbLayer.Save();

                var uploadedFile = new UploadedFile
                {
                    Id = Guid.NewGuid(),
                    AccountId = createAccountCommand.AccountId,
                    Status = UploadedFileStatus.UploadSuccess,
                    FileName = zipFileName,
                    UploadDateTime = DateTime.Now,
                    FullFilePath = TestContext.ServiceProvider.GetRequiredService<UploadingFilePathHelper>().GetFullUploadedFilePath(createAccountCommand.Account.Id, zipFileName)
                };

                DbLayer.UploadedFileRepository.Insert(uploadedFile);
                DbLayer.Save();

                _accountDatabaseParseZipFileProvider.ValidateVersionXmlInZipFile(uploadedFile.Id);
                throw new InvalidOperationException ("При загрузке не корректного зип файла валидация прошла успешно");
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
