﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ValidateVersionInLoadZipFile
{
    /// <summary>
    /// Распаковка корректного зип файла
    /// Предусловия: пользователем загружен зип файл правильной версии и поддерживаемой конфигурации для баз на разделителях.
    ///             Полный путь к файлу: FileUploadPath что в базе + userLogin + zipFileName
    ///                
    /// Действия: проверяем зип файл на наличие в нем файла DumpInfo.xml, проверяем в ней название конфигурации и версию
    /// Проверка: файл должен быть валидным 
    /// </summary>
    public class LoadingCorrectVersionFileTest : ScenarioBase
    {
        private readonly string userLogin = "TestLoad";
        private readonly string zipFileName = "tradeCorrectConfig.zip";
        private readonly IAccountDatabaseParseZipFileProvider _accountDatabaseParseZipFileProvider;

        public LoadingCorrectVersionFileTest()
        {
            _accountDatabaseParseZipFileProvider = TestContext.ServiceProvider.GetRequiredService<IAccountDatabaseParseZipFileProvider>();
        }

        public override void Run()
        {
            var createZipFileHelper = ServiceProvider.GetRequiredService<CreateZipFileHelper>();
            var uploadingFilePathHelper = TestContext.ServiceProvider.GetRequiredService<UploadingFilePathHelper>();

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var uploadedFile = new UploadedFile
            {
                Id = Guid.NewGuid(),
                AccountId = createAccountCommand.AccountId,
                Status = UploadedFileStatus.UploadSuccess,
                FileName = zipFileName,
                UploadDateTime = DateTime.Now,
                FullFilePath = uploadingFilePathHelper.GetFullUploadedFilePath(createAccountCommand.Account.Id, zipFileName)
            };

            DbLayer.UploadedFileRepository.Insert(uploadedFile);
            DbLayer.Save();

            if (File.Exists(uploadedFile.FullFilePath))
            {
                createZipFileHelper.DeleteTestFile(uploadedFile.FullFilePath);
            }

            var user = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == createAccountCommand.AccountAdminId);
            user.Login = userLogin;
            DbLayer.AccountUsersRepository.Update(user);
            var dbTemplateDelimitersModel = new DbTemplateDelimitersModelTest(TestContext);
            var createdelimiter = new CreateDbTemplateDelimitersCommand(TestContext, dbTemplateDelimitersModel);
            createdelimiter.Run();
           
            createZipFileHelper.CreateZipFileWithDumpInfo(uploadedFile.FullFilePath);

            _accountDatabaseParseZipFileProvider.ValidateVersionXmlInZipFile(uploadedFile.Id);

            createZipFileHelper.DeleteTestFile(uploadedFile.FullFilePath);
        }
    }
}
