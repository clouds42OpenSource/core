﻿using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Application.Features.AccountContext.Queries.Filters;
using NUnit.Framework.Legacy;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Accounts
{
    /// <summary>
    /// Сценарий получения аккаунтов
    ///     1) Создаём два аккаунта
    ///     2) Проверяем что записи появились в базе данных
    ///     3) Проверяем что ИНН индетичны для первого и второго аккаунта
    ///     4) Проверяем что названия аккаунтов разные для первого и второго аккаунта
    ///     5) Ищем аккаунты по ИНН, должно быть два
    ///     6) Ищем аккаунты по названию аккаунта, должен быть один
    /// </summary>
    public class ScenarioGetAccountListTest : ScenarioBase
    {
        public override void Run()
        {
            #region Создаём два аккаунта

            var createAccountCommand1 = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand1.Run();

            var createAccountCommand2 = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                }
            });
            createAccountCommand2.Run();
            
            #endregion

            #region Проверяем что записи появились в базе данных

            var firstAccount = DbLayer.AccountsRepository.FirstOrDefault(item => item.Id == createAccountCommand1.AccountId);
            var secondAccount = DbLayer.AccountsRepository.FirstOrDefault(item => item.Id == createAccountCommand2.AccountId);

            ClassicAssert.IsNotNull(firstAccount, "Аккаунт не создался!");
            ClassicAssert.IsNotNull(secondAccount, "Аккаунт не создался!");

            #endregion

            #region Проверяем что ИНН индетичны для первого и второго аккаунта

            ClassicAssert.AreEqual(firstAccount.AccountRequisites.Inn, secondAccount.AccountRequisites.Inn);

            #endregion

            #region Проверяем что названия аккаунтов разные для первого и второго аккаунта

            ClassicAssert.AreNotEqual(firstAccount.AccountCaption, secondAccount.AccountCaption);

            #endregion
            
            #region Ищем аккаунты по ИНН, должно быть два

            var foundAccounts = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter { SearchLine = firstAccount.AccountRequisites.Inn }
            }).Result;

            ClassicAssert.IsFalse(foundAccounts.Error);
            ClassicAssert.AreEqual(2, foundAccounts.Result.Records.Count);

            #endregion
            
            #region Ищем аккаунты по названию аккаунта, должен быть один

            #region 1 аккаунт

            foundAccounts = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter { SearchLine = firstAccount.AccountCaption }
            }).Result;
            ClassicAssert.IsFalse(foundAccounts.Error);
            ClassicAssert.AreEqual(1, foundAccounts.Result.Records.Count);

            #endregion

            #region 2 аккаунт

            foundAccounts  = Mediator.Send(new GetFilteredAccountsQuery
            {
                Filter = new AccountFilter { SearchLine = secondAccount.AccountCaption }
            }).Result;
            ClassicAssert.IsFalse(foundAccounts.Error);
            ClassicAssert.AreEqual(1, foundAccounts.Result.Records.Count);

            #endregion

            #endregion
        }
    }
}
