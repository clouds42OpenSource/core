﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.Accounts
{
    /// <summary>
    /// Сценарий получения информационных баз аккаунта для миграций
    ///     1) Создаём аккаунт и базу, вторая база для контента
    ///     2) Проверяем что записи появились в базе данных
    ///     3) Ищем базы аккаунта по номеру аккаунта первой базы
    ///     4) Ищем базы аккаунта по номеру первой базы
    /// </summary>
    public class ScenarioGetAccountDatabasesForMigrationTest : ScenarioBase
    {
        private readonly AccountDataManager _accountDataManager;

        public ScenarioGetAccountDatabasesForMigrationTest()
        {
            _accountDataManager = ServiceProvider.GetRequiredService<AccountDataManager>();
        }

        public override void Run()
        {
            #region Создаём аккаунт и базу

            var createAccAndDb1 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb1.Run();

            var createAccAndDb2 = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccAndDb2.Run();

            #endregion

            #region Проверяем что записи появились в базе данных

            var db1 = DbLayer.DatabasesRepository.FirstOrDefault(item => item.Id == createAccAndDb1.AccountDatabaseId);
            var account1 = DbLayer.AccountsRepository.FirstOrDefault(item => item.Id == createAccAndDb1.AccountDetails.AccountId);

            AssertHelper.Execute(() => Assert.IsNotNull(account1, "Аккаунт не создался!"));
            AssertHelper.Execute(() => Assert.IsNotNull(db1, "База не создалась!"));


            var db2 = DbLayer.DatabasesRepository.FirstOrDefault(item => item.Id == createAccAndDb2.AccountDatabaseId);
            var account2 = DbLayer.AccountsRepository.FirstOrDefault(item => item.Id == createAccAndDb2.AccountDetails.AccountId);


            AssertHelper.Execute(() => Assert.IsNotNull(account2, "Аккаунт не создался!"));
            AssertHelper.Execute(() => Assert.IsNotNull(db2, "База не создалась!"));


            #endregion


            #region Ищем базы аккаунта

            #region По номеру аккаунта

            var foundDbs = _accountDataManager.GetAccountDatabasesForMigrationAsync(new GetAccountDatabasesForMigrationQuery
            {
                Filter = new AccountDatabaseMigrationFilterParamsDto
                {
                    AccountId = account1.Id
                }
            }).Result;
            AssertHelper.Execute(() => Assert.IsFalse(foundDbs.Error));
            AssertHelper.Execute(() => Assert.AreEqual(1, foundDbs.Result.Databases.Records.Count));
            AssertHelper.Execute(() => Assert.AreEqual(db1.V82Name, foundDbs.Result.Databases.Records[0].V82Name));

            #endregion

            #region По номеру базы

            foundDbs = _accountDataManager.GetAccountDatabasesForMigrationAsync(new GetAccountDatabasesForMigrationQuery
            {
                Filter = new AccountDatabaseMigrationFilterParamsDto
                {
                    AccountId = account1.Id,
                    SearchLine = db1.V82Name
                }
            }).Result;
            AssertHelper.Execute(() => Assert.IsFalse(foundDbs.Error));
            AssertHelper.Execute(() => Assert.AreEqual(1, foundDbs.Result.Databases.Records.Count));
            AssertHelper.Execute(() => Assert.AreEqual(db1.V82Name, foundDbs.Result.Databases.Records[0].V82Name));
            


            #endregion

            #endregion


        }
    }
}
