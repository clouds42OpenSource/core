﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки покупки сервиса Дополнительные сеансы
    ///     
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    ///     баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    /// 
    ///     Действия: подключаем 3 дополнительных сессии
    /// 
    ///     Проверка: сервис должен быть подключен, сумма списана с баланса, сервис подорожал
    ///     но дата остается прежней, есть ОП
    /// </summary>
    public class ScenarioPayAvailableSessionWithThePromisedPayment : ScenarioBase
    {
        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);

            //Аренда +10 дней от тек.даты. 
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba =>
                ba.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            Assert.IsNotNull(billingAccount);

            //Пополнили баланс для того чтобы взять ОП
            var balance = 100m;
            var billingService = GetRent1CServiceOrThrowException();

            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, balance);

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");

            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Complete);

            //покупаем Дополнительные сеансы за счет ОП
            var result = _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                сloudServicePayment.CanGetPromisePayment, сloudServicePayment.CostOftariff, 3);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            Assert.IsTrue(result.Result.Complete);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsNotNull(billingAccount.PromisePaymentSum);
            Assert.AreEqual(сloudServicePayment.CostOftariff - balance, billingAccount.PromisePaymentSum);
            Assert.AreEqual(0, billingAccount.Balance);
            Assert.AreEqual(сloudServicePayment.MonthlyCost, resourceAvailableSession.Cost);
            Assert.AreEqual(false, resourceAvailableSession.FrozenValue);
            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceAvailableSession.ExpireDateValue.ToShortDateString());

        }
    }
}
