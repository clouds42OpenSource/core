﻿using Clouds42.Billing;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.TestCommands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста получения модели для запуска базы
    /// Создаем аккаунт и сервис
    /// 1) Проверяем как поведет себя система если указать не существующий сервис
    /// 2) Проверяем получение модели для запуска если у сервиса не указанные совместимые
    /// конфигурации 1С
    /// 3) Проверяем получение если есть совместимы конфигурации но нет баз для этих конфигураций
    /// 4) Проверяем получение модели для запуска базы если:
    ///     1) Есть 2 подходящие базы(По статусу, для аккаунта, с совместимыми конфигурациями)
    ///     2) Есть база не на разделителях
    ///     3) Есть база подходящая, но принадлежит другому аккаунту
    ///     3) Есть база с не совместимой конфигурацией
    ///     4) Есть база с со статусом "Ошибка создания"
    /// Результат: имеются 2 базы для запуска
    /// </summary>
    public class ScenarioGetAccountDatabaseToRunTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        public ScenarioGetAccountDatabaseToRunTest()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {            
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var sourceAccountDatabaseCommand = new CreateDbTemplateDelimitersCommand(TestContext);
            sourceAccountDatabaseCommand.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            var dbTemplates = DbLayer.DbTemplateRepository.WhereLazy().Select(w => w.Id).ToList();

            if (!dbTemplates.Any())
                throw new InvalidOperationException("Не заполнен справочник шаблоны");

            var utTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.UtName,
                DefaultCaption = "Управление торговлей 1.1"
            });

            var kaTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.KaName,
                DefaultCaption = "Комплексная автоматизация"
            });
            
            var dbTemplateDelimitersOne =
                _testDataGenerator.GenerateDbTemplateDelimiters(utTemplate.Id, "ut");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersOne);

            var dbTemplateDelimitersTwo =
                _testDataGenerator.GenerateDbTemplateDelimiters(kaTemplate.Id, "ka");
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimitersTwo);
            DbLayer.Save();

            var defaultconfiguration1C = dbTemplateDelimitersOne.ConfigurationId;
            var defaultconfiguration1CTwo = dbTemplateDelimitersTwo.ConfigurationId;

            var result = _billingServiceManager.GetAccountDatabaseToRun(Guid.NewGuid(), createAccountCommand.AccountId).Result;

            if (!result.Error)
                throw new InvalidOperationException("Нельзя получить модель для не существующего сервиса");

            RefreshDbCashContext(TestContext.Context.DbTemplateDelimiters);

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext)
                {
                    CreateBillingServiceTest =
                    {
                        BillingServiceStatus = BillingServiceStatusEnum.Draft
                    }
                };
           createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.SystemService == null && w.Name != "Дополнительные сеансы");
            
            result = _billingServiceManager.GetAccountDatabaseToRun(service.Id, createAccountCommand.AccountId).Result;

            if (result.Result.AvailabilityOfSuitableConfigurations1C || result.Result.AccountDatabasesOnDelimitersToRun.Any())
                throw new InvalidOperationException("У сервиса нет совместимых конфигураций.");

            var editServiceManager = ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.Name = "nName";

            var editResult = editServiceManager.EditBillingService(editBillingServiceDto);

            if (editResult.Error)
                throw new InvalidOperationException("Не корректно работает редактирование сервиса");
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp"]);
            result = _billingServiceManager.GetAccountDatabaseToRun(service.Id, createAccountCommand.AccountId).Result;

            if (!result.Result.AvailabilityOfSuitableConfigurations1C)
                throw new InvalidOperationException("У сервиса есть совместимые конфигурации");

            if (result.Result.AccountDatabasesOnDelimitersToRun.Any())
                throw new InvalidOperationException("Нет подходящих баз для сервиса");
            
            var dbOnDelimter = new InfoDatabaseDomainModelTest(TestContext, utTemplate) { DbTemplateDelimiters = true, DataBaseName = "Тестовая база на разделителях" };
            var dbOnDelimter2 = new InfoDatabaseDomainModelTest(TestContext, kaTemplate) { DbTemplateDelimiters = true, DataBaseName = "Тестовая база на разделителях" };
            
            var accountDbOne = new CreateAccountDatabaseViaDelimiterCommand(TestContext, createAccountCommand, dbOnDelimter);
            accountDbOne.Run();

            var accountDbTwo = new CreateAccountDatabaseViaDelimiterCommand(TestContext, createAccountCommand, dbOnDelimter2);
            accountDbTwo.Run();

            var accountDbFour = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 4,
                createAccountCommand.Account.IndexNumber, state: DatabaseState.ErrorCreate);

            var accountDbOnDelimitersFour = _testDataGenerator.GenerateAccountDatabaseOnDelimiters(accountDbFour.Id, null,
                defaultconfiguration1CTwo);

            var accountDbFive = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 5,
                createAccountCommand.Account.IndexNumber);

            var anotherAccount = _testDataGenerator.GenerateAccount();

            var accountDbSix = _testDataGenerator.GenerateAccountDatabase(anotherAccount.Id, 1,
                anotherAccount.IndexNumber, state: DatabaseState.NewItem);

            var accountDbOnDelimitersSix = _testDataGenerator.GenerateAccountDatabaseOnDelimiters(accountDbSix.Id, null,
                defaultconfiguration1C);

            DbLayer.AccountsRepository.Insert(anotherAccount);
            DbLayer.DatabasesRepository.Insert(accountDbFour);
            DbLayer.DatabasesRepository.Insert(accountDbFive);
            DbLayer.DatabasesRepository.Insert(accountDbSix);
            DbLayer.AccountDatabaseDelimitersRepository.Insert(accountDbOnDelimitersFour);
            DbLayer.AccountDatabaseDelimitersRepository.Insert(accountDbOnDelimitersSix);
            DbLayer.Save();

            result = _billingServiceManager.GetAccountDatabaseToRun(service.Id, createAccountCommand.AccountId).Result;

            Assert.IsFalse(result.Error, result.Message);
            Assert.IsTrue(result.Result.AvailabilityOfSuitableConfigurations1C, "У сервиса есть совместимые конфигурации");
            Assert.IsFalse(result.Result.TemplateId.IsNullOrEmpty() || result.Result.TemplateNames.IsNullOrEmpty(), "Не указан дефолтный шаблон для создания");
            Assert.IsFalse(result.Result.AccountDatabasesOnDelimitersToRun.Any(), "Если не установлено расширение для баз, то в список для запуска они не попадут");
            getConfigurationDatabaseCommandTest.SetListConfiguration(["ka", "ut"]);
            result = _billingServiceManager.GetAccountDatabaseToRun(service.Id, createAccountCommand.AccountId).Result;

            Assert.IsFalse(result.Error, result.Message);
            Assert.IsTrue(result.Result.AvailabilityOfSuitableConfigurations1C, "У сервиса есть совместимые конфигурации");
            Assert.IsFalse(result.Result.TemplateId.IsNullOrEmpty() || result.Result.TemplateNames.IsNullOrEmpty(), "Не указан дефолтный шаблон для создания");

            if (result.Result.AccountDatabasesOnDelimitersToRun.Count != 2 ||
                result.Result.AccountDatabasesOnDelimitersToRun.All(w => w.Id != accountDbOne.AccountDatabaseId) ||
                result.Result.AccountDatabasesOnDelimitersToRun.All(w => w.Id != accountDbTwo.AccountDatabaseId))
            {
                throw new InvalidOperationException("Не правильно работает выборка баз для запуска");
            }
        }

    }
}
