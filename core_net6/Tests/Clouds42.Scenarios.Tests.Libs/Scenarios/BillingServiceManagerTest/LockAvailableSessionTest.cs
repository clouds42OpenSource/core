﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки блокировки сервиса Дополнительные сеансы при истечении срока Аренды 1С
    /// 
    ///     Предусловия: для аккаунта test_account подключено 3 дополнительных сессии
    ///     баланс 0 руб; дата окончания сервиса  -1 дней от тек.даты.
    /// 
    ///     Действия: запускаем таску пролонгации
    /// 
    ///     Проверка: сервис должен быть подключен, сервис заблокирован
    /// </summary>
    public class LockAvailableSessionTest : ScenarioBase
    {

        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");
            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountCommand.AccountId, serviceAvailableSession.Id, 3);

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Result.Complete);
            Assert.IsFalse(сloudServicePayment.Result.CanGetPromisePayment);

            //Пополнили баланс для того чтобы купить допсеансы
            var balance = сloudServicePayment.Result.CostOftariff + 200;
            CreateInflowPayment(createAccountCommand.AccountId, serviceAvailableSession.Id, balance);

            //покупаем Дополнительные сеансы 
            _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountCommand.AccountId,
                false, сloudServicePayment.Result.CostOftariff, 3);

            // Устанавливаем дату Аренды -1 день от текущей
            var dateValue = DateTime.Now.AddDays(-1);
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            // Блочим сервис так как Аренда просрочена
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProlongOrLockExpiredServices();
            Assert.IsFalse(processRes.Error);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);


            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, serviceAvailableSession.Id);

            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNull(billingAcc.PromisePaymentSum);
            Assert.AreEqual(200, billingAcc.Balance);
        }
    }
}
