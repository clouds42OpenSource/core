﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий получения Id сервиса по ключу
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис
    ///         2) Получаем Id сервиса по ключу, проверяем, что Id правильный
    ///         3) Пытаемся получить Id сервиса по не существующему ключу, проверяем, что вернулась ошибка получения
    /// </summary>
    public class TryGetServiceIdByKeyTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;

        public TryGetServiceIdByKeyTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var serviceName = "Good Service";
            var serviceKey = Guid.NewGuid();
            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes,
                    Key = serviceKey
                });
            createBillingServiceCommand.Run();
            Assert.IsFalse(createBillingServiceCommand.Result.Error);

            Assert.IsNotNull(createBillingServiceCommand.Result);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Name == serviceName);
            Assert.IsNotNull(service);
            Assert.IsNotNull(service.Key);

            var serviceIdByKey = _billingServiceManager.TryGetServiceIdByKey(serviceKey);
            Assert.IsFalse(serviceIdByKey.Error);
            Assert.AreEqual(serviceIdByKey.Result, service.Id);

            var serviceIdByNotExistKey = _billingServiceManager.TryGetServiceIdByKey(Guid.NewGuid());
            Assert.IsTrue(serviceIdByNotExistKey.Error);
            Assert.AreEqual(serviceIdByNotExistKey.Result, Guid.Empty);
        }
    }
}
