﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для достаточного баланса
    /// 
    ///     Действия: создать аккаунт, пополнить средства, проверить сумму на списание по аккаунту, в случае если баланс больше или соотвествует сумме
    /// 
    ///     Проверка: положительный ответ валидации на списание
    /// </summary>
    public class CheckAccountBalanceValidationIfHaveMoneyTest : ScenarioBase
    {
        public override void Run()
        {
            //Создаем аккаунт
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            //Создаем входящий платеж
            CreateInflowPayment(createAccountCommand.AccountId, null, 100);

            //Проверяем возможность списания
            var result = TestContext.ServiceProvider.GetRequiredService<IBillingPaymentsProvider>()
                .CanCreatePayment(createAccountCommand.AccountId, 50, out decimal needMoney);

            Assert.IsTrue(result == PaymentOperationResult.Ok);
            Assert.AreEqual(0M, needMoney);
        }
    }
}
