﻿using System;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста получения транзакций партнера
    /// где имеются транзакции только по системным сервисам
    /// Создаем 3 аккаунта. Первый аккаунт будет партнером и реффералом для второго.
    /// 1) Проверяем результат если есть платеж на пополнение
    /// и платеж на списание без указания суммы(список должен быть пустым)
    /// 2) Проверяем результат если есть платеж у его клиента и платеж у чужого клиента
    /// (список будет содержать всего 1 транзакцию)
    /// 3) Проверяем парвильность посчитанной суммы для транзакции
    /// </summary>
    public class ScenarioGetPartnerTransactionsWhereOnlyBySystemServicesTest : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly CreateAccountHelperTest _createAccountHelperTest;

        public ScenarioGetPartnerTransactionsWhereOnlyBySystemServicesTest()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createAccountHelperTest = TestContext.ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
        }

        public override void Run()
        {
            var rent1CService = GetRent1CServiceOrThrowException();
            decimal defaultSum = 200;

            var createSegmentCommand = new CreateSegmentCommand(TestContext);
            createSegmentCommand.Run();

            var account = _createAccountHelperTest.Create();
            var anotherAccount = _createAccountHelperTest.Create();

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = account.Id
                }
            });

            createAccountCommand.Run();

            var calculateAgentRewardProvider = ServiceProvider.GetRequiredService<ICalculateAgentRewardProvider>();
            var paymentHelper = ServiceProvider.GetRequiredService<PaymentsHelper>();

            paymentHelper.MakePayment(createAccountCommand.AccountId);
            paymentHelper.MakePayment(createAccountCommand.AccountId, PaymentType.Outflow, defaultSum);

            var result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {AccountId = account.Id, PageNumber = 1});

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 0)
                throw new InvalidOperationException ("Не верно работает выборка транзакций");

            paymentHelper.MakePayment(createAccountCommand.AccountId, PaymentType.Outflow, defaultSum,
                rent1CService.Id);
            paymentHelper.MakePayment(anotherAccount.Id, PaymentType.Outflow, defaultSum, rent1CService.Id);

            var agentReward = calculateAgentRewardProvider.Calculate(defaultSum,
                IsVipAccount(createAccountCommand.AccountId), rent1CService.SystemService);

            result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {AccountId = account.Id, PageNumber = 1});

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 1)
                throw new InvalidOperationException ("Не верно работает выборка транзакций");

            if (result.Result.ChunkDataOfPagination.FirstOrDefault().TransactionSum != agentReward)
                throw new InvalidOperationException ("Сумма транзакции не совпадает");
        }
    }
}
