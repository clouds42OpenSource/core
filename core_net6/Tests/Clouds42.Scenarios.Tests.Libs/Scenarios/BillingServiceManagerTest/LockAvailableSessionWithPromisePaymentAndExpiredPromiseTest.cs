﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Billing.Billing.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки блокировки сервиса Дополнительные сеансы при просроченном ОП
    /// 
    ///     Предусловия: для аккаунта test_account подключено 3 дополнительных сессии
    ///     баланс 200 руб
    /// 
    ///     Действия: запускаем таску пролонгации
    /// 
    ///     Проверка: сервис должен быть подключен, сумма не списана с баланса, сервис заблокирован
    ///     есть ОП
    /// </summary>
    public class LockAvailableSessionWithPromisePaymentAndExpiredPromiseTest : ScenarioBase
    {

        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");
            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountCommand.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Complete);

            //берем ОП
            var accountManager = TestContext.ServiceProvider.GetRequiredService<BillingAccountManager>();

            var promisePayment = accountManager.CreatePromisePayment(createAccountCommand.AccountId, new CalculationOfInvoiceRequestModelTest
            {
                SuggestedPayment = new SuggestedPaymentModelDto
                {
                    PaymentSum = 1500
                }
            });
            Assert.IsFalse(promisePayment.Error, promisePayment.Message);

            //покупаем Дополнительные сеансы
            _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountCommand.AccountId,
                false, сloudServicePayment.CostOftariff, 3);

            // Устанавливаем дату ОП
            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount);

            billingAccount.PromisePaymentDate = DateTime.Now.AddDays(-8);
            DbLayer.BillingAccountRepository.Update(billingAccount);
            DbLayer.Save();

            // Блочим сервис так как ОП просрочена
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProcessExpiredPromisePayments();

            Assert.IsFalse(processRes.Error);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);


            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, serviceAvailableSession.Id);

            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNotNull(billingAcc.PromisePaymentSum);
            Assert.AreEqual(1500- сloudServicePayment.CostOftariff, billingAcc.Balance);
            Assert.AreEqual(1500, billingAcc.PromisePaymentSum);

        }
    }
}
