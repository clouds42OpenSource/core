﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки разблокировки сервиса Дополнительные сеансы при пополнении баланса
    /// 
    ///     Предусловия: для аккаунта test_account подключено 3 дополнительных сессии
    ///     баланс достаточный для оплаты Аренды и 2 допсенсов; 
    ///     дата окончания сервиса  -1 дней от тек.даты; сервис заблокирован
    /// 
    ///     Действия: изменить количество доп сеансов
    /// 
    ///     Проверка: сервис разблокирован, дата окончания сервиса +1 месяц от тек.даты
    /// </summary>
    public class UnlockAvailableSessionWithMoneyAndChangeCountTest : ScenarioBase
    {
        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");

            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountCommand.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Complete);

            //Пополнили баланс для того чтобы купить допсеансы
            var balance = сloudServicePayment.CostOftariff;
            CreateInflowPayment(createAccountCommand.AccountId, serviceAvailableSession.Id, balance);

            //покупаем Дополнительные сеансы 
            _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountCommand.AccountId,
                false, сloudServicePayment.CostOftariff, 3);

            // Устанавливаем дату Аренды -1 день от текущей
            var dateValue = DateTime.Now.AddDays(-1);
            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            // Блочим сервис 
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProlongOrLockExpiredServices();

            Assert.IsFalse(processRes.Error);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);

            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, serviceAvailableSession.Id);

            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNull(billingAcc.PromisePaymentSum);

            //Пополнили баланс для того чтобы купить 2 допсеансы и Аренду 1С
            balance = (сloudServicePayment.MonthlyCost + resourceRent1C.Cost) - 950;
            CreateInflowPayment(createAccountCommand.AccountId, serviceAvailableSession.Id, balance);

            resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, serviceAvailableSession.Id);

            billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNull(billingAcc.PromisePaymentSum);
            Assert.AreEqual(balance, billingAcc.Balance);

            //изменяем количество допсеансов до 2
            var сloudChangeServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountCommand.AccountId, 
                serviceAvailableSession.Id, 2).Result;

            Assert.IsNotNull(сloudChangeServicePayment);
            Assert.IsTrue(сloudChangeServicePayment.Complete);

            var resultServiceTypeInfo = _billingServiceManager.GetBillingServiceTypeInfo(serviceAvailableSession.Id,
                createAccountCommand.AccountId).Result;

            Assert.IsNotNull(resultServiceTypeInfo);
            Assert.AreEqual(2, resultServiceTypeInfo.UsedLicenses);

            resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountCommand.AccountId, serviceAvailableSession.Id);

            billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsFalse(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNull(billingAcc.PromisePaymentSum);
            Assert.AreEqual(0, billingAcc.Balance);
        }
    }
}
