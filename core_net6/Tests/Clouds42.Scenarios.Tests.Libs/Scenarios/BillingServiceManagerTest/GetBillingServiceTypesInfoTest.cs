﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Тест получения информации об услугах сервиса
    /// Сценарий:
    ///         1. Создаем сервис и аккаунт
    ///         2. Подключаем этому аккаунту сервис
    ///         3. Получаем информацию о сервисе, проверяем, что информация правильная
    /// </summary>
    public class GetBillingServiceTypesInfoTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly ReactivateServiceManager _reactivateServiceManager;

        public GetBillingServiceTypesInfoTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
        }

        public override void Run()
        {
            var serviceName = "My Custom Service";

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();

            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            GetBillingServiceTypesInfo(billingService.Id, createAccountCommand.AccountId, 0);

            var activateServiceForExistingAccount = _reactivateServiceManager.ActivateServiceForExistingAccount(
                billingService.Id,
                createAccountCommand.AccountAdminId);
            Assert.IsFalse(activateServiceForExistingAccount.Error);

            GetBillingServiceTypesInfo(billingService.Id, createAccountCommand.AccountId, 1);
        }

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usedLicensesCount">Количество использованных лицензий</param>
        private void GetBillingServiceTypesInfo(Guid serviceId, Guid accountId, int usedLicensesCount)
        {
            var billingServiceTypes = DbLayer.BillingServiceTypeRepository.Where(bst => bst.ServiceId == serviceId).ToList();
            Assert.IsNotNull(billingServiceTypes);
            Assert.AreEqual(1, billingServiceTypes.Count);
            
            var billingServiceTypesInfo =
                _billingServiceManager.GetBillingServiceTypesInfo(serviceId, accountId);
            Assert.IsFalse(billingServiceTypesInfo.Error);
            Assert.IsNotNull(billingServiceTypesInfo);
            Assert.AreEqual(billingServiceTypesInfo.Result[0].Id, billingServiceTypes[0].Id);
            Assert.AreEqual(billingServiceTypesInfo.Result[0].BillingType, billingServiceTypes[0].BillingType);
            Assert.AreEqual(billingServiceTypesInfo.Result[0].UsedLicenses, usedLicensesCount);
        }
    }
}

