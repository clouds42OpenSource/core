﻿using System;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста получения транзакций партнера по фильтру
    /// Создаем 3 аккаунта, два аккаунта будут привлеченные первым аккаунтом
    /// 1) Проверяем фильтр по "Названию компании"
    /// 2) Проверяем фильтр по "Период"
    /// </summary>
    public class ScenarioGetPartnerTransactionsByFilterTest: ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioGetPartnerTransactionsByFilterTest()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var rent1CService = GetRent1CServiceOrThrowException();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                }
            });

            createAccountCommand.Run();

            var createAccountHelperTest = ServiceProvider.GetRequiredService<CreateAccountHelperTest>();

            var accountSecond = createAccountHelperTest.Create(createAccountCommand.AccountId, "accountSecond");
            var accountTwo = createAccountHelperTest.Create(createAccountCommand.AccountId, "accountTwo");

            var paymentHelper = ServiceProvider.GetRequiredService<PaymentsHelper>();

            paymentHelper.MakePayment(accountSecond.Id, PaymentType.Outflow, 500, rent1CService.Id,
                DateTime.Now.AddDays(-2));
            paymentHelper.MakePayment(accountTwo.Id, PaymentType.Outflow, 500, rent1CService.Id);

            var result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                { AccountId = createAccountCommand.AccountId, PageNumber = 1 });

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 2)
                throw new InvalidOperationException ("Не верно работает выборка транзакций");


            result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {
                    AccountId = createAccountCommand.AccountId,
                    PageNumber = 1,
                    AccountData = "Two"

            });

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 1 ||
                result.Result.ChunkDataOfPagination.FirstOrDefault().AccountCaption != nameof(accountTwo)) 
                throw new InvalidOperationException ("Не верно работает выборка транзакций по фильтру Название компании");

            result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
            {
                AccountId = createAccountCommand.AccountId,
                PageNumber = 1,
                PeriodFrom = DateTime.Now.AddDays(-3),
                PeriodTo = DateTime.Now.AddDays(-1)
            });

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 1 ||
                result.Result.ChunkDataOfPagination.FirstOrDefault().AccountCaption != nameof(accountSecond))
                throw new InvalidOperationException ("Не верно работает выборка транзакций по фильтру Период");
        }
    }
}
