﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий для проверки получения активных услуг сервиса
    /// Действия:
    /// 1. Создадим аккаунт и сервис. [EntityCreation]
    /// 2. Активируем сервис и изменим ресурс аккаунта. [ServiceActivation]
    /// 3. Проверим, что будут корректно получена услуга сервиса. [CheckGettingActiveServiceTypes]
    /// 4. Проверим, что не будут получены услуги сервиса при невалидных данных. [CheckNotValidData]
    /// </summary>
    public class ScenarioGetActiveAccountServiceTypes : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly ReactivateServiceManager _reactivateServiceManager;

        public ScenarioGetActiveAccountServiceTypes()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _reactivateServiceManager = TestContext.ServiceProvider.GetRequiredService<ReactivateServiceManager>();
        }

        public override void Run()
        {
            #region EntityCreation

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = "My Custom Service",
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();

            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            #endregion

            #region ServiceActivation

            var activateServiceForExistingAccount = _reactivateServiceManager.ActivateServiceForExistingAccount(
                billingService.Id,
                createAccountCommand.AccountAdminId);
            Assert.IsFalse(activateServiceForExistingAccount.Error);

            ChangeAccountResources(accountId, billingService.Id);

            #endregion

            #region CheckGettingActiveServiceTypes

            var result =
                _billingServiceManager.GetActiveAccountServiceTypes(billingService.Id, accountId);

            Assert.IsFalse(result.Error);
            Assert.AreEqual(1, result.Result.Count, "Неверно получили активные ресурсы сервиса");

            var serviceTypeId = result.Result[0];

            var serviceType = DbLayer.BillingServiceTypeRepository.FirstOrDefault(servType => servType.Id == serviceTypeId) ??
                           throw new NotFoundException($"Не удалось найти ресурс с Id {serviceTypeId}");

            Assert.AreEqual(billingService.Id, serviceType.ServiceId);

            #endregion

            #region CheckNotValidData

            result =
                _billingServiceManager.GetActiveAccountServiceTypes(Guid.NewGuid(), accountId);
            Assert.IsFalse(result.Error);
            Assert.AreEqual(0, result.Result.Count);

            result =
                _billingServiceManager.GetActiveAccountServiceTypes(billingService.Id, Guid.NewGuid());
            Assert.IsFalse(result.Error);
            Assert.AreEqual(0, result.Result.Count);

            #endregion
        }

        /// <summary>
        /// Изменить ресурс аккунта для сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="billingServiceId">Id сервиса</param>
        private void ChangeAccountResources(Guid accountId, Guid billingServiceId)
        {
            var billingServiceType =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(servType =>
                    servType.ServiceId == billingServiceId) ??
                throw new NotFoundException($"Не удалось найти услугу сервиса Id {billingServiceId}");

            var accountResource =
                DbLayer.ResourceRepository.FirstOrDefault(res =>
                    res.AccountId == accountId && res.Subject == accountId &&
                    res.BillingServiceTypeId != billingServiceId) ??
                throw new NotFoundException($"Не удалось найти ресурс аккаунта Id {accountId}");

            accountResource.BillingServiceTypeId = billingServiceType.Id;
            DbLayer.ResourceRepository.Update(accountResource);
            DbLayer.Save();
        }
    }
}