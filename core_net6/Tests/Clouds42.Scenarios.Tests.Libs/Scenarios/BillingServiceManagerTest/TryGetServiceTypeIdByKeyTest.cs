﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий получения Id услуги сервиса по ключу
    /// Сценарий:
    ///         1) Создаем аккаунт и сервис с двумя услугами
    ///         2) Получаем Id услуг сервиса по ключам, проверяем, что Id правильные
    ///         3) Пытаемся получить Id услуг сервиса по по не существующим ключам, проверяем, что вернулась ошибка получения
    /// </summary>
    public class TryGetServiceTypeIdByKeyTest : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;

        public TryGetServiceTypeIdByKeyTest()
        {
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            var billingServiceTypes = new List<BillingServiceTypeDto>();
            
            var serviceName = "Good Service";

            var dependServiceTypeId = GetWebServiceTypeOrThrowException().Id;

            var firstServiceTypeKey = Guid.NewGuid();
            var firstServiceType = GenerateBillingServiceType(firstServiceTypeKey, dependServiceTypeId);
            billingServiceTypes.Add(firstServiceType);

            var secondServiceTypeKey = Guid.NewGuid();
            var secondServiceType = GenerateBillingServiceType(secondServiceTypeKey, dependServiceTypeId);
            billingServiceTypes.Add(secondServiceType);
            
            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();
            Assert.IsFalse(createBillingServiceCommand.Result.Error);
            
            TryGetServiceTypeIdByKey(firstServiceTypeKey, firstServiceType.Id);
            TryGetServiceTypeIdByKey(secondServiceTypeKey, secondServiceType.Id);

            TryGetServiceTypeIdByKey(Guid.NewGuid(), firstServiceType.Id, true);
            TryGetServiceTypeIdByKey(Guid.NewGuid(), secondServiceType.Id, true);

            TryGetServiceTypeIdByKey(firstServiceType.Id, firstServiceType.Id);
        }

        /// <summary>
        /// Получить модель услуги сервиса
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="dependServiceTypeId">Id зависимой услуги</param>
        /// <returns>Услуга сервиса</returns>
        private BillingServiceTypeDto GenerateBillingServiceType(Guid key, Guid dependServiceTypeId) => new()
        {
            Id = Guid.NewGuid(), 
            Key = key, 
            Name = $"ServiceType {GetFirstPartOfGuid()}",
            BillingType = BillingTypeEnum.ForAccountUser, 
            DependServiceTypeId = dependServiceTypeId,
            ServiceTypeCost = 500, 
            Description = "Тестовая услуга"
        };

        /// <summary>
        /// Получить первую часть гуида
        /// </summary>
        /// <returns>Первая часть гуида</returns>
        private string GetFirstPartOfGuid()
        {
            var guid = Guid.NewGuid();
            var array = guid.ToString().Split('-');
            return array[0];
        }

        /// <summary>
        /// Попробовать получить ID услуги по ключу(Id) 
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="serviceTypeId">Id услуги сервиса</param>
        /// <param name="isKeyWrong">Ключ не правильный (да/нет)</param>
        private void TryGetServiceTypeIdByKey(Guid key, Guid serviceTypeId, bool isKeyWrong = false)
        {
            var serviceTypeIdByKey = _billingServiceManager.TryGetServiceTypeIdByKey(key);

            if (isKeyWrong)
            {
                Assert.IsTrue(serviceTypeIdByKey.Error);
                Assert.AreEqual(serviceTypeIdByKey.Result, Guid.Empty);
            }

            if (!isKeyWrong)
            {
                Assert.IsFalse(serviceTypeIdByKey.Error);
                Assert.AreEqual(serviceTypeIdByKey.Result, serviceTypeId);
            }
        }
    }
}
