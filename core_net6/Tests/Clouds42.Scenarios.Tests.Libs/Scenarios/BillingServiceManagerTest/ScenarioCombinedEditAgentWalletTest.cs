﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Проверка пересчёта агенского кошелька после череды транзакций и вывода сердств
    /// Сценарий:
    /// 1) Создаём два аккаунта ,партнёр и клиент
    /// 2) Клиент покупает аренду, партнёр получает "партнёрские"
    /// 3) Партнёр кладёт себе на кошелёк денньги через создание
    ///     входящей транзакции в партнёрке
    /// 4) Партнёр делает вывод средств
    /// 5) Вновь делает входящую транзакцию
    /// Проверяем, что у него на кошельке должны быть средства
    /// равные последней входящей транзакции
    /// </summary>
    public class ScenarioCombinedEditAgentWalletTest : ScenarioBase
    {
        public override void Run()
        {
            #region accounts
            var createReferralCommand = new CreateAccountCommand(TestContext);
            createReferralCommand.Run();
            var referral = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createReferralCommand.AccountId);

            var referralAdminRole =
                DbLayer.AccountUserRoleRepository.FirstOrDefault(r =>
                    r.AccountUserId == createReferralCommand.AccountAdminId);
            referralAdminRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(referralAdminRole);
            DbLayer.Save();

            var createClientCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = referral.Id
                }
            });

            createClientCommand.Run();
            var clientAcc = createClientCommand.Account;

            #endregion accounts

            var rent1C = GetResourcesConfigurationOrThrowException(clientAcc.Id, Clouds42Service.MyEnterprise);
            
            rent1C.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.Save();

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.ProlongOrLockExpiredServices();

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            var newPayment = new PaymentDefinitionDto
            {
                Account = clientAcc.Id,
                Date = DateTime.Now,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                Id = Guid.NewGuid(),
                OperationType = PaymentType.Inflow,
                System = PaymentSystem.Admin,
                Status = PaymentStatus.Done,
                Description = "",
                Total = 1500
            };
            var paymentsManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();
            var paymentState = paymentsManager.AddPayment(newPayment);
            if (paymentState.Error || paymentState.Result != PaymentOperationResult.Ok)
                throw new InvalidOperationException("Ошибка внесения платежа платежа");

            var agencyPaymentCreation = new AgencyPaymentCreationDto
            {
                AgentAccountId = referral.Id,
                Sum = 2000,
                PaymentType = PaymentType.Inflow,
                Date = DateTime.Now,
                Comment = "Ручная входящая транзакция",
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                AccountNumber = clientAcc.IndexNumber,
                ClientPaymentSum = 6000
            };

            var agentPaymentManager = TestContext.ServiceProvider.GetRequiredService<AgentPaymentManager>();
            var data = agentPaymentManager.CreateAgentPayment(agencyPaymentCreation);

            Assert.IsFalse(data.Error, "Ошибка создания ручной транзакции");
            
            #region settings

            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = referral.Id,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };

            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == referral.Id);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            var cashOutRequestFile =
                fakeAgencyDocumentFileHelper.GenerateFileData("Очень важный отчет агента.png", "image");

            var files = new List<CloudFileDataDto<byte[]>>
            {
                cashOutRequestFile
                
            };

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var agentWallet = DbLayer.AgentWalletRepository.FirstOrDefault(aw => aw.AccountOwnerId == referral.Id);

            var agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = referral.Id,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = agentWallet.AvailableSum,
                PaySum = agentWallet.AvailableSum,
                Files = files,
                AgentRequisitesId = agentRequisitesId
            };
            var createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            var result = createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);
            Assert.IsFalse(result.Error);
            var agentCashOutRequestInDb = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);
            Assert.IsNotNull(agentCashOutRequestInDb);

            #endregion settings

            var changeAgentCashOutRequestStatusManager = ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();
            var editResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(new ChangeAgentCashOutRequestStatusDto
            {
                RequestNumber = agentCashOutRequestInDb.RequestNumber,
                Sum = agentCashOutRequestInDb.RequestedSum,
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid
            });

            Assert.IsFalse(editResult.Error);

            RefreshDbCashContext(TestContext.Context.AgentWallets);

            var walletSum = DbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == referral.Id).AvailableSum;
            Assert.AreEqual(0,walletSum, "После вывода должно остатся 0р");

            var secondHandTrans = 1000;
            
            agencyPaymentCreation = new AgencyPaymentCreationDto
            {
                AgentAccountId = referral.Id,
                Sum = secondHandTrans,
                PaymentType = PaymentType.Inflow,
                Date = DateTime.Now,
                Comment = "Ручная входящая транзакция",
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                AccountNumber = clientAcc.IndexNumber,
                ClientPaymentSum = 3000
            };

            data = agentPaymentManager.CreateAgentPayment(agencyPaymentCreation);
            Assert.IsFalse(data.Error);
            RefreshDbCashContext(TestContext.Context.AgentWallets);

            walletSum = DbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == referral.Id).AvailableSum;
            Assert.AreEqual(walletSum, secondHandTrans, $"После ввода должно остатся {secondHandTrans}р.");
        }
    }
}
