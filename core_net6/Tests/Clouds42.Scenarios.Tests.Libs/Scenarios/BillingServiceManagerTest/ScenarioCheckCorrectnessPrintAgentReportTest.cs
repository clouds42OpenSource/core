﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Проверка корректности заполнения агенского отчёта о выводе средств
    /// Сценарий:
    /// 1) Создаём два аккаунта : партнёр и клиент
    /// 2) Создаём ручную входящую транзакцию для партнёра
    /// 3) Создаём заявку на вывод средстав, но не подтверждаем
    /// 4) Создаём ещё одну входящую транзакцию для партнёра
    /// 5) Проверяем, что втрая не отображается в документе
    /// 6) Одобряем первую заявку на вывод, проверяем кошелёк,
    /// там должна быть сумма второй транзакции
    /// 7) Создаём вторую заявку на вывод стредств
    /// 8) В отчёте лишь вторая транзакция
    /// 9) Одобряем заявку, в кошельке должен быть ноль.
    /// </summary>
    public class ScenarioCheckCorrectnessPrintAgentReportTest : ScenarioBase
    {
        public override void Run()
        {
            #region accounts
            var createReferralCommand = new CreateAccountCommand(TestContext);
            createReferralCommand.Run();
            var referral = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createReferralCommand.AccountId);

            var referralAdminRole =
                DbLayer.AccountUserRoleRepository.FirstOrDefault(r =>
                    r.AccountUserId == createReferralCommand.AccountAdminId);
            referralAdminRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(referralAdminRole);
            DbLayer.Save();

            var createClientCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = referral.Id
                }
            });

            createClientCommand.Run();
            var clientAcc = createClientCommand.Account;

            #endregion accounts

            var firstPaymentSum = 2000;
            CreatePayment(referral.Id, clientAcc, firstPaymentSum, -3);
            
            #region setings
            var fakeAgencyDocumentFileHelper = TestContext.ServiceProvider.GetRequiredService<FakeAgencyDocumentFileHelper>();
            var createAgentRequisitesManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentRequisitesManager>();
            var requisitesFiles = fakeAgencyDocumentFileHelper.GenerateFilesDataForLegalPersonRequisites();

            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = referral.Id,
                AgentRequisitesStatus = AgentRequisitesStatusEnum.OnCheck,
                AgentRequisitesType = AgentRequisitesTypeEnum.LegalPersonRequisites,
                LegalPersonRequisites = new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = requisitesFiles
                }
            };
            var agentRequisitesId = createAgentRequisitesManager.CreateAgentRequisites(agentRequisitesDto).Result;
            Assert.IsNotNull(agentRequisitesId);

            var agentRequisitesInDb = DbLayer.AgentRequisitesRepository.FirstOrDefault(w =>
                w.Id == agentRequisitesId && w.AccountOwnerId == referral.Id);

            Assert.IsNotNull(agentRequisitesInDb);
            Assert.IsNotNull(agentRequisitesInDb.LegalPersonRequisites);

            agentRequisitesInDb.AgentRequisitesStatus = AgentRequisitesStatusEnum.Verified;
            DbLayer.AgentRequisitesRepository.Update(agentRequisitesInDb);
            DbLayer.Save();

            #endregion setings

            var cashOutRequestFile =
                fakeAgencyDocumentFileHelper.GenerateFileData("Очень важный отчет агента.png", "image");

            var files = new List<CloudFileDataDto<byte[]>>();
            files.Add(cashOutRequestFile);

            var agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = referral.Id,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = firstPaymentSum,
                PaySum = firstPaymentSum,
                Files = files,
                AgentRequisitesId = agentRequisitesId
            };

            #region 1CashOut
            var createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            var result = createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);
            Assert.IsFalse(result.Error);
            var agentCashOutRequestInDb = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);
            Assert.IsNotNull(agentCashOutRequestInDb);
            #endregion 1CashOut

            #region secondTrans
            var secondPaymentSum = 2666;

            CreatePayment(referral.Id, clientAcc, secondPaymentSum, 0);
            #endregion secondTrans

            #region docchecking

            var agentReport =
                TestContext.ServiceProvider.GetRequiredService<AgentReportBuildHelperTestAdapter>()
                    .GenerateAgentReportDtoTest(result.Result);

            Assert.IsTrue(agentReport.AgentPayments.Count==1, "В отчёт должна попасть одна транзакия");
            Assert.IsTrue(agentReport.AgentPayments[0].Sum== firstPaymentSum,"Не та транзакция");

            #endregion docchecking

          var changeAgentCashOutRequestStatusManager = ServiceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();
            var managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(new ChangeAgentCashOutRequestStatusDto
            {
                RequestNumber = agentCashOutRequestInDb.RequestNumber,
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid,
                Sum = agentCashOutRequestInDb.RequestedSum
            });

            Assert.IsFalse(managerResult.Error,"Ошибка редактирования статуса заявки");

            var walletSum = DbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == referral.Id).AvailableSum;
            Assert.AreEqual(secondPaymentSum, walletSum, $"После вывода должно остатся {secondPaymentSum}р");

            #region 2CashOut

            agentCashOutRequest = new CreateAgentCashOutRequestDto
            {
                AccountId = referral.Id,
                RequestStatus = AgentCashOutRequestStatusEnum.InProcess,
                TotalSum = secondPaymentSum,
                PaySum = secondPaymentSum,
                Files = files,
                AgentRequisitesId = agentRequisitesId
            };

            createAgentCashOutRequestManager = TestContext.ServiceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
            result = createAgentCashOutRequestManager.CreateAgentCashOutRequest(agentCashOutRequest);
            agentCashOutRequestInDb = DbLayer.AgentCashOutRequestRepository.FirstOrDefault(w => w.Id == result.Result);
            Assert.IsFalse(result.Error,"Ошибка создания второй заявки на вывод средств");
            #endregion 2CashOut

            #region docchecking2
            agentReport =
                TestContext.ServiceProvider.GetRequiredService<AgentReportBuildHelperTestAdapter>()
                    .GenerateAgentReportDtoTest(result.Result);

            Assert.IsTrue(agentReport.AgentPayments.Count == 1, "В отчёт должна попасть одна транзакия");
            Assert.IsTrue(agentReport.AgentPayments[0].Sum == secondPaymentSum, "Не та транзакция");

            #endregion docchecking2
            
            managerResult = changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(new ChangeAgentCashOutRequestStatusDto
            {
                RequestNumber = agentCashOutRequestInDb.RequestNumber,
                Sum = agentCashOutRequestInDb.RequestedSum,
                AgentCashOutRequestStatus = AgentCashOutRequestStatusEnum.Paid
            });

            Assert.IsFalse(managerResult.Error,"Ошибка изменения статуса второй заявки на вывод средств");

            walletSum = DbLayer.AgentWalletRepository.FirstOrDefault(w => w.AccountOwnerId == referral.Id).AvailableSum;
            Assert.AreEqual(0,walletSum, "После вывода должно остатся 0р");

        }

        /// <summary>
        /// Создать агентский платеж
        /// </summary>
        /// <param name="referralAccountId">ID аккаунта рефферала</param>
        /// <param name="clientAccount">Аккаунт клиента</param>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="dateTimeDiff">Разница во времени при создании платежа</param>
        private void CreatePayment(Guid referralAccountId, Account clientAccount, decimal paymentSum, int dateTimeDiff)
        {
            var paymentCreationDateTime = DateTime.Now.AddMinutes(dateTimeDiff);

            var agencyPaymentCreation = new AgencyPaymentCreationDto
            {
                AgentAccountId = referralAccountId,
                Sum = paymentSum,
                PaymentType = PaymentType.Inflow,
                Date = paymentCreationDateTime,
                Comment = "Ручная входящая транзакция",
                AgentPaymentSourceType = AgentPaymentSourceTypeEnum.ManualInput,
                AccountNumber = clientAccount.IndexNumber,
                ClientPaymentSum = 6000
            };

            var agentPaymentManager = TestContext.ServiceProvider.GetRequiredService<AgentPaymentManager>();
            var data = agentPaymentManager.CreateAgentPayment(agencyPaymentCreation);
            Assert.IsFalse(data.Error, "Ошибка создания ручной транзакции");

            var clientPayment = new Payment
            {
                AccountId = clientAccount.Id,
                Date = paymentCreationDateTime,
                TransactionType = TransactionType.Money,
                Id = Guid.NewGuid(),
                OperationType = PaymentType.Inflow.ToString(),
                Status = PaymentStatus.Done.ToString(),
                PaymentSystem = PaymentSystem.ControlPanel.ToString(),
                Sum = agencyPaymentCreation.Sum,
                OriginDetails = "core-test"
            };

            DbLayer.PaymentRepository.Insert(clientPayment);

            DbLayer.Save();
            var agentPayment = DbLayer.AgentPaymentRepository.FirstOrDefault(w => w.Sum == agencyPaymentCreation.Sum);
            
            agentPayment.ClientPaymentId = clientPayment.Id;
            agentPayment.PaymentEntryDateTime = paymentCreationDateTime;

            DbLayer.AgentPaymentRepository.Update(agentPayment);
            DbLayer.Save();
        }
    }
}
