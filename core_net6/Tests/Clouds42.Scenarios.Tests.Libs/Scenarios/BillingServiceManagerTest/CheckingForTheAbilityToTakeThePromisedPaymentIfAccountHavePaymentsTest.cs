﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки возможности взятия ОП
    /// 
    ///     Действия: Создать аккаунт, проводим платеж
    /// 
    ///     Проверка: возможность взять ОП
    /// </summary>
    public class CheckingForTheAbilityToTakeThePromisedPaymentIfAccountHavePaymentsTest : ScenarioBase
    {
        public override void Run()
        {
            //Создаем аккаунт
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            //Создаем входящий платеж
            CreateInflowPayment(createAccountCommand.AccountId, null, 100);

            //Проверяем возможность списания
            var result = TestContext.ServiceProvider.GetRequiredService<IAbilityToCreatePaymentProvider>()
                .CanUsePromisePayment(createAccountCommand.AccountId);

            Assert.IsTrue(result);
        }
    }
}
