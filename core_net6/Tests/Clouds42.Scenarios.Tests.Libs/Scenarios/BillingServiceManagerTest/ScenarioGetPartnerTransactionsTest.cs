﻿using System;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста получения транзакций партнера
    /// Создаем 3 аккаунта, и 2 сервиса. Первые два аккаунта
    /// являются владельцами своего сервиса. Третий аккаунт является
    /// привлеченным первым аккаунтом, но использует оба кастомных сервиса
    /// 1) Проверяем результат если аккаунт без привленных пользователей, но владелиц сериса
    /// 2) Проверяем результат первого аккаунта у которого есть 1 привлеченный клиент
    /// 3) Проверяем что сумма транзакций считается верно, что 100% берется только по сервису аккаунта
    /// </summary>
    public class ScenarioGetPartnerTransactionsTest : ScenarioBase
    {
        private readonly CreateAccountHelperTest _createAccountHelperTest;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly IActivateServiceForRegistrationAccountProvider _activateServiceForRegistrationAccountProvider;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;

        public ScenarioGetPartnerTransactionsTest()
        {
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createAccountHelperTest = ServiceProvider.GetRequiredService<CreateAccountHelperTest>();
            _activateServiceForRegistrationAccountProvider = TestContext.ServiceProvider.GetRequiredService<IActivateServiceForRegistrationAccountProvider>();
        }

        public override void Run()
        {
            decimal defaultSum = 200;
            var rent1CService = GetRent1CServiceOrThrowException();
            var createFirstAccountCommand = new CreateAccountCommand(TestContext);
            createFirstAccountCommand.Run();
            var anotherAccount = _createAccountHelperTest.Create();
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();

            #region 1customService

            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                serviceOwnerAccountId => new CreateBillingServiceTest(TestContext, createFirstAccountCommand.AccountId)
                {
                    BillingServiceTypes = serviceTypeDtos,
                    Name = "Кастомный сервис 1",
                    AccountId = createFirstAccountCommand.AccountId
                });
            createCustomService.Run();

            var customService = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Id == createCustomService.Id);
            customService.AccountOwnerId = createFirstAccountCommand.AccountId;
            DbLayer.BillingServiceRepository.Update(customService);
            DbLayer.Save();

            #endregion 1customService

            #region 2custService

            var createCustomServiceTwoTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Кастомный сервис 2",
                AccountId = anotherAccount.Id
            };

            var createCustomServiceTwo =
                new CreateEmptyBillingServiceCommand(TestContext,
                    createCustomServiceTwoTest);
            createCustomServiceTwo.Run();

            var customServiceTwo =
                DbLayer.BillingServiceRepository.FirstOrDefault(w => w.AccountOwnerId == anotherAccount.Id);

            #endregion 2custService

            #region 3acc

            var createThirdAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = anotherAccount.Id
                }
            });
            createThirdAccountCommand.Run();

            #endregion 3acc

            #region 2serFor3acc

            _activateServiceForRegistrationAccountProvider.ActivateServiceForExistingAccount(customService.Id,
                createThirdAccountCommand.AccountAdminId);

            #endregion 2serFor3acc

            var calculateAgentRewardProvider = ServiceProvider.GetRequiredService<ICalculateAgentRewardProvider>();
            var paymentHelper = ServiceProvider.GetRequiredService<PaymentsHelper>();

            paymentHelper.MakePayment(createThirdAccountCommand.AccountId);
            paymentHelper.MakePayment(createThirdAccountCommand.AccountId, PaymentType.Outflow, defaultSum,
                rent1CService.Id);
            paymentHelper.MakePayment(createThirdAccountCommand.AccountId, PaymentType.Outflow, defaultSum,
                createCustomService.Id);
            paymentHelper.MakePayment(createThirdAccountCommand.AccountId, PaymentType.Outflow, defaultSum,
                customServiceTwo.Id);

            var result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {AccountId = createFirstAccountCommand.AccountId, PageNumber = 1});

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 1)
                throw new InvalidOperationException(
                    "Не верно работает выборка транзакций, аккаунт является владельцом сервиса");

            var agencyAgreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();

            var defaultSumByAgencyAgreement = defaultSum * agencyAgreement.ServiceOwnerRewardPercent / 100;

            if (result.Result.ChunkDataOfPagination.FirstOrDefault()?.TransactionSum != defaultSumByAgencyAgreement)
                throw new InvalidOperationException("Не верно считается сумма партнера");

            var isVipAccount = IsVipAccount(createThirdAccountCommand.AccountId);

            var agentReward =
                calculateAgentRewardProvider.Calculate(defaultSum, isVipAccount,
                    rent1CService.SystemService) +
                calculateAgentRewardProvider.Calculate(defaultSum, isVipAccount, null);

            result = _billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {AccountId = anotherAccount.Id, PageNumber = 1});

            if (result.Result.ChunkDataOfPagination.Count() != 2)
                throw new InvalidOperationException("Не корректно работает выборка транзакций для партнера");

            AssertHelper.Execute(() =>
                Assert.AreEqual(result.Result.ChunkDataOfPagination.Sum(w => w.TransactionSum), agentReward));
        }
    }
}
