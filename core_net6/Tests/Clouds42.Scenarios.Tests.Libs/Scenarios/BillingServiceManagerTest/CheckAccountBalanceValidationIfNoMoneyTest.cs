﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для достаточного баланса
    /// 
    ///     Действия: проверить сумму на списание по аккаунту, в случае если баланс ниже нужной суммы
    /// 
    ///     Проверка: поля модели соотвествуют тому что баланса не хватает, сумма нехватки указана верная, есть возможность взять ОП
    /// </summary>
    public class CheckAccountBalanceValidationIfNoMoneyTest : ScenarioBase
    {
        public override void Run()
        {
            //Создаем аккаунт
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();
            
            //Проверяем возможность списания
            var result = TestContext.ServiceProvider.GetRequiredService<IBillingPaymentsProvider>()
                .CanCreatePayment(createAccountCommand.AccountId, 50, out decimal needMoney);

            Assert.IsTrue(result != PaymentOperationResult.Ok);
            Assert.AreEqual(50M, needMoney);
        }
    }
}
