﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Billing.BillingServices.Helpers;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.TestCommands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий для проверки сохранения настроек с управления сервиса
    /// Действия:
    /// 1. Создадим аккаунт и получим необходимые сущности. [EntitiesCreation]
    /// 2. Проверим, что новые настройки корректно сохранились. [CheckNewOptionsSavedCorrectly]
    /// 3. Проверим, что будет получена ошибка при невалидных данных. [CheckNotValidData]
    /// </summary>
    public class ScenarioSaveNewOptionsControl : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly BillingServiceTypeRateHelper _typeRateHelper;
        private readonly IResourceDataProvider _resourceDataProvider;

        public ScenarioSaveNewOptionsControl()
        {
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _typeRateHelper = TestContext.ServiceProvider.GetRequiredService<BillingServiceTypeRateHelper>();
            _resourceDataProvider = TestContext.ServiceProvider.GetRequiredService<IResourceDataProvider>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);

            var createAccountCommandOne = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommandOne.Run();

            var accountId = createAccountCommandOne.AccountId;

            var serviceMyEnterprise =
                DbLayer.BillingServiceRepository.FirstOrDefault(serv =>
                    serv.SystemService == Clouds42Service.MyEnterprise);

            var rates = _typeRateHelper.GetRates(serviceMyEnterprise.Id, accountId);
            var newRateCost = 100;

            var newRates = CreateNewRatesCost(rates.Select(rate => rate.Key).ToList(), newRateCost);

            var model = new BillingServiceOptionsControlDto(accountId, serviceMyEnterprise.Id, newRates, DateTime.Now.AddDays(1));

            #endregion

            #region CheckNewOptionsSavedCorrectly

            var result = _billingServiceManager.SaveNewOptionsControl(model);
            Assert.IsFalse(result.Error);
            CheckNewOptionsSaved(model, newRates);

            model.NewExpireDate = DateTime.Now.AddDays(-5);
            result = _billingServiceManager.SaveNewOptionsControl(model);
            Assert.IsFalse(result.Error);
            CheckNewOptionsSaved(model, newRates);

            #endregion

            #region CheckNotValidData

            model.AccountId = Guid.NewGuid();
            result = _billingServiceManager.SaveNewOptionsControl(model);
            Assert.IsTrue(result.Error);

            model.AccountId = accountId;
            model.NewCosts = new Dictionary<Guid, decimal>
            {
                {Guid.NewGuid(), 150 }
            };

            result = _billingServiceManager.SaveNewOptionsControl(model);
            Assert.IsTrue(result.Error);

            #endregion
        }

        /// <summary>
        /// Создать новую стоимость услуг 
        /// </summary>
        /// <param name="servicesId">Id услуг</param>
        /// <param name="newRatesCost">Новая стоимость</param>
        /// <returns>Словрь Id услуги - новая цена</returns>
        private Dictionary<Guid, decimal> CreateNewRatesCost(List<Guid> servicesId, decimal newRatesCost)
        {
            var newRates = new Dictionary<Guid, decimal>();
            foreach (var rateId in servicesId)
                newRates.Add(rateId, newRatesCost);

            return newRates;
        }

        /// <summary>
        /// Проверить что новые настройки были сохранены корректно
        /// </summary>
        /// <param name="model">Модель настроек</param>
        /// <param name="newRatesCost">Новая стоимость</param>
        private void CheckNewOptionsSaved(BillingServiceOptionsControlDto model, Dictionary<Guid, decimal> newRatesCost)
        {
            var resources = DbLayer.ResourceRepository
                .WhereLazy(w => w.AccountId == model.AccountId && w.BillingServiceType.ServiceId == model.BillingServiceId).ToList();

            foreach (var resource in resources)
            {
                if (!newRatesCost.ContainsKey(resource.BillingServiceTypeId))
                    continue;

                Assert.AreEqual(newRatesCost[resource.BillingServiceTypeId], resource.Cost);
            }

            var resourceConfiguration = DbLayer.ResourceConfigurationRepository
                .FirstOrDefault(x => x.AccountId == model.AccountId && x.BillingServiceId == model.BillingServiceId);

            Assert.AreEqual(model.NewExpireDate, resourceConfiguration.ExpireDate);
            Assert.AreEqual(model.NewExpireDate <= DateTime.Now, resourceConfiguration.FrozenValue);

            var sumResources = _resourceDataProvider.GetUsedPaidResourcesCostByService(model.BillingServiceId, model.AccountId);
            Assert.AreEqual(sumResources, resourceConfiguration.Cost);
        }
    }
}
