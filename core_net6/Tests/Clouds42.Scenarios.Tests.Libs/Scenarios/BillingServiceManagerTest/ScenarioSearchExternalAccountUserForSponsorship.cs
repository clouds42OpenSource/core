﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий для проверки поиска внешнего пользователя для спонсирования.
    /// 1. Создадим два аккаунта и неоходимые сущности. [EntityCreation]
    /// 2. Проверим, что внешний пользователь будет найден с правильными параметрами. [CheckExternalAccountUserFound]
    /// 3. Проверим, что внешний пользователь не будет найден при неправильно заполненной эл. почтой. [CheckExternalUserWontBeFoundWithNotValidEmail]
    /// 4. Проверим, что внешний пользователь не будет найден если сервис аккаунта спонсора заблокирован. [CheckSponsoredAccountServiceIsFrozen]
    /// 5. Проверим, что внешний пользователь не будет найден если сервис аккаунта спонсора не оплачен. [CheckSponsoredAccountServiceIsNotPaid]
    /// 6. Проверим, что нельзя спонсировать пользователей из разных стран. [CheckCantSponsorUserFromAnotherCountry]
    /// 7. Проверим, что внешний пользователь не будет найден, если сервис спонсируемого аккаунта заблокирован. [CheckSponsoringAccountServiceIsFrozen]
    /// 8. Проверим, что внешний пользователь не будет найден, если у него уже подключена выбранная услуга. [CheckUserHasAlreadyThisBillingServiceType]
    /// 9. Проверим, что внешний пользователь не будет найден при передаче невалидных данных. [CheckNotValidData]
    /// </summary>
    public class ScenarioSearchExternalAccountUserForSponsorship : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioSearchExternalAccountUserForSponsorship()
        {
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            #region EntityCreation

            var createAccountCommandOne = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommandOne.Run();

            var accountIdOne = createAccountCommandOne.AccountId;
            var accountOne = DbLayer.AccountsRepository.GetAccount(accountIdOne);
            var accountAdminOne =
                DbLayer.AccountUsersRepository.FirstOrDefault(user =>
                    user.Id == createAccountCommandOne.AccountAdminId);

            var createAccountCommandTwo = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommandTwo.Run();

            var accountIdTwo = createAccountCommandTwo.AccountId;
            var accountTwo = DbLayer.AccountsRepository.GetAccountAndAccountUsers(accountIdTwo);
            var accountAdminTwo =
                DbLayer.AccountUsersRepository.FirstOrDefault(user =>
                    user.Id == createAccountCommandTwo.AccountAdminId);

            var serviceMyEnterprise =
                DbLayer.BillingServiceRepository.FirstOrDefault(serv =>
                    serv.SystemService == Clouds42Service.MyEnterprise);

            var serviceTypeId = serviceMyEnterprise.BillingServiceTypes.FirstOrDefault().Id;

            var resourceTwo = DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.Subject == accountAdminTwo.Id && res.AccountId == accountIdTwo &&
                res.BillingServiceTypeId == serviceTypeId);
            resourceTwo.Subject = null;
            DbLayer.ResourceRepository.Update(resourceTwo);
            DbLayer.Save();

            var createPaymentCommand =
                new CreatePaymentCommand(TestContext, new PaymentDefinitionModelTest(accountIdOne));
            createPaymentCommand.Run();

            var model = new SearchExternalAccountUserForSponsorshipDto
            {
                AccountId = accountIdOne,
                ServiceId = serviceMyEnterprise.Id,
                ServiceTypeId = serviceTypeId,
                Email = accountAdminTwo.Email
            };

            #endregion

            #region CheckExternalAccountUserFound

            var result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            CheckAccountUserBillingServiceType(result.Result, accountAdminTwo.Id);

            #endregion

            #region CheckExternalUserWontBeFoundWithNotValidEmail

            model.Email = string.Empty;
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            model.Email = "NotFoundEmail";
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            model.Email = accountAdminOne.Email;
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion

            #region CheckSponsoredAccountServiceIsFrozen

            model.Email = accountAdminTwo.Email;
            ChangeResourceConfigurationFrozenValue(accountIdOne, serviceMyEnterprise.Id);
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion

            #region CheckSponsoredAccountServiceIsNotPaid

            ChangeResourceConfigurationFrozenValue(accountIdOne, serviceMyEnterprise.Id, false);
            ChangePaymentStatus(createPaymentCommand.PaymentId, PaymentStatus.Canceled);
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion

            #region CheckCantSponsorUserFromAnotherCountry

            ChangePaymentStatus(createPaymentCommand.PaymentId);
            var accountOneLocaleId = GetAccountLocaleId(accountOne.Id);
            var locale = DbLayer.LocaleRepository.FirstOrDefault(loc => loc.ID != accountOneLocaleId);
            ChangeAccountLocale(accountIdOne, locale.ID);
            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion

            #region CheckSponsoringAccountServiceIsFrozen

            ChangeAccountLocale(accountIdOne, GetAccountLocaleId(accountTwo.Id));
            ChangeResourceConfigurationFrozenValue(accountIdTwo, serviceMyEnterprise.Id);

            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion

            #region CheckUserHasAlreadyThisBillingServiceType

            var resourceAdminTwo = DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.Subject == accountAdminTwo.Id);

            model.ServiceTypeId = resourceAdminTwo.BillingServiceTypeId;

            ChangeResourceConfigurationFrozenValue(accountIdTwo, serviceMyEnterprise.Id, false);

            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            model.ServiceTypeId = serviceTypeId;

            #endregion

            #region CheckNotValidData

            model.AccountId = Guid.NewGuid();

            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            model.AccountId = accountIdOne;
            model.ServiceTypeId = Guid.NewGuid();

            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsFalse(result.Error);

            model.ServiceTypeId = serviceTypeId;
            model.ServiceId = Guid.NewGuid();

            result = _billingServiceManager.SearchExternalAccountUserForSponsorship(model);
            Assert.IsTrue(result.Error);

            #endregion
        }

        /// <summary>
        /// Изменить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localeId">Id локали</param>
        private void ChangeAccountLocale(Guid accountId, Guid localeId) => TestContext.ServiceProvider.
            GetRequiredService<IUpdateAccountConfigurationProvider>().UpdateLocaleId(accountId, localeId);

        /// <summary>
        /// Изменить статус платежа
        /// </summary>
        /// <param name="paymentId">Id платежа</param>
        /// <param name="paymentStatus">Статус платежа</param>
        public void ChangePaymentStatus(Guid paymentId, PaymentStatus paymentStatus = PaymentStatus.Done)
        {
            var payment = DbLayer.PaymentRepository.FindPayment(paymentId);

            payment.StatusEnum = paymentStatus;
            DbLayer.PaymentRepository.Update(payment);
            DbLayer.Save();
        }

        /// <summary>
        /// Изменить значение признака заблокированно ресурс конфигурации
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="frozenValue">Значение заблокированно</param>
        public void ChangeResourceConfigurationFrozenValue(Guid accountId, Guid serviceId, bool frozenValue = true)
        {
            var currentResourceConfiguration =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(x =>
                    x.AccountId == accountId && x.BillingServiceId == serviceId);
            currentResourceConfiguration.Frozen = frozenValue;
            DbLayer.ResourceConfigurationRepository.Update(currentResourceConfiguration);
            DbLayer.Save();
        }

        /// <summary>
        /// Проверить модель пользователя и его услуг  
        /// </summary>
        /// <param name="accountUserBillingServiceTypeDto">Модель пользователя и его услуг  </param>
        /// <param name="userId">Id пользователя</param>
        public void CheckAccountUserBillingServiceType(
            AccountUserBillingServiceTypeDto accountUserBillingServiceTypeDto, Guid userId)
        {
            var user = DbLayer.AccountUsersRepository.GetAccountUser(userId);
            var accountName = user.Account.AccountCaption;

            Assert.AreEqual(userId, accountUserBillingServiceTypeDto.AccountUserId);
            Assert.AreEqual(0, accountUserBillingServiceTypeDto.Services.Count);
            Assert.AreEqual(0, accountUserBillingServiceTypeDto.SponsoredServiceTypesByMyAccount.Count);
            Assert.IsTrue(accountUserBillingServiceTypeDto.Sponsorship.I);
            Assert.AreEqual(accountName, accountUserBillingServiceTypeDto.Sponsorship.Label);
        }
    }
}
