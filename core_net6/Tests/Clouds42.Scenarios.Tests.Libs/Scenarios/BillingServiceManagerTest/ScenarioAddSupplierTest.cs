﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.Suppliers.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Проверка создания новых поставщиков
    /// Сценарий :
    /// 1) Сначала пытаемся создать поставщика "по умолчанию"
    /// без указания договора аферты, получаем ошибку
    /// 2) Создаём обычного поставщика-проверяем, что ему выставился стандартный договор
    /// 3) Пытаемся создать ещё одного поставщика с тем же кодом-получаем ошибку
    /// </summary>
    public class ScenarioAddSupplierTest : ScenarioBase
    {
        public override void Run()
        {
            var ruLocale = DbLayer.LocaleRepository.FirstOrDefault(l => l.CurrencyCode == 643);
            var standartRuAggrementId = DbLayer.SupplierRepository
                .FirstOrDefault(s => s.LocaleId == ruLocale.ID && s.IsDefault).AgreementId;
            var printedHtmlFormInvoiceId = DbLayer.PrintedHtmlFormRepository
                .FirstOrDefault(f => f.Name == "Россия (ООО \"УчетОнлайн\")- Форма счета на оплату").Id;
            var printedHtmlFormInvoiceReceiptId = DbLayer.PrintedHtmlFormRepository
                .FirstOrDefault(f => f.Name == "Россия (ООО \"УчетОнлайн\")- Форма фискального чека").Id;

            var supplierModel = new SupplierDto
            {
                Name = $"Тестовый поставщик №{new Random().Next(1, 99)}",
                LocaleId = ruLocale.ID,
                Code = $"{new Random().Next(1, 99)}",
                IsDefault = true,
                PrintedHtmlFormInvoiceId = printedHtmlFormInvoiceId,
                PrintedHtmlFormInvoiceReceiptId  = printedHtmlFormInvoiceReceiptId
            };

            var newSupplCode = supplierModel.Code;

            var supplierReferenceManager = TestContext.ServiceProvider.GetRequiredService<SupplierReferenceManager>();

            var result = supplierReferenceManager.CreateSupplier(supplierModel);
            Assert.IsTrue(result.Error,"Нельзя создавать дефолтного поставщика без договора аферты");

            supplierModel.IsDefault = false;
            result = supplierReferenceManager.CreateSupplier(supplierModel);
            Assert.IsFalse(result.Error, "Не дефолтный поставщик без договора аферты создаваться должен");

            var newSupplAggrId = DbLayer.SupplierRepository.FirstOrDefault(s => s.Code == newSupplCode).AgreementId;
            Assert.AreEqual(newSupplAggrId,standartRuAggrementId,"Должен был выставится стандартный ру договор.");

            RefreshDbCashContext(TestContext.Context.Suppliers);

            var secondSupplierModel = new SupplierDto
            {
                Name = $"Тестовый поставщик №{new Random().Next(1, 99)}",
                LocaleId = ruLocale.ID,
                Code = newSupplCode,
                IsDefault = false,
                PrintedHtmlFormInvoiceId = printedHtmlFormInvoiceId,
                PrintedHtmlFormInvoiceReceiptId = printedHtmlFormInvoiceReceiptId
            };

            result = supplierReferenceManager.CreateSupplier(secondSupplierModel);
            Assert.IsTrue(result.Error, "Нельзя создавать поставщика с уже существующим кодом.");

             var newSupplier = DbLayer.SupplierRepository.FirstOrDefault(s => s.Code == newSupplCode);
             Assert.IsNotNull(newSupplier,"Ошибка добавления нового поставщика");

             DbLayer.SupplierRepository.Delete(newSupplier);
             DbLayer.Save();

        }
    }
}
