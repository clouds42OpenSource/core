﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Тест проверки создания цен для услуг сервиса, если они = 0
    /// Сценарий:
    ///         1. Создаем Аккаунт
    ///         2. Создаем сервис с двумя услугами, у которых стоимость = 0
    ///         3. Проверяем, что создались цены для этих услуг
    /// </summary>
    public class CheckRatesCreatedIfServiceCostIsZero : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public CheckRatesCreatedIfServiceCostIsZero()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var standardResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;
            
            var billingServiceTypes = GenerateBillingServiceTypes(2, standardResourceTypeId);
            var billingService = CreateCustomService(billingServiceTypes);
            Assert.IsNotNull(billingService);
            
            var rates = DbLayer.RateRepository.Where(r => r.BillingServiceType.ServiceId == billingService.Id);
            Assert.IsNotNull(rates);
            Assert.AreEqual(billingServiceTypes.Count, rates.Count());

        }

        /// <summary>
        /// Создать кастомный сервис
        /// </summary>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService CreateCustomService(List<BillingServiceTypeDto> billingServiceTypes)
        {
            var createIndustry = new CreateIndustryCommand(TestContext);
            createIndustry.Run();

            CreateDelimiterTemplate();

            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Key = Guid.NewGuid(),
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            Assert.IsFalse(createBillingServiceCommand.Result.Error, createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            Assert.IsNotNull(service, "Произошла ошибка в процессе создания. Сервис не найден.");

            return service;
        }

        /// <summary>
        /// Сгенерировать услуги сервиса
        /// </summary>
        /// <param name="count">количество услуг</param>
        /// <param name="dependServiceId">От какого сервиса зависят</param>
        /// <returns>Список услуг сервиса</returns>
        private List<BillingServiceTypeDto> GenerateBillingServiceTypes(int count, Guid dependServiceId)
        {
            var billingServiceTypes =
                _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(count, dependServiceId);

            foreach (var billingServiceTypeDto in billingServiceTypes)
            {
                billingServiceTypeDto.BillingType = BillingTypeEnum.ForAccountUser;
                billingServiceTypeDto.ServiceTypeCost = 0;
            }

            return billingServiceTypes;
        }
    }
}
