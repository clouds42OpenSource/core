﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки изменения сервиса Дополнительные сеансы
    ///     
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    ///     подключены 3 дополнительных сессии
    /// 
    ///     Действия: уменьшаем до 2 сессий, а потом возвращаем до 3
    /// 
    ///     Проверка: сервис должен быть подключен, сумма не списана с баланса, 
    ///     не требует покупки после увеличения с 2 до 3 сессий
    /// </summary>
    public class ScenarioChangeCountAvailableSession : ScenarioBase
    {

        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);

            //Аренда +10 дней от тек.даты. 
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba =>
                ba.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            Assert.IsNotNull(billingAccount);

            //Пополнили баланс 
            var balance = 2850m;
            var billingService = GetRent1CServiceOrThrowException();

            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, balance);

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");
            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Complete);

            //покупаем Дополнительные сеансы за свои деньги
            var result = _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                false, сloudServicePayment.CostOftariff, 3);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            Assert.IsTrue(result.Result.Complete);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.AreEqual(2850 - сloudServicePayment.CostOftariff, billingAccount.Balance);

            //изменяем количество допсеансов до 2
            var сloudChangeServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 2).Result;

            Assert.IsNotNull(сloudChangeServicePayment);
            Assert.IsTrue(сloudChangeServicePayment.Complete);
            Assert.AreEqual(сloudChangeServicePayment.CostOftariff, 0);
            Assert.AreEqual(2850 - сloudServicePayment.CostOftariff, billingAccount.Balance);

            var resultServiceTypeInfo = _billingServiceManager.GetBillingServiceTypeInfo(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId).Result;

            Assert.IsNotNull(resultServiceTypeInfo);
            Assert.AreEqual(resultServiceTypeInfo.ServiceExpireDate.ToShortDateString(), dateValue.ToShortDateString());
            Assert.AreEqual(2, resultServiceTypeInfo.UsedLicenses);

            //изменяем количество допсеансов до 3
            сloudChangeServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudChangeServicePayment);
            Assert.IsTrue(сloudChangeServicePayment.Complete);
            Assert.AreEqual(сloudChangeServicePayment.CostOftariff, 0);
            Assert.AreEqual(2850 - сloudServicePayment.CostOftariff, billingAccount.Balance);

            resultServiceTypeInfo = _billingServiceManager.GetBillingServiceTypeInfo(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId).Result;

            Assert.IsNotNull(resultServiceTypeInfo);
            Assert.AreEqual(resultServiceTypeInfo.ServiceExpireDate.ToShortDateString(), dateValue.ToShortDateString());
            Assert.AreEqual(3, resultServiceTypeInfo.UsedLicenses);
        }
    }
}
