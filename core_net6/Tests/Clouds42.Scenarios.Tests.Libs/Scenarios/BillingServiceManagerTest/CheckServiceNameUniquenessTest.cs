﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.BillingServices.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Тест проверки уникальности названия нового сервиса
    /// Сценарий:
    ///         1. Проверяем на названии существующего сервиса
    ///         2. Проверяем, чтовернулся правильный ответ и название не уникально
    ///         3. Передаем новое имя сервиса, проверяем, что название уникально
    ///         4. Создаем сервис с этим названием
    /// </summary>
    public class CheckServiceNameUniquenessTest : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public CheckServiceNameUniquenessTest()
        {
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            var serviceName = "Fasta";
            CheckServiceNameUniqueness(serviceName,false);

            serviceName = "RandomServceName";
            CheckServiceNameUniqueness(serviceName);

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);


            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();

            var billingService = DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);
            Assert.IsNotNull(billingService);
        }

        /// <summary>
        /// Проверить уникальность названия нового сервиса
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="isUniq">Название уникально (да/нет)</param>
        private void CheckServiceNameUniqueness(string serviceName, bool isUniq = true)
        {
            var checkUniquenessOfNameOfNewService = _billingServiceManager.CheckUniquenessOfNameOfNewService(serviceName);
            Assert.IsFalse(checkUniquenessOfNameOfNewService.Error);

            if (isUniq)
                Assert.IsTrue(checkUniquenessOfNameOfNewService.Result);
            else
                Assert.IsFalse(checkUniquenessOfNameOfNewService.Result);
        }
    }
}
