﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий для проверки уникальности имени сервиса
    /// Действия:
    /// 1. Созадим аккаунт и сервис. [EntitiesCreation]
    /// 2. Проверим, что имя неуникально. [CheckNotUniqueName]
    /// 3. Проверим кейсы для уникальности имени. [CheckUniqueNames]
    /// </summary>
    public class ScenarioCheckUniquenessOfServiceName : ScenarioBase
    {
        private readonly BillingServiceManager _billingServiceManager;
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public ScenarioCheckUniquenessOfServiceName()
        {
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var serviceName = "Test service 1";
            var newServiceName = "New service name";

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();

            var serviceId = createBillingServiceCommand.Id;

            #endregion

            #region CheckNotUniqueName

            var result = _billingServiceManager.CheckUniquenessOfServiceName(Guid.NewGuid(), serviceName);
            Assert.IsFalse(result.Result);

            #endregion

            #region CheckUniqueNames

            result = _billingServiceManager.CheckUniquenessOfServiceName(serviceId, serviceName);
            Assert.IsTrue(result.Result);

            result = _billingServiceManager.CheckUniquenessOfServiceName(null, newServiceName);
            Assert.IsTrue(result.Result);

            result = _billingServiceManager.CheckUniquenessOfServiceName(serviceId, newServiceName);
            Assert.IsTrue(result.Result);

            #endregion
        }
    }
}