﻿using System;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Проверка транзакций после того как привлечённый клиент
    /// продлевает заблокированную аренду.
    /// Сценарий: создаём 2 аккаунта, партнёр и клиент. Блокируем аренду и ЗД.
    /// Пополняем счёт на сумму, достаточную для оплаты аренды лишь для одного
    /// пользователя. Отключаем аренду второму пользователю.
    /// Проверяем, что аренда и ЗД активировались, для реферала создалась новая транзакция
    /// и пополнен кошелёк.
    /// </summary>
    public class ScenarioGetPartnerTransactionsWhenClientProlongRent : ScenarioBase
    {
        public override void Run()
        {
            #region acc

            var createReferralCommand = new CreateAccountCommand(TestContext);
            createReferralCommand.Run();
            var referral = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createReferralCommand.AccountId);

            var createClientCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    ReferralAccountId = referral.Id
                }
            });

            createClientCommand.Run();
            var clientAcc = createClientCommand.Account;

            #endregion acc

            var secondUserId = CreateAccountUser(clientAcc.Id);
            var rent1CService = GetRent1CServiceOrThrowException();

            ApplyRent1CChangesForUser(secondUserId, clientAcc.Id, true);
            BlockRent1CAndEsdlForAccount(clientAcc.Id);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = clientAcc.Id,
                Date = DateTime.Now,
                Description = "Пополнение баланса",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                BillingServiceId = rent1CService.Id,
                OperationType = PaymentType.Inflow,
                Total = 1500m,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            ApplyRent1CChangesForUser(secondUserId, clientAcc.Id, false);

            var calculateAgentRewardProvider = ServiceProvider.GetRequiredService<ICalculateAgentRewardProvider>();

            var billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            var result = billingServiceManager.GetPartnerTransactions(new PartnerTransactionsFilterDto
                {AccountId = referral.Id, PageNumber = 1});

            if (result.Error || result.Result.ChunkDataOfPagination.Count() != 1)
                throw new InvalidOperationException("Не верно работает выборка транзакций");

            var agentReward =
                calculateAgentRewardProvider.Calculate(1500, IsVipAccount(clientAcc.Id), rent1CService.SystemService);

            if (result.Result.ChunkDataOfPagination.FirstOrDefault()?.TransactionSum != agentReward)
                throw new InvalidOperationException("Сумма транзакции не совпадает");

            var recognition42Id = CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId();

            DbLayer.RefreshAll();
            var flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(f =>
                f.AccountId == clientAcc.Id && f.FlowResourceId == recognition42Id);

            Assert.IsTrue(flowResourcesScope.ScopeValue >= 1000,
                "При продлении аренды количество страниц ЗД должно стать как минимум 1000.");

            var esdl =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == clientAcc.Id && r.BillingService.SystemService == Clouds42Service.Esdl);

            Assert.AreEqual(esdl.ExpireDate.Value.Date, DateTime.Now.AddMonths(1).Date,
                "Лицензия ЗД не продлилась");
        }

        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>ID пользователя</returns>
        private Guid CreateAccountUser(Guid accountId)
        {
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = accountId,
                AccountIdString = SimpleIdHash.GetHashById(accountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };

            var accountUsersProfileManager =
                ServiceProvider.GetRequiredService<AccountUsersProfileManager>();

            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;
            var secondUserObject = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == secondUser.Login);
            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };

            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();

            if (res.Error)
                throw new InvalidOperationException("Ошибка создания нового пользователя. " + res.Message);

            return secondUserObject.Id;
        }

        /// <summary>
        /// Заблокировать Аренду 1С для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void BlockRent1CAndEsdlForAccount(Guid accountId)
        {
            var rent1C =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.MyEnterprise);
            rent1C.ExpireDate = DateTime.Now.AddDays(-1);

            var esdl =
                DbLayer.ResourceConfigurationRepository.FirstOrDefault(r =>
                    r.AccountId == accountId && r.BillingService.SystemService == Clouds42Service.Esdl);

            var recognition42Id = CloudConfigurationProvider.BillingServices.GetRecognition42ServiceId();
            var flowResourcesScope = DbLayer.FlowResourcesScopeRepository.FirstOrDefault(f =>
                f.AccountId == accountId && f.FlowResourceId == recognition42Id);
            flowResourcesScope.ScopeValue = 800;
            esdl.ExpireDate = DateTime.Now.AddDays(-1);

            DbLayer.FlowResourcesScopeRepository.Update(flowResourcesScope);
            DbLayer.ResourceConfigurationRepository.Update(rent1C);
            DbLayer.ResourceConfigurationRepository.Update(esdl);
            DbLayer.Save();

            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            billingManager.ProlongOrLockExpiredServices();
        }

        /// <summary>
        /// Применить изменения Аренды 1С для пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="status">Тип изменения (true - подключение)</param>
        private void ApplyRent1CChangesForUser(Guid accountUserId, Guid accountId, bool status)
        {
            var rent1CConfigurationAccessManager = ServiceProvider.GetRequiredService<Rent1CConfigurationAccessManager>();

            var accountWebResource = DbLayer.ResourceRepository.FirstOrDefault(res =>
                res.AccountId == accountId && res.Subject == null &&
                res.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);

            rent1CConfigurationAccessManager.ConfigureAccesses(accountId, [
                new()
                {
                    AccountUserId = accountUserId,
                    StandartResource = false,
                    WebResource = status,
                    WebResourceId = status ? accountWebResource.Id : (Guid?)null
                }
            ]);
        }
    }
}
