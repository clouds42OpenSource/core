﻿using System;
using System.Linq;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.CloudServices.Contracts;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста проверки агентского вознаграждения по платежу вип аккаунта
    ///
    /// 1) Создаем реферальный аккаунт(аккаунт партнера) и сервис для аккаунта #CreateAccountAndServiceRefferal
    /// 2) Создаем аккаунт ВИП для партнера и подключаем ему сервис партнера #CreatePartnerAccount
    /// 3) Применяем подписку кастомного сервиса для клиента партнера и отключаем Аренду 1С #ApplyServiceSubscribe
    /// 4) Создаем платеж для клиента партнера и проверяем начисление вознаграждения #CheckAgentReward
    /// </summary>
    public class ScenarioCheckingAgentRewardByPaymentOfVipAccountTest : ScenarioBase
    {
        public override void Run()
        {
            Services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommandTest>();
            var sp = Services.BuildServiceProvider();
            var createOrEditBillingServiceHelper = sp.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceManager = sp.GetRequiredService<BillingServiceManager>();
            var createPaymentManager = sp.GetRequiredService<CreatePaymentManager>();
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration(["bp"]);
            #region CreateAccountAndServiceRefferal
            var createReferralAccountCommand = new CreateAccountCommand(TestContext);
            createReferralAccountCommand.Run();
            var referralAccount = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createReferralAccountCommand.AccountId);

            Assert.IsNotNull(referralAccount);

            var serviceTypeDtos = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, referralAccount.Id)
                {
                    BillingServiceTypes = serviceTypeDtos,
                    Name = "Кастомный сервис 1",
                    AccountId = referralAccount.Id
                });
            createCustomService.Run();
            #endregion

            #region CreatePartnerAccount
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise,
                    CloudServiceId = createCustomService.Id,
                    ReferralAccountId = referralAccount.Id
                }
            });

            createAccountCommand.Run();

            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(account);

            sp.GetRequiredService<IUpdateAccountConfigurationProvider>()
                .UpdateSignVipAccount(account.Id, true);
            #endregion

            #region ApplyServiceSubscribe
            billingServiceManager.ApplyDemoServiceSubscribe(createCustomService.Id, account.Id, false);

            sp.GetRequiredService<IRent1CManageHelper>().ManageRent1C(new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-10),
            }, account.Id);

            var resourceConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == account.Id && rc.BillingService.SystemService == Clouds42Service.MyEnterprise);

            Assert.IsNotNull(resourceConfiguration);
            #endregion

            #region CheckAgentReward
            createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = serviceTypeDtos[0].ServiceTypeCost + resourceConfiguration.Cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var agentReward =
                CalculateAgentRewardForVipAccount(resourceConfiguration.Cost, serviceTypeDtos[0].ServiceTypeCost, sp);

            var agentPayments = DbLayer.AgentPaymentRepository.AsQueryableNoTracking().ToList();
            Assert.AreEqual(2, agentPayments.Count);
            Assert.AreEqual(agentPayments.Sum(ap => ap.Sum), agentReward);
            #endregion
        }

        /// <summary>
        /// Посчитать вознаграждение для вип аккаунта
        /// </summary>
        /// <param name="rent1CCost">Стоимость Аренды 1С</param>
        /// <param name="customServiceCost">Стоимость кастомного сервиса</param>
        /// <returns>Вознаграждение для вип аккаунта</returns>
        private decimal CalculateAgentRewardForVipAccount(decimal rent1CCost, decimal customServiceCost, ServiceProvider sp)
        {
            var actualAgencyAgreement = sp.GetRequiredService<IAgencyAgreementDataProvider>().GetActualAgencyAgreement();
            Assert.IsNotNull(actualAgencyAgreement);

            return rent1CCost * (actualAgencyAgreement.Rent1CRewardPercentForVipAccounts / 100) + customServiceCost *
                   (actualAgencyAgreement.ServiceOwnerRewardPercentForVipAccounts / 100);
        }
    }
}
