﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Тест получения информации о сервисе для его активации
    /// Сценарий:
    ///         1. Создаем аккаунт и сервис
    ///         2. Получаем информацию о сервисе для его активации
    ///         3. Проверяем, что информация правильная
    /// </summary>
    public class GetBillingServiceForActivationTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;

        public GetBillingServiceForActivationTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var serviceName = "My Custom Service";

            var billingServiceTypes = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);

            var createBillingServiceCommand = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    Name = serviceName,
                    BillingServiceTypes = billingServiceTypes
                });
            createBillingServiceCommand.Run();
            Assert.IsFalse(createBillingServiceCommand.Result.Error);

            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(bs => bs.Id == createBillingServiceCommand.Id);
            Assert.IsNotNull(billingService.AccountOwnerId);
            Assert.IsNotNull(billingService);

            var billingServiceForActivation = _billingServiceManager.GetBillingServiceForActivation(createBillingServiceCommand.Id,
                createAccountCommand.AccountId);

            Assert.IsFalse(billingServiceForActivation.Error);
            Assert.AreEqual(billingServiceForActivation.Result.Id, billingService.Id);
            Assert.AreEqual(billingServiceForActivation.Result.Name, billingService.Name);
            Assert.AreEqual(billingServiceForActivation.Result.ShortDescription, billingService.ShortDescription);
            Assert.AreEqual(billingServiceForActivation.Result.ServiceActivationLink,
                GetServiceActivationLink(billingService.AccountOwnerId.Value, billingService.Id));
        }

        /// <summary>
        /// Получить ссылку для активации сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Ссылка для активации сервиса</returns>
        private string GetServiceActivationLink(Guid accountId, Guid serviceId) =>
                $"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/signup?ReferralAccountId={accountId}&CloudServiceId={serviceId}";
    }
}
