﻿using System;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Scenarios.Tests.Libs.TestCommands;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    ///     Сценарий для проверки изменения сервиса Дополнительные сеансы и последующей блокировки сервиса
    ///     
    ///     Предусловия: для аккаунта test_account подключен один пользователь test_user_1 по тарифу Стандарт;
    ///     подключены 3 дополнительных сессии
    /// 
    ///     Действия: уменьшаем до 2 сессий, блокируем Аренду1С
    /// 
    ///     Проверка: сервис должен быть заблокирован, сумма не списана с баланса, 

    /// </summary>
    public class ScenarioChangeCountAvailableSessionBlockRent1C : ScenarioBase
    {

        public override void Run()
        {
            var getConfigurationDatabaseCommandTest = new GetConfigurationDatabaseCommandTest();
            getConfigurationDatabaseCommandTest.SetListConfiguration([]);
            //создали пользователя
            var createAccountDatabaseAndAccountCommand = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);

            //Аренда +10 дней от тек.даты. 
            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba =>
                ba.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);
            Assert.IsNotNull(billingAccount);

            //Пополнили баланс 
            var balance = 2850m;
            var billingService = GetRent1CServiceOrThrowException();

            CreateInflowPayment(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, billingService.Id, balance);

            //проверяем возможность подключить Дополнительные сеансы
            var serviceAvailableSession = DbLayer.BillingServiceRepository.FirstOrDefault(s =>
                   s.Name == "Дополнительные сеансы");
            var _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            var сloudServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 3).Result;

            Assert.IsNotNull(сloudServicePayment);
            Assert.IsFalse(сloudServicePayment.Complete);

            //покупаем Дополнительные сеансы за свои деньги
            var result = _billingServiceManager.PaymentDopSessionService(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId,
                false, сloudServicePayment.CostOftariff, 3);

            var resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            Assert.IsTrue(result.Result.Complete);
            Assert.IsNotNull(billingAccount);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.AreEqual(2850 - сloudServicePayment.CostOftariff, billingAccount.Balance);

            //изменяем количество допсеансов до 2
            var сloudChangeServicePayment = _billingServiceManager.CalculateDopSessionServiceType(createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id, 2).Result;

            Assert.IsNotNull(сloudChangeServicePayment);
            Assert.IsTrue(сloudChangeServicePayment.Complete);
            Assert.AreEqual(сloudChangeServicePayment.CostOftariff, 0);
            Assert.AreEqual(2850 - сloudServicePayment.CostOftariff, billingAccount.Balance);

            var resultServiceTypeInfo = _billingServiceManager.GetBillingServiceTypeInfo(serviceAvailableSession.Id, createAccountDatabaseAndAccountCommand.AccountDetails.AccountId).Result;

            Assert.IsNotNull(resultServiceTypeInfo);
            Assert.AreEqual(resultServiceTypeInfo.ServiceExpireDate.ToShortDateString(), dateValue.ToShortDateString());
            Assert.AreEqual(2, resultServiceTypeInfo.UsedLicenses);

            // Устанавливаем дату Аренды -1 день от текущей
            dateValue = DateTime.Now.AddDays(-1);
            resourcesConfiguration = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, Clouds42Service.MyEnterprise);

            resourcesConfiguration.ExpireDate = dateValue;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            // Блочим сервис 
            Services.AddTransient<IProlongServicesProvider>();
            var billingManager = ServiceProvider.GetRequiredService<IBillingManager>();
            var processRes = billingManager.ProlongOrLockExpiredServices();

            Assert.IsFalse(processRes.Error);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.BillingAccounts);
            RefreshDbCashContext(TestContext.Context.Resources);

            var resourceRent1C =
                TestContext.DbLayer
                    .ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourceAvailableSession = GetResourcesConfigurationOrThrowException(
                createAccountDatabaseAndAccountCommand.AccountDetails.AccountId, serviceAvailableSession.Id);

            var billingAcc = TestContext.DbLayer.BillingAccountRepository.FirstOrDefault(x => x.Account.Id == createAccountDatabaseAndAccountCommand.AccountDetails.AccountId);

            Assert.IsNotNull(resourceRent1C);
            Assert.IsTrue(resourceRent1C.FrozenValue);
            Assert.IsNotNull(resourceAvailableSession);
            Assert.IsFalse(resourceAvailableSession.FrozenValue);
            Assert.IsNull(billingAcc.PromisePaymentSum);
        }
    }
}
