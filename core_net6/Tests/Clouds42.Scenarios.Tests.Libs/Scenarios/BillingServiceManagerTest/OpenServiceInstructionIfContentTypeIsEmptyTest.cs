﻿using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using CommonLib.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Тест проверки открытия инструкции сервиса если при создании не указывать ContentType
    /// Сценарий:
    ///         1. Создаем акаунт
    ///         2. Создаем сервис
    ///         3. Открываем инструцкию сервиса, проверяем, что в можели есть ContentType
    /// </summary>
    public class OpenServiceInstructionIfContentTypeIsEmptyTest : ScenarioBase
    {
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;

        public OpenServiceInstructionIfContentTypeIsEmptyTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var standardResourceTypeId =
                DbLayer.BillingServiceTypeRepository.FirstOrDefault(s =>
                    s.SystemServiceType == ResourceType.MyEntUser).Id;

            var billingServiceTypes =
                _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1, standardResourceTypeId);
            billingServiceTypes[0].BillingType = BillingTypeEnum.ForAccountUser;

            var billingService = CreateCustomService(billingServiceTypes);
            Assert.IsNotNull(billingService);

            DbLayer.CloudFileRepository.FirstOrDefault(cf => cf.Id == billingService.InstructionServiceCloudFileId);

        }

        /// <summary>
        /// Создать кастомный сервис
        /// </summary>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <returns>Сервис</returns>
        private BillingService CreateCustomService(List<BillingServiceTypeDto> billingServiceTypes)
        {
            var createIndustry = new CreateIndustryCommand(TestContext);
            createIndustry.Run();

            CreateDelimiterTemplate();

            var serviceName = "Тестовый Сервис";

            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Key = Guid.NewGuid(),
                Name = serviceName,
                BillingServiceTypes = billingServiceTypes,
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            Assert.IsFalse(createBillingServiceCommand.Result.Error, createBillingServiceCommand.Result.Message);

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == serviceName);
            Assert.IsNotNull(service, "Произошла ошибка в процессе создания. Сервис не найден.");

            return service;
        }
    }
}
