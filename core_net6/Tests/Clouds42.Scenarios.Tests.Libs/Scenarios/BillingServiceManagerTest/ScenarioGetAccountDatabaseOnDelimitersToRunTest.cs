﻿using Clouds42.Billing;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.BillingServiceManagerTest
{
    /// <summary>
    /// Сценарий теста получения базы на разделителях для запуска
    /// Создаем аккаунт и 2 базы, одна на разделителях, другая нет
    /// 1) Проверяем результат получения если запросим не на разделителях
    /// 2) Проверяем результат получения базы на разделителях
    /// и проверям валидность модели результата
    /// </summary>
    public class ScenarioGetAccountDatabaseOnDelimitersToRunTest: ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioGetAccountDatabaseOnDelimitersToRunTest()
        {
            _billingServiceManager = ServiceProvider.GetRequiredService<BillingServiceManager>();
            _testDataGenerator = ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var result = _billingServiceManager.GetAccountDatabaseOnDelimitersToRun(Guid.NewGuid());

            if (!result.Error)
                throw new InvalidOperationException("Такой базы не существует");

            var dbTemplate = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });

            var dbTemplateDelimiters =
                _testDataGenerator.GenerateDbTemplateDelimiters(dbTemplate.Id);
            DbLayer.DbTemplateDelimitersReferencesRepository.Insert(dbTemplateDelimiters);

            var accountDbOne = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 1,
                createAccountCommand.Account.IndexNumber, state: DatabaseState.NewItem);

            var accountDbOnDelimitersOne = _testDataGenerator.GenerateAccountDatabaseOnDelimiters(accountDbOne.Id, null,
                dbTemplateDelimiters.ConfigurationId);

            var accountDbTwo = _testDataGenerator.GenerateAccountDatabase(createAccountCommand.AccountId, 2,
                createAccountCommand.Account.IndexNumber);

            DbLayer.DatabasesRepository.Insert(accountDbOne);
            DbLayer.DatabasesRepository.Insert(accountDbTwo);
            DbLayer.AccountDatabaseDelimitersRepository.Insert(accountDbOnDelimitersOne);
            DbLayer.Save();

            result = _billingServiceManager.GetAccountDatabaseOnDelimitersToRun(accountDbTwo.Id);

            if (!result.Error)
                throw new InvalidOperationException("База для запуска должна быть на разделителях");

            result = _billingServiceManager.GetAccountDatabaseOnDelimitersToRun(accountDbOne.Id);

            if (result.Error || result.Result == null)
                throw new InvalidOperationException("База существует и является на разделителях");

            if (result.Result.Id != accountDbOne.Id || result.Result.DatabaseState != DatabaseState.NewItem ||
                result.Result.Caption.IsNullOrEmpty() )
                throw new InvalidOperationException ("Не корректная модель запуска базы");
        }
    }
}
