﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AdvertisingBanner.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.Enums.Marketing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdvertisingBannerTemplateTests
{
    /// <summary>
    /// Тест создания, удаления, редактирования шаблона рекламного баннера
    /// Сценарий:
    ///         1. Создаем рекланый баннер, проверяем, что баннер успешно создан
    ///         2. Редактируем рекланый баннер, проверяем, что баннер успешно отредактирован
    ///         3. Удаляем рекланый баннер, проверяем, что баннер успешно удален
    /// </summary>
    public class CrudAdvertisingBannerTemplateTest : ScenarioBase
    {
        private readonly AdvertisingBannerTemplateManager _advertisingBannerTemplateManager;

        public CrudAdvertisingBannerTemplateTest()
        {
            _advertisingBannerTemplateManager = TestContext.ServiceProvider.GetRequiredService<AdvertisingBannerTemplateManager>();
        }

        public override void Run()
        {
            var localesId = DbLayer.LocaleRepository.All().Select(l => l.ID).ToList();
            Assert.IsNotNull(localesId);
            Assert.IsTrue(localesId.Any());

            var bannerDisplayFrom = DateTime.Now;
            var bannerDisplayTo = DateTime.MaxValue;
            var header = $"Header{Guid.NewGuid():N}";
            var image = new FileDataDto<IFormFile>
            {
                FileName = "icon.png",
                ContentType = "image",
                Content = GetPostedFileSimulation("icon.png", "image")
            };
            var advertisingBannerAudienceResultDto = new CalculationAdvertisingBannerAudienceDto
            {
                AccountUsersCount = 200,
                AccountsCount = 6
            };
            var advertisingBannerAudienceDto = new AdvertisingBannerAudienceDto
            {
                AccountType = AccountTypeEnum.All,
                AvailabilityPayment = AvailabilityPaymentEnum.All,
                Locales = localesId,
                RentalServiceState = RentalServiceStateEnum.All
            };

            var advertisingBannerTemplateDto = new AdvertisingBannerTemplateDetailsDto
            {
                TemplateId = 1500,
                Body = "This is Body",
                CaptionLink = "github.com",
                Header = header,
                Link = "Link",
                ShowFrom = bannerDisplayFrom,
                ShowTo = bannerDisplayTo,
                Image = image,
                CalculationAdvertisingBannerAudience = advertisingBannerAudienceResultDto,
                AdvertisingBannerAudience = advertisingBannerAudienceDto
            };

            CreateAdvertisingBannerTemplate(advertisingBannerTemplateDto);

            var banner = GetAdvertisingBannerTemplateByHeader(header);
            Assert.IsNotNull(banner);

            var newLink = "New Link";
            var newBody = "New Body";
            var newCaptionLink = "New Caption Link";

            var editAdvertisingBannerTemplateDto = new EditAdvertisingBannerTemplateDto
            {
                TemplateId = banner.Id,
                Link = newLink,
                Body = newBody,
                Header = banner.Header,
                AdvertisingBannerAudience = advertisingBannerAudienceDto,
                ShowFrom = banner.DisplayBannerDateFrom,
                ShowTo = banner.DisplayBannerDateTo,
                CaptionLink = newCaptionLink,
                Image = image
            };

            EditAdvertisingBannerTemplate(editAdvertisingBannerTemplateDto);

            banner = GetAdvertisingBannerTemplateByHeader(header);
            Assert.IsNotNull(banner);
            Assert.AreEqual(banner.Header, header);
            Assert.AreEqual(banner.Body, newBody);
            Assert.AreEqual(banner.Link, newLink);
            Assert.AreEqual(banner.CaptionLink, newCaptionLink);

            DeleteAdvertisingBannerTemplateDto(banner.Id);

            banner = GetAdvertisingBannerTemplateByHeader(header);
            Assert.IsNull(banner);
        }

        /// <summary>
        /// Создать шаблон рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerTemplateDto">Модель шаблона рекламного баннера</param>
        private void CreateAdvertisingBannerTemplate(AdvertisingBannerTemplateDetailsDto advertisingBannerTemplateDto)
        {
            var creAdvertisingBannerTemplate = _advertisingBannerTemplateManager.CreateAdvertisingBannerTemplate(advertisingBannerTemplateDto);
            Assert.IsFalse(creAdvertisingBannerTemplate.Error);
        }

        /// <summary>
        /// Редактировать шаблон рекламного баннера
        /// </summary>
        /// <param name="editAdvertisingBannerTemplateDto">Модель редактирования шаблона рекламного баннера</param>
        private void EditAdvertisingBannerTemplate(EditAdvertisingBannerTemplateDto editAdvertisingBannerTemplateDto)
        {
            var editAdvertisingBannerTemplate = _advertisingBannerTemplateManager.EditAdvertisingBannerTemplate(editAdvertisingBannerTemplateDto);
            Assert.IsFalse(editAdvertisingBannerTemplate.Error);
            
        }

        /// <summary>
        /// Удалить рекламный баннер
        /// </summary>
        /// <param name="bannerId">Id рекламного баннера</param>
        private void DeleteAdvertisingBannerTemplateDto(int bannerId)
        {
            var deleteAdvertisingBannerTemplate = _advertisingBannerTemplateManager.DeleteAdvertisingBannerTemplate(bannerId);
            Assert.IsFalse(deleteAdvertisingBannerTemplate.Error);
        }

        /// <summary>
        /// Получить рекламный баннер по Хэдеру
        /// </summary>
        /// <param name="header">Хедер рекламного баннера</param>
        /// <returns>Рекламный банне</returns>
        private AdvertisingBannerTemplate GetAdvertisingBannerTemplateByHeader(string header) => DbLayer
            .GetGenericRepository<AdvertisingBannerTemplate>()
            .FirstOrDefault(abt => abt.Header == header);


        /// <summary>
        /// Симуляция загрузки файла
        /// </summary>
        /// <param name="docType">Тип загруженного документа</param>
        /// <returns>Файл</returns>
        private FakeFormFile GetPostedFileSimulation(string fileName, string docType)
        {
            var buffer = new byte[1024];
            var stream = new MemoryStream(buffer);

            return new FakeFormFile(fileName, docType, stream);
        }
    }
}
