﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AdvertisingBanner.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Marketing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdvertisingBannerTemplateTests
{
    /// <summary>
    /// Тест подсчета аудитории рекламного баннера
    /// Сценарий:
    ///         1. Создаем 3 аккаунта в разных локалях
    ///         2. Получаем аудиторию для баннера, проверяем, что количество правильное
    ///         3. Получаем аудиторию для баннера (только ураинская локаль), проверяем, что количество правильное
    ///         4. Получаем аудиторию для баннера (только казахская локаль), проверяем, что количество правильное
    ///         5. Получаем аудиторию для баннера (только русская локаль), проверяем, что количество правильное
    ///         6. Создаем платеж для одного аккаунта
    ///         7. Получаем аудиторию для баннера, проверяем (только с платежами), проверяем, что количество правильное
    ///         8. Блокируем Аренду для одного аккаунта
    ///         9. Получаем аудиторию для баннера, проверяем (только с неактивной Арендой), проверяем, что количество правильное
    ///         9. Устанавливаем ВИП статус для одного аккаунта
    ///         9. Получаем аудиторию для баннера, проверяем (только ВИП аккаунты), проверяем, что количество правильное
    /// 
    /// </summary>
    public class CalculateAdvertisingBannerAudienceTest : ScenarioBase
    {
        private readonly CalculateAdvertisingBannerAudienceManager _advertisingBannerAudienceManager;

        public CalculateAdvertisingBannerAudienceTest()
        {
            _advertisingBannerAudienceManager =
                TestContext.ServiceProvider.GetRequiredService<CalculateAdvertisingBannerAudienceManager>();
        }

        public override void Run()
        {
            var ruLocale = GetLocaleByNameOrThrowException("ru-ru");
            var uaLocale = GetLocaleByNameOrThrowException("ru-ua");
            var kzLocale = GetLocaleByNameOrThrowException("ru-kz");

            var ruAccount = CreateAccount(ruLocale.Name);
            var uaAccount = CreateAccount(uaLocale.Name);
            var kzAccount = CreateAccount(kzLocale.Name);

            var localesId = DbLayer.LocaleRepository.All().Select(l => l.ID).ToList();

            var advertisingBannerAudienceDto = new AdvertisingBannerAudienceDto
            {
                AccountType = AccountTypeEnum.All,
                RentalServiceState = RentalServiceStateEnum.All,
                AvailabilityPayment = AvailabilityPaymentEnum.All,
                Locales = localesId
            };
            CalculateAudience(advertisingBannerAudienceDto, 3, 3);

            advertisingBannerAudienceDto.Locales = [uaLocale.ID];
            CalculateAudience(advertisingBannerAudienceDto, 1,1);

            advertisingBannerAudienceDto.Locales = [kzLocale.ID];
            CalculateAudience(advertisingBannerAudienceDto, 1, 1);

            advertisingBannerAudienceDto.Locales = [ruLocale.ID];
            CalculateAudience(advertisingBannerAudienceDto, 1, 1);

            CreateInflowPayment(ruAccount.Id, null, 800);

            advertisingBannerAudienceDto.AvailabilityPayment = AvailabilityPaymentEnum.WithPayment;
            advertisingBannerAudienceDto.Locales = localesId;
            CalculateAudience(advertisingBannerAudienceDto, 1, 1);

            BlockRent1CForAccount(kzAccount.Id);

            advertisingBannerAudienceDto.AvailabilityPayment = AvailabilityPaymentEnum.All;
            advertisingBannerAudienceDto.RentalServiceState = RentalServiceStateEnum.Locked;
            CalculateAudience(advertisingBannerAudienceDto, 1, 1);

            advertisingBannerAudienceDto.AvailabilityPayment = AvailabilityPaymentEnum.WithPayment;
            CalculateAudience(advertisingBannerAudienceDto, 0, 0);

            SetVipStatusToAccount(uaAccount.Id);

            advertisingBannerAudienceDto.Locales = localesId;
            advertisingBannerAudienceDto.AvailabilityPayment = AvailabilityPaymentEnum.All;
            advertisingBannerAudienceDto.RentalServiceState = RentalServiceStateEnum.All;
            advertisingBannerAudienceDto.AccountType = AccountTypeEnum.Vip;
            CalculateAudience(advertisingBannerAudienceDto, 1, 1);
        }

        /// <summary>
        /// Создать аккаунт
        /// </summary>
        /// <returns>Аккаунт</returns>
        private Account CreateAccount(string localeName)
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                LocaleName = localeName,
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            return createAccountCommand.Account;
        }

        /// <summary>
        /// Посчитать аудиторию рекламного баннера
        /// </summary>
        /// <param name="advertisingBannerAudienceDto">Модель аудитории рекламного баннера</param>
        /// <param name="accountsCount">Ожидаемое количество аккаунтов</param>
        /// <param name="usersCount">Ожидаемое количество пользователей</param>
        private void CalculateAudience(AdvertisingBannerAudienceDto advertisingBannerAudienceDto, int accountsCount, int usersCount)
        {
            var calculate = _advertisingBannerAudienceManager.Calculate(advertisingBannerAudienceDto);
            Assert.IsFalse(calculate.Error);
            Assert.AreEqual(calculate.Result.AccountUsersCount, usersCount);
            Assert.AreEqual(calculate.Result.AccountsCount, accountsCount);
        }

        /// <summary>
        /// Установить ВИП статус для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        private void SetVipStatusToAccount(Guid accountId) => TestContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
            .UpdateSignVipAccount(accountId, true);
    }
}
