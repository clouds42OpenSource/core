﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AdvertisingBanner.Managers;
using Clouds42.DataContracts.Service.Market;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AdvertisingBannerTemplateTests
{
    /// <summary>
    /// Тест ошибочного создания рекламного баннера
    /// Сценарий:
    ///         1. При создании баннера передаем пустую модель создания
    ///         2. Проверяем, что в результате создания вернулась ошибка
    ///         3. Заполняем в модели все поля кроме изображения
    ///         4. Проверяем, что в результате создания вернулась ошибка
    ///         4. Заполняем в модели все поле изображение, и не заполняем заголовок
    ///         4. Проверяем, что в результате создания вернулась ошибка
    /// </summary>
    public class ErrorCreateAdvertisingBannerTemplateTest : ScenarioBase
    {
        private readonly AdvertisingBannerTemplateManager _advertisingBannerTemplateManager;

        public ErrorCreateAdvertisingBannerTemplateTest()
        {
            _advertisingBannerTemplateManager = TestContext.ServiceProvider.GetRequiredService<AdvertisingBannerTemplateManager>();
        }

        public override void Run()
        {
            var advertisingBannerTemplateDto = new CreateAdvertisingBannerTemplateDto();
            CreateAdvertisingBannerTemplateWithError(advertisingBannerTemplateDto);

            advertisingBannerTemplateDto.Body = "Body";
            advertisingBannerTemplateDto.Header = "Header";
            advertisingBannerTemplateDto.CaptionLink = "CaptionLink";
            advertisingBannerTemplateDto.ShowFrom = DateTime.Now;
            advertisingBannerTemplateDto.ShowTo = DateTime.Now.AddDays(1);
            advertisingBannerTemplateDto.Link = "Link";

            CreateAdvertisingBannerTemplateWithError(advertisingBannerTemplateDto);

            advertisingBannerTemplateDto.Image = new FileDataDto<IFormFile>
            {
                Content = GetPostedFileSimulation("img.jpg", "image"),
                FileName = "img.jpg",
                ContentType = "image"
            };
            advertisingBannerTemplateDto.Header = null;

            CreateAdvertisingBannerTemplateWithError(advertisingBannerTemplateDto);
        }

        /// <summary>
        /// Создание рекамного банера с ошибкой
        /// </summary>
        /// <param name="advertisingBannerTemplateDto">Модель создания шаблона рекламного баннера</param>
        private void CreateAdvertisingBannerTemplateWithError(CreateAdvertisingBannerTemplateDto advertisingBannerTemplateDto)
        {
            var advertisingBannerTemplate = _advertisingBannerTemplateManager.CreateAdvertisingBannerTemplate(advertisingBannerTemplateDto);

            Assert.IsTrue(advertisingBannerTemplate.Error);
        }

        /// <summary>
        /// Получить загруженный файл
        /// </summary>
        /// <param name="fileName">Название файла</param>
        /// <param name="docType">Тип загруженного документа</param>
        /// <returns>Загруженный файл</returns>
        private FakeFormFile GetPostedFileSimulation(string fileName, string docType)
        {
            var buffer = new byte[1024];
            var stream = new MemoryStream(buffer);

            return new FakeFormFile(fileName, docType, stream);
        }
    }
}
