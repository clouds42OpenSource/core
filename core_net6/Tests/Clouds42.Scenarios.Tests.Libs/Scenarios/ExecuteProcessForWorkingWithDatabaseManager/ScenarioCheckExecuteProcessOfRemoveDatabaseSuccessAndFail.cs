﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Contracts.Processors.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ExecuteProcessForWorkingWithDatabaseManager
{
    /// <summary>
    /// Сценарий для успешного и неуспешного запуска обработки процесса удаления инф. базы
    /// Действия:
    /// 1. Создания аккаунта и его инф. баз. [AccountAndDbsCreation]
    /// 2. Проверим, что процесс успешно будет запущен для валидных параметров. [CheckProcessExecuted]
    /// 3. Проверим, что процесс не будет запущен при финальном статусе инф. базы. [CheckProcessNotExecutedWithFinalStatusEqualToDbStatus]
    /// 4. Проверим, что процесс не будет запущен при статусе DeletedFromCloud в переданных параметрах, но статус инф. базы изменится. [CheckProcessNotExecutedWithDeletedFromCloudStatus]
    /// 5. Проверим, что процесс не будет запущен для несуществующей инф. базы. [CheckProcessNotExecutedWithNotValidDatabaseId]
    /// </summary>
    public class ScenarioCheckExecuteProcessOfRemoveDatabaseSuccessAndFail : ScenarioBase
    {
        private readonly IExecuteProcessForWorkingWithDatabaseManager _executeProcessForWorkingWithDatabaseManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;

        public ScenarioCheckExecuteProcessOfRemoveDatabaseSuccessAndFail()
        {
            _executeProcessForWorkingWithDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IExecuteProcessForWorkingWithDatabaseManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
        }

        public override void Run()
        {
            #region AccountAndDbsCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var databasesId = _accountDatabasesTestHelper.CreateAccountDatabases(createAccountCommand.AccountId, 4);

            #endregion

            #region CheckProcessExecuted

            var processOfRemoveDatabaseParamsDc = new ProcessOfRemoveDatabaseParamsDto
            {
                DatabaseId = databasesId[0],
                FinalDeletionDatabaseStatus = DatabaseState.DeletedToTomb,
                InitiatorId = createAccountCommand.AccountId
            };

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfRemoveDatabase(processOfRemoveDatabaseParamsDc);
            _databaseProcessTestHelper.CheckProcessOfDatabaseStarted(processOfRemoveDatabaseParamsDc.DatabaseId, ProcessesNameConstantsTest.RemovingDatabaseJobName, false);

            #endregion

            #region CheckProcessNotExecutedWithFinalStatusEqualToDbStatus

            processOfRemoveDatabaseParamsDc.DatabaseId = databasesId[1];
            processOfRemoveDatabaseParamsDc.FinalDeletionDatabaseStatus = DatabaseState.DetachingToTomb;

            _accountDatabasesTestHelper.ChangeDatabaseState(processOfRemoveDatabaseParamsDc.DatabaseId, DatabaseState.DetachingToTomb);
            RefreshDbCashContext(Context.AccountDatabases);

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfRemoveDatabase(processOfRemoveDatabaseParamsDc);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(processOfRemoveDatabaseParamsDc.DatabaseId, ProcessesNameConstantsTest.RemovingDatabaseJobName);

            #endregion

            #region CheckProcessNotExecutedWithDeletedFromCloudStatus

            var finalDbStatus = DatabaseState.DeletedFromCloud;

            processOfRemoveDatabaseParamsDc.DatabaseId = databasesId[1];
            processOfRemoveDatabaseParamsDc.FinalDeletionDatabaseStatus = finalDbStatus;

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfRemoveDatabase(processOfRemoveDatabaseParamsDc);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(processOfRemoveDatabaseParamsDc.DatabaseId, ProcessesNameConstantsTest.RemovingDatabaseJobName);

            var currentDatabaseStatus = DbLayer.DatabasesRepository.GetAll()
                .FirstOrDefault(db => db.Id == processOfRemoveDatabaseParamsDc.DatabaseId)?.StateEnum;
            if (currentDatabaseStatus != finalDbStatus)
                throw new InvalidOperationException($"Не изменился статус базы {processOfRemoveDatabaseParamsDc.DatabaseId} на финальный {finalDbStatus}");

            #endregion

            #region CheckProcessNotExecutedWithNotValidDatabaseId

            processOfRemoveDatabaseParamsDc.DatabaseId = Guid.NewGuid();
            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfRemoveDatabase(processOfRemoveDatabaseParamsDc);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(processOfRemoveDatabaseParamsDc.DatabaseId, ProcessesNameConstantsTest.RemovingDatabaseJobName);

            #endregion

        }
    }
}
