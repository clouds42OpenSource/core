﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.AccountDatabase.Contracts.Processors.Managers;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ExecuteProcessForWorkingWithDatabaseManager
{
    /// <summary>
    /// Сценарий для проверки выполнения обработки процесса архивации инф. базы
    /// Действия:
    /// 1. Создадим аккаунт и его инф. базы. [AccountAndDbsCreation]
    /// 2. Проверим, что процесс архивации инф. базы будет запущен для валидных данных. [CheckProcessOfArchivingStarted]
    /// 3. Проверим, что процесс архивации инф. базы не будет запущен для несуществующего Id инф. базы. [CheckProcessOfArchivingNotStartWithNotValidDbId]
    /// 4. Проверим, что процесс архивации инф. базы не будет запущен для инф. базы в статусе DetachedToTomb. [CheckProcessNotStartedWithDetachedToTombDbState]
    /// 5. Проверим, что процесс архивации инф. базы не будет запущен для инф. базы на разделителях. [CheckProcessNotStartedForDbOnDelimiters]
    /// </summary>
    public class ScenarioCheckExecuteProcessOfArchiveDatabaseToTombSuccessAndFail : ScenarioBase
    {
        private readonly IExecuteProcessForWorkingWithDatabaseManager _executeProcessForWorkingWithDatabaseManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;

        public ScenarioCheckExecuteProcessOfArchiveDatabaseToTombSuccessAndFail()
        {
            _executeProcessForWorkingWithDatabaseManager = TestContext.ServiceProvider.GetRequiredService<IExecuteProcessForWorkingWithDatabaseManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountAndDbsCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var databasesId = _accountDatabasesTestHelper.CreateAccountDatabases(createAccountCommand.AccountId, 4);

            #endregion

            #region CheckProcessOfArchivingStarted

            var processOfArchiveDatabaseToTombParamsDc = new ProcessOfArchiveDatabaseToTombParamsDto
            {
                DatabaseId = databasesId[0],
                InitiatorId = createAccountCommand.AccountId,
                NeedForceFileUpload = false,
                NeedNotifyHotlineAboutFailure = false,
                Trigger = CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase
            };

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfArchiveDatabaseToTomb(
                processOfArchiveDatabaseToTombParamsDc);

            _databaseProcessTestHelper.CheckProcessOfDatabaseStarted(processOfArchiveDatabaseToTombParamsDc.DatabaseId,
                ProcessesNameConstantsTest.ArchiveDatabaseJobName, false);

            #endregion

            #region CheckProcessOfArchivingNotStartWithNotValidDbId

            processOfArchiveDatabaseToTombParamsDc.DatabaseId = Guid.NewGuid();
            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfArchiveDatabaseToTomb(
                processOfArchiveDatabaseToTombParamsDc);

            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(
                processOfArchiveDatabaseToTombParamsDc.DatabaseId, ProcessesNameConstantsTest.ArchiveDatabaseJobName);

            #endregion

            #region CheckProcessNotStartedWithDetachedToTombDbState

            processOfArchiveDatabaseToTombParamsDc.DatabaseId = databasesId[1];

            _accountDatabasesTestHelper.ChangeDatabaseState(processOfArchiveDatabaseToTombParamsDc.DatabaseId, DatabaseState.DetachedToTomb);
            Context.ChangeTracker.Clear();

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfArchiveDatabaseToTomb(
                processOfArchiveDatabaseToTombParamsDc);

            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(
                processOfArchiveDatabaseToTombParamsDc.DatabaseId, ProcessesNameConstantsTest.ArchiveDatabaseJobName);

            #endregion

            #region CheckProcessNotStartedForDbOnDelimiters

            processOfArchiveDatabaseToTombParamsDc.DatabaseId = databasesId[2];
            _accountDatabasesTestHelper.CreateDbOnDelimiters(processOfArchiveDatabaseToTombParamsDc.DatabaseId, "unf");

            _executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfArchiveDatabaseToTomb(
                processOfArchiveDatabaseToTombParamsDc);

            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(processOfArchiveDatabaseToTombParamsDc.DatabaseId, ProcessesNameConstantsTest.ArchiveDatabaseJobName);

            #endregion
        }
    }
}
