﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.PublishNode.Managers;
using Clouds42.Segment.CloudServicesContentServer.Managers;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ContentServer
{
    /// <summary>
    /// Тест получения доступных нод публикации
    /// Сценарий:
    ///         1. Получаем ноды публикаций, должно быть 0 нод
    ///         2. Создаем 2 ноды публикаций
    ///         3. Получаем ноды публикаций, должно быть 2 ноды
    ///         4. Удаляем одну ноду
    ///         5. Получаем ноды публикаций, должна быть 1 нода
    /// </summary>
    public class GetAvailablePublishNodesTest : ScenarioBase
    {
        private readonly CloudContentServerManager _cloudContentServerManager;
        private readonly PublishNodesManager _publishNodesManager;

        public GetAvailablePublishNodesTest()
        {
            _cloudContentServerManager = TestContext.ServiceProvider.GetRequiredService<CloudContentServerManager>();
            _publishNodesManager = TestContext.ServiceProvider.GetRequiredService<PublishNodesManager>();
        }

        public override void Run()
        {
            var publishNodes = _cloudContentServerManager.GetAvailablePublishNodes();
            Assert.IsFalse(publishNodes.Error, publishNodes.Message);
            Assert.AreEqual(0, publishNodes.Result.Count);

            //Create first publish node
            var firstPublishNodeId = CreatePublishNode(1);
            //Create second publish node
            CreatePublishNode(2);

            publishNodes = _cloudContentServerManager.GetAvailablePublishNodes();
            Assert.IsFalse(publishNodes.Error, publishNodes.Message);
            Assert.AreEqual(2, publishNodes.Result.Count);

            var deletePublishNode = _publishNodesManager.DeletePublishNode(firstPublishNodeId);
            Assert.IsFalse(deletePublishNode.Error, deletePublishNode.Message);

            publishNodes = _cloudContentServerManager.GetAvailablePublishNodes();
            Assert.IsFalse(publishNodes.Error, publishNodes.Message);
            Assert.AreEqual(1, publishNodes.Result.Count);
        }

        /// <summary>
        /// Добавить новую ноду публикаций        
        /// </summary>
        /// <param name="index"> Индекс ноды публикации. 
        /// Будет добавлен к адресу ноды во избежание одинаковых адресов.</param>
        private Guid CreatePublishNode(int index)
        {
            var nodeDto = new PublishNodeDtoTest();
            nodeDto.Address += index;
            var addNewPublishNodeResult = _publishNodesManager.AddNewPublishNode(nodeDto);
            Assert.IsFalse(addNewPublishNodeResult.Error, addNewPublishNodeResult.Message);

            return addNewPublishNodeResult.Result;
        }
    }
}
