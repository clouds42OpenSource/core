﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudServicesContentServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ContentServer
{
    /// <summary>
    /// Тест создания/удаления/редактирования сервера публикации
    /// Сценарий:
    ///         1. Создаем сервер публикации, проверяем, что создание прошло без ошибок 
    ///         2. Редактируем сервер публикации, проверяем, что редактирование прошло без ошибок 
    ///         3. Удаляем сервер публикации, проверяем, что удаление прошло без ошибок 
    /// </summary>
    public class CrudCloudContentServerTest : ScenarioBase
    {
        private readonly CloudContentServerManager _cloudContentServerManager;

        public CrudCloudContentServerTest()
        {
            _cloudContentServerManager = TestContext.ServiceProvider.GetRequiredService<CloudContentServerManager>();
        }

        public override void Run()
        {
            var cloudContentServerId = CreateCloudContentServer();
            EditCloudContentServer(cloudContentServerId);
            DeleteCloudContentServer(cloudContentServerId);
        }

        /// <summary>
        /// Создать сервер публикации
        /// </summary>
        /// <returns>Id сервера публикации</returns>
        private Guid CreateCloudContentServer()
        {
            var cloudContentServerDto = new CreateCloudContentServerDto
            {
                Name = "Name",
                PublishSiteName = $"{DateTime.Now:mmssfff}",
                Description = "Desc",
                GroupByAccount = true
            };
            var createResult = _cloudContentServerManager.CreateCloudContentServer(cloudContentServerDto);
            Assert.IsFalse(createResult.Error, createResult.Message);

            return createResult.Result;
        }

        /// <summary>
        /// Редактировать сервер публикации
        /// </summary>
        /// <param name="cloudContentServerId">Id сервера публикации</param>
        private void EditCloudContentServer(Guid cloudContentServerId)
        {
            var cloudContentServer = _cloudContentServerManager.GetCloudContentServerDto(cloudContentServerId);
            Assert.IsFalse(cloudContentServer.Error, cloudContentServer.Message);

            var cloudContentServerDto = cloudContentServer.Result;

            cloudContentServerDto.Name = "New Name";
            cloudContentServerDto.PublishSiteName = "New Publish Site Name";
            cloudContentServerDto.Description = "Description";
            cloudContentServerDto.GroupByAccount = false;

            var editCloudContentServer = _cloudContentServerManager.EditCloudContentServer(cloudContentServerDto);
            Assert.IsFalse(editCloudContentServer.Error, editCloudContentServer.Message);

            var editedCloudContentServer = _cloudContentServerManager.GetCloudContentServerDto(cloudContentServerId);
            Assert.IsFalse(editedCloudContentServer.Error, editedCloudContentServer.Message);

            var editedCloudContentServerDto = editedCloudContentServer.Result;
            Assert.AreEqual(cloudContentServerDto.Name, editedCloudContentServerDto.Name);
            Assert.AreEqual(cloudContentServerDto.Description, editedCloudContentServerDto.Description);
            Assert.AreEqual(cloudContentServerDto.GroupByAccount, editedCloudContentServerDto.GroupByAccount);
        }

        /// <summary>
        /// Удалить сервер публикации
        /// </summary>
        /// <param name="cloudContentServerId">Id сервера публикации</param>
        private void DeleteCloudContentServer(Guid cloudContentServerId)
        {
            var deleteCloudContentServer = _cloudContentServerManager.DeleteCloudContentServer(cloudContentServerId);
            Assert.IsFalse(deleteCloudContentServer.Error, deleteCloudContentServer.Message);
        }
    }
}
