﻿using System;
using System.Threading.Tasks;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.Segment.CloudServicesContentServer.Managers;
using Core42.Application.Features.ContentServerContext.Queries;
using Core42.Application.Features.ContentServerContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ContentServer
{
    /// <summary>
    /// Тест выборки серверов публикации по фильтру
    /// Сценарий:
    ///         1. Создаем 3 сервера публикации
    ///         2. Делаем выборку по фильтрам, проверяем, что возвращается корректное количество записей
    /// </summary>
    public class CloudContentServerSamplingTest : ScenarioBase
    {
        private readonly CloudContentServerManager _cloudContentServerManager;

        public CloudContentServerSamplingTest()
        {
            _cloudContentServerManager = TestContext.ServiceProvider.GetRequiredService<CloudContentServerManager>();
        }

        public override void Run()
        {
        }

        public override async Task RunAsync()
        {
            var firstCloudContentServer = new CreateCloudContentServerDto
            {
                Name = "TestName",
                Description = "Description",
                GroupByAccount = true,
                PublishSiteName = $"{DateTime.Now:mmssfff}"
            };
            CreateCloudContentServer(firstCloudContentServer);

            var secondCloudContentServer = new CreateCloudContentServerDto
            {
                Name = "TestName",
                Description = "Descr",
                GroupByAccount = true,
                PublishSiteName = $"{DateTime.Now:mmssfff}"
            };
            CreateCloudContentServer(secondCloudContentServer);

            var thirdCloudContentServer = new CreateCloudContentServerDto
            {
                Name = "Server",
                Description = "Desc",
                GroupByAccount = true,
                PublishSiteName = $"Publish_{DateTime.Now:mmssfff}"
            };
            CreateCloudContentServer(thirdCloudContentServer);

            var filter = new CloudContentServerFilterDto
            {
                Name = nameof(Test),
                PageNumber = 1
            };

            var query = new GetFilteredContentServersQuery
            {
                Filter = new ContentServerFilter { Name = "Test" },
                PageNumber = 1
            };

            await GetFilteredContentServers(2, query, GetData);

            query.Filter.Name = null;
            query.Filter.Description = "Desc";

            await GetFilteredContentServers(3, query, GetData);

            query.Filter.Description = null;
            query.Filter.PublishSiteName = "Publish_";
            await GetFilteredContentServers(1, query, GetData);

            query.Filter.Name = "Server";
            query.Filter.PublishSiteName = null;
            await GetFilteredContentServers(1, query, GetData);
        }

        /// <summary>
        /// Создать сервер публикации
        /// </summary>
        /// <returns>Id сервера публикации</returns>
        private void CreateCloudContentServer(CreateCloudContentServerDto cloudContentServerDto)
        {
            var createResult = _cloudContentServerManager.CreateCloudContentServer(cloudContentServerDto);
            Assert.IsFalse(createResult.Error, createResult.Message);
        }

        /// <summary>
        /// Получить сервера публикации по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество серверов публикации</param>
        /// <param name="filter">Фильтр</param>
        /// <param name="action">Действие, для выбора и проверки выборки северов</param>
        private static async Task GetFilteredContentServers(int expectedCount, GetFilteredContentServersQuery filter,
            Func<GetFilteredContentServersQuery, int, Task> action)
        {
            await action(filter, expectedCount);
        }

        /// <summary>
        /// Получить сервера публикации по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <param name="expectedCount">Ожидаемое количество серверов публикации<</param>
        private async Task GetData(GetFilteredContentServersQuery filter, int expectedCount)
        {
            var enterpriseServers = await Mediator.Send(filter);
            Assert.IsFalse(enterpriseServers.Error);
            Assert.AreEqual(expectedCount, enterpriseServers.Result.Records.Count);
        }

    }
}
