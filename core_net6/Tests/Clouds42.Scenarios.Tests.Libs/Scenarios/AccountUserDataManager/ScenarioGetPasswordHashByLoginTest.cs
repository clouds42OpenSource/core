﻿using System;
using Clouds42.Billing;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUserDataManager
{
    /// <summary>
    /// Сценарий теста получения хеша пароля по логину пользователя
    /// Создаем аккаунт, создаем дополнительного пользователя,
    /// получаем хэш пароля по его логину. Потом пробуем получить хэш пароля
    /// по не существующему логину.
    /// </summary>
    public class ScenarioGetPasswordHashByLoginTest : ScenarioBase
    {
        private readonly TestDataGenerator _testDataGenerator;

        public ScenarioGetPasswordHashByLoginTest()
        {
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();
            var accountUser = _testDataGenerator.GenerateAccountUser(createAccountCommand.AccountId);
            DbLayer.AccountUsersRepository.Insert(accountUser);
            DbLayer.Save();

            var accountUserDataManager = TestContext.ServiceProvider.GetRequiredService<Clouds42.AccountUsers.AccountUser.AccountUserDataManager>();

            var result = accountUserDataManager.GetPasswordHashByLogin(accountUser.Login);

            if (result.Error || accountUser.Password != result.Result)
                throw new InvalidOperationException("Не удалось получить хеш пароля");

            result = accountUserDataManager.GetPasswordHashByLogin("NonExistentLogin");

            if (!result.Error)
                throw new InvalidOperationException("Такой логин не существует.");
        }
    }
}
