﻿using System;
using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PrepareYookassaAggregatorPaymentManagerTest
{
    /// <summary>
    /// Сценарий теста для проверки модели подтверждения платежа,
    /// при подготовке платежа агрегатора ЮKassa
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Выполняем подготовку платежа ЮKassa и ожидаем
    /// что она завершится успешно #PreparePaymentOrPayWithSavedPaymentMethod
    ///
    /// 3) Проверяем данные для подтверждения платежа
    /// агрегатора ЮKassa(токен, урл для редиректа) #CheckConfirmationData
    /// 
    /// </summary>
    public class ScenarioCheckConfirmationDataAtPrepareYookassaPaymentTest : ScenarioBase
    {
        private readonly PrepareOrPayYookassaAggregatorPaymentManager _prepareOrPayYookassaAgregatorPaymentManager;
        private readonly YookassaPaymentDataHelperTest _yookassaPaymentDataHelperTest;

        public ScenarioCheckConfirmationDataAtPrepareYookassaPaymentTest()
        {
            _prepareOrPayYookassaAgregatorPaymentManager = TestContext.ServiceProvider.GetRequiredService<PrepareOrPayYookassaAggregatorPaymentManager>();
            _yookassaPaymentDataHelperTest = TestContext.ServiceProvider.GetRequiredService<YookassaPaymentDataHelperTest>();
        }

        public override void Run()
        {
            var defaultSum = 200;
            

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            
            
          
            var prepareYookassaAgregatorPaymentDto = new PrepareYookassaAggregatorPaymentDto
            {
                AccountId = createAccountCommand.AccountId,
                PaymentSum = defaultSum,
                ReturnUrl = GenerateUrlForRedirectAfterConfirmationPayment()
            };

            var managerResult =
                _prepareOrPayYookassaAgregatorPaymentManager.PreparePayment(prepareYookassaAgregatorPaymentDto);
            Assert.IsFalse(managerResult.Error);

            var yookassaPayment = _yookassaPaymentDataHelperTest.GetFirstYookassaPayment(createAccountCommand.AccountId);
            Assert.IsNotNull(yookassaPayment);

            Assert.IsNotNull(managerResult.Result);
            Assert.IsNotNull(managerResult.Result.ConfirmationToken);
            Assert.AreNotEqual(managerResult.Result.ConfirmationToken, string.Empty);

            var returnUrl = GenerateUrlForRedirectAfterConfirmationPayment();
            Assert.AreEqual(managerResult.Result.ReturnUrl, returnUrl);
            
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProviderFake>();
        }


        /// <summary>
        /// Сформировать урл для редиректа
        /// после подтверждения оплаты платежа
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        /// <returns>Урл для редиректа
        /// после подтверждения оплаты платежа</returns>
        private string GenerateUrlForRedirectAfterConfirmationPayment() => new Uri(
                $@"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/{CloudConfigurationProvider.Cp.GetRouteForRunTaskToCheckYookassaPaymentStatus()}")
            .AbsoluteUri;
    }
}
