﻿using System;
using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PrepareYookassaAggregatorPaymentManagerTest
{
    /// <summary>
    /// Сценарий теста по подготовке платежа 
    /// агрегатора ЮKassa с ошибкой(не успешные кейсы)
    ///
    /// 1) Создаем аккаунт #CreateAccount
    /// 
    /// 2) Пытаемся подготовить платеж с не корректным идентификатором аккаунта(ожидаем ошибку) #PreparePaymentWithIncorrectAccountId
    ///
    /// 3) Пытаемся подготовить платеж с не корректной суммой (ожидаем ошибку) #PreparePaymentWithIncorrectSum
    /// 
    /// </summary>
    public class ScenarioPrepareYookassaPaymentWithErrorTest : ScenarioBase
    {
        private readonly PrepareOrPayYookassaAggregatorPaymentManager _prepareOrPayYookassaAgregatorPaymentManager;
        private readonly YookassaPaymentDataHelperTest _yookassaPaymentDataHelperTest;

        public ScenarioPrepareYookassaPaymentWithErrorTest()
        {
            _prepareOrPayYookassaAgregatorPaymentManager = TestContext.ServiceProvider.GetRequiredService<PrepareOrPayYookassaAggregatorPaymentManager>();
            _yookassaPaymentDataHelperTest = TestContext.ServiceProvider.GetRequiredService<YookassaPaymentDataHelperTest>();
        }

        public override void Run()
        {
            var incorrectAccountId = Guid.NewGuid();
            var defaultSum = 200;
            var incorrectSum = 0;

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();
            

            var prepareYookassaAgregatorPaymentDto = new PrepareYookassaAggregatorPaymentDto
            {
                AccountId = incorrectAccountId,
                PaymentSum = defaultSum
            };

            var managerResult = _prepareOrPayYookassaAgregatorPaymentManager.PreparePayment(prepareYookassaAgregatorPaymentDto);
            Assert.IsTrue(managerResult.Error);

            Assert.IsNull(_yookassaPaymentDataHelperTest.GetFirstYookassaPayment(incorrectAccountId));
            

            prepareYookassaAgregatorPaymentDto.AccountId = createAccountCommand.AccountId;
            prepareYookassaAgregatorPaymentDto.PaymentSum = incorrectSum;

            managerResult = _prepareOrPayYookassaAgregatorPaymentManager.PreparePayment(prepareYookassaAgregatorPaymentDto);
            Assert.IsTrue(managerResult.Error);

            prepareYookassaAgregatorPaymentDto.PaymentSum = -5;
            managerResult = _prepareOrPayYookassaAgregatorPaymentManager.PreparePayment(prepareYookassaAgregatorPaymentDto);
            Assert.IsTrue(managerResult.Error);

            Assert.IsNull(_yookassaPaymentDataHelperTest.GetFirstYookassaPayment(incorrectAccountId));
            
        }
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProviderFake>();
        }
    }
}
