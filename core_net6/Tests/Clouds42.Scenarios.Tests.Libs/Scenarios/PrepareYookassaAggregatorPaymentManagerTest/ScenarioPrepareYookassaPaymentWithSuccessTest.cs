﻿using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PrepareYookassaAggregatorPaymentManagerTest
{
    /// <summary>
    /// Сценарий теста по подготовке платежа 
    /// агрегатора ЮKassa(успешный кейс)
    ///
    /// 1) Создаем аккаунт #CreateAccount
    /// 
    /// 2) Выполняем подготовку платежа ЮKassa и ожидаем
    /// что она завершится успешно #PreparePaymentOrPayWithSavedPaymentMethod
    ///
    /// 3) Проверяем поля платежа агрегатора ЮKassa #CheckYookassaPayment
    ///
    /// 4) Проверяем поля системного платежа #CheckPayment
    /// 
    /// </summary>
    public class ScenarioPrepareYookassaPaymentWithSuccessTest : ScenarioBase
    {
        private readonly PrepareOrPayYookassaAggregatorPaymentManager _prepareOrPayYookassaAgregatorPaymentManager;
        private readonly YookassaPaymentDataHelperTest _yookassaPaymentDataHelperTest;

        public ScenarioPrepareYookassaPaymentWithSuccessTest()
        {
            _prepareOrPayYookassaAgregatorPaymentManager = TestContext.ServiceProvider.GetRequiredService<PrepareOrPayYookassaAggregatorPaymentManager>();
            _yookassaPaymentDataHelperTest = TestContext.ServiceProvider.GetRequiredService<YookassaPaymentDataHelperTest>();
        }

        public override void Run()
        {
            var defaultSum = 200;

            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region PreparePaymentOrPayWithSavedPaymentMethod

            var prepareYookassaAgregatorPaymentDto = new PrepareYookassaAggregatorPaymentDto
            {
                AccountId = createAccountCommand.AccountId,
                PaymentSum = defaultSum
            };

            var managerResult = _prepareOrPayYookassaAgregatorPaymentManager.PreparePayment(prepareYookassaAgregatorPaymentDto);
            Assert.IsFalse(managerResult.Error);

            #endregion

            #region CheckYookassaPayment

            var yookassaPayment = _yookassaPaymentDataHelperTest.GetFirstYookassaPayment(createAccountCommand.AccountId);
            Assert.IsNotNull(yookassaPayment);
            Assert.AreEqual(yookassaPayment.Status, PaymentStatus.Waiting.ToString());
            Assert.AreEqual(yookassaPayment.Sum, defaultSum);

            #endregion

            #region CheckPayment

            var payment = yookassaPayment;
            Assert.AreEqual(payment.AccountId, createAccountCommand.AccountId);
            Assert.AreEqual(payment.Sum, defaultSum);
            Assert.AreEqual(payment.Status, PaymentStatus.Waiting.ToString());
            Assert.AreEqual(payment.PaymentSystem, PaymentSystem.Yookassa.ToString());

            #endregion
        }
        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProviderFake>();
        }
    }
}
