﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.BLL.Common.Helpers;
using Clouds42.CloudServices.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// Сценарий смены тарифа диска за бонусы
    /// Сценарий:
    ///         1) Создаем аккаунт
    ///         2) Создаем бонусный платеж на произвольную сумму(в нашем случае 500)
    ///         3) Изменяем тариф Диска
    ///         4) Проверяем, что тариф изменился и с бонусного баланса списалась правильная сумма
    /// </summary>
    public class ChangeMyDiskTariffForBonuses : ScenarioBase
    {
        private readonly BillingServiceDataProvider _billingServiceDataProvider;
        private readonly MyDiskManager _myDiskManager;
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper;

        public ChangeMyDiskTariffForBonuses()
        {
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
            _myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            var newDiskSize = 20;
            var bonusesAmount = 500;

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var service = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Платеж",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = bonusesAmount,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = TransactionType.Bonus
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = newDiskSize, ByPromisedPayment = false };
            var changeTariffResult = _myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsNotNull(changeTariffResult.Result, "Смена тарифа завершилась не успешно");

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            var resourceConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId && rc.BillingServiceId == service.Id);

            var myDiskCost = _cloud42ServiceHelper.CalculateDiskCost(newDiskSize, createAccountCommand.Account);
            var partialCostOfMyDisk =
                Math.Round(BillingServicePartialCostCalculator.CalculateUntilExpireDateWithoutRound(myDiskCost,
                    resourceConfiguration.ExpireDateValue));
            var diskInfo = _cloud42ServiceHelper.GetMyDiskInfo(createAccountCommand.AccountId);

            Assert.AreEqual(bonusesAmount - partialCostOfMyDisk, billingAccount.BonusBalance,
                $"На бонусном балансе должно было остаться {bonusesAmount - partialCostOfMyDisk}");
            Assert.AreEqual(newDiskSize * 1024, diskInfo.AvailableSize,
                $"Объем дискового пространства должен быть {newDiskSize * 1024}");
        }
    }
}
