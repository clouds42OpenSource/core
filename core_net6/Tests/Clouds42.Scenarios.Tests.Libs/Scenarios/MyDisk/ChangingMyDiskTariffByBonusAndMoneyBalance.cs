﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.BLL.Common.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// Смена тарифа сервиса Мой диск деньгами и бонусами
    /// Сценарий:
    ///         1) Созаем аккаунт
    ///         2) Пополняем бонусный и денежный балансы
    ///         3) Покупаем другой тариф
    ///         4) Проверяем, что тариф изменился и с баланса списались средства
    /// </summary>
    public class ChangingMyDiskTariffByBonusAndMoneyBalance : ScenarioBase
    {
        private readonly BillingServiceDataProvider _billingServiceDataProvider;
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        private readonly MyDiskManager _myDiskManager;

        public ChangingMyDiskTariffByBonusAndMoneyBalance()
        {
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
            _myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
        }

        public override void Run()
        {
            var newDiskSize = 50;

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var service = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

            var resourceConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingServiceId == service.Id && rc.AccountId == createAccountCommand.AccountId);

            var cost = _cloud42ServiceHelper.CalculateDiskCost(newDiskSize, createAccountCommand.Account);
            var partialCost = Math.Round(
                BillingServicePartialCostCalculator.CalculateUntilExpireDateWithoutRound(cost,
                    resourceConfiguration.GetExpireDateValue()));

            var moneyAmount = partialCost / 2;
            var bonusAmount = partialCost / 2;

            CreatePayment(createAccountCommand.AccountId,  service.Id, moneyAmount);
            CreatePayment(createAccountCommand.AccountId, service.Id, bonusAmount, TransactionType.Bonus);

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.AreEqual(moneyAmount, billingAccount.Balance, $"Денежный баланс должен быть {moneyAmount}");
            Assert.AreEqual(bonusAmount, billingAccount.BonusBalance, $"Бонусный баланс должен быть {bonusAmount}");

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = newDiskSize, ByPromisedPayment = false };
            var changeTariffResult = _myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsNotNull(changeTariffResult.Result, "Смена тарифа завершилась не успешно");

            var diskInfo = _cloud42ServiceHelper.GetMyDiskInfo(createAccountCommand.AccountId);

            var billingAccountAfterChangeTariff =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.AreEqual(0, billingAccountAfterChangeTariff.Balance, "Денежный баланс должен быть 0");
            Assert.AreEqual(0, billingAccountAfterChangeTariff.BonusBalance, "Бонусный баланс должен быть 0");
            Assert.AreEqual(newDiskSize * 1024, diskInfo.AvailableSize, $"Размер дискового пространства должне быть {newDiskSize * 1024}");
        }

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="transactionType">Тип транзакции</param>
        private void CreatePayment(Guid accountId, Guid serviceId, decimal sum, TransactionType transactionType = TransactionType.Money)
        {
            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = accountId,
                Date = DateTime.Now,
                Description = "Платеж",
                BillingServiceId = serviceId,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = sum,
                OriginDetails = PaymentSystem.ControlPanel.ToString(),
                TransactionType = transactionType
            });
        }
    }
}
