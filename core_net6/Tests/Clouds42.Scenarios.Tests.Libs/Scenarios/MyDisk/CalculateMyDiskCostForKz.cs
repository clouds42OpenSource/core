﻿using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Проверка правильности подсчета стоимости Гб-ов в Мой Диск (Казахстан)
    /// </summary>
    public class CalculateMyDiskCostForKz : ScenarioBase
    {
        private readonly MyDiskServiceCostHelper _myDiskServiceCostHelper;

        public CalculateMyDiskCostForKz()
        {
            _myDiskServiceCostHelper = TestContext.ServiceProvider.GetRequiredService<MyDiskServiceCostHelper>();
        }

        public override void Run()
        {
            var account = CreateAccountOrThrowException(new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Kazakhstan
            });

            _myDiskServiceCostHelper.CheckServiceCostBySize(account.Id, CheckCostOfMyDiskParams.GetParamsForKazakhstan());
        }
    }
}
