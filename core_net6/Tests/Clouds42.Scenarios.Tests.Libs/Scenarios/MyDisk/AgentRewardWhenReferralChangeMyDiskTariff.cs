﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.BLL.Common.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// Начисление агентского вознаграждения при смене тарифа севиса Мой диск
    /// Сценарий:
    ///         1) Создаем аккаунт и реферал этого аккаунта
    ///         2) Изменяем тариф диска рефералу
    ///         3) Проверяем, что начислилось агентское вознаграждение
    ///         4) Проверяем, что изменился тариф диска
    /// </summary>
    public class AgentRewardWhenReferralChangeMyDiskTariff : ScenarioBase
    {
        private readonly BillingServiceDataProvider _billingServiceDataProvider;
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        private readonly MyDiskManager _myDiskManager;
        private readonly IAgencyAgreementDataProvider _agencyAgreementDataProvider;

        public AgentRewardWhenReferralChangeMyDiskTariff()
        {
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
            _myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            _agencyAgreementDataProvider = TestContext.ServiceProvider.GetRequiredService<IAgencyAgreementDataProvider>();
        }

        public override void Run()
        {
            var newDiskSize = 50;

            var createReferrerAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createReferrerAccount.Run();

            var createReferralAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    ReferralAccountId = createReferrerAccount.AccountId
                }
            });
            createReferralAccountCommand.Run();

            var service = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var resourceConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.BillingServiceId == service.Id && rc.AccountId == createReferralAccountCommand.AccountId);

            var myDiskCost = _cloud42ServiceHelper.CalculateDiskCost(newDiskSize, createReferralAccountCommand.Account);
            var partialCostOfMyDisk =
                Math.Round(BillingServicePartialCostCalculator.CalculateUntilExpireDateWithoutRound(myDiskCost,
                    resourceConfiguration.ExpireDateValue));

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createReferralAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = partialCostOfMyDisk,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createReferralAccountCommand.AccountId, SizeGb = newDiskSize, ByPromisedPayment = false };
            var changeTariffResult = _myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsNotNull(changeTariffResult.Result, "Смена тарифа завершилась не успешно");

            var diskInfo = _cloud42ServiceHelper.GetMyDiskInfo(createReferralAccountCommand.AccountId);
            Assert.AreEqual(newDiskSize * 1024, diskInfo.AvailableSize,
                $"Объем дискового пространства должен быть {newDiskSize * 1024}");

            var agencyAgreement = _agencyAgreementDataProvider.GetActualAgencyAgreement();
            var agentReward = (partialCostOfMyDisk * agencyAgreement.MyDiskRewardPercent) / 100;

            RefreshDbCashContext(Context.AgentWallets);
            var agentWallet =
                DbLayer.AgentWalletRepository.FirstOrDefault(aw =>
                    aw.AccountOwnerId == createReferrerAccount.AccountId);
            Assert.AreEqual(agentReward, agentWallet.AvailableSum,
                $"На кошельке агента должна быть сумма{agentReward}");
        }
    }
}
