﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.CloudServices.MyDisk.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.DataContracts.MyDisk;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk
{
    /// <summary>
    ///     Сценарий уменьшения дискового пространства при неактивном сервисе + баланса 
    /// достаточно для продления измененной подписки
    /// 
    ///     Предусловия: для аккаунта test_account подключен тариф МойДиск 13 Гб;
    /// баланс 1000 руб; дата окончания сервиса (-15) дней от тек.даты.
    /// 
    ///     Действия: попытка изменить тариф на 11Гб
    /// 
    ///     Проверка: попытка смены тарифа успешна, с баланса списанна сумма за
    /// продление сервиса по новому тарифу, дата и цена сервиса изменилась,
    /// сервис разблокирован, пустые ресурсы отсутствуют
    /// </summary>
    public class DecreaseSizeWithMoneyAndFrozenServiceTest : ScenarioBase
    {
        readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        public DecreaseSizeWithMoneyAndFrozenServiceTest()
        {
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var diskSize = 13;

            var costOfDisk = ServiceProvider.GetRequiredService<MyDiskManager>().TryChangeMyDiskTariff(createAccountCommand.AccountId, diskSize).Result.CostOftariff;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = costOfDisk,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = diskSize, ByPromisedPayment = false };
            var result = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsNotNull(result.Result);
            Assert.IsTrue(result.Result);

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var ballance = 1000m + resourcesConfiguration.Cost;

            var billingService = new BillingServiceDataProvider(DbLayer).GetSystemService(Clouds42Service.MyEnterprise);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = ballance,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            // act
            diskSize = 11;
            costOfDisk = _cloud42ServiceHelper.ChangeSizeOfServicePreview(createAccountCommand.AccountId, diskSize).Cost;
            var newResult = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().TryChangeMyDiskTariff(createAccountCommand.AccountId, diskSize).Result;


            // assert
            var resourceMyDisk = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyDisk);

            var resourceRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyDisk && !res.Subject.HasValue);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(newResult);

            Assert.IsNotNull(billingAccount);

            Assert.IsNotNull(resourceMyDisk);

            Assert.AreEqual(ballance - (costOfDisk + resourcesConfiguration.Cost), billingAccount.Balance);

            Assert.AreEqual(false, resourceRent1C.FrozenValue);

            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceRent1C.ExpireDateValue.ToShortDateString());

            Assert.AreEqual(costOfDisk, resourceMyDisk.Cost);

            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов МойДиск не должно оставатся!!");
        }
    }
}
