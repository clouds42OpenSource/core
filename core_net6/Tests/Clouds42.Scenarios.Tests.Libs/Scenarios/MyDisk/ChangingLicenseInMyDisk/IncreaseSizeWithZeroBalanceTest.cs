﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk
{
    /// <summary>
    ///     Сценарий добавления дискового пространства при активном
    /// сервисе и недостаточном балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен тариф МойДиск 10 Гб;
    /// баланс 0 руб; дата окончания сервиса  +10 дней от тек.даты.
    ///  
    ///     Действия: попытка купить тариф 13Гб
    /// 
    ///     Проверка: попытка купить диск не успешна, цена и дата сервиса остались прежними
    /// </summary>
    public class IncreaseSizeWithZeroBalanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyDisk);

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;

            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);


            // act
            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = 13, ByPromisedPayment = false };
            var result = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().ChangeMyDiskTariff(changemydiskmodel);


            // assert
            var resourceMyDisk = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyDisk);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);


            Assert.IsNotNull(result);

            Assert.IsNotNull(billingAccount);

            Assert.IsNotNull(resourceMyDisk);

            Assert.IsTrue(result.Result);

            Assert.AreEqual(0, billingAccount.Balance);

            Assert.AreEqual(false, resourceMyDisk.FrozenValue);

            Assert.AreEqual(dateValue.ToShortDateString(), resourceMyDisk.ExpireDateValue.ToShortDateString());

            Assert.AreEqual(0, resourceMyDisk.Cost);
        }
    }
}
