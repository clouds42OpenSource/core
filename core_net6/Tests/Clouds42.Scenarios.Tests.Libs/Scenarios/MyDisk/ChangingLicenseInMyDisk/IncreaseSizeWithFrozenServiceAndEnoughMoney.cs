﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.CloudServices.MyDisk.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk
{
    /// <summary>
    ///     Сценарий добавления дискового пространства при заблокированом
    /// сервисе и достаточном балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен тариф МойДиск 10 Гб;
    /// баланс 1000 руб; дата окончания сервиса -1 месяц от сегодняшней даты.
    ///  
    ///     Действия: попытка увеличить тариф от 11 до 25Гб
    /// 
    ///     Проверка: попытка увеличения тарифа успешна, деньги списаны с баланса,
    /// дата сервиса продлена на месяц, цена сервиса изменилась, пустые ресурсы отсутствуют
    /// </summary>
    public class IncreaseSizeWithFrozenServiceAndEnoughMoney : ScenarioBase
    {
        readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        public IncreaseSizeWithFrozenServiceAndEnoughMoney()
        {
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();


            var diskSize = 11;

            var costOfDisk = ServiceProvider.GetRequiredService<MyDiskManager>().TryChangeMyDiskTariff(createAccountCommand.AccountId, diskSize).Result.CostOftariff;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = costOfDisk,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = diskSize, ByPromisedPayment = false };
            var result = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsNotNull(result.Result);
            Assert.IsTrue(result.Result);

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyEnterprise);

            var ballance = 1000m + resourcesConfiguration.Cost;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = ballance,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            resourcesConfiguration.ExpireDate = DateTime.Now.AddMonths(-1);
            resourcesConfiguration.Frozen = true;
            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var randomSize = new Random().Next(12, 25);
            var costOfTariff = _cloud42ServiceHelper.ChangeSizeOfServicePreview(createAccountCommand.AccountId, randomSize).Cost;

            // act
            var changedTariffResult = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().TryChangeMyDiskTariff(createAccountCommand.AccountId, randomSize).Result;

            // assert
            var resourceMyDisk = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyDisk);

            var resourceMyRent1C = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyDisk);

            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyDisk && !res.Subject.HasValue);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);


            Assert.IsNotNull(changedTariffResult);

            Assert.IsNotNull(billingAccount);

            Assert.IsNotNull(resourceMyDisk);

            Assert.IsNotNull(resourceMyRent1C);

            Assert.AreEqual(ballance - (costOfTariff + resourcesConfiguration.Cost), billingAccount.Balance);

            Assert.AreEqual(false, resourceMyRent1C.FrozenValue);

            Assert.AreEqual(DateTime.Now.AddMonths(1).ToShortDateString(), resourceMyRent1C.ExpireDateValue.ToShortDateString());

            Assert.AreEqual(costOfTariff, resourceMyDisk.Cost);

            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов МойДиск не должно оставатся!!");
        }
    }
}
