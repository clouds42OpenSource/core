﻿using System.Collections.Generic;
using Clouds42.Billing.Contracts.Billing.Interfaces;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk
{
    /// <summary>
    /// Сценарий изменения лицензий "Мой Диск" и "Аренда 1С" сейлменеджером
    /// у другого аккаунта
    /// Действия:
    /// 1) Создаём первый аккаунт, подключаем ему аренду
    /// 2) Создаём второй аккаунт, меняем роль его админа на сейлменеджер
    /// 3) Заходим сейлменеджером с-под первого аккаунта
    /// 4) Пробуем изменить стоимость диска
    /// 5) Пробудем изменить стоимость аренды со "Стандарт" на "Веб"
    /// Проверяем: мы должны получить два отказа на пробы.
    /// </summary>
    public class ScenarioChangingLicenseInAlienAccAsSaleManagerTest : ScenarioBase
    {
        public override void Run()
        {
            #region accounts
            var createFirstAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createFirstAccountCommand.Run();

            var createSecondAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createSecondAccountCommand.Run();

            var secondadmin =
                DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == createSecondAccountCommand.AccountAdminId);
            var role = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == secondadmin.Id);
            role.AccountUserGroup = AccountUserGroup.AccountSaleManager;
            DbLayer.AccountUserRoleRepository.Update(role);
            DbLayer.Save();
            #endregion accounts

            var accessProvider = new TestBaseAccessProvider(DbLayer, Logger, Configuration, HandlerException, AccessMapping);
            var rent1CConfigurationAccessManager = new Rent1CConfigurationAccessManager(
                accessProvider, 
                DbLayer, 
                CloudLocalizer,
                new Rent1CConfigurationAccessProvider( 
                    ServiceProvider.GetRequiredService<IResourcesService>(), 
                    ServiceProvider.GetRequiredService<IResourceConfigurationDataProvider>(), 
                    ServiceProvider.GetRequiredService<IServiceUnlocker>(),
                    ServiceProvider.GetRequiredService<IDocLoaderByRent1CProlongationProcessor>(), 
                    ServiceProvider,
                    Logger, HandlerException),
                HandlerException);
            var accessRent1C = new List<UpdaterAccessRent1CRequestDto>
            {
                new()
                {
                    AccountUserId = createFirstAccountCommand.AccountAdminId,
                    StandartResource = false,
                    WebResource = true,
                    SponsorAccountUser = false
                }
            };
            var result = rent1CConfigurationAccessManager.ConfigureAccesses(createSecondAccountCommand.AccountId, accessRent1C);
            Assert.IsTrue(result.Error, "Должна вернуться ошибка!");
            Assert.IsTrue(result.Exception is AccessDeniedException,result.Message);
            Assert.AreEqual(result.State, ManagerResultState.PreconditionFailed, "Неверный тип ошибки.");
        }
    }
}
