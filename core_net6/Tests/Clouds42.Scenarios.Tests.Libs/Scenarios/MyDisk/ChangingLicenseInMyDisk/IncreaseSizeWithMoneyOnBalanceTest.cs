﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk.ChangingLicenseInMyDisk
{
    /// <summary>
    ///     Сценарий добавления дискового пространства при активном
    /// сервисе и при достаточном балансе
    /// 
    ///     Предусловия: для аккаунта test_account подключен тариф МойДиск 10 Гб;
    /// баланс 1000 руб; дата окончания сервиса +10 дней от тек.даты.
    ///  
    ///     Действия: попытка купить тариф 13Гб
    /// 
    ///     Проверка: попытка купить диск успешна, деньги списаны с баланса,
    /// дата сервиса осталась прежней, цена сервиса изменилась, пустые ресурсы отсутствуют
    /// </summary>
    public class IncreaseSizeWithMoneyOnBalanceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });

            createAccountCommand.Run();

            var dateValue = DateTime.Now.AddDays(10);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(config =>
                config.AccountId == createAccountCommand.AccountId &&
                config.BillingService.SystemService == Clouds42Service.MyDisk);

            resourcesConfiguration.ExpireDate = dateValue;
            resourcesConfiguration.Frozen = false;

            DbLayer.ResourceConfigurationRepository.Update(resourcesConfiguration);
            DbLayer.Save();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(billingAccount);

            var ballance = 1000m;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = ballance,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            // act
            var myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var costOfTariff = myDiskManager.TryChangeMyDiskTariff(createAccountCommand.AccountId, 13).Result.CostOftariff;
            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = 13, ByPromisedPayment = false };
            var result = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);


            // assert
            var resourceMyDisk = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                        r =>
                            r.AccountId == createAccountCommand.AccountId &&
                            r.BillingService.SystemService == Clouds42Service.MyDisk);

            var emptyResources = DbLayer.ResourceRepository.Where(res =>
                res.BillingServiceType.Service.SystemService == Clouds42Service.MyDisk && !res.Subject.HasValue);

            billingAccount = DbLayer.BillingAccountRepository
                .FirstOrDefault(acc => acc.Id == createAccountCommand.AccountId);


            Assert.IsNotNull(result.Result);

            Assert.IsNotNull(billingAccount);

            Assert.IsNotNull(resourceMyDisk);

            Assert.AreEqual(ballance - costOfTariff, billingAccount.Balance);

            Assert.AreEqual(false, resourceMyDisk.FrozenValue);

            Assert.AreEqual(dateValue.ToShortDateString(), resourceMyDisk.ExpireDateValue.ToShortDateString());

            Assert.AreEqual(150, resourceMyDisk.Cost);

            Assert.AreEqual(0, emptyResources.Count(), "Свободных ресурсов МойДиск не должно оставатся!!");
        }
    }
}
