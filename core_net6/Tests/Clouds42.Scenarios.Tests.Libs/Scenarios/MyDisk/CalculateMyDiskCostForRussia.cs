﻿using Clouds42.Common.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Проверка правильности подсчета стоимости Гб-ов в Мой Диск (Россия)
    /// </summary>
    public class CalculateMyDiskCostForRussia : ScenarioBase
    {
        private readonly MyDiskServiceCostHelper _myDiskServiceCostHelper;

        public CalculateMyDiskCostForRussia()
        {
            _myDiskServiceCostHelper = TestContext.ServiceProvider.GetRequiredService<MyDiskServiceCostHelper>();
        }

        public override void Run()
        {
            var account = CreateAccountOrThrowException(new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Russia
            }); 
            
            _myDiskServiceCostHelper.CheckServiceCostBySize(account.Id, CheckCostOfMyDiskParams.GetParamsForRussia());
        }
    }
}
