﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Core42.Application.Features.MyDiskContext.Queries;
using System.Threading.Tasks;
using Clouds42.CloudServices.Contracts;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Тест на совпадение стоимости за Гб после смены тарифа (Россия) 
    /// </summary>
    public class CalculateAndChangeTariffRus : ScenarioBase
    {
        readonly ICloud42ServiceHelper _cloud42ServiceHelper;

        public CalculateAndChangeTariffRus()
        {
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createRusAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createRusAccount.Run();

            var account = createRusAccount.Account;

            if (account == null)
                throw new InvalidOperationException ("Что то пошло не так");

            var size = new Random().Next(11, 500);

            var calculateCost = _cloud42ServiceHelper.ChangeSizeOfServicePreview(account.Id, size);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = calculateCost.Cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = account.Id, SizeGb = size, ByPromisedPayment = false };
            var changeTariff = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsTrue(changeTariff.Result);

            var actualTariff = await Mediator.Send(new GetMyDiskDataQuery { AccountId = account.Id });
            Assert.AreEqual(calculateCost.Cost, actualTariff.Result.MonthlyCost);
        }
    }
}
