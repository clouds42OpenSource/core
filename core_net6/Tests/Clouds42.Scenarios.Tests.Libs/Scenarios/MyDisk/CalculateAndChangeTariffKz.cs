﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices;
using Clouds42.DataContracts.MyDisk;
using Clouds42.CloudServices.Contracts;
using MediatR;
using Core42.Application.Features.MyDiskContext.Queries;
using System.Threading.Tasks;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Тест на совпадение стоимости за Гб после смены тарифа (Казахстан) 
    /// </summary>
    public class CalculateAndChangeTariffKz : ScenarioBase
    {
        readonly ICloud42ServiceHelper _cloud42ServiceHelper;
        public CalculateAndChangeTariffKz()
        {
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var model = new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Kazakhstan,
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            };

            var createKzAccount = new CreateAccountCommand(TestContext, model);
            createKzAccount.Run();

            var account = createKzAccount.Account;

            if (account == null)
                throw new InvalidOperationException ("Что то пошло не так");

            var size = new Random().Next(11, 500);

            var calculateCost = _cloud42ServiceHelper.ChangeSizeOfServicePreview(account.Id, size);

            var billingService = new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = account.Id,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = calculateCost.Cost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = account.Id, SizeGb = size, ByPromisedPayment = false };
            var changeTariff = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>().ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsTrue(changeTariff.Result);

            var actualTariff = await TestContext.ServiceProvider.GetRequiredService<ISender>().Send(new GetMyDiskDataQuery { AccountId = account.Id });
            Assert.AreEqual(calculateCost.Cost, actualTariff.Result.MonthlyCost);
        }
    }
}
