﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.CloudServices.MyDisk.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Тест покупки Гб-ов в Мой Диск (Россия)
    /// </summary>
    public class ChangeMyDiskTariffRus : ScenarioBase
    {
        public override void Run()
        {
            var createRusAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createRusAccount.Run();

            const decimal costOf11Gb = 50m;
            const decimal costOf16Gb = 295m;

            var account = createRusAccount.Account;

            if (account == null)
                throw new InvalidOperationException ("Аккаунт не найден или не был создан");

            var myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var result = myDiskManager.TryChangeMyDiskTariff(account.Id, 5).Result;
            
            Assert.IsTrue(result.Complete);
            
            account.BillingAccount.Balance = costOf11Gb;
            TestContext.DbLayer.BillingAccountRepository.Update(account.BillingAccount);


            result = myDiskManager.TryChangeMyDiskTariff(account.Id, 11).Result;

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = account.Id, SizeGb = 11, ByPromisedPayment = false };
            var tryToPay = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            
            Assert.IsFalse(result.Complete);
            Assert.AreEqual(costOf11Gb, result.MonthlyCost);
            Assert.IsTrue(tryToPay.Result);

            account.BillingAccount.Balance = costOf16Gb;
            TestContext.DbLayer.BillingAccountRepository.Update(account.BillingAccount);

            changemydiskmodel.SizeGb = 16;
            result = myDiskManager.TryChangeMyDiskTariff(account.Id, 16).Result;
            tryToPay = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsFalse(result.Complete);
            Assert.AreEqual(costOf16Gb, result.MonthlyCost);
            Assert.IsTrue(tryToPay.Result);
        }
    }
}
