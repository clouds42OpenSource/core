﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// Смена тарифа сервиса Мой диск (оплата тарифа обещанным платежом)
    /// Сценарий:
    ///         1) Создаем аккаунт
    ///         2) Создаем платеж (чтобы можно было брать ОП)
    ///         3) Изменяем тариф сервиса
    ///         4) Проверяем, что обещанный платеж создан
    ///         5) Проверяем, что  тариф Моего диска изменился
    /// </summary>
    public class ChangeMyDiskTariffByPromisePayment : ScenarioBase
    {
        private readonly BillingServiceDataProvider _billingServiceDataProvider;
        private readonly MyDiskManager _myDiskManager;
        private readonly ICloud42ServiceHelper _cloud42ServiceHelper;

        public ChangeMyDiskTariffByPromisePayment()
        {
            _billingServiceDataProvider = TestContext.ServiceProvider.GetRequiredService<BillingServiceDataProvider>();
            _myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            _cloud42ServiceHelper = TestContext.ServiceProvider.GetRequiredService<ICloud42ServiceHelper>();
        }

        public override void Run()
        {
            var myDiskSize = 50;
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var service = _billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var blockRent1C = new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-1)
            };
            TestContext.ServiceProvider.GetRequiredService<Rent1CManager>().ManageRent1C(createAccountCommand.AccountId, blockRent1C);

            var localeId = GetAccountLocaleId(createAccountCommand.AccountId);

            var rent1CStandartCost = DbLayer.RateRepository
                .Where(rc =>
                    rc.BillingServiceType.ServiceId == service.Id &&
                    rc.LocaleId == localeId && rc.AccountType == "Standart")
                .Sum(rc => rc.Cost);

            TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Платеж",
                BillingServiceId = service.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = rent1CStandartCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = createAccountCommand.AccountId, SizeGb = myDiskSize, ByPromisedPayment = true };
            var result = _myDiskManager.ChangeMyDiskTariff(changemydiskmodel);
            Assert.IsTrue(result.Result, "Смена тарифа завершилась не успешно");

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount.PromisePaymentDate, "Дата ОП не должна быть null");

            var diskInfo = _cloud42ServiceHelper.GetMyDiskInfo(createAccountCommand.AccountId);
            Assert.AreEqual(myDiskSize * 1024, diskInfo.AvailableSize,
                $"Объем дискового пространства должен быть {myDiskSize * 1024}");
        }
    }
}
