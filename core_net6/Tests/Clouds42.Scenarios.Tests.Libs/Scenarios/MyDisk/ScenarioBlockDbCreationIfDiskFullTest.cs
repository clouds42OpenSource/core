﻿using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// Проверка работы с базами когда диск переполнен
    /// Действия:
    /// 1) Создаём аккаунт
    /// 2) Симулируем переполнение диска
    /// 3) Получаем модель для отображения страницы бд
    /// Проверяем:
    /// 1) Статус сервиса заблокирован.
    /// </summary>
    public class ScenarioBlockDbCreationIfDiskFullTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingAccount = DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            AssertHelper.Execute(() => Assert.IsNotNull(billingAccount, "Ошибка создания аккаунта"));

            SetMyDiskFilesSizeInMbForAccount(createAccountCommand.AccountId, 20000);

           DbLayer.RefreshAll();

            var result = TestContext.ServiceProvider.GetRequiredService<AccountDatabasesCommonDataManager>()
                .GetCommonDataForWorkingWithDatabases(createAccountCommand.AccountId,
                    createAccountCommand.AccountAdminId);

            AssertHelper.Execute(() => Assert.IsFalse(result.Error, "Ошибка получения модели"));
            AssertHelper.Execute(() =>
                Assert.AreEqual(result.State, ManagerResultState.Ok, "Неверный статус результата, должен быть ОК"));
            AssertHelper.Execute(() => Assert.IsTrue(result.Result.MainServiceStatusInfo.ServiceIsLocked,
                "Сервис должен быть заблокирован."));
            AssertHelper.Execute(() =>
                Assert.IsTrue(result.Result.MainServiceStatusInfo.ServiceLockReason.Contains("Нет свободного")));
        }
    }
}
