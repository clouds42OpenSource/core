﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Common.Constants;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    ///     Тест покупки Гб-ов в Мой Диск (Украина)
    /// </summary>
    public class ChangeMyDiskTariffUkr : ScenarioBase
    {
        public override void Run()
        {
            var model = new AccountRegistrationModelTest
            {
                LocaleName = LocaleConst.Ukraine,
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            };

            var createUkrAccount = new CreateAccountCommand(TestContext, model);
            createUkrAccount.Run();

            const decimal costOf11Gb = 35m;
            const decimal costOf16Gb = 208m;

            var account = createUkrAccount.Account;

            if (account == null)
                throw new InvalidOperationException ("Аккаунт не найден или не был создан");

            var myDiskManager = TestContext.ServiceProvider.GetRequiredService<MyDiskManager>();
            var result = myDiskManager.TryChangeMyDiskTariff(account.Id, 5).Result;

            Assert.IsTrue(result.Complete);

            account.BillingAccount.Balance = costOf11Gb;
            TestContext.DbLayer.BillingAccountRepository.Update(account.BillingAccount);

            var changemydiskmodel = new MyDiskChangeTariffDto { AccountId = account.Id, SizeGb = 11, ByPromisedPayment = false };
            result = myDiskManager.TryChangeMyDiskTariff(account.Id, 11).Result;
            var tryToPay = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsFalse(result.Complete);
            Assert.AreEqual(costOf11Gb, result.MonthlyCost);
            Assert.IsTrue(tryToPay.Result);

            account.BillingAccount.Balance = costOf16Gb;
            TestContext.DbLayer.BillingAccountRepository.Update(account.BillingAccount);

            changemydiskmodel.SizeGb = 16;
            result = myDiskManager.TryChangeMyDiskTariff(account.Id, 16).Result;
            tryToPay = myDiskManager.ChangeMyDiskTariff(changemydiskmodel);

            Assert.IsFalse(result.Complete);
            Assert.AreEqual(costOf16Gb, result.MonthlyCost);
            Assert.IsTrue(tryToPay.Result);
        }
    }
}
