﻿using Clouds42.Billing.Billing.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Billing.Billing.Providers;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.Accounts.Contracts.MyDiskPropertiesByAccount.Interfaces;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.MyDisk
{
    /// <summary>
    /// тест пролонгации диска (заблокированного по причине переполения места)
    /// после запуска операции пересчёта
    /// Сценарий:
    /// 1) Создаём аккаунт
    /// 2) Кладём деньги на счёт
    /// 3) Симулируем заполение диска пользовательскими файлами
    /// 4) Блокируем диск sql операцией
    /// 5) Уменьшаем обьём пользовательских фалов
    /// 6) Запускаем операцию пересчёта
    /// 7) Проверяем, что дата сервис разблокирован,
    ///    деньги не сняты
    /// </summary>
    public class ScenarioProlongDiskAfterRecalculateAccountSizeTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            var billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);
            Assert.IsNotNull(billingAccount, "Ошибка создания аккаунта");

            var billingService =
                new BillingServiceDataProvider(TestContext.DbLayer).GetSystemService(Clouds42Service.MyDisk);
            var startBalance = 3500;

            ServiceProvider.GetRequiredService<CreatePaymentManager>().AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Задаем начальный баланс",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = startBalance,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            DbLayer.ResourceConfigurationRepository
                .AsQueryable()
                .ExecuteUpdate(x => x.SetProperty(y => y.Frozen, true));

            SetMyDiskFilesSizeInMbForAccount(createAccountCommand.AccountId, 10240);

            RefreshDbCashContext(TestContext.Context.ResourcesConfigurations);
            RefreshDbCashContext(TestContext.Context.Accounts);

            var myDiskManager = TestContext.ServiceProvider.GetRequiredService<RecalculateMyDiskUsedSizeManager>();
            myDiskManager.RecalculateMyDiskUsedSize(createAccountCommand.AccountId);

            #region Checking

            var accountFilesSize = TestContext.ServiceProvider.GetRequiredService<IMyDiskPropertiesByAccountDataProvider>()
                .GetIfExistsOrCreateNew(createAccountCommand.AccountId).MyDiskFilesSizeInMb;

            Assert.AreEqual(0, accountFilesSize, "Истинный объём файлов - 0.");

            RefreshDbCashContext(Context.ResourcesConfigurations);

            var resourcesConfiguration = DbLayer.ResourceConfigurationRepository.FirstOrDefault(
                r =>
                    r.AccountId == createAccountCommand.AccountId &&
                    r.BillingService.SystemService == Clouds42Service.MyEnterprise);

            billingAccount =
                DbLayer.BillingAccountRepository.FirstOrDefault(ba => ba.Id == createAccountCommand.AccountId);

            Assert.IsNotNull(resourcesConfiguration);
            Assert.AreEqual(false, resourcesConfiguration.FrozenValue, "Должна быть разморозка.");
            Assert.AreEqual(billingAccount.Balance, startBalance, "Баланс должен быть неизменным.");

            #endregion Checking
        }
    }
}
