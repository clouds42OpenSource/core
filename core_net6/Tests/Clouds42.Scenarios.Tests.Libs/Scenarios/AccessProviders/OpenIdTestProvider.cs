﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders
{
    /// <summary>
    ///     Тестовый OpenId провайдер
    /// </summary>
    public class OpenIdTestProvider : IOpenIdProvider
    {
        /// <summary>
        /// Создать пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        public void AddUser(SauriOpenIdControlUserModel model, string locale)
        {
            // заглушка
        }

        /// <summary>
        /// Удалить пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        public void DeleteUser(SauriOpenIdControlUserModel model)
        {
            // заглушка
        }

        /// <summary>
        /// Изменить пароль для пользователя в OpenId
        /// </summary>
        /// <param name="model">Модель данных</param>
        public void ChangeUserProperties(SauriOpenIdControlUserModel model, string locale)
        {
            // заглушка
        }
    }
}
