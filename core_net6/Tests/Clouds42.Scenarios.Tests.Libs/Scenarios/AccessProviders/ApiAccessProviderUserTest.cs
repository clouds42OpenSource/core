﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common.Access;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders
{
    /// <summary>
    /// Класс для проверки работы методов ApiAccessProvider для пользователя
    /// </summary>
    public class ApiAccessProviderUserTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountAdminId = createAccountCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.FirstOrDefault(item => item.Id == accountAdminId);

            if (adminUser == null)
                throw new NotFoundException("Пользователь-администратор аккаунта не найден.");

            var httpContextAccessor = ServiceProvider.GetRequiredService<IHttpContextAccessor>();

            httpContextAccessor.HttpContext.InstallAsCurrentContext(createAccountCommand.Token.ToString());

            DbLayer.AccountUserRoleRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountUserId == accountAdminId)
                .ExecuteDelete();

            var apiAccessProvider = ServiceProvider.GetRequiredService<ApiAccessProvider>();

            var userPrincipal = apiAccessProvider.GetUser();

            if (userPrincipal.Groups.Count != 0)
                throw new NotFoundException("Группы пользователя возвращены некорректно, должно быть 0");

            DbLayer.AccountUserRoleRepository.InsertRange(new List<AccountUserRole>
            {
                new()
                {
                    Id = Guid.NewGuid(),
                    AccountUserId = accountAdminId,
                    AccountUserGroup = AccountUserGroup.AccountUser
                },
                new()
                {
                    Id = Guid.NewGuid(),
                    AccountUserId = accountAdminId,
                    AccountUserGroup = AccountUserGroup.AccountAdmin
                },
                new()
                {
                    Id = Guid.NewGuid(),
                    AccountUserId = accountAdminId,
                    AccountUserGroup = AccountUserGroup.CloudAdmin
                }
            });
            DbLayer.Save();
            
            var apiAccessProvider2 = ServiceProvider.GetRequiredService<ApiAccessProvider>();
            var userPrincipal2 = apiAccessProvider2.GetUser();

            var listOfActualGroups = userPrincipal2.Groups;

            var listOfExpectedGroups = new List<AccountUserGroup>
            {
                AccountUserGroup.AccountUser, AccountUserGroup.AccountAdmin, AccountUserGroup.CloudAdmin
            };

            var expectedNotActual = listOfExpectedGroups.Except(listOfActualGroups).ToList();
            var actualNotExpected = listOfActualGroups.Except(listOfExpectedGroups).ToList();

            if (expectedNotActual.Any() || actualNotExpected.Any())
                throw new InvalidOperationException(
                    $@"Рассчитанные группы пользователя-администратора аккаунта c id={createAccountCommand.AccountAdminId}
                            равны {string.Join(",", listOfActualGroups.ToArray())} 
                            и не соответствуют ожидаемым {string.Join(",", listOfExpectedGroups.ToArray())}");
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IHttpContextAccessor, FakeHttpContextAccessor>();
            serviceCollection.AddTransient<ApiAccessProvider>();
            serviceCollection.AddTransient<IUserPrincipalHelper, UserPrincipalHelper>();
        }
    }
}
