﻿using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders
{
    public class ScenarioTestAccessProvider(
        IAccountDetails accountDetails,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        protected override string GetUserIdentity()
        {
            return accountDetails.AccountId.ToString();
        }

        public override IUserPrincipalDto GetUser()
        {
            return new AccountUserPrincipalDto
            {
                Token = accountDetails.Token,
                ContextAccountId = accountDetails.AccountId,
                Id = accountDetails.AccountAdminId,
                Name = GetUserIdentity(),
                Groups = [AccountUserGroup.AccountAdmin]
            };
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new System.NotImplementedException();
        }

        public override string GetUserAgent()
        {
            return null;
        }

        public override string GetUserHostAddress()
        {
            return null;
        }

        public override string Name => GetUserIdentity();

    }
}
