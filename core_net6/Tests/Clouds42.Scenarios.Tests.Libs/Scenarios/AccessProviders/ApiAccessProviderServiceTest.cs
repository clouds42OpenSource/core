﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common.Access;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders
{
    /// <summary>
    /// Класс для проверки работы методов ApiAccessProvider для службы 
    /// </summary>
    public class ApiAccessProviderServiceTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccountAndServiceCommand = new CreateAccountAndServiceCommand(TestContext);
            createAccountAndServiceCommand.Run();

            var accountAdminId = createAccountAndServiceCommand.AccountAdminId;

            var adminUser = DbLayer.AccountUsersRepository.FirstOrDefault(item => item.Id == accountAdminId);

            if (adminUser == null)
                throw new NotFoundException("Учётная запись службы не найдена.");

            var httpContextAccessor = ServiceProvider.GetRequiredService<IHttpContextAccessor>();

            httpContextAccessor.HttpContext.InstallAsCurrentContext(createAccountAndServiceCommand.Token.ToString());

            var apiAccessProvider = ServiceProvider.GetRequiredService<ApiAccessProvider>();
            var userPrincipal = apiAccessProvider.GetUser();

            var listOfActualGroups = userPrincipal.Groups;

            var listOfExpectedGroups = new List<AccountUserGroup>
            {
                AccountUserGroup.Cloud42Service
            };

            var expectedNotActual = listOfExpectedGroups.Except(listOfActualGroups).ToList();
            var actualNotExpected = listOfActualGroups.Except(listOfExpectedGroups).ToList();

            if (expectedNotActual.Any() || actualNotExpected.Any())
                throw new InvalidOperationException(
                    $@"Рассчитанные группы пользователя-администратора аккаунта c id={createAccountAndServiceCommand.AccountAdminId} равны 
                            {string.Join(",", listOfActualGroups.ToArray())} 
                            и не соответствуют ожидаемым {string.Join(",", listOfExpectedGroups.ToArray())}");
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IHttpContextAccessor, FakeHttpContextAccessor>();
            serviceCollection.AddTransient<ApiAccessProvider>();
            serviceCollection.AddTransient<IUserPrincipalHelper, UserPrincipalHelper>();
        }
    }
}