﻿using Clouds42.BLL.Common.Access;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccessProviders
{
    public class AccountTestAccessProvider(
        CreateAccountCommand createAccountCommand,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : FakeAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        protected override string GetUserIdentity()
        {
            return createAccountCommand.AccountId.ToString();
        }

        public override IUserPrincipalDto GetUser()
        {
            return new AccountUserPrincipalDto
            {
                Token = createAccountCommand.Token,
                ContextAccountId = createAccountCommand.AccountId,
                Id = createAccountCommand.AccountAdminId,
                Name = GetUserIdentity(),
                Groups = createAccountCommand.Groups
            };
        }

        public override string GetUserAgent()
        {
            return null;
        }

        public override string GetUserHostAddress()
        {
            return null;
        }

        public override string Name => GetUserIdentity();
    }
}
