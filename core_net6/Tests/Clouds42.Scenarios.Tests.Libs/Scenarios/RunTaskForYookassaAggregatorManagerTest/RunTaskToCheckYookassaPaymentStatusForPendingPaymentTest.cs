﻿using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RunTaskForYookassaAggregatorManagerTest
{
    /// <summary>
    /// Сценарий теста запуска задачи по проверке
    /// статуса платежа агрегатора ЮKassa, для платежа со статусом Pending
    ///
    /// 1) Создаем аккаунт #CreateAccount
    ///
    /// 2) Создаем платеж Юкассы со статусом Pending #CreateYookassaPayment
    ///
    /// 3) Вызываем метод, который должен запустить задачу по проверке статуса #RunTaskToCheckYookassaPaymentStatus
    ///
    /// 4) Проверяем что создалась задача и проверяем параметры задачи #CheckCoreWorkerTasksQueue
    /// 
    /// </summary>
    public class RunTaskToCheckYookassaPaymentStatusForPendingPaymentTest : ScenarioBase
    {
        private readonly RunTaskForYookassaAggregatorManager _runTaskForYookassaAgregatorManager;

        public RunTaskToCheckYookassaPaymentStatusForPendingPaymentTest()
        {
            _runTaskForYookassaAgregatorManager =
                TestContext.ServiceProvider.GetRequiredService<RunTaskForYookassaAggregatorManager>();
        }

        public override void Run()
        {
            #region CreateAccount

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            createAccountCommand.Run();

            #endregion

            #region CreateYookassaPayment

            var createYookassaAgregatorPaymentCommand =
                new CreateYookassaPaymentCommand(TestContext, createAccountCommand.AccountId);
            createYookassaAgregatorPaymentCommand.Run();

            #endregion

            #region RunTaskToCheckYookassaPaymentStatus

            var managerResult = _runTaskForYookassaAgregatorManager.RunTaskToCheckYookassaPaymentStatus(
                createYookassaAgregatorPaymentCommand.Payment.Id);

            Assert.IsFalse(managerResult.Error);

            #endregion

            #region CheckCoreWorkerTasksQueue

            var coreWorkerTasksQueue = GetCoreWorkerTasksQueue(nameof(FinalizeYookassaPaymentJob));
            Assert.IsNotNull(coreWorkerTasksQueue);

            var taskParams = GetCoreWorkerTaskParametersModel<FinalizeYookassaPaymentJobParamsDto>(coreWorkerTasksQueue.Id);

            Assert.AreEqual(taskParams.YookassaAgregatorPaymentId.ToString(),
                createYookassaAgregatorPaymentCommand.Payment.PaymentSystemTransactionId);

            #endregion
        }

        protected override void RegisterTestServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IGetYookassaPaymentObjectInfoProvider, GetPendingYookassaPaymentObjectInfoProviderFake>();
        }
    }
}
