﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Enums;
using Clouds42.Segment.CloudsServicesTerminalFarm.Managers;
using Core42.Application.Features.TerminalFarmContext.Dtos;
using Core42.Application.Features.TerminalFarmContext.Queries;
using Core42.Application.Features.TerminalFarmContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.TerminalFarm
{
    /// <summary>
    /// Тест выборки ферм ТС по фильтру
    /// Сценарий:
    ///         1. Создаем 3 фермы ТС
    ///         2. Делаем выборку по фильтрам, проверяем, что возвращается корректное количество записей
    /// </summary>
    public class TerminalFarmSamplingTest : ScenarioBase
    {
        private readonly TerminalFarmManager _terminalFarmManager;

        public TerminalFarmSamplingTest()
        {
            _terminalFarmManager = TestContext.ServiceProvider.GetRequiredService<TerminalFarmManager>();
        }

        public override void Run()
        {
            var firstTerminalFarmDto = new CloudTerminalFarmDto
            {
                Name = "Name",
                Description = "Desc",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}"
            };
            CreateTerminalFarm(firstTerminalFarmDto);

            var secondTerminalFarmDto = new CloudTerminalFarmDto
            {
                Name = "OOOO",
                Description = "Dsc",
                ConnectionAddress = $"con_{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}"
            };
            CreateTerminalFarm(secondTerminalFarmDto);

            var thirdTerminalFarmDto = new CloudTerminalFarmDto
            {
                Name = "st_Name",
                Description = "Desc",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}"
            };
            CreateTerminalFarm(thirdTerminalFarmDto);

            var filter = new CloudServerFilterDto
            {
                Name = "Name",
                PageNumber = 1
            };
            GetFilteredTerminalFarms(2, filter, GetData);

            filter.Name = "O";
            GetFilteredTerminalFarms(1, filter, GetData);

            filter.Name = null;
            filter.ConnectionAddress = "con";
            GetFilteredTerminalFarms(1, filter, GetData);

            filter.ConnectionAddress = null;
            filter.Description = "desc";
            GetFilteredTerminalFarms(2, filter, GetData);

            filter.Description = "dsc";
            filter.Name = "Name";
            GetFilteredTerminalFarms(0, filter, GetData);

            filter.Name = "OOOO";
            GetFilteredTerminalFarms(1, filter, GetData);
        }

        /// <summary>
        /// Добавить новую ферму ТС
        /// </summary>
        /// <returns>Id фермы ТС</returns>
        private void CreateTerminalFarm(CloudTerminalFarmDto terminalFarmDto)
        {
            var createResult = _terminalFarmManager.AddNewTerminalFarm(terminalFarmDto);
            Assert.IsFalse(createResult.Error, createResult.Message);
        }

        /// <summary>
        /// Получить данные ферм ТС по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество ферм ТС</param>
        /// <param name="filter">Фильтр</param>
        /// <param name="action">Действие, для выбора и проверки выборки ферм ТС</param>
        private void GetFilteredTerminalFarms(int expectedCount, CloudServerFilterDto filter,
            Action<CloudServerFilterDto, int> action)
        {
            action(filter, expectedCount);
        }

        /// <summary>
        /// Получить данные терминальных шлюзов по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <param name="expectedCount">Ожидаемое количество терминальных шлюзов<</param>
        private void GetData(CloudServerFilterDto filter, int expectedCount)
        {
            var enterpriseServers = Mediator.Send(new GetFilteredTerminalFarmsQuery
            {
                Filter = new TerminalFarmFilter
                {
                    ConnectionAddress = filter.ConnectionAddress,
                    Description = filter.Description,
                    Name = filter.Name,
                },
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize,
                OrderBy = string.IsNullOrEmpty(filter.SortFieldName) && !filter.SortType.HasValue ? $"{nameof(TerminalFarmDto.Name)}.asc" : $"{filter.SortFieldName}.{(filter.SortType == SortType.Asc ? "asc" : "desc")}"
            }).Result;
            Assert.IsFalse(enterpriseServers.Error);
            Assert.AreEqual(expectedCount, enterpriseServers.Result.Records.Count);
        }
    }
}
