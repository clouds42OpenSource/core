﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesTerminalFarm.Managers;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.TerminalFarm
{
    /// <summary>
    /// Тест создания/удаления/редактирования фермы ТС
    /// Сценарий:
    ///         1. Создаем ферму ТС, проверяем, что создание прошло без ошибок 
    ///         2. Редактируем ферму ТС, проверяем, что редактирование прошло без ошибок 
    ///         3. Удаляем ферму ТС, проверяем, что удаление прошло без ошибок 
    /// </summary>
    public class CrudTerminalFarmTest : ScenarioBase
    {
        private readonly TerminalFarmManager _terminalFarmManager;

        public CrudTerminalFarmTest()
        {
            _terminalFarmManager = TestContext.ServiceProvider.GetRequiredService<TerminalFarmManager>();
        }

        public override void Run()
        {
            var terminalFarmId = CreateTerminalFarm();
            EditTerminalFarm(terminalFarmId);
            DeleteTerminalFarm(terminalFarmId);
        }

        /// <summary>
        /// Добавить новую ферму ТС
        /// </summary>
        /// <returns>Id фермы ТС</returns>
        private Guid CreateTerminalFarm()
        {
            var terminalFarmDto = new CloudTerminalFarmDto
            {
                Name = "Name",
                Description = "Desc",
                ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}"
            };
            var createResult = _terminalFarmManager.AddNewTerminalFarm(terminalFarmDto);
            Assert.IsFalse(createResult.Error, createResult.Message);

            return createResult.Result;
        }

        /// <summary>
        /// Редактировать ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">Id фермы ТС</param>
        private void EditTerminalFarm(Guid terminalFarmId)
        {
            var cloudContentServer = _terminalFarmManager.GetTerminalFarmDto(terminalFarmId);
            Assert.IsFalse(cloudContentServer.Error, cloudContentServer.Message);

            var terminalFarmDto = cloudContentServer.Result;

            terminalFarmDto.Name = "New Name";
            terminalFarmDto.Description = "Description";
            terminalFarmDto.ConnectionAddress = $"{DateTime.Now:hh}.{DateTime.Now:mm}.{DateTime.Now:ss}.{DateTime.Now:fff}";

            var editTerminalFarm = _terminalFarmManager.EditTerminalFarm(terminalFarmDto);
            Assert.IsFalse(editTerminalFarm.Error, editTerminalFarm.Message);

            var editedTerminalFarm = _terminalFarmManager.GetTerminalFarmDto(terminalFarmId);
            Assert.IsFalse(editedTerminalFarm.Error, editedTerminalFarm.Message);

            var editedTerminalFarmDto = editedTerminalFarm.Result;
            Assert.AreEqual(terminalFarmDto.Name, editedTerminalFarmDto.Name);
            Assert.AreEqual(terminalFarmDto.Description, editedTerminalFarmDto.Description);
            Assert.AreEqual(terminalFarmDto.ConnectionAddress, editedTerminalFarmDto.ConnectionAddress);
        }

        /// <summary>
        /// Удалить ферму ТС
        /// </summary>
        /// <param name="terminalFarmId">Id фермы ТС</param>
        private void DeleteTerminalFarm(Guid terminalFarmId)
        {
            var deleteTerminalFarm = _terminalFarmManager.DeleteTerminalFarm(terminalFarmId);
            Assert.IsFalse(deleteTerminalFarm.Error, deleteTerminalFarm.Message);
        }
    }
}
