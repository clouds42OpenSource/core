﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Core42.Application.Contracts.Features.ResourcesContext.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.RecalculateServiceCostAfterChangesTest
{
    /// <summary>
    /// Сценарий теста по проверке пересчета
    /// стоимости сервиса для аккаунтов после изменений
    ///
    /// 1) Создаем активный сервис
    /// 2) Создаем три аккаунта и подключаем им сервис. Одному аккаунту делаем просроченную Аренду 1С
    /// 3) Редактируем стоимость услуг сервиса и подтверждаем заявку
    /// 4) Запускаем процесс пересчета и проверяем после:
    ///     1) Стоимость ресурсов по каждой услуге, для каждого аккаунта
    ///     2) Стоимость конфигурации ресурса для каждого аккаунта
    ///     3) Наличие тарифов на аккаунт. Они должны почиститься
    /// </summary>
    public class ScenarioCheckRecalculateServiceCostAfterChangesTest : ScenarioBase
    {
        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override async Task RunAsync()
        {
            var createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();
            var moderationResultManager = ServiceProvider.GetRequiredService<ModerationResultManager>();

            var billingServiceTypes = createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(3);
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                serviceOwnerAccountId => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration,
                    BillingServiceTypes = billingServiceTypes
                });
            createCustomService.Run();

            var account = CreateNewAccountForService(createCustomService.Id, $"test{DateTime.Now:mmssfff}");
            var secondAccount = CreateNewAccountForService(createCustomService.Id, $"test2{DateTime.Now:mmssfff}");
            var thirdAccount = CreateNewAccountForService(createCustomService.Id, $"test3{DateTime.Now:mmssfff}");

            var resConf1C =
                GetResourcesConfigurationOrThrowException(thirdAccount.AccountId,
                    GetRent1CServiceOrThrowException().Id);

            resConf1C.ExpireDate = DateTime.Now.AddMonths(-4);
            DbLayer.ResourceConfigurationRepository.Update(resConf1C);
            await DbLayer.SaveAsync();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(createCustomService.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();

            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            var serviceTypeOne = editBillingServiceDto.BillingServiceTypes[0];
            var serviceTypeTwo = editBillingServiceDto.BillingServiceTypes[1];
            var serviceTypeThree = editBillingServiceDto.BillingServiceTypes[2];
            serviceTypeOne.ServiceTypeCost = 20;
            serviceTypeTwo.ServiceTypeCost = 10;

            var editBillingServiceManagerResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editBillingServiceManagerResult.Error);

            var editServiceRequest =
                await DbLayer.GetGenericRepository<EditServiceRequest>().FirstOrDefaultAsync(csr =>
                    csr.BillingServiceId == createCustomService.Id &&
                    csr.Status == ChangeRequestStatusEnum.OnModeration);
            Assert.IsNotNull(editServiceRequest);

            var changeServiceRequestModerationResultDto = new ChangeServiceRequestModerationResultDto
            {
                Id = editServiceRequest.Id,
                ModeratorComment = "тестирование",
                Status = ChangeRequestStatusEnum.Implemented
            };

            var managerResult = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResultDto);
            Assert.IsFalse(managerResult.Error);

            RefreshDbCashContext(Context.AccountRates);

            await Mediator.Send(new RecalculateResourcesCostForAccountsCommand { AccountsIds =
                [account.AccountId, secondAccount.AccountId, thirdAccount.AccountId], BillingServiceChangesId = 
                editServiceRequest.BillingServiceChangesId });

            RefreshDbCashContext(TestContext.Context.Resources);
            CheckResourcesCostForServiceType(serviceTypeOne.Id, account.AccountId, serviceTypeOne.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeOne.Id, secondAccount.AccountId, serviceTypeOne.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeOne.Id, thirdAccount.AccountId, serviceTypeOne.ServiceTypeCost);

            CheckResourcesCostForServiceType(serviceTypeTwo.Id, account.AccountId, serviceTypeTwo.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeTwo.Id, secondAccount.AccountId, serviceTypeTwo.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeTwo.Id, thirdAccount.AccountId, serviceTypeTwo.ServiceTypeCost);

            CheckResourcesCostForServiceType(serviceTypeThree.Id, account.AccountId, serviceTypeThree.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeThree.Id, secondAccount.AccountId, serviceTypeThree.ServiceTypeCost);
            CheckResourcesCostForServiceType(serviceTypeThree.Id, thirdAccount.AccountId, serviceTypeThree.ServiceTypeCost);

            var totalCost = serviceTypeOne.ServiceTypeCost + serviceTypeTwo.ServiceTypeCost +
                            serviceTypeThree.ServiceTypeCost;

            CheckResourcesConfigurationCost(editServiceRequest.BillingServiceId, account.AccountId, totalCost);
            CheckResourcesConfigurationCost(editServiceRequest.BillingServiceId, secondAccount.AccountId, totalCost);
            CheckResourcesConfigurationCost(editServiceRequest.BillingServiceId, thirdAccount.AccountId, totalCost);

            CheckWhatIsDeletedAccountRate(account.AccountId, serviceTypeOne.Id);
            CheckWhatIsDeletedAccountRate(account.AccountId, serviceTypeTwo.Id);
            CheckWhatIsDeletedAccountRate(account.AccountId, serviceTypeThree.Id);

            CheckWhatIsDeletedAccountRate(secondAccount.AccountId, serviceTypeOne.Id);
            CheckWhatIsDeletedAccountRate(secondAccount.AccountId, serviceTypeTwo.Id);
            CheckWhatIsDeletedAccountRate(secondAccount.AccountId, serviceTypeThree.Id);

            CheckWhatIsDeletedAccountRate(thirdAccount.AccountId, serviceTypeOne.Id);
            CheckWhatIsDeletedAccountRate(thirdAccount.AccountId, serviceTypeTwo.Id);
            CheckWhatIsDeletedAccountRate(thirdAccount.AccountId, serviceTypeThree.Id);
        }

        /// <summary>
        /// Проверить стоимость конфигурации ресурса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Стоимость</param>
        private void CheckResourcesConfigurationCost(Guid serviceId, Guid accountId, decimal cost)
        {
            var resConf = GetResourcesConfigurationOrThrowException(accountId, serviceId);

            Assert.AreEqual(resConf.Cost, cost);
        }

        /// <summary>
        /// Проверить стоимость ресурсов для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="cost">Стоимость</param>
        private void CheckResourcesCostForServiceType(Guid serviceTypeId, Guid accountId, decimal cost)
        {
            if (cost <= 0) throw new ArgumentOutOfRangeException(nameof(cost));
            var resources = GetResources(accountId, serviceTypeId);

            if (resources.Any(r => r.Cost != cost))
                throw new InvalidOperationException($"Не верно пересчитались ресурсы для услуги {serviceTypeId}");

        }

        /// <summary>
        /// Создать новый аккаунт для сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="login">Логин</param>
        /// <returns>Комманда которая создала сервис</returns>
        private CreateAccountCommand CreateNewAccountForService(Guid serviceId, string login)
        {
            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudServiceId = serviceId
                    },
                    Login = login
                });
            createAccountCommand.Run();

            return createAccountCommand;
        }

        /// <summary>
        /// Проверить что курс на аккаунт удален
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        private void CheckWhatIsDeletedAccountRate(Guid accountId, Guid serviceTypeId)
        {
            var accountRate = DbLayer.AccountRateRepository.FirstOrDefault(ar =>
                ar.AccountId == accountId && ar.BillingServiceTypeId == serviceTypeId);

            Assert.IsNull(accountRate);
        }
    }
}
