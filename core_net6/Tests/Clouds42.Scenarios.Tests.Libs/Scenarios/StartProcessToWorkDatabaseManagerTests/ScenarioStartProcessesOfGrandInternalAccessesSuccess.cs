﻿using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests
{
    /// <summary>
    /// Сценарий для проверки успешного предоставления доступа пользователям в инф. базу
    /// Действия:
    /// 1. Создадим аккаунт, пользователей, нужный ресурс и инф. базу. [EntitiesCreation]
    /// 2. Проверим, что доступы будут выданы для пользователей в инф. базу. [CheckAcDbAccessWereCreated]
    /// 3. Проверим, что доступы будут выданы для пользователя в инф. базу на разделителях и процесс будет запущен. [CheckAcDbAccessWereCreatedForDbOnDelimiters]
    /// </summary>
    public class ScenarioStartProcessesOfGrandInternalAccessesSuccess : ScenarioBase
    {
        private readonly StartProcessToWorkDatabaseManager _startProcessToWorkDatabaseManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioStartProcessesOfGrandInternalAccessesSuccess()
        {
            _startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId)[0];
            var userOneId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userTwoId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userThreeId = _accountUserTestHelper.CreateAccountUser(accountId);

            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userOneId, accountId);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userTwoId, accountId);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userThreeId, accountId);

            #endregion

            #region CheckAcDbAccessWereCreated

            var model = new StartProcessesOfManageAccessesToDbDto
            {
                DatabaseId = databaseId,
                UsersId = [userOneId, userTwoId]
            };

            var result = _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if(!result.Error)
                _acDbAccessTestHelper.CheckAcDbAccessesCreated(model.UsersId, model.DatabaseId, accountId);

            #endregion

            #region CheckAcDbAccessWereCreatedForDbOnDelimiters

            _accountDatabasesTestHelper.CreateDbOnDelimiters(databaseId, "unf");
            model.UsersId = [userThreeId];

            _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            _acDbAccessTestHelper.CheckAcDbAccessesCreated(model.UsersId, model.DatabaseId, accountId);

            #endregion
        }
    }
}
