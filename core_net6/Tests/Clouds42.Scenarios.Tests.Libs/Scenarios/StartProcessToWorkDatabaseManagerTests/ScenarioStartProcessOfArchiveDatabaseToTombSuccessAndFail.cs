﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.DataHelpers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests
{
    /// <summary>
    /// Сценарий для успешного и неуспешного запуска процесса архивации инф. базы.
    /// Действия:
    /// 1. Создадим аккаунты и его базы. [AccountAndDbsCreation]
    /// 2. Проверим, что процесс архивации будет запущен для валидных данных. [CheckProcessStartForDatabase]
    /// 3. Проверим, что процесс архивации не будет запущен для баз в статусах DetachingToTomb, DelitingToTomb, DetachedToTomb. [CheckProcessDoesntStartWithNotValidDbStatus]
    /// 4. Проверим, что процесс архивации не будет запущен для баз, директории которых не существуют. [CheckProcessDoesntStartWithEmptyDbDirectory]
    /// 5. Создадим базу на разделителях с пустым значением зоны и проверим что процесс архивации для такой базы не будет запущен. [CheckProcessDesntStartWithEmptyZoneOnDelimiters]
    /// 6. Проверим, что процесс архивации инф. баз будет запущен при валидных данных. [CheckProcessStartForDatabases]
    /// 7. Проверим, что процесс архивации инф. баз не будет запущен при пустых значениях Id баз. [CheckProcessDoesntStartForNotValidDatabases]
    /// </summary>
    public class ScenarioStartProcessOfArchiveDatabaseToTombSuccessAndFail : ScenarioBase
    {

        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly StartProcessToWorkDatabaseManager _processToWorkDatabaseManager;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioStartProcessOfArchiveDatabaseToTombSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _processToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
            _accountDatabasePathHelper = TestContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }
        public override void Run()
        {
            #region AccountAndDbsCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var databasesId = _accountDatabasesTestHelper.CreateAccountDatabases(createAccountCommand.AccountId, 8);

            #endregion

            #region CheckProcessStartForDatabase

            var databaseToTombParams = new StartProcessOfArchiveDatabaseToTombParamsDto
            {
                DatabaseId = databasesId[0],
                NeedForceFileUpload = true,
                NeedNotifyHotlineAboutFailure = false,
                Trigger = CreateBackupAccountDatabaseTrigger.ManualRemoval
            };

            _processToWorkDatabaseManager.StartProcessOfArchiveDatabaseToTomb(databaseToTombParams);
            _databaseProcessTestHelper.CheckProcessOfDatabaseStarted(databaseToTombParams.DatabaseId, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);

            #endregion

            #region CheckProcessDoesntStartWithNotValidDbStatus

            databaseToTombParams.DatabaseId = databasesId[1];
            StartProcessOfArchiveDatabaseToTombWithNotValidDbState(databaseToTombParams, DatabaseState.DetachingToTomb);

            databaseToTombParams.DatabaseId = databasesId[2];
            StartProcessOfArchiveDatabaseToTombWithNotValidDbState(databaseToTombParams, DatabaseState.DelitingToTomb);

            databaseToTombParams.DatabaseId = databasesId[3];
            StartProcessOfArchiveDatabaseToTombWithNotValidDbState(databaseToTombParams, DatabaseState.DetachedToTomb);

            #endregion

            #region CheckProcessDoesntStartWithEmptyDbDirectory

            databaseToTombParams.DatabaseId = databasesId[4];
            var pathToDb = _accountDatabasePathHelper.GetPath(databaseToTombParams.DatabaseId);
            Directory.Delete(pathToDb, true);
            _processToWorkDatabaseManager.StartProcessOfArchiveDatabaseToTomb(databaseToTombParams);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(databaseToTombParams.DatabaseId, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);

            #endregion

            #region CheckProcessDesntStartWithEmptyZoneOnDelimiters

            databaseToTombParams.DatabaseId = databasesId[5];
            var configurationId = ConfigurationsIdTestConstants.Unf;
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(_createDbTemplateTestHelper.GetDbTemplateTestName(1), configurationId);

            var model = new AccountDatabaseOnDelimiters
            {
                AccountDatabaseId = databaseToTombParams.DatabaseId,
                LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                DbTemplateDelimiterCode = configurationId
            };

            DbLayer.AccountDatabaseDelimitersRepository.Insert(model);

            _processToWorkDatabaseManager.StartProcessOfArchiveDatabaseToTomb(databaseToTombParams);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(databaseToTombParams.DatabaseId, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);

            #endregion

            #region CheckProcessStartForDatabases

            var databasesIdToStartProcess = databasesId.GetRange(6, 2);

            _processToWorkDatabaseManager.StartProcessOfArchiveDatabasesToTomb(databasesIdToStartProcess);
            _databaseProcessTestHelper.CheckProcessesOfDatabasesStarted(databasesIdToStartProcess, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);

            #endregion

            #region CheckProcessDoesntStartForNotValidDatabases

            var databasesIdNotToStartProcess = new List<Guid> {Guid.NewGuid(), Guid.NewGuid()};
            _processToWorkDatabaseManager.StartProcessOfArchiveDatabasesToTomb(databasesIdNotToStartProcess);
            _databaseProcessTestHelper.CheckProcessesOfDatabasesNotStarted(databasesIdNotToStartProcess, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);

            #endregion
        }

        /// <summary>
        /// Начать процесс архивации инф. базы с невалидным статусом инф. базы
        /// </summary>
        /// <param name="databaseToTombParams"></param>
        /// <param name="databaseState"></param>
        private void StartProcessOfArchiveDatabaseToTombWithNotValidDbState(
            StartProcessOfArchiveDatabaseToTombParamsDto databaseToTombParams, DatabaseState databaseState)
        {
            ChangeDbStateAndRefreshContext(databaseToTombParams.DatabaseId, databaseState);
            _processToWorkDatabaseManager.StartProcessOfArchiveDatabaseToTomb(databaseToTombParams);
            _databaseProcessTestHelper.CheckProcessOfDatabaseNotStarted(databaseToTombParams.DatabaseId, ProcessesNameConstantsTest.ArchivingDatabaseProcessName);
        }

        /// <summary>
        /// Обновить статус инф. базы и рефрешнуть контекст
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseState">Новый статус инф. базы</param>
        private void ChangeDbStateAndRefreshContext(Guid databaseId, DatabaseState databaseState)
        {
            var result = _accountDatabasesTestHelper.ChangeDatabaseState(databaseId, DatabaseState.DetachingToTomb);
            if (!result)
                throw new InvalidOperationException($"Не удалось изменить статус базы {databaseId} на {databaseState}");

            RefreshDbCashContext(Context.AccountDatabases.ToList());
        }
    }
}
