﻿using System;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests
{
    /// <summary>
    /// Сценарий для проверки успешного и неуспешного запуска процесса удаления инф. баз
    /// Действия:
    /// 1. Создадим аккаунт и его инф. базы. [AccountAndDbsCreation]
    /// 2. Запустим процесс удаления одной из баз и проверим, что создалась задача с правильными параметрами. [CheckProcessOfRemoveDatabaseStarted]
    /// 3. Запустим процесс удаления одной базы с невалидным Id базы и проверим, что метод выбросит исключение. [CheckProcessOfRemoveNotValidDatabaseNotStarted]
    /// 4. Запустим процесс для удаления двух баз и проверим, что создались две задачи с правильными параметрами. CheckProcessOfRemoveDatabasesStarted
    /// 5. Запустим процесс удалениях двух баз с невалидными Id баз и проверим, что метод выбросит исключение. [CheckProcessOfRemoveNotValidDatabasesNotStarted]
    /// </summary>
    public class ScenarioStartProcessOfRemoveDatabaseSuccessAndFail : ScenarioBase
    {
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly StartProcessToWorkDatabaseManager _processToWorkDatabaseManager;
        private readonly DatabaseProcessTestHelper _databaseProcessTestHelper;

        public ScenarioStartProcessOfRemoveDatabaseSuccessAndFail()
        {
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _processToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            _databaseProcessTestHelper = new DatabaseProcessTestHelper(TestContext);
        }
        public override void Run()
        {
            #region AccountAndDbsCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var databasesId = _accountDatabasesTestHelper.CreateAccountDatabases(createAccountCommand.AccountId, 3);

            #endregion

            #region CheckProcessOfRemoveDatabaseStarted

            _processToWorkDatabaseManager.StartProcessOfRemoveDatabase(databasesId[0]);
            _databaseProcessTestHelper.CheckProcessOfDatabaseStarted(databasesId[0], ProcessesNameConstantsTest.RemovingDatabaseProcessName);

            #endregion

            #region CheckProcessOfRemoveNotValidDatabaseNotStarted

            var result = _processToWorkDatabaseManager.StartProcessOfRemoveDatabase(Guid.NewGuid());
            if (!result.Error)
                throw new InvalidOperationException("Процесс удаления инф. базы успешно запущен при невалидных данных");

            #endregion

            #region CheckProcessOfRemoveDatabasesStarted

            var databasesToRemove = databasesId.GetRange(1, 2);
            _processToWorkDatabaseManager.StartProcessOfRemoveDatabases(databasesToRemove);
            _databaseProcessTestHelper.CheckProcessesOfDatabasesStarted(databasesToRemove, ProcessesNameConstantsTest.RemovingDatabaseProcessName);

            #endregion

            #region CheckProcessOfRemoveNotValidDatabasesNotStarted

            result = _processToWorkDatabaseManager.StartProcessOfRemoveDatabases([Guid.NewGuid(), Guid.NewGuid()]);
            if (!result.Error)
                throw new InvalidOperationException("Процесс удаления инф. баз успешно запущен при невалидных данных");

            #endregion

        }
    }
}
