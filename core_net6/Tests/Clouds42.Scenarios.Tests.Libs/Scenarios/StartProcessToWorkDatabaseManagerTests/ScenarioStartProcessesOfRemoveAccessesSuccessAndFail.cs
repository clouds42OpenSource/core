﻿using System;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests
{
    /// <summary>
    /// Сценарий для проверки успешного удаления доступа пользователям в инф. базу
    /// Действия:
    /// 1. Создадим аккаунт, пользователей, доступы и инф. базу. [EntitiesCreation]
    /// 2. Проверим, что доступы будут удалены для пользователей в инф. базу. [CheckAcDbAccessWereDeleted]
    /// 3. Проверим, что доступы процесс на удаление доступов пользователей в инф. базу будет запущен.[CheckAcDbAccessDeletionProccessWasStarted]
    /// 4. Проверим, что доступ не будет удален для несуществующего Id инф. базы. [CheckAcDbAccessWasNotDeletedForEmptyDbId]
    /// 5. Проверим, что доступ не будет удален для несуществуюещго Id пользователя. [CheckAcDbAccessWasNotDeletedForEmptyUserId]
    /// 6. Проверим, что доступ не будет удален в базе на разделителях и процесс выдачи не будет запущен, если доступ выдан ранее. [CheckProccessOfDeletionAccessWasNotStartedOnDelimitersIfAlreadyExists]
    /// </summary>
    public class ScenarioStartProcessesOfRemoveAccessesSuccessAndFail : ScenarioBase
    {
        private readonly StartProcessToWorkDatabaseManager _startProcessToWorkDatabaseManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioStartProcessesOfRemoveAccessesSuccessAndFail()
        {
            _startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }

        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId, 2)[0];
            var userOneId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userTwoId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userThreeId = _accountUserTestHelper.CreateAccountUser(accountId);


            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userOneId);
            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userTwoId);

            #endregion

            #region CheckAcDbAccessWereDeleted

            var model = new StartProcessesOfManageAccessesToDbDto
            {
                DatabaseId = databaseId,
                UsersId = [userOneId, userTwoId]
            };

            var result = _startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(model);
            if (!result.Error)
                _acDbAccessTestHelper.CheckAcDbAccessesDeleted(model.UsersId, model.DatabaseId, accountId);

            #endregion

            #region CheckAcDbAccessWereDeletedForDbOnDelimiters

            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userThreeId);

            _accountDatabasesTestHelper.CreateDbOnDelimiters(databaseId, "unf");
            model.UsersId = [userThreeId];

            _startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(model);

            #endregion

            #region CheckAcDbAccessWasNotDeletedForEmptyDbId

            model.DatabaseId = Guid.NewGuid();
            result = _startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(model);
            if (!result.Error)
                throw new InvalidOperationException("Выдался доступ пользователю для несуществующей инф. базы");

            #endregion

            #region CheckAcDbAccessWasNotDeletedForEmptyUserId

            model.DatabaseId = databaseId;
            model.UsersId = [Guid.NewGuid()];
            result = _startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(model);
            if (result.Result[0].Success)
                throw new InvalidOperationException("Выдался доступ несуществующему пользователю в инф. базу");

            #endregion

            #region CheckProccessOfDeletionAccessWasNotStartedOnDelimitersIfAlreadyExists

            var userFourId = _accountUserTestHelper.CreateAccountUser(accountId);
            var acDbAccess = _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userFourId);
            _acDbAccessTestHelper.CreateDbOnDelimitersAccess(acDbAccess.ID, AccountDatabaseAccessState.ProcessingDelete);

            result = _startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(model);
            if (result.Result[0].Success)
                throw new InvalidOperationException("Начался процесс удаления доступа, когда уже есть заявка на удаление");

            #endregion
        }
    }
}
