﻿using System;
using System.Linq;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.Enums;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.StartProcessToWorkDatabaseManagerTests
{
    /// <summary>
    /// Сценарий для неуспешного создания доступов пользователя в инф. базу
    /// Действия:
    /// 1. Создадим аккаунт, инф. базу и пользователя с ресурсом. [EntitiesCreation]
    /// 2. Проверим, что доступ не будет выдан снова, если доступ был выдан ранее. [CheckAcDbAccessWasNotGrantedIfAlreadyExists]
    /// 3. Проверим, что доступ не будет выдан для несуществующего Id инф. базы. [CheckAcDbAccessWasNotGrantedForEmptyDbId]
    /// 4. Проверим, что доступ не будет выдан для несуществуюещго Id пользователя. [CheckAcDbAccessWasNotGrantedForEmptyUserId]
    /// 5. Проверим, что доступ не будет выдан в базу в неподходящем статусе инф. базы. [CheckAcDbAccessWasNotGrantedForNotValidDbStatus]
    /// 6. Проверим, что доступ не будет выдан в базу на разделителях и процесс выдачи не будет запущен, если доступ выдан ранее. [CheckAcDbAccessWasNotGrantedIfAlreadyExistsOnDelimiters]
    /// </summary>
    public class ScenarioStartProcessesOfGrandInternalAccessesFail : ScenarioBase
    {
        private readonly StartProcessToWorkDatabaseManager _startProcessToWorkDatabaseManager;
        private readonly AccountDatabasesTestHelper _accountDatabasesTestHelper;
        private readonly AccountUserTestHelper _accountUserTestHelper;
        private readonly AcDbAccessTestHelper _acDbAccessTestHelper;

        public ScenarioStartProcessesOfGrandInternalAccessesFail()
        {
            _startProcessToWorkDatabaseManager = TestContext.ServiceProvider.GetRequiredService<StartProcessToWorkDatabaseManager>();
            _accountDatabasesTestHelper = new AccountDatabasesTestHelper(TestContext);
            _accountUserTestHelper = new AccountUserTestHelper(TestContext);
            _acDbAccessTestHelper = TestContext.ServiceProvider.GetRequiredService<AcDbAccessTestHelper>();
        }
        public override void Run()
        {
            #region EntitiesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var accountId = createAccountCommand.AccountId;

            var databaseId = _accountDatabasesTestHelper.CreateAccountDatabases(accountId)[0];
            var userOneId = _accountUserTestHelper.CreateAccountUser(accountId);
            var userTwoId = _accountUserTestHelper.CreateAccountUser(accountId);

            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userOneId, accountId);
            _accountUserTestHelper.CreateMyEnterpriseResourceForUser(userTwoId, accountId);

            _acDbAccessTestHelper.CreateAcDbAccess(databaseId, accountId, Guid.NewGuid(), userOneId);

            #endregion

            #region CheckAcDbAccessWasNotGrantedIfAlreadyExists

            var model = new StartProcessesOfManageAccessesToDbDto
            {
                DatabaseId = databaseId,
                UsersId = [userOneId]
            };

             _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);

            _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if (DbLayer.AcDbAccessesRepository.Where(acDb=> acDb.AccountDatabaseID == databaseId && acDb.AccountUserID == userOneId).Count() > 1)
                throw new InvalidOperationException("Выдался доступ пользователю, у которого уже был доступ");

            #endregion

            #region CheckAcDbAccessWasNotGrantedForEmptyDbId

            model.DatabaseId = Guid.NewGuid();
            var result = _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if (!result.Error)
                throw new InvalidOperationException("Выдался доступ пользователю для несуществующей инф. базы");

            #endregion

            #region CheckAcDbAccessWasNotGrantedForEmptyUserId

            model.DatabaseId = databaseId;
            model.UsersId = [Guid.NewGuid()];
            result = _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if (result.Result.Count > 0)
                throw new InvalidOperationException("Выдался доступ несуществующему пользователю в инф. базу");

            #endregion

            #region CheckAcDbAccessWasNotGrantedForNotValidDbStatus

            model.UsersId = [userTwoId];

            _accountDatabasesTestHelper.ChangeDatabaseState(databaseId, DatabaseState.DeletedFromCloud);
            RefreshDbCashContext(Context.AccountDatabases.ToList());

            result = _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if (result.Result[0].Success)
                throw new InvalidOperationException("Выдался доступ для пользователя в удаленную инф. базу");

            _accountDatabasesTestHelper.ChangeDatabaseState(databaseId, DatabaseState.DetachedToTomb);
            RefreshDbCashContext(Context.AccountDatabases.ToList());

            result = _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);
            if (result.Result[0].Success)
                throw new InvalidOperationException("Выдался доступ для пользователя в архивную инф. базу");

            #endregion

            #region CheckAcDbAccessWasNotGrantedIfAlreadyExistsOnDelimiters

            _accountDatabasesTestHelper.CreateDbOnDelimiters(databaseId, "unf");

            model.UsersId = [userOneId];

            _startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(model);

            #endregion
        }
    }
}
