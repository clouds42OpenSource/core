﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.CloudServices.Fasta.Helpers;
using Clouds42.CloudServices.Fasta.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeFastaDemoPeriodTest
{
    /// <summary>
    /// Сценарий теста на не успешное изменение демо периода
    /// для сервиса Fasta
    ///
    /// 1) Создаем аккаунт и инициализируем все необходимые данные для теста #CreateAccount
    /// 2) Пытаемся поменть демо период, когда дата меньше допустимой #MinDate
    /// 3) Пытаемся поменть демо период, когда указан не вырный аккаунт #Account
    /// 4) Пытаемся поменть демо период, когда аккаунт не демо #NotDemo
    /// </summary>
    public class ScenarioChangeFastaDemoPeriodErrorTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount
            var fastaServiceManager = ServiceProvider.GetRequiredService<FastaServiceManager>();
            var fastaServiceHelper = ServiceProvider.GetRequiredService<FastaServiceHelper>();
            var createPaymentManager = ServiceProvider.GetRequiredService<CreatePaymentManager>();

            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.Esdl

                    }
                });
            createAccountCommand.Run();

            var minDate = DateTime.Now;
            var maxDate = fastaServiceHelper.GetMaxDemoExpiredDate(createAccountCommand.AccountId);

            var manageFastaDemoPeriodDto = fastaServiceManager.GetManageFastaDemoPeriodDto().Result;
            Assert.IsNotNull(manageFastaDemoPeriodDto);
            #endregion

            #region MinDate
            manageFastaDemoPeriodDto.DemoExpiredDate = minDate.AddDays(-1);
            var managerResult = fastaServiceManager.ChangeFastaDemoPeriod(manageFastaDemoPeriodDto);
            Assert.IsTrue(managerResult.Error);
            #endregion

            #region Account
            manageFastaDemoPeriodDto.DemoExpiredDate = maxDate.GetValueOrDefault(minDate);
            manageFastaDemoPeriodDto.AccountId = Guid.Empty;
            managerResult = fastaServiceManager.ChangeFastaDemoPeriod(manageFastaDemoPeriodDto);
            Assert.IsTrue(managerResult.Error);
            #endregion

            #region NotDemo
            createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccountCommand.AccountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = 900,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            manageFastaDemoPeriodDto.AccountId = createAccountCommand.AccountId;
            managerResult = fastaServiceManager.ChangeFastaDemoPeriod(manageFastaDemoPeriodDto);
            Assert.IsTrue(managerResult.Error);
            #endregion
        }
    }
}
