﻿using System;
using Clouds42.CloudServices.Fasta.Helpers;
using Clouds42.CloudServices.Fasta.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeFastaDemoPeriodTest
{
    /// <summary>
    /// Сценарий теста на успешное изменение демо периода
    /// для сервиса Fasta
    ///
    /// 1) Создаем аккаунт и инициализируем все необходимые данные для теста #CreateAccount
    /// 2) Меняем демо период #ChangeFastaDemoPeriod
    /// 3) Проверяем что демо период изменился #CheckRowInDb
    /// </summary>
    public class ScenarioChangeFastaDemoPeriodSuccessTest : ScenarioBase
    {
        public override void Run()
        {
            #region CreateAccount
            var fastaServiceManager = ServiceProvider.GetRequiredService<FastaServiceManager>();
            var fastaServiceHelper = ServiceProvider.GetRequiredService<FastaServiceHelper>();

            var createAccountCommand = new CreateAccountCommand(TestContext,
                new AccountRegistrationModelTest
                {
                    RegistrationConfig = new RegistrationConfigDomainModelDto
                    {
                        CloudService = Clouds42Service.Esdl

                    }
                });
            createAccountCommand.Run();

            var minDate = DateTime.Now;
            var maxDate = fastaServiceHelper.GetMaxDemoExpiredDate(createAccountCommand.AccountId)
                .GetValueOrDefault(minDate.AddDays(10));

            var manageFastaDemoPeriodDto = fastaServiceManager.GetManageFastaDemoPeriodDto().Result;
            Assert.IsNotNull(manageFastaDemoPeriodDto);
            #endregion

            #region ChangeFastaDemoPeriod
            manageFastaDemoPeriodDto.DemoExpiredDate = maxDate.AddDays(-1);
            var managerResult = fastaServiceManager.ChangeFastaDemoPeriod(manageFastaDemoPeriodDto);
            Assert.IsFalse(managerResult.Error);
            #endregion

            #region CheckRowInDb

            var resConf = DbLayer.ResourceConfigurationRepository.FirstOrDefault(rc =>
                rc.AccountId == createAccountCommand.AccountId &&
                rc.BillingService.SystemService == Clouds42Service.Esdl);

            Assert.IsNotNull(resConf);
            Assert.AreEqual(resConf.ExpireDateValue.Date, manageFastaDemoPeriodDto.DemoExpiredDate.Date);
            #endregion
        }
    }
}
