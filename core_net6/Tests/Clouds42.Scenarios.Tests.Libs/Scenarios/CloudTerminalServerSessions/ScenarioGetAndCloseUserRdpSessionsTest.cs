﻿using System;
using System.Collections.Generic;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestSupportHelper;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.CloudTerminalServerSessions
{
    /// <summary>
    /// Получить сессии пользователей для аккаунта
    /// Сценарий:
    ///     1) Создаем аккаунт, считаем что у этого аккаунта есть активные сессии
    ///     2) Проверяем список сессий, полученный с помощью Id аккаунта, на всех терминальных серверах
    ///     3) Проверяем список сессий, полученный с помощью Id аккаунта, на одном терминальном сервере
    ///     4) Подсовываем неверную модель сессий и проверяем, что метод вернул PreconditionFailed
    ///     5) подсовываем правильную модель сесии и проверяем, что метод вернул Ok
    /// </summary>
    public class ScenarioGetAndCloseUserRdpSessionsTest: ScenarioBase
    {
        private readonly CloudTerminalServerSessionsManager _cloudTerminalServerSessionsManager;

        public ScenarioGetAndCloseUserRdpSessionsTest()
        {
            _cloudTerminalServerSessionsManager = TestContext.ServiceProvider.GetRequiredService<CloudTerminalServerSessionsManager>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var terminalServerId = Guid.Empty;
            var login = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.AccountId == account.AccountId).Login;
            var expected = new List<UserRdpSessionsDto>
            {
                new()
                {
                    UserName = login,
                    TerminalServerID = terminalServerId
                }
            };
            var rdpSessionsByAccountIdResult = _cloudTerminalServerSessionsManager.FindUserRdpSessionsByAccountIdAsync(account.AccountId).Result;
            AssertHelper.HasEqualFieldValues(expected, rdpSessionsByAccountIdResult.Result);

            var userRdpSessionsResult = _cloudTerminalServerSessionsManager.GetUserRdpSessionsAsync(terminalServerId, account.AccountId).Result;
            AssertHelper.HasEqualFieldValues(expected, userRdpSessionsResult.Result);

            var model = new CloudTerminalServerPostRequestCloseDto { TerminalServerID = terminalServerId, UserRdpSessionID = 0 };
            var dropSessionResult = _cloudTerminalServerSessionsManager.CloseUserRdpSession(model);
            Assert.AreEqual(ManagerResultState.PreconditionFailed, dropSessionResult.State);

            model.TerminalServerID = Guid.NewGuid();
            dropSessionResult = _cloudTerminalServerSessionsManager.CloseUserRdpSession(model);
            Assert.AreEqual(ManagerResultState.Ok, dropSessionResult.State);
        }
    }
}
