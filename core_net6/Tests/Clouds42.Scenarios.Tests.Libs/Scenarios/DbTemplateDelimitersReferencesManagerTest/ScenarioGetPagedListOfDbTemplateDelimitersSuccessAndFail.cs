﻿using System;
using System.Collections.Generic;
using Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;
using PagedList.Core;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного получения пагинированного списка конфигураций баз на разделителях
    /// Действия:
    /// 1. Создадим шаблоны инф. баз на разделителях. [DbTemplateDelimitersCreation]
    /// 2. Проверим, что шаблоны существую и пагинированный список будет получен по валидному фильтру. [CheckValidFilter]
    /// 3. Проверим, что пагинированный список будет получен по пустому фильтру. [CheckEmptyFilter]
    /// 4. Проверим, что пагинированный список не будет получен с несуществующей текущей страницей. [CheckNonExistentCurrentPageInFilter]
    /// </summary>
    public class ScenarioGetPagedListOfDbTemplateDelimitersSuccessAndFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersReferencesManager _dbTemplateDelimiterManager;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioGetPagedListOfDbTemplateDelimitersSuccessAndFail()
        {
            _dbTemplateDelimiterManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersReferencesManager>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreation

            var configurationsId = new List<string>
            {
                "bt",
                "bk",
                "ut",
                "up",
                "unf"
            };

            var templateName = _createDbTemplateTestHelper.GetDbTemplateTestName();

            configurationsId.ForEach(id =>
                _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(templateName, id));

            var createdDbTemplatesDelimitersCount = DbLayer.DbTemplateDelimitersReferencesRepository.Count();
            if (createdDbTemplatesDelimitersCount != configurationsId.Count)
                throw new InvalidOperationException("Не создались все нашблоны инф. баз");

            #endregion

            #region CheckValidFilter

            var filter = new PagedListFilter
            {
                CurrentPage = 1,
                PageSize = 2
            };

            var result = _dbTemplateDelimiterManager.GetPagedListOfDbTemplateDelimiters(filter);
            if (result.Error || !result.Result.IsFirstPage || result.Result.Count != filter.PageSize)
                throw new InvalidOperationException("Неверно получили пагинированный список по фильтру");

            CheckDbTemplatesDelimitersFromPagedLisExist(result.Result, filter.PageSize);

            #endregion

            #region CheckEmptyFilter

            result = _dbTemplateDelimiterManager.GetPagedListOfDbTemplateDelimiters();
            if (result.Error || result.Result.Count != configurationsId.Count || !result.Result.IsFirstPage)
                throw new InvalidOperationException("Не получили пагинированный список по пустому фильтру");

            #endregion

            #region CheckNonExistentCurrentPageInFilter

            filter.CurrentPage = 5;
            result = _dbTemplateDelimiterManager.GetPagedListOfDbTemplateDelimiters(filter);
            if (result.Error || result.Result.Count != 0)
                throw new InvalidOperationException("Получили пагинированный список по несуществующей странице");

            #endregion
        }

        /// <summary>
        /// Проверить что шаблоны инф. баз на разделителях в пагинированном списке существуют
        /// </summary>
        /// <param name="pagedList"></param>
        /// <param name="pageSize"></param>
        private void CheckDbTemplatesDelimitersFromPagedLisExist(IPagedList<IDbTemplateDelimiters> pagedList,
            int pageSize)
        {

            var dbTemplateDelimiterManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            for (var i = 0; i < pageSize; i++)
            {
                var result = dbTemplateDelimiterManager.GetDbTemplateDelimiter(pagedList[i].ConfigurationId);

                if (result.Error)
                    throw new InvalidOperationException(result.Message);

                if (result.Result == null)
                    throw new InvalidOperationException(
                        "В пагинированном списке несуществующий шаблон инф. базы на разделителях");
            }
        }
    }
}
