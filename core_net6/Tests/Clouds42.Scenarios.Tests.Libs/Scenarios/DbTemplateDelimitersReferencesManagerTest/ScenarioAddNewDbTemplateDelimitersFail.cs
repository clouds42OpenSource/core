﻿using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для неуспешного добавления нового шаблона конфигурации баз на разделителях
    /// Действия:
    /// 1. Создадим шаблон конфигурации баз на разделителях. [DbTemplateDelimitersCreations]
    /// 2. Проверим что шаблон не будет добавлен с пустым значением ConfigurationReleaseVersion. [ReleaseVerisonCheck]
    /// 3. Проверим что шаблон не будет добавлен с пустым значением MinReleaseVersion. [EmptyMinVersionCheck]
    /// 4. Проверим что не будет создана копии шаблона из 1-го пункта. [EmptyMinVersionCheck]
    /// </summary>
    public class ScenarioAddNewDbTemplateDelimitersFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioAddNewDbTemplateDelimitersFail()
        {
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreations

            var dbTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.BpName,
                DefaultCaption = "Бухгалтерия предприятия 3.0"
            });

            var correctReleaseVersion = "3.0.67.72";
            var correctDbTemplateDelimitersModelTest = new DbTemplateDelimitersModelTest(TestContext, templateId: dbTemplate.Id);

            var result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(correctDbTemplateDelimitersModelTest);
            var createdDbTemplateDelimiter = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(correctDbTemplateDelimitersModelTest.ConfigurationId);
            if (result.Error || createdDbTemplateDelimiter == null)
                throw new InvalidOperationException("Не создался шаблон конфигурации баз на разделителях");

            #endregion

            #region ReleaseVerisonCheck

            var dbTemplateDelimiterTest = new DbTemplateDelimitersModelTest(TestContext)
            {
                ConfigurationReleaseVersion = string.Empty,
                ConfigurationId = "ka",
                Name = "New Test Name"
            };

            result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(dbTemplateDelimiterTest);
            if (!result.Error)
                throw new InvalidOperationException("Удалось добавить шаблон баз на разделителях при пустом ConfigurationReleaseVersion");

            #endregion

            #region EmptyMinVersionCheck

            dbTemplateDelimiterTest.ConfigurationReleaseVersion = correctReleaseVersion;
            dbTemplateDelimiterTest.MinReleaseVersion = string.Empty;

            result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(dbTemplateDelimiterTest);
            if (!result.Error)
                throw new InvalidOperationException("Удалось добавить шаблон баз на разделителях при пустом MinReleaseVersion");

            #endregion

            #region CheckCopyWasNotAdded

            result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(correctDbTemplateDelimitersModelTest);
            var dbTemplateDelimitersCount = DbLayer.DbTemplateDelimitersReferencesRepository.Where(dbTmp =>
                dbTmp.Name == correctDbTemplateDelimitersModelTest.Name && dbTmp.ConfigurationId == correctDbTemplateDelimitersModelTest.ConfigurationId).Count();
            if (!result.Error || dbTemplateDelimitersCount > 1)
                throw new InvalidOperationException("Добавилась копия шаблона конфигурации баз на разделителях");

            #endregion
        }
    }
}