﻿using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using System;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.IDataModels;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного редактирования шаблона конфигурации баз на разделителях
    /// Действия:
    /// 1. Создадим шаблон конфигураций на разделителях. [DbTemplateDelimitersCreation]
    /// 2. Отредактируем созданный шаблон и проверим корректно ли отредактировалась запись. [EditAndCheckDbTemplateDelimiters]
    /// 3. Попробуем отредактировать запись с пустой моделью. [CheckWithEmptyModel]
    /// 4. Попробуем отредактировать шаблон с несуществующим Id. [CheckWithNonExistingId]
    /// </summary>
    public class ScenarioEditExistingConfigurationSuccessAndFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioEditExistingConfigurationSuccessAndFail()
        {
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreation

            var utTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.UtName,
                DefaultCaption = "Управление торговлей"
            });

            var oldDbTemplateDelimiters = new DbTemplateDelimitersModelTest(TestContext, templateId: utTemplate.Id);
            var result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(oldDbTemplateDelimiters);

            var createdDbTemplateDelimiters =
                _dbTemplateDelimitersManager.GetDbTemplateDelimiter(oldDbTemplateDelimiters.ConfigurationId);

            if (result.Error || createdDbTemplateDelimiters.Result == null)
                throw new NotFoundException(createdDbTemplateDelimiters.Message);

            #endregion

            #region EditAndCheckDbTemplateDelimiters

            var newDbTemplateDelimiters = new Domain.DataModels.DbTemplateDelimiters
            {
                Name = "New name",
                DemoDatabaseOnDelimitersPublicationAddress = "New address",
                ShortName = "New short name",
                TemplateId = _createDbTemplateTestHelper.CreateDbTemplate().Id,
                ConfigurationId = oldDbTemplateDelimiters.ConfigurationId
            };

            result = _dbTemplateDelimitersManager.UpdateDbTemplateDelimiterItem(newDbTemplateDelimiters);
            if (result.Error)
                throw new InvalidOperationException(result.Message);

            var editedDbTemplateDelimiters =
                _dbTemplateDelimitersManager.GetDbTemplateDelimiter(newDbTemplateDelimiters.ConfigurationId);

            CompareModelsAfterEditing(oldDbTemplateDelimiters, editedDbTemplateDelimiters.Result);

            #endregion

            #region CheckWithEmptyModel

            result = _dbTemplateDelimitersManager.UpdateDbTemplateDelimiterItem(
                new Domain.DataModels.DbTemplateDelimiters());

            if (!result.Error)
                throw new InvalidOperationException(
                    "Успешно отредактирован шаблон конфигурации баз на разделителях с пустой модели");

            #endregion

            #region CheckWithNonExistingId

            newDbTemplateDelimiters.ConfigurationId = "ckfl";
            result = _dbTemplateDelimitersManager.UpdateDbTemplateDelimiterItem(newDbTemplateDelimiters);
            if (!result.Error)
                throw new InvalidOperationException(
                    "Успешно отредактирован шаблон конфигурации баз на разделителях по несуществующему Id");

            #endregion
        }

        /// <summary>
        /// Сравнить модель до редактирования и после редактирования
        /// </summary>
        /// <param name="dbTemplateDelimitersModelTest">Модель до редактирования</param>
        /// <param name="editedDbTemplateDelimiters">Модель после редактирования</param>
        private void CompareModelsAfterEditing(DbTemplateDelimitersModelTest dbTemplateDelimitersModelTest,
            IDbTemplateDelimiters editedDbTemplateDelimiters)
        {
            if (dbTemplateDelimitersModelTest.Name == editedDbTemplateDelimiters.Name)
                throw new InvalidOperationException(
                    "Совпадает значение Name у начальной и отредактированной модели");

            if (dbTemplateDelimitersModelTest.DemoDatabaseOnDelimitersPublicationAddress == editedDbTemplateDelimiters.DemoDatabaseOnDelimitersPublicationAddress)
                throw new InvalidOperationException(
                    "Совпадает значение DemoDatabaseOnDelimitersPublicationAddress у начальной и отредактированной модели");

            if (dbTemplateDelimitersModelTest.ShortName == editedDbTemplateDelimiters.ShortName)
                throw new InvalidOperationException(
                    "Совпадает значение ShortName у начальной и отредактированной модели");

            if (dbTemplateDelimitersModelTest.TemplateId == editedDbTemplateDelimiters.TemplateId)
                throw new InvalidOperationException(
                    "Совпадает значение TemplateId у начальной и отредактированной модели");
        }
    }
}