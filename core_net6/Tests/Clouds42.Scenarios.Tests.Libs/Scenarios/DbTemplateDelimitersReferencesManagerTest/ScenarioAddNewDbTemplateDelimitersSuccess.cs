﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного добавления новой конфигурации баз на разделителях
    /// 1. Создадим шаблон конфигурации баз на разделителях через хелпер. [DbTemplateDelimitersCreation]
    /// 2. Проверим что количество шаблонов увеличилось после создания и созданный шаблон будет найден. [CheckDbTemplateDelimitersWasCreated]
    /// </summary>
    public class ScenarioAddNewDbTemplateDelimitersSuccess : ScenarioBase
    {
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioAddNewDbTemplateDelimitersSuccess()
        {
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreation

            var countDbTemplatesBeforeCreation = DbLayer.DbTemplateDelimitersReferencesRepository.Count();

            var configurationId = ConfigurationsIdTestConstants.Bp;
            var templateName = _createDbTemplateTestHelper.GetDbTemplateTestName();

            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(templateName, configurationId);

            #endregion

            #region CheckDbTemplateDelimitersWasCreated

            var countDbTemplatesAfterCreation = DbLayer.DbTemplateDelimitersReferencesRepository.Count();

            if (countDbTemplatesAfterCreation < countDbTemplatesBeforeCreation)
                throw new InvalidOperationException(
                    "Количество шаблонов баз на разделителях не изменилось после создания");

            var dbTemplateDelimiters = DbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(dbTmp =>
                dbTmp.ConfigurationId == configurationId && dbTmp.Template.Name == templateName);

            if (dbTemplateDelimiters == null)
                throw new NotFoundException("Не найден шаблон конфигурация после создания");

            #endregion

        }
    }
}
