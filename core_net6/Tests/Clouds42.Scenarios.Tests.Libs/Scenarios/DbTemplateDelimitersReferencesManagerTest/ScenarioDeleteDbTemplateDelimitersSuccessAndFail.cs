﻿using System;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного удаления шаблона конфигурации баз на разделителях
    /// Действия:
    /// 1. Создадим шаблон конфигурации баз на разделителях. [DbTemplateDelimitersCreation]
    /// 2. Удалим созданный шаблон и проверим что в бд запись удалилась. [DbTemplateDelimitersDeletionAndCheckSuccessDeletion]
    /// 3. Проверим, что не будет удален шаблон с несуществующим Id. [DeletionWithEmptyIdCheck]
    /// </summary>
    public class ScenarioDeleteDbTemplateDelimitersSuccessAndFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioDeleteDbTemplateDelimitersSuccessAndFail()
        {
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreation

            var countDbTemplatesBeforeDeletion = DbLayer.DbTemplateDelimitersReferencesRepository.Count();

            var configurationId = ConfigurationsIdTestConstants.Bp;
            var templateName = _createDbTemplateTestHelper.GetDbTemplateTestName();

            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(templateName, configurationId);

            #endregion

            #region DbTemplateDelimitersDeletionAndCheckSuccessDeletion

            var dbTemplateDelimiters =
                _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;

            var result = _dbTemplateDelimitersManager.DeleteDbTemplateDelimiterItem(new DeleteDbTemplateDelimiterDto {ConfigurationId = dbTemplateDelimiters.ConfigurationId});
            if (result.Error)
                throw new InvalidOperationException(result.Message);

            var countDbTemplatesAfterDeletion = DbLayer.DbTemplateDelimitersReferencesRepository.Count();
            
            dbTemplateDelimiters =
                _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;

            if (countDbTemplatesAfterDeletion != countDbTemplatesBeforeDeletion || dbTemplateDelimiters != null)
                throw new InvalidOperationException("Не удалился шаблон конфигурации баз на разделителях");

            #endregion

            #region DeletionWithEmptyIdCheck

            result = _dbTemplateDelimitersManager.DeleteDbTemplateDelimiterItem(new DeleteDbTemplateDelimiterDto{ConfigurationId = string.Empty});
            if (!result.Error)
                throw new InvalidOperationException("Получилось удалить шаблон конфигурации баз на разделителях по несуществующему Id");

            #endregion
        }
    }
}