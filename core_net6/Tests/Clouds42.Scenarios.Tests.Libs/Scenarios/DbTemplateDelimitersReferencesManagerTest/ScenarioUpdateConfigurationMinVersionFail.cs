﻿using System;
using Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для неуспешного обновления минимальной версии шаблона конфигурации при невалидных значениях
    /// Действия:
    /// 1. Создадим шаблон инф базы на разделителях. [DbTemplateCreation]
    /// 2. Проверим, что минимальная версия шаблона конфигурации не будет обновлена для пустого значения Id базы. [CheckAbilityToUpdateMinVersionWithEmptyDbId]
    /// 3. Проверим, что минимальная версия шаблона конфигурации не будет обновлена для пустого значения версии релиза. [CheckAbilityToUpdateMinVersionWithEmptyVersion]
    /// 4. Проверим, что минимальная версия шаблона конфигурации не будет обновлена для несуществующего значения Id базы. [CheckAbilityToUpdateMinVersionWithNonExistentDbId]
    /// </summary>
    public class ScenarioUpdateConfigurationMinVersionFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersReferencesManager _dbTemplateDelimitersReferencesManager;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;

        public ScenarioUpdateConfigurationMinVersionFail()
        {
            _dbTemplateDelimitersReferencesManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersReferencesManager>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
        }
        public override void Run()
        {
            #region DbTemplateCreation

            var configurationId = ConfigurationsIdTestConstants.Ka;
            var newVersion = "3.0.67.73";
            var templateName = _createDbTemplateTestHelper.GetDbTemplateTestName();

            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(templateName, configurationId);

            #endregion

            #region CheckAbilityToUpdateMinVersionWithEmptyDbId

            var result = _dbTemplateDelimitersReferencesManager.UpdateConfigurationMinVersion("", newVersion);
            var dbTemplate = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;
            if (!result.Error || dbTemplate.ConfigurationReleaseVersion == newVersion)
                throw new InvalidOperationException("Минимальная версия шаблона конфигурации обновилась для пустого значения шаблона");

            #endregion

            #region CheckAbilityToUpdateMinVersionWithEmptyVersion

            result = _dbTemplateDelimitersReferencesManager.UpdateConfigurationMinVersion(configurationId, "");
            dbTemplate = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;
            if (!result.Error || dbTemplate.ConfigurationReleaseVersion == newVersion)
                throw new InvalidOperationException("Минимальная версия шаблона конфигурации обновилась на пустую версию");

            #endregion

            #region CheckAbilityToUpdateMinVersionWithNonExistentDbId

            result = _dbTemplateDelimitersReferencesManager.UpdateConfigurationMinVersion("bp", newVersion);
            dbTemplate = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;
            if (!result.Error || dbTemplate.ConfigurationReleaseVersion == newVersion)
                throw new InvalidOperationException("Минимальная версия шаблона конфигурации обновилась для несуществующего шаблона");

            #endregion
        }
    }
}
