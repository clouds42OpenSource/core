﻿using System;
using Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного обновления релизной версии шаблона конфигурации
    /// Действия:
    /// 1. Создадим шаблон инф базы на разделителях. [DbTemplateCreation]
    /// 2. Обновим релизную версию созданного шаблона. [DbTemplateUpdating]
    /// </summary>
    public class ScenarioUpdateConfigurationReleaseVersionSuccess : ScenarioBase
    {
        private readonly DbTemplateDelimitersReferencesManager _dbTemplateDelimitersReferencesManager;
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;


        public ScenarioUpdateConfigurationReleaseVersionSuccess()
        {
            _dbTemplateDelimitersReferencesManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersReferencesManager>();
            _createDbTemplateDelimitersTestHelper = new CreateDbTemplateDelimitersTestHelper(TestContext);
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
        }

        public override void Run()
        {
            #region DbTemplateCreation

            var configurationId = ConfigurationsIdTestConstants.Ka;
            var newVersion = "3.0.67.73";
            var templateName = _createDbTemplateTestHelper.GetDbTemplateTestName();

            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(templateName, configurationId);

            #endregion

            #region DbTemplateUpdating

            var result = _dbTemplateDelimitersReferencesManager.UpdateConfigurationReleaseVersion(configurationId, newVersion);

            var dbTemplate = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(configurationId).Result;

            if (result.Error && dbTemplate.ConfigurationReleaseVersion != newVersion)
                throw new InvalidOperationException("Релизная версия шаблона не обновилась");

            #endregion
        }
    }
}
