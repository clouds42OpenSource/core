﻿using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using System;
using Clouds42.Common.Exceptions;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного получения шаблона конфигурации баз на разделителях
    /// Действия:
    /// 1. Создаем шаблон конфигурации баз на разделителях. [DbTemplateDelimitersCreation]
    /// 2. Получаем шаблон конфигурации баз на разделителях по созданному Id. [GettingDbTemplateDelimitersByValidId]
    /// 3. Проверяем, что шаблон конфигурации баз на разделителях не будет получен по несуществующему Id. [GettingDbTemplateDelimitersByValidId]
    /// </summary>
    public class ScenarioGetDbTemplateDelimitersSuccessAndFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersManager _dbTemplateDelimitersManager;
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper;

        public ScenarioGetDbTemplateDelimitersSuccessAndFail()
        {
            _dbTemplateDelimitersManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();
            _createDbTemplateTestHelper = TestContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        }

        public override void Run()
        {
            #region DbTemplateDelimitersCreation

            var utTemplate = _createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(DbLayer)
            {
                Name = DbTemplatesNameConstants.UtName,
                DefaultCaption = "Управление торговлей"
            });

            var dbTemplateDelimiters = new DbTemplateDelimitersModelTest(TestContext, templateId: utTemplate.Id);
            var result = _dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(dbTemplateDelimiters);

            var createdDbTemplateDelimiters =
                _dbTemplateDelimitersManager.GetDbTemplateDelimiter(dbTemplateDelimiters.ConfigurationId);

            if (result.Error || createdDbTemplateDelimiters.Result == null)
                throw new NotFoundException(createdDbTemplateDelimiters.Message);

            #endregion

            #region GettingDbTemplateDelimitersByValidId

            var foundDbTemplateDelimiters = _dbTemplateDelimitersManager.GetDbTemplateDelimiter(dbTemplateDelimiters.ConfigurationId);
            if (foundDbTemplateDelimiters.Error || foundDbTemplateDelimiters.Result == null)
                throw new NotFoundException("Созданный шаблон не был найден");

            #endregion

            #region CheckGettingDbTemplateDelimitersByNotValidId

            foundDbTemplateDelimiters = _dbTemplateDelimitersManager.GetDbTemplateDelimiter("test_configuration");
            if (!foundDbTemplateDelimiters.Error && foundDbTemplateDelimiters.Result != null)
                throw new InvalidOperationException("Был найден шаблон по несуществующему Id");

            #endregion

        }
    }
}
