﻿using System;
using Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.DbTemplateDelimitersReferencesManagerTest
{
    /// <summary>
    /// Сценарий для успешного и неуспешного поиска информационных баз для выпадающего списка
    /// Действия:
    /// 1. Создадим аккаунт и информационные базы этого аккаунта. [AccountAndDatabasesCreation]
    /// 2. Проверим, что базы будут найдены по валидной строке поиска. [ValidSearchString]
    /// 3. Проверим, что базы не будут найдены с пустой строкой поиска. [EmptySearchString]
    /// 4. Проверим, что базы не будут найдены по несуществующему Id аккаунта. [EmptyAccountId]
    /// </summary>
    public class ScenarioFindAccountDatabaseSuccessAndFail : ScenarioBase
    {
        private readonly DbTemplateDelimitersReferencesManager _dbTemplateDelimiterManager;

        public ScenarioFindAccountDatabaseSuccessAndFail()
        {
            _dbTemplateDelimiterManager = TestContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersReferencesManager>();
        }

        public override void Run()
        {
            #region AccountAndDatabasesCreation

            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var dbName = "Тестовая база";
            var dbToCreateCount = 10;
            var accountId = createAccountCommand.AccountId;

            CreateAccountDatabases(dbToCreateCount, dbName, createAccountCommand);

            #endregion

            #region ValidSearchString

            var result = _dbTemplateDelimiterManager.FindAccountDatabase("1", accountId);
            if (result.Count != 2)
                throw new InvalidOperationException("Не найдены базы 1 и 10 по строке поиска");

            #endregion

            #region EmptySearchString

            result = _dbTemplateDelimiterManager.FindAccountDatabase("", accountId);
            if (result.Count != 0)
                throw new InvalidOperationException("Найдены базы по пустой строке поиска");

            #endregion

            #region EmptyAccountId

            result = _dbTemplateDelimiterManager.FindAccountDatabase(dbToCreateCount.ToString(), Guid.NewGuid());
            if (result.Count != 0)
                throw new InvalidOperationException("Найдены базы по несуществующему Id аккаунта");

            #endregion
        }

        /// <summary>
        /// Создать информационные базы аккаунта
        /// </summary>
        /// <param name="dbCountToCreate">Количество баз для создания</param>
        /// <param name="dbName">Название баз</param>
        /// <param name="accountDetails">Детали аккаунта родителя</param>
        private void CreateAccountDatabases(int dbCountToCreate, string dbName, IAccountDetails accountDetails)
        {
            var infoDatabaseModel = new InfoDatabaseDomainModelTest(TestContext);

            for (var i = 0; i < dbCountToCreate; i++)
            {
                infoDatabaseModel.DataBaseName = $"{dbName} {i}";
                var createAccountDatabaseCommand = new CreateAccountDatabaseCommand(TestContext, accountDetails, infoDatabaseModel, TestContext.AccessProvider);
                createAccountDatabaseCommand.Run();
            }
        }
    }
}
