﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.CloudsServicesSQLServer.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SqlServer
{
    /// <summary>
    /// Тест проверки создания/редактирования/удаления sql сервера
    /// Сценарий:
    ///         1. Создаем sql сервер, проверяем, что создание прошло без ошибок
    ///         2. Редактируем sql сервер, проверяем, что редактирование прошло без ошибок
    ///         3. Удаляем sql сервер, проверяем, что удаление прошло без ошибок
    /// </summary>
    public class CrudSqlServerTest : ScenarioBase
    {
        private readonly CloudSqlServerManager _cloudSqlServerManager;

        public CrudSqlServerTest()
        {
            _cloudSqlServerManager = TestContext.ServiceProvider.GetRequiredService<CloudSqlServerManager>();
        }
        
        public override void Run()
        {
            var sqlServerId = CreateSqlServer();
            EditSqlServer(sqlServerId);
            DeleteSqlServer(sqlServerId);

        }

        /// <summary>
        /// Создать sql сервер
        /// </summary>
        /// <returns>Id sql сервера</returns>
        private Guid CreateSqlServer()
        {
            var createCloudSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = $"Name{DateTime.Now:mmssfff}Sql",
                Description = "Desc",
                ConnectionAddress = $"10.42.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Simple
            };

            var createCloudSqlServerResult = _cloudSqlServerManager.CreateCloudSqlServer(createCloudSqlServerDto);
            Assert.IsFalse(createCloudSqlServerResult.Error, createCloudSqlServerResult.Message);

            return createCloudSqlServerResult.Result;
        }

        /// <summary>
        /// Редактировать sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        private void EditSqlServer(Guid sqlServerId)
        {
            var sqlServer = _cloudSqlServerManager.GetCloudSqlServerDto(sqlServerId);
            Assert.IsFalse(sqlServer.Error, sqlServer.Message);

            var cloudSqlServerDto = new CloudSqlServerDto
            {
                Id = sqlServerId,
                Name = $"NewName{DateTime.Now:mmssfff}",
                Description = "NewDesc",
                ConnectionAddress = $"10.122.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Mixed
            };

            var sqlServerEditResult = _cloudSqlServerManager.EditCloudSqlServer(cloudSqlServerDto);
            Assert.IsFalse(sqlServerEditResult.Error, sqlServerEditResult.Message);

            var editedSqlServer = _cloudSqlServerManager.GetCloudSqlServerDto(sqlServerId).Result;
            Assert.IsFalse(sqlServer.Error, sqlServer.Message);

            Assert.AreEqual(cloudSqlServerDto.Name, editedSqlServer.Name);
            Assert.AreEqual(cloudSqlServerDto.Description, editedSqlServer.Description);
            Assert.AreEqual(cloudSqlServerDto.ConnectionAddress, editedSqlServer.ConnectionAddress);
            Assert.AreEqual(cloudSqlServerDto.RestoreModelType, editedSqlServer.RestoreModelType);
        }

        /// <summary>
        /// Удалить sql сервер
        /// </summary>
        /// <param name="sqlServerId">Id sql сервера</param>
        private void DeleteSqlServer(Guid sqlServerId)
        {
            var deleteSqlServerResult = _cloudSqlServerManager.DeleteCloudSqlServer(sqlServerId);
            Assert.IsFalse(deleteSqlServerResult.Error, deleteSqlServerResult.Message);

            var sqlServer = _cloudSqlServerManager.GetCloudSqlServerDto(sqlServerId);
            Assert.IsTrue(sqlServer.Error);
        }
    }
}
