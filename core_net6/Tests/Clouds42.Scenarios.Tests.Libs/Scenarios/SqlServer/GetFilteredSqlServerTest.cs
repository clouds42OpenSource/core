﻿using System;
using System.Threading.Tasks;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Segment.CloudsServicesSQLServer.Managers;
using Core42.Application.Features.SqlServerContext.Queries;
using Core42.Application.Features.SqlServerContext.Queries.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.SqlServer
{
    /// <summary>
    /// Тест получения Sql серверов по фильтру
    /// Сценарий:
    ///         1. Создаем 5 Sql серверов
    ///         2. Получаем сервера, отфильтрованные по модели восстановления
    ///         3. Получаем сервера, отфильтрованные по названию
    ///         4. Получаем сервера, отфильтрованные по описанию
    ///         5. Получаем сервера, отфильтрованные по описанию и названию
    ///         6. Получаем сервера, отфильтрованные по адресу подключения
    /// </summary>
    public class GetFilteredSqlServerTest : ScenarioBase
    {
        private readonly CloudSqlServerManager _cloudSqlServerManager;

        public GetFilteredSqlServerTest()
        {
            _cloudSqlServerManager = TestContext.ServiceProvider.GetRequiredService<CloudSqlServerManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var firstSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = "ServerSql",
                Description = "SQL",
                ConnectionAddress = $"10.42.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full
            };
            CreateSqlServer(firstSqlServerDto);

            var secondSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = "NameSql",
                Description = "Desc",
                ConnectionAddress = $"10.42.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Simple
            };
            CreateSqlServer(secondSqlServerDto);

            var thirdSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = "ServerName",
                Description = "DescSql",
                ConnectionAddress = $"10.45.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full
            };
            CreateSqlServer(thirdSqlServerDto);

            var fourthSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = $"{DateTime.Now:mmssfff}Sql",
                Description = "Description",
                ConnectionAddress = $"10.45.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Mixed
            };
            CreateSqlServer(fourthSqlServerDto);

            var fifthSqlServerDto = new CreateCloudSqlServerDto
            {
                Name = $"Name{DateTime.Now:mmssfff}",
                Description = "server",
                ConnectionAddress = $"10.42.{DateTime.Now:mmssfff}.{DateTime.Now:mmssfff}",
                RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Simple
            };
            CreateSqlServer(fifthSqlServerDto);


            #region Получить отфилльтрованные sql сервера отфильтрованные по модели восстановления

            var queryFilter = new GetFilteredSqlServersQuery
            {
                PageNumber = 1,
                Filter = new SqlServerFilter { RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Full }
            };

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Mixed;
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(1, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Simple;
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            #endregion

            #region Получить отфилльтрованные sql сервера отфильтрованные по названию
            
            queryFilter.Filter.RestoreModelType = null;
            queryFilter.Filter.Name = "Name";

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(3, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Name = "Server";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Name = "Sql";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(3, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            #endregion

            #region Получить отфилльтрованные sql сервера отфильтрованные по описанию
            

            queryFilter.Filter.Name = null;
            queryFilter.Filter.Description = "Desc";

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(3, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Description = "Server";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(1, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Description = "Sql";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            #endregion

            #region Получить отфилльтрованные sql сервера отфильтрованные по описанию и названию
            
            queryFilter.Filter.Description = "Desc";
            queryFilter.Filter.Name = "Name";

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Name = "Server";
            queryFilter.Filter.Description = "sql";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.Name = "Name";
            queryFilter.Filter.Description = "Server";
            await GetSqlServersByFilterAndCheckSamplingIsCorrect(1, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            #endregion

            #region Получить отфилльтрованные sql сервера отфильтрованные по адресу подключения
            

            queryFilter.Filter.Name = null;
            queryFilter.Filter.Description = null;
            queryFilter.Filter.ConnectionAddress = "10.45";

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(2, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            queryFilter.Filter.ConnectionAddress = "10.42";

            await GetSqlServersByFilterAndCheckSamplingIsCorrect(3, queryFilter, GetSqlServersByFilterAndCheckSamplingIsCorrectAction);

            #endregion
        }

        /// <summary>
        /// Создать sql сервер
        /// </summary>
        /// <param name="cloudSqlServerDto">Модель создания Sql сервера</param>
        private void CreateSqlServer(CreateCloudSqlServerDto cloudSqlServerDto)
        {
            var createCloudSqlServerResult = _cloudSqlServerManager.CreateCloudSqlServer(cloudSqlServerDto);
            Assert.IsFalse(createCloudSqlServerResult.Error, createCloudSqlServerResult.Message);
        }

        /// <summary>
        /// Получить sql сервера по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="expectedCount">Ожидаемое количество серверов полученных по фильтру</param>
        /// <param name="filter">Модель фильтра</param>
        /// <param name="action">Действие, для выбора и проверки выборки северов</param>
        private static async Task GetSqlServersByFilterAndCheckSamplingIsCorrect(int expectedCount, GetFilteredSqlServersQuery filter, Func<GetFilteredSqlServersQuery, int, Task> action)
        {
            await action(filter, expectedCount);
        }

        /// <summary>
        /// Действие получения sql сервера по фильтру и проверить, что выборка корректная
        /// </summary>
        /// <param name="query">Модель фильтра</param>
        /// <param name="count">Ожидаемое количество серверов полученных по фильтру</param>
        private async Task GetSqlServersByFilterAndCheckSamplingIsCorrectAction(GetFilteredSqlServersQuery query, int count)
        {
            var filteredSqlServers = await Mediator.Send(query);
            Assert.IsFalse(filteredSqlServers.Error, filteredSqlServers.Message);
            Assert.AreEqual(count, filteredSqlServers.Result.Records.Count);
        }
    }
}
