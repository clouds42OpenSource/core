﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Segment.PublishNode.Managers;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Clouds42.Segment.Contracts.CloudsServicesSegment.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishNodeScenarios
{
    /// <summary>
    /// Тест доступности ноды публикации
    /// Сценарий:
    ///         1. Создаем 2 ноды, с реальным адресом и фейковым
    ///         2. Проверяем, что нода с реальным адресом доступна
    ///         3. Проверяем, что нода с фейковым адресом недоступна
    /// </summary>
    public class CheckPublishNodeAvailabilityTest : ScenarioBase
    {
        private readonly PublishNodesManager _publishNodesManager;
        private readonly ICheckIisAvailabilityHelper _checkIisAvailabilityHelper;

        public CheckPublishNodeAvailabilityTest()
        {
            _checkIisAvailabilityHelper = ServiceProvider.GetRequiredService<ICheckIisAvailabilityHelper>();
            _publishNodesManager = ServiceProvider.GetRequiredService<PublishNodesManager>();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            await Task.Run(() =>
            {
                var publishNodeDtoFirst = new PublishNodeDto { Address = "localhost", Description = "Description" };
                var createPublishNodeFirst = _publishNodesManager.AddNewPublishNode(publishNodeDtoFirst);
                Assert.IsFalse(createPublishNodeFirst.Error);

                var availabilityFirstNode =
                    _checkIisAvailabilityHelper.CheckIisAvailability(publishNodeDtoFirst.Address);

                Assert.IsTrue(availabilityFirstNode);

                var publishNodeDtoSecond = new PublishNodeDto
                {
                    Address = "0000.001.2222.212", Description = "Description2"
                };

                var createPublishNodeSecond = _publishNodesManager.AddNewPublishNode(publishNodeDtoSecond);
                Assert.IsFalse(createPublishNodeSecond.Error);

                _checkIisAvailabilityHelper.CheckIisAvailability(publishNodeDtoSecond.Address);
                Assert.IsTrue(availabilityFirstNode);
                _publishNodesManager.DeletePublishNode(createPublishNodeFirst.Result);
                _publishNodesManager.DeletePublishNode(createPublishNodeSecond.Result);
            });
        }
    }
}
