﻿using Clouds42.Segment.PublishNode.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishNodeScenarios
{
    /// <summary>
    /// Тест логирования действий с нодами публикаций
    /// Сценарий:
    ///         1. Выполняем действия по созданию/удаению/редактированию нод публикации
    ///         2. Проверяем, действия залогированы
    /// </summary>
    public class PublishNodeLoggingTest : ScenarioBase
    {
        private readonly PublishNodesManager _publishNodesManager;

        public PublishNodeLoggingTest()
        {
            _publishNodesManager = TestContext.ServiceProvider.GetRequiredService<PublishNodesManager>();
        }

        public override void Run()
        {
        
        }

        public override async Task RunAsync()
        {
            await Task.Run(() =>
            {
                var createAccountCommand = new CreateAccountCommand(TestContext);
                createAccountCommand.Run();

                CheckPublishNodeActionsLogging(false);

                var createPublishNode = _publishNodesManager.AddNewPublishNode(new PublishNodeDtoTest());
                Assert.IsFalse(createPublishNode.Error);

                var publishNodeDto = new PublishNodeDtoTest
                {
                    Id = createPublishNode.Result, Description = "DESCRIPTION", Address = "ADDRESS"
                };

                var editPublishNode = _publishNodesManager.EditPublishNode(publishNodeDto);
                Assert.IsFalse(editPublishNode.Error);

                var deletePublishNode = _publishNodesManager.DeletePublishNode(createPublishNode.Result);
                Assert.IsFalse(deletePublishNode.Error);

                CheckPublishNodeActionsLogging(true);
            });
        }

        /// <summary>
        /// Проверить логирование действий с нодами публикаций
        /// </summary>
        /// <param name="actionsWereTaken">Флаг были ли совершены действия</param>
        private void CheckPublishNodeActionsLogging(bool actionsWereTaken)
        {
            var cloudChanges = DbLayer.CloudChangesRepository.GetAll().ToList();

            if (!actionsWereTaken)
            {
                Assert.IsFalse(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.AddSegmentOrElement));
                Assert.IsFalse(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.EditSegmentOrElement));
                Assert.IsFalse(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.DeleteSegmentOrElement));
            }

            else
            {
                Assert.IsTrue(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.AddSegmentOrElement));
                Assert.IsTrue(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.EditSegmentOrElement));
                Assert.IsTrue(cloudChanges.Any(cc => cc.CloudChangesAction.IndexEnum == LogActions.DeleteSegmentOrElement));
            }
        }
    }
}
