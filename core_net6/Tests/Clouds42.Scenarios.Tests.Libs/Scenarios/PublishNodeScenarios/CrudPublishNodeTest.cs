﻿using Clouds42.Segment.PublishNode.Managers;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using Core42.Application.Features.PublishNodeContext.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.PublishNodeScenarios
{
    /// <summary>
    /// Тест проверки создания/удаления/редактирования ноды публикации
    /// Сценарий:
    ///         1. Создаем ноду публикации, проверяем, что создание прошло без ошибок
    ///         2. Редактируем ноду публикации, проверяем, что редактирование прошло без ошибок
    ///         3. Удаляем ноду публикации, проверяем, что удаление прошло без ошибок
    /// </summary>
    public class CrudPublishNodeTest : ScenarioBase
    {
        private readonly Random _random;
        private readonly PublishNodesManager _publishNodesManager;

        public CrudPublishNodeTest()
        {
            _publishNodesManager = TestContext.ServiceProvider.GetRequiredService<PublishNodesManager>();
            _random = new Random();
        }

        public override void Run()
        {
          
        }

        public override async Task RunAsync()
        {
            var publishNodeId = CreatePublishNode();
            await EditPublishNode(publishNodeId);
            await DeletePublishNode(publishNodeId);
        }

        /// <summary>
        /// Создать ноду публикации
        /// </summary>
        /// <returns>Id ноды публикации</returns>
        private Guid CreatePublishNode()
        {
            var publishNodeDto = new PublishNodeDto
            {
                Address = _random.Next(0,50000).ToString(),
                Description = "TestDescription"
            };

            var publishNode = _publishNodesManager.AddNewPublishNode(publishNodeDto);
            Assert.IsFalse(publishNode.Error);

            return publishNode.Result;
        }

        /// <summary>
        /// Редактировать ноду публикации
        /// </summary>
        /// <param name="publishNodeId">Id ноды публикации</param>
        private async Task EditPublishNode(Guid publishNodeId)
        {
            var currentPublishNode = await Mediator.Send(new GetPublishNodeByIdQuery { Id = publishNodeId });
            Assert.IsFalse(currentPublishNode.Error);

            var editedPublishNodeDto = new PublishNodeDto
            {
                Id = currentPublishNode.Result.Id,
                Address = _random.Next(10000, 60000).ToString(),
                Description = "Desc"
            };
            var editPublishNode = _publishNodesManager.EditPublishNode(editedPublishNodeDto);
            Assert.IsFalse(editPublishNode.Error);

            var editedPublishNode = await Mediator.Send(new GetPublishNodeByIdQuery { Id = publishNodeId });
            Assert.IsFalse(editedPublishNode.Error);
            Assert.AreEqual(editedPublishNodeDto.Address, editedPublishNode.Result.Address);
            Assert.AreEqual(editedPublishNodeDto.Description, editedPublishNode.Result.Description);
        }

        /// <summary>
        /// Удалить ноду публикации
        /// </summary>
        /// <param name="publishNodeId">Id ноды публикации</param>
        private async Task DeletePublishNode(Guid publishNodeId)
        {
            var deletePublishNode = _publishNodesManager.DeletePublishNode(publishNodeId);
            Assert.IsFalse(deletePublishNode.Error);

            var publishNode = await Mediator.Send(new GetPublishNodeByIdQuery { Id = publishNodeId });
            Assert.IsNull(publishNode.Result);
        }
    }
}
