﻿using System.Linq;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSaleManager = Clouds42.Accounts.Account.Managers.AccountSaleManager;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    public class ScenarioRemoveSaleManagerFromAccountTest: ScenarioBase
    {
        /// <summary>
        /// Отключение ответственного от аккаунта
        /// Действия:
        /// 1) Создаём аккаунт
        /// 2) Даём админу роль клаудадмина
        /// 3) Назначаем его ответственным за аккаунт
        /// 4) Убираем у аккаунта ответственного
        /// 5) Проверяем записи в базе
        /// </summary>
        public override void Run()
        {
            var createAccCommand = new CreateAccountCommand(TestContext);
            createAccCommand.Run();

            var account = createAccCommand.Account;
            var adminId = createAccCommand.AccountAdminId;
            var admin = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == adminId);

            var adminRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == adminId);
            adminRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(adminRole);

            var division = "test division";
            var accountSaleManagerDto = new AccountSaleManagerDto
            {
                AccountNumber = account.IndexNumber,
                Login = admin.Login,
                Division = division
            };

            var accountSaleManager = TestContext.ServiceProvider.GetRequiredService<AccountSaleManager>();

            var result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsFalse(result.Error, "Операция вернула ошибку.");
            #region 2check
            var accountSales =
                DbLayer.AccountSaleManagerRepository.Where(s => s.AccountId == account.Id || s.SaleManagerId == adminId);
            Assert.AreEqual(1,accountSales.Count(), "Должна быть лишь одна запись");

            var accountSale = accountSales.FirstOrDefault(s => s.AccountId == account.Id);
            Assert.IsNotNull(accountSale, "неверный гуид аккаунта");
            Assert.AreEqual(accountSale.SaleManagerId, adminId, "Неверный гуид ответственного");
            Assert.AreEqual(account.AccountSaleManager.SaleManagerId, admin.Id, "В поле ответственного записан неверный Guid.");

            var action = DbLayer.CloudChangesActionRepository.FirstOrDefault(c => c.Index == 71);

            RefreshDbCashContext(TestContext.Context.CloudChanges);

            var cloudChanges =
                DbLayer.CloudChangesRepository.Where(c => c.Action == action.Id && c.AccountId == account.Id);
            Assert.AreEqual(1,cloudChanges.Count(), "Должна быть одна запись");

            var cloudChange = cloudChanges.FirstOrDefault(c => c.Action == action.Id);
            Assert.IsNotNull(cloudChange.Date, "Не заполнена дата");
            #endregion 2check

            result = accountSaleManager.RemoveSaleManagerFromAccount(account.IndexNumber);
            Assert.IsFalse(result.Error, "Операция вернула ошибку.");
            accountSales =
                DbLayer.AccountSaleManagerRepository.Where(s => s.AccountId == account.Id || s.SaleManagerId == adminId);
            Assert.IsFalse(accountSales.Any(), "Таблица должна быть пустой.");

            var cloudChanges2 =
                DbLayer.CloudChangesRepository.Where(c => c.Action == action.Id && c.AccountId == account.Id);
            Assert.AreEqual(2,cloudChanges2.Count(), "Должно быть две записи");
        }
    }
}
