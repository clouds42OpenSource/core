﻿using System;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Core42.Application.Features.AccountContext.Queries;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Тест на получение дитальной информации об аккаунте
    /// </summary>
    public class GetAccountDetailsTest : ScenarioBase
    {
        private readonly ISender _mediator;
        public GetAccountDetailsTest()
        {
            _mediator = ServiceProvider.GetRequiredService<ISender>();
        }
        public override void Run()
        {
            var createAccountDatabaseAndAccount = new CreateAccountDatabaseAndAccountCommand(TestContext);
            createAccountDatabaseAndAccount.Run();
        
            var account = DbLayer.AccountsRepository.FirstOrDefault(x => x.Id == createAccountDatabaseAndAccount.AccountDetails.AccountId);

            var detailsOfAccount =  _mediator.Send(new GetAccountByIdQuery(account.Id)).Result;

            if(detailsOfAccount.Result == null)
                throw new InvalidOperationException("Ошибка получения информации об аккаунте");
        }
    }
}
