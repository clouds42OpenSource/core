﻿using System;
using System.IO;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Audit.Managers;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Сценарий проверки прав на папку аккаунта
    /// Действия:
    ///     1) Создаем аккаунт
    ///     2) Имитируем запуск джобы аудита
    ///     3) Проверяем что права на папку проставились верно и менеджер не вернул ошибку
    /// </summary>
    public class ScenarioAuditAccoutFolderAccessRules : ScenarioBase
    {

        public override void Run()
        {
            Services.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            var sp = Services.BuildServiceProvider();
            var auditAccountFoldersManager = sp.GetRequiredService<AuditAccountFoldersManager>();
            CreateAccount(sp);

            var auditResult = auditAccountFoldersManager.AuditAccountFoldersNtfsRules();
            Assert.IsFalse(auditResult.Error);
        }

        public void CreateAccount(IServiceProvider serviceProvider)
        {
            var accountData = new AccountRegistrationModelTest();


            var createSegmentCommand = new CreateSegmentCommand(TestContext, defaultSegment: true);
            createSegmentCommand.Run();

            var accountsManager = serviceProvider.GetRequiredService<CreateAccountManager>();
            var managerResult = accountsManager.AddAccountAndCreateSessionAsync(accountData).Result;

            var account = TestContext.DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == managerResult.Result.AccountId.Value);


            var accountSegmentHelper = serviceProvider.GetRequiredService<AccountSegmentHelper>();

            var accountFileStoragePath = accountSegmentHelper.GetAccountFileStoragePath(account);
            if (!Directory.Exists(accountFileStoragePath))
                Directory.CreateDirectory(accountFileStoragePath);

            accountSegmentHelper.GetFullClientFileStoragePath(account.Id);
            if (!Directory.Exists(accountFileStoragePath))
                Directory.CreateDirectory(accountFileStoragePath);

        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
    }
}
