﻿using Clouds42.Accounts.Account.Managers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Billing.Billing.Internal;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Сценарий теста для проверки общей стоимости сервисов аккаунта,
    /// при получении детальной информации об аккаунте
    ///
    /// 1) Создаем аккаунт владельца сервиса
    ///
    /// 2) Создаем кастомный сервис
    ///
    /// 3) Создаем клиентский аккаунт с новым кастомным сервисом
    ///
    /// 4) Выполняем получение детальной информации об аккаунте клиента
    /// и проверяем общую стоимость сервисов (Все не демо сервисы).
    ///
    /// 5) Принимаем подписку кастомного демо сервиса для клиентского аккаунта
    ///
    /// 6) Выполняем получение детальной информации об аккаунте клиента
    /// и проверяем общую стоимость сервисов (Все не демо сервисы).
    /// Проверяем что стоимость сервисов изменилась после принятия подписки
    /// 
    /// </summary>
    public class ScenarioCheckServiceTotalSumWhenGettingAccountDetailsTest : ScenarioBase
    {
        private readonly AccountDetailsProvider _accountDetailsProvider;
        private readonly CreateOrEditBillingServiceHelper _createOrEditBillingServiceHelper;
        private readonly ResourcesService _resourcesService;
        private readonly EditAccountManager _editAccountManager;
        private readonly BillingServiceManager _billingServiceManager;

        public ScenarioCheckServiceTotalSumWhenGettingAccountDetailsTest()
        {
            _createOrEditBillingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _resourcesService = (ResourcesService)TestContext.ServiceProvider.GetRequiredService<IResourcesService>();
            _editAccountManager = TestContext.ServiceProvider.GetRequiredService<EditAccountManager>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _accountDetailsProvider = TestContext.ServiceProvider.GetRequiredService<AccountDetailsProvider>();
        }

        public override void Run()
        {
            #region CreateAccount

            var createOwnerAccountCommand = new CreateAccountCommand(TestContext);
            createOwnerAccountCommand.Run();

            #endregion

            #region CreateService

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var billingServiceType = _createOrEditBillingServiceHelper.GenerateListBillingServiceTypesByCount(1);
            var createBillingServiceTest = new CreateBillingServiceTest(TestContext)
            {
                Name = "Тестовый Сервис",
                AccountId = createOwnerAccountCommand.AccountId,
                BillingServiceTypes = billingServiceType
            };

            var createBillingServiceCommand =
                new CreateEmptyBillingServiceCommand(TestContext, createBillingServiceTest);
            createBillingServiceCommand.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w => w.Name == createBillingServiceTest.Name);
            Assert.IsNotNull(service);

            #endregion

            #region CreateClientAccountCommand

            var createClientAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                AccountCaption = "Test account2",
                Login = $"SecondTestLogin2{DateTime.Now:mmss}",
                Email = "TestEmail2@efsol.ru",
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = service.Id
                }
            });

            createClientAccountCommand.Run();

            var account = _editAccountManager.GetAccount(createClientAccountCommand.AccountId).Result;

            #endregion

            #region CheckServiceTotalSumWithServiceDemoPeriod

            var totalSum = GetServiceTotalSum(createClientAccountCommand.AccountId);
            var accountDetails = _accountDetailsProvider.GetAccountDetails(account.Id);
            Assert.AreEqual(accountDetails.ServiceTotalSum, totalSum);

            #endregion

            #region ApplyDemoServiceSubscribe

            _billingServiceManager.ApplyDemoServiceSubscribe(service.Id, account.Id, false);

            #endregion

            #region CheckServiceTotalSumWithoutServiceDemoPeriod

            var newTotalSum = GetServiceTotalSum(createClientAccountCommand.AccountId);
            accountDetails = _accountDetailsProvider.GetAccountDetails(account.Id);
            Assert.AreEqual(accountDetails.ServiceTotalSum, newTotalSum);
            Assert.AreNotEqual(newTotalSum, totalSum);

            #endregion
        }

        /// <summary>
        /// Получить общую стоимость сервисов аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Общая стоимость сервисов аккаунта</returns>
        private decimal GetServiceTotalSum(Guid accountId) => _resourcesService.GetResourcesConfigurations(accountId)
            .Where(rc => !rc.IsDemoPeriod)
            .Sum(x => x.Cost);
    }
}
