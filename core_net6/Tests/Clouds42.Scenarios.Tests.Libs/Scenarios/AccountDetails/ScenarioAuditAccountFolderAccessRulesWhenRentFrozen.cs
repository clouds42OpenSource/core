﻿using Clouds42.Accounts.Audit.Managers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Helpers;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Сценарий проверки прав на папку аккаунта когда Аренда 1С заблокирована
    /// Действия:
    ///     1) Создаем аккаунт
    ///     2) Блокируем аренду
    ///     2) Имитируем запуск джобы аудита
    ///     3) Проверяем что права на папку проставились верно и менеджер не вернул ошибку
    /// </summary>
    public class ScenarioAuditAccountFolderAccessRulesWhenRentFrozen : ScenarioBase
    {
        private Rent1CManageHelper _rent1CManager;
        private Cloud42ServiceFactory _cloud42ServiceFactory;
        private Guid AccountId;

        public override void Run()
        {
            Services.AddTransient<IActiveDirectoryTaskProcessor, TestActiveDirectoryTaskProcessor>();
            var sp = Services.BuildServiceProvider();
            var auditAccountFoldersManager = sp.GetRequiredService<AuditAccountFoldersManager>();
            _rent1CManager = (Rent1CManageHelper)sp.GetRequiredService<IRent1CManageHelper>();
            _cloud42ServiceFactory = (Cloud42ServiceFactory)sp.GetRequiredService<ICloud42ServiceFactory>();

            CreateAccount(sp);

            var rent1CService = _cloud42ServiceFactory.Get(Clouds42Service.MyEnterprise, AccountId);
            rent1CService.ActivateService();

            var auditResult = auditAccountFoldersManager.AuditAccountFoldersNtfsRules();
            Assert.IsFalse(auditResult.Error);

            _rent1CManager.ManageRent1C(new Rent1CServiceManagerDto
            {
                ExpireDate = DateTime.Now.AddDays(-10)
            }, AccountId);

            auditResult = auditAccountFoldersManager.AuditAccountFoldersNtfsRules();
            Assert.IsFalse(auditResult.Error);
        }

        public void CreateAccount(IServiceProvider serviceProvider)
        {
            var accountData = new AccountRegistrationModelTest();


            var createSegmentCommand = new CreateSegmentCommand(TestContext, defaultSegment: true);
            createSegmentCommand.Run();

            var accountsManager = serviceProvider.GetRequiredService<CreateAccountManager>();
            var managerResult = accountsManager.AddAccountAndCreateSessionAsync(accountData).Result;

            var account = TestContext.DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == managerResult.Result.AccountId.Value);

            AccountId = account.Id;

            var accountSegmentHelper = serviceProvider.GetRequiredService<AccountSegmentHelper>();

            var accountFileStoragePath = accountSegmentHelper.GetAccountFileStoragePath(account);
            if (!Directory.Exists(accountFileStoragePath))
                Directory.CreateDirectory(accountFileStoragePath);

            accountSegmentHelper.GetFullClientFileStoragePath(account.Id);
            if (!Directory.Exists(accountFileStoragePath))
                Directory.CreateDirectory(accountFileStoragePath);

        }

        /// <summary>
        /// Нужно ли использовать функции АД
        /// </summary>
        /// <returns>Флаг нужно ли использовать функции АД</returns>
        protected override bool NeedUseAd() => true;
    }
}
