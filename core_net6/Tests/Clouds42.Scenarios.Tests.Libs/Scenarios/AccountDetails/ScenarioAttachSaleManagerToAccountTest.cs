﻿using System;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Закрепление ответственного за аккаунт
    /// Действия:
    /// 1) Создаём аккаунт с двумя пользователями,делаем одного клаудадмином.
    /// 2) Проверяем работу валидатора пустой моделью и невалидной моделью.
    /// 3) Делаем первого ответственным за аккаунт
    /// 4) Проверяем корректность записей в таблице аккасейлов и CloudChanges
    /// 5) Делаем второго юзера ответственным
    /// 6) Проверяем корректность записей в таблице аккасейлов и CloudChanges
    /// </summary>
    public class ScenarioAttachSaleManagerToAccountTest: ScenarioBase
    {
        public override void Run()
        {
            var createAccCommand = new CreateAccountCommand(TestContext);
            createAccCommand.Run();

            var account = createAccCommand.Account;
            var adminId = createAccCommand.AccountAdminId;
            var admin = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == adminId);

            #region 2user
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = account.Id,
                AccountIdString = SimpleIdHash.GetHashById(createAccCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;

            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false, 
                ClientDescription = "GetTokenByLogin", 
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]", 
                ClientIPAddress = AccessProvider.GetUserHostAddress(), 
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();
            #endregion 2user

            var secondUserDb = DbLayer.AccountUsersRepository.FirstOrDefault(u=>u.Login == secondUser.Login);
            var adminRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == adminId);
            adminRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(adminRole);

            var division = "test division";
            var accountSaleManagerDto = new AccountSaleManagerDto
            {
                AccountNumber = account.IndexNumber,
                Login = admin.Login,
                Division = division
            };

            var accountSaleManager = TestContext.ServiceProvider.GetRequiredService<Clouds42.Accounts.Account.Managers.AccountSaleManager>();

            var result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);

            Assert.IsFalse(result.Error, "Операция вернула ошибку.");
            Assert.AreEqual(result.State, ManagerResultState.Ok, "Операция вернула не тот статус");

            var accountSales =
                DbLayer.AccountSaleManagerRepository.Where(s => s.AccountId == account.Id || s.SaleManagerId == adminId);
            Assert.AreEqual(1,accountSales.Count(), "Должна быть лишь одна запись");

            var accountSale = accountSales.FirstOrDefault(s => s.AccountId == account.Id);
            Assert.IsNotNull(accountSale,"неверный гуид аккаунта");
            Assert.AreEqual(accountSale.SaleManagerId,adminId,"Неверный гуид ответственного");
            Assert.AreEqual(account.AccountSaleManager.SaleManagerId, admin.Id, "В поле ответственного записан неверный Guid.");

            var action = DbLayer.CloudChangesActionRepository.FirstOrDefault(c => c.Index == 71);

            RefreshDbCashContext(TestContext.Context.CloudChanges);

            var cloudChanges =
                DbLayer.CloudChangesRepository.Where(c => c.Action == action.Id && c.AccountId == account.Id);
            Assert.AreEqual(1,cloudChanges.Count(), "Должна быть одна запись");

            var cloudChange = cloudChanges.FirstOrDefault(c => c.Action == action.Id);
            Assert.IsNotNull(cloudChange.Date,"Не заполнена дата");

            accountSaleManagerDto.Login = secondUser.Login;
            result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsFalse(result.Error, "Операция вернула ошибку.");
            Assert.AreEqual(result.State, ManagerResultState.Ok, "Операция вернула не тот статус");

            var cloudChanges2 =
                DbLayer.CloudChangesRepository.Where(c => c.Action == action.Id && c.AccountId == account.Id);
            Assert.AreEqual(2,cloudChanges2.Count(), "Должно быть две записи");

            accountSale =  DbLayer.AccountSaleManagerRepository.FirstOrDefault(s => s.AccountId == account.Id || s.SaleManagerId == adminId);
            Assert.IsNotNull(accountSale, "неверный гуид аккаунта");
            Assert.AreEqual(accountSale.SaleManagerId, secondUserDb.Id, "Неверный гуид ответственного");
            Assert.AreEqual(account.AccountSaleManager.SaleManagerId, secondUserDb.Id, "В поле ответственного записан неверный Guid.");

        }
    }
}
