﻿using System;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccountSaleManager = Clouds42.Accounts.Account.Managers.AccountSaleManager;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Проверка валидации при закреплении ответственного за аккаунт
    /// Сценарий:
    /// 1)Создаём аккаунт с двумя пользователями,делаем одного клаудадмином.
    /// 2) Делаем первого ответственным за аккаунт передавая в метод некорректные модели
    /// 3) Проверяем работу валидатора
    /// </summary>
    public class ScenarioAttachSaleManagerToAccountValidationTest : ScenarioBase
    {
        public override void Run()
        {
            var createAccCommand = new CreateAccountCommand(TestContext);
            createAccCommand.Run();

            var account = createAccCommand.Account;
            var adminId = createAccCommand.AccountAdminId;
            var admin = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == adminId);

            #region 2user
            var secondUser = new AccountUserRegistrationToAccountTest
            {
                AccountId = account.Id,
                AccountIdString = SimpleIdHash.GetHashById(createAccCommand.AccountId),
                Login = $"LoginSecondUser{DateTime.Now:mmss}",
                Email = "LoginSecondUser@efsol.ru",
                FirstName = "LoginSecondUserName"
            };
            var accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            var res = accountUsersProfileManager.AddToAccount(secondUser).Result;

            var secondUserSession = new AccountUserSession
            {
                Id = Guid.NewGuid(),
                Token = Guid.NewGuid(),
                StaticToken = false,
                ClientDescription = "GetTokenByLogin",
                ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                ClientIPAddress = AccessProvider.GetUserHostAddress(),
                TokenCreationTime = DateTime.Now,
                AccountUserId = res.Result,
            };
            DbLayer.AccountUserSessionsRepository.Insert(secondUserSession);
            DbLayer.Save();
            #endregion 2user

            var adminRole = DbLayer.AccountUserRoleRepository.FirstOrDefault(r => r.AccountUserId == adminId);
            adminRole.AccountUserGroup = AccountUserGroup.CloudAdmin;
            DbLayer.AccountUserRoleRepository.Update(adminRole);

            var accountSaleManager = TestContext.ServiceProvider.GetRequiredService<AccountSaleManager>();

            var division = "test division";
            var accountSaleManagerDto = new AccountSaleManagerDto();

            var result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsTrue(result.Error, "Операция должна вернуть ошибку.");

            accountSaleManagerDto.AccountNumber = account.IndexNumber;
            accountSaleManagerDto.Login = admin.Login;
            result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsTrue(result.Error, "Без указания дивизиона операция должна вернуть ошибку.");

            accountSaleManagerDto.AccountNumber = 0;
            accountSaleManagerDto.Division = division;
            result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsTrue(result.Error, "С неверным номером акка операция должна вернуть ошибку.");

            accountSaleManagerDto.AccountNumber = account.IndexNumber;
            accountSaleManagerDto.Login = "логин";
            result = accountSaleManager.AttachSaleManagerToAccount(accountSaleManagerDto);
            Assert.IsTrue(result.Error, "С неверным логинов пользователя операция должна вернуть ошибку.");

            var accountSales =
                DbLayer.AccountSaleManagerRepository.Where(s => s.AccountId == account.Id || s.SaleManagerId == adminId);
            Assert.AreEqual(0,accountSales.Count(), "Ответственный так и не должен быть назначен");

        }
    }
}
