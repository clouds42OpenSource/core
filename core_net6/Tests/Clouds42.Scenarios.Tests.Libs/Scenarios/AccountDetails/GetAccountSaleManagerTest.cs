﻿using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using AccountSaleManager = Clouds42.Accounts.Account.Managers.AccountSaleManager;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountDetails
{
    /// <summary>
    /// Тест получения сейла для аккаунта
    /// Сценарий:
    ///         1. Создаем аккаунт
    ///         2. Добавляем к нему сейла
    ///         3. Получаем сейла аккаунта
    /// </summary>
    public class GetAccountSaleManagerTest : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly AccountSaleManager _accountSaleManager;

        public GetAccountSaleManagerTest()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _accountSaleManager = TestContext.ServiceProvider.GetRequiredService<AccountSaleManager>();
        }

        public override void Run()
        {
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();

            var saleManagerId = AddUserToAccount(createAccountCommand.Account);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == saleManagerId);
            Assert.IsNotNull(accountUser);

            var accountSaleManager = new Domain.DataModels.AccountSaleManager
            {
                AccountId = createAccountCommand.AccountId,
                SaleManagerId = saleManagerId,
                CreationDate = DateTime.Now,
                Division = "TestDivision"
            };
            DbLayer.AccountSaleManagerRepository.Insert(accountSaleManager);
            DbLayer.Save();

            var saleManager = DbLayer.AccountSaleManagerRepository.FirstOrDefault(sm => sm.SaleManagerId == saleManagerId);
            
            var createAccountCommand2 = new CreateAccountCommand(TestContext);
            createAccountCommand2.Run();

            var attachSaleManagerToAccount = _accountSaleManager.AttachSaleManagerToAccount(new AccountSaleManagerDto
            {
                Login = accountUser.Login,
                AccountNumber = createAccountCommand2.Account.IndexNumber,
                Division = saleManager.Division
            });
            Assert.IsFalse(attachSaleManagerToAccount.Error);

            var accountSaleManagerInfo = _accountSaleManager.GetAccountSaleManagerInfo(createAccountCommand2.AccountId);
            Assert.IsNotNull(accountSaleManagerInfo);
            Assert.AreEqual(accountSaleManagerInfo.Result.Login, saleManager.AccountUser.Login);
        }

        /// <summary>
        /// Добавляем в аккаунт нового пользователя
        /// </summary>
        /// <param name="account"></param>
        private Guid AddUserToAccount(Account account)
        {
            var addUserToAccount = _accountUsersProfileManager.AddToAccount(new AccountUserDto
            {
                Login = GetLogin(),
                Password = "<lpOKM098",
                AccountId = account.Id,
                Email = $"{GetLogin()}@posv.sqw",
                AccountCaption = account.AccountCaption,
                Roles = [AccountUserGroup.AccountSaleManager]
            });
            Assert.IsFalse(addUserToAccount.Error);
            Assert.IsNotNull(addUserToAccount.Result);
            Assert.IsTrue(addUserToAccount.Result != Guid.Empty);

            return addUserToAccount.Result;
        }
    }
}
