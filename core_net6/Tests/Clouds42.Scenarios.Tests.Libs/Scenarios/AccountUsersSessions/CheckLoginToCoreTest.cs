﻿using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Common.Encrypt;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест проверки логина и пароля
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Выполняем вход пользователя в облако
    ///         3)Выполняем вход пользователя в облако, указываем неправильный логин         
    /// </summary>
    public class CheckLoginToCoreTest : ScenarioBase
    {
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator = new(new CryptoRandomGeneric());

        public override void Run()
        {
            var accountUserLogin = $"testLogin{DateTime.Now:hhmmssfff}";
            var accountUserPassword = _alphaNumeric42CloudsGenerator.GeneratePassword();
            var phoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Login = accountUserLogin,
                Password = accountUserPassword,
                FullPhoneNumber = phoneNumber
            });

            createAccountCommand.Run();

            var accountUserSessionManager = ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
        
            var model = new LoginModelDto
            {
                AccountUserLogin = accountUserLogin,
                AccountUserPassword = accountUserPassword,
                ClientDescription = "TestLogin",
                ClientDeviceInfo = "TestDevice"
            };

            var res = accountUserSessionManager.Login(model);
            if (res.Result == Guid.Empty)
                throw new InvalidOperationException("Wrong pair login/password");
            
            model = new LoginModelDto
            {
                AccountUserLogin = phoneNumber,
                AccountUserPassword = accountUserPassword,
                ClientDescription = "TestPhone",
                ClientDeviceInfo = "TestDevice"
            };
            res = accountUserSessionManager.Login(model);
            if (res.Result == Guid.Empty)
                throw new InvalidOperationException("Wrong pair login/password");
        }

    }
}