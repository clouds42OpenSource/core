﻿using Clouds42.AlphaNumericsSupport;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Common.DataModels;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Encrypt;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Сценарий проверки правил валидации пароля
    /// Действия:
    ///     1) Генерируем различные пароли
    ///     2) Проверяем пароль в валидаторе
    /// </summary>
    public class ScenarioCheckPasswordValidationRulesTest : ScenarioBase
    {
        private readonly string TestLogin = nameof(TestLogin);
        private readonly IAlphaNumericsGenerator _alphaNumericsGenerator = new AlphaNumericsGenerator(new CryptoRandomGeneric());
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator = new(new CryptoRandomGeneric());

        public override void Run()
        {
            var onlyDigitsPassword = GeneratePassword(8, AlphaNumericsTypes.Numerics);
            var passwordValidationResult = ValidatePassword(onlyDigitsPassword);
            Assert.IsTrue(passwordValidationResult.DataIsValid);

            var onlyLettersPassword =
                GeneratePassword(7, AlphaNumericsTypes.LowerCaseAlpha, AlphaNumericsTypes.UpperCaseAlpha);
            passwordValidationResult = ValidatePassword(onlyLettersPassword);
            Assert.IsFalse(passwordValidationResult.DataIsValid);

            var correctPassword = _alphaNumeric42CloudsGenerator.GeneratePassword();
            passwordValidationResult = ValidatePassword(correctPassword);
            Assert.IsTrue(passwordValidationResult.DataIsValid);

            var charSequencePassword = "12345678";
            passwordValidationResult = ValidatePassword(charSequencePassword);
            Assert.IsTrue(passwordValidationResult.DataIsValid);

            var incorrectLengthPassword = GeneratePassword(5, AlphaNumericsTypes.LowerCaseAlpha,
                AlphaNumericsTypes.UpperCaseAlpha, AlphaNumericsTypes.Numerics, AlphaNumericsTypes.SpecSymbols);
            passwordValidationResult = ValidatePassword(incorrectLengthPassword);
            Assert.IsFalse(passwordValidationResult.DataIsValid);

            passwordValidationResult = ValidatePassword(TestLogin);
            Assert.IsTrue(passwordValidationResult.DataIsValid);
        }

        /// <summary>
        /// Сгенерировать пароль
        /// </summary>
        /// <param name="length">Длина</param>
        /// <param name="availableAlphaNumericsTypes">Параметры генерации</param>
        /// <returns>Пароль</returns>
        private string GeneratePassword(int length, params AlphaNumericsTypes[] availableAlphaNumericsTypes)
            => _alphaNumericsGenerator.GetRandomString(length, availableAlphaNumericsTypes);

        /// <summary>
        /// Проверить пароль на соответствие требованиям
        /// </summary>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>Результат валидации пароля</returns>
        private AccountUserDataValidationResultDto ValidatePassword(string password)
            => new PasswordValidation(password).PasswordIsValid();

    }
}
