﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Выполняем вход в облако несуществующего пользователя
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Выполняем вход в облако несуществующего пользоваетля
    ///         3)Проверяем, что возращается коррктное сообщение
    /// </summary>
    public class LoginAccountUserNotExistTest : ScenarioBase
    {
        private readonly AccountUserSessionLoginManager _accountUserSessionLoginManager;

        public LoginAccountUserNotExistTest()
        {
            _accountUserSessionLoginManager = TestContext.ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
        }

        public override void Run()
        {
            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var loginModel = new LoginModelTest
            {
                AccountUserLogin = "123Login123",
                AccountUserPassword = "0000000",
            };

            var managerResult = _accountUserSessionLoginManager.Login(loginModel);

            Assert.IsTrue(managerResult.Error);
            Assert.AreEqual(Guid.Empty, managerResult.Result);
        }
    }
}
