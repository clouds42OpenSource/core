﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест получения пользователя аккаунта по токену
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем сессию пользователя
    ///         3)Проверяем валидность токена
    ///         4)Получаем по токену пользователя аккаунта
    /// </summary>
    public class GetAccountUserIdByTokenTest : ScenarioBase
    {
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public GetAccountUserIdByTokenTest()
        {
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {
           
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();
            
            var res = _accountUserSessionManager.GetAccountUserIdByToken(createAccountCommand.Token);

            if (res == null || res.Result.Data == Guid.Empty)
                throw  new InvalidOperationException("Тест проверки валидности токена не отработал корректно");

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == res.Result.Data);

            if (accountUser != null)
            {
                return;
            }

            throw new InvalidOperationException("Не удалось получить пользователя по токену");

        }

    }
}
