﻿using Clouds42.AlphaNumericsSupport;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Common.Encrypt;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Выполнить вход в облако по средствам логин/пароля. Создается запись с новой сессией пользователя.
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Выполняем вход в облако
    ///         3)Проверяем, что полученный токен соответствует токену пользователя
    /// </summary>
    public class AccountUserSessionManagerLoginTest : ScenarioBase
    {
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator = new(new CryptoRandomGeneric());

        public override void Run()
        {
            var accountUserPassword = _alphaNumeric42CloudsGenerator.GeneratePassword();
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Password = accountUserPassword
            });
            createAccountCommand.Run();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);

            var accountUserSessionManager = ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();

            var loginModelTest = new LoginModelTest
            {
                AccountUserLogin = accountUser.Login,
                AccountUserPassword = accountUserPassword
            };

            var managerResult = accountUserSessionManager.Login(loginModelTest);

            var session = DbLayer.AccountUserSessionsRepository.FirstOrDefault(aus =>
                aus.AccountUserId == createAccountCommand.AccountAdminId);
            
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(session.Token, managerResult.Result);
        }
    }
}
