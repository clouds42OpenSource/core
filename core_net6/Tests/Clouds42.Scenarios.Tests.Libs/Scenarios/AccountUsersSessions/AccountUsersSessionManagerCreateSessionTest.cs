﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест получения деталей сесии
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем сессию пользователя
    ///         3)Получаем сессию пользователя аккаунта
    /// </summary>
    public class AccountUsersSessionManagerCreateSessionTest : ScenarioBase
    {
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public AccountUsersSessionManagerCreateSessionTest()
        {
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {            
            var createAccountCommand = new CreateAccountCommand(
                TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();

            var newToken = _accountUserSessionManager.CreateSession(createAccountCommand.AccountAdminId).Result;

            var accountUserSession = DbLayer.AccountUserSessionsRepository.FirstOrDefault(s => s.Token == newToken && s.AccountUserId == createAccountCommand.AccountAdminId);

            if (accountUserSession == null)
                throw new InvalidOperationException("Не удалось создать новую сессию для пользователя");

        }
    }
}
