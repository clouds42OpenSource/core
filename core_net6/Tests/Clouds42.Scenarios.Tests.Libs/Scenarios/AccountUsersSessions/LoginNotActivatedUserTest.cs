﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clouds42.Billing;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Выполнить вход в облако по средствам логин/пароля, если пользователь заблокирован
    /// Сценарий:
    ///         1)Создаем аккаунт, создаем пользователя аккаунта
    ///         2)Блокируем пользователя
    ///         3)Выполняем вход в облако
    ///         4)Проверяем, что вернулась ошибка и сообщение о, том, что пользователь заблокирован
    /// </summary>
    public class LoginNotActivatedUserTest : ScenarioBase
    {
        private readonly AccountUserSessionLoginManager _accountUserSessionLoginManager;
        private readonly TestDataGenerator _testDataGenerator;

        public LoginNotActivatedUserTest()
        {
            _accountUserSessionLoginManager = TestContext.ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
            _testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var user = _testDataGenerator.GenerateAccountUser(account.AccountId);
            user.Activated = false;

            dbLayer.AccountUsersRepository.InsertAccountUser(user);
            dbLayer.Save();

            var loginModel = new LoginModelTest
            {
                AccountUserLogin = user.Login,
                AccountUserPassword = user.Password,
            };

            var managerResult = _accountUserSessionLoginManager.Login(loginModel);

            Assert.IsTrue(managerResult.Error);
            Assert.AreEqual(Guid.Empty, managerResult.Result);
        }
    }
}
