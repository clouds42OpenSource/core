﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Попытаться авторизоваться в логине если пароль пустой или null
    /// Сценарий:
    ///         1. Создаем аккаунт и пользователя
    ///         2. Выполняем вход этого пользователя в облако не указывая пароль
    ///         3. Проверяем, что логин прошел и вернулась ошибка выполнения, но не было выброшено исключение
    /// </summary>
    public class TryToLoginWithoutPasswordTest : ScenarioBase
    {
        private readonly AccountUserSessionLoginManager _accountUserSessionLoginManager;

        public TryToLoginWithoutPasswordTest()
        {
            _accountUserSessionLoginManager = TestContext.ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
        }

        public override void Run()
        {
            var password = "";
            var userEmail = $"Test_{DateTime.Now:mmssfff}_user@ew.test";

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Email = userEmail,
                Password = password
            });
            createAccountCommand.Run();

            var loginModel = new LoginModelDto
            {
                AccountUserLogin = userEmail,
                AccountUserPassword = password,
                ClientDescription = "TEST",
                ClientDeviceInfo = "TEST"
            };

            var accountUser =
                DbLayer.AccountUserSessionsRepository.FirstOrDefault(au => au.AccountUserId == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(accountUser);

            var managerResult = _accountUserSessionLoginManager.Login(loginModel);
            Assert.IsTrue(managerResult.Error);
            Assert.IsNull(managerResult.Exception);
            Assert.AreEqual(Guid.Empty, managerResult.Result);

            loginModel.AccountUserPassword = null;

            managerResult = _accountUserSessionLoginManager.Login(loginModel);
            Assert.IsTrue(managerResult.Error);
            Assert.IsNull(managerResult.Exception);
            Assert.AreEqual(Guid.Empty, managerResult.Result);
        }
    }
}
