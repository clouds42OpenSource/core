﻿using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест логина посредством почты и пароля
    /// Сценарий:
    ///         1. Создаем аккаунт и пользователя
    ///         2. Выполняем вход этого пользователя в облако посредством почты и пароля
    ///         3. Проверяем, что логин прошел без ошибок и вернулся правильный токен
    /// </summary>
    public class LoginByEmailTest : ScenarioBase
    {
        private readonly AccountUserSessionLoginManager _accountUserSessionLoginManager;

        public LoginByEmailTest()
        {
            _accountUserSessionLoginManager = TestContext.ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
        }

        public override void Run()
        {
            var password = "Fghjuj_567";
            var userEmail = $"Test_{DateTime.Now:mmssfff}_user@ew.test";

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                Email = userEmail,
                Password = password
            });
            createAccountCommand.Run();

            var loginModel = new LoginModelDto
            {
                AccountUserLogin = userEmail,
                AccountUserPassword = password,
                ClientDescription = "TEST",
                ClientDeviceInfo = "TEST"
            };

            var accountUser =
                DbLayer.AccountUserSessionsRepository.FirstOrDefault(au => au.AccountUserId == createAccountCommand.AccountAdminId);
            Assert.IsNotNull(accountUser);

            var managerResult = _accountUserSessionLoginManager.Login(loginModel);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(managerResult.Result, accountUser.Token);
        }
    }
}
