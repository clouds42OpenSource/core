﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Выполняем вход в облако пользователя(указываем неправильный пароль)
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Выполняем вход в облако пользоваетля (указываем неправильный пароль)
    ///         3)Проверяем, что возращается корректное сообщение
    /// </summary>
    public class LoginIncorrectPasswordTest : ScenarioBase
    {
        private readonly AccountUserSessionLoginManager _accountUserSessionLoginManager;

        public LoginIncorrectPasswordTest()
        {
            _accountUserSessionLoginManager = TestContext.ServiceProvider.GetRequiredService<AccountUserSessionLoginManager>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;

            var account = new CreateAccountCommand(TestContext);
            account.Run();

            var user = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == account.AccountAdminId);

            var loginModel = new LoginModelTest
            {
                AccountUserLogin = user.Login,
                AccountUserPassword = "000000",
            };

            var managerResult = _accountUserSessionLoginManager.Login(loginModel);
            
            Assert.IsTrue(managerResult.Error);
            Assert.AreEqual(Guid.Empty, managerResult.Result);
        }
    }
}
