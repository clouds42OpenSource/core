﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест проверки валидности токена
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем сессию пользователя аккаунта
    ///         3)Проверяем валидность токена 
    /// </summary>
    public class CheckTokenValidityTest : ScenarioBase
    {
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public CheckTokenValidityTest()
        {
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {
          
            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();

            var managerResult = _accountUserSessionManager.CheckTokenValidityAsync(createAccountCommand.Token).Result;

            if (managerResult == null || !managerResult.Result)
                throw  new InvalidOperationException("Тест проверки валидности токена не отработал корректно");
        }

    }
}
