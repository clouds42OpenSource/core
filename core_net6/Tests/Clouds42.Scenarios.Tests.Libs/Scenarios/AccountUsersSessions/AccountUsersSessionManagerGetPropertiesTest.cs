﻿using System;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест получения деталей сесии
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем сессию пользователя
    ///         3)Получаем сессию пользователя аккаунта
    ///         4)Получаем детали сессии
    /// </summary>
    public class AccountUsersSessionManagerGetPropertiesTest : ScenarioBase
    {
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public AccountUsersSessionManagerGetPropertiesTest()
        {
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {            
            var createAccountCommand = new CreateAccountCommand(
                TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();

            var accountUserSession =
                DbLayer.AccountUserSessionsRepository.FirstOrDefault(s => s.Token == createAccountCommand.Token);

            if (accountUserSession == null)
                throw new InvalidOperationException("Не удалось получить токен доступа");

            var findedAccountUserSession = _accountUserSessionManager.GetProperties(accountUserSession.Id);

            if (accountUserSession.Token != findedAccountUserSession.Result.Token)
                throw new InvalidOperationException("Не удалось получить детали сесии");
        }
    }
}
