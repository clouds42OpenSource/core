﻿using System;
using System.Linq;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Тест получения списка сессий пользователя аккаунта
    /// Сценарий:
    ///         1)Создаем аккаунт
    ///         2)Создаем сессию пользователя
    ///         3)Получаем сессию пользователя аккаунта
    ///         4)Получаем список сессий пользователя
    /// </summary>
    public class AccountUsersSessionManagerGetIdsTest : ScenarioBase
    {
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public AccountUsersSessionManagerGetIdsTest()
        {
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {            
            var createAccountCommand = new CreateAccountCommand(
                TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();

            var findedAccountUserSessions = _accountUserSessionManager.GetIDs(createAccountCommand.AccountAdminId);

            var accountUserSession =
                DbLayer.AccountUserSessionsRepository.FirstOrDefault(s => s.Token == createAccountCommand.Token);

            if (accountUserSession == null)
                throw new InvalidOperationException("Не удалось получить токен доступа");

            var findedSession = findedAccountUserSessions.Result.Single(s => s == accountUserSession.Id);

            if (findedSession == Guid.Empty)
                throw new InvalidOperationException("Не удалось получить список сессий у пользователя");
        }
    }
}
