﻿using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.Domain.Access;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountUsersSessions
{
    /// <summary>
    /// Сценарий проверки валидации хэша введенного пароля пользователя
    ///     Действия:
    ///         1) Создаем аккаунт
    ///         2) Имитируем обращение пользователя к методу логина
    ///         3) Проверяем что при правильно введенном пароле вернется OK
    ///         4) Проверяем что при ошибочно введенном пароле вернется PreconditionFailed
    /// </summary>
    public class ScenarioCheckAccountUserPasswordHashValidation : ScenarioBase
    {
        private readonly AccountUsersProfileManager _accountUsersProfileManager;
        private readonly IAccountUserSessionManager _accountUserSessionManager;

        public ScenarioCheckAccountUserPasswordHashValidation()
        {
            _accountUsersProfileManager = TestContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
            _accountUserSessionManager = TestContext.ServiceProvider.GetRequiredService<IAccountUserSessionManager>();
        }

        public override void Run()
        {
            var dbLayer = TestContext.DbLayer;
            var createAccountCommand = new CreateAccountCommand(TestContext);
            createAccountCommand.Run();
            
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == createAccountCommand.AccountAdminId);
            var password = "Qwerty_123";
            var updatedModel = new AccountUserDto
            {
                Id = accountUser.Id,
                AccountCaption = "Test account",
                Login = accountUser.Login,
                Password = password,
                Email = "TestEmail@efsol.ru",
                FirstName = "TestFirstName",
                PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
                Roles =
                [
                    AccountUserGroup.AccountSaleManager,
                    AccountUserGroup.AccountUser,
                    AccountUserGroup.AccountAdmin,
                    AccountUserGroup.Hotline
                ]
            };
            _accountUsersProfileManager.Update(updatedModel);

            var loginModel = new AccountLoginModelDto
            {
                Username = accountUser.Login,
                Password = password
            };

            var managerResult = _accountUserSessionManager.ValidateAccountUserPassword(loginModel);

            Assert.IsFalse(managerResult.Error);

            loginModel.Password = "1234567";
            managerResult = _accountUserSessionManager.ValidateAccountUserPassword(loginModel);
            Assert.IsTrue(managerResult.Error);
        }
    }
}
