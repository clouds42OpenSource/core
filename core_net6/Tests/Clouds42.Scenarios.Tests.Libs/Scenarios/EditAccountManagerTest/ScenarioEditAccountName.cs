﻿using Microsoft.Extensions.DependencyInjection;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Scenarios.Tests.Libs.Mappers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.EditAccountManagerTest
{
    /// <summary>
    /// Сценарий теста проверки возможности редактирования аккауна
    /// Действия:
    ///     1) Создаем новый аккаунт. [AccountCreation]
    ///     2) Изменяем его поле AccountCaption и проверим, что изменение аккаунта прошло успешно. [ChangeLocaleAccount]
    /// </summary>
    public class ScenarioEditAccountName : ScenarioBase
    {
        private readonly EditAccountManager _editAccountManager;
        private readonly AccountMapperTest _accountMapperTest;

        public ScenarioEditAccountName()
        {
            _editAccountManager = ServiceProvider.GetRequiredService<EditAccountManager>();
            _accountMapperTest = ServiceProvider.GetRequiredService<AccountMapperTest>();
        }
        public override void Run()
        {
            #region AccountCreation

            var createAccountCommand = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest());
            createAccountCommand.Run();

            Assert.IsNotNull(createAccountCommand);

            #endregion

            #region ChangeLocaleAccount

            var editAccountDto = _accountMapperTest.GetEditAccountDcFromAccount(createAccountCommand.Account);

            editAccountDto.AccountCaption = "Test_Name_For_Account";

            var accountEditionResult = _editAccountManager.UpdateAccount(editAccountDto);

            Assert.IsNotNull(accountEditionResult);
            Assert.IsTrue(accountEditionResult.Result);

            var account = DbLayer.AccountsRepository.FirstOrDefault(w => w.Id == createAccountCommand.Account.Id);

            Assert.IsNotNull(account);
            Assert.AreEqual(createAccountCommand.Account.AccountCaption, account.AccountCaption);

            #endregion

        }
    }
}
