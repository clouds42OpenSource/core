﻿using System;
using System.Linq;
using Clouds42.Billing;
using Clouds42.DataContracts.BillingService;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Scenarios.Tests.Libs.Scenarios.ModerationResultManagerTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.ChangeServiceRequestDataManagerTest
{
    /// <summary>
    /// Сценарий теста, по проверке получения порции данных
    /// заявок сервиса на модерацию
    ///
    /// 1) Создаем сервис и заявки на модерацию сервиса
    /// 2) Выполняем выборку меняя условия фильтра и проверяем выборку
    /// </summary>
    public class ScenarioGetChangeServiceRequestsTest : ScenarioBase
    {
        public override void Run()
        {
            var testDataGenerator = TestContext.ServiceProvider.GetRequiredService<TestDataGenerator>();
            var changeServiceRequestDataManager = TestContext.ServiceProvider.GetRequiredService<ChangeServiceRequestDataManager>();
            var billingServiceCardDataManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceCardDataManager>();
            var editBillingServiceManager = TestContext.ServiceProvider.GetRequiredService<EditBillingServiceManager>();

            var filter = new ChangeServiceRequestsFilterDto
            {
                ServiceId = Guid.NewGuid(),
                PageNumber = 1
            };

            var managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(0, managerResult.Result.ChunkDataOfPagination.Count());
            
            var createCustomService = new CreateBillingServiceCommand(TestContext,
                (serviceOwnerAccountId) => new CreateBillingServiceTest(TestContext, serviceOwnerAccountId)
                {
                    BillingServiceStatus = BillingServiceStatusEnum.OnModeration
                });

            createCustomService.Run();

            var service = DbLayer.BillingServiceRepository.FirstOrDefault(w =>
                w.Name == createCustomService.CreateBillingServiceTest.Value.Name);
            Assert.IsNotNull(service);

            var changeServiceRequestOne = testDataGenerator.GenerateChangeServiceRequest(service.Id, "test",
                ChangeRequestTypeEnum.Editing, ChangeRequestStatusEnum.Implemented);

            DbLayer.GetGenericRepository<ChangeServiceRequest>().Insert(changeServiceRequestOne);
            DbLayer.Save();

            var billingServiceCardDto = billingServiceCardDataManager.GetData(service.Id);
            Assert.IsNotNull(billingServiceCardDto);
            Assert.IsFalse(billingServiceCardDto.Error);

            var editBillingServiceDto = billingServiceCardDto.Result.MapToEditBillingServiceDto();
            editBillingServiceDto.BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            editBillingServiceDto.Name = "NewServiceName";

            var editResult = editBillingServiceManager.EditBillingService(editBillingServiceDto);
            Assert.IsFalse(editResult.Error);

            var changeServiceRequestTwo = testDataGenerator.GenerateChangeServiceRequest(service.Id, "test",
                ChangeRequestTypeEnum.Editing, ChangeRequestStatusEnum.Rejected);

            var changeServiceRequestThree = testDataGenerator.GenerateChangeServiceRequest(service.Id, "test",
                ChangeRequestTypeEnum.Editing);

            DbLayer.GetGenericRepository<ChangeServiceRequest>().Insert(changeServiceRequestTwo);
            DbLayer.GetGenericRepository<ChangeServiceRequest>().Insert(changeServiceRequestThree);
            DbLayer.Save();

            filter.ServiceId = service.Id;
            filter.Status = ChangeRequestStatusEnum.Rejected;
            managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(1, managerResult.Result.ChunkDataOfPagination.Count());

            filter.Status = ChangeRequestStatusEnum.OnModeration;
            managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(2, managerResult.Result.ChunkDataOfPagination.Count());

            filter.Status = null;
            filter.ChangeRequestType = ChangeRequestTypeEnum.Editing;
            managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(4, managerResult.Result.ChunkDataOfPagination.Count());

            filter.ChangeRequestType = ChangeRequestTypeEnum.Creation;
            managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(1, managerResult.Result.ChunkDataOfPagination.Count());

            filter.ChangeRequestType = null;
            managerResult = changeServiceRequestDataManager.GetChangeServiceRequests(filter);
            Assert.IsFalse(managerResult.Error);
            Assert.AreEqual(5, managerResult.Result.ChunkDataOfPagination.Count());
        }
    }
}
