﻿using Clouds42.AccountDatabase.Contracts.Audit.Interfaces;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Scenarios.AccountResourcesAudit
{
    /// <summary>
    /// Тест получения всех типов пропущенных услуг ресурсов для аккаунтов
    /// Сценарий:
    ///         1. Создаем аккаунт и сервис
    ///         2. Создаем второй аккаунт и подлключаем ему этот сервис
    ///         3. Получаем аккаунты, у которых отсутсвуют ресурсы (Должен вернуться пустой список)
    ///         4. Подтвержаем подписку и оплачиваем сервис для второго аккаунта
    ///         5. Удаляем один ресурс у второго аккаунта
    ///         6. Получаем аккаунты, у которых отсутсвуют ресурсы (В списке должен быть второй аккаунт)
    /// </summary>
    public class CheckAccountResourcesSamplingTest : ScenarioBase
    {
        private readonly IAccountResourceIntegralityAuditProvider _accountResourcesAuditProvider;
        private readonly CreateOrEditBillingServiceHelper _billingServiceHelper;
        private readonly BillingServiceManager _billingServiceManager;
        private readonly CreatePaymentManager _createPaymentManager;

        public CheckAccountResourcesSamplingTest()
        {
            _accountResourcesAuditProvider = TestContext.ServiceProvider.GetRequiredService<IAccountResourceIntegralityAuditProvider>();
            _billingServiceHelper = TestContext.ServiceProvider.GetRequiredService<CreateOrEditBillingServiceHelper>();
            _billingServiceManager = TestContext.ServiceProvider.GetRequiredService<BillingServiceManager>();
            _createPaymentManager = TestContext.ServiceProvider.GetRequiredService<CreatePaymentManager>();
        }

        public override void Run()
        {
            #region Создание аккаунта и сервиса

            var account = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = Clouds42Service.MyEnterprise
                }
            });
            account.Run();

            var createIndustryCommand = new CreateIndustryCommand(TestContext);
            createIndustryCommand.Run();

            CreateDelimiterTemplate();

            var firstServiceType = _billingServiceHelper.GenerateBillingServiceType("firstServiceType");
            var secondServiceType = _billingServiceHelper.GenerateBillingServiceType("secondServiceType");

            firstServiceType.ServiceTypeRelations = [secondServiceType.Id];

            var billingService = new CreateEmptyBillingServiceCommand(TestContext, new CreateBillingServiceTest(TestContext)
            {
                BillingServiceTypes =
                [
                    firstServiceType,
                    secondServiceType
                ]
            });
            billingService.Run();

            #endregion

            #region Создание второго аккаунта с подключенным сервисом

            var createAccount = new CreateAccountCommand(TestContext, new AccountRegistrationModelTest
            {
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudServiceId = billingService.Id
                }
            });
            createAccount.Run();

            #endregion

            var missingResources = _accountResourcesAuditProvider.GetListOfMissingResources();
            Assert.IsFalse(missingResources.Any());

            #region Подтверждение подписки

            _billingServiceManager.ApplyDemoServiceSubscribe(billingService.Id, createAccount.AccountId, false);

            var sad = DbLayer.ResourceConfigurationRepository.FirstOrDefault(w =>
                w.AccountId == createAccount.AccountId && w.BillingServiceId == billingService.Id);
            sad.ExpireDate = DateTime.Now.AddDays(-1);
            DbLayer.ResourceConfigurationRepository.Update(sad);
            DbLayer.Save();

            _createPaymentManager.AddPayment(new PaymentDefinitionDto
            {
                Account = createAccount.AccountId,
                Date = DateTime.Now,
                Description = "Зачисление денег",
                BillingServiceId = billingService.Id,
                System = PaymentSystem.ControlPanel,
                Status = PaymentStatus.Done,
                OperationType = PaymentType.Inflow,
                Total = firstServiceType.ServiceTypeCost + secondServiceType.ServiceTypeCost,
                OriginDetails = PaymentSystem.ControlPanel.ToString()
            });

            #endregion

            #region Удаление ресурса

            var resource = DbLayer.ResourceRepository.FirstOrDefault(r =>
                r.Subject == createAccount.AccountAdminId && r.BillingServiceTypeId == secondServiceType.Id);

            DbLayer.ResourceRepository.Delete(resource);
            DbLayer.Save();

            #endregion

            missingResources = _accountResourcesAuditProvider.GetListOfMissingResources();
            Assert.IsTrue(missingResources.Any());
            
            foreach (var accountBillingServiceTypesModel in missingResources)
            {
                Assert.AreEqual(createAccount.AccountId, accountBillingServiceTypesModel.AccountId);
                Assert.IsTrue(accountBillingServiceTypesModel.BillingServiceTypes.Any(w => w.BillingServiceTypeName == secondServiceType.Name));
            }
        }
    }
}
