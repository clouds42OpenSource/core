﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;

namespace Clouds42.Scenarios.Tests.Libs.Access
{
    /// <summary>
    /// Фейковый access провайдер
    /// </summary>
    public class SystemFakeAccessProvider : ISystemAccessProvider
    {
        /// <summary>
        /// Контекст базы
        /// </summary>
        private readonly IUnitOfWork _dbLayer;

        /// <summary>
        /// Конструктор для класса <see cref="SystemFakeAccessProvider"/>
        /// </summary>
        /// <param name="dbLayer">Контекст базы</param>
        public SystemFakeAccessProvider(IUnitOfWork dbLayer)
        {
            _dbLayer = dbLayer;
        }

        /// <summary>
        /// Получить логин аутентифицированного пользователя
        /// </summary>
        /// <returns>Логин аутентифицированного пользователя</returns>
        protected string GetUserIdentity()
            => null;

        /// <summary>
        /// Получить аутентифицированного пользователя
        /// </summary>
        /// <returns>Аутентифицированный пользователь</returns>
        public IUserPrincipalDto GetUser()
        {
            var user = _dbLayer.AccountUsersRepository.FirstOrDefault() 
                       ?? throw new NotFoundException("Не удалось получить первого попавшегося пользователя.");

            return new AccountUserPrincipalDto
            {
                Id = user.Id,
                Name = Name,
                Groups = []
            };
        }

        public bool IsCurrentContextAccount()
        {
           return true;
        }

        /// <summary>
        /// Получить от какого клиента пришёл запрос
        /// </summary>
        /// <returns>Клиент от которого пришёл запрос</returns>
        public string GetUserAgent()
            => null;

        /// <summary>
        /// Полуить адресс запроса
        /// </summary>
        /// <returns>адресс запроса</returns>
        public string GetUserHostAddress()
            => null;

        /// <summary>
        /// Получить сейлс менеджеров
        /// </summary>
        /// <returns>Сейлс менеджеры</returns>
        public List<AccountUser> GetSalesManagers()
            => [];

        /// <summary>
        /// Получить группы <see cref="AccountUserGroup"/> для переданного пользователя
        /// </summary>
        /// <param name="accountUserId">Для какого пользователя получить группы <see cref="AccountUserGroup"/> </param>
        /// <returns>Группы <see cref="AccountUserGroup"/> для переданного пользователя</returns>
        public List<AccountUserGroup> GetGroups(Guid accountUserId)
            => [];

        /// <summary>
        /// Получение имени аутентифицированного пользователя
        /// </summary>
        public string Name => "Core system";

        /// <summary>
        /// Номер аккаунта текущего контекста
        /// </summary>
        public string ContextAccountIndex { get; }

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public void HasAccess(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null,
            Func<bool> optionalCheck = null)
        {
            // заглушка
        }

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает <code>true</code> если доступ есть</returns>
        public bool HasAccessBool(ObjectAction objectAction, Func<Guid?> getAccountId = null,
            Func<Guid?> getAccountUserId = null,
            Func<bool> optionalCheck = null)
            => true;

        public Task HasAccessAsync(ObjectAction objectAction, Guid? getAccountId = null, Func<Task<Guid?>> getAccountUserId = null,
            Func<Task<bool>> optionalCheck = null)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// ID аккаунта текущего контекста
        /// </summary>
        public Guid ContextAccountId { get; }

        /// <summary>
        /// Название аккаунта текущего контекста
        /// </summary>
        public string ContextAccountName { get; }

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает <code>true</code>, если доступ есть</returns>
        public bool HasAccessForMultiAccountsBool(ObjectAction objectAction)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public void HasAccessForMultiAccounts(ObjectAction objectAction)
        {
            // заглушка
        }

        /// <summary>
        /// Получить админов AccountUserId для переданного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого получить админов</param>
        /// <returns>AccountUserId полученных админов</returns>
        public List<Guid> GetAccountAdmins(Guid accountId)
        {
            var account = _dbLayer.AccountsRepository.FirstOrDefault(acc => acc.Id == accountId);

            if (account == null)
                throw new NotFoundException($"Не удалось найти аккаунт по Id {accountId}");

            var accountAdmins = account.AccountUsers.Where(
                    usr => usr.AccountUserRoles
                        .Any(role => role.AccountUserGroup == AccountUserGroup.AccountAdmin))
                .Select(usr => usr.Id).ToList();

            if (!accountAdmins.Any())
                throw new NotFoundException($"Не найдено ни одного админа аккаунта {accountId}");

            return accountAdmins;
        }

        public void SetCurrentContextAccount(IResponseCookies cookies)
        {
            throw new NotImplementedException();
        }

        public Guid? GetContextAccountId(IRequestCookieCollection cookie)
        {
            throw new NotImplementedException();
        }

        public bool SetContextAccount(Guid accountId)
        {
            throw new NotImplementedException();
        }

        public Guid? AccountIdByAccountDatabase(Guid accountDatabaseId)
        {
            throw new NotImplementedException();
        }

        public List<string> GetEmailAdmin()
        {
            throw new NotImplementedException();
        }

        public void SetHttpContext(HttpContext context)
        {
            throw new NotImplementedException();
        }

        public Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new NotImplementedException();
        }
    }
}
