﻿using Clouds42.BLL.Common.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Access
{
    /// <summary>
    /// Фейковый провайдер доступа для тестов
    /// </summary>
    public class FakeAccessProviderTest(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : FakeAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping);
}
