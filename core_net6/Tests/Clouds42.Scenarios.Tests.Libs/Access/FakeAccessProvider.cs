﻿using System;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Access
{
    /// <summary>
    /// Фейковый провайдер доступа
    /// </summary>
    public class FakeAccessProvider : BaseAccessProvider
    {
        private readonly Lazy<AccountUser> _accountUser;

        private readonly IUnitOfWork _dbLayer;

        public FakeAccessProvider(IUnitOfWork dbLayer, ILogger42 logger, IConfiguration configuration, IHandlerException handlerException, IAccessMapping accessMapping)
            : base(dbLayer, logger, configuration, handlerException, accessMapping)
        {
            _accountUser = new Lazy<AccountUser>(() => dbLayer.AccountUsersRepository.FirstOrDefault());
            _dbLayer = dbLayer;
        }

        public FakeAccessProvider(Guid accountUserId, IUnitOfWork dbLayer, ILogger42 logger, IConfiguration configuration, IHandlerException handlerException, IAccessMapping accessMapping)
            : base(dbLayer, logger, configuration, handlerException, accessMapping)
        {
            _accountUser = new Lazy<AccountUser>(() => dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accountUserId));
            _dbLayer = dbLayer;
        }

        public override void HasAccess(ObjectAction objectAction, Func<Guid?> getAccountId = null,
            Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null)
        {
            // Заглушка для тестов
        }


        public override bool HasAccessBool(ObjectAction objectAction, Func<Guid?> getAccountId = null,
            Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null)
        {
            return true;
        }

        public override void HasAccessForMultiAccounts(ObjectAction objectAction)
        {
            // Заглушка для тестов
        }

        protected override string GetUserIdentity()
            => _accountUser.Value.Id.ToString();

        public override IUserPrincipalDto GetUser()
        {
            var accountUser = _accountUser.Value;
            if (accountUser != null)
                return CreateAccountUserPrincipalObj(accountUser);

            accountUser = _dbLayer.AccountUsersRepository.FirstOrDefault();
            return accountUser == null ? 
                new AccountUserPrincipalDto(): CreateAccountUserPrincipalObj(accountUser);
        }

        public override async Task<IUserPrincipalDto> GetUserAsync()
        {
            var accountUser = _accountUser.Value;

           var userFromContext = await CreateAccountUserPrincipalObjAsync(accountUser);

           if (accountUser != null)
           {
               return userFromContext;
           }

           accountUser = await _dbLayer.AccountUsersRepository.FirstOrDefaultAsync();

           userFromContext = await CreateAccountUserPrincipalObjAsync(accountUser);

           return accountUser == null ? null : userFromContext;

        }

        public override string GetUserAgent()
            => "this is fake agent";

        public override string GetUserHostAddress()
            => "this is fake host address";

        public override string Name => _accountUser.Value?.Login ?? "";

        /// <summary>
        /// Создать объект пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Объект пользователя</returns>
        private AccountUserPrincipalDto CreateAccountUserPrincipalObj(AccountUser accountUser)
            => new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                RequestAccountId = accountUser.AccountId,
                Token = Guid.Empty,
                Groups = GetGroups(accountUser.Id),
                Name = accountUser.Login,
                ContextAccountId = accountUser.AccountId
            };

        /// <summary>
        /// Создать объект пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь</param>
        /// <returns>Объект пользователя</returns>
        private async Task<AccountUserPrincipalDto> CreateAccountUserPrincipalObjAsync(AccountUser accountUser)
            => new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                RequestAccountId = accountUser.AccountId,
                Token = Guid.Empty,
                Groups = GetGroups(accountUser.Id),
                Name = accountUser.Login,
                ContextAccountId = accountUser.AccountId
            };
    }
}
