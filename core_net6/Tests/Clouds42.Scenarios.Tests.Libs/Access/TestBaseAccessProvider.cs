﻿using System;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Access
{
    /// <summary>
    ///     AccessProvider для тестов. Использует базовые методы HasAccess и т.д.
    /// </summary>
    public class TestBaseAccessProvider : BaseAccessProvider
    {
        private Guid? _accountUser;
        private readonly IUnitOfWork _dbLayer;
        public TestBaseAccessProvider(IUnitOfWork dbLayer, ILogger42 logger, IConfiguration configuration, IHandlerException handlerException, IAccessMapping accessMapping)
            : base(dbLayer, logger, configuration, handlerException, accessMapping)
        {
            _dbLayer = dbLayer;
        }

        public TestBaseAccessProvider(Guid accountUser, IUnitOfWork dbLayer, ILogger42 logger, IConfiguration configuration, IHandlerException handlerException, IAccessMapping accessMapping)
            : base(dbLayer, logger, configuration, handlerException, accessMapping)
        {
            _accountUser = accountUser;
            _dbLayer = dbLayer;
        }

        protected override string GetUserIdentity()
            => _accountUser.ToString();

        public override IUserPrincipalDto GetUser()
        {
                if (_accountUser == null)
                {
                    _accountUser = _dbLayer.AccountUsersRepository.FirstOrDefault()?.Id;
                    if (_accountUser == null)
                        throw new InvalidOperationException("Не установлен текущий пользователь");
                }

                var aus = _dbLayer.AccountUserSessionsRepository.FirstOrDefault(x => x.AccountUserId == _accountUser);
                if (aus == null)
                    throw new InvalidOperationException($"Для пользователя {_accountUser} не найдена сессия");

                return new AccountUserPrincipalDto
                {
                    Id = aus.AccountUser.Id,
                    RequestAccountId = aus.AccountUser.AccountId,
                    Token = aus.Token,
                    Groups = GetGroups(_accountUser.Value),
                    Name = aus.AccountUser.Login,
                    ContextAccountId = aus.AccountUser.AccountId
                };
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new NotImplementedException();
        }

        public override string GetUserAgent()
        {
            return "this is fake agent";
        }

        public override string GetUserHostAddress()
        {
            return "this is fake host address";
        }
        
        public override string Name
        {
            get { return !_accountUser.HasValue ? "" : _dbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == _accountUser.Value).Login; }
        }
    }
}
