﻿using System.Linq;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Core42.Application.Features.SegmentContext.Dtos;

namespace Clouds42.Scenarios.Tests.Libs.Mappers
{
    /// <summary>
    /// Маппер моделей сегментов для тестов
    /// </summary>
    public static class CloudServicesSegmentMapperTest
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="cloudServicesSegment">Модель сегмента</param>
        /// <returns>Модель редактирования сегмента</returns>
        public static EditCloudServicesSegmentDto MapToEditCloudServicesSegmentDto(
            this SegmentExtendedDto cloudServicesSegment)
        {
            return new EditCloudServicesSegmentDto
            {
                Id = cloudServicesSegment.Id,
                Name = cloudServicesSegment.BaseData.Name,
                Description = cloudServicesSegment.BaseData.Description,
                SQLServerID = cloudServicesSegment.BaseData.SqlServerId,
                ContentServerID = cloudServicesSegment.BaseData.ContentServerId,
                Alpha83VersionId = cloudServicesSegment.BaseData.Alpha83VersionId,
                BackupStorageID = cloudServicesSegment.BaseData.BackupStorageId,
                CustomFileStoragePath = cloudServicesSegment.BaseData.CustomFileStoragePath,
                DefaultSegmentStorageId = cloudServicesSegment.BaseData.DefaultSegmentStorageId,
                EnterpriseServer82ID = cloudServicesSegment.BaseData.EnterpriseServer82Id,
                EnterpriseServer83ID = cloudServicesSegment.BaseData.EnterpriseServer83Id,
                FileStorageServersID = cloudServicesSegment.BaseData.FileStorageServersId,
                GatewayTerminalsID = cloudServicesSegment.BaseData.GatewayTerminalsId,
                ServicesTerminalFarmID = cloudServicesSegment.BaseData.ServicesTerminalFarmId,
                Stable82VersionId = cloudServicesSegment.BaseData.Stable82VersionId,
                Stable83VersionId = cloudServicesSegment.BaseData.Stable83VersionId,
                HostingName = cloudServicesSegment.BaseData.HostingName,
                DelimiterDatabaseMustUseWebService = cloudServicesSegment.BaseData.DelimiterDatabaseMustUseWebService,
                AvailableMigrationSegment = cloudServicesSegment.MigrationData.Available.Select(ms => new SegmentElementDto
                {
                    Id = ms.Key,
                    Name = ms.Value
                }).ToList(),
                SegmentFileStorageServers = cloudServicesSegment.FileStoragesData.Selected.Select(x => new SegmentElementDto { Id = x.Key, Name = x.Value }).ToList(),
                SegmentTerminalServers = cloudServicesSegment.TerminalServersData.Selected.Select(x => new DataContracts.CloudServicesSegment.CloudSegment.SegmentTerminalServerDto { Id = x.Key, Name = x.Value }).ToList()
            };
        }
    }
}
