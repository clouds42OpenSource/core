﻿using System.Linq;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Mappers
{
    /// <summary>
    /// Хелпер для мэппинга модели аккаунта
    /// </summary>
    public class AccountMapperTest
    {
        /// <summary>
        /// Полоучить модель редактирования аккаунта из доменной модели
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Модель редактирования аккаунта</returns>
        public EditAccountDto GetEditAccountDcFromAccount(Account account)
        {
            return new EditAccountDto
            {
                AccountCaption = account.AccountCaption,
                AccountEmails = account.AccountEmails.Select(em => em.Email).ToList(),
                Description = account.Description,
                Id = account.Id,
                Inn = account.AccountRequisites.Inn,
                IsVip = account.AccountConfiguration.IsVip, 
                LocaleId = account.AccountConfiguration.LocaleId
            };
        }
    }
}
