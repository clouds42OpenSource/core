﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class CloudElementViewModelTest : CloudTerminalFarmDto
    {
        public CloudElementViewModelTest()
        {
            Id = Guid.NewGuid();
            ConnectionAddress = $"{Guid.NewGuid():N}";
            Description = ConnectionAddress;
            Name = ConnectionAddress;
        }
    }
}