﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель создания sql сервера
    /// </summary>
    public class CreateCloudSqlServerModelTest : CreateCloudSqlServerDto
    {
        private readonly Random _random = new();

        public CreateCloudSqlServerModelTest()
        {
            ConnectionAddress = $"10.20.{DateTime.Now:mmssfff}.{_random.Next(0, 10000)}";
            Description = ConnectionAddress;
            Name = ConnectionAddress;
            RestoreModelType = AccountDatabaseRestoreModelTypeEnum.Mixed;
        }
    }
}
