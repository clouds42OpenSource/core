﻿using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель создания терминального шлюза
    /// </summary>
    public class CreateCloudGatewayTerminalModelTest : CreateCloudGatewayTerminalDto
    {
        public CreateCloudGatewayTerminalModelTest()
        {
            ConnectionAddress = "gw.42clouds.com";
            Description = ConnectionAddress;
            Name = ConnectionAddress;
        }
    }
}
