﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель сервера 1С:Предприятие для тестов
    /// </summary>
    public class CloudEnterpriseElementViewModelTest : EnterpriseServerDto
    {
        private readonly Random _random = new();

        public CloudEnterpriseElementViewModelTest()
        {
            Id = Guid.NewGuid();
            ConnectionAddress = $"ent1c83-prod-{DateTime.Now:mm-ss-fff}-{_random.Next(0, 10000)}";
            Description = $"Desc_{Guid.NewGuid():N}";
            Name = $"Name-{DateTime.Now:mm-ss-fff}-{_random.Next(0, 10000)}";
            Version = PlatformType.V83.ToString();            
        }
    }
}