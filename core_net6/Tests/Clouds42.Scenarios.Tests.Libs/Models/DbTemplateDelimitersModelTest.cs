﻿using Clouds42.Scenarios.Tests.Libs.Contexts;
using System;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class DbTemplateDelimitersModelTest : DbTemplateDelimiters
    {
        public DbTemplateDelimitersModelTest(ITestContext testContext, Guid templateId = default, string configId = null)
        {
            var template = testContext.DbLayer.DbTemplateRepository.FirstOrDefault(t => t.Id == templateId);

            Name = template == null ? "Бухгалтерия предприятия 3.0" : template.DefaultCaption;
            ConfigurationId = configId ?? "bp";
            TemplateId = templateId;
            ShortName = "БухгалтерияПредприятия";
            ConfigurationReleaseVersion = "3.0.67.72";
            MinReleaseVersion = "3.0.64.29";
        }
    }
}