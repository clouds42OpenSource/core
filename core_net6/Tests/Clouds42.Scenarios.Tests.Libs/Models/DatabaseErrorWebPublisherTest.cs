﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая реализация DatabaseWebPublisher с генерацией ошибок
    /// </summary>
    public class DatabaseErrorWebPublisherTest : IDatabaseWebPublisher
    {
        /// <summary>
        /// Публикация базы на IIS
        /// </summary>
        /// <param name="ibName">Название папки, в которой будет размещена база (в формате V82Name)</param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        /// <param name="accountGroupName">
        /// Название компании в формате company_XXX
        /// (необходимо при формировании web.config-а)
        /// </param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void PublishAccountDatabase(string ibName, string accountEncodeId, string ibPath, string module1CPath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Переопубликация базы на IIS
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        public void RePublishAccountDatabase(string ibName, string accountEncodeId, string ibPath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Переопубликация базы на IIS с изменением web.config файла
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void ModifyVersionPlatformInWebConfig(string ibName, string accountEncodeId, string module1CPath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Отмена доменной аутентификации
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="accountGroupName">Название группы компании</param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void DisableDomainAuthentication(string ibName, string accountEncodeId, string accountGroupName, string module1CPath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Создание .vrd-файла базы с необходимыми параметрами
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="ibPath">Физический путь информационной базы 1С</param>
        /// <param name="expansionName">Название расширения</param>   
        /// <param name="thinkClientLink">Ссылка на скачивание тонкого клиента</param>   
        public string PublishExpansion(string ibName, string accountEncodeId, string ibPath, string expansionName, string thinkClientLink)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление опубликованной базы с IIS
        /// </summary>
        /// <param name="baseName">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        public void UnpublishDatabase(string baseName, string accountEncodeId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удалить старую публикацию инф. базы
        /// </summary>
        /// <param name="acDbV82Name">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        public void RemoveOldDatabasePublication(string acDbV82Name, string accountEncodeId, Guid oldSegmentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Создание web-config-а базы с необходимыми параметрами
        /// </summary>
        /// <param name="versionTitle">Необходимая версия 1С</param>
        /// <param name="companyGroupName">Название группы компании</param>
        /// <param name="basePath">Физический путь опубликованного приложения (куда писать файл)</param>
        public void CreateWebConfigFile__(string versionTitle, string companyGroupName, string basePath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Изменение .vrd-файла базы при переопубликации
        /// </summary>
        public bool EditOrCreateVrdFile(string ibPath, string basePath, string publishUrl, string thinkClientPath)
        {
            throw new NotImplementedException();
        }
    }
}
