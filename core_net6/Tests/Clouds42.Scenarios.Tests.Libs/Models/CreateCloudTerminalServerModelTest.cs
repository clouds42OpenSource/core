﻿using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель для создания терминального сервера
    /// </summary>
    public class CreateCloudTerminalServerModelTest : CreateCloudTerminalServerDto
    {
        public CreateCloudTerminalServerModelTest()
        {
            ConnectionAddress = "10.20.1.200";
            Description = ConnectionAddress;
            Name = ConnectionAddress;
        }
    }
}
