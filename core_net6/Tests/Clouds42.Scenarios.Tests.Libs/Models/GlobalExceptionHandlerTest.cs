﻿using System;
using System.Linq.Expressions;
using Clouds42.HandlerExeption.Contract;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class GlobalExceptionHandlerTest : IHandlerException
    {
        public static Exception Exception { get; set; }

        public void Handle(Exception exception, string operationTitle, params Expression<Func<object>>[] parameters)
        {
            Exception = exception;
        }

        public void Handle(Exception exception, string operationTitle, string extraErrorMessage, params Expression<Func<object>>[] parameters)
            => Handle(exception, operationTitle);

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">полный стек ошибки</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        public void HandleWarning(string errorMessage, string operationTitle)
        {
            Exception = new Exception(errorMessage);
        }

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">Текст предупреждения.</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        public void HandleWarning(string errorMessage, string operationTitle, params Expression<Func<object>>[] parameters)
        {
            Exception = new Exception(errorMessage);
        }
    }
}
