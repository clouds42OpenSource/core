﻿using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands.CommandResults;
using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.DataModels;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Детальная информация об аккаунте
    /// </summary>
    public class AccountDetailsTest : IAccountDetails
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id админа аккаунта
        /// </summary>
        public Guid AccountAdminId { get; set; }

        /// <summary>
        /// Токен
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Модель аккаунта
        /// </summary>
        public Account Account { get; set; }

        /// <summary>
        /// Сегмент аккаунта
        /// </summary>
        public ISegmentDetails Segment { get; set; }

        /// <summary>
        /// Объект AccessProvider
        /// </summary>
        public IAccessProvider AccessProvider { get; set; }
    }
}
