﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.YookassaAggregator.Сonstants;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель с финальным объектом платежа Юкасса
    /// </summary>
    public class YookassaFinalPaymentObjectTestDto : YookassaFinalPaymentObjectDto
    {
        public YookassaFinalPaymentObjectTestDto(Guid paymentId, bool isPaid, decimal paymentSum,
            PaymentStatus paymentStatus, YookassaPaymentMethodTestDto yookassaPaymentMethodTest)
        {
            Id = paymentId;
            Description = nameof(Test);
            CreationDate = DateTime.Now.AddMinutes(-5);
            Status = paymentStatus == PaymentStatus.Done ? "succeeded" : paymentStatus.GetStringValue();
            Test = true;
            IsPaid = isPaid;
            PaymentAmountData = new YookassaPaymentAmountDataDto
            {
                Currency = YookassaAggregatorConst.CurrencyRub,
                Amount = paymentSum
            };
            AuthorizationDetails = new YookassaPaymentAuthorizationDetailsDto
            {
                ReferenceRetrievalNumber = "618105675769",
                AuthCode = "674933"
            };
            CapturedDate = DateTime.Now.AddMinutes(-3);
            IncomeAmount = new YookassaPaymentAmountDataDto
            {
                Currency = YookassaAggregatorConst.CurrencyRub,
                Amount = paymentSum
            };
            YookassaPaymentMetadata = new YookassaPaymentMetadataDto();
            YookassaPaymentRecipientData = new YookassaPaymentRecipientDataDto
            {
                AccountId = "780876",
                GatewayId = "1819860"
            };
            PaymentMethod = yookassaPaymentMethodTest;
        }
    }
}
