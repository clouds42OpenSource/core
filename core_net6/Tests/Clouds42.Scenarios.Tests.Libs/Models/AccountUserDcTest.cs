﻿using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Scenarios.Tests.Libs.Helpers;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class AccountUserDcTest : AccountUserDto
    {        
        public AccountUserDcTest()
        {            
            AccountCaption = "Test caption";
            Login = $"{PhoneNumberGeneratorHelperTest.Generate()}{DateTime.Now:mmssfff}";
            Password = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric()).GeneratePassword();
            FirstName = nameof(Test);
            LastName = nameof(Test);
            Email = $"{Guid.NewGuid().ToString("N")}@efsol.ru";
            PhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
        }
    }
}