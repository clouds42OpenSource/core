﻿using Clouds42.Scenarios.Tests.Libs.Contexts;
using CommonLib.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Фейковый класс для работы с файлом
    /// </summary>
    public class FakeFormFile(string filename, string contentType, Stream stream)
        : FormFile(stream, 0, stream.Length, filename, filename)
    {
        /// <summary>
        /// Название поля
        /// </summary>
        private readonly string _filename = filename;
        
        /// <summary>
        /// Тип контента файла
        /// </summary>
        private readonly string _contentType = contentType;

        /// <summary>
        /// Поток
        /// </summary>
        private readonly Stream _stream = stream;

        /// <summary>
        /// Название поля
        /// </summary>
        public new string FileName => _filename;

        /// <summary>
        /// Тип контента файла
        /// </summary>
        public new string ContentType => _contentType;

        /// <summary>
        /// Длина контента
        /// </summary>
        public new int Length => (int)_stream.Length;

        /// <summary>
        /// Поток
        /// </summary>
        public new Stream InputStream => _stream;

        /// <summary>
        /// Сохранить файл
        /// </summary>
        public void SaveAs(string filename)
        {
            using var file = File.Open(filename, FileMode.CreateNew);
            _stream.CopyTo(file);
        }
    }

    /// <summary>
    /// Тестовая модель создания сервиса
    /// </summary>
    public class CreateBillingServiceTest : CreateBillingServiceDto
    {
        private readonly Guid _dependentServiceTypeId;

        public CreateBillingServiceTest(ITestContext testContext, Guid? serviceOwnerAccountId = null, bool freeService = false)
        {
            _dependentServiceTypeId = testContext.DbLayer.BillingServiceTypeRepository.FirstOrDefault(st =>
                st.SystemServiceType == ResourceType.MyEntUserWeb).Id;

            _ = testContext.DbLayer.IndustryRepository.FirstOrDefault()?.Id;

            var id = Guid.NewGuid();
            Key= id;
            BillingServiceTypes = GetBillingServiceTypes(freeService);
            BillingServiceStatus = BillingServiceStatusEnum.OnModeration;
            AccountId = serviceOwnerAccountId ?? testContext.AccessProvider.ContextAccountId;
            Name = "Тестовый сервис";
            CreateServiceRequestId = Guid.NewGuid();
        }

        /// <summary>
        /// Получить тестовые услуги
        /// </summary>
        /// <returns>Услуги</returns>
        private List<BillingServiceTypeDto> GetBillingServiceTypes(bool freeService = false)
        {
            var billingServiceTypes = new List<BillingServiceTypeDto>();
            var id = Guid.NewGuid();
            var billingServiceTypeFirst = new BillingServiceTypeDto
            {
                Id = id,
                Key = id,
                Name = "ServiceTypeOne",
                Description = "CustomServiceTypeFirst",
                BillingType = BillingTypeEnum.ForAccountUser,
                ServiceTypeCost = freeService ? 0 : 100,
                DependServiceTypeId = _dependentServiceTypeId
            };
            billingServiceTypes.Add(billingServiceTypeFirst);
            id = Guid.NewGuid();
            var billingServiceTypeSecond = new BillingServiceTypeDto
            {
                Id = id,
                Key = id,
                Name = "ServiceTypeTwo",
                Description = "CustomServiceTypeSecond",
                BillingType = BillingTypeEnum.ForAccountUser,
                ServiceTypeCost = freeService ? 0 : 300,
                ServiceTypeRelations = []
            };
            billingServiceTypes.Add(billingServiceTypeSecond);

            return billingServiceTypes;
        }
        
        /// <summary>
        /// Симуляция загрузки файла
        /// </summary>
        /// <param name="docType">Тип загруженного документа</param>
        /// <returns>Файл</returns>
        private FakeFormFile GetPostedFileSimulation(string fileName, string docType)
        {
            byte[] buffer = new byte[1024];
            Stream stream = new MemoryStream(buffer);

            return new FakeFormFile(fileName, docType, stream);
        }


    }
}
