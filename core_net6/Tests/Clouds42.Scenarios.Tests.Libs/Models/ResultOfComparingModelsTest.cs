﻿namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель результата сравнения моделей 
    /// </summary>
    public class ResultOfComparingModelsTest
    {
        /// <summary>
        /// Сходство
        /// </summary>
        public bool Similarity { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
