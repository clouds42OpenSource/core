﻿using System;
using System.IO;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая реализация DatabaseWebPublisher
    /// </summary>
    public class DatabaseWebPublisherTest(
        IUnitOfWork dbLayer,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger) : IDatabaseWebPublisher
    {
        /// <summary>
        /// Публикация базы на IIS
        /// </summary>
        /// <param name="ibName">Название папки, в которой будет размещена база (в формате V82Name)</param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        /// <param name="accountGroupName">
        /// Название компании в формате company_XXX
        /// (необходимо при формировании web.config-а)
        /// </param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void PublishAccountDatabase(string ibName, string ibPath,
            string accountGroupName, string module1CPath)
        {
            logger.Trace($"Имитация публикации инф. базы {ibName} в тестах");
        }

        /// <summary>
        /// Переопубликация базы на IIS
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        public void RePublishAccountDatabase(string ibName, string accountEncodeId, string ibPath)
        {
            logger.Trace($"Имитация переопубликации инф. базы {ibName} в тестах");
        }

        /// <summary>
        ///     Переопубликация базы на IIS с изменением web.config файла
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void ModifyVersionPlatformInWebConfig(string ibName, string accountEncodeId, string module1CPath)
        {
            logger.Trace($"Имитация изменения web.config файла инф. базы {ibName} в тестах");
        }

        /// <summary>
        /// Отмена доменной аутентификации
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="accountGroupName">Название группы компании</param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void DisableDomainAuthentication(string ibName, string accountEncodeId, string accountGroupName, string module1CPath)
        {
            logger.Trace($"Имитация отмены доменной аутентификации инф. базы {ibName} в тестах");
        }

        /// <summary>
        /// Создание .vrd-файла базы с необходимыми параметрами
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="ibPath">Физический путь информационной базы 1С</param>
        /// <param name="expansionName">Название расширения</param>   
        public string PublishExpansion(string ibName, string accountEncodeId, string ibPath, string expansionName, string thinkClientLink)
        {
            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);
            var segmentHelper = new SegmentHelper(dbLayer, cloudLocalizer);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);

            return Path.Combine(publishSiteName, accountEncodeId, ibName);
        }

        /// <summary>
        /// Удаление опубликованной базы с IIS
        /// </summary>
        /// <param name="baseName">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        public void UnpublishDatabase(string baseName, string accountEncodeId)
        {
            logger.Trace($"Имитация отмены публикации инф. базы {baseName} в тестах");
        }

        /// <summary>
        /// Удалить старую публикацию инф. базы
        /// </summary>
        /// <param name="acDbV82Name">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        public void RemoveOldDatabasePublication(string acDbV82Name, string accountEncodeId, Guid oldSegmentId)
        {
            logger.Trace($"Имитация удаления старой публикации инф. базы {acDbV82Name} в тестах");
        }

        /// <summary>
        /// Создание web-config-а базы с необходимыми параметрами
        /// </summary>
        /// <param name="versionTitle">Необходимая версия 1С</param>
        /// <param name="companyGroupName">Название группы компании</param>
        /// <param name="basePath">Физический путь опубликованного приложения (куда писать файл)</param>
        public void CreateWebConfigFile__(string versionTitle, string companyGroupName, string basePath)
        {
            logger.Trace($"Имитация создания файла web.config инф. базы {basePath} в тестах");
        }

        /// <summary>
        /// Получить инф. базу по номеру
        /// </summary>
        /// <param name="v82Name">Номер инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(string v82Name)
            => dbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.V82Name == v82Name)
               ?? throw new NotFoundException($"Инф. база по номеру {v82Name} не найдена");

        /// <summary>
        /// Получить аккаунт по ID
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Account GetAccount(Guid accountId)
            => dbLayer.AccountsRepository.FirstOrDefault(ac => ac.Id == accountId)
               ?? throw new NotFoundException($"Аккаунт по ID {accountId} не найден");


        /// <summary>
        /// Изменение .vrd-файла базы при переопубликации
        /// </summary>
        public bool EditOrCreateVrdFile(string ibPath, string basePath, string publishUrl, string thinkClientPath)
        {
            logger.Trace($"Имитация изменения .vrd-файла инф. базы {basePath} в тестах");
            return true;
        }
    }
}
