﻿using System;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.CloudServicesSegment.PublishNode;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Нода публикаций для тестов
    /// </summary>
    public class PublishNodeDtoTest : PublishNodeDto
    {
        private readonly Random _rnd = new();
        public PublishNodeDtoTest()
        {
            Description = $"TestNode{Guid.NewGuid()}";
            Address = CloudConfigurationProvider.Tests.GetCloudServicesContentServer() + $"{_rnd.Next(0, 1000)}";
        }
    }
}
