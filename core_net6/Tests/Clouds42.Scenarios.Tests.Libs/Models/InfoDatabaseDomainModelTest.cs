﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Helpers;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class InfoDatabaseDomainModelTest : InfoDatabaseDomainModelDto
    {
        private readonly DbTemplateItemDto _dbTemplate;
        public InfoDatabaseDomainModelTest(ITestContext testContext, DbTemplateItemDto templateName = null)
        {
            DataBaseName = "Тестовая база";
            DemoData = false;
            IsChecked = true;
            Publish = false;
            templateName ??= new DbTemplateModelTest(testContext.DbLayer);
            _dbTemplate = testContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>().CreateDbTemplate(templateName);
        }

        private Guid? _templateId;
        public override Guid TemplateId
        {
            get
            {
                if (_templateId.HasValue)
                    return _templateId.Value;

                return _dbTemplate.Id;
            }
            set { _templateId = value; }
        }

    }
}
