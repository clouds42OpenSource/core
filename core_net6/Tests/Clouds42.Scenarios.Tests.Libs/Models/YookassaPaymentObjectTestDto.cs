﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель объекта платежа ЮKassa
    /// </summary>
    public class YookassaPaymentObjectTestDto : YookassaPaymentObjectDto
    {
        public YookassaPaymentObjectTestDto(decimal paymentSum = 200,
            bool isPaid = false)
        {
            PaymentAmountData = new YookassaPaymentAmountDataDto
            {
                Amount = paymentSum
            };
            IsPaid = isPaid;
            Status = YookassaPaymentStatusEnum.Pending.GetStringValue();
            CreationDate = DateTime.Now;
            Id = Guid.NewGuid();
        }
    }
}