﻿namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель версий файлов разработки
    /// </summary>
    public class ServiceVersionComparerTestModel
    {
        /// <summary>
        /// Старая версия
        /// </summary>
        public string OldVersion { get; set; }

        /// <summary>
        /// Новая версия
        /// </summary>
        public string NewVersion { get; set; }

        /// <summary>
        /// Результат
        /// </summary>
        public bool Result { get; set; }
    }
}
