﻿using System;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель создания агентского соглашения
    /// </summary>
    public class CreateAgencyAgreementDtoTest : AddAgencyAgreementItemDto
    {
        public CreateAgencyAgreementDtoTest(IUnitOfWork unitOfWork)
        {
            EffectiveDate = DateTime.Now;
            Name = $"This is agreement-{new Random().Next(0, 100)}";
            ServiceOwnerRewardPercent = 100;
            Rent1CRewardPercent = 10;
            MyDiskRewardPercent = 10;
            PrintedHtmlFormId = GetPrintedAgencyAgreementDto(unitOfWork).Id;
        }

        private static PrintedHtmlForm GetPrintedAgencyAgreementDto(IUnitOfWork unitOfWork)
        {
            return unitOfWork.PrintedHtmlFormRepository
                .FirstOrDefault(phf => phf.ModelType == "Clouds42.DataContracts.AgencyAgreement.PrintedAgencyAgreementDto");
        }
    }
}
