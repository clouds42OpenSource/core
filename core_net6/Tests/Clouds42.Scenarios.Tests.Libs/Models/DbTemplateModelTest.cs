﻿using Clouds42.Common.Constants;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Constants;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель Шаблона для тестов
    /// </summary>
    public class DbTemplateModelTest : DbTemplateItemDto
    {
        public DbTemplateModelTest(IUnitOfWork unitOfWork)
        {
            Platform = PlatformType.V83;
            AdminLogin = "admin";
            Name = DbTemplatesNameConstants.EmptyName;
            DefaultCaption = "Чистый шаблон";
            Order = 5;
            CanWebPublish = true;
            LocaleId = GetRuLocale(unitOfWork).ID;
            NeedUpdate = true;
        }
        public DbTemplateModelTest(IUnitOfWork unitOfWork, string templateName)
        {
            Platform = PlatformType.V83;
            AdminLogin = "admin";
            Name = DbTemplatesNameConstants.EmptyName;
            DefaultCaption = templateName;
            Order = 5;
            CanWebPublish = true;
            LocaleId = GetRuLocale(unitOfWork).ID;
            NeedUpdate = true;
        }

        private static Locale GetRuLocale(IUnitOfWork unitOfWork)
            => unitOfWork.LocaleRepository.FirstOrDefault(w => w.Name == LocaleConst.Russia);
    }
}
