﻿using Clouds42.Scenarios.Tests.Libs.Helpers;
using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Constants;
using Clouds42.Common.Encrypt;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class AccountRegistrationModelTest : AccountRegistrationModelDto, IPrincipalUserAdapterDto
    {
        public AccountRegistrationModelTest()
        {
            AccountCaption = $"Test account{DateTime.Now:ddMMHHmmss}";
            Login = $"TestLogin{GenerateTestLoginPostfix()}";
            Password = string.IsNullOrEmpty(Password) ? new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric()).GeneratePassword() : Password;
            Email = $"TestEmail{GenerateTestLoginPostfix()}@efsol.ru";
            FirstName = $"TestFirstName{DateTime.Now:ddMMHHmmss}";
            Inn = "1234567890";
            FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
            RegistrationConfig = new RegistrationConfigDomainModelDto();
            LocaleName = LocaleConst.Russia;
        }

        public string GetUserName()
            => Login;

        public string GetDisplayName()
            => Login;

        public string PhoneNumber => FullPhoneNumber;

        /// <summary>
        /// Сгенерировать постфикс для логина пользователя
        /// </summary>
        /// <returns>Постфикс для логина пользователя</returns>
        private string GenerateTestLoginPostfix()
        {
            var guid = Guid.NewGuid().ToString();
            return guid.Split('-')[0];
        }
    }
}