﻿namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель результата проверки
    /// </summary>
    public class ResultOfCheckingModelTest
    {
        /// <summary>
        /// Успех
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
