﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель запроса на калькуляцию счета
    /// </summary>
    public class CalculationOfInvoiceRequestModelTest : CalculationOfInvoiceRequestModel
    {
        public CalculationOfInvoiceRequestModelTest()
        {
            SuggestedPayment = new SuggestedPaymentModelDto{PaymentSum = 100000 };
            EsdlLicenseYears = 1;
            EsdlPagesCount = 1000;
            NeedSendEmail = false;
        }
    }
}
