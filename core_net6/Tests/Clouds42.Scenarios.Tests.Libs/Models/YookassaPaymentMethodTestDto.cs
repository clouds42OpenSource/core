﻿using System;
using Clouds42.Billing.BillingOperations.Constants;
using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Тестовая модель метода платежа
    /// </summary>
    public class YookassaPaymentMethodTestDto : YookassaPaymentMethodDto
    {
        public YookassaPaymentMethodTestDto()
        {
            IsSaved = true;
            Type = EventOnChangeYookassaPaymentStatusConst.PaymentMethodTypeBankCard;
            PaymentTemplateId = Guid.NewGuid();
            Card = new YookassaPaymentCardDto
            {
                CardType = "Visa",
                IssuerCountry = "US",
                LastFourDigits = "6789",
                FirstSixDigits = "012345",
                ExpiryMonth = DateTime.Now.AddMonths(1).Month.ToString(),
                ExpiryYear = DateTime.Now.Year.ToString(),
                IssuerName = "TestBank"
            };
        }
    }
}
