﻿using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class CloudFilestorageElementViewModelTest : CreateCloudFileStorageServerDto
    {
        private readonly Random _random = new();

        public CloudFilestorageElementViewModelTest(string connectionAddress)
        {
            ConnectionAddress = connectionAddress;
            Description = ConnectionAddress;
            Name = "Test file storage";
            DnsName = $"Sever_Name_{_random.Next(0, 100000)}";
        }
    }
}