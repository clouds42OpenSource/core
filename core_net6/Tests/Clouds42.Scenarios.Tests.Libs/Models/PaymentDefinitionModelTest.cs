﻿using System;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Описание тестового платежа
    /// </summary>
    public class PaymentDefinitionModelTest : PaymentDefinitionDto
    {
        public PaymentDefinitionModelTest(Guid accountId)
        {
            Id = Guid.NewGuid();
            Account = accountId;
            Date = DateTime.Now;
            TransactionType = TransactionType.Money;
            OperationType = PaymentType.Inflow;
            System = PaymentSystem.Corp;
            Status = PaymentStatus.Done;
            Total = 1000;
            OriginDetails = "test";
        }
    }
}
