﻿using Clouds42.DataContracts.Service.Industry;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель отрасли тестовая
    /// </summary>
    public class IndustryModelTest : AddIndustryDataItemDto
    {
        public IndustryModelTest()
        {
            Name = "Тестовая отрасль";
            Description = "Тест";
        }
    }
}
