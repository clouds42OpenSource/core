﻿using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель параметров входа для тестов
    /// </summary>
    public class LoginModelTest : LoginModelDto
    {
        public LoginModelTest()
        {
            AccountUserLogin = "TestLogin";
        }
    }
}
