﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель серверной инфомационной базы для тестов.
    /// </summary>
    public class AccountDatabaseEnterpriseModelTest : AccountDatabaseEnterpriseModelDto
    {
        /// <summary>
        /// Признак что есть активные сеансы
        /// </summary>
        public bool HasSessions { get; set; }

        /// <summary>
        /// Признак что база существует на сервере
        /// </summary>
        public bool ExistOnServer { get; set; }
    }
}
