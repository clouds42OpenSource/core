﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель ресурсов аккаунта по услуге
    /// </summary>
    public class AccountResourcesByBillingTypeModelTest
    {
        /// <summary>
        /// Модель ресурса
        /// </summary>
        public Resource Resource { get; set; }

        /// <summary>
        /// Модель услуги
        /// </summary>
        public BillingServiceType ServiceType{ get; set; }
    }
}
