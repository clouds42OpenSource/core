﻿using System;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Scenarios.Tests.Libs.Helpers;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    /// Модель добавления пользователя к аккаунту для тестов
    /// </summary>
    public class AccountUserRegistrationToAccountTest : AccountUserRegistrationToAccountModel
    {
        public AccountUserRegistrationToAccountTest()
        {
            AccountId = Guid.Empty;
            AccountIdString = "";
            Email = "UserTestEmail@efsol.ru";
            FirstName = "UserTestName";
            Login = $"UserTestLogin{DateTime.Now:mmss}";
            Password = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric()).GeneratePassword();
            FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}";
        }
    }
}