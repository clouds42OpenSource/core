﻿using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    /// <summary>
    ///     Тестовая модель данных для вью модели Сервера публикаций
    /// </summary>
    public class CloudContentServerElementViewModelTest : CreateCloudContentServerDto 
    {
        public CloudContentServerElementViewModelTest()
        {
            Description = "10.20.1.200";
            Name = "10.20.1.200";
            PublishSiteName = "beta-web-1c.42clouds.com";
            GroupByAccount = true;
        }
    }
}
