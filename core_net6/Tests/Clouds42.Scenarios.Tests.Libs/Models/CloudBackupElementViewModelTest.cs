﻿using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;

namespace Clouds42.Scenarios.Tests.Libs.Models
{
    public class CloudBackupElementViewModelTest : CreateCloudBackupStorageDto
    {
        public CloudBackupElementViewModelTest()
        {
            ConnectionAddress = "10.20.1.200";
            Description = ConnectionAddress;
            Name = ConnectionAddress;
        }

    }
}