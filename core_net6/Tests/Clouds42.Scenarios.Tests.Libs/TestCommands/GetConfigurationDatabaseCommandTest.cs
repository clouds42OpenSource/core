﻿using System;
using System.Collections.Generic;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// Тестовая комманда получения списка конфигураций информационных баз
    /// </summary>
    public class GetConfigurationDatabaseCommandTest : IGetConfigurationDatabaseCommand
    {

        public static List<string> ListConfig;
        /// <summary>
        /// Выполнить запрос в МС для получения списка конфигураций информационных баз для сервиса
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public List<String> Execute(Guid serviceId)
        {
            if (ListConfig == null)
                return ["bp"];
            return ListConfig;
        }

        public void SetListConfiguration(List<string> listconf ) 
        {
            ListConfig = listconf;
        }
    }
}
