﻿using System;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// Тестовая команда на удаление баз на разделителях из МС
    /// </summary>
    public class DeleteDelimiterCommandTestAdapter : IDeleteDelimiterCommand
    {
        /// <summary>
        /// Выполнения запроса на удаление базы на разделителях в МС
        /// </summary>
        public string Execute(Guid accountDatabaseId,string locale, string type = "manual")
        {
            return String.Empty;

        }
    }
}
