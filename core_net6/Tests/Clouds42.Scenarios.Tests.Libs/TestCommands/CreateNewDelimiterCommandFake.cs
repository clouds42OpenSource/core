﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using System;
using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// Команда создания инф. базы на разделителях
    /// </summary>
    public class CreateNewDelimiterCommandFake : ICreateNewDelimiterCommand
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="accountUsers">Список пользователей аккаунта</param>
        /// <param name="applicationName">Название приложения</param>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="demoData">Демо данные</param>
        public void Execute(List<IAccountUser> accountUsers, string applicationName, Guid templateId, Guid accountId, Guid accountDatabaseId,
            bool demoData)
        {
            // заглушка для тестов
        }
    }
}
