﻿using System;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// Команда для предоставления доступа в инф. базу для тестов
    /// </summary>
    public class AccessToDbOnDelimiterCommandTest : IAccessToDbOnDelimiterCommand
    {
        /// <summary>
        /// Фейк метод выполнить запрос на предоставление/удаление доступа пользователя в инф. базу
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="url">Адрес СМ.</param>
        /// <param name="corpParameters">Опционально. Список ролей пользователя.</param>
        /// <returns></returns>
        public ManageAcDbAccessResultDto Execute(Domain.DataModels.AccountDatabase database, Guid userId, string url,
            string corpParameters, bool isInstall = true) => new()
        {
            AccessState = AccountDatabaseAccessState.Done,
            ErrorMessage = string.Empty,
            UpdateStateDateTime = DateTime.Now
        };
    }
}
