﻿using System;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// Тестовая команда на удаление баз на разделителях из МС
    /// которая будет выполняться с ошибкой
    /// </summary>
    public class DeleteDelimiterCommandErrorTestAdapter : IDeleteDelimiterCommand
    {
        public string Execute(Guid accountDatabaseId, string locale, string type = "manual")
        {
            throw new InvalidOperationException(
                $"Удаление базы {accountDatabaseId} на разделителях из МС завершилось с ошибкой");
        }
    }
}
