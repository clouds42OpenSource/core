﻿using System;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.Scenarios.Tests.Libs.TestCommands
{
    /// <summary>
    /// заглушка коменды получения инфы про сервис с МС
    /// </summary>
    public class TestGetServiceInfoCommand : IGetServiceInfoCommand
    {
        public InformationForServiceDto Execute(Guid serviceId)
        {
            return null;
        }
    }
}
