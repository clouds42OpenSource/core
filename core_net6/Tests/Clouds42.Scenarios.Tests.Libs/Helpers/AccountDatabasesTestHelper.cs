﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Data.Interfaces;
using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания и редактирования инф. баз аккаунта для тестов
    /// </summary>
    public class AccountDatabasesTestHelper(ITestContext testContext)
    {
        private readonly CreateDatabaseFromTemplateTestAdapter _createDatabaseFromTemplateTestAdapter = testContext.ServiceProvider.GetRequiredService<CreateDatabaseFromTemplateTestAdapter>();
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
        private readonly CreateDbTemplateTestHelper _createDbTemplateTestHelper = testContext.ServiceProvider.GetRequiredService<CreateDbTemplateTestHelper>();
        private readonly DbTemplatesManager _dbTemplatesManager = testContext.ServiceProvider.GetRequiredService<DbTemplatesManager>();
        private readonly IGetAccountDatabasesDataProcessor _getAccountDatabasesDataProcessor = testContext.ServiceProvider.GetRequiredService<IGetAccountDatabasesDataProcessor>();
        private readonly CreateDbTemplateDelimitersTestHelper _createDbTemplateDelimitersTestHelper = new(testContext);

        /// <summary>
        /// Шаблон названия инф. базы для создания
        /// </summary>
        public static string DbNamePattern => "Test database";

        /// <summary>
        /// Создать инф. базы аккаунта по заданному количеству
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbCount">Количество баз для создания</param>
        /// <returns>Список Id созданных инф. баз</returns>
        public List<Guid> CreateAccountDatabases(Guid accountId, int dbCount = 1, string templateName = null)
        {
            var databases = templateName is not null
                ? GetDatabasesModel(accountId, dbCount, templateName)
                : GetDatabasesModel(accountId, dbCount);

            var createDatabaseResult = _createDatabaseFromTemplateTestAdapter.CreateDatabases(accountId, databases);

            if(!createDatabaseResult.Complete || !createDatabaseResult.Ids.Any())
                throw new InvalidOperationException($"Не удалось создать инф. базы '{createDatabaseResult.Comment}'");

            if (createDatabaseResult.Ids.Count != dbCount)
                throw new InvalidOperationException($"Создалось баз - {createDatabaseResult.Ids.Count} вместо - {dbCount}");

            return createDatabaseResult.Ids;
        }

        /// <summary>
        /// Создать инф. базу на разделителях
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="confId">Id конфигурации</param>
        public void CreateDbOnDelimiters(Guid databaseId, string confId)
        {
            _createDbTemplateDelimitersTestHelper.CreateDbTemplateDelimiters(_createDbTemplateTestHelper.GetDbTemplateTestName(1), confId);

            var model = new AccountDatabaseOnDelimiters
            {
                AccountDatabaseId = databaseId,
                LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                DbTemplateDelimiterCode = confId,
                Zone = 1,
                DatabaseSourceId = databaseId
            };

            _dbLayer.AccountDatabaseDelimitersRepository.Insert(model);
            _dbLayer.Save();
        }

        /// <summary>
        /// Изменить статус инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="databaseState">Новый статус инф. базы</param>
        /// <returns>Признак успешного изменения</returns>
        public bool ChangeDatabaseState(Guid databaseId, DatabaseState databaseState) =>
            ChangeDatabasesState([databaseId], databaseState);

        /// <summary>
        /// Изменить статус инф. баз
        /// </summary>
        /// <param name="databaseIds">Список Id инф. баз</param>
        /// <param name="databaseState">Новый статус инф. базы</param>
        /// <returns>Признак успешного изменения</returns>
        public bool ChangeDatabasesState(List<Guid> databaseIds, DatabaseState databaseState)
        {
            try
            {
                _dbLayer.DatabasesRepository
                    .AsQueryable()
                    .Where(x => databaseIds.Contains(x.Id))
                    .ExecuteUpdate(x => x.SetProperty(y => y.State, databaseState.ToString()));

                return true;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Создать доступ к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localUserId">Id локального пользователя</param>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        /// <returns>Объект доступа</returns>
        public AcDbAccess CreateAcDbAccess(Guid accountDatabaseId, Guid accountId, Guid localUserId, Guid accountUserId)
        {
            var accessEntity =
                CreateAcDbAccessObjHelper.CreateAcDbAccessObj(accountDatabaseId, accountId, localUserId, accountUserId);

            _dbLayer.AcDbAccessesRepository.Insert(accessEntity);
            _dbLayer.Save();

            return accessEntity;
        }

        /// <summary>
        /// Проверить корретно ли получились данные инф. баз аккаунта по фильтру
        /// </summary>
        /// <param name="accountDatabasesFilterParamsDto">Фильтр для получения инф. баз</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <param name="requiredDbsCount">Количество подходящих инф. баз аккаунта</param>
        public void GetAndCheckDatabasesDataByFilter(AccountDatabasesFilterParamsDto accountDatabasesFilterParamsDto, string errorMessage, int requiredDbsCount)
        {
            var databasesData =
                _getAccountDatabasesDataProcessor.GetDatabasesDataByFilter(accountDatabasesFilterParamsDto);

            if (databasesData.Records.Length != requiredDbsCount)
                throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Получить модель инф. базы для создания
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="dbCount">Количество инф. баз для создания</param>
        /// <returns>Список моделей созданных инф. баз</returns>
        private List<InfoDatabaseDomainModelDto> GetDatabasesModel(Guid accountId, int dbCount, string templateName = null)
        {
            
            var result = new List<InfoDatabaseDomainModelDto>();
            var dbTemplate = templateName is not null
                ? _dbTemplatesManager.GetDbTemplateByName(_createDbTemplateTestHelper.CreateDbTemplate(new DbTemplateModelTest(_dbLayer, templateName)).Name).Result
                : _dbTemplatesManager.GetDbTemplateByName(_createDbTemplateTestHelper.CreateDbTemplate().Name).Result;

            CreateConfigurationDbTemplateRelations(dbTemplate.Id);

            for (var i = 1; i <= dbCount; i++)
                result.Add(new InfoDatabaseDomainModelTest(testContext)
                {
                    DataBaseName = $"{DbNamePattern} {i}",
                    DemoData = false,
                    IsChecked = true,
                    Publish = false,
                    TemplateId = dbTemplate.Id,
                    AccountId = accountId
                });

            return result;
        }

        /// <summary>
        /// Создать зависимость шаблона и конфигурации 1С
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона</param>
        private void CreateConfigurationDbTemplateRelations(Guid dbTemplateId)
        {
            var existingDbTemplateRelation = _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>()
                .FirstOrDefault(confRel => confRel.DbTemplateId == dbTemplateId);

            if (existingDbTemplateRelation != null)
                return;

            var configuration = _dbLayer.Configurations1CRepository.FirstOrDefault() ??
                                throw new NotFoundException("Не удалось найти конфигурацию 1С");

            var newConfigurationDbTemplateRelation = new ConfigurationDbTemplateRelation
            {
                DbTemplateId = dbTemplateId,
                Configuration1CName = configuration.Name
            };

            _dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().Insert(newConfigurationDbTemplateRelation);
            _dbLayer.Save();

        }
    }
}
