﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    /// Хелпер для проверок связанных с инф. базами для тестов
    /// </summary>
    public class AccountDatabaseCheckerTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Проверить, что инф. база создана и создана на раделителях по названию
        /// </summary>
        /// <param name="dbCaption">Название инф. базы</param>
        /// <param name="accountId">Id инф. базы</param>
        public void CheckAccountDatabaseCreatedLkAndDelimsByCaption(string dbCaption, Guid accountId)
        {
            CheckAccountDatabaseCreated(dbCaption, accountId, out var databaseId);
            CheckAccountDatabaseOnDelimitersCreated(databaseId);
        }

        /// <summary>
        /// Получить инф. базу аккаунта по названию
        /// </summary>
        /// <param name="dbCaption">Название инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель инф. базы</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabaseByCaption(string dbCaption, Guid accountId) =>
            _dbLayer.DatabasesRepository.FirstOrDefault(db => db.Caption == dbCaption && db.AccountId == accountId);


        /// <summary>
        /// Проверить, что инф. база была создана
        /// </summary>
        /// <param name="dbCaption">Название инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="databaseId">Id найденной инф. базы</param>
        public void CheckAccountDatabaseCreated(string dbCaption, Guid accountId, out Guid databaseId)
        {
            var database = GetAccountDatabaseByCaption(dbCaption, accountId);
            if(database == null)
                throw new NotFoundException($"Не удалось найти созданную базу аккаунта {accountId} по названию {dbCaption}");

            databaseId = database.Id;
        }

        /// <summary>
        /// Проверить, что инф. база создана на разделителях
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        public void CheckAccountDatabaseOnDelimitersCreated(Guid databaseId)
        {
            var databaseOnDelimiters =
                _dbLayer.AccountDatabaseDelimitersRepository.FirstOrDefault(db => db.AccountDatabaseId == databaseId);
            if(databaseOnDelimiters == null)
                throw new NotFoundException($"Не удалось найти созданную базу на разделителях по Id {databaseId}");
        }

        /// <summary>
        /// Проверить, что инф. база не создана
        /// </summary>
        /// <param name="dbCaption">Название инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        public void CheckAccountDatabaseNotCreated(string dbCaption, Guid accountId)
        {
            var database = GetAccountDatabaseByCaption(dbCaption, accountId);
            if (database != null)
                throw new NotFoundException($"Создалась инф. база с названием {dbCaption} при условии невозможности создания");
        }

    }
}
