﻿using System.IO;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    /// Хелпер для сохранения врд файла
    /// </summary>
    public static class SaveVrdFileHelper
    {
        /// <summary>
        /// Сохранить vrd файл
        /// </summary>
        /// <param name="vrd">Vrd файл</param>
        /// <param name="basePath">Физический путь опубликованного приложения (базы)</param>
        public static void SaveVrd(this string vrd, string basePath)
        {
            const string fileName = "default.vrd";
            var filePath = Path.Combine(basePath, fileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
            
            File.WriteAllText(filePath, vrd);
        }
    }
}
