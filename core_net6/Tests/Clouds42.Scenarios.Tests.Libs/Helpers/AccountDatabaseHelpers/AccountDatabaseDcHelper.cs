﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    /// Хелпер для работы с моделью
    /// отображения информационной базы
    /// </summary>
    public class AccountDatabaseDcHelper(AccountDatabaseWebPublishPathHelper accountDatabaseWebPublishPathHelper)
    {
        /// <summary>
        /// Проверить корректность моделей отображения
        /// информационных баз
        /// </summary>
        /// <param name="accountDatabases">Информационные базы</param>
        /// <param name="accountDatabasesDc">Модели отображения базы</param>
        public void CheckCorrectnessOfAccountDatabasesDc(List<Domain.DataModels.AccountDatabase> accountDatabases,
            List<AccountDatabaseDc> accountDatabasesDc)
        {
            accountDatabasesDc.ForEach(db =>
                CheckCorrectnessOfAccountDatabaseDc(accountDatabases.FirstOrDefault(w => w.Id == db.Id), db));
        }

        /// <summary>
        /// Проверить корректность модели отображения
        /// информационной базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountDatabaseDc">Модель отображения базы</param>
        private void CheckCorrectnessOfAccountDatabaseDc(
            Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseDc accountDatabaseDc)
        {
            CheckAccountDatabaseDcCommonProperties(accountDatabase, accountDatabaseDc);

            CheckAccountDatabaseDcState(accountDatabase, accountDatabaseDc);

            CheckAccountDatabaseDcPublishState(accountDatabase, accountDatabaseDc);

            if (accountDatabase.IsDelimiter() != accountDatabaseDc.IsDbOnDelimiters)
                throw new InvalidOperationException("Флаг по статусу на разделителях базы в модели не верный");
        }

        /// <summary>
        /// Проверить общие свойства в модели отображения инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountDatabaseDc">Модель отображения базы</param>
        private static void CheckAccountDatabaseDcCommonProperties(Domain.DataModels.AccountDatabase accountDatabase,
            AccountDatabaseDc accountDatabaseDc)
            => CheckConditionsEqualityOrThrowException(new List<Func<bool>>
            {
                () => accountDatabase.V82Name == accountDatabaseDc.V82Name,
                () => accountDatabase.Caption == accountDatabaseDc.Caption,
                () => accountDatabase.GetTemplateName() == accountDatabaseDc.TemplateName,
                () => TimezoneHelper.DateTimeToTimezone(accountDatabase.Account.AccountConfiguration.Locale.Name,
                          accountDatabase.LastActivityDate) ==
                      accountDatabaseDc.LastActivityDate,
                () => accountDatabase.SizeInMB == accountDatabaseDc.SizeInMB,
                () => accountDatabase.GetTemplateImgUrl() == accountDatabaseDc.TemplateImgUrl
            }, "Общие свойства модели заполнены не верно");

        /// <summary>
        /// Проверить статус в модели отображения инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountDatabaseDc">Модель отображения базы</param>
        private static void CheckAccountDatabaseDcState(Domain.DataModels.AccountDatabase accountDatabase,
            AccountDatabaseDc accountDatabaseDc)
            => CheckConditionsEqualityOrThrowException(new List<Func<bool>>
            {
                () => accountDatabase.StateEnum == accountDatabaseDc.State,
                () => accountDatabase.CreateAccountDatabaseComment == accountDatabaseDc.CreateAccountDatabaseComment,
                () => accountDatabase.IsDeleted() == accountDatabaseDc.IsDeleted
            }, "Статус в модели заполнен не верно");

        /// <summary>
        /// Проверить состояние публикации в модели отображения инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="accountDatabaseDc">Модель отображения базы</param>
        private void CheckAccountDatabaseDcPublishState(Domain.DataModels.AccountDatabase accountDatabase,
            AccountDatabaseDc accountDatabaseDc)
            => CheckConditionsEqualityOrThrowException(new List<Func<bool>>
            {
                () => accountDatabase.PublishStateEnum == accountDatabaseDc.PublishState,
                () => accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase)?.Split('%')[0] ==
                      accountDatabaseDc.WebPublishPath?.Split('%')[0]
            }, "Статус публикации или путь веб публикации базы в модели не верный");

        /// <summary>
        /// Проверить соответствие условий или выбросить исключение
        /// </summary>
        /// <param name="checkConditionsFuncList">Список функций для проверки условий</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        private static void CheckConditionsEqualityOrThrowException(IEnumerable<Func<bool>> checkConditionsFuncList,
            string errorMessage)
        {
            var result = checkConditionsFuncList.Select(func => func()).ToList();
            if (result.All(res => res))
                return;

            throw new InvalidOperationException(errorMessage);
        }
    }
}
