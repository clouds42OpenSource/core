﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    /// Хелпер для создания записей бэкапов инф. баз для тестов
    /// </summary>
    public class AccountDatabaseBackupTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Создать бэкапы инф. баз
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="initiatorId">Id пользователя инициатора</param>
        /// <param name="trigger">Триггер создания</param>
        /// <param name="creationTime">Дата создания</param>
        /// <param name="countToCreate">Количество для создания</param>
        public void CreateDbBackups(Guid databaseId, Guid initiatorId, CreateBackupAccountDatabaseTrigger trigger, DateTime creationTime,
            int countToCreate = 1)
        {
            for (var i = 1; i <= countToCreate; i++)
                CreateDbBackup(databaseId, initiatorId, trigger, creationTime);
        }

        /// <summary>
        /// Создать бэкап инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="initiatorId">Id пользователя инициатора</param>
        /// <param name="trigger">Триггер создания</param>
        /// <param name="creationTime">Дата создания</param>
        /// <returns>Id созданного бэкапа</returns>
        public Guid CreateDbBackup(Guid databaseId, Guid initiatorId, CreateBackupAccountDatabaseTrigger trigger, DateTime creationTime)
        {
            var newBackup = GetAccountDatabaseBackupModel(databaseId, initiatorId, trigger, creationTime);
            _dbLayer.AccountDatabaseBackupRepository.Insert(newBackup);
            _dbLayer.Save();

            return newBackup.Id;
        }

        /// <summary>
        /// Получить модель бэкапа инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="initiatorId">Id пользователя инициатора</param>
        /// <param name="trigger">Триггер создания</param>
        /// <param name="creationTime">Дата создания</param>
        /// <returns>Модель бэкапа инф. базы</returns>
        private AccountDatabaseBackup GetAccountDatabaseBackupModel(Guid databaseId, Guid initiatorId,
            CreateBackupAccountDatabaseTrigger trigger, DateTime creationTime) => new()
        {
            AccountDatabaseId = databaseId,
            BackupPath = databaseId.ToString(),
            CreationBackupDateTime = creationTime,
            EventTrigger = trigger,
            SourceType = AccountDatabaseBackupSourceType.Local,
            InitiatorId = initiatorId,
            Id = Guid.NewGuid()
        };
    }
}
