﻿using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Restore.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    /// Хэлпер для моделей восстановления инф. баз
    /// </summary>
    public class AccountDatabaseRestoreModelHelperTest(IUnitOfWork dbLayer) : IAccountDatabaseRestoreModelHelper
    {
        /// <summary>
        /// Получить список инф. баз для аудита и корректировки моделей восстановления
        /// </summary>
        /// <returns>Cписок инф. баз для аудита и корректировки моделей восстановления</returns>
        public List<ManageAcDbRestoreModelDto> GetAccountDatabasesForManageRestoreModel()
        => dbLayer.DatabasesRepository.AsQueryableNoTracking().Where(x => (x.IsFile == null || !x.IsFile.Value) && 
            x.AccountDatabaseOnDelimiter == null && 
            (x.State == DatabaseState.Ready.ToString() || x.State == DatabaseState.ProcessingSupport.ToString() || x.State == DatabaseState.DetachedToTomb.ToString()))
            .Select(x => new ManageAcDbRestoreModelDto
            {
                AccountDatabaseId = x.Id,
                AccountDatabaseV82Name = x.V82Name,
                AccountDatabaseRestoreModel = x.RestoreModelType,
                SqlServerRestoreModel = x.Account.AccountConfiguration.Segment.CloudServicesSQLServer.RestoreModelType
            }).ToList();

        /// <summary>
        /// Обновить тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="restoreModelType">Тип модели восстановления</param>
        public void UpdateAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase, AccountDatabaseRestoreModelTypeEnum restoreModelType)
        {
            // не используется в тестовой имплементации
        }

        /// <summary>
        /// Получает тип модели восстановления инф. базы на SQL сервере
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        public string GetAcDbRestoreModelInSqlServer(Domain.DataModels.AccountDatabase accountDatabase)
            => accountDatabase.RestoreModelType == null
                ? AccountDatabaseRestoreModelTypeEnum.Simple.ToString()
                : accountDatabase.RestoreModelType.ToString();
    }
}
