﻿using System;
using System.IO;
using System.Xml;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers
{
    /// <summary>
    ///     Тестовый хелпер для работы с файлом web.config
    /// </summary>
    public static class TestWebConfigHelper
    {
        /// <summary>
        ///     Получить значение scriptProcessor в 
        /// файле web.config
        /// </summary>
        /// <param name="basePath">Путь к файлу web.config</param>
        public static string Get1CScriptProcessorValue(string basePath)
        {
            var webConfigFilePath = Path.Combine(basePath, "web.config");
            var webXmlDoc = new XmlDocument();
            webXmlDoc.Load(webConfigFilePath);
            var xmlRoot = webXmlDoc.DocumentElement;

            if (xmlRoot == null)
                throw new InvalidOperationException($"Не удалось распарсить файл: {webConfigFilePath}");

            var elements = xmlRoot.GetElementsByTagName("add");

            var scriptProcessorValue = "";
            foreach (var element in elements)
            {
                var node = (XmlNode)element;
                if (node.Attributes?["name"]?.Value == "1C Web-service Extension" &&
                    node.Attributes?["scriptProcessor"] != null)
                    scriptProcessorValue = node.Attributes["scriptProcessor"].Value;
            }

            return scriptProcessorValue;
        }
    }
}
