﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers
{
    /// <summary>
    ///     Провайдер создания и выполнения тасок по публикации базы для тестов
    /// </summary>
    public class TestExecuteDatabasePublishTasksProvider(
        IUnitOfWork dbLayer,
        TestRepublishDatabaseJobWrapper testRepublishDatabaseJobWrapper,
        TestPublishDatabaseJobWrapper testPublishDatabaseJobWrapper,
        TestCancelPublishDatabaseJobWrapper testCancelPublishDatabaseJobWrapper,
        TestRepublishChangingConfigJobWrapper testRepublishChangingConfigJobWrapper,
        TestRestartAccountApplicationPoolOnIisJobWrapper testRestartAccountApplicationPoolOnIisJobWrapper,
        TestRemoveOldPublicationJobWrapper testRemoveOldPublicationJobWrapper)
        : IExecuteDatabasePublishTasksProvider
    {
        /// <summary>
        ///     Создать и выполнить таску по переопубликации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        public IWorkerTask CreateAndStartRepublishTask(Domain.DataModels.AccountDatabase database)
        {
            ChangePublishStateToInPendingPublication(database);

            return testRepublishDatabaseJobWrapper.Start(new PublishOrCancelPublishDatabaseJobParamsDto
            {
                AccountDatabaseId = database.Id
            });
        }

        /// <summary>
        ///     Создать и выполнить таску по публикации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        public IWorkerTask CreateAndStartPublishTask(Domain.DataModels.AccountDatabase database)
        {
            ChangePublishStateToInPendingPublication(database);

            return testPublishDatabaseJobWrapper.Start(new PublishOrCancelPublishDatabaseJobParamsDto
            {
                AccountDatabaseId = database.Id
            });
        }

        /// <summary>
        ///     Создать и выполнить таску по снятию с публикации базы
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        public IWorkerTask CreateAndStartCancelPublishTask(Domain.DataModels.AccountDatabase database)
        {
            ChangePublishStateToInPendingUnpublication(database);

            return testCancelPublishDatabaseJobWrapper.Start(new PublishOrCancelPublishDatabaseJobParamsDto
            {
                AccountDatabaseId = database.Id
            });
        }

        /// <summary>
        /// Создать и выполнить задачу удаления старой публикации инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        /// <returns>Обработчик задачи</returns>
        public IWorkerTask CreateAndStartRemoveOldPublicationJob(Guid accountDatabaseId, Guid oldSegmentId)
        {
            return testRemoveOldPublicationJobWrapper.Start(new RemoveOldPublicationJobParamsDto
            {
                AccountDatabaseId = accountDatabaseId,
                OldSegmentId = oldSegmentId
            });
        }

        /// <summary>
        ///     Создать и выполнить таску по переопубликации базы
        /// с изменением файла web.config
        /// </summary>
        /// <param name="database">Идентификатор базы</param>
        public IWorkerTask CreateAndStartRepublishWebConfigTask(Domain.DataModels.AccountDatabase database)
        {
            ChangePublishStateToInPendingPublication(database);

            return testRepublishChangingConfigJobWrapper.Start(new PublishOrCancelPublishDatabaseJobParamsDto
            {
                AccountDatabaseId = database.Id
            });
        }

        /// <summary>
        ///     Создать и выполнить таску по перезапуску пула приложения
        /// на нодах публикций
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public IWorkerTask CreateAndStartRestartApplicationPoolTask(Guid accountDatabaseId)
        {
            ChangePublishStateToRestartingPool(accountDatabaseId);

            return testRestartAccountApplicationPoolOnIisJobWrapper.Start(new RestartAccountApplicationPoolOnIisJobParamsDto
            {
                AccountDatabaseId = accountDatabaseId
            });
        }

        /// <summary>
        ///     Сменить статус публикации на "В процессе публикации"
        /// </summary>
        /// <param name="database">База</param>
        private void ChangePublishStateToInPendingPublication(Domain.DataModels.AccountDatabase database)
        {
            database.PublishStateEnum = PublishState.PendingPublication;
            dbLayer.DatabasesRepository.Update(database);
            dbLayer.Save();
        }

        /// <summary>
        ///     Сменить статус публикации на "В процессе снятия публиикации"
        /// </summary>
        /// <param name="database">База</param>
        private void ChangePublishStateToInPendingUnpublication(Domain.DataModels.AccountDatabase database)
        {
            database.PublishStateEnum = PublishState.PendingUnpublication;
            dbLayer.DatabasesRepository.Update(database);
            dbLayer.Save();
        }

        /// <summary>
        /// Сменить статус публикации на "В процессе перезапуска пула"
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        private void ChangePublishStateToRestartingPool(Guid accountDatabaseId)
        {
            var database = dbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == accountDatabaseId);
            database.PublishStateEnum = PublishState.RestartingPool;
            dbLayer.DatabasesRepository.Update(database);
            dbLayer.Save();
        }
    }
}
