﻿using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers
{
    /// <summary>
    /// Обработчик задачи удаления старой публикации инф. базы для тестов
    /// </summary>
    public class TestRemoveOldPublicationJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        PublishHelper publishHelper)
        : SynchronizationParamsJobWrapperBase<RemoveOldPublicationJob, RemoveOldPublicationJobParamsDto>(dbLayer,
            registerTaskInQueueProvider)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик задачи</returns>
        public override TypingParamsJobWrapperBase<RemoveOldPublicationJob, RemoveOldPublicationJobParamsDto> Start(RemoveOldPublicationJobParamsDto paramsJob)
        {
            publishHelper.RemoveOldDatabasePublication(paramsJob.AccountDatabaseId, paramsJob.OldSegmentId);
            return this;
        }

        /// <summary>
        /// Создать ключ синхронизации
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Ключ синхронизации</returns>
        protected override string CreateSynchronizationKey(RemoveOldPublicationJobParamsDto paramsJob)
            => TaskSynchronizationKeyDto.PublishAccountDatabases.CreateKey(paramsJob.AccountDatabaseId);

        /// <summary>
        /// Дождаться выполнения задачи
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override IWorkerTaskResult Wait(int timeoutInMinutes = 20)
            => null;
    }
}
