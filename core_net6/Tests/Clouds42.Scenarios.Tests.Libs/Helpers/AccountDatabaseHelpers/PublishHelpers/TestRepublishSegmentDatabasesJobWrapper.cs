﻿using System.Linq;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.CoreWorker.JobWrappers.PublishDatabase;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers
{
    /// <summary>
    ///     Тест класс обертка для перепубликации баз, без использования воркера
    /// </summary>
    public class TestRepublishSegmentDatabasesJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork unitOfWork,
        PublishHelper publishHelper)
        : ParametrizationJobWrapperBase<RepublishSegmentDatabasesJobParamsDto>(registerTaskInQueueProvider, unitOfWork), IRepublishSegmentDatabasesJobWrapper
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        protected override CoreWorkerTaskType TaskType { get; }

        public override ParametrizationJobWrapperBase<RepublishSegmentDatabasesJobParamsDto> Start(RepublishSegmentDatabasesJobParamsDto paramsJob)
        {
            var databases = _unitOfWork.DatabasesRepository.GetPublishedDbs(paramsJob.SegmentId,
                platformType: paramsJob.PlatformType, distributionType: paramsJob.DistributionType).ToList();

            foreach (var database in databases)
            {
                publishHelper.ModifyVersionPlatformInWebConfig(database.Id);
            }
            return this;
        }

        public override IWorkerTaskResult Wait(int timeoutInMinutes = 20)
        {
            return null;
        }
    }
}
