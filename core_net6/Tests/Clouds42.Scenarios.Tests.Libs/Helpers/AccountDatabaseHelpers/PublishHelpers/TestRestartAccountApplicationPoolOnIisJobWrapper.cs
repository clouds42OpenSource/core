﻿using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Logger;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.AccountDatabase.Publishes.Managers;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers
{
    /// <summary>
    ///     Задача по перезапуску пула приложения аккаунта на нодах публикаций 
    /// </summary>
    public class TestRestartAccountApplicationPoolOnIisJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        DatabaseIisApplicationPoolManager databaseIisApplicationPoolManager)
        : SynchronizationParamsJobWrapperBase<RestartAccountApplicationPoolOnIisJob,
            RestartAccountApplicationPoolOnIisJobParamsDto>(dbLayer, registerTaskInQueueProvider)
    {
        /// <summary>
        /// Запустить таску перезапуска пула аккаунта на нодах публикаций
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<RestartAccountApplicationPoolOnIisJob, RestartAccountApplicationPoolOnIisJobParamsDto> Start(RestartAccountApplicationPoolOnIisJobParamsDto paramsJob)
        {
            databaseIisApplicationPoolManager.RestartIisApplicationPool(paramsJob.AccountDatabaseId);
            return this;
        }

        /// <summary>
        /// Создать ключ синхронизации
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Ключ синхронизации</returns>
        protected override string CreateSynchronizationKey(RestartAccountApplicationPoolOnIisJobParamsDto paramsJob)
            => TaskSynchronizationKeyDto.PublishAccountDatabases.CreateKey(paramsJob.AccountDatabaseId);
    }
}
