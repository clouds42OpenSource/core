﻿using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.AccountDatabaseHelpers.PublishHelpers
{
    /// <summary>
    ///     Тест класс обертка для публикации без использования воркера
    /// </summary>
    public class TestPublishDatabaseJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        PublishHelper publishHelper)
        : SynchronizationParamsJobWrapperBase<PublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto>(dbLayer,
            registerTaskInQueueProvider)
    {
        /// <summary>
        /// Запустить таску публикации базы
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<PublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto> Start(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
        {
            publishHelper.Publish(paramsJob.AccountDatabaseId);
            return this;
        }

        /// <summary>
        /// Создать ключ синхронизации
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Ключ синхронизации</returns>
        protected override string CreateSynchronizationKey(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
            => TaskSynchronizationKeyDto.PublishAccountDatabases.CreateKey(paramsJob.AccountDatabaseId);

        /// <summary>
        /// Дождаться выполнения задачи
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override IWorkerTaskResult Wait(int timeoutInMinutes = 20)
            => null;
    }
}
