﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый провайдер CloudTerminalServerSessionsProvider
    /// </summary>
    internal class CloudTerminalServerSessionsProviderFake(IUnitOfWork dbLayer) : ICloudTerminalServerSessionsProvider
    {
        /// <summary>
        /// Найти сессии пользователей по Id аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Сессии пользователей</returns>
        public async Task<List<UserRdpSessionsDto>> FindUserRdpSessionsByAccountIdAsync(Guid accountId,
            Guid? accountUserId = null)
        {
            var userRdpSessionsList = await GetSessionsAsync(new Dictionary<string, Guid> { {string.Empty, Guid.Empty} }, accountId);

            return userRdpSessionsList;
        }

        /// <summary>
        /// Получить список пользователей с активными сессиями
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns>Список пользователей с активными сессиями</returns>
        public Task<List<AccountUserDto>> GetAccountUsersConnectionsInfoList(Guid accountId)
        {
            return Task.FromResult(new List<AccountUserDto>());
        }

        /// <summary>
        /// Завершить сессию пользователя
        /// </summary>
        /// <param name="model">Данные сессии</param>
        public void DropSession(CloudTerminalServerPostRequestCloseDto model)
        {
            // заглушка
        }

        /// <summary>
        /// Получить сессии пользователей на терминальном сервере
        /// </summary>
        /// <param name="terminalServerId">Id терминального сервера</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Сессии пользователей</returns>
        public List<UserRdpSessionsDto> GetSessions(Guid terminalServerId, Guid accountId)
        {
            var login = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.AccountId == accountId).Login;

            var userRdpSessionList = new List<UserRdpSessionsDto>
            {
                new()
                {
                    UserName = login,
                    TerminalServerID = terminalServerId
                }
            };

            return userRdpSessionList;
        }

        public async Task<List<UserRdpSessionsDto>> GetSessionsAsync(Dictionary<string, Guid> terminalServersDict, Guid accountId)
        {
            var login = (await dbLayer.AccountUsersRepository.FirstOrDefaultAsync(au => au.AccountId == accountId)).Login;

            return terminalServersDict
                .Select(x => new UserRdpSessionsDto { UserName = login, TerminalServerID = x.Value }).ToList();
        }

        /// <summary>
        /// Дропнуть сессии пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        public Task DropUserRdpSessionsAsync(Guid userId)
        {
            return  Task.CompletedTask;
        }

        /// <summary>
        /// Найти и закрыть все RDP сессии пользователей
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="userIds">Id's пользователей</param>
        /// <returns>Результат заверешения сессий</returns>
        public Task<CloseRdpSessionsDto> FindAndCloseUsersRdpSessionsAsync(Guid accountId, Guid[] userIds)
        {
            return Task.FromResult( new CloseRdpSessionsDto
            {
                IsSuccess = true
            });
        }
    }
}
