﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с данными платежа ЮKassa в тестах
    /// </summary>
    public class YookassaPaymentDataHelperTest(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить системный платеж
        /// </summary>
        /// <param name="paymentNumber">Номер платежа</param>
        /// <returns>Cистемный платеж</returns>
        public Payment GetPayment(Guid id) =>
            dbLayer.PaymentRepository.FirstOrDefault(p => p.Id == id) ??
            throw new NotFoundException(
                $"Не удалось получить платеж по идентификатору {id}");

        public Payment GetFirstYookassaPayment(Guid accountId) =>
            dbLayer.PaymentRepository.FirstOrDefault(p => p.AccountId == accountId && p.PaymentSystem == PaymentSystem.Yookassa.ToString());
        
        /// <summary>
        /// Получить счет по Id системного платежа
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        /// <returns>Счет на оплату</returns>
        public Invoice GetInvoiceByPaymentId(Guid paymentId) =>
            dbLayer.InvoiceRepository.FirstOrDefault(i => i.PaymentID == paymentId) ??
            throw new NotFoundException(nameof(CloudConfigurationProvider.YookassaAggregatorPayment));
        
    }
}
