﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Logger;
using Clouds42.AccountDatabase.Create;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
	internal class CreateDatabaseOnDelimitersTestCreator(
        IUnitOfWork dbLayer,
        IServiceProvider serviceProvider,
        ILogger42 logger)
        : AccountDatabaseCreatorBase(serviceProvider)
    {

        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {
            var database = dbLayer.DatabasesRepository.FirstOrDefault(b => b.Id == createdDatabase.AccountDatabase.Id);

            var sourceDb = dbLayer.DatabasesRepository.FirstOrDefault(db => db.AccountId != createdDatabase.AccountDatabase.AccountId);

            if (sourceDb != null && database.IsDelimiter())
                database.AccountDatabaseOnDelimiter.DatabaseSourceId = sourceDb.Id;

            database.StateEnum = DatabaseState.Ready;
            dbLayer.AccountDatabaseDelimitersRepository.Update(database.AccountDatabaseOnDelimiter);
            dbLayer.Save();
            dbLayer.DatabasesRepository.Update(database);

            var accessUsers = dbLayer.AccountUsersRepository.Where(u => u.AccountId == createdDatabase.AccountDatabase.AccountId
                                                                        && u.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.AccountAdmin));

            foreach (var accessUser in accessUsers)
            {
                var access = new AcDbAccess
                {
                    AccountDatabaseID = database.Id,
                    AccountID = createdDatabase.AccountDatabase.AccountId,
                    AccountUserID = accessUser.Id,
                    ID = Guid.NewGuid()
                };
                dbLayer.AcDbAccessesRepository.Insert(access);
            }
            dbLayer.Save();
        }

        /// <summary>
        /// Метод добавление в список с выбранными базами баз с демо данными
        /// </summary>		
        protected override List<InfoDatabaseDomainModelDto> AddDemoDatabases(List<InfoDatabaseDomainModelDto> databases)
        {
            return databases;
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <param name="uploadData">Информация о зип пакете.</param>
        /// <param name="isFile">Файловая база.</param>
        protected override Domain.DataModels.AccountDatabase CreateNewDataBase(DbTemplate template, PlatformType platformType,
            string caption,
            Guid accountId,
            IUploadDataDto uploadData,
            bool isFile = true,
            bool isDemo = false,
            bool hasAutoUpdate = false,
            bool hasSupport = false)
        {
            var account = dbLayer.AccountsRepository.GetById(accountId);

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {

                var accountDatabase = dbLayer.DatabasesRepository.CreateNewRecord(template, account, platformType, caption);
                var dbTemplateDelimiter = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(d => d.TemplateId == template.Id);

                if (dbTemplateDelimiter == null)
                    throw new NotFoundException($"По шаблону {template.DefaultCaption}-> {template.Id} не найдено соответсвие шаблона разделителей.");

                var model = new AccountDatabaseOnDelimiters
                {
                    AccountDatabaseId = accountDatabase.Id,
                    LoadTypeEnum = DelimiterLoadType.UserCreateNew,
                    DbTemplateDelimiterCode = dbTemplateDelimiter.ConfigurationId
                };
                logger.Info($"Подготовлена модель для записи в таблицу для информационной базы '{accountDatabase.Id}'");

                dbLayer.AccountDatabaseDelimitersRepository.Insert(model);

                dbLayer.Save();
                transaction.Commit();

                return accountDatabase;
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }

        }
    }
}
