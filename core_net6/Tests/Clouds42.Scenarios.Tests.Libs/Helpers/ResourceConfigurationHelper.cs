﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с конфигурациями ресурсов
    /// </summary>
    public class ResourceConfigurationHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить конфигурации ресурсов Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурации ресурсов Аренды 1С для аккаунта</returns>
        public IEnumerable<ResourcesConfiguration> GetResourcesConfigurationsRental1CForAccount(Guid accountId)
        {
            var resourcesConfigurationsFirstAccount = dbLayer.ResourceConfigurationRepository.Where(w =>
                w.AccountId == accountId &&
                w.BillingService.SystemService == Clouds42Service.MyEnterprise);

            return resourcesConfigurationsFirstAccount;
        }

        /// <summary>
        ///     Добавить запись в базу конфигурацию ресурсов сервиса
        /// </summary>
        /// <param name="accountId">Номер аккаунта пользователя</param>
        /// <param name="serviceId">Номер сервиса</param>
        /// <param name="isExpired">Сервис просрочен или нет</param>
        public void InsertServiceResourceConfiguration(Guid accountId, Guid serviceId, bool isExpired)
        {
            var expireDate = DateTime.Now.AddDays(15);
            var frozen = false;
            if (isExpired)
            {
                expireDate = DateTime.Now.AddDays(-1);
                frozen = true;
            }

            var resourcesConfiguration = new ResourcesConfiguration
            {
                AccountId = accountId,
                BillingServiceId = serviceId,
                Cost = 20,
                CostIsFixed = false,
                DiscountGroup = 0,
                ExpireDate = expireDate,
                Id = Guid.NewGuid(),
                Frozen = frozen,
                CreateDate = DateTime.Now
            };

            dbLayer.ResourceConfigurationRepository.Insert(resourcesConfiguration);
            dbLayer.Save();
        }

        /// <summary>
        /// Добавление в базу записи ресурсконфига в дэмо-режиме на 7 дней
        /// </summary>
        /// <param name="accountId">гуид аккаунта</param>
        /// <param name="serviceId">гуид сервиса</param>
        public void InsertServiceDemoResourceConfiguration(Guid accountId, Guid serviceId)
        {

            var resourcesConfiguration = new ResourcesConfiguration
            {
                AccountId = accountId,
                BillingServiceId = serviceId,
                Cost = 0,
                CostIsFixed = false,
                DiscountGroup = 0,
                ExpireDate = DateTime.Now.AddDays(7),
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                IsDemoPeriod = true,
                Frozen = false
            };

            dbLayer.ResourceConfigurationRepository.Insert(resourcesConfiguration);
            dbLayer.Save();
        }


        /// <summary>
        /// Добавление ресурсов с пересчётом ResourceConfiguration
        /// </summary>
        /// <param name="accountId">аккаунт</param>
        /// <param name="billingServiceTypes">услуги</param>
        /// <param name="skipCount">скоклько услуг из общего списка пропустить</param>
        public void InsertResourcesWithResConfigChanging(Guid accountId,Guid subjectId, List<BillingServiceTypeDto> billingServiceTypes,Guid serviceId, int skipCount)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var resourceConfig = dbLayer.ResourceConfigurationRepository.FirstOrDefault(i =>
                    i.AccountId == accountId && i.BillingServiceId == serviceId);
                if (resourceConfig == null)
                { throw new InvalidOperationException("Нельзя добавлять услуги не подключив сам сервис этих услуг"); }

                foreach (var billingServiceType in billingServiceTypes.Skip(skipCount))
                {
                    var resource = new Resource
                    {
                        AccountId = accountId,
                        Cost = billingServiceType.ServiceTypeCost,
                        Subject = subjectId,
                        BillingServiceTypeId = billingServiceType.Id,
                        Id = Guid.NewGuid(),
                        DemoExpirePeriod = null,
                        IsFree = false,
                        Tag = null
                    };
                    resourceConfig.Cost += resource.Cost;
                    dbLayer.ResourceRepository.Insert(resource);

                    dbLayer.Save();
                }

                dbLayer.ResourceConfigurationRepository.Update(resourceConfig);
                dbLayer.Save();

                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }



        /// <summary>
        ///     Записать в базу ресурсы для пользователя
        /// </summary>
        /// <param name="accountId">Номер аккаунта пользователя</param>
        /// <param name="billingServiceTypes">Список услуг сервиса</param>
        /// <param name="skipCount">Кол-во пропускаемых элементов в списке</param>
        public void InsertServiceTypeResources(Guid accountId, Guid subjectId, List<BillingServiceTypeDto> billingServiceTypes, int skipCount)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                foreach (var billingServiceType in billingServiceTypes.Skip(skipCount))
                {
                    var resource = new Resource
                    {
                        AccountId = accountId,
                        Cost = billingServiceType.ServiceTypeCost,
                        Subject = subjectId,
                        BillingServiceTypeId = billingServiceType.Id,
                        Id = Guid.NewGuid(),
                        DemoExpirePeriod = null,
                        IsFree = false,
                        Tag = null
                    };

                    dbLayer.ResourceRepository.Insert(resource);
                    dbLayer.Save();
                }
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

        public void InsertServiceTypeResourcesWithSponsorship(Guid accountId, Guid subjectId, List<BillingServiceTypeDto> billingServiceTypes, int skipCount, Guid accountSponsorId)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                foreach (var billingServiceType in billingServiceTypes.Skip(skipCount))
                {
                    var resource = new Resource
                    {
                        AccountId = accountId,
                        Cost = billingServiceType.ServiceTypeCost,
                        Subject = subjectId,
                        BillingServiceTypeId = billingServiceType.Id,
                        Id = Guid.NewGuid(),
                        DemoExpirePeriod = null,
                        IsFree = false,
                        Tag = null,
                        AccountSponsorId = accountSponsorId
                    };

                    dbLayer.ResourceRepository.Insert(resource);
                    dbLayer.Save();
                }
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }

    }
}
