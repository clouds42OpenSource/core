﻿using System;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public class TaskFactoryRunnerTest(
        IAccessProvider accessProvider,
        IRequestContextTaskDispatcher requestContextTaskDispatcher,
        IUnitOfWork dbLayer)
    {
        private readonly IAccessProvider _accessProvider = accessProvider;
        private readonly IRequestContextTaskDispatcher _requestContextTaskDispatcher = requestContextTaskDispatcher;
        private readonly IUnitOfWork _dbLayer = dbLayer;

        public void Run(Action<IServiceCollection> action)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var services = new ServiceCollection();
            var serviceProvider = services.BuildServiceProvider();
            TestScenarioDependencyRegistrator.Register(services, configuration);
            services.AddTelegramBots(configuration);
            InitContainer(services);
            Logger42.Init(serviceProvider);

            action(services);
        }

        protected virtual void InitContainer(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork>();
            services.AddTransient<IAccessProvider>();
            services.AddTransient<IRequestContextTaskDispatcher>();
        }

    }

    public class TaskFactoryRunnerTest<TAccessProvider>(
        IAccessProvider accessProvider,
        IRequestContextTaskDispatcher requestContextTaskDispatcher,
        IUnitOfWork dbLayer)
        : TaskFactoryRunnerTest(accessProvider, requestContextTaskDispatcher, dbLayer)
        where TAccessProvider : CommonSystemAccessProvider
    {
        protected override void InitContainer(IServiceCollection services)
        {
            base.InitContainer(services);
            services.AddTransient<IAccessProvider>();
        }

    }
}
