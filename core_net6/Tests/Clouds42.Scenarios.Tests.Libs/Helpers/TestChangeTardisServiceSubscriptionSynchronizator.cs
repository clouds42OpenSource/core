﻿using System;
using System.Collections.Generic;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Logger;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Тестовый синхронизатор изменения подписки сервиса Тардис с апи Тардис.
    /// </summary>
    public class TestChangeTardisServiceSubscriptionSynchronizator : IChangeTardisServiceSubscriptionSynchronizator
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resources">Список изменяемых ресурсов.</param>
        public void SynchronizeWithTardis(List<Resource> resources)
        {
            _logger.Trace($"Тестовая имплементация синхронизации с Тардис для {resources.Count} ресурсов");
        }

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resource">Изменяемый ресурс.</param>
        public void SynchronizeWithTardis(Resource resource)
        {
            _logger.Trace($"Тестовая имплементация синхронизации с Тардис для ресурса {resource.Id}");
        }

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        public void SynchronizeWithTardis(Guid accountUserId)
        {
            _logger.Trace($"Тестовая имплементация синхронизации с Тардис для пользователя {accountUserId}");
        }
    }
}
