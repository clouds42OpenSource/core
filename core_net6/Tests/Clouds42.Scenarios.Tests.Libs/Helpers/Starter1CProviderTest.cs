﻿using System;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.IDataModels;
using Clouds42.Starter1C.Helpers.Interfaces;
using Clouds42.Starter1C.Providers.Interfaces;
using static Clouds42.Starter1C.Providers.Starter1CProvider;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Тестовый провайдер для запуска 1С.
    /// </summary>
    public class Starter1CProviderTest : IStarter1CProvider
    {
        private IStarter1C _starter1C;
        private IUpdateDatabaseDto _updateDatabase;
        private CanContinue _canContinueAction;
        /// <summary>
        /// Обновить информационную базу до последней доступной версии.
        /// </summary>
        /// <param name="currentVersionDb">Текущая версия конфигурации инф. базы</param>
        /// <param name="configuration">Конфигурация 1С</param>
        /// <param name="afterEachUpdateAction">Действие после каждого обновления</param>
        /// <param name="targetVersion">Последняя версия, на которую получилось обновить</param>
        /// <returns>Результат обновления</returns>
        public bool TryUpdateToLastVersion(string currentVersionDb, IConfigurations1C configuration, Action<string> afterEachUpdateAction,
            out string targetVersion)
        {
            targetVersion = currentVersionDb;
            return true;
        }

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool TryApplyUpdate()
        {
            return true;
        }

        /// <summary>
        /// Попытаться получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>
        public ConnectorResultDto<MetadataResultDto> TryGetMetadata()
        {
            return new ConnectorResultDto<MetadataResultDto>
            {
                ConnectCode = ConnectCodeDto.Success,
                Result = new MetadataResultDto("","")
            };
        }

        public IStarter1CProvider SetStarter1C(IStarter1C starter1C)
        {
            _starter1C = starter1C;
            return this;
        }
        public IStarter1CProvider SetUpdateDatabase(IUpdateDatabaseDto updateDatabase)
        {
            _updateDatabase = updateDatabase;
            return this;
        }
        public IStarter1CProvider SetCanContinueAction(CanContinue canContinueAction)
        {
            _canContinueAction = canContinueAction;
            return this;
        }
    }
}
