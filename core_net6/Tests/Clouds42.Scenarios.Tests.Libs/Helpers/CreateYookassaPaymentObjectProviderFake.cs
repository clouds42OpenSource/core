﻿using System;
using System.Threading.Tasks;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Clouds42.YookassaAggregator.Сonstants;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый провайдер для создания объекта платежа
    /// в агрегаторе ЮKassa
    /// </summary>
    internal class CreateYookassaPaymentObjectProviderFake : ICreateYookassaPaymentObjectProvider
    {
        /// <summary>
        /// Создать объект платежа в агрегаторе ЮKassa (Заглушка)
        /// </summary>
        /// <param name="creationData">Модель данных для создания объекта платежа в ЮKassa</param>
        /// <returns>Объект платежа ЮKassa</returns>
        public YookassaPaymentObjectDto Create(YookassaPaymentObjectCreationDataDto creationData) =>
            new()
            {
                Id = Guid.NewGuid(),
                Description = creationData.Description,
                CreationDate = DateTime.Now,
                Status = YookassaPaymentStatusEnum.Pending.GetStringValue(),
                PaymentConfirmationData = new YookassaPaymentConfirmationDataDto
                {
                    Type = YookassaAggregatorConst.EmbeddedConfirmationType,
                    ConfirmationToken = $"{Guid.NewGuid():N}"
                },
                Test = true,
                PaymentAmountData = new YookassaPaymentAmountDataDto
                {
                    Currency = YookassaAggregatorConst.CurrencyRub,
                    Amount = creationData.PaymentSum
                },
                YookassaPaymentMetadata = new YookassaPaymentMetadataDto(),
                YookassaPaymentRecipientData = new YookassaPaymentRecipientDataDto
                {
                    AccountId = "780876",
                    GatewayId = "1819860"
                }
            };

        public async Task<YookassaPaymentObjectDto> CreateQrAsync(YookassaPaymentObjectCreationDataDto creationData)
        {
            throw new NotImplementedException();
        }
    }
}
