﻿using System;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер по созданию тестовых данных
    /// </summary>
    public class CreateTestDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Создать сессию пользователю
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="client">Тип клиента</param>
        public void CreateSession(Guid accountUserId, ExternalClient client)
        {
            var newToken = Guid.NewGuid();
            dbLayer.AccountUserSessionsRepository.Insert(new AccountUserSession
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUserId,
                StaticToken = false,
                Token = newToken,
                TokenCreationTime = DateTime.Now,
                ClientDescription = client.ToString()
            });

            dbLayer.Save();
        }

        /// <summary>
        /// Создать пользователю роль
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        public void CreateAccountUserRole(Guid accountUserId)
        {
            dbLayer.AccountUserRoleRepository.Insert(new AccountUserRole
            {
                Id = Guid.NewGuid(),
                AccountUserId = accountUserId,
                AccountUserGroup = AccountUserGroup.CloudAdmin
            });
            dbLayer.Save();
        }

        /// <summary>
        /// Создать объект поддержки инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="configurationName">Название конфигурации</param>
        public void CreateAcDbSupport(Guid accountDatabaseId, string configurationName)
        {
            var config1C = dbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == configurationName);
            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = accountDatabaseId,
                HasAutoUpdate = false,
                Login = string.Empty,
                Password = string.Empty,
                State = (int)SupportState.NotAutorized,
                PrepearedForUpdate = true,
                CurrentVersion = "3.0.73.50",
                ConfigurationName = config1C.Name
            };
            dbLayer.AcDbSupportRepository.Insert(acDbSupport);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать объект для обновления информационной базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="configurationName">Название конфигурации</param>
        public void CreateAcDbCfu(string configurationName)
        {
            var config1C = dbLayer.Configurations1CRepository.FirstOrDefault(w => w.Name == configurationName);
            var сonfigurationsCfu = new ConfigurationsCfu
            {
                Id = Guid.NewGuid(),
                Version = "3.0.74.46",
                Platform1CVersion = "8.3.12.1685",
                DownloadDate = DateTime.Now,
                ValidateState = true,
                FilePath = "1c/Accounting/3_0_73_46/1cv8.zip",
                ConfigurationName = config1C.Name,

            };
            dbLayer.ConfigurationsCfuRepository.Insert(сonfigurationsCfu);
            dbLayer.Save();

            var udateVersionMappings = new AccountDatabaseUpdateVersionMapping
            {
                CfuId = сonfigurationsCfu.Id,
                ConfigurationName = config1C.Name,
                TargetVersion = "3.0.73.50"

            };

            dbLayer.AccountDatabaseUpdateVersionMappingRepository.Insert(udateVersionMappings);
            dbLayer.Save();
        }
    }
}
