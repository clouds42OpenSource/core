﻿using System;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый провайдер для получения данных по конфигурациям локалей
    /// </summary>
    public class FakeLocalesConfigurationDataProvider(IUnitOfWork dbLayer) : ILocalesConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию локали для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Конфигурация локали для аккаунта</returns>
        public LocaleConfigurationDto GetLocaleConfigurationForAccount(Guid accountId)
            => new()
            {
                CpSiteUrl = GetCpSiteUrlForAccount(accountId),
                DefaultSegmentId = GetDefaultSegmentId(Guid.NewGuid())
            };

        /// <summary>
        /// Получить адрес сайта личного кабинета для локали по аккаунту
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Адрес сайта личного кабинета для локали</returns>
        public string GetCpSiteUrlForAccount(Guid accountId)
            => $"https://{accountId}";

        /// <summary>
        /// Получить ID сегмента по умолчанию для локали
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>ID сегмента по умолчанию для локали</returns>
        public Guid GetDefaultSegmentId(Guid localeId)
            => dbLayer.CloudServicesSegmentRepository.FirstOrDefault()?.ID ?? Guid.NewGuid();

        /// <summary>
        /// Получить полный путь до экшена
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="partialActionPath">Частичный путь до экшена</param>
        /// <returns>Полный путь до экшена</returns>
        public string GetFullPathToAction(Guid accountId, string partialActionPath) =>
            $"{GetCpSiteUrlForAccount(accountId)}{partialActionPath}";
    }
}
