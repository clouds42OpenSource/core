﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания связей конфигурации 1С с шаблоном инф. базы(для тестов)
    /// </summary>
    public class CreateConfigurationDbTemplateRelationsTestHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Создать связи конфигурации 1С с шаблоном инф. базы
        /// </summary>
        public void CreateRelations()
        {
            var temlates = GetDbTemplatesForWhichNoRelationsHaveBeenCreated().ToList();

            if (!temlates.Any())
                return;

            temlates.ForEach(CreateRelationForTemplate);
        }

        /// <summary>
        /// Создать связь с конфигурацией 1С для шаблона 
        /// </summary>
        /// <param name="template">Шаблон инф. базы</param>
        private void CreateRelationForTemplate(DbTemplate template)
        {
            var configurationName = template.DefaultCaption.DefineConfigurationName();
            if (configurationName == null)
                return;

            var configuration = GetConfigurations1C(configurationName);
            if (configuration == null)
                return;

            CreateConfigurationDbTemplateRelation(template.Id, configurationName);
        }
        
        /// <summary>
        /// Создать связь конфигурации 1С с шаблоном инф. базы
        /// </summary>
        /// <param name="templateId">Id шаблона</param>
        /// <param name="configurationName">Название конфигурации</param>
        private void CreateConfigurationDbTemplateRelation(Guid templateId, string configurationName)
        {
            var configurationDbTemplateRelation = new ConfigurationDbTemplateRelation
            {
                DbTemplateId = templateId,
                Configuration1CName = configurationName
            };

            dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().Insert(configurationDbTemplateRelation);
            dbLayer.Save();
        }

        /// <summary>
        /// Получить шаблоны инф. баз для которых
        /// не созданы связи с конфигурацией
        /// </summary>
        /// <returns>Шаблоны инф. баз</returns>
        private IEnumerable<DbTemplate> GetDbTemplatesForWhichNoRelationsHaveBeenCreated() =>
            (from template in dbLayer.DbTemplateRepository.WhereLazy()
                join templateRelation in dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().WhereLazy() on
                    template.Id equals templateRelation.DbTemplateId into templateRelations
                from relation in templateRelations.DefaultIfEmpty()
                where relation == null
                select template).AsEnumerable();

        /// <summary>
        /// Получить конфигурацию 1С
        /// </summary>
        /// <param name="configuration1CName">Название конфигурации 1С</param>
        /// <returns>Конфигурация 1С</returns>
        private Domain.DataModels.Configurations1C GetConfigurations1C(string configuration1CName) =>
            dbLayer.Configurations1CRepository.FirstOrDefault(conf => conf.Name == configuration1CName);
    }
}
