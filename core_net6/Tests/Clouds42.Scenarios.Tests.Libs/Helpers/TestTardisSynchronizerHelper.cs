﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.Logger;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Синхронизатор с Тардис для тестов
    /// </summary>
    public class TestTardisSynchronizerHelper : ITardisSynchronizerHelper
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Выполнить синхронизацию пользователя с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountUser<TTrigger, TTriggerModel>(TTriggerModel model) 
            where TTrigger : SynchTriggerBase<TTriggerModel> 
            where TTriggerModel : class, IAccountUserIdDto
        {
            _logger.Trace($"Тестовая имплементация синхронизации с Тардис для пользователя {model.AccountUserId}");
        }

        /// <summary>
        /// Выполнить синхронизацию базы с Тардис.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountDatabase<TTrigger, TTriggerModel>(TTriggerModel model) 
            where TTrigger : SynchTriggerBase<TTriggerModel> 
            where TTriggerModel : class, IAccountDatabaseIdDto
        {
            _logger.Trace($"Тестовая имплементация синхронизации с Тардис для инф. базы {model.AccountDatabaseId}");
        }

        /// <summary>
        /// Выполнить синхронизацию с Тардис при изменение доступа пользователя в информационной базе.
        /// </summary>
        /// <typeparam name="TTrigger">Тип вызываемого триггера.</typeparam>
        /// <typeparam name="TTriggerModel">Тип модели вызываемого триггера.</typeparam>
        /// <param name="model">Модель вызываемого триггера.</param>
        public void SynchronizeAccountDatabaseAccess<TTrigger, TTriggerModel>(TTriggerModel model) 
            where TTrigger : SynchTriggerBase<TTriggerModel> 
            where TTriggerModel : class, IAccountDatabaseAccessDto
        {
            _logger.Trace(
                $"Тестовая имплементация синхронизации с Тардис для доступа пользователя {model.AccountUserId} к инф. базе {model.AccountDatabaseId}");
        }
    }
}
