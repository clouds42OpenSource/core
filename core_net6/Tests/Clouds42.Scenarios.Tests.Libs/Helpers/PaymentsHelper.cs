﻿using System;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с платежами
    /// </summary>
    public class PaymentsHelper(IUnitOfWork dbLayer, ICalculateAgentRewardProvider calculateAgentRewardProvider)
    {
        /// <summary>
        /// Создание платежа с выплатой агерту
        /// </summary>
        /// <param name="accountId">кто платит</param>
        /// <param name="paymentType">тип поступления</param>
        /// <param name="sum">сумма</param>
        /// <param name="billingServiceId">оплачиваемый сервис</param>
        /// <param name="date">дата</param>
        /// <returns></returns>
        public PaymentOperationResult MakePayment(Guid accountId, PaymentType paymentType = PaymentType.Inflow,
            decimal sum = 5000, Guid? billingServiceId = null, DateTime? date = null)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var newPayment = new Payment
                {
                    Id = Guid.NewGuid(),
                    AccountId = accountId,
                    Date = date ?? DateTime.Now,
                    Description = "Test Payment",
                    OriginDetails = "Admin import",
                    Status = PaymentStatus.Done.ToString(),
                    OperationType = paymentType.ToString(),
                    Sum = sum,
                    PaymentSystem = "Admin",
                    BillingServiceId = billingServiceId
                };
                dbLayer.PaymentRepository.Insert(newPayment);
                dbLayer.Save();

                if (paymentType == PaymentType.Outflow && billingServiceId != null)
                {
                    Guid? referrerAccId;
                    var service = dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Id == billingServiceId);

                    if (!service.SystemService.HasValue)
                    {
                        referrerAccId = service.AccountOwnerId;
                    }
                    else
                    {
                        referrerAccId = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId)
                            .ReferralAccountID;
                    }

                    if (referrerAccId != null)
                    {
                        var agentReward = calculateAgentRewardProvider.Calculate(sum,
                            newPayment.Account.AccountConfiguration.IsVip, service.SystemService);
                        dbLayer.AgentPaymentRepository.InsertAgentPaymentAndUpdateAgentWallet((Guid) referrerAccId,
                            agentReward, newPayment.Id, PaymentType.Inflow, comment: "");
                        dbLayer.Save();
                    }
                }

                transaction.Commit();
                return PaymentOperationResult.Ok;
            }
            catch (Exception)
            {
                transaction.Rollback();
                return PaymentOperationResult.ErrorInvalidOperation;
            }
        }
    }
}