﻿namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Контстанты Id конфигураций 1С для тестов
    /// </summary>
    public static class ConfigurationsIdTestConstants
    {
        /// <summary>
        /// Управление нашей фирмой
        /// </summary>
        public static string Unf => "unf";

        /// <summary>
        /// Бухглатерия предприятия
        /// </summary>
        public static string Bp => "bp";

        /// <summary>
        /// Комплексная автоматизация
        /// </summary>
        public static string Ka => "ka";

        /// <summary>
        /// Управление торговлей
        /// </summary>
        public static string Ut => "ut";
    }
}
