﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using CommonLib.Enums;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания пользователей аккаунта для тестов
    /// </summary>
    public class AccountUserTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
        private readonly AccountUsersProfileManager _accountUsersProfileManager = testContext.ServiceProvider.GetRequiredService<AccountUsersProfileManager>();
        private readonly IAccessProvider _accessProvider = testContext.ServiceProvider.GetRequiredService<IAccessProvider>();
        private readonly AccountUserRolesProvider _accountUserRolesProvider = testContext.ServiceProvider.GetRequiredService<AccountUserRolesProvider>();

        /// <summary>
        /// Шаблон логина пользователя для создания
        /// </summary>
        public static string LoginPattern => "LoginUser";

        /// <summary>
        /// Создать указанное количество пользователей аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="usersToCreateCount">Количество пользователей для создания</param>
        public void CreateAccountUsers(Guid accountId, int usersToCreateCount = 1)
        {
            for(var i = 1; i <= usersToCreateCount; i++)
                CreateAccountUser(accountId);
        }

        /// <summary>
        /// Создать пользователя аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public Guid CreateAccountUser(Guid accountId)
        {
            var accountUserRegistrationToAccountTest = GetAccountUserRegistrationToAccountTest(accountId);

            var res = _accountUsersProfileManager.AddToAccount(accountUserRegistrationToAccountTest).Result;

            if (res.Error)
                throw new InvalidOperationException("Ошибка создания нового пользователя. " + res.Message);

            var userSession = GetAccountUserSession(res.Result);

            _dbLayer.AccountUserSessionsRepository.Insert(userSession);
            _dbLayer.Save();

            return res.Result;
        }

        /// <summary>
        /// Изменить роли пользователя
        /// </summary>
        /// <param name="user">Пользователь аккаунта</param>
        /// <param name="newGroups">Новые роли</param>
        public void ChangeUserRoles(AccountUser user, AccountUserGroup newGroups)
        {

        var adminRoles = new List<AccountUserGroup> 
        { 
            AccountUserGroup.AccountUser,
            AccountUserGroup.CloudAdmin,
            AccountUserGroup.Hotline
        };

        _accountUserRolesProvider.SetAccountUserRoles(user, adminRoles, [newGroups]);

        }

        /// <summary>
        /// Создать ресурс MyEnterprise для пользователя аккаунта
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="accountId">Id аккаунта пользователя</param>
        public void CreateMyEnterpriseResourceForUser(Guid userId, Guid accountId)
        {
            var serviceMyEnt =
                _dbLayer.BillingServiceRepository.FirstOrDefault(serv =>
                    serv.SystemService == Clouds42Service.MyEnterprise);

            var billingTypeMyEntUser = serviceMyEnt.BillingServiceTypes.FirstOrDefault(typeServ =>
                typeServ.SystemServiceType == ResourceType.MyEntUser);

            var resource =
                _dbLayer.ResourceRepository.FirstOrDefault(res =>
                    res.BillingServiceTypeId == billingTypeMyEntUser.Id && res.AccountId == accountId &&
                    res.Subject == userId); 

            if(resource != null)
                throw new InvalidOperationException(
                    $"Для пользователя {userId} аккаунта {accountId} уже существует ресурс {ResourceType.MyEntUser.GetDisplayName()}");


            resource = new Resource
            {
                AccountId = accountId,
                Subject = userId,
                BillingServiceTypeId = billingTypeMyEntUser.Id,
                Cost = 100,
                IsFree = false,
                Id = Guid.NewGuid()

            };

            _dbLayer.ResourceRepository.Insert(resource);
            _dbLayer.Save();

        }

        /// <summary>
        /// Удалить пользователя аккаунта (и все необходимые зависимости)
        /// </summary>
        /// <param name="accoundUserId">Id пользователя аккаунта</param>
        public void DeleteAccountUser(Guid accoundUserId)
        {
            var user = _dbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == accoundUserId);
            if (user == null)
                return;

            var userSessions = _dbLayer.AccountUserSessionsRepository.Where(session => session.AccountUserId == accoundUserId);
            if(userSessions != null)
                _dbLayer.AccountUserSessionsRepository.DeleteRange(userSessions);

            var userAcDbAccesses = _dbLayer.AcDbAccessesRepository.Where(ac => ac.AccountUserID == accoundUserId);
            if(userAcDbAccesses != null)
                _dbLayer.AcDbAccessesRepository.DeleteRange(userAcDbAccesses);

            var userGroups = _dbLayer.AccountUserRoleRepository.Where(role => role.AccountUserId == accoundUserId);
            if(userGroups != null)
                _dbLayer.AccountUserRoleRepository.DeleteRange(userGroups);

            var userInitiatedBackups = _dbLayer.AccountDatabaseBackupRepository.Where(backup => backup.InitiatorId == accoundUserId);
            if(userInitiatedBackups != null)
                _dbLayer.AccountDatabaseBackupRepository.DeleteRange(userInitiatedBackups);

            _dbLayer.AccountUsersRepository.DeleteUser(user);

            _dbLayer.Save();
        }

        /// <summary>
        /// Получить пользователя аккаунта по роли
        /// </summary>
        /// <param name="accountUserGroup">Роль пользователя</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Пользователь аккаунта</returns>
        public AccountUser GetAccountUserByGroup(AccountUserGroup accountUserGroup, Guid accountId) =>
            _dbLayer.AccountsRepository.FirstOrDefault(acc => acc.Id == accountId)?.AccountUsers.FirstOrDefault(user =>
                user.AccountUserRoles.All(role => role.AccountUserGroup == accountUserGroup));

        /// <summary>
        /// Получить пользователя аккаунта с ролью AccountUser
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Пользователь аккаунта</returns>
        public AccountUser GetOrCreateUserWithAccountUserRole(Guid accountId)
        {
            var accUser = GetAccountUserByGroup(AccountUserGroup.AccountUser, accountId);
            if (accUser != null) 
                return accUser;

            CreateAccountUser(accountId);
            accUser = GetAccountUserByGroup(AccountUserGroup.AccountUser, accountId);

            return accUser;
        }

        /// <summary>
        /// Получить тестовую модель регистрации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Тестовая модель регистрации аккаунта</returns>
        private AccountUserRegistrationToAccountTest GetAccountUserRegistrationToAccountTest(Guid accountId) =>
            new()
            {
                AccountId = accountId,
                AccountIdString = accountId.ToString(),
                Login = $"{LoginPattern}{DateTime.Now:mmssfff}",
                Email = $"{Guid.NewGuid()}LoginUser@efsol.ru",
                FirstName = $"{Guid.NewGuid()}LoginUserName",
                FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}"
            };

        /// <summary>
        /// Получить сессию пользователя аккаунта
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns></returns>
        private AccountUserSession GetAccountUserSession(Guid userId) => new()
        {
            Id = Guid.NewGuid(),
            Token = Guid.NewGuid(),
            StaticToken = false,
            ClientDescription = "GetTokenByLogin",
            ClientDeviceInfo = $"[{_accessProvider.GetUserAgent()}]",
            ClientIPAddress = _accessProvider.GetUserHostAddress(),
            TokenCreationTime = DateTime.Now,
            AccountUserId = userId,
        };
    }
}
