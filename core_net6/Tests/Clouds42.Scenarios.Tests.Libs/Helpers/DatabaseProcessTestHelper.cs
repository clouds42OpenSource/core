﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с задачами инф. баз для тестов
    /// </summary>
    public class DatabaseProcessTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Соответствие статуса инф. базы к названию задачи
        /// </summary>
        private readonly Dictionary<string, DatabaseState> _dbStatusesByProcess = new()
        {
            {ProcessesNameConstantsTest.RemovingDatabaseProcessName, DatabaseState.DelitingToTomb},
            {ProcessesNameConstantsTest.ArchivingDatabaseProcessName, DatabaseState.DetachingToTomb},
            {ProcessesNameConstantsTest.RestoreAccountDatabaseFromTombJobName, DatabaseState.RestoringFromTomb}
        };

        /// <summary>
        /// Проверить, что процессы для инф. баз не были запущены
        /// </summary>
        /// <param name="databasesId">Id инф. баз</param>
        /// <param name="taskName">Название задачи</param>
        public void CheckProcessesOfDatabasesNotStarted(List<Guid> databasesId, string taskName) =>
            databasesId.ForEach(databaseId => CheckProcessOfDatabaseNotStarted(databaseId, taskName));


        /// <summary>
        /// Проверить, что процесс для инф. базы не был запущен
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="taskName">Название задачи</param>
        public void CheckProcessOfDatabaseNotStarted(Guid databaseId, string taskName)
        {
            var workerTask = GetCoreWorkerTaskByNameOrThrowException(taskName);

            var createdTasks = GetCreatedCoreWorkerTasksInQueue(workerTask.ID);

            CheckTaskParametersContainDbId(createdTasks, databaseId, false);
        }

        /// <summary>
        /// Проверить, что процесс для инф. базы не был запущен
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        public void CheckProcessOfDatabaseNotStarted(string taskName)
        {
            var workerTask = GetCoreWorkerTaskByNameOrThrowException(taskName);

            var createdTasks = GetCreatedCoreWorkerTasksInQueue(workerTask.ID);

            if (createdTasks.Any())
                throw new InvalidOperationException($"Создались задачи {taskName} при условии невозможности создания");
        }

        /// <summary>
        /// Проверить верно ли запустились процессы
        /// </summary>
        /// <param name="databasesId">Id инф. баз</param>
        /// <param name="taskName">Название процесса</param>
        public void CheckProcessesOfDatabasesStarted(IEnumerable<Guid> databasesId, string taskName)
        {
            foreach (var databaseId in databasesId)
                CheckProcessOfDatabaseStarted(databaseId, taskName);
        }

        /// <summary>
        /// Проверить верно ли запустился процесс
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="taskName">Название процесса</param>
        /// <param name="dbStatusHasToBeChange">Признак нужно ли проверять изменение статуса инф. базы</param>
        public void CheckProcessOfDatabaseStarted(Guid databaseId, string taskName, bool dbStatusHasToBeChange = true)
        {
            var createdTasks = GetCreatedWorkerTasksByName(taskName);

            CheckTaskParametersContainDbId(createdTasks, databaseId);

            if(dbStatusHasToBeChange)
                CheckDbStatusChanged(databaseId, taskName);
        }

        /// <summary>
        /// Проверить верно ли запустился процесс по дополнительным параметрам
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="idHasToBeInParams">Id который должен быть в параметрах</param>
        /// <param name="taskName">Название процесса</param>
        /// <param name="dbStatusHasToBeChange">Признак нужно ли проверять изменение статуса инф. базы</param>
        public void CheckProcessOfDatabaseStartedByAdditionalParams(Guid databaseId, Guid idHasToBeInParams, string taskName, bool dbStatusHasToBeChange = true)
        {
            var createdTasks = GetCreatedWorkerTasksByName(taskName);

            CheckTaskParametersContainDbId(createdTasks, idHasToBeInParams);

            if (dbStatusHasToBeChange)
                CheckDbStatusChanged(databaseId, taskName);
        }

        /// <summary>
        /// Получить созданные задачи по названию задачи
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <returns>Список созданных задач</returns>
        private IEnumerable<CoreWorkerTasksQueue> GetCreatedWorkerTasksByName(string taskName) =>
            GetCreatedCoreWorkerTasksInQueue(GetCoreWorkerTaskByNameOrThrowException(taskName).ID);

        /// <summary>
        /// Получить задачу воркера по названию
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <returns>Задача воркера</returns>
        private Domain.DataModels.CoreWorkerTask GetCoreWorkerTaskByNameOrThrowException(string taskName) =>
            _dbLayer.CoreWorkerTaskRepository.FirstOrDefault(tsk => tsk.TaskName == taskName) ??
            throw new NotFoundException($"Не найдена задача для процесса {taskName} инф. базы");

        /// <summary>
        /// Получить задачи в очереди воркера по Id задачи
        /// </summary>
        /// <param name="taskId">Id задачи</param>
        /// <returns>Список задач в очереди</returns>
        private IEnumerable<CoreWorkerTasksQueue> GetCreatedCoreWorkerTasksInQueue(Guid taskId) =>
            _dbLayer.CoreWorkerTasksQueueRepository.Where(task =>
                task.CoreWorkerTaskId == taskId);

        /// <summary>
        /// Проверить, что статус инф. базы изменился в соответствии с процессом
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="taskName">Название процесса</param>
        private void CheckDbStatusChanged(Guid databaseId, string taskName)
        {
            var database = _dbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == databaseId) ??
                           throw new NotFoundException($"Не найдена инф. база по Id {databaseId}");

            if (database.StateEnum != _dbStatusesByProcess[taskName])
                throw new InvalidOperationException(
                    $"Статус базы {databaseId} не изменился при старте процесса {taskName}");

        }

        /// <summary>
        /// Проверить, что параметры процессов были правильно созданы
        /// </summary>
        /// <param name="createdTasks">Созданные задачи</param>
        /// <param name="idToSearch">Id для поиска в параметрах</param>
        /// <param name="taskHasToBeCreated">Признак что задача должна быть создана</param>
        private static void CheckTaskParametersContainDbId(IEnumerable<CoreWorkerTasksQueue> createdTasks, Guid idToSearch, bool taskHasToBeCreated = true)
        {
            var taskCreated = false;

            foreach (var createdTask in createdTasks)
            {
                if (createdTask.TaskParameter.TaskParams.Contains(idToSearch.ToString()))
                    taskCreated = true;
            }

            switch (taskCreated)
            {
                case false when taskHasToBeCreated:
                    throw new InvalidOperationException($"Не создалась задача с параметром {idToSearch}");
                case true when !taskHasToBeCreated:
                    throw new InvalidOperationException(
                        $"Создалась задача с параметром {idToSearch} при условии невозможности создания");
            }
        }
    }
}
