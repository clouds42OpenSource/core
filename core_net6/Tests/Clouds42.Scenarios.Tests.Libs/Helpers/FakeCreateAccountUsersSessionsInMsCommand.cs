﻿using System;
using System.Collections.Generic;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.Cloud42Services;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейк комманда для начисления доп сеансов аккаунту
    /// </summary>
    public class FakeCreateAccountUsersSessionsInMsCommand : ICreateAccountUsersSessionsInMsCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для начисления доп сеансов
        /// </summary>
        /// <param name="commonSessions">кол-во общих сеансов
        /// <param name="userSessions">кол-во по пользователям сеансов
        /// <param name="accountId">идентификатор аккаунта
        public void CreateAccountUsersSessionsInMs(int commonSessions, List<AccountUserSession>? userSessions, Guid accountId)
        {
            
        }
    }
}
