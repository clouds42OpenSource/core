﻿using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using System;
using System.IO;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для профиля пользователя (для тестов)
    /// </summary>
    public class TestAccountUserProfileHelper : IAccountUserProfileHelper
    {
        /// <summary>
        /// Запустить задачу по созданию директории 1CeStart.
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        public void RunTaskCreate1CStartFolder(string login)
        {
            try
            {
                var directoryPath = AccountUserProfileDirectoryHelper.GetPathForCreate1CeStartDirectory(login);

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}
