﻿using System;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public static class PhoneNumberGeneratorHelperTest
    {
        public static string Generate()
        {
            return $"996778{GenerateWithoutContryCode()}";
        }

        public static string GenerateWithPrefixCode(int prefixCode)
        {
            return $"996555{GenerateWithoutContryCode()}"[..11] + prefixCode;
        }

        public static string GenerateWithoutContryCode()
        {
            var r = new Random();
            return ((long)r.Next(0, 1000) * (long)r.Next(0, 1000)).ToString().PadLeft(6, '0');
        }
    }
}
