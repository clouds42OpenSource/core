﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.DataModels.CoreWorkerModels;
using Clouds42.Domain.DataModels.History;
using Clouds42.Domain.DataModels.JsonWebToken;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для чистки мусора (Для тестов)
    /// </summary>
    public static class GarbageClearHelper
    {
        private static readonly string PathToAccountUserProfilesDirectory =
            CloudConfigurationProvider.Files.GetAccountUserProfilesDirectory();

        /// <summary>
        /// Выполнить чистку мусора
        /// </summary>
        /// <param name="dbLayer">Объект UnitOfWork</param>
        /// <param name="needUseAd">Флаг нужно ли использовать функции АД</param>
        public static void ClearGarbage(IUnitOfWork dbLayer, bool needUseAd = false)
        {
            var accountUsers =  dbLayer.AccountUsersRepository.AsQueryable().ToList();

            foreach (var accountUser in accountUsers)
            {
                if (needUseAd)
                    ActiveDirectoryUserFunctions.DeleteUser(accountUser.Login);

                DeleteDirectory(string.Format(PathToAccountUserProfilesDirectory, accountUser.Login));
            }

            if (needUseAd)
                ActiveDirectoryUserFunctions.DeleteUser("TestLogin");

            var accounts = dbLayer.AccountsRepository.AsQueryable().ToList();

            if (needUseAd)
                DeleteAccountsFromAd(accounts);

            DeleteDirectory(ExecutionTestContext.GetTestDataPath());

            DeleteObjectsFromDb(dbLayer);
        }


        /// <summary>
        /// Удалиьть директорию
        /// </summary>
        /// <param name="path">Путь до директории</param>
        private static void DeleteDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    DirectoryHelper.DeleteDirectory(path);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        /// <summary>
        /// Удалить объекты из БД
        /// </summary>
        /// <param name="dbLayer">Объект UnitOfWork</param>
        private static void DeleteObjectsFromDb(IUnitOfWork dbLayer)
        {
            dbLayer.AccountAutoPayConfigurationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.SavedPaymentMethodBankCardRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.SavedPaymentMethodRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentTransferBalanceRequestRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.SessionResourceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentRequisitesFileRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.LegalPersonRequisitesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.PhysicalPersonRequisitesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.SoleProprietorRequisitesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentRequisitesFileRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentCashOutRequestFileRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentCashOutRequestRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentRequisitesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<AcDbSupportHistoryTasksQueueRelation>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AcDbSupportHistoryRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AcDbSupportRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudChangesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<ServiceManagerAcDbBackup>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountDatabaseBackupHistoryRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountDatabaseBackupRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountDatabaseDelimitersRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.UploadedFileRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DelimiterSourceAccountDatabaseRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<ServiceManager1CFileRelation>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<Service1CFileDbTemplateRelation>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.BillingService1CFileRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<Service1CFileVersion>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<BillingServiceExtension>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DbTemplateDelimitersReferencesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.MigrationAccountHistoryRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.MigrationAccountDatabaseHistoryRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<MigrationHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountRateRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.InvoiceReceiptRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.InvoiceProductRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.InvoiceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentPaymentRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentPaymentRelationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.PaymentRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DiskResourceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<MyDatabasesResource>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ResourceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ResourceConfigurationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<LimitOnFreeCreationDbForAccount>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.BillingAccountRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.NotificationBufferRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudCoreRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountSaleManagerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountUserSessionsRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AcDbAccRolesJhoRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ManageDbOnDelimitersAccessRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<ServiceExtensionDatabase>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AcDbAccessesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountUserNotificationSettingsRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<AccountDatabaseAutoUpdateResult>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<TerminationSessionsInDatabase>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DatabasesRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ServiceAccountRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<ChangeServiceRequest>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<IndustryDependencyBillingServiceChange>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.BillingServiceTypeActivityForAccountRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<BillingServiceTypeCostChangedHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<MainServiceResourcesChangesHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<BillingServiceTypeCostChangedHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<BillingServiceTypeChange>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<RecalculationServiceCostAfterChange>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<BillingServiceChanges>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.WebSocketConnectionRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<AccountUserJsonWebToken>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountDatabaseUpdateVersionMappingRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ConfigurationsCfuRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountUsersRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ProvidedServiceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountEmailRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.IndustryDependencyBillingServiceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.IndustryRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountCsResourceValueRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.RateRepository
                .AsQueryableNoTracking()
                .Where(x => x.BillingServiceType.SystemServiceType == null && x.BillingServiceType.Name != "Дополнительный сеанс")
                .ExecuteDelete();

            dbLayer.BillingServiceTypeRepository
                .AsQueryableNoTracking()
                .Where(x => x.SystemServiceType == null && x.Name != "Дополнительный сеанс")
                .ExecuteDelete();

            dbLayer.BillingServiceRepository
                .AsQueryableNoTracking()
                .Where(x => x.SystemService == null && x.Name != "Дополнительные сеансы")
                .ExecuteDelete();
            dbLayer.FlowResourcesScopeRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AgentWalletRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<AccountLocaleChangesHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.SupplierReferralAccountRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<MyDiskPropertiesByAccount>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<AccountRequisites>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountConfigurationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AccountsRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<LocaleConfiguration>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesSegmentStorageRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesSegmentRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CoreHostingRepository
                .AsQueryableNoTracking()
                .Where(x => x.Name.ToLower() != "42clouds".ToLower())
                .ExecuteDelete();
            dbLayer.CloudServicesFileStorageServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesSegmentTerminalServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudTerminalServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesTerminalFarmRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesSqlServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesBackupStorageRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesContentServerNodeRepository.AsQueryableNoTracking().ExecuteDelete();

            dbLayer.PublishNodeReferenceRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesContentServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesEnterpriseServerRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServicesGatewayTerminalRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AvailableMigrationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AvailableMigrationRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<CoreWorkerTaskParameter>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CoreWorkerTasksQueueLiveRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CoreWorkerTasksQueueRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ActionFlowRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.ProcessFlowRepository.AsQueryableNoTracking().ExecuteDelete();

            dbLayer.AgencyAgreementRepository
                .AsQueryableNoTracking()
                .Where(x => x.Name != "Агентское соглашение")
                .ExecuteDelete();

            dbLayer.GetGenericRepository<ConfigurationDbTemplateRelation>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DbTemplateRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.DbTemplateUpdateRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.AutoUpdateNodeRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.GetGenericRepository<DatabaseLaunchRdpStartHistory>().AsQueryableNoTracking().ExecuteDelete();
            dbLayer.CloudServiceRepository
                .AsQueryableNoTracking()
                .Where(x => x.ServiceCaption == "SomeTestServiceCaption")
                .ExecuteDelete();
            dbLayer.IncidentRepository.AsQueryableNoTracking().ExecuteDelete();
            dbLayer.PlatformVersionReferencesRepository
                .AsQueryableNoTracking()
                .Where(x => x.PathToPlatfromX64 == null)
                .ExecuteDelete();
        }

        /// <summary>
        /// Удалить аккаунты из АД
        /// </summary>
        /// <param name="accounts">Список аккаунтов</param>
        private static void DeleteAccountsFromAd(List<Account> accounts)
        {
            foreach (var account in accounts)
            {
                ActiveDirectoryCompanyFunctions.DeleteGroups(
                    DomainCoreGroups.CompanyGroupBuild(account.IndexNumber), 
                    DomainCoreGroups.WebCompanyGroupBuild(account.IndexNumber));
            }
           
        }
    }
}
