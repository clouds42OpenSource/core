﻿using System.IO;
using System.Security.AccessControl;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    ///     Класс для управления доступом к папкам
    /// </summary>
    internal static class DirectoryPermissionHelperTest
    {
        /// <summary>
        ///     Добавить доступ и права
        /// </summary>
        /// <param name="dirPath">Путь к папке</param>
        /// <param name="account">Кому будет заблокировано</param>
        /// <param name="rights">Виды прав</param>
        /// <param name="controlType">Виды управления</param>
        public static void AddDirectorySecurity(string dirPath, string account, FileSystemRights rights, AccessControlType controlType)
        {
            if (string.IsNullOrEmpty(dirPath))
                throw new NotFoundException("dirPath is empty or null");

            if (string.IsNullOrEmpty(account))
                throw new NotFoundException("Account is null or empty");

            var directory = new DirectoryInfo(dirPath);

            DirectorySecurity directorySecurity = directory.GetAccessControl();

            directorySecurity.AddAccessRule(new FileSystemAccessRule(account, rights, controlType));

            directory.SetAccessControl(directorySecurity);
        }

        /// <summary>
        ///     Убрать добавленый доступ и права
        /// </summary>
        /// <param name="dirPath">Путь к папке</param>
        /// <param name="account">Кому будет заблокировано</param>
        /// <param name="rights">Виды прав</param>
        /// <param name="controlType">Виды управления</param>
        public static void RemoveDirectorySecurity(string dirPath, string account, FileSystemRights rights, AccessControlType controlType)
        {
            if (string.IsNullOrEmpty(dirPath))
                throw new NotFoundException("dirPath is empty or null");

            if (string.IsNullOrEmpty(account))
                throw new NotFoundException("Account is null or empty");

            var directory = new DirectoryInfo(dirPath);

            DirectorySecurity directorySecurity = directory.GetAccessControl();

            directorySecurity.RemoveAccessRule(new FileSystemAccessRule(account, rights, controlType));

            directory.SetAccessControl(directorySecurity);
        }
    }
}
