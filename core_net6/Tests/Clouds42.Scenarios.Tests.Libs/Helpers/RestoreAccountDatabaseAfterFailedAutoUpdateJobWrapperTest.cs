﻿using System;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.DataContracts.CoreWorkerTasks.Results;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Обертка джобы восстановления инф. базы из бэкапа после не успешной попытки АО для тестов 
    /// </summary>
    public class RestoreAccountDatabaseAfterFailedAutoUpdateJobWrapperTest(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam,
                RestoreAccountDatabaseFromTombResultDto>(registerTaskInQueueProvider, dbLayer),
            IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        public ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam, RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob, DateTime dateTimeDelayOperation)
        {
            return this;
        }

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        public override ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam, RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob)
        {
            return this;
        }

        /// <summary>
        /// Ожидать результат выполнения
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override RestoreAccountDatabaseFromTombResultDto WaitResult()
        {
            return new RestoreAccountDatabaseFromTombResultDto
            {
                Success = true
            };
        }

        /// <summary>
        /// Ожидать выполнения
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override IWorkerTaskResult Wait(int timeoutInMinutes = 20)
        {
            return null;
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType { get; }
    }
}
