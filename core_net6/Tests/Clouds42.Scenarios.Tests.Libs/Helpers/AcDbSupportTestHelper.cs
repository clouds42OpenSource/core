﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.AccountDatabase._1C.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с ТиИ инф. базы в тестах
    /// </summary>
    public class AcDbSupportTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
        private readonly CreateTestDataHelper _createTestDataHelper = testContext.ServiceProvider.GetRequiredService<CreateTestDataHelper>();
        private readonly Configurations1CDataManager _configurations1CDataManager = testContext.ServiceProvider.GetRequiredService<Configurations1CDataManager>();

        /// <summary>
        /// Создать запись AcDbSupport
        /// </summary>
        /// <param name="databaseIds">Список Id инф. баз</param>
        /// <param name="configurationName">Название конфигурации</param>
        public void CreateAcDbSupport(IEnumerable<Guid> databaseIds, string configurationName)
        {
            var configuration1C = _dbLayer.Configurations1CRepository.FirstOrDefault(conf => conf.Name == configurationName);
            if (configuration1C == null)
                CreateNewConfiguration1C(configurationName);

            foreach (var dbId in databaseIds)
                _createTestDataHelper.CreateAcDbSupport(dbId, configurationName);
        }

        /// <summary>
        /// Создать запись AcDbSupportHistory
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="supportOperation">Тип операции</param>
        public void CreateAcDbSupportHistory(Guid databaseId, DatabaseSupportOperation supportOperation)
        {
            var acDbSupportHistory = new AcDbSupportHistory
            {
                AccountDatabaseId = databaseId,
                State = SupportHistoryState.Success,
                Description = supportOperation.Description(),
                Operation = supportOperation,
                ID = Guid.NewGuid()
            };

            _dbLayer.AcDbSupportHistoryRepository.Insert(acDbSupportHistory);
            _dbLayer.Save();
        }

        /// <summary>
        /// Создать конфигурацию 1С
        /// </summary>
        private void CreateNewConfiguration1C(string configurationName)
        {
            var createResult = _configurations1CDataManager.AddConfiguration1C(new AddOrUpdateConfiguration1CDataItemDto
            {
                UseComConnectionForApplyUpdates = false,
                Name = configurationName,
                ConfigurationCatalog = "Accounting",
                PlatformCatalog = "8.3",
                RedactionCatalogs = "3.0",
                UpdateCatalogUrl = "http://downloads.v8.1c.ru/tmplts/",
                ConfigurationVariations = [configurationName],
                ItsAuthorizationDataId = GetDefaultItsAuthorizationData().Id
            });

            if (createResult.Error)
                throw new InvalidOperationException("Не удалось создать конфигуруацию 1С");
        }

        /// <summary>
        /// Получить дефолтные данные авторизации в ИТС
        /// </summary>
        /// <returns>Дефолтные данные авторизации в ИТС</returns>
        private ItsAuthorizationData GetDefaultItsAuthorizationData() =>
            _dbLayer.GetGenericRepository<ItsAuthorizationData>().FirstOrDefault() ??
            throw new NotFoundException("Не удалось получить данные авторизации в ИТС");
    }
}
