﻿using System;
using System.Threading.Tasks;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Тестовый Менеджер по работе с актами счетов на оплату
    /// </summary>
    public class InvoiceActManagerTestAdapter(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Установить номер акта для счета на оплату.
        /// </summary>
        /// <param name="invoiceId">Номер счета.</param>
        /// <param name="actId">Номер присваемого акта.</param>
        /// <param name="description">Коментарий к прикрепленному акту.</param>
        public async Task<ManagerResult> SetAct(Guid invoiceId, Guid? actId, string description, bool requiresSignature)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.Invoices_Confirmation);
                
                await DbLayer.InvoiceRepository.SetActId(invoiceId, actId, description, requiresSignature);
                
            }
            catch (Exception ex)
            {
                return PreconditionFailed(ex.Message);
            }

            return Ok();
        }
    }
}
