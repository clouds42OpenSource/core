﻿using System;
using System.Collections.Generic;
using Clouds42.CloudServices.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы со стоимостью сервиса "Мой Диск"
    /// </summary>
    public class MyDiskServiceCostHelper(ICloud42ServiceHelper cloud42ServiceHelper)
    {
        /// <summary>
        /// Проверить стоимость сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="checkConditionParams">Параметры для проверки условий</param>
        public void CheckServiceCostBySize(Guid accountId, List<CheckCostOfMyDiskParam> checkConditionParams)
        {
            var startCost = 0m;

            checkConditionParams.ForEach(checkCostOfMyDiskParam =>
            {
                startCost = CheckServiceCost(accountId, checkCostOfMyDiskParam.DiskSize, checkCostOfMyDiskParam.TryCount, startCost,
                    checkCostOfMyDiskParam.ExpectedCostForSize);
            });
        }

        /// <summary>
        /// Проверить стоимость сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="myDiskSize">Размер диска</param>
        /// <param name="tryCount">Кол-во попыток</param>
        /// <param name="startCost">Начальная стоимость</param>
        /// <param name="costPerIteration">Стоимость за итерацию</param>
        /// <returns>Рассчитанная стоимость</returns>
        private decimal CheckServiceCost(Guid accountId, int myDiskSize, int tryCount, decimal startCost,
            decimal costPerIteration)
        {
            startCost += costPerIteration;
            for (var item = 0; item < tryCount; item++, myDiskSize++)
            {
                var calculateCostOfMyDisk = cloud42ServiceHelper.ChangeSizeOfServicePreview(accountId, myDiskSize);
                Assert.AreEqual(startCost, calculateCostOfMyDisk.Cost);
                if (item != tryCount - 1)
                {
                    startCost += costPerIteration;
                }
            }

            return startCost;
        }
    }
}
