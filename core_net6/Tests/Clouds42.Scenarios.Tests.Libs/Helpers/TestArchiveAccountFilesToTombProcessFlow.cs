﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine;
using Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb;
using Clouds42.StateMachine.Contracts.ArchiveAccountFilesToTombProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Процесс архивации файлов аккаунта в склеп. Для тестов.
    /// </summary>
    public class TestArchiveAccountFilesToTombProcessFlow(
        Configurator configurator,
        AccountDataProvider accountDataProvider,
        IUnitOfWork dbLayer)
        : StateMachineBaseFlow<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>(
                configurator, dbLayer),
            IArchiveAccountFilesToTombProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса в тесте.
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(CopyAccountFilesBackupToTombParamsDto model)=>model.AccountId.ToString();


        /// <summary>
        /// Инициализировать карту архивации файлов аккаунта в склеп
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<CreateAccountFilesBackupAction, CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<RemoveAccountFilesFromStorageAction, CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>(
                ));
        }

        /// <summary>
        /// Сконвертировать результат архивации в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override CopyAccountFilesBackupToTombParamsDto ConvertResultToOutput(object result)=>(CopyAccountFilesBackupToTombParamsDto) result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(CopyAccountFilesBackupToTombParamsDto model)
        {
            var account =
                accountDataProvider.GetAccountOrThrowException(model.AccountId);

            return $"{account.Id}::{account.IndexNumber}"; 
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName  => "Тестовая архивация файлов аккаунта в склеп";
    }



}
