﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейк команда для установки расширения сервиса в информационную базу
    /// </summary>
    public class FakeInstallServiceExtensionDatabaseCommand : IInstallServiceExtensionDatabaseCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для установки расширения в базу
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public ManageServiceExtensionDatabaseResultDto Execute(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            return new ManageServiceExtensionDatabaseResultDto
            {
                ErrorMessage = string.Empty,
                IsSuccess = true,
                NeedRetry = false
            };
        }
    }
}
