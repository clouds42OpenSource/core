﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Тестовый хелпер для проверки корректности получения информации о пользователи
    /// </summary>
    public class UserInfoDataCheckTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Проверить, что корректно получили информацию про пользователя для предоставления доступа
        /// </summary>
        /// <param name="userInfoDataDto">Модель информации о пользователе</param>
        /// <param name="userId">Id пользователя</param>
        /// <param name="mushHaveAccess">Должен иметь доступ</param>
        public void CheckCorrectnessOfUserForGrantAccess(UserInfoDataDto userInfoDataDto, Guid userId, bool mushHaveAccess)
        {
            var user = _dbLayer.AccountUsersRepository.FirstOrDefault(usr => usr.Id == userId) ??
                       throw new NotFoundException($"Не найден пользователь аккаунта по Id {userId}");

            Assert.AreEqual(userInfoDataDto.UserEmail, user.Email);
            Assert.AreEqual(userInfoDataDto.UserFirstName, user.FirstName);
            Assert.AreEqual(userInfoDataDto.UserLastName, user.LastName);
            Assert.AreEqual(userInfoDataDto.UserMiddleName, user.MiddleName);
            Assert.AreEqual(userInfoDataDto.UserId, user.Id);
            Assert.AreEqual(userInfoDataDto.UserLogin, user.Login);
            Assert.AreEqual(userInfoDataDto.HasAccess, mushHaveAccess);
        }
    }
}
