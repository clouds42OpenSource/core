﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Clouds42.YookassaAggregator.Сonstants;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый провайдер для получения информации
    /// об объекте платежа агрегатора ЮKassa, который находится в процессе оплаты
    /// </summary>
    internal class GetPendingYookassaPaymentObjectInfoProviderFake : IGetYookassaPaymentObjectInfoProvider
    {
        /// <summary>
        /// Получить информацию об объекте платежа
        /// агрегатора ЮKassa
        /// (Заглушка)
        /// </summary>
        /// <param name="model">Модель для получения информации
        /// об объекте платежа агрегатора ЮKassa</param>
        /// <returns>Финальный объект платежа Юкасса</returns>
        public YookassaFinalPaymentObjectDto GetPaymentObjectInfo(GetYookassaPaymentObjectInfoDto model) =>
            new()
            {
                Id = model.YookassaPaymentId,
                CreationDate = DateTime.Now,
                Status = YookassaPaymentStatusEnum.Pending.GetStringValue(),
                Test = true,
                PaymentAmountData = new YookassaPaymentAmountDataDto
                {
                    Currency = YookassaAggregatorConst.CurrencyRub,
                    Amount = 200
                },
                YookassaPaymentMetadata = new YookassaPaymentMetadataDto(),
                YookassaPaymentRecipientData = new YookassaPaymentRecipientDataDto
                {
                    AccountId = "780876",
                    GatewayId = "1819860"
                }
            };
    }
}