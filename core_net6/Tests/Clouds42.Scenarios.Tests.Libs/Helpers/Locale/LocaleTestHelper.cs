﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Locales.Managers;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.Locale
{
    /// <summary>
    /// Хелпер для работы с локалями в тестах
    /// </summary>
    public class LocaleTestHelper(ITestContext testContext)
    {
        /// <summary>
        /// Создать конфигурацию локали
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="localeId">Id локали</param>
        /// <returns>Конфигурация локали</returns>
        public LocaleConfiguration CreateLocaleConfiguration(Guid segmentId, Guid localeId)
        {
            var localeConfiguration = new LocaleConfiguration
            {
                LocaleId = localeId,
                DefaultSegmentId = segmentId,
                CpSiteUrl = "url.test",
                ChangeDate = DateTime.Now
            };

            testContext.DbLayer.GetGenericRepository<LocaleConfiguration>().Insert(localeConfiguration);
            testContext.DbLayer.Save();

            return localeConfiguration;
        }

        /// <summary>
        /// Создать конфигурации локалей для всех локалей 
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <returns>Список конфигураций локалей</returns>
        public List<LocaleConfiguration> CreateLocaleConfigurationForAllLocales(Guid segmentId)
        {
            var localeConfigurations = new List<LocaleConfiguration>();

            testContext.ServiceProvider.GetRequiredService<LocaleManager>().GetLocales().Result.ForEach(locale =>
            {
                localeConfigurations.Add(new LocaleConfiguration
                {
                    LocaleId = locale.Id,
                    ChangeDate = DateTime.Now,
                    CpSiteUrl = $"test_url_{locale.Country}",
                    DefaultSegmentId = segmentId
                });
            });

            testContext.DbLayer.GetGenericRepository<LocaleConfiguration>().InsertRange(localeConfigurations);
            testContext.DbLayer.Save();

            return localeConfigurations;
        }
    }
}
