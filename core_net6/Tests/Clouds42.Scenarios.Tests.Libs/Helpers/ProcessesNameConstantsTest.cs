﻿namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public static class ProcessesNameConstantsTest
    {
        /// <summary>
        /// Название процесса для удаления инф. базы
        /// </summary>
        public const string RemovingDatabaseProcessName = "StartProcessOfRemoveDatabaseJob";

        /// <summary>
        /// Название процесса для архивации инф. базы
        /// </summary>
        public const string ArchivingDatabaseProcessName = "StartProcessOfArchiveDatabaseToTombJob";

        /// <summary>
        /// Название задачи для удаления инф. базы
        /// </summary>
        public const string RemovingDatabaseJobName = "DeleteAccountDatabaseJob";

        /// <summary>
        /// Название задачи для архивации инф. базы
        /// </summary>
        public const string ArchiveDatabaseJobName = "ArchivateAccountDatabaseToTombJob";

        /// <summary>
        /// Название задачи для создания инф. базы
        /// </summary>
        public const string CreateDatabaseFromDtJobName = "CreateDatabaseFromDtJob";

        /// <summary>
        /// Название задачи для восстановления из бэкапа
        /// </summary>
        public const string RestoreAccountDatabaseFromTombJobName = "RestoreAccountDatabaseFromTombJob";
    }
}
