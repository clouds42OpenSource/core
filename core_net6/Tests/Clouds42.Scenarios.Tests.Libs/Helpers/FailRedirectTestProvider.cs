﻿using System;
using Clouds42.CloudServices.Contracts;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{

	/// <summary>
	/// Тестовый фейловый провайдер блокировки и разблокировки опубликованных баз
	/// </summary>
	public class FailRedirectTestProvider : IRedirectPublishAccountDatabase
	{
		public void SetRedirectNeedMoneyToAccount(Guid accountId)
		{
			throw new InvalidOperationException("Произшла ошибка при установке редиректа");
		}

		public void DeleteRedirectionNeedMoneyForAccount(Guid accountId)
		{
			throw new InvalidOperationException("Произшла ошибка при снятии редиректа");
		}

	}
}
