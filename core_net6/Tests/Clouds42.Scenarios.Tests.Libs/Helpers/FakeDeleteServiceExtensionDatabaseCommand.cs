﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейк комманда для удаления расширения сервиса из информационной базы
    /// </summary>
    public class FakeDeleteServiceExtensionDatabaseCommand : IDeleteServiceExtensionDatabaseCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для удаления расширения в базе
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public ManageServiceExtensionDatabaseResultDto Execute(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            return new ManageServiceExtensionDatabaseResultDto
            {
                ErrorMessage = string.Empty,
                NeedRetry = false,
                IsSuccess = true
            };
        }
    }
}
