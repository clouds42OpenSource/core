﻿using Clouds42.Configurations;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public static class GetPublishServerHelper
    {
        public static string PathToPublishServer()
        {
            return ConfigurationHelper.GetConfigurationValue(nameof(PathToPublishServer)); 
        }
    }
}
