﻿using System;
using System.Linq.Expressions;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.TaskProviders;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public class TestActiveDirectoryTaskProcessor(IServiceProvider serviceProvider) : IActiveDirectoryTaskProcessor
    {
        public void TryDoImmediately(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            var action = actionExpression.Compile();
            var provider = serviceProvider.GetRequiredService<ActiveDirectoryProvider>();
            action(provider);
        }

        public void RunTask(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            TryDoImmediately(actionExpression);
        }
    }
}
