﻿using System.Threading.Tasks;
using Clouds42.Logger;
using Clouds42.SmsClient;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Заглушка для отправки смс
    /// </summary>
    public class FakeSmsClient : ISmsClient
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Отправить SMS
        /// </summary>
        /// <param name="phoneNumbers">Номер телефона</param>
        /// <param name="message">отправляемое сообщение</param>
        public async Task SendSms(string phoneNumbers, string message)
        {
            await Task.Run(() => _logger.Trace($"Имитация отправки смс на номер {phoneNumbers} с текстом {message}"));
        }
    }
}
