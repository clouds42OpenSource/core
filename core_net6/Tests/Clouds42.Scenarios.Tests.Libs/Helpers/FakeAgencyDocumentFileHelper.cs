﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хэлпер для создания фейковых файлов агентских документов
    /// </summary>
    public class FakeAgencyDocumentFileHelper
    {

        /// <summary>
        /// Сгенерировать файл
        /// </summary>
        /// <param name="fileName">Название файла</param>
        /// <param name="contentType">Тип контента</param>
        /// <param name="fileType">Тип файла</param>
        /// <returns>Файл</returns>
        public CloudFileDataDto<byte[]> GenerateFileData(string fileName, string contentType, AgentRequisitesFileTypeEnum fileType)
        {
            return new CloudFileDataDto<byte[]>
            {
                FileName = fileName,
                ContentType = contentType,
                Content = new byte[1024],
                AgentRequisitesFileType = fileType
            };
        }

        /// <summary>
        /// Сгенерировать файл
        /// </summary>
        /// <param name="fileName">Название файла</param>
        /// <param name="contentType">Тип контента</param>
        /// <returns>Файл</returns>
        public CloudFileDataDto<byte[]> GenerateFileData(string fileName, string contentType)
        {
            return new CloudFileDataDto<byte[]>
            {
                FileName = fileName,
                ContentType = contentType,
                Content = new byte[1024],
            };
        }

        /// <summary>
        /// Сгенерировать набор файлов для реквизитов юр. лица
        /// </summary>
        /// <returns>Набор файлов для реквизитов юр. лица</returns>
        public List<CloudFileDataDto<byte[]>> GenerateFilesDataForLegalPersonRequisites()
        {
            var files = new List<CloudFileDataDto<byte[]>>();

            var scanCopyContract = GenerateFileData("Очень важный скан договора.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyСontract);

            var scanCopyCertificate = GenerateFileData("Очень важный скан свидетельства о регистрации.png", "image",
                AgentRequisitesFileTypeEnum.ScanCopyCertificateRegistrationLegalEntity);

            var scanCopyInn = GenerateFileData("Очень важный скан инн.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyInn);

            files.Add(scanCopyContract);
            files.Add(scanCopyCertificate);
            files.Add(scanCopyInn);

            return files;
        }

        /// <summary>
        /// Сгенерировать набор файлов для реквизитов физ. лица
        /// </summary>
        /// <returns>Набор файлов для реквизитов физ. лица</returns>
        public List<CloudFileDataDto<byte[]>> GenerateFilesDataForPhysicalPersonRequisites()
        {
            var files = new List<CloudFileDataDto<byte[]>>();

            var scanCopyContract = GenerateFileData("Очень важный скан договора.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyСontract);

            var scanCopyPassport = GenerateFileData("Очень важный скан паспорта.png", "image",
                AgentRequisitesFileTypeEnum.ScanCopyPassportAndRegistration);

            var scanCopyInn = GenerateFileData("Очень важный скан инн.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyInn);

            var scanCopySnils = GenerateFileData("Очень важный скан снилс.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopySnils);

            files.Add(scanCopyContract);
            files.Add(scanCopyPassport);
            files.Add(scanCopyInn);
            files.Add(scanCopySnils);

            return files;
        }

        /// <summary>
        /// Сгенерировать набор файлов для реквизитов ИП
        /// </summary>
        /// <returns>Набор файлов для реквизитов ИП</returns>
        public List<CloudFileDataDto<byte[]>> GenerateFilesDataForSoleProprietorRequisites()
        {
            var files = new List<CloudFileDataDto<byte[]>>();

            var scanCopyContract = GenerateFileData("Очень важный скан договора.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyСontract);

            var scanCopyPassport = GenerateFileData("Очень важный скан паспорта.png", "image",
                AgentRequisitesFileTypeEnum.ScanCopyPassportAndRegistration);

            var scanCopyInn = GenerateFileData("Очень важный скан инн.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopyInn);

            var scanCopySnils = GenerateFileData("Очень важный скан снилс.png", "image",
                AgentRequisitesFileTypeEnum.ScanСopySnils);

            var scanCopyCertificate = GenerateFileData("Очень важный скан свидетельства о регистрации.png", "image",
                AgentRequisitesFileTypeEnum.ScanCopyCertificateRegistrationSoleProprietor);

            files.Add(scanCopyContract);
            files.Add(scanCopyPassport);
            files.Add(scanCopyInn);
            files.Add(scanCopySnils);
            files.Add(scanCopyCertificate);

            return files;
        }

        /// <summary>
        /// Генерация обьекта агентских реквизитов
        /// </summary>
        /// <param name="accountId">Guid аккаунта</param>
        /// <param name="requisitesType">Тип контракта</param>
        /// <param name="requisitesStatus">Статус реквизитов</param>
        /// <returns>Обьект агентских реквизитов</returns>
        public CreateAgentRequisitesDto GenerateAgentRequisites(Guid accountId, AgentRequisitesTypeEnum requisitesType, AgentRequisitesStatusEnum requisitesStatus)
        {
            var agentRequisitesDto = new CreateAgentRequisitesDto
            {
                AccountId = accountId,
                AgentRequisitesStatus = requisitesStatus,
                AgentRequisitesType = requisitesType,
                LegalPersonRequisites = (requisitesType != AgentRequisitesTypeEnum.LegalPersonRequisites) ?
                    new LegalPersonRequisitesDto() : new LegalPersonRequisitesDto
                {
                    Inn = "2132131231",
                    BankName = "TEST BANK",
                    Bik = "213213213",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    HeadFullName = "Тестов Тест Тестович",
                    HeadPosition = "Царь и Бог",
                    Kpp = "213213123",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    Ogrn = "2132131232132",
                    OrganizationName = "ОАО 'И так сойдет'",
                    PhoneNumber = "+79076172121",
                    SettlementAccount = "12412412412412412412",
                    Files = GenerateFilesDataForLegalPersonRequisites()
                }
                ,
                PhysicalPersonRequisites = (requisitesType != AgentRequisitesTypeEnum.PhysicalPersonRequisites) ? 
                    new PhysicalPersonRequisitesDto() : new PhysicalPersonRequisitesDto
                {
                    FullName = "Тестов Тест Тестович",
                    DateOfBirth = DateTime.Now.AddYears(-30).Date,
                    PassportSeries = "1234",
                    PassportNumber = "123456",
                    WhomIssuedPassport = "Не помню",
                    PassportDateOfIssue = DateTime.Now.AddYears(-14).Date,
                    RegistrationAddress = "г. Тест, ул. Тестовая",
                    Inn = "123456123456",
                    Snils = "12345612345",
                    SettlementAccount = "12412412412412412412",
                    Bik = "213213213",
                    BankName = "TEST BANK",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    Files = GenerateFilesDataForPhysicalPersonRequisites()

                },
                SoleProprietorRequisites = (requisitesType != AgentRequisitesTypeEnum.SoleProprietorRequisites) ? 
                    new SoleProprietorRequisitesDto() : new SoleProprietorRequisitesDto
                {
                    FullName = "Тестов Тест Тестович",
                    LegalAddress = "г. Тест, ул. Тестовая",
                    PhoneNumber = "+79076172121",
                    Ogrn = "123456789012345",
                    Inn = "123456789012",
                    SettlementAccount = "12412412412412412412",
                    Bik = "213213213",
                    BankName = "TEST BANK",
                    CorrespondentAccount = "12321312312312321321",
                    AddressForSendingDocuments = "г. Тест, ул. Тестовая",
                    Files = GenerateFilesDataForSoleProprietorRequisites()
                }

            };

            return agentRequisitesDto;
        }
    }
}
