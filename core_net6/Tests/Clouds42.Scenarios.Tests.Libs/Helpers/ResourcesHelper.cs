﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с ресурсами
    /// </summary>
    public class ResourcesHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить список ресурсов Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sponsorAccountId">Id спонсора аккаунта</param>
        /// <param name="checkSubjectForAvailability">Проверка субъекта на наличие</param>
        /// <returns>Список ресурсов аккаунта</returns>
        public List<Resource> GetListResourcesRental1CForAccount(Guid accountId, Guid? sponsorAccountId = null,
            bool checkSubjectForAvailability = false)
        {
            var resources = GetResourcesRental1CForAccount(accountId, sponsorAccountId).ToList();

            if (checkSubjectForAvailability)
                resources = resources.Where(w => w.Subject == null).ToList();

            return resources;
        }

        /// <summary>
        /// Получить ресурсы Аренды 1С для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="sponsorAccountId">Id спонсора аккаунта</param>
        /// <returns>Ресурсы аккаунта</returns>
        public IEnumerable<Resource> GetResourcesRental1CForAccount(Guid accountId, Guid? sponsorAccountId = null)
        {
            var resources = dbLayer.ResourceRepository.Where(w =>
                w.AccountId == accountId && w.AccountSponsorId == sponsorAccountId &&
                (w.BillingServiceType.SystemServiceType == ResourceType.MyEntUser || w.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb));

            return resources;
        }
    }
}
