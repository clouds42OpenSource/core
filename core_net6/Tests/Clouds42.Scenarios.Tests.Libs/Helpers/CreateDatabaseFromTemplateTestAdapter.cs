using Clouds42.AccountDatabase.Create;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using System;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    internal class CreateDatabaseFromTemplateTestAdapter(
        IServiceProvider serviceProvider,
        IRegisterDatabaseFromTemplateProvider registerDatabaseFromTemplateProvider)
        : AccountDatabaseCreatorBase(serviceProvider)
    {
        protected override void RegisterDatabaseOnServer(CreateDatabaseResult createdDatabase)
        {
            registerDatabaseFromTemplateProvider.RegisterDatabaseOnServer(new RegisterFileDatabaseFromTemplateModelDto
            {
                AccountDatabaseId = createdDatabase.AccountDatabase.Id,
                NeedPublish = createdDatabase.InfoDatabase.Publish,
                UsersIdsForAddAccess = createdDatabase.InfoDatabase.UsersToGrantAccess
            });
        }

    }
}
