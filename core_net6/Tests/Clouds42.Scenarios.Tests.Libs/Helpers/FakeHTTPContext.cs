﻿using Microsoft.AspNetCore.Http;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    public static class FakeHttpContextExtensions
    {
        public static void InstallAsCurrentContext(this HttpContext httpContext, string token, string url = "http://localhost:3465/", string queryString = "", string fileName = "")
        {
            var headers = httpContext.Request.Headers;
            headers.Add("Token", token);
        }
    }

    public class FakeHttpContextAccessor : IHttpContextAccessor
    {
        public HttpContext HttpContext { get; set; } = new DefaultHttpContext();
    }
}
