﻿using System.Collections.Generic;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Параметры проверки стоимости сервиса "Мой Диск"
    /// </summary>
    public static class CheckCostOfMyDiskParams
    {
        /// <summary>
        /// Получить параметры для Казахстана
        /// </summary>
        /// <returns>Параметры для Казахстана</returns>
        public static List<CheckCostOfMyDiskParam> GetParamsForKazakhstan()
            =>
            [
                new CheckCostOfMyDiskParam { DiskSize = 0, TryCount = 10, ExpectedCostForSize = 0m },
                new CheckCostOfMyDiskParam { DiskSize = 11, TryCount = 5, ExpectedCostForSize = 280m },
                new CheckCostOfMyDiskParam { DiskSize = 16, TryCount = 10, ExpectedCostForSize = 250m },
                new CheckCostOfMyDiskParam { DiskSize = 26, TryCount = 25, ExpectedCostForSize = 225m },
                new CheckCostOfMyDiskParam { DiskSize = 51, TryCount = 25, ExpectedCostForSize = 195m }
            ];

        /// <summary>
        /// Получить параметры для России
        /// </summary>
        /// <returns>Параметры для России</returns>
        public static List<CheckCostOfMyDiskParam> GetParamsForRussia()
            =>
            [
                new CheckCostOfMyDiskParam { DiskSize = 0, TryCount = 10, ExpectedCostForSize = 0m },
                new CheckCostOfMyDiskParam { DiskSize = 11, TryCount = 5, ExpectedCostForSize = 50m },
                new CheckCostOfMyDiskParam { DiskSize = 16, TryCount = 10, ExpectedCostForSize = 45m },
                new CheckCostOfMyDiskParam { DiskSize = 26, TryCount = 25, ExpectedCostForSize = 40m },
                new CheckCostOfMyDiskParam { DiskSize = 51, TryCount = 25, ExpectedCostForSize = 35m }
            ];

        /// <summary>
        /// Получить параметры для Украины
        /// </summary>
        /// <returns>Параметры для Украины</returns>
        public static List<CheckCostOfMyDiskParam> GetParamsForUkraine()
            =>
            [
                new CheckCostOfMyDiskParam { DiskSize = 0, TryCount = 10, ExpectedCostForSize = 0m },
                new CheckCostOfMyDiskParam { DiskSize = 11, TryCount = 5, ExpectedCostForSize = 35m },
                new CheckCostOfMyDiskParam { DiskSize = 16, TryCount = 10, ExpectedCostForSize = 33m },
                new CheckCostOfMyDiskParam { DiskSize = 26, TryCount = 25, ExpectedCostForSize = 30m },
                new CheckCostOfMyDiskParam { DiskSize = 51, TryCount = 25, ExpectedCostForSize = 28m }
            ];
    }

    /// <summary>
    /// Параметры проверки стоимости сервиса "Мой Диск"
    /// </summary>
    public class CheckCostOfMyDiskParam
    {
        /// <summary>
        /// Кол-во попыток
        /// </summary>
        public int TryCount { get; set; }

        /// <summary>
        /// Размер диска
        /// </summary>
        public int DiskSize { get; set; }

        /// <summary>
        /// Ожидаемая стоимость для указанного размера
        /// </summary>
        public decimal ExpectedCostForSize { get; set; }
    }
}
