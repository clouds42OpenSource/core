﻿using System;
using System.Collections.Generic;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для рабоы с дотсупами в базы в тестах
    /// </summary>
    public class AcDbAccessTestHelper(
        IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Проверить был ли удален доступ пользователей к инф. базе
        /// </summary>
        /// <param name="usersId">Список Id пользователей</param>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        public void CheckAcDbAccessesCreated(List<Guid> usersId, Guid databaseId, Guid accountId) =>
            usersId.ForEach(userId => CheckAcDbAccessInDbByCondition(userId, databaseId, accountId));

        /// <summary>
        /// Проверить был ли предоставлен доступ пользователям к инф. базе
        /// </summary>
        /// <param name="usersId">Список Id пользователей</param>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        public void CheckAcDbAccessesDeleted(List<Guid> usersId, Guid databaseId, Guid accountId) =>
            usersId.ForEach(userId => CheckAcDbAccessInDbByCondition(userId, databaseId, accountId, false));

        /// <summary>
        /// Проверить был ли предоставлен доступ пользователю к инф. базе
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        public void CheckAcDbAccessDeleted(Guid userId, Guid databaseId, Guid accountId) =>
            CheckAcDbAccessInDbByCondition(userId, databaseId, accountId, false);

        /// <summary>
        /// Создать операцию с доступом на разделителях
        /// </summary>
        /// <param name="acDbAccessId">Id доступа в инф. базу</param>
        /// <param name="accessState">Состояние доступа в инф. базу</param>
        public void CreateDbOnDelimitersAccess(Guid acDbAccessId, AccountDatabaseAccessState accessState)
        {
            var dbOnDelimitersAccessModel = new ManageDbOnDelimitersAccess
            {
                Id = acDbAccessId,
                AccessState = accessState,
                StateUpdateDateTime = DateTime.Now
            };

            dbLayer.ManageDbOnDelimitersAccessRepository.Insert(dbOnDelimitersAccessModel);
            dbLayer.Save();
        }

        /// <summary>
        /// Создать доступ к инф. базе
        /// </summary>
        /// <param name="accountDatabaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="localUserId">Id локального пользователя</param>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        /// <returns>Объект доступа</returns>
        public AcDbAccess CreateAcDbAccess(Guid accountDatabaseId, Guid accountId, Guid localUserId, Guid accountUserId)
        {
            var accessEntity =
                CreateAcDbAccessObjHelper.CreateAcDbAccessObj(accountDatabaseId, accountId, localUserId, accountUserId);

            dbLayer.AcDbAccessesRepository.Insert(accessEntity);
            dbLayer.Save();

            return accessEntity;
        }

        /// <summary>
        /// Проверить что операция с доступом на разделителях была удалена
        /// </summary>
        /// <param name="acDbAccessId">Id доступа к инф. базе</param>
        /// <returns>Признак удалена запись</returns>
        public bool CheckDbOnDelimitersAccessWasDeleted(Guid acDbAccessId) =>
            dbLayer.ManageDbOnDelimitersAccessRepository.FirstOrDefault(w => w.Id == acDbAccessId) == null;
        

        /// <summary>
        /// Проверить доступ пользователя к инф. базе по условию hasToBeCreated
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="hasToBeCreated">Признак доступ должен быть создан/удален</param>
        private void CheckAcDbAccessInDbByCondition(Guid userId, Guid databaseId, Guid accountId, bool hasToBeCreated = true)
        {
            var acDbAccess = dbLayer.AcDbAccessesRepository.FirstOrDefault(acDb =>
                acDb.AccountID == accountId && acDb.AccountUserID == userId && acDb.AccountDatabaseID == databaseId);

            var message = $"Доступ в инф. базу {databaseId} для пользователя {userId} аккаунта {accountId} не был ";

            switch (hasToBeCreated)
            {
                case true when acDbAccess == null:
                    throw new NotFoundException(message +  "создан");
                case false when acDbAccess != null:
                    throw new NotFoundException(message + "удален");
            }
        }
    }
}
