﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.IISApplication.Constants;
using Clouds42.AccountDatabase.Contracts.IISApplication.Models;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Accounts.Account.Helpers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер по формированию путей публикации
    /// </summary>
    public class FormationOfPublicationPathHelper(IUnitOfWork dbLayer, IisHttpClient connectionProviderIis, ICloudLocalizer cloudLocalizer)
    {
        /// <summary>
        /// Получить пути публикации
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Модель путей публикации</returns>
        public PublicationPathsModel GetPublicationPaths(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var publicationPathsModel = new PublicationPathsModel();
            var accountEncodeId = accountDatabase.Account.GetEncodeAccountId();
            var ibName = accountDatabase.V82Name;
            var segmentHelper = new SegmentHelper(dbLayer, cloudLocalizer);
            var accountContentServer = segmentHelper.GetContentServer(accountDatabase.Account);
            var publishSiteName = segmentHelper.GetPublishSiteName(accountDatabase.Account);

            var contentServerNode = dbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
           
            var siteRoot = connectionProviderIis.Get<Root>(contentServerNode.PublishNodeReference.Address, IISLinksConstants.WebSites);
            if (siteRoot?.Websites == null || !siteRoot.Websites.Any())
            {
                throw new InvalidOperationException("Нода не доступна");
            }
            var site = siteRoot.Websites.FirstOrDefault(x => x.Name == publishSiteName) ?? throw new InvalidOperationException($"Не найдено опубликованное приложение {publishSiteName} на ноде {contentServerNode.PublishNodeReference.Address}"); ;
       
            var host = contentServerNode.PublishNodeReference.Address;
            var siteInfo = connectionProviderIis.Get<SiteModel>(host, site.Links.Self.Href);
           
            publicationPathsModel.BasePath = segmentHelper.GetPublishedDatabasePhycicalPath(siteInfo.PhysicalPath, accountEncodeId, ibName, accountDatabase.Account);

            publicationPathsModel.BaseAddress = segmentHelper.GetIisWebAddress(accountEncodeId, ibName, accountDatabase.Account);

            if (!Directory.Exists(publicationPathsModel.BasePath))
            {
                Directory.CreateDirectory(publicationPathsModel.BasePath);
            }
            else
            {
                var appRoot =
                    connectionProviderIis.Get<Root>(host, siteInfo.Links.Webapps.Href);
                if (appRoot?.Webapps == null)
                {
                    return publicationPathsModel;
                }

                var oldApp = appRoot.Webapps.FirstOrDefault(a => a.Path == publicationPathsModel.BaseAddress);

                if (oldApp != null)
                {
                    connectionProviderIis.Delete(host, $"{IISLinksConstants.WebApps}?id={oldApp.Id}");
                }
            }

            return publicationPathsModel;
        }

    }

    /// <summary>
    /// Модель путей публикации
    /// </summary>
    public class PublicationPathsModel
    {
        /// <summary>
        /// Физический пть где будет размещена база
        /// </summary>
        public string BasePath { get; set; }

        /// <summary>
        /// web-адрес для опубликованной базы
        /// </summary>
        public string BaseAddress { get; set; }
    }
}
