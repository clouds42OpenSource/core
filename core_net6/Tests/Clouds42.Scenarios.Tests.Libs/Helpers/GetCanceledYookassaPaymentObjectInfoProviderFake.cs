﻿using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.YookassaAggregator.Providers.Interfaces;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый провайдер для получения информации
    /// об объекте отмененного платежа агрегатора ЮKassa
    /// </summary>
    internal class GetCanceledYookassaPaymentObjectInfoProviderFake : IGetYookassaPaymentObjectInfoProvider
    {
        /// <summary>
        /// Получить информацию об объекте платежа
        /// агрегатора ЮKassa
        /// (Заглушка)
        /// </summary>
        /// <param name="model">Модель для получения информации
        /// об объекте платежа агрегатора ЮKassa</param>
        /// <returns>Финальный объект платежа Юкасса</returns>
        public YookassaFinalPaymentObjectDto GetPaymentObjectInfo(GetYookassaPaymentObjectInfoDto model) =>
            GenerateYookassaFinalPaymentObjectDtoHelperTest.Generate(model.YookassaPaymentId, false,
                YookassaPaymentStatusEnum.Canceled);
    }
}