﻿using System;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.StateMachine.Contracts.Models;
using Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейк процесс создания инф. базы из zip в МС
    /// </summary>
    public class CreateDatabaseFromZipInMsProcessFlowFake : ICreateDatabaseFromZipInMsProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров обновления названия инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат обновления названия</returns>
        public StateMachineResult<bool> Run(CreateDatabaseFromZipInMsParamsDto model, bool throwExceptionIfError = true)
        {
            return new StateMachineResult<bool>
            {
                Message = "",
                Finish = true,
                ResultModel = true,
                ProcessFlowId = Guid.NewGuid()
            };
        }
    }
}
