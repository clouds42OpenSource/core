﻿using System;
using System.Collections.Generic;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.Requisites.Interfaces;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Service.CoreHosting;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Clouds42.Scenarios.Tests.Libs.Scenarios.Commands;
using Clouds42.Segment.CoreHosting.Mappers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Класс, хелпер по созданию аккаунтов
    /// </summary>
    public class CreateAccountTestHelper(IUnitOfWork dbLayer, ITestContext testContext)
    {
        private readonly AlphaNumeric42CloudsGenerator _alphaNumeric42CloudsGenerator = new(new CryptoRandomGeneric());
        private readonly IUpdateAccountRequisitesProvider _updateAccountRequisitesProvider = testContext.ServiceProvider.GetRequiredService<IUpdateAccountRequisitesProvider>();

        /// <summary>
        /// Создать Account и UserAccount по полям Accont
        /// </summary>
        /// <param name="registrationDate">Дата регистрации</param>
        /// <param name="accountCaption">Учетная запись</param>
        /// <param name="inn">ИНН</param>
        /// <returns></returns>
        public CreateAccountCommand CreateAccountAndAccountUserOnAccountDataFields(DateTime? registrationDate,
            string accountCaption = null, string inn = null)
        {
            var createAccountCommand = new CreateAccountCommand(testContext, GenerateAccountRegistrationModel);
            createAccountCommand.Run();

            var account = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == createAccountCommand.AccountId);

            if (registrationDate.HasValue)
                account.RegistrationDate = registrationDate;

            if (!string.IsNullOrEmpty(accountCaption))
                account.AccountCaption = accountCaption;

            if (!string.IsNullOrEmpty(inn))
                _updateAccountRequisitesProvider.Update(new AccountRequisitesDto
                {
                    AccountId = account.Id,
                    Inn = inn
                });

            dbLayer.AccountsRepository.Update(account);
            dbLayer.Save();

            return createAccountCommand;
        }

        /// <summary>
        /// Создать Account и UserAccount по полям AccontUser
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="middleName">Отчетсво</param>
        /// <param name="email">Почтовый адрес</param>
        /// <param name="phoneNumber">Номер телефона</param>
        /// <returns></returns>
        public CreateAccountCommand CreateAccountAndAccountUserOnAccUserDataFields(RegistrationConfigDomainModelDto registrationConfig = null,
            string login = null,string firstName = null, string lastName = null, string middleName = null, string email = null,
            string phoneNumber = null)
        {

            var accountRegistrationModel = new AccountRegistrationModelTest
            {
                Login = login ?? $"Test{DateTime.Now:HHmmssffff}",
                Inn = "914567890",
                FirstName = firstName ?? "Сергей",
                Email = email ?? $"Email{DateTime.Now:HHmmssffff}@efsol.ru",
                LastName = lastName ?? "Шушабтыр",
                MiddleName = middleName ?? "Ешенович",
                AccountCaption = "Test account",
                FullPhoneNumber = phoneNumber ?? $"{PhoneNumberGeneratorHelperTest.Generate()}",
                RegistrationConfig = registrationConfig ??new RegistrationConfigDomainModelDto(),
                Password = _alphaNumeric42CloudsGenerator.GeneratePassword(),
                LocaleName = "test"
            };

            var createAccountCommand = new CreateAccountCommand(testContext, accountRegistrationModel);
            createAccountCommand.Run();

            var accountUser =
                dbLayer.AccountUsersRepository.FirstOrDefault(
                    w => w.Id == createAccountCommand.AccountAdminId);

            if (!string.IsNullOrEmpty(phoneNumber))
                accountUser.PhoneNumber = phoneNumber;

            dbLayer.AccountUsersRepository.Update(accountUser);
            dbLayer.Save();

            return createAccountCommand;
        }

        /// <summary>
        /// Создать Account и UserAccount по конфиг. полям
        /// </summary>
        /// <param name="corpUserSyncStatus">Статус пользователя</param>
        /// <param name="isVipAccount">Является ли аккаунт VIP</param>
        /// <param name="groups">Список груп, к которому принадлежит пользователь</param>
        /// <param name="isSaleManager">Является ли пользователь менеджером по продажам</param>
        /// <param name="status">Статус аккаунта</param>
        /// <returns></returns>
        public CreateAccountCommand CreateAccountAndAccountUserOnConfigurationFields(string corpUserSyncStatus = null, bool? isVipAccount = null,
            List<AccountUserGroup> groups = null, bool? isSaleManager = null, string status = null)
        {
            var createAccountCommand = new CreateAccountCommand(testContext, GenerateAccountRegistrationModel);

            if (groups != null)
                createAccountCommand.Groups = groups;
            createAccountCommand.Run();

            var account = dbLayer.AccountsRepository.FirstOrDefault(w => w.Id == createAccountCommand.AccountId);

            var accountUser =
                dbLayer.AccountUsersRepository.FirstOrDefault(
                    w => w.Id == createAccountCommand.AccountAdminId);

            if (corpUserSyncStatus != null)
                accountUser.CorpUserSyncStatus = corpUserSyncStatus;

            if (isVipAccount.HasValue)
                testContext.ServiceProvider.GetRequiredService<IUpdateAccountConfigurationProvider>()
                    .UpdateSignVipAccount(account.Id, isVipAccount.Value);

            if (isSaleManager != null && (bool) isSaleManager)
            {
                var accountSaleManager = new AccountSaleManager
                {
                    AccountId = account.Id,
                    SaleManagerId = accountUser.Id,
                    CreationDate = DateTime.Now,
                    Division = nameof(Test)
                };

                dbLayer.AccountSaleManagerRepository.Insert(accountSaleManager);
            }

            if (!string.IsNullOrEmpty(status))
                account.Status = status;

            dbLayer.AccountUsersRepository.Update(accountUser);
            dbLayer.AccountsRepository.Update(account);
            dbLayer.Save();

            return createAccountCommand;
        }

        /// <summary>
        /// Сгенерировать модель регистрации тестового аккаунта
        /// </summary>
        public AccountRegistrationModelTest GenerateAccountRegistrationModel => new()
        {
            Login = $"Test{DateTime.Now:HHmmssffff}",
            Inn = "914567890",
            FirstName = "Сергей",
            Email = $"Email{DateTime.Now:HHmmssffff}@efsol.ru",
            LastName = "Лепешкин",
            MiddleName = "Суренович",
            AccountCaption = "Test account",
            FullPhoneNumber = $"{PhoneNumberGeneratorHelperTest.Generate()}",
            RegistrationConfig = new RegistrationConfigDomainModelDto(),
            Password = _alphaNumeric42CloudsGenerator.GeneratePassword()
        };

        /// <summary>
        /// Генерирует модель хостинга для согздания сегмента в тесте
        /// </summary>
        /// <returns>Модель хостинга</returns>
        public CoreHostingDto GetDefaultCoreHosting()
        {
            var coreHosting = dbLayer.CoreHostingRepository.FirstOrDefault(w => w.Name.ToLower() == "42Clouds".ToLower()) ??
                              throw new NotFoundException("Хостинг не найден");

            return coreHosting.MapToCoreHostingDc();
        }

    }
}
