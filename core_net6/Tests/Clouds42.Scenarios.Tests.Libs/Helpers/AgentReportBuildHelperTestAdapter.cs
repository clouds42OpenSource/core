﻿using System;
using Clouds42.AgencyAgreement.Contracts.AgencyAgreement;
using Clouds42.AgencyAgreement.DocumentBuilder.RazorBuilderData;
using Clouds42.AgencyAgreement.Partner.Helpers;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    internal class AgentReportBuildHelperTestAdapter(
        IUnitOfWork dbLayer,
        PrintedHtmlFormBuilder printedHtmlFormBuilder,
        IAgencyAgreementDataProvider agencyAgreementDataProvider,
        IHandlerException handlerException)
        : AgentReportBuildHelper(dbLayer, printedHtmlFormBuilder, handlerException, agencyAgreementDataProvider)
    {
        public AgentReportDto GenerateAgentReportDtoTest(Guid agentCashOutRequestId)
        {
            return GenerateAgentReportDto(agentCashOutRequestId);
        }
    }
}
