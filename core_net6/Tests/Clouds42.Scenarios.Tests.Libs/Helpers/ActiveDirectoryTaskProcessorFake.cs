﻿using Clouds42.ActiveDirectory.Contracts;
using System;
using System.Linq.Expressions;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Фейковый процессор задач Active Directory
    /// </summary>
    public class ActiveDirectoryTaskProcessorFake : IActiveDirectoryTaskProcessor
    {
        /// <summary>
        /// Попытаться выполнить таск немедленно (Заглушка)
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        public void TryDoImmediately(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            // Заглушка для тестов
        }

        /// <summary>
        /// Запустить таску (Заглушка)
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        public void RunTask(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            // Заглушка для тестов
        }
    }
}
