﻿using System;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Clouds42.Scenarios.Tests.Libs.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания шаблонов информационных баз на разделителях для тестов
    /// </summary>
    public class CreateDbTemplateDelimitersTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
        private readonly DbTemplateDelimitersManager _dbTemplateDelimiterManager = testContext.ServiceProvider.GetRequiredService<DbTemplateDelimitersManager>();

        /// <summary>
        /// Создать шаблон информационной базы на разделителях
        /// </summary>
        /// <param name="templateName">Название шаблона</param>
        /// <param name="configurationId">Id конфигурации</param>
        public void CreateDbTemplateDelimiters(string templateName, string configurationId)
        {

            var existingTemplate = _dbTemplateDelimiterManager.GetDbTemplateDelimiter(configurationId);
            if (existingTemplate.Result != null)
                throw new InvalidOperationException($"Шаблон с Id {configurationId} уже существует");

            var templateId = GetDbTemplateByTemplateName(templateName);
            AddNewDbTemplateDelimiters(configurationId, templateId);
        }

        /// <summary>
        /// Получить шаблон по Id
        /// </summary>
        /// <param name="templateName">Название шаблона</param>
        /// <returns>Id шаблона</returns>
        private Guid GetDbTemplateByTemplateName(string templateName)
        {
            var template = _dbLayer.DbTemplateRepository
                .FirstOrDefault(dbtemplate => dbtemplate.Name == templateName);

            if (template == null)
                throw new NotFoundException($"Не найден шаблон конфигурации по названию {templateName}");

            return template.Id;
        }

        /// <summary>
        /// Добавить новый шаблон инф. базы на разделителях
        /// </summary>
        /// <param name="configurationId">Id конфигурации</param>
        /// <param name="templateId">Id шаблона</param>
        private void AddNewDbTemplateDelimiters(string configurationId, Guid templateId)
        {
            var dbTemplate = new DbTemplateDelimitersModelTest(testContext)
            {
                ConfigurationId = configurationId,
                TemplateId = templateId
            };

            var result = _dbTemplateDelimiterManager.AddDbTemplateDelimiterItem(dbTemplate);

            if (result.Error)
                throw new InvalidOperationException(result.Message);

            var dbTemplateDelimiter = _dbTemplateDelimiterManager.GetDbTemplateDelimiter(configurationId);
            if (dbTemplateDelimiter == null)
                throw new NotFoundException($"Не найден шаблон инф. базы на разделителях по Id {configurationId}");
        }
    }
}
