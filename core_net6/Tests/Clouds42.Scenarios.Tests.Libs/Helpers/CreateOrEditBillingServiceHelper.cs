﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Models;
using CommonLib.Enums;
using shortid;
using shortid.Configuration;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с созданием или редактированием сервиса биллинга
    /// </summary>
    public class CreateOrEditBillingServiceHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Сгенерировать модель услуги сервиса
        /// </summary>
        /// <param name="name">Название услуги</param>
        /// <param name="billingType">Тип работы биллинга</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        /// <param name="dependServiceTypeId">Зависимость от услуги</param>
        public BillingServiceTypeDto GenerateBillingServiceType(string name = null, BillingTypeEnum? billingType = null,
            decimal serviceTypeCost = 400, Guid? dependServiceTypeId = null)
        {

            if (dependServiceTypeId.IsNullOrEmpty())
                dependServiceTypeId = GetWebServiceTypeOrThrowException().Id;

            var id = Guid.NewGuid();
            var billingServiceTypeDto = new BillingServiceTypeDto
            {
                Id = id,
                Key = id,
                Name = name ?? $"{ShortId.Generate(new GenerationOptions(true, false, 16))}",
                BillingType = billingType ?? BillingTypeEnum.ForAccountUser,
                DependServiceTypeId = dependServiceTypeId,
                ServiceTypeCost = serviceTypeCost,
                Description = "Тестовая услуга"
            };

            return billingServiceTypeDto;
        }

        /// <summary>
        /// Сгенерировать список услуг биллинга по количеству
        /// </summary>
        /// <param name="count">Количество услуг биллинга</param>
        /// <param name="dependServiceTypeId">Id услуги от которой зависят</param>
        /// <returns>Список услуг</returns>
        public List<BillingServiceTypeDto> GenerateListBillingServiceTypesByCount(int count, Guid? dependServiceTypeId = null)
        {
            var billingServiceTypesDto = new List<BillingServiceTypeDto>();

            for (int i = 0; i < count; i++)
            {
                billingServiceTypesDto.Add(GenerateBillingServiceType(dependServiceTypeId: dependServiceTypeId));
            }
            return billingServiceTypesDto;
        }


        private BillingServiceType GetWebServiceTypeOrThrowException() =>
            dbLayer.BillingServiceTypeRepository.FirstOrDefault(
                st => st.SystemServiceType == ResourceType.MyEntUserWeb) ??
            throw new InvalidOperationException("Не заполнен справочник billing.ServiceTypes не указана web услуга сервиса Аренда 1С");


        /// <summary>
        /// Сравнить схожесть модели редактирования сервиса и модели сервиса
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Резултат сравнения схожести</returns>
        public ResultOfComparingModelsTest CompareSimilarityOfServiceEditingModelAndService(
            EditBillingServiceDto editBillingServiceDto,
            BillingService billingService)
        {
            var comparingResultOfPerformingOperations =
                CompareSimilarityOfPerformingOperationsOnBillingServiceModelAndService(editBillingServiceDto,
                    billingService);

            return !comparingResultOfPerformingOperations.Similarity ? comparingResultOfPerformingOperations : new ResultOfComparingModelsTest { Similarity = true };
        }

        /// <summary>
        /// Сравнить схожесть модели редактирования черновика сервиса и модели сервиса
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика сервиса</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Резултат сравнения схожести</returns>
        public ResultOfComparingModelsTest CompareSimilarityOfServiceEditingDraftModelAndService(
            EditBillingServiceDraftDto editBillingServiceDraftDto,
            BillingService billingService)
        {
            var comparingResultOfPerformingOperations =
                CompareSimilarityOfPerformingOperationsOnBillingServiceModelAndService(editBillingServiceDraftDto,
                    billingService);

            return !comparingResultOfPerformingOperations.Similarity ? comparingResultOfPerformingOperations : new ResultOfComparingModelsTest { Similarity = true };
        }

        /// <summary>
        /// Сравнить схожесть модели создания сервиса и модели сервиса
        /// </summary>
        /// <param name="createBillingService">Модель создания сервиса</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns>Результат сравнения схожести</returns>
        public ResultOfComparingModelsTest CompareSimilarityOfServiceCreationModelAndService(CreateBillingServiceDto createBillingService,
            BillingService billingService)
        {

            var comparingResultOfPerformingOperations =
                CompareSimilarityOfPerformingOperationsOnBillingServiceModelAndService(createBillingService,
                    billingService);

            if (!comparingResultOfPerformingOperations.Similarity)
                return comparingResultOfPerformingOperations;
            
            var billingServiceTypes = billingService.BillingServiceTypes.ToList();

            var comparingServiceTypes = CompareSimilarityOfServiceTypes(createBillingService.BillingServiceTypes,
                billingServiceTypes);

            if (!comparingServiceTypes.Similarity)
                return comparingServiceTypes;

            var comparingServiceTypeRelations = CompareSimilarityOfServiceTypeRelations(
                createBillingService.BillingServiceTypes, billingServiceTypes);

            return !comparingServiceTypeRelations.Similarity ? comparingServiceTypeRelations : new ResultOfComparingModelsTest { Similarity = true };
        }

        /// <summary>
        /// Сравнить схожесть модели выполнения операций
        /// над сервисом и модели сервиса
        /// </summary>
        /// <param name="baseOperationBillingServiceDto">Модель создания сервиса</param>
        /// <param name="billingService">Модель сервиса</param>
        /// <returns></returns>
        private static ResultOfComparingModelsTest CompareSimilarityOfPerformingOperationsOnBillingServiceModelAndService(BaseOperationBillingServiceDto baseOperationBillingServiceDto,
            BillingService billingService)
        {
            return baseOperationBillingServiceDto.Name != billingService.Name ? new ResultOfComparingModelsTest { Similarity = false, Message = "Названия сервиса не совпадают" } : new ResultOfComparingModelsTest { Similarity = true };
        }

        /// <summary>
        /// Сравнить схожесть услуг
        /// </summary>
        /// <param name="billingServiceTypeDtos">Модели создания услуг</param>
        /// <param name="billingServiceTypes">Модели услуг</param>
        /// <returns>Результат сравнения схожести</returns>
        private ResultOfComparingModelsTest CompareSimilarityOfServiceTypes(
            List<BillingServiceTypeDto> billingServiceTypeDtos, List<BillingServiceType> billingServiceTypes)
        {
            foreach (var billingServiceType in billingServiceTypes)
            {
                var billingServiceTypeDto =
                    billingServiceTypeDtos.FirstOrDefault(w => w.Name == billingServiceType.Name);

                if (billingServiceTypeDto == null)
                    return new ResultOfComparingModelsTest { Similarity = false, Message = "Названия услуг не совпадают" };

                if(billingServiceType.BillingType != billingServiceTypeDto.BillingType)
                    return new ResultOfComparingModelsTest { Similarity = false, Message = "Тип биллинга у услуг не совпадают" };

                if (billingServiceType.DependServiceTypeId != billingServiceTypeDto.DependServiceTypeId)
                    return new ResultOfComparingModelsTest { Similarity = false, Message = "Зависимость от услуги Аренды у услуг не совпадают" };

                if (billingServiceType.DependServiceTypeId != billingServiceTypeDto.DependServiceTypeId)
                    return new ResultOfComparingModelsTest { Similarity = false, Message = "Зависимость от услуги Аренды у услуг не совпадают" };

                var serviceTypeCost =
                    dbLayer.RateRepository.FirstOrDefault(w => w.BillingServiceTypeId == billingServiceType.Id);

                if (serviceTypeCost != null && serviceTypeCost.Cost != billingServiceTypeDto.ServiceTypeCost)
                    return new ResultOfComparingModelsTest { Similarity = false, Message = "Стоимость услуг не совпадают" };

                ChangeTemporaryIdToConstantId(billingServiceTypeDto.Id, billingServiceType.Id, billingServiceTypeDtos);
                billingServiceTypeDto.Id = billingServiceType.Id;
            }

            return new ResultOfComparingModelsTest{Similarity = true};
        }

        /// <summary>
        /// Сравнить схожесть связи услуг
        /// </summary>
        /// <param name="billingServiceTypeDtos">Модели создания услуг</param>
        /// <param name="billingServiceTypes">Модели услуг</param>
        /// <returns>Результат сравнения схожести</returns>
        private static ResultOfComparingModelsTest CompareSimilarityOfServiceTypeRelations(
            List<BillingServiceTypeDto> billingServiceTypeDtos, List<BillingServiceType> billingServiceTypes)
        {
            return (from billingServiceType in billingServiceTypes ?? Enumerable.Empty<BillingServiceType>()
                    let billingServiceTypeDto = billingServiceTypeDtos?.FirstOrDefault(w => w.Id == billingServiceType.Id)
                    let billingServiceTypeRelations = billingServiceType.ChildRelations?.Select(w => w.ChildServiceTypeId).ToList() ?? new List<Guid>()
                    where billingServiceTypeDto != null && !billingServiceTypeDto.ServiceTypeRelations.All(billingServiceTypeRelations.Contains)
                    select billingServiceTypeDto).Any() ?
                new ResultOfComparingModelsTest { Similarity = false, Message = "Связи услуг не совпадают" } :
                new ResultOfComparingModelsTest { Similarity = true };
        }

        /// <summary>
        /// Сменить временный Id на постоянный
        /// </summary>
        /// <param name="temporaryId">Временный Id</param>
        /// <param name="constantId">Постоянный Id</param>
        /// <param name="billingServiceTypesDto">Услуги сервиса</param>
        private void ChangeTemporaryIdToConstantId(Guid temporaryId, Guid constantId,
            List<BillingServiceTypeDto> billingServiceTypesDto)
        {
            foreach (var billingServiceTypeDto in billingServiceTypesDto)
            {
                var serviceTypeRelationIdx = billingServiceTypeDto.ServiceTypeRelations.IndexOf(temporaryId);

                if (serviceTypeRelationIdx == -1)
                    continue;

                billingServiceTypeDto.ServiceTypeRelations[serviceTypeRelationIdx] = constantId;
            }
        }
    }
}
