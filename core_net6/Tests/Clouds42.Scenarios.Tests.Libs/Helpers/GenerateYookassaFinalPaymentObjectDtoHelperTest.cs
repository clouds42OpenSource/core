﻿using System;
using Clouds42.Billing.BillingOperations.Constants;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.Enums;
using Clouds42.YookassaAggregator.Сonstants;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для генерации тестового финального объекта платежа Юкасса
    /// </summary>
    public static class GenerateYookassaFinalPaymentObjectDtoHelperTest
    {
        /// <summary>
        /// Сформировать финальный объект платежа Юкасса
        /// </summary>
        /// <param name="paymentId">Id платежа</param>
        /// <param name="isPaid">Платеж оплачен</param>
        /// <param name="yookassaPaymentStatus">Статус платежа</param>
        /// <returns>Финальный объект платежа Юкасса</returns>
        public static YookassaFinalPaymentObjectDto Generate(Guid paymentId, bool isPaid,
            YookassaPaymentStatusEnum yookassaPaymentStatus) => new()
        {
                Id = paymentId,
                Description = nameof(Test),
                CreationDate = DateTime.Now.AddMinutes(-5),
                Status = yookassaPaymentStatus.GetStringValue(),
                Test = true,
                IsPaid = isPaid,
                PaymentAmountData = new YookassaPaymentAmountDataDto
                {
                    Currency = YookassaAggregatorConst.CurrencyRub,
                    Amount = 200
                },
                AuthorizationDetails = new YookassaPaymentAuthorizationDetailsDto
                {
                    ReferenceRetrievalNumber = "618105675769",
                    AuthCode = "674933"
                },
                CapturedDate = DateTime.Now.AddMinutes(-3),
                IncomeAmount = new YookassaPaymentAmountDataDto
                {
                    Currency = YookassaAggregatorConst.CurrencyRub,
                    Amount = 195
                },
                YookassaPaymentMetadata = new YookassaPaymentMetadataDto(),
                YookassaPaymentRecipientData = new YookassaPaymentRecipientDataDto
                {
                    AccountId = "780876",
                    GatewayId = "1819860"
                },
                PaymentMethod = new YookassaPaymentMethodDto
                {
                    IsSaved = true,
                    Type = EventOnChangeYookassaPaymentStatusConst.PaymentMethodTypeBankCard,
                    PaymentTemplateId = Guid.NewGuid(),
                    Card = new YookassaPaymentCardDto
                    {
                        CardType = "Visa",
                        IssuerCountry = "US",
                        LastFourDigits = "6789",
                        FirstSixDigits = "012345",
                        ExpiryMonth = DateTime.Now.AddMonths(1).Month.ToString(),
                        ExpiryYear = DateTime.Now.Year.ToString(),
                        IssuerName = "TestBank"
                    }
                }
            };
    }
}
