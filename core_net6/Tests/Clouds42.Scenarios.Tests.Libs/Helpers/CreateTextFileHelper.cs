﻿using System.IO;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для работы с текстовыми файлами в тестах
    /// </summary>
    public class CreateTextFileHelper
    {
        /// <summary>
        /// Создание текстового файла для теста
        /// </summary>
        /// <param name="filePath">Полное имя файла</param>
        public void CreateTextFileForTest(string filePath)
        {
            var file = new FileInfo(filePath);

            if (file.Exists)
                return;

            using var sw = file.CreateText();
            sw.WriteLine("Text");
            sw.WriteLine("For");
            sw.WriteLine("TestFile");
        }
    }
}
