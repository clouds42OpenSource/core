﻿using System;
using System.IO;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Logger;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers.JobWrappers
{
    /// <summary>
    /// Обработчик задачи создания директории
    /// </summary>
    public class TestCreateDirectoryJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ParametrizationJobWrapperBase<CreateDirectoryInfoParams>(registerTaskInQueueProvider, dbLayer),
            ICreateDirectoryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик</returns>
        public override ParametrizationJobWrapperBase<CreateDirectoryInfoParams> Start(CreateDirectoryInfoParams paramsJob)
        {
            try
            {
                if (!Directory.Exists(paramsJob.DirectoryPath))
                    Directory.CreateDirectory(paramsJob.DirectoryPath);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Не удалось создать директорию] {paramsJob.DirectoryPath}");
            }

            return this;
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.CreateDirectoryJob;
    }
}
