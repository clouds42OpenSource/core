﻿using System;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework.Legacy;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания тестовых платежей агрегатора ЮKassa
    /// </summary>
    public class CreateYookassaAggregatorPaymentHelperTest(IServiceProvider diContainer)
    {
        private readonly IUnitOfWork _dbLayer = diContainer.GetRequiredService<IUnitOfWork>();

        /// <summary>
        /// Создать платеж агрегатора ЮKassa
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="creationDate">Дата создания</param>
        /// <param name="paymentAmount">Сумма платежа</param>
        /// <returns>Id платежа агрегатора ЮKassa</returns>
        public Guid CreateYookassaPayment(Guid accountId, DateTime creationDate, decimal paymentAmount = 200,
            PaymentStatus status = PaymentStatus.Waiting)
        {
            var paymentDefinition = GenerateYookassaPaymentDefinition(accountId, paymentAmount, creationDate, status);
            var createPaymentResult = diContainer.GetRequiredService<CreatePaymentManager>().AddPayment(paymentDefinition);

            ClassicAssert.IsFalse(createPaymentResult.Error);
            ClassicAssert.IsTrue(createPaymentResult.Result == PaymentOperationResult.Ok);
            return _dbLayer.PaymentRepository.GetById(paymentDefinition.Id).Id;
        }

        /// <summary>
        /// Сформировать модель определения платежа
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="paymentAmount">Сумма платежа</param>
        /// <param name="creationDate">Дата создания</param>
        /// <returns>Модель определения платежа</returns>
        private static PaymentDefinitionDto GenerateYookassaPaymentDefinition(Guid accountId, decimal paymentAmount,
            DateTime creationDate, PaymentStatus status) => new()
        {
               Id = Guid.NewGuid(),
                Account = accountId,
                Date = creationDate,
                Description = "Тестовый платеж",
                System = PaymentSystem.Yookassa,
                Status = status,
                OperationType = PaymentType.Inflow,
                Total = paymentAmount,
                OriginDetails = PaymentSystem.Yookassa.ToString(),
                TransactionType = TransactionType.Money
            };
    }
}
