﻿using System;
using System.IO;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Scenarios.Tests.Libs.Contexts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания загруженных файлов для тестов
    /// </summary>
    public class CreateUploadedFileTestHelper(ITestContext testContext)
    {
        private readonly IUnitOfWork _dbLayer = testContext.ServiceProvider.GetRequiredService<IUnitOfWork>();
        private readonly AccountDatabasePathHelper _accountDatabasePathHelper = testContext.ServiceProvider.GetRequiredService<AccountDatabasePathHelper>();

        /// <summary>
        /// Название папки для создания zip файла
        /// </summary>
        private readonly string _testZipFolderName = "/TestZipFolder";

        /// <summary>
        /// Создать запись в бд про загруженный файл
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        public Guid CreateUploadedFileDbRecord(Guid accountId)
        {
            var uploadFileModel = new UploadedFile
            {
                AccountId = accountId,
                Id = Guid.NewGuid(),
                Comment = "Test comment",
                FileName = "Test filename",
                FileSizeInMegabytes = 10,
                FullFilePath = "FullFilePath",
                Status = UploadedFileStatus.UploadSuccess,
                UploadDateTime = DateTime.Now
            };

            _dbLayer.UploadedFileRepository.Insert(uploadFileModel);
            _dbLayer.Save();

            return uploadFileModel.Id;
        }

        /// <summary>
        /// Создать загруженный файл физически в хранилище
        /// </summary>
        /// <param name="uploadedFileId">Id загруженного файла</param>
        /// <param name="databaseId">Id инф. базы</param>
        public void CreateUploadedFilePhysicallyInDbStorage(Guid uploadedFileId, Guid databaseId)
        {
            var database = _dbLayer.DatabasesRepository.FirstOrDefault(db => db.Id == databaseId) ??
                           throw new NotFoundException($"Не удалось найти инф. базу по Id {databaseId}");

            var fileStorage =
                _dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(storage =>
                    storage.ID == database.FileStorageID) ??
                throw new NotFoundException($"Не удалось найти файловое хранилище по id {database.FileStorageID}");

            var dbPath =_accountDatabasePathHelper.GetPath(database, fileStorage);

            var fileName = $"backup_{databaseId}";

            var pathToZipFile = CreateZipFileInFileStorage(dbPath, fileName, fileStorage.ConnectionAddress);

            UpdateUploadedFileRecord(pathToZipFile, uploadedFileId);

        }

        /// <summary>
        /// Создать zip файл в хранилище
        /// </summary>
        /// <param name="pathToBeZipped">Путь к папке, которую нужно заархивировать</param>
        /// <param name="fileName">Название файла для создания</param>
        /// <param name="fileStoragePath">Путь к файловому хранилищу</param>
        /// <returns></returns>
        private string CreateZipFileInFileStorage(string pathToBeZipped, string fileName, string fileStoragePath)
        {
            var testZipDirectory = Directory.CreateDirectory(fileStoragePath + _testZipFolderName);

            var zipFilePath = Path.Combine(testZipDirectory.FullName, $"{fileName}.zip");

            ZipHelper.SafelyCreateZipFromDirectory(pathToBeZipped, zipFilePath);

            return zipFilePath;
        }

        /// <summary>
        /// Обновить запись о загруженном файле в БД
        /// </summary>
        /// <param name="newFullPath">Новый путь к загруженному файлу</param>
        /// <param name="uploadedFileId">Id загруженного файла</param>
        private void UpdateUploadedFileRecord(string newFullPath, Guid uploadedFileId)
        {
            var uploadedFile = _dbLayer.UploadedFileRepository.FirstOrDefault(uplFile => uplFile.Id == uploadedFileId);
            uploadedFile.FullFilePath = newFullPath;
            _dbLayer.UploadedFileRepository.Update(uploadedFile);
            _dbLayer.Save();
        }
    }
}
