﻿using System;
using System.Collections.Generic;
using Clouds42.Billing;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания аккаунта(для тестов)
    /// </summary>
    public class CreateAccountHelperTest(TestDataGenerator testDataGenerator, IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Создать аккаунт для тестовой среды
        /// </summary>
        /// <param name="refferalAccountId">Id аккаунта рефферала</param>
        /// <param name="name">Название аккаунта</param>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="accountLocaleId">Id локали</param>
        /// <returns>Аккаунт для тестовой среды</returns>
        public Account Create(Guid? refferalAccountId = null, string name = null, Guid? segmentId = null,
            Guid? accountLocaleId = null)
        {
            var account = testDataGenerator.GenerateAccount(refferalAccountId, name);
            var accountRequisites = testDataGenerator.GenerateAccountRequisites(account.Id);
            var myDiskPropertiesByAccount = testDataGenerator.GenerateMyDiskPropertiesByAccount(account.Id);
            var accountConfiguration =
                testDataGenerator.GenerateAccountConfiguration(account.Id, segmentId, accountLocaleId);

            dbLayer.AccountsRepository.Insert(account);
            dbLayer.GetGenericRepository<AccountConfiguration>().Insert(accountConfiguration);
            dbLayer.GetGenericRepository<MyDiskPropertiesByAccount>().Insert(myDiskPropertiesByAccount);
            dbLayer.GetGenericRepository<AccountRequisites>().Insert(accountRequisites);
            dbLayer.Save();
            account.AccountRequisites = accountRequisites;
            account.MyDiskPropertiesByAccount = myDiskPropertiesByAccount;
            account.AccountConfiguration = accountConfiguration;
            return account;
        }

        /// <summary>
        /// Создать аккаунты в определенном количестве
        /// </summary>
        /// <param name="count">Количество аккаунтов, которые необходимо создать</param>
        /// <param name="accountLocaleId">Id локали</param>
        /// <returns>Созданные аккаунты</returns>
        public List<Account> CreateByCount(int count, Guid? accountLocaleId = null)
        {
            var accounts = new List<Account>();

            for (var i = 0; i < count; i++)
            {
                accounts.Add(Create(accountLocaleId: accountLocaleId));
            }

            return accounts;
        }
    }
}