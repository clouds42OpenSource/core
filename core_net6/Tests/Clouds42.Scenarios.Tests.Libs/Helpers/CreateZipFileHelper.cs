﻿using System;
using System.IO;
using System.Xml;
using Clouds42.BLL.Common.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    ///     Хэлпер для создания zip архивов
    /// </summary>
    public class CreateZipFileHelper
    {
        /// <summary>
        ///     Создать пустой zip архив
        /// </summary>
        /// <param name="storagePath">путь, где будет создан архив</param>
        /// <param name="fileName">название архива (без расширения .zip)</param>
        public void CreateEmptyZipFile(string storagePath, string fileName)
        {
            var storage = new DirectoryInfo(storagePath);
            var startPath = storagePath + @"\" + fileName;

            storage.CreateSubdirectory(fileName);
            ZipHelper.SafelyCreateZipFromDirectory(startPath, storagePath + @"\" + fileName + ".zip");
        }

        /// <summary>
        ///     Создать zip архив с файлом DumpInfo.xml внутри
        /// </summary>
        /// <param name="filePath">путь, где будет создан архив</param>
        public void CreateZipFileWithDumpInfo(string filePath)
        {
            var directory = Path.GetDirectoryName(filePath) ??
                           throw new InvalidOperationException(
                               $"Не удалось получить путь для файла {filePath}");

            var subDirectoryName = Path.GetFileNameWithoutExtension(filePath);

            var storage = new DirectoryInfo(directory);
            var startPath = directory + @"\" + subDirectoryName;
            storage.CreateSubdirectory(subDirectoryName);

            var dumpInfo = new DumpInfoMetadataDto
            {
                V8 = "http://v8.1c.ru/8.1/data/core",
                Xs = "http://www.w3.org/2001/XMLSchema",
                Xmlns = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1",
                DumpInfo = new DataDumpInfoDto
                {
                    Created = "2019-02-26T05:21:49",
                    Xsi = "http://www.w3.org/2001/XMLSchema-instance",
                    Configuration = new DataDumpConfigurationDto
                    {
                        Name = "БухгалтерияПредприятия",
                        Presentation = "Бухгалтерия предприятия, редакция 3.0",
                        Vendor = "3.0.67.58",
                        Version = "Фирма \"1С\""
                    }
                }
            };

            var dumpStr = dumpInfo.SerializeToCleanXml();
            var dumpXml = new XmlDocument { InnerXml = dumpStr };
            dumpXml.Save(startPath + @"\DumpInfo.xml");

            ZipHelper.SafelyCreateZipFromDirectory(startPath, filePath);
        }

        /// <summary>
        ///     Удалить тестовый файл
        /// </summary>
        /// <param name="filePath">полный путь к файлу на удаление</param>
        public void DeleteTestFile(string filePath)
        {
            File.Delete(filePath);
        }
    }
}
