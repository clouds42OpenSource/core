﻿using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.Scenarios.Tests.Libs.Constants;
using Clouds42.Scenarios.Tests.Libs.Models;
using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Helpers
{
    /// <summary>
    /// Хелпер для создания шаблона инф. базы для тестов
    /// </summary>
    public class CreateDbTemplateTestHelper(
        DbTemplatesManager dbTemplatesManager,
        IUnitOfWork dbLayer,
        CreateConfigurationDbTemplateRelationsTestHelper createConfigurationDbTemplateRelationsHelper)
    {
        /// <summary>
        /// Название шаблона для создания
        /// Используется в GarbageClearHelper.DeleteObjectsFromDb
        /// </summary>
        private readonly string _templateTestName = DbTemplatesNameConstants.EmptyName;

        /// <summary>
        /// Создать шаблон для теста
        /// </summary>
        /// <returns>Модель созданного щаблона</returns>
        public DbTemplateItemDto CreateDbTemplate(DbTemplateItemDto dbTemplateItem = null)
        {
            var dbTemplateItemDto = dbTemplateItem ?? new DbTemplateModelTest(dbLayer);

            var dbTemplate = dbTemplatesManager.GetDbTemplateByName(dbTemplateItemDto.Name);

            if (!dbTemplate.Error && dbTemplate.Result != null)
                return dbTemplate.Result;
            dbTemplateItemDto.Configuration1CName = "Бухгалтерия предприятия, редакция 3.0";
            var addNewDbTemplate = dbTemplatesManager.AddDbTemplateItem(dbTemplateItemDto);

            if (addNewDbTemplate.Error)
                throw new InvalidOperationException($"Не удалось создать шаблон с названием {_templateTestName}");

            var createdDbTemplate = dbTemplatesManager.GetDbTemplateByName(dbTemplateItemDto.Name);

            if (createdDbTemplate.Error)
                throw new NotFoundException($"Не удалось найти созданный шаблон {_templateTestName}");

            createConfigurationDbTemplateRelationsHelper.CreateRelations();

            return createdDbTemplate.Result;
        }

        
        /// <summary>
        /// Получить имя шаблона для теста
        /// </summary>
        /// <param name="lineNumber">Номер записи в бд</param>
        /// <returns>Имя шаблона</returns>
        public string GetDbTemplateTestName(int lineNumber = 0)
        {
            var templates = dbLayer.DbTemplateRepository.All().ToList();
            if (templates.Count - 1 < lineNumber)
                return CreateDbTemplate().Name;

            var template = templates[lineNumber];

            return template == null
                ? CreateDbTemplate().Name
                : template.Name;
        }
    }
}
