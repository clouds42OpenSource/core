﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DomainContext.Context;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using System;
using Clouds42.BLL.Common.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Contexts
{
    public class TestContext(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IServiceProvider services,
        ILogger42 logger,
        IConfiguration configurations,
        Clouds42DbContext context,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : ITestContext
    {
        public IUnitOfWork DbLayer { get; } = dbLayer;
        public IServiceProvider ServiceProvider { get; } = services;
        public IAccessProvider AccessProvider { get; } = accessProvider;
        public ILogger42 Logger { get; } = logger;
        public IHandlerException HandlerException { get; } = handlerException;
        public Clouds42DbContext Context { get; } = context;
        public IConfiguration Configurations { get; } = configurations;
        public IAccessMapping AccessMapping { get; } = accessMapping;


        private string _testDataPath;
        public string TestDataPath
        {
            get
            {
                if (!string.IsNullOrEmpty(_testDataPath))
                    return _testDataPath;

                return _testDataPath = ExecutionTestContext.GetTestDataPath();
            }
        }

    }
}
