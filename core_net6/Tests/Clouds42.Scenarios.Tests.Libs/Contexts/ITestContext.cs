﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DomainContext.Context;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using System;
using Clouds42.BLL.Common.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Scenarios.Tests.Libs.Contexts
{
    public interface ITestContext
    {
        IServiceProvider ServiceProvider { get; }
        IUnitOfWork DbLayer { get; }
        Clouds42DbContext Context { get; }
        IConfiguration Configurations { get; }
        IAccessProvider AccessProvider { get; }
        ILogger42 Logger { get; }
        IHandlerException HandlerException{ get; }

        IAccessMapping AccessMapping { get; }
        string TestDataPath { get; }
    }
}
