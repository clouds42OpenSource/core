﻿using System.IO;
using Clouds42.Common.Exceptions;

namespace Clouds42.Scenarios.Tests.Libs.Contexts
{
    /// <summary>
    /// Контекст выполнения тестов
    /// </summary>
    public static class ExecutionTestContext
    {
        /// <summary>
        /// Получить путь к директории для тестовых данных
        /// </summary>
        /// <returns>Путь к директории для тестовых данных</returns>
        public static string GetTestDataPath()
        {
            var path = Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (path == null)
                throw new NotFoundException("Среда тестирования не сконфигурирована");

            return Path.Combine(path, "TestData");
        }
    }
}
