﻿namespace Clouds42.Scenarios.Tests.Libs.Constants
{
    /// <summary>
    /// Константы заголовков шаблонов информационных баз
    /// </summary>
    public static class DbTemplateCaptionConstants
    {
        /// <summary>
        /// Шаблон Комплексная автоматизация
        /// </summary>
        public static string KaCaption => "Комплексная автоматизация 1.1";

        /// <summary>
        /// Шаблон Бухгалтерия предприятия
        /// </summary>
        public static string BpCaption => "Бухгалтерия предприятия 3.0";

        /// <summary>
        /// Шаблон Управление торговлей
        /// </summary>
        public static string UtCaption => "Управление торговлей 11";

        /// <summary>
        /// Шаблон Управление нашей фирмой
        /// </summary>
        public static string UnfCaption => "Управление нашей фирмой 1.6";

        /// <summary>
        /// Шаблон Розница Проф
        /// </summary>
        public static string RoznCaption => "Розница Проф";

        /// <summary>
        /// Шаблон Зарплата и Управление Персоналом
        /// </summary>
        public static string ZupCaption => "Зарплата и Управление Персоналом 3.0";

    }
}
