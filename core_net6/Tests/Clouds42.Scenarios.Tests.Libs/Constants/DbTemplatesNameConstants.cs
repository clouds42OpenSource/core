﻿namespace Clouds42.Scenarios.Tests.Libs.Constants
{
    /// <summary>
    /// Константы названий шаблонов информационных баз
    /// </summary>
    public static class DbTemplatesNameConstants
    {
        /// <summary>
        /// Шаблон Бухгалтерия предприятия
        /// </summary>
        public const string BpName = "db_1c_bp30_shablon";

        /// <summary>
        /// Шаблон Комплексная автоматизация
        /// </summary>
        public const string KaName = "db_1c_ka22_shablon";

        /// <summary>
        /// Шаблон Зарплата и Управление персоналом
        /// </summary>
        public const string ZupName = "db_1c_zup30_shablon";

        /// <summary>
        /// Шаблон Управление нашей фирмой
        /// </summary>
        public const string UnfName = "db_1c_unf16_shablon";

        /// <summary>
        /// Шаблон Управление торговлей
        /// </summary>
        public const string UtName = "db_1c_ut11_shablon";

        /// <summary>
        /// Шаблон Чистый шаблон
        /// </summary>
        public const string EmptyName = "db_1c_my_shablon";
    }
}
