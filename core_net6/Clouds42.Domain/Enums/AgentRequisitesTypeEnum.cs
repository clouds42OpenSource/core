﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип реквизитов агента
    /// </summary>
    public enum AgentRequisitesTypeEnum
    {
        /// <summary>
        /// Реквизиты физ. лица
        /// </summary>
        [Description("Реквизиты физ. лица")]
        PhysicalPersonRequisites = 1,

        /// <summary>
        /// Реквизиты юр. лица
        /// </summary>
        [Description("Реквизиты юр. лица")]
        LegalPersonRequisites = 2,

        /// <summary>
        /// Реквизиты ИП
        /// </summary>
        [Description("Реквизиты (ИП)")]
        SoleProprietorRequisites = 3,
    }
}
