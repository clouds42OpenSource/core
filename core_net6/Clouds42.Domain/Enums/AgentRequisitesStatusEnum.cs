﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус реквизитов агента
    /// </summary>
    public enum AgentRequisitesStatusEnum
    {
        /// <summary>
        /// Черновик
        /// </summary>
        [Description("Черновик")]
        Draft = 1,

        /// <summary>
        /// На проверке
        /// </summary>
        [Description("На проверке")]
        OnCheck = 2,

        /// <summary>
        /// Проверено
        /// </summary>
        [Description("Проверено")]
        Verified = 3,
    }
}
