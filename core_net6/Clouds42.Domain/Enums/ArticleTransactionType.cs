﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип транзакции
    /// </summary>
    public enum ArticleTransactionType
    {
        [Description("не задан")]
        Undefined = 0,

        /// <summary>
        /// Начисление ДС
        /// </summary>
        [Description("начисление ДС")]
        Inflow = 1,

        /// <summary>
        /// Вывод ДС
        /// </summary>
        [Description("вывод ДС")]
        Outflow = 2,
    }
}
