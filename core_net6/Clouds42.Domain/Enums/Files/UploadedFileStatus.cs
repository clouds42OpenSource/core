﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Files
{
    /// <summary>
    /// Статус загруженного файла
    /// </summary>
    public enum UploadedFileStatus
    {
        /// <summary>
        /// Файл в процессе загрузки
        /// </summary>
        [Description("Файл в процессе загрузки")]
        UploadingInProcess = 0,

        /// <summary>
        /// Ошибка загрузки
        /// </summary>
        [Description("Ошибка загрузки")]
        UploadFailed = 1,

        /// <summary>
        /// Файл загружен успешно
        /// </summary>
        [Description("Файл загружен успешно")]
        UploadSuccess = 2,

        /// <summary>
        /// Загруженный файл удален
        /// </summary>
        [Description("Загруженный файл удален")]
        Deleted = 3
    }
}
