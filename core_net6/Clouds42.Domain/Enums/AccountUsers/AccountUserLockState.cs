﻿namespace Clouds42.Domain.Enums.AccountUsers
{
    /// <summary>
    /// Состояние блокировки пользователя
    /// </summary>
    public enum AccountUserLockState
    {
        /// <summary>
        /// Операция завершена
        /// </summary>
        Done = 0,

        /// <summary>
        /// В процессе блокировки
        /// </summary>
        ProcessingLock = 1,

        /// <summary>
        /// В процессе разблокировки
        /// </summary>
        ProcessingUnlock = 2,

        /// <summary>
        /// Ошибка
        /// </summary>
        Error = 3,

        /// <summary>
        /// Неизвестное состояние
        /// </summary>
        Undefined = 4
    }
}
