﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип активации Аренды 1С
    /// </summary>
    public enum Rent1CActivationType
    {
        /// <summary>
        /// Для нового аккаунта
        /// </summary>
        ForNewAccount,

        /// <summary>
        /// Для уже существующего аккаунта
        /// </summary>
        ForExistingAccount
    }
}
