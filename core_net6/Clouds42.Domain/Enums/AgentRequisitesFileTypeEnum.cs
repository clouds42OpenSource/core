﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип файла реквизитов агента
    /// </summary>
    public enum AgentRequisitesFileTypeEnum
    {
        /// <summary>
        /// Скан копия подписанного договора
        /// </summary>
        [Description("Скан копия договора")]
        ScanСopyСontract = 1,

        /// <summary>
        /// Скан копия ИНН
        /// </summary>
        [Description("Скан копия ИНН")]
        ScanСopyInn = 2,

        /// <summary>
        /// Скан копия СНИЛС
        /// </summary>
        [Description("Скан копия СНИЛС")]
        ScanСopySnils = 3,

        /// <summary>
        /// Скан копия паспорта и прописки
        /// </summary>
        [Description("Скан копия паспорта и прописки")]
        ScanCopyPassportAndRegistration = 4,

        /// <summary>
        /// Скан копия свидетельства о регистрации юридического лица
        /// </summary>
        [Description("Скан копия свидетельства о регистрации юр. лица")]
        ScanCopyCertificateRegistrationLegalEntity = 5,

        /// <summary>
        /// Скан копия свидетельства о регистрации ИП
        /// </summary>
        [Description("Скан копия свидетельства о регистрации ИП")]
        ScanCopyCertificateRegistrationSoleProprietor = 6
    }
}
