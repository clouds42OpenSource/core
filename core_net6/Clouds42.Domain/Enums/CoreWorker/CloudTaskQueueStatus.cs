﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.CoreWorker
{
    /// <summary>
    /// Статус задачи воркера
    /// </summary>
    public enum CloudTaskQueueStatus
    {
        /// <summary>
        /// Новая
        /// </summary>
        [Description("Новая")]
        New,

        /// <summary>
        /// Захваченна воркером
        /// </summary>
        [Description("Захваченна воркером")]
        Captured,

        /// <summary>
        /// В процессе выполнения
        /// </summary>
        [Description("В процессе выполнения")]
        Processing,

        /// <summary>
        /// Готова
        /// </summary>
        [Description("Готова")]
        Ready,

        /// <summary>
        /// Ошибка
        /// </summary>
        [Description("Ошибка")]
        Error,

        /// <summary>
        /// Будет перезапущена
        /// </summary>
        [Description("Будет перезапущена")]
        NeedRetry
    }
}
