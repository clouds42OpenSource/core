﻿namespace Clouds42.Domain.Enums.CoreWorker
{
    /// <summary>
    /// Тип задачи воркера
    /// </summary>
    public enum WorkerTaskTypeEnum
    {
        /// <summary>
        /// Внутренняя задача
        /// </summary>
        InternalTask = 0,

        /// <summary>
        /// Регламентная задача
        /// </summary>
        RegulationsTask = 1
    }
}
