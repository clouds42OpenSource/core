﻿namespace Clouds42.Domain.Enums.CoreWorker 
{
    /// <summary>
    /// Тип задачи воркера
    /// </summary>
    public enum CoreWorkerTaskType
    {
        /// <summary>
        /// Задача для регулярной пролонгации сервисов
        /// </summary>
        Regular42CloudServiceProlongationJob,
        /// <summary>
        /// Задача по обновлению шаблонов инф. баз
        /// </summary>
        TemplatesUpdater,

        /// <summary>
        /// Задача для обновления CFU пакетов
        /// </summary>
        CfuUpdater,
        /// <summary>
        ///  Задача для выполнения регламентных операций с инф. базой
        /// </summary>
        DatabaseRegularOperations,
        /// <summary>
        /// Задача по архивации и удалению в склеп ИБ неактивных аккаунтов 
        /// </summary>
        ArchivDatabase,
        /// <summary>
        /// Задача по пересчету занимаемого дискового пространства
        /// </summary>
        MyDiskRegularOperations,
        /// <summary>
        /// Задача по установке прав на инф. базу МЦОБ
        /// </summary>
        McobEditUserRolesOfDatabase,
        /// <summary>
        /// Задача для авторизации в инф. базу
        /// </summary>
        SupportAuthVerification,
        /// <summary>
        /// Задача для смены сегмента аккаунта
        /// </summary>
        SegmentMigrationJob,
        /// <summary>
        /// Задача для смены хранилища инф. базы
        /// </summary>
        AccountDatabaseMigrationJob,
        /// <summary>
        /// Задача по удалению директории
        /// </summary>
        RemoveDirectoryJob,
        /// <summary>
        /// Задача по удалению файла
        /// </summary>
        RemoveFileJob,
        /// <summary>
        /// Задача по удалению загруженного файла
        /// </summary>
        RemoveUploadFileJob,
        /// <summary>
        /// Задача длоя восстановления инф. базы из склепа
        /// </summary>
        RestoreAccountDatabaseFromTombJob,
        /// <summary>
        /// Задача для завершения сессий инф. базы на сервере предприятия
        /// </summary>
        DropSessionsFromClusterJob,
        /// <summary>
        /// Задача для удаления базы с сервера предприятия
        /// </summary>
        DropDatabaseFromClusterJob,
        /// <summary>
        /// Задача для публикации инф. базы
        /// </summary>
        PublishDatabaseJob,
        /// <summary>
        /// Задача для отмены публикации базы
        /// </summary>
        CancelPublishDatabaseJob,
        /// <summary>
        /// Задача для перепубликации инф. базы
        /// </summary>
        RepublishDatabaseJob,
        /// <summary>
        /// Задача перезапуска пула на нодах сервера публицкации
        /// </summary>
        RestartAccountApplicationPoolOnIisJob,
        /// <summary>
        /// Задача перепубликации баз сегмента
        /// </summary>
        RepublishSegmentDatabasesJob,
        /// <summary>
        /// Задача для перепубликации базы с изменением web.config
        /// </summary>
        RepublishWithChangingConfigJob,
        /// <summary>
        /// Задача для создания инф. базы на сервере предприятия
        /// </summary>
        CreateDatabaseClusterJob,
        /// <summary>
        /// Задача для удаления информационной базы в склеп
        /// </summary>
        SendBackupAccountDatabaseToTombJob,
        /// <summary>
        /// Задача для выдачи веб доступа к инф. базе
        /// </summary>
        AddWebAccessToDatabaseJob,
        /// <summary>
        /// Задача для удаления веб доступа к инф. базе
        /// </summary>
        RemoveWebAccessToDatabaseJob,
        /// <summary>
        /// Задача по загрузке файла бэкапа
        /// </summary>
        BackupFileFetchingJob,
        /// <summary>
        /// Задача по склейке частей загруженного файла 
        /// </summary>
        MergeChunksToInitialFileJob,
        /// <summary>
        /// Задача по управлению доступом к инф. базе
        /// </summary>
        ManageAcDbAccessForAccountUserJob,
        /// <summary>
        /// Задача по удалению данных неактивных аккаунтов
        /// </summary>
        DeleteInactiveAccountsDataJob,
        /// <summary>
        /// Задача по удалению инф. базы
        /// </summary>
        DeleteAccountDatabaseJob,
        /// <summary>
        /// Задача по восстановлению инф. базы после не успешной попытки АО
        /// </summary>
        RestoreAccountDatabaseAfterFailedAutoUpdateJob,
        /// <summary>
        /// Задача по пересчету стоимости сервиса после изменений
        /// </summary>
        RecalculateServiceCostAfterChangesJob,
        /// <summary>
        /// Задача по уведомлению аккаунтов
        /// о скором изменении стоимости сервиса
        /// </summary>
        NotifyBeforeChangeServiceCostJob,
        /// <summary>
        /// Задача по созданию фискального чека
        /// </summary>
        CreateInvoiceFiscalReceiptJob,
        /// <summary>
        /// Задача для обновления конфигурации инф. базы
        /// </summary>
        AccountDatabaseAutoUpdateJob,
        /// <summary>
        /// Задача для проверки целостности ресурсов аккаунта
        /// </summary>
        AuditAccountResourceIntegralityJob,
        /// <summary>
        /// Задача для управления моделью восстановления для инф. баз
        /// </summary>
        ManageAccountDatabaseRestoreModelJob,
        /// <summary>
        /// Задача обновления логина
        /// веб доступа базы
        /// </summary>
        UpdateLoginForWebAccessDatabaseJob,
        /// <summary>
        /// Задача для загрузки файла инф. базы через веб сервис
        /// </summary>
        UploadAcDbZipFileThroughWebServiceJob,
        /// <summary>
        /// Создание директории
        /// </summary>
        CreateDirectoryJob,
        InstallExtension,

        /// <summary>
        /// обработать запрос на смену режима базы
        /// </summary>
        HandleRequestToChangeAccDbTypeJob,
        
        /// <summary>
        /// Задача по трансформации файловой базы в серверную
        /// </summary>
        TransformFileBaseToServerJob,
    }
}
