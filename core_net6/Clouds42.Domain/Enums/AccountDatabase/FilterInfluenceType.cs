﻿
namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary> Перечисление для указания к каким таблицам применять фильтр </summary>
    public enum FilterInfluenceType : short
    {
        /// <summary> Применение фильтра на все таблицы </summary>
        All = 0,
        /// <summary> Применение фильтра на Databases </summary>
        My = 1,
        /// <summary> Применение фильтра на SharedDatabases </summary>
        Shared = 2
    }
}
