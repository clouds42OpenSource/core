﻿
namespace Clouds42.Domain.Enums.AccountDatabase
{
    public enum AutoUpdateTimeEnum
    {
        day = 14,
        evening = 18,
        night = 21
    }
}
