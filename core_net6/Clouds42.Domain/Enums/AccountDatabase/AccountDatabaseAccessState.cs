﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Состояние доступа к инф. базе
    /// </summary>
    public enum AccountDatabaseAccessState
    {
        /// <summary>
        /// Выдан
        /// </summary>
        Done = 0,

        /// <summary>
        /// В процессе выдачи
        /// </summary>
        ProcessingGrant = 1,

        /// <summary>
        /// В процессе удаления
        /// </summary>
        ProcessingDelete = 2,

        /// <summary>
        /// Ошибка
        /// </summary>
        Error = 3,

        /// <summary>
        /// Неизвестное состояние
        /// </summary>
        Undefined = 4
    }
}