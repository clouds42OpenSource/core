﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Модель восстановления инф. базы
    /// </summary>
    public enum AccountDatabaseRestoreModelTypeEnum
    {
        /// <summary>
        /// Простая
        /// </summary>
        [Description("Простая")]
        Simple,

        /// <summary>
        /// Полная
        /// </summary>
        [Description("Полная")]
        Full,

        /// <summary>
        /// Смешанная
        /// </summary>
        [Description("Смешанная")]
        Mixed
    }
}
