﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    ///     Тип восстановления информационной базы из бэкапа
    /// </summary>
    public enum AccountDatabaseBackupRestoreType
    {
        /// <summary>
        ///     Восстановить с заменой в текущую
        /// </summary>
        ToCurrent = 0,

        /// <summary>
        ///     Восстановить в новую
        /// </summary>
        ToNew = 1
    }
}