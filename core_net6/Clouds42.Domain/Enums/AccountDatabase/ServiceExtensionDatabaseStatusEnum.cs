﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Статус расширения сервиса для информационной базы
    /// </summary>
    public enum ServiceExtensionDatabaseStatusEnum
    {
        /// <summary>
        /// Расширение установленно
        /// </summary>
        DoneInstall = 0,

        /// <summary>
        /// Расширение удалено
        /// </summary>
        DoneDelete = 1,

        /// <summary>
        /// Расширение в процессе установки
        /// </summary>
        ProcessingInstall = 2,

        /// <summary>
        /// Расширение в процессе удаления
        /// </summary>
        ProcessingDelete = 3,

        /// <summary>
        /// Ошибка в процессе установки
        /// </summary>
        ErrorProcessingInstall = 4,

        /// <summary>
        /// Ошибка в процессе удаления
        /// </summary>
        ErrorProcessingDelete = 5,

        /// <summary>
        /// Сервис никогда не установлен
        /// </summary>
        NotInstalled = 6
    }
}
