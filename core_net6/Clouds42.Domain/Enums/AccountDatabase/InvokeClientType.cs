﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    ///     Тип вызываемого клиета
    /// </summary>
    public enum InvokeClientType
	{
		/// <summary>
		/// Дефолтный
		/// </summary>
		Default = 0,

		/// <summary>
		/// Вызов клиента Sauri
		/// </summary>
		SauriInvoke = 1
    }
}