﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Статус завершения сеансов в инф. базе
    /// </summary>
    public enum TerminateSessionsInDbStatusEnum
    {
        /// <summary>
        /// Успешное завершение сеансов в инф. базе
        /// </summary>
        [Description("Успешное завершение сеансов в инф. базе")]
        Success = 0,

        /// <summary>
        /// Завершение сеансов в инф. базе выполнилось с ошибкой
        /// </summary>
        [Description("Завершение сеансов выполнилось с ошибкой")]
        Error = 1,

        /// <summary>
        /// В процессе завершения сеансов
        /// </summary>
        [Description("В процессе завершения сеансов")]
        InProcess = 2
    }
}
