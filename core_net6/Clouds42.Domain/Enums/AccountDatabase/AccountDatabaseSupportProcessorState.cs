﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Состояние процессора поддерджки инф. базы
    /// </summary>
    public enum AccountDatabaseSupportProcessorState
    {
        /// <summary>
        /// Новый процессор, ожидает команды Process
        /// </summary>
        [Description("Новый процессор, ожидает команды Process.")]
        New,

        /// <summary>
        /// В процессе провдения операции
        /// </summary>
        [Description("В процессе провдения операции.")]
        Processing,

        /// <summary>
        /// Не фатальная ошибка операции
        /// </summary>
        [Description("Не фатальная ошибка операции.")]
        Error,

        /// <summary>
        /// Фатальная ошибка проведения операции
        /// </summary>
        [Description("Фатальная ошибка проведения операции.")]
        FatalError,

        /// <summary>
        /// Фатальная ошибка валидации
        /// </summary>
        [Description("Фатальная ошибка валидации.")]
        FatalValidationError,

        /// <summary>
        /// База содержит программные доработки
        /// </summary>
        [Description("База содержит программные доработки.")]
        DbHasModifications,

        /// <summary>
        /// Выполнено обновление
        /// </summary>
        [Description("Выполнено обновление.")]
        Updated,

        /// <summary>
        /// Операция проведена успешно
        /// </summary>
        [Description("Операция проведена успешно.")]
        Success,

        /// <summary>
        /// Процессор пропустил обработку информационной базы
        /// </summary>
        [Description("Процессор пропустил обработку информационной базы.")]
        Skip,

        /// <summary>
        /// В базе есть активные сессии
        /// </summary>
        [Description("В базе есть активные сессии")]
        HasActiveSessions
    }
}
