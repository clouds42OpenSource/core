﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    ///     Источник хранилища бэкапа
    /// </summary>
    public enum AccountDatabaseBackupSourceType
    {
        /// <summary>
        ///     На хранилке
        /// </summary>
        Local = 0,

        /// <summary>
        ///     На Google cloud
        /// </summary>
        GoogleCloud = 1
    }
}
