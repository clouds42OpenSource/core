﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    ///     Состояния таймера для загрузки информационной базы
    /// </summary>
    public enum AccountDatabaseCreationTimerType
    {
        /// <summary>
        ///     Зеленое состояние таймера.
        ///     Загрузка проходит без каких либо задержек
        /// </summary>
        Green = 0,

        /// <summary>
        ///     Оранжевое состояние таймера.
        ///     Загрузка идет больше рассчитанного времени.
        /// </summary>
        Orange = 1,

        /// <summary>
        ///     Красное состояние таймера.
        ///     Загрузка прервана, необходимо связаться со специалистами.
        /// </summary>
        Red = 2
    }
}