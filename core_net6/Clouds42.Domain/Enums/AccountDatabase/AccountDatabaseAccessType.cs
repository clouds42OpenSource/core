﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Типы доступов к информационной базе
    /// </summary>
    public enum AccountDatabaseAccessType
    {
        /// <summary>
        /// Внутренний доступ
        /// </summary>
        [Description("Внутренний доступ")]
        InternalAccess,

        /// <summary>
        /// Внешний доступ
        /// </summary>
        [Description("Внешний доступ")]
        ExternalAccess
    }
}
