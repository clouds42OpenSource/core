﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    ///     Триггер удаления в склеп информационной базы
    /// </summary>
    public enum CreateBackupAccountDatabaseTrigger
    {
        /// <summary>
        ///     Ручное удаление базы
        /// </summary>
        [Description("Ручное удаление")]
        ManualRemoval = 0,

        /// <summary>
        ///     Просроченная аренда
        /// </summary>
        [Description("Архивация данных неактивного аккаунта")]
        OverdueRent = 1,

        /// <summary>
        ///     CoreApi
        /// </summary>
        [Description("API")]
        FromApi = 2,

        /// <summary>
        ///     Миграция информационной базы
        /// </summary>
        [Description("Миграция информационной базы")]
        MigrateAccountDatabase = 3,

        /// <summary>
        ///     Автообновление информационной базы
        /// </summary>
        [Description("Автообновление информационной базы")]
        AutoUpdateAccountDatabase = 4,

        /// <summary>
        ///     ТиИ информационной базы
        /// </summary>
        [Description("ТиИ информационной базы")]
        TehSupportAccountDatabase = 5,

        /// <summary>
        ///     Регламентное бэкопирование
        /// </summary>
        [Description("Регламентное резервное копирование")]
        RegulationBackup = 6,

        /// <summary>
        ///     Перемещение в архив
        /// </summary>
        [Description("Создание архивной копии")]
        MovingDbToArchive = 7,


        /// <summary>
        ///     Ручное удаление базы
        /// </summary>
        [Description("Ручное архивирование")]
        ManualArchive = 8,

        /// <summary>
        ///     Технический бекап
        /// </summary>
        [Description("Технический бекап")]
        TechnicalBackup = 9,
    }
}
