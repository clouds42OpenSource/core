﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Тип расширений сервисов для инф. базы
    /// </summary>
    public enum ServiceExtensionsForDatabaseTypeEnum
    {
        /// <summary>
        /// Все сервисы
        /// </summary>
        [Description("Все сервисы")]
        All = 0,

        /// <summary>
        /// Установленные
        /// </summary>
        [Description("Подключенные")]
        Installed = 1,

        /// <summary>
        /// Удаленные
        /// </summary>
        [Description("Отключенные")]
        Deleted = 2,

        /// <summary>
        /// Доступные
        /// </summary>
        [Description("Доступные")]
        Available = 3
    }
}
