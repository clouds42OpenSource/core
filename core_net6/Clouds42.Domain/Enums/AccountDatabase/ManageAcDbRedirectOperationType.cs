﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Тип операции для управления редиректом инф. базы
    /// </summary>
    public enum ManageAcDbRedirectOperationType
    {
        /// <summary>
        /// Установить редирект
        /// </summary>
        [Description("Установка редиректа")]
        Install,

        /// <summary>
        /// Удалить редирект
        /// </summary>
        [Description("Удаление редиректа")]
        Remove
    }
}
