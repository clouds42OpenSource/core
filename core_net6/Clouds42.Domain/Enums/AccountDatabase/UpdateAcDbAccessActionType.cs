﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Тип действия по обновлению доступа к инф. базе
    /// </summary>
    public enum UpdateAcDbAccessActionType
    {
        /// <summary>
        /// Выдача
        /// </summary>
        Grant = 0,

        /// <summary>
        ///  Удаление
        /// </summary>
        Delete = 1
    }
}
