﻿namespace Clouds42.Domain.Enums.AccountDatabase
{
    /// <summary>
    /// Тип принадлежности базы
    /// </summary>
    public enum AccountDatabaseAffiliationTypeEnum
    {
        /// <summary>
        /// Мои информационные базы
        /// </summary>
        My = 0,

        /// <summary>
        /// Доступные информационные
        /// базы других аккаунтов
        /// </summary>
        Shared = 1
    }

    /// <summary>
    /// Енамка для фильтров иб во вкладке управление аккаунтом -> информационные базы
    /// </summary>
    public enum AccountDatabaseTypeEnum
    {
        /// <summary>
        /// Все базы
        /// </summary>
        All = 0,

        /// <summary>
        /// Только Серверные
        /// </summary>
        ServerDatabase = 1,

        /// <summary>
        /// Только в Архиве
        /// </summary>
        ArchievedDatabase = 2,

        /// <summary>
        /// Только файловые
        /// </summary>
        FileDatabases = 3,

        /// <summary>
        /// Только на разделителях
        /// </summary>
        DelimeterDatabases = 4,

        /// <summary>
        /// Только удаленные
        /// </summary>
        DeletedDatabases = 5,
    }
}
