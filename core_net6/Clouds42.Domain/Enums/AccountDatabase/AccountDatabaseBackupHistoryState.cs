﻿namespace Clouds42.Domain.Enums.AccountDatabase
{

    /// <summary>
    /// Состояние бэкапа.
    /// </summary>
    public enum AccountDatabaseBackupHistoryState
    {

        /// <summary>
        /// Бэкап создан.
        /// </summary>
        Done = 0,

        /// <summary>
        /// Ошибка создания бэкапа.
        /// </summary>
        Error = 1,

        /// <summary>
        /// Пропущено создание бэкапа.
        /// </summary>
        Skip = 2
    }
}
