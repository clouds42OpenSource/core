﻿using System;
using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус платежа
    /// </summary>
    [Serializable]
    public enum PaymentStatus
    {
        /// <summary>
        /// Успешно
        /// </summary>
        [Description("Успешно")]
        Done,

        /// <summary>
        /// Отменен
        /// </summary>
        [Description("Отменен")]
        Canceled,

        /// <summary>
        /// Ожидает
        /// </summary>
        [Description("Ожидает")]
        Waiting
    }
}