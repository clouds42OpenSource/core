﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип предоставления услуги
    /// </summary>
    public enum KindOfServiceProvisionEnum
    {
        /// <summary>
        /// Разовый
        /// </summary>
        Single = 0,

        /// <summary>
        /// По периоду
        /// </summary>
        Periodic = 1
    }
}