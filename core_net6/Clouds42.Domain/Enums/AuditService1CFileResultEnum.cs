﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Результат аудита файла сервиса 1С
    /// </summary>
    public enum AuditService1CFileResultEnum
    {
        /// <summary>
        /// Ожидает аудита
        /// </summary>
        [Description("Ожидает аудита")]
        PendingAudit = 0,

        /// <summary>
        /// Аудит пройден
        /// </summary>
        [Description("Аудит пройден")]
        AuditPassed = 1,

        /// <summary>
        /// Отклонен
        /// </summary>
        [Description("Отклонена")]
        Rejected = 2,

        /// <summary>
        /// Опубликован
        /// </summary>
        [Description("Опубликована")]
        Published = 3
    }
}
