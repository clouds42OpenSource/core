﻿namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// 
    /// </summary>
    public enum SupportState
    {
        
        /// <summary>
        /// Отсутствует авторизация.
        /// </summary>
        NotAutorized = 0,

        /// <summary>
        /// Идет процесс авторизации.
        /// </summary>
        AutorizationProcessing = 1,

        /// <summary>
        /// Авторизация успешно завершена.
        /// </summary>
        AutorizationSuccess = 2,

        /// <summary>
        /// Не верные авторизационные данные.
        /// </summary>
        NotValidAuthData = 3,

        /// <summary>
        /// Ошибка подключения.
        /// </summary>
        AutorizationFail = 4
    }

}