﻿namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// Результат выполнения команды RAS
    /// </summary>
    public enum RasExecutionResultState
    {
        /// <summary>
        /// Успешный результат выполнения
        /// </summary>
        Success,

        /// <summary>
        /// Не верные авторизационные данные
        /// </summary>
        AuthenticationFailed,

        /// <summary>
        /// Ras.exe не существует в папке с платформой
        /// </summary>
        RasNotExistInPlatformPath,

        /// <summary>
        /// Ошибка подключения
        /// </summary>
        ConnectionError,

        /// <summary>
        /// Ошибка выполнения команды
        /// </summary>
        ExecutionError
    }
}
