﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// 
    /// </summary>
    public enum DatabaseSupportOperation
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Автообновление")] AutoUpdate = 0,

        /// <summary>
        /// 
        /// </summary>
        [Description("ТиИ")] TechSupport = 1,

        /// <summary>
        /// 
        /// </summary>
        [Description("Планирование АО")] PlanAutoUpdate = 2,

        /// <summary>
        /// 
        /// </summary>
        [Description("Планирование ТиИ")] PlanTehSupport = 3,

        /// <summary>
        /// 
        /// </summary>
        [Description("Авторизация к ИБ")] AuthToDb = 4,

        /// <summary>
        /// 
        /// </summary>
        [Description("Заявка на автообновление")] AutoUpdateRequest = 5,

        /// <summary>
        /// 
        /// </summary>
        [Description("Отмена авторизации в ИБ")] DisableAuthToDb = 6,
        
        
        [Description("Пометка что пользователь уведомлен")]
         UserNotified = 7,
    }
}
