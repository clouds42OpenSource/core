﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// Результат выполнения технической поддержки инф. базы
    /// </summary>
    public enum SupportHistoryState
    {
        /// <summary>
        /// Ошибка
        /// </summary>
        [Description("Ошибка")]
        Error = 0,

        /// <summary>
        /// Успешное выполнение
        /// </summary>
        [Description("Успех")]
        Success = 1
    }
}