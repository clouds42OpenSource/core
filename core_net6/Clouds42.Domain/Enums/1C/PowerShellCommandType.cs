﻿namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// Тип PowerShell команды
    /// </summary>
    public enum PowerShellCommandType
    {
        /// <summary>
        /// Сменить рабочую директорию
        /// </summary>
        ChangeWorkingDirectory,

        /// <summary>
        /// Запустить приложение ras.exe
        /// </summary>
        RunRasApplication,

        /// <summary>
        /// Получить список кластеров
        /// </summary>
        GetClusterList,

        /// <summary>
        /// Получить список инф. баз
        /// </summary>
        GetAccountDatabasesList,

        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        GetAccountDatabaseSessions,

        /// <summary>
        /// Закрыть сессию инф. баз
        /// </summary>
        DropAccountDatabaseSession,

        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        DeleteAccountDatabase,

        /// <summary>
        /// Создать инф. базу
        /// </summary>
        CreateAccountDatabase
    }
}
