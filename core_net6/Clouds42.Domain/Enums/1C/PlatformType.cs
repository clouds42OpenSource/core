﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Attributes;

namespace Clouds42.Domain.Enums._1C;

/// <summary>
/// Тип платформы
/// </summary>
public enum PlatformType
{
    /// <summary>
    /// Не определено
    /// </summary>
    [Display(Name = "")] [Description("Не определено")] [NotDrawInHtml]
    Undefined = 0,

    /// <summary>
    /// Платформа 8.2
    /// </summary>
    [Display(Name = "8.2")] [Description("Платформа 8.2")]
    V82 = 1,

    /// <summary>
    /// Платформа 8.3
    /// </summary>
    [Display(Name = "8.3")] [Description("Платформа 8.3")]
    V83 = 2
}

public static class PlatformTypeExtensions
{
    public static PlatformType ToPlatformTypeEnum(this string platformType)
    {
        var trimmValue = platformType?.Trim();
        if (string.IsNullOrEmpty(trimmValue))
            return PlatformType.Undefined;

        return trimmValue == "8.3" || trimmValue == "83" || trimmValue == "V83" ? PlatformType.V83 : PlatformType.V82;
    }
}