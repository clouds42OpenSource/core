﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums._1C
{
    /// <summary>
    /// 
    /// </summary>
    public enum TestingParamsValue
    {
        /// <summary>
        /// реиндексация таблиц
        /// </summary>
        [Description("-ReIndex")]
        ReIndex,

        /// <summary>
        /// сжатие таблиц
        /// </summary>
        [Description("-IBCompression")]
        IBCompression,

        /// <summary>
        /// реструктуризация таблиц информационной базы
        /// </summary>
        [Description("-Rebuild")]
        Rebuild,

        /// <summary>
        /// пересчет итогов
        /// </summary>
        [Description("-RecalcTotals")]
        RecalcTotals
    }
}