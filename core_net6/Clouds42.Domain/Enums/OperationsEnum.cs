﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Операции для измерения времени выполнения
    /// </summary>
    public enum ExecutionTimeOperationsEnum
    {
        /// <summary>
        /// Создание пула приложения
        /// </summary>
        [Description("Создание пула приложения")]
        CreatePool = 0,

        /// <summary>
        /// Создание Web-config файла
        /// </summary>
        [Description("Создание Web-config файла")]
        CreateWebConfig = 1,

        /// <summary>
        /// Создание VRD файла
        /// </summary>
        [Description("Создание VRD файла")]
        CreateVrdFile = 2,

        /// <summary>
        /// Редактирование Web-config файла
        /// </summary>
        [Description("Редактирование Web-config файла")]
        EditWebConfig = 3,

        /// <summary>
        /// Редактирование VRD файла
        /// </summary>
        [Description("Редактирование VRD файла")]
        EditVrdFile = 4,

        /// <summary>
        /// Остановка пула приложения
        /// </summary>
        [Description("Остановка пула приложения")]
        StopApplicationPool = 5,

        /// <summary>
        /// Запуск пула приложения
        /// </summary>
        [Description("Запуск пула приложения")]
        StartApplicationPool = 6,

        /// <summary>
        /// Получение пула приложения
        /// </summary>
        [Description("Получение пула приложения")]
        GetApplicationPool = 7,

        /// <summary>
        /// 
        /// </summary>
        [Description("Получение пути до приложения")]
        GetApplicationPath = 8,

        /// <summary>
        /// Получение пути до приложения
        /// </summary>
        [Description("Получение статуса пула приложения")]
        GetApplicationPoolState = 9,

    }
}
