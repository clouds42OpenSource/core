﻿using System.ComponentModel;
using Clouds42.Domain.Attributes;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Период оплаты
    /// </summary>
    public enum PayPeriod
    {
        /// <summary>
        /// Нет
        /// </summary>
        [NotDrawInHtml]
        None = 0,

        /// <summary>
        /// Оплата за 1 месяц
        /// </summary>
        [Description(@"Оплата за 1 месяц")]
        Month1 = 1,

        /// <summary>
        /// Оплата за 3 месяца
        /// </summary>
        [Description(@"Оплата за 3 месяца")]
        Month3 = 3,

        /// <summary>
        /// Оплата за 6 месяцев
        /// </summary>
        [Description(@"Оплата за 6 месяцев")]
        Month6 = 6,

        /// <summary>
        /// Оплата за 12 месяцев
        /// </summary>
        [Description(@"Оплата за 12 месяцев")]
        Month12 = 12
    }
}
