﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.Enums
{

    /// <summary>
    /// Тип платежной системы
    /// </summary>
    public enum PaymentSystem
    {
        /// <summary>
        /// Неизвестно
        /// </summary>
        Undefined,
        
        /// <summary>
        /// Робокасса
        /// </summary>
        Robokassa,

        /// <summary>
        /// Paybox
        /// </summary>
        Paybox,

        /// <summary>
        /// UkrPays
        /// </summary>
        UkrPays,

        /// <summary>
        /// Рудимент
        /// </summary>
        ProstoPlateg,

        /// <summary>
        /// Ручная операция
        /// </summary>
        ManualOperation,

        /// <summary>
        /// Корп
        /// </summary>
        Corp,

        /// <summary>
        /// Рудимент
        /// </summary>
        CreditCard,

        /// <summary>
        /// Рудимент
        /// </summary>
        RobokassaTest,

        /// <summary>
        /// Зачисление клауд админом через ЛК
        /// </summary>
        Admin,

        /// <summary>
        /// ЛК
        /// </summary>
        ControlPanel,

        /// <summary>
        /// Платежная система/агрегатор ЮKassa
        /// </summary>
        Yookassa
    }
    
    /// <summary>
    /// Результат платежной операции
    /// </summary>
    public enum PaymentOperationResult
    {
        /// <summary>
        /// OK
        /// </summary>
        [Description("OK")] [Display(Name = "OK")]
        Ok,

        /// <summary>
        /// Аккаунт не найден
        /// </summary>
        [Description("Account Not Found")] [Display(Name = "Аккаунт не найден")]
        ErrorAccountNotFound,

        /// <summary>
        /// Платеж не найден
        /// </summary>
        [Description("Payment Not Found")] [Display(Name = "Платеж не найден")]
        ErrorPaymentNotFound,

        /// <summary>
        /// Не хватает денег на счету
        /// </summary>
        [Description("Not Enough Balance")] [Display(Name = "Не хватает денег на счету")]
        ErrroNotEnoughBalance,

        /// <summary>
        /// Недопустимая операция
        /// </summary>
        [Description("Invalid Operation")] [Display(Name = "Недопустимая операция")]
        ErrorInvalidOperation
    }
}
