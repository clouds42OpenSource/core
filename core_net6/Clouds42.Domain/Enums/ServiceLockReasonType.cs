﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип причины блокировки сервиса
    /// </summary>
    public enum ServiceLockReasonType
    {
        /// <summary>
        /// Не оплачен сервис
        /// </summary>
        [Description("Не оплачен сервис")] ServiceNotPaid = 0,

        /// <summary>
        /// Просрочен обещанный платеж
        /// </summary>
        [Description("Просрочен обещанный платеж")] OverduePromisedPayment = 1,

        /// <summary>
        /// Нет места на диске
        /// </summary>
        [Description("Нет места на диске ")] NoDiskSpace = 2
    }
}
