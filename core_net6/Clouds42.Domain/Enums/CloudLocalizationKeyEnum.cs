﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Перечисление ключей локализации облака
    /// </summary>
    public enum CloudLocalizationKeyEnum
    {
        /// <summary>
        /// Аренда 1С
        /// </summary>
        [Description("Аренда 1С")]
        Rent1C = 0,

        /// <summary>
        /// Загрузить свою базу 1С
        /// </summary>
        [Description("Загрузить свою базу 1С")]
        UploadYourDatabase = 1,

        /// <summary>
        /// У вас еще нет ни одной базы данных 1С
        /// </summary>
        [Description("У вас еще нет ни одной базы данных 1С")]
        YouDontHaveAnyDatabaseYet = 2,

        /// <summary>
        /// Скачайте ярлык подключения к 1С
        /// </summary>
        [Description("Скачайте ярлык подключения к 1С")]
        DownloadShortcutForConnectingTo1C = 3,

        /// <summary>
        /// конфигураций 1С
        /// </summary>
        [Description("конфигураций 1С")]
        Configurations1C = 4,

        /// <summary>
        /// работа с 1С через браузер
        /// </summary>
        [Description("работа с 1С через браузер")]
        WorkWith1CThroughBrowser = 5,

        /// <summary>
        /// Подключение к 1С для пользователей
        /// </summary>
        [Description("Подключение к 1С для пользователей")]
        ConnectionTo1CForUsers = 6,

        /// <summary>
        /// Подключение к 1С для устройств
        /// </summary>
        [Description("Подключение к 1С для устройств")]
        ConnectionTo1CForDevices = 7,

        /// <summary>
        /// Достаточно запустить ярлык 1С
        /// </summary>
        [Description("Достаточно запустить ярлык 1С")]
        ItIsEnoughToRun1CShortcut = 8,

        /// <summary>
        /// После ввода пароля можете приступать к работе в 1С
        /// </summary>
        [Description("После ввода пароля можете приступать к работе в 1С")]
        AfterEnteringPasswordYouCanStartWorkingIn1C = 9,

        /// <summary>
        /// Для подключения к 1С
        /// </summary>
        [Description("Для подключения к 1С")]
        ToConnectTo1C = 10,

        /// <summary>
        /// Не обнаружены пользователи 1С
        /// </summary>
        [Description("Не обнаружены пользователи 1С")]
        No1CUsersFound = 11,

        /// <summary>
        /// Создание базы 1C
        /// </summary>
        [Description("Создание базы 1C")]
        CreationOf1CDatabase = 12,

        /// <summary>
        /// Для пользователей 1С
        /// </summary>
        [Description("Для пользователей 1С")]
        For1CUsers = 13,

        /// <summary>
        /// Проводится создание базы 1С
        /// </summary>
        [Description("Проводится создание базы 1С")]
        Database1CIsBeingCreated = 14,

        /// <summary>
        /// Конфигурация 1C
        /// </summary>
        [Description("Конфигурация 1C")]
        Configuration1C = 15,

        /// <summary>
        /// Тут вы можете перенести свою базу 1С в облако
        /// </summary>
        [Description("Тут вы можете перенести свою базу 1С в облако")]
        HereYouCanTransferYour1CDatabaseToCloud = 16,

        /// <summary>
        /// Пользователь 1С
        /// </summary>
        [Description("Пользователь 1С")]
        User1C = 17,

        /// <summary>
        /// Укажите соответствие хотя бы для одного пользователя 1С
        /// </summary>
        [Description("Укажите соответствие хотя бы для одного пользователя 1С")]
        SpecifyComplianceForAtLeastOne1CUser = 18,

        /// <summary>
        /// Пользователь 1С является администратором в базе 1С
        /// </summary>
        [Description("Пользователь 1С является администратором в базе 1С")]
        UserIsAnAdministratorInThe1CDatabase = 19,

        /// <summary>
        /// Восстановление базы 1С
        /// </summary>
        [Description("Восстановление базы 1С")]
        Restoring1CDatabase = 20,

        /// <summary>
        /// Укажите название вашей базы данных 1С
        /// </summary>
        [Description("Укажите название вашей базы данных 1С")]
        EnterNameOfYour1CDatabase = 21,

        /// <summary>
        /// Авторизационные данные в базе 1С
        /// </summary>
        [Description("Авторизационные данные в базе 1С")]
        AuthorizationDataIn1CDatabase = 22,

        /// <summary>
        /// Откройте вашу локальную базу 1С
        /// </summary>
        [Description("Откройте вашу локальную базу 1С")]
        OpenYourLocal1CDatabase = 23,

        /// <summary>
        /// В вашей базе 1С нет активных сеансов
        /// </summary>
        [Description("В вашей базе 1С нет активных сеансов")]
        NoActiveSessionsInYour1CDatabase = 24,

        /// <summary>
        /// Выберите наиболее удобный для Вас способ подключения к 1С
        /// </summary>
        [Description("Выберите наиболее удобный для Вас способ подключения к 1С")]
        ChooseMostConvenientWayToConnectTo1C = 25,

        /// <summary>
        /// Подключение к 1С
        /// </summary>
        [Description("Подключение к 1С")]
        ConnectionTo1C = 26,

        /// <summary>
        /// с активной Арендой 1С
        /// </summary>
        [Description("с активной Арендой 1С")]
        WithActiveRent1C = 27,

        /// <summary>
        /// Для копирования фрагментов текста с планшета в 1С
        /// </summary>
        [Description("Для копирования фрагментов текста с планшета в 1С")]
        ToCopyFragmentsOfTextFromTabletTo1C = 28,

        /// <summary>
        /// удаленный рабочий стол 1С
        /// </summary>
        [Description("удаленный рабочий стол 1С")]
        RemoteDesktop1C = 29,

        /// <summary>
        /// Процесс создания новой базы 1С
        /// </summary>
        [Description("Процесс создания новой базы 1С")]
        ProcessOfCreatingNew1CDatabase = 30,

        /// <summary>
        /// Подключить Аренду 1С
        /// </summary>
        [Description("Подключить Аренду 1С")]
        ConnectRent1C = 31,

        /// <summary>
        /// Проверка логина и пароля 1С базы
        /// </summary>
        [Description("Проверка логина и пароля 1С базы")]
        CheckingLoginAndPasswordOf1CDatabase = 32,

        /// <summary>
        /// Ошибка получения данных об аренде 1С
        /// </summary>
        [Description("Ошибка получения данных об аренде 1С")]
        ErrorGettingDataAboutRent1C = 33,

        /// <summary>
        /// Ошибка блокировки Аренды 1С для пользователя
        /// </summary>
        [Description("Ошибка блокировки Аренды 1С для пользователя")]
        ErrorBlockingRent1CForUser = 34,

        /// <summary>
        /// Ошибка разблокировки Аренды 1С для пользователя
        /// </summary>
        [Description("Ошибка разблокировки Аренды 1С для пользователя")]
        ErrorUnlockingRent1CForUser = 35,

        /// <summary>
        /// Завершение процессов 1С для информационной базы
        /// </summary>
        [Description("Завершение процессов 1С для информационной базы")]
        CompletionOfProcesses1CForDatabase = 36,

        /// <summary>
        /// Не удалось получить путь к веб расширению 1С из файла
        /// </summary>
        [Description("Не удалось получить путь к веб расширению 1С из файла")]
        FailedToGetPathTo1CWebExtensionFromFile = 37,

        /// <summary>
        /// Список соответствия пользователей 1С и пользователей аккаунта
        /// </summary>
        [Description("Список соответствия пользователей 1С и пользователей аккаунта")]
        ListOfCorrespondenceBetween1CUsersAndAccountUsers = 38,

        /// <summary>
        /// Проверка выхода новых релизов по конфигурациям 1С 
        /// </summary>
        [Description("Проверка выхода новых релизов по конфигурациям 1С")]
        CheckingReleaseOfNewReleasesFor1CConfigurations = 39,

        /// <summary>
        /// Проверка выхода новых релизов по конфигурациям 1С 
        /// </summary>
        [Description("На сервере не установлено платформы 1С")]
        Platform1CIsNotInstalledOnServer = 40,

        /// <summary>
        /// на сервере 1С Предприятие 
        /// </summary>
        [Description("на сервере 1С Предприятие")]
        On1CEnterpriseServer = 41,

        /// <summary>
        /// В сегменте не указана альфа версия платформы 1С
        /// </summary>
        [Description("В сегменте не указана альфа версия платформы 1С")]
        SegmentDoesNotIndicateAlphaVersionOf1CPlatform = 42,

        /// <summary>
        /// ИНН
        /// </summary>
        [Description("ИНН")]
        Inn = 43
    }
}
