﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Доступные типы хостов: Ядро, ЛК, Промо, Криптосервис.
    /// </summary>
    public enum HostTypes
    {
        Core,
        Cp,
        Ext,
        WsServer,
        AuthServer
    }
}