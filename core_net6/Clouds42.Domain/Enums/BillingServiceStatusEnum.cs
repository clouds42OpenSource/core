﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус сервиса
    /// </summary>
    public enum BillingServiceStatusEnum
    {
        /// <summary>
        /// Черновик сервиса
        /// </summary>
        [Description("Черновик")]
        Draft = 1,

        /// <summary>
        /// Сервис находиться на модерации
        /// </summary>
        [Description("На модерации")]
        OnModeration = 2,

        /// <summary>
        /// Сервис активен
        /// </summary>
        [Description("Активен")]
        IsActive = 3,
    }
}
