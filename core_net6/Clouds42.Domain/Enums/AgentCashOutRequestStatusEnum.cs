﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус заявки на вывод средств агента
    /// </summary>
    public enum AgentCashOutRequestStatusEnum
    {
        /// <summary>
        /// Статус "Новая"
        /// </summary>
        [Description("Новая")]
        New = 1,

        /// <summary>
        /// Статус "В работе"
        /// </summary>
        [Description("В работе")]
        InProcess = 2,

        /// <summary>
        /// Статус "Оплачена"
        /// </summary>
        [Description("Оплачена")]
        Paid = 3
    }
}
