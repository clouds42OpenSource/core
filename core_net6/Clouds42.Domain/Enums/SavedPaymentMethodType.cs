﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип сохраненного способа оплаты
    /// </summary>
    public enum SavedPaymentMethodType
    {
        /// <summary>
        /// Банковская карта
        /// </summary>
        [Description("bank_card")]
        BankCard = 0,
    }
}
