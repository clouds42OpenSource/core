﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип запроса к сервису
    /// на получение реквизитов
    /// </summary>
    public enum DaDataRequestType
    {
        /// <summary>
        /// Получение реквизитов банка
        /// </summary>
        [Description("Получение реквизитов банка")]
        GettingBankDetails = 1,

        /// <summary>
        /// Получение реквизитов компании
        /// </summary>
        [Description("Получение реквизитов компании")]
        GettingCompanyDetails = 2,
    }
}
