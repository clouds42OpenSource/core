﻿using System;
using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип платежа
    /// </summary>
    [Serializable]
    public enum PaymentType
    {
        /// <summary>
        /// Входящий
        /// </summary>
        [Description("Входящий")]
        Inflow,

        /// <summary>
        /// Исходящий
        /// </summary>
        [Description("Исходящий")]
        Outflow
    }
}