﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип подключения к информационной базе
    /// </summary>
    public enum ConnectionToDatabaseTypeEnum
    {
        /// <summary>
        /// Web и Rdp подключение
        /// </summary>
        [Description("Web и Rdp подключение")]
        WebAndRdpConnection = 0,

        /// <summary>
        /// Web или Rdp подключение
        /// </summary>
        [Description("Web или Rdp подключение")]
        WebOrRdpConnection = 1,

        /// <summary>
        /// Без подключения к базе
        /// </summary>
        [Description("Без подключения к базе")]
        WithoutConnection = 2
    }
}
