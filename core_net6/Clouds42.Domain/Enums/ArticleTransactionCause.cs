﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    public enum ArticleTransactionCause
    {
        [Description("Без категории")]
        UndeCause = 0,

        [Description("публикация статьи")]
        ArticlePublication = 1,

        [Description("регистрация по статье")]
        UserRegistration = 2,
    }
}
