﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип транзакции
    /// </summary>
    public enum TransactionType
    {
        /// <summary>
        /// Денежная транзакция
        /// </summary>
        [Description("Денежная")]
        Money,

        /// <summary>
        /// Бонусная транзакция
        /// </summary>
        [Description("Бонусная")]
        Bonus
    }
}