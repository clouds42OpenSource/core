﻿namespace Clouds42.Domain.Enums.Billing
{
    /// <summary>
    /// Тип назначения счета на оплату
    /// </summary>
    public enum InvoicePurposeType
    {
        /// <summary>
        /// Основные сервисы биллинга
        /// </summary>
        BaseBillingServices,

        /// <summary>
        /// Дополнительные услуги и сервисы
        /// </summary>
        AdditionalBillingServices,

        /// <summary>
        /// Произвольная сумма
        /// </summary>
        ArbitraryAmount
    }
}
