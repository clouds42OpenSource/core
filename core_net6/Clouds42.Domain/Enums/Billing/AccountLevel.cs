﻿using System;

namespace Clouds42.Domain.Enums.Billing;

///Не актуально
/// <summary>Тарифный план аккаунта</summary>
public enum AccountLevel
{
    /// <summary>Подходит для любого тарифного плана аккаунта</summary>
    Any,

    /// <summary> Тариф "Стандартный" от 600 руб./мес.</summary>
    Standart,

    /// <summary>Тариф "Профессиональный" от 1000 руб./мес.</summary>
    Professional
}


/// <summary>
/// Расширения для биллинга
/// </summary>
public static class BillingExtensions
{
    /// <summary>
    /// Получить тип уровеня аккаунта
    /// </summary>
    /// <param name="accountLevel">Уровень аккаунта</param>
    /// <returns>Тип уровеня аккаунта</returns>
    public static AccountLevel GetAccountLevel(this string accountLevel)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(accountLevel))
                return AccountLevel.Any;

            return (AccountLevel)Enum.Parse(typeof(AccountLevel), accountLevel);
        }
        catch
        {
            // все экзепшны должна перехватывать вызывающая сторона 
            // по умолчанию - стандарт
            return AccountLevel.Standart;
        }
            
    }
}