﻿namespace Clouds42.Domain.Enums.Billing
{
    /// <summary>
    /// Значение по которому будет округлятся частичная оплата за сервисы для каждой локали
    /// </summary>
    public enum LocaleRoundCost
    {
        /// <summary>
        /// Округление для украинской локали
        /// </summary>
        Ukr = 20,

        /// <summary>
        /// Округление для российской локали
        /// </summary>
        Rus = 100,

        /// <summary>
        /// Округление для казахской локали
        /// </summary>
        Kaz = 100,

        /// <summary>
        /// Округление для узбекской локали
        /// </summary>
        Uzb = 100
    }
}
