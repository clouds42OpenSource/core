﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Billing
{
    /// <summary>
    /// Тип действия платежа
    /// </summary>
    public enum PaymentActionType
    {
        /// <summary>
        /// Продление работы сервиса
        /// </summary>
        [Description("Продление работы сервиса")]
        ProlongService = 0,

        /// <summary>
        /// Покупка сервиса
        /// </summary>
        [Description("Покупка сервиса")]
        PayService = 1
    }
}
