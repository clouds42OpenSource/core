﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Внутренний облачный сервис
    /// </summary>
    public enum InternalCloudServiceEnum
    {
        /// <summary>
        /// Деланс
        /// </summary>
        [Description("Деланс")]
        Delans = 1,

        /// <summary>
        /// Саюри
        /// </summary>
        [Description("Саюри")]
        Sauri = 2,

        /// <summary>
        /// Кладовой 
        /// </summary>
        [Description("Кладовой")]
        Kladovoy = 3,

        /// <summary>
        /// Управление бухгалтерией (Тардис)
        /// </summary>
        [Description("Управление бухгалтерией")]
        Tardis = 4,

        /// <summary>
        /// Сервис управления и оптимизации налогов (MCOB)
        /// </summary>
        [Description("Сервис управления и оптимизации налогов")]
        Mcob = 5,

        /// <summary>
        /// Сервис автоматического подведение итогов встречи в телеграмм (AlpacaMeet)
        /// </summary>
        [Description("Сервис автоматического подведение итогов встречи в телеграмм")]
        AlpacaMeet = 6,

        /// <summary>
        /// Платформа для взаимодействия ИИ с бизнесом
        /// </summary>
        [Description("Платформа для взаимодействия ИИ с бизнесом")]
        Neuro42 = 7
    }
}
