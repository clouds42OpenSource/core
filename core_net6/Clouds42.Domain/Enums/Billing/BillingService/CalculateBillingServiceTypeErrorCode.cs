﻿namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Результат активации услуги сервиса. Код ошибки активации
    /// </summary>
    public enum CalculateBillingServiceTypeErrorCode
    {
        /// <summary>
        /// Не подключена Аренда 1С
        /// </summary>
        Rent1CIsNotActive = 0,

        /// <summary>
        /// Пользователь заблокирован
        /// </summary>
        AccountUserIsNotActivated = 1
    }
}