﻿namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Статус сервиса для аккаунта
    /// </summary>
    public enum BillingServiceStatusForAccountEnum
    {
        /// <summary>
        /// Сервис не активен
        /// </summary>
        NotActivated = 0,

        /// <summary>
        /// Сервис не активен для аккаунта
        /// </summary>
        NotActiveForAccount = 1,

        /// <summary>
        /// Сервис активен
        /// </summary>
        Activated = 2
    }
}
