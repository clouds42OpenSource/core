﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Расширение файла разработки 1С
    /// </summary>
    public enum Service1CFileExtensionEnum
    {
        /// <summary>
        /// Zip файл
        /// </summary>
        [Description(".zip")]
        Zip = 1,

        /// <summary>
        /// Cfe файл
        /// </summary>
        [Description(".cfe")]
        Cfe = 2
    }
}
