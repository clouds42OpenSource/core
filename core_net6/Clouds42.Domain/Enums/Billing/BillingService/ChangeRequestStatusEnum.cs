﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Статус заявки на модерацию
    /// </summary>
    public enum ChangeRequestStatusEnum
    {
        /// <summary>
        /// На модерации
        /// </summary>
        [Description("на модерации")]
        OnModeration = 1,

        /// <summary>
        /// Проверена
        /// </summary>
        [Description("проверено")]
        Checked = 2,

        /// <summary>
        /// Отклонена
        /// </summary>
        [Description("отклонено")]
        Rejected = 3,

        /// <summary>
        /// Внедрено
        /// </summary>
        [Description("внедрено")]
        Implemented = 4
    }
}
