﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Billing.BillingService
{
    /// <summary>
    /// Тип заявки на модерацию
    /// </summary>
    public enum ChangeRequestTypeEnum
    {
        /// <summary>
        /// Создание 
        /// </summary>
        [Description("Создание")]
        Creation = 1,

        /// <summary>
        /// Редактирование
        /// </summary>
        [Description("Редактирование")]
        Editing = 2
    }
}
