﻿namespace Clouds42.Domain.Enums.Billing
{
    /// <summary>
    /// Типы продуктов инвойса    
    /// </summary>
    /// <remarks>
    /// Используется для того что бы определить каким образом загружать калькулятор продукта при расчете инвойса
    /// </remarks>
    public enum ServiceCalculatorType
    {
        /// <summary>
        /// Дисковое пространство
        /// </summary>        
        DiskSpace = 1,

        /// <summary>
        /// Основные серивисы по подписке
        /// </summary>        
        SubscriptionService = 2,

        /// <summary>
        /// Сервисы информационных баз
        /// </summary>        
        MyDatabaseService = 3,

        /// <summary>
        /// Дополнительные ресурсы
        /// </summary>        
        AdditionalResources = 4,

        /// <summary>
        /// Лицензии Fasta
        /// </summary>        
        Esdl = 5,

        /// <summary>
        /// Страницы сервиса Recognition
        /// </summary>        
        Recognition = 6
    }
}
