﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Платежный агрегатор русской локали
    /// </summary>
    public enum RussianLocalePaymentAggregatorEnum
    {
        /// <summary>
        /// Платежный агрегатор Робокасса
        /// </summary>
        [Description("Робокасса")]
        Robokassa = 0,

        /// <summary>
        /// Платежный агрегатор ЮKassa
        /// </summary>
        [Description("ЮKassa")]
        Yookassa = 1
    }
}
