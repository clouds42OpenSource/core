﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип источника регистрации пользователя
    /// </summary>
    public enum ExternalClient
    {
        /// <summary>
        /// Промо-сайт
        /// </summary>
        Promo = 0,

        /// <summary>
        /// Sauri
        /// </summary>
        Sauri = 1,

        /// <summary>
        /// Доставка
        /// </summary>
        Dostavka = 2,

        /// <summary>
        /// Efsol
        /// </summary>
        Efsol = 3,

        /// <summary>
        /// Delans
        /// </summary>
        Delans = 4,

        /// <summary>
        /// Profit-Account
        /// </summary>
        ProfitAccount = 5,

        /// <summary>
        /// АбонЦентр
        /// </summary>
        AbonCenter = 6,

        /// <summary>
        /// Розница 42
        /// </summary>
        Roznica42 = 7,

        /// <summary>
        /// Кладовой.
        /// </summary>
        Kladovoy = 8,

        /// <summary>
        /// Dmitriy Yashchenko
        /// </summary>
        Yashchenko = 9,

        /// <summary>
        /// Маркет
        /// </summary>
        Market = 10,
        
        /// <summary>
        /// МЦОБ
        /// </summary>
        MCOB = 11,

        /// <summary>
        /// Сбербанк Клауд
        /// </summary>
        Cloud = 12
    }
}
