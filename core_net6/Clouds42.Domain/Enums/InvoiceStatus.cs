﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус счета
    /// </summary>
    public enum InvoiceStatus
    {
        /// <summary>
        /// Ожидает оплаты
        /// </summary>
        [Description("Ожидает оплаты")]
        Processing,

        /// <summary>
        /// Оплачен
        /// </summary>
        [Description("Оплачен")]
        Processed,

        /// <summary>
        /// Отменен
        /// </summary>
        [Description("Отменен")]
        Canceled
    }
}
