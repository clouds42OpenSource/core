﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Тип позиции элемента
    /// </summary>
    public enum Positions
    {
        /// <summary>
        /// Сверху
        /// </summary>
        Top = 0,

        /// <summary>
        /// Слева
        /// </summary>
        Left = 1,

        /// <summary>
        /// Справа
        /// </summary>
        Right = 2
    }
}