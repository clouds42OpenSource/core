﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Режимы линка
    /// </summary>
    public enum AppModes
    {
        /// <summary>
        /// Неопределён
        /// </summary>
        None = -1,

        /// <summary>
        /// Клиентский Линк
        /// </summary>
        ClientApp = 0,

        /// <summary>
        /// Терминальный Линк
        /// </summary>
        CloudApp = 1,

        /// <summary>
        /// Терминальный Линк в режиме RemoteApp, используется только программно, в настройках указывать его нельзя
        /// </summary>
        RemoteApp = 2
    }
}