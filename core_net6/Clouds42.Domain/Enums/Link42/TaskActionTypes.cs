﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Не понятно что это. Возможно нужно выкосить
    /// </summary>
    public enum TaskActionTypes
    {
        /// <summary>
        /// Запрос
        /// </summary>
        Query = 0,

        /// <summary>
        /// Получение
        /// </summary>
        Get = 1,

        /// <summary>
        /// Создание
        /// </summary>
        Create = 2,

        /// <summary>
        /// Редактирование
        /// </summary>
        Edit = 3,

        /// <summary>
        /// Удаление
        /// </summary>
        Delete = 4,

        /// <summary>
        /// Поиск
        /// </summary>
        Find = 5,

        /// <summary>
        /// Отправка
        /// </summary>
        Post = 6,

        /// <summary>
        /// Получение ID
        /// </summary>
        GetID = 7,

        /// <summary>
        /// Команда
        /// </summary>
        Command = 8,

        /// <summary>
        /// Ничего не делать
        /// </summary>
        DoNothing = 9
    }
}
