﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Действия Линка
    /// </summary>
    public enum LinkActionType
    {
        /// <summary>
        /// Действие запустить базу
        /// </summary>
        LaunchDatabase = 1,

        /// <summary>
        /// Действие запустить RDP
        /// </summary>
        OpenRdp = 2
    }
}