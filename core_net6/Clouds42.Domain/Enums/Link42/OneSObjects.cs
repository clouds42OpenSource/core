﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Не понятно что это. Возможно нужно выкосить
    /// </summary>
    public enum OneSObjects
    {
        /// <summary>
        /// Строка
        /// </summary>
        String = 0,
        
        /// <summary>
        /// Справочник
        /// </summary>
        Catalog = 1,
        
        /// <summary>
        /// Документ
        /// </summary>
        Document = 2,
        
        /// <summary>
        /// Перечисление
        /// </summary>
        Enum = 3,
        
        /// <summary>
        /// Число
        /// </summary>
        Number = 4,
        
        /// <summary>
        /// Дата
        /// </summary>
        Date = 5,

        /// <summary>
        /// Булево
        /// </summary>
        Boolean = 6,

        /// <summary>
        /// Неопределено
        /// </summary>
        Undefined = 7
    }
}