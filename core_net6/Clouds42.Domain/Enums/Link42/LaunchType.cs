﻿namespace Clouds42.Domain.Enums.Link42
{
    /// <summary>
    /// Типы запуска информационных баз
    /// </summary>
    public enum LaunchType
    {
        /// <summary>
        /// Тип запуска не определён
        /// </summary>
        None = -1,

        /// <summary>
        /// Запуск определяется автоматически
        /// </summary>
        Auto = 0,

        /// <summary>
        /// Тонкий клиент
        /// </summary>
        Thin = 1,

        /// <summary>
        /// Тонкий веб клиент.
        /// </summary>
        ThinWeb = 2,

        /// <summary>
        /// Удаленное приложение.
        /// </summary>
        RemApp = 3,

        /// <summary>
        /// Вэб клиент.
        /// </summary>
        WebClient = 4,

        /// <summary>
        /// Толстый клиент.
        /// </summary>
        Thick = 5
    }
}