﻿namespace Clouds42.Domain.Enums
{

    /// <summary>
    /// Тип партнерства.
    /// </summary>
    public enum TypeOfPartnership
    {

        /// <summary>
        /// Агент.
        /// </summary>
        Agent = 1,

        /// <summary>
        /// Владелец программного продукта.
        /// </summary>
        SoftOwner = 2
    }
}