﻿using System;
using System.ComponentModel;

namespace Clouds42.Domain.Enums;

/// <summary>
/// Состояние инф. базы
/// </summary>
public enum DatabaseState
{
    /// <summary>
    /// База в неизвестном состоянии
    /// </summary>
    [Description("База в неизвестном состоянии")]
    Undefined,

    /// <summary>
    /// Новая база данных, еще ничего не загружено
    /// </summary>
    [Description("База в процессе создания")]
    NewItem,

    /// <summary>
    /// База данных готова к работе
    /// </summary>
    [Description("База готова к использованию")]
    Ready,

    /// <summary>
    /// Статус не определен
    /// </summary>
    [Description("Статус базы не определен")]
    Unknown,

    /// <summary>
    /// База отправлена в хранилище 
    /// </summary>
    [Description("База отправлена в хранилище")]
    Detached,

    /// <summary>
    /// Идет процесс восстановления базы из архива
    /// </summary>
    [Description("База в процессе восстановления из архива")]
    Attaching,

    /// <summary>
    /// База не создана, недостаточно места на диске
    /// </summary>
    [Description("База не создана, недостаточно места на диске")]
    ErrorNotEnoughSpace,

    /// <summary>
    /// База не создана, ошибка создания
    /// </summary>
    [Description("База не создана, ошибка создания")]
    ErrorCreate,

    /// <summary>
    /// База не создана, ошибка загрузки из dt
    /// </summary>
    [Description("База не создана, ошибка загрузки из dt")]
    ErrorDtFormat,

    /// <summary>
    /// Идет выполнение регламентных операций
    /// </summary>
    [Description("Идет выполнение регламентных операций.  Это может занять некоторое время.")]
    ProcessingSupport,

    /// <summary>
    /// База в состоянии переноса
    /// </summary>
    [Description("База в состоянии переноса")]
    TransferDb,

    /// <summary>
    /// Архив базы в состоянии переноса
    /// </summary>
    [Description("Архив базы в состоянии переноса")]
    TransferArchive,

    /// <summary>
    /// База в очереди на восстановление
    /// </summary>
    [Description("База в очереди на восстановление")]
    RestoringFromTomb,

    /// <summary>
    /// База в очереди на удаление в склеп
    /// </summary>
    [Description("База в очереди на удаление в склеп")]
    DelitingToTomb,

    /// <summary>
    /// База в очереди на архивацию
    /// </summary>
    [Description("База в очереди на архивацию")]
    DetachingToTomb,

    /// <summary>
    /// База удалена в склеп
    /// </summary>
    [Description ("База удалена в склеп")]
    DeletedToTomb,

    /// <summary>
    /// База в архиве
    /// </summary>
    [Description ("База в архиве")]
    DetachedToTomb,

    /// <summary>
    /// База удалена с облака
    /// </summary>
    [Description("База удалена с облака")]
    DeletedFromCloud
}

public static class DatabaseStateEnumExtensions
{
    /// <summary>
    /// Получить статус инф. базы в виде значения Enum
    /// </summary>
    /// <param name="state">Cтатус инф. базы строкой</param>
    /// <returns>Статус инф. базы в виде значения Enum</returns>
    public static DatabaseState GetDatabaseState(this string state)
    {
        return Enum.TryParse(state, out DatabaseState res) ? res : DatabaseState.Unknown;
    }
}