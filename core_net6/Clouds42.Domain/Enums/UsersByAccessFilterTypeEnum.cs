﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип фильтра пользователей по доступам
    /// </summary>
    public enum UsersByAccessFilterTypeEnum
    {
        /// <summary>
        /// Все пользователи
        /// </summary>
        [Description("Все пользователи")]
        All = 0,

        /// <summary>
        /// С доступом
        /// </summary>
        [Description("С доступом")]
        WithAccess = 1,

        /// <summary>
        /// Без доступа
        /// </summary>
        [Description("Без доступа")]
        WithoutAccess = 2,

        /// <summary>
        /// Внешние пользователи
        /// </summary>
        [Description("Внешние пользователи")]
        ExternalUsers = 3
    }
}
