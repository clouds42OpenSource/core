﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Разрядность приложений
    /// </summary>
    public enum ApplicationBitDepthEnum
    {
        /// <summary>
        /// Разрядность приложения X86
        /// </summary>
        X86,
        /// <summary>
        /// Разрядность приложения X64
        /// </summary>
        X64
    }
}