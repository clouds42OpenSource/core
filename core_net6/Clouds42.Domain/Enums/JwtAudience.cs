﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// К чему пренадлежит JWT токен
    /// </summary>
    public enum JwtAudience
    {
        /// <summary>
        /// Для кого угодно
        /// </summary>
        Any = 1,

        /// <summary>
        /// Для ЛК
        /// </summary>
        Cp = 2
    }
}