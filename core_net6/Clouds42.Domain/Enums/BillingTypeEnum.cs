﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип работы биллинга
    /// </summary>
    public enum BillingTypeEnum
    {
        /// <summary>
        /// По пользователю
        /// </summary>
        [Description("По пользователю")]
        ForAccountUser = 0,

        /// <summary>
        /// По аккауну
        /// </summary>
        [Description("На аккаунт")]
        ForAccount = 1
    }
}