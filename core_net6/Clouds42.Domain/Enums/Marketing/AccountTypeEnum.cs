﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Marketing
{
    /// <summary>
    /// Тип аккаунта
    /// </summary>
    public enum AccountTypeEnum
    {
        /// <summary>
        /// Все
        /// </summary>
        [Description("Все")]
        All = 0,

        /// <summary>
        /// VIP аккаунт
        /// </summary>
        [Description("VIP")]
        Vip = 1,

        /// <summary>
        /// Не VIP аккаут
        /// </summary>
        [Description("Не VIP")]
        NotVip = 2
    }
}
