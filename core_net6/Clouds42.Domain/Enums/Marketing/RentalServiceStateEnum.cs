﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Marketing
{
    /// <summary>
    /// Состояние сервиса Аренда 1С
    /// </summary>
    public enum RentalServiceStateEnum
    {
        /// <summary>
        /// Все
        /// </summary>
        [Description("Все")]
        All = 0,

        /// <summary>
        /// Активная Аренда 1С
        /// </summary>
        [Description("Активен")]
        Active = 1,

        /// <summary>
        /// Заблокирована Аренда 1С
        /// </summary>
        [Description("Заблокирован")]
        Locked = 2
    }
}
