﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Marketing
{
    /// <summary>
    /// Наличие оплаты
    /// </summary>
    public enum AvailabilityPaymentEnum
    {
        /// <summary>
        /// Все
        /// </summary>
        [Description("Все")]
        All = 0,

        /// <summary>
        /// С оплатой
        /// </summary>
        [Description("С оплатой")]
        WithPayment = 1,

        /// <summary>
        /// Без оплаты
        /// </summary>
        [Description("Без оплаты")]
        WithoutPayment = 2
    }
}
