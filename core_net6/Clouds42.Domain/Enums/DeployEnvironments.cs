﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Перечисление доступных окружений развёртывания.
    /// </summary>
    public enum DeployEnvironments
    {
        Production,
        Beta,
        Testing,
        Development
    }
}