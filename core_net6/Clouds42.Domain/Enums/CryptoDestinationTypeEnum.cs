﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип шифровки
    /// </summary>
    public enum CryptoDestinationTypeEnum
    {
        /// <summary>
        /// Тип провайдера общей шифровки по алгритму AES
        /// </summary>
        CommonAes,

        /// <summary>
        /// Тип провайдера пользовательской шифровки по алгритму AES
        /// </summary>
        UserAes
    }
}