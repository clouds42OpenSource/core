﻿using System.ComponentModel;
using Clouds42.Domain.Attributes;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус платежа ЮKassa
    /// </summary>
    public enum YookassaPaymentStatusEnum
    {
        /// <summary>
        /// Платеж создан и ожидает действий от пользователя.
        /// Из этого статуса платеж может перейти в succeeded, waiting_for_capture
        /// или canceled (если что-то пошло не так)
        /// </summary>
        [Description("В ожидании")] [StringValue("pending")]
        Pending = 0,

        /// <summary>
        /// Платеж оплачен, деньги авторизованы и ожидают списания.
        /// Из этого статуса платеж может перейти в succeeded
        /// или canceled(если платеж отменен или что-то пошло не так)
        /// </summary>
        [Description("Платеж оплачен, деньги ожидают списания")] [StringValue("waiting_for_capture")]
        WaitingForCapture = 1,

        /// <summary>
        /// Платеж успешно завершен, деньги будут перечислены на расчетный счет.
        /// Это финальный и неизменяемый статус
        /// </summary>
        [Description("Платеж успешно завершен")] [StringValue("succeeded")]
        Succeeded = 2,

        /// <summary>
        /// Платеж отменен. Если платеж отменен самостоятельно, истекло время
        /// на принятие платежа или платеж был отклонен ЮKassa.
        /// Это финальный и неизменяемый статус
        /// </summary>
        [Description("Платеж отменен")] [StringValue("canceled")]
        Canceled = 3
    }
}
