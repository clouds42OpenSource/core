﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип элемента сегмента
    /// </summary>
    public enum CloudSegmentElementType
    {
        /// <summary>
        /// Шлюз терминала
        /// </summary>
        [Description("Шлюз терминала")]
        CloudServicesGatewayTerminals,

        /// <summary>
        /// Ферма ТС
        /// </summary>
        [Description("Ферма ТС")]
        CloudServicesTerminalFarm,

        /// <summary>
        /// Сервер SQL
        /// </summary>
        [Description("Сервер SQL")]
        CloudServicesSqlServer,

        /// <summary>
        /// Сервер публикаций
        /// </summary>
        [Description("Сервер публикаций")]
        CloudServicesContentServer,

        /// <summary>
        /// Сервер 1С:Предприятие 8.2
        /// </summary>
        [Description("Сервер 1С:Предприятие 8.2")]
        CloudServicesEnterpriseServer82,

        /// <summary>
        /// Сервер 1С:Предприятие 8.3
        /// </summary>
        [Description("Сервер 1С:Предприятие 8.3")]
        CloudServicesEnterpriseServer83,

        /// <summary>
        /// Хранилище для файлов
        /// </summary>
        [Description("Хранилище для файлов")]
        CloudServicesFileStorageServers,

        /// <summary>
        /// Сервер бекапов
        /// </summary>
        [Description("Сервер бекапов")]
        CloudServicesBackupStorage,

        /// <summary>
        /// Хостинг облака
        /// </summary>
        [Description("Хостинг облака")]
        CoreHosting
    }
}
