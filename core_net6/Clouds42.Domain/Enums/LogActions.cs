﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Действия для логирования
    /// </summary>
    public enum LogActions
    {
        /// <summary>
        /// Добавление пользователя
        /// </summary>
        [Description("Добавление пользователя")]
        AddingUser = 1,
        /// <summary>
        /// Удаление пользователя
        /// </summary>
        [Description("Удаление пользователя")]
        RemoveUser = 2,
        /// <summary>
        /// Подключение пользователя к сервису
        /// </summary>
        [Description("Подключение пользователя к сервису")]
        ConnectToService = 3,
        /// <summary>
        /// Отключение пользователя от сервиса
        /// </summary>
        [Description("Отключение пользователя от сервиса")]
        DisconnectFromService = 4,
        /// <summary>
        /// Продление сервиса
        /// </summary>
        [Description("Продление сервиса")]
        ProlongationService = 5,
        /// <summary>
        /// Блокировка сервиса
        /// </summary>
        [Description("Блокировка сервиса")]
        LockService = 6,
        /// <summary>
        /// Изменение стоимости сервиса
        /// </summary>
        [Description("Изменение стоимости сервиса")]
        EditServiceCost = 7,
        /// <summary>
        /// Внесение средств через КОРП
        /// </summary>
        [Description("Внесение средств через КОРП")]
        RefillByCorp = 8,
        /// <summary>
        /// Внесение средств через платежную систему
        /// </summary>
        [Description("Внесение средств через платежную систему")]
        RefillByPaymentSystem = 9,
        /// <summary>
        /// Взятие обещанного платежа
        /// </summary>
        [Description("Взятие обещанного платежа")]
        TakePromisePayment = 10,
        /// <summary>
        /// Списание обещанного платежа
        /// </summary>
        [Description("Списание обещанного платежа")]
        RepayPromisePayment = 11,
        /// <summary>
        /// Изменение баланса вручную
        /// </summary>
        [Description("Изменение баланса вручную")]
        ManualBalanceEdit = 12,
        /// <summary>
        /// Изменен тариф диска
        /// </summary>
        [Description("Изменен тариф диска")]
        DickRateEdit = 13,
        /// <summary>
        /// Изменение количества страниц ЗД
        /// </summary>
        [Description("Изменение количества страниц ЗД")]
        PagesAmountChanged = 14,
        /// <summary>
        /// Покупка лицензии ЗД
        /// </summary>
        [Description("Покупка лицензии ЗД")]
        BuyLicense = 15,
        /// <summary>
        /// Добавление информационной базы
        /// </summary>
        [Description("Добавление информационной базы")]
        AddInfoBase = 16,
        /// <summary>
        /// Удаление информационной базы
        /// </summary>
        [Description("Удаление информационной базы")]
        DeleteAccountDatabase = 17,
        /// <summary>
        /// Добавлен доступ к информационной базе
        /// </summary>
        [Description("Добавлен доступ к информационной базе")]
        AddAccessToInfoBase = 18,
        /// <summary>
        /// Удален доступ к информационной базе
        /// </summary>
        [Description("Удален доступ к информационной базе")]
        RemoveAccessToInfoBase = 19,
        /// <summary>
        /// Подключение базы к ТиИ 
        /// </summary>
        [Description("Подключение базы к ТиИ")]
        ConnectInfoBaseTaC = 20,
        /// <summary>
        /// Отключение базы от ТиИ 
        /// </summary>
        [Description("Отключение базы от ТиИ")]
        DisConnectInfoBaseTaC = 21,
        /// <summary>
        /// Ошибка проведения ТиИ
        /// </summary>
        [Description("Ошибка проведения ТиИ")]
        ErrorTehSupport = 22,
        /// <summary>
        /// Успешно проведенное ТиИ
        /// </summary>
        [Description("Успешно проведенное ТиИ")]
        SuccessTehSupport = 23,
        /// <summary>
        /// Публикация базы
        /// </summary>
        [Description("Публикация базы")]
        PublishInfoBase = 24,
        /// <summary>
        /// Снятие публикации базы
        /// </summary>
        [Description("Снятие публикации базы")]
        UnpublishInfoBase = 25,
        /// <summary>
        /// Выставление счета на оплату
        /// </summary>
        [Description("Выставление счета на оплату")]
        AddInvoice = 26,
        /// <summary>
        /// Подключение пользователя к спонсированию
        /// </summary>
        [Description("Подключение пользователя к спонсированию")]
        ConnectToSponsoring = 27,
        /// <summary>
        /// Отключение пользователя от спонсирования
        /// </summary>
        [Description("Отключение пользователя от спонсирования")]
        DisconnectToSponsoring = 28,
        /// <summary>
        /// Формирование акта о выполненных работах
        /// </summary>
        [Description("Формирование акта о выполненных работах")]
        FormServiceAct = 29,
        /// <summary>
        /// Перенос аккаунта между сегментами
        /// </summary>
        [Description("Перенос аккаунта между сегментами")]
        MovingAccountBetweenSegments = 30,
        /// <summary>
        /// Перенос базы между хранилищами
        /// </summary>
        [Description("Перенос базы между хранилищами")]
        MovingBaseBetweenStorage = 31,
        /// <summary>
        /// Письменное оповещение
        /// </summary>
        [Description("Письменное оповещение")]
        MailNotification = 32,
        /// <summary>
        /// Редактирование карточки аккаунта
        /// </summary>
        [Description("Редактирование карточки аккаунта")]
        EditAccountCard = 33,
        /// <summary>
        /// Редактирование карточки пользователя
        /// </summary>
        [Description("Редактирование карточки пользователя")]
        EditUserCard = 34,
        /// <summary>
        /// Блокировка базы
        /// </summary>
        [Description("Блокировка базы")]
        LockDatabase = 35,
        /// <summary>
        /// Разблокировка базы
        /// </summary>
        [Description("Разблокировка базы")]
        UnlockDatabase = 36,
        /// <summary>
        /// Редактирование информационной базы
        /// </summary>
        [Description("Редактирование информационной базы")]
        EditInfoBase = 37,
        /// <summary>
        /// Восстановление базы с архива
        /// </summary>
        [Description("Восстановление базы с архива")]
        RestoreInfoBase = 38,
        /// <summary>
        /// Ошибка подключения в ИБ
        /// </summary>
        [Description("Ошибка подключения в ИБ")]
        ErrorConnectInfoBase = 39,
        /// <summary>
        /// Успешное подключение в ИБ
        /// </summary>
        [Description("Успешное подключение в ИБ")]
        SuccessConnectInfoBase = 40,
        /// <summary>
        /// Перерасчёт дискового пространства
        /// </summary>
        [Description("Перерасчёт дискового пространства")]
        RecalculateMyDisk = 41,
        /// <summary>
        /// Восстановление информационной базы
        /// </summary>
        [Description("Восстановление информационной базы")]
        RestoreAccountDatabase = 42,
        /// <summary>
        /// Операция с базой на разделителях
        /// </summary>
		[Description("Операция с базой на разделителях")]
		DbTemplateDelimiters = 43,
        /// <summary>
        /// Ошибка проведения АО
        /// </summary>
        [Description("Ошибка проведения АО")]
        ErrorAutoUpdate = 44,
        /// <summary>
        /// Успешно проведенное АО
        /// </summary>
        [Description("Успешно проведенное АО")]
        SuccessAutoUpdate = 45,
        /// <summary>
        /// Покупка дискового пространства
        /// </summary>
        [Description("Покупка дискового пространства")]
        BuyingDiskSpace = 46,
        /// <summary>
        /// Создание счёта для аккаунта на произвольную сумму
        /// </summary>
        [Description("Создание счёта для аккаунта на произвольную сумму")]
        CreatingInvoiceForSpecifiedInvoiceAmount = 47,
        /// <summary>
        /// Выставление счета для аккаунта
        /// </summary>
        [Description("Выставление счета для аккаунта")]
        CreatingInvoiceBasedOnCalculationInvoiceModel = 48,
        /// <summary>
        /// Изменение подписки сервиса у аккаунта
        /// </summary>
        [Description("Изменение подписки сервиса у аккаунта")]
        ApplyOrPayForAllChangesAccountServiceTypes = 49,
        /// <summary>
        /// Сохранение новых данных из управления сервисом
        /// </summary>
        [Description("Сохранение новых данных из управления сервисом")]
        SaveNewOptionsControl = 50,
        /// <summary>
        /// Применение подписки демо сервиса
        /// </summary>
        [Description("Применение подписки демо сервиса")]
        ApplicationDemoServiceSubscribe = 51,
        /// <summary>
        /// Активация сервиса у существуещего пользователя
        /// </summary>
        [Description("Активация сервиса у существуещего пользователя")]
        ActivateServiceForExistingAccount = 52,
        /// <summary>
        /// Создание сервиса биллинга
        /// </summary>
        [Description("Создание сервиса биллинга")]
        CreateBillingService = 53,
        /// <summary>
        /// Сохранение изменений сервиса
        /// </summary>
        [Description("Сохранение изменений сервиса")]
        SaveBillingServiceChanges = 54,
        /// <summary>
        /// Редактирование сервиса биллинга
        /// </summary>
        [Description("Редактирование сервиса биллинга")]
        EditBillingService = 55,
        /// <summary>
        /// Изменение стоимости ресурса
        /// </summary>
        [Description("Изменение стоимости ресурса")]
        EditResourceCost = 56,
        /// <summary>
        /// Перезапуск пула на нодах публикаций
        /// </summary>
        [Description("Перезапуск пула на нодах публикаций")]
        RestartApplicationPoolOnIis = 57,
        /// <summary>
        /// Создание реквизитов агента
        /// </summary>
        [Description("Создание реквизитов агента")]
        CreateAgentRequisites = 58,
        /// <summary>
        /// Удаление реквизитов агента
        /// </summary>
        [Description("Удаление реквизитов агента")]
        RemoveAgentRequisites = 59,
        /// <summary>
        /// Редактирование реквизитов агента
        /// </summary>
        [Description("Редактирование реквизитов агента")]
        EditAgentRequisites = 60,
        /// <summary>
        /// Редактирование сегмента или элемента сегмента
        /// </summary>
	    [Description("Редактирование сегмента или элемента сегмента")]
        EditSegmentOrElement = 61,
        /// <summary>
        /// Добавление сегмента или элемента сегмента
        /// </summary>
        [Description("Добавление сегмента или элемента сегмента")]
        AddSegmentOrElement = 62,
        /// <summary>
        /// Удаление сегмента или элемента сегмента
        /// </summary>
        [Description("Удаление сегмента или элемента сегмента")]
        DeleteSegmentOrElement = 63,
        /// <summary>
        /// Создание агентской заявки на вывод средств
        /// </summary>
        [Description("Создание агентской заявки на вывод средств")]
        CreateAgentCashOutRequest = 64,
        /// <summary>
        /// Редактирование агентской заявки на вывод средств
        /// </summary>
        [Description("Редактирование агентской заявки на вывод средств")]
        EditAgentCashOutRequest = 65,
        /// <summary>
        /// Удаление агентской заявки на вывод средств
        /// </summary>
        [Description("Удаление агентской заявки на вывод средств")]
        RemoveAgentCashOutRequest = 66,
        /// <summary>
        /// Изменение печатной формы HTML
        /// </summary>
        [Description("Изменение печатной формы HTML")]
        ChangePrintedHtmlForm = 67,
        /// <summary>
        /// Изменение демо периода для сервиса Аренда 1С
        /// </summary>
        [Description("Изменение демо периода для сервиса Аренда 1С")]
        EditRent1CDemoExpireDate = 68,
        /// <summary>
        /// Изменение типа аккаунта
        /// </summary>
        [Description("Изменение типа аккаунта")]
        BindAccountToPartner = 69,
        /// <summary>
        /// Изменение агентского баланса
        /// </summary>
        [Description("Изменение агентского баланса")]
        CreateAgentPayment = 70,
        /// <summary>
        /// Изменение ответственного за аккаунт
        /// </summary>
        [Description("Изменение ответственного за аккаунт")]
        ChangeAccountSaleManager = 71,
        /// <summary>
        /// Создание служебного аккаунта
        /// </summary>
        [Description("Создание служебного аккаунта")]
        CreateServiceAccount = 72,
        /// <summary>
        /// Удаление служебного аккаунта
        /// </summary>
        [Description("Удаление служебного аккаунта")]
        DeleteServiceAccount = 73,
        /// <summary>
        /// Создание агентского соглашения
        /// </summary>
        [Description("Создание агентского соглашения")]
        CreateAgencyAgreement = 74,
        /// <summary>
        /// Удаление данных неактивного аккаунта
        /// </summary>
        [Description("Удаление архивных данных")]
        DeleteInactiveAccountData = 75,
        /// <summary>
        /// Соглашение с условиями оферты
        /// </summary>
        [Description("Соглашение с условиями оферты")]
        ApplyAgencyAgreement = 76,

        /// <summary>
        /// Создание заявки на перевод баланса агента
        /// </summary>
        [Description("Создание заявки на перевод баланса агента")]
        CreateAgentTransferBalanseRequest = 77,

        /// <summary>
        /// Выплата вознаграждения партнеру
        /// </summary>
        [Description("Выплата вознаграждения партнеру")]
        PartnerRewardPayment = 78,

        /// <summary>
        /// Отмена выплаты вознаграждения партнеру
        /// </summary>
        [Description("Отмена выплаты вознаграждения партнеру")]
        CancelPartnerRewardPayment = 79,

        /// <summary>
        /// Создание фермы серверов
        /// </summary>
        [Description("Создание фермы серверов")]
        CreateServerFarm = 80,

        /// <summary>
        /// Редактирование фермы серверов
        /// </summary>
        [Description("Редактирование фермы серверов")]
        EditServerFarm = 81,

        /// <summary>
        /// Удаление фермы серверов
        /// </summary>
        [Description("Удаление фермы серверов")]
        DeleteServerFarm = 82,
        
        /// <summary>
        /// Создание кластера ARR
        /// </summary>
        [Description("Создание кластера ARR")]
        CreateArrCluster = 83,

        /// <summary>
        /// Редактирование кластера ARR
        /// </summary>
        [Description("Редактирование кластера ARR")]
        EditArrCluster = 84,

        /// <summary>
        /// Удаление кластера ARR
        /// </summary>
        [Description("Удаление кластера ARR")]
        DeleteArrCluster = 85,

        /// <summary>
        /// Создание сервера ARR
        /// </summary>
        [Description("Создание сервера ARR")]
        AddArrServer = 86,

        /// <summary>
        /// Удаление сервера ARR
        /// </summary>
        [Description("Удаление сервера ARR")]
        DeleteArrServer = 87,

        /// <summary>
        /// Редактирование сервера ARR
        /// </summary>
        [Description("Редактирование сервера ARR")]
        EditArrServer = 88,

        /// <summary>
        /// Изменение демо периода для сервиса Fasta
        /// </summary>
        [Description("Изменение демо периода для сервиса Fasta")]
        EditFastaDemoExpireDate = 89,

        /// <summary>
        /// Установка сервиса в базу
        /// </summary>
        [Description("Установка сервиса в базу")]
        InstallingServiceInDatabase = 90,

        /// <summary>
        /// Отключение сервиса в базе
        /// </summary>
        [Description("Отключение сервиса в базе")]
        DeletionServiceInDatabase = 91,

        /// <summary>
        /// Активация сервиса в базе
        /// </summary>
        [Description("Активация сервиса в базе")]
        ActivateServiceInDatabase = 92,

        /// <summary>
        /// Управление редиректом для инф. баз
        /// </summary>
        [Description("Управление редиректом для инф. баз")]
        ManagePublishedAccountDatabaseRedirect = 93,

        /// <summary>n
        /// Завершение запущенной задачи
        /// </summary>
        [Description("Завершение запущенной задачи")]
        CancelCoreWorkerTasksQueue = 94,

        /// <summary>
        /// Редактирование задачи воркера
        /// </summary>
        [Description("Редактирование задачи воркера")]
        EditCoreWorkerTask = 95,

        /// <summary>
        /// Редактирование шаблона письма
        /// </summary>
        [Description("Редактирование шаблона письма")]
        EditLetterTemplate = 96,

        /// <summary>
        /// Отправка тестового письма
        /// </summary>
        [Description("Отправка тестового письма")]
        SendTestLetter = 97,

        /// <summary>
        /// Создание рекламного баннера для писем
        /// </summary>
        [Description("Создание рекламного баннера для писем")]
        CreateAdvertisingBannerTemplate = 98,

        /// <summary>
        /// Редактирование рекламного баннера для писем
        /// </summary>
        [Description("Редактирование рекламного баннера для писем")]
        EditAdvertisingBannerTemplate = 99,

        /// <summary>
        /// Удаление рекламного баннера для писем
        /// </summary>
        [Description("Удаление рекламного баннера для писем")]
        DeleteAdvertisingBannerTemplate = 100,

        /// <summary>
        /// Установка рекламного баннера в письме
        /// </summary>
        [Description("Установка рекламного баннера в письме")]
        SetAdvertisingBannerTemplateInLetter = 101,

        /// <summary>
        /// Смена статуса версии файла
        /// </summary>
        [Description("Смена статуса версии файла")]
        SetAuditResultForService1CFileVersion = 102,

        /// <summary>
        /// Отправка новой версия файла на аудит
        /// </summary>
        [Description("Отправка новой версии файла на аудит")]
        SubmitService1CFileVersionForAudit = 103,

        /// <summary>
        /// Создание аккаунта
        /// </summary>
        [Description("Создание аккаунта")]
        CreateAcccount = 104,

        /// <summary>
        /// Возврат на предыдущую версию
        /// </summary>
        [Description("Возврат на предыдущую версию")]
        SetService1CFileVersionRollbackResult = 105,

        /// <summary>
        /// Перемещение базы в архив
        /// </summary>
        [Description("Перемещение базы в архив")]
        MovingDbToArchive = 106,

        /// <summary>
        /// Изменение стоимости лицензии конфигурации 1С
        /// </summary>
        [Description("Изменение стоимости лицензии конфигурации 1С")]
        ChangeConfigurationCost = 107,

        /// <summary>
        /// Завершение сеансов в информационной базе
        /// </summary>
        [Description("Завершение сеансов в информационной базе")]
        TerminateSessionsInDatabase = 108,

        /// <summary>
        /// Редактирование локали
        /// </summary>
        [Description("Редактирование локали")]
        EditLocale = 109,

        /// <summary>
        /// Редактирование локали
        /// </summary>
        [Description("Покупка дополнительных сеансов")]
        BuyDopSession = 110,

        [Description("Создание личного сервиса")]
        CreatePersonalService = 111,

        [Description("Удаление личного сервиса")]
        DeletePersonalService = 112,

        /// <summary>
        /// Подключение базы к ТиИ и АО
        /// </summary>АП
        [Description("Подключение базы к АО")]
        ConnectInfoBaseToAU = 113,
        /// <summary>
        /// Отключение базы от ТиИ и АО
        /// </summary>
        [Description("Отключение базы от АО")]
        DisConnectInfoBaseToAU = 114,

        [Description("Отправка кода верификации Email")]
        SendEmailVerificationCodeToUser = 115,

        [Description("Отправка кода верификации телефона")]
        SendPhoneVerificationCodeToUser = 116,

        [Description("Успешная верификация Email")]
        SuccessEmailVerification = 117,

        [Description("Успешная верификацая телефона")]
        SuccessPhoneVerification = 118,

        /// <summary>
        /// Подключение базы к ТиИ и АО
        /// </summary>АП
        [Description("Удаление счета")]
        CancelIvoice = 119,

        /// <summary>
        /// Подключение базы к ТиИ и АО
        /// </summary>АП
        [Description("Изменение поставщика для аккаунта")]
        EditSupplairForAccount = 120,

        /// <summary>
        /// Создание статьи
        /// </summary>АП
        [Description("Создание статьи")]
        CreateArticle = 121,

        /// <summary>
        /// Изменение статуса статьи
        /// </summary>АП
        [Description("Изменение статуса статьи")]
        ChangeArticleStatus = 122,

        /// <summary>
        /// Удаление статьи
        /// </summary>АП
        [Description("Удаление статьи")]
        DeleteArticle = 123,

        /// <summary>
        /// Редактирование статьи
        /// </summary>АП
        [Description("Редактирование статьи")]
        EditArticle = 124,

        /// <summary>
        /// Ручное обновление базы
        /// </summary>
        [Description("Ручное обновление базы")]
        ManualUpdateDatabase = 125,

        /// <summary>
        /// Отмена подключения в ИБ
        /// </summary>
        [Description("Отмена подключения в ИБ")]
        DisableConnectInfoBase = 126,

        /// <summary>
        /// Ошибка проведения РО
        /// </summary>
        [Description("Ошибка проведения РО")]
        ErrorManualUpdate = 127,
        /// <summary>
        /// Успешно проведенное АО
        /// </summary>
        [Description("Успешно проведенное РО")]
        SuccessManualUpdate = 128,
        /// <summary>
        /// Вознаграждение за публикации статьи
        /// </summary>
        [Description("Вознаграждение за статью")]
        RewardArticle = 129,
        /// <summary>
        /// Изменение статуса документу
        /// </summary>
        [Description("Выставление статуса документу")]
        ChangeAccountFileStatus = 130,
        /// <summary>
        /// Загрузка документа
        /// </summary>
        [Description("Загрузка документа")]
        LoadAccountFile = 131,
        /// <summary>
        /// Аннулирование документа
        /// </summary>
        [Description("Аннулирование документа")]
        CancelTaxAuthorityRegistrationCertificate = 132,
        /// <summary>
        /// Создание доп организации
        /// </summary
        [Description("Создание доп организации")]
        CreateAdditionalCompany = 133,
        /// <summary>
        /// Редактирование доп организации
        /// </summary
        [Description("Редактирование доп организации")]
        EditAdditionalCompany = 134,
        /// <summary>
        /// Удаление доп организации
        /// </summary
        [Description("Удаление доп организации")]
        
        DeleteAdditionalCompany = 135,
        /// <summary>
        /// Уведомление об фидбеке
        /// </summary
        [Description("Пользователям аккаунта направлена нотификация об отзыве")]
        FeedbackNotificationSent = 136,
        
        [Description("Пользователь отложил отзыв")]
        FeedbackDelayed = 137,
        
        [Description("Пользовтель оставил отзыв")]
        FeedbackLeft = 138,
        
        [Description("Статус акта был обновлен")]
        ActStatusUpdated = 139,
        
        [Description("Подписанный акт загружен")]
        SignedActUploaded = 140,
        
        [Description("Генерация временной ссылки к бекапу")]
        SignedUrlGenerating= 141,
        
        [Description("Ошибка загрузки базы")]
        DatabaseLoadFailed = 142,
        
        [Description("Ошибка перевода в DT")]
        DtTransformationFailed = 143,
        
        [Description("Ошибка перевода файловой в серверную")]
        FileToClusterTransformationFailed = 145,
        
        [Description("Ошибка остановки сессии")]
        SessionStoppingFailed = 146,
        
        [Description("Создание серверной базы")]
        ClusteredDatabaseCreationStarting = 147,
        
        [Description("Действие с департаментом аккаунта")]
        DepartmentAccountAction = 148,
        
        [Description("Действие с департаментом пользователя")]
        DepartmentAccountUserAction = 149,
        
        /// <summary>
        /// Используется фронтом в поинте logs/log-event
        /// </summary>
        [Description("Отправлена заявка")]
        RequestSent = 150,
        
        [Description("Начинаю перевод файловой в серверную")]
        FileToClusterTransformationStart = 151,
        
        [Description("Перевод файловой в серверную успешно закончен")]
        FileToClusterTransformationSuccesses = 152,
        
    }
}
