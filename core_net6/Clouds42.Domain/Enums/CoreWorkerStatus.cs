﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус воркера
    /// </summary>
    public enum CoreWorkerStatus : short
    {
        /// <summary>
        /// Выключен
        /// </summary>
        OFF = 0,

        /// <summary>
        /// Включен
        /// </summary>
        ON = 1
    }
}
