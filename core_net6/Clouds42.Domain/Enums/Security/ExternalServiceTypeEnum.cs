﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.Security
{
    public enum ExternalServiceTypeEnum
    {
        [Description("Не определен")]
        Undefined = 0,
        
        [Description("Платежный агрегатор ЮKassa")]
        YookassaAggregator = 1,

        [Description(nameof(Corp))]
        Corp = 2,

        [Description("ADL WindowsClient")]
        Adl,

        [Description(nameof(Link42))]
        Link42,

        [Description("ESDLClient")]
        Esdl,

        [Description(nameof(Promo))]
        Promo
    }
}
