﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.StateMachine
{
    /// <summary>
    /// Статус компоменты конечного автомата.
    /// </summary>
    public enum StateMachineComponentStatus
    {
        /// <summary>
        /// В процессе работы
        /// </summary>
        [Description("В процессе работы.")]
        Processing = 0,

        /// <summary>
        /// Завершен с ошибкой
        /// </summary>
        [Description("Завершен с ошибкой.")]
        Error = 1,

        /// <summary>
        /// Завершен
        /// </summary>
        [Description("Завершен.")]
        Finish = 2,

        /// <summary>
        /// В процоцессе откатывания
        /// </summary>
        [Description("В процоцессе откатывания.")]
        RollingBack = 3,

        /// <summary>
        /// Завершено откатывание
        /// </summary>
        [Description("Завершено откатывание.")]
        RolledBack = 4
    }
}