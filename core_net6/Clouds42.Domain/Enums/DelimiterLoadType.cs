﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип создания базы на разделителях
    /// </summary>
    public enum DelimiterLoadType 
    {
        /// <summary>
        /// Пользователь создал новую
        /// </summary>
        [Description("Пользователь создал новую")]
        UserCreateNew = 1,

        /// <summary>
        /// Пользователь загрузил архив
        /// </summary>
        [Description("Пользователь загрузил архив")]
        UserUploadZip= 2,

        /// <summary>
        /// Восстановленная из архива
        /// </summary>
        [Description("Восстановленная из архива")]
        Restored = 3
    }
}
