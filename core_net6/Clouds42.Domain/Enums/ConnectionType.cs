﻿using System.ComponentModel;
using Clouds42.Domain.Attributes;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип RDP подключения
    /// </summary>
    public enum ConnectionType
    {
        /// <summary>
        /// Нет подключения
        /// </summary>
        [NotDrawInHtml]
        None,

        /// <summary>
        /// Удаленное приложение
        /// </summary>
        [Description("Удаленное приложение")]
        RemoteApp,

        /// <summary>
        /// Удаленный рабочий стол
        /// </summary>
        [Description("Удаленный рабочий стол")]
        Rdp,

        /// <summary>
        /// Веб клиент
        /// </summary>
        [Description("Веб клиент")]
        WebRdp
    }
}
