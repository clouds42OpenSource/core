﻿namespace Clouds42.Domain.Enums.DbTemplateUpdates
{
    /// <summary>
    /// Статус обновлённого файлового шаблона
    /// </summary>
    public enum UpdateDatabaseTemplateStateEnum
    {
        /// <summary>
        /// Отменённый файловый шаблон
        /// </summary>
        Skip = 0,

        /// <summary>
        /// Подтверждённый файловый шаблон
        /// </summary>
        Success = 1,

        /// <summary>
        /// Файловый шаблон с ошибкой
        /// </summary>
        Error = 2
    }
}
