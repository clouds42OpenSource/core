﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums.DbTemplateUpdates
{
    /// <summary>
    /// Статус для обновленного шаблона инф. базы
    /// </summary>
    public enum DbTemplateUpdateState
    {
        /// <summary>
        /// Новое
        /// </summary>
        [Description("Новое")]
        New = 0,

        /// <summary>
        /// Принятое
        /// </summary>
        [Description("Принятое")]
        Apllied = 1,

        /// <summary>
        /// Отклоненное
        /// </summary>
        [Description("Отклоненное")]
        Rejected = 2
    }
}
