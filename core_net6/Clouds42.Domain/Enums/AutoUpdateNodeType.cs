﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип ноды
    /// </summary>
    public enum AutoUpdateNodeType
    {
        Common = 0,
        Vip = 1,
        Htznr = 2,
    }
}
