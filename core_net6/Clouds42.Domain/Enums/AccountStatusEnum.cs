﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус аккаунта
    /// </summary>
    public enum AccountStatusEnum
    {
        /// <summary>
        /// Добавлен
        /// </summary>
        Added,

        /// <summary>
        /// Удален
        /// </summary>
        Deleted
    }
}
