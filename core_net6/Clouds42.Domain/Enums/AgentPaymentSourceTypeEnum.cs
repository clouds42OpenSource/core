﻿using System.ComponentModel;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип источника агентского платежа
    /// </summary>
    public enum AgentPaymentSourceTypeEnum
    {
        /// <summary>
        /// Заявка на вывод средств агента
        /// </summary>
        [Description("Заявка на вывод средств агента")]
        AgentCashOutRequest = 0,

        /// <summary>
        /// Агентское вознаграждение
        /// </summary>
        [Description("Агентское вознаграждение")]
        AgentReward = 1,

        /// <summary>
        /// Ручной ввод
        /// </summary>
        [Description("Ручной ввод")]
        ManualInput = 2,

        /// <summary>
        /// Заявка на перевод баланса агента 
        /// </summary>
        [Description("Заявка на перевод баланса агента")]
        AgentTransferBalanseRequest = 3
    }
}
