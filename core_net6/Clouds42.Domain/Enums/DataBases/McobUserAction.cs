﻿namespace Clouds42.Domain.Enums.DataBases 
{
    /// <summary>
    /// Тип действия в обработке от МЦОБ
    /// </summary>
    public enum McobUserAction
    {
        /// <summary>
        /// Добавить пользователя
        /// </summary>
        AddUser,
        
        /// <summary>
        /// Удалить роли пользователя
        /// </summary>
        RemoveUserRoles,

        /// <summary>
        /// Удалить все роли
        /// </summary>
        RemoveAllRoles
    }
}