﻿namespace Clouds42.Domain.Enums.DataBases
{
    /// <summary>
    /// Тип действия в списке управления доступом (ACL)
    /// </summary>
    public enum AclAction
    {
        /// <summary>
        /// Установить доступ
        /// </summary>
        SetAccessControl,

        /// <summary>
        /// Удалить доступ
        /// </summary>
        RemoveAccessRule
    }
}
