﻿namespace Clouds42.Domain.Enums.DataBases
{
    /// <summary>
    /// Статус публикации инф. базы
    /// </summary>
    public enum PublishState
    {
        /// <summary>
        /// База опубликована
        /// </summary>
        Published,

        /// <summary>
        /// База в процессе публикации 
        /// </summary>
        PendingPublication,

        /// <summary>
        /// База в процессе снятия с публикации
        /// </summary>
        PendingUnpublication,

        /// <summary>
        /// База не опубликована
        /// </summary>
        Unpublished,

        /// <summary>
        /// База в процессе перезапуска пула
        /// </summary>
        RestartingPool
    }
}
