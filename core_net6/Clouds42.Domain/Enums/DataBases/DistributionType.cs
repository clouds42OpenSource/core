﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.Enums.DataBases
{
    /// <summary>
    /// Тип распространения
    /// </summary>
    public enum DistributionType
    {
        /// <summary>
        /// Стабильная версия
        /// </summary>
        [Display(Name = "Стабильная")] [Description("Стабильная версия")]
        Stable = 0,

        /// <summary>
        /// Альфа версия
        /// </summary>
        [Display(Name = "Альфа")] [Description("Альфа версия")]
        Alpha = 1
    }
}
