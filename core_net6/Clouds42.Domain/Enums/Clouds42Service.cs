﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Constants;

namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Тип системного сервиса
    /// </summary>
    public enum Clouds42Service
    {
        /// <summary>
        /// Неизвестно
        /// </summary>
        [Display(Name = "Ваш Сервис")]
        Unknown = 0,

        /// <summary>
        /// Мой Диск
        /// </summary>
        [Display(Name = NomenclaturesNameConstants.MyDisk)]
        MyDisk = 1,

        /// <summary>
        /// Аренда 1С
        /// </summary>
        [Display(Name = "Аренда 1С")]
        MyEnterprise = 2,

        /// <summary>
        /// Распознавание документов
        /// </summary>
        [Display(Name = NomenclaturesNameConstants.Recognition)]
        Recognition = 3,

        /// <summary>
        /// Fasta
        /// </summary>
        [Display(Name = NomenclaturesNameConstants.Esdl)]
        Esdl = 4,

        /// <summary>
        /// Мои информационные базы
        /// </summary>
        [Display(Name = NomenclaturesNameConstants.MyDatabases)] [Description("Мои информационные базы")]
        MyDatabases = 5
    }
}
