﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Сервис маркет42
    /// </summary>
    public enum MarketServiceEnum
    {
        /// <summary>
        /// Штрихкод-информер
        /// </summary>
        InformerBarcode = 0,

        /// <summary>
        /// Контроль ввода данных в 1С
        /// </summary>
        DataControl = 1,

        /// <summary>
        /// 30 служб доставки
        /// </summary>
        Delivery = 2,

        /// <summary>
        /// Чат-боты
        /// </summary>
        ChatBots = 3
    }
}
