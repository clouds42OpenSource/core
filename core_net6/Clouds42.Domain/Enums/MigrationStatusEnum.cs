﻿namespace Clouds42.Domain.Enums
{
    /// <summary>
    /// Статус миграции аккаунта
    /// </summary>
    public enum MigrationStatusEnum : byte
    {
        /// <summary>
        /// Успешно
        /// </summary>
        Success = 1,

        /// <summary>
        /// Ошибка
        /// </summary>
        Error = 2
    }
}