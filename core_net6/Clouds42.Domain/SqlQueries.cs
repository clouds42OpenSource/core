﻿using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain
{
    public static class SqlQueries
    {
        public static class DatabaseCore
        {
            public static string ConnectionStringToSql1CFormat = "data source={0};initial catalog=master; Integrated security=True; MultipleActiveResultSets=True;";

            public static string SetRecovery = "ALTER DATABASE [{0}] SET RECOVERY {1} WITH NO_WAIT";

            public static string GetRecovery = "SELECT [recovery_model_desc] FROM SYS.DATABASES WHERE [name] = '{0}'";

            public static string CheckDatabaseExists = """
                                                        
                                                                            IF db_id('{0}') IS NOT NULL
                                                                                BEGIN 
                                                                                    PRINT 'success'
                                                                                END 
                                                                            ELSE 
                                                                                BEGIN
                                                                                    RAISERROR ('База {0} не найдена на сервере.',  16,  1); 
                                                                                END
                                                        """;

            public static string GetDatabaseSizeInMb = """
                                                        
                                                                            SELECT
                                                                            total_size_mb = CAST(SUM(size) * 8. / 1024 AS INT) 
                                                                            FROM sys.master_files WITH(NOWAIT) 
                                                                            WHERE database_id = DB_ID('{0}')
                                                        """;
            
            public static string DropDatabase = "IF db_id('{0}') IS NOT NULL DROP DATABASE {0}";

            public static string BackupDatabase = "BACKUP DATABASE [{0}] TO DISK='{1}' WITH COPY_ONLY, NAME='{0}'";

            public static string RestoreDatabase = "RESTORE DATABASE [{0}] FROM DISK='{1}' WITH REPLACE";
            
            public static string KillDatabaseSession = """
                                                        
                                                                            DECLARE @dbname sysname
                                                                            SET @dbname = '{0}'
                                                                            DECLARE @spid int
                                                                            SELECT @spid = min(spid) FROM master.dbo.sysprocesses
                                                                            WHERE dbid = db_id(@dbname)
                                                                            WHILE @spid IS NOT NULL
                                                                            Begin
                                                                            Execute ('Kill ' + @spid)
                                                                                    SELECT @spid = min(spid) FROM master.dbo.sysprocesses
                                                                                    WHERE dbid = db_id(@dbname) AND spid > @spid
                                                                            End
                                                        """;
            
            public static string ExtendedRestoreDatabase = """
                                                            
                                                                                USE tempdb;
                                                            
                                                                                DECLARE @NewDatabase SYSNAME = '{0}';
                                                                                DECLARE @TemplateBackups SYSNAME = '{1}';
                                                                                DECLARE @TemplateDatabase SYSNAME = '{2}';
                                                            
                                                                                DECLARE @TemplateDatabaseLog SYSNAME = @TemplateDatabase + '_log';
                                                            
                                                                                -- Get the backup file list as a table variable
                                                                                CREATE TABLE #BackupFiles
                                                                                (LogicalName nvarchar(128),
                                                                                PhysicalName nvarchar(260),
                                                                                [Type] char(1),
                                                                                FileGroupName nvarchar(128) NULL,
                                                                                Size numeric(20,0),
                                                                                MaxSize numeric(20,0),
                                                                                FileId bigint,
                                                                                CreateLSN numeric(25,0),
                                                                                DropLSN numeric(25,0) NULL,
                                                                                UniqueID uniqueidentifier,
                                                                                ReadOnlyLSN numeric(25,0) NULL,
                                                                                ReadWriteLSN numeric(25,0) NULL,
                                                                                BackupSizeInBytes bigint,
                                                                                SourceBlockSize int,
                                                                                FileGroupId int,
                                                                                LogGroupGUID  uniqueidentifier NULL,
                                                                                DifferentialBaseLSN numeric(25,0) NULL,
                                                                                DifferentialBaseGUID  uniqueidentifier NULL,
                                                                                IsReadOnly bit,
                                                                                IsPresent bit);
                                                                                IF cast(cast(SERVERPROPERTY('ProductVersion') as char(4)) as float) > 9 -- Greater than SQL 2005 
                                                                                BEGIN
                                                                                ALTER TABLE #BackupFiles ADD TDEThumbprint  varbinary(32) NULL
                                                                                END
                                                                                IF cast(cast(SERVERPROPERTY('ProductVersion') as char(2)) as float) > 12 -- Greater than 2014
                                                                                BEGIN
                                                                                ALTER TABLE #BackupFiles ADD SnapshotURL    nvarchar(360) NULL
                                                                                END
                                                                                INSERT INTO #BackupFiles EXEC('RESTORE FILELISTONLY FROM DISK = ''' + @TemplateBackups + '''');
                                                            
                                                                                -- Create  the backup file list as a table variable
                                                                                DECLARE @NewDatabaseData VARCHAR(MAX);
                                                                                DECLARE @NewDatabaseLog VARCHAR(MAX);
                                                            					DECLARE @InstanceDefaultDataPath VARCHAR(MAX);
                                                            					DECLARE @InstanceDefaultLogPath VARCHAR(MAX);
                                                            
                                                            					SELECT @TemplateDatabase = LogicalName FROM #BackupFiles WHERE Type = 'D';
                                                                                SELECT @TemplateDatabaseLog = LogicalName FROM #BackupFiles WHERE Type = 'L';
                                                            
                                                                                SELECT 
                                                                  				@InstanceDefaultDataPath = cast(SERVERPROPERTY('InstanceDefaultDataPath') as VARCHAR(MAX)),
                                                                                @InstanceDefaultLogPath = cast(SERVERPROPERTY('InstanceDefaultLogPath') as VARCHAR(MAX)) 
                                                            
                                                                                SET @NewDatabaseData = CONCAT(@InstanceDefaultDataPath, @NewDatabase, '.mdf');
                                                                                SET @NewDatabaseLog = CONCAT(@InstanceDefaultLogPath, @NewDatabase, '_log.ldf');
                                                            
                                                                                RESTORE DATABASE @NewDatabase FROM DISK = @TemplateBackups WITH RECOVERY, REPLACE, STATS = 100,
                                                                                   MOVE @TemplateDatabase TO @NewDatabaseData,
                                                                                   MOVE @TemplateDatabaseLog TO @NewDatabaseLog;
                                                            
                                                                                -- Change Logical File Name
                                                                                DECLARE @SQL_SCRIPT VARCHAR(MAX)='
                                                                                    ALTER DATABASE [{{NewDatabase}}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
                                                                                    ALTER DATABASE [{{NewDatabase}}] MODIFY FILE (NAME=N''{{TemplateDatabase}}'', NEWNAME=N''{{NewDatabase}}'');
                                                                                    ALTER DATABASE [{{NewDatabase}}] MODIFY FILE (NAME=N''{{TemplateDatabase}}_log'', NEWNAME=N''{{NewDatabase}}_log'');
                                                                                    ALTER DATABASE [{{NewDatabase}}] SET MULTI_USER WITH ROLLBACK IMMEDIATE;
                                                                                    SELECT name AS logical_name, physical_name FROM SYS.MASTER_FILES WHERE database_id = DB_ID(N''{{NewDatabase}}'');
                                                                                ';
                                                                                SET @SQL_SCRIPT = REPLACE(@SQL_SCRIPT, '{{TemplateDatabase}}', @TemplateDatabase);
                                                                                SET @SQL_SCRIPT = REPLACE(@SQL_SCRIPT, '{{NewDatabase}}', @NewDatabase);
                                                                                DROP TABLE #BackupFiles
                                                                                EXECUTE (@SQL_SCRIPT);
                                                                                
                                                            """;
        }

        public static class Partners
        {
            public static string GetMonthlyCharge = """
                                                     SELECT (
                                                         (
                                                             SELECT 
                                                                 COALESCE(SUM(
                                                                     CASE
                                                                         WHEN ssf."SystemService" = 2 AND ssf."IsVipAccount" = 0 THEN ssf."Cost" * {0}
                                                                         WHEN ssf."SystemService" = 2 AND ssf."IsVipAccount" = 1 THEN ssf."Cost" * {1}
                                                                         WHEN ssf."SystemService" = 1 AND ssf."IsVipAccount" = 1 THEN ssf."Cost" * {3}
                                                                         ELSE ssf."Cost" * {2}
                                                                     END
                                                                 ), 0) 
                                                             FROM (
                                                                 SELECT DISTINCT 
                                                                     resConf."Id", 
                                                                     resConf."Cost", 
                                                                     ser."SystemService",
                                                                     CASE WHEN accConf."IsVip" = false THEN 0 ELSE 1 END AS "IsVipAccount"
                                                                 FROM public."Accounts" a
                                                                 INNER JOIN public."AccountConfigurations" accConf ON accConf."AccountId" = a."Id"
                                                                 INNER JOIN billing."BillingAccounts" ba ON ba."Id" = a."Id"
                                                                 INNER JOIN billing."ResourcesConfigurations" resConf ON resConf."AccountId" = ba."Id"
                                                                 INNER JOIN billing."Services" ser ON ser."Id" = resConf."BillingServiceId"
                                                                 LEFT JOIN billing."ServiceTypes" st ON st."ServiceId" = ser."Id"
                                                                 LEFT JOIN billing."ServiceTypes" depSt ON depSt."Id" = st."DependServiceTypeId"
                                                                 LEFT JOIN billing."ResourcesConfigurations" depResConf ON depResConf."AccountId" = a."Id" 
                                                                     AND depResConf."BillingServiceId" = depSt."ServiceId"
                                                                 WHERE 
                                                                     a."ReferrerId" = '{5}' 
                                                                     AND (ser."SystemService" = 2 OR ser."SystemService" = 1)
                                                                     AND (
                                                                         (depResConf."Frozen" IS NOT NULL AND depResConf."Frozen" = false AND depResConf."IsDemoPeriod" = false) 
                                                                         OR (resConf."Frozen" IS NOT NULL AND resConf."Frozen" = false AND resConf."IsDemoPeriod" = false)
                                                                     )
                                                             ) AS ssf
                                                         ) +
                                                         COALESCE((
                                                             SELECT SUM(tServiceCost."ServiceCost")
                                                             FROM billing."Services" AS sr
                                                             LEFT JOIN (
                                                                 SELECT 
                                                                     pp."BillingServiceId" AS "ServiceId", 
                                                                     SUM(pp."Cost") AS "ServiceCost"
                                                                 FROM (
                                                                     SELECT DISTINCT 
                                                                         resConf."BillingServiceId", 
                                                                         resConf."Id",
                                                                         COALESCE(resConf."Cost" * {4}, 0) AS "Cost"
                                                                     FROM billing."ResourcesConfigurations" AS resConf
                                                                     INNER JOIN billing."ServiceTypes" st ON st."ServiceId" = resConf."BillingServiceId"
                                                                     INNER JOIN billing."ServiceTypes" dst ON st."DependServiceTypeId" = dst."Id"
                                                                     INNER JOIN billing."ResourcesConfigurations" drc ON drc."BillingServiceId" = dst."ServiceId" 
                                                                         AND drc."AccountId" = resConf."AccountId"
                                                                     INNER JOIN public."AccountConfigurations" accConf ON accConf."AccountId" = resConf."AccountId"
                                                                     WHERE 
                                                                         resConf."IsDemoPeriod" = false
                                                                         AND drc."Frozen" = false 
                                                                         AND drc."IsDemoPeriod" = false
                                                                 ) pp
                                                                 GROUP BY pp."BillingServiceId"
                                                             ) AS tServiceCost ON tServiceCost."ServiceId" = sr."Id"
                                                             WHERE sr."AccountOwnerId" = '{5}'
                                                         ), 0)
                                                     ) AS "MonthlyCharge";
                                                     """;

            public static string GetAccounts = """
                                                WITH "AccountUsersCTE" AS (
                                                    SELECT 
                                                        au."AccountId",
                                                        au."Login",
                                                        ROW_NUMBER() OVER (PARTITION BY au."AccountId" ORDER BY NULLIF(aur."AccountUserGroup", 2) ASC) AS "RowNumber"
                                                    FROM public."AccountUsers" au
                                                    INNER JOIN public."AccountUserRoles" aur ON aur."AccountUserId" = au."Id"
                                                    WHERE aur."AccountUserGroup" = 2
                                                      AND LOWER(au."Login") LIKE LOWER('%' || '{0}' || '%')
                                                ),
                                                "AccountUsersCTE2" AS (
                                                    SELECT 
                                                        au."AccountId",
                                                        au."Login",
                                                        ROW_NUMBER() OVER (PARTITION BY au."AccountId" ORDER BY NULLIF(aur."AccountUserGroup", 2) ASC) AS "RowNumber"
                                                    FROM public."AccountUsers" au
                                                    INNER JOIN public."AccountUserRoles" aur ON aur."AccountUserId" = au."Id"
                                                    WHERE aur."AccountUserGroup" = 2
                                                ),
                                                "AccountIDs" AS (
                                                    SELECT 
                                                        a."Id" AS "AccountId",
                                                        CONCAT(a."IndexNumber"::TEXT, ' (', a."Name", ' ', 
                                                            CASE 
                                                                WHEN aur."AccountId" IS NOT NULL THEN aur."Login" 
                                                                ELSE au."Login" 
                                                            END, ')') AS "AccountCaption",
                                                        a."IndexNumber" AS "AccountIndexNumber"
                                                    FROM public."Accounts" a
                                                    LEFT JOIN "AccountUsersCTE" aur ON a."Id" = aur."AccountId" AND aur."RowNumber" = 1
                                                    LEFT JOIN "AccountUsersCTE2" au ON a."Id" = au."AccountId" AND au."RowNumber" = 1
                                                    WHERE 
                                                        ((a."Status" IS NOT NULL AND a."Status" <> 'Deleted' AND a."Status" <> 'SyncDeleted') OR a."Status" IS NULL)
                                                        AND (
                                                            (a."Name" IS NOT NULL AND LOWER(a."Name") LIKE LOWER('%' || '{0}' || '%'))
                                                            OR (POSITION('{0}' IN a."IndexNumber"::TEXT) = 1)
                                                            OR (aur."AccountId" IS NOT NULL)
                                                        )
                                                )
                                                SELECT * 
                                                FROM "AccountIDs"
                                                LIMIT 20;
                                                """;
        }

        public static class ResourceConfiguration
        {
            public static string LockedSponsoredServiceConfigurations = """
                                                                          CREATE TEMP TABLE Services (
                                                                              "ResConfigId" UUID, 
                                                                              "AccountId" UUID, 
                                                                              "ServiceId" UUID, 
                                                                              "TotalCost" DECIMAL
                                                                          );
                                                                          
                                                                          INSERT INTO Services ("ResConfigId", "AccountId", "ServiceId", "TotalCost")
                                                                          SELECT 
                                                                              rc."Id", 
                                                                              rc."AccountId", 
                                                                              rc."BillingServiceId", 
                                                                              rc."Cost"
                                                                          FROM 
                                                                              billing."ResourcesConfigurations" rc
                                                                          INNER JOIN 
                                                                              billing."Services" ser ON ser."Id" = rc."BillingServiceId"
                                                                          INNER JOIN 
                                                                              (
                                                                                  SELECT 
                                                                                      st."ServiceId",
                                                                                      SUM(CASE WHEN st."DependServiceTypeId" IS NOT NULL THEN 1 ELSE 0 END) AS "CountDependServiceTypes"
                                                                                  FROM 
                                                                                      billing."ServiceTypes" st
                                                                                  GROUP BY 
                                                                                      st."ServiceId"
                                                                              ) AS tDependedServiceTypes ON tDependedServiceTypes."ServiceId" = ser."Id"
                                                                          WHERE 
                                                                              tDependedServiceTypes."CountDependServiceTypes" = 0 
                                                                              AND rc."Frozen" = true;
                                                                          
                                                                          WITH Dependencies AS (
                                                                              SELECT 
                                                                                  tDependedService."Id" AS "DependedServiceId",
                                                                                  rc."AccountId",
                                                                                  SUM(rc."Cost") AS "TotalSum"
                                                                              FROM 
                                                                                  billing."ResourcesConfigurations" rc
                                                                              INNER JOIN 
                                                                                  billing."Services" ser ON ser."Id" = rc."BillingServiceId"
                                                                              LEFT JOIN 
                                                                                  billing."ServiceTypes" st ON st."ServiceId" = ser."Id"
                                                                              LEFT JOIN 
                                                                                  billing."ServiceTypes" tDependedServiceType ON tDependedServiceType."Id" = st."DependServiceTypeId"
                                                                              LEFT JOIN 
                                                                                  billing."Services" tDependedService ON tDependedService."Id" = tDependedServiceType."ServiceId"
                                                                              WHERE 
                                                                                  rc."Cost" > 0 
                                                                                  AND rc."IsDemoPeriod" = false
                                                                              GROUP BY 
                                                                                  tDependedService."Id", rc."AccountId"
                                                                          )
                                                                          
                                                                          UPDATE Services s
                                                                          SET "TotalCost" = s."TotalCost" + d."TotalSum"
                                                                          FROM Dependencies d
                                                                          WHERE s."ServiceId" = d."DependedServiceId"
                                                                            AND s."AccountId" = d."AccountId";
                                                                          
                                                                          SELECT DISTINCT 
                                                                              rc.*
                                                                          FROM 
                                                                              Services s
                                                                          INNER JOIN 
                                                                              billing."ResourcesConfigurations" rc ON rc."Id" = s."ResConfigId"
                                                                          INNER JOIN 
                                                                              billing."BillingAccounts" ba ON ba."Id" = rc."AccountId"
                                                                          INNER JOIN 
                                                                              billing."Resources" res ON rc."AccountId" = res."AccountId"
                                                                          INNER JOIN 
                                                                              billing."ServiceTypes" st ON st."Id" = res."BillingServiceTypeId" AND st."ServiceId" = s."ServiceId"
                                                                          INNER JOIN 
                                                                              billing."Services" ser ON ser."Id" = st."ServiceId"
                                                                          LEFT JOIN 
                                                                              public."ServiceAccounts" serAc ON serAc."Id" = rc."AccountId"
                                                                          WHERE 
                                                                              (rc."Frozen" IS NULL OR rc."Frozen" = true)
                                                                              AND s."TotalCost" = 0 
                                                                              AND rc."IsDemoPeriod" = false 
                                                                              AND res."AccountSponsorId" IS NOT NULL 
                                                                              AND (ser."SystemService" IS NULL OR (ser."SystemService" <> 4 AND ser."SystemService" <> 6)) 
                                                                              AND (ba."PromisePaymentSum" IS NULL OR ba."PromisePaymentSum" = 0 OR ba."PromisePaymentDate" + INTERVAL '{0} days' > '{1}') 
                                                                              AND EXISTS (
                                                                                  SELECT 1
                                                                                  FROM billing."ResourcesConfigurations" sponsorResConf
                                                                                  WHERE sponsorResConf."AccountId" = res."AccountSponsorId"
                                                                                  AND sponsorResConf."BillingServiceId" = s."ServiceId"
                                                                                  AND (sponsorResConf."Frozen" IS NULL OR sponsorResConf."Frozen" = false)
                                                                              )
                                                                              AND serAc."Id" IS NULL;
                                                                          """;
        }

        public static class Reports
        {
            public static class Account
            {
                public static string GetData = """
                                                    WITH "AccountUsersCTE" AS (
                                                    SELECT 
                                                        "AccountId", 
                                                        ROW_NUMBER() OVER (PARTITION BY "AccountId" ORDER BY CASE WHEN "AccountUserGroup" = 2 THEN 1 ELSE 0 END) AS "RowNumber",
                                                        "Login", 
                                                        "Email"
                                                    FROM public."AccountUsers"
                                                    INNER JOIN public."AccountUserRoles" ON public."AccountUserRoles"."AccountUserId" = public."AccountUsers"."Id"
                                                ),
                                                "AccountSaleManagersCTE" AS (
                                                    SELECT 
                                                        asm."AccountId", 
                                                        ROW_NUMBER() OVER (PARTITION BY asm."AccountId" ORDER BY asm."AccountId" ASC) AS "RowNumber",
                                                        COALESCE(au."FullName", au."LastName" || ' ' || au."FirstName" || ' ' || au."MiddleName") AS "FullName",
                                                        "Division"
                                                    FROM public."AccountSaleManagers" asm
                                                    INNER JOIN public."AccountUsers" au ON au."Id" = asm."SaleManagerId"
                                                ),
                                                "UserCounts" AS (
                                                    SELECT 
                                                        "AccountId", 
                                                        COUNT(*) AS "countUser"
                                                    FROM public."AccountUsers"
                                                    WHERE ("CorpUserSyncStatus" IS NOT NULL AND "CorpUserSyncStatus" NOT IN ('Deleted', 'SyncDeleted')) OR "CorpUserSyncStatus" IS NULL
                                                    GROUP BY "AccountId"
                                                ),
                                                "DatabaseCounts" AS (
                                                    SELECT 
                                                        "OwnerId", 
                                                        COUNT(*) AS "countDb"
                                                    FROM public."AccountDatabases"
                                                    WHERE "State" = 'Ready'
                                                    GROUP BY "OwnerId"
                                                ),
                                                "ResourceConfigurations" AS (
                                                    SELECT 
                                                        "AccountId", 
                                                        MAX("ExpireDate") AS "ExpireDate"
                                                    FROM billing."ResourcesConfigurations"
                                                    INNER JOIN billing."Services" ON billing."Services"."Id" = billing."ResourcesConfigurations"."BillingServiceId"
                                                    WHERE billing."Services"."SystemService" = 2
                                                    GROUP BY "AccountId"
                                                ),
                                                "DatabaseSizes" AS (
                                                    SELECT 
                                                        "OwnerId", 
                                                        COALESCE(SUM("SizeInMB"), 0) AS "AccountDbSize"
                                                    FROM public."AccountDatabases"
                                                    WHERE "State" NOT IN ('DeletedToTomb', 'DelitingToTomb', 'DetachingToTomb', 'DeletedFromCloud')
                                                    GROUP BY "OwnerId"
                                                ),
                                                "RegularPayments" AS (
                                                    SELECT 
                                                        "AccountId", 
                                                        COALESCE(SUM("Cost"), 0) AS "RegularPayment"
                                                    FROM billing."ResourcesConfigurations"
                                                    INNER JOIN billing."Services" ON billing."Services"."Id" = billing."ResourcesConfigurations"."BillingServiceId"
                                                    WHERE ("SystemService" IS NULL OR ("SystemService" <> 3 AND "SystemService" <> 4))
                                                          AND "IsDemoPeriod" = false
                                                    GROUP BY "AccountId"
                                                ),
                                                "InflowPayments" AS (
                                                    SELECT 
                                                        "Account", 
                                                        COUNT(*) AS "CountInflowPayments"
                                                    FROM billing."Payments"
                                                    WHERE "OperationType" = 'Inflow' AND "Status" = 'Done' AND "Sum" > 0
                                                    GROUP BY "Account"
                                                ),
                                                "EsdlLicenseCounts" AS (
                                                    SELECT 
                                                        "AccountId", 
                                                        MAX("ScopeValue") AS "ScopeValue"
                                                    FROM billing."FlowResourcesScope"
                                                    INNER JOIN public."CSResources" ON public."CSResources"."Id" = billing."FlowResourcesScope"."FlowResourceId"
                                                    WHERE "CloudServiceId" = 'Recognition42' AND "ResourcesName" = 'LicenseCount'
                                                    GROUP BY "AccountId"
                                                )
                                                                                                
                                                SELECT 
                                                    a."IndexNumber" AS "AccountNumber", 
                                                    COALESCE(a."UserSource", 'Не указан') AS "UserSource",
                                                    CASE 
                                                        WHEN ra."Id" IS NULL THEN ac."Type"
                                                        WHEN ra."Id" IS NOT NULL AND (ra."Name" IS NULL OR ra."Name" = '') THEN 'company_' || ra."IndexNumber"
                                                        ELSE ra."Name"
                                                    END AS "AccountType",
                                                    l."Country" AS "Country",
                                                    ac."IsVip" AS "IsVip",
                                                    a."Name" AS "CompanyName",
                                                    auc."Login" AS "AccountAdminLogin",
                                                    auc."Email" AS "AccountAdminEmail",
                                                    a."RegistrationDate" AS "RegistrationDate",
                                                    COALESCE(uc."countUser", 0) AS "AccountUserCount",
                                                    COALESCE(dc."countDb", 0) AS "AccountDatabaseCount",
                                                    rc."ExpireDate" AS "ExpireDateRent1C",
                                                   CAST(COALESCE(CAST(ds."AccountDbSize" AS numeric) + CAST(COALESCE(dp."MyDiskFilesSizeInMb", 0) AS numeric), 0) / 1024 AS numeric) AS "UsedSizeOnDisk",
                                                   CAST(COALESCE(ba."Balance", 0) AS numeric) AS "Balance",
                                                   CAST(COALESCE(rp."RegularPayment", 0) AS numeric) AS "RegularPayment",
                                                    CASE 
                                                    WHEN COALESCE(ip."CountInflowPayments", 0) > 0 THEN TRUE
                                                    ELSE FALSE
                                                END AS "HasInflowPayment",
                                                    COALESCE(elc."ScopeValue", 0) AS "EsdlLicenseCount",
                                                    CASE 
                                                        WHEN ba."PromisePaymentDate" IS NOT NULL AND ba."PromisePaymentSum" > 0 THEN ba."PromisePaymentDate"
                                                        ELSE NULL
                                                    END AS "PromisePaymentDate", 
                                                    CASE 
                                                        WHEN ba."PromisePaymentDate" IS NOT NULL AND ba."PromisePaymentSum" > 0 THEN ba."PromisePaymentSum"
                                                        ELSE NULL
                                                    END AS "PromisePaymentSum",
                                                    asm."FullName" AS "SaleManagerFullName",
                                                    asm."Division" AS "SaleManagerDivision",
                                                   CAST(COALESCE(ba."BonusBalance", 0) AS numeric) AS "BonusBalance"
                                                FROM public."Accounts" a
                                                INNER JOIN public."AccountConfigurations" ac ON ac."AccountId" = a."Id"
                                                LEFT JOIN public."Accounts" ra ON ra."Id" = a."ReferrerId"
                                                LEFT JOIN public."Locale" l ON l."ID" = ac."LocaleId"
                                                LEFT JOIN "AccountUsersCTE" auc ON auc."AccountId" = a."Id" AND auc."RowNumber" = 1
                                                LEFT JOIN "AccountSaleManagersCTE" asm ON asm."AccountId" = a."Id" AND asm."RowNumber" = 1
                                                LEFT JOIN "UserCounts" uc ON uc."AccountId" = a."Id"
                                                LEFT JOIN "DatabaseCounts" dc ON dc."OwnerId" = a."Id"
                                                LEFT JOIN "ResourceConfigurations" rc ON rc."AccountId" = a."Id"
                                                LEFT JOIN "DatabaseSizes" ds ON ds."OwnerId" = a."Id"
                                                LEFT JOIN billing."BillingAccounts" ba ON ba."Id" = a."Id"
                                                LEFT JOIN "RegularPayments" rp ON rp."AccountId" = a."Id"
                                                LEFT JOIN "InflowPayments" ip ON ip."Account" = a."Id"
                                                LEFT JOIN "EsdlLicenseCounts" elc ON elc."AccountId" = a."Id"
                                                LEFT JOIN (
                                                    SELECT "AccountId", COALESCE(SUM("MyDiskFilesSizeInMb"), 0) AS "MyDiskFilesSizeInMb"
                                                    FROM public."MyDiskPropertiesByAccounts"
                                                    GROUP BY "AccountId"
                                                ) dp ON dp."AccountId" = a."Id"
                                                ORDER BY a."RegistrationDate" DESC;
                                                """;
            }

            public static class AccountUser
            {
                public static string GetData = $"""
                                                 WITH "AccountUsersCTE" AS (
                                                     SELECT 
                                                         au."Id" AS "AccountUserID",
                                                         a."IndexNumber" AS "AccountNumber",
                                                         au."Login",
                                                         COALESCE(au."Email", '') AS "Email",
                                                         au."PhoneNumber",
                                                         au."FullName",
                                                         au."CreationDate",
                                                         CASE 
                                                             WHEN au."Activated" = true THEN 'Активен' 
                                                             ELSE 'Заблокирован' 
                                                         END AS "Status",
                                                         0 AS "CountInternalAccessesToDatabases",
                                                         0 AS "CountExternalAccessesToDatabases",
                                                         NULL::TEXT AS "AccountUserRoles"
                                                     FROM public."AccountUsers" au
                                                     INNER JOIN public."Accounts" a ON a."Id" = au."AccountId"
                                                     WHERE au."CorpUserSyncStatus" <> 'Deleted'
                                                       AND au."CorpUserSyncStatus" <> 'SyncDeleted'
                                                       AND (au."Removed" IS NULL OR au."Removed" <> false)
                                                 ),
                                                 "InternalAccessCounts" AS (
                                                     SELECT 
                                                         acd."AccountUserID",
                                                         COUNT(*) AS "counts"
                                                     FROM public."AcDbAccesses" acd
                                                     INNER JOIN public."AccountUsers" au ON au."Id" = acd."AccountUserID"
                                                     INNER JOIN public."AccountDatabases" ad ON ad."Id" = acd."AccountDatabaseID"
                                                     WHERE ad."OwnerId" = au."AccountId"
                                                       AND ad."State" IN ({GetEnumerationOfActiveDatabaseStatusesString()})
                                                     GROUP BY acd."AccountUserID"
                                                 ),
                                                 "ExternalAccessCounts" AS (
                                                     SELECT 
                                                         acd."AccountUserID",
                                                         COUNT(*) AS "counts"
                                                     FROM public."AcDbAccesses" acd
                                                     INNER JOIN public."AccountUsers" au ON au."Id" = acd."AccountUserID"
                                                     INNER JOIN public."AccountDatabases" ad ON ad."Id" = acd."AccountDatabaseID"
                                                     WHERE ad."OwnerId" <> au."AccountId"
                                                       AND ad."State" IN ({GetEnumerationOfActiveDatabaseStatusesString()})
                                                     GROUP BY acd."AccountUserID"
                                                 ),
                                                 "UserRoles" AS (
                                                     SELECT 
                                                         aur."AccountUserId",
                                                         STRING_AGG(
                                                             CASE 
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.AccountUser} THEN '{AccountUserGroup.AccountUser.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.AccountAdmin} THEN '{AccountUserGroup.AccountAdmin.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.AccountSaleManager} THEN '{AccountUserGroup.AccountSaleManager.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.ProcOperator} THEN '{AccountUserGroup.ProcOperator.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.Hotline} THEN '{AccountUserGroup.Hotline.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.CloudAdmin} THEN '{AccountUserGroup.CloudAdmin.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.Cloud42Service} THEN '{AccountUserGroup.Cloud42Service.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.Anonymous} THEN '{AccountUserGroup.Anonymous.Description()}'
                                                                 WHEN aur."AccountUserGroup" = {(int)AccountUserGroup.CloudSE} THEN '{AccountUserGroup.CloudSE.Description()}'
                                                                 ELSE 'Не определен'
                                                             END, ', ' ORDER BY aur."AccountUserGroup"
                                                         ) AS "groups"
                                                     FROM public."AccountUserRoles" aur
                                                     GROUP BY aur."AccountUserId"
                                                 )
                                                 SELECT 
                                                     au."AccountNumber", 
                                                     au."Login", 
                                                     COALESCE(au."Email", '') AS "Email", 
                                                     au."PhoneNumber", 
                                                     au."FullName", 
                                                     au."CreationDate", 
                                                     au."Status", 
                                                     COALESCE(ia."counts", 0) AS "CountInternalAccessesToDatabases",
                                                     COALESCE(ea."counts", 0) AS "CountExternalAccessesToDatabases",
                                                     ur."groups" AS "AccountUserRoles"
                                                 FROM "AccountUsersCTE" au
                                                 LEFT JOIN "InternalAccessCounts" ia ON ia."AccountUserID" = au."AccountUserID"
                                                 LEFT JOIN "ExternalAccessCounts" ea ON ea."AccountUserID" = au."AccountUserID"
                                                 LEFT JOIN "UserRoles" ur ON ur."AccountUserId" = au."AccountUserID"
                                                 ORDER BY au."CreationDate" DESC;
                                                 """;

                private static string GetEnumerationOfActiveDatabaseStatusesString() =>
                    $"'{DatabaseState.Ready.ToString()}', '{DatabaseState.NewItem.ToString()}', " +
                    $"'{DatabaseState.ProcessingSupport.ToString()}', '{DatabaseState.TransferDb.ToString()}', " +
                    $"'{DatabaseState.RestoringFromTomb.ToString()}'";
            }
        }
    }
}
