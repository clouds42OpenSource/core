﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Доменая модель регистрации служб
    /// </summary>
    [Table("CloudServices")]
    public class CloudService
    {
        /// <summary>
        /// ID таблицы
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        /// <summary>
        /// ID сервиса
        /// </summary>
        [Key]
        
        [Required]
        public string CloudServiceId { get; set; }

        /// <summary>
        /// Описание сервиса
        /// </summary>
        
        public string ServiceCaption { get; set; }

        /// <summary>
        /// Токен аутентификации
        /// </summary>
        public Guid? ServiceToken { get; set; }

        /// <summary>
        /// JWT токен аутентификации
        /// </summary>
        public byte[] JsonWebToken { get; set; }

        /// <summary>
        /// Refresh-токен
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// С каким пользователем связана служба
        /// </summary>
        public Guid? AccountUserId { get; set; }

        public virtual AccountUser AccountUser { get; set; }
    }
}
