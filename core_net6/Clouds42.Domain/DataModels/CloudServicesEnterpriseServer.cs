﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    [Table(nameof(CloudServicesEnterpriseServer))]
    public class CloudServicesEnterpriseServer : ICloudServicesEnterpriseServer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }


        [Required] 
        public string ConnectionAddress { get; set; }

        public string Description { get; set; }

        [Required]
        public string Version { get; set; }

        [Required]
        public string Name { get; set; }

        /// <summary>
        ///     Имя администратора кластера
        /// </summary>

        public string AdminName { get; set; }

        /// <summary>
        ///     Пароль администратора в кластер
        /// </summary>

        public string AdminPassword { get; set; }

        /// <summary>
        /// Путь к настройкам кластера
        /// </summary>

        public string ClusterSettingsPath { get; set; }
    }
}
