using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table(nameof(CloudServicesSegmentStorage))]
    public class CloudServicesSegmentStorage
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }
        
        public bool IsDefault { get; set; }

        [ForeignKey(nameof(CloudServicesFileStorageServer))]
        public Guid FileStorageID { get; set; }
        public virtual CloudServicesFileStorageServer CloudServicesFileStorageServer { get; set; }

        [ForeignKey(nameof(CloudServicesSegment))]
        public Guid SegmentID { get; set; }
        public virtual CloudServicesSegment CloudServicesSegment { get; set; }
    }
}