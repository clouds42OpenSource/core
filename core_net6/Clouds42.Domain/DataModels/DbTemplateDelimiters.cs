﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Информация о базах на разделителях
    /// </summary>
    [Table(nameof(DbTemplateDelimiters))]
	public class DbTemplateDelimiters :  IDbTemplateDelimiters
	{
		/// <summary>
		/// Код конфигурации
		/// </summary>
        
        [Key]
        public string ConfigurationId { get; set; }

		/// <summary>
		/// Id шаблона
		/// </summary>
		[ForeignKey(nameof(Template))]
		public Guid TemplateId { get; set; }
		public virtual DbTemplate Template { get; set; }

		/// <summary>
		/// Название конфигурации 1С
		/// </summary>
		
		public string Name { get; set; }

	    /// <summary>
	    /// Название конфигурации в файле DumpInfo
	    /// </summary>
	    
	    public string ShortName { get; set; }

	    /// <summary>
	    /// Адрес публикации демо базы на разделителях
	    /// </summary>
	    public string DemoDatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Версия релиза конфигурации
        /// </summary>
        
        [Required]
        public string ConfigurationReleaseVersion { get; set; }

        /// <summary>
        /// минимальная версия релиза конфигурации
        /// </summary>
        
        [Required]
        public string MinReleaseVersion { get; set; }

	    /// <summary>
	    /// Материнские базы разделителей
	    /// </summary>
	    public virtual ICollection<DelimiterSourceAccountDatabase> DelimiterSourceAccountDatabases { get; set; }
	    public virtual ICollection<Service1CFileDbTemplateRelation> Service1CFileDbTemplateRelations { get; set; }
        public virtual ICollection<AccountDatabaseOnDelimiters> AccountDatabaseOnDelimiters { get; set; }
    }
}
