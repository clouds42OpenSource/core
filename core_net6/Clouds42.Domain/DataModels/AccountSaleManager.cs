﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Менеджер прикрепленный к аккаунту
    /// </summary>
    [Table("AccountSaleManagers")]
    public class AccountSaleManager : IAccountSaleManager
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Аккаунт
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Id менеджера прикрепленного
        /// к аккаунту
        /// </summary>
        [ForeignKey(nameof(AccountUser))]
        public Guid SaleManagerId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual AccountUser AccountUser { get; set; }

        /// <summary>
        /// Отдел менеджера
        /// </summary>
        [Required]
        public string Division { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; } = DateTime.Now;
    }
}
