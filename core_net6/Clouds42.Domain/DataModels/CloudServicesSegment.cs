﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.AccountProperties;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Сегмент облака
    /// </summary>
    [Table(nameof(CloudServicesSegment))]
    public class CloudServicesSegment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        [ForeignKey(nameof(CloudServicesGatewayTerminal))]
        public Guid? GatewayTerminalsID { get; set; }
        public virtual CloudServicesGatewayTerminal CloudServicesGatewayTerminal { get; set; }

        [ForeignKey(nameof(CloudServicesTerminalFarm))]
        public Guid ServicesTerminalFarmID { get; set; }
        public virtual CloudServicesTerminalFarm CloudServicesTerminalFarm { get; set; }

        [ForeignKey(nameof(CloudServicesContentServer))]
        public Guid ContentServerID { get; set; }
        public virtual CloudServicesContentServer CloudServicesContentServer { get; set; }

        [ForeignKey(nameof(CoreHosting))]
        public Guid CoreHostingId { get; set; }
        public virtual CoreHosting CoreHosting { get; set; }

        [ForeignKey(nameof(CloudServicesSQLServer))]
        public Guid SQLServerID { get; set; }
        public virtual CloudServicesSqlServer CloudServicesSQLServer { get; set; }

        [ForeignKey(nameof(CloudServicesBackupStorage))]
        public Guid BackupStorageID { get; set; }
        public virtual CloudServicesBackupStorage CloudServicesBackupStorage { get; set; }
        public bool IsDefault { get; set; }
        public string Description { get; set; }
        [Required] public string Name { get; set; }

        [ForeignKey(nameof(CloudServicesFileStorageServer))]
        public Guid FileStorageServersID { get; set; }
        public virtual CloudServicesFileStorageServer CloudServicesFileStorageServer { get; set; }
        public string WindowsAccountsPath { get; set; }
        public string CustomFileStoragePath { get; set; }

        [ForeignKey(nameof(Stable82PlatformVersionReference))]
        public string Stable82Version { get; set; }
        public virtual PlatformVersionReference Stable82PlatformVersionReference { get; set; }


        [ForeignKey(nameof(Alpha83PlatformVersionReference))]
        public string Alpha83Version { get; set; }
        public virtual PlatformVersionReference Alpha83PlatformVersionReference { get; set; }


        [ForeignKey(nameof(Stable83PlatformVersionReference))]
        public string Stable83Version { get; set; }
        public virtual PlatformVersionReference Stable83PlatformVersionReference { get; set; }


        [ForeignKey(nameof(CloudServicesEnterpriseServer82))]
        public Guid EnterpriseServer82ID { get; set; }
        public virtual CloudServicesEnterpriseServer CloudServicesEnterpriseServer82 { get; set; }


        [ForeignKey(nameof(CloudServicesEnterpriseServer83))]
        public Guid EnterpriseServer83ID { get; set; }
        public virtual CloudServicesEnterpriseServer CloudServicesEnterpriseServer83 { get; set; }

        [ForeignKey(nameof(AutoUpdateNode))]
        public Guid? AutoUpdateNodeId { get; set; }
        public virtual AutoUpdateNode AutoUpdateNode { get; set; }

        public virtual ICollection<CloudServicesSegmentStorage> CloudServicesSegmentStorages { get; set; }
        public virtual ICollection<CloudServicesSegmentTerminalServer> CloudServicesSegmentTerminalServers { get; set; }
        public virtual ICollection<AccountConfiguration> AccountConfigurations { get; set; }

        /// <summary>
        /// База на разделителях должна использовать вэб сервис
        /// </summary>
        [DefaultValue(false)]
        public bool DelimiterDatabaseMustUseWebService { get; set; }

        [NotMapped]
        public string ClientFileFolder
        {
            get
            {
                var fileStorageFolder = "filestorage";
                if (!string.IsNullOrEmpty(CustomFileStoragePath))
                    fileStorageFolder = CustomFileStoragePath;

                return fileStorageFolder;
            }
        }

        public bool NotMountDiskR { get; set; }
    }
}
