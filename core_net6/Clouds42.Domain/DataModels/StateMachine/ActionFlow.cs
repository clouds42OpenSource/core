﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.Domain.DataModels.StateMachine
{

    [Table("ActionFlows", Schema = SchemaType.StateMachine)]
    public class ActionFlow
    {

        /// <summary>
        /// Идентификатор действия.
        /// </summary>
        [Required]
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Идентифиактор процесса в котором выполняется действие.
        /// </summary>
        [Required]
        [ForeignKey(nameof(ProcessFlow))]
        public Guid ProcessFlowId { get; set; }
        public virtual ProcessFlow ProcessFlow { get; set; }

        /// <summary>
        /// Статус действия.
        /// </summary>
        [Required]
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// Входящая модель исполнения действия в JSON формате.
        /// </summary>
        public string InputDataInJson { get; set; }

        /// <summary>
        /// Снэпшот в JSON формате.
        /// </summary>
        public string SnapshotRollbackDataInJson { get; set; }

        /// <summary>
        /// Результат выполения действия в JSON формате.
        /// </summary>
        public string ResultDataInJson { get; set; }

        /// <summary>
        /// Комментарий среды исполнения.
        /// </summary>
        
        public string Comment { get; set; }

        /// <summary>
        /// Дата создания действия.
        /// </summary>
        [Required]
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Количество попыток выполнения.
        /// </summary>
        [Required]
        public int CountOfAttempts { get; set; }

        /// <summary>
        /// Составной ключ конфигурации действия.
        /// </summary>
        public string StateRefKey { get; set; }

        /// <summary>
        /// Ошибка выполнения действия.
        /// </summary>
        
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Стей ошибки выполнения действия.
        /// </summary>
        public string ErrorStackTrace { get; set; }

        /// <summary>
        /// Ошибка выполнения Rollback.
        /// </summary>
        
        public string RollbackErrorMessage { get; set; }

        /// <summary>
        /// Стэк ошибки выполнения Rollback.
        /// </summary>
        public string RollbackErrorStackTrace { get; set; }

    }
}