﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.Domain.DataModels.StateMachine
{

    [Table("ProcessFlows", Schema = SchemaType.StateMachine)]
    public class ProcessFlow
    {

        /// <summary>
        /// Идентификатор процесса.
        /// </summary>        
        [Required]
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания процесса.
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Ключ процесса.
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// Имя процесса.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Статус процесса.
        /// </summary>
        [Required]
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// Имя обрабатываемого процессом объекта.
        /// </summary>
        
        public string ProcessedObjectName { get; set; }

        /// <summary>
        /// Состояние процесса.
        /// </summary>        
        public string State { get; set; }

        /// <summary>
        /// Комментарий среды исполнения.
        /// </summary>        
        public string Comment { get; set; }

        public virtual ICollection<ActionFlow> ActionFlows { get; set; }

    }
}
