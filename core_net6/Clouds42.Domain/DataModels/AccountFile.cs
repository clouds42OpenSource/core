﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    public class AccountFile : BaseDomainModel
    {
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        [ForeignKey(nameof(CloudFile))]
        public Guid CloudFileId { get; set; }
        public virtual Account Account { get; set; }
        public virtual CloudFile CloudFile { get; set; }
        public VerificationStatus Status { get; set; }
        public DocumentType Type { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public enum VerificationStatus
    {
        [Display(Name = "На проверке")]
        UnderReview = 0,

        [Display(Name = "Проверено")]
        Verified = 1,

        [Display(Name = "Аннулировано")]
        Cancelled = 2
    }

    public enum DocumentType
    {
        [Display(Name = "Свидетельство о постановке на учет в налоговый орган")]
        TaxAuthorityRegistrationCertificate = 0,
    }
}
