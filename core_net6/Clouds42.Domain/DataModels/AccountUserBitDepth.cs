﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels;

public class AccountUserBitDepth : BaseDomainModel
{

    public ApplicationBitDepthEnum BitDepth { get; set; }

    public Guid AccountUserId { get; set; }
    
    public virtual AccountUser AccountUser { get; set; }

    public Guid AccountDatabaseId { get; set; }

    public virtual AccountDatabase AccountDatabase { get; set; }
}
