﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table(nameof(CloudServicesContentServer))]
    public class CloudServicesContentServer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        [Required] public string PublishSiteName { get; set; }
        public string Description { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool GroupByAccount { get; set; }
        public virtual ICollection<CloudServicesSegment> CloudServicesSegments { get; set; }
    }
}
