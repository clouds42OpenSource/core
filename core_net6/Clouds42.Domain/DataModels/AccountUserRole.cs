using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Access;

namespace Clouds42.Domain.DataModels
{
    public class AccountUserRole : BaseDomainModel
    {
        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }

        public AccountUserGroup AccountUserGroup { get; set; }
    }
}