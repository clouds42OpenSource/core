using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    ///     Электронные почты аккаунта
    /// </summary>
    public class AccountEmail : BaseDomainModel
    {
        /// <summary>
        ///     Электронная почта
        /// </summary>
        
        [Required]
        public string Email { get; set; }

        /// <summary>
        ///     Аккаунт
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
