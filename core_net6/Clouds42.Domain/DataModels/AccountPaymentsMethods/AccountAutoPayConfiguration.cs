﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountPaymentsMethods
{
    /// <summary>
    /// Способ оплат для автоплатежа
    /// </summary>
    [Table("AccountAutoPayConfigurations", Schema = SchemaType.Billing)]
    public class AccountAutoPayConfiguration
    {
        /// <summary>
        /// Id аккаунта пользователя и записи
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Аккаунт
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Id аккаунта пользователя
        /// </summary>
        [ForeignKey(nameof(SavedPaymentMethod))]
        public Guid SavedPaymentMethodId { get; set; }

        /// <summary>
        /// Сохраненный способ оплаты
        /// </summary>
        public virtual SavedPaymentMethod SavedPaymentMethod { get; set; }
    }
}
