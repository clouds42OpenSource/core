﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountPaymentsMethods
{
    /// <summary>
    /// Сохраненный платежный способ пользователя
    /// </summary>
    [Table("SavedPaymentMethods", Schema = SchemaType.Billing)]
    public class SavedPaymentMethod
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Id аккаунта пользователя
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Аккаунт
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Платежная система
        /// </summary>
        
        [Required]
        public string PaymentSystem { get; set; }

        /// <summary>
        /// Токен аггрегатора платежа
        /// </summary>
        
        [Required]
        public string Token { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        
        public string Title { get; set; }

        /// <summary>
        /// Тип способа платежа
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Подтип способа платежа
        /// </summary> 
        public string SubType { get; set; }

        /// <summary>
        /// Дата сохранения
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// Метаданные ответа аггрегатора платежа
        /// </summary>
        
        public string Metadata { get; set; }

        /// <summary>
        /// Информация о карте
        /// </summary>
        public virtual SavedPaymentMethodBankCard BankCard { get; set; }
    }
}
