﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountPaymentsMethods
{
    /// <summary>
    /// Сохраненные банковские карты пользователей
    /// </summary>
    [Table("SavedPaymentMethodBankCards", Schema = SchemaType.Billing)]
    public class SavedPaymentMethodBankCard
    {
        /// <summary>
        /// Идентификатор способа оплаты и Id записи
        /// </summary>
        [Key]
        [ForeignKey(nameof(SavedPaymentMethod))]
        public Guid SavedPaymentMethodId { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        public virtual SavedPaymentMethod SavedPaymentMethod { get; set; }

        /// <summary>
        /// Первые 6 цифр карты
        /// </summary>
        [Required]
        
        public string FirstSixDigits { get; set; }

        /// <summary>
        /// Последние 4 цифры карты
        /// </summary>
        [Required]
        
        public string LastFourDigits { get; set; }

        /// <summary>
        /// Месяц окончания срока сгорания
        /// </summary>
        [Required]
        
        public string ExpiryMonth { get; set; }

        /// <summary>
        /// Год окончания срока сгорания
        /// </summary>
        [Required]
        
        public string ExpiryYear { get; set; }

        /// <summary>
        /// Тип карты (Visa, Mastercard, etc)
        /// </summary>
        
        public string CardType { get; set; }
    }
}
