﻿using System;

namespace Clouds42.Domain.DataModels;

public class AccountUserDepartment : BaseDomainModel
{
    public Guid DepartmentId { get; set; }
    public virtual Department Department { get; set; }

    public Guid AccountUserId { get; set; }
    public virtual AccountUser AccountUser { get; set; }
}
