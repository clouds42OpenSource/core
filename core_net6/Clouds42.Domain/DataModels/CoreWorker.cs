﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Модель воркера
    /// </summary>
    [Table("CoreWorkers")]
    public class CoreWorker
    {
        /// <summary>
        /// Идентификатор воркера
        /// </summary>
        [Key]
        [Column("Id")]
        public short CoreWorkerId { get; set; }

        /// <summary>
        /// Статус воркера
        /// </summary>
        public CoreWorkerStatus? Status { get; set; }

        /// <summary>
        /// Дата последнего обновления статистики воркера
        /// </summary>
        public DateTime? Tick { get; set; }

        /// <summary>
        /// Максимальное количество потоков
        /// </summary>
        public short? MaxThreadsCount { get; set; }

        /// <summary>
        /// Количество занятых потоков
        /// </summary>
        public short? BusyThreadsCount { get; set; }

        /// <summary>
        /// Адрес воркера
        /// </summary>
        public string CoreWorkerAddress { get; set; }

        /// <summary>
        /// Ссылка на очередь задач воркера
        /// </summary>
        public virtual ICollection<CoreWorkerTasksQueue> CoreWorkerTasksQueues { get; set; }
        public virtual ICollection<CoreWorkerAvailableTasksBag> CoreWorkerAvailableTasksBags { get; set; }
    }
}
