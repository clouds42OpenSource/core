﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Служебный аккаунт
    /// </summary>
    [Table("ServiceAccounts")]
    public class ServiceAccount
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Key]
        public Guid Id { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// ID пользователя, который создал служебный аккаунт
        /// </summary>
        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserInitiatorId { get; set; }
        public virtual AccountUser AccountUser { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;
    }
}
