﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// История архивации информационной базы.
    /// </summary>
    public class AccountDatabaseBackupHistory : BaseDomainModel, IAccountDatabaseBackupHistory
    {
        /// <summary>
        /// Номер информационной базы.
        /// </summary>
        [ForeignKey(nameof(AccountDatabase))]
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }
        
        /// <summary>
        /// Коментарий архивации.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата создания записи.
        /// </summary>        
        public DateTime CreateDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Номер созданного бэкапа.
        /// </summary>
        [ForeignKey(nameof(AccountDatabaseBackup))]
        public Guid? AccountDatabaseBackupId { get; set; }
        public virtual AccountDatabaseBackup AccountDatabaseBackup { get; set; }

        /// <summary>
        /// Состояние бэкапа.
        /// </summary>
        public AccountDatabaseBackupHistoryState State { get; set; }

    }
}