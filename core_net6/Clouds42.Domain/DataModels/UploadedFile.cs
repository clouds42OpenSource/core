﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.Files;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Загруженный файл
    /// </summary>
    [Table("UploadedFiles")]
    public class UploadedFile : BaseDomainModel
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Имя файла
        /// </summary>
        public string FileName { get; set; }
        
        /// <summary>
        /// Полный путь до файла.
        /// </summary>
        public string FullFilePath { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public UploadedFileStatus Status { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Дата загрузки
        /// </summary>
        public DateTime UploadDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Размер загруженного файла в мегабайтах.
        /// </summary>
        public long? FileSizeInMegabytes { get; set; }
        public virtual AccountDatabaseOnDelimiters AccountDatabaseOnDelimiters { get; set; }
    }
}
