﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Marketing
{
    /// <summary>
    /// Шаблон рекламного баннера
    /// </summary>
    [Table(nameof(AdvertisingBannerTemplate), Schema = SchemaType.Marketing)]
    public class AdvertisingBannerTemplate
    {
        /// <summary>
        /// Id рекламного баннера(первичный ключ)
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Изображение баннера
        /// </summary>
        [ForeignKey(nameof(Image))]
        public Guid ImageCloudFileId { get; set; }
        public virtual CloudFile Image { get; set; }

        /// <summary>
        /// Дата создания рекламного баннера
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело баннера
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Описание ссылки
        /// </summary>
        public string CaptionLink { get; set; }

        /// <summary>
        /// Ссылка баннера
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Аудитория баннера
        /// </summary>
        [ForeignKey(nameof(AdvertisingBannerAudience))]
        public int AdvertisingBannerAudienceId { get; set; }
        public virtual AdvertisingBannerAudience AdvertisingBannerAudience { get; set; }

        /// <summary>
        /// Html шаблон
        /// </summary>
        [ForeignKey(nameof(HtmlTemplate))]
        public int HtmlTemplateId { get; set; }
        public virtual HtmlTemplate HtmlTemplate { get; set; }

        /// <summary>
        /// Показывать баннер с
        /// </summary>
        public DateTime? DisplayBannerDateFrom { get; set; }

        /// <summary>
        /// Показывать баннер по
        /// </summary>
        public DateTime? DisplayBannerDateTo { get; set; }
    }
}
