﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.Marketing;

namespace Clouds42.Domain.DataModels.Marketing
{
    /// <summary>
    /// Аудитория рекламного баннера
    /// </summary>
    [Table(nameof(AdvertisingBannerAudience), Schema = SchemaType.Marketing)]
    public class AdvertisingBannerAudience
    {
        /// <summary>
        /// Id аудитории рекламного баннера(первичный ключ)
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public AccountTypeEnum AccountType { get; set; }

        /// <summary>
        /// Наличие оплаты
        /// </summary>
        public AvailabilityPaymentEnum AvailabilityPayment { get; set; }

        /// <summary>
        /// Состояние сервиса Аренда 1С
        /// </summary>
        public RentalServiceStateEnum RentalServiceState { get; set; }
        
        /// <summary>
        /// Коллекция связей аудитории рекламного баннера и локали аккаунта
        /// </summary>
        public virtual ICollection<AdvertisingBannerAudienceLocaleRelation> AdvertisingBannerAudienceLocaleRelations { get; set; }
    }
}
