﻿using System;

namespace Clouds42.Domain.DataModels.Marketing
{
    /// <summary>
    /// Связь аудитории рекламного баннера и локали аккаунта
    /// </summary>
    public class AdvertisingBannerAudienceLocaleRelation
    {
        /// <summary>
        /// Id связи(первичный ключ)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Аудитория баннера
        /// </summary>                
        public int AdvertisingBannerAudienceId { get; set; }
        public virtual AdvertisingBannerAudience AdvertisingBannerAudience { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public Guid LocaleId { get; set; }
        public virtual Locale Locale { get; set; }
    }
}
