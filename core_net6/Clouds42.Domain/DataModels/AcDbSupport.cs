﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Поддержка инф. базы
    /// </summary>
    [Table(nameof(AcDbSupport))]
    public class AcDbSupport : IAcDbSupport, IVersionSupport
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        [ForeignKey(nameof(AccountDatabase))]
        [Key]
        public Guid AccountDatabasesID { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        
        public string Login { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        
        public string Password { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// База готова к АО
        /// </summary>
        public bool PrepearedForUpdate { get; set; }

        /// <summary>
        /// База подключена к ТиИ
        /// </summary>
        public bool HasSupport { get; set; }

        /// <summary>
        /// База подключена к АО
        /// </summary>
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Старая версия
        /// </summary>
        public string OldVersion { get; set; }

        /// <summary>
        /// Текущая версия
        /// </summary>
        public string CurrentVersion { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }
        public int Build { get; set; }

        /// <summary>
        /// Версия, на которую возможно обновление
        /// </summary>
        public string UpdateVersion { get; set; }

        /// <summary>
        /// База готова к ТиИ
        /// </summary>
        public bool PreparedForTehSupport { get; set; }

        /// <summary>
        /// Конфигурация базы
        /// </summary>
        
        [ForeignKey(nameof(Configurations1C))]
        public string ConfigurationName { get; set; }
        public virtual Configurations1C Configurations1C { get; set; }

        /// <summary>
        /// Дата последнего ТиИ
        /// </summary>
        
        public DateTime? TehSupportDate { get; set; }

        /// <summary>
        /// Дата обновления версии
        /// </summary>
        
        public DateTime? UpdateVersionDate { get; set; }

        /// <summary>
        /// Синоним конфигурации
        /// </summary>
        public string SynonymConfiguration { get; set; }

        /// <summary>
        /// История поддержки инф. базы
        /// </summary>
        public virtual ICollection<AcDbSupportHistory> AcDbSupportHistories { get; set; }

        /// <summary>
        /// Признак, что в базе есть доработки
        /// </summary>
        public bool DatabaseHasModifications { get; set; }

        /// <summary>
        /// Дата подключения
        /// </summary>
        public DateTime? ConnectDate { get; set; }

        /// <summary>
        /// Время обновления 
        /// </summary>
        public AutoUpdateTimeEnum TimeOfUpdate { get; set; } = AutoUpdateTimeEnum.night;
        
        /// <summary>
        /// Завершить сеансы при обновлении
        /// </summary>
        public bool CompleteSessioin { get; set; }
    }
}
