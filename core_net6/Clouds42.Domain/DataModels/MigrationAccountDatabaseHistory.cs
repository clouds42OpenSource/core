using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("MigrationAccountDatabaseHistories", Schema = SchemaType.Migration)]
    public class MigrationAccountDatabaseHistory
    {
        [Key]
        [ForeignKey(nameof(MigrationHistory))]
        public Guid HistoryId { get; set; }
        public virtual MigrationHistory MigrationHistory { get; set; }

        /// <summary>
        ///     ������������� ����������� ����
        /// </summary>
        [ForeignKey(nameof(AccountDatabas))]
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabas { get; set; }

        /// <summary>
        ///     �� ������� � �������� ����������� ����
        /// </summary>
        [ForeignKey(nameof(SourceFileStorage))]
        public Guid? SourceFileStorageId { get; set; }
        public virtual CloudServicesFileStorageServer SourceFileStorage { get; set; }

        /// <summary>
        ///     �� ��������� � ������� ����������� ����
        /// </summary>
        [ForeignKey(nameof(TargetFileStorage))]
        public Guid TargetFileStorageId { get; set; }
        public virtual CloudServicesFileStorageServer TargetFileStorage { get; set; }
    }
}