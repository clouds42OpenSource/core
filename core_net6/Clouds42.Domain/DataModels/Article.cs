﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("Articles")]
    public class Article
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Topic { get; set; }
        public string TopicId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? PublicationDate { get; set; }
        public ArticleStatus Status { get; set; }
        public ArticleType Type { get; set; }
        public string GoogleCloudLink { get; set; }
        public string GoogleCloudDocumentId { get; set; }
        public string WpLink { get; set; }
        public string WpId { get; set; }

        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
        public int? RegistrationCount { get; set; }
        public string Reason { get; set; }
        public decimal RewardAmount { get; set; }
    }

    public enum ArticleStatus
    { 
        [Description("Черновик")]
        Draft = 1,

        [Description("На проверке")]
        UnderInspection = 2,

        [Description("Принята")]
        Accepted = 3,

        [Description("На доработку")]
        ForRevision = 4,

        [Description("Опубликована")]
        Published = 5
    }

    public enum ArticleType
    {
        [Description("Для сайта")]
        ForSite = 1,
        [Description("Для блога")]
        ForBlog = 2
    }
}
