﻿using System;

namespace Clouds42.Domain.DataModels
{
    public class UpdateNodeQueue : BaseDomainModel
    {
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        public DateTime AddDate { get; set; } = DateTime.Now;
    }
}
