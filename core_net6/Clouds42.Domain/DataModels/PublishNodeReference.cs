﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Нода публикаций
    /// </summary>
    [Table("PublishNodesReferences")]
    public class PublishNodeReference
    {
        /// <summary>
        /// Идентификатор ноды
        /// </summary>
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// IP адрес ноды
        /// </summary>
        
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        
        public virtual  ICollection<CloudServicesContentServerNode> CloudServicesContentServerNodes { get; set; }
    }
}
