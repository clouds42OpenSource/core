﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Запущенная задача
    /// </summary>
    public class CoreWorkerTasksQueue : BaseDomainModel, IWorkerTaskResult
    {
        /// <summary>
        /// Статус задачи
        /// </summary>
        
        public string Status { get; set; }

        /// <summary>
        /// Дата создания задачи
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Дата начала выполнения
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Дата изменения задачи
        /// </summary>
        public DateTime? EditDate { get; set; }

        /// <summary>
        /// Id воркера
        /// </summary>
        [ForeignKey(nameof(CapturedWorker))]
        public short? CapturedWorkerId { get; set; }
        public virtual CoreWorker CapturedWorker { get; set; }

        /// <summary>
        /// Комментарий для задачи
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Id задачи
        /// </summary>
        [ForeignKey(nameof(CoreWorkerTask))]
        public Guid CoreWorkerTaskId { get; set; }
        public virtual CoreWorkerTask CoreWorkerTask { get; set; }

        /// <summary>
        /// Дата и время отложенной операции
        /// </summary>
        public DateTime? DateTimeDelayOperation { get; set; }

        /// <summary>
        /// Результат выполнения
        /// </summary>
        public string ExecutionResult { get; set; }

        /// <summary>
        /// Параметры задачи
        /// </summary>
        public virtual CoreWorkerTaskParameter TaskParameter { get; set; }

        public virtual ICollection<AcDbSupportHistoryTasksQueueRelation> AcDbSupportHistoryTasksQueues { get; set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        [NotMapped]
        public CloudTaskQueueStatus QueueStatus
            => (CloudTaskQueueStatus) Enum.Parse(typeof(CloudTaskQueueStatus), Status);

        /// <summary>
        /// Количество попыток перезапуска задачи
        /// </summary>
        public int? RetryTaskAttemptsCount { get; set; }
    }
}
