﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels.Supplier
{
    /// <summary>
    /// Сущность: Поставщик.
    /// Таблица: Справочник поставщиков.
    /// </summary>
    public class Supplier : BaseDomainModel, ISupplier
    {
        public Supplier()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Название поставщика
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public Guid LocaleId { get; set; }
        public virtual Locale Locale { get; set; }

        /// <summary>
        /// Код поставщика
        /// </summary>
        /// <remarks>
        /// Можно получить из справочника организаций в КОРП-е
        /// </remarks>
        [Required]
        
        public string Code { get; set; }

        /// <summary>
        /// Стандартный поставщик на локаль
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Договор оферты
        /// </summary>
        public Guid AgreementId { get;set; }
        public virtual CloudFile Agreement { get; set; }

        /// <summary>
        /// Печатная форма счета на оплату
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceId { get; set; }
        public virtual PrintedHtmlForm PrintedHtmlFormInvoice { get; set; }

        /// <summary>
        /// Печатная форма фискального чека
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceReceiptId { get; set; }
        public virtual PrintedHtmlForm PrintedHtmlFormInvoiceReceipt { get; set; }

        public virtual ICollection<CloudConfiguration> Configurations { get; set; }
    }
}
