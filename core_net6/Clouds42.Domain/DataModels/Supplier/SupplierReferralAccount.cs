﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.Supplier
{
    /// <summary>
    /// Сущность: Связь для поставщика и реферального аккаунта.
    /// Таблица: Справочник связей рефералов и поставщиков.
    /// </summary>
    [Table("SupplierReferralAccounts")]
    [PrimaryKey(nameof(SupplierId), nameof(ReferralAccountId))]
    public class SupplierReferralAccount
    {
        /// <summary>
        /// Поставщик
        /// </summary>
        [Column(Order = 0)]
        [ForeignKey(nameof(Supplier))]
        public Guid SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }

        /// <summary>
        /// Реферал
        /// </summary>
        [Column(Order = 1)]
        [ForeignKey(nameof(ReferralAccount))]
        public Guid ReferralAccountId { get; set; }
        public virtual Account ReferralAccount { get; set; }
    }
}
