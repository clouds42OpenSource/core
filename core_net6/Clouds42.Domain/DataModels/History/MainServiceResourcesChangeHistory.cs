﻿using System;

namespace Clouds42.Domain.DataModels.History
{
    /// <summary>
    /// История изменений ресурсов главного сервиса(Аренда 1С) для аккаунта
    /// </summary>
    
    public class MainServiceResourcesChangesHistory
    {
        /// <summary>
        /// Id изменений ресурсов(первичный ключ)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Количество отключенных ресурсов
        /// </summary>
        public int DisabledResourcesCount { get; set; }

        /// <summary>
        /// Количество подключенных ресурсов
        /// </summary>
        public int EnabledResourcesCount { get; set; }

        /// <summary>
        /// Дата изменений
        /// </summary>
        public DateTime ChangesDate { get; set; }
    }
}
