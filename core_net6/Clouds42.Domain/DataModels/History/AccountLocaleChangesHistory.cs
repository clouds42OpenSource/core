﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.History
{
    /// <summary>
    /// История изменения локали у аккаунта
    /// </summary>
    [Table("AccountLocaleChanges", Schema = SchemaType.History)]
    public class AccountLocaleChangesHistory
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime ChangesDate { get; set; } = DateTime.Now;
    }
}
