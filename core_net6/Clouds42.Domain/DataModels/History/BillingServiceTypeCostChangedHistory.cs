﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.billing.BillingChanges;

namespace Clouds42.Domain.DataModels.History
{
    /// <summary>
    /// История изменения стоимости услуги сервиса
    /// </summary>
    public class BillingServiceTypeCostChangedHistory : BaseDomainModel
    {
        /// <summary>
        /// ID изменений по услуге сервиса
        /// </summary>
        [ForeignKey(nameof(BillingServiceTypeChange))]
        public Guid BillingServiceTypeChangeId { get; set; }

        /// <summary>
        /// Изменения по услуге сервиса
        /// </summary>
        public virtual BillingServiceTypeChange BillingServiceTypeChange { get; set; }

        /// <summary>
        /// Старая стоимость
        /// </summary>
        
        public decimal OldCost { get; set; }
    }
}