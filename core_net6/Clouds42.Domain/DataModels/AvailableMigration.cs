using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table(nameof(AvailableMigration))]
    public class AvailableMigration
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey(nameof(SegmentFrom))]
        public Guid SegmentIdFrom { get; set; }
        public virtual CloudServicesSegment SegmentFrom { get; set; }

        [ForeignKey(nameof(SegmentTo))]
        public Guid SegmentIdTo { get; set; }
        public virtual CloudServicesSegment SegmentTo { get; set; }
    }
}