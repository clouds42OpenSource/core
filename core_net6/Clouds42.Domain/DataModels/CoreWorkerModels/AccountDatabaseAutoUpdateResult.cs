﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.CoreWorkerModels
{
    /// <summary>
    /// Результат выполнения АО для инф. базы
    /// </summary>
    public class AccountDatabaseAutoUpdateResult : BaseDomainModel
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        [ForeignKey(nameof(AccountDatabase))]
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// ID задачи из очереди
        /// </summary>
        [ForeignKey(nameof(CoreWorkerTasksQueue))]
        public Guid CoreWorkerTasksQueueId { get; set; }
        public virtual CoreWorkerTasksQueue CoreWorkerTasksQueue { get; set; }

        /// <summary>
        /// Информация для отладки
        /// </summary>
        public string DebugInformation { get; set; }
    }
}
