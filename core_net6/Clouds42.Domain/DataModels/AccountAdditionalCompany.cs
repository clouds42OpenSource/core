﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    public class AccountAdditionalCompany: BaseDomainModel
    {
        public string Name { get; set; }
        public string Inn { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}
