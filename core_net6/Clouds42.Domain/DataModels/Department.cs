﻿using System;
using System.Collections.Generic;

namespace Clouds42.Domain.DataModels;

public class Department : BaseDomainModel
{
    public Guid AccountId { get; set; }
    public virtual Account Account { get; set; }
    public string Name { get; set; }
    public virtual ICollection<AccountUserDepartment> AccountUserDepartments { get; set; }
}
