using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Clouds42.Domain.DataModels
{
    public class AccountCSResourceValue : BaseDomainModel
    {
        public Guid InitiatorCloudService { get; set; }
        public int ModifyResourceValue { get; set; }
        public DateTime ModifyResourceDateTime { get; set; }

         [Required] public string ModifyResourceComment { get; set; }
        
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }
        
        public Guid CSResourceId { get; set; }
        public virtual CsResource CSResource { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder(nameof(AccountCSResourceValue));
            sb.AppendLine("AccountId: " + AccountId);
            sb.AppendLine("CSResourceId: " + CSResourceId);
            sb.AppendLine("InitiatorCloudService: " + InitiatorCloudService);
            sb.AppendLine("ModifyResourceValue: " + ModifyResourceValue);
            sb.AppendLine("ModifyResourceComment: " + ModifyResourceComment);
            return sb.ToString();
        }
    }
}