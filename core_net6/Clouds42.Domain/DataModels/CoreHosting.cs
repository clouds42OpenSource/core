﻿namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Хостинг облака
    /// </summary>
    public class CoreHosting : BaseDomainModel
    {
        /// <summary>
        /// Название хостинга
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Путь для загружаемых файлов
        /// </summary>
        public string UploadFilesPath { get; set; }

        /// <summary>
        /// Адрес API для загрузки файлов
        /// </summary>
        public string UploadApiUrl { get; set; }
    }
}
