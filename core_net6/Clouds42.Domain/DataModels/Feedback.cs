﻿using System;

namespace Clouds42.Domain.DataModels;
/// <summary>
/// Модель данных для хранения отзывов пользователей
/// </summary>
public class Feedback : BaseDomainModel
{
    /// <summary>
    /// Идентификатор пользователя, оставившего отзыв
    /// </summary>

    public Guid AccountUserId { get; set; }
    
    /// <summary>
    /// Навигационное свойство для связи с пользователем
    /// </summary>
    public virtual AccountUser AccountUser { get; set; }
    
    /// <summary>
    /// Задержка обработки отзыва
    /// </summary>
    public DateTime? Delay { get; set; }
    
    /// <summary>
    /// Дата, когда был оставлен отзыв
    /// </summary>
    public DateTime? FeedbackLeftDate { get; set; }
}
