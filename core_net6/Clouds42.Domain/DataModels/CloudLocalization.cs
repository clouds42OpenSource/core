﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Локализация облака
    /// </summary>    
    public class CloudLocalization
    {
        /// <summary>
        /// Ключ(ключевое значение для поиска)
        /// </summary>        
        public CloudLocalizationKeyEnum Key { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public Guid LocaleId { get; set; }
        public virtual Locale Locale { get; set; }

        /// <summary>
        /// Фраза/слово на языке локали
        /// </summary>
        
        [Required]
        public string Value { get; set; }
    }
}
