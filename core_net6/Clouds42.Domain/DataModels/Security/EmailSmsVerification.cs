﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Security
{
    [Table("EmailSmsVerifications", Schema = SchemaType.Security)]
    public class EmailSmsVerification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
        public VerificationType Type { get; set; }
        public string Code { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public enum VerificationType
    {
        Email = 1,
        Sms = 2
    }
}
