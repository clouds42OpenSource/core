﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Security
{
    /// <summary>
    /// Доменный модель данных авторизации в ИТС
    /// </summary>
    [Table(nameof(ItsAuthorizationData), Schema = SchemaType.Security)]
    public class ItsAuthorizationData : BaseDomainModel
    {
        /// <summary>
        /// Логин 
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Хэш пароля
        /// </summary>
        public string PasswordHash { get; set; }
    }
}