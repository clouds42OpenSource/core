﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Таблица бэкапов информационных баз
    /// </summary>
    public class AccountDatabaseBackup : BaseDomainModel, IBackupSource
    {
        /// <summary>
        ///     Время создания записи
        /// </summary>
        public DateTime CreateDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Время создания бекапа
        /// </summary>        
        public DateTime CreationBackupDateTime { get; set; }

        /// <summary>
        ///     Путь к бекапу. (Веб ссылка на облачное хранилище\локальный путь к хранилке)
        /// </summary>
        [Required]
        public string BackupPath { get; set; }

        /// <summary>
        /// Путь до файла в облачном хранилище, с учетом папок и подпапок
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        ///     Триггер создания бекапа
        /// </summary>
        public CreateBackupAccountDatabaseTrigger EventTrigger { get; set; }

        /// <summary>
        ///     Источник хранилища бэкапа
        /// </summary>
        public AccountDatabaseBackupSourceType SourceType { get; set; }

        /// <summary>
        ///     Информационная база
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        ///     Инициатор создания бекапа
        /// </summary>
        public Guid InitiatorId { get; set; }
        public virtual AccountUser Initiator { get; set; }

        /// <summary>
        /// Связь бэкапа инф. базы с бэкапом от МС
        /// </summary>
        public virtual ServiceManagerAcDbBackup ServiceManagerAcDbBackup { get; set; }
    }
}
