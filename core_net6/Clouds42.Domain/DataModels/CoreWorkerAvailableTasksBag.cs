﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Таблица связей воркера к доступным задачам.
    /// </summary>
    [Table("CoreWorkerAvailableTasksBags")]
    [PrimaryKey(nameof(CoreWorkerId), nameof(CoreWorkerTaskId))]
    public class CoreWorkerAvailableTasksBag
    {
        /// <summary>
        /// Номер воркера.
        /// </summary>
        [ForeignKey(nameof(CoreWorker))]
        [Column(Order = 0)]
        public short CoreWorkerId { get; set; }
        public virtual CoreWorker CoreWorker { get; set; }

        /// <summary>
        /// Идентификатор типа задачи.
        /// </summary>
        [ForeignKey(nameof(CoreWorkerTask))]
        [Column(Order = 1)]
        public Guid CoreWorkerTaskId { get; set; }
        public virtual CoreWorkerTask CoreWorkerTask { get; set; }

        /// <summary>
        /// Приоритет задачи
        /// </summary>
        public int Priority { get; set; }
    }
}
