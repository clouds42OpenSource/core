﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("AccountUserSessions")]
    public class AccountUserSession
    {   
        public Guid Id { get; set; }
        public Guid Token { get; set; }

        public bool? StaticToken { get; set; }

        
        public string ClientDescription { get; set; }

        
        public string ClientDeviceInfo { get; set; }

        
        public string ClientIPAddress { get; set; }

        [Required]
        public DateTime? TokenCreationTime { get; set; }

        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
    }
}
