﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    public class CsResource : BaseDomainModel
    {

        [ForeignKey(nameof(CloudService))]
        public string CloudServiceId { get; set; }
        public virtual CloudService CloudService { get; set; }

        
        public string ResourcesName { get; set; }

        public bool? DaysAutoDecrease { get; set; }

        public virtual ICollection<AccountCSResourceValue> AccountCsResourceValues { get; set; }
    }
}
