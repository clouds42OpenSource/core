﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountDatabases
{
    /// <summary>
    /// Связь конфигурации 1С с шаблоном инф. базы
    /// </summary>
    [Table("ConfigurationDbTemplateRelations")]
    public class ConfigurationDbTemplateRelation
    {
        /// <summary>
        /// Название конфигурации 1С
        /// </summary>
        [ForeignKey(nameof(Configuration1C))]
        public string Configuration1CName { get; set; }
        public virtual Configurations1C Configuration1C { get; set; }

        /// <summary>
        /// Шаблон инф. базы
        /// </summary>
        [Key]
        [ForeignKey(nameof(DbTemplate))]
        public Guid DbTemplateId { get; set; }
        public virtual DbTemplate DbTemplate { get; set; }
    }
}
