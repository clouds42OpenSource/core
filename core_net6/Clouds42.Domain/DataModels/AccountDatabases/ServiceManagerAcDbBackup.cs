﻿using System;

namespace Clouds42.Domain.DataModels.AccountDatabases
{
    /// <summary>
    /// Связь бэкапа инф. базы с бэкапом МС
    /// </summary>    
    public class ServiceManagerAcDbBackup
    {
        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>        
        public Guid AccountDatabaseBackupId { get; set; }
        public virtual AccountDatabaseBackup AccountDatabaseBackup { get; set; }

        /// <summary>
        /// ID бэкапа инф. базы от МС
        /// </summary>        
        public Guid ServiceManagerAcDbBackupId { get; set; }
    }
}
