﻿using System;

namespace Clouds42.Domain.DataModels.AccountDatabases
{
    /// <summary>
    /// Таблица связи истории провередения работ для инф. базы
    /// и задачи по авто обновлению
    /// </summary>    
    public class AcDbSupportHistoryTasksQueueRelation
    {
        /// <summary>
        /// Id записи
        /// </summary>        
        public int Id { get; set; }

        /// <summary>
        /// ID задачи автообновления
        /// </summary>        
        public Guid TasksQueueId { get; set; }
        public virtual CoreWorkerTasksQueue AutoUpdateTasksQueue { get; set; }

        /// <summary>
        /// ID истории проведения технических работ в инф. базе
        /// </summary>        
        public Guid AcDbSupportHistoryId { get; set; }
        public virtual AcDbSupportHistory AcDbSupportHistory { get; set; }
    }
}
