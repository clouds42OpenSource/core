﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.DataModels.AccountDatabases
{
    /// <summary>
    /// Завершение сеансов в информационной базе
    /// </summary>
    [Table("TerminationSessionsInDatabases")]
    public class TerminationSessionsInDatabase : BaseDomainModel
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        [ForeignKey(nameof(AccountDatabase))]
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Статус завершения сеансов в инф. базе
        /// </summary>
        public TerminateSessionsInDbStatusEnum TerminateSessionsInDbStatus { get; set; }

        /// <summary>
        /// Дата создания записи о заершении сеансов
        /// </summary>
        public DateTime CreationDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Дата завершения сеансов в инф. базе
        /// </summary>
        public DateTime? TerminatedSessionsDate { get; set; }
    }
}
