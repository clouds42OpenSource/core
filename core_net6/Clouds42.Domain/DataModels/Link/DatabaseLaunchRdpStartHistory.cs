﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.Domain.DataModels.Link
{
    /// <summary>
    /// Таблица для регистрации истории запуска баз через Линк
    /// </summary>
    [Table(nameof(DatabaseLaunchRdpStartHistory))]
    public class DatabaseLaunchRdpStartHistory
    {
        /// <summary>
        /// ID таблицы
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [Required]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Дата запуска базы
        /// </summary>
        [Required]
        public DateTime ActionCreated { get; set; }

        /// <summary>
        /// Выполненное действие
        /// </summary>
        [Required]
        [DefaultValue(1)]
        public LinkActionType Action { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Login { get; set; }

        /// <summary>
        /// Версия линка
        /// </summary>
        [MaxLength(20)]
        public string LinkAppVersion { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        [MaxLength(50)]
        public string V82Name { get; set; }

        /// <summary>
        /// Тип Линка
        /// </summary>
        [Required]
        [DefaultValue(1)]
        public AppModes LinkAppType { get; set; }

        /// <summary>
        /// Тип запуска базы 1С
        /// </summary>
        [Required]
        [DefaultValue(1)]
        public LaunchType LaunchType { get; set; }

        /// <summary>
        /// Внешний IP адрес
        /// </summary>
        [MaxLength(20)]
        public string ExternalIpAddress { get; set; }

        /// <summary>
        /// Внутренний IP адрес
        /// </summary>
        [MaxLength(20)]
        public string InternalIpAddress { get; set; }
    }
}