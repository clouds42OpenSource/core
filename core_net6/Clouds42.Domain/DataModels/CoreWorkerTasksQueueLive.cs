﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Активная задача в очереди
    /// </summary>
    [Table(nameof(CoreWorkerTasksQueueLive))]
    public class CoreWorkerTasksQueueLive
    {
        /// <summary>
        /// ID задачи в очереди
        /// </summary>
        [Key]
        [ForeignKey(nameof(CoreWorkerTasksQueue))]
        public Guid CoreWorkerTasksQueueId { get; set; }
        public virtual CoreWorkerTasksQueue CoreWorkerTasksQueue { get; set; }


        /// <summary>
        /// Дата и время отложенной операции
        /// </summary>
        public DateTime? DateTimeDelayOperation { get; set; }
        
        /// <summary>
        /// Статус задачи
        /// </summary>
        public CloudTaskQueueStatus Status { get; set; }

        /// <summary>
        /// Id задачи
        /// </summary>
        [ForeignKey(nameof(CoreWorkerTask))]
        public Guid CoreWorkerTaskId { get; set; }
        public virtual CoreWorkerTask CoreWorkerTask { get; set; }
        
        /// <summary>
        /// Дата создания задачи
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Дата начала выполнения
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Дата изменения задачи
        /// </summary>
        public DateTime? EditDate { get; set; }

        /// <summary>
        /// Параметры для задачи
        /// </summary>
        public string TaskParams { get; set; }

        /// <summary>
        /// Ключ синхронизации
        /// </summary>
        public string SynchronizationKey { get; set; }

        /// <summary>
        /// Зависимая задача
        /// </summary>
        [ForeignKey(nameof(DependTask))]
        public Guid? DependTaskId { get; set; }
        public virtual CoreWorkerTasksQueue DependTask { get; set; }

        /// <summary>
        /// Id воркера
        /// </summary>
        [ForeignKey(nameof(CapturedWorker))]
        public short? CapturedWorkerId { get; set; }
        public virtual CoreWorker CapturedWorker { get; set; }
    }
}