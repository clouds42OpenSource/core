using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    [Table("DiskResources", Schema = SchemaType.Billing)]    
    public class DiskResource
    {
        [Key] [ForeignKey(nameof(Resource))] public Guid ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public int VolumeGb { get; set; }
    }
}