﻿using System;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// 1С файл сервиса биллинга
    /// </summary>
    public interface IBillingService1CFile
    {
        /// <summary>
        /// Id файла
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        string FileName { get; set; }

        /// <summary>
        /// Оригинальное название файла
        /// </summary>
        string OriginalName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime CreationDate { get; set; }
    }
}
