﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Счет на оплату
    /// </summary>
    public class Invoice : BaseDomainModel
    {

        /// <summary>
        /// Дата создания счета
        /// </summary>
        public DateTime Date { get; set; } = DateTime.Now;

        /// <summary>
        /// Сумма счета
        /// </summary>
        
        public decimal Sum { get; set; }

        /// <summary>
        /// Бонусное вознаграждение
        /// </summary>
        
        public decimal? BonusReward { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        
        public string Requisite { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Комментарий к счету
        /// </summary>
        
        public string Comment { get; set; }

        /// <summary>
        /// Статус обработки
        /// </summary>
        
        public string State { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Uniq { get; set; }

        /// <summary>
        /// ID акта
        /// </summary>
        public Guid? ActID { get; set; }
        
        /// <summary>
        /// Флаг нужна ли подпись для акта или нет
        /// </summary>

        public bool RequiredSignature { get; set; }
        
        /// <summary>
        /// Статус акта
        /// </summary>

        public Status Status { get; set; }

        /// <summary>
        /// Описание акта
        /// </summary>
        
        public string ActDescription { get; set; }

        /// <summary>
        /// Период оплаты
        /// </summary>
        public int? Period { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        [ForeignKey(nameof(AccountAdditionalCompany))]
        public Guid? AccountAdditionalCompanyId { get; set; }
        public virtual AccountAdditionalCompany AccountAdditionalCompany { get; set; }

        /// <summary>
        /// ID клиентского платежа
        /// </summary>
        [ForeignKey(nameof(Payment))]
        public Guid? PaymentID { get; set; }
        public virtual Payment Payment { get; set; }

        /// <summary>
        /// Чек на оплату
        /// </summary>
        public virtual InvoiceReceipt InvoiceReceipt { get; set; }

        /// <summary>
        /// Оплачиваемые услуги
        /// </summary>
        public virtual ICollection<InvoiceProduct> InvoiceProducts { get; set; }


        /// <summary>
        /// Статус обработки (Enum репрезентация)
        /// </summary>
        [NotMapped]
        public InvoiceStatus? StateEnum
        {
            get => Enum.TryParse<InvoiceStatus>(State, out var stateEnum) ? stateEnum : default;
            set => State = value.ToString();
        }
    }
}
