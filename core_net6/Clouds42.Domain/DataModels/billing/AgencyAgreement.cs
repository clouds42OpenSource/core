﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Агентское соглашение
    /// </summary>
    public class AgencyAgreement : BaseDomainModel
    {
        public AgencyAgreement()
        {
            CreationDate = DateTime.Now;
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Название агентского соглашения
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Печатная форма
        /// </summary>
        [ForeignKey(nameof(PrintedHtmlForm))]
        public Guid PrintedHtmlFormId { get; set; }
        public virtual PrintedHtmlForm PrintedHtmlForm { get; set; }

        /// <summary>
        /// Дата вступления в силу
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Аренда 1С"
        /// (проценты)
        /// </summary>
        public decimal Rent1CRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Мой диск"
        /// (проценты)
        /// </summary>
        public decimal MyDiskRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис разработчика
        /// (проценты)
        /// </summary>
        public decimal ServiceOwnerRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Аренда 1С" для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal Rent1CRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Мой диск" для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal MyDiskRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Вознаграждение за сервис разработчика для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal ServiceOwnerRewardPercentForVipAccounts { get; set; }
    }
}
