﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    public class ResourcesConfiguration : BaseDomainModel, IResourcesConfiguration
    {
        public decimal Cost { get; set; }

        public bool? CostIsFixed { get; set; }

        public DateTime? ExpireDate { get; set; }

        public bool? Frozen { get; set; }

        public int? DiscountGroup { get; set; }

        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Признак демо периода.
        /// </summary>
        [DefaultValue(false)]
        public bool IsDemoPeriod { get; set; }

        [ForeignKey(nameof(BillingAccounts))]
        public Guid AccountId { get; set; }
        public virtual BillingAccount BillingAccounts { get; set; }

        [ForeignKey(nameof(BillingService))]
        [Required]
        public Guid BillingServiceId { get; set; }
        
        public virtual BillingService BillingService { get; set; }

        [NotMapped] public DateTime ExpireDateValue => GetExpireDateValue();

        [NotMapped]
        public bool CostIsFixedValue => CostIsFixed ?? false;

        [NotMapped] public bool FrozenValue => Frozen ?? false;

        public DateTime GetExpireDateValue()
            => ExpireDate ?? DateTime.Today.AddMonths(1);

        public IBillingService GetBillingService() => BillingService;

        /// <summary>
        /// Флаг авто-подписки
        /// </summary>
        public bool? IsAutoSubscriptionEnabled { get; set; }
    }
}
