﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Аккаунт биллинга
    /// </summary>
    [Table("BillingAccounts", Schema = SchemaType.Billing)]
    public class BillingAccount
    {
        /// <summary> 
        /// Аккаунт
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Key]
        public Guid Id { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Баланс
        /// </summary>
        
        public decimal Balance { get; set; }

        /// <summary>
        /// Баланс бонусов
        /// </summary>
        
        public decimal BonusBalance { get; set; }

        /// <summary>
        /// Ежемесячный платеж
        /// </summary>
        
        public DateTime? MonthlyPaymentDate { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        
        public string AccountType { get; set; }

        /// <summary>
        /// Дата взятия общанного платежа
        /// </summary>
        public DateTime? PromisePaymentDate { get; set; }

        /// <summary>
        /// Сумма обещанного платежа
        /// </summary>
        
        public decimal? PromisePaymentSum { get; set; }

        /// <summary>
        /// Описание доп. ресурсов
        /// </summary>
        
        public string AdditionalResourceName { get; set; }

        /// <summary>
        /// Стоимость доп. ресурсов
        /// </summary>
        public decimal? AdditionalResourceCost { get; set; }

        /// <summary>
        /// Список ресурс конфигураций
        /// </summary>
        public virtual ICollection<ResourcesConfiguration> ResourcesConfigurations { get; set; }

        /// <summary>
        /// Список ресурсов
        /// </summary>
        public virtual ICollection<Resource> Resources { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        [NotMapped]
        public AccountLevel AccountTypeEnum => string.IsNullOrEmpty(AccountType) ? AccountLevel.Standart : AccountType.GetAccountLevel();

        /// <summary>
        /// Проверить активен ли обещаный платеж
        /// </summary>
        /// <returns>Bool активности обещанного платежа</returns>
        public bool PromisedPaymentIsActive()
        {
            return PromisePaymentSum is > decimal.Zero;
        }

        /// <summary>
        /// Активировать обещанный платеж
        /// </summary>
        /// <param name="sum">Сумма обещанного платежа</param>
        /// <exception cref="InvalidOperationException"></exception>
        public void ActivatePromisedPayment(decimal sum)
        {
            if (sum <= 0)
                throw new InvalidOperationException("Сумма обещанного платежа должна быть больше 0");
            if (PromisedPaymentIsActive())
                throw new InvalidOperationException("Обещаный платеж уже активирован");
            PromisePaymentSum = sum;
            PromisePaymentDate = DateTime.Now;
        }
    }
}
