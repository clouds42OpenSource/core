﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Услуга сервиса облака
    /// </summary>    
    public class BillingServiceType : BaseDomainModel, IBillingServiceType
    {
        /// <summary>
        /// Сервис облака
        /// </summary>        
        public Guid ServiceId { get; set; }
        public virtual BillingService Service { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ключ услуги
        /// </summary>
        [Required]
        public Guid Key { get; set; }

        /// <summary>
        /// Описание услуги
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Зависимость от другой услуги
        /// </summary>
        public Guid? DependServiceTypeId { get; set; }        

        public virtual BillingServiceType DependServiceType { get; set; }

        /// <summary>
        /// Предки
        /// </summary>
        public virtual ICollection<BillingServiceTypeRelation> ParentRelations { get; set; }

        /// <summary>
        /// Наследники
        /// </summary>
        public virtual ICollection<BillingServiceTypeRelation> ChildRelations { get; set; }
        public virtual ICollection<BillingServiceTypeActivityForAccount> BillingServiceTypesActivities { get; set; }
        public virtual ICollection<BillingServiceType> BillingServiceTypes { get; set; }

        /// <summary>
        /// Продукты счета для услуги
        /// </summary>
        public virtual ICollection<InvoiceProduct> InvoiceProducts { get; set; }

        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<AccountRate> AccountRates { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }

        /// <summary>
        /// Список связей услуги с конфигурацией 1С
        /// </summary>
        public virtual ICollection<ConfigurationServiceTypeRelation> ConfigurationServiceTypeRelations { get; set; }

        /// <summary>
        /// Получить обьект сервиса.
        /// </summary>
        /// <returns>Сервис биллинга.</returns>
        public IBillingService GetService()
            => Service;

        /// <summary>
        /// Признак что услуга удалена
        /// </summary>
        public bool IsDeleted { get; set; }

    }
}
