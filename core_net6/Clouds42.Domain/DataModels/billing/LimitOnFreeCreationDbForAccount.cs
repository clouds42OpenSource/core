﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Лимит на бесплатное создание баз для аккаунта
    /// </summary>
    [Table("LimitOnFreeCreationDbForAccounts", Schema = SchemaType.Billing)]
    public class LimitOnFreeCreationDbForAccount
    {
        /// <summary>
        /// Id аккаунта биллинга
        /// </summary>
        [Key]
        [ForeignKey(nameof(BillingAccount))]
        public Guid AccountId { get; set; }
        public virtual BillingAccount BillingAccount { get; set; }

        /// <summary>
        /// Лимитированное количество баз,
        /// на бесплатное создание
        /// </summary>
        public int LimitedQuantity { get; set; }
    }
}
