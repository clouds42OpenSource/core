﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Связь конфигурации 1С с услугой сервиса(Мои информационные базы)
    /// </summary>
    [Table("ConfigurationServiceTypeRelations", Schema = SchemaType.Billing)]
    public class ConfigurationServiceTypeRelation
    {
        /// <summary>
        /// Название конфигурации 1С
        /// </summary>
        [Key]
        [ForeignKey(nameof(Configuration1C))]
        public string Configuration1CName { get; set; }
        public virtual Configurations1C Configuration1C { get; set; }

        /// <summary>
        /// Id услуги сервиса
        /// </summary>
        [ForeignKey(nameof(ServiceType))]
        public Guid ServiceTypeId { get; set; }
        public virtual BillingServiceType ServiceType { get; set; }
    }
}
