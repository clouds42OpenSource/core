﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// 1С файл сервиса биллинга
    /// </summary>
    public class BillingService1CFile : BaseDomainModel, IBillingService1CFile
    {
        /// <summary>
        /// Название загруженного файла
        /// </summary>
        
        [Required]
        public string FileName { get; set; }

        /// <summary>
        /// Оригинальное название загруженного файла
        /// </summary>
        
        [Required]
        public string OriginalName { get; set; }

        /// <summary>
        /// Название файла разработки
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Синоним файла разработки
        /// </summary>
        
        [Required]
        public string Synonym { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Сервис облака
        /// </summary>
        [ForeignKey(nameof(Service))]
        public Guid ServiceId { get; set; }
        public virtual BillingService Service { get; set; }

        /// <summary>
        /// Id версии файла разработки
        /// </summary>
        [ForeignKey(nameof(Service1CFileVersion))]
        public Guid Service1CFileVersionId { get; set; }
        public virtual Service1CFileVersion Service1CFileVersion { get; set; }

        /// <summary>
        /// Коллекция связей файла 1С с конфигурациями на разделителях
        /// </summary>
        public virtual ICollection<Service1CFileDbTemplateRelation> Service1CFileDbTemplateRelations { get; set; }
        public virtual ICollection<ServiceManager1CFileRelation> ServiceManager1CFileRelations { get; set; }
    }
}
