﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Клиентский платеж
    /// </summary>
    public class Payment : BaseDomainModel
    {
        /// <summary>
        /// Номер платежа
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Number { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        
        public decimal Sum { get; set; }
        
        /// <summary>
        /// Остаток после проведенной транзакции
        /// </summary>
        public decimal? Remains { get; set; }

        /// <summary>
        /// Дата совершения платежа
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        
        [Required]
        public string OperationType { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// Статус обработки платежа
        /// </summary>
        
        [Required]
        public string Status { get; set; }

        /// <summary>
        /// Платежная система
        /// </summary>
        
        [Required]
        public string PaymentSystem { get; set; }

        /// <summary>
        /// Источник платежа
        /// </summary>
        
        [Required]
        public string OriginDetails { get; set; }

        /// <summary>
        /// Назначение платежа
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID платежа в КОРП
        /// </summary>
        public Guid? ExchangeDataId { get; set; }     

        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        [ForeignKey(nameof(BillingService))]
        public Guid? BillingServiceId { get; set; }
        
        /// <summary>
        /// Сервис биллинга
        /// </summary>
        public virtual BillingService BillingService { get; set; }

        /// <summary>
        /// Внешний номер платежа 
        /// </summary>
        /// На удаление
        public int? ExternalPaymentNumber { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Column(nameof(Account))]
        public Guid AccountId { get; set; }
        
        /// <summary>
        /// Аккаунт
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Список счетов на оплату
        /// </summary>
        public virtual ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// Внешний номер платежа
        /// </summary>
        public string PaymentSystemTransactionId { get; set; }

        /// <summary>
        /// Токен подтверждения платежа (для Yookassa)
        /// </summary>
        public string ConfirmationToken { get; set; }

        /// <summary>
        /// Тип подтверждения (для Yookassa)
        /// </summary>
        public string ConfirmationType { get; set; }

        /// <summary>
        /// Ссылка редирект на страницу подтверждения
        /// </summary>
        public string ConfirmationRedirectUrl { get; set; }

        /// <summary>
        /// Флаг указывающий на то что платеж был проведен через функцию добавления автоплатежа
        /// </summary>
        public bool IsAutoPay { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        [NotMapped]
        public PaymentType OperationTypeEnum
        {
            get => (PaymentType) Enum.Parse(typeof(PaymentType), OperationType);
            set => OperationType = value.ToString();
        }

        /// <summary>
        /// Статус обработки
        /// </summary>
        [NotMapped]
        public PaymentStatus StatusEnum
        {
            get => (PaymentStatus) Enum.Parse(typeof(PaymentStatus), Status);
            set => Status = value.ToString();
        }
    }
}
