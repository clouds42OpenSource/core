﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    public class Rate : BaseDomainModel
    {
        public string RatePeriod { get; set; }
        public decimal Cost { get; set; }
        public string AccountType { get; set; }
        public int? DiscountGroup { get; set; }
        public string DemoPeriod { get; set; }
        public string Remark { get; set; }
        public int? UpperBound { get; set; }
        public int? LowerBound { get; set; }        

        [ForeignKey(nameof(Locale))]
        public Guid? LocaleId { get; set; }
        public virtual Locale Locale { get; set; }

        [ForeignKey(nameof(BillingServiceType))]
        [Required]
        public Guid BillingServiceTypeId { get; set; }

        public virtual BillingServiceType BillingServiceType { get; set; }
        [NotMapped] public DateTime? DemoExpire { get; set; }

    }
}
