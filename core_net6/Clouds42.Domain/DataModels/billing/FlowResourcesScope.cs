﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.billing
{

    /// <summary>
    /// Сводная таблица движимых ресурсов.
    /// </summary>
    [Table(nameof(FlowResourcesScope), Schema = SchemaType.Billing)]
    [PrimaryKey(nameof(AccountId), nameof(FlowResourceId))]
    public class FlowResourcesScope
    {

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Column(Order = 0)]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Номер ресурса.
        /// </summary>
        [ForeignKey(nameof(FlowResource))]
        [Column(Order = 1)]
        public Guid FlowResourceId { get; set; }
        public virtual CsResource FlowResource { get; set; }

        /// <summary>
        /// Сводное значение ресурсов.
        /// </summary>
        public int ScopeValue { get; set; }
    }
}
