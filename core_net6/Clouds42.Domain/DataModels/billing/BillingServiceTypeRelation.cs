﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Связь услуги
    /// </summary>
    [Table("ServiceTypeRelations", Schema = SchemaType.Billing)]
    [PrimaryKey(nameof(MainServiceTypeId), nameof(ChildServiceTypeId))]
    public class BillingServiceTypeRelation
    {
        /// <summary>
        /// Услуга сервиса
        /// </summary>
        [Column(Order = 0)]
        [ForeignKey(nameof(MainServiceType))]
        public Guid MainServiceTypeId { get; set; }
        public virtual BillingServiceType MainServiceType { get; set; }

        /// <summary>
        /// Зависимая услуга
        /// </summary>
        [Column(Order = 1)]
        [ForeignKey(nameof(ChildServiceType))]
        public Guid ChildServiceTypeId { get; set; }
        public virtual BillingServiceType ChildServiceType { get; set; }

        /// <summary>
        /// Признак что связь удалена
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
