﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Ресурс
    /// </summary>
    public class Resource : BaseDomainModel, IClone<Resource>
    {        
        /// <summary>
        /// Стоимость ресурса
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// ID пользователя, которому назначен ресурс
        /// </summary>
        public Guid? Subject { get; set; }
        
        /// <summary>
        /// Дата окончания демо периода
        /// </summary>
        public DateTime? DemoExpirePeriod { get; set; }

        /// <summary>
        /// Ресур свободен
        /// </summary>
        public bool? IsFree { get; set; }

        /// <summary>
        /// Тэг
        /// </summary>
        public int? Tag { get; set; }

        /// <summary>
        /// ID аккаунта, который спонсирует данный ресурс
        /// </summary>
        public Guid? AccountSponsorId { get; set; }
        public virtual Account AccountSponsor { get; set; }

        /// <summary>
        /// ID аккаунта биллинга
        /// </summary>
        public Guid AccountId { get; set; }
        public virtual BillingAccount BillingAccounts { get; set; }

        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        [ForeignKey(nameof(BillingServiceType))]
        public Guid BillingServiceTypeId { get; set; }
        public virtual BillingServiceType BillingServiceType { get; set; }

        [ForeignKey(nameof(MyDatabasesResource))]
        public Guid? MyDatabasesResourceId { get; set; }
        public virtual MyDatabasesResource MyDatabasesResource { get; set; }       

        /// <summary>
        /// Инициализировать модель ресурса
        /// </summary>
        /// <returns>Модель ресурса</returns>
        public Resource Init()
        {
            IsFree ??= false;
            return this;
        }

        /// <summary>
        /// Сделать копию объекта ресурса
        /// </summary>
        /// <returns>Копия объекта ресурса</returns>
        public Resource Clone()
            => new()
            {
                Id = Guid.NewGuid(),
                AccountId = AccountId,
                Subject = Subject,
                Cost = Cost,
                AccountSponsorId = AccountSponsorId,
                BillingServiceTypeId = BillingServiceTypeId,
                DemoExpirePeriod = DemoExpirePeriod,
                IsFree = IsFree,
                Tag = Tag
            };
    }
}
