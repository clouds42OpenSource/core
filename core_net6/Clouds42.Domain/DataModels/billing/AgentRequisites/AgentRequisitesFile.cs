﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Файл реквизитов агента
    /// </summary>
    [Table("AgentRequisitesFiles", Schema = SchemaType.Billing)]
    [PrimaryKey(nameof(AgentRequisitesId), nameof(CloudFileId))]
    public class AgentRequisitesFile
    {
        /// <summary>
        /// Агентский договор
        /// </summary>
        [ForeignKey(nameof(AgentRequisites))]
        [Column(Order = 0)]
        public Guid AgentRequisitesId { get; set; }
        public virtual AgentRequisites AgentRequisites { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        [ForeignKey(nameof(CloudFile))]
        [Column(Order = 1)]
        public Guid CloudFileId { get; set; }
        public virtual CloudFile CloudFile { get; set; }

        /// <summary>
        /// Тип файла реквизитов агента
        /// </summary>
        public AgentRequisitesFileTypeEnum AgentRequisitesFileType { get; set; }
    }
}
