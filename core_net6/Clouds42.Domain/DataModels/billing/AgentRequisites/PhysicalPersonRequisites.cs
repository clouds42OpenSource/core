﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Реквизиты физ. лица
    /// </summary>
    [Table(nameof(PhysicalPersonRequisites), Schema = SchemaType.Billing)]
    public class PhysicalPersonRequisites : BasePersonRequisites
    {
        /// <summary>
        /// ФИО
        /// </summary>
        
        public string FullName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Серия паспорта
        /// </summary>
        
        public string PassportSeries { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        
        public string PassportNumber { get; set; }

        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        
        public string WhomIssuedPassport { get; set; }

        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime? PassportDateOfIssue { get; set; }

        /// <summary>
        /// Адрес прописки
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        
        public string Inn { get; set; }

        /// <summary>
        /// СНИЛС
        /// </summary>
        
        public string Snils { get; set; }
    }
}
