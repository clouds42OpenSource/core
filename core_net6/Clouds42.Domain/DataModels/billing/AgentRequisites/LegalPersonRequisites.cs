﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Реквизиты юр лица
    /// </summary>
    [Table(nameof(LegalPersonRequisites), Schema = SchemaType.Billing)]
    public class LegalPersonRequisites: BasePersonRequisites
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        
        public string OrganizationName { get; set; }

        /// <summary>
        /// ФИО руководителя
        /// </summary>
        
        public string HeadFullName { get; set; }

        /// <summary>
        /// Должность руководителя
        /// </summary>
        
        public string HeadPosition { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        
        public string Ogrn { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        
        public string Kpp { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        
        public string Inn { get; set; }
    }
}
