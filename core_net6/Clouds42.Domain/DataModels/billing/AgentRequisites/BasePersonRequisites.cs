﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Базовый объект реквизитов
    /// </summary>
    public abstract class BasePersonRequisites
    {
        /// <summary>
        /// Расчетный счет
        /// </summary>
        
        public string SettlementAccount { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        
        public string Bik { get; set; }

        /// <summary>
        /// Наименование банка
        /// </summary>
        
        public string BankName { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Адрес для отправки документов
        /// </summary>
        
        public string AddressForSendingDocuments { get; set; }

        /// <summary>
        /// Реквизиты агента
        /// </summary>
        [Key]
        [ForeignKey(nameof(AgentRequisites))]
        public Guid AgentRequisitesId { get; set; }
        public virtual AgentRequisites AgentRequisites { get; set; }
    }
}
