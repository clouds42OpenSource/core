﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Реквизиты ИП
    /// </summary>
    [Table(nameof(SoleProprietorRequisites), Schema = SchemaType.Billing)]
    public class SoleProprietorRequisites : BasePersonRequisites
    {
        /// <summary>
        /// ФИО
        /// </summary>
        
        public string FullName { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        
        public string Ogrn { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        
        public string Inn { get; set; }
    }
}
