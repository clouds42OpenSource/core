﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing.AgentRequisites
{
    /// <summary>
    /// Реквизиты агента
    /// </summary>
    [Table(nameof(AgentRequisites), Schema = SchemaType.Billing)]
    public class AgentRequisites : BaseDomainModel
    {
        public AgentRequisites()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Номер
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Статус реквизитов агента
        /// </summary>
        public AgentRequisitesStatusEnum AgentRequisitesStatus { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }

        /// <summary>
        /// Реквизиты юридического лица
        /// </summary>
        [ForeignKey(nameof(LegalPersonRequisites))]
        public Guid? LegalPersonRequisitesId { get; set; }
        public virtual LegalPersonRequisites LegalPersonRequisites { get; set; }

        /// <summary>
        /// Реквизиты физического лица
        /// </summary>
        [ForeignKey(nameof(PhysicalPersonRequisites))]
        public Guid? PhysicalPersonRequisitesId { get; set; }
        public virtual PhysicalPersonRequisites PhysicalPersonRequisites { get; set; }

        /// <summary>
        /// Реквизиты ИП
        /// </summary>
        [ForeignKey(nameof(SoleProprietorRequisites))]
        public Guid? SoleProprietorRequisitesId { get; set; }
        public virtual SoleProprietorRequisites SoleProprietorRequisites { get; set; }

        /// <summary>
        /// Файлы агентского договора
        /// </summary>
        public virtual ICollection<AgentRequisitesFile> AgentRequisitesFiles { get; set; }
        public virtual ICollection<AgentCashOutRequest> AgentCashOutRequests { get; set; }

        /// <summary>
        /// Аккаунт агента
        /// которому пренадлежат реквизиты
        /// </summary>
        [ForeignKey(nameof(AccountOwner))]
        public Guid AccountOwnerId { get; set; }
        public virtual Account AccountOwner { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; } = DateTime.Now;
    }
}
