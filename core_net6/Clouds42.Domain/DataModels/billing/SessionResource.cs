﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    [Table(nameof(SessionResource), Schema = SchemaType.Billing)]    
    public class SessionResource
    {
        [Key] [ForeignKey(nameof(Resource))] public Guid ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        public int SessionCount { get; set; }
    }
}
