﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    [Table("ArticleTransaction", Schema = SchemaType.Billing)]
    public class ArticleTransaction
    {
        [Key]
        //идентификатор записи транзакции
        public Guid Id { get; set; }
        //идентификатор статьи
        public int ArticleId { get; set; }
        [ForeignKey(nameof(ArticleId))]
        public virtual Article Article { get; set; }
        //причина транзакции за статью или пользователя
        public ArticleTransactionCause Cause { get; set; }
        //тип транзакции начисление/списание
        public ArticleTransactionType TransactionType { get; set; }
        //сумма
        public decimal Amount { get; set; }
        //дата
        public DateTime CreatedOn { get; set; }
    }
}
