﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Расширение сервиса биллинга
    /// </summary>
    public class BillingServiceExtension : BaseDomainModel
    {
        /// <summary>
        /// Название файла разработки 1C
        /// </summary>
        
        [Required]
        public string Service1CFileName { get; set; }
    }
}
