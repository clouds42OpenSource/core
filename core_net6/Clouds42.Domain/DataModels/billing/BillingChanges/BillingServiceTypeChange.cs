﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing.BillingChanges
{
    /// <summary>
    /// Изменения по услуге сервиса биллинга
    /// </summary>
    public class BillingServiceTypeChange : BaseDomainModel
    {
        /// <summary>
        /// ID изменений по сервису биллинга
        /// </summary>
        [ForeignKey(nameof(BillingServiceChanges))]
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Изменения по сервису биллинга
        /// </summary>
        public virtual BillingServiceChanges BillingServiceChanges { get; set; }

        /// <summary>
        /// Услуга сервиса
        /// </summary>
        [ForeignKey(nameof(BillingServiceType))]
        public Guid? BillingServiceTypeId { get; set; }

        public virtual BillingServiceType BillingServiceType { get; set; }

        /// <summary>
        /// Услуга является новой
        /// </summary>
        [DefaultValue(false)]
        public bool IsNew { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        
        public decimal? Cost { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum? BillingType { get; set; }

        /// <summary>
        /// Зависимость от другой услуги
        /// </summary>
        [ForeignKey(nameof(DependServiceType))]
        public Guid? DependServiceTypeId { get; set; }

        public virtual BillingServiceType DependServiceType { get; set; }
    }
}
