﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Domain.DataModels.billing.BillingChanges
{
    /// <summary>
    /// Заявка изменения сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequest : BaseDomainModel
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        [ForeignKey(nameof(BillingService))]
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Сервис биллинга
        /// </summary>
        public virtual BillingService BillingService { get; set; }
        
        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public DateTime StatusDateTime { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public ChangeRequestStatusEnum Status { get; set; }

        /// <summary>
        /// ID модератора
        /// </summary>
        [ForeignKey(nameof(Moderator))]
        public Guid? ModeratorId { get; set; }
        public virtual AccountUser Moderator { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Тип заявки на модерацию
        /// </summary>
        public ChangeRequestTypeEnum ChangeRequestType { get; set; }

        public virtual BillingServiceChanges BillingServiceChanges { get; set; }
        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        [ForeignKey(nameof(BillingServiceChanges))]
        public Guid? BillingServiceChangesId { get; set; }
    }
}
