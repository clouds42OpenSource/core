﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.BillingChanges
{
    /// <summary>
    /// Изменение в зависимости отрасли и сервиса
    /// </summary>
    [Table("IndustryDependencyBillingServiceChanges", Schema = SchemaType.BillingCommits)]
    public class IndustryDependencyBillingServiceChange : BaseDomainModel
    {
        /// <summary>
        /// ID изменений по сервису биллинга
        /// </summary>
        [ForeignKey(nameof(BillingServiceChanges))]
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Изменения по сервису биллинга
        /// </summary>
        public virtual BillingServiceChanges BillingServiceChanges { get; set; }

        /// <summary>
        /// Конфигурация
        /// </summary>
        [Required]
        [ForeignKey(nameof(Industry))]
        [Column(Order = 2)]
        public Guid IndustryId { get; set; }
        public virtual Industry Industry { get; set; }
    }
}
