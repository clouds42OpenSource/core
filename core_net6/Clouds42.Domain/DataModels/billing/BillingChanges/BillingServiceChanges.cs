﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.BillingChanges
{
    /// <summary>
    /// Изменения по сервису биллинга
    /// </summary>
    public class BillingServiceChanges : BaseDomainModel
    {
        /// <summary>
        /// Название сервиса.
        /// </summary>
        
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// Иконка сервиса(измененная при загрузке)
        /// </summary>
        [ForeignKey(nameof(IconCloudFile))]
        public Guid? IconCloudFileId { get; set; }
        public virtual CloudFile IconCloudFile { get; set; }

        /// <summary>
        /// Оригинальная иконка сервиса(исходное загруженное изображение)
        /// </summary>
        [ForeignKey(nameof(OriginalIconCloudFile))]
        public Guid? OriginalIconCloudFileId { get; set; }
        public virtual CloudFile OriginalIconCloudFile { get; set; }

        /// <summary>
        /// признак гибридного сервиса
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// Инструкция сервиса
        /// </summary>
        [ForeignKey(nameof(InstructionServiceCloudFile))]
        public Guid? InstructionServiceCloudFileId { get; set; }
        public virtual CloudFile InstructionServiceCloudFile { get; set; }
        
        /// <summary>
        /// Список изменений по зависимостям отрасли и сервиса
        /// </summary>
        public virtual ICollection<IndustryDependencyBillingServiceChange> IndustryDependencyBillingServiceChanges { get; set; }

        /// <summary>
        /// Список изменений по услугам сервиса
        /// </summary>
        public virtual ICollection<BillingServiceTypeChange> BillingServiceTypeChanges { get; set; }
    }
}
