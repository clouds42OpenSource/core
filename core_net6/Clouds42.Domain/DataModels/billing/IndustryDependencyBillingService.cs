﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Зависимость отрасли и сервиса
    /// </summary>
    [Table("IndustriesDependencyBillingServices", Schema = SchemaType.Billing)]
    [PrimaryKey(nameof(BillingServiceId), nameof(IndustryId))]
    public class IndustryDependencyBillingService
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        [ForeignKey(nameof(BillingService))]
        [Column(Order = 1)]
        public Guid BillingServiceId { get; set; }
        public virtual BillingService BillingService { get; set; }

        /// <summary>
        /// Конфигурация
        /// </summary>
        [Required]
        [ForeignKey(nameof(Industry))]
        [Column(Order = 2)]
        public Guid IndustryId { get; set; }
        public virtual Industry Industry { get; set; }
    }
}
