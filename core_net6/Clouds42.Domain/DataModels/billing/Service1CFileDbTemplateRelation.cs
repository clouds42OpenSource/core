﻿using System;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Связь файла 1С с конфигурацией шаблона на разделителях
    /// </summary>
    public class Service1CFileDbTemplateRelation
    {
        /// <summary>
        /// Id связи
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 1С файл сервиса биллинга
        /// </summary>
        public Guid BillingService1CFileId { get; set; }
        public virtual BillingService1CFile BillingService1CFile { get; set; }

        /// <summary>
        /// Конфигурация на разделителях
        /// </summary>
        public string DbTemplateDelimitersId { get; set; }
        public virtual DbTemplateDelimiters DbTemplateDelimiters { get; set; }
    }
}
