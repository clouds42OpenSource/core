using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    [Table("InvoiceReceipts", Schema = SchemaType.Billing)]
    public class InvoiceReceipt
    {
        [Key] [ForeignKey(nameof(Invoice))] public Guid InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        /// <summary>
        ///     ���� � ����� ������������ ���������
        /// </summary>
        
        public DateTime? DocumentDateTime { get; set; }

        /// <summary>
        ///     ������ ����� ����������� ����
        /// </summary>
        [Required]
        public string ReceiptInfo { get; set; }

        /// <summary>
        ///     QR-��� ����
        /// </summary>
        public string QrCodeData { get; set; }

        /// <summary>
        ///     ������ QR-����
        /// </summary>
        
        public string QrCodeFormat { get; set; }

        /// <summary>
        ///     ����� ����������� ���������
        /// </summary>
        
        public string FiscalNumber { get; set; }

        /// <summary>
        ///     ����� ����������� ����������, � ������� ��� ����������� ��������
        /// </summary>
        
        public string FiscalSerial { get; set; }

        /// <summary>
        ///     ���������� ������� ���������
        /// </summary>
        
        public string FiscalSign { get; set; }
    }
}