﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.billing.BillingChanges;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Пересчет стоимости сервиса после изменения
    /// </summary>
    public class RecalculationServiceCostAfterChange : BaseDomainModel
    {
        public RecalculationServiceCostAfterChange()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Id изменений по сервису биллинга
        /// </summary>
        [ForeignKey(nameof(BillingServiceChanges))]
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Изменения по сервису биллинга
        /// </summary>
        public virtual BillingServiceChanges BillingServiceChanges { get; set; }

        /// <summary>
        /// Id запущенной задачи
        /// на пересчет стоимости сервиса
        /// </summary>
        [ForeignKey(nameof(CoreWorkerTasksQueue))]
        public Guid CoreWorkerTasksQueueId { get; set; }

        /// <summary>
        /// Запущенная задача
        /// на пересчет стоимости сервиса
        /// </summary>
        public virtual CoreWorkerTasksQueue CoreWorkerTasksQueue { get; set; }

        /// <summary>
        /// Старая стоимость сервиса
        /// </summary>
        
        public decimal OldServiceCost { get; set; }

        /// <summary>
        /// Новая стоимость сервиса
        /// </summary>
        
        public decimal NewServiceCost { get; set; }
    }
}
