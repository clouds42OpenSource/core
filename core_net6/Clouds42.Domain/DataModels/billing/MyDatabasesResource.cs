﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Ресурс сервиса "Мои информационные базы"
    /// </summary>
    [Table("MyDatabasesResources", Schema = SchemaType.Billing)]
    public class MyDatabasesResource
    {
        /// <summary>
        /// Id ресурса
        /// </summary>
        [Key]
        [ForeignKey(nameof(Resource))]
        public Guid ResourceId { get; set; }
        public virtual Resource Resource { get; set; }

        /// <summary>
        /// Количество оплаченных информационных баз
        /// </summary>
        public int PaidDatabasesCount { get; set; }

        /// <summary>
        /// Актуальное количество инф. баз
        /// </summary>
        public int ActualDatabasesCount { get; set; }
    }
}
