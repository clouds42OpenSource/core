﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Услуга сервиса облака
    /// </summary>
    public interface IBillingServiceType
    {
        Guid Id { get; set; }

        Guid ServiceId { get; set; }

        /// <summary>
        /// Название услуги.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Описание услуги.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Тип работы биллинга.
        /// </summary>
        BillingTypeEnum BillingType { get; set; }

        ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Зависимость от другой услуги
        /// </summary>
        Guid? DependServiceTypeId { get; set; }

        /// <summary>
        /// Получить обьект сервиса.
        /// </summary>
        /// <returns>Сервис биллинга.</returns>
        IBillingService GetService();

        /// <summary>
        /// Признак что услуга удалена
        /// </summary>
        bool IsDeleted { get; set; }
    }
}