using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Продукт счета
    /// </summary>
    public class InvoiceProduct : BaseDomainModel
    {
        /// <summary>
        /// Название услуги
        /// </summary>
        [Required]
        public string ServiceName { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        
        public decimal ServiceSum { get; set; }

        /// <summary>
        /// Описание продукта счета
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Счет
        /// </summary>
        [ForeignKey(nameof(Invoice))]
        public Guid InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        /// <summary>
        /// Услуга
        /// </summary>
        [ForeignKey(nameof(ServiceType))]
        public Guid? ServiceTypeId { get; set; }
        public virtual BillingServiceType ServiceType { get; set; }
    }
}