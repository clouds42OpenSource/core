﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.DataModels.billing
{
    [Table("ArticleWallets", Schema = SchemaType.Billing)]
    public class ArticleWallets
    {
        [Key]
        public Guid AccountUserId { get; set; }
        [ForeignKey(nameof(AccountUserId))]
        public virtual AccountUser AccountUser { get; set; }
        //сумма доступная к выводу
        public decimal AvailableSum { get; set; }
        //сумма доступная к выводу
        public decimal TotalEarned { get; set; }
    }
}
