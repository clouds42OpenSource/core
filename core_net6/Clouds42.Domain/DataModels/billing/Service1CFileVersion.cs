﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Версия файлов 1С сервиса биллинга
    /// </summary>
    public class Service1CFileVersion : BaseDomainModel
    {
        /// <summary>
        /// Версия 1С файла
        /// </summary>
        [Required]
        
        public string Version1CFile { get; set; }

        /// <summary>
        /// Описание изменений файла 1С
        /// </summary>
        public string DescriptionOf1CFileChanges { get; set; }

        /// <summary>
        /// Комментарий для модератора
        /// </summary>
        public string CommentForModerator { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Уведомить пользователей сервиса об изменениях
        /// </summary>
        public bool NeedNotifyAboutVersionChanges { get; set; }

        /// <summary>
        /// Версия файла разработки актуальная
        /// </summary>
        [DefaultValue(true)]
        public bool IsActualVersion { get; set; }

        /// <summary>
        /// Результат аудита файла 1С
        /// </summary>
        [DefaultValue(AuditService1CFileResultEnum.PendingAudit)]
        public AuditService1CFileResultEnum AuditResult { get; set; }
    }
}
