﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Сервис облака.
    /// </summary>    
    public class BillingService : BaseDomainModel, IBillingService
    {

        /// <summary>
        /// Название сервиса.
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Ключ сервиса
        /// </summary>
        [Required]        
        public Guid Key { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        
        public string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// Иконка сервиса(измененная при загрузке)
        /// </summary>        
        public Guid? IconCloudFileId { get; set; }
        public virtual CloudFile IconCloudFile { get; set; }

        /// <summary>
        /// Оригинальная иконка сервиса(исходное загруженное изображение)
        /// </summary>        
        public Guid? OriginalIconCloudFileId { get; set; }
        public virtual CloudFile OriginalIconCloudFile { get; set; }

        /// <summary>
        /// Инструкция сервиса
        /// </summary>        
        public Guid? InstructionServiceCloudFileId { get; set; }
        public virtual CloudFile InstructionServiceCloudFile { get; set; }

        /// <summary>
        /// Аккаунт которому пренадлежит сервис.
        /// Если не указан значит сервис системный.
        /// </summary>        
        public Guid? AccountOwnerId { get; set; }
        public virtual Account AccountOwner { get; set; }
        
        /// <summary>
        /// Сервис является системным, номер соотвествия.
        /// </summary>
        public Clouds42Service? SystemService { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        [DefaultValue(BillingServiceStatusEnum.Draft)]
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Дата активации сервиса
        /// </summary>
        public DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }

        /// <summary>
        /// Внутренний облачный сервис
        /// </summary>
        public InternalCloudServiceEnum? InternalCloudService { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        [DefaultValue(true)]
        public bool IsActive { get; set; }

        /// <summary>
        /// Признак означающий что сервис - гибридный
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// 1С файлы сервиса
        /// </summary>
        public virtual ICollection<BillingService1CFile> BillingService1CFiles { get; set; }

        /// <summary>
        /// Коллекция сервисов
        /// </summary>
        public virtual ICollection<BillingServiceType> BillingServiceTypes { get; set; }
        
        /// <summary>
        /// Коллекция зависимостей отраслей с сервисом
        /// </summary>
        public virtual ICollection<IndustryDependencyBillingService> IndustriesDependencyBillingService { get; set; }

        public virtual ICollection<ResourcesConfiguration> ResourcesConfigurations { get; set; }
        public virtual ICollection<ServiceExtensionDatabase> ServiceExtensionDatabases { get; set; }

    }
}
