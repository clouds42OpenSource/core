﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Активность услуги сервиса для аккаунта
    /// </summary>
    public class BillingServiceTypeActivityForAccount
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }

        /// <summary>
        /// Сервис биллинга
        /// </summary>
        public virtual BillingServiceType BillingServiceType { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Информационная база
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Дата до которой доступна услуга сервиса
        /// (дата первой пролонгации после удаления услуги)
        /// </summary>
        public DateTime AvailabilityDateTime { get; set; }

        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;
    }
}
