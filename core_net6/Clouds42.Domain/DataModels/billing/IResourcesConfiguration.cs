﻿using System;

namespace Clouds42.Domain.DataModels.billing
{
    public interface IResourcesConfiguration
    {

        Guid Id { get; }

        decimal Cost { get; }

        bool? CostIsFixed { get; }

        DateTime? ExpireDate { get; }

        bool? Frozen { get; }

        int? DiscountGroup { get; }

        DateTime? CreateDate { get; }

        /// <summary>
        /// Признак демо периода.
        /// </summary>
        bool IsDemoPeriod { get; }

        Guid AccountId { get; }                                                

        Guid BillingServiceId { get; }

        DateTime GetExpireDateValue();

        IBillingService GetBillingService();
    }
}