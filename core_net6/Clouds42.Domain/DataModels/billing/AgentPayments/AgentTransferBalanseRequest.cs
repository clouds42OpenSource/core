﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Заявка на перевод баланса агента
    /// </summary>
    public class AgentTransferBalanseRequest : BaseDomainModel
    {
        /// <summary>
        /// Дата создания завявки
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Сумма по заявке
        /// </summary>
        
        public decimal Sum { get; set; }

        /// <summary>
        /// От аккаунта (перевод баланса)
        /// </summary>
        [ForeignKey(nameof(FromAccount))]
        public Guid FromAccountId { get; set; }
        public virtual Account FromAccount { get; set; }

        /// <summary>
        /// Для аккаунта (перевод баласа)
        /// </summary>
        [ForeignKey(nameof(ToAccount))]
        public Guid ToAccountId { get; set; }
        public virtual Account ToAccount { get; set; }

        /// <summary>
        /// Инициатор создания заявки
        /// </summary>
        [ForeignKey(nameof(Initiator))]
        public Guid InitiatorId { get; set; }
        public virtual AccountUser Initiator { get; set; }

        /// <summary>
        /// Комментарий к заявке
        /// </summary>
        public string Comment { get; set; }
    }
}
