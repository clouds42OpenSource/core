﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Связь платежа агента
    /// </summary>
    public class AgentPaymentRelation : BaseDomainModel
    {
        /// <summary>
        /// Аккаунт агента
        /// которому принадлежат платежи
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        
        public decimal Sum { get; set; }
    }
}
