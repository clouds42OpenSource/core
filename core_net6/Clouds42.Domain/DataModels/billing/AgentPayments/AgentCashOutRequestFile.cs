﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Файл заявки на вывод средств агента
    /// </summary>
    [Table("AgentCashOutRequestFiles", Schema = SchemaType.Billing)]
    [PrimaryKey(nameof(AgentCashOutRequestId), nameof(CloudFileId))]
    public class AgentCashOutRequestFile
    {
        /// <summary>
        /// Заявка на вывод средств
        /// </summary>
        [ForeignKey(nameof(AgentCashOutRequest))]
        [Key]
        [Column(Order = 0)]
        public Guid AgentCashOutRequestId { get; set; }
        public virtual AgentCashOutRequest AgentCashOutRequest { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        [ForeignKey(nameof(CloudFile))]
        [Column(Order = 1)]
        public Guid CloudFileId { get; set; }
        public virtual CloudFile CloudFile { get; set; }
    }
}
