﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Заявка на вывод средств агента
    /// </summary>    
    public class AgentCashOutRequest : BaseDomainModel
    {
        /// <summary>
        /// Номер заявки
        /// </summary>
        [Required]
        
        public string RequestNumber { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Дата создания завявки
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum RequestStatus { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }

        /// <summary>
        /// Реквизиты
        /// которым принадлежит заявка
        /// </summary>        
        public Guid AgentRequisitesId { get; set; }
        public virtual AgentRequisites.AgentRequisites AgentRequisites { get; set; }

        /// <summary>
        /// Сумма по заявке
        /// </summary>        
        public decimal RequestedSum { get; set; }

        /// <summary>
        /// Файлы заявки
        /// </summary>
        public virtual ICollection<AgentCashOutRequestFile> AgentCashOutRequestFiles { get; set; }

        public Guid? AccountUserId { get; set; }

    }
}
