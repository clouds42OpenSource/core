﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Кошелек агента
    /// </summary>
    [Table("AgentWallets", Schema = SchemaType.Billing)]
    public class AgentWallet
    {
        /// <summary>
        /// Аккаунт агента
        /// которому принадлежит кошелек
        /// </summary>
        [Key]
        [ForeignKey(nameof(AccountOwner))]
        public Guid AccountOwnerId { get; set; }
        public virtual Account AccountOwner { get; set; }

        /// <summary>
        /// Доступная сумма
        /// </summary>
        
        public decimal AvailableSum { get; set; }

        /// <summary>
        /// Дата принятия агентского соглашения
        /// </summary>
        public DateTime? ApplyAgencyAgreementDate { get; set; }
    }
}
