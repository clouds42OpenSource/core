﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing.AgentPayments
{
    /// <summary>
    /// Платежи агента
    /// </summary>
    public class AgentPayment : BaseDomainModel
    {
        /// <summary>
        /// Аккаунт агента
        /// которому принадлежат платежи
        /// </summary>
        [ForeignKey(nameof(AccountOwner))]
        public Guid AccountOwnerId { get; set; }
        public virtual Account AccountOwner { get; set; }

        /// <summary>
        /// Id клиентского платежа.
        /// </summary>
        [ForeignKey(nameof(Payment))]
        public Guid? ClientPaymentId { get; set; }
        public virtual Payment Payment { get; set; }

        /// <summary>
        /// Связь агентского платежа
        /// </summary>
        [ForeignKey(nameof(AgentPaymentRelation))]
        public Guid? AgentPaymentRelationId { get; set; }
        public virtual AgentPaymentRelation AgentPaymentRelation { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// Тип источника агентского платежа
        /// </summary>
        public AgentPaymentSourceTypeEnum AgentPaymentSourceType { get; set; }

        /// <summary>
        /// Дата совершения платежа
        /// </summary>
        public DateTime PaymentDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Время ввода платежа (Ручной ввод)
        /// </summary>
        public DateTime? PaymentEntryDateTime { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        
        public decimal Sum { get; set; }

        /// <summary>
        /// Коментарий к платежу.
        /// </summary>
        
        public string Comment { get; set; }
    }
}
