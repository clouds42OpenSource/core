﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Сервис облака.
    /// </summary>
    public interface IBillingService
    {
        /// <summary>
        /// Номер сервиса.
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>        
        string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>        
        string Opportunities { get; set; }

        /// <summary>
        /// Иконка сервиса.
        /// </summary>
        Guid? IconCloudFileId { get; set; }        

        /// <summary>
        /// Аккаунт которому пренадлежит сервис.
        /// Если не указан значит сервис системный.
        /// </summary>

        Guid? AccountOwnerId { get; set; }

        /// <summary>
        /// Сервис является системным, номер соотвествия.
        /// </summary>
        Clouds42Service? SystemService { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Дата активации сервиса
        /// </summary>
        DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        bool IsActive { get; set; }

        /// <summary>
        /// Признак означающий что сервис - гибридный
        /// </summary>
        public bool IsHybridService { get; set; }
    }
}
