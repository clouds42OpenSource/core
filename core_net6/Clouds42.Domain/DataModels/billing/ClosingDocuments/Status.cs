﻿namespace Clouds42.Domain.DataModels.billing.ClosingDocuments;


/// <summary>
/// Статус обработки акта
/// </summary>
public enum Status
{
    // новый документ, либо старый, который был до фичи
    New = 0,
    // если после обработки со стороны корпа пришла ошибка
    CorpError = 1,
    // менеджер отклонил файл
    Rejected = 2,
    // менеджер принял файл
    Accepted = 3,
    // файл в процессе рассмотрения
    Processing = 4,
    // файл обработан
    Processed = 5,
}
