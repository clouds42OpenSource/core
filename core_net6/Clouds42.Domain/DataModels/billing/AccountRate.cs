﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Тариф услуги на аккаунт
    /// </summary>
    [Table("AccountRates", Schema = SchemaType.Billing)]
    public class AccountRate
    {
        /// <summary>
        /// Id тарифа
        /// </summary>
        [Key]
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Id услуги сервиса
        /// </summary>
        [ForeignKey(nameof(BillingServiceType))]
        [Required]
        public Guid BillingServiceTypeId { get; set; }
        public virtual BillingServiceType BillingServiceType { get; set; }

        /// <summary>
        /// Стоимость тарифа на аккаунт
        /// </summary>
        
        public decimal Cost { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Required]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}