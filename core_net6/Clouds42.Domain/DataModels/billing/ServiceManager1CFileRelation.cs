﻿using System;

namespace Clouds42.Domain.DataModels.billing
{
    /// <summary>
    /// Связь файла разработки сервиса с загруженным файлом разработки в МС
    /// </summary>    
    public class ServiceManager1CFileRelation
    {
        /// <summary>
        /// Id связи
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 1С файл сервиса биллинга
        /// </summary>        
        public Guid BillingService1CFileId { get; set; }
        public virtual BillingService1CFile BillingService1CFile { get; set; }

        public Guid ServiceManagerUploadedFileId { get; set; }
    }
}
