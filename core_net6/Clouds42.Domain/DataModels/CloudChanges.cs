using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    public class CloudChanges : BaseDomainModel
    {
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey(nameof(AccountUser))]
        public Guid? Initiator { get; set; }
        public virtual AccountUser AccountUser { get; set; }

        [ForeignKey(nameof(CloudChangesAction))]
        public Guid Action { get; set; }
        public virtual CloudChangesAction CloudChangesAction { get; set; }

        [Required]
        public string Description { get; set; }
    }
}