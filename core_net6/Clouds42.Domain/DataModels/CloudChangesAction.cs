﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    [Table("CloudChangesActions")]
    public class CloudChangesAction
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey(nameof(CloudChangesGroup))]
        public Guid GroupId { get; set; }
        public virtual CloudChangesGroup CloudChangesGroup { get; set; }

         [Required] public string Name { get; set; }

        public short Index { get; set; } = 0;
        
        public virtual ICollection<CloudChanges> CloudChanges { get; set; }

        [NotMapped]
        public LogActions IndexEnum
        {
            get { return (LogActions) Index; }
            set { Index = (short) value; }
        }
    }
}
