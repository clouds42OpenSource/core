﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Отрасль
    /// </summary>
    public class Industry : IIndustry
    {
        /// <summary>
        /// Id отрасли
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название отрасли
        /// </summary>
        [Required]
        
        public string Name { get; set; }

        /// <summary>
        /// Описание отрасли
        /// </summary>
        [StringLength(int.MaxValue)]
        public string Description { get; set; }

        /// <summary>
        /// Коллекция зависимостей отрасли с сервисами
        /// </summary>
        public virtual ICollection<IndustryDependencyBillingService> IndustryDependencyBillingServices { get; set; }
    }
}
