﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Модель оказанных услуг
    /// </summary>
    public class ProvidedService : BaseDomainModel
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime CreationDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Аккаунт споносора
        /// </summary>
        [ForeignKey(nameof(SponsoredAccount))]
        public Guid? SponsoredAccountId { get; set; }
        public virtual Account SponsoredAccount { get; set; }

        /// <summary>
        /// Тариф
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Тип сервиса
        /// </summary>
        
        [Required]
        public string Service { get; set; }

        /// <summary>
        /// Тип услуги сервиса
        /// </summary>
        
        public string ServiceType { get; set; }

        /// <summary>
        /// Начальный период действия
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Конечный период действия
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Тип предоставления услуги
        /// </summary>
        public KindOfServiceProvisionEnum KindOfServiceProvision { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        
        public string Comment { get; set; }

        /// <summary>
        /// Тип услуги сервиса
        /// </summary>
        [NotMapped]
        public ResourceType ServiceTypeEnum
        {
            get { return (ResourceType) Enum.Parse(typeof(ResourceType), ServiceType); }
            set { ServiceType = value.ToString(); }
        }

        /// <summary>
        /// Тип сервиса
        /// </summary>
        [NotMapped]
        public Clouds42Service ServiceEnum
        {
            get { return (Clouds42Service) Enum.Parse(typeof(Clouds42Service), Service); }
            set { Service = value.ToString(); }
        }
    }
}