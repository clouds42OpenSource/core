﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Локаль
    /// </summary>
    [Table(nameof(Locale))]
    public class Locale : ILocale
    {
        /// <summary>
        /// ID локали
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        
        [Required]
        public string Currency { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        public int? CurrencyCode { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        
        [Required]
        public string Country { get; set; }

        /// <summary>
        /// Конфигурация локали
        /// </summary>
        public virtual LocaleConfiguration LocaleConfiguration { get; set; }

        /// <summary>
        /// Тарифы локали
        /// </summary>
        public virtual ICollection<Rate> Rates { get; set; }

        /// <summary>
        /// Шаблоны локали
        /// </summary>
        public virtual ICollection<DbTemplate> DbTemplates { get; set; }

        /// <summary>
        /// Аккаунты локали
        /// </summary>
        public virtual ICollection<AccountConfiguration> AccountConfigurations { get; set; }
        public virtual ICollection<AdvertisingBannerAudienceLocaleRelation> AccountConAdvertisingBannerAudienceLocaleRelations { get; set; }
        public virtual ICollection<CloudLocalization> CloudLocalizations { get; set; }
        public virtual ICollection<Supplier.Supplier> Suppliers { get; set; }
    }
}
