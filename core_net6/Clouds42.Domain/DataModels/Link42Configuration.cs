﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("Link42Configurations")]
    public class Link42Configuration : BaseDomainModel
    {
        public string Version { get; set; }
        public bool IsCurrentVersion { get; set; }
        public int DownloadsLimit { get; set; }
        public int DownloadsCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public virtual AccountUser CreatedByAccountUser { get; set; }
        public Guid? CreatedBy { get; set; }
        public virtual AccountUser UpdatedByAccountUser { get; set; }
        public Guid? UpdatedBy { get; set; }
        public virtual ICollection<Link42ConfigurationBitDepth> BitDepths { get; set; }
    }

    [Table("Link42ConfigurationBitDepths")]
    public class Link42ConfigurationBitDepth : BaseDomainModel
    {
        public SystemType SystemType { get; set; }
        public LinkAppType LinkAppType { get; set; }
        public BitDepth BitDepth { get; set; }
        public string DownloadLink { get; set; }
        public virtual Link42Configuration Configuration { get; set; }
        public int MinSupportedMajorVersion { get; set; }
        public int MinSupportedMinorVersion { get; set; }

        [ForeignKey(nameof(Configuration))]
        public Guid ConfigurationId { get; set; }
    }

    public enum BitDepth
    {
        X86 = 1,
        X64 = 2,
        Arm
    }

    public enum LinkAppType
    {
        CloudApp = 1,
        ClientApp = 2,
        CmdApp = 3,
    }

    public enum SystemType
    {
        Windows = 1,
        MacOs = 2,
        Linux = 3,
    }
}
