using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    [Table("MigrationHistories", Schema = SchemaType.Migration)]
    public class MigrationHistory
    {
        [Key]
        public Guid HistoryId { get; set; }
        
        /// <summary>
        ///     ���� � ����� ������ ��������
        /// </summary>
        public DateTime StartDateTime { get; set; }

        /// <summary>
        ///     ���� � ����� ��������� ��������
        /// </summary>
        public DateTime FinishDateTime { get; set; }

        /// <summary>
        ///     ������ ��������
        /// </summary>
        public MigrationStatusEnum Status { get; set; }

        [ForeignKey(nameof(AccountUser))]
        public Guid InitiatorId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
    }
}