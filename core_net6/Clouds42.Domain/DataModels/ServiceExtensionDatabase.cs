﻿using System;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Расширение сервиса для информационной базы
    /// </summary>    
    public class ServiceExtensionDatabase
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id сервиса биллинга (BillingService property)
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Сервис биллинга
        /// </summary>
        public virtual BillingService BillingService { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>        
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Информационная база
        /// </summary>
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Статус расширения сервиса для информационной базы
        /// </summary>
        public ServiceExtensionDatabaseStatusEnum ServiceExtensionDatabaseStatus { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public DateTime? StateDate { get; set; }

        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime CreationDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
