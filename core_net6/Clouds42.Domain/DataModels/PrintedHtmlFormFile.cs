﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Файл печатной формы
    /// </summary>
    [Table("PrintedHtmlFormFiles")]
    [PrimaryKey(nameof(PrintedHtmlFormId), nameof(CloudFileId))]
    public class PrintedHtmlFormFile
    {
        /// <summary>
        /// Печатная форма Html
        /// </summary>
        [ForeignKey(nameof(PrintedHtmlForm))]
        [Column(Order = 0)]
        public Guid PrintedHtmlFormId { get; set; }
        public virtual PrintedHtmlForm PrintedHtmlForm { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        [ForeignKey(nameof(CloudFile))]
        [Column(Order = 1)]
        public Guid CloudFileId { get; set; }
        public virtual CloudFile CloudFile { get; set; }
    }
}
