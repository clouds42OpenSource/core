﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace Clouds42.Domain.DataModels.WaitInfos;

public class WaitInfo
{
    // Идентификатор записи
    public int Id { get; set; }
        
    // Тип ожидания
    public string WaitType { get; set; }
        
    // Описание ожидания
    public string Description { get; set; }

    // Список причин ожидания, не сохраняется напрямую в базу данных
    [NotMapped]
    public List<string> Causes 
    {
        get => JsonSerializer.Deserialize<List<string>>(CausesJson ?? "[]");
        set => CausesJson = JsonSerializer.Serialize(value);
    }

    // Список рекомендаций, не сохраняется напрямую в базу данных
    [NotMapped]
    public List<string> Recommendations 
    {
        get => JsonSerializer.Deserialize<List<string>>(RecommendationsJson ?? "[]");
        set => RecommendationsJson = JsonSerializer.Serialize(value);
    }

    // JSON-представление списка причин ожидания, сохраняется в базу данных
    public string CausesJson { get; set; }
        
    // JSON-представление списка рекомендаций, сохраняется в базу данных
    public string RecommendationsJson { get; set; }
}
