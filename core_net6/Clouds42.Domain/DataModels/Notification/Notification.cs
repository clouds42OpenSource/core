﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Notification
{
    [Table("Notifications")]
    public class Notification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSent { get; set; }
        public DateTime CreatedOn { get; set; }
        public NotificationState State { get; set;}
    }

    public enum NotificationState
    {
        Normal = 0,
        Critical = 1,
        Information = 2,
        Feedback = 3,
    }
}
