﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Notification
{
    [Table("NotificationSettings")]
    public class NotificationSettings: BaseDomainModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public new Guid Id { get; set; }

        public NotificationType Type { get; set; }

        [MaxLength(50)]
        public string TypeText { get; set; }
        public bool IsActive { get; set; }
    }

    [Table("AccountUserNotificationSettings")]
    public class AccountUserNotificationSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }
        public Guid NotificationSettingsId { get; set; }
        public virtual NotificationSettings NotificationSettings { get; set; }
        public bool IsActive { get; set; }
        public long? ChatId { get; set; }
        public string Code { get; set; }
    }

    public enum NotificationType
    {
        Email = 1,
        Sms,
        Telegram,
        WhatsApp
    }
}
