﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Материнская база разделителей
    /// </summary>
    [Table("DelimiterSourceAccountDatabases")]
    [PrimaryKey(nameof(AccountDatabaseId), nameof(DbTemplateDelimiterCode))]
    public class DelimiterSourceAccountDatabase
    {
        /// <summary>
        ///  Id информационной базы
        /// </summary>
        [ForeignKey(nameof(AccountDatabase))]
        [Column(Order = 0)]
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Код конфигурации базы на разделителях
        /// </summary>
        [ForeignKey(nameof(DbTemplateDelimiter))]
        [Column(Order = 1)]
        public string DbTemplateDelimiterCode { get; set; }
        public virtual DbTemplateDelimiters DbTemplateDelimiter { get; set; }

        /// <summary>
        /// Адрес публикации базы на разделителях
        /// </summary>
        public string DatabaseOnDelimitersPublicationAddress { get; set; }
    }
}
