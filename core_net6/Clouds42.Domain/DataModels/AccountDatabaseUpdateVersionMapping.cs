﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Domain.DataModels
{
    [Table("AccountDatabaseUpdateVersionMappings")]
    [PrimaryKey(nameof(ConfigurationName), nameof(CfuId), nameof(TargetVersion))]
    public class AccountDatabaseUpdateVersionMapping
    {
        /// <summary>
        ///     Конфигурация 1С
        /// </summary>
        [Column(Order = 0)]
        [ForeignKey(nameof(Configurations1C))]
        public string ConfigurationName { get; set; }
        public virtual Configurations1C Configurations1C { get; set; }

        /// <summary>
        ///     Номер пакета обновления
        /// </summary>
        [Column(Order = 1)]
        [ForeignKey(nameof(ConfigurationsCfu))]
        public Guid CfuId { get; set; }
        public virtual ConfigurationsCfu ConfigurationsCfu { get; set; }

        /// <summary>
        ///     Версия с которой можно обновиться до версии пакета CfuId
        /// </summary>
        [Column(Order = 2)]
        public string TargetVersion { get; set; }
    }
}
