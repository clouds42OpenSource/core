﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Операция с доступом к базе на разделителях
    /// </summary>
    [Table("ManageDbOnDelimitersAccesses", Schema = SchemaType.AccessDatabase)]
    public class ManageDbOnDelimitersAccess
    {
        /// <summary>
        /// ID записи
        /// </summary>
        [ForeignKey(nameof(AcDbAccess))]
        [Key]
        public Guid Id { get; set; }
        
        /// <summary>
        /// Доступ пользователя
        /// </summary>
        public virtual AcDbAccess AcDbAccess { get; set; }

        /// <summary>
        /// Состояние доступа
        /// </summary>
        public AccountDatabaseAccessState AccessState { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime StateUpdateDateTime { get; set; }

        /// <summary>
        /// Причина задержки обработки
        /// </summary>
        public string ProcessingDelayReason { get; set; }
    }
}
