using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("MigrationAccountHistories", Schema = SchemaType.Migration)]
    public class MigrationAccountHistory
    {
        [Key]
        [ForeignKey(nameof(MigrationHistory))]
        public Guid HistoryId { get; set; }
        public virtual MigrationHistory MigrationHistory { get; set; }

        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        [ForeignKey(nameof(TargetSegment))]
        public Guid TargetSegmentId { get; set; }
        public virtual CloudServicesSegment TargetSegment { get; set; }

        [ForeignKey(nameof(SourceSegment))]
        public Guid SourceSegmentId { get; set; }
        public virtual CloudServicesSegment SourceSegment { get; set; }
    }
}