﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Таблица для настройки облака
    /// </summary>
    [Table(nameof(CloudCore))]
    public class CloudCore
    {
        /// <summary>
        /// Id записи(первичный ключ)
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid PrimaryKey { get; set; }

        /// <summary>
        /// Уведомление для главной страницы
        /// </summary>
        public string MainPageNotification { get; set; }

        /// <summary>
        /// Часовая разница Украины от Москвы
        /// </summary>
        public int? HourseDifferenceOfUkraineAndMoscow { get; set; }

        /// <summary>
        /// Платежный агрегатор русской локали
        /// </summary>
        public RussianLocalePaymentAggregatorEnum RussianLocalePaymentAggregator { get; set; }
        
        /// <summary>
        /// Путь до баннера
        /// </summary>
        public string BannerUrl { get; set; }
    }
}
