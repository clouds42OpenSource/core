﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Файловое хранилище облака
    /// </summary>
    [Table("CloudServicesFileStorageServers")]
    public class CloudServicesFileStorageServer : IConnection, ICloudServicesFileStorageServer
    {
        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Адрес подключения к файловому хранилищу
        /// </summary>
        
        [Required]
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Описание файлового хранилища
        /// </summary>
        
        public string Description { get; set; }

        /// <summary>
        /// Название файлового хранилища
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Физический путь файлового хранилища
        /// </summary>
        
        [Required]
        public string PhysicalPath { get; set; }

        /// <summary>
        /// DNS имя хранилища
        /// </summary>
        public string DnsName { get; set; }
    }
}