﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.WebSockets
{
    public class WebSocketConnection: BaseDomainModel
    {
        [ForeignKey(nameof(AccountUser))]
        [Required]
        public Guid AccountUserId { get; set; }
        public virtual AccountUser AccountUser { get; set; }

        
        public string WebSocketId { get; set; }

        public DateTime ConnectionOpenDateTime { get; set; } = DateTime.Now;

        public DateTime? ConnectionCloseeDateTime { get; set; }

    }

}
