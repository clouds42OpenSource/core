﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// таблица с адресами до папок с обновлятором
    /// </summary>
    public class AutoUpdateNode
    {
        /// <summary>
        /// id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Адрес ноды
        /// </summary>
        public string NodeAddress { get; set; }

        /// <summary>
        /// Путь до авто обновлятора
        /// </summary>
        [Required] 
        public string AutoUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// Путь до ручного обновлятора 
        /// </summary>
        public string ManualUpdateObnovlyatorPath { get; set; }

        /// <summary>
        /// флаг отражающий участие в общем АО
        /// </summary>
        public bool DontRunInGlobalAU { get; set; }

        /// <summary>
        /// Занят воркер или нет?
        /// </summary>
        public bool IsBusy { get; set; }

        /// <summary>
        /// Тип ноды
        /// </summary>
        public AutoUpdateNodeType AutoUpdateNodeType { get; set; }
    }
}
