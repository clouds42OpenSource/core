using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// ��������: ���� ������������.
    /// �������: ����� �����������
    /// </summary>    
    public class CloudConfiguration : BaseDomainModel
    {
        public CloudConfiguration()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// ����
        /// </summary>
        
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// �������� ����������
        /// </summary>        
        public Guid? ContextSupplierId { get; set; }
        public virtual Supplier.Supplier ContextSupplier { get; set; }

        /// <summary>
        /// ��������
        /// </summary>
        [Required]
        public string Value { get; set; }
    }
}