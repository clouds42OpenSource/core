﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Конфигурация инф. базы
    /// </summary>
    [Table(nameof(Configurations1C))]
    public class Configurations1C : IConfigurations1C
    {

        /// <summary>
        /// Название конфигурации
        /// </summary>
        [Key]

        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Каталог конфигурации на ИТС
        /// </summary>

        [Required]
        public string ConfigurationCatalog { get; set; }

        /// <summary>
        /// Редакция
        /// </summary>

        [Required]
        public string RedactionCatalogs { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>

        [Required]
        public string PlatformCatalog { get; set; }

        /// <summary>
        /// Адрес каталога обновлений.
        /// </summary>

        [Required]
        public string UpdateCatalogUrl { get; set; }

        /// <summary>
        /// Краткий код конфигурации, например bp, zup и тд.
        /// </summary>

        public string ShortCode { get; set; }

        /// <summary>
        /// Признак что необходимо использовать COM соединения для принятия обновлений в базе
        /// </summary>
        [Required]
        public bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Признак необходимости проверять наличие обновлений
        /// </summary>
        [Required]
        public bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// Вариации названия
        /// </summary>
        [NotMapped]
        public List<string> ConfigurationVariations { get; set; }

        /// <summary>
        /// Карта маппинга для обновления конфигурации
        /// </summary>
        public virtual ICollection<AccountDatabaseUpdateVersionMapping> AccountDatabaseUpdateVersionMappings { get; set; }

        /// <summary>
        /// Версии пакетов cfu
        /// </summary>
        public virtual ICollection<ConfigurationsCfu> ConfigurationsCfus { get; set; }

        /// <summary>
        /// Список баз на поддержке, использующих данную конфигурацию
        /// </summary>
        public virtual ICollection<AcDbSupport> AcDbSupports { get; set; }

        /// <summary>
        /// Вариации названия
        /// </summary>
        public virtual ICollection<ConfigurationNameVariation> ConfigurationNameVariations { get; set; }

        /// <summary>
        /// Связь конфигурации 1С с услугой сервиса
        /// </summary>
        public virtual ConfigurationServiceTypeRelation ConfigurationServiceTypeRelation { get; set; }


        /// <summary>
        /// ID данных авторизации в ИТС
        /// </summary>
        [ForeignKey(nameof(ItsAuthorizationData))]
        public Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Связь с моделью данных авторизации в ИТС
        /// </summary>
        public virtual ItsAuthorizationData ItsAuthorizationData { get; set; }
    }
}
