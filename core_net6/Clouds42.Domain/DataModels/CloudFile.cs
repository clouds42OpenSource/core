﻿using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Domain.DataModels
{
    public class CloudFile : BaseDomainModel
    {
        
        public string FileName { get; set; }
        
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public virtual ICollection<BillingService> BillingServices { get; set; }
        public virtual ICollection<BillingService> BillingServices2 { get; set; }
        public virtual ICollection<BillingService> BillingServices3 { get; set; }
        public virtual ICollection<Supplier.Supplier> Suppliers { get; set; }
    }
}
