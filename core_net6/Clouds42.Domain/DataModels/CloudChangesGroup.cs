﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("CloudChangesGroups")]
    public class CloudChangesGroup
    {
        [Key]
        public Guid Id { get; set; }

        [Required] 
        public string GroupName { get; set; }

        public virtual ICollection<CloudChangesAction> CloudChangesActions { get; set; }
    }
}
