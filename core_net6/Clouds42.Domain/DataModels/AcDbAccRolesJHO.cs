﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Роли ЖХО
    /// </summary>
    public class AcDbAccRolesJho : BaseDomainModel
    {
        /// <summary>
        /// Доступ в инф. базу
        /// </summary>
        [ForeignKey(nameof(AcDbAccess))]
        public Guid AcDbAccessesID { get; set; }
        public virtual AcDbAccess AcDbAccess { get; set; }

        /// <summary>
        /// Роли ЖХО
        /// </summary>
        [Required]
        
        public string RoleJHO { get; set; }
    }
}
