﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Конфигурация локали
    /// </summary>
    [Table("LocaleConfigurations", Schema = SchemaType.Configuration)]
    public class LocaleConfiguration
    {
        /// <summary>
        /// Id локали
        /// </summary>
        [Key]
        [ForeignKey(nameof(Locale))]
        public Guid LocaleId { get; set; }
        public virtual Locale Locale { get; set; }

        /// <summary>
        /// Адрес сайта личного кабинета
        /// </summary>
        public string CpSiteUrl { get; set; }

        /// <summary>
        /// ID сегмента по умолчанию
        /// </summary>
        [ForeignKey(nameof(DefaultSegment))]
        public Guid DefaultSegmentId { get; set; }
        public virtual CloudServicesSegment DefaultSegment { get; set; }

        /// <summary>
        /// Дата изменения записи
        /// </summary>
        public DateTime ChangeDate { get; set; }
    }
}
