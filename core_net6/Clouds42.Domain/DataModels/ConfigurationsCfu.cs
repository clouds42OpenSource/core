﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    public class ConfigurationsCfu : BaseDomainModel, IConfigurationsCfu
    {

        [Required]
        public string Version { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }
        public int Build { get; set; }

        public string Platform1CVersion { get; set; }

        public DateTime? DownloadDate { get; set; }

        public bool? ValidateState { get; set; }

        [Required]
        public string FilePath { get; set; }

        /// <summary>
        ///     Ответственный за применение\отклонение релиза
        /// </summary>
        [ForeignKey(nameof(Initiator))]
        public Guid? InitiatorId { get; set; }
        public virtual AccountUser Initiator { get; set; }


        [Required]
        [ForeignKey(nameof(Configurations1C))]
        public string ConfigurationName { get; set; }
        public virtual Configurations1C Configurations1C { get; set; }

        public virtual ICollection<AccountDatabaseUpdateVersionMapping> AccountDatabaseUpdateVersionMappings { get; set; }
    }
}
