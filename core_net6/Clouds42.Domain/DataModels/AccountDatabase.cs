﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Модель инф. базы
    /// </summary>
    [Table(nameof(AccountDatabases))]
    public class AccountDatabase :  IAccountDatabase
    {
        public AccountDatabase()
        {
            LastActivityDate = DateTime.Now;
            CreationDate = DateTime.Now;
            LastEditedDateTime = DateTime.Now;
            State = "New";
            DbNumber = 1;
            SizeInMB = 1;
            PublishStateEnum  = Enums.DataBases.PublishState.Unpublished;
            DistributionTypeEnum = Enums.DataBases.DistributionType.Stable;
        }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
		
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        
        public string Caption { get; set; }

        /// <summary>
        /// Статус инф. базы
        /// </summary>
        
        [Required]
        public string State { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        
        [Required]
        public DateTime StateDateTime { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public int DbNumber { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        
        public string ServiceName { get; set; }

        /// <summary>
        /// Название базы на SQL сервере
        /// </summary>
        
        public string SqlName { get; set; }

        /// <summary>
        /// Номер инф. базы
        /// </summary>
        
        public string V82Name { get; set; }

        /// <summary>
        /// Сервер предприятия
        /// </summary>
        
        public string V82Server { get; set; }

        /// <summary>
        /// Размер инф. базы в Мб
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Состояние блокировки
        /// </summary>
        
        
        public string LockedState { get; set; }

        /// <summary>
        /// Признак что база файловая
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// Путь к архивной копии
        /// </summary>
        
        public string ArchivePath { get; set; }

        /// <summary>
        /// Статус публикации
        /// </summary>
        [Required]
        public string PublishState { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        [Column("Platform")]
        
        public string ApplicationName { get; set; }

        /// <summary>
        /// Тип распространения (альфа/стабильная)
        /// </summary>
        [Required]
        
        public string DistributionType { get; set; }
        
        /// <summary>
        /// Тип запуска
        /// </summary>
        public int? LaunchType { get; set; }

        /// <summary>
        /// Дата последнего рассчета занимаемого места
        /// </summary>
        public DateTime? CalculateSizeDateTime { get; set; }
        
        /// <summary>
        ///  todo Рудимент, нужно выкосить
        /// ID файлового хранилища
        /// </summary>
        [ForeignKey(nameof(CloudServicesFileStorageServer))]
        public Guid? FileStorageID { get; set; }
        public virtual CloudServicesFileStorageServer CloudServicesFileStorageServer { get; set; }

        /// <summary>
        /// Параметры запуска
        /// </summary>
        public string LaunchParameters { get; set; }

        /// <summary>
        /// Признак что в базе используются веб сервисы
        /// </summary>
        public bool? UsedWebServices { get; set; }
        
        /// <summary>
        /// Путь к файлам базы в склепе
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// Дата последнего редактирования
        /// </summary>
        
        public DateTime LastEditedDateTime { get; set; }

        /// <summary>
        /// Комментарий по созданию базы
        /// </summary>
        public string CreateAccountDatabaseComment { get; set; }

        /// <summary>
        /// Шаблон инф. базы
        /// </summary>
        [ForeignKey(nameof(DbTemplate))]
        public Guid? TemplateId { get; set; }
        public virtual DbTemplate DbTemplate { get; set; }
        
        /// <summary>
        /// Аккаунт владельца инф. базы
        /// </summary>
        [ForeignKey(nameof(Account))]
        [Column("OwnerId")]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Модель поддержки инф. базы
        /// </summary>
        public virtual AcDbSupport AcDbSupport { get; set; }

        /// <summary>
        /// Связь с инф. базой на разделителях
        /// </summary>
		public virtual AccountDatabaseOnDelimiters AccountDatabaseOnDelimiter { get; set; }
		public virtual ICollection<AccountDatabaseOnDelimiters> AccountDatabaseOnDelimiters { get; set; }

        /// <summary>
        /// Материнские базы разделителей
        /// </summary>
        public virtual ICollection<DelimiterSourceAccountDatabase> DelimiterSourceAccountDatabases { get; set; }
        
        /// <summary>
        /// Список доступов к инф. базе
        /// </summary>
        public virtual ICollection<AcDbAccess> AcDbAccesses { get; set; }
        
        /// <summary>
        /// История миграции инф. базы
        /// </summary>
        public virtual ICollection<MigrationAccountDatabaseHistory> MigrationAccountDatabaseHistories { get; set; }
        
        /// <summary>
        /// Список бэкапов инф. базы
        /// </summary>
        public virtual ICollection<AccountDatabaseBackup> AccountDatabaseBackups { get; set; }

        /// <summary>
        /// Завершение сеансов в инф. базе
        /// </summary>
        public virtual ICollection<TerminationSessionsInDatabase> TerminationDatabaseSessions { get; set; }
        public virtual ICollection<ServiceExtensionDatabase> ServiceExtensionDatabases { get; set; }

        /// <summary>
        /// Тип платформы
        /// </summary>
		[NotMapped]
        public PlatformType PlatformType
        {
            get
            {
                var platformType = ApplicationName.ToPlatformTypeEnum();
                return platformType == PlatformType.Undefined ? 
                    PlatformType.V82 : 
                    platformType;
            }
        }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }

        /// <summary>
        /// Статус инф. базы
        /// </summary>
        [NotMapped]
        public DatabaseState StateEnum
        {
            get { return (DatabaseState) Enum.Parse(typeof(DatabaseState), State); }
            set
            {
                State = value.ToString();
                StateDateTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Статус публикации
        /// </summary>
        [NotMapped]
        public PublishState PublishStateEnum
        {
            get { return (PublishState) Enum.Parse(typeof(PublishState), PublishState); }
            set { PublishState = value.ToString(); }
        }

        /// <summary>
        /// Тип распространения
        /// </summary>
        [NotMapped]
        public DistributionType DistributionTypeEnum
        {
            get { return (DistributionType)Enum.Parse(typeof(DistributionType), DistributionType); }
            set { DistributionType = value.ToString(); }
        }
    }
}
