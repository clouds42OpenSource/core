﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Буфер уведомлений
    /// </summary>
    public class NotificationBuffer : BaseDomainModel
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        [ForeignKey(nameof(Account1))]
        public Guid Account { get; set; }
        public virtual Account Account1 { get; set; }

        /// <summary>
        /// Ключ уведомления
        /// </summary>
        public string NotifyKey { get; set; }

        /// <summary>
        /// Дата действия
        /// </summary>
        public DateTime? ActualPeriod { get; set; }

        /// <summary>
        /// Количество уведомлений
        /// </summary>
        public int? Counter { get; set; }
    }
}