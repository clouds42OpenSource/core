﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.History;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Аккаунт облака
    /// </summary>
    public class Account : BaseDomainModel, IAccount
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        [Column("Name")]
        [Required(ErrorMessage = "Обязательное поле")]
        
        public string AccountCaption { get; set; }

        /// <summary>
        /// ID аккаунта рефферала
        /// </summary>
        [Column("ReferrerId")]
        public Guid? ReferralAccountID { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IndexNumber { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime? RegistrationDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        
        public string Status { get; set; }

        /// <summary>
        /// Источник, откуда пришёл пользователь
        /// </summary>
        public string UserSource { get; set; }

        /// <summary>
        /// Признак что аккаунт удален
        /// </summary>
        public bool? Removed { get; set; }

        public int? Deployment { get; set; }

        /// <summary>
        /// Аккаунт биллинга
        /// </summary>
        public virtual BillingAccount BillingAccount { get; set; }

        /// <summary>
        /// Свойства сервиса "Мой диск" по аккаунту
        /// </summary>
        public virtual MyDiskPropertiesByAccount MyDiskPropertiesByAccount { get; set; }

        /// <summary>
        /// Реквизиты аккаунта
        /// </summary>
        public virtual AccountRequisites AccountRequisites { get; set; }

        /// <summary>
        /// Конфигурация аккаунта
        /// </summary>
        public virtual AccountConfiguration AccountConfiguration { get; set; }
        public virtual ServiceAccount ServiceAccount { get; set; }

        /// <summary>
        /// Менеджер аккаунта
        /// </summary>
        public virtual AccountSaleManager AccountSaleManager { get; set; }

        /// <summary>
        /// Список изменений по аккаунту
        /// </summary>
        public virtual ICollection<CloudChanges> CloudChanges { get; set; }

        /// <summary>
        /// Список инф. баз аккаунта
        /// </summary>
        public virtual ICollection<AccountDatabase> AccountDatabases { get; set; }

        /// <summary>
        /// Список доступов к базам аккаунта
        /// </summary>
        public virtual ICollection<AcDbAccess> AcDbAccesses { get; set; }


        /// <summary>
        /// Список счетов на оплату
        /// </summary>
        public virtual ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// Список платежей
        /// </summary>
        public virtual ICollection<Payment> Payments { get; set; }

        /// <summary>
        /// Список пользователей аккаунта
        /// </summary>
        public virtual ICollection<AccountUser> AccountUsers { get; set; }

        /// <summary>
        /// Список почт аккаунта
        /// </summary>
        public virtual ICollection<AccountEmail> AccountEmails { get; set; }
        public virtual ICollection<BillingService> BillingServices { get; set; }
        public virtual ICollection<BillingServiceTypeActivityForAccount> BillingServiceTypeActivities { get; set; }
        public virtual ICollection<AccountFile> AccountFiles { get; set; }
        public virtual ICollection<AccountAdditionalCompany> AdditionalCompanies { get; set; }
        public virtual ICollection<MainServiceResourcesChangesHistory> MainServiceResourcesChangesHistories { get; set; }

        /// <summary>
        /// Получить имя аккаунта
        /// </summary>
        /// <returns>Имя аккаунта</returns>
        public string GetAccountName() => $"account_{IndexNumber}";

        /// <summary>
        /// Получить зашифрованный id аккаунта
        /// </summary>
        /// <returns>Зашифрованный id аккаунта</returns>
        public string GetEncodeAccountId() => GetEncodeGuid(Id);

        /// <summary>
        /// Получить декодированный Guid
        /// </summary>
        /// <param name="value">Guid</param>
        /// <returns>Декодированный Guid</returns>
        private static string GetEncodeGuid(Guid value)
        {
            var guidParts = value.ToString().Split('-');
            var cryptResBuilder = new StringBuilder();
            for (var i = guidParts.Length - 1; i > 0; i--)
            {
                cryptResBuilder.Append(guidParts[i]);
            }
            return cryptResBuilder.ToString();
        }

        public string GetAccountCaptionOrIndexNumber()
        {
            return !string.IsNullOrEmpty(AccountCaption) ? AccountCaption : IndexNumber.ToString();
        }
    }
}
