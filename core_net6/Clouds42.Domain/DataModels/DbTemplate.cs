﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    public class DbTemplate : BaseDomainModel, IDbTemplate
    {
        [Required]
        public string Name { get; set; }
        public string DefaultCaption { get; set; }
        public int Order { get; set; }
        public string ImageUrl { get; set; }
        public string Remark { get; set; }
        public string Platform { get; set; }
        public bool CanWebPublish { get; set; }
        public Guid? LocaleId { get; set; }
        public virtual Locale Locale { get; set; }
        public Guid? DemoTemplateId { get; set; }
        public virtual DbTemplate DemoTemplate { get; set; }

        /// <summary>
        /// Шаблон нужно обновлять.
        /// </summary>
        public bool NeedUpdate { get; set; } = false;

        /// <summary>
        /// Логин администратора в шаблон.
        /// </summary>
        public string AdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора.
        /// </summary>
        public string AdminPassword { get; set; }

        /// <summary>
        /// Текущая версия шаблона.
        /// </summary>
        public string CurrentVersion { get; set; }

        public PlatformType PlatformType => Platform.ToPlatformTypeEnum();

        public virtual ICollection<DbTemplateUpdate> Updates { get; set; }
        public virtual ICollection<DbTemplateDelimiters> Delimiters { get; set; }
        public virtual ICollection<DbTemplate> DbTemplates { get; set; }
        public virtual ICollection<ConfigurationDbTemplateRelation> ConfigurationDbTemplateRelations { get; set; }
        public DbTemplateDelimiters GetDbTemplateDelimiters => Delimiters.FirstOrDefault();
    }
}
