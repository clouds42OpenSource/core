﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.DataModels.Its
{
    /// <summary>
    /// Вариация имени конфигурации 1C
    /// </summary>
    public class ConfigurationNameVariation : BaseDomainModel
    {
        /// <summary>
        /// Название вариации
        /// </summary>
        [Required]
        
        public string VariationName { get; set; }

        /// <summary>
        /// Конфигурация 1С
        /// </summary>
        [Required]
        
        public string Configuration1CName { get; set; }
        public virtual Configurations1C Configurations1C { get; set; }
    }
}
