﻿using System;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Инциденты облака
    /// </summary>
    public class Incident : BaseDomainModel
    {
        /// <summary>
        /// Сам инцидент
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Первое время обнаружения
        /// </summary>
        public DateTime DateOfDiscover { get; set; }
        
        /// <summary>
        /// Последнее время обнаружения
        /// </summary>
        public DateTime DateOfLastDiscover { get; set; }
         
        /// <summary>
        /// Количество инцидента за день
        /// </summary>
        public int CountOfIncedentToday { get;  set; } 

    }
}
