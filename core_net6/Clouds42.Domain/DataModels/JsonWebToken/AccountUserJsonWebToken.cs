﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.JsonWebToken
{
    [Table(nameof(AccountUserJsonWebToken))]
    public class AccountUserJsonWebToken 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Номер аккаунта пользователя
        /// </summary>
        [ForeignKey(nameof(AccountUser))]
        public Guid AccountUserId { get; set; }
        
        public virtual AccountUser AccountUser { get; set; }

        /// <summary>
        /// JsonWebToken для аутентификации
        /// </summary>
        [Required]
        public byte[] AccessToken { get; set; }

        /// <summary>
        /// Токен для обновления пары токенов (аутентификации и обновления)
        /// </summary>
        [Required]
        [MaxLength(120)]
        public byte[] RefreshToken { get; set; }

        /// <summary>
        /// JsonWebToken для аутентификации
        /// </summary>
        public DateTime Created { get; set; } = DateTime.Now;

        /// <summary>
        /// Показывает, использован ли RefreshToken
        /// </summary>
        [Required]
        public bool HasBeenTakenRefreshToken { get; set; }
    }
}
