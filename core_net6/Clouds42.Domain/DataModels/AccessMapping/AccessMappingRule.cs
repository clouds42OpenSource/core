﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Access;

namespace Clouds42.Domain.DataModels.AccessMapping
{
    /// <summary>
    /// Уровни доступов к действиям <see cref="Action"/>
    /// </summary>
    [Table(nameof(AccessMappingRule))]
    public class AccessMappingRule
    {
        /// <summary>
        /// Ключ уровня доступа
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Для какого действия доступ
        /// </summary>
        public ObjectAction Action { set; get; }

        /// <summary>
        /// Для какой группы доступ
        /// </summary>
        public AccountUserGroup Group { set; get; }

        /// <summary>
        /// Уровень доступности
        /// </summary>
        public AccessLevel Level { set; get; }
    }
}