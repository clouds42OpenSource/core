﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("CloudTerminalServers")]
    public class CloudTerminalServer
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; } = " ";

        [Required]
        public string ConnectionAddress { get; set; }

        public virtual ICollection<CloudServicesSegmentTerminalServer> CloudServicesSegmentTerminalServers { get; set; }
    }
}
