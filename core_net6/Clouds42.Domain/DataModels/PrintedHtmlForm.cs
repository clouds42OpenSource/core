﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Печатная форма Html
    /// </summary>
    [Table("PrintedHtmlForms")]
    public class PrintedHtmlForm : BaseDomainModel
    {
        public PrintedHtmlForm()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Название печатной формы Html
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Html разметка
        /// </summary>
        public string HtmlData { get; set; }

        /// <summary>
        /// Тип модели
        /// </summary>
        public string ModelType { get; set; }

        /// <summary>
        /// Файлы
        /// </summary>
        public virtual ICollection<PrintedHtmlFormFile> Files { get; set; }
        public virtual ICollection<Supplier.Supplier> Suppliers { get; set; }
        public virtual ICollection<Supplier.Supplier> Suppliers2 { get; set; }
    }
}
