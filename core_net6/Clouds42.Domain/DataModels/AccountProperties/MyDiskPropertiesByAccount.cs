﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountProperties
{
    /// <summary>
    /// Свойства сервиса "Мой диск" по аккаунту
    /// </summary>
    [Table("MyDiskPropertiesByAccounts")]
    public class MyDiskPropertiesByAccount
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежат свойства
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Размер файлов на диске
        /// </summary>
        public long MyDiskFilesSizeInMb { get; set; }

        /// <summary>
        /// Актульная дата оценки занимаемого места на диске
        /// </summary>
        public DateTime? MyDiskFilesSizeActualDateTime { get; set; }
    }
}
