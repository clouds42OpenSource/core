﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountProperties
{
    /// <summary>
    /// Конфигурация аккаунта
    /// </summary>
    [Table("AccountConfigurations")]
    public class AccountConfiguration
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежит конфигурация
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id поставщика аккаунта
        /// </summary>
        [ForeignKey(nameof(Supplier))]
        public Guid SupplierId { get; set; }

        /// <summary>
        /// Id сегмента аккаунта
        /// </summary>
        [ForeignKey(nameof(Segment))]
        public Guid SegmentId { get; set; }

        /// <summary>
        /// Id файлового хранилища аккаунта
        /// </summary>
        [ForeignKey(nameof(FileStorageServer))]
        public Guid FileStorageId { get; set; }

        /// <summary>
        /// Id локали аккаунта
        /// </summary>
        [ForeignKey(nameof(Locale))]
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool IsVip { get; set; }
        
        /// <summary>
        /// Признак того, что нужно создавать серверную базу по умолчанию
        /// </summary>

        public bool CreateClusterDatabase { get; set; }

        /// <summary>
        /// Ссылка на склеп аккаунта
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// Аккаунт, которому принадлежит конфигурация
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// Файловое хранилище аккаунта
        /// </summary>
        public virtual CloudServicesFileStorageServer FileStorageServer { get; set; }

        /// <summary>
        /// Поставщик аккаунта
        /// </summary>
        public virtual Supplier.Supplier Supplier { get; set; }

        /// <summary>
        /// Сегмент аккаунта
        /// </summary>
        public virtual CloudServicesSegment Segment { get; set; }

        /// <summary>
        /// Локаль аккаунта
        /// </summary>
        public virtual Locale Locale { get; set; }
    }
}
