﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.AccountProperties
{
    /// <summary>
    /// Реквизиты аккаунта
    /// </summary>
    [Table(nameof(AccountRequisites))]
    public class AccountRequisites
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежат реквизиты
        /// </summary>
        [Key]
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Аккаунт, которому принадлежат реквизиты
        /// </summary>
        public virtual Account Account { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        [StringLength(14, ErrorMessage = "Допустимое количество символов не более 14")]
        public string Inn { get; set; }
    }
}