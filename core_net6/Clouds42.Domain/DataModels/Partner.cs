using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("Partners")]
    public class Partner
    {
        [Key]
        public Guid PartnerId { get; set; }
    }
}