﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels.Mailing
{
    /// <summary>
    /// Шаблон письма
    /// </summary>
    [Table(nameof(LetterTemplate), Schema = SchemaType.Mailing)]
    public class LetterTemplate
    {
        /// <summary>
        /// Id шаблона письма
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Заголовок письма
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело письма
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Тип письма
        /// </summary>
        public string LetterTypeName { get; set; }

        /// <summary>
        /// Полная информация о классе письма
        /// </summary>
        public string LetterAssemblyClassName { get; set; }

        /// <summary>
        /// ID шаблона письма в SendGrid
        /// </summary>
        
        public string SendGridTemplateId { get; set; }

        /// <summary>
        /// Флаг показывающий что письмо является системным
        /// </summary>
        [DefaultValue(false)]
        public bool IsSystemLetter { get; set; }

        /// <summary>
        /// Текст для различных уведомлений
        /// </summary>
        public string NotificationText { get; set; }
    }
}
