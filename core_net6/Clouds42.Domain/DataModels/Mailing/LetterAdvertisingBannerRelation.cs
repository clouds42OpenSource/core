﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.Marketing;

namespace Clouds42.Domain.DataModels.Mailing
{
    /// <summary>
    /// Связь шаблона письма с рекламным баннером
    /// </summary>
    [Table(nameof(LetterAdvertisingBannerRelation), Schema = SchemaType.Mailing)]
    public class LetterAdvertisingBannerRelation
    {
        /// <summary>
        /// Шаблон письма
        /// </summary>
        [Key]
        [ForeignKey(nameof(LetterTemplate))]
        public int LetterTemplateId { get; set; }
        public virtual LetterTemplate LetterTemplate { get; set; }

        /// <summary>
        /// Шаблон рекламного баннера
        /// </summary>
        [ForeignKey(nameof(AdvertisingBannerTemplate))]
        public int AdvertisingBannerTemplateId { get; set; }
        public virtual AdvertisingBannerTemplate AdvertisingBannerTemplate { get; set; }
    }
}
