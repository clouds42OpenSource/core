﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table(nameof(CloudServicesTerminalFarm))]
    public class CloudServicesTerminalFarm
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }
        public string Description { get; set; }

        [Required]
        public string ConnectionAddress { get; set; }

        [Required]
        public string Name { get; set; }

        public bool UsingOutdatedWindows { get; set; }

        public virtual ICollection<CloudServicesSegment> CloudServicesSegments { get; set; }
    }
}
