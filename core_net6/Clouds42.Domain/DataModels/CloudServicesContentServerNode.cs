﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Связь сервера и ноды публикаций
    /// </summary>
    [Table("CloudServicesContentServerNodes")]
    public class CloudServicesContentServerNode
    {
        /// <summary>
        /// Идентификатор связи
        /// </summary>
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Идентификатор сервера публикаций
        /// </summary>
        [ForeignKey(nameof(CloudServicesContentServer))]
        public Guid ContentServerId { get; set; }
        public virtual CloudServicesContentServer CloudServicesContentServer { get; set; }

        /// <summary>
        /// Идентификатор ноды публикаций
        /// </summary>
        [ForeignKey(nameof(PublishNodeReference))]
        public Guid NodeReferenceId { get; set; }
        public virtual PublishNodeReference PublishNodeReference { get; set; }
    }
}
