﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Версия плаформы 1С
    /// </summary>
    [Table("PlatformVersionReferences")]
    public class PlatformVersionReference : IPlatformVersionReference
    {
        /// <summary>
        ///     Версия платформы
        /// </summary>
        [Key]
        [Required]
        public string Version { get; set; }

        /// <summary>
        ///     Путь к платформе
        /// </summary>
        [Required]
        public string PathToPlatform { get; set; }

        /// <summary>
        ///     Путь к платформе x64
        /// </summary>
        public string PathToPlatfromX64 { get; set; }

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для MacOs
        /// </summary>
        public string MacOsThinClientDownloadLink { get; set; }

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для Windows
        /// </summary>
        public string WindowsThinClientDownloadLink { get; set; }

        /// <summary>
        /// Список сегментов на данной версии платформы 8.2
        /// </summary>
        public virtual ICollection<CloudServicesSegment> CloudServicesSegmentsStable82 { get; set; }

        /// <summary>
        /// Список сегментов на данной альфа версии платформы 8.3
        /// </summary>
        public virtual ICollection<CloudServicesSegment> CloudServicesSegmentsAlpha83 { get; set; }

        /// <summary>
        /// Список сегментов на данной стабильной версии платформы 8.3
        /// </summary>
        public virtual ICollection<CloudServicesSegment> CloudServicesSegmentsStable83 { get; set; }
    }
}
