﻿using System;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Таблица связей сегмента и терминального сервера
    /// </summary>
    public class CloudServicesSegmentTerminalServer : BaseDomainModel
    {
        /// <summary>
        /// Id сегмента
        /// </summary>
        public Guid SegmentId { get; set; }
        public virtual CloudServicesSegment CloudServicesSegment { get; set; }

        /// <summary>
        /// Id терминального сервера
        /// </summary>
        public Guid TerminalServerId { get; set; }
        public virtual CloudTerminalServer CloudTerminalServer { get; set; }
    }
}