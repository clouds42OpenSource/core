﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Задача воркера
    /// </summary>
    [Table(nameof(CoreWorkerTask))]
    public class CoreWorkerTask
    {
        /// <summary>
        /// Id задача
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Название задачи(джобы)
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public bool? Status { get; set; }

        /// <summary>
        /// Параметры задачи
        /// </summary>
        public string TaskParams { get; set; }

        /// <summary>
        /// Тип задачи воркера
        /// </summary>
        [DefaultValue(WorkerTaskTypeEnum.InternalTask)]
        public WorkerTaskTypeEnum WorkerTaskType { get; set; }

        /// <summary>
        /// Признак что задача может выполняться в нескольких потоках одного воркера
        /// </summary>
        [DefaultValue(true)]
        public bool TaskAllowsMultithreading { get; set; }

        /// <summary>
        /// Время жизни выполнения задачи в минутах
        /// </summary>
        [DefaultValue(-1)]
        public int TaskExecutionLifetimeInMinutes { get; set; }

        /// <summary>
        /// Коллекция запущенных задач
        /// </summary>
        public virtual ICollection<CoreWorkerTasksQueue> CoreWorkerTasksQueues { get; set; }

        public virtual ICollection<CoreWorkerAvailableTasksBag> CoreWorkerAvailableTasksBags { get; set; }
    }
}
