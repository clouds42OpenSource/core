﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.DbTemplateUpdates;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{

    /// <summary>
    /// Обновление шаблона.
    /// </summary>
    public class DbTemplateUpdate : BaseDomainModel, IDbTemplateUpdate
    {

        /// <summary>
        /// Ссылка на шаблон.
        /// </summary>
        [ForeignKey(nameof(Template))]        
        public Guid TemplateId { get; set; }
        public virtual DbTemplate Template { get; set; }

        /// <summary>
        /// Ответсвенный за обновление пользователь.
        /// </summary>
        [ForeignKey(nameof(ResponsibleAccountUser))]
        public Guid? ResponsibleAccountUserId { get; set; }
        public virtual AccountUser ResponsibleAccountUser { get; set; }

        /// <summary>
        /// Версия обновления.
        /// </summary>
        
        public string UpdateVersion { get; set; }

        /// <summary>
        /// Дата обновления.
        /// </summary>
        
        public DateTime? UpdateVersionDate { get; set; }

        /// <summary>
        /// Признак проврки обновления на ошибки человеком.
        /// </summary>
        public DbTemplateUpdateState ValidateState { get; set; }

        /// <summary>
        /// Полный путь до обновленного шаблона.
        /// </summary>        
        
        public string UpdateTemplatePath { get; set; }
    }
}