﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// SQL сервер облака
    /// </summary>
    [Table("CloudServicesSQLServer")]
    public class CloudServicesSqlServer : ICloudServicesSqlServer
    {
        /// <summary>
        /// ID сервера
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        
        [Required]
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        
        public string Description { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum RestoreModelType { get; set; }

        /// <summary>
        /// Список сегментов, в которых используется данный сервер
        /// </summary>
        public virtual ICollection<CloudServicesSegment> CloudServicesSegments { get; set; }
    }
}
