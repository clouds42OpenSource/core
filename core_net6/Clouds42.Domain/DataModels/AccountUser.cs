﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Пользователь аккаунта
    /// </summary>
    public class AccountUser : BaseDomainModel, IAccountUser
    {
        /// <summary>
        /// Логин
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        
        public string FirstName { get; set; }

        /// <summary>
        /// Хэш пароля (старое поле)
        /// </summary>
        
        public string Password { get; set; }

        /// <summary>
        /// Хэш пароля (новое поле)
        /// </summary>
        
        public string PasswordHash { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     Пользователь активирован (подтвержден мобильный или одобрен менеджером)
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        ///     Пользователь создан в домене Active Directory
        /// </summary>
        public bool CreatedInAd { get; set; }

        /// <summary>
        /// Код подтверждения
        /// </summary>
        
        public string ConfirmationCode { get; set; }

        /// <summary>
        /// Дата последнего входа в систему
        /// </summary>
        public DateTime? LastLoggedIn { get; set; }

        /// <summary>
        /// Признак что пользователь отписан от рассылки
        /// </summary>
        public bool? Unsubscribed { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        
        public string MiddleName { get; set; }

        /// <summary>
        /// Полное имя
        /// </summary>
        
        public string FullName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Признак что пользователь удален
        /// </summary>
        public bool? Removed { get; set; }

        public bool? IsPhoneVerified { get; set; }

        /// <summary>
        /// Дата редактирования
        /// </summary>
        public DateTime? EditDate { get; set; }

        /// <summary>
        /// Токен восстановления пароля
        /// </summary>
        public Guid? ResetCode { get; set; }

        /// <summary>
        /// Токен авторизации
        /// </summary>
        public Guid? AuthGuid { get; set; }

        /// <summary>
        /// Токен авторизации службы
        /// </summary>
        public Guid? AuthToken { get; set; }

        /// <summary>
        /// Аккаунт пользователя
        /// </summary>
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Дата и время последней смены логина или пароля
        /// </summary>
        public DateTime? DateTimeOfLastChangePasswordOrLogin { get; set; }

        [Required] public string EmailStatus { get; set; } = "Unchecked";
        public string LoginCode { get; set; }

        public DateTime? DateLoginCode { get; set; }

        public Guid? ReferralId { get; set; }

        
        public string PrimaryPhone { get; set; }

        public byte? ConfirmationAttemptsLeft { get; set; }

        public string Comment { get; set; }

        public Guid? CorpUserID { get; set; }

        
        public string CorpUserSyncStatus { get; set; }
        public virtual ICollection<AccountUserRole> AccountUserRoles { get; set; }

        public virtual ICollection<AccountUserDepartment> AccountUserDepartments { get; set; }
        public virtual ICollection<Link42Configuration> CreatedLink42Configurations { get; set; }
        public virtual ICollection<Link42Configuration> UpdatedLink42Configurations { get; set; }
        public virtual ICollection<Connection> Connections { get; set; }
        public virtual ICollection<Notification.Notification> Notifications { get; set; }
        public virtual ICollection<AccountUserNotificationSettings> NotificationsSettings { get; set; }
        public virtual ICollection<AccountDatabaseBackup> InitiatorBackups { get; set; }
        public virtual ICollection<AccountUserSession> AccountUserSessions { get; set; }
        public virtual ICollection<AcDbAccess> AcDbAccesses { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public string CreationDateValue => CreationDate?.ToString("O") ?? string.Empty;

        /// <summary>
        /// Полное имя пользователя
        /// </summary>
        public string Name => $"{LastName} {FirstName} {MiddleName}";

        /// <summary>
        /// Преобразовать объект в строку
        /// </summary>
        /// <returns>Строка с указанными полями</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Id: " + Id);
            sb.AppendLine("AccountId: " + AccountId);
            sb.AppendLine("Login: " + Login);
            sb.AppendLine("FirstName: " + FirstName);
            sb.AppendLine("Email: " + Email);
            sb.AppendLine("PhoneNumber: " + PhoneNumber);
            sb.AppendLine("Activated: " + Activated);
            sb.AppendLine("CorpUserID: " + CorpUserID);
            sb.AppendLine("CorpUserSyncStatus: " + CorpUserSyncStatus);
            sb.AppendLine("LastName: " + LastName);
            sb.AppendLine("MiddleName: " + MiddleName);
            sb.AppendLine("FullName: " + FullName);
            return sb.ToString();
        }

        /// <summary>
        /// Активировать пользователя
        /// </summary>
        public void Activate()
        {
            Activated = true;
        }

        /// <summary>
        /// Сравнить текущий объект с указанным
        /// </summary>
        /// <param name="obj">Объект для сравнения</param>
        /// <returns>Результат сравнения</returns>
        public override bool Equals(object obj) 
            => !ReferenceEquals(null, obj) && ReferenceEquals(this, obj);

        /// <summary>
        /// Получить хэш код
        /// </summary>
        /// <returns>Хэш код</returns>
        public override int GetHashCode() => Id.GetHashCode();
    }
}
