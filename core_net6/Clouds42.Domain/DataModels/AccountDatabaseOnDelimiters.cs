﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Enums;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    ///  Таблица связи информационной базы с основной базой на разделителях с указанием номера зоны
    /// </summary>
    [Table(nameof(AccountDatabaseOnDelimiters))]
    public class AccountDatabaseOnDelimiters
    {
        /// <summary>
        /// Информационная база
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
        public virtual AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        ///  Id базы источника на разделителях
        /// </summary>
        public Guid? DatabaseSourceId { get; set; }
        public virtual AccountDatabase SourceAccountDatabase { get; set; }

        public string DbTemplateDelimiterCode { get; set; }
        public virtual DbTemplateDelimiters DbTemplateDelimiter { get; set; }

        /// <summary>
        ///   Номер зоны в базе на разделителях
        /// </summary>
        public int? Zone { get; set; }        

        /// <summary>
        /// Флаг показывающий создана ли база с демо данными
        /// </summary>
        public bool IsDemo { get; set; } = false;

        /// <summary>
        /// Идентификатор загруженного Zip-файла.
        /// </summary>
        public Guid? UploadedFileId { get; set; }
        public virtual UploadedFile UploadedFile { get; set; }

        /// <summary>
        /// Тип загрузки.
        /// </summary>
        public string LoadType { get; private set; }

        [NotMapped]
        public DelimiterLoadType LoadTypeEnum
        {
            get { return (DelimiterLoadType)Enum.Parse(typeof(DelimiterLoadType), LoadType); }
            set { LoadType = value.ToString(); }
        }
    }
}
