﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    [Table("CoreWorkerTaskParameters")]
    public class CoreWorkerTaskParameter
    {
        [ForeignKey(nameof(Task))] 
        [Key] 
        public Guid TaskId { get; set; }
        public virtual CoreWorkerTasksQueue Task { get; set; }

        public string TaskParams { get; set; }

        public string SynchronizationKey { get; set; }

        [ForeignKey(nameof(DependTask))]
        public Guid? DependTaskId { get; set; }
        public virtual CoreWorkerTasksQueue DependTask { get; set; }

    }
}
