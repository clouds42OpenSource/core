﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// История проведения технических работ в инф. базе
    /// </summary>
    [Table(nameof(AcDbSupportHistory))]
    public class AcDbSupportHistory
    {
        /// <summary>
        /// ID истории
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ID { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Статус выполнения
        /// </summary>
        public SupportHistoryState State { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Дата проведения
        /// </summary>
        public DateTime EditDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Тип операции
        /// </summary>
        public DatabaseSupportOperation? Operation { get; set; }

        /// <summary>
        /// Текущая версия конфигурации
        /// </summary>
        public string CurrentVersionCfu { get; set; }

        /// <summary>
        /// Предыдущая версия конфигурации
        /// </summary>
        public string OldVersionCfu { get; set; }

        /// <summary>
        /// Модель поддержки инф. базы
        /// </summary>
        [ForeignKey(nameof(AcDbSupport))]
        public Guid AccountDatabaseId { get; set; }
        public virtual AcDbSupport AcDbSupport { get; set; }
        public virtual ICollection<AcDbSupportHistoryTasksQueueRelation> AcDbSupportHistoryTasksQueues { get; set; }
    }
}
