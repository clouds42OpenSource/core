﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Доступ к инф. базе
    /// </summary>
    [Table("AcDbAccesses")]
    public class AcDbAccess
    {
        /// <summary>
        /// ID доступа
        /// </summary>
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// ID локального пользователя инф. базы
        /// </summary>
        public Guid LocalUserID { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        [ForeignKey(nameof(Database))]
        public Guid AccountDatabaseID { get; set; }
        public virtual AccountDatabase Database { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountID { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// ID пользователя аккаунта
        /// </summary>
        [ForeignKey(nameof(AccountUser))]
        public Guid? AccountUserID { get; set; }
        public virtual AccountUser AccountUser { get; set; }

        /// <summary>
        /// Доступ заблокирован
        /// </summary>
        public bool IsLock { get; set; }

        /// <summary>
        /// Тип запуска
        /// </summary>
        public int? LaunchType { get; set; }

        /// <summary>
        /// Дата создания записи.
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Роли пользователя
        /// </summary>
        public virtual ICollection<AcDbAccRolesJho> AcDbAccRolesJHOes { get; set; }
        public virtual ManageDbOnDelimitersAccess ManageDbOnDelimitersAccess { get; set; }
    }
}
