﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Временная модель пока тестируется корп облако дальше будет переделываться под требуемую логику
    /// </summary>
    public class AccountCorpCloud : BaseDomainModel
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        [ForeignKey(nameof(Account))]
        public Guid AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Количество кликов
        /// </summary>
        public int CountOfClicks { get; set; }

        /// <summary>
        /// Дата последнего клика
        /// </summary>
        public DateTime LastUseDate { get; set; }
    }
}
