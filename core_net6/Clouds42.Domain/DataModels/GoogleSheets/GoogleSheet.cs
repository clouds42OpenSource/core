﻿using System;

namespace Clouds42.Domain.DataModels.GoogleSheets;

public class GoogleSheet : BaseDomainModel
{
    public Guid AccountId { get; set; }
    public virtual Account Account { get; set; }
    public string DocumentId { get; set; }
    public DateTime? PeriodFrom { get; set; }
    public DateTime? PeriodTo { get; set; }
    public  SheetType SheetType { get; set; }
}
