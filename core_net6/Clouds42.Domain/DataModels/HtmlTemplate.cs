﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.Domain.DataModels
{
    /// <summary>
    /// Html шаблон
    /// </summary>
    [Table(nameof(HtmlTemplate))]
    public class HtmlTemplate
    {
        /// <summary>
        /// Id шаблона(первичный ключ)
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Html разметка
        /// </summary>
        public string HtmlData { get; set; }
    }
}
