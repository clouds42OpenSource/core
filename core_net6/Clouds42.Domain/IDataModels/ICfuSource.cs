﻿namespace Clouds42.Domain.IDataModels
{
    public interface ICfuSource
    {
        string FilePath { get; }
        string Version { get; }
    }
}