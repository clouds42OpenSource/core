﻿namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Интерфейс описывает состояние авторизации ИБ. 
    /// </summary>
    public interface IStateSupport
    {
        /// <summary>
        /// Состояние авторизации
        /// </summary>
        int State { get; }
     
    }
}