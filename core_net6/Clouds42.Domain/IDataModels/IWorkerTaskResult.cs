﻿using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Результат выполнения задачи воркера.
    /// </summary>
    public interface IWorkerTaskResult
    {
        CloudTaskQueueStatus QueueStatus { get; }

        string Comment { get; }

        string ExecutionResult { get; }
    }
}