﻿using System;
using System.Collections.Generic;

namespace Clouds42.Domain.IDataModels
{

    /// <summary>
    /// Конфигурация 1С.
    /// </summary>
    public interface IConfigurations1C
    {

        /// <summary>
        /// Имя конфигурации.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Каталог конфигурации.
        /// </summary>
        string ConfigurationCatalog { get; }

        /// <summary>
        /// Каталоги редакции.
        /// </summary>
        string RedactionCatalogs { get; }        

        /// <summary>
        /// Каталог платформы.
        /// </summary>
        string PlatformCatalog { get; }

        /// <summary>
        /// Адрес каталога обновлений.
        /// </summary>
        string UpdateCatalogUrl { get; }

        /// <summary>
        /// Краткий код конфигурации, например bp, zup и тд.
        /// </summary>        
        string ShortCode { get; set; }

        /// <summary>
        /// Признак что необходимо использовать COM соединения для принятия обновлений в базе
        /// </summary>
        bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Признак необходимости проверять наличие обновлений
        /// </summary>
        bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// ID данных авторизации в ИТС
        /// </summary>
        Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1C
        /// </summary>
        List<string> ConfigurationVariations { get; set; }
    }
}
