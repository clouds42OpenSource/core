﻿namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Валютные свойтсва.
    /// </summary>
    public interface ICurrency
    {

        /// <summary>
        /// Название валюты. Сокрощенное руб, тг, грн и тд.
        /// </summary>
        string Currency { get; }

        /// <summary>
        /// Международны код валюты.
        /// </summary>
        int? CurrencyCode { get; }
    }
}