﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Аккаунт облака
    /// </summary>
    public interface IAccount
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        string AccountCaption { get; set; }

        /// <summary>
        /// ID аккаунта рефферала
        /// </summary>
        Guid? ReferralAccountID { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        int IndexNumber { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        string Status { get; set; }

        /// <summary>
        /// Признак что аккаунт удален
        /// </summary>
        bool? Removed { get; set; }
    }
}
