﻿namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Версия плаформы 1С
    /// </summary>
    public interface IPlatformVersionReference
    {
        /// <summary>
        ///     Версия платформы
        /// </summary>
        string Version { get; }

        /// <summary>
        ///     Путь к платформе
        /// </summary>
        string PathToPlatform { get; }

        /// <summary>
        ///     Путь к платформе x64
        /// </summary>
        string PathToPlatfromX64 { get; }

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для MacOs
        /// </summary>
        string MacOsThinClientDownloadLink { get;}

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для Windows
        /// </summary>
        string WindowsThinClientDownloadLink { get;}
    }
}
