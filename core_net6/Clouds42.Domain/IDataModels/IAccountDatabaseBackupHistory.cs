﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.IDataModels
{
    public interface IAccountDatabaseBackupHistory
    {
        Guid Id { get; }

        /// <summary>
        /// Дата создания записи.
        /// </summary>
        DateTime CreateDateTime { get; }

        /// <summary>
        /// Коментарий архивации.
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Номер информационной базы.
        /// </summary>
        Guid AccountDatabaseId { get; }

        /// <summary>
        /// Номер созданного бэкапа.
        /// </summary>
        Guid? AccountDatabaseBackupId { get; }

        /// <summary>
        /// Состояние бэкапа.
        /// </summary>
        AccountDatabaseBackupHistoryState State { get; set; }
    }
}