﻿using System;
using Clouds42.Domain.Enums.DbTemplateUpdates;

namespace Clouds42.Domain.IDataModels
{
    public interface IDbTemplateUpdate
    {

        Guid Id { get; set; }

        /// <summary>
        /// Ссылка на шаблон.
        /// </summary>
        Guid TemplateId { get; }

        /// <summary>
        /// Ответсвенный за обновление пользователь.
        /// </summary>
        Guid? ResponsibleAccountUserId { get; set; }

        /// <summary>
        /// Версия обновления.
        /// </summary>
        string UpdateVersion { get; }

        /// <summary>
        /// Дата обновления.
        /// </summary>
        DateTime? UpdateVersionDate { get; }

        /// <summary>
        /// Признак проврки обновления на ошибки человеком.
        /// </summary>
        DbTemplateUpdateState ValidateState { get; }

        /// <summary>
        /// Полный путь до обновленного шаблона.
        /// </summary>
        string UpdateTemplatePath { get; }
    }
}