﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// SQL сервер облака
    /// </summary>
    public interface ICloudServicesSqlServer
    {
        /// <summary>
        /// ID сервера
        /// </summary>
        Guid ID { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        string ConnectionAddress { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        string Name { get; set; }
    }
}
