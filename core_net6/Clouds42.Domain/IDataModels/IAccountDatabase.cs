﻿using System;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Модель инф. базы
    /// </summary>
    public interface IAccountDatabase : IPersistentObject
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        Guid Id { get; }
        
        /// <summary>
        /// Дата последней активности
        /// </summary>
        DateTime LastActivityDate { get; }
        
        /// <summary>
        /// Название инф. базы
        /// </summary>
        string Caption { get; }
        
        /// <summary>
        /// Статус инф. базы
        /// </summary>
        string State { get; }
        
        /// <summary>
        /// ID шаблона инф. базы
        /// </summary>
        Guid? TemplateId { get; }
        
        /// <summary>
        /// Номер базы
        /// </summary>
        int DbNumber { get; }
        
        /// <summary>
        /// Название сервиса
        /// </summary>
        string ServiceName { get; }
        
        /// <summary>
        /// Название базы на SQL сервере
        /// </summary>
        string SqlName { get; }
        
        /// <summary>
        /// Номер инф. базы
        /// </summary>
        string V82Name { get; }
        
        /// <summary>
        /// Сервер предприятия
        /// </summary>
        string V82Server { get; }
        
        /// <summary>
        /// Размер инф. базы в Мб
        /// </summary>
        int SizeInMB { get; }        
        
        /// <summary>
        /// Состояние блокировки
        /// </summary>
        string LockedState { get; }
        
        /// <summary>
        /// Признак что база файловая
        /// </summary>
        bool? IsFile { get; }
        
        /// <summary>
        /// Путь к архивной копии
        /// </summary>
        string ArchivePath { get; }
        
        /// <summary>
        /// Статус публикации
        /// </summary>
        string PublishState { get; }
        
        /// <summary>
        /// Версия платформы
        /// </summary>
        string ApplicationName { get; }
        
        /// <summary>
        /// Тип распространения (альфа/стабильная)
        /// </summary>
        string DistributionType { get; }
        
        /// <summary>
        /// Тип запуска
        /// </summary>
        int? LaunchType { get; }
        
        /// <summary>
        /// Дата последнего рассчета занимаемого места
        /// </summary>
        DateTime? CalculateSizeDateTime { get; }

        /// <summary>
        /// ID файлового хранилища
        /// </summary>
        Guid? FileStorageID { get; }
        
        /// <summary>
        /// Параметры запуска
        /// </summary>
        string LaunchParameters { get; }
        
        /// <summary>
        /// Признак что в базе используются веб сервисы
        /// </summary>
        bool? UsedWebServices { get; }
        
        /// <summary>
        /// ID аккаунта
        /// </summary>
        Guid AccountId { get; }
        
        /// <summary>
        /// Путь к файлам базы в склепе
        /// </summary>
        string CloudStorageWebLink { get; }
        
        /// <summary>
        /// Тип платформы
        /// </summary>
        PlatformType PlatformType { get; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }
    }
}
