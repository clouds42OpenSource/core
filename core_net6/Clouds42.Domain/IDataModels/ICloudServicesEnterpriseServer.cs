﻿namespace Clouds42.Domain.IDataModels
{
    public interface ICloudServicesEnterpriseServer
    {
        System.Guid ID { get; set; }
        string ConnectionAddress { get; set; }
        string Description { get; set; }
        string Version { get; set; }
        string Name { get; set; }        
        string AdminName { get; set; }
        string AdminPassword { get; set; }
        string ClusterSettingsPath { get; set; }
    }
}
