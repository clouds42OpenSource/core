﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Менеджер прикрепленный к аккаунту
    /// </summary>
    public interface IAccountSaleManager
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        Guid AccountId { get; set; }

        /// <summary>
        /// Id менеджера прикрепленного
        /// к аккаунту
        /// </summary>
        Guid SaleManagerId { get; set; }

        /// <summary>
        /// Отдел менеджера
        /// </summary>
        string Division { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime CreationDate { get; set; }
    }
}
