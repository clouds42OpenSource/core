﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Поставщик
    /// </summary>
    public interface ISupplier
    {
        /// <summary>
        /// Название поставщика
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название поставщика
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        Guid LocaleId { get; set; }

        /// <summary>
        /// Стандартный поставщик на локаль
        /// </summary>
        bool IsDefault { get; set; }
    }
}