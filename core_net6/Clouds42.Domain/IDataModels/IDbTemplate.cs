﻿using System;

namespace Clouds42.Domain.IDataModels
{
    public interface IDbTemplate
    {
        Guid Id { get; }
        string Name { get; }
        string DefaultCaption { get; }
        int Order { get; }
        string ImageUrl { get; }
        string Remark { get; }                        
        string Platform { get; }
        Guid? LocaleId { get; }
        Guid? DemoTemplateId { get; }
        bool CanWebPublish { get; }

        /// <summary>
        /// Шаблон нужно обновлять.
        /// </summary>
        bool NeedUpdate { get; set; }

        /// <summary>
        /// Логин администратора в шаблон.
        /// </summary>        
        string AdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора.
        /// </summary>        
        string AdminPassword { get; set; }

    }
}