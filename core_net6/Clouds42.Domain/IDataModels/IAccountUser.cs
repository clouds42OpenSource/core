﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Пользователь аккаунта
    /// </summary>
    public interface IAccountUser
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Логин.
        /// </summary>
        string Login { get;}

        /// <summary>
        /// Электронная почта.
        /// </summary>
        string Email { get;}

        /// <summary>
        /// Телефонный номер.
        /// </summary>
        string PhoneNumber { get;}

        /// <summary>
        /// Имя.
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        string LastName { get;}

        /// <summary>
        /// Отчество.
        /// </summary>
        string MiddleName { get;}

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        Guid AccountId { get;}

    }
}
