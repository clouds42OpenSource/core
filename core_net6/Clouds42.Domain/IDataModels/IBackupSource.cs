﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Domain.IDataModels
{

    /// <summary>
    /// Источник бэкапа.
    /// </summary>
    public interface IBackupSource
    {

        /// <summary>
        /// Путь к бекапу.
        /// </summary>
        string BackupPath { get; }

        /// <summary>
        /// Тип хранилища.
        /// </summary>
        AccountDatabaseBackupSourceType SourceType { get; }
    }
}
