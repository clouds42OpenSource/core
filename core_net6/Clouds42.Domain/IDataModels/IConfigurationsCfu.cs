using System;

namespace Clouds42.Domain.IDataModels
{
    public interface IConfigurationsCfu : ICfuSource
    {
        System.Guid Id { get; }        
        string Platform1CVersion { get; }
        Nullable<System.DateTime> DownloadDate { get; }
        Nullable<bool> ValidateState { get; }        
        Nullable<System.Guid> InitiatorId { get; }
        string ConfigurationName { get; }
    }
}