﻿using System;

namespace Clouds42.Domain.IDataModels
{
    public interface IPersistentObject
    {
        DateTime CreationDate { get; set; }
        DateTime LastEditedDateTime { get; set; }
    }
}