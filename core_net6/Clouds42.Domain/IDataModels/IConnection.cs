﻿namespace Clouds42.Domain.IDataModels
{
    public interface IConnection
    {
        string ConnectionAddress { get; }
    }
}
