﻿using System;

namespace Clouds42.Domain.IDataModels
{
    public interface IAcDbSupport
    { 
        System.Guid AccountDatabasesID { get; }
        string Login { get; }
        string Password { get; }
        int State { get; set; }
        bool PrepearedForUpdate { get; }
        bool HasSupport { get; }
        bool HasAutoUpdate { get; }
        string OldVersion { get; }
        string CurrentVersion { get; }
        string UpdateVersion { get; }
        bool PreparedForTehSupport { get; }
        Nullable<System.DateTime> TehSupportDate { get; }
        Nullable<System.DateTime> UpdateVersionDate { get; }
    }
}