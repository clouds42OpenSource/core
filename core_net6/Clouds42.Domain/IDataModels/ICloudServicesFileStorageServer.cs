using System;

namespace Clouds42.Domain.IDataModels
{
    public interface ICloudServicesFileStorageServer
    {
        Guid ID { get; set; }

        string ConnectionAddress { get; set; }

        string Description { get; set; }

        string Name { get; set; }

        string PhysicalPath { get; set; }
    }
}