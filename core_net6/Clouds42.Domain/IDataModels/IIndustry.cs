﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Отрасль
    /// </summary>
    public interface IIndustry
    {
        /// <summary>
        /// Id отрасли
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название отрасли
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Описание отрасли
        /// </summary>
        string Description { get; set; }
    }
}
