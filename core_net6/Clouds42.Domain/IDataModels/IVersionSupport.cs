﻿namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Интерфейс описывает версию ИБ.
    /// </summary>
    public interface IVersionSupport : IStateSupport
    {       
        /// <summary>
        /// Текущая версия информационной базы.
        /// </summary>
        string CurrentVersion { get; }
    }
}