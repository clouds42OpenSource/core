﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// База на разделителях
    /// </summary>
    public interface IDbTemplateDelimiters
    {
		/// <summary>
		///   Код конфигурации
		/// </summary>
		string ConfigurationId { get; set; }

        /// <summary>
        /// Название конфигурации 1С
        /// </summary>
        string Name { get; set; }

		/// <summary>
		/// Id шаблона
		/// </summary>
		Guid TemplateId { get; set; }

        /// <summary>
        /// название конфигурации в файле DumpInfo
        /// </summary>
        string ShortName { get; set; }

        /// <summary>
        /// Адрес публикации демо базы на разделителях
        /// </summary>
        string DemoDatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Версия релиза конфигурации
        /// </summary>
        string ConfigurationReleaseVersion { get; set; }

        /// <summary>
        /// Минимальная версия релиза конфигурации
        /// </summary>
        string MinReleaseVersion { get; set; }

    }
}
