﻿namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Копируемый объект
    /// </summary>
    /// <typeparam name="TModel">Модель</typeparam>
    public interface IClone<out TModel>
    {
        /// <summary>
        /// Сделать копию объекта
        /// </summary>
        /// <returns>Копия объекта</returns>
        TModel Clone();
    }
}
