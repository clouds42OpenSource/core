﻿using System;

namespace Clouds42.Domain.IDataModels
{
    /// <summary>
    /// Свойства локали.
    /// </summary>
    public interface ILocale : ICurrency
    {

        /// <summary>
        /// Идентификатор локали.
        /// </summary>
        Guid ID { get; }

        /// <summary>
        /// Наименование локали ru-ru, ru-ua и тд.
        /// </summary>
        string Name { get; }                

        /// <summary>
        /// Страна.
        /// </summary>
        string Country { get; }
    }
}