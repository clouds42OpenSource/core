﻿namespace Clouds42.Domain.IDataModels
{
    public interface IWorkerTask
    {
        IWorkerTaskResult Wait(int timeoutMinutes = 20);
    }

    public interface IWorkerTask<out TResult> : IWorkerTask
    {
        TResult WaitResult();
    }
}
