﻿using System.ComponentModel;
using System;

namespace Clouds42.Domain
{
    public static class EnumExtensions
    {
        public static string Description(this Enum value)
        {
            if (value == null)
                return string.Empty;

            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());

            if (field == null)
                return string.Empty;

            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes.Length == 0
                ? value.ToString()
                : ((DescriptionAttribute)attributes[0]).Description;
        }
    }
}
