﻿namespace Clouds42.Domain
{
    /// <summary>
    ///     Тип схемы
    /// </summary>
    public static class SchemaType
    {
        public const string Billing = "billing";
        public const string Mailing = "mailing";
        public const string Migration = "migration";
        public const string Security = "security";
        public const string Its = "its";
        public const string StateMachine = "statemachine";
        public const string Segment = "segment";
        public const string AccessDatabase = "accessDatabase";
        public const string BillingCommits = "billingCommits";
        public const string History = "history";
        public const string Marketing = "marketing";
        public const string Configuration = "configuration";
    }
}
