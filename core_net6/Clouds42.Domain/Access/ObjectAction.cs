﻿namespace Clouds42.Domain.Access
{
    // ReSharper disable All InconsistentNaming
    /// <summary>
    /// 
    /// </summary>
    public enum ObjectAction
    {
        #region Accounts
        /// <summary>
        /// 
        /// </summary>
        Accounts_View,
        /// <summary>
        /// 
        /// </summary>
        Accounts_Add,
        /// <summary>
        /// 
        /// </summary>
        Accounts_Edit,
        /// <summary>
        /// 
        /// </summary>
        Accounts_EditIin,
        /// <summary>
        /// 
        /// </summary>
        Accounts_Delete,
        /// <summary>
        /// 
        /// </summary>
        Accounts_GetCompanyNameById,
        /// <summary>
        /// 
        /// </summary>
        Accounts_GetCurrentPhone,
        /// <summary>
        /// 
        /// </summary>
        Accounts_ImportPaymentCorp,
        /// <summary>
        /// 
        /// </summary>
        Accounts_Confirm,
        /// <summary>
        /// 
        /// </summary>
        Accounts_ReSendSms,
        /// <summary>
        /// 
        /// </summary>
        Accounts_AddDefaultResources,
        /// <summary>
        /// 
        /// </summary>
        Accounts_ViewFullInfo,
        /// <summary>
        /// 
        /// </summary>
        Accounts_ViewFullInfoWithSegments,
        /// <summary>
        /// 
        /// </summary>
        Accounts_EditFullInfoWithSegments,
        /// <summary>
        /// 
        /// </summary>
        Accounts_PostMoveCompanyInfobase,
        /// <summary>
        /// 
        /// </summary>
        Accounts_MoveInfoBase,
        /// <summary>
        /// Файлы аккаунта в склеп
        /// </summary>
        AccountFilesToTomb,
        /// <summary>
        /// Получить информацию о сегменте
        /// </summary>
        Accounts_ViewSegment,
        /// <summary>
        /// Просмотр статуса аккаунта 
        /// </summary>
        Accounts_ViewIsDemo,

        /// <summary>
        /// Удалить сейл менеджера у аккаунта
        /// </summary>
        Account_RemoveSaleManager,

        /// <summary>
        /// Прикрепить сейл менеджера к аккаунту
        /// </summary>
        Account_AttachSaleManager,
        
        /// <summary>
        /// Удаление данных аккаунта 
        /// </summary>
        Accounts_DeleteData,
        #endregion

        #region AccountUsers
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_GetPasswordHash,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_View,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Add,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Edit,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Delete,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_VerificationUserByComfirmationCode,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_GetIDs,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Count,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_GetInactiveUserIDs,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_AddToAccount,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Activate,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Sync,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_SendPasswordChangeRequest,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_SetNewUserPassword,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_AuthTokenEdit,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_RoleEdit,
        /// <summary>
        /// 
        /// </summary>
        AccountUsers_Unsubscribe,
        /// <summary>
        /// Просмотр ролей пользователя
        /// </summary>
        AccountUserRoles_View,
        /// <summary>
        /// Просмотр информации о заблокированном сервисе
        /// </summary>
        AccountUsers_LockServiceInformation,
        /// <summary>
        /// Получение списка локалей
        /// </summary>
        AccountUsers_GetLocale,
        #endregion

        #region AccountDatabases
        /// <summary>
        /// 
        /// </summary>
        AccountDatabase_Detail,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabase_GetUserAccessList,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_Add,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_AddDbInTemplateForDelimiter,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabase_Change,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditFull,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditCaption,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditV82Name,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_AccessToAllAccountUsers,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_GrantAccessToUser,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_RemoveUserAccess,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditTemplate,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditStatus,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_ChangeDbFlagUsedWebServices,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditSupport,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditDbType,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditUsedWebServices,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_EditLaunchType,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_ViewSqlServerInfo,
        /// <summary>
        /// Получить список баз, доступных пользователю.
        /// </summary>
        AccountDatabases_ViewAccessDatabaseList,
        /// <summary>
        /// Редактирование (ЛинкЮзер, ЛинкПассворд)
        /// </summary>
        AccountDatabases_EditLinkInfo,

        /// <summary>
        /// Смена версии платформы
        /// </summary>
        AccountDatabases_EditPlatform,

        /// <summary>
        /// Смена версии платформы с 8.3 на 8.2
        /// </summary>
        AccountDatabases_ChangePlatformFrom83To82,

        /// <summary>
        /// Удаление базы в склеп
        /// </summary>
        AccountDatabase_DeleteToTomb,

        /// <summary>
        /// Удаление активных сессий
        /// </summary>
        AccountDatabases_DropSessions,

        /// <summary>
        /// Публикация
        /// </summary>
        AccountDatabases_Publish,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_Republish,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_RepublishWithChangingWebConfig,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_RepublishSegmentDatabases,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_Unpublish,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_DisableDomainAuthentication,
        /// <summary>
        /// 
        /// </summary>
        InstallExtension,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_WebAccess,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_RestartIisAppPool,
        /// <summary>
        /// Заблокировать/разблокировать
        /// </summary>
        AccountDatabases_LockUnlock,
        /// <summary>
        /// Разархивировать
        /// </summary>
        AccountDatabase_Restore,
		/// <summary>
        /// Скачать информационную базу со склпа.
        /// </summary>
        AccountDatabase_DownloadFromTomb,
		/// <summary>
        /// Execute 1C query with specific 1C base under specific user
        /// </summary>
        AccountDatabases_LinkedExecuteQuery,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_McobEditRolesOfDatabase,
        /// <summary>
        /// 
        /// </summary>
        AccountDatabases_McobRemoveUserRoles,
        /// <summary>
        /// Просмотр деталей карточки ИБ пока она в статусе NewItem
        /// </summary>
        AccountDatabases_ViewInfoDeteil,
        /// <summary>
        /// Просмотр информации по базе на разделителях
        /// </summary>
        AccountDatabases_ViewDelimitersInfo,
        /// <summary>
        /// Получить данные о пользователях из ZIP файла
        /// </summary>
        AccountDatabases_ParseUsersXml,
        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза
        /// </summary>
        AccountDatabases_ValidateVersionXml,

        /// <summary>
        /// Загрузить файл инофрмационной базы
        /// </summary>
        AccountDatabases_UploadDbFile,

        /// <summary>
        /// Создать базу на разделителях и выдать права доступа пользователю
        /// </summary>
        AccountDatabaseOnDelimiters_CreateAndAddAcces,

        /// <summary>
        /// Аудит и корректировка модели восстановления инф. базы
        /// </summary>
        AccountDatabase_ManageRestoreModel,

        #endregion

        #region AcDbAccesses
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_View,
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_Add,
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_Edit,
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_Delete,
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_AddAccessesForAllAccountsUsers,
        /// <summary>
        /// 
        /// </summary>
        AcDbAccesses_Lock,
        #endregion

        #region AccountUserSessions
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_View,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_Add,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_Edit,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_Delete,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_Login,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_LoginByToken,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_TokenByLogin,
        /// <summary>
        /// 
        /// </summary>
        AccountUserSessions_Auth,
        #endregion

        #region AcDbLocalUsers
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_View,
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_Add,
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_Edit,
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_Delete,
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_CloseAllConnections,
        /// <summary>
        /// 
        /// </summary>
        AcDbLocalUsers_OpenAllConnections,
        #endregion

        #region AcDbLinkTasks
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDbLinkTasks_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDbLinkTasks_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDbLinkTasks_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDbLinkTasks_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDbLinkTasks_WaitForResponse,
        #endregion

        #region AcDb1CDocuments
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_Post,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocuments_ExecuteTask,
        #endregion

        #region AcDb1CCatalog
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CCatalog_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CCatalog_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CCatalog_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CCatalog_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CCatalog_ExecuteTask,
        #endregion

        #region AcDb1CDocumentTableParts
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocumentTableParts_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocumentTableParts_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocumentTableParts_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocumentTableParts_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AcDb1CDocumentTableParts_ExecuteTask,
        #endregion

        #region CloudFileStorageServers
        /// <summary>
        /// Получить информацию по файловому хранилищу
        /// </summary>
        CloudFileStorageServers_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudFileStorageServers_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudFileStorageServers_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudFileStorageServers_Delete,
        #endregion

        #region CloudTerminalServers
        /// <summary>
        /// Поиск сеансов пользователей для текущего аккаунта
        /// </summary>
        CloudTerminalServers_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudTerminalServers_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudTerminalServers_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudTerminalServers_Delete,
        /// <summary>
        /// Завершить сессию пользователя
        /// </summary>
        CloudTerminalServers_CloseUserRdpSession,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudTerminalServers_SegmentView,
		/// <summary>
        /// сессии пользователя
        /// </summary>
        CloudTerminalServers_UsersRdpSessions,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudBackupServers_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudBackupServers_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudBackupServers_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudBackupServers_Delete,
        #endregion

        #region DbTemplates
        /// <summary>
        /// Получить список шаблонов.
        /// </summary>
        DbTemplates_View,
        /// <summary>
        /// Добавить шаблон в справочник
        /// </summary>
        DbTemplates_Add,
        /// <summary>
        /// Обновить шаблон
        /// </summary>
        DbTemplates_Edit,
        /// <summary>
        /// Удалить шаблон
        /// </summary>
        DbTemplates_Delete,
        #endregion

        #region ERPUpdatesData
        /// <summary>
        /// Рудимент
        /// </summary>
        ERPUpdatesData_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        ERPUpdatesData_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        ERPUpdatesData_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        ERPUpdatesData_Delete,
        #endregion

        #region CloudServices
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudServices_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudServices_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudServices_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CloudServices_Delete,
        /// <summary>
        /// Получить информацию по сервису Мой Диск
        /// </summary>
        CloudServices_ViewMyDisc,
        #endregion

        #region CSResources
        /// <summary>
        /// Рудимент
        /// </summary>
        CSResources_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSResources_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSResources_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSResources_Delete,
        #endregion

        #region AccountCSResourceValues
        /// <summary>
        /// Получить суммарное значение ресурсов облачного сервиса для указанного пользователя, 
        /// ресурса и аккаунта администратора пользователя, если таковой указан.
        /// </summary>
        AccountCSResourceValues_View,
        /// <summary>
        /// Начислить ресурс.
        /// </summary>
        AccountCSResourceValues_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountCSResourceValues_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountCSResourceValues_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountCSResourceValues_DecreseDay,
        /// <summary>
        /// Ресурсы платных сервисов
        /// </summary>
        AccountCSResource_PaidService,
        #endregion

        #region CSPerformanceCounters
        /// <summary>
        /// Рудимент
        /// </summary>
        CSPerformanceCounters_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSPerformanceCounters_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSPerformanceCounters_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CSPerformanceCounters_Delete,
        #endregion

        #region AccountDomains
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomains_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomains_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomains_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomains_Delete,
        #endregion

        #region AccountDomainUsers
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_Delete,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_GetIDs,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_Count,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountDomainUsers_FindAccountIDsByDomainUserName,
        #endregion

        #region CoreWorkerTasks
        /// <summary>
        /// Получить все задачи воркера
        /// </summary>
        CoreWorkerTasks_View,
        /// <summary>
        /// Добавить задачу воркера
        /// </summary>
        CoreWorkerTasks_Add,
        /// <summary>
        /// Установить тип задачи
        /// </summary>
        CoreWorkerTasks_Edit,
        /// <summary>
        /// Удалить задачу воркера
        /// </summary>
        CoreWorkerTasks_Delete,
        #endregion

        #region CoreWorkerTaskQueueCoreWorkers
        /// <summary>
        /// Отображение очереди задач воркера
        /// </summary>
        CoreWorkerTaskQueue_View,
        /// <summary>
        /// Добавить задачу в очередь
        /// </summary>
        CoreWorkerTaskQueue_Add,
        /// <summary>
        /// Установить комментарий для задачи
        /// </summary>
        CoreWorkerTaskQueue_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CoreWorkerTaskQueue_Delete,
        #endregion

        #region CoreWorkers
        /// <summary>
        /// Рудимент
        /// </summary>
        CoreWorkers_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        CoreWorkers_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        CoreWorkers_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        CoreWorkers_Delete,
        #endregion

        #region ProcCenter
        /// <summary>
        /// Рудимент
        /// </summary>
        ProcCenter,
        #endregion

        #region Messages
        /// <summary>
        /// Рудимент
        /// </summary>
        Messages_View,
        /// <summary>
        /// Отправить отчет воркера
        /// </summary>
        Messages_Add,
        #endregion

        #region ControlPanel_Menu
        /// <summary>
        /// Доступ к Админ панель -> Информационные базы
        /// </summary>
        ControlPanel_Main_AccountDatabase,
        /// <summary>
        /// Доступ к Админ панель -> Аккаунты
        /// </summary>
        ControlPanel_Main_Accounts,
        /// <summary>
        /// Доступ к Админ панель -> Рабочие процессы
        /// </summary>
        ControlPanel_Main_FlowProcess,
        /// <summary>
        /// Доступ к Сервисы -> Аренда 1С
        /// </summary>
        ControlPanel_ServiceMenu_AccountDatabase,
        /// <summary>
        /// Доступ к Сервисы -> Мой Офис
        /// </summary>
        ControlPanel_ServiceMenu_MyOffice,
        /// <summary>
        /// Доступ к Сервисы -> Саюри
        /// </summary>
        ControlPanel_ServiceMenu_Sauri,
        /// <summary>
        /// Доступ к Сервисы -> Fasta
        /// </summary>
        ControlPanel_ServiceMenu_Esdl,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ServiceMenu_Crm,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ServiceMenu_MyStaff,
        /// <summary>
        /// Доступ к Сервисы -> Мой Диск
        /// </summary>
        ControlPanel_ServiceMenu_MyDisk,
        /// <summary>
        /// Доступ к Сервисы -> Кастомные сервисы
        /// </summary>
        ControlPanel_ServiceMenu_CustomService,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ServiceMenu_MyTaxExpert,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_Menu_SendNotifications,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_Menu_RdpConnections,
        /// <summary>
        /// Доступ к Управление аккаунтом -> Инф. базы
        /// </summary>
        ControlPanel_ManagmentMenu_AccountDatabase,
        /// <summary>
        /// Доступ к Управление аккаунтом -> Пользователи
        /// </summary>
        ControlPanel_ManagmentMenu_AccountUsers,
        /// <summary>
        /// Доступ к Управление аккаунтом -> Данные о компании
        /// </summary>
        ControlPanel_ManagmentMenu_AccountDatas,
        /// <summary>
        /// Доступ к Управление аккаунтом -> Баланс
        /// </summary>
        ControlPanel_ManagmentMenu_BillingAccounts,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ManagmentMenu_Recognition42,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ManagmentMenu_Partners,
        /// <summary>
        /// Доступ к Админ панель -> Задачи
        /// </summary>
        ControlPanel_ManagmentMenu_ConfigurationCloud,
        /// <summary>
        /// Доступ к Админ панель -> Сегменты
        /// </summary>
        ControlPanel_ManagmentMenu_SegmentCloud,
        /// <summary>
        /// Доступ к Админ панель -> Логирование
        /// </summary>
        ControlPanel_ManagmentMenu_Logging,
        /// <summary>
        /// Доступ к ТиИ/АО -> АРМ
        /// </summary>
        ControlPanel_ManagmentMenu_ARM,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_ManagmentMenu_UpdateTemplate,
        /// <summary>
        /// Доступ к Админ панель -> Биллинг уведомления
        /// </summary>
        ControlPanel_ManagmentMenu_InvoiceNotification,
        /// <summary>
        /// Доступ к Справочники -> Конфигурации
        /// </summary>
        ControlPanel_ManagmentMenu_CatalogOfConfigurations,
        /// <summary>
        /// Доступ к Справочники -> Отрасли
        /// </summary>
        ControlPanel_ManagmentMenu_IndustryReference,
        /// <summary>
        /// Доступ к Справочники -> Шаблоны
        /// </summary>
        ControlPanel_ManagmentMenu_DbTemplateReference,
        /// <summary>
        /// Доступ к Справочники -> Базы на разделителях
        /// </summary>
		ControlPanel_ManagmentMenu_DbTemplateDelimiters,
        /// <summary>
        /// Доступ к Справочники -> Поставщики
        /// </summary>
		ControlPanel_ManagmentMenu_SupplierReference,
        /// <summary>
        /// Доступ к Справочники -> Печатные формы
        /// </summary>
        ControlPanel_ManagmentMenu_PrintedHtmlFormReference,
        /// <summary>
        /// Доступ к Справочники -> Служебные аккаунты
        /// </summary>
        ControlPanel_ManagmentMenu_ServiceAccounts,
        /// <summary>
        /// Доступ к Админ панель -> Партнеры
        /// </summary>
        ControlPanel_Main_Partners,
        /// <summary>
        /// Доступ к Справочники -> Службы Clouds 42
        /// </summary>
        ControlPanel_ManagmentMenu_CloudServices,
        /// <summary>
        /// Создание задачи для воркера
        /// </summary>
        ControlPanel_ManagementMenu_CreateTask,
        #endregion

        #region ControlPanel_EditResource
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_EditResources_Crm,
        /// <summary>
        /// Изменить конфигурацию сервиса Мой Диск
        /// </summary>
        ControlPanel_EditResources_MyDisk,
        /// <summary>
        /// Изменить конфигурацию сервиса Аренда 1С
        /// </summary>
        ControlPanel_EditResources_Rent1C,
        /// <summary>
        /// Изменить конфигурацию сервиса Fasta
        /// </summary>
        ControlPanel_EditResources_Esdl,
        /// <summary>
        /// Доступ к вкладке Управление сервиса Аренда 1С
        /// </summary>
        ControlPanel_EditResources_Rent1CManagement,
        /// <summary>
        /// Доступ к вкладке Управление сервиса Аренда 1С
        /// </summary>
        ControlPanel_EditResources_ChangeRent1CData,
        /// <summary>
        /// Доступ к вкладке Управление сервиса Аренда 1С
        /// </summary>
        ControlPanel_EditResources_ChangeRent1CDemoPeriod,
        /// <summary>
        /// Доступ к вкладке Управление сервиса Аренда 1С
        /// </summary>
        ControlPanel_EditResources_ChangeRent1CResource,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_AddResource,
        /// <summary>
        /// Рудимент
        /// </summary>
        ControlPanel_EditResource,
        /// <summary>
        /// Просмотреть доп. ресурсы Аренды 1С
        /// </summary>
        ControlPanel_ViewResource,
        /// <summary>
        /// Просмотреть ресурсы сервиса Мой Диск для вип аккаунта
        /// </summary>
        ControlPanel_ViewMyDiskVipInfo,
        /// <summary>
        /// Установить менеджера для аккаунта
        /// </summary>
        ControlPanel_SetSalemanager,
        /// <summary>
        /// Перейти в контекст аккаунта
        /// </summary>
        ControlPanel_ModeOtherAccount,
        /// <summary>
        /// Установить локаль аккаунта
        /// </summary>
        ControlPanel_SetLocale,
        #endregion

        #region Payments
        /// <summary>
        /// Добавление платежа
        /// </summary>
        Payments_Add,

        /// <summary>
        /// Просмотр платежей
        /// </summary>
        Payments_View,       
        
        /// <summary>
        /// Подтверждение платежа
        /// </summary>
        Payments_ConfirmPayment,

        /// <summary>
        /// Подготовка платежа
        /// </summary>
        Payments_PreparePayment,

        /// <summary>
        /// Совершение платежа
        /// </summary>
        Payments_Bill,

        /// <summary>
        /// Редактировние платежа
        /// </summary>
        Payments_Edit,

        /// <summary>
        /// Отображение счета на оплату
        /// </summary>
        Invoices_View,

        /// <summary>
        /// Подтверждение счета на оплату
        /// </summary>
        Invoices_Confirmation,

        /// <summary>
        /// Получение суммы платежей
        /// </summary>
        Payments_Amounts,

        /// <summary>
        /// Получение фискального чека
        /// </summary>
        Invoices_GetFiscalReceipt,

        /// <summary>
        /// Создание фискального чека
        /// </summary>
        Invoices_CreateFiscalReceipt,

        /// <summary>
        /// Возможность сменить назначение платежа
        /// </summary>
        Payments_ChangeDescription,

        /// <summary>
        /// Получение информации о ПС
        /// </summary>
        Payments_System,
        #endregion

        #region PromisePayment
        /// <summary>
        /// Продлить обещанный платёж
        /// </summary>
        PromisePayment_Prolong,
        /// <summary>
        /// Создать обещанный платеж
        /// </summary>
        PromisePayment_CreatePromisePayment,
        /// <summary>
        /// Преждeвременное погашение обещанного платежа
        /// </summary>
        PromisePayment_RepayPromisePayment,
        #endregion

        #region Resources
        /// <summary>
        /// Назначить ресурс сервиса пользователю
        /// </summary>
        ManageResources,
        /// <summary>
        /// Получить конфигурацию ресурсов сервиса
        /// </summary>
        Resources_GetResConf,
        /// <summary>
        /// Пересчет занятого место на диске
        /// </summary>
        Resources_RecalculateMyDisk,
        /// <summary>
        /// Установка конфигурации ресурса
        /// </summary>
        Resources_SetResConf,
        #endregion

        #region AsDbLinkEvent
        /// <summary>
        /// Рудимент
        /// </summary>
        AsDbLinkEvent_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AsDbLinkEvent_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AsDbLinkEvent_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AsDbLinkEvent_Delete,
        #endregion

        #region AccountQuota
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountQuota_Add,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountQuota_View,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountQuota_Edit,
        /// <summary>
        /// Рудимент
        /// </summary>
        AccountQuota_Delete,
        #endregion

        #region RolesJHO
        /// <summary>
        /// Добавить роли JHO
        /// </summary>
        RolesJHO_Add,
        /// <summary>
        /// Просмотреть роли JHO
        /// </summary>
        RolesJHO_View,
        /// <summary>
        /// Изменить роли JHO
        /// </summary>
        RolesJHO_Edit,
        /// <summary>
        /// Удалить роли JHO
        /// </summary>
        RolesJHO_Delete,
        #endregion

        #region Stripe
        /// <summary>
        /// Просмотр баланса аккаунта
        /// </summary>
        Balance_View,
        #endregion

        #region Segments
        /// <summary>
        /// Редактирование сегмента или его элемента
        /// </summary>
        Segment_Edit,
        /// <summary>
        /// Удаление сегмента или его элемента
        /// </summary>
        Segment_Delete,
        /// <summary>
        /// Добавление сегмента или его элемента
        /// </summary>
        Segment_Add,
        /// <summary>
        /// Просмотр сегмента или его элемента
        /// </summary>
        Segment_View,
        /// <summary>
        /// Сегмент - получение путей до до 1С
        /// </summary>
        Segment_GetFoldersPathsTo1C,
        /// <summary>
        /// Сегмент - миграция
        /// </summary>
        Segment_Migration,
        #endregion

        #region Migration
        /// <summary>
        /// Сменить сегмент для аккаунта
        /// </summary>
        AccountMigration,
        /// <summary>
        /// Перенести инф. базы в новое файловое хранилище
        /// </summary>
        AccountDatabaseMigration,
        /// <summary>
        /// Получение информации о базах аккаунта для переноса между хранилищами
        /// </summary>
        AccountDatabase_MigrationListView,
        #endregion

        #region ProvidedServices
        /// <summary>
        /// Отобразить список оказанных услуг 
        /// </summary>
        ProvidedServices_View,
        /// <summary>
        /// Вставить или обновить акт о выполненных работах
        /// </summary>
        ProvidedServices_InsertOrUpdateAct,
        /// <summary>
        /// Удалить акт о выполненных работах
        /// </summary>
        ProvidedServices_DeleteAct,
        #endregion

        #region Cloud42Service

        /// <summary>
        /// Отобразить список учетных записей с ролью "Служба"
        /// </summary>
        Cloud42Service_View,
        #endregion

        #region AccountDatabaseBackup
        
        /// <summary>
        /// Перенести бэкап в склеп.
        /// </summary>
        AccountDatabaseBackup_MoveToTomp,
        
        /// <summary>
        /// Создать бэкап информационной базы
        /// </summary>
        AccountDatabaseBackup_Add,

        /// <summary>
        /// Удалить бэкап инф. базы
        /// </summary>
        AccountDatabaseBackup_Delete,

        /// <summary>
        /// Провести аудит бэкапа инф. базы
        /// </summary>
        AccountDatabaseBackup_Audit,

        /// <summary>
        /// Просмотр истории бэкапирования
        /// </summary>
        AccountDatabaseBackupHistory_View,
        #endregion

        #region BillingManager
        /// <summary>
        /// Обработать просроченные обещанные платежи.
        /// </summary>
        BillingManager_ProcessExpiredPromisePayments,
        /// <summary>
        /// Продлить или заблокировать просроченные сервисы.
        /// </summary>
        BillingManager_ProlongOrLockExpiredServices,
        /// <summary>
        /// Уведомить клиентов о скорой блокировке сервисов.
        /// </summary>
        BillingManager_NotifyBeforeLockServices,
        /// <summary>
        /// Разблокировать заблокированные сервисы на которые есть деньги что бы разблокировать.
        /// </summary>
        BillingManager_ProlongLockedServices,
        /// <summary>
        /// Заблокировать сервисы с просроченным демо периодом
        /// </summary>
        BillingManager_LockExpireDemoServices,
        #endregion

        #region AcDbSupport
        /// <summary>
        /// Актуализация данных поддержки информационной базы.
        /// </summary>
        AcDbSupport_AutorizationConnect,
        /// <summary>
        /// Включение автообновления.
        /// </summary>
        AcDbSupport_TurnAutoUpdate,
        /// <summary>
        /// Включить в план на автообновление.
        /// </summary>
        AcDbSupport_PlanUpdateVersion,
        /// <summary>
        /// Включить в план на автообновление.
        /// </summary>
        AcDbSupport_PlanTehSupport,
        /// <summary>
        /// Запустить провдение ТиИ.
        /// </summary>
        AcDbSupport_ProcessTehSupport,
        /// <summary>
        /// Запустить провдение АО.
        /// </summary>
        AcDbSupport_ProcessAutoUpdate,
        #endregion

        #region CatalogOfConfigurations1C
        /// <summary>
        /// Редактирование конфигурации в каталоге
        /// </summary>
        EditConfiguration1C,
        /// <summary>
        /// Добавление новой конф. в каталог
        /// </summary>
        AddNewConfiguration1C,
        /// <summary>
        /// Удаление конф. из каталога
        /// </summary>
        DeleteConfiguration1C,
        /// <summary>
        /// Просмотр и получение конф. из каталога
        /// </summary>
        ViewConfiguration1C,
        #endregion

        #region IndustryReference

        /// <summary>
        /// Редактирование отрасли
        /// </summary>
        EditIndustry,

        /// <summary>
        /// Добавление новой отрасли
        /// </summary>
        AddNewIndustry,

        /// <summary>
        /// Удаление отрасли
        /// </summary>
        DeleteIndustry,

        /// <summary>
        /// Просмотр и получение отрасли
        /// </summary>
        ViewIndustries,

        #endregion

        #region SupplierReference

        /// <summary>
        /// Справочник поставщиков. Просмотр
        /// </summary>
        SupplierReference_View,

        /// <summary>
        /// Справочник поставщиков. Создание
        /// </summary>
        SupplierReference_Create,

        /// <summary>
        /// Справочник поставщиков. Изменение
        /// </summary>
        SupplierReference_Edit,

        /// <summary>
        /// Справочник поставщиков. Удаление
        /// </summary>
        SupplierReference_Delete,

        #endregion

        #region PlatformVersionReferences

        /// <summary>
        /// Редактирование платф. в каталоге
        /// </summary>
        UpdatePlatformVersionReference,

        /// <summary>
        /// Добавление новой платф. в каталог
        /// </summary>
        AddNewPlatformVersionReference,

        /// <summary>
        /// Удаление платф. из каталога
        /// </summary>
        DeletePlatformVersionReference,

        /// <summary>
        /// Просмотр и получение платф. из каталога
        /// </summary>
        ViewPlatformVersionReference,

        #endregion

        #region PublishNodeReferences

        /// <summary>
        /// Редактирование ноды публикаций в каталоге
        /// </summary>
        UpdatePublishNodeReference,

        /// <summary>
        /// Добавление новой ноды публикаций в каталог
        /// </summary>
        AddNewPublishNodeReference,

        /// <summary>
        /// Удаление ноды публикаций из каталога
        /// </summary>
        DeletePublishNodeReference,

        /// <summary>
        /// Просмотр и получение ноды публикаций из каталога
        /// </summary>
        ViewPublishNodeReference,

        #endregion

        #region CloudServiceTerminalFarm

        /// <summary>
        /// Редактирование фермы тс в каталоге
        /// </summary>
        UpdateCloudServiceTerminalFarm,

        /// <summary>
        /// Добавление новой фермы тс в каталог
        /// </summary>
        AddNewCloudServiceTerminalFarm,

        /// <summary>
        /// Удаление фермы тс из каталога
        /// </summary>
        DeleteCloudServiceTerminalFarm,

        /// <summary>
        /// Просмотр и получение фермы тс из каталога
        /// </summary>
        ViewCloudServiceTerminalFarm,

        #endregion

        #region CloudServiceSegment

        /// <summary>
        /// Редактирование сегмента в каталоге
        /// </summary>
        UpdateCloudServiceSegment,

        /// <summary>
        /// Добавление нового сегмента в каталог
        /// </summary>
        AddNewCloudServiceSegment,

        /// <summary>
        /// Удаление сегмента из каталога
        /// </summary>
        DeleteCloudServiceSegment,

        /// <summary>
        /// Просмотр и получение сегмента из каталога
        /// </summary>
        ViewCloudServiceSegment,

        #endregion


        #region CloudServiceEnterpriseServer

        /// <summary>
        /// Редактирование сервера 1С:Предприятие в каталоге
        /// </summary>
        UpdateCloudServiceEnterpriseServer,

        /// <summary>
        /// Добавление нового сервера 1С:Предприятие в каталог
        /// </summary>
        AddNewCloudServiceEnterpriseServer,

        /// <summary>
        /// Удаление сервера 1С:Предприятие из каталога
        /// </summary>
        DeleteCloudServiceEnterpriseServer,

        /// <summary>
        /// Просмотр и получение сервера 1С:Предприятие из каталога
        /// </summary>
        ViewCloudServiceEnterpriseServer,

        #endregion

        #region ArrServer

        /// <summary>
        /// Просмотр и получение сервера ARR
        /// </summary>
        ViewArrServer,

        /// <summary>
        /// Добавление нового сервера ARR
        /// </summary>
        AddNewArrServer,

        /// <summary>
        /// Редактирование сервера ARR
        /// </summary>
        EditArrServer,

        /// <summary>
        /// Удаление сервера ARR
        /// </summary>
        DeleteArrServer,

        /// <summary>
        /// Проверить доступность сервера ARR
        /// </summary>
        CheckArrServeAvailability,

        #endregion

        #region DbTemplateDelimiters
        /// <summary>
        /// Редактирование записи о базе на разделителях в каталоге
        /// </summary>
        EditDbTemplateDelimitersReferences,
		/// <summary>
		/// Добавление записи о базе на разделителях в каталоге
		/// </summary>
		AddNewDbTemplateDelimitersReferences,
		/// <summary>
		/// Удаление записи о базе на разделителях в каталоге
		/// </summary>
		DeleteDbTemplateDelimitersReferences,
		/// <summary>
		/// Просмотр и получение информации записи о базе на разделителях в каталоге
		/// </summary>
		ViewDbTemplateDelimitersReferences,
	    /// <summary>
	    /// Добавление записи о зонах и изменение статуса базы
	    /// </summary>
	    EditAcDbStateAndAddDelimiters,
		/// <summary>
		/// изменение статуса базы и запуск таски для баз на разделителях
		/// </summary>
		SetStatusAndRunTaskForDbOnDelimiters,
        /// <summary>
        /// Обновить версию релиза конфигурации
        /// </summary>
        UpdateConfigurationReleaseVersions,
		/// <summary>
        /// Добавление доступов к базе на разделителях
        /// </summary>
        AddAccessToDbOnDelimiters,
        /// <summary>
        /// Управление доступом к базе на разделителях
        /// </summary>
        ManageAccessToDbOndelimiters,
        /// <summary>
        /// Удаление доступа для одного пользователя
        /// </summary>
        DeleteAccessFromDelimiterDatabase,

        /// <summary>
        /// Установить статус доступа к инф. базе на разделителях
        /// </summary>
        SetAcDbOnDelimitersAccessStatus,
        #endregion

        #region PerfomSystemDiagnostig
        /// <summary>
        /// Проведение диагностики системы.
        /// </summary>
        PerfomSystemDiagnostig,
        /// <summary>
        /// Создание тестовых данных
        /// </summary>
        CreatingTestData,
        #endregion

        #region BillingService
        /// <summary>
        /// Получить список объектов сервисов.
        /// </summary>
        BillingService_GetList,
        /// <summary>
        /// Получить детальную информацию по сервсису.
        /// </summary>
        BillingService_GetInfo,
        /// <summary>
        /// Получить справочные данные для сервиса биллинга
        /// </summary>
        BillingService_GetReferenceData,
        /// <summary>
        /// Валидация поля сервиса биллинга
        /// </summary>
        BillingService_FieldValidation,
        /// <summary>
        /// Получить текстовое описание назначения платежа по сервису
        /// </summary>
        BillingService_GetPaymentPurposeText,
        /// <summary>
        /// Изменение ресурсов сервиса
        /// </summary>
        BillingService_EditResources,
        /// <summary>
        /// Принять демо подписку для сервиса
        /// </summary>
        BillingService_ApplyDemoSubscribe,
        /// <summary>
        /// Активировать сервис у существуещего пользователя
        /// </summary>
        BillingService_ActivateForExistingAccount,
        /// <summary>
        /// Создать сервис биллинга
        /// </summary>
        BillingService_Create,
        /// <summary>
        /// Редактирование сервиса
        /// </summary>
        BillingService_Edit,
        /// <summary>
        /// Получить черновик сервиса
        /// </summary>
        BillingService_GetDraft,
        /// <summary>
        /// Сохранить черновик сервиса
        /// </summary>
        BillingService_SaveDraft,
        /// <summary>
        /// Управление сервисом
        /// </summary>
        BillingService_Control,
        /// <summary>
        /// Получить список услуг сервиса.
        /// </summary>
        BillingServiceType_GetList,
        /// <summary>
        /// Получить детальную иформацию по услуге сервиса.
        /// </summary>
        BillingServiceType_GetInfo,
        /// <summary>
        /// Получить детальную иформацию по услугам сервиса.
        /// </summary>
        BillingServiceTypes_GetInfo,
        /// <summary>
        /// Получить список пользователей с информацией о услугах сервиса
        /// </summary>
        BillingServiceTypes_GetAccountUsers,
        /// <summary>
        /// Расчитать стоимость услуг сервиса
        /// </summary>
        BillingServiceTypes_CalculateCost,
        /// <summary>
        /// Получить список подключенных услуг сервиса у клиента.
        /// </summary>
        BillingServiceType_GetStateForUser,
        /// <summary>
        /// Изменить подписку сервиса.
        /// </summary>
        BillingService_ChangeSubscribe,

        /// <summary>
        /// Активировать Аренду 1С
        /// </summary>
        BillingService_ActivateRent1C,
        #endregion

        #region Partners
        /// <summary>
        /// Получить информацию для партнера
        /// </summary>
        Partners_GetInfo,
        /// <summary>
        /// Получить сервисы партнера
        /// </summary>
        Partner_GetBillingServices,
        /// <summary>
        /// Получить клиентов партнера
        /// </summary>
        Partner_GetClients,
        /// <summary>
        /// Получить транзакции партнера
        /// </summary>
        Partner_GetTransactions,

        /// <summary>
        /// Привязать аккаунт к партнеру
        /// </summary>
        Partner_BindAccountToPartner,

        /// <summary>
        /// Получить данные
        /// по пересчету стоимости сервисов 
        /// </summary>
        Partner_GetRecalculationServiceCostData,
        #endregion

        #region ProcessFlow
        /// <summary>
        /// Просмотр списка рабочих процессов.
        /// </summary>
        ProcessFlow_ViewList,
        /// <summary>
        /// Перезапуск рабочего процесса.
        /// </summary>
        ProcessFlow_Retry,
        #endregion

        #region Agency
        /// <summary>
        /// Получить агентские договоры
        /// </summary>
        Agency_GetRequisites,
        /// <summary>
        /// Получить шаблон агентского договора
        /// </summary>
        Agency_GetContractTemplate,

        /// <summary>
        /// Создание реквизитов агента
        /// </summary>
        Agency_CreateRequisites,
        /// <summary>
        /// Удаление реквизитов агента
        /// </summary>
        Agency_RemoveRequisites,
        /// <summary>
        /// Редактирование реквизитов агента
        /// </summary>
        Agency_EditRequisites,
        /// <summary>
        /// Сменить статус реквизитов
        /// </summary>
        Agency_EditRequisitesStatus,
        /// <summary>
        /// Получить заявки на вывод средств
        /// </summary>
        Agency_GetCashOutRequests,

        /// <summary>
        /// Получить заявки на расходование средств в статусе "Новая"
        /// </summary>
        Agency_GetAgentCashOutRequestsInStatusNew,

        /// <summary>
        /// Печать отчета агента
        /// </summary>
        Agency_PrintAgentReport,
        /// <summary>
        /// Создание заявки на вывод средств
        /// </summary>
        Agency_CreateCashOutRequest,
        /// <summary>
        /// Редактирование заявки на вывод средств
        /// </summary>
        Agency_EditCashOutRequest,
        /// <summary>
        /// Редактирование статуса заявки на вывод средств
        /// </summary>
        Agency_EditCashOutRequestStatus,
        /// <summary>
        /// Удаление заявки на вывод средств
        /// </summary>
        Agency_RemoveCashOutRequest,
        /// <summary>
        /// Создание агентского платежа.
        /// </summary>
        Agency_CreatePayment,
        /// <summary>
        /// Просмотр файла документа агента
        /// </summary>
        Agency_OpenDocumentFile,
        /// <summary>
        /// Получить список моделей данных по счету
        /// </summary>
        GetInvoiceReportDataDcs,
        /// <summary>
        /// Получить список моделей данных ресурсов
        /// </summary>
        GetResourceReportDataDcs,
        /// <summary>
        /// Получение список моделей данных по платежу для отчета
        /// </summary>
        GetPaymentReportDataDcs,
        /// <summary>
        /// Получение список моделей данных по пользователям для отчета
        /// </summary>
        GetAccountUserReportDataDcs,
        /// <summary>
        /// Получение печатных форм HTML
        /// </summary>
        GetPrintedHtmlForm,
        /// <summary>
        /// Изменение печатной формы
        /// </summary>
        ChangePrintedHtmlForm,
        /// <summary>
        /// Получение списка моделей данных по аккаунтам для отчета
        /// </summary>
        GetAccountReportDataDcs,
        /// <summary>
        /// Получение списка моделей данных по инф. базам для отчета
        /// </summary>
        GetAccountDatabaseReportDataDcs,

        /// <summary>
        /// Экспорт данных облака в Google таблицы
        /// </summary>
        ExportReportDataToGoogleSheets,

        /// <summary>
        /// Создание агентского соглашения
        /// </summary>
        AgencyAgreement_CreateAgencyAgreement,

        /// <summary>
        /// Получение данных агентских соглашений
        /// </summary>
        AgencyAgreement_GetAgencyAgreementsData,

        /// <summary>
        /// Получение детальной информации
        /// об агентском соглашении
        /// </summary>
        AgencyAgreement_GetAgencyAgreementDetails,

        /// <summary>
        /// Печать агентского соглашения
        /// </summary>
        AgencyAgreement_PrintAgencyAgreement,

        /// <summary>
        /// Принятие агентского соглашения
        /// </summary>
        AgencyAgreement_ApplyAgencyAgreement,

        /// <summary>
        /// Создание заявки на перевод баланса агента
        /// </summary>
        AgentTransferBalanseRequest_Create,

        /// <summary>
        /// Создание материнской базы разделителей
        /// </summary>
        DelimiterSourceAccountDatabase_Create,

        /// <summary>
        /// Получение материнской базы разделителей
        /// </summary>
        DelimiterSourceAccountDatabase_Get,

        /// <summary>
        /// Редактирование материнской базы разделителей
        /// </summary>
        DelimiterSourceAccountDatabase_Edit,

        /// <summary>
        /// Удаление материнской базы разделителей
        /// </summary>
        DelimiterSourceAccountDatabase_Remove,

        /// <summary>
        /// Получение данных материнских баз разделителей
        /// </summary>
        DelimiterSourceAccountDatabase_GetData,
        #endregion

        #region Cfu
        /// <summary>
        /// Скачивание доступных Cfu
        /// </summary>
        Cfu_DownloadAvailable,
        #endregion

        #region Configuration1C
        /// <summary>
        /// Получить карту обновлений конфиугации 1С
        /// </summary>
        Configuration1C_UpdateMap,
        /// <summary>
        /// Получить путь до обновлений конфиугации 1С
        /// </summary>
        Configuration1C_CfuFullPath,
        #endregion

        #region DaData
        /// <summary>
        /// Получения реквизитов с сервиса DaData
        /// </summary>
        DaData_View,
        #endregion

        #region DbTemplateUpdate
        /// <summary>
        /// Просмотр обновлений шаблона
        /// </summary>
        DbTemplateUpdate_View,
		/// <summary>
        /// Добавление обновлений шаблона
        /// </summary>
        DbTemplateUpdate_Add,
		/// <summary>
        /// Изменение обновлений шаблона
        /// </summary>
        DbTemplateUpdate_Edit,
		/// <summary>
        /// Удаление обновлений шаблона
        /// </summary>
        DbTemplateUpdate_Delete,
        #endregion

        #region Link
        /// <summary>
        /// Получение списка активности
        /// </summary>
        LinkActivitySchedules_Get,

        /// <summary>
        /// Получение данных имперсонации пользоватяля.
        /// </summary>
        LinkUserImpersonate_Get,
        #endregion

        #region InvoiceNotification
        /// <summary>
        /// Просмотр уведомлений о платежах
        /// </summary>
        InvoiceNotification_View,
        /// <summary>
        /// Отправка уведомлений о платежах
        /// </summary>
        InvoiceNotification_Send,
        /// <summary>
        /// Изменение уведомлений о платежах
        /// </summary>
        InvoiceNotification_Edit,
        #endregion

        #region UploadFileApi

        /// <summary>
        /// Склеить части в исходный файл
        /// </summary>
        MergeChunksToInitialFile,

        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        UploadChunkOfFile,

        /// <summary>
        /// Запустить процесс загрузки файла
        /// </summary>
        InitUploadProcess,

        /// <summary>
        /// Создать служебный аккаунт
        /// </summary>
        CreateServiceAccount,

        /// <summary>
        /// Удалить служебный аккаунт
        /// </summary>
        DeleteServiceAccount,

        /// <summary>
        /// Получить список служебных аккаунтов
        /// </summary>
        GetServiceAccounts,

        #endregion

        /// <summary>
        /// Получение данных
        /// заявок сервиса на модерацию
        /// </summary>
        ChangeServiceRequest_GetData,

        /// <summary>
        /// Обработать результат модерации заявки
        /// на изменения сервиса
        /// </summary>
        ChangeServiceRequest_ProcessModerationResult,

        /// <summary>
        /// Получить данные фермы серверов
        /// </summary>
        ServerFarm_GetData,

        /// <summary>
        /// Создать ферму серверов
        /// </summary>
        ServerFarm_Create,

        /// <summary>
        /// Редактировать ферму серверов
        /// </summary>
        ServerFarm_Edit,

        /// <summary>
        /// Удалить ферму серверов
        /// </summary>
        ServerFarm_Delete,

        /// <summary>
        /// Получить данные кластеров ARR
        /// </summary>
        ArrCluster_GetData,

        /// <summary>
        /// Создать кластер ARR
        /// </summary>
        ArrCluster_Create,

        /// <summary>
        /// Редактировать кластер ARR
        /// </summary>
        ArrCluster_Edit,

        /// <summary>
        /// Удалить кластер ARR
        /// </summary>
        ArrCluster_Delete,

        /// <summary>
        /// Создать фермы на Arr кластере
        /// </summary>
        ArrCluster_CreateFarmsOnArrServer,

        /// <summary>
        /// Удаление ферм на ARR сервере
        /// </summary>
        ArrCluster_RemoveFarmsOnArrServer,

        /// <summary>
        /// Удаление серверов ферм на сервере ARR
        /// </summary>
        ServerFarm_RemoveServers,

        /// <summary>
        /// Создание серверов ферм на сервере ARR
        /// </summary>
        ServerFarm_CreateServers,

        /// <summary>
        /// Изменение демо периода для сервиса Fasta
        /// </summary>
        ControlPanel_Fasta_ChangeDemoPeriod,

        /// <summary>
        /// Отображение тех поддержки инф. базы
        /// </summary>
        AccountDatabases_DisplaySupportDb,

        /// <summary>
        /// Зарегестрировать результат установки расширения сервиса
        /// для базы в область данных
        /// </summary>
        RegisterExtentionInstallStatus,

        /// <summary>
        /// Зарегестрировать результат удаления расширения сервиса
        /// для базы с области данных
        /// </summary>
        RegisterExtentionDeleteSatus,

        /// <summary>
        /// Управление расширением сервиса для информационной базы
        /// в МС
        /// </summary>
        ManageServiceExtensionDatabaseInMs,

        /// <summary>
        /// Получить расширение сервиса для информационной базы
        /// </summary>
        GetServiceExtensionDatabase,

        /// <summary>
        /// Управление расширением сервиса для информационной базы
        /// </summary>
        ManageServiceExtensionDatabase,

        /// <summary>
        /// Установить статус активации расширения
        /// для инф. базы в МС
        /// </summary>
        SetServiceExtensionDatabaseActivationStatusInMs,
		
        /// <summary>
        /// Обработать удаленные услуги для аккаунтов
        /// </summary>
        ProcessDeletedServiceTypesForAccounts,

        /// <summary>
        /// Удалить время активности услуги сервиса после ее удаления для аккаунтов
        /// </summary>
        RemoveBillingServiceTypeAvailabilityDateTime,

        /// <summary>
        /// Обновить время активности услуги сервиса после ее удаления для аккаунтов
        /// </summary>
        UpdateBillingServiceTypeAvailabilityDateTime,
		
		/// <summary>
        /// Загрузка файла инф. базы в МС
        /// </summary>
        AccountDatabase_UploadZipFileToSm,

        /// <summary>
        /// Получить данные АО для инф. баз 
        /// </summary>
        AccountDatabase_GetAutoUpdateData,

        /// <summary>
        /// Получить названия конфигураций 1С
        /// </summary>
        AccountDatabase_GetConfiguration1CNames,

        /// <summary>
        /// Получить информацию по сейл менеджеру для аккаунта
        /// </summary>
        Account_GetSaleManager,

        /// <summary>
        /// Получить модель шаблона рекламного баннера
        /// </summary>
        AdvertisingBannerTemplate_Get,

        /// <summary>
        /// Посчитать аудиторию рекламного баннера
        /// </summary>
        AdvertisingBannerAudience_Calculate,

        /// <summary>
        /// Создать шаблон рекламного баннера
        /// </summary>
        AdvertisingBannerTemplate_Create,

        /// <summary>
        /// Редактировать шаблон рекламного баннера
        /// </summary>
        AdvertisingBannerTemplate_Edit,

        /// <summary>
        /// Страница маркетинга
        /// </summary>
        ControlPanel_Main_Marketing,

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        BillingAccount_GetBalance,

        /// <summary>
        /// Получить дату пролонгации ресурс конфигурации
        /// </summary>
        ResourceConfiguration_GetExpireDate,

        /// <summary>
        /// Отменить задачу
        /// </summary>
        CoreWorkerTasksQueue_Cancel,

        /// <summary>
        /// Получить Id аккаунта по логину пользователя
        /// </summary>
        Account_GetIdByUserLogin,

        /// <summary>
        /// Получить задачи воркера
        /// </summary>
        CoreWorkerTask_GetAll,

        /// <summary>
        /// Выполнить редактирование задачи воркера
        /// </summary>
        CoreWorkerTask_Edit,

        /// <summary>
        /// Найти задачи воркера
        /// </summary>
        CoreWorkerTask_Search,

        /// <summary>
        /// Получить шаблоны писем
        /// </summary>
        LetterTemplate_Get,

        /// <summary>
        /// Редактировать шаблон письма
        /// </summary>
        LetterTemplate_Edit,

        /// <summary>
        /// Отправить тестовое письмо
        /// </summary>
        LetterTemplate_SendTestLetter,

        /// <summary>
        /// Удалить шаблон рекламного баннера
        /// </summary>
        AdvertisingBannerTemplate_Delete,

        /// <summary>
        /// Уведомить об об изменении ресурсов главного сервиса
        /// </summary>
        AboutMainServiceResourcesChanges_Notify,

        /// <summary>
        /// Установить результат аудита
        /// версии файла разработки сервиса биллинга
        /// </summary>
        Service1CFileVersion_SetAuditResult,

        /// <summary>
        /// Получить данные файлов разработки сервиса биллинга
        /// </summary>
        Service1CFile_GetData,

        /// <summary>
        /// Проверить файл разработки 1С
        /// </summary>
        CheckService1CFile,

        /// <summary>
        /// Создать файл разработки 1С с новой версией
        /// </summary>
        Service1CFile_CreateWithNewVersion,

        /// <summary>
        /// Уведомить о результате аудита версии файла разработки 1С
        /// </summary>
        AboutService1CFileVersionAuditResult_Notify,

        /// <summary>
        /// Провести аудит очереди задач воркера
        /// </summary>
        CoreWorkerTasksQueue_Audit,

        /// <summary>
        /// Отправить на аудит версию файла разработки 1C
        /// </summary>
        Service1CFileVersion_SubmitForAudit,

        /// <summary>
        /// Получить метаданные файла разработки 1С
        /// </summary>
        Service1CFile_GetMetadata,

        /// <summary>
        /// Установить активность сервиса
        /// </summary>
        BillingService_SetActivity,

        /// <summary>
        /// Создать базу на разделителях(без регистрации в МС)
        /// </summary>
        DatabaseOnDelimiters_CreateWithoutRegistrationInMs,

        /// <summary>
        /// Получить список данных о шаблонах инф. баз
        /// которые совместимы с сервисом
        /// </summary>
        BillingService_GetDatabaseTemplates,

        /// <summary>
        /// Зарегестрировать/создать инф. базу по шалону
        /// при активации сервиса биллинга
        /// </summary>
        AccountDatabse_RegisterUponServiceActivation,

        /// <summary>
        /// Установить результат отката
        /// версии файла разработки 1С
        /// </summary>
        Service1CFileVersion_SetRollbackResult,

        /// <summary>
        /// Уведомить о возврате версии файла
        /// разработки 1С на предыдущую(более раннюю)
        /// </summary>
        AboutRevertToEarlyService1CFileVersion_Notify,

        /// <summary>
        /// Выполнить откат версии файла разработки 1С
        /// </summary>
        Service1CFileVersion_Rollback,

        /// <summary>
        /// Отправить версию файла разработки 1C на откат
        /// к предыдущей версии файла
        /// </summary>
        Service1CFileVersion_SubmitForRollback,

        /// <summary>
        /// Отменить зависшие платежи
        /// </summary>
        Payment_CancelPending,

        /// <summary>
        /// Проверить изменение статуса платежа агрегатора ЮKassa
        /// </summary>
        YookassaPayment_CheckStatusChange,

        /// <summary>
        /// Проверить статус базы на разделителях
        /// </summary>
        DatabaseOnDelimiters_CheckStatus,

        /// <summary>
        /// Запустить процесс удаления инф. базы
        /// </summary>
        AccountDatabase_StartProcessOfRemove,

        /// <summary>
        /// Запустить процессы на архивацию
        /// информационных баз в склеп
        /// </summary>
        AccountDatabase_OfArchiveToTomb,

        /// <summary>
        /// Получить модель общих/основных данных
        /// карточки информационной базы
        /// </summary>
        AccountDatabase_GetCardGeneralData,

        /// <summary>
        /// Получить данные технической поддержки инф. базы
        /// </summary>
        AccountDatabase_GetAcDbSupportData,

        /// <summary>
        /// Получить данные бэкапов (базы)
        /// карточки инф. базы
        /// </summary>
        AccountDatabase_GetBackupsData,

        /// <summary>
        /// Получить данные доступов (к инф. базе)
        /// карточки инф. базы
        /// </summary>
        AccountDatabase_GetAcDbAccesses,

        /// <summary>
        /// Пересчитать стоимость услуги сервиса для всех аккаунтов
        /// после изменения
        /// </summary>
        ServiceType_RecalculateCost,

        /// <summary>
        /// Подсчитать стоимость создания инф. баз для аккаунта 
        /// </summary>
        Database_CalculateCostOfCreating,

        /// <summary>
        /// Удалить устаревшие токены
        /// пользователей
        /// </summary>
        AccountUserJsonWebToken_RemoveObsolete,

        /// <summary>
        /// Завершить сеансы в информационной базе
        /// </summary>
        AccountDatabase_TerminateSessions,

        /// <summary>
        /// Получить данные по завершению сеансов в информационной базе
        /// </summary>
        AccountDatabase_GetTerminationSessionsData,

        /// <summary>
        /// Получить общие/базовые данные для работы с пользователями аккаунта
        /// </summary>
        AccountUsers_GetCommonData,

        /// <summary>
        /// Редактирование локали
        /// </summary>
        Locales_Edit,

		/// <summary>
        /// Обновить информацию о сервисе биллинга в МС
        /// </summary>
        BillingService_UpdateInfoInMs,

        /// <summary>
        /// Экспорт данных в BigQuery
        /// </summary>
        ExportDataToBigQuery,

        /// <summary>
        /// Получить настройки аккаунта
        /// </summary>
        Accounts_GetSettings,

        #region ItsAuthorizationData

        /// <summary>
        /// Редактирование данных авторизации ИТС
        /// </summary>
        EditItsAuthorizationData,

        /// <summary>
        /// Добавление новых данных авторизации ИТС
        /// </summary>
        CreateItsAuthorizationData,

        /// <summary>
        /// Удаление данных авторизации ИТС
        /// </summary>
        DeleteItsAuthorizationData,

        /// <summary>
        /// Просмотр и получение данных авторизации ИТС
        /// </summary>
        GetItsDataAuthorizations,

        /// <summary>
        /// Поиск данных авторизации ИТС для выпадающего списка
        /// </summary>
        GetItsAuthorizationDataForSelectList,

        #endregion

        /// <summary>
        /// Провести аудит операций по смене сегмента у аккаунтов
        /// </summary>
        AuditChangeAccountSegmentOperations,

        /// <summary>
        /// Уведомить администратора аккаунта, что демо период сервиса завершается
        /// </summary>
        BillingManager_NotifyDemoPeriodIsComingToEnd,

        /// <summary>
        /// Уведомить клиентов о том, что демо период завершён, нужна оплата
        /// </summary>
        BillingManager_NotifyServiceDemoPeriodEndedPaymentRequired
    }
}
