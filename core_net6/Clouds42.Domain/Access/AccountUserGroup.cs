﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Clouds42.Domain.Attributes;

namespace Clouds42.Domain.Access
{
    /// <summary>
    /// Тип роли пользователя
    /// </summary>
    public enum AccountUserGroup
    {
        /// <summary>
        /// Не определен
        /// </summary>
        [NotDrawInHtml] [Description("")] Undefined = 0,

        /// <summary>
        /// Пользователь аккаунта
        /// </summary>
        [Description("Пользователь аккаунта")] [AccountBound]
        AccountUser = 1,

        /// <summary>
        /// Менеджер аккаунта
        /// </summary>
        [Description("Менеджер аккаунта")] [AccountBound]
        AccountAdmin = 2,

        /// <summary>
        /// Сейлc менеджер
        /// </summary>
        [Description("Сейлc менеджер")] [AccountBound]
        AccountSaleManager = 3,

        /// <summary>
        /// Оператор процес центра
        /// </summary>
        [Description("Оператор процес центра")]
        ProcOperator = 4,

        /// <summary>
        /// Хотлайн
        /// </summary>
        [Description("Хотлайн")]
        Hotline = 5,

        /// <summary>
        /// Администратор облака
        /// </summary>
        [NotDrawInHtml] [Description("Администратор облака")]
        CloudAdmin = 6,

        /// <summary>
        /// Служба
        /// </summary>
        [Description("Служба")]
        Cloud42Service = 7,

        /// <summary>
        /// Anonymous
        /// </summary>
        [Description(nameof(Anonymous))] [NotDrawInHtml]
        Anonymous = 8,

        /// <summary>
        /// Инженер облака
        /// </summary>
        [Description("Инженер облака")]
        CloudSE = 9,

        /// <summary>
        /// Внешняя служба
        /// </summary>
        [Description("Внешняя служба")]
        ExternalService = 10,

        [Description("Редактор статей")]
        ArticleEditor = 11
    }

    /// <summary>
    /// Аттрибут показывающий что группа существует только в контексте аккаунта
    /// </summary>
    public class AccountBoundAttribute : Attribute { }

    /// <summary>
    /// Расширения для работы с Enum-ом AccountUserGroup
    /// </summary>
    public static class AccountUserGroupExtensions
    {
        /// <summary>
        /// Проверить может ли группа существовать только в контексте аккаунта
        /// </summary>
        /// <param name="group">Группа</param>        
        /// <returns>Группа существует только в контексте аккаунта</returns>
        public static bool IsAccountBound(this AccountUserGroup group)
        {
            return HasAttribute<AccountBoundAttribute>(group);
        }

        /// <summary>
        /// Проверить является ли группа скрытой
        /// </summary>
        /// <param name="group">Группа</param>
        /// <returns>Группа является скрытой</returns>
        public static bool IsHidden(this AccountUserGroup group)
        {
            return HasAttribute<NotDrawInHtmlAttribute>(group);
        }

        private static bool HasAttribute<TAttribute>(Enum value, Func<TAttribute, bool> filter = null)
             where TAttribute : Attribute
        {
            var attributes = GetEnumAttributes<TAttribute>(value);
            if (filter == null)
                return attributes.Any();
            return attributes.Any(filter);
        }

        private static IList<TAttribute> GetEnumAttributes<TAttribute>(Enum value) where TAttribute : Attribute
        {
            Type enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);

            MemberInfo member = enumType.GetMember(enumValue)[0];
            var attrs = member.GetCustomAttributes(typeof(TAttribute), false)
                .Cast<TAttribute>().ToList();
            return attrs;
        }
    }
}
