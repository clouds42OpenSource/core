﻿namespace Clouds42.Domain.Access {
    /// <summary>
    /// 
    /// </summary>
    public enum AccessLevel 
    {
        /// <summary>
        /// 
        /// </summary>
        Denide = 0,
        /// <summary>
        /// 
        /// </summary>
        ControlledAccounts = 1,
        /// <summary>
        /// 
        /// </summary>
        HimSelfUser = 2,
        /// <summary>
        /// 
        /// </summary>
        HimSelfAccount = 3,                
        /// <summary>
        /// 
        /// </summary>
        Allow = 4       
    }
}