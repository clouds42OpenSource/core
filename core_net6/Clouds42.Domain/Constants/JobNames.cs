﻿using System;
using System.Linq;

namespace Clouds42.Domain.Constants
{
    public static class CoreWorkerTasksCatalog
    {
        public const string CorpPingTask = nameof(CorpPingTask);
        public const string RemoveFarmServersOnArrServerJob = nameof(RemoveFarmServersOnArrServerJob);
        public const string CheckAccountDatabaseOnDelimitersStatusJob = nameof(CheckAccountDatabaseOnDelimitersStatusJob);
        public const string AccountDatabaseAutoUpdateJob = nameof(AccountDatabaseAutoUpdateJob);
        public const string RepublishDatabaseJob = nameof(RepublishDatabaseJob);
        public const string UpdateAccountDatabasesJob = nameof(UpdateAccountDatabasesJob);
        public const string CreateFarmOnArrServerJob = nameof(CreateFarmOnArrServerJob);
        public const string DeleteUserInAdJob = nameof(DeleteUserInAdJob);
        public const string AuditCoreWorkerTasksQueueJob = nameof(AuditCoreWorkerTasksQueueJob);
        public const string StartProcessOfArchiveDatabaseToTombJob = nameof(StartProcessOfArchiveDatabaseToTombJob);
        public const string ManageAccountDatabaseRestoreModelJob = nameof(ManageAccountDatabaseRestoreModelJob);
        public const string CreateDatabaseFromTemplateJob = nameof(CreateDatabaseFromTemplateJob);
        public const string NotifyAboutService1CFileVersionAuditResultJob = nameof(NotifyAboutService1CFileVersionAuditResultJob);
        public const string Regular42CloudServiceProlongationJob = nameof(Regular42CloudServiceProlongationJob);
        public const string McobEditUserRolesOfDatabase = nameof(McobEditUserRolesOfDatabase);
        public const string ExportAccountsDataToBigQueryJob = nameof(ExportAccountsDataToBigQueryJob);
        public const string TemplatesUpdaterJob = nameof(TemplatesUpdaterJob);
        public const string RemoveOldPublicationJob = nameof(RemoveOldPublicationJob);
        public const string TehSupportAccountDatabasesJob = nameof(TehSupportAccountDatabasesJob);
        public const string DropSessionsFromClusterJob = nameof(DropSessionsFromClusterJob);
        public const string RestartAccountApplicationPoolOnIisJob = nameof(RestartAccountApplicationPoolOnIisJob);
        public const string AccountDatabaseMigrationJob = nameof(AccountDatabaseMigrationJob);
        public const string ArchivateAccountDatabaseToTombJob = nameof(ArchivateAccountDatabaseToTombJob);
        public const string RemoveDirectoryAclForUserInJob = nameof(RemoveDirectoryAclForUserInJob);
        public const string CreateDirectoryJob = nameof(CreateDirectoryJob);
        public const string CreateFarmServersOnArrServerJob = nameof(CreateFarmServersOnArrServerJob);
        public const string ExportPaymentsDataToBigQueryJob = nameof(ExportPaymentsDataToBigQueryJob);
        public const string SetDirectoryAclInAdJob = nameof(SetDirectoryAclInAdJob);
        public const string UpdatePartnerClientsGoogleSheetJob = nameof(UpdatePartnerClientsGoogleSheetJob);
        public const string EditUserGroupsInAdListInAdJob = nameof(EditUserGroupsInAdListInAdJob);
        public const string RemoveUploadFileJob = nameof(RemoveUploadFileJob);
        public const string RegisterNewUserInAdJob = nameof(RegisterNewUserInAdJob);
        public const string RemoveFarmsOnArrServerJob = nameof(RemoveFarmsOnArrServerJob);
        public const string DocLoaderByRent1CProlongationJob = nameof(DocLoaderByRent1CProlongationJob);
        public const string CfuUpdater = nameof(CfuUpdater);
        public const string EditUserInAdJob = nameof(EditUserInAdJob);
        public const string FinalizeYookassaPaymentJob = nameof(FinalizeYookassaPaymentJob);
        public const string FinalizeYookassaPaymentsJob = nameof(FinalizeYookassaPaymentsJob);
        public const string Check1CDatabaseFileForExistenceJob = nameof(Check1CDatabaseFileForExistenceJob);
        public const string SegmentMigrationJob = nameof(SegmentMigrationJob);
        public const string ExportDataToBigQueryJob = nameof(ExportDataToBigQueryJob);
        public const string SetDirectoryAclForUserInAdJob = nameof(SetDirectoryAclForUserInAdJob);
        public const string CreateInvoiceFiscalReceiptJob = nameof(CreateInvoiceFiscalReceiptJob);
        public const string RemoveDirectoryJob = nameof(RemoveDirectoryJob);
        public const string RegisterNewAccountInAdJob = nameof(RegisterNewAccountInAdJob);
        public const string MyDiskRegularOperations = nameof(MyDiskRegularOperations);
        public const string DeleteAccountDatabaseToTombJob = nameof(DeleteAccountDatabaseToTombJob);
        public const string CreateDatabaseClusterJob = nameof(CreateDatabaseClusterJob);
        public const string CreateDatabaseFromDtJob = nameof(CreateDatabaseFromDtJob);
        public const string RemoveFileJob = nameof(RemoveFileJob);
        public const string TransformFileBaseToServerJob = nameof(TransformFileBaseToServerJob);
        public const string NotifyBeforeChangeServiceCostJob = nameof(NotifyBeforeChangeServiceCostJob);
        public const string SupportAuthVerification = nameof(SupportAuthVerification);
        public const string ReportJob = nameof(ReportJob);
        public const string MergeChunksToInitialFileJob = nameof(MergeChunksToInitialFileJob);
        public const string RemoveObsoleteAccountUserJsonWebTokensJob = nameof(RemoveObsoleteAccountUserJsonWebTokensJob);
        public const string RepublishSegmentDatabasesJob = nameof(RepublishSegmentDatabasesJob);
        public const string AddWebAccessToDatabaseJob = nameof(AddWebAccessToDatabaseJob);
        public const string MyDiskUsedSizeRecalculationJob = nameof(MyDiskUsedSizeRecalculationJob);
        public const string ChangeUserPasswordInAdJob = nameof(ChangeUserPasswordInAdJob);
        public const string BackupFileFetchingJob = nameof(BackupFileFetchingJob);
        public const string NotifyAboutMainServiceResourcesChangesJob = nameof(NotifyAboutMainServiceResourcesChangesJob);
        public const string InfoBaseLastActiveUpdateJob = nameof(InfoBaseLastActiveUpdateJob);
        public const string SendBackupAccountDatabaseToTombJob = nameof(SendBackupAccountDatabaseToTombJob);
        public const string ManageAccountUserLockStateJob = nameof(ManageAccountUserLockStateJob);
        public const string RecalculateServiceTypeCostAfterChangeJob = nameof(RecalculateServiceTypeCostAfterChangeJob);
        public const string ManagePublishedAccountDatabaseRedirectJob = nameof(ManagePublishedAccountDatabaseRedirectJob);
        public const string RestoreAccountDatabaseFromTombJob = nameof(RestoreAccountDatabaseFromTombJob);
        public const string PublishDatabaseJob = nameof(PublishDatabaseJob);
        public const string ExportResourcesDataToBigQueryJob = nameof(ExportResourcesDataToBigQueryJob);
        public const string ArchivDatabaseJob = nameof(ArchivDatabaseJob);
        public const string ExportInvoicesDataToBigQueryJob = nameof(ExportInvoicesDataToBigQueryJob);
        public const string StartProcessOfRemoveDatabaseJob = nameof(StartProcessOfRemoveDatabaseJob);
        public const string ChangeUserLoginInAdJob = nameof(ChangeUserLoginInAdJob);
        public const string DatabaseRegularOperations = nameof(DatabaseRegularOperations);
        public const string RecalculateServiceCostAfterChangesJob = nameof(RecalculateServiceCostAfterChangesJob);
        public const string UploadAcDbZipFileThroughWebServiceJob = nameof(UploadAcDbZipFileThroughWebServiceJob);
        public const string PublishingPathsDatabasesAuditJob = nameof(PublishingPathsDatabasesAuditJob);
        public const string CancelPublishDatabaseJob = nameof(CancelPublishDatabaseJob);
        public const string SetFeedbackNotificationSendJob = nameof(SetFeedbackNotificationSendJob);
        public const string RemoveAccessesFromDatabaseJob = nameof(RemoveAccessesFromDatabaseJob);
        public const string ProcessDeletedServiceTypesForAccountsJob = nameof(ProcessDeletedServiceTypesForAccountsJob);
        public const string SetServiceExtensionDatabaseActivationStatusJob = nameof(SetServiceExtensionDatabaseActivationStatusJob);
        public const string AuditAccountResourceIntegralityJob = nameof(AuditAccountResourceIntegralityJob);
        public const string RestoreAccountDatabaseAfterFailedAutoUpdateJob = nameof(RestoreAccountDatabaseAfterFailedAutoUpdateJob);
        public const string DeleteInactiveAccountsDataJob = nameof(DeleteInactiveAccountsDataJob);
        public const string RemoveWebAccessToDatabaseJob = nameof(RemoveWebAccessToDatabaseJob);
        public const string DropDatabaseFromClusterJob = nameof(DropDatabaseFromClusterJob);
        public const string UpdateLoginForWebAccessDatabaseJob = nameof(UpdateLoginForWebAccessDatabaseJob);
        public const string ArchivateAccountFilesToTombJob = nameof(ArchivateAccountFilesToTombJob);
        public const string PlanSupportAccountDatabaseJob = nameof(PlanSupportAccountDatabaseJob);
        public const string AccountDatabasesAuditAndChangeStateJob = nameof(AccountDatabasesAuditAndChangeStateJob);
        public const string SubmitService1CFileVersionForAuditJob = nameof(SubmitService1CFileVersionForAuditJob);
        public const string RepublishWithChangingConfigJob = nameof(RepublishWithChangingConfigJob);
        public const string ExportDatabasesDataToBigQueryJob = nameof(ExportDatabasesDataToBigQueryJob);
        public const string DeleteAccountDatabaseJob = nameof(DeleteAccountDatabaseJob);
        public const string DeleteWindowsProfileJob = nameof(DeleteWindowsProfileJob);
        public const string ExportAccountUsersDataToBigQueryJob = nameof(ExportAccountUsersDataToBigQueryJob);
        public const string TerminateSessionsInDatabaseJob = nameof(TerminateSessionsInDatabaseJob);
        public const string AutoPayJob = "AutoPayBeforeLock";
        public const string DeleteOldBackups = "DeleteOldBackupsJob";
        public const string HandleRequestToChangeAccDbTypeJob = nameof(HandleRequestToChangeAccDbTypeJob);
        public const string AccountDatabaseUpdateJob = nameof(AccountDatabaseUpdateJob);
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CoreWorkerJobNameAttribute(string jobName) : Attribute
    {
        public string JobName { get; } = jobName;
    }

    public static class CoreWorkerJobNameExtensions
    {
        public static string GetCoreWorkerTaskName(this Type jobType)
        {
            return Attribute.GetCustomAttributes(jobType, typeof(CoreWorkerJobNameAttribute))?.FirstOrDefault() is not CoreWorkerJobNameAttribute attribute ? null : attribute.JobName;
        }
    }
}
