﻿namespace Clouds42.Domain.Constants
{
    /// <summary>
    /// Название номенклатур сервиса
    /// </summary>
    public static class NomenclaturesNameConstants
    {
        /// <summary>
        /// Аренда 1С - Стандарт
        /// </summary>
        public const string Renst1CStanart = "Аренда 1С - Стандарт";

        /// <summary>
        /// Аренда 1С - Web
        /// </summary>
        public const string Renst1CWeb = "Аренда 1С - Web";

        /// <summary>
        /// Мой Диск
        /// </summary>
        public const string MyDisk = "Мой Диск";

        /// <summary>
        /// Fasta
        /// </summary>
        public const string Esdl = "Fasta";

        /// <summary>
        /// Распознавание документов
        /// </summary>
        public const string Recognition = "Распознавание документов";

        /// <summary>
        /// Мои информационные базы
        /// </summary>
        public const string MyDatabases = "Мои информационные базы";
    }
}