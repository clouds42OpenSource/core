﻿using System;

namespace Clouds42.Domain.Attributes
{
    /// <summary>
    /// Аттрибут для метки поля, которое BigQuery должен проигнорировать
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class BigQueryIgnoreAttribute : Attribute
    {
    }
}
