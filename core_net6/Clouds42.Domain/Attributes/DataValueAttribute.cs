﻿using System;

namespace Clouds42.Domain.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class DataValueAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public int Value { set; get; }
    }
}
