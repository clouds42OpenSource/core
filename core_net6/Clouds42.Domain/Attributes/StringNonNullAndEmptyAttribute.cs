﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.Domain.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class StringNonNullAndEmptyAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Complete required fields";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
            var comment = (string)value;
            return !string.IsNullOrWhiteSpace(comment) ? ValidationResult.Success : new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
        }
    }
}
