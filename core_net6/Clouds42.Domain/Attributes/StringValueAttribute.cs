﻿using System;

namespace Clouds42.Domain.Attributes
{
    /// <summary>
    /// Атрибут используется для представления строкового значения для значения в перечислении.
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        /// <summary>
        /// Строковое значение
        /// </summary>
        public string StringValue { get; protected set; }

        /// <inheritdoc />
        public StringValueAttribute(string value)
        {
            this.StringValue = value;
        }
    }
}
