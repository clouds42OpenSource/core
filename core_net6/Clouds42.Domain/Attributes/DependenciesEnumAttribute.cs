﻿using System;

namespace Clouds42.Domain.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class DependenciesEnumAttribute : Attribute
    {

        /// <summary>
        /// Dependency Enum Value
        /// </summary>
        public readonly int Dependencies;

        /// <summary>
        /// Dependency Enum type
        /// </summary>
        public readonly Type EnumType;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dependenciesEnumAttribute"></param>
        /// <param name="enumType"></param>
        public DependenciesEnumAttribute(int dependenciesEnumAttribute, Type enumType = null)
        {
            Dependencies = dependenciesEnumAttribute;
            EnumType = enumType;
        }
    }
}
