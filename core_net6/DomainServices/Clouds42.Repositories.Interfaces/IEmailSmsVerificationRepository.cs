﻿using Clouds42.Domain.DataModels.Security;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IEmailSmsVerificationRepository : IGenericRepository<EmailSmsVerification>
    {
    }
}
