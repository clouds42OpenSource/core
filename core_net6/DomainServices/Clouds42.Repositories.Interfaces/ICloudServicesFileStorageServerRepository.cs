﻿using System;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICloudServicesFileStorageServerRepository : IGenericRepository<CloudServicesFileStorageServer>
    {

        /// <summary>
        /// Получить данные по файловому хранилищу пользователя.
        /// </summary>
        /// <param name="accountUserId">Номер пользователя.</param>        
        Task<CloudServicesFileStorageServer> GetFileStorageData(Guid accountUserId);
    }
}