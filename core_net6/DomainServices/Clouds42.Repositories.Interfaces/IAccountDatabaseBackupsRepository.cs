﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    ///     Репозиторий бекапов баз
    /// </summary>
    public interface IAccountDatabaseBackupsRepository : IGenericRepository<AccountDatabaseBackup>
    {
        /// <summary>
        ///     Получить количество бекапов для определенной базы
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        int GetCountOfDatabaseBackups(Guid accountDatabaseId);

        /// <summary>
        ///     Получить список баз отсортированных по дате создания
        /// (Первые в списке это новые бэкапы)
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        IOrderedQueryable<AccountDatabaseBackup> GetBackupsSortedByDate(Guid accountDatabaseId);

        /// <summary>
        /// Получить путь до бекапа информационной базы
        /// </summary>
        /// <param name="backupId">Id бекапа информационной базы</param>
        /// <returns>Путь до бекапа информационной базы</returns>
        string GetAccountDatabaseBackupPath(Guid backupId);
    }
}
