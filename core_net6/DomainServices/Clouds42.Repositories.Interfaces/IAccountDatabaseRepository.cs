﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountDatabaseRepository : IGenericRepository<AccountDatabase> {

        AccountDatabase GetAccountDatabase(Guid accountDatabaseId);

        void InsertOrUpdateAccountDatabaseValue(AccountDatabase accountDatabaseValue);

        /// <summary>
        ///     Получить список опубликованных баз в сегменте
        /// по платформе и дистрибутиву
        /// </summary>
        /// <remarks>
        /// Список опубликованных баз данных не включает т.н. базы на разделителях
        /// поскольку такие базы считаются всегда опубликованными
        /// </remarks>
        List<AccountDatabase> GetPublishedDbs(Guid segmentId,
            PlatformType platformType,
            DistributionType distributionType);

        bool AnyPublishedDbs(Guid segmentId,
            PlatformType platformType,
            DistributionType distributionType);

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="fileStorage">Файловое хранилище куда создается база.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="account">Аккаунт в который создается база.</param>
        /// <param name="isFile">Файловая база.</param>
        AccountDatabase CreateNewRecord(
            DbTemplate template,
            Account account,
            PlatformType platformType,
            string caption,
            CloudServicesFileStorageServer fileStorage,
            bool isFile);

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="account">Аккаунт в который создается база.</param>
        AccountDatabase CreateNewRecord(
            DbTemplate template,
            Account account,
            PlatformType platformType,
            string caption);

        /// <summary>
        /// Получить спискок свойств доступных баз данных для пользователя аккаунта.
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя аккаунта</param>
        Task<List<AccessDatabaseListQueryDataDto>> GetAccountDatabasesForAccountUserAsync(Guid accountUserId, Guid serviceAccountId);

        /// <summary>
        ///     Получить список всех баз на разделителях для пользователя
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        Task<List<DelimitersDatabaseQueryDataDto>> GetDelimitersDbsForAccUserAsync(Guid accountUserId);

        /// <summary>
        ///     Получить список баз на разделителях для пользователя
        /// с указанием кода конфигурации конкретного разделителя
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>
        Task<List<DelimitersDatabaseQueryDataDto>> GetDelimitersDbsForAccUserAsync(Guid accountUserId, string configurationCode);

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesById(Guid accountDatabaseId);

        /// <summary>
        /// Получить данные информационной базы по имени базы.
        /// </summary>
        /// <param name="v82Name">Имя информационной базы.</param>
        Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesByNameAsync(string v82Name);

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>
        Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesForUserById(Guid accountDatabaseId, Guid accountUserId);

    }
}
