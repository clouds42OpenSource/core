﻿using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для агентских заявок на вывод средств
    /// </summary>
    public interface IAgentCashOutRequestRepository : IGenericRepository<AgentCashOutRequest>
    {
        /// <summary>
        /// Вставить новую запись заявки на вывод средств
        /// </summary>
        /// <param name="cashOutRequest">Заявка на вывод средств</param>
        void InsertAgentCashOutRequest(AgentCashOutRequest cashOutRequest);
    }
}
