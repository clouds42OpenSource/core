﻿using Clouds42.Repositories.Interfaces.Common;
using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Repositories.Interfaces
{
    public interface IBillingServiceRepository : IGenericRepository<BillingService>
    {
        /// <summary>
        /// Получить список Id аккаунтов
        /// которые используют сервис
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список Id аккаунтов</returns>
        List<Guid> GetAccountIdsThatUseService(Guid serviceId);
    }
}
