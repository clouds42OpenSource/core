﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountUserRepository : IGenericRepository<AccountUser>
    {
        IQueryable<AccountUser> GetNotDeletedAccountUsersQuery();

        /// <summary>
        /// Query валидных (не удаленных) пользователей
        /// </summary>
        /// <returns>Объект запроса пользователей</returns>
        IQueryable<AccountUser> GetValidUsersQuery();

        List<AccountUser> GetAccountUsers(Guid accountId, bool includeDeleted = false);
        Task<List<AccountUser>> GetAccountUsersAsync(Guid accountId);
        AccountUser GetManager(Guid managerId);

        /// <summary>
        /// Возвращает объект пользователя по его логину (Id)
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        AccountUser GetByLogin(string login);

        /// <summary>
        /// Проверяет доступность логина
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        AccountUser IsUserExist(string login);

        /// <summary>
        /// Возвращает пользователя по его email'у
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        AccountUser GetByEmail(string email);

        /// <summary>
        /// Возвращает первого пользователя по номеру телефона
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        AccountUser GetByPhoneNumber(string phone);

        /// <summary>
        /// Возвращает пользователя по токену
        /// </summary>
        /// <param name="resetToken">Зашифрованное поле ResetCode</param>
        /// <returns></returns>
        AccountUser GetAccountUserByResetToken(string resetToken);

        /// <summary>
        /// Устанавливает пароль пользователя в базе данных
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="notEncryptNewPassword">Пароль в чистом виде</param>
        void SetUserPassword(AccountUser user, string notEncryptNewPassword, IConfiguration configuration);

        /// <summary>
        /// Удаляет пользователя из базы
        /// </summary>
        void DeleteUser(AccountUser user);

        /// <summary>
        /// Отписывает пользователя от рассылок
        /// </summary>
        Task<AccountUser> Unsubscribe(Guid accountUserId);

        AccountUser GetById(Guid id);

        AccountUser GetAccountUser(Guid accountUserId);
        AccountUser GetAccountUserByCorpId(Guid corpUserId);
        AccountUser GetAccountUserByEmail(string email);
        public AccountUser GetAccountUserByVerifiedEmail(string email);
        public AccountUser GetAccountUserByNoVerifiedEmail(string email);
        AccountUser GetAccountUserByLogin(string login);
        AccountUser GetAccountUserByeVerifiedPhoneNumber(string phoneNomber);
        AccountUser GetAccountUserByeNoVerifiedPhoneNumber(string phoneNomber);
        List<AccountUser> GetAllAccountUsers(Guid accountId);
        void InsertOrUpdateAccountUser(AccountUser accountUser);
        void InsertAccountUser(AccountUser accountUser);
        Task<AccountUserImpersonateQueryDataModelDto> GetAccountUserImpersonateDataByAccountUserIdAsync(Guid accountUserId);
    }
}
