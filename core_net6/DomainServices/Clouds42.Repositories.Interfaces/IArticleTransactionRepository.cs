﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IArticleTransactionRepository : IGenericRepository<ArticleTransaction>
    {
    }
}
