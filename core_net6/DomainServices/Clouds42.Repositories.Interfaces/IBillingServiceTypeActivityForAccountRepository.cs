﻿using Clouds42.Repositories.Interfaces.Common;
using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для работы с моделями активности услуги сервиса для аккаунта
    /// </summary>
    public interface IBillingServiceTypeActivityForAccountRepository : IGenericRepository<BillingServiceTypeActivityForAccount>
    {
        /// <summary>
        /// Получить список ID удаленных, но еще доступных услуг для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список ID удаленных, но еще доступных услуг для аккаунта</returns>
        List<Guid> GetDeletedButStillAvailableServiceTypesIdForAccount(Guid billingServiceId, Guid accountId);
    }
}
