﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.WebSockets;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IWebSocketConnectionRepository : IGenericRepository<WebSocketConnection>
    {
        Task<List<string>> GetAllOpenConnections();
        Task<List<string>> GetOpenUserConnectionsWhichAvailableDatabase(Guid accountUserId);
        Task<List<string>> GetOpenConnectionsWhichAvailableDatabase(Guid accountDatabaseId);
    }
}