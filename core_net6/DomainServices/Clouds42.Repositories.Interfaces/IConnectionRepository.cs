﻿using Clouds42.Domain.DataModels.Notification;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IConnectionRepository : IGenericRepository<Connection>
    {
    }
}
