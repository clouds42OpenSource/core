﻿using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IResourcesRepository : IGenericRepository<Resource>
    {
        /// <summary>
        /// Получить ресурсы Аренды 1С (Вэб или RDP)
        /// </summary>
        /// <returns>Ресурсы Аренды 1С</returns>
        IQueryable<Resource> GetRent1CResources();
    }
}
