﻿using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Repositories.Interfaces.Common;
using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountConfigurationRepository : IGenericRepository<AccountConfiguration>
    {
        AccountConfiguration GetAccountConfiguration(Guid accountId);
        AccountConfigurationDto GetAccountConfigurationDc(Guid accountId);
        AccountWithConfigurationDto GetAccountWithConfiguration(Guid accountId);
        IQueryable<AccountWithConfigurationDto> GetAccountsWithConfiguration();
        AccountConfigurationByLocaleDataDto GetAccountConfigurationByLocaleData(Guid accountId);
        IQueryable<AccountConfigurationByLocaleDataDto> GetAccountConfigurationsDataByLocale();
        IQueryable<AccountConfigurationBySegmentDataDto> GetAccountConfigurationsDataBySegment();
        AccountConfigurationBySegmentDataDto GetAccountConfigurationBySegmentData(Guid accountId);
        Task<CloudServicesBackupStorage> GetBackupStorageByAccountSegment(Guid accountId);
        IQueryable<AccountConfigurationBySupplierDataDto> GetAccountConfigurationsDataBySupplier();
        ILocale GetAccountUserLocale(Guid accountUserId);

    }
}
