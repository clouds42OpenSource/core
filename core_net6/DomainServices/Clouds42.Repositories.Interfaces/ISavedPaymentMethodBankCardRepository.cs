﻿using System;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ISavedPaymentMethodBankCardRepository : IGenericRepository<SavedPaymentMethodBankCard>
    {
        SavedPaymentMethodBankCard GetByPaymentMethodId(Guid paymentMethodId);
        Task<SavedPaymentMethodBankCard> GetByPaymentMethodIdAsync(Guid paymentMethodId);
    }
}
