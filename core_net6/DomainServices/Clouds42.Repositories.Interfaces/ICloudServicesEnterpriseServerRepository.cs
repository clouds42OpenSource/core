﻿using Clouds42.Repositories.Interfaces.Common;
using System;
using Clouds42.Domain.DataModels;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICloudServicesEnterpriseServerRepository: IGenericRepository<CloudServicesEnterpriseServer>
    {
        CloudServicesEnterpriseServer GetEnterpriseServer(Guid cloudServicesEnterpriseServerId);
    }
}