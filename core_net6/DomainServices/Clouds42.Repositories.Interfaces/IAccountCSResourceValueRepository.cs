﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountCsResourceValueRepository : IGenericRepository<AccountCSResourceValue>
    {
        IEnumerable<AccountCSResourceValue> GetAccountCsResourceValues(Guid accountId, Guid csResourceId);
        int GetAccountCsResourceValuesCount(Guid accountId, Guid csResourceId);
        AccountCSResourceValue GetAccountCsResourceValue(Guid accountCsResourceValueId);
        int GetAccountCsResourceValuesModifyCount(Guid accountId, Guid csResourceId, DateTime dateTime);
        Task<int> GetAccountCsResourceValuesModifyCountAsync(Guid accountId, Guid csResourceId, DateTime dateTime);
    }
}
