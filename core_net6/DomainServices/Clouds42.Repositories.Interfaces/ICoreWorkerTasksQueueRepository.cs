﻿using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для работы с очередью задач
    /// </summary>
    public interface ICoreWorkerTasksQueueRepository : IGenericRepository<CoreWorkerTasksQueue>
    {
    }
}