﻿using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICloudServicesSegmentStorageRepository : IGenericRepository<CloudServicesSegmentStorage>
    {
        CloudServicesSegmentStorage GetByAccountDatabase(AccountDatabase database);
    }
}