﻿using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для реквизитов агента
    /// </summary>
    public interface IAgentRequisitesRepository : IGenericRepository<AgentRequisites>
    {
        /// <summary>
        /// Вставить новую запись реквизитов агента
        /// </summary>
        /// <param name="agentRequisites">Реквизиты агента</param>
        void InsertAgentRequisites(AgentRequisites agentRequisites);
    }
}
