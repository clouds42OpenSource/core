﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICsResourceRepository : IGenericRepository<CsResource>
    {
        void UpdateCsResource(CsResource csResource);
	    void InsertCsResource(CsResource csResource);
		CsResource GetCsResourceByCloudServiceIdAndResourceName(string cloudServiceId, string resourceName);
        CsResource GetCsResourceByResourceName(string resourcesName);
        CsResource GetCsResourceById(Guid csResourceId);
        CsResource GetCsResourceFromServiceByName(string cloudServiceId, string csResourceName);
        List<CsResource> GetCsResources(string cloudServiceId);
        void DeleteCsResource(CsResource csResource);
    }
}
