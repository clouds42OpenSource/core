﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий по работе с сохраненными платежными способами пользователя
    /// </summary>
    public interface ISavedPaymentMethodRepository : IGenericRepository<SavedPaymentMethod>
    {
        SavedPaymentMethod GetById(Guid id);
        Task<SavedPaymentMethod> GetByIdAsync(Guid id);
        Task<List<SavedPaymentMethod>> GetAllAsync();
        Task<List<SavedPaymentMethod>> GetAllByAccountIdAsync(Guid accountId);
        SavedPaymentMethod GetByAccountId(Guid accountId);
        SavedPaymentMethod GetByToken(string token);
    }
}
