﻿using System;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountAutoPayConfigurationRepository : IGenericRepository<AccountAutoPayConfiguration>
    {
        Task<AccountAutoPayConfiguration> GetByPaymentMethodIdAsync(Guid paymentMethodId);
        Task<AccountAutoPayConfiguration> GetByAccountIdAsync(Guid accountId);
        AccountAutoPayConfiguration GetByAccountId(Guid accountId);
    }
}
