﻿using System;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Расширение для репозитория кошельков агентов
    /// </summary>
    public interface IAgentWalletRepository : IGenericRepository<AgentWallet>
    {
        /// <summary>
        /// Получить кошелек агента или создать новый
        /// </summary>
        /// <param name="accountAgentId">Id агента</param>
        /// <returns>Кошелек агента</returns>
        AgentWallet GetAgentWalletOrCreateNew(Guid accountAgentId);
    }
}
