﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICloudServiceRepository : IGenericRepository<CloudService>
    {
        void InsertCloudService(CloudService cloudService);
	    void UpdateCloudService(CloudService cloudService);
		IQueryable<string> GetCloudServices();
        int GetCloudServicesCount();
        CloudService GetCloudService(string cloudServiceId);
        CloudService GetCloudServiceByToken(Guid cloudServiceToken);
        void DeleteCloudService(CloudService cloudService);
    }
}