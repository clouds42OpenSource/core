﻿using Clouds42.Repositories.Interfaces.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Repositories.Interfaces
{
    public interface IResourcesConfigurationRepository : IGenericRepository<ResourcesConfiguration>
    {

        /// <summary>
        /// Получить дату окончания срока действия для сервиса, привязанного к указанному аккаунту.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="service">Cервис</param>
        /// <param name="promisePaymentDays">Количество дней активности обещанного платежа.</param>
        /// <returns>Если дата устаревания сервиса для аккаунта найдена и она не заморожена (Frozen), то возвращается она,
        /// если же она существует и заморожена, то возвращается параметр dateTime, если он установлен, или текущая дата/время
        /// если не установлен. В других случаях возвращается NULL.</returns>
        Task<DateTime?> GetServiceExpirationDate(Guid accountId, Clouds42Service service, int promisePaymentDays);

        /// <summary>
        /// Получить список активных не дочерних сервисов, которые можно продлить или нужно заблокировать.       
        /// </summary>
        List<ResourcesConfiguration> GetActiveMainServicesForProlongOrLock();

        /// <summary>
        /// Получить список сервисов, срок действия которых скоро истечет.
        /// </summary>        
        /// <param name="beforeLockDays">Кол-во дней до блокировки.</param>
        List<ResourcesConfiguration> GetSoonExpiredMainServices(int beforeLockDays);

        /// <summary>
        /// Получить список для разблокирования платных сервисов, которые заблокированны, на которые есть деньги что бы разблокировать и у аккаунтов которых нет просроченного обещанного платежа.
        /// </summary>         
        List<Guid> GetPaidLockedServicesWithAvailableBalanceOnAccount(int promisePaymentDays);

        /// <summary>
        /// Получить список заблокированных сервисов у которых 
        /// цена = 0 и обязательно есть спонсируемый пользователь,
        /// у аккаунта который спонсирует Аренда активна
        /// </summary>
        List<ResourcesConfiguration> GetLockedSponsoredServiceConfigurations(int promisePaymentDays);

        /// <summary>
        /// Признак, что основной сервис аккаунта имеет активные спонсируемые лицензии
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса</param>
        /// <returns>true - если есть спонсируемые спонсируемые лицензии и у спонсора сервис активен</returns>
        bool IsMainServiceHasActiveSponsoredLicenses(ResourcesConfiguration configuration);

        /// <summary>
        /// Получить список всех платных не заблокированных сервисов 
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        List<ResourcesConfiguration> GetAllPaidActiveMainServices(Guid accountId);

        /// <summary>
        /// Получить список всех сервисов с просроченным демо периодом
        /// </summary>
        List<ResourcesConfiguration> GetAllExpiredDemoServices();

        /// <summary>
        /// Получить список всех гибридных сервисов с просроченным периодом
        /// </summary>
        List<ResourcesConfiguration> GetAllExpiredHybridServices();

        /// <summary>
        /// Получить список всех конфигураций, у которых осталось количество дней до завершения демо = <paramref name="numberOfRemainingDays"/>
        /// </summary>
        /// <returns>Список найденных конфигураций у которых осталось количество дней до завершения демо = <paramref name="numberOfRemainingDays"/></returns>
        List<ResourcesConfiguration> GetAllDemoResourcesConfigurationsWithNumberOfRemainingDays(int numberOfRemainingDays);

        /// <summary>
        /// Получить список всех конфигураций, у которых истёк демо период, заблокирован и есть автоподписка
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <returns>Список всех конфигураций, у которых истёк демо период, заблокирован и есть автоподписка</returns>
        List<ResourcesConfiguration> GetAllFrozenDemoResourcesConfigurationsWithAutoSubscription(Guid accountId);
    }
}
