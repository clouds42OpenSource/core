﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountUserRoleRepository: IGenericRepository<AccountUserRole>
    {
        /// <summary>
        /// Получить роли пользователя по ID пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Лист ролей пользователя</returns>
        ICollection<AccountUserRole> GetUserRoles(Guid accountUserId);
    }
}