﻿using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces;

public interface IWaitsInfoRepository : IGenericRepository<WaitInfo>
{
}

