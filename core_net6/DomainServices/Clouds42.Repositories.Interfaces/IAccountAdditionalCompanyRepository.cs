﻿using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountAdditionalCompanyRepository: IGenericRepository<AccountAdditionalCompany>
    {
    }
}
