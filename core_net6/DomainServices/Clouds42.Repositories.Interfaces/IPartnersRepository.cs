﻿using Clouds42.Repositories.Interfaces.Common;
using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels;

namespace Clouds42.Repositories.Interfaces
{
    public interface IPartnersRepository : IGenericRepository<Partner>
    {     
        IEnumerable<Partner> GetReferrerList(Guid referrerId);
        IEnumerable<Partner> GetAllReferrerHierarchy();
        bool IsPartner(Guid companyId);
    }
}