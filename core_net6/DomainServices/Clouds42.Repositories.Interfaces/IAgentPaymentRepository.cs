﻿using System;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для платежей агента
    /// </summary>
    public interface IAgentPaymentRepository : IGenericRepository<AgentPayment>
    {
        /// <summary>
        /// Вставить запись платежа агента и обновить сумму его кошелька
        /// </summary>
        /// <param name="accountId">ID аккаунта агента</param>
        /// <param name="clientPaymentId">ID платежа клиента</param>
        /// <param name="amount">Сумма платежа</param>
        /// <param name="paymentType">Тип платежа - начисления/снятие.</param>
        /// <param name="date">Дата платежа</param>
        /// <param name="comment">Комментарий к платежу.</param>
        /// <param name="agentPaymentSourceType">Тип источника агентского платежа</param>
        /// <param name="clientAccountId">Id аккаунта клиента</param>
        /// <param name="clientPaymentSum">Сумма платежа клиента</param>
        void InsertAgentPaymentAndUpdateAgentWallet(Guid accountId, decimal amount, Guid? clientPaymentId = null,
            PaymentType paymentType = PaymentType.Inflow, DateTime? date = null, string comment = null,
            AgentPaymentSourceTypeEnum agentPaymentSourceType = AgentPaymentSourceTypeEnum.AgentReward,
            Guid? clientAccountId = null, decimal? clientPaymentSum = null);
    }
}
