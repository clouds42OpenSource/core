﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для работы с загружаемыми файлами
    /// </summary>
    public interface IUploadedFileRepository : IGenericRepository<UploadedFile>
    {
        /// <summary>
        /// Получить статус загружаемого файла
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загружаемого файла</param>
        /// <returns>Статус загружаемого файла</returns>
        UploadedFileStatus GetUploadedFileStatus(Guid uploadedFileId);
    }
}