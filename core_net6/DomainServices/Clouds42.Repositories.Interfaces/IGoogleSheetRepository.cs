﻿using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces;

public interface IGoogleSheetRepository :  IGenericRepository<GoogleSheet>
{
    
}
