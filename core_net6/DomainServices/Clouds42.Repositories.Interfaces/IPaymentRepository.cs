﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IPaymentRepository : IGenericRepository<Payment>
    {
        Payment GetById(Guid id);
        List<Payment> GetPendingPaymentsByPaymentSystemAndLessSomeDate(string paymentSystem, DateTime date);
        Payment GetByPaymentSystemTransactionId(string transactionId);
        Task<Payment> GetByPaymentSystemTransactionIdAsync(string transactionId);
        int PreparePayment(Payment payment);
        IEnumerable<Payment> GetPayments(Guid accountId);
        IQueryable<Payment> GetPayments();
        Payment GetPaymentByExchangeId(Guid exchangeIdentifier);
        Payment FindPayment(Guid paymentId);
        Payment FindPayment(int number);
        void RemovePaymentByNumber(int number);
    }
}
