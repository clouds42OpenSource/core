﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для поддержки инф. базы
    /// </summary>
    public interface IAcDbSupportRepository : IGenericRepository<AcDbSupport>
    {
        /// <summary>
        /// Установить конфигурацию инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <param name="configuration">Конфигурация 1С</param>
        void SetConfiguration(AcDbSupport acDbSupport, IConfigurations1C configuration);
    }
}