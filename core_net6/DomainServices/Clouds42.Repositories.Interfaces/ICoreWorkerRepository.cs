﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICoreWorkerRepository : IGenericRepository<CoreWorker>
    {
        bool CoreWorkerCanProcessTask(Type jobType, short? coreWorkerId);
    }
}