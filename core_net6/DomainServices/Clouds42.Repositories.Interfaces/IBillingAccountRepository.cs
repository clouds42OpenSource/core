﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IBillingAccountRepository : IGenericRepository<BillingAccount>
    {
        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        decimal GetBalance(string login, TransactionType transactionType = TransactionType.Money);

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        decimal GetBalance(Guid accountId, TransactionType transactionType = TransactionType.Money);

        /// <summary>
        /// Получить аккаунт биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт биллинга</returns>
        BillingAccount GetBillingAccount(Guid accountId);

        /// <summary>
        /// Получить аккаунт биллинга или вызывать NotFoundException
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <exception cref="NotFoundException"></exception>
        /// <returns>Аккаунт биллинга</returns>
        Task<BillingAccount> GetBillingAccountOrThrowAsync(Guid accountId);

        /// <summary>
        /// Получить список аккаунтов у которых просрочен обещанный платеж.
        /// </summary>
        /// <param name="promisePaymentDays">Кол-во дней активности ОП.</param>        
        List<BillingAccount> GetAccountsWithExpiredPromise(int promisePaymentDays);
    }
}
