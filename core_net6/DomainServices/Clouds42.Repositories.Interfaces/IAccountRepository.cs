﻿using Clouds42.Repositories.Interfaces.Common;
using Clouds42.DataContracts.SQLNativeModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий по работе с сущностью аккаунта
    /// </summary>
    public interface IAccountRepository : IGenericRepository<Account>
    {
        Account GetAccountAndAccountUsers(Guid id);
        
        Account GetAccountByUserId(Guid userId);

        Account GetAccount(Guid accountId);


        /// <summary>
        /// Найти аккаунт по ID
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт</returns>
        Task<Account> GetAccountAsync(Guid accountId);

        void InsertAccount(Account newAccount);

        /// <summary>
        /// Получить количество сервисов у аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Количество сервисов у аккаунта</returns>
        int GetCountBillingServicesForAccount(Guid accountId);

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Локаль аккаунта</returns>
        Locale GetLocale(Guid accountId, Guid localeId);

        /// <summary>
        /// Получить локаль аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Локаль аккаунта</returns>
        Task<Locale> GetLocaleAsync(Guid accountId, Guid localeId);

        /// <summary>
        /// Получить локаль аккаунта, или вызвать NotFoundException
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <exception cref="NotFoundException"></exception>
        /// <returns></returns>
        Task<Locale> GetLocaleOrThrowAsync(Guid accountId, Guid localeId);

        List<Guid> GetAccountAdminIds(Guid accountId);

        /// <summary>
        /// Получает список AccountUser с правами админа и активной арендой
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        List<AccountUser> GetAccountAdminsWithActivatedRent1C(Guid accountId);

        /// <summary>
        ///     Получить доступные почты для рассылки у аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        List<string> GetEmails(Guid accountId);

        /// <summary>
        /// Получить данные аккаунта.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        Task<AccountInfoQueryDataDto> GetAccountInfoByIdAsync(Guid accountId);

        /// <summary>
        /// Необходимость аккаунту принять
        /// агентское соглашение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость принять агентское соглашение</returns>
        bool NeedApplyAgencyAgreement(Guid accountId);

        /// <summary>
        /// Получить Id всех аккаунтов
        /// </summary>
        /// <returns>Id аккаунтов</returns>
        List<Guid> GetAllAccountIds();

        /// <summary>
        /// Получить аккаунт по номеру аккаунта
        /// </summary>
        /// <param name="indexNumber">Номер аккаунта</param>
        /// <returns>Аккаунт</returns>
        Account GetAccountByIndexNumber(int indexNumber);

        /// <summary>
        /// Получить локаль по умолчанию
        /// </summary>
        /// <returns>Локаль по умолчанию</returns>
        Locale GetDefaultLocale(Guid defaultLocaleId);
    }
}
