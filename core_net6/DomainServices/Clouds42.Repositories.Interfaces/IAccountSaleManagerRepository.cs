﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для работы с Менеджерами Аккаунтов
    /// </summary>
    public interface IAccountSaleManagerRepository : IGenericRepository<AccountSaleManager>
    {
        /// <summary>
        /// Получить Менеджера Аккаунта по ID аккаунта
        /// </summary>
        /// <param name="accountId">ИД аккаунта</param>
        /// <returns>Менеджер аккаунта</returns>
        AccountSaleManager GetForAccount(Guid accountId);
    }
}