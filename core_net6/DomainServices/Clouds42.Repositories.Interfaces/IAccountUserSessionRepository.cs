﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;

namespace Clouds42.Repositories.Interfaces
{
    public interface IAccountUserSessionRepository : IGenericRepository<AccountUserSession>
    {

        AccountUserSession GetRecentSessionByUserId(Guid accounUserId);

        AccountUserSession GetAccountUserSessionByToken(Guid token);

        AccountUserSession GetAccountUserSession(Guid sessionId);

        IEnumerable<AccountUserSession> GetAccountUserSessionByUserId(Guid userId);

        void InsertOrUpdateAccountUserSession(AccountUserSession accountUserSession);

        bool DeleteAccounUserSession(Guid sessionId);

        Task<AccountUser> GetAccountUserForTokenAsync(Guid token);

        AccountUserSessionQueryDataDto GetValidAccountUserSessionDataForToken(Guid token, int expirationDays,
            AccountUserGroup cloud42ServiceId = AccountUserGroup.Cloud42Service);

        void RefreshTokenCreationTime(Guid sessionId);

        /// <summary>
        /// Получить данные сессии по токену.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <param name="expDays">Количество дней жизни токена.</param>
        /// <returns>AccountUserSessionInfoQueryData</returns>
        /// <seealso cref="AccountUserSessionInfoQueryDataDto"/>
        Task<AccountUserSessionInfoQueryDataDto> GetAccountUserSessionInfoAsync(Guid token, int expDays);

        /// <summary>
        /// Получить идентификатор аккаунта по токену сессии.
        /// </summary>
        /// <param name="token">Токен сессии.</param>
        Task<Guid> GetAccountIdBySessionTokenAsync(Guid token);

        /// <summary>
        /// Выбирает последний активный токен сессии для пользователя.
        /// </summary>
        /// <param name="accountUserId">Id польтзователя, для которого вытащить последний токен сессии</param>
        /// <param name="expirationDays">Дни перед тем, как сессия будет считаться устаревшей</param>
        /// <returns>Последний активный токен сессии.</returns>
        [Obsolete("Удалить после внедрения нового механизма аутентификации.")]
        Guid GetLastValidTokenForUser(Guid accountUserId, int expirationDays);
    }
}