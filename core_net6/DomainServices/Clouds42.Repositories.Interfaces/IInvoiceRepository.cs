﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface IInvoiceRepository : IGenericRepository<Invoice>
    {
        Task<List<Invoice>> GetAllProcessing();
        IEnumerable<Invoice> GetAccountInvoices(Guid accountId);
        Invoice GetInvoiceById(Guid id);
        void AddInvoice(Invoice invoice);
        void DeleteInvoice(Guid invoiceId);
        void DeleteInvoice(Invoice invoice);

        /// <summary>
        /// Получить счет на оплату или создать по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="uniq">Номер счета.</param>
        /// <param name="invoiceNmbPrefix">Префикс номера инвойса.</param>
        Task<Invoice> GetProcessingOrCreateNewInvoiceAsync(decimal sum, long uniq, string invoiceNmbPrefix);

        /// <summary>
        /// Получить необработанный инвойс на заданную сумму.
        /// </summary>
        /// <param name="sum">Сумма счёта.</param>
        /// <param name="inn">ИНН аккаунта.</param>
        Task<Invoice> GetProcessingInvoiceAsync(string inn, decimal sum);

        /// <summary>
        /// Установить номер акт счету
        /// </summary>
        /// <param name="invoiceId">Номер счета.</param>
        /// <param name="actId">Номер присваемого акта.</param>
        /// <param name="description">Коментарий к прикрепленному акту.</param>
        /// <param name="requiresSignature">Флаг нужна подпись или нет</param>
        Task SetActId(Guid invoiceId, Guid? actId, string description, bool requiresSignature);

        /// <summary>
        /// Получить или создать счёт по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="forceCreate">Флаг принудительного создания счёта</param>
        /// <param name="inn">ИНН аккаунта для которого будет сформирован или получен счёт</param>
        /// <param name="uniq">Номер счета.</param>
        Task<Invoice> GetOrCreateInvoiceAsync(bool forceCreate, string inn, decimal sum, long uniq);

    }
}
