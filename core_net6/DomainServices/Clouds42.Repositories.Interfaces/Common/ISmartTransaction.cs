﻿using System;
using System.Data;

namespace Clouds42.Repositories.Interfaces.Common
{
    public interface ISmartTransaction : IDisposable
    {
        ISmartTransaction Get(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        void Commit();
        void Rollback();

    }
}
