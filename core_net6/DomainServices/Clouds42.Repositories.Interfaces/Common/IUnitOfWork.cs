﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.DataModels.Supplier;

namespace Clouds42.Repositories.Interfaces.Common
{
    public interface IUnitOfWork : IDisposable
    {
        ISmartTransaction SmartTransaction { get; }

        #region  Repositories

        IAgentRequisitesRepository AgentRequisitesRepository { get; }
        IAccountUserBitDepthRepository AccountUserBitDepthRepository { get; }

        IAgentWalletRepository AgentWalletRepository { get; }
        IFeedbackRepository FeedbackRepository { get; }
        IAgentPaymentRepository AgentPaymentRepository { get; }
        IAgentCashOutRequestRepository AgentCashOutRequestRepository { get; }
        IAccountConfigurationRepository AccountConfigurationRepository { get; }
        IPaymentRepository PaymentRepository { get; }
        IAccountRepository AccountsRepository { get; }
        IAccountUserRepository AccountUsersRepository { get; }
        IAccountDatabaseRepository DatabasesRepository { get; }
        IAccountDatabaseBackupsRepository AccountDatabaseBackupRepository { get; }
        IAccountUserSessionRepository AccountUserSessionsRepository { get; }
        ICloudServiceRepository CloudServiceRepository { get; }
        ICsResourceRepository CsResourceRepository { get; }
        IAccountCsResourceValueRepository AccountCsResourceValueRepository { get; }
        IBillingAccountRepository BillingAccountRepository { get; }
        IResourcesRepository ResourceRepository { get; }
        ICoreWorkerRepository CoreWorkerRepository { get; }
        IResourcesConfigurationRepository ResourceConfigurationRepository { get; }
        ICoreWorkerTasksQueueRepository CoreWorkerTasksQueueRepository { get; }
        ICoreWorkerTasksQueueLiveRepository CoreWorkerTasksQueueLiveRepository { get; }
        IInvoiceRepository InvoiceRepository { get; }
        IPartnersRepository PartnerRepository { get; }
        ICloudServicesSegmentRepository CloudServicesSegmentRepository { get; }
        ICloudServicesSegmentStorageRepository CloudServicesSegmentStorageRepository { get; }
        ICloudServicesEnterpriseServerRepository CloudServicesEnterpriseServerRepository { get; }
        ICloudServicesFileStorageServerRepository CloudServicesFileStorageServerRepository { get; }
        ICloudChangesRepository CloudChangesRepository { get; }
        IAcDbSupportRepository AcDbSupportRepository { get; }
        IPlatformVersionReferencesRepository PlatformVersionReferencesRepository { get; }
        IWebSocketConnectionRepository WebSocketConnectionRepository { get; }
        IBillingServiceRepository BillingServiceRepository { get; }
        IUploadedFileRepository UploadedFileRepository { get; }
        IGenericRepository<AccountEmail> AccountEmailRepository { get; }
        IGenericRepository<AcDbAccess> AcDbAccessesRepository { get; }
        IGenericRepository<CloudTerminalServer> CloudTerminalServerRepository { get; }
        IGenericRepository<PublishNodeReference> PublishNodeReferenceRepository { get; }
        IGenericRepository<CloudServicesContentServerNode> CloudServicesContentServerNodeRepository { get; }
        IGenericRepository<CloudConfiguration> CloudConfigurationRepository { get; }
        IGenericRepository<DiskResource> DiskResourceRepository { get; }
        IGenericRepository<SessionResource> SessionResourceRepository { get; }
        IGenericRepository<BillingServiceType> BillingServiceTypeRepository { get; }
        IGenericRepository<DbTemplate> DbTemplateRepository { get; }
        IGenericRepository<DbTemplateUpdate> DbTemplateUpdateRepository { get; }
        IGenericRepository<Configurations1C> Configurations1CRepository { get; }
        IGenericRepository<ConfigurationNameVariation> ConfigurationNameVariationRepository { get; }
        IGenericRepository<CoreWorkerTask> CoreWorkerTaskRepository { get; }
        IGenericRepository<CloudCore> CloudCoreRepository { get; }
        IGenericRepository<Rate> RateRepository { get; }
        IGenericRepository<AccountRate> AccountRateRepository { get; }
        IGenericRepository<InvoiceProduct> InvoiceProductRepository { get; }
        IGenericRepository<InvoiceReceipt> InvoiceReceiptRepository { get; }
        IGenericRepository<FlowResourcesScope> FlowResourcesScopeRepository { get; }
        IAccountUserRoleRepository AccountUserRoleRepository { get; }
        IGenericRepository<AcDbAccRolesJho> AcDbAccRolesJhoRepository { get; }
        IGenericRepository<Locale> LocaleRepository { get; }
        IGenericRepository<CloudServicesGatewayTerminal> CloudServicesGatewayTerminalRepository { get; }
        IGenericRepository<CloudServicesTerminalFarm> CloudServicesTerminalFarmRepository { get; }
        IGenericRepository<CloudServicesContentServer> CloudServicesContentServerRepository { get; }
        IGenericRepository<CloudServicesSqlServer> CloudServicesSqlServerRepository { get; }
        IGenericRepository<CloudServicesBackupStorage> CloudServicesBackupStorageRepository { get; }
        IGenericRepository<AvailableMigration> AvailableMigrationRepository { get; }
        IGenericRepository<AcDbSupportHistory> AcDbSupportHistoryRepository { get; }
        IGenericRepository<CloudChangesAction> CloudChangesActionRepository { get; }
        IGenericRepository<CloudChangesGroup> CloudChangesGroupRepository { get; }
        IGenericRepository<ProvidedService> ProvidedServiceRepository { get; }
        IGenericRepository<CloudServicesSegmentTerminalServer> CloudServicesSegmentTerminalServerRepository { get; }
        IGenericRepository<AccountDatabaseUpdateVersionMapping> AccountDatabaseUpdateVersionMappingRepository { get; }
        IGenericRepository<ConfigurationsCfu> ConfigurationsCfuRepository { get; }
        IGenericRepository<MigrationAccountHistory> MigrationAccountHistoryRepository { get; }
        IGenericRepository<MigrationAccountDatabaseHistory> MigrationAccountDatabaseHistoryRepository { get; }
        IGenericRepository<NotificationBuffer> NotificationBufferRepository { get; }
        IGenericRepository<DbTemplateDelimiters> DbTemplateDelimitersReferencesRepository { get; }
        IGenericRepository<AccountDatabaseOnDelimiters> AccountDatabaseDelimitersRepository { get; }
        IGenericRepository<AccountDatabaseBackupHistory> AccountDatabaseBackupHistoryRepository { get; }
        IGenericRepository<Industry> IndustryRepository { get; }
        IGenericRepository<CloudFile> CloudFileRepository { get; }
        IGenericRepository<IndustryDependencyBillingService> IndustryDependencyBillingServiceRepository { get; }
        IGenericRepository<BillingServiceTypeRelation> BillingServiceTypeRelationRepository { get; }
        IGenericRepository<BillingService1CFile> BillingService1CFileRepository { get; }
        IGenericRepository<ProcessFlow> ProcessFlowRepository { get; }
        IGenericRepository<ActionFlow> ActionFlowRepository { get; }
        IGenericRepository<Supplier> SupplierRepository { get; }
        IGenericRepository<SupplierReferralAccount> SupplierReferralAccountRepository { get; }
        IGenericRepository<PhysicalPersonRequisites> PhysicalPersonRequisitesRepository { get; }
        IGenericRepository<LegalPersonRequisites> LegalPersonRequisitesRepository { get; }
        IGenericRepository<SoleProprietorRequisites> SoleProprietorRequisitesRepository { get; }
        IGenericRepository<AgentRequisitesFile> AgentRequisitesFileRepository { get; }
        IGenericRepository<PrintedHtmlForm> PrintedHtmlFormRepository { get; }
        IGenericRepository<PrintedHtmlFormFile> PrintedHtmlFormFileRepository { get; }
        IGenericRepository<AgentCashOutRequestFile> AgentCashOutRequestFileRepository { get; }
        IGenericRepository<CoreHosting> CoreHostingRepository { get; }
        IAccountSaleManagerRepository AccountSaleManagerRepository { get; }
        IGenericRepository<AgentPaymentRelation> AgentPaymentRelationRepository { get; }
        IGenericRepository<ServiceAccount> ServiceAccountRepository { get; }
        IGenericRepository<ManageDbOnDelimitersAccess> ManageDbOnDelimitersAccessRepository { get; }
        IGenericRepository<AgencyAgreement> AgencyAgreementRepository { get; }
        IGenericRepository<AgentTransferBalanseRequest> AgentTransferBalanceRequestRepository { get; }
        IGenericRepository<DelimiterSourceAccountDatabase> DelimiterSourceAccountDatabaseRepository { get; }
        IBillingServiceTypeActivityForAccountRepository BillingServiceTypeActivityForAccountRepository { get; }
        ISavedPaymentMethodRepository SavedPaymentMethodRepository { get; }
        ISavedPaymentMethodBankCardRepository SavedPaymentMethodBankCardRepository { get; }
        IAccountAutoPayConfigurationRepository AccountAutoPayConfigurationRepository { get; }
        ILink42ConfigurationsRepository Link42ConfigurationsRepository { get; }
        ILink42ConfigurationBitDepthRepository Link42ConfigurationBitDepthRepository { get; }
        IConnectionRepository ConnectionRepository { get; }
        IGoogleSheetRepository GoogleSheetRepository { get; }
        IWaitsInfoRepository WaitsInfoRepository { get; }
        INotificationRepository NotificationRepository { get; }
        IGenericRepository<Incident> IncidentRepository { get; }
        IGenericRepository<Department> DepartmentRepository { get; }
        IGenericRepository<AccountUserDepartment> AccountUserDepartmentRepository { get; }
        IGenericRepository<AutoUpdateNode> AutoUpdateNodeRepository { get; }
        IEmailSmsVerificationRepository EmailSmsVerificationRepository { get; }
        INotificationSettingsRepository NotificationSettingsRepository { get; }
        IAccountUserNotificationSettingsRepository AccountUserNotificationSettingsRepository { get; }
        IArticleRepository ArticleRepository { get; }
        IGenericRepository<UpdateNodeQueue> UpdateNodeQueueRepository { get; }
        IArticleWalletsRepository ArticleWalletsRepository { get; }
        IArticleTransactionRepository ArticleTransactionRepository { get; }
        IAccountAdditionalCompanyRepository AccountAdditionalCompanyRepository { get; }
        IAccountFileRepository AccountFileRepository { get; }

        #endregion

        IGenericRepository<TEntity> GetGenericRepository<TEntity>() where TEntity : class, new();
        List<TResult> ExecuteSqlQueryList<TResult>(string query);
        Task<TResult> ExecuteSqlQueryAsync<TResult>(string query);
        void SetCommandTimeout(int timeout);
        void Save();
        Task SaveAsync();
        Task BulkUpdateAsync<T>(IEnumerable<T> entities) where T : class;
        void BulkUpdate<T>(IEnumerable<T> entities) where T : class;
        void BulkDelete<T>(IEnumerable<T> entities) where T : class;
        Task BulkDeleteAsync<T>(IEnumerable<T> entities) where T : class;
        void BulkInsert<T>(IEnumerable<T> entities) where T : class;
        Task BulkInsertAsync<T>(IEnumerable<T> entities) where T : class;
        Task RefreshAllAsync();
        void RefreshAll();
        void Execute1CSqlQuery(string query, string serverName, int timeout = 5 * 60);
        IEnumerable Execute1CSqlReader(string query, string serverName);
    }
}
