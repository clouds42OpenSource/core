﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Clouds42.Repositories.Interfaces.Common
{
    public interface IGenericRepository<TEntity> where TEntity : class, new()
    {
        /// <summary> Возвращает список сущностей. Аналог LINQ Where </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <returns></returns>
        IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> filter = null);
        IQueryable<TEntity> WhereLazy(Expression<Func<TEntity, bool>> filter = null);
        IQueryable<TEntity> AsQueryable();

        /// <summary> Возвращает сущность. Аналог LINQ FirstOrDefault </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <returns></returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null);

        /// <summary> Возвращает сущность. Аналог LINQ FirstOrDefault </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <param name="exceptionMessage">Текст исключения.</param>
        /// <returns></returns>
        TEntity FirstOrThrowException(Expression<Func<TEntity, bool>> filter = null, string exceptionMessage = null);
        TEntity GetFirst(Expression<Func<TEntity, bool>> predicate);

        /// <summary> Возвращает сущность. Аналог LINQ FirstOrDefault </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <returns></returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null);
        ISmartTransaction SmartTransaction { get; }
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> All();
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);

        /// <summary> Получает сущность по первичным ключам </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(params object[] id);

        /// <summary> Получает сущность по первичным ключам </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<TEntity> GetByIdAsync(params object[] id);

        /// <summary> Вставляет новую сущность в контекст. </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary> Вставляет массив новых сущностей в контекст. </summary>
        /// <param name="entities"></param>
        void InsertRange(IEnumerable<TEntity> entities);

        /// <summary> Удаляет сущность по первичным ключам </summary>
        /// <param name="id"></param>
        void Delete(object id);

        /// <summary> Удаляет сущность </summary>
        /// <param name="entityToDelete"></param>
        void Delete(TEntity entityToDelete);

        /// <summary> Удаляет список сущностей</summary>
        /// <param name="toDelete"></param>
        void DeleteRange(IEnumerable<TEntity> toDelete);

        /// <summary> Обновляет сущность в репозитории </summary>
        /// <param name="entityToUpdate"></param>
        void Update(TEntity entityToUpdate);
        void Reload(TEntity entity);
        bool Any(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<T> Select<T>(Expression<Func<TEntity, T>> selector);
        IOrderedQueryable<TEntity> OrderBy<T>(Expression<Func<TEntity, T>> selector);
        IOrderedQueryable<TEntity> OrderByDescending<T>(Expression<Func<TEntity, T>> selector);
        int Count();
        IQueryable<TEntity> AsQueryableNoTracking();
    }
}
