﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    public interface ICloudServicesSegmentRepository: IGenericRepository<CloudServicesSegment>
    {
        /// <summary>
        ///     Получить сегмент
        /// </summary>
        /// <param name="segmentId">Идентификатор сегмента</param>
        CloudServicesSegment GetSegment(Guid segmentId);
    }
}
