﻿using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    ///     Репозиторий для PlatformVersionReferences
    /// </summary>
    public interface IPlatformVersionReferencesRepository : IGenericRepository<PlatformVersionReference>
    {
        /// <summary>
        ///     Получить список стабильных версий 8.2
        /// </summary>
        IQueryable<string> GetStable82Versions();

        /// <summary>
        ///     Получить список стабильных версий 8.3
        /// </summary>
        IQueryable<string> GetStable83Versions();

        /// <summary>
        ///     Получить список альфа версий 8.3
        /// </summary>
        IQueryable<string> GetAlpha83Versions();
    }
}
