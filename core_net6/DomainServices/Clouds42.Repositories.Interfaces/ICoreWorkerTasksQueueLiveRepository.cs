﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Repositories.Interfaces
{
    /// <summary>
    /// Репозиторий для работы с очередью задач
    /// </summary>
    public interface ICoreWorkerTasksQueueLiveRepository : IGenericRepository<CoreWorkerTasksQueueLive>
    {
        /// <summary>
        /// Получить все взаимосвязанные задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные задачи</param>
        /// <returns>все взаимосвязанные задачи</returns>
        Task<List<CoreWorkerTasksQueueLive>> GetAllDependentTasks(Guid coreWorkerTasksQueueIdFromToSearch);


        /// <summary>
        /// Проверяет, все ли задачи завершины начиная с переданной, находит все взаимосвязанные и смотрит на состояние
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные</param>
        /// <returns>Если все состояния в задачах <see cref="CloudTaskQueueStatus.Ready"/> или есть хотя бы одна с <see cref="CloudTaskQueueStatus.Error"/>, то возвращает (<c>true</c>, список взаимосвязанных задач), иначе (<c>false</c>, [])</returns>
        Task<ValueTuple<bool, List<CoreWorkerTasksQueueLive>>> GetMultiTaskCompletedInfo(Guid coreWorkerTasksQueueIdFromToSearch);


        /// <summary>
        /// Проверяет, все ли задачи завершины начиная с переданной, находит все взаимосвязанные и смотрит на состояние, если считается завершённой:(<see cref="GetMultiTaskCompletedInfo"/>), то удаляет все взаимосвязанные задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные</param>
        Task DeleteAllMultiTasksWhenCompleted(Guid coreWorkerTasksQueueIdFromToSearch);
    }
}
