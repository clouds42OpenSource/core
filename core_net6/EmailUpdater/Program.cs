﻿using System.Text.RegularExpressions;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations;
using Clouds42.DependencyRegistration;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using EmailUpdater;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;

ServiceCollection serviceCollection = [];
IUnitOfWork dbLayer;
ILogger42 _logger;
List<int> accountIndexes = [];
string originalDomainMail;
string expectedDomainMail;
Console.OutputEncoding = System.Text.Encoding.UTF8;

IConfiguration configuration = new ConfigurationBuilder()
    .AddGcpKeyValueSecrets()
    .AddJsonFile("appsettings.json", true, true)
    .Build();
serviceCollection
    .AddSerilogWithElastic(configuration)
    .AddScoped<IUnitOfWork, UnitOfWork>()
    .AddGlobalServices(configuration)
    .AddHttpClient()
    .AddTransient<IAccessProvider, SystemAccessProvider>()
    .AddTelegramBots(configuration)
    .AddSingleton(configuration)
    .AddTransient<ILogger42, SerilogLogger42>();
var serviceProvider = serviceCollection.BuildServiceProvider();
_logger = serviceProvider.GetRequiredService<ILogger42>();
dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
accountIndexes = configuration.GetSection("AccountsIndexes").Get<List<int>>();
originalDomainMail = "@" + configuration.GetSection("OriginalDomainMail").Value;
expectedDomainMail = configuration.GetSection("ExpectedDomainMail").Value;

ConfigurationHelper.SetConfiguration(configuration);
HandlerException42.Init(serviceProvider);
Logger42.Init(serviceProvider);

Console.WriteLine("Work started");

var accountsIds = await dbLayer.AccountsRepository
    .AsQueryableNoTracking()
    .Where(x => accountIndexes.Contains(x.IndexNumber))
    .Select(x => x.Id)
    .ToListAsync();

Console.WriteLine($"Accounts finded - {accountsIds.Count}");

var users = await dbLayer.AccountUsersRepository
    .AsQueryable()
    .Include(x => x.Account)
    .ThenInclude(x => x.AccountConfiguration)
    .ThenInclude(x => x.Locale)
    .Where(x => accountsIds.Contains(x.AccountId) && x.Email.EndsWith(originalDomainMail))
    .ToListAsync();

var usersToUpdate = new List<AccountUser>();
Console.WriteLine($"Users finded - {users.Count}");

var errorListEmails = new List<string>();

foreach (var user in users)
{
    try
    {
        var newEmail = ReplaceEmailDomain(user.Email, expectedDomainMail);

        Console.WriteLine($"Replaced user email {user.Email} to {newEmail}");

        user.Email = newEmail;

        ActiveDirectoryUserFunctions.ChangeEmail(newEmail, user.Login);

        Console.WriteLine(
            $"Updated in ActiveDirectory - user login {user.Login} new userPrincipalName - {newEmail}");

        var openIdModel = new SauriOpenIdControlUserModel
        {
            AccountUserID = user.Id, AccountID = user.Account.Id, Email = newEmail
        };

        var openIdProvider = serviceProvider.GetRequiredService<IOpenIdProvider>();

        openIdProvider.ChangeUserProperties(openIdModel, user.Account.AccountConfiguration.Locale.Name);

        Console.WriteLine($"Updated in open-id provider - user login {user.Login} new Email - {newEmail}");

        usersToUpdate.Add(user);
    }
    catch (Exception e)
    {
        _logger.Error(e, $"Ошибка обновления email {user.Login}");

        Console.WriteLine($"{user.Login} {e.Message}");

        errorListEmails.Add($"{user.Email} | {e.Message}");
    }
}
await dbLayer.BulkUpdateAsync(usersToUpdate);
await dbLayer.SaveAsync();

Console.WriteLine($"Updated in database - {usersToUpdate.Count}");

if (errorListEmails.Any())
{
    await File.WriteAllTextAsync("errors.txt", string.Join(Environment.NewLine, errorListEmails));
}

Console.WriteLine("Work ended");
Console.ReadKey();


string ReplaceEmailDomain(string email, string newDomain)
{
    string pattern = @"@([a-zA-Z0-9-]+\.[a-zA-Z]{2,})$";
    string replacement = "@" + newDomain;

    return Regex.Replace(email, pattern, replacement);
}

namespace EmailUpdater
{
    public static class DependencyConfiguration
    {
        public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
        {
            var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

            var elasticConfig = new ElasticConfigDto(
                conf["Elastic:Uri"],
                conf["Elastic:UserName"],
                conf["Elastic:Password"],
                conf["Elastic:ApiIndex"],
                conf["Logs:AppName"]
            );

            SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);

            return collection;
        }
    }
}
