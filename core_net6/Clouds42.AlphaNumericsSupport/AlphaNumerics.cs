﻿using System.Collections.Generic;
using System.Text;

namespace Clouds42.AlphaNumericsSupport
{
    /// <summary>
    /// Алфавит и цифры
    /// </summary>
    public static class AlphaNumerics
    {
        private const string LowerCaseAlpha = "abcdefghijklmnopqrstuvwxyz";
        private static readonly string UpperCaseAlpha = LowerCaseAlpha.ToUpper();
        private const string Numerics = "0123456789";
        private const string SpecSymbols = "$%#@!*?;:^&";


        private static readonly Dictionary<AlphaNumericsTypes, string> Container = new()
        {
            [AlphaNumericsTypes.LowerCaseAlpha] = LowerCaseAlpha,
            [AlphaNumericsTypes.UpperCaseAlpha] = UpperCaseAlpha,
            [AlphaNumericsTypes.Numerics] = Numerics,
            [AlphaNumericsTypes.SpecSymbols] = SpecSymbols
        };

        /// <summary>
        /// Формирует алфавит по заданным параметрам
        /// </summary>
        /// <param name="requieredAlphaNumericsTypes"></param>
        /// <returns>Алфавит</returns>
        public static string GetSymbolsRange(params AlphaNumericsTypes[] requieredAlphaNumericsTypes)
        {
            var uniqueTypes = new HashSet<AlphaNumericsTypes>(requieredAlphaNumericsTypes);
            return GetSymbolsRange(uniqueTypes);
        }

        /// <summary>
        /// Формирует алфавит по заданным параметрам
        /// </summary>
        /// <param name="requiredAlphaNumericsTypesSet"></param>
        /// <returns>Алфавит</returns>
        private static string GetSymbolsRange(IEnumerable<AlphaNumericsTypes> requiredAlphaNumericsTypesSet)
        {
            var result = new StringBuilder();

            foreach (var requiredAlphaNumericsType in requiredAlphaNumericsTypesSet)
            {
                result.Append(Container[requiredAlphaNumericsType]);
            }
            return result.ToString();
        }
    }
}
