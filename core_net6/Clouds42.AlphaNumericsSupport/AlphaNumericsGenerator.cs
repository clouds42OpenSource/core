﻿using System;
using System.Text;

namespace Clouds42.AlphaNumericsSupport
{
    /// <summary>
    /// Генератор случайной строки
    /// </summary>
    public class AlphaNumericsGenerator(Random randomGen) : IAlphaNumericsGenerator
    {
        /// <summary>
        /// Генерирует стоку произвольной длины и с произвольными символами
        /// </summary>
        /// <param name="strLength">Длина произвольной строки</param>
        /// <param name="availableAlphaNumericsTypes">Использующиеся символы</param>
        /// <returns>Возвращает случайную строку</returns>
        public string GetRandomString(int strLength, params AlphaNumericsTypes[] availableAlphaNumericsTypes)
        {
            var symbols = AlphaNumerics.GetSymbolsRange(availableAlphaNumericsTypes);
            var sb = new StringBuilder();
            for (var i = 0; i < strLength; i++)
            {
                sb.Append(symbols[randomGen.Next(symbols.Length)]);
            }

            return sb.ToString();
        }
    }
}
