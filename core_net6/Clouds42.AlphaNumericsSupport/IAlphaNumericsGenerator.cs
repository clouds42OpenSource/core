﻿namespace Clouds42.AlphaNumericsSupport
{
    /// <summary>
    /// Генератор случайной строки
    /// </summary>
    public interface IAlphaNumericsGenerator
    {
        /// <summary>
        /// Генерирует стоку произвольной длины и с произвольными символами
        /// </summary>
        /// <param name="strLength">Длина произвольной строки</param>
        /// <param name="availableAlphaNumericsTypes">Использующиеся символы</param>
        /// <returns>Возвращает случайную строку</returns>
        string GetRandomString(int strLength, params AlphaNumericsTypes[] availableAlphaNumericsTypes);
    }
}