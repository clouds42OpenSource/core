﻿namespace Clouds42.AlphaNumericsSupport
{
    public enum AlphaNumericsTypes
    {
        LowerCaseAlpha = 0,
        UpperCaseAlpha,
        Numerics,
        SpecSymbols
    }
}