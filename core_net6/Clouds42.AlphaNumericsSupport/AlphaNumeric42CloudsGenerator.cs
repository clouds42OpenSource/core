﻿using Clouds42.Common.Encrypt;
using Clouds42.Configurations.Configurations;

namespace Clouds42.AlphaNumericsSupport
{
    /// <summary>
    /// Генератор строки
    /// </summary>
    public class AlphaNumeric42CloudsGenerator(CryptoRandomGeneric randomGen)
    {
        private readonly AlphaNumericsGenerator _alphaNumericsGenerator = new(randomGen);
        private const int PasswordLength = 6;
        private const int ConfirmationLoginCodeLength = 6;

        /// <summary>
        /// Стандартный генератор пароля
        /// </summary>
        /// <returns>Сгенерированный пароль</returns>
        public string GeneratePassword()
        {
            var passwordPrefix = CloudConfigurationProvider.Registration.GetPasswordPrefix();

            var password = _alphaNumericsGenerator.GetRandomString(PasswordLength, AlphaNumericsTypes.Numerics,
                AlphaNumericsTypes.LowerCaseAlpha, AlphaNumericsTypes.UpperCaseAlpha);

            return $"{passwordPrefix}{password}";
        }

        /// <summary>
        /// Генератор кода подтверждения смс
        /// </summary>
        /// <returns>Код подтверждения</returns>
        public string GenerateConfirmationCode() =>
            _alphaNumericsGenerator.GetRandomString(ConfirmationLoginCodeLength, AlphaNumericsTypes.Numerics);
    }
}
