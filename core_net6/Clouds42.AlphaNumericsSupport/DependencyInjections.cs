﻿using Clouds42.Common.Encrypt;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AlphaNumericsSupport
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddAlphaNumeric(this IServiceCollection service)
        {
            service.AddTransient<IAlphaNumericsGenerator, AlphaNumericsGenerator>(sp =>
                new AlphaNumericsGenerator(new CryptoRandomGeneric()));

            return service;
        }
    }
}
