﻿namespace Clouds42.DomainContext
{
    public static class ProviderTypeConstants
    {
        public const string MsSql = "mssql";
        public const string PostgreSql = "postgresql";
    }
}
