﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Clouds42.DomainContext.Migrations
{
    /// <inheritdoc />
    public partial class PostgreInitial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "billing");

            migrationBuilder.EnsureSchema(
                name: "history");

            migrationBuilder.EnsureSchema(
                name: "statemachine");

            migrationBuilder.EnsureSchema(
                name: "marketing");

            migrationBuilder.EnsureSchema(
                name: "billingCommits");

            migrationBuilder.EnsureSchema(
                name: "its");

            migrationBuilder.EnsureSchema(
                name: "segment");

            migrationBuilder.EnsureSchema(
                name: "security");

            migrationBuilder.EnsureSchema(
                name: "mailing");

            migrationBuilder.EnsureSchema(
                name: "configuration");

            migrationBuilder.EnsureSchema(
                name: "accessDatabase");

            migrationBuilder.EnsureSchema(
                name: "migration");

            migrationBuilder.CreateTable(
                name: "AccessMappingRule",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Action = table.Column<int>(type: "integer", nullable: false),
                    Group = table.Column<int>(type: "integer", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessMappingRule", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    ReferrerId = table.Column<Guid>(type: "uuid", nullable: true),
                    IndexNumber = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RegistrationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true),
                    UserSource = table.Column<string>(type: "text", nullable: true),
                    Removed = table.Column<bool>(type: "boolean", nullable: true),
                    Deployment = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingBannerAudience",
                schema: "marketing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountType = table.Column<int>(type: "integer", nullable: false),
                    AvailabilityPayment = table.Column<int>(type: "integer", nullable: false),
                    RentalServiceState = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingBannerAudience", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AutoUpdateNodes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    NodeAddress = table.Column<string>(type: "text", nullable: true),
                    AutoUpdateObnovlyatorPath = table.Column<string>(type: "text", nullable: false),
                    ManualUpdateObnovlyatorPath = table.Column<string>(type: "text", nullable: true),
                    DontRunInGlobalAU = table.Column<bool>(type: "boolean", nullable: false),
                    IsBusy = table.Column<bool>(type: "boolean", nullable: false),
                    AutoUpdateNodeType = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AutoUpdateNodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BillingServiceExtension",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Service1CFileName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingServiceExtension", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CloudChangesGroups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GroupName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudChangesGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CloudCore",
                columns: table => new
                {
                    PrimaryKey = table.Column<Guid>(type: "uuid", nullable: false),
                    MainPageNotification = table.Column<string>(type: "text", nullable: true),
                    HourseDifferenceOfUkraineAndMoscow = table.Column<int>(type: "integer", nullable: true),
                    RussianLocalePaymentAggregator = table.Column<int>(type: "integer", nullable: false),
                    BannerUrl = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudCore", x => x.PrimaryKey);
                });

            migrationBuilder.CreateTable(
                name: "CloudFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    ContentType = table.Column<string>(type: "text", nullable: true),
                    Content = table.Column<byte[]>(type: "bytea", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudFiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesBackupStorage",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PhysicalPath = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesBackupStorage", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesContentServer",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    PublishSiteName = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    GroupByAccount = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesContentServer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesEnterpriseServer",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Version = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    AdminName = table.Column<string>(type: "text", nullable: true),
                    AdminPassword = table.Column<string>(type: "text", nullable: true),
                    ClusterSettingsPath = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesEnterpriseServer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesFileStorageServers",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PhysicalPath = table.Column<string>(type: "text", nullable: false),
                    DnsName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesFileStorageServers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesGatewayTerminals",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesGatewayTerminals", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesSQLServer",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    RestoreModelType = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesSQLServer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesTerminalFarm",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    UsingOutdatedWindows = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesTerminalFarm", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CloudTerminalServers",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ConnectionAddress = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudTerminalServers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CoreHosting",
                schema: "segment",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    UploadFilesPath = table.Column<string>(type: "text", nullable: true),
                    UploadApiUrl = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreHosting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkers",
                columns: table => new
                {
                    Id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Status = table.Column<short>(type: "smallint", nullable: true),
                    Tick = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    MaxThreadsCount = table.Column<short>(type: "smallint", nullable: true),
                    BusyThreadsCount = table.Column<short>(type: "smallint", nullable: true),
                    CoreWorkerAddress = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkerTask",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    TaskName = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<bool>(type: "boolean", nullable: true),
                    MethodName = table.Column<string>(type: "text", nullable: true),
                    TaskParams = table.Column<string>(type: "text", nullable: true),
                    WorkerTaskType = table.Column<int>(type: "integer", nullable: false),
                    TaskAllowsMultithreading = table.Column<bool>(type: "boolean", nullable: false),
                    TaskExecutionLifetimeInMinutes = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkerTask", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DatabaseLaunchRdpStartHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountNumber = table.Column<int>(type: "integer", nullable: false),
                    ActionCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Action = table.Column<int>(type: "integer", nullable: false),
                    Login = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    LinkAppVersion = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    V82Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    LinkAppType = table.Column<int>(type: "integer", nullable: false),
                    LaunchType = table.Column<int>(type: "integer", nullable: false),
                    ExternalIpAddress = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    InternalIpAddress = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatabaseLaunchRdpStartHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HtmlTemplate",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HtmlData = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HtmlTemplate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Incidents",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    DateOfDiscover = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DateOfLastDiscover = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CountOfIncedentToday = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Incidents", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", maxLength: 2147483647, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItsAuthorizationData",
                schema: "security",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: true),
                    PasswordHash = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItsAuthorizationData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LetterTemplate",
                schema: "mailing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Header = table.Column<string>(type: "text", nullable: true),
                    Body = table.Column<string>(type: "text", nullable: true),
                    Subject = table.Column<string>(type: "text", nullable: true),
                    LetterTypeName = table.Column<string>(type: "text", nullable: true),
                    LetterAssemblyClassName = table.Column<string>(type: "text", nullable: true),
                    SendGridTemplateId = table.Column<string>(type: "text", nullable: true),
                    IsSystemLetter = table.Column<bool>(type: "boolean", nullable: false),
                    NotificationText = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LetterTemplate", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locale",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Currency = table.Column<string>(type: "text", nullable: false),
                    CurrencyCode = table.Column<int>(type: "integer", nullable: true),
                    Country = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locale", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "NotificationSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    TypeText = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Partners",
                columns: table => new
                {
                    PartnerId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partners", x => x.PartnerId);
                });

            migrationBuilder.CreateTable(
                name: "PlatformVersionReferences",
                columns: table => new
                {
                    Version = table.Column<string>(type: "text", nullable: false),
                    PathToPlatform = table.Column<string>(type: "text", nullable: false),
                    PathToPlatfromX64 = table.Column<string>(type: "text", nullable: true),
                    MacOsThinClientDownloadLink = table.Column<string>(type: "text", nullable: true),
                    WindowsThinClientDownloadLink = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlatformVersionReferences", x => x.Version);
                });

            migrationBuilder.CreateTable(
                name: "PrintedHtmlForms",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    HtmlData = table.Column<string>(type: "text", nullable: true),
                    ModelType = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintedHtmlForms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessFlows",
                schema: "statemachine",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Key = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    ProcessedObjectName = table.Column<string>(type: "text", nullable: true),
                    State = table.Column<string>(type: "text", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessFlows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PublishNodesReferences",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublishNodesReferences", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Service1CFileVersion",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Version1CFile = table.Column<string>(type: "text", nullable: false),
                    DescriptionOf1CFileChanges = table.Column<string>(type: "text", nullable: true),
                    CommentForModerator = table.Column<string>(type: "text", nullable: true),
                    ModeratorComment = table.Column<string>(type: "text", nullable: true),
                    NeedNotifyAboutVersionChanges = table.Column<bool>(type: "boolean", nullable: false),
                    IsActualVersion = table.Column<bool>(type: "boolean", nullable: false),
                    AuditResult = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service1CFileVersion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WaitInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WaitType = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: false),
                    CausesJson = table.Column<string>(type: "text", nullable: true),
                    RecommendationsJson = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WaitInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountAdditionalCompanies",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Inn = table.Column<string>(type: "text", nullable: true),
                    Number = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountAdditionalCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountAdditionalCompanies_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountCorpClouds",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    CountOfClicks = table.Column<int>(type: "integer", nullable: false),
                    LastUseDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountCorpClouds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountCorpClouds_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountEmails",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountEmails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountEmails_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountLocaleChanges",
                schema: "history",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    ChangesDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountLocaleChanges", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_AccountLocaleChanges_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountRequisites",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    Inn = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountRequisites", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_AccountRequisites_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Activated = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedInAd = table.Column<bool>(type: "boolean", nullable: false),
                    ConfirmationCode = table.Column<string>(type: "text", nullable: true),
                    LastLoggedIn = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Unsubscribed = table.Column<bool>(type: "boolean", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    MiddleName = table.Column<string>(type: "text", nullable: true),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Removed = table.Column<bool>(type: "boolean", nullable: true),
                    IsPhoneVerified = table.Column<bool>(type: "boolean", nullable: true),
                    EditDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ResetCode = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthGuid = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthToken = table.Column<Guid>(type: "uuid", nullable: true),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateTimeOfLastChangePasswordOrLogin = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    EmailStatus = table.Column<string>(type: "text", nullable: false),
                    LoginCode = table.Column<string>(type: "text", nullable: true),
                    DateLoginCode = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ReferralId = table.Column<Guid>(type: "uuid", nullable: true),
                    PrimaryPhone = table.Column<string>(type: "text", nullable: true),
                    ConfirmationAttemptsLeft = table.Column<byte>(type: "smallint", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    CorpUserID = table.Column<Guid>(type: "uuid", nullable: true),
                    CorpUserSyncStatus = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUsers_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentPaymentRelations",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentPaymentRelations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentPaymentRelations_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentWallets",
                schema: "billing",
                columns: table => new
                {
                    AccountOwnerId = table.Column<Guid>(type: "uuid", nullable: false),
                    AvailableSum = table.Column<decimal>(type: "numeric", nullable: false),
                    ApplyAgencyAgreementDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentWallets", x => x.AccountOwnerId);
                    table.ForeignKey(
                        name: "FK_AgentWallets_Accounts_AccountOwnerId",
                        column: x => x.AccountOwnerId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingAccounts",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Balance = table.Column<decimal>(type: "numeric", nullable: false),
                    BonusBalance = table.Column<decimal>(type: "numeric", nullable: false),
                    MonthlyPaymentDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    AccountType = table.Column<string>(type: "text", nullable: true),
                    PromisePaymentDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    PromisePaymentSum = table.Column<decimal>(type: "numeric", nullable: true),
                    AdditionalResourceName = table.Column<string>(type: "text", nullable: true),
                    AdditionalResourceCost = table.Column<decimal>(type: "numeric(18,0)", precision: 18, scale: 0, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingAccounts_Accounts_Id",
                        column: x => x.Id,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GoogleSheets",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentId = table.Column<string>(type: "text", nullable: true),
                    PeriodFrom = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    PeriodTo = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    SheetType = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoogleSheets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoogleSheets_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MainServiceResourcesChanges",
                schema: "history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    DisabledResourcesCount = table.Column<int>(type: "integer", nullable: false),
                    EnabledResourcesCount = table.Column<int>(type: "integer", nullable: false),
                    ChangesDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainServiceResourcesChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MainServiceResourcesChanges_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MyDiskPropertiesByAccounts",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    MyDiskFilesSizeInMb = table.Column<long>(type: "bigint", nullable: false),
                    MyDiskFilesSizeActualDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MyDiskPropertiesByAccounts", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_MyDiskPropertiesByAccounts_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NotificationBuffer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Account = table.Column<Guid>(type: "uuid", nullable: false),
                    NotifyKey = table.Column<string>(type: "text", nullable: true),
                    ActualPeriod = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Counter = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationBuffer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NotificationBuffer_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProvidedServices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    SponsoredAccountId = table.Column<Guid>(type: "uuid", nullable: true),
                    Rate = table.Column<decimal>(type: "numeric(18,0)", precision: 18, scale: 0, nullable: false),
                    Service = table.Column<string>(type: "text", nullable: false),
                    ServiceType = table.Column<string>(type: "text", nullable: true),
                    DateFrom = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DateTo = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Count = table.Column<int>(type: "integer", nullable: false),
                    KindOfServiceProvision = table.Column<int>(type: "integer", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProvidedServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProvidedServices_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProvidedServices_Accounts_SponsoredAccountId",
                        column: x => x.SponsoredAccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SavedPaymentMethods",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    PaymentSystem = table.Column<string>(type: "text", nullable: false),
                    Token = table.Column<string>(type: "text", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Type = table.Column<string>(type: "text", nullable: true),
                    SubType = table.Column<string>(type: "text", nullable: true),
                    CreatedOn = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Metadata = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedPaymentMethods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedPaymentMethods_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UploadedFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    FullFilePath = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    UploadDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    FileSizeInMegabytes = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadedFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UploadedFiles_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudChangesActions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    GroupId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Index = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudChangesActions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudChangesActions_CloudChangesGroups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "CloudChangesGroups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountFiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    CloudFileId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountFiles_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountFiles_CloudFiles_CloudFileId",
                        column: x => x.CloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingServiceChanges",
                schema: "billingCommits",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    ShortDescription = table.Column<string>(type: "text", nullable: true),
                    Opportunities = table.Column<string>(type: "text", nullable: true),
                    IconCloudFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    OriginalIconCloudFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsHybridService = table.Column<bool>(type: "boolean", nullable: false),
                    InstructionServiceCloudFileId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingServiceChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingServiceChanges_CloudFiles_IconCloudFileId",
                        column: x => x.IconCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BillingServiceChanges_CloudFiles_InstructionServiceCloudFil~",
                        column: x => x.InstructionServiceCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BillingServiceChanges_CloudFiles_OriginalIconCloudFileId",
                        column: x => x.OriginalIconCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Services",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ShortDescription = table.Column<string>(type: "text", nullable: true),
                    Opportunities = table.Column<string>(type: "text", nullable: true),
                    IconCloudFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    OriginalIconCloudFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    InstructionServiceCloudFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    AccountOwnerId = table.Column<Guid>(type: "uuid", nullable: true),
                    SystemService = table.Column<int>(type: "integer", nullable: true),
                    BillingServiceStatus = table.Column<int>(type: "integer", nullable: false),
                    ServiceActivationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    StatusDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    InternalCloudService = table.Column<int>(type: "integer", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    IsHybridService = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Services_Accounts_AccountOwnerId",
                        column: x => x.AccountOwnerId,
                        principalTable: "Accounts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Services_CloudFiles_IconCloudFileId",
                        column: x => x.IconCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Services_CloudFiles_InstructionServiceCloudFileId",
                        column: x => x.InstructionServiceCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Services_CloudFiles_OriginalIconCloudFileId",
                        column: x => x.OriginalIconCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkerAvailableTasksBags",
                columns: table => new
                {
                    CoreWorkerId = table.Column<short>(type: "smallint", nullable: false),
                    CoreWorkerTaskId = table.Column<Guid>(type: "uuid", nullable: false),
                    Priority = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkerAvailableTasksBags", x => new { x.CoreWorkerId, x.CoreWorkerTaskId });
                    table.ForeignKey(
                        name: "FK_CoreWorkerAvailableTasksBags_CoreWorkerTask_CoreWorkerTaskId",
                        column: x => x.CoreWorkerTaskId,
                        principalTable: "CoreWorkerTask",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoreWorkerAvailableTasksBags_CoreWorkers_CoreWorkerId",
                        column: x => x.CoreWorkerId,
                        principalTable: "CoreWorkers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkerTasksQueue",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    StartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    EditDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CapturedWorkerId = table.Column<short>(type: "smallint", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    CoreWorkerTaskId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateTimeDelayOperation = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ExecutionResult = table.Column<string>(type: "text", nullable: true),
                    RetryTaskAttemptsCount = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkerTasksQueue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueue_CoreWorkerTask_CoreWorkerTaskId",
                        column: x => x.CoreWorkerTaskId,
                        principalTable: "CoreWorkerTask",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueue_CoreWorkers_CapturedWorkerId",
                        column: x => x.CapturedWorkerId,
                        principalTable: "CoreWorkers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingBannerTemplate",
                schema: "marketing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ImageCloudFileId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Header = table.Column<string>(type: "text", nullable: true),
                    Body = table.Column<string>(type: "text", nullable: true),
                    CaptionLink = table.Column<string>(type: "text", nullable: true),
                    Link = table.Column<string>(type: "text", nullable: true),
                    AdvertisingBannerAudienceId = table.Column<int>(type: "integer", nullable: false),
                    HtmlTemplateId = table.Column<int>(type: "integer", nullable: false),
                    DisplayBannerDateFrom = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DisplayBannerDateTo = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingBannerTemplate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingBannerTemplate_AdvertisingBannerAudience_Adverti~",
                        column: x => x.AdvertisingBannerAudienceId,
                        principalSchema: "marketing",
                        principalTable: "AdvertisingBannerAudience",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertisingBannerTemplate_CloudFiles_ImageCloudFileId",
                        column: x => x.ImageCloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertisingBannerTemplate_HtmlTemplate_HtmlTemplateId",
                        column: x => x.HtmlTemplateId,
                        principalTable: "HtmlTemplate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Configurations1C",
                columns: table => new
                {
                    Name = table.Column<string>(type: "text", nullable: false),
                    ConfigurationCatalog = table.Column<string>(type: "text", nullable: false),
                    RedactionCatalogs = table.Column<string>(type: "text", nullable: false),
                    PlatformCatalog = table.Column<string>(type: "text", nullable: false),
                    UpdateCatalogUrl = table.Column<string>(type: "text", nullable: false),
                    ShortCode = table.Column<string>(type: "text", nullable: true),
                    UseComConnectionForApplyUpdates = table.Column<bool>(type: "boolean", nullable: false),
                    NeedCheckUpdates = table.Column<bool>(type: "boolean", nullable: false),
                    ItsAuthorizationDataId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configurations1C", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Configurations1C_ItsAuthorizationData_ItsAuthorizationDataId",
                        column: x => x.ItsAuthorizationDataId,
                        principalSchema: "security",
                        principalTable: "ItsAuthorizationData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdvertisingBannerAudienceLocaleRelation",
                schema: "marketing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AdvertisingBannerAudienceId = table.Column<int>(type: "integer", nullable: false),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertisingBannerAudienceLocaleRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdvertisingBannerAudienceLocaleRelation_AdvertisingBannerAu~",
                        column: x => x.AdvertisingBannerAudienceId,
                        principalSchema: "marketing",
                        principalTable: "AdvertisingBannerAudience",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdvertisingBannerAudienceLocaleRelation_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudLocalizations",
                columns: table => new
                {
                    Key = table.Column<int>(type: "integer", nullable: false),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudLocalizations", x => new { x.Key, x.LocaleId });
                    table.ForeignKey(
                        name: "FK_CloudLocalizations_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DbTemplates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    DefaultCaption = table.Column<string>(type: "text", nullable: true),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    ImageUrl = table.Column<string>(type: "text", nullable: true),
                    Remark = table.Column<string>(type: "text", nullable: true),
                    Platform = table.Column<string>(type: "text", nullable: true),
                    CanWebPublish = table.Column<bool>(type: "boolean", nullable: false),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: true),
                    DemoTemplateId = table.Column<Guid>(type: "uuid", nullable: true),
                    NeedUpdate = table.Column<bool>(type: "boolean", nullable: false),
                    AdminLogin = table.Column<string>(type: "text", nullable: true),
                    AdminPassword = table.Column<string>(type: "text", nullable: true),
                    CurrentVersion = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbTemplates_DbTemplates_DemoTemplateId",
                        column: x => x.DemoTemplateId,
                        principalTable: "DbTemplates",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DbTemplates_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesSegment",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    GatewayTerminalsID = table.Column<Guid>(type: "uuid", nullable: true),
                    ServicesTerminalFarmID = table.Column<Guid>(type: "uuid", nullable: false),
                    ContentServerID = table.Column<Guid>(type: "uuid", nullable: false),
                    CoreHostingId = table.Column<Guid>(type: "uuid", nullable: false),
                    SQLServerID = table.Column<Guid>(type: "uuid", nullable: false),
                    BackupStorageID = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDefault = table.Column<bool>(type: "boolean", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    FileStorageServersID = table.Column<Guid>(type: "uuid", nullable: false),
                    WindowsAccountsPath = table.Column<string>(type: "text", nullable: true),
                    CustomFileStoragePath = table.Column<string>(type: "text", nullable: true),
                    Stable82Version = table.Column<string>(type: "text", nullable: false),
                    Alpha83Version = table.Column<string>(type: "text", nullable: true),
                    Stable83Version = table.Column<string>(type: "text", nullable: false),
                    EnterpriseServer82ID = table.Column<Guid>(type: "uuid", nullable: false),
                    EnterpriseServer83ID = table.Column<Guid>(type: "uuid", nullable: false),
                    AutoUpdateNodeId = table.Column<Guid>(type: "uuid", nullable: true),
                    DelimiterDatabaseMustUseWebService = table.Column<bool>(type: "boolean", nullable: false),
                    NotMountDiskR = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesSegment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_AutoUpdateNodes_AutoUpdateNodeId",
                        column: x => x.AutoUpdateNodeId,
                        principalTable: "AutoUpdateNodes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesBackupStorage_BackupStora~",
                        column: x => x.BackupStorageID,
                        principalTable: "CloudServicesBackupStorage",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesContentServer_ContentServ~",
                        column: x => x.ContentServerID,
                        principalTable: "CloudServicesContentServer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesEnterpriseServer_Enterpri~",
                        column: x => x.EnterpriseServer82ID,
                        principalTable: "CloudServicesEnterpriseServer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesEnterpriseServer_Enterpr~1",
                        column: x => x.EnterpriseServer83ID,
                        principalTable: "CloudServicesEnterpriseServer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesFileStorageServers_FileSt~",
                        column: x => x.FileStorageServersID,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesGatewayTerminals_GatewayT~",
                        column: x => x.GatewayTerminalsID,
                        principalTable: "CloudServicesGatewayTerminals",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesSQLServer_SQLServerID",
                        column: x => x.SQLServerID,
                        principalTable: "CloudServicesSQLServer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CloudServicesTerminalFarm_ServicesTerm~",
                        column: x => x.ServicesTerminalFarmID,
                        principalTable: "CloudServicesTerminalFarm",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_CoreHosting_CoreHostingId",
                        column: x => x.CoreHostingId,
                        principalSchema: "segment",
                        principalTable: "CoreHosting",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_PlatformVersionReferences_Alpha83Versi~",
                        column: x => x.Alpha83Version,
                        principalTable: "PlatformVersionReferences",
                        principalColumn: "Version",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_PlatformVersionReferences_Stable82Vers~",
                        column: x => x.Stable82Version,
                        principalTable: "PlatformVersionReferences",
                        principalColumn: "Version",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegment_PlatformVersionReferences_Stable83Vers~",
                        column: x => x.Stable83Version,
                        principalTable: "PlatformVersionReferences",
                        principalColumn: "Version",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgencyAgreements",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    PrintedHtmlFormId = table.Column<Guid>(type: "uuid", nullable: false),
                    EffectiveDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Rent1CRewardPercent = table.Column<decimal>(type: "numeric", nullable: false),
                    MyDiskRewardPercent = table.Column<decimal>(type: "numeric", nullable: false),
                    ServiceOwnerRewardPercent = table.Column<decimal>(type: "numeric", nullable: false),
                    Rent1CRewardPercentForVipAccounts = table.Column<decimal>(type: "numeric", nullable: false),
                    MyDiskRewardPercentForVipAccounts = table.Column<decimal>(type: "numeric", nullable: false),
                    ServiceOwnerRewardPercentForVipAccounts = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgencyAgreements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgencyAgreements_PrintedHtmlForms_PrintedHtmlFormId",
                        column: x => x.PrintedHtmlFormId,
                        principalTable: "PrintedHtmlForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PrintedHtmlFormFiles",
                columns: table => new
                {
                    PrintedHtmlFormId = table.Column<Guid>(type: "uuid", nullable: false),
                    CloudFileId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrintedHtmlFormFiles", x => new { x.PrintedHtmlFormId, x.CloudFileId });
                    table.ForeignKey(
                        name: "FK_PrintedHtmlFormFiles_CloudFiles_CloudFileId",
                        column: x => x.CloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrintedHtmlFormFiles_PrintedHtmlForms_PrintedHtmlFormId",
                        column: x => x.PrintedHtmlFormId,
                        principalTable: "PrintedHtmlForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: false),
                    IsDefault = table.Column<bool>(type: "boolean", nullable: false),
                    AgreementId = table.Column<Guid>(type: "uuid", nullable: false),
                    PrintedHtmlFormInvoiceId = table.Column<Guid>(type: "uuid", nullable: true),
                    PrintedHtmlFormInvoiceReceiptId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Suppliers_CloudFiles_AgreementId",
                        column: x => x.AgreementId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Suppliers_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Suppliers_PrintedHtmlForms_PrintedHtmlFormInvoiceId",
                        column: x => x.PrintedHtmlFormInvoiceId,
                        principalTable: "PrintedHtmlForms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Suppliers_PrintedHtmlForms_PrintedHtmlFormInvoiceReceiptId",
                        column: x => x.PrintedHtmlFormInvoiceReceiptId,
                        principalTable: "PrintedHtmlForms",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ActionFlows",
                schema: "statemachine",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ProcessFlowId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    InputDataInJson = table.Column<string>(type: "text", nullable: true),
                    SnapshotRollbackDataInJson = table.Column<string>(type: "text", nullable: true),
                    ResultDataInJson = table.Column<string>(type: "text", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    CreateDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CountOfAttempts = table.Column<int>(type: "integer", nullable: false),
                    StateRefKey = table.Column<string>(type: "text", nullable: true),
                    ErrorMessage = table.Column<string>(type: "text", nullable: true),
                    ErrorStackTrace = table.Column<string>(type: "text", nullable: true),
                    RollbackErrorMessage = table.Column<string>(type: "text", nullable: true),
                    RollbackErrorStackTrace = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionFlows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActionFlows_ProcessFlows_ProcessFlowId",
                        column: x => x.ProcessFlowId,
                        principalSchema: "statemachine",
                        principalTable: "ProcessFlows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesContentServerNodes",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    ContentServerId = table.Column<Guid>(type: "uuid", nullable: false),
                    NodeReferenceId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesContentServerNodes", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CloudServicesContentServerNodes_CloudServicesContentServer_~",
                        column: x => x.ContentServerId,
                        principalTable: "CloudServicesContentServer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesContentServerNodes_PublishNodesReferences_Node~",
                        column: x => x.NodeReferenceId,
                        principalTable: "PublishNodesReferences",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountSaleManagers",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    SaleManagerId = table.Column<Guid>(type: "uuid", nullable: false),
                    Division = table.Column<string>(type: "text", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountSaleManagers", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_AccountSaleManagers_AccountUsers_SaleManagerId",
                        column: x => x.SaleManagerId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountSaleManagers_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountUserJsonWebToken",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccessToken = table.Column<byte[]>(type: "bytea", nullable: false),
                    RefreshToken = table.Column<byte[]>(type: "bytea", maxLength: 120, nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    HasBeenTakenRefreshToken = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserJsonWebToken", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUserJsonWebToken_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountUserNotificationSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    NotificationSettingsId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    ChatId = table.Column<long>(type: "bigint", nullable: true),
                    Code = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserNotificationSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUserNotificationSettings_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountUserNotificationSettings_NotificationSettings_Notifi~",
                        column: x => x.NotificationSettingsId,
                        principalTable: "NotificationSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountUserRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserGroup = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUserRoles_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountUserSessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Token = table.Column<Guid>(type: "uuid", nullable: false),
                    StaticToken = table.Column<bool>(type: "boolean", nullable: true),
                    ClientDescription = table.Column<string>(type: "text", nullable: true),
                    ClientDeviceInfo = table.Column<string>(type: "text", nullable: true),
                    ClientIPAddress = table.Column<string>(type: "text", nullable: true),
                    TokenCreationTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserSessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUserSessions_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentTransferBalanseRequests",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false),
                    FromAccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    ToAccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    InitiatorId = table.Column<Guid>(type: "uuid", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentTransferBalanseRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentTransferBalanseRequests_AccountUsers_InitiatorId",
                        column: x => x.InitiatorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentTransferBalanseRequests_Accounts_FromAccountId",
                        column: x => x.FromAccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentTransferBalanseRequests_Accounts_ToAccountId",
                        column: x => x.ToAccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Topic = table.Column<string>(type: "text", nullable: true),
                    TopicId = table.Column<string>(type: "text", nullable: true),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Text = table.Column<string>(type: "text", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    PublicationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    GoogleCloudLink = table.Column<string>(type: "text", nullable: true),
                    GoogleCloudDocumentId = table.Column<string>(type: "text", nullable: true),
                    WpLink = table.Column<string>(type: "text", nullable: true),
                    WpId = table.Column<string>(type: "text", nullable: true),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    RegistrationCount = table.Column<int>(type: "integer", nullable: true),
                    Reason = table.Column<string>(type: "text", nullable: true),
                    RewardAmount = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArticleWallets",
                schema: "billing",
                columns: table => new
                {
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    AvailableSum = table.Column<decimal>(type: "numeric", nullable: false),
                    TotalEarned = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleWallets", x => x.AccountUserId);
                    table.ForeignKey(
                        name: "FK_ArticleWallets_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudServices",
                columns: table => new
                {
                    CloudServiceId = table.Column<string>(type: "text", nullable: false),
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceCaption = table.Column<string>(type: "text", nullable: true),
                    ServiceToken = table.Column<Guid>(type: "uuid", nullable: true),
                    JsonWebToken = table.Column<byte[]>(type: "bytea", nullable: true),
                    RefreshToken = table.Column<string>(type: "text", nullable: true),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServices", x => x.CloudServiceId);
                    table.ForeignKey(
                        name: "FK_CloudServices_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Connections",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    ConnectionId = table.Column<string>(type: "text", nullable: true),
                    UserAgent = table.Column<string>(type: "text", nullable: true),
                    Connected = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Connections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Connections_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailSmsVerifications",
                schema: "security",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Code = table.Column<string>(type: "text", nullable: true),
                    ExpiresOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSmsVerifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailSmsVerifications_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Link42Configurations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: true),
                    IsCurrentVersion = table.Column<bool>(type: "boolean", nullable: false),
                    DownloadsLimit = table.Column<int>(type: "integer", nullable: false),
                    DownloadsCount = table.Column<int>(type: "integer", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Link42Configurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Link42Configurations_AccountUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Link42Configurations_AccountUsers_UpdatedBy",
                        column: x => x.UpdatedBy,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MigrationHistories",
                schema: "migration",
                columns: table => new
                {
                    HistoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    StartDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    FinishDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Status = table.Column<byte>(type: "smallint", nullable: false),
                    InitiatorId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MigrationHistories", x => x.HistoryId);
                    table.ForeignKey(
                        name: "FK_MigrationHistories_AccountUsers_InitiatorId",
                        column: x => x.InitiatorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: true),
                    IsRead = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    IsSent = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    State = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceAccounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserInitiatorId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceAccounts_AccountUsers_AccountUserInitiatorId",
                        column: x => x.AccountUserInitiatorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceAccounts_Accounts_Id",
                        column: x => x.Id,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebSocketConnections",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    WebSocketId = table.Column<string>(type: "text", nullable: true),
                    ConnectionOpenDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ConnectionCloseeDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebSocketConnections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebSocketConnections_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LimitOnFreeCreationDbForAccounts",
                schema: "billing",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    LimitedQuantity = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LimitOnFreeCreationDbForAccounts", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_LimitOnFreeCreationDbForAccounts_BillingAccounts_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "billing",
                        principalTable: "BillingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountAutoPayConfigurations",
                schema: "billing",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    SavedPaymentMethodId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountAutoPayConfigurations", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_AccountAutoPayConfigurations_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountAutoPayConfigurations_SavedPaymentMethods_SavedPayme~",
                        column: x => x.SavedPaymentMethodId,
                        principalSchema: "billing",
                        principalTable: "SavedPaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SavedPaymentMethodBankCards",
                schema: "billing",
                columns: table => new
                {
                    SavedPaymentMethodId = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstSixDigits = table.Column<string>(type: "text", nullable: false),
                    LastFourDigits = table.Column<string>(type: "text", nullable: false),
                    ExpiryMonth = table.Column<string>(type: "text", nullable: false),
                    ExpiryYear = table.Column<string>(type: "text", nullable: false),
                    CardType = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedPaymentMethodBankCards", x => x.SavedPaymentMethodId);
                    table.ForeignKey(
                        name: "FK_SavedPaymentMethodBankCards_SavedPaymentMethods_SavedPaymen~",
                        column: x => x.SavedPaymentMethodId,
                        principalSchema: "billing",
                        principalTable: "SavedPaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudChanges",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Initiator = table.Column<Guid>(type: "uuid", nullable: true),
                    Action = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudChanges_AccountUsers_Initiator",
                        column: x => x.Initiator,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CloudChanges_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CloudChanges_CloudChangesActions_Action",
                        column: x => x.Action,
                        principalTable: "CloudChangesActions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IndustryDependencyBillingServiceChanges",
                schema: "billingCommits",
                columns: table => new
                {
                    IndustryId = table.Column<Guid>(type: "uuid", nullable: false),
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceChangesId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndustryDependencyBillingServiceChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IndustryDependencyBillingServiceChanges_BillingServiceChang~",
                        column: x => x.BillingServiceChangesId,
                        principalSchema: "billingCommits",
                        principalTable: "BillingServiceChanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IndustryDependencyBillingServiceChanges_Industries_Industry~",
                        column: x => x.IndustryId,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingService1CFiles",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FileName = table.Column<string>(type: "text", nullable: false),
                    OriginalName = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Synonym = table.Column<string>(type: "text", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Service1CFileVersionId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingService1CFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingService1CFiles_Service1CFileVersion_Service1CFileVer~",
                        column: x => x.Service1CFileVersionId,
                        principalSchema: "billing",
                        principalTable: "Service1CFileVersion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillingService1CFiles_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ChangeServiceRequests",
                schema: "billingCommits",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    BillingServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    StatusDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    ModeratorId = table.Column<Guid>(type: "uuid", nullable: true),
                    ModeratorComment = table.Column<string>(type: "text", nullable: true),
                    ChangeRequestType = table.Column<int>(type: "integer", nullable: false),
                    BillingServiceChangesId = table.Column<Guid>(type: "uuid", nullable: true),
                    Discriminator = table.Column<string>(type: "character varying(21)", maxLength: 21, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChangeServiceRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChangeServiceRequests_AccountUsers_ModeratorId",
                        column: x => x.ModeratorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ChangeServiceRequests_BillingServiceChanges_BillingServiceC~",
                        column: x => x.BillingServiceChangesId,
                        principalSchema: "billingCommits",
                        principalTable: "BillingServiceChanges",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ChangeServiceRequests_Services_BillingServiceId",
                        column: x => x.BillingServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IndustriesDependencyBillingServices",
                schema: "billing",
                columns: table => new
                {
                    BillingServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    IndustryId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndustriesDependencyBillingServices", x => new { x.BillingServiceId, x.IndustryId });
                    table.ForeignKey(
                        name: "FK_IndustriesDependencyBillingServices_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IndustriesDependencyBillingServices_Services_BillingService~",
                        column: x => x.BillingServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false),
                    Remains = table.Column<decimal>(type: "numeric", nullable: true),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    OperationType = table.Column<string>(type: "text", nullable: false),
                    TransactionType = table.Column<int>(type: "integer", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: false),
                    PaymentSystem = table.Column<string>(type: "text", nullable: false),
                    OriginDetails = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    ExchangeDataId = table.Column<Guid>(type: "uuid", nullable: true),
                    BillingServiceId = table.Column<Guid>(type: "uuid", nullable: true),
                    ExternalPaymentNumber = table.Column<int>(type: "integer", nullable: true),
                    Account = table.Column<Guid>(type: "uuid", nullable: false),
                    PaymentSystemTransactionId = table.Column<string>(type: "text", nullable: true),
                    ConfirmationToken = table.Column<string>(type: "text", nullable: true),
                    ConfirmationType = table.Column<string>(type: "text", nullable: true),
                    ConfirmationRedirectUrl = table.Column<string>(type: "text", nullable: true),
                    IsAutoPay = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Payments_Accounts_Account",
                        column: x => x.Account,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payments_Services_BillingServiceId",
                        column: x => x.BillingServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ResourcesConfigurations",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Cost = table.Column<decimal>(type: "numeric", nullable: false),
                    CostIsFixed = table.Column<bool>(type: "boolean", nullable: true),
                    ExpireDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Frozen = table.Column<bool>(type: "boolean", nullable: true),
                    DiscountGroup = table.Column<int>(type: "integer", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IsDemoPeriod = table.Column<bool>(type: "boolean", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsAutoSubscriptionEnabled = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourcesConfigurations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResourcesConfigurations_BillingAccounts_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "billing",
                        principalTable: "BillingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ResourcesConfigurations_Services_BillingServiceId",
                        column: x => x.BillingServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceTypes",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    BillingType = table.Column<int>(type: "integer", nullable: false),
                    SystemServiceType = table.Column<int>(type: "integer", nullable: true),
                    DependServiceTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceTypes_ServiceTypes_DependServiceTypeId",
                        column: x => x.DependServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ServiceTypes_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkerTaskParameters",
                columns: table => new
                {
                    TaskId = table.Column<Guid>(type: "uuid", nullable: false),
                    TaskParams = table.Column<string>(type: "text", nullable: true),
                    SynchronizationKey = table.Column<string>(type: "text", nullable: true),
                    DependTaskId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkerTaskParameters", x => x.TaskId);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTaskParameters_CoreWorkerTasksQueue_DependTaskId",
                        column: x => x.DependTaskId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CoreWorkerTaskParameters_CoreWorkerTasksQueue_TaskId",
                        column: x => x.TaskId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CoreWorkerTasksQueueLive",
                columns: table => new
                {
                    CoreWorkerTasksQueueId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateTimeDelayOperation = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    CoreWorkerTaskId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    StartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    EditDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    TaskParams = table.Column<string>(type: "text", nullable: true),
                    SynchronizationKey = table.Column<string>(type: "text", nullable: true),
                    DependTaskId = table.Column<Guid>(type: "uuid", nullable: true),
                    CapturedWorkerId = table.Column<short>(type: "smallint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoreWorkerTasksQueueLive", x => x.CoreWorkerTasksQueueId);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueueLive_CoreWorkerTask_CoreWorkerTaskId",
                        column: x => x.CoreWorkerTaskId,
                        principalTable: "CoreWorkerTask",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueueLive_CoreWorkerTasksQueue_CoreWorkerTas~",
                        column: x => x.CoreWorkerTasksQueueId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueueLive_CoreWorkerTasksQueue_DependTaskId",
                        column: x => x.DependTaskId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CoreWorkerTasksQueueLive_CoreWorkers_CapturedWorkerId",
                        column: x => x.CapturedWorkerId,
                        principalTable: "CoreWorkers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "RecalculationServiceCostAfterChanges",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceChangesId = table.Column<Guid>(type: "uuid", nullable: false),
                    CoreWorkerTasksQueueId = table.Column<Guid>(type: "uuid", nullable: false),
                    OldServiceCost = table.Column<decimal>(type: "numeric", nullable: false),
                    NewServiceCost = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecalculationServiceCostAfterChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RecalculationServiceCostAfterChanges_BillingServiceChanges_~",
                        column: x => x.BillingServiceChangesId,
                        principalSchema: "billingCommits",
                        principalTable: "BillingServiceChanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RecalculationServiceCostAfterChanges_CoreWorkerTasksQueue_C~",
                        column: x => x.CoreWorkerTasksQueueId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LetterAdvertisingBannerRelation",
                schema: "mailing",
                columns: table => new
                {
                    LetterTemplateId = table.Column<int>(type: "integer", nullable: false),
                    AdvertisingBannerTemplateId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LetterAdvertisingBannerRelation", x => x.LetterTemplateId);
                    table.ForeignKey(
                        name: "FK_LetterAdvertisingBannerRelation_AdvertisingBannerTemplate_A~",
                        column: x => x.AdvertisingBannerTemplateId,
                        principalSchema: "marketing",
                        principalTable: "AdvertisingBannerTemplate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LetterAdvertisingBannerRelation_LetterTemplate_LetterTempla~",
                        column: x => x.LetterTemplateId,
                        principalSchema: "mailing",
                        principalTable: "LetterTemplate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ConfigurationNameVariations",
                schema: "its",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    VariationName = table.Column<string>(type: "text", nullable: false),
                    Configuration1CName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurationNameVariations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConfigurationNameVariations_Configurations1C_Configuration1~",
                        column: x => x.Configuration1CName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConfigurationsCfu",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Version = table.Column<string>(type: "text", nullable: false),
                    Platform1CVersion = table.Column<string>(type: "text", nullable: true),
                    DownloadDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ValidateState = table.Column<bool>(type: "boolean", nullable: true),
                    FilePath = table.Column<string>(type: "text", nullable: false),
                    InitiatorId = table.Column<Guid>(type: "uuid", nullable: true),
                    ConfigurationName = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurationsCfu", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConfigurationsCfu_AccountUsers_InitiatorId",
                        column: x => x.InitiatorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ConfigurationsCfu_Configurations1C_ConfigurationName",
                        column: x => x.ConfigurationName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabases",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    LastActivityDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Caption = table.Column<string>(type: "text", nullable: true),
                    State = table.Column<string>(type: "text", nullable: false),
                    StateDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DbNumber = table.Column<int>(type: "integer", nullable: false),
                    ServiceName = table.Column<string>(type: "text", nullable: true),
                    SqlName = table.Column<string>(type: "text", nullable: true),
                    V82Name = table.Column<string>(type: "text", nullable: true),
                    V82Server = table.Column<string>(type: "text", nullable: true),
                    SizeInMB = table.Column<int>(type: "integer", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    LockedState = table.Column<string>(type: "text", nullable: true),
                    IsFile = table.Column<bool>(type: "boolean", nullable: true),
                    ArchivePath = table.Column<string>(type: "text", nullable: true),
                    PublishState = table.Column<string>(type: "text", nullable: false),
                    Platform = table.Column<string>(type: "text", nullable: true),
                    DistributionType = table.Column<string>(type: "text", nullable: false),
                    LaunchType = table.Column<int>(type: "integer", nullable: true),
                    CalculateSizeDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    FileStorageID = table.Column<Guid>(type: "uuid", nullable: true),
                    LaunchParameters = table.Column<string>(type: "text", nullable: true),
                    UsedWebServices = table.Column<bool>(type: "boolean", nullable: true),
                    CloudStorageWebLink = table.Column<string>(type: "text", nullable: true),
                    LastEditedDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreateAccountDatabaseComment = table.Column<string>(type: "text", nullable: true),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: true),
                    OwnerId = table.Column<Guid>(type: "uuid", nullable: false),
                    RestoreModelType = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDatabases_Accounts_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountDatabases_CloudServicesFileStorageServers_FileStorag~",
                        column: x => x.FileStorageID,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_AccountDatabases_DbTemplates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "DbTemplates",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ConfigurationDbTemplateRelations",
                columns: table => new
                {
                    DbTemplateId = table.Column<Guid>(type: "uuid", nullable: false),
                    Configuration1CName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurationDbTemplateRelations", x => x.DbTemplateId);
                    table.ForeignKey(
                        name: "FK_ConfigurationDbTemplateRelations_Configurations1C_Configura~",
                        column: x => x.Configuration1CName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name");
                    table.ForeignKey(
                        name: "FK_ConfigurationDbTemplateRelations_DbTemplates_DbTemplateId",
                        column: x => x.DbTemplateId,
                        principalTable: "DbTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DbTemplateDelimiters",
                columns: table => new
                {
                    ConfigurationId = table.Column<string>(type: "text", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    ShortName = table.Column<string>(type: "text", nullable: true),
                    DemoDatabaseOnDelimitersPublicationAddress = table.Column<string>(type: "text", nullable: true),
                    ConfigurationReleaseVersion = table.Column<string>(type: "text", nullable: false),
                    MinReleaseVersion = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTemplateDelimiters", x => x.ConfigurationId);
                    table.ForeignKey(
                        name: "FK_DbTemplateDelimiters_DbTemplates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "DbTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DbTemplateUpdates",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: false),
                    ResponsibleAccountUserId = table.Column<Guid>(type: "uuid", nullable: true),
                    UpdateVersion = table.Column<string>(type: "text", nullable: true),
                    UpdateVersionDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ValidateState = table.Column<int>(type: "integer", nullable: false),
                    UpdateTemplatePath = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTemplateUpdates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbTemplateUpdates_AccountUsers_ResponsibleAccountUserId",
                        column: x => x.ResponsibleAccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DbTemplateUpdates_DbTemplates_TemplateId",
                        column: x => x.TemplateId,
                        principalTable: "DbTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AvailableMigration",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SegmentIdFrom = table.Column<Guid>(type: "uuid", nullable: false),
                    SegmentIdTo = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvailableMigration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AvailableMigration_CloudServicesSegment_SegmentIdFrom",
                        column: x => x.SegmentIdFrom,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AvailableMigration_CloudServicesSegment_SegmentIdTo",
                        column: x => x.SegmentIdTo,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesSegmentStorage",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDefault = table.Column<bool>(type: "boolean", nullable: false),
                    FileStorageID = table.Column<Guid>(type: "uuid", nullable: false),
                    SegmentID = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesSegmentStorage", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegmentStorage_CloudServicesFileStorageServers~",
                        column: x => x.FileStorageID,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegmentStorage_CloudServicesSegment_SegmentID",
                        column: x => x.SegmentID,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudServicesSegmentTerminalServers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SegmentId = table.Column<Guid>(type: "uuid", nullable: false),
                    TerminalServerId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudServicesSegmentTerminalServers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegmentTerminalServers_CloudServicesSegment_Se~",
                        column: x => x.SegmentId,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CloudServicesSegmentTerminalServers_CloudTerminalServers_Te~",
                        column: x => x.TerminalServerId,
                        principalTable: "CloudTerminalServers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LocaleConfigurations",
                schema: "configuration",
                columns: table => new
                {
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: false),
                    CpSiteUrl = table.Column<string>(type: "text", nullable: true),
                    DefaultSegmentId = table.Column<Guid>(type: "uuid", nullable: false),
                    ChangeDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocaleConfigurations", x => x.LocaleId);
                    table.ForeignKey(
                        name: "FK_LocaleConfigurations_CloudServicesSegment_DefaultSegmentId",
                        column: x => x.DefaultSegmentId,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LocaleConfigurations_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountConfigurations",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    SupplierId = table.Column<Guid>(type: "uuid", nullable: false),
                    SegmentId = table.Column<Guid>(type: "uuid", nullable: false),
                    FileStorageId = table.Column<Guid>(type: "uuid", nullable: false),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: false),
                    Type = table.Column<string>(type: "text", nullable: true),
                    IsVip = table.Column<bool>(type: "boolean", nullable: false),
                    CreateClusterDatabase = table.Column<bool>(type: "boolean", nullable: false),
                    CloudStorageWebLink = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountConfigurations", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_AccountConfigurations_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountConfigurations_CloudServicesFileStorageServers_FileS~",
                        column: x => x.FileStorageId,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountConfigurations_CloudServicesSegment_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountConfigurations_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountConfigurations_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudConfiguration",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Key = table.Column<string>(type: "text", nullable: false),
                    ContextSupplierId = table.Column<Guid>(type: "uuid", nullable: true),
                    Value = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudConfiguration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudConfiguration_Suppliers_ContextSupplierId",
                        column: x => x.ContextSupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SupplierReferralAccounts",
                columns: table => new
                {
                    SupplierId = table.Column<Guid>(type: "uuid", nullable: false),
                    ReferralAccountId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupplierReferralAccounts", x => new { x.SupplierId, x.ReferralAccountId });
                    table.ForeignKey(
                        name: "FK_SupplierReferralAccounts_Accounts_ReferralAccountId",
                        column: x => x.ReferralAccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SupplierReferralAccounts_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArticleTransaction",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ArticleId = table.Column<int>(type: "integer", nullable: false),
                    Cause = table.Column<int>(type: "integer", nullable: false),
                    TransactionType = table.Column<int>(type: "integer", nullable: false),
                    Amount = table.Column<decimal>(type: "numeric", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleTransaction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ArticleTransaction_Articles_ArticleId",
                        column: x => x.ArticleId,
                        principalTable: "Articles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CSResources",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CloudServiceId = table.Column<string>(type: "text", nullable: true),
                    ResourcesName = table.Column<string>(type: "text", nullable: true),
                    DaysAutoDecrease = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CSResources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CSResources_CloudServices_CloudServiceId",
                        column: x => x.CloudServiceId,
                        principalTable: "CloudServices",
                        principalColumn: "CloudServiceId");
                });

            migrationBuilder.CreateTable(
                name: "Link42ConfigurationBitDepths",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SystemType = table.Column<int>(type: "integer", nullable: false),
                    LinkAppType = table.Column<int>(type: "integer", nullable: false),
                    BitDepth = table.Column<int>(type: "integer", nullable: false),
                    DownloadLink = table.Column<string>(type: "text", nullable: true),
                    MinSupportedMajorVersion = table.Column<int>(type: "integer", nullable: false),
                    MinSupportedMinorVersion = table.Column<int>(type: "integer", nullable: false),
                    ConfigurationId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Link42ConfigurationBitDepths", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Link42ConfigurationBitDepths_Link42Configurations_Configura~",
                        column: x => x.ConfigurationId,
                        principalTable: "Link42Configurations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MigrationAccountHistories",
                schema: "migration",
                columns: table => new
                {
                    HistoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    TargetSegmentId = table.Column<Guid>(type: "uuid", nullable: false),
                    SourceSegmentId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MigrationAccountHistories", x => x.HistoryId);
                    table.ForeignKey(
                        name: "FK_MigrationAccountHistories_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MigrationAccountHistories_CloudServicesSegment_SourceSegmen~",
                        column: x => x.SourceSegmentId,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MigrationAccountHistories_CloudServicesSegment_TargetSegmen~",
                        column: x => x.TargetSegmentId,
                        principalTable: "CloudServicesSegment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MigrationAccountHistories_MigrationHistories_HistoryId",
                        column: x => x.HistoryId,
                        principalSchema: "migration",
                        principalTable: "MigrationHistories",
                        principalColumn: "HistoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceManager1CFileRelation",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BillingService1CFileId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceManagerUploadedFileId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceManager1CFileRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceManager1CFileRelation_BillingService1CFiles_BillingS~",
                        column: x => x.BillingService1CFileId,
                        principalSchema: "billing",
                        principalTable: "BillingService1CFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentPayments",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountOwnerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClientPaymentId = table.Column<Guid>(type: "uuid", nullable: true),
                    AgentPaymentRelationId = table.Column<Guid>(type: "uuid", nullable: true),
                    PaymentType = table.Column<int>(type: "integer", nullable: false),
                    AgentPaymentSourceType = table.Column<int>(type: "integer", nullable: false),
                    PaymentDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    PaymentEntryDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentPayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentPayments_Accounts_AccountOwnerId",
                        column: x => x.AccountOwnerId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentPayments_AgentPaymentRelations_AgentPaymentRelationId",
                        column: x => x.AgentPaymentRelationId,
                        principalSchema: "billing",
                        principalTable: "AgentPaymentRelations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AgentPayments_Payments_ClientPaymentId",
                        column: x => x.ClientPaymentId,
                        principalSchema: "billing",
                        principalTable: "Payments",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Invoices",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Sum = table.Column<decimal>(type: "numeric", nullable: false),
                    BonusReward = table.Column<decimal>(type: "numeric", nullable: true),
                    Requisite = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    State = table.Column<string>(type: "text", nullable: true),
                    Uniq = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ActID = table.Column<Guid>(type: "uuid", nullable: true),
                    RequiredSignature = table.Column<bool>(type: "boolean", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    ActDescription = table.Column<string>(type: "text", nullable: true),
                    Period = table.Column<int>(type: "integer", nullable: true),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountAdditionalCompanyId = table.Column<Guid>(type: "uuid", nullable: true),
                    PaymentID = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invoices_AccountAdditionalCompanies_AccountAdditionalCompan~",
                        column: x => x.AccountAdditionalCompanyId,
                        principalTable: "AccountAdditionalCompanies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Invoices_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Invoices_Payments_PaymentID",
                        column: x => x.PaymentID,
                        principalSchema: "billing",
                        principalTable: "Payments",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AccountRates",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    Cost = table.Column<decimal>(type: "numeric", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountRates_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountRates_ServiceTypes_BillingServiceTypeId",
                        column: x => x.BillingServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingServiceTypeActivityForAccounts",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BillingServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    AvailabilityDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingServiceTypeActivityForAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeActivityForAccounts_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeActivityForAccounts_ServiceTypes_BillingS~",
                        column: x => x.BillingServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingServiceTypeChanges",
                schema: "billingCommits",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceChangesId = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceTypeId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsNew = table.Column<bool>(type: "boolean", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Cost = table.Column<decimal>(type: "numeric", nullable: true),
                    BillingType = table.Column<int>(type: "integer", nullable: true),
                    DependServiceTypeId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingServiceTypeChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeChanges_BillingServiceChanges_BillingServ~",
                        column: x => x.BillingServiceChangesId,
                        principalSchema: "billingCommits",
                        principalTable: "BillingServiceChanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeChanges_ServiceTypes_BillingServiceTypeId",
                        column: x => x.BillingServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeChanges_ServiceTypes_DependServiceTypeId",
                        column: x => x.DependServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ConfigurationServiceTypeRelations",
                schema: "billing",
                columns: table => new
                {
                    Configuration1CName = table.Column<string>(type: "text", nullable: false),
                    ServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfigurationServiceTypeRelations", x => x.Configuration1CName);
                    table.ForeignKey(
                        name: "FK_ConfigurationServiceTypeRelations_Configurations1C_Configur~",
                        column: x => x.Configuration1CName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ConfigurationServiceTypeRelations_ServiceTypes_ServiceTypeId",
                        column: x => x.ServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rates",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RatePeriod = table.Column<string>(type: "text", nullable: true),
                    Cost = table.Column<decimal>(type: "numeric", nullable: false),
                    AccountType = table.Column<string>(type: "text", nullable: true),
                    DiscountGroup = table.Column<int>(type: "integer", nullable: true),
                    DemoPeriod = table.Column<string>(type: "text", nullable: true),
                    Remark = table.Column<string>(type: "text", nullable: true),
                    UpperBound = table.Column<int>(type: "integer", nullable: true),
                    LowerBound = table.Column<int>(type: "integer", nullable: true),
                    LocaleId = table.Column<Guid>(type: "uuid", nullable: true),
                    BillingServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rates_Locale_LocaleId",
                        column: x => x.LocaleId,
                        principalTable: "Locale",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_Rates_ServiceTypes_BillingServiceTypeId",
                        column: x => x.BillingServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceTypeRelations",
                schema: "billing",
                columns: table => new
                {
                    MainServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    ChildServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceTypeRelations", x => new { x.MainServiceTypeId, x.ChildServiceTypeId });
                    table.ForeignKey(
                        name: "FK_ServiceTypeRelations_ServiceTypes_ChildServiceTypeId",
                        column: x => x.ChildServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceTypeRelations_ServiceTypes_MainServiceTypeId",
                        column: x => x.MainServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabaseUpdateVersionMappings",
                columns: table => new
                {
                    ConfigurationName = table.Column<string>(type: "text", nullable: false),
                    CfuId = table.Column<Guid>(type: "uuid", nullable: false),
                    TargetVersion = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabaseUpdateVersionMappings", x => new { x.ConfigurationName, x.CfuId, x.TargetVersion });
                    table.ForeignKey(
                        name: "FK_AccountDatabaseUpdateVersionMappings_Configurations1C_Confi~",
                        column: x => x.ConfigurationName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseUpdateVersionMappings_ConfigurationsCfu_CfuId",
                        column: x => x.CfuId,
                        principalTable: "ConfigurationsCfu",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabaseAutoUpdateResults",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    CoreWorkerTasksQueueId = table.Column<Guid>(type: "uuid", nullable: false),
                    DebugInformation = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabaseAutoUpdateResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseAutoUpdateResults_AccountDatabases_AccountDa~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseAutoUpdateResults_CoreWorkerTasksQueue_CoreW~",
                        column: x => x.CoreWorkerTasksQueueId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabaseBackups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    CreateDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreationBackupDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    BackupPath = table.Column<string>(type: "text", nullable: false),
                    FilePath = table.Column<string>(type: "text", nullable: true),
                    EventTrigger = table.Column<int>(type: "integer", nullable: false),
                    SourceType = table.Column<int>(type: "integer", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    InitiatorId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabaseBackups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseBackups_AccountDatabases_AccountDatabaseId",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseBackups_AccountUsers_InitiatorId",
                        column: x => x.InitiatorId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AcDbAccesses",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    LocalUserID = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseID = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountID = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserID = table.Column<Guid>(type: "uuid", nullable: true),
                    IsLock = table.Column<bool>(type: "boolean", nullable: false),
                    LaunchType = table.Column<int>(type: "integer", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcDbAccesses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AcDbAccesses_AccountDatabases_AccountDatabaseID",
                        column: x => x.AccountDatabaseID,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AcDbAccesses_AccountUsers_AccountUserID",
                        column: x => x.AccountUserID,
                        principalTable: "AccountUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AcDbAccesses_Accounts_AccountID",
                        column: x => x.AccountID,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AcDbSupport",
                columns: table => new
                {
                    AccountDatabasesID = table.Column<Guid>(type: "uuid", nullable: false),
                    Login = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    State = table.Column<int>(type: "integer", nullable: false),
                    PrepearedForUpdate = table.Column<bool>(type: "boolean", nullable: false),
                    HasSupport = table.Column<bool>(type: "boolean", nullable: false),
                    HasAutoUpdate = table.Column<bool>(type: "boolean", nullable: false),
                    OldVersion = table.Column<string>(type: "text", nullable: true),
                    CurrentVersion = table.Column<string>(type: "text", nullable: true),
                    UpdateVersion = table.Column<string>(type: "text", nullable: true),
                    PreparedForTehSupport = table.Column<bool>(type: "boolean", nullable: false),
                    ConfigurationName = table.Column<string>(type: "text", nullable: true),
                    TehSupportDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    UpdateVersionDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    SynonymConfiguration = table.Column<string>(type: "text", nullable: true),
                    DatabaseHasModifications = table.Column<bool>(type: "boolean", nullable: false),
                    ConnectDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    TimeOfUpdate = table.Column<int>(type: "integer", nullable: false),
                    CompleteSessioin = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcDbSupport", x => x.AccountDatabasesID);
                    table.ForeignKey(
                        name: "FK_AcDbSupport_AccountDatabases_AccountDatabasesID",
                        column: x => x.AccountDatabasesID,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AcDbSupport_Configurations1C_ConfigurationName",
                        column: x => x.ConfigurationName,
                        principalTable: "Configurations1C",
                        principalColumn: "Name");
                });

            migrationBuilder.CreateTable(
                name: "MigrationAccountDatabaseHistories",
                schema: "migration",
                columns: table => new
                {
                    HistoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    SourceFileStorageId = table.Column<Guid>(type: "uuid", nullable: true),
                    TargetFileStorageId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MigrationAccountDatabaseHistories", x => x.HistoryId);
                    table.ForeignKey(
                        name: "FK_MigrationAccountDatabaseHistories_AccountDatabases_AccountD~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MigrationAccountDatabaseHistories_CloudServicesFileStorageS~",
                        column: x => x.SourceFileStorageId,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_MigrationAccountDatabaseHistories_CloudServicesFileStorage~1",
                        column: x => x.TargetFileStorageId,
                        principalTable: "CloudServicesFileStorageServers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MigrationAccountDatabaseHistories_MigrationHistories_Histor~",
                        column: x => x.HistoryId,
                        principalSchema: "migration",
                        principalTable: "MigrationHistories",
                        principalColumn: "HistoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceExtensionDatabases",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceExtensionDatabaseStatus = table.Column<int>(type: "integer", nullable: false),
                    StateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ErrorMessage = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceExtensionDatabases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceExtensionDatabases_AccountDatabases_AccountDatabaseId",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceExtensionDatabases_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalSchema: "billing",
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TerminationSessionsInDatabases",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    TerminateSessionsInDbStatus = table.Column<int>(type: "integer", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TerminatedSessionsDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TerminationSessionsInDatabases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TerminationSessionsInDatabases_AccountDatabases_AccountData~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpdateNodeQueues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AddDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpdateNodeQueues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpdateNodeQueues_AccountDatabases_AccountDatabaseId",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabaseOnDelimiters",
                columns: table => new
                {
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    DatabaseSourceId = table.Column<Guid>(type: "uuid", nullable: true),
                    DbTemplateDelimiterCode = table.Column<string>(type: "text", nullable: false),
                    Zone = table.Column<int>(type: "integer", nullable: true),
                    IsDemo = table.Column<bool>(type: "boolean", nullable: false),
                    UploadedFileId = table.Column<Guid>(type: "uuid", nullable: true),
                    LoadType = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabaseOnDelimiters", x => x.AccountDatabaseId);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_AccountDatabas~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_DatabaseSource~",
                        column: x => x.DatabaseSourceId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountDatabaseOnDelimiters_DbTemplateDelimiters_DbTemplate~",
                        column: x => x.DbTemplateDelimiterCode,
                        principalTable: "DbTemplateDelimiters",
                        principalColumn: "ConfigurationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseOnDelimiters_UploadedFiles_UploadedFileId",
                        column: x => x.UploadedFileId,
                        principalTable: "UploadedFiles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DelimiterSourceAccountDatabases",
                columns: table => new
                {
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    DbTemplateDelimiterCode = table.Column<string>(type: "text", nullable: false),
                    DatabaseOnDelimitersPublicationAddress = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelimiterSourceAccountDatabases", x => new { x.AccountDatabaseId, x.DbTemplateDelimiterCode });
                    table.ForeignKey(
                        name: "FK_DelimiterSourceAccountDatabases_AccountDatabases_AccountDat~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DelimiterSourceAccountDatabases_DbTemplateDelimiters_DbTemp~",
                        column: x => x.DbTemplateDelimiterCode,
                        principalTable: "DbTemplateDelimiters",
                        principalColumn: "ConfigurationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Service1CFileDbTemplateRelation",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BillingService1CFileId = table.Column<Guid>(type: "uuid", nullable: false),
                    DbTemplateDelimitersId = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service1CFileDbTemplateRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Service1CFileDbTemplateRelation_BillingService1CFiles_Billi~",
                        column: x => x.BillingService1CFileId,
                        principalSchema: "billing",
                        principalTable: "BillingService1CFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Service1CFileDbTemplateRelation_DbTemplateDelimiters_DbTemp~",
                        column: x => x.DbTemplateDelimitersId,
                        principalTable: "DbTemplateDelimiters",
                        principalColumn: "ConfigurationId");
                });

            migrationBuilder.CreateTable(
                name: "AccountCSResourceValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    InitiatorCloudService = table.Column<Guid>(type: "uuid", nullable: false),
                    ModifyResourceValue = table.Column<int>(type: "integer", nullable: false),
                    ModifyResourceDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ModifyResourceComment = table.Column<string>(type: "text", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    CSResourceId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountCSResourceValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountCSResourceValues_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountCSResourceValues_CSResources_CSResourceId",
                        column: x => x.CSResourceId,
                        principalTable: "CSResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FlowResourcesScope",
                schema: "billing",
                columns: table => new
                {
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    FlowResourceId = table.Column<Guid>(type: "uuid", nullable: false),
                    ScopeValue = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlowResourcesScope", x => new { x.AccountId, x.FlowResourceId });
                    table.ForeignKey(
                        name: "FK_FlowResourcesScope_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FlowResourcesScope_CSResources_FlowResourceId",
                        column: x => x.FlowResourceId,
                        principalTable: "CSResources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceProducts",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceName = table.Column<string>(type: "text", nullable: false),
                    ServiceSum = table.Column<decimal>(type: "numeric", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    Count = table.Column<int>(type: "integer", nullable: false),
                    InvoiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceTypeId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InvoiceProducts_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalSchema: "billing",
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InvoiceProducts_ServiceTypes_ServiceTypeId",
                        column: x => x.ServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "InvoiceReceipts",
                schema: "billing",
                columns: table => new
                {
                    InvoiceId = table.Column<Guid>(type: "uuid", nullable: false),
                    DocumentDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ReceiptInfo = table.Column<string>(type: "text", nullable: false),
                    QrCodeData = table.Column<string>(type: "text", nullable: true),
                    QrCodeFormat = table.Column<string>(type: "text", nullable: true),
                    FiscalNumber = table.Column<string>(type: "text", nullable: true),
                    FiscalSerial = table.Column<string>(type: "text", nullable: true),
                    FiscalSign = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceReceipts", x => x.InvoiceId);
                    table.ForeignKey(
                        name: "FK_InvoiceReceipts_Invoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalSchema: "billing",
                        principalTable: "Invoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingServiceTypeCostChanges",
                schema: "history",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceTypeChangeId = table.Column<Guid>(type: "uuid", nullable: false),
                    OldCost = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingServiceTypeCostChanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillingServiceTypeCostChanges_BillingServiceTypeChanges_Bil~",
                        column: x => x.BillingServiceTypeChangeId,
                        principalSchema: "billingCommits",
                        principalTable: "BillingServiceTypeChanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountDatabaseBackupHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: true),
                    CreateDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    AccountDatabaseBackupId = table.Column<Guid>(type: "uuid", nullable: true),
                    State = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDatabaseBackupHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDatabaseBackupHistories_AccountDatabaseBackups_Accou~",
                        column: x => x.AccountDatabaseBackupId,
                        principalTable: "AccountDatabaseBackups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AccountDatabaseBackupHistories_AccountDatabases_AccountData~",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceManagerAcDbBackups",
                columns: table => new
                {
                    AccountDatabaseBackupId = table.Column<Guid>(type: "uuid", nullable: false),
                    ServiceManagerAcDbBackupId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceManagerAcDbBackups", x => x.AccountDatabaseBackupId);
                    table.ForeignKey(
                        name: "FK_ServiceManagerAcDbBackups_AccountDatabaseBackups_AccountDat~",
                        column: x => x.AccountDatabaseBackupId,
                        principalTable: "AccountDatabaseBackups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AcDbAccRolesJHO",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AcDbAccessesID = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleJHO = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcDbAccRolesJHO", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AcDbAccRolesJHO_AcDbAccesses_AcDbAccessesID",
                        column: x => x.AcDbAccessesID,
                        principalTable: "AcDbAccesses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ManageDbOnDelimitersAccesses",
                schema: "accessDatabase",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccessState = table.Column<int>(type: "integer", nullable: false),
                    StateUpdateDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ProcessingDelayReason = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManageDbOnDelimitersAccesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManageDbOnDelimitersAccesses_AcDbAccesses_Id",
                        column: x => x.Id,
                        principalTable: "AcDbAccesses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AcDbSupportHistory",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uuid", nullable: false),
                    State = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    EditDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Operation = table.Column<int>(type: "integer", nullable: true),
                    CurrentVersionCfu = table.Column<string>(type: "text", nullable: true),
                    OldVersionCfu = table.Column<string>(type: "text", nullable: true),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcDbSupportHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AcDbSupportHistory_AcDbSupport_AccountDatabaseId",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AcDbSupport",
                        principalColumn: "AccountDatabasesID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AcDbSupportHistoryTasksQueueRelations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TasksQueueId = table.Column<Guid>(type: "uuid", nullable: false),
                    AcDbSupportHistoryId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcDbSupportHistoryTasksQueueRelations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AcDbSupportHistoryTasksQueueRelations_AcDbSupportHistory_Ac~",
                        column: x => x.AcDbSupportHistoryId,
                        principalTable: "AcDbSupportHistory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AcDbSupportHistoryTasksQueueRelations_CoreWorkerTasksQueue_~",
                        column: x => x.TasksQueueId,
                        principalTable: "CoreWorkerTasksQueue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AgentCashOutRequestFiles",
                schema: "billing",
                columns: table => new
                {
                    AgentCashOutRequestId = table.Column<Guid>(type: "uuid", nullable: false),
                    CloudFileId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentCashOutRequestFiles", x => new { x.AgentCashOutRequestId, x.CloudFileId });
                    table.ForeignKey(
                        name: "FK_AgentCashOutRequestFiles_CloudFiles_CloudFileId",
                        column: x => x.CloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentCashOutRequests",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestNumber = table.Column<string>(type: "text", nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    CreationDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RequestStatus = table.Column<int>(type: "integer", nullable: false),
                    StatusDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    AgentRequisitesId = table.Column<Guid>(type: "uuid", nullable: false),
                    RequestedSum = table.Column<decimal>(type: "numeric", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentCashOutRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AgentRequisites",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    AgentRequisitesStatus = table.Column<int>(type: "integer", nullable: false),
                    StatusDateTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    LegalPersonRequisitesId = table.Column<Guid>(type: "uuid", nullable: true),
                    PhysicalPersonRequisitesId = table.Column<Guid>(type: "uuid", nullable: true),
                    SoleProprietorRequisitesId = table.Column<Guid>(type: "uuid", nullable: true),
                    AccountOwnerId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentRequisites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgentRequisites_Accounts_AccountOwnerId",
                        column: x => x.AccountOwnerId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AgentRequisitesFiles",
                schema: "billing",
                columns: table => new
                {
                    AgentRequisitesId = table.Column<Guid>(type: "uuid", nullable: false),
                    CloudFileId = table.Column<Guid>(type: "uuid", nullable: false),
                    AgentRequisitesFileType = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentRequisitesFiles", x => new { x.AgentRequisitesId, x.CloudFileId });
                    table.ForeignKey(
                        name: "FK_AgentRequisitesFiles_AgentRequisites_AgentRequisitesId",
                        column: x => x.AgentRequisitesId,
                        principalSchema: "billing",
                        principalTable: "AgentRequisites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AgentRequisitesFiles_CloudFiles_CloudFileId",
                        column: x => x.CloudFileId,
                        principalTable: "CloudFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LegalPersonRequisites",
                schema: "billing",
                columns: table => new
                {
                    AgentRequisitesId = table.Column<Guid>(type: "uuid", nullable: false),
                    OrganizationName = table.Column<string>(type: "text", nullable: true),
                    HeadFullName = table.Column<string>(type: "text", nullable: true),
                    HeadPosition = table.Column<string>(type: "text", nullable: true),
                    LegalAddress = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Ogrn = table.Column<string>(type: "text", nullable: true),
                    Kpp = table.Column<string>(type: "text", nullable: true),
                    Inn = table.Column<string>(type: "text", nullable: true),
                    SettlementAccount = table.Column<string>(type: "text", nullable: true),
                    Bik = table.Column<string>(type: "text", nullable: true),
                    BankName = table.Column<string>(type: "text", nullable: true),
                    CorrespondentAccount = table.Column<string>(type: "text", nullable: true),
                    AddressForSendingDocuments = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalPersonRequisites", x => x.AgentRequisitesId);
                    table.ForeignKey(
                        name: "FK_LegalPersonRequisites_AgentRequisites_AgentRequisitesId",
                        column: x => x.AgentRequisitesId,
                        principalSchema: "billing",
                        principalTable: "AgentRequisites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhysicalPersonRequisites",
                schema: "billing",
                columns: table => new
                {
                    AgentRequisitesId = table.Column<Guid>(type: "uuid", nullable: false),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    DateOfBirth = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    PassportSeries = table.Column<string>(type: "text", nullable: true),
                    PassportNumber = table.Column<string>(type: "text", nullable: true),
                    WhomIssuedPassport = table.Column<string>(type: "text", nullable: true),
                    PassportDateOfIssue = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    RegistrationAddress = table.Column<string>(type: "text", nullable: true),
                    Inn = table.Column<string>(type: "text", nullable: true),
                    Snils = table.Column<string>(type: "text", nullable: true),
                    SettlementAccount = table.Column<string>(type: "text", nullable: true),
                    Bik = table.Column<string>(type: "text", nullable: true),
                    BankName = table.Column<string>(type: "text", nullable: true),
                    CorrespondentAccount = table.Column<string>(type: "text", nullable: true),
                    AddressForSendingDocuments = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhysicalPersonRequisites", x => x.AgentRequisitesId);
                    table.ForeignKey(
                        name: "FK_PhysicalPersonRequisites_AgentRequisites_AgentRequisitesId",
                        column: x => x.AgentRequisitesId,
                        principalSchema: "billing",
                        principalTable: "AgentRequisites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SoleProprietorRequisites",
                schema: "billing",
                columns: table => new
                {
                    AgentRequisitesId = table.Column<Guid>(type: "uuid", nullable: false),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    LegalAddress = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    Ogrn = table.Column<string>(type: "text", nullable: true),
                    Inn = table.Column<string>(type: "text", nullable: true),
                    SettlementAccount = table.Column<string>(type: "text", nullable: true),
                    Bik = table.Column<string>(type: "text", nullable: true),
                    BankName = table.Column<string>(type: "text", nullable: true),
                    CorrespondentAccount = table.Column<string>(type: "text", nullable: true),
                    AddressForSendingDocuments = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SoleProprietorRequisites", x => x.AgentRequisitesId);
                    table.ForeignKey(
                        name: "FK_SoleProprietorRequisites_AgentRequisites_AgentRequisitesId",
                        column: x => x.AgentRequisitesId,
                        principalSchema: "billing",
                        principalTable: "AgentRequisites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DiskResources",
                schema: "billing",
                columns: table => new
                {
                    ResourceId = table.Column<Guid>(type: "uuid", nullable: false),
                    VolumeGb = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiskResources", x => x.ResourceId);
                });

            migrationBuilder.CreateTable(
                name: "MyDatabasesResources",
                schema: "billing",
                columns: table => new
                {
                    ResourceId = table.Column<Guid>(type: "uuid", nullable: false),
                    PaidDatabasesCount = table.Column<int>(type: "integer", nullable: false),
                    ActualDatabasesCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MyDatabasesResources", x => x.ResourceId);
                });

            migrationBuilder.CreateTable(
                name: "Resources",
                schema: "billing",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Cost = table.Column<decimal>(type: "numeric", nullable: false),
                    Subject = table.Column<Guid>(type: "uuid", nullable: true),
                    DemoExpirePeriod = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IsFree = table.Column<bool>(type: "boolean", nullable: true),
                    Tag = table.Column<int>(type: "integer", nullable: true),
                    AccountSponsorId = table.Column<Guid>(type: "uuid", nullable: true),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    BillingServiceTypeId = table.Column<Guid>(type: "uuid", nullable: false),
                    MyDatabasesResourceId = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resources_Accounts_AccountSponsorId",
                        column: x => x.AccountSponsorId,
                        principalTable: "Accounts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Resources_BillingAccounts_AccountId",
                        column: x => x.AccountId,
                        principalSchema: "billing",
                        principalTable: "BillingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Resources_MyDatabasesResources_MyDatabasesResourceId",
                        column: x => x.MyDatabasesResourceId,
                        principalSchema: "billing",
                        principalTable: "MyDatabasesResources",
                        principalColumn: "ResourceId");
                    table.ForeignKey(
                        name: "FK_Resources_ServiceTypes_BillingServiceTypeId",
                        column: x => x.BillingServiceTypeId,
                        principalSchema: "billing",
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SessionResource",
                schema: "billing",
                columns: table => new
                {
                    ResourceId = table.Column<Guid>(type: "uuid", nullable: false),
                    SessionCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionResource", x => x.ResourceId);
                    table.ForeignKey(
                        name: "FK_SessionResource_Resources_ResourceId",
                        column: x => x.ResourceId,
                        principalSchema: "billing",
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountAdditionalCompanies_AccountId",
                table: "AccountAdditionalCompanies",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountAutoPayConfigurations_SavedPaymentMethodId",
                schema: "billing",
                table: "AccountAutoPayConfigurations",
                column: "SavedPaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountConfigurations_FileStorageId",
                table: "AccountConfigurations",
                column: "FileStorageId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountConfigurations_LocaleId",
                table: "AccountConfigurations",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountConfigurations_SegmentId",
                table: "AccountConfigurations",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountConfigurations_SupplierId",
                table: "AccountConfigurations",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountCorpClouds_AccountId",
                table: "AccountCorpClouds",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "Index_AccountId",
                table: "AccountCSResourceValues",
                columns: new[] { "AccountId", "CSResourceId" });

            migrationBuilder.CreateIndex(
                name: "IX_AccountCSResourceValues_CSResourceId",
                table: "AccountCSResourceValues",
                column: "CSResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseAutoUpdateResults_AccountDatabaseId",
                table: "AccountDatabaseAutoUpdateResults",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseAutoUpdateResults_CoreWorkerTasksQueueId",
                table: "AccountDatabaseAutoUpdateResults",
                column: "CoreWorkerTasksQueueId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseBackupHistories_AccountDatabaseBackupId",
                table: "AccountDatabaseBackupHistories",
                column: "AccountDatabaseBackupId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseBackupHistories_AccountDatabaseId",
                table: "AccountDatabaseBackupHistories",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseBackups_InitiatorId",
                table: "AccountDatabaseBackups",
                column: "InitiatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Backup_CreateDateTime",
                table: "AccountDatabaseBackups",
                columns: new[] { "AccountDatabaseId", "CreationBackupDateTime" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseOnDelimiters_DatabaseSourceId",
                table: "AccountDatabaseOnDelimiters",
                column: "DatabaseSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseOnDelimiters_DbTemplateDelimiterCode",
                table: "AccountDatabaseOnDelimiters",
                column: "DbTemplateDelimiterCode");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseOnDelimiters_UploadedFileId",
                table: "AccountDatabaseOnDelimiters",
                column: "UploadedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabases_FileStorageID",
                table: "AccountDatabases",
                column: "FileStorageID");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabases_OwnerId",
                table: "AccountDatabases",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabases_TemplateId",
                table: "AccountDatabases",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDatabaseUpdateVersionMappings_CfuId",
                table: "AccountDatabaseUpdateVersionMappings",
                column: "CfuId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountEmails_AccountId",
                table: "AccountEmails",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountFiles_AccountId",
                table: "AccountFiles",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountFiles_CloudFileId",
                table: "AccountFiles",
                column: "CloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountRates_AccountId",
                schema: "billing",
                table: "AccountRates",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountRates_BillingServiceTypeId",
                schema: "billing",
                table: "AccountRates",
                column: "BillingServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountSaleManagers_SaleManagerId",
                table: "AccountSaleManagers",
                column: "SaleManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserJsonWebToken_AccountUserId",
                table: "AccountUserJsonWebToken",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserNotificationSettings_AccountUserId",
                table: "AccountUserNotificationSettings",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserNotificationSettings_NotificationSettingsId",
                table: "AccountUserNotificationSettings",
                column: "NotificationSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserRoles_AccountUserId",
                table: "AccountUserRoles",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserEmail",
                table: "AccountUsers",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserLogin",
                table: "AccountUsers",
                column: "Login",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccountUsers_AccountId",
                table: "AccountUsers",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthToken",
                table: "AccountUsers",
                column: "AuthToken");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserSession_TokenCreationTime",
                table: "AccountUserSessions",
                column: "TokenCreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserSessions_AccountUserId",
                table: "AccountUserSessions",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserSessions_Token",
                table: "AccountUserSessions",
                column: "Token");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbAccesses_AccountDatabaseID",
                table: "AcDbAccesses",
                column: "AccountDatabaseID");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbAccesses_AccountID",
                table: "AcDbAccesses",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbAccesses_AccountUserID",
                table: "AcDbAccesses",
                column: "AccountUserID");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbAccRolesJHO_AcDbAccessesID",
                table: "AcDbAccRolesJHO",
                column: "AcDbAccessesID");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbSupport_ConfigurationName",
                table: "AcDbSupport",
                column: "ConfigurationName");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbSupportHistory_AccountDatabaseId",
                table: "AcDbSupportHistory",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_AcDbSupportHistoryTasksQueueRelations",
                table: "AcDbSupportHistoryTasksQueueRelations",
                columns: new[] { "TasksQueueId", "AcDbSupportHistoryId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AcDbSupportHistoryTasksQueueRelations_AcDbSupportHistoryId",
                table: "AcDbSupportHistoryTasksQueueRelations",
                column: "AcDbSupportHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ActionFlows_ProcessFlowId",
                schema: "statemachine",
                table: "ActionFlows",
                column: "ProcessFlowId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingBannerAudienceLocaleRelation",
                schema: "marketing",
                table: "AdvertisingBannerAudienceLocaleRelation",
                columns: new[] { "AdvertisingBannerAudienceId", "LocaleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingBannerAudienceLocaleRelation_LocaleId",
                schema: "marketing",
                table: "AdvertisingBannerAudienceLocaleRelation",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingBannerTemplate_AdvertisingBannerAudienceId",
                schema: "marketing",
                table: "AdvertisingBannerTemplate",
                column: "AdvertisingBannerAudienceId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingBannerTemplate_HtmlTemplateId",
                schema: "marketing",
                table: "AdvertisingBannerTemplate",
                column: "HtmlTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertisingBannerTemplate_ImageCloudFileId",
                schema: "marketing",
                table: "AdvertisingBannerTemplate",
                column: "ImageCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AgencyAgreements_PrintedHtmlFormId",
                schema: "billing",
                table: "AgencyAgreements",
                column: "PrintedHtmlFormId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentCashOutRequestFiles_CloudFileId",
                schema: "billing",
                table: "AgentCashOutRequestFiles",
                column: "CloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentCashOutRequest_RequestNumber",
                schema: "billing",
                table: "AgentCashOutRequests",
                column: "RequestNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AgentCashOutRequests_AgentRequisitesId",
                schema: "billing",
                table: "AgentCashOutRequests",
                column: "AgentRequisitesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentPaymentRelations_AccountId",
                schema: "billing",
                table: "AgentPaymentRelations",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentPayments_AccountOwnerId",
                schema: "billing",
                table: "AgentPayments",
                column: "AccountOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentPayments_AgentPaymentRelationId",
                schema: "billing",
                table: "AgentPayments",
                column: "AgentPaymentRelationId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentPayments_ClientPaymentId",
                schema: "billing",
                table: "AgentPayments",
                column: "ClientPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentRequisites_AccountOwnerId",
                schema: "billing",
                table: "AgentRequisites",
                column: "AccountOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentRequisites_LegalPersonRequisitesId",
                schema: "billing",
                table: "AgentRequisites",
                column: "LegalPersonRequisitesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentRequisites_PhysicalPersonRequisitesId",
                schema: "billing",
                table: "AgentRequisites",
                column: "PhysicalPersonRequisitesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentRequisites_SoleProprietorRequisitesId",
                schema: "billing",
                table: "AgentRequisites",
                column: "SoleProprietorRequisitesId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentRequisitesFiles_CloudFileId",
                schema: "billing",
                table: "AgentRequisitesFiles",
                column: "CloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransferBalanseRequests_FromAccountId",
                schema: "billing",
                table: "AgentTransferBalanseRequests",
                column: "FromAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransferBalanseRequests_InitiatorId",
                schema: "billing",
                table: "AgentTransferBalanseRequests",
                column: "InitiatorId");

            migrationBuilder.CreateIndex(
                name: "IX_AgentTransferBalanseRequests_ToAccountId",
                schema: "billing",
                table: "AgentTransferBalanseRequests",
                column: "ToAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_AccountUserId",
                table: "Articles",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleTransaction_ArticleId",
                schema: "billing",
                table: "ArticleTransaction",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_AvailableMigration_SegmentIdFrom",
                table: "AvailableMigration",
                column: "SegmentIdFrom");

            migrationBuilder.CreateIndex(
                name: "IX_AvailableMigration_SegmentIdTo",
                table: "AvailableMigration",
                column: "SegmentIdTo");

            migrationBuilder.CreateIndex(
                name: "IX_BillingService1CFiles_Service1CFileVersionId",
                schema: "billing",
                table: "BillingService1CFiles",
                column: "Service1CFileVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingService1CFiles_ServiceId",
                schema: "billing",
                table: "BillingService1CFiles",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceChanges_IconCloudFileId",
                schema: "billingCommits",
                table: "BillingServiceChanges",
                column: "IconCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceChanges_InstructionServiceCloudFileId",
                schema: "billingCommits",
                table: "BillingServiceChanges",
                column: "InstructionServiceCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceChanges_OriginalIconCloudFileId",
                schema: "billingCommits",
                table: "BillingServiceChanges",
                column: "OriginalIconCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceExtension",
                schema: "billing",
                table: "BillingServiceExtension",
                column: "Service1CFileName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeActivityForAccount",
                schema: "billing",
                table: "BillingServiceTypeActivityForAccounts",
                columns: new[] { "BillingServiceTypeId", "AccountId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeActivityForAccounts_AccountId",
                schema: "billing",
                table: "BillingServiceTypeActivityForAccounts",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeChanges_BillingServiceChangesId",
                schema: "billingCommits",
                table: "BillingServiceTypeChanges",
                column: "BillingServiceChangesId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeChanges_BillingServiceTypeId",
                schema: "billingCommits",
                table: "BillingServiceTypeChanges",
                column: "BillingServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeChanges_DependServiceTypeId",
                schema: "billingCommits",
                table: "BillingServiceTypeChanges",
                column: "DependServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceTypeCostChanges_BillingServiceTypeChangeId",
                schema: "history",
                table: "BillingServiceTypeCostChanges",
                column: "BillingServiceTypeChangeId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeServiceRequests_BillingServiceChangesId",
                schema: "billingCommits",
                table: "ChangeServiceRequests",
                column: "BillingServiceChangesId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeServiceRequests_BillingServiceId",
                schema: "billingCommits",
                table: "ChangeServiceRequests",
                column: "BillingServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_ChangeServiceRequests_ModeratorId",
                schema: "billingCommits",
                table: "ChangeServiceRequests",
                column: "ModeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudChanges_AccountId",
                table: "CloudChanges",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudChanges_Action",
                table: "CloudChanges",
                column: "Action");

            migrationBuilder.CreateIndex(
                name: "IX_CloudChanges_Initiator",
                table: "CloudChanges",
                column: "Initiator");

            migrationBuilder.CreateIndex(
                name: "IX_CloudChangesActions_GroupId",
                table: "CloudChangesActions",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudConfiguration_ContextSupplierId",
                table: "CloudConfiguration",
                column: "ContextSupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_ContextSupplier_Key",
                table: "CloudConfiguration",
                columns: new[] { "Key", "ContextSupplierId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CloudLocalization",
                table: "CloudLocalizations",
                column: "Value");

            migrationBuilder.CreateIndex(
                name: "IX_CloudLocalizations_LocaleId",
                table: "CloudLocalizations",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServices_AccountUserId",
                table: "CloudServices",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesContentServerNodes_ContentServerId",
                table: "CloudServicesContentServerNodes",
                column: "ContentServerId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesContentServerNodes_NodeReferenceId",
                table: "CloudServicesContentServerNodes",
                column: "NodeReferenceId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_Alpha83Version",
                table: "CloudServicesSegment",
                column: "Alpha83Version");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_AutoUpdateNodeId",
                table: "CloudServicesSegment",
                column: "AutoUpdateNodeId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_BackupStorageID",
                table: "CloudServicesSegment",
                column: "BackupStorageID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_ContentServerID",
                table: "CloudServicesSegment",
                column: "ContentServerID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_CoreHostingId",
                table: "CloudServicesSegment",
                column: "CoreHostingId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_EnterpriseServer82ID",
                table: "CloudServicesSegment",
                column: "EnterpriseServer82ID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_EnterpriseServer83ID",
                table: "CloudServicesSegment",
                column: "EnterpriseServer83ID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_FileStorageServersID",
                table: "CloudServicesSegment",
                column: "FileStorageServersID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_GatewayTerminalsID",
                table: "CloudServicesSegment",
                column: "GatewayTerminalsID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_ServicesTerminalFarmID",
                table: "CloudServicesSegment",
                column: "ServicesTerminalFarmID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_SQLServerID",
                table: "CloudServicesSegment",
                column: "SQLServerID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_Stable82Version",
                table: "CloudServicesSegment",
                column: "Stable82Version");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegment_Stable83Version",
                table: "CloudServicesSegment",
                column: "Stable83Version");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegmentStorage_FileStorageID",
                table: "CloudServicesSegmentStorage",
                column: "FileStorageID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegmentStorage_SegmentID",
                table: "CloudServicesSegmentStorage",
                column: "SegmentID");

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegmentTerminalServers",
                table: "CloudServicesSegmentTerminalServers",
                columns: new[] { "SegmentId", "TerminalServerId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CloudServicesSegmentTerminalServers_TerminalServerId",
                table: "CloudServicesSegmentTerminalServers",
                column: "TerminalServerId");

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationDbTemplateRelations_Configuration1CName",
                table: "ConfigurationDbTemplateRelations",
                column: "Configuration1CName");

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationNameVariations_Configuration1CName",
                schema: "its",
                table: "ConfigurationNameVariations",
                column: "Configuration1CName");

            migrationBuilder.CreateIndex(
                name: "IX_VariationName",
                schema: "its",
                table: "ConfigurationNameVariations",
                column: "VariationName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Configurations1C_ItsAuthorizationDataId",
                table: "Configurations1C",
                column: "ItsAuthorizationDataId");

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationsCfu_ConfigurationName",
                table: "ConfigurationsCfu",
                column: "ConfigurationName");

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationsCfu_InitiatorId",
                table: "ConfigurationsCfu",
                column: "InitiatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ConfigurationServiceTypeRelations_ServiceTypeId",
                schema: "billing",
                table: "ConfigurationServiceTypeRelations",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Connections_AccountUserId",
                table: "Connections",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerAvailableTasksBags_CoreWorkerTaskId",
                table: "CoreWorkerAvailableTasksBags",
                column: "CoreWorkerTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTaskParameters_DependTaskId",
                table: "CoreWorkerTaskParameters",
                column: "DependTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTasksQueue_CapturedWorkerId",
                table: "CoreWorkerTasksQueue",
                column: "CapturedWorkerId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTasksQueue_CoreWorkerTaskId",
                table: "CoreWorkerTasksQueue",
                column: "CoreWorkerTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTasksQueueLive_CapturedWorkerId",
                table: "CoreWorkerTasksQueueLive",
                column: "CapturedWorkerId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTasksQueueLive_CoreWorkerTaskId",
                table: "CoreWorkerTasksQueueLive",
                column: "CoreWorkerTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CoreWorkerTasksQueueLive_DependTaskId",
                table: "CoreWorkerTasksQueueLive",
                column: "DependTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_CSResources_CloudServiceId",
                table: "CSResources",
                column: "CloudServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplateDelimiters_TemplateId",
                table: "DbTemplateDelimiters",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplates",
                table: "DbTemplates",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplates_DemoTemplateId",
                table: "DbTemplates",
                column: "DemoTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplates_LocaleId",
                table: "DbTemplates",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplateUpdates_ResponsibleAccountUserId",
                table: "DbTemplateUpdates",
                column: "ResponsibleAccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTemplateUpdates_TemplateId",
                table: "DbTemplateUpdates",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_DelimiterSourceAccountDatabases_DbTemplateDelimiterCode",
                table: "DelimiterSourceAccountDatabases",
                column: "DbTemplateDelimiterCode");

            migrationBuilder.CreateIndex(
                name: "IX_EmailSmsVerifications_AccountUserId",
                schema: "security",
                table: "EmailSmsVerifications",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FlowResourcesScope_FlowResourceId",
                schema: "billing",
                table: "FlowResourcesScope",
                column: "FlowResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_GoogleSheets_AccountId",
                table: "GoogleSheets",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_IndustryName",
                table: "Industries",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_IndustriesDependencyBillingServices_IndustryId",
                schema: "billing",
                table: "IndustriesDependencyBillingServices",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_IndustryDependencyBillingServiceChanges_BillingServiceChang~",
                schema: "billingCommits",
                table: "IndustryDependencyBillingServiceChanges",
                column: "BillingServiceChangesId");

            migrationBuilder.CreateIndex(
                name: "IX_IndustryDependencyBillingServiceChanges_IndustryId",
                schema: "billingCommits",
                table: "IndustryDependencyBillingServiceChanges",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceProducts_InvoiceId",
                schema: "billing",
                table: "InvoiceProducts",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceProducts_ServiceTypeId",
                schema: "billing",
                table: "InvoiceProducts",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_AccountAdditionalCompanyId",
                schema: "billing",
                table: "Invoices",
                column: "AccountAdditionalCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_AccountId",
                schema: "billing",
                table: "Invoices",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Invoices_PaymentID",
                schema: "billing",
                table: "Invoices",
                column: "PaymentID");

            migrationBuilder.CreateIndex(
                name: "IX_LetterAdvertisingBannerRelation_AdvertisingBannerTemplateId",
                schema: "mailing",
                table: "LetterAdvertisingBannerRelation",
                column: "AdvertisingBannerTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_Link42ConfigurationBitDepths_ConfigurationId",
                table: "Link42ConfigurationBitDepths",
                column: "ConfigurationId");

            migrationBuilder.CreateIndex(
                name: "IX_Link42Configurations_CreatedBy",
                table: "Link42Configurations",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Link42Configurations_UpdatedBy",
                table: "Link42Configurations",
                column: "UpdatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_LocaleConfigurations_DefaultSegmentId",
                schema: "configuration",
                table: "LocaleConfigurations",
                column: "DefaultSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_MainServiceResourcesChangesHistory",
                schema: "history",
                table: "MainServiceResourcesChanges",
                columns: new[] { "AccountId", "ChangesDate" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountDatabaseHistories_AccountDatabaseId",
                schema: "migration",
                table: "MigrationAccountDatabaseHistories",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountDatabaseHistories_SourceFileStorageId",
                schema: "migration",
                table: "MigrationAccountDatabaseHistories",
                column: "SourceFileStorageId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountDatabaseHistories_TargetFileStorageId",
                schema: "migration",
                table: "MigrationAccountDatabaseHistories",
                column: "TargetFileStorageId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountHistories_AccountId",
                schema: "migration",
                table: "MigrationAccountHistories",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountHistories_SourceSegmentId",
                schema: "migration",
                table: "MigrationAccountHistories",
                column: "SourceSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationAccountHistories_TargetSegmentId",
                schema: "migration",
                table: "MigrationAccountHistories",
                column: "TargetSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_MigrationHistories_InitiatorId",
                schema: "migration",
                table: "MigrationHistories",
                column: "InitiatorId");

            migrationBuilder.CreateIndex(
                name: "IX_NotificationBuffer_Account",
                table: "NotificationBuffer",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_AccountUserId",
                table: "Notifications",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_Account",
                schema: "billing",
                table: "Payments",
                column: "Account");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_BillingServiceId",
                schema: "billing",
                table: "Payments",
                column: "BillingServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_PrintedHtmlFormFiles_CloudFileId",
                table: "PrintedHtmlFormFiles",
                column: "CloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProvidedServices_AccountId",
                table: "ProvidedServices",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_ProvidedServices_SponsoredAccountId",
                table: "ProvidedServices",
                column: "SponsoredAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_BillingServiceTypeId",
                schema: "billing",
                table: "Rates",
                column: "BillingServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Rates_LocaleId",
                schema: "billing",
                table: "Rates",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_RecalculationServiceCostAfterChanges_BillingServiceChangesId",
                schema: "billing",
                table: "RecalculationServiceCostAfterChanges",
                column: "BillingServiceChangesId");

            migrationBuilder.CreateIndex(
                name: "IX_RecalculationServiceCostAfterChanges_CoreWorkerTasksQueueId",
                schema: "billing",
                table: "RecalculationServiceCostAfterChanges",
                column: "CoreWorkerTasksQueueId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_AccountId",
                schema: "billing",
                table: "Resources",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_AccountSponsorId",
                schema: "billing",
                table: "Resources",
                column: "AccountSponsorId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_BillingServiceTypeId",
                schema: "billing",
                table: "Resources",
                column: "BillingServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_MyDatabasesResourceId",
                schema: "billing",
                table: "Resources",
                column: "MyDatabasesResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_Subject",
                schema: "billing",
                table: "Resources",
                column: "Subject");

            migrationBuilder.CreateIndex(
                name: "IX_ResourcesConfigurations_AccountId",
                schema: "billing",
                table: "ResourcesConfigurations",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_ResourcesConfigurations_BillingServiceId",
                schema: "billing",
                table: "ResourcesConfigurations",
                column: "BillingServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedPaymentMethods_AccountId",
                schema: "billing",
                table: "SavedPaymentMethods",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Service1CFileDbTemplateRelation",
                schema: "billing",
                table: "Service1CFileDbTemplateRelation",
                columns: new[] { "BillingService1CFileId", "DbTemplateDelimitersId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Service1CFileDbTemplateRelation_DbTemplateDelimitersId",
                schema: "billing",
                table: "Service1CFileDbTemplateRelation",
                column: "DbTemplateDelimitersId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceAccounts_AccountUserInitiatorId",
                table: "ServiceAccounts",
                column: "AccountUserInitiatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceExtensionDatabase",
                table: "ServiceExtensionDatabases",
                columns: new[] { "ServiceId", "AccountDatabaseId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceExtensionDatabases_AccountDatabaseId",
                table: "ServiceExtensionDatabases",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceManager1CFileRelation",
                schema: "billing",
                table: "ServiceManager1CFileRelation",
                columns: new[] { "BillingService1CFileId", "ServiceManagerUploadedFileId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Backup_ServiceManagerId",
                table: "ServiceManagerAcDbBackups",
                column: "ServiceManagerAcDbBackupId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BillingService_Key",
                schema: "billing",
                table: "Services",
                column: "Key",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_AccountOwnerId",
                schema: "billing",
                table: "Services",
                column: "AccountOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_IconCloudFileId",
                schema: "billing",
                table: "Services",
                column: "IconCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_InstructionServiceCloudFileId",
                schema: "billing",
                table: "Services",
                column: "InstructionServiceCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_OriginalIconCloudFileId",
                schema: "billing",
                table: "Services",
                column: "OriginalIconCloudFileId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceTypeRelations_ChildServiceTypeId",
                schema: "billing",
                table: "ServiceTypeRelations",
                column: "ChildServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BillingServiceType_Key",
                schema: "billing",
                table: "ServiceTypes",
                column: "Key",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceTypes_DependServiceTypeId",
                schema: "billing",
                table: "ServiceTypes",
                column: "DependServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceTypes_ServiceId",
                schema: "billing",
                table: "ServiceTypes",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_SupplierReferralAccounts_ReferralAccountId",
                table: "SupplierReferralAccounts",
                column: "ReferralAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Supplier_Code",
                table: "Suppliers",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_AgreementId",
                table: "Suppliers",
                column: "AgreementId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_LocaleId",
                table: "Suppliers",
                column: "LocaleId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_PrintedHtmlFormInvoiceId",
                table: "Suppliers",
                column: "PrintedHtmlFormInvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_PrintedHtmlFormInvoiceReceiptId",
                table: "Suppliers",
                column: "PrintedHtmlFormInvoiceReceiptId");

            migrationBuilder.CreateIndex(
                name: "IX_TerminationSessionsInDatabases_AccountDatabaseId",
                table: "TerminationSessionsInDatabases",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_UpdateNodeQueues_AccountDatabaseId",
                table: "UpdateNodeQueues",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFiles_AccountId",
                table: "UploadedFiles",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_WebSocketConnections_AccountUserId",
                table: "WebSocketConnections",
                column: "AccountUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AgentCashOutRequestFiles_AgentCashOutRequests_AgentCashOutR~",
                schema: "billing",
                table: "AgentCashOutRequestFiles",
                column: "AgentCashOutRequestId",
                principalSchema: "billing",
                principalTable: "AgentCashOutRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AgentCashOutRequests_AgentRequisites_AgentRequisitesId",
                schema: "billing",
                table: "AgentCashOutRequests",
                column: "AgentRequisitesId",
                principalSchema: "billing",
                principalTable: "AgentRequisites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AgentRequisites_LegalPersonRequisites_LegalPersonRequisites~",
                schema: "billing",
                table: "AgentRequisites",
                column: "LegalPersonRequisitesId",
                principalSchema: "billing",
                principalTable: "LegalPersonRequisites",
                principalColumn: "AgentRequisitesId");

            migrationBuilder.AddForeignKey(
                name: "FK_AgentRequisites_PhysicalPersonRequisites_PhysicalPersonRequ~",
                schema: "billing",
                table: "AgentRequisites",
                column: "PhysicalPersonRequisitesId",
                principalSchema: "billing",
                principalTable: "PhysicalPersonRequisites",
                principalColumn: "AgentRequisitesId");

            migrationBuilder.AddForeignKey(
                name: "FK_AgentRequisites_SoleProprietorRequisites_SoleProprietorRequ~",
                schema: "billing",
                table: "AgentRequisites",
                column: "SoleProprietorRequisitesId",
                principalSchema: "billing",
                principalTable: "SoleProprietorRequisites",
                principalColumn: "AgentRequisitesId");

            migrationBuilder.AddForeignKey(
                name: "FK_DiskResources_Resources_ResourceId",
                schema: "billing",
                table: "DiskResources",
                column: "ResourceId",
                principalSchema: "billing",
                principalTable: "Resources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MyDatabasesResources_Resources_ResourceId",
                schema: "billing",
                table: "MyDatabasesResources",
                column: "ResourceId",
                principalSchema: "billing",
                principalTable: "Resources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AgentRequisites_Accounts_AccountOwnerId",
                schema: "billing",
                table: "AgentRequisites");

            migrationBuilder.DropForeignKey(
                name: "FK_BillingAccounts_Accounts_Id",
                schema: "billing",
                table: "BillingAccounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Resources_Accounts_AccountSponsorId",
                schema: "billing",
                table: "Resources");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Accounts_AccountOwnerId",
                schema: "billing",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_CloudFiles_IconCloudFileId",
                schema: "billing",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_CloudFiles_InstructionServiceCloudFileId",
                schema: "billing",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_CloudFiles_OriginalIconCloudFileId",
                schema: "billing",
                table: "Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Resources_ServiceTypes_BillingServiceTypeId",
                schema: "billing",
                table: "Resources");

            migrationBuilder.DropForeignKey(
                name: "FK_LegalPersonRequisites_AgentRequisites_AgentRequisitesId",
                schema: "billing",
                table: "LegalPersonRequisites");

            migrationBuilder.DropForeignKey(
                name: "FK_PhysicalPersonRequisites_AgentRequisites_AgentRequisitesId",
                schema: "billing",
                table: "PhysicalPersonRequisites");

            migrationBuilder.DropForeignKey(
                name: "FK_SoleProprietorRequisites_AgentRequisites_AgentRequisitesId",
                schema: "billing",
                table: "SoleProprietorRequisites");

            migrationBuilder.DropForeignKey(
                name: "FK_MyDatabasesResources_Resources_ResourceId",
                schema: "billing",
                table: "MyDatabasesResources");

            migrationBuilder.DropTable(
                name: "AccessMappingRule");

            migrationBuilder.DropTable(
                name: "AccountAutoPayConfigurations",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AccountConfigurations");

            migrationBuilder.DropTable(
                name: "AccountCorpClouds");

            migrationBuilder.DropTable(
                name: "AccountCSResourceValues");

            migrationBuilder.DropTable(
                name: "AccountDatabaseAutoUpdateResults");

            migrationBuilder.DropTable(
                name: "AccountDatabaseBackupHistories");

            migrationBuilder.DropTable(
                name: "AccountDatabaseOnDelimiters");

            migrationBuilder.DropTable(
                name: "AccountDatabaseUpdateVersionMappings");

            migrationBuilder.DropTable(
                name: "AccountEmails");

            migrationBuilder.DropTable(
                name: "AccountFiles");

            migrationBuilder.DropTable(
                name: "AccountLocaleChanges",
                schema: "history");

            migrationBuilder.DropTable(
                name: "AccountRates",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AccountRequisites");

            migrationBuilder.DropTable(
                name: "AccountSaleManagers");

            migrationBuilder.DropTable(
                name: "AccountUserJsonWebToken");

            migrationBuilder.DropTable(
                name: "AccountUserNotificationSettings");

            migrationBuilder.DropTable(
                name: "AccountUserRoles");

            migrationBuilder.DropTable(
                name: "AccountUserSessions");

            migrationBuilder.DropTable(
                name: "AcDbAccRolesJHO");

            migrationBuilder.DropTable(
                name: "AcDbSupportHistoryTasksQueueRelations");

            migrationBuilder.DropTable(
                name: "ActionFlows",
                schema: "statemachine");

            migrationBuilder.DropTable(
                name: "AdvertisingBannerAudienceLocaleRelation",
                schema: "marketing");

            migrationBuilder.DropTable(
                name: "AgencyAgreements",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentCashOutRequestFiles",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentPayments",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentRequisitesFiles",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentTransferBalanseRequests",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentWallets",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "ArticleTransaction",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "ArticleWallets",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AvailableMigration");

            migrationBuilder.DropTable(
                name: "BillingServiceExtension",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "BillingServiceTypeActivityForAccounts",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "BillingServiceTypeCostChanges",
                schema: "history");

            migrationBuilder.DropTable(
                name: "ChangeServiceRequests",
                schema: "billingCommits");

            migrationBuilder.DropTable(
                name: "CloudChanges");

            migrationBuilder.DropTable(
                name: "CloudConfiguration");

            migrationBuilder.DropTable(
                name: "CloudCore");

            migrationBuilder.DropTable(
                name: "CloudLocalizations");

            migrationBuilder.DropTable(
                name: "CloudServicesContentServerNodes");

            migrationBuilder.DropTable(
                name: "CloudServicesSegmentStorage");

            migrationBuilder.DropTable(
                name: "CloudServicesSegmentTerminalServers");

            migrationBuilder.DropTable(
                name: "ConfigurationDbTemplateRelations");

            migrationBuilder.DropTable(
                name: "ConfigurationNameVariations",
                schema: "its");

            migrationBuilder.DropTable(
                name: "ConfigurationServiceTypeRelations",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Connections");

            migrationBuilder.DropTable(
                name: "CoreWorkerAvailableTasksBags");

            migrationBuilder.DropTable(
                name: "CoreWorkerTaskParameters");

            migrationBuilder.DropTable(
                name: "CoreWorkerTasksQueueLive");

            migrationBuilder.DropTable(
                name: "DatabaseLaunchRdpStartHistory");

            migrationBuilder.DropTable(
                name: "DbTemplateUpdates");

            migrationBuilder.DropTable(
                name: "DelimiterSourceAccountDatabases");

            migrationBuilder.DropTable(
                name: "DiskResources",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "EmailSmsVerifications",
                schema: "security");

            migrationBuilder.DropTable(
                name: "FlowResourcesScope",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "GoogleSheets");

            migrationBuilder.DropTable(
                name: "Incidents");

            migrationBuilder.DropTable(
                name: "IndustriesDependencyBillingServices",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "IndustryDependencyBillingServiceChanges",
                schema: "billingCommits");

            migrationBuilder.DropTable(
                name: "InvoiceProducts",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "InvoiceReceipts",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "LetterAdvertisingBannerRelation",
                schema: "mailing");

            migrationBuilder.DropTable(
                name: "LimitOnFreeCreationDbForAccounts",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Link42ConfigurationBitDepths");

            migrationBuilder.DropTable(
                name: "LocaleConfigurations",
                schema: "configuration");

            migrationBuilder.DropTable(
                name: "MainServiceResourcesChanges",
                schema: "history");

            migrationBuilder.DropTable(
                name: "ManageDbOnDelimitersAccesses",
                schema: "accessDatabase");

            migrationBuilder.DropTable(
                name: "MigrationAccountDatabaseHistories",
                schema: "migration");

            migrationBuilder.DropTable(
                name: "MigrationAccountHistories",
                schema: "migration");

            migrationBuilder.DropTable(
                name: "MyDiskPropertiesByAccounts");

            migrationBuilder.DropTable(
                name: "NotificationBuffer");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "Partners");

            migrationBuilder.DropTable(
                name: "PrintedHtmlFormFiles");

            migrationBuilder.DropTable(
                name: "ProvidedServices");

            migrationBuilder.DropTable(
                name: "Rates",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "RecalculationServiceCostAfterChanges",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "ResourcesConfigurations",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "SavedPaymentMethodBankCards",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Service1CFileDbTemplateRelation",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "ServiceAccounts");

            migrationBuilder.DropTable(
                name: "ServiceExtensionDatabases");

            migrationBuilder.DropTable(
                name: "ServiceManager1CFileRelation",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "ServiceManagerAcDbBackups");

            migrationBuilder.DropTable(
                name: "ServiceTypeRelations",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "SessionResource",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "SupplierReferralAccounts");

            migrationBuilder.DropTable(
                name: "TerminationSessionsInDatabases");

            migrationBuilder.DropTable(
                name: "UpdateNodeQueues");

            migrationBuilder.DropTable(
                name: "WaitInfos");

            migrationBuilder.DropTable(
                name: "WebSocketConnections");

            migrationBuilder.DropTable(
                name: "UploadedFiles");

            migrationBuilder.DropTable(
                name: "ConfigurationsCfu");

            migrationBuilder.DropTable(
                name: "NotificationSettings");

            migrationBuilder.DropTable(
                name: "AcDbSupportHistory");

            migrationBuilder.DropTable(
                name: "ProcessFlows",
                schema: "statemachine");

            migrationBuilder.DropTable(
                name: "AgentCashOutRequests",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentPaymentRelations",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "BillingServiceTypeChanges",
                schema: "billingCommits");

            migrationBuilder.DropTable(
                name: "CloudChangesActions");

            migrationBuilder.DropTable(
                name: "PublishNodesReferences");

            migrationBuilder.DropTable(
                name: "CloudTerminalServers");

            migrationBuilder.DropTable(
                name: "CSResources");

            migrationBuilder.DropTable(
                name: "Industries");

            migrationBuilder.DropTable(
                name: "Invoices",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AdvertisingBannerTemplate",
                schema: "marketing");

            migrationBuilder.DropTable(
                name: "LetterTemplate",
                schema: "mailing");

            migrationBuilder.DropTable(
                name: "Link42Configurations");

            migrationBuilder.DropTable(
                name: "AcDbAccesses");

            migrationBuilder.DropTable(
                name: "CloudServicesSegment");

            migrationBuilder.DropTable(
                name: "MigrationHistories",
                schema: "migration");

            migrationBuilder.DropTable(
                name: "CoreWorkerTasksQueue");

            migrationBuilder.DropTable(
                name: "SavedPaymentMethods",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "DbTemplateDelimiters");

            migrationBuilder.DropTable(
                name: "BillingService1CFiles",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AccountDatabaseBackups");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "AcDbSupport");

            migrationBuilder.DropTable(
                name: "BillingServiceChanges",
                schema: "billingCommits");

            migrationBuilder.DropTable(
                name: "CloudChangesGroups");

            migrationBuilder.DropTable(
                name: "CloudServices");

            migrationBuilder.DropTable(
                name: "AccountAdditionalCompanies");

            migrationBuilder.DropTable(
                name: "Payments",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AdvertisingBannerAudience",
                schema: "marketing");

            migrationBuilder.DropTable(
                name: "HtmlTemplate");

            migrationBuilder.DropTable(
                name: "AutoUpdateNodes");

            migrationBuilder.DropTable(
                name: "CloudServicesBackupStorage");

            migrationBuilder.DropTable(
                name: "CloudServicesContentServer");

            migrationBuilder.DropTable(
                name: "CloudServicesEnterpriseServer");

            migrationBuilder.DropTable(
                name: "CloudServicesGatewayTerminals");

            migrationBuilder.DropTable(
                name: "CloudServicesSQLServer");

            migrationBuilder.DropTable(
                name: "CloudServicesTerminalFarm");

            migrationBuilder.DropTable(
                name: "CoreHosting",
                schema: "segment");

            migrationBuilder.DropTable(
                name: "PlatformVersionReferences");

            migrationBuilder.DropTable(
                name: "CoreWorkerTask");

            migrationBuilder.DropTable(
                name: "CoreWorkers");

            migrationBuilder.DropTable(
                name: "Service1CFileVersion",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "PrintedHtmlForms");

            migrationBuilder.DropTable(
                name: "AccountDatabases");

            migrationBuilder.DropTable(
                name: "Configurations1C");

            migrationBuilder.DropTable(
                name: "AccountUsers");

            migrationBuilder.DropTable(
                name: "CloudServicesFileStorageServers");

            migrationBuilder.DropTable(
                name: "DbTemplates");

            migrationBuilder.DropTable(
                name: "ItsAuthorizationData",
                schema: "security");

            migrationBuilder.DropTable(
                name: "Locale");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "CloudFiles");

            migrationBuilder.DropTable(
                name: "ServiceTypes",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Services",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "AgentRequisites",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "LegalPersonRequisites",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "PhysicalPersonRequisites",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "SoleProprietorRequisites",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "Resources",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "BillingAccounts",
                schema: "billing");

            migrationBuilder.DropTable(
                name: "MyDatabasesResources",
                schema: "billing");
        }
    }
}
