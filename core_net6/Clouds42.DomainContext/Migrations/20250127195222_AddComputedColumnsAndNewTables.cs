﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clouds42.DomainContext.Migrations
{
    /// <inheritdoc />
    public partial class AddComputedColumnsAndNewTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Build",
                table: "ConfigurationsCfu",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"Version\", '.', 4)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Major",
                table: "ConfigurationsCfu",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"Version\", '.', 1)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Minor",
                table: "ConfigurationsCfu",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"Version\", '.', 2)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Patch",
                table: "ConfigurationsCfu",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"Version\", '.', 3)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Build",
                table: "AcDbSupport",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"CurrentVersion\", '.', 4)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Major",
                table: "AcDbSupport",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"CurrentVersion\", '.', 1)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Minor",
                table: "AcDbSupport",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"CurrentVersion\", '.', 2)), '')::INT,  0)",
                stored: true);

            migrationBuilder.AddColumn<int>(
                name: "Patch",
                table: "AcDbSupport",
                type: "integer",
                nullable: false,
                computedColumnSql: "COALESCE( NULLIF(trim(split_part(\"CurrentVersion\", '.', 3)), '')::INT,  0)",
                stored: true);

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountUserDepartments",
                columns: table => new
                {
                    DepartmentId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserDepartments", x => new { x.AccountUserId, x.DepartmentId });
                    table.ForeignKey(
                        name: "FK_AccountUserDepartments_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountUserDepartments_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserDepartments_DepartmentId",
                table: "AccountUserDepartments",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_AccountId",
                table: "Departments",
                column: "AccountId");

            migrationBuilder.Sql("""
          DO $$
          DECLARE
              taskId UUID := gen_random_uuid();
          BEGIN
              -- Insert into CoreWorkerTask
              INSERT INTO "CoreWorkerTask"
              (
                  "ID",
                  "TaskName",
                  "Status",
                  "MethodName",
                  "TaskParams",
                  "WorkerTaskType",
                  "TaskAllowsMultithreading",
                  "TaskExecutionLifetimeInMinutes"
              )
              VALUES
              (
                  taskId,
                  'TransformFileBaseToServerJob',
                  true,
                  'Clouds42.CoreWorker.Jobs.TransformFileBaseToServerJob, Clouds42.CoreWorker.Jobs, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null',
                  NULL,
                  0,
                  TRUE,
                  -1
              );
          
              -- Insert into CoreWorkerAvailableTasksBags
              IF EXISTS (SELECT 1 FROM "CoreWorkers" WHERE "Id" = 1) THEN
                  INSERT INTO "CoreWorkerAvailableTasksBags"
                  (
                      "CoreWorkerId",
                      "CoreWorkerTaskId",
                      "Priority"
                  )
                  VALUES
                  (
                      1,
                      taskId,
                      15
                  );
              END IF;
          
              -- Conditionally insert into CoreWorkerAvailableTasksBags for CoreWorkerId 7
              IF EXISTS (SELECT 1 FROM "CoreWorkers" WHERE "Id" = 7) THEN
                  INSERT INTO "CoreWorkerAvailableTasksBags"
                  (
                      "CoreWorkerId",
                      "CoreWorkerTaskId",
                      "Priority"
                  )
                  VALUES
                  (
                      7,
                      taskId,
                      25
                  );
              END IF;
          END $$
          """);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountUserDepartments");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropColumn(
                name: "Build",
                table: "ConfigurationsCfu");

            migrationBuilder.DropColumn(
                name: "Major",
                table: "ConfigurationsCfu");

            migrationBuilder.DropColumn(
                name: "Minor",
                table: "ConfigurationsCfu");

            migrationBuilder.DropColumn(
                name: "Patch",
                table: "ConfigurationsCfu");

            migrationBuilder.DropColumn(
                name: "Build",
                table: "AcDbSupport");

            migrationBuilder.DropColumn(
                name: "Major",
                table: "AcDbSupport");

            migrationBuilder.DropColumn(
                name: "Minor",
                table: "AcDbSupport");

            migrationBuilder.DropColumn(
                name: "Patch",
                table: "AcDbSupport");

            migrationBuilder.Sql("""
                                 DO $$
                                 DECLARE
                                     taskId UUID;
                                 BEGIN
                                     -- Получаем ID задачи
                                     SELECT "ID" INTO taskId
                                     FROM "CoreWorkerTask"
                                     WHERE "TaskName" = 'TransformFileBaseToServerJob'
                                     LIMIT 1;
                                 
                                     -- Удаляем записи из CoreWorkerAvailableTasksBags
                                     DELETE FROM "CoreWorkerAvailableTasksBags"
                                     WHERE "CoreWorkerTaskId" = taskId;
                                 
                                     -- Удаляем запись из CoreWorkerTask
                                     DELETE FROM "CoreWorkerTask"
                                     WHERE "ID" = taskId;
                                 END $$;
                                 """);
        }
    }
}
