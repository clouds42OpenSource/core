﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clouds42.DomainContext.Migrations
{
    /// <inheritdoc />
    public partial class AddFeedbacksTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountUserDepartments",
                table: "AccountUserDepartments");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountUserDepartments",
                table: "AccountUserDepartments",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Feedbacks",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Delay = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    FeedbackLeftDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Feedbacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Feedbacks_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserDepartments_AccountUserId",
                table: "AccountUserDepartments",
                column: "AccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Feedbacks_AccountUserId",
                table: "Feedbacks",
                column: "AccountUserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Feedbacks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AccountUserDepartments",
                table: "AccountUserDepartments");

            migrationBuilder.DropIndex(
                name: "IX_AccountUserDepartments_AccountUserId",
                table: "AccountUserDepartments");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AccountUserDepartments",
                table: "AccountUserDepartments",
                columns: new[] { "AccountUserId", "DepartmentId" });
        }
    }
}
