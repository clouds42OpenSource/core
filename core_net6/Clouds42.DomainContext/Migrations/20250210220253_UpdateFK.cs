﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clouds42.DomainContext.Migrations
{
    /// <inheritdoc />
    public partial class UpdateFK : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_AccountDatabas~",
                table: "AccountDatabaseOnDelimiters");

            migrationBuilder.DropColumn(
                name: "MethodName",
                table: "CoreWorkerTask");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_AccountDatabas~",
                table: "AccountDatabaseOnDelimiters",
                column: "AccountDatabaseId",
                principalTable: "AccountDatabases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_AccountDatabas~",
                table: "AccountDatabaseOnDelimiters");

            migrationBuilder.AddColumn<string>(
                name: "MethodName",
                table: "CoreWorkerTask",
                type: "text",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountDatabaseOnDelimiters_AccountDatabases_AccountDatabas~",
                table: "AccountDatabaseOnDelimiters",
                column: "AccountDatabaseId",
                principalTable: "AccountDatabases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
