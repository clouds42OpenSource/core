﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clouds42.DomainContext.Migrations
{
    /// <inheritdoc />
    public partial class AddAccountUsersBitDepths : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountUserBitDepths",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    BitDepth = table.Column<int>(type: "integer", nullable: false),
                    AccountUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountDatabaseId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountUserBitDepths", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountUserBitDepths_AccountDatabases_AccountDatabaseId",
                        column: x => x.AccountDatabaseId,
                        principalTable: "AccountDatabases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountUserBitDepths_AccountUsers_AccountUserId",
                        column: x => x.AccountUserId,
                        principalTable: "AccountUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserBitDepths_AccountDatabaseId",
                table: "AccountUserBitDepths",
                column: "AccountDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountUserBitDepths_AccountUserId",
                table: "AccountUserBitDepths",
                column: "AccountUserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountUserBitDepths");
        }
    }
}
