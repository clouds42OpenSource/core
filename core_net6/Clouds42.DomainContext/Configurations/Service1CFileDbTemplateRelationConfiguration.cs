﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class Service1CFileDbTemplateRelationConfiguration :
        IEntityTypeConfiguration<Service1CFileDbTemplateRelation>
    {
        public void Configure(EntityTypeBuilder<Service1CFileDbTemplateRelation> builder)
        {
            builder.ToTable(nameof(Service1CFileDbTemplateRelation), SchemaType.Billing);

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder
                .HasOne(x => x.BillingService1CFile)
                .WithMany(x => x.Service1CFileDbTemplateRelations)
                .HasForeignKey(x => x.BillingService1CFileId)
                .IsRequired();

            builder
                .HasOne(x => x.DbTemplateDelimiters)
                .WithMany(x => x.Service1CFileDbTemplateRelations)
                .HasForeignKey(x => x.DbTemplateDelimitersId)
                .IsRequired(false);

            builder.HasIndex(x => new { x.BillingService1CFileId, x.DbTemplateDelimitersId })
                .HasDatabaseName("IX_Service1CFileDbTemplateRelation")
                .IsUnique();
        }
    }
}
