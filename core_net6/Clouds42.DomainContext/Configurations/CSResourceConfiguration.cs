﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CSResourceConfiguration :
        BaseDomainModelConfiguration<CsResource>
    {
        public override void Configure(EntityTypeBuilder<CsResource> builder)
        {
            builder.ToTable("CSResources");
        }
    }
}
