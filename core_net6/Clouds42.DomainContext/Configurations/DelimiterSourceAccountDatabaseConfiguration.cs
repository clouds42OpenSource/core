﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class DelimiterSourceAccountDatabaseConfiguration : IEntityTypeConfiguration<DelimiterSourceAccountDatabase>
    {
        public void Configure(EntityTypeBuilder<DelimiterSourceAccountDatabase> builder)
        {
            builder.HasKey(x => new { x.AccountDatabaseId, x.DbTemplateDelimiterCode });

            builder.HasOne(x => x.AccountDatabase)
                .WithMany(x => x.DelimiterSourceAccountDatabases)
                .HasForeignKey(x => x.AccountDatabaseId);

            builder.HasOne(x => x.DbTemplateDelimiter)
                .WithMany(x => x.DelimiterSourceAccountDatabases)
                .HasForeignKey(x => x.DbTemplateDelimiterCode);
        }
    }
}
