﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountDatabaseBackupConfiguration :
        BaseDomainModelConfiguration<AccountDatabaseBackup>
    {
        public override void Configure(EntityTypeBuilder<AccountDatabaseBackup> builder)
        {
            builder.ToTable("AccountDatabaseBackups");

            builder
                .HasIndex(x => new { x.AccountDatabaseId, x.CreationBackupDateTime })
                .HasDatabaseName("IX_Backup_CreateDateTime")
                .IsUnique();

            builder
                .HasOne(x => x.AccountDatabase)
                .WithMany(x => x.AccountDatabaseBackups)
                .HasForeignKey(x => x.AccountDatabaseId)
                .IsRequired();

            builder
                .HasOne(x => x.Initiator)
                .WithMany(x => x.InitiatorBackups)
                .HasForeignKey(x => x.InitiatorId)
                .IsRequired();
        }
    }
}
