﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AgentCashOutRequestConfiguration :
        BaseDomainModelConfiguration<AgentCashOutRequest>
    {
        public override void Configure(EntityTypeBuilder<AgentCashOutRequest> builder)
        {
            builder.ToTable("AgentCashOutRequests", SchemaType.Billing);

            builder
                .HasIndex(x => x.RequestNumber)
                .HasDatabaseName("IX_AgentCashOutRequest_RequestNumber")
                .IsUnique();

            builder
                .HasOne(x => x.AgentRequisites)
                .WithMany(x => x.AgentCashOutRequests)
                .HasForeignKey(x => x.AgentRequisitesId)
                .IsRequired();

        }
    }
}
