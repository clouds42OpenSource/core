﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ProvidedServiceConfiguration : BaseDomainModelConfiguration<ProvidedService>
    {
        public override void Configure(EntityTypeBuilder<ProvidedService> builder)
        {
            builder.ToTable("ProvidedServices");

            builder
                .Property(x => x.Rate)
                .HasPrecision(18, 0);
        }
    }
}
