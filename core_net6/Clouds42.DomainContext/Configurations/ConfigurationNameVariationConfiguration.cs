﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.Its;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ConfigurationNameVariationConfiguration :
        BaseDomainModelConfiguration<ConfigurationNameVariation>
    {
        public override void Configure(EntityTypeBuilder<ConfigurationNameVariation> builder)
        {
            builder.ToTable("ConfigurationNameVariations", SchemaType.Its);

            builder
                .HasOne(x => x.Configurations1C)
                .WithMany(x => x.ConfigurationNameVariations)
                .HasForeignKey(x => x.Configuration1CName)
                .IsRequired();

            builder
                .HasIndex(x => x.VariationName)
                .HasDatabaseName("IX_VariationName")
                .IsUnique();
        }
    }
}
