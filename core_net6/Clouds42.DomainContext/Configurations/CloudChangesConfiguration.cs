﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudChangesConfiguration : 
        BaseDomainModelConfiguration<CloudChanges>
    {
        public override void Configure(EntityTypeBuilder<CloudChanges> builder)
        {
            builder.ToTable(nameof(CloudChanges));

            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.CloudChanges)
                .HasForeignKey(x => x.AccountId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(x => x.CloudChangesAction)
                .WithMany(x => x.CloudChanges)
                .HasForeignKey(x => x.Action)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
