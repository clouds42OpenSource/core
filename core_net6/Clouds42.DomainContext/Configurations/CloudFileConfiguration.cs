﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudFileConfiguration :
         BaseDomainModelConfiguration<CloudFile>
    {
        public override void Configure(EntityTypeBuilder<CloudFile> builder)
        {
            builder.ToTable("CloudFiles");

            builder
                .HasMany(x => x.BillingServices)
                .WithOne(x => x.IconCloudFile)
                .HasForeignKey(x => x.IconCloudFileId)
                .IsRequired(false);

            builder
                .HasMany(x => x.BillingServices2)
                .WithOne(x => x.OriginalIconCloudFile)
                .HasForeignKey(x => x.OriginalIconCloudFileId)
                .IsRequired(false);

            builder
                .HasMany(x => x.BillingServices3)
                .WithOne(x => x.InstructionServiceCloudFile)
                .HasForeignKey(x => x.InstructionServiceCloudFileId)
                .IsRequired(false);
        }
    }
}
