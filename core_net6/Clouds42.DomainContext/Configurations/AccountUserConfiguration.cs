﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountUserConfiguration :
        BaseDomainModelConfiguration<AccountUser>
    {
        public override void Configure(EntityTypeBuilder<AccountUser> builder)
        {
            builder.ToTable("AccountUsers");

            builder
                .HasIndex(x => x.Login)
                .HasDatabaseName("IX_AccountUserLogin")
                .IsUnique();

            builder
                .HasIndex(x => x.Email)
                .HasDatabaseName("IX_AccountUserEmail");

            builder
                .HasIndex(x => x.AuthToken)
                .HasDatabaseName("IX_AuthToken")
                .IsUnique(false);

            builder.HasOne(x => x.Account)
                .WithMany(x => x.AccountUsers)
                .HasForeignKey(x => x.AccountId)
                .IsRequired();

            builder
                .HasMany(x => x.UpdatedLink42Configurations)
                .WithOne(x => x.UpdatedByAccountUser)
                .HasForeignKey(x => x.UpdatedBy)
                .IsRequired(false);

            builder
                .HasMany(x => x.CreatedLink42Configurations)
                .WithOne(x => x.CreatedByAccountUser)
                .HasForeignKey(x => x.CreatedBy)
                .IsRequired(false);

            builder.Ignore(x => x.CreationDateValue);
            builder.Ignore(x => x.Name);
        }
    }
}
