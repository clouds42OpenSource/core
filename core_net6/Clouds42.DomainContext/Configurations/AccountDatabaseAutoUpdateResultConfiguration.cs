﻿using Clouds42.Domain.DataModels.CoreWorkerModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountDatabaseAutoUpdateResultConfiguration :
        BaseDomainModelConfiguration<AccountDatabaseAutoUpdateResult>
    {
        public override void Configure(EntityTypeBuilder<AccountDatabaseAutoUpdateResult> builder)
        {
            builder.ToTable("AccountDatabaseAutoUpdateResults");
        }
    }
}
