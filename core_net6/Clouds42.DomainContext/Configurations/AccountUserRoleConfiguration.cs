﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountUserRoleConfiguration : BaseDomainModelConfiguration<AccountUserRole>
    {
        public override void Configure(EntityTypeBuilder<AccountUserRole> builder)
        {
            builder.ToTable("AccountUserRoles");

            builder
                .HasOne(x => x.AccountUser)
                .WithMany(x => x.AccountUserRoles)
                .HasForeignKey(x => x.AccountUserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
