﻿using Clouds42.Domain.DataModels.GoogleSheets;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations;

public class GoogleSheetsConfiguration : IEntityTypeConfiguration<GoogleSheet>
{
    public void Configure(EntityTypeBuilder<GoogleSheet> builder)
    {
        builder
            .ToTable("GoogleSheets")
            .HasKey(x => x.Id);
    }
}
