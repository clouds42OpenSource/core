﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class RecalculationServiceCostAfterChangeConfiguration :
        BaseDomainModelConfiguration<RecalculationServiceCostAfterChange>
    {
        public override void Configure(EntityTypeBuilder<RecalculationServiceCostAfterChange> builder)
        {
            builder.ToTable("RecalculationServiceCostAfterChanges", SchemaType.Billing);
        }
    }
}
