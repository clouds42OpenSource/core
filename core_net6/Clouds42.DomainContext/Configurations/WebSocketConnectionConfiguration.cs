﻿using Clouds42.Domain.DataModels.WebSockets;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class WebSocketConnectionConfiguration :
    BaseDomainModelConfiguration<WebSocketConnection>
    {
        public override void Configure(EntityTypeBuilder<WebSocketConnection> builder)
        {
            builder.ToTable("WebSocketConnections");
        }
    }
}
