﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceTypeChangeConfiguration :
        BaseDomainModelConfiguration<BillingServiceTypeChange>
    {
        public override void Configure(EntityTypeBuilder<BillingServiceTypeChange> builder)
        {
            builder.ToTable("BillingServiceTypeChanges", SchemaType.BillingCommits);
        }
    }
}
