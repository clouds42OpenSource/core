﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceChangesConfiguration :
        BaseDomainModelConfiguration<BillingServiceChanges>
    {
        public override void Configure(EntityTypeBuilder<BillingServiceChanges> builder)
        {
            builder.ToTable(nameof(BillingServiceChanges), SchemaType.BillingCommits);
        }
    }
}
