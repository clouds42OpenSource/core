﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations;

public class AccountUserDepartmentConfiguration :  IEntityTypeConfiguration<AccountUserDepartment>
{

    public void Configure(EntityTypeBuilder<AccountUserDepartment> builder)
    {
        builder.HasOne(ad => ad.AccountUser)
            .WithMany(ad => ad.AccountUserDepartments)
            .HasForeignKey(sc => sc.AccountUserId)
            .IsRequired();

        builder.HasOne(ad => ad.Department)
            .WithMany(d => d.AccountUserDepartments)
            .HasForeignKey(sc => sc.DepartmentId)
            .IsRequired();
    }
}
