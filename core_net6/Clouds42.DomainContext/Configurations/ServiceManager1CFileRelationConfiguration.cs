﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ServiceManager1CFileRelationConfiguration :
        IEntityTypeConfiguration<ServiceManager1CFileRelation>
    {
        public void Configure(EntityTypeBuilder<ServiceManager1CFileRelation> builder)
        {
            builder.ToTable(nameof(ServiceManager1CFileRelation), SchemaType.Billing);

            builder.HasKey(x => x.Id);
                
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            
            builder
                .HasOne(x => x.BillingService1CFile)
                .WithMany(x => x.ServiceManager1CFileRelations)
                .HasForeignKey(x => x.BillingService1CFileId)
                .IsRequired();

            builder
                .HasIndex(x => new { x.BillingService1CFileId, x.ServiceManagerUploadedFileId })
                .HasDatabaseName("IX_ServiceManager1CFileRelation")
                .IsUnique(true);
        }
    }
}
