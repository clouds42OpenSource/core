﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class Link42ConfigurationConfiguration : BaseDomainModelConfiguration<Link42Configuration>
    {
        public override void Configure(EntityTypeBuilder<Link42Configuration> builder)
        {
            builder.ToTable("Link42Configurations");

            builder
                .HasMany(x => x.BitDepths)
                .WithOne(x => x.Configuration)
                .HasForeignKey(x => x.ConfigurationId)
                .IsRequired();

            builder
                .HasOne(x => x.CreatedByAccountUser)
                .WithMany(x => x.CreatedLink42Configurations)
                .HasForeignKey(x => x.CreatedBy)
                .IsRequired(false);

            builder
                .HasOne(x => x.UpdatedByAccountUser)
                .WithMany(x => x.UpdatedLink42Configurations)
                .HasForeignKey(x => x.UpdatedBy)
                .IsRequired(false);
        }
    }

    public class Link42ConfigurationBitDepthConfiguration : BaseDomainModelConfiguration<Link42ConfigurationBitDepth>
    {
        public override void Configure(EntityTypeBuilder<Link42ConfigurationBitDepth> builder)
        {
            builder.ToTable("Link42ConfigurationBitDepths");


            builder
                .HasOne(x => x.Configuration)
                .WithMany(x => x.BitDepths)
                .HasForeignKey(x => x.ConfigurationId)
                .IsRequired();
        }
    }
}
