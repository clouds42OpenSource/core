﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class Service1CFileVersionConfiguration :
        BaseDomainModelConfiguration<Service1CFileVersion>
    {
        public override void Configure(EntityTypeBuilder<Service1CFileVersion> builder)
        {
           builder.ToTable(nameof(Service1CFileVersion), SchemaType.Billing);
        }
    }
}
