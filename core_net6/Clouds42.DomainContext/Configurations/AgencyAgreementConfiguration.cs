﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AgencyAgreementConfiguration :
        BaseDomainModelConfiguration<AgencyAgreement>
    {
        public override void Configure(EntityTypeBuilder<AgencyAgreement> builder)
        {
            builder.ToTable("AgencyAgreements", SchemaType.Billing);
        }
    }
}
