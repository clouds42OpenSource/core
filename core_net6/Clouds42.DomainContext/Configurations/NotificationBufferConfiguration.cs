﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class NotificationBufferConfiguration :
        BaseDomainModelConfiguration<NotificationBuffer>
    {
        public override void Configure(EntityTypeBuilder<NotificationBuffer> builder)
        {
            builder.ToTable(nameof(NotificationBuffer));
        }
    }
}
