﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class DbTemplateConfiguration :
        BaseDomainModelConfiguration<DbTemplate>
    {
        public override void Configure(EntityTypeBuilder<DbTemplate> builder)
        {
            builder.ToTable("DbTemplates");

            builder
                .HasIndex(x => x.Name)
                .HasDatabaseName("IX_DbTemplates");

            builder.HasOne(x => x.DemoTemplate)
                .WithMany(x => x.DbTemplates)
                .HasForeignKey(x => x.DemoTemplateId)
                .IsRequired(false);

            builder.Ignore(x => x.PlatformType);
            builder.Ignore(x => x.GetDbTemplateDelimiters);
        }
    }
}
