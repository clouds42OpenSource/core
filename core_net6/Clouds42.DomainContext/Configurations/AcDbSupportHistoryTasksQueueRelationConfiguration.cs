﻿using Clouds42.Domain.DataModels.AccountDatabases;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AcDbSupportHistoryTasksQueueRelationConfiguration :
        IEntityTypeConfiguration<AcDbSupportHistoryTasksQueueRelation>
    {
        public void Configure(EntityTypeBuilder<AcDbSupportHistoryTasksQueueRelation> builder)
        {
            builder.ToTable("AcDbSupportHistoryTasksQueueRelations");

            builder.HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder
                .HasOne(x => x.AutoUpdateTasksQueue)
                .WithMany(x => x.AcDbSupportHistoryTasksQueues)
                .HasForeignKey(x => x.TasksQueueId)
                .IsRequired();

            builder
                .HasOne(x => x.AcDbSupportHistory)
                .WithMany(x => x.AcDbSupportHistoryTasksQueues)
                .HasForeignKey(x => x.AcDbSupportHistoryId)
                .IsRequired();

            builder
                .HasIndex(x => new { x.TasksQueueId, x.AcDbSupportHistoryId })
                .HasDatabaseName("IX_AcDbSupportHistoryTasksQueueRelations")
                .IsUnique();
        }
    }
}
