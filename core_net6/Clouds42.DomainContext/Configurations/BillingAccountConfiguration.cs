﻿using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    /// <summary>
    /// Конфигурация сущности/таблицы аккаунта биллинга 
    /// </summary>
    public class BillingAccountConfiguration : IEntityTypeConfiguration<BillingAccount>
    {
        public void Configure(EntityTypeBuilder<BillingAccount> builder)
        {
            builder
                .Property(x => x.AdditionalResourceCost)
                .HasPrecision(18, 0);
        }
    }
}
