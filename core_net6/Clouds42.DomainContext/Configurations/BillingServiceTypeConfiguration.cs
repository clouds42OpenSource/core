﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceTypeConfiguration : BaseDomainModelConfiguration<BillingServiceType>
    {
        public override void Configure(EntityTypeBuilder<BillingServiceType> builder)
        {
            builder.ToTable("ServiceTypes", SchemaType.Billing);

            builder
                .HasOne(x => x.Service)
                .WithMany(x => x.BillingServiceTypes)
                .HasForeignKey(x => x.ServiceId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasIndex(x => x.Key)
                .HasDatabaseName("IX_BillingServiceType_Key")
                .IsUnique();

            builder
                .HasOne(x => x.DependServiceType)
                .WithMany(x => x.BillingServiceTypes)
                .HasForeignKey(x => x.DependServiceTypeId)
                .IsRequired(false);

            builder
                .HasMany(x => x.Resources)
                .WithOne(x => x.BillingServiceType)
                .IsRequired()
                .HasForeignKey(x => x.BillingServiceTypeId);
        }
    }
}
