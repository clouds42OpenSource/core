﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudConfigurationConfiguration :
        BaseDomainModelConfiguration<CloudConfiguration>
    {
        public override void Configure(EntityTypeBuilder<CloudConfiguration> builder)
        {
            builder.ToTable(nameof(CloudConfiguration));

            builder
                .HasIndex(x => new { x.Key, x.ContextSupplierId })
                .HasDatabaseName("IX_ContextSupplier_Key")
                .IsUnique();

            builder
                .HasOne(x => x.ContextSupplier)
                .WithMany(x => x.Configurations)
                .HasForeignKey(x => x.ContextSupplierId)
                .IsRequired(false);
        }
    }
}
