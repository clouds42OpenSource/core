﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.History;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceTypeCostChangedHistoryConfiguration :
        BaseDomainModelConfiguration<BillingServiceTypeCostChangedHistory>
    {
        public override void Configure(EntityTypeBuilder<BillingServiceTypeCostChangedHistory> builder)
        {
           builder.ToTable("BillingServiceTypeCostChanges", SchemaType.History);
        }
    }
}
