﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.Marketing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AdvertisingBannerAudienceLocaleRelationConfiguration :
        IEntityTypeConfiguration<AdvertisingBannerAudienceLocaleRelation>
    {
        public void Configure(EntityTypeBuilder<AdvertisingBannerAudienceLocaleRelation> builder)
        {
            builder.ToTable(nameof(AdvertisingBannerAudienceLocaleRelation), SchemaType.Marketing);

            builder.HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder
                .HasOne(x => x.AdvertisingBannerAudience)
                .WithMany(x => x.AdvertisingBannerAudienceLocaleRelations)
                .HasForeignKey(x => x.AdvertisingBannerAudienceId)
                .IsRequired();

            builder
                .HasOne(x => x.Locale)
                .WithMany(x => x.AccountConAdvertisingBannerAudienceLocaleRelations)
                .HasForeignKey(x => x.LocaleId)
                .IsRequired();

            builder.HasIndex(x => new { x.AdvertisingBannerAudienceId, x.LocaleId })
                .HasDatabaseName("IX_AdvertisingBannerAudienceLocaleRelation")
                .IsUnique();
        }
    }
}
