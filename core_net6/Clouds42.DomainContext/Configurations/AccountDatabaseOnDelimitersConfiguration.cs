﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountDatabaseOnDelimitersConfiguration : IEntityTypeConfiguration<AccountDatabaseOnDelimiters>
    {
        public void Configure(EntityTypeBuilder<AccountDatabaseOnDelimiters> builder)
        {
            builder.HasKey(x => x.AccountDatabaseId);

            builder
                .Property(x => x.LoadType)
                .IsRequired();

            builder
                .HasOne(x => x.AccountDatabase)
                .WithOne(x => x.AccountDatabaseOnDelimiter)
                .HasForeignKey<AccountDatabaseOnDelimiters>(x => x.AccountDatabaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(x => x.SourceAccountDatabase)
                .WithMany(x => x.AccountDatabaseOnDelimiters)
                .HasForeignKey(x => x.DatabaseSourceId)
                .IsRequired(false);

            builder
                .HasOne(x => x.DbTemplateDelimiter)
                .WithMany(x => x.AccountDatabaseOnDelimiters)
                .HasForeignKey(x => x.DbTemplateDelimiterCode)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(x => x.UploadedFile)
                .WithOne(x => x.AccountDatabaseOnDelimiters)
                .HasForeignKey<AccountDatabaseOnDelimiters>(x => x.UploadedFileId)
                .IsRequired(false);

            builder.HasIndex(x => x.UploadedFileId).IsUnique(false);
        }
    }
}
