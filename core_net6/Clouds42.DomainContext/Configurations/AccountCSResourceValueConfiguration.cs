﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountCSResourceValueConfiguration :
        BaseDomainModelConfiguration<AccountCSResourceValue>
    {
        public override void Configure(EntityTypeBuilder<AccountCSResourceValue> builder)
        {
            builder.ToTable("AccountCSResourceValues");

            builder
                .HasIndex(x => new { x.AccountId, x.CSResourceId })
                .HasDatabaseName("Index_AccountId");

            builder
                .HasOne(x => x.CSResource)
                .WithMany(x => x.AccountCsResourceValues)
                .HasForeignKey(x => x.CSResourceId)
                .IsRequired();
        }
    }
}
