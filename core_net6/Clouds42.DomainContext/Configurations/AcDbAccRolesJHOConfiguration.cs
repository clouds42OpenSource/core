﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AcDbAccRolesJhoConfiguration : 
        BaseDomainModelConfiguration<AcDbAccRolesJho>
    {
        public override void Configure(EntityTypeBuilder<AcDbAccRolesJho> builder)
        {
            builder.ToTable("AcDbAccRolesJHO");

            builder
                .HasOne(x => x.AcDbAccess)
                .WithMany(x => x.AcDbAccRolesJHOes)
                .HasForeignKey(x => x.AcDbAccessesID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
