﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountUserSessionConfiguration :
        IEntityTypeConfiguration<AccountUserSession>
    {
        public void Configure(EntityTypeBuilder<AccountUserSession> builder)
        {
            builder.ToTable("AccountUserSessions");

            builder.HasKey(x => x.Id);

            builder
                .HasIndex(x => x.Token)
                .HasDatabaseName("IX_AccountUserSessions_Token");

            builder
                .HasIndex(x => x.TokenCreationTime)
                .HasDatabaseName("IX_AccountUserSession_TokenCreationTime");

            builder
                .HasOne(x => x.AccountUser)
                .WithMany(x => x.AccountUserSessions)
                .HasForeignKey(x => x.AccountUserId);
        }
    }
}
