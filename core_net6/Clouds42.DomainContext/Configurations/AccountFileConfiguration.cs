﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountFileConfiguration: BaseDomainModelConfiguration<AccountFile>
    {
        public override void Configure(EntityTypeBuilder<AccountFile> builder)
        {
            base.Configure(builder);

            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.AccountFiles)
                .HasForeignKey(x => x.AccountId);
        }
    }
}
