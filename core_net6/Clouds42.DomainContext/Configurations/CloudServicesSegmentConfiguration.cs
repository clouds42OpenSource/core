﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudServicesSegmentConfiguration : IEntityTypeConfiguration<CloudServicesSegment>
    {
        public void Configure(EntityTypeBuilder<CloudServicesSegment> builder)
        {
            builder
                .HasOne(x => x.Stable82PlatformVersionReference)
                .WithMany(x => x.CloudServicesSegmentsStable82)
                .HasForeignKey(x => x.Stable82Version)
                .HasPrincipalKey(x => x.Version)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(x => x.Alpha83PlatformVersionReference)
                .WithMany(x => x.CloudServicesSegmentsAlpha83)
                .HasForeignKey(x => x.Alpha83Version)
                .HasPrincipalKey(x => x.Version)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(x => x.Stable83PlatformVersionReference)
                .WithMany(x => x.CloudServicesSegmentsStable83)
                .HasForeignKey(x => x.Stable83Version)
                .HasPrincipalKey(x => x.Version)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
