﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ResourcesConfigurationConfiguration :
        BaseDomainModelConfiguration<ResourcesConfiguration>
    {
        public override void Configure(EntityTypeBuilder<ResourcesConfiguration> builder)
        {
            builder.ToTable("ResourcesConfigurations", SchemaType.Billing);
        }
    }
}
