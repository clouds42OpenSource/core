﻿using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceTypeRelationConfiguration : IEntityTypeConfiguration<BillingServiceTypeRelation>
    {
        public void Configure(EntityTypeBuilder<BillingServiceTypeRelation> builder)
        {
            builder
                .HasOne(w => w.MainServiceType)
                .WithMany(x => x.ChildRelations)
                .HasForeignKey(x => x.MainServiceTypeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(w => w.ChildServiceType)
                .WithMany(x => x.ParentRelations)
                .HasForeignKey(x => x.ChildServiceTypeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
