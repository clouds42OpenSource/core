﻿using Clouds42.Domain.DataModels.WaitInfos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations;

public class WaitInfosConfiguration : IEntityTypeConfiguration<WaitInfo>
{
    public void Configure(EntityTypeBuilder<WaitInfo> builder)
    {
        builder
            .ToTable("WaitInfos")
            .HasKey(w => w.Id);

        builder
            .Property(w => w.Description)
            .IsRequired();

        builder.Ignore(w => w.Causes);
        builder.Ignore(w => w.Recommendations);
    }
}
