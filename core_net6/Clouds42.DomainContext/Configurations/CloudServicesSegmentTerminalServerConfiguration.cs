﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudServicesSegmentTerminalServerConfiguration : 
        BaseDomainModelConfiguration<CloudServicesSegmentTerminalServer>
    {
        public override void Configure(EntityTypeBuilder<CloudServicesSegmentTerminalServer> builder)
        {
            builder.ToTable("CloudServicesSegmentTerminalServers");

            builder
                .HasOne(x => x.CloudServicesSegment)
                .WithMany(x => x.CloudServicesSegmentTerminalServers)
                .HasForeignKey(x => x.SegmentId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(x => x.CloudTerminalServer)
                .WithMany(x => x.CloudServicesSegmentTerminalServers)
                .HasForeignKey(x => x.TerminalServerId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasIndex(x => new { x.SegmentId, x.TerminalServerId })
                .HasDatabaseName("IX_CloudServicesSegmentTerminalServers")
                .IsUnique();
        }
    }
}
