﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class MigrationAccountDatabaseHistoryConfiguration : IEntityTypeConfiguration<MigrationAccountDatabaseHistory>
    {
        public void Configure(EntityTypeBuilder<MigrationAccountDatabaseHistory> builder)
        {
            builder
                .HasOne(x => x.AccountDatabas)
                .WithMany(x => x.MigrationAccountDatabaseHistories)
                .HasForeignKey(x => x.AccountDatabaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
