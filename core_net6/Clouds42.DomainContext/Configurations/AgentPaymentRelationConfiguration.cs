﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AgentPaymentRelationConfiguration :
        BaseDomainModelConfiguration<AgentPaymentRelation>
    {
        public override void Configure(EntityTypeBuilder<AgentPaymentRelation> builder)
        {
            builder.ToTable("AgentPaymentRelations", SchemaType.Billing);
        }
    }
}
