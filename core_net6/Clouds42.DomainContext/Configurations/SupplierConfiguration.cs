﻿using Clouds42.Domain.DataModels.Supplier;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class SupplierConfiguration :
        BaseDomainModelConfiguration<Supplier>
    {
        public override void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.ToTable("Suppliers");

            builder
                .HasOne(x => x.Locale)
                .WithMany(x => x.Suppliers)
                .HasForeignKey(x => x.LocaleId)
                .IsRequired();

            builder
                .HasIndex(x => x.Code)
                .HasDatabaseName("IX_Supplier_Code")
                .IsUnique();

            builder.HasOne(x => x.Agreement)
                .WithMany(x => x.Suppliers)
                .HasForeignKey(x => x.AgreementId)
                .IsRequired();

            builder.HasOne(x => x.PrintedHtmlFormInvoice)
                .WithMany(x => x.Suppliers)
                .HasForeignKey(x => x.PrintedHtmlFormInvoiceId);

            builder.HasOne(x => x.PrintedHtmlFormInvoiceReceipt)
                .WithMany(x => x.Suppliers2)
                .HasForeignKey(x => x.PrintedHtmlFormInvoiceReceiptId);
        }
    }
}
