﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class RateConfiguration :
        BaseDomainModelConfiguration<Rate>
    {
        public override void Configure(EntityTypeBuilder<Rate> builder)
        {
            builder.ToTable("Rates", SchemaType.Billing);
        }
    }
}
