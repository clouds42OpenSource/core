﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class InvoiceProductConfiguration : BaseDomainModelConfiguration<InvoiceProduct>
    {
        public override void Configure(EntityTypeBuilder<InvoiceProduct> builder)
        {
            builder.ToTable("InvoiceProducts", SchemaType.Billing);

            builder
                .HasOne(w => w.Invoice)
                .WithMany(w => w.InvoiceProducts)
                .HasForeignKey(x => x.InvoiceId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
