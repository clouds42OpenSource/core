﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceTypeActivityForAccountConfiguration :
        IEntityTypeConfiguration<BillingServiceTypeActivityForAccount>
    {
        public void Configure(EntityTypeBuilder<BillingServiceTypeActivityForAccount> builder)
        {
            builder.ToTable("BillingServiceTypeActivityForAccounts", SchemaType.Billing);

            builder
                .HasIndex(x => new { x.BillingServiceTypeId, x.AccountId })
                .HasDatabaseName("IX_BillingServiceTypeActivityForAccount")
                .IsUnique();

            builder
                .HasOne(x => x.BillingServiceType)
                .WithMany(x => x.BillingServiceTypesActivities)
                .HasForeignKey(x => x.BillingServiceTypeId)
                .IsRequired();

            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.BillingServiceTypeActivities)
                .HasForeignKey(x => x.AccountId)
                .IsRequired();
        }
    }
}
