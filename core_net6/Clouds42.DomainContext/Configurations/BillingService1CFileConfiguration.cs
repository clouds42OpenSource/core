﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingService1CFileConfiguration :
         BaseDomainModelConfiguration<BillingService1CFile>
    {
        public override void Configure(EntityTypeBuilder<BillingService1CFile> builder)
        {
            builder.ToTable("BillingService1CFiles", SchemaType.Billing);
        }
    }
}
