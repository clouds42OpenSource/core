﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AgentTransferBalanseRequestConfiguration :
        BaseDomainModelConfiguration<AgentTransferBalanseRequest>
    {
        public override void Configure(EntityTypeBuilder<AgentTransferBalanseRequest> builder)
        {
            builder.ToTable("AgentTransferBalanseRequests", SchemaType.Billing);
        }
    }
}
