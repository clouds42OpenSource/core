﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CoreHostingConfiguration :
        BaseDomainModelConfiguration<CoreHosting>
    {

        public override void Configure(EntityTypeBuilder<CoreHosting> builder)
        {
            builder.ToTable(nameof(CoreHosting), SchemaType.Segment);
        }
    }
}
