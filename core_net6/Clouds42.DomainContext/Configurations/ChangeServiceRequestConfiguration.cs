﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ChangeServiceRequestConfiguration : BaseDomainModelConfiguration<ChangeServiceRequest>
    {
        public override void Configure(EntityTypeBuilder<ChangeServiceRequest> builder)
        {
            builder.ToTable("ChangeServiceRequests", SchemaType.BillingCommits);
        }
    }
}
