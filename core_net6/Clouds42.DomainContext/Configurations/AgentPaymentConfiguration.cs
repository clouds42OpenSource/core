﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AgentPaymentConfiguration : BaseDomainModelConfiguration<AgentPayment>
    {
        public override void Configure(EntityTypeBuilder<AgentPayment> builder)
        {
            builder.ToTable("AgentPayments", SchemaType.Billing);
        }
    }
}
