﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.History;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class MainServiceResourcesChangesHistoryConfiguration :
        IEntityTypeConfiguration<MainServiceResourcesChangesHistory>
    {
        public void Configure(EntityTypeBuilder<MainServiceResourcesChangesHistory> builder)
        {
            builder.ToTable("MainServiceResourcesChanges", SchemaType.History);

            builder.HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.MainServiceResourcesChangesHistories)
                .HasForeignKey(x => x.AccountId)
                .IsRequired();

            builder
                .Property(x => x.ChangesDate)
                .HasColumnType("DATE");

            builder
                .HasIndex(x => new { x.AccountId, x.ChangesDate })
                .HasDatabaseName("IX_MainServiceResourcesChangesHistory")
                .IsUnique();
        }
    }
}
