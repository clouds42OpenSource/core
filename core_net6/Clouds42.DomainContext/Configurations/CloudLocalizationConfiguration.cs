﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudLocalizationConfiguration :
        IEntityTypeConfiguration<CloudLocalization>
    {
        public void Configure(EntityTypeBuilder<CloudLocalization> builder)
        {
            builder.ToTable("CloudLocalizations");

            builder.HasKey(x => new { x.Key, x.LocaleId });

            builder
                .HasOne(x => x.Locale)
                .WithMany(x => x.CloudLocalizations)
                .HasForeignKey(x => x.LocaleId)
                .IsRequired();

            builder
                .HasIndex(x => x.Value)
                .HasDatabaseName("IX_CloudLocalization");
        }
    }
}
