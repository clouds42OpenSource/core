﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AcDbAccessConfiguration : IEntityTypeConfiguration<AcDbAccess>
    {
        public void Configure(EntityTypeBuilder<AcDbAccess> builder)
        {
            builder
                .HasOne(x => x.Account)
                .WithMany(x => x.AcDbAccesses)
                .HasForeignKey(x => x.AccountID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            
            builder
                .HasOne(x => x.Database)
                .WithMany(x => x.AcDbAccesses)
                .HasForeignKey(x => x.AccountDatabaseID)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
