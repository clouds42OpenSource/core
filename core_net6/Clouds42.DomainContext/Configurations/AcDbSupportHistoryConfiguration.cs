﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AcDbSupportHistoryConfiguration : IEntityTypeConfiguration<AcDbSupportHistory>
    {
        public void Configure(EntityTypeBuilder<AcDbSupportHistory> builder)
        {
            builder
                .HasOne(x => x.AcDbSupport)
                .WithMany(x => x.AcDbSupportHistories)
                .HasForeignKey(x => x.AccountDatabaseId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
