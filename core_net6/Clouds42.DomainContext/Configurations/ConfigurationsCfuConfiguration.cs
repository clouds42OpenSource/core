﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ConfigurationsCfuConfiguration :
        BaseDomainModelConfiguration<ConfigurationsCfu>
    {
        public override void Configure(EntityTypeBuilder<ConfigurationsCfu> builder)
        {
            builder.ToTable(nameof(ConfigurationsCfu));
        }
    }
}
