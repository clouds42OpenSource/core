﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ServiceExtensionDatabaseConfiguration :
        IEntityTypeConfiguration<ServiceExtensionDatabase>
    {
        public void Configure(EntityTypeBuilder<ServiceExtensionDatabase> builder)
        {
            builder.ToTable("ServiceExtensionDatabases");

            builder.HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder
                .HasOne(x => x.BillingService)
                .WithMany(x => x.ServiceExtensionDatabases)
                .HasForeignKey(x => x.ServiceId)
                .IsRequired();
            
            builder
                .HasOne(x => x.AccountDatabase)
                .WithMany(x => x.ServiceExtensionDatabases)
                .HasForeignKey(x => x.AccountDatabaseId)
                .IsRequired();

            builder
                .HasIndex(x => new { x.ServiceId, x.AccountDatabaseId })
                .HasDatabaseName("IX_ServiceExtensionDatabase")
                .IsUnique();
        }
    }
}
