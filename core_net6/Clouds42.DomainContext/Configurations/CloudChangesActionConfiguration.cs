﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CloudChangesActionConfiguration : IEntityTypeConfiguration<CloudChangesAction>
    {
        public void Configure(EntityTypeBuilder<CloudChangesAction> builder)
        {
            builder
                .HasOne(x => x.CloudChangesGroup)
                .WithMany(x => x.CloudChangesActions)
                .HasForeignKey(x => x.GroupId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
