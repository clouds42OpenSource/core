﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceExtensionConfiguration :
        BaseDomainModelConfiguration<BillingServiceExtension>
    {
        public override void Configure(EntityTypeBuilder<BillingServiceExtension> builder)
        {
            builder.ToTable(nameof(BillingServiceExtension), SchemaType.Billing);

            builder
                .HasIndex(x => x.Service1CFileName)
                .HasDatabaseName("IX_BillingServiceExtension")
                .IsUnique();
        }
    }
}
