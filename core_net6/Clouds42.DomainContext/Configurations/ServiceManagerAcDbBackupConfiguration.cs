﻿using Clouds42.Domain.DataModels.AccountDatabases;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ServiceManagerAcDbBackupConfiguration :
        IEntityTypeConfiguration<ServiceManagerAcDbBackup>
    {
        public void Configure(EntityTypeBuilder<ServiceManagerAcDbBackup> builder)
        {
            builder.ToTable("ServiceManagerAcDbBackups");
            builder.HasKey(x => x.AccountDatabaseBackupId);

            builder
                .HasOne(x => x.AccountDatabaseBackup)
                .WithOne(x => x.ServiceManagerAcDbBackup)
                .IsRequired(false);

            builder
                .HasIndex(x => x.ServiceManagerAcDbBackupId)
                .HasDatabaseName("IX_Backup_ServiceManagerId")
                .IsUnique();
        }
    }
}
