﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class CoreWorkerTasksQueueConfiguration : BaseDomainModelConfiguration<CoreWorkerTasksQueue>
    {
        public override void Configure(EntityTypeBuilder<CoreWorkerTasksQueue> builder)
        {
            builder.ToTable(nameof(CoreWorkerTasksQueue));

            builder
                .HasOne(x => x.TaskParameter)
                .WithOne(x => x.Task)
                .IsRequired(false);
        }
    }
}
