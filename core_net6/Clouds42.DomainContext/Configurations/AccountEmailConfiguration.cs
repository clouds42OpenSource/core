﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountEmailConfiguration :
      BaseDomainModelConfiguration<AccountEmail>
    {
        public override void Configure(EntityTypeBuilder<AccountEmail> builder)
        {
            builder.ToTable("AccountEmails");
        }
    }
}
