﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class BillingServiceConfiguration :
        BaseDomainModelConfiguration<BillingService>
    {
        public override void Configure(EntityTypeBuilder<BillingService> builder)
        {

            builder.ToTable("Services", SchemaType.Billing);

            builder
                .HasIndex(x => x.Key)
                .HasDatabaseName("IX_BillingService_Key")
                .IsUnique();

            builder
                .HasOne(x => x.IconCloudFile)
                .WithMany(x => x.BillingServices)
                .HasForeignKey(x => x.IconCloudFileId)
                .IsRequired(false);

            builder
                .HasOne(x => x.OriginalIconCloudFile)
                .WithMany(x => x.BillingServices2)
                .HasForeignKey(x => x.OriginalIconCloudFileId)
                .IsRequired(false);

            builder
                .HasOne(x => x.InstructionServiceCloudFile)
                .WithMany(x => x.BillingServices3)
                .HasForeignKey(x => x.InstructionServiceCloudFileId)
                .IsRequired(false);

            builder
                .HasOne(x => x.AccountOwner)
                .WithMany(x => x.BillingServices)
                .HasForeignKey(x => x.AccountOwnerId)
                .IsRequired(false);

            builder
                .HasMany(x => x.BillingServiceTypes)
                .WithOne(x => x.Service)
                .HasForeignKey(x => x.ServiceId)
                .IsRequired();
        }
    }
}
