﻿using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class ResourceEntityConfiguration :
        BaseDomainModelConfiguration<Resource>
    {
        public override void Configure(EntityTypeBuilder<Resource> builder)
        {
            builder.ToTable("Resources", SchemaType.Billing);


            builder
                .HasIndex(x => x.Subject)
                .HasDatabaseName("IX_Resources_Subject");
            
            builder
                .HasOne(x => x.BillingAccounts)
                .WithMany(x => x.Resources)
                .HasForeignKey(x => x.AccountId)
                .IsRequired();

            builder
                .HasOne(x => x.BillingServiceType)
                .WithMany(x => x.Resources)
                .HasForeignKey(x => x.BillingServiceTypeId)
                .IsRequired();
        }
    }
}
