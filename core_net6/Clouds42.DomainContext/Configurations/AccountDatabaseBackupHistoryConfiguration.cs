﻿using Clouds42.Domain.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clouds42.DomainContext.Configurations
{
    public class AccountDatabaseBackupHistoryConfiguration :
     BaseDomainModelConfiguration<AccountDatabaseBackupHistory>
    {
        public override void Configure(EntityTypeBuilder<AccountDatabaseBackupHistory> builder)
        {
            builder.ToTable("AccountDatabaseBackupHistories");
        }
    }
}
