﻿using System;
using Clouds42.DomainContext.Context;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.DomainContext
{
    public static class DependencyInjections
    {
        private static readonly ILogger42 Logger = new SerilogLogger42();
        public static IServiceCollection AddClouds42DbContext(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            if (configuration.GetSection("ConnectionStrings:DatabaseType").Value
                    ?.Equals(ProviderTypeConstants.MsSql, StringComparison.InvariantCultureIgnoreCase) ?? true)
            {
                serviceCollection
                    .AddDbContext<Clouds42DbContext>(options =>
                    {
                        options
                            .UseLazyLoadingProxies()
                            .UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

                        if (configuration.GetSection("EnableEFLogging").Value == "1")
                        {
                            options.LogTo(Logger.Info);
                        }
                    });
            }

            else
            {
                serviceCollection
                    .AddDbContext<Clouds42DbContext>(options =>
                    {
                        options
                            .UseLazyLoadingProxies()
                            .UseNpgsql(configuration.GetConnectionString("DefaultConnection"));

                        if (configuration.GetSection("EnableEFLogging").Value == "1")
                        {
                            options.LogTo(Logger.Info);
                        }
                    });
            }

            return serviceCollection;
        }
    }
}
