﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccessMapping;
using Clouds42.Domain.DataModels.AccountDatabases;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.DataModels.CoreWorkerModels;
using Clouds42.Domain.DataModels.History;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Domain.DataModels.JsonWebToken;
using Clouds42.Domain.DataModels.Link;
using Clouds42.Domain.DataModels.Mailing;
using Clouds42.Domain.DataModels.Marketing;
using Clouds42.Domain.DataModels.Notification;
using Clouds42.Domain.DataModels.Security;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.DataModels.WebSockets;
using Clouds42.DomainContext.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.DomainContext.Context
{
    public class Clouds42DbContext : DbContext
    {

        public Clouds42DbContext()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }
        public Clouds42DbContext(DbContextOptions<Clouds42DbContext> options) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
        }

        #region DbSets

        public DbSet<ManageDbOnDelimitersAccess> ManageDbOnDelimitersAccesses { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<AccountRate> AccountRates { get; set; }
        public DbSet<BillingAccount> BillingAccounts { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<ResourcesConfiguration> ResourcesConfigurations { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceProduct> InvoiceProducts { get; set; }
        public DbSet<InvoiceReceipt> InvoiceReceipts { get; set; }
        public DbSet<DiskResource> DiskResources { get; set; }
        public DbSet<SessionResource> SessionResources { get; set; }
        public DbSet<BillingServiceType> ServiceTypes { get; set; }
        public DbSet<BillingServiceTypeRelation> ServiceTypeRelations { get; set; }
        public DbSet<BillingService> Services { get; set; }
        public DbSet<IndustryDependencyBillingService> IndustriesDependencyBillingServices { get; set; }
        public DbSet<BillingService1CFile> BillingService1CFiles { get; set; }
        public DbSet<FlowResourcesScope> FlowResourcesScope { get; set; }
        public DbSet<AgentWallet> AgentWallets { get; set; }
        public DbSet<AgentPayment> AgentPayments { get; set; }
        public DbSet<AgentPaymentRelation> AgentPaymentRelations { get; set; }
        public DbSet<AgentCashOutRequest> AgentCashOutRequests { get; set; }
        public DbSet<AgentCashOutRequestFile> AgentCashOutRequestFile { get; set; }
        public DbSet<AgencyAgreement> AgencyAgreements { get; set; }
        public DbSet<AgentRequisites> AgentRequisites { get; set; }
        public DbSet<LegalPersonRequisites> LegalPersonRequisites { get; set; }
        public DbSet<PhysicalPersonRequisites> PhysicalPersonRequisites { get; set; }
        public DbSet<SoleProprietorRequisites> SoleProprietorRequisites { get; set; }
        public DbSet<AgentRequisitesFile> AgentRequisitesFiles { get; set; }
        public DbSet<AgentTransferBalanseRequest> AgentTransferBalanseRequests { get; set; }
        public DbSet<RecalculationServiceCostAfterChange> RecalculationServiceCostAfterChanges { get; set; }
        public DbSet<BillingServiceTypeActivityForAccount> BillingServiceTypeActivityForAccounts { get; set; }
        public DbSet<Service1CFileDbTemplateRelation> Service1CFileDbTemplateRelations { get; set; }
        public DbSet<Service1CFileVersion> Service1CFileVersions { get; set; }
        public DbSet<ServiceManager1CFileRelation> ServiceManager1CFileRelations { get; set; }
        public DbSet<BillingServiceExtension> BillingServiceExtensions { get; set; }
        public DbSet<MyDatabasesResource> MyDatabasesResources { get; set; }
        public DbSet<ConfigurationServiceTypeRelation> ConfigurationServiceTypeRelations { get; set; }
        public DbSet<LimitOnFreeCreationDbForAccount> LimitOnFreeCreationDbForAccounts { get; set; }
        public DbSet<MyDiskPropertiesByAccount> MyDiskPropertiesByAccounts { get; set; }
        public DbSet<AccountRequisites> AccountRequisites { get; set; }
        public DbSet<AccountConfiguration> AccountConfigurations { get; set; }
        public DbSet<SavedPaymentMethod> SavedPaymentMethods { get; set; }
        public DbSet<AccountAutoPayConfiguration> AccountAutoPayConfigurations { get; set; }
        public DbSet<SavedPaymentMethodBankCard> SavedPaymentMethodBankCards { get; set; }
        public DbSet<ChangeServiceRequest> ChangeServiceRequests { get; set; }
        public DbSet<CreateServiceRequest> CreateServiceRequests { get; set; }
        public DbSet<EditServiceRequest> EditServiceRequests { get; set; }
        public DbSet<BillingServiceChanges> BillingServiceChanges { get; set; }
        public DbSet<IndustryDependencyBillingServiceChange> IndustryDependencyBillingServiceChanges { get; set; }
        public DbSet<BillingServiceTypeChange> BillingServiceTypeChanges { get; set; }
        public DbSet<LocaleConfiguration> LocaleConfiguration { get; set; }
        public DbSet<CloudConfiguration> CloudConfigurations { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountUser> AccountUsers { get; set; }
        public DbSet<AccountUserRole> AccountUserRoles { get; set; }
        public DbSet<AccountEmail> AccountEmails { get; set; }
        public DbSet<AccountUserSession> AccountUserSessions { get; set; }
        public DbSet<Locale> Locale { get; set; }
        public DbSet<DbTemplate> DbTemplates { get; set; }
        public DbSet<DbTemplateUpdate> DbTemplateUpdates { get; set; }
        public DbSet<AccountDatabase> AccountDatabases { get; set; }
        public DbSet<AccountDatabaseBackup> AccountDatabaseBackups { get; set; }
        public DbSet<AccountDatabaseBackupHistory> AccountDatabaseBackupHistories { get; set; }
        public DbSet<ServiceManagerAcDbBackup> ServiceManagerAcDbBackups { get; set; }
        public DbSet<AccountDatabaseUpdateVersionMapping> AccountDatabaseUpdateVersionMappings { get; set; }
        public DbSet<AcDbAccRolesJho> AcDbAccRolesJhOes { get; set; }
        public DbSet<AcDbSupport> AcDbSupports { get; set; }
        public DbSet<AcDbSupportHistory> AcDbSupportHistories { get; set; }
        public DbSet<AcDbAccess> AcDbAccesses { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<SupplierReferralAccount> SupplierReferralAccounts { get; set; }
        public DbSet<PrintedHtmlForm> PrintedHtmlForms { get; set; }
        public DbSet<PrintedHtmlFormFile> PrintedHtmlFormFiles { get; set; }
        public DbSet<AccountCSResourceValue> AccountCsResourceValues { get; set; }
        public DbSet<CloudTerminalServer> CloudTerminalServers { get; set; }
        public DbSet<PublishNodeReference> PublishNodeReference { get; set; }
        public DbSet<CloudServicesContentServerNode> CloudServicesContentServerNode { get; set; }
        public DbSet<CloudCore> CloudCores { get; set; }
        public DbSet<ProvidedService> ProvidedServices { get; set; }
        public DbSet<CloudServicesBackupStorage> CloudServicesBackupStorages { get; set; }
        public DbSet<CloudServicesContentServer> CloudServicesContentServers { get; set; }
        public DbSet<CloudServicesEnterpriseServer> CloudServicesEnterpriseServers { get; set; }
        public DbSet<CloudServicesFileStorageServer> CloudServicesFileStorageServers { get; set; }
        public DbSet<CloudServicesGatewayTerminal> CloudServicesGatewayTerminals { get; set; }
        public DbSet<CloudServicesSegmentStorage> CloudServicesSegmentStorages { get; set; }
        public DbSet<CloudServicesSqlServer> CloudServicesSqlServers { get; set; }
        public DbSet<CloudServicesTerminalFarm> CloudServicesTerminalFarms { get; set; }
        public DbSet<CloudServicesSegmentTerminalServer> CloudServicesSegmentTerminalServers { get; set; }
        public DbSet<CloudServicesSegment> CloudServicesSegments { get; set; }
        public DbSet<CloudChangesAction> CloudChangesActions { get; set; }
        public DbSet<CloudChangesGroup> CloudChangesGroups { get; set; }
        public DbSet<CloudChanges> CloudChanges { get; set; }
        public DbSet<CoreWorker> CoreWorkers { get; set; }
        public DbSet<CoreWorkerTask> CoreWorkerTasks { get; set; }
        public DbSet<CoreWorkerTasksQueue> CoreWorkerTasksQueues { get; set; }
        public DbSet<CoreWorkerTaskParameter> CoreWorkerTaskParameters { get; set; }
        public DbSet<CoreWorkerAvailableTasksBag> CoreWorkerTasksBags { get; set; }
        public DbSet<NotificationBuffer> NotificationBuffers { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<AvailableMigration> AvailableMigrations { get; set; }
        public DbSet<Configurations1C> Configurations1C { get; set; }
        public DbSet<ConfigurationsCfu> ConfigurationsCfu { get; set; }
        public DbSet<DbTemplateDelimiters> DbTemplateDelimiters { get; set; }
        public DbSet<AccountDatabaseOnDelimiters> AccountDatabaseDelimiters { get; set; }
        public DbSet<PlatformVersionReference> PlatformVersionReferences { get; set; }
        public DbSet<CloudFile> CloudFiles { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<UploadedFile> UploadedFiles { get; set; }
        public DbSet<AccountSaleManager> AccountSaleManagers { get; set; }
        public DbSet<ServiceAccount> ServiceAccounts { get; set; }
        public DbSet<DelimiterSourceAccountDatabase> DelimiterSourceAccountDatabases { get; set; }
        public DbSet<WebSocketConnection> WebSocketConnections { get; set; }
        public DbSet<AccountUserJsonWebToken> AccountUerJsonWebTokens { get; }
        public DbSet<AccessMappingRule> AccessMappingRules { get; }
        public DbSet<ServiceExtensionDatabase> ServiceExtensionDatabases { get; }
        public DbSet<AccountDatabaseAutoUpdateResult> AccountDatabaseAutoUpdateResults { get; set; }
        public DbSet<AcDbSupportHistoryTasksQueueRelation> AcDbSupportHistoryTasksQueueRelations { get; set; }
        public DbSet<HtmlTemplate> HtmlTemplates { get; set; }
        public DbSet<DatabaseLaunchRdpStartHistory> DatabaseLaunchRdpStartHistories { get; }
        public DbSet<ConfigurationDbTemplateRelation> ConfigurationDbTemplateRelations { get; }
        public DbSet<TerminationSessionsInDatabase> TerminationSessionsInDatabases { get; }
        public DbSet<CloudLocalization> CloudLocalizations { get; }
        public DbSet<CoreWorkerTasksQueueLive> CoreWorkerTasksQueuesLive { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<AutoUpdateNode> AutoUpdateNodes { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<AccountCorpCloud> AccountCorpClouds { get; set; }
        public DbSet<NotificationSettings> NotificationSettings { get; set; }
        public DbSet<AccountUserNotificationSettings> AccountUserNotificationSettings { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<BillingServiceTypeCostChangedHistory> BillingServiceTypeCostChangedHistory { get; set; }
        public DbSet<MainServiceResourcesChangesHistory> MainServiceResourcesChanges { get; set; }
        public DbSet<AccountLocaleChangesHistory> AccountLocaleChanges { get; set; }
        public DbSet<ConfigurationNameVariation> ConfigurationNameVariations { get; set; }
        public DbSet<LetterTemplate> LetterTemplates { get; set; }
        public DbSet<LetterAdvertisingBannerRelation> LetterAdvertisingBannerRelations { get; set; }
        public DbSet<AdvertisingBannerAudience> AdvertisingBannerAudiences { get; set; }
        public DbSet<AdvertisingBannerTemplate> AdvertisingBannerTemplates { get; set; }
        public DbSet<AdvertisingBannerAudienceLocaleRelation> AdvertisingBannerAudienceLocaleRelations { get; set; }
        public DbSet<MigrationAccountDatabaseHistory> MigrationAccountDatabaseHistories { get; set; }
        public DbSet<MigrationAccountHistory> MigrationAccountHistories { get; set; }
        public DbSet<MigrationHistory> MigrationHistories { get; set; }
        public DbSet<ItsAuthorizationData> ItsAuthorizationData { get; set; }
        public DbSet<EmailSmsVerification> EmailSmsVerification { get; set; }
        public DbSet<CoreHosting> CoreHosting { get; set; }
        public DbSet<UpdateNodeQueue> UpdateNodeQueues { get; set; }
        public DbSet<ProcessFlow> ProcessFlows { get; set; }
        public DbSet<ActionFlow> ActionFlows { get; set; }
        public DbSet<CloudService> CloudServices { get; set; }
        public DbSet<AccountAdditionalCompany> AccountAdditionalCompanies { get; set; }
        public DbSet<ArticleTransaction> ArticleTransactions { get; set; }
        public DbSet<ArticleWallets> ArticleWallets { get; set; }
        public DbSet<AccountFile> AccountFiles { get; set; }
        public DbSet<AccountUserBitDepth> AccountUserBitDepths { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<AccountUserDepartment> AccountUserDepartments { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SavedPaymentMethod>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Connection>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<EmailSmsVerification>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<NotificationSettings>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Notification>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<AccountUserNotificationSettings>()
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<AcDbSupport>(entity =>
            {
                entity.Property(e => e.Major)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("CurrentVersion", '.', 1)), '')::INT,  0)""", stored: true);
                 
                entity.Property(e => e.Minor)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("CurrentVersion", '.', 2)), '')::INT,  0)""", stored: true);

                entity.Property(e => e.Patch)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("CurrentVersion", '.', 3)), '')::INT,  0)""", stored: true);

                entity.Property(e => e.Build)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("CurrentVersion", '.', 4)), '')::INT,  0)""", stored: true);
            });

            modelBuilder.Entity<ConfigurationsCfu>(entity =>
            {
                entity.Property(e => e.Major)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("Version", '.', 1)), '')::INT,  0)""", stored: true);

                entity.Property(e => e.Minor)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("Version", '.', 2)), '')::INT,  0)""", stored: true);

                entity.Property(e => e.Patch)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("Version", '.', 3)), '')::INT,  0)""", stored: true);

                entity.Property(e => e.Build)
                    .HasComputedColumnSql("""COALESCE( NULLIF(trim(split_part("Version", '.', 4)), '')::INT,  0)""", stored: true);
            });


            var entityTypes = modelBuilder.Model.GetEntityTypes();

            var cascadeFKs = entityTypes
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(WaitInfosConfiguration).Assembly);


            modelBuilder
                .Entity<AccountUserNotificationSettings>()
                .HasOne(x => x.AccountUser)
                .WithMany(x => x.NotificationsSettings)
                .OnDelete(DeleteBehavior.Cascade);

            foreach (var entityType in entityTypes)
            {
                var properties = entityType.GetProperties()
                    .Where(p => p.ClrType == typeof(DateTime));

                foreach (var property in properties)
                {
                    property.SetColumnType("timestamp without time zone");
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                return;
            }

            var type = ConnectionStringProvider.GetProviderType();

            switch (type)
            {
                case ProviderTypeConstants.MsSql:
                    optionsBuilder.UseSqlServer(ConnectionStringProvider.GetConnectionStringFromEnvironment());
                    break;
                case ProviderTypeConstants.PostgreSql:
                    optionsBuilder.UseNpgsql(ConnectionStringProvider.GetConnectionStringFromEnvironment());
                    break;
                default:
                    throw new NotImplementedException(
                        $"Не установлен тип провайдера в переменных среды. Убедитесь что значение CORE42_DB_PROVIDER_TYPE заполнено (возможные значения {ProviderTypeConstants.MsSql} | {ProviderTypeConstants.PostgreSql}");
            }
        }
    }

    /// <summary>
    /// Провайдер строки подключения к базе данных.
    /// </summary>
    public static class ConnectionStringProvider
    {
        private const string ConnectionStringEnvironmentKey = "CORE42_DB_CONNECTION_STRING";
        private const string ProviderTypeEnvironmentKey = "CORE42_DB_PROVIDER_TYPE";

        public static string GetConnectionStringFromEnvironment()
        {
            return Environment.GetEnvironmentVariable(ConnectionStringEnvironmentKey, EnvironmentVariableTarget.User);
        }

        public static string GetProviderType()
        {
            return Environment.GetEnvironmentVariable(ProviderTypeEnvironmentKey, EnvironmentVariableTarget.User);
        }
    }
}
