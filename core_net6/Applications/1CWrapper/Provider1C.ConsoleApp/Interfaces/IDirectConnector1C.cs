﻿using Clouds42.Tools1C.Helpers;

namespace Provider1C.ConsoleApp.Interfaces
{
    /// <summary>
    /// Коннектор для подключения к 1С напрямую
    /// </summary>
    public interface IDirectConnector1C
    {
        /// <summary>
        /// Запустить 1С с параметрами.
        /// </summary>
        /// <param name="param">параметр запуска.</param>
        /// <param name="isEnterprise">Признак Enterprise</param>
        /// <param name="logOut">Логировать процесс.</param>
        /// <returns>Признак успешности.</returns>
        bool StartApp(string param, bool isEnterprise, bool logOut = false);

        /// <summary>
        /// Обновить информационную базу.
        /// </summary>
        /// <param name="logResult">Записывает лог выполненич обнавления версии конфигурации</param>
        /// <param name="cfuFilePath">Путь к конфигурационному файлу для обновления версии</param>
        /// <returns>Возвращает true, при успешном обновлении, иначе false..</returns>
        bool UpdateVersion(string cfuFilePath, out LogUpdate1CParser.Result logResult);

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Возвращает true, при успешном применении обновлений, иначе false..</returns>
        bool ApplyUpdate();
    }
}