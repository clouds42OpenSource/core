﻿using System;
using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Provider1C.ConsoleApp.Interfaces
{
    /// <summary>
    /// Коннектор для подключения к 1С через COM
    /// </summary>
    public interface IComConnector1C: IDisposable
    {
        /// <summary>
        /// Подключится к COM 1С
        /// </summary>
        /// <returns>При успешном подключении возвращается true, иначе false</returns>
        bool Connect();

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>
        /// <returns>Метаданные информационной базы</returns>     
        ConnectorResultDto<MetadataResultDto> GetMetadataInfo();

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>
        void ApplyUpdates();
    }
}