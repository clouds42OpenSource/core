﻿using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.Enums._1C;

namespace Provider1C.ConsoleApp.Models
{
    /// <summary>
    /// Параметры для подключения к 1С через COM
    /// </summary>
    internal sealed class ComConnector1CParametersModel : IPlatformDto
    {
        /// <summary>
        /// Название базы
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Если true, то база является файловой иначе серверной.
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Если база серверная, то содержит адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Если база НЕ серверная, то содержит путь к папке базы
        /// </summary>
        public string PathToDatabaseFolder { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string DbAdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        public string DbAdminPassword { get; set; }

        /// <summary>
        /// Путь до 1С движка, к папки bin
        /// </summary>
        public string Engine1CFolderBinPath { get; set; }

        /// <summary>
        /// Тип платформы 1С
        /// </summary>
        public PlatformType Engine1CType { get; set; }

        /// <summary>
        /// Версия платформы 1С
        /// </summary>
        public string Engine1CVersion { get; set; }

        /// <summary>
        /// Получить строку параметров для серверной базы
        /// </summary>
        /// <returns>Строка параметров</returns>
        private string GetServerDatabaseArguments()
            => $"Srvr=\'{ConnectionAddress}\';Ref=\'{DatabaseName}\';Usr=\'{DbAdminLogin}\';pwd=\'{DbAdminPassword}\';";

        /// <summary>
        /// Получить строку параметров для файловой базы
        /// </summary>
        /// <returns>Строка параметров</returns>
        private string GetFileDatabaseArguments()
            => $"File=\'{PathToDatabaseFolder}\';Usr=\'{DbAdminLogin}\';pwd=\'{DbAdminPassword}\';";

        /// <summary>
        /// Получить строку параметров для базы
        /// </summary>
        /// <returns>Строка параметров</returns>
        public string GetArguments()
        {
            return IsFile
                ? GetFileDatabaseArguments()
                : GetServerDatabaseArguments();
        }

        /// <summary>
        /// Версия платформы 1С.
        /// </summary>
        string IPlatformDto.PlatformVersion
        {
            get => Engine1CVersion;
            set => Engine1CVersion = value;
        }

        /// <summary>
        /// Путь к платформе 1С
        /// </summary>
        string IPlatformDto.PathToPlatform
        {
            get => Engine1CFolderBinPath;
            set => Engine1CFolderBinPath = value;
        }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        PlatformType IPlatformDto.PlatformType
        {
            get => Engine1CType;
            set => Engine1CType = value;
        }
    }
}
