﻿namespace Provider1C.ConsoleApp.Models
{
    /// <summary>
    /// Параметры для подключения к 1С напрямую через 1C приложение
    /// </summary>
    public sealed class DirectConnector1CParametersModel
    {
        /// <summary>
        /// Название базы
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Если <code>true</code>, то база является файловой иначе серверной.
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Если база серверная, то содержит адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Если база НЕ серверная, то содержит путь к папке бвзы
        /// </summary>
        public string PathToDatabaseFolder { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string DbAdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        public string DbAdminPassword { get; set; }

        /// <summary>
        /// Путь до 1С движка, к папки bin
        /// </summary>
        public string Engine1CFolderBinPath { get; set; }        
    }
}
