﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Clouds42.Common.Constants;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
 
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Tools1C.Helpers;
using Provider1C.ConsoleApp.Interfaces;
using Provider1C.ConsoleApp.Models;
using Provider1C.ConsoleApp.Utilities;

namespace Provider1C.ConsoleApp.Connectors
{
    /// <summary>
    /// Коннектор к информационной базе напрямую помощи команд к 1С
    /// </summary>
    internal sealed class DirectConnector1C: IDirectConnector1C
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger42 _logger = new SerilogLogger42();

        /// <summary>
        /// Параметры для подключения к базе 1С напрямую через приложение 1С
        /// </summary>
        private readonly DirectConnector1CParametersModel _directConnector1CParameters;

        /// <summary>
        /// Справочник кодов выхода приложения
        /// </summary>
        private readonly IDictionary<int, bool> _processExitCodeDictionary;

        /// <summary>
        /// Создание нового объекта класса <see cref="DirectConnector1C"/>
        /// </summary>
        /// <param name="directConnector1CParameters">Параметры для подключения к 1С</param>
        public DirectConnector1C(DirectConnector1CParametersModel directConnector1CParameters)
        {
            SerilogConfig.ConfigureSerilog(new LocalLogsConfigDto(nameof(Provider1C)));
            _directConnector1CParameters = directConnector1CParameters;
            _processExitCodeDictionary = new Dictionary<int, bool>
            {
                { 0, true },
                { 1, false }
            };
        }

        /// <summary>
        /// Запустить 1С с параметрами.
        /// </summary>
        /// <param name="param">параметр запуска.</param>
        /// <param name="isEnterprise">Признак Enterprise</param>
        /// <param name="logOut">Логировать процесс.</param>
        /// <returns>Признак успешности.</returns>
        public bool StartApp(string param, bool isEnterprise, bool logOut = false)
        {            
            var commandLine = $"{GetStartLine(isEnterprise)} {param}";

            if (logOut)
                commandLine += $" /out \"{Log1CPathUtility.GetPathOutByTime(DateTime.Now, _directConnector1CParameters.DatabaseName)}\"";

            _logger.Debug($"[StartApp] {_directConnector1CParameters.DatabaseName} {commandLine}");            

            var process = new Process { StartInfo = { FileName = Get1CExePath(), Arguments = commandLine } };

            try
            {
                var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetTehSupportTimeout();

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                process.Start();
                process.WaitForExit(timeout);
                stopwatch.Stop();

                if (process.HasExited)
                {
                    _logger.Debug($"[StartApp] {_directConnector1CParameters.DatabaseName} end. duration - {stopwatch.Elapsed.Hours}:{stopwatch.Elapsed.Minutes}:{stopwatch.Elapsed.Seconds}.{stopwatch.Elapsed.Milliseconds / 10}");
                    return true;
                }

                _logger.Debug($"[StartApp] {_directConnector1CParameters.DatabaseName} Not end");

                process.Kill();
                
                return false;
            }
            catch (Exception ex)
            {
                _logger.Error(ex,$"[Ошибка запуска 1С с параметрами]{_directConnector1CParameters.DatabaseName}");
                return false;
            }
        }                                                    

        private string GetStartLine(bool isEnterprise)
        {
            var commandLine = isEnterprise ? "ENTERPRISE" : "DESIGNER";

            if (_directConnector1CParameters.IsFile)
                commandLine += $" /F\"{_directConnector1CParameters.PathToDatabaseFolder}\"";
            else
                commandLine += $" /S\"{_directConnector1CParameters.ConnectionAddress.Split(';').FirstOrDefault()}\\{_directConnector1CParameters.DatabaseName}\"";

            commandLine += $" /N \"{_directConnector1CParameters.DbAdminLogin}\"";

            if (!string.IsNullOrEmpty(_directConnector1CParameters.DbAdminPassword))
                commandLine += $" /P \"{_directConnector1CParameters.DbAdminPassword}\"";

            return commandLine;
        }


        private static LogUpdate1CParser.Result Parse1CLogFile(string pathTo1CLogFile)
        {
            var parser = new LogUpdate1CParser();
            return parser.Parse(pathTo1CLogFile);
        }

        /// <summary>
        /// Обновить информационную базу.
        /// </summary>
        /// <param name="cfuFilePath">Путь к конфигурационному файлу для обновления версии</param>
        /// <param name="logResult">Результат выполнения из лога 1С</param>
        /// <returns>Результат выполнения</returns>    
        public bool UpdateVersion(string cfuFilePath, out LogUpdate1CParser.Result logResult)
        {
            var logFilePath = Log1CPathUtility.GetPathOutByName(_directConnector1CParameters.DatabaseName,
                Path.GetFileNameWithoutExtension(cfuFilePath));

            var commandLine = GetStartLine(false) +
                              $" /UpdateCfg \"{cfuFilePath}\" /UpdateDBCfg /Out {logFilePath}";

            _logger.Debug(commandLine);
            var process = new Process {StartInfo = {FileName = Get1CExePath(), Arguments = commandLine}};

            var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetAutoUpdateTimeout();

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            process.Start();
            process.WaitForExit(timeout);

            stopwatch.Stop();

            if (!process.HasExited)
            {
                _logger.Warn($"Обновление базы данных {_directConnector1CParameters.DatabaseName} прервано принудительно.");
                process.Kill();

                logResult = Parse1CLogFile(logFilePath);
                return false;
            }

            var fileName = Path.GetFileName(cfuFilePath);
            _logger.Debug(
                $"База данных {_directConnector1CParameters.DatabaseName} обновлена успешно к релизу {fileName}. Время обновления {stopwatch.ElapsedMilliseconds / 1000} секунд.");
            logResult = Parse1CLogFile(logFilePath);
            return true;
        }

        /// <summary>
        /// Принять обновления в инф. базе
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public bool ApplyUpdate()
        {
            var commandLine = $"{GetStartLine(true)} /C\"{CloudConfigurationProvider.AccountDatabase.Support.GetApplyUpdateCommand()}\"";
            _logger.Debug(commandLine);
            var timeout = CloudConfigurationProvider.AccountDatabase.Support.GetApplyUpdateTimeout();

            var applyUpdateProcess = new Process
            {
                StartInfo =
                {
                    FileName = Get1CExePath(),
                    Arguments = commandLine,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    ErrorDialog = false
                }
            };

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            applyUpdateProcess.Start();
            applyUpdateProcess.WaitForExit(timeout);
            stopwatch.Stop();

            var processRunningTime = stopwatch.ElapsedMilliseconds / 1000;

            return FinalizeApplyUpdateProcess(applyUpdateProcess, processRunningTime);
        }

        /// <summary>
        /// Обработать результат выполнения процесса принятия обновления
        /// </summary>
        /// <param name="applyUpdateProcess">Процесс принятия обновления</param>
        /// <param name="processRunningTime">Время выполнения процесса</param>
        /// <returns>Результат выхода процесса</returns>
        private bool FinalizeApplyUpdateProcess(Process applyUpdateProcess, long processRunningTime)
        {
            var logAdditionalParams =
                $"Время выполнения: {processRunningTime} секунд. Код выхода: {(applyUpdateProcess.HasExited ? applyUpdateProcess.ExitCode.ToString() : "[Время ожидания истекло]")}";

            if (!applyUpdateProcess.HasExited)
            {
                applyUpdateProcess.Kill();
                throw new InvalidOperationException(
                    $@"Ошибка принятия обновления в инф. базе {_directConnector1CParameters.DatabaseName}. {logAdditionalParams}");
            }

            var processExitResult = GetProcessExitResult(applyUpdateProcess.ExitCode);

            if (!processExitResult)
                throw new InvalidOperationException($@"Ошибка принятия обновлений в инф. базе {_directConnector1CParameters.DatabaseName}. {logAdditionalParams}");

            _logger.Trace(
                $@"В инф. базе {_directConnector1CParameters.DatabaseName} успешно приняты обновления. {logAdditionalParams}");

            return true;
        }

        /// <summary>
        /// Получить результат выхода процесса
        /// </summary>
        /// <param name="processExitCode">Код выхода процесса</param>
        /// <returns>Результат выхода процесса</returns>
        private bool GetProcessExitResult(int processExitCode)
        {
            return _processExitCodeDictionary.TryGetValue(processExitCode, out var value) && value;
        }

        /// <summary>
        /// Получить путь до exe 1С пердприятия на сервере.
        /// </summary>
        /// <returns></returns>
        private string Get1CExePath()
        {
            var path1C = Path.Combine(_directConnector1CParameters.Engine1CFolderBinPath, Client1CConstants.ThickNameWithExe);

            if (!File.Exists(path1C))
                throw new NotFoundException($"Отсутствует файл 1С {path1C} на сервере");

            _logger.Trace($"{_directConnector1CParameters.DatabaseName} :: {path1C}");

            return path1C;
        }
    }
}
