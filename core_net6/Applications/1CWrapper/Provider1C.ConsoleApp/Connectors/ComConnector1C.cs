﻿using System;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Tools1C.Helpers;
using Provider1C.Common.Exceptions;
using Provider1C.ConsoleApp.Interfaces;
using Provider1C.ConsoleApp.Models;
using Provider1C.ConsoleApp.Utilities;

namespace Provider1C.ConsoleApp.Connectors
{
    /// <summary>
    /// Коннектор к информационной базе по COM соединению.
    /// </summary>
    internal sealed class ComConnector1C : IComConnector1C
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger42 _logger = new SerilogLogger42();

        /// <summary>
        /// Регистратор для COM коннекторов
        /// </summary>
        private readonly ComConnectorRegistrar _comConnectorRegistrar;
        
        /// <summary>
        /// Параметры для подключения к базе 1С по COM
        /// </summary>
        private readonly ComConnector1CParametersModel _comConnector1CParameters;
        
        /// <summary>
        /// Коннектор к 1С подключённый по COM
        /// </summary>
        private dynamic _connector1C;

        /// <summary>
        /// Создание нового объекта класса <see cref="ComConnector1C"/>
        /// </summary>
        /// <param name="comConnector1CParameters">Параметры для подключения к 1С</param>
        public ComConnector1C(ComConnector1CParametersModel comConnector1CParameters)
        {
            SerilogConfig.ConfigureSerilog(new LocalLogsConfigDto(nameof(Provider1C)));
            _comConnector1CParameters = comConnector1CParameters;
            _comConnectorRegistrar = new ComConnectorRegistrar();
        }        


        /// <summary>
        /// Подключиться к информационной базе.
        /// </summary>
        [HandleProcessCorruptedStateExceptions]
        public bool Connect()
        {
            try
            {
                var connectString = _comConnector1CParameters.GetArguments();
                Trace($"Подключение: {connectString}");

                var comConnectorName = ComConnectorNameBuilderUtility.Build(_comConnector1CParameters);
                Trace($"Полученно имя коннектора {comConnectorName}");

                var connectorInstance = GetOrCreateConnectorInstance(comConnectorName);
                Trace($"Добавили или создали коннектор {comConnectorName}");
                _connector1C = connectorInstance.Connect(connectString);
            }
            catch (AccessViolationException ave)
            {
                var ex = new AccessViolationReplacementException(ave);
                var errorMessage = $"{ex.Message} {ex.InnerException?.Message}";
                Trace($"Ошибка подлючения : {errorMessage}");
                throw ex;
            }
            catch (Exception ex)
            {
                var errorMessage = $"{ex.Message} {ex.InnerException?.Message}";
                Trace($"Ошибка подлючения : {errorMessage}");
                throw;
            }

            return true;
        }

        /// <summary>
        /// Получить инстанс COM коннектора.
        /// </summary>
        /// <param name="comConnectorName">Имя COM коннектора.</param>       
        private dynamic GetOrCreateConnectorInstance(string comConnectorName)
        {
            try
            {
                return _comConnectorRegistrar.GetComConnectorInstance(_comConnector1CParameters);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Не зарегистрирован COM коннектор '{comConnectorName}' :: {ex.Message}";
                Trace(errorMessage);
                throw new COMException(errorMessage);
            }
        }


        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>        
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo()
        {
            try
            {
                return new ConnectorResultDto<MetadataResultDto>
                {
                    Result = GetMetadataInfoWrap(),
                    ConnectCode = ConnectCodeDto.Success
                };
            }
            catch (Exception ex)
            {
                var errorModel = Connector1CErrorParser.Parse(ex);
                return new ConnectorResultDto<MetadataResultDto>
                {
                    ConnectCode = errorModel.ConnectCode,
                    Message = errorModel.ErorMessage
                };
            }
        }

        /// <summary>
        /// Враппер к методу получение метаданных информационной базы.
        /// Примечание!!! Метод нужен для корректной обработки GC.Collect по утилизации объектов коннектора.
        /// </summary>        
        private MetadataResultDto GetMetadataInfoWrap()
        {
            return new MetadataResultDto(_connector1C.Метаданные.Версия, _connector1C.Метаданные.Синоним);
        }

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>        
        public void ApplyUpdates()
        {
            try
            {
                _connector1C.Обработки.ОбновлениеИнформационнойБазы.Создать().ВыполнитьОбновление();
                Trace($"Успешно приняты обновления для базы '{_comConnector1CParameters.GetArguments()}'");
                SetAdminRolesForUser(_comConnector1CParameters.DbAdminLogin);
                return;
            }
            catch (Exception ex)
            {
                Trace($"Принятие обновлений. Способ 1 - ошибка ? {ex.Message}");
            }

            try
            {
                _connector1C.ОбновлениеИнформационнойБазы.ВыполнитьОбновлениеИнформационнойБазы();
                Trace($"Успешно приняты обновления для базы '{_comConnector1CParameters.GetArguments()}'");
                TryApplyLegalUpdateWrap();
                SetAdminRolesForUser(_comConnector1CParameters.DbAdminLogin);
            }
            catch (Exception ex)
            {
                var message = $"Ошибка принятия обновлений способ 2. {ex.Message}";
                Trace(message);
                throw;
            }
        }

        /// <summary>
        /// Попытаться принять легальность полученных обновлений.
        /// Примечание!!! Метод нужен для корректной обработки GC.Collect по утилизации объектов коннектора
        /// </summary>
        /// <returns>Признак успешности.</returns>
        private void TryApplyLegalUpdateWrap()
        {
            try
            {
                _connector1C.ОбновлениеИнформационнойБазыСлужебный.ЗаписатьПодтверждениеЛегальностиПолученияОбновлений();
            }
            catch (Exception ex)
            {
                Trace($"Ошибка вызова ЗаписатьПодтверждениеЛегальностиПолученияОбновлений :: {ex}");
            }
        }

        /// <summary>
        /// Задать права администратора пользователю в 1С.       
        /// </summary>        
        private void SetAdminRolesForUser(string userName)
        {
            try
            {                
                var user = _connector1C.ПользователиИнформационнойБазы.НайтиПоИмени(userName);

                if (user == null)
                {
                    var message =
                        $"В информационной базе '{_comConnector1CParameters.GetArguments()}' не удалось найти пользователя '{userName}'";
                    Trace(message);
                    throw new NotFoundException(message);
                }

                TrySetRoleForUser(user, _connector1C.Метаданные.Роли.ПолныеПрава);
                TrySetRoleForUser(user, _connector1C.Метаданные.Роли.АдминистраторСистемы);

                Trace(
                    $"В информационной базе '{_comConnector1CParameters.GetArguments()}' пользователю '{userName}' выданы права 'ПолныеПрава' и 'АдминистраторСистемы'");
            }
            catch (Exception ex)
            {
                var message = $"Ошибка установки прав администратора пользователю '{userName}' в базе '{_comConnector1CParameters.GetArguments()}' :: {ex}";
                Trace(message);                
            }
        }

        /// <summary>
        /// Попытаться добавить роль пользователю.
        /// </summary>
        /// <param name="user">Пользователь 1С.</param>
        /// <param name="role">Роль 1С.</param>
        private void TrySetRoleForUser(dynamic user, dynamic role)
        {
            try
            {
                user.Роли.Добавить(role);
                user.Записать();
            }
            catch (Exception ex)
            {
                Trace($"Не удалось добавить роль {role} пользователю {user} :: {ex.Message}");
            }
        }

        /// <summary>
        /// Написать в лог трассировку.
        /// </summary>
        private void Trace(string message)
        {
            _logger.Info($"{_comConnector1CParameters.DatabaseName} :: {message}");
        }

        /// <summary>
        /// Освобождение ресурсов 
        /// </summary>
        public void Dispose()
        {
            _connector1C = null;
        }
    }
}
