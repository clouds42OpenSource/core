﻿using Provider1C.ConsoleApp.Connectors;
using Provider1C.ConsoleApp.Interfaces;
using Provider1C.ConsoleApp.Models;

namespace Provider1C.ConsoleApp.Helpers
{
    /// <summary>
    /// Помощник по созданию коннекторов к 1С
    /// </summary>
    internal sealed class Connector1CCreator: IConnector1CCreator
    {
        /// <summary>
        /// Создать прямой коннектр обращение напрямую к 1С НЕ через COM
        /// </summary>
        /// <param name="connectTo1CDatabaseArguments">Параметры для подключения к 1С</param>
        /// <returns>Возвращает коннектор к 1С</returns>
        public IDirectConnector1C CreateDirectConnector1C(DirectConnector1CParametersModel connectTo1CDatabaseArguments)
        {
            return new DirectConnector1C(connectTo1CDatabaseArguments);
        }

        /// <summary>
        /// Создать коннектр через COM
        /// </summary>
        /// <param name="connectTo1CDatabaseArguments">Параметры для подключения к 1С</param>
        /// <returns>Возвращает коннектор к 1С</returns>
        public IComConnector1C CreateComConnector1C(ComConnector1CParametersModel connectTo1CDatabaseArguments)
        {
            return new ComConnector1C(connectTo1CDatabaseArguments);
        }
    }
}