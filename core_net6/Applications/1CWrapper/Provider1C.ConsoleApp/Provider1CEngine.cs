﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Newtonsoft.Json;
using Provider1C.Common.Enums;
using Provider1C.Common.Models.Commands;
using Provider1C.Common.Models.Commands.Args;
using Provider1C.Common.Models.Commands.Results;
using Provider1C.ConsoleApp.Interfaces;
using Provider1C.ConsoleApp.Models;

namespace Provider1C.ConsoleApp
{
    /// <summary>
    /// Делегат для выполнения команды
    /// </summary>
    /// <param name="commandArgs">параметры для выполнения команды</param>
    /// <returns>Результат выполнения команды</returns>
    internal delegate CommandResultModel ExecuteCommandDelegate(dynamic commandArgs);
    
    /// <summary>
    /// Провайдер к 1С
    /// </summary>
    internal sealed class Provider1CEngine : IDisposable
    {
        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger42 _logger = new SerilogLogger42();
        private readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        /// <summary>
        /// Зарегестрированные команды, которые можно выполнять, где
        /// <para>commandArgsType - Тип параметров для метода, который выполняет команда</para>
        /// <para>commandToExecute - Метод, который выполняет команду</para>
        /// </summary>
        private readonly Dictionary<Provider1CCommandEnum, (Type commandArgsType, ExecuteCommandDelegate commandToExecute)> _commandActions;

        /// <summary>
        /// Помошник по зозданию коннекторов к 1С
        /// </summary>
        private readonly IConnector1CCreator _connector1CCreator;

        /// <summary>
        /// COM Коннектор к 1С
        /// </summary>
        private IComConnector1C _comConnector1C;

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="Provider1CEngine"/>.
        /// </summary>
        /// <param name="connector1CCreator">Помошник по созданию коннекторов к 1С</param>
        public Provider1CEngine(IConnector1CCreator connector1CCreator)
        {
            SerilogConfig.ConfigureSerilog(new LocalLogsConfigDto(nameof(Provider1C)));
            _connector1CCreator = connector1CCreator;
            _commandActions = new Dictionary<Provider1CCommandEnum, (Type commandArgsType, ExecuteCommandDelegate commandToExecute)>
            {
                {
                    Provider1CCommandEnum.UpdateDatabaseConfigurationVersionCommand,
                    (typeof(DatabaseUpdateConfigurationVersionCommandArgumentsModel),
                        commandArgs => DatabaseUpdateVersion(commandArgs))
                },
                {
                    Provider1CCommandEnum.ConnectTo1CCommand,
                    (typeof(ConnectTo1CDatabaseCommandArgumentsModel), commandArgs => ConnectToDatabase1C(commandArgs))
                },
                {
                    Provider1CCommandEnum.GetDatabaseMetadataCommand,
                    (null, commandArgs => GetDatabaseMetadata())
                },
                {
                    Provider1CCommandEnum.DatabaseApplyUpdatesCommand,
                    (null, commandArgs => DatabaseApplyUpdates())
                }
            };
        }

        /// <summary>
        /// Выполняет команду
        /// </summary>
        /// <param name="commandId">ID команды</param>
        /// <param name="commandJsonArgs">Аргументы для выполнения команды в JSON формате</param>
        /// <returns>Результат выполнения команды</returns>
        public CommandResultModel ExecuteCommand(Provider1CCommandEnum commandId, string commandJsonArgs)
        {
            if (!_commandActions.TryGetValue(commandId, out var foundItem))
            {
                _logger.Warn($"Комманда: {commandId} не найдена.");
                return CommandResultModel.FromFail($"Комманда: {commandId} не найдена.");
            }
            
            if (foundItem.commandArgsType != null && string.IsNullOrEmpty(commandJsonArgs))
            {
                _logger.Warn($"Для команды {commandId} требуются аргументы, но не были представлены.");
                return CommandResultModel.FromFail($"Для команды {commandId} требуются аргументы, но не были представлены.");
            }


            if (foundItem.commandArgsType == null)
            {
                return foundItem.commandToExecute((object) null);
            }

            object commandArgs;
            try
            {
                _logger.Info("Пытаемся диссерилизовать в модель ");
                commandArgs = JsonConvert.DeserializeObject(commandJsonArgs, foundItem.commandArgsType);
            }
            catch (Exception exception)
            {
                _handlerException.Handle(exception, $"[Ошибка десереализации аргументов ]Не получилось десериализовать аргументы для команды:{commandId},\nJSON аргументы: {commandJsonArgs}");
                return CommandResultModel.FromFail($"Не получилось десериализовать аргументы для команды:{commandId}");
            }
            _logger.Info("Диссерилизовали и выполняем команду");
            return foundItem.commandToExecute(commandArgs);
        }

        /// <summary>
        /// Обновляет версию конфигурации инфобазы 1С
        /// </summary>
        /// <param name="commandArgs">Параметры для обновления версии конфигурации инфобазы 1С</param>
        /// <returns>Результат в виде <see cref="DatabaseUpdateVersionCommandResultModel"/> объекта.</returns>
        private CommandResultModel DatabaseUpdateVersion(DatabaseUpdateConfigurationVersionCommandArgumentsModel commandArgs)
        {
            _logger.Info($"Обновляет версию конфигурации инфобазы 1С {commandArgs.DatabaseName}");
            var directConnector1C = _connector1CCreator.CreateDirectConnector1C(new DirectConnector1CParametersModel
            {
                IsFile = commandArgs.IsFile,
                Engine1CFolderBinPath = commandArgs.Engine1CFolderBinPath,
                ConnectionAddress = commandArgs.ConnectionAddress,
                PathToDatabaseFolder = commandArgs.PathToDatabaseFolder,
                DatabaseName = commandArgs.DatabaseName,
                DbAdminLogin = commandArgs.DbAdminLogin,
                DbAdminPassword = commandArgs.DbAdminPassword
            });
            directConnector1C.UpdateVersion(commandArgs.CfuFilePath, out var logResult);

            var result = new DatabaseUpdateVersionCommandResultModel
            {
                LogState = logResult.LogState,
                Lines = logResult.Lines
            };

            return CommandResultModel.FromSuccess(result);
        }
        
        /// <summary>
        /// Подключается к 1С
        /// </summary>
        /// <param name="commandArgs">Параметры для подключения к 1С по COM</param>
        /// <returns>Возвращает <c>true</c> при успешном соединении, иначе <c>false</c>.</returns>
        private CommandResultModel ConnectToDatabase1C(ConnectTo1CDatabaseCommandArgumentsModel commandArgs)
        {
            _logger.Info($"Подключается к 1С {commandArgs.DatabaseName}");
            if (_comConnector1C != null)
            {
                return CommandResultModel.FromSuccess(true);
            }

            bool result;
            var comConnector1C = _connector1CCreator.CreateComConnector1C(new ComConnector1CParametersModel
            {
                IsFile = commandArgs.IsFile,
                Engine1CFolderBinPath = commandArgs.Engine1CFolderBinPath,
                Engine1CVersion = commandArgs.Engine1CVersion,
                Engine1CType = commandArgs.Engine1CType,
                ConnectionAddress = commandArgs.ConnectionAddress,
                PathToDatabaseFolder = commandArgs.PathToDatabaseFolder,
                DatabaseName = commandArgs.DatabaseName,
                DbAdminLogin = commandArgs.DbAdminLogin,
                DbAdminPassword = commandArgs.DbAdminPassword
            });
            _logger.Info($"Создали коннектор к 1С {commandArgs.DatabaseName}");
            try
            {
                result = comConnector1C.Connect();
            }
            catch (Exception exception)
            {
                return CommandResultModel.FromFail(exception.GetFullInfo(false));
            }

            _comConnector1C = comConnector1C;
            return CommandResultModel.FromSuccess(result);
        }
        
        /// <summary>
        /// Получение метаданных инфобазы 1С
        /// </summary>
        /// <returns>Метаданные инфобазы 1С</returns>
        private CommandResultModel GetDatabaseMetadata()
        {
            if (_comConnector1C == null)
            {
                return CommandResultModel.FromFail($"Перед вызовом команды получения метаданных({Provider1CCommandEnum.GetDatabaseMetadataCommand}), нужно вызвать команду: подключения к базе 1С{Provider1CCommandEnum.ConnectTo1CCommand}");
            }

            var result = _comConnector1C.GetMetadataInfo();

            if (result.ConnectCode == ConnectCodeDto.Success)
            {
                return CommandResultModel.FromSuccess(new DatabaseMetadataCommandResultModel
                {
                    ConfigurationVersion = result.Result.Version,
                    ConfigurationSynonym = result.Result.SynonymConfiguration,
                    ConnectCode = result.ConnectCode
                });
            }

            return CommandResultModel.FromFail(new DatabaseMetadataCommandResultModel
            {
                ConnectCode = result.ConnectCode
            }, result.Message);
        }
        
        /// <summary>
        /// Применяет обновления к инфобазе 1С
        /// </summary>
        /// <returns>Возвращает <c>true</c> при успешном обновлении, иначе <c>false</c>.</returns>
        private CommandResultModel DatabaseApplyUpdates()
        {
            if (_comConnector1C == null)
            {
                return CommandResultModel.FromFail($"Перед вызовом команды применения обновлений({Provider1CCommandEnum.DatabaseApplyUpdatesCommand}), нужно вызвать команду: подключения к базе 1С{Provider1CCommandEnum.ConnectTo1CCommand}.");
            }

            try
            {
                _comConnector1C.ApplyUpdates();
            }
            catch (Exception exception)
            {
                return CommandResultModel.FromFail(exception.GetFullInfo(false));
            }

            return CommandResultModel.FromSuccess(true);
        }

        ///<inheritdoc/>
        public void Dispose()
        {
            _comConnector1C?.Dispose();
        }
    }
}
