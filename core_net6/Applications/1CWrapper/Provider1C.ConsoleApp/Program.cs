﻿using System;
using System.Text;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Newtonsoft.Json;
using Provider1C.Common.Enums;
using Provider1C.Common.Models.Commands;
using Provider1C.ConsoleApp.Helpers;

namespace Provider1C.ConsoleApp
{
    /// <summary>
    /// <para>ВХОДНАЯ СТРОКА: {комманда} {id-запроса:GUID} {Base64(JSON)}</para>
    /// <para>ВЫХОДНАЯ СТРОКА: {id-запроса:GUID:N} {1 или 0} {Base64(JSON)}, где</para>
    /// <para>   если второй аргумент 1, то третий: Base64(JSON успешного результата команды),</para>
    /// <para>   если второй аргумент 0, то третий: Base64(JSON of <see cref="CommandErrorResultModel"/>)</para>
    /// </summary>
    internal static class Program
    {
        private const string ExitCommand = "EXIT";

        /// <summary>
        /// Логгер
        /// </summary>
        private static readonly ILogger42 Logger = new SerilogLogger42();

        /// <summary>
        /// Провайдер к 1С
        /// </summary>
        private static readonly Provider1CEngine Provider1CEngine = new(new Connector1CCreator());

        /// <summary>
        /// Точка входа в приложение
        /// </summary>
        private static void Main()
        {

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            SerilogConfig.ConfigureSerilog(new LocalLogsConfigDto(nameof(Provider1C)));
            while (true)
            {
                var inputLine = Console.ReadLine();
                if (inputLine is null or ExitCommand)
                {
                    break;
                }
                Logger.Info($"Получена новая строка {inputLine}");
                HandleInputCommand(inputLine);
            }
            FreeResources();
        }

        /// <summary>
        /// Пытается обработать полученную строчку с командой
        /// </summary>
        /// <param name="inputLine">Строчка с командой</param>
        private static void HandleInputCommand(string inputLine)
        {
            object outputObject;
            byte successMark;

            if (!TrySplitInputLine(inputLine, out var commandId, out var command, out var commandJsonArgs))
            {
                outputObject = new CommandErrorResultModel
                {
                    ErrorMessage = $"Неверные входные параметры. {inputLine}"
                };
                successMark = 0;
            }
            else
            {
                var commandResponse = Provider1CEngine.ExecuteCommand(command, commandJsonArgs);

                outputObject = commandResponse.IsSuccess
                    ? commandResponse.Result
                    : commandResponse.Error;

                successMark = (byte) (commandResponse.IsSuccess ? 1 : 0);
            }


            var resultJson = JsonConvert.SerializeObject(outputObject);

            var resultBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(resultJson));

            var resultLine = $"{commandId:N} {successMark} {resultBase64}";

            Console.WriteLine(resultLine);
        }

        /// <summary>
        /// Пытается разделить входную команду на части
        /// </summary>
        /// <param name="inputLine">Строка входной команды</param>
        /// <param name="requestId">Записывает сюда ID запроса</param>
        /// <param name="commandId">Записывает сюда ID команды</param>
        /// <param name="commandJsonArgs">Записывает сюда параметры для выполнения командв в JSON</param>
        /// <returns>При успешном разделении возвращает <c>true</c>, иначе <c>false</c>.</returns>
        private static bool TrySplitInputLine(string inputLine, out Guid requestId, out Provider1CCommandEnum commandId, out string commandJsonArgs)
        {
            requestId = Guid.Empty;
            commandId = Provider1CCommandEnum.Unknown;
            commandJsonArgs = null;

            var data = inputLine?.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            Logger.Info("Распарсили строку ");
            if (data == null || data.Length < 2)
            {
                return false;
            }

            if (!Enum.TryParse(data[0], true, out commandId) || commandId == Provider1CCommandEnum.Unknown)
            {
                Logger.Warn($"Команда({data[0]}) не найдена.");
                return false;   
            }
            Logger.Info($"Получили первое значение {data[0]} ");
            if (!Guid.TryParse(data[1], out requestId))
            {
                Logger.Warn($"ID({data[1]}) не является GUID-ом.");
                return false;
            }
            Logger.Info($"Получили второе значение {data[1]} ");
            if (data.Length < 3)
            {
                return true;
            }
            Logger.Info($"Получили третье значение значение {data[2]}");
            try
            {
                commandJsonArgs = Encoding.UTF8.GetString(Convert.FromBase64String(data[2]));
                Logger.Info($"Получили третье значение значение {data[2]} и конвертировали {commandJsonArgs} ");
            }
            catch (Exception exception)
            {
                Logger.Error(exception, $"Аргументы команды[{commandId}] закодированы не в формате Base64.\nАргументы: {data[2]}");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Освобождает ресурсы, если таковы имеются
        /// </summary>
        private static void FreeResources()
        {
            Provider1CEngine.Dispose();
        }

        /// <summary>
        /// Обработчик ошибок, которые не были отловлены
        /// </summary>
        /// <param name="sender">Отправитель</param>
        /// <param name="args">Аргументы полученной ошибки</param>
        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            if (args.ExceptionObject is not Exception ex) return;
            Logger.Error(ex, "Неотработанная ошибка");

            if (args.IsTerminating)
            {
                FreeResources();
            }
        }

    }
}
