﻿using System;
using System.IO;
using Clouds42.Configurations;

namespace Provider1C.ConsoleApp.Utilities
{
    /// <summary>
    /// Класс для определения файла файла лог 1С.
    /// </summary>
    internal static class Log1CPathUtility
    {
        /// <summary>
        /// Получить путь до файла с логами 1С по времени.        
        /// </summary>
        public static string GetPathOutByTime(DateTime dateTime, string dbName)
        {
            var timeFormat = dateTime.ToString("yyyy-MM-dd_hh-mm-ss");           
            var sharePath = ConfigurationHelper.GetConfigurationValue("TaSLogsSharePath");

            var folderPath = $@"{sharePath}/{dbName}";

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            //Если не поставить GUID, то может возникнуть ошибка модального окна о том, что файл используется.
            var filePath = $@"{folderPath}/{timeFormat}__{Guid.NewGuid():N}.txt";
            return filePath;
        }
        
        /// <summary>
        /// Получить путь до файла с логами 1С по времени.        
        /// </summary>               
        public static string GetPathOutByName(string dbName, string logfileName)
        {            

            if (string.IsNullOrEmpty(logfileName))
                throw  new ArgumentException($"ожидается не пустое значение {logfileName}");

            dbName = dbName
                .Replace(" ", "_")
                .Replace(".", "_");
            var sharePath = ConfigurationHelper.GetConfigurationValue("TaSLogsSharePath");

            var folderPath = $@"{sharePath}\{dbName}";

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            //Если не поставить GUID, то может возникнуть ошибка модального окна о том, что файл используется.
            var filePath = $@"{folderPath}\{logfileName.Replace(".", "_")}__{Guid.NewGuid():N}.txt";
            return filePath;
        }
    }
}
