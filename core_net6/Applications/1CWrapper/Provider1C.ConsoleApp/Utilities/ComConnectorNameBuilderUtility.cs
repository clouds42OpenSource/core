﻿using System;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.Enums._1C;

namespace Provider1C.ConsoleApp.Utilities
{
    /// <summary>
    /// Помощник по созданию имени для COM коннектора 1С
    /// </summary>
    internal static class ComConnectorNameBuilderUtility
    {
        /// <summary>
        /// Сформировать имя COM коннектора для версии платформы.
        /// </summary>
        /// <param name="platform">Платформа.</param>
        /// <returns>Имя COM коннектора.</returns>
        public static string Build(IPlatformDto platform)
        {
            return platform.PlatformType switch
            {
                PlatformType.V83 => string.IsNullOrEmpty(platform.PlatformVersion)
                    ? "V83.COMConnector.1"
                    : $"V{platform.PlatformVersion}.COMConnector",
                PlatformType.V82 => "V82.COMConnector.1",
                _ => throw new InvalidOperationException(
                    $"Не удалось получить имя коннектора для платформы {platform.PlatformType} версии {platform.PlatformVersion}")
            };
        }   
    }
}
