﻿using System;
using Provider1C.Client.Models;
using Provider1C.Common.Enums;
using Provider1C.Common.Models;
using Provider1C.Common.Models.Commands.Args;
using Provider1C.Common.Models.Commands.Results;

namespace Provider1C.Client
{
    /// <summary>
    /// Класс для проведения комманд к 1С через консольное приложение Provider1C.ConsoleApp
    /// </summary>
    public sealed class Provider1CClient : Provider1CClientBase
    {
        /// <summary>
        /// Инициализвция класс <see cref="Provider1CClient"/>
        /// </summary>
        /// <param name="database1CConnectArgs">Аргументы для подключения к 1С через COM</param>
        /// <param name="connectTo1CTimeToWait">Время ожидания при подключении к 1С через COM</param>
        /// <param name="connectTo1CAttempts">Количество попыток подключится к 1С через COM</param>
        public Provider1CClient(ConnectTo1CDatabaseCommandArgumentsModel database1CConnectArgs,
            TimeSpan connectTo1CTimeToWait, byte connectTo1CAttempts, string attachToProcessSettings)
            : base(database1CConnectArgs, connectTo1CTimeToWait, connectTo1CAttempts, attachToProcessSettings)
        {
        }

        /// <summary>
        /// Обновить версию конфигурации в базе 1С
        /// </summary>
        /// <param name="commandArgs">Параметры для обновления</param>
        /// <param name="timeToWait">Время ожидания выполнения команды</param>
        /// <param name="numberOfAttempts">Количество попыток выполнить команду</param>
        /// <returns>Возвращает лог после обновления</returns>
        public CommandExecuteResultModel<DatabaseUpdateVersionCommandResultModel> UpdateDatabaseVersion(
            UpdateDatabaseConfigurationVersionModel commandArgs, TimeSpan timeToWait, byte numberOfAttempts) =>
            ExecuteCommandInNewProcess(
                new Command1CModel<DatabaseUpdateVersionCommandResultModel>
                {
                    TimeToWait = timeToWait,
                    CommandArguments = new DatabaseUpdateConfigurationVersionCommandArgumentsModel
                    {
                        Engine1CFolderBinPath = Database1CConnectArgs.Engine1CFolderBinPath,
                        IsFile = Database1CConnectArgs.IsFile,
                        PathToDatabaseFolder = Database1CConnectArgs.PathToDatabaseFolder,
                        ConnectionAddress = Database1CConnectArgs.ConnectionAddress,
                        DatabaseName = Database1CConnectArgs.DatabaseName,
                        DbAdminLogin = Database1CConnectArgs.DbAdminLogin,
                        DbAdminPassword = Database1CConnectArgs.DbAdminPassword,
                        CfuFilePath = commandArgs.CfuFilePath
                    },
                    CommandId = Provider1CCommandEnum.UpdateDatabaseConfigurationVersionCommand,
                    NumberOfAttempts = numberOfAttempts
                });

        /// <summary>
        /// Получение метаданных базы 1С
        /// </summary>
        /// <param name="timeToWait">Время ожидания выполнения команды</param>
        /// <param name="numberOfAttempts">Количество попыток выполнить команду</param>
        /// <param name="options">Дополнительные опции к выполнению команды</param>
        /// <returns>Возвращает матаданные базы 1С</returns>
        public CommandExecuteResultModel<DatabaseMetadataCommandResultModel> GetDatabaseMetadata(TimeSpan timeToWait,
            byte numberOfAttempts, CommandOptions options = null) =>
            ExecuteCommandInActiveProcessWhenExist(
                new Command1CModel<DatabaseMetadataCommandResultModel>
                {
                    TimeToWait = timeToWait,
                    CommandArguments = null,
                    CommandId = Provider1CCommandEnum.GetDatabaseMetadataCommand,
                    NumberOfAttempts = numberOfAttempts
                }, options);

        /// <summary>
        /// Применение обновлений к базе 1С
        /// </summary>
        /// <param name="timeToWait">Время ожидания выполнения команды</param>
        /// <param name="numberOfAttempts">Количество попыток выполнить команду</param>
        /// <param name="options">Дополнительные опции к выполнению команды</param>
        /// <returns>Возвращает true при успешном принятии</returns>
        public CommandExecuteResultModel<bool> ApplyDatabaseUpdates(TimeSpan timeToWait, byte numberOfAttempts,
            CommandOptions options = null) =>
            ExecuteCommandInActiveProcessWhenExist(
                new Command1CModel<bool>
                {
                    TimeToWait = timeToWait,
                    CommandArguments = null,
                    CommandId = Provider1CCommandEnum.DatabaseApplyUpdatesCommand,
                    NumberOfAttempts = numberOfAttempts
                }, options);
    }
}
