﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
 
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Newtonsoft.Json;
using Provider1C.Client.Models;
using Provider1C.Common;
using Provider1C.Common.Enums;
using Provider1C.Common.Exceptions;
using Provider1C.Common.Models;
using Provider1C.Common.Models.Commands;
using Provider1C.Common.Models.Commands.Args;
using Provider1C.Common.Models.Interfaces;

namespace Provider1C.Client
{
    /// <summary>
    /// Класс для проведения комманд к 1С через консольное приложение Provider1C.ConsoleApp
    /// </summary>
    public abstract class Provider1CClientBase : IDisposable
    {
        /// <summary>
        /// Наименование консольного приложения через которое работает данный класс
        /// </summary>
        public const string PROVIDER_1C_CONSOLE_APP_NAME = "Provider1C.ConsoleApp.exe";

        /// <summary>
        /// Логгер
        /// </summary>
        protected ILogger42 Logger { get; } = new SerilogLogger42();

        /// <summary>
        /// Путь к консольному приложению <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>
        /// </summary>
        private readonly string _pathToProvider1CApp;

        /// <summary>
        /// Время ожидания при подключении к 1С через COM
        /// </summary>
        private readonly TimeSpan _connectTo1CTimeToWait;

        /// <summary>
        /// Количество попыток подключится к 1С через COM
        /// </summary>
        private readonly byte _connectTo1CAttempts;

        /// <summary>
        /// Аргументы для подключения к 1С через COM
        /// </summary>
        protected ConnectTo1CDatabaseCommandArgumentsModel Database1CConnectArgs { get; }
        
        /// <summary>
        /// Процесс приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>
        /// </summary>
        private Process _provider1CProcess;

        private bool _disposed;

        private readonly string _attachProcessSettings;


        /// <summary>
        /// Инициализвция класс <see cref="Provider1CClientBase"/>
        /// </summary>
        /// <param name="database1CConnectArgs">Аргументы для подключения к 1С через COM</param>
        /// <param name="connectTo1CTimeToWait">Время ожидания при подключении к 1С через COM</param>
        /// <param name="connectTo1CAttempts">Количество попыток подключится к 1С через COM</param>
        /// <param name="configuration"></param>
        protected Provider1CClientBase(ConnectTo1CDatabaseCommandArgumentsModel database1CConnectArgs,
            TimeSpan connectTo1CTimeToWait, byte connectTo1CAttempts, string attachProcessSettings)
        {
            Database1CConnectArgs = database1CConnectArgs;
            _pathToProvider1CApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Provider1C.ConsoleApp");
            _connectTo1CTimeToWait = TimeSpan.Zero.Equals(connectTo1CTimeToWait)
                ? TimeSpan.FromMinutes(1)
                : connectTo1CTimeToWait;
            _connectTo1CAttempts = Math.Max(connectTo1CAttempts, (byte)1);
            _attachProcessSettings = attachProcessSettings;
        }

        /// <summary>
        /// Убивает запущенный процесс <see cref="_provider1CProcess"/>
        /// <para>При необходимости, после проверки завершился процесс или нет, если не завершился, можно добавить код:</para>
        /// <para>
        /// <code>
        ///     _provider1CProcess.StandardInput.WriteLine("EXIT");    
        ///     _provider1CProcess.StandardInput.Flush();    
        ///     Thread.Sleep(500);
        /// </code>
        /// </para>
        /// для нормального завершения процесса и сохранения некой информации находящейся в приложении <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>,
        /// которое при нормальном завершении вызывает метод "FreeResources"
        /// </summary>
        private void Kill1CProviderProcess()
        {
            if (_provider1CProcess == null || _provider1CProcess.HasExited)
            {
                return;
            }


            if (_provider1CProcess.HasExited)
            {
                return;
            }

            try
            {
                _provider1CProcess.Kill();
            }
            catch 
            {
                //Ignored, possible the process has already exited.
            }
        }

        /// <summary>
        /// Возвращает <c>true</c>, если процесс <see cref="_provider1CProcess"/> ещё активный, иначе <c>false</c>.
        /// </summary>
        private bool IsProvider1CProcessAlive => _provider1CProcess is { HasExited: false };

        /// <summary>
        /// Запускает процесс <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>,
        /// Завершает текущий процесс если есть и создаёт новый
        /// </summary>
        /// <param name="exception">В случае ошибки, сохраняет сюда эту ошибку</param>
        /// <returns>При успешном запуске консольного приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/> возвращает <c>true</c>, иначе <c>false</c></returns>
        private bool Open1CProviderProcess(out Exception exception)
        {
            exception = null;

            Kill1CProviderProcess();

            var filePath = Path.Combine(_pathToProvider1CApp, PROVIDER_1C_CONSOLE_APP_NAME);
            
            if (!File.Exists(filePath))
            {
                exception = new FileNotFoundException($"Файл [{PROVIDER_1C_CONSOLE_APP_NAME}] не найден по пути [{_pathToProvider1CApp}].");
                Logger.Warn(exception.Message);
                return false;
            }

            _provider1CProcess = new Process
            {
                StartInfo =
                {
                    FileName = filePath,
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false
                }
            };

            var isSuccess = false;

            try
            {
                isSuccess =_provider1CProcess.Start();
            }
            catch(Exception ex)
            {
                exception = ex;
                Logger.Error(exception, $"Не получилось запустить процесс: {PROVIDER_1C_CONSOLE_APP_NAME}, по пути: {_pathToProvider1CApp}");
            }

            return isSuccess;
        }

        /// <summary>
        /// Отправляет команду в консольное приложение <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>
        /// </summary>
        /// <param name="commandLine">Комманда для выполнения</param>
        private void WriteCommandTo1CProviderProcess(string commandLine)
        {
            _provider1CProcess.StandardInput.WriteLine(commandLine);
            _provider1CProcess.StandardInput.Flush();
        }

        /// <summary>
        /// Ждёт ответа от консольного приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/> на переданную комманду.
        /// Если же консольное приложение внезапно закрылось, то вернётся <c>null</c>
        /// </summary>
        /// <returns>Ответ от консольного приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>.</returns>
        private string ReadResponseLineFrom1CProviderProcess()
        {
            var responseLineResult = _provider1CProcess.StandardOutput.ReadLine();

            if (responseLineResult != null)
            {
                return responseLineResult;
            }

            Logger.Warn($"Неожиданное завершение процесса: {PROVIDER_1C_CONSOLE_APP_NAME}.");
            return null;
        }

        /// <summary>
        /// Ожидает ответа от процесса <see cref="_provider1CProcess"/> и возвращает его, если ответ не получен, то возвращается <c>null</c>
        /// </summary>
        /// <param name="requestId">ID запроса, на который ожидается ответ</param>
        /// <param name="waitTime">Время ожидания ответа</param>
        /// <returns>Возвращает полученный ответ от процесса <see cref="_provider1CProcess"/> или <c>null</c> если ответ не был получен.</returns>
        private string WaitForResponseFrom1CProviderProcess(Guid requestId, TimeSpan waitTime)
        {
            string responseLine = null;
            var requestIdStr = requestId.ToString("N");
            
            bool hasResponse;
            Task.Factory.StartNew(() =>
            {
                do
                {
                    responseLine = ReadResponseLineFrom1CProviderProcess();

                    if (responseLine == null)
                    {
                        break;
                    }

                    hasResponse = responseLine.StartsWith(requestIdStr, StringComparison.OrdinalIgnoreCase);
                } while(!hasResponse);
            }).Wait(waitTime);

            return responseLine;
        }

        /// <summary>
        /// Пытается получить ответ от процесса <see cref="_provider1CProcess"/> на отправленную команду.
        /// </summary>
        /// <param name="requestId">ID запроса, на который ожидается ответ</param>
        /// <param name="timeToWait">Время ожидания ответа</param>
        /// <param name="responseLine">Сюда будет записан полученный ответ на выполненную команду.</param>
        /// <returns>При успешном получении ответа возвращает <c>true</c>, иначе <c>false</c>.</returns>
        private bool TryReadResponseLineFrom1CProviderProcess(Guid requestId, TimeSpan timeToWait, out string responseLine)
        {
            var waitTime = timeToWait;
            #if DEBUG
            waitTime = TimeSpan.FromHours(1);
            #endif

            responseLine = WaitForResponseFrom1CProviderProcess(requestId, waitTime);
            return responseLine != null;
        }

        /// <summary>
        /// Генерирует строку с командой для отправке в процесс <see cref="_provider1CProcess"/>.
        /// </summary>
        /// <typeparam name="TCommandResult">Тип успешного результата из полученного ответа</typeparam>
        /// <param name="requestId">ID запроса, на который ожидается ответ</param>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <returns>Сгенерированную строку для отправки.</returns>
        private static string GenerateCommandLine<TCommandResult>(Guid requestId, ICommand1CModel<TCommandResult> command1C)
        {
            var base64CommandArgs = string.Empty;
            if (command1C.CommandArguments != null)
            {
                var jsonCommandArgs = JsonConvert.SerializeObject(command1C.CommandArguments);
                base64CommandArgs = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonCommandArgs));
            }

            var commandLine = string.IsNullOrEmpty(base64CommandArgs)
                ? $"{command1C.CommandId} {requestId}"
                : $"{command1C.CommandId} {requestId} {base64CommandArgs}";

            return commandLine;
        }

        /// <summary>
        /// Пытается десериализовать JSON в объект с типом <typeparamref name="TCommandResult"/>
        /// </summary>
        /// <typeparam name="TCommandResult">Тип для десериализации JSON</typeparam>
        /// <param name="json">JSON для десериализации</param>
        /// <param name="resultObject">Сюда запишет десериализованный объект.</param>
        /// <returns>При успешной десериализации возвращает <c>true</c>, иначе <c>false</c>.</returns>
        private bool TryDeserializeJson<TCommandResult>(string json, out TCommandResult resultObject)
        {
            resultObject = default;

            try
            {
                resultObject = JsonConvert.DeserializeObject<TCommandResult>(json);
            }
            catch(Exception ex)
            {
                Logger.Error(ex, $"Не верный формат JSON строки.\nСтрока:{json}.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Парсит ответ из полученой строки выполненной комманды
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="responseLine">Полученный ответ на выполненную команду</param>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <returns>Результат полученный после выполнения команды</returns>
        private CommandExecuteResultModel<TCommandResult> ParseCommandResult<TCommandResult>(string responseLine, ICommand1CModel<TCommandResult> command1C)
        {
            var commandId = command1C.CommandId;
            var responseItems = responseLine.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            if (responseItems.Length < 2)
            {
                return CommandExecuteResultModel<TCommandResult>.FromFail(
                    new FormatException($"Результат команды [{commandId}] представлен в неверном формате."));
            }

            //responseItems[0] -> RequestId
            var isSuccess = responseItems[1] == "1";

            if (isSuccess && responseItems.Length == 2)
            {
                return CommandExecuteResultModel<TCommandResult>.Success();
            }

            string resultJson;

            try
            {
                resultJson = Encoding.UTF8.GetString(Convert.FromBase64String(responseItems[2]));
            }
            catch(Exception ex)
            {
                Logger.Error(ex, $"Результат команды [{commandId}] закодирован не в формате Base64.");
                return CommandExecuteResultModel<TCommandResult>.FromFail(
                    new FormatException($"Результат команды [{commandId}] закодирован не в формате Base64."));
            }
            
            if (isSuccess)
            {
                if (typeof(TCommandResult) == typeof(Nothing))
                {
                    return CommandExecuteResultModel<TCommandResult>.Success();
                }

                if (TryDeserializeJson(resultJson, out TCommandResult resultObject))
                {
                    return CommandExecuteResultModel<TCommandResult>.FromSuccess(resultObject);
                }

                return CommandExecuteResultModel<TCommandResult>.FromFail(
                    new FormatException($"Результат команды [{commandId}] представлен в неверном формате JSON."));
            }

            if (TryDeserializeJson(resultJson, out CommandErrorResultModel errorResult))
            {
                return CommandExecuteResultModel<TCommandResult>.FromFail( new Provider1CException(errorResult.ErrorMessage));
            }

            return CommandExecuteResultModel<TCommandResult>.FromFail(new Provider1CException($"Возникла ошибка при выполнении команды [{commandId}], ответ ошибки не получен из-за передачи её в неверном формате JSON."));
        }

        /// <summary>
        /// Выполняет комманду, отправляя её в процесс <see cref="_provider1CProcess"/>.
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <returns>Результат полученный после выполнения команды</returns>
        private CommandExecuteResultModel<TCommandResult> ExecuteCommand<TCommandResult>(ICommand1CModel<TCommandResult> command1C)
        {
            var requestId = Guid.NewGuid();
            
            var commandLine = GenerateCommandLine(requestId, command1C);
            Logger.Info($"Строка с командой для отправке в процесс provider1CProcess [{commandLine}].");

            WriteCommandTo1CProviderProcess(commandLine);

            if (!TryReadResponseLineFrom1CProviderProcess(requestId, command1C.TimeToWait, out var responseLine))
            {
                return CommandExecuteResultModel<TCommandResult>.FromFail(
                    new TimeoutException($"Время ожидания выполнения команды [{command1C.CommandId}] истекло."));
            }

            var result = ParseCommandResult(responseLine, command1C);

            return result;
        }

        /// <summary>
        /// Пред выполнени функции, которое разрешает или звапрещает выполнение команды, например подключится к 1С
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="attempt">Какая попытка выполнить команду</param>
        /// <param name="preExecuteFunc">Функция которую нужно выполнить перед выполнением команды</param>
        /// <returns>Результата выполнения функции <paramref name="preExecuteFunc"/>, если успешно выполнена, то можно выполнять команду к 1С</returns>
        private static CommandExecuteResultModel<TCommandResult> PreExecute<TCommandResult>(byte attempt, Func<byte, CommandExecuteResultModel<bool>> preExecuteFunc)
        {
            if (preExecuteFunc == null)
            {
                return CommandExecuteResultModel<TCommandResult>.Success();
            }

            var result = preExecuteFunc(attempt);

            if (result is { IsSuccess: false })
            {
                return result.GetAsErrorOf<TCommandResult>();
            }

            return CommandExecuteResultModel<TCommandResult>.Success();
        }

        /// <summary>
        /// Выполняет комманду, отправляя её в процесс <see cref="_provider1CProcess"/>, пытаясь несколько раз.
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="requestId">ID запроса</param>
        /// <param name="preExecuteFunc">Пред выполнени функции, которая разрешает или запрещает выполнение команды</param>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <param name="openNew1CProviderProcess">Если <c>true</c>, то открывает новый процесс приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>.</param>
        /// <param name="close1CProviderProcessAfterCommandExecuted">Если <c>true</c>, то закрывает процесс после выполнения команды.</param>
        /// <returns>Результат полученный после выполнения команды</returns>
        private CommandExecuteResultModel<TCommandResult> ExecuteCommandWithAttempts<TCommandResult>(
            Guid requestId,
            Func<byte, CommandExecuteResultModel<bool>> preExecuteFunc,
            ICommand1CModel<TCommandResult> command1C, 
            bool openNew1CProviderProcess,
            bool close1CProviderProcessAfterCommandExecuted)
        {
            var numberOfAttempts = Math.Max(command1C.NumberOfAttempts, (byte)1);

            CommandExecuteResultModel<TCommandResult> result = null;
            for (byte attempt = 1; attempt <= numberOfAttempts; ++attempt)
            {
                Logger.Info($@"ЗАПРОС [{requestId:N}], БАЗА:({Database1CConnectArgs.DatabaseName}), ПОПЫТКА №{attempt}/{numberOfAttempts} ВЫПОЛНИТЬ КОМАНДУ [{command1C.CommandId}].");
                result = PreExecute<TCommandResult>(attempt, preExecuteFunc);
                if (!result.IsSuccess)
                {
                    break;
                }

                if ((openNew1CProviderProcess || !IsProvider1CProcessAlive) && !Open1CProviderProcess(out var error))
                {
                    result = CommandExecuteResultModel<TCommandResult>.FromFail(new RunProvider1CProcessException(
                        $"Процесс для выполнения команды [{command1C.CommandId}] не был запущен.", error));
                    break;
                }

                result = ExecuteCommand(command1C);

                if (result.Error != null && result.Error.GetType() == typeof(TimeoutException))
                {
                    continue;
                }

                break;
            }

            if (close1CProviderProcessAfterCommandExecuted)
            {
                Kill1CProviderProcess();
            }

            return result;
        }

        /// <summary>
        /// Пытается подключиться к 1С
        /// </summary>
        /// <param name="requestId">ID запроса</param>
        /// <returns>Ответ подключения к 1С</returns>
        private CommandExecuteResultModel<bool> TryConnectTo1C(Guid requestId)
        {
            var result = ExecuteCommandWithAttempts(
                requestId,
                null, 
                new Command1CModel<bool>
                {
                    TimeToWait = _connectTo1CTimeToWait,
                    NumberOfAttempts = _connectTo1CAttempts,
                    CommandArguments = Database1CConnectArgs,
                    CommandId = Provider1CCommandEnum.ConnectTo1CCommand,
                }, true, false);

            return result;
        }

        /// <summary>
        /// Генерирует новый ID запроса
        /// </summary>
        /// <returns></returns>
        private static Guid GenerateRequestId() => Guid.NewGuid();

        /// <summary>
        /// Выполняет команду всегда в новом процессе
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <returns>Результат полученный после выполнения команды</returns>
        protected CommandExecuteResultModel<TCommandResult> ExecuteCommandInNewProcess<TCommandResult>(
            ICommand1CModel<TCommandResult> command1C)
        {
            var result = ExecuteCommandWithAttempts(
                GenerateRequestId(),
                null,
                command1C,
                true, true);

            return result;
        }

        /// <summary>
        /// Пытается выполнить команду уже в открытом процессе приложения <see cref="PROVIDER_1C_CONSOLE_APP_NAME"/>.
        /// </summary>
        /// <typeparam name="TCommandResult">Тип результирующего объекта</typeparam>
        /// <param name="command1C">Комманда для отправки в процесс <see cref="_provider1CProcess"/> на выполнение.</param>
        /// <param name="options">Дополнительные опции</param>
        /// <returns>Результат полученный после выполнения команды</returns>
        protected CommandExecuteResultModel<TCommandResult> ExecuteCommandInActiveProcessWhenExist<TCommandResult>(
            ICommand1CModel<TCommandResult> command1C, CommandOptions options)
        {
            var requestId = GenerateRequestId();
            var result = ExecuteCommandWithAttempts(
                requestId,
                attempt => IsProvider1CProcessAlive && attempt == 1 ? null : TryConnectTo1C(requestId),
                command1C,
                false, false);

            if (options is { KillProcessAfterExecution: true })
            {
                Kill1CProviderProcess();
            }
            return result;
        }

        ///<inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Освободить ресурсы
        /// </summary>
        /// <param name="disposing">Признак что ресурсы освобождаются</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                Kill1CProviderProcess();

            _disposed = true;
        }
    }
}
