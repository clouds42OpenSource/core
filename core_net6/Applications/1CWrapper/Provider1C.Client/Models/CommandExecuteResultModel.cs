﻿using System;

namespace Provider1C.Client.Models
{
    /// <summary>
    /// Содержит результат выполнения команды
    /// </summary>
    /// <typeparam name="TCommandResult">Тип результата</typeparam>
    public sealed class CommandExecuteResultModel<TCommandResult>
    {
        /// <summary>
        /// Если true, то команда выполнена успешна, иначе false.
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Результат выполненой команды
        /// </summary>
        public TCommandResult Result { get; set; }
        
        /// <summary>
        /// Если команда выпонена не успешна, содержит ошибку выполнения
        /// </summary>
        public Exception Error { get; set; }

        /// <summary>
        /// Создаёт новый экземпляр класса <see cref="CommandExecuteResultModel{TCommandResult}"/>.
        /// </summary>
        private CommandExecuteResultModel()
        {
        }

        /// <summary>
        /// Создаёт НЕ успешный результат
        /// </summary>
        /// <param name="error">Ошибка</param>
        /// <returns>Не успешный результат выполнения команды</returns>
        public static CommandExecuteResultModel<TCommandResult> FromFail(Exception error)
        {
            return new CommandExecuteResultModel<TCommandResult>
            {
                IsSuccess = false,
                Result = default,
                Error = error
            };
        }

        /// <summary>
        /// Создаёт успешный результат
        /// </summary>
        /// <param name="result">Результат выполненной команды</param>
        /// <returns>Успешный результат выполнения команды</returns>
        public static CommandExecuteResultModel<TCommandResult> FromSuccess(TCommandResult result)
        {
            return new CommandExecuteResultModel<TCommandResult>
            {
                IsSuccess = true,
                Result = result,
                Error = null
            };
        }

        /// <summary>
        /// Создаёт успешный результат
        /// </summary>
        /// <returns>Успешный результат выполнения команды</returns>
        public static CommandExecuteResultModel<TCommandResult> Success()
        {
            return new CommandExecuteResultModel<TCommandResult>
            {
                IsSuccess = true,
                Result = default,
                Error = null
            };
        }


        /// <summary>
        /// Копирует ошибкой в новый объект для нового типа результата
        /// </summary>
        /// <typeparam name="TCommandResaltNew">Новый тип результата</typeparam>
        /// <returns>Возврощает новый объект <see cref="CommandExecuteResultModel{TCommandResaltNew}"/> с ошибкой</returns>
        public CommandExecuteResultModel<TCommandResaltNew> GetAsErrorOf<TCommandResaltNew>()
        {
            return new CommandExecuteResultModel<TCommandResaltNew>
            {
                IsSuccess = false,
                Error = Error
            };
        }
    }
}