﻿namespace Provider1C.Client.Models
{
    /// <summary>
    /// Аргументы при выполнении команды на обновление конфигурации инфобазы 1С
    /// </summary>
    public sealed class UpdateDatabaseConfigurationVersionModel
    {
        /// <summary>
        /// Путь к конфигурационному файлу для обновления
        /// </summary>
        public string CfuFilePath { get; set; }
    }
}