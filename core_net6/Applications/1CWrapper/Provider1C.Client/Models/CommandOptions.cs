﻿namespace Provider1C.Client.Models
{
    /// <summary>
    /// Дополнительнные опции при выполнении команды
    /// </summary>
    public sealed class CommandOptions
    {
        /// <summary>
        /// Если <c>true</c>, то завершает процесс после выполнения команды, иначе <c>false</c>.
        /// </summary>
        public bool KillProcessAfterExecution { get; set; }
    }
}