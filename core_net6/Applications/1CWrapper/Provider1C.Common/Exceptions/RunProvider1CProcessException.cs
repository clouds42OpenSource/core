﻿using System;

namespace Provider1C.Common.Exceptions
{
    /// <summary>
    /// Ошибка, которая показывает, что не получилось запустить процесс Provider1C.ConsoleApp
    /// </summary>
    [Serializable]
    public class RunProvider1CProcessException : Exception
    {
        /// <summary>
        /// Созаёт новый экземпляр класса <see cref="RunProvider1CProcessException"/>
        /// </summary>
        /// <param name="errorMessage">Текст ошибки</param>
        /// <param name="innerException">Дополнительная ошибка, которую нужно включить</param>
        public RunProvider1CProcessException(string errorMessage, Exception innerException)
            : base(errorMessage, innerException)
        {

        }
    }
}
