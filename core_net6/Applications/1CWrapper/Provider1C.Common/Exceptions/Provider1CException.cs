﻿using System;

namespace Provider1C.Common.Exceptions
{
    /// <summary>
    /// Ошибка, которая показывает, что она была получена в консольном приложении Provider1C.ConsoleApp
    /// </summary>
    [Serializable]
    public class Provider1CException : Exception
    {
        /// <summary>
        /// Созаёт новый экземпляр класса <see cref="Provider1CException"/>
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        public Provider1CException(string message)
            : base(message??string.Empty)
        {
        }
    }
}
