﻿using System;
using System.Collections;

namespace Provider1C.Common.Exceptions
{
    /// <summary>
    /// Ошибка заменяющая <see cref="AccessViolationException"/>, что бы можно было далее делать throw.
    /// </summary>
    [Serializable]
    public class AccessViolationReplacementException : Exception
    {
        private string _stackTrace;

        /// <inheritdoc />
        public override string StackTrace => _stackTrace;

        /// <summary>
        /// Созаёт новый экземпляр класса <see cref="AccessViolationReplacementException"/>
        /// </summary>
        /// <param name="ave">Ошибка из которой будут скопированы все возможные свойства.</param>
        public AccessViolationReplacementException(AccessViolationException ave)
            : base(ave.Message, ave.InnerException)
        {
            FillProps(ave);
        }

        /// <summary>
        /// Заполняет свойства ошибки
        /// </summary>
        /// <param name="ave">Ошибка из который взять значения свойств.</param>
        private void FillProps(AccessViolationException ave)
        {
            _stackTrace =  ave.StackTrace;
            HResult = ave.HResult;
            Source = ave.Source;

            foreach (DictionaryEntry dictionaryEntry in ave.Data)
            {
                Data.Add(dictionaryEntry.Key, dictionaryEntry.Value);
            }
        }
    }
}
