﻿using System;
using Provider1C.Common.Enums;

namespace Provider1C.Common.Models.Interfaces
{
    /// <summary>
    /// Модель для выполнения команды к 1С
    /// </summary>
    /// <typeparam name="TCommandResult">Тип результата выполнения команды</typeparam>
    public interface ICommand1CModel<TCommandResult>
    {
        /// <summary>
        /// Команда, которую нужно выполнить
        /// </summary>
        Provider1CCommandEnum CommandId { get; }

        /// <summary>
        /// Время ожидания выполнения команды
        /// </summary>
        TimeSpan TimeToWait { get; }

        /// <summary>
        /// Аргументы для команды
        /// </summary>
        object CommandArguments { get; }

        /// <summary>
        /// Количество попыток выполнения команды
        /// </summary>
        byte NumberOfAttempts { get; }
    }
}