﻿using Clouds42.Domain.Enums._1C;

namespace Provider1C.Common.Models.Commands.Args
{
    /// <summary>
    /// Параметры для подключения к 1С через COM
    /// </summary>
    public sealed class ConnectTo1CDatabaseCommandArgumentsModel
    {
        /// <summary>
        /// Название базы для обновления
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Если <code>true</code>, то база является файловой иначе серверной.
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Если база серверная, то содержит адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Если база НЕ серверная, то содержит путь к папке бвзы
        /// </summary>
        public string PathToDatabaseFolder { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string DbAdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        public string DbAdminPassword { get; set; }

        /// <summary>
        /// Путь до 1С движка, к папки bin
        /// </summary>
        public string Engine1CFolderBinPath { get; set; }

        /// <summary>
        /// Версия 1С движка
        /// </summary>
        public string Engine1CVersion{ get; set; }

        /// <summary>
        /// Тип платформы 1С
        /// </summary>
        public PlatformType Engine1CType { get; set; }
    }
}
