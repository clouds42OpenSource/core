﻿using Clouds42.Tools1C.Helpers;

namespace Provider1C.Common.Models.Commands.Results
{
    /// <summary>
    /// Результат анализа лога 1С.
    /// </summary>
    public sealed class DatabaseUpdateVersionCommandResultModel
    {
        /// <summary>
        /// Результат операции обновления.
        /// </summary>
        public LogUpdate1CParser.LogState LogState { get; set; }

        /// <summary>
        /// Прочитанные данные из лога.
        /// </summary>
        public string[] Lines { get; set; }
    }
}