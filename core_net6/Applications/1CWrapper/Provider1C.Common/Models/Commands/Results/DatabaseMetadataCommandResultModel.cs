﻿using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Provider1C.Common.Models.Commands.Results
{
    /// <summary>
    /// Результат выполнения команды получения метаданных инфобазы 1С
    /// </summary>
    public sealed class DatabaseMetadataCommandResultModel
    {
        /// <summary>
        /// Версия релиза.
        /// </summary>
        public string ConfigurationVersion { get; set; }

        /// <summary>
        /// Синоним конфигурации
        /// </summary>
        public string ConfigurationSynonym { get; set; }

        /// <summary>
        /// Код подключения.
        /// </summary>
        public ConnectCodeDto ConnectCode { get; set; }
    }
}