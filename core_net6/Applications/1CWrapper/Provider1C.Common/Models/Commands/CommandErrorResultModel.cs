﻿namespace Provider1C.Common.Models.Commands
{
    /// <summary>
    /// Модель при не успешных результатах выполнения комманды к 1С
    /// </summary>
    public sealed class CommandErrorResultModel
    {
        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}