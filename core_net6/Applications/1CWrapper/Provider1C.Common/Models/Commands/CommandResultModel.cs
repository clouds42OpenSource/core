﻿namespace Provider1C.Common.Models.Commands
{
    /// <summary>
    /// Модель для передачи результата выполнения команды к 1С
    /// </summary>
    public sealed class CommandResultModel
    {
        /// <summary>
        /// Признак успешности выполнения
        /// </summary>
        public bool IsSuccess { get; private set; }

        /// <summary>
        /// Результат выполнения команды
        /// </summary>
        public object Result { get; private set; }

        /// <summary>
        /// Ошибка выполнения команды
        /// </summary>
        public CommandErrorResultModel Error { get; private set; }

        /// <summary>
        /// Создать не успешный результат
        /// </summary>
        /// <param name="errorMessage">Текст ошибки</param>
        public static CommandResultModel FromFail(string errorMessage)
        {
            return new CommandResultModel
            {
                IsSuccess = false,
                Error = new CommandErrorResultModel
                {
                    ErrorMessage = errorMessage
                }
            };
        }

        /// <summary>
        /// Создать не успешный результат
        /// </summary>
        /// <param name="data">Данные неуспешно результата</param>
        /// <param name="errorMessage">Текст ошибки</param>
        public static CommandResultModel FromFail(object data, string errorMessage)
        {
            return new CommandResultModel
            {
                IsSuccess = false,
                Result = data,
                Error = new CommandErrorResultModel
                {
                    ErrorMessage = errorMessage
                }
            };
        }

        /// <summary>
        /// Создать успешный результат
        /// </summary>
        /// <param name="data">Успешный результат выполненной команды</param>
        public static CommandResultModel FromSuccess(object data)
        {
            return new CommandResultModel
            {
                IsSuccess = true,
                Result = data
            };
        }

    }
}