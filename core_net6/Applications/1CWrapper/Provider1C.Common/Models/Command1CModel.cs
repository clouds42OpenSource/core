﻿using System;
using Provider1C.Common.Enums;
using Provider1C.Common.Models.Interfaces;

namespace Provider1C.Common.Models
{
    /// <summary>
    /// Модель для выполнения команды к 1С
    /// </summary>
    /// <typeparam name="TCommandResult">Тип результата выполнения команды</typeparam>
    public sealed class Command1CModel<TCommandResult> : ICommand1CModel<TCommandResult>
    {
        /// <summary>
        /// Команда, которую нужно выполнить
        /// </summary>
        public Provider1CCommandEnum CommandId { get; set; }

        /// <summary>
        /// Время ожидания выполнения команды
        /// </summary>
        public TimeSpan TimeToWait { get; set; }
        
        /// <summary>
        /// Аргументы для команды
        /// </summary>
        public object CommandArguments { get; set; }
        
        /// <summary>
        /// Количество попыток выполнения команды
        /// </summary>
        public byte NumberOfAttempts { get; set; }
    }
}