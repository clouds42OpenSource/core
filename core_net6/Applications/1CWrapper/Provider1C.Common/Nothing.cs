﻿namespace Provider1C.Common
{
    /// <summary>
    /// Класс, для передчи как параметр в дженерик тип, который показывает что результата нет, взамен типа Void, который невозможно передать как дженерик
    /// </summary>
    public sealed class Nothing
    {
    }
}