﻿namespace Provider1C.Common.Enums
{
    /// <summary>
    /// Команды к 1С через консольное приложение Provider1C.ConsoleApp
    /// </summary>
    public enum Provider1CCommandEnum : byte
    {
        /// <summary>
        /// Неизвестная команда
        /// </summary>
        Unknown,

        /// <summary>
        /// Команда подключения к COM 1С
        /// </summary>
        ConnectTo1CCommand,

        /// <summary>
        /// Команда обновления версии конфигурации информационной базы 
        /// </summary>
        UpdateDatabaseConfigurationVersionCommand,

        /// <summary>
        /// Команда применения новой версии конфигурации
        /// </summary>
        DatabaseApplyUpdatesCommand,

        /// <summary>
        /// Команда получения метаданных информационной базы
        /// </summary>
        GetDatabaseMetadataCommand
    }
}