﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace UploadFileApi.Providers
{
    /// <summary>
    /// Провайдер доступа к АПИ загрузки файлов
    /// </summary>
    public sealed class UploadApiAccessProvider(
        IUnitOfWork dbLayer,
        IHttpContextAccessor httpContextAccessor,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        /// <summary>
        /// Названия заголовков для получения с какого клиента был запрос
        /// </summary>
        private static readonly string[] UserAgentHeaderNames = ["User-Agent", "UserAgent"];

        /// <summary>
        /// Получатель текущего HttpContext-а
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;

        /// <summary>
        /// Текущий HttpContext
        /// </summary>
        private HttpContext HttpContext => _httpContextAccessor.HttpContext;

        /// <summary>
        /// Получить Логин текущего пользователя
        /// </summary>
        /// <returns>Логин текущего пользователя</returns>
        protected override string GetUserIdentity() => HttpContext?.User?.Identity?.Name;

        /// <summary>
        /// Текущий пользователь
        /// </summary>
        private IUserPrincipalDto _user;

        /// <summary>
        /// Получить текущего пользователя
        /// </summary>
        /// <returns>Пользователь</returns>
        public override IUserPrincipalDto GetUser()
        {
            if (_user != null)
                return _user;

            var userIdentity = GetUserIdentity();

            Logger.Trace($"Данные пользователя: {userIdentity}");

            if (string.IsNullOrEmpty(userIdentity))
                return null;

            var accountUser = DbLayer.AccountUsersRepository.AsQueryable().Include(i => i.AccountUserRoles).FirstOrDefault(au => au.Login == userIdentity);

            if (accountUser == null)
                return null;

            _user = new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                Name = accountUser.Login,
                RequestAccountId = accountUser.AccountId,
                ContextAccountId = accountUser.AccountId,
                Groups = accountUser.AccountUserRoles.Select(r => r.AccountUserGroup).ToList()
            };

            return _user;
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Получить агента пользователя
        /// </summary>
        /// <returns>Агент пользователя</returns>
        public override string GetUserAgent()
        {
            var currentRequest = HttpContext?.Request;

            if (currentRequest == null)
                return string.Empty;

            foreach (var userAgentName in UserAgentHeaderNames)
            {
                if (currentRequest.Headers[userAgentName].Count > 0)
                {
                    return currentRequest.Headers[userAgentName][0];
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Получить адрес хоста пользователя
        /// </summary>
        /// <returns>Адрес хоста пользователя</returns>
        public override string GetUserHostAddress() => HttpContext?.Connection?.RemoteIpAddress?.ToString() ?? string.Empty;

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public override string Name => GetUser().Name;

    }
}
