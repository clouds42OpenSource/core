﻿using System.Collections.Specialized;
using System.Linq;
using Clouds42.Common.Http;
using Microsoft.AspNetCore.Http;

namespace UploadFileApi.Extensions
{
    /// <summary>
    /// Extensions для класса <see cref="HttpRequest"/>
    /// </summary>
    public static class RequestExtensions
    {
        /// <summary>
        /// Создать <see cref="ThinHttpRequest"/> по <see cref="HttpRequest"/>
        /// </summary>
        /// <param name="request">Реквест по которому создать <see cref="ThinHttpRequest"/></param>
        /// <returns>Возвращает ThinHttpRequest объект</returns>
        public static ThinHttpRequest CreateThinHttpRequestObject(this HttpRequest request)
        {
            var files = request.ContentLength > 0 &&
                        !string.IsNullOrEmpty(request.ContentType) &&
                        request.Form is { Files.Count: > 0 }
                ? request.Form.Files.Select(file => new UploadedFileItem
                {
                    ContentLength = request.ContentLength.Value,
                    FileName = file.FileName,
                    InputStream = file.OpenReadStream()
                })
                : null;

            var headers = new NameValueCollection();
            foreach (var requestHeader in request.Headers)
            {
                headers.Add(requestHeader.Key, requestHeader.Value);
            }

            return new ThinHttpRequest(headers, new UploadedFilesCollection(files));
        }
    }
}