﻿using Clouds42.Common.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UploadFileApi.Filters
{
    /// <summary>
    /// Фильтр для валидауии моделей экшена
    /// </summary>
    public sealed class ValidateModelActionFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Содержит значение <с>true</с> для Header-а <see cref="WebHeaders.ValidationModelFailedWebHeader"/>, если модель не прошла валидацию.
        /// </summary>
        private const string ValidationModelFailedWebHeaderValue = "true";

        /// <summary>
        /// Вызывается до вызова Action
        /// </summary>
        /// <param name="context">Контекст выполняющего экшена</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid) return;

            context.HttpContext.Response.Headers.Add(WebHeaders.ValidationModelFailedWebHeader, ValidationModelFailedWebHeaderValue);
            context.Result = new BadRequestObjectResult(context.ModelState);
        }
    }
}