﻿using Clouds42.HandlerExeption.Contract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UploadFileApi.Filters
{
    /// <summary>
    /// Фильтр для обработки не обработанных ошибок
    /// </summary>
    public sealed class UnhandledExceptionFilterAttribute(IHandlerException handlerException) : ExceptionFilterAttribute
    {
        /// <summary>
        /// Обработчик ошибки
        /// </summary>
        /// <param name="context">Контекст ошибки</param>
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            context.HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            context.HttpContext.Response.ContentType = "Application/json";

            var message = exception.Message;

            handlerException.Handle(exception, "[ Обработчик ошибок ]" +context.HttpContext.Request.Path.Value);

            context.Result = new JsonResult(new
            {
                Error = message
            });

            base.OnException(context);
        }
    }
}
