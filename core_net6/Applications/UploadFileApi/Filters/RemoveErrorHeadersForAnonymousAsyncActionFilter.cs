﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UploadFileApi.Filters
{
    /// <summary>
    /// Фильтр для удаления заголовков невалидности токена в Response, так JWT не смотрит на атрибут <see cref="AllowAnonymousAttribute"/>, и всегда проверяет токен
    /// </summary>
    public sealed class RemoveErrorHeadersForAnonymousAsyncActionFilter : IAsyncActionFilter
    {
        /// <summary>
        /// Вызывается после биндинга модели
        /// </summary>
        /// <param name="context">Контекст выполняющего экшена</param>
        /// <param name="next">Делегат на следующий экшен</param>
        public Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var allowAnonymous = context.ActionDescriptor.EndpointMetadata.Any(em => em.GetType() == typeof(AllowAnonymousAttribute));

            if (!allowAnonymous)
            {
                return next();
            }

            var headers = context.HttpContext.Response.Headers;
            if (headers.ContainsKey(JsonWebTokenNotValidHttpHeader.TokenExpiredHeader))
            {
                headers.Remove(JsonWebTokenNotValidHttpHeader.TokenExpiredHeader);
            }

            if (headers.ContainsKey(JsonWebTokenNotValidHttpHeader.InvalidTokenHeader))
            {
                headers.Remove(JsonWebTokenNotValidHttpHeader.InvalidTokenHeader);
            }

            return next();
        }
    }
}