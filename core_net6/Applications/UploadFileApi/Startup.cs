﻿using System;
using System.IO;
using System.Reflection;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Configurations;
using Clouds42.CoreWorkerTask;
using Clouds42.DependencyRegistration;
using Clouds42.DomainContext;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Jwt;
using Clouds42.Logger;
using Clouds42.TelegramBot;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using UploadFileApi.Extensions;

namespace UploadFileApi
{
    public class Startup(IWebHostEnvironment env)
    {
        public IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddGcpKeyValueSecrets(env.EnvironmentName)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddEnvironmentVariables()
            .Build();

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddClouds42DbContext(Configuration)
                .ConfigureMaxContentSize()
                .AddHttpContextAccessor()
                .AddUnitOfWork()
                .AddGlobalServices(Configuration,ignoreAppServices: true)
                .AddManagers()
                .AddProviders(Configuration)
                .AddValidators()
                .AddHelpers()
                .AddMemoryCache()
                .AddHttpClient()
                .AddJwt()
                .AddDistributedIsolatedExecutor(new DbObjectLockProvider(Configuration.GetConnectionString("DefaultConnection"), Configuration.GetSection("ConnectionStrings:DatabaseType").Value))
                .AddTelegramBots(Configuration)
                .AddSerilogWithElastic(Configuration)
                .AddExceptionHandlers()
                .AddCorsWithDefaultPolicy()
                .ConfigureAllOptions(Configuration)
                .AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Version = "v1",
                        Title = nameof(UploadFileApi)
                    });
                    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        Scheme = "Bearer",
                        Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n\r\n\r\nExample: 'Bearer {your jwt-token}'",
                    });
                    options.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            Array.Empty<string>()
                        }
                    });

                    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
                })
                .AddControllers();

            services.AddHttpClient<IisHttpClient>();

            services.AddAuthentication(Configuration)
                    .AddAuthorization();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            Logger42.Init(app.ApplicationServices);
            HandlerException42.Init(app.ApplicationServices);
            ConfigurationHelper.SetConfiguration(Configuration);

            app
                .UseCors()
                .UseRouting()
                .UseSwagger()
                .UseSwaggerUI(options =>
                     {
                         options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                     })
                .UseAuthentication()
                .UseAuthorization()
                .UseEndpoints(endpoints =>
                    {
                        endpoints.MapGet("/", context =>
                        {
                            context.Response.ContentType = "text/html";
                            return context.Response.WriteAsync(
                                "<center style='margin-top:50px'>" +
                                "<b>UploadFileApi works!</b>" +
                                "</center>");
                        });
                        endpoints.MapControllers();
                    });
        }
    }
}
