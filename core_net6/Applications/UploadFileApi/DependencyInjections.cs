﻿using Microsoft.Extensions.DependencyInjection;
using Repositories;
using Clouds42.HandlerExeption.Contract;
using HandlerExceptionClass = Clouds42.HandlerException.HandlerException;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Clouds42.CoreWorker.JobWrappers.UploadFile;
using Clouds42.AccountDatabase.Create.Managers;
using Clouds42.AccountDatabase.Create.Helpers;
using Clouds42.AccountDatabase.Contracts.Create.Interfaces;
using Clouds42.AccountDatabase.Create.Providers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Common.Cache;
using Clouds42.Logger;
using Clouds42.RuntimeContext;
using Clouds42.Repositories.Interfaces.Common;
using UploadFileApi.Filters;
using UploadFileApi.Providers;
using Clouds42.Logger.Serilog;
using System.Linq;
using System.Net.Http.Headers;
using Clouds42.Jwt.Contracts;

namespace UploadFileApi
{
    public static class DependencyInjections
    {

        /// <summary>
        /// Регестрирует DI объекты классов менеджеров
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddManagers(this IServiceCollection services)
        {
            services.AddTransient<UploadFilesManager, UploadFilesManager>();
            services.AddTransient<IMergeChunksToInitialFileJobWrapper, MergeChunksToInitialFileJobWrapper>();
            services.AddTransient<AccountDatabaseFilesManager, AccountDatabaseFilesManager>();
            
            return services;
        }

        /// <summary>
        /// Регестрирует DI объекты классов помощников
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddHelpers(this IServiceCollection services)
        {
            services.AddTransient<UploadedFileHelper, UploadedFileHelper>();
            services.AddTransient<UploadingFilePathHelper, UploadingFilePathHelper>();
          
            return services;
        }

        /// <summary>
        /// Регестрирует DI UnitOfWork
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }

        /// <summary>
        /// Регестрирует DI обработчика ошибок
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddExceptionHandlers(this IServiceCollection services)
        {
            services.AddTransient<IHandlerException, HandlerExceptionClass>();

            return services;
        }

        /// <summary>
        /// Регестрирует DI обработчика ошибок
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddTransient<LoadFileValidator, LoadFileValidator>();

            return services;
        }


        /// <summary>
        /// Регестрирует DI объекты классов провайдеров
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddProviders(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAccountDatabaseParseZipFileProvider, AccountDatabaseParseZipFileProvider>();
            services.AddTransient<AccountDataProvider, AccountDataProvider>();
            services.AddTransient<IUploadedFileProvider, UploadedFileProvider>();
            services.AddTransient<IUploadFilesProvider, UploadFilesProvider>();
            services.AddTransient<IAccessProvider, UploadApiAccessProvider>();
            services.AddTransient<ILogger42, SerilogLogger42>();
            services.AddTransient<ICoreExecutionContext, UploadFileExecutionContext>();

            var cacheOptions = configuration.GetSection(nameof(CacheOptions));

            if(cacheOptions.Exists() && cacheOptions[nameof(CacheOptions.CacheType)] == "Local")
            {
               services.AddTransient<ICacheProvider, LocalCacheProvider>();
            }
          
            return services;
        }


        /// <summary>
        /// Редактирует настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection ConfigureAllOptions(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .ConfigureMvcOptions()
                .ConfigureJsonOptions()
                .ConfigureApiBehaviorOptions()
                .ConfigureCacheOptions(configuration);
        }

        /// <summary>
        /// Редактирует MVC настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        private static IServiceCollection ConfigureMvcOptions(this IServiceCollection services)
        {
            return services.Configure<MvcOptions>(options =>
            {
                //Регестрируем свой фильтр валидации моделей
                options.Filters.Add<ValidateModelActionFilterAttribute>();
                options.Filters.Add<UnhandledExceptionFilterAttribute>();
                options.Filters.Add<RemoveErrorHeadersForAnonymousAsyncActionFilter>();
            });
        }

        

        /// <summary>
        /// Редактирует JSON настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        private static IServiceCollection ConfigureJsonOptions(this IServiceCollection services)
        {
            return services.Configure<JsonOptions>(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });
        }

        /// <summary>
        /// Редактирует Cache настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        private static IServiceCollection ConfigureCacheOptions(this IServiceCollection services, IConfiguration configuration)
        {
            return services.Configure<CacheOptions>(configuration.GetSection(nameof(CacheOptions)));
        }

        /// <summary>
        /// Редактирует ApiBehavior настройки
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        private static IServiceCollection ConfigureApiBehaviorOptions(this IServiceCollection services)
        {
            return services.Configure<ApiBehaviorOptions>(options =>
            {
                //Отключаем системную проверку волидации модели
                options.SuppressModelStateInvalidFilter = true;
            });
        }


        /// <summary>
        /// Редактирует настройки максимального размера переданного контента
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection ConfigureMaxContentSize(this IServiceCollection services)
        {
            services.Configure<FormOptions>(options =>
            {
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = int.MaxValue;
                options.MultipartHeadersLengthLimit = int.MaxValue;
            });

            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = int.MaxValue;
            });

            services.Configure<KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = int.MaxValue;
            });

            return services;
        }

        /// <summary>
        /// Регестрирует CORS
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddCorsWithDefaultPolicy(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                    builder.SetIsOriginAllowed(_ => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            return services;
        }

        /// <summary>
        /// Регестрирует возможность аутентификации при помощи JWT
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <param name="configuration"></param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
          
            services
                .AddAuthentication(option =>
                {
                    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(async options =>
                {
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            _ = AuthenticationHeaderValue.TryParse(context.Request.Headers.Authorization.FirstOrDefault(),
                                    out var headerValue);

                            context.Token = context.Request.Cookies.ContainsKey("AccessToken")
                                ? context.Request.Cookies["AccessToken"]
                                : headerValue?.Parameter;

                            return Task.CompletedTask;
                        }
                    };
                    options.RequireHttpsMetadata = true;
                    options.SaveToken = true;
                    options.TokenValidationParameters = await services.BuildServiceProvider().GetRequiredService<IJwtHelper>().GeTokenValidationParameters();
                });

            return services;
        }
    }
}
