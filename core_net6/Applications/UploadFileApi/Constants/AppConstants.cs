﻿namespace UploadFileApi.Constants
{
    /// <summary>
    /// Константы приложения
    /// </summary>
    public static class AppConstants
    {
        /// <summary>
        /// Константы для CORS
        /// </summary>
        public static class Cors
        {
            /// <summary>
            /// Дефолтные настройки CORS
            /// </summary>
            public static class Default
            {
                /// <summary>
                /// Дефолтное значение имени для настроек CorsPolicy
                /// </summary>
                public const string PolicyName = "CrossOriginPolicy";

                /// <summary>
                /// Дефолтные заголовки для настроек CorsPolicy (<see cref="PolicyName"/>)
                /// </summary>
                public static readonly string[] AcceptHeaders = ["Authorization", "Accept", "Content-Type", "Origin", "UploadedFileId", "CountOfFileParts"
                ];

                /// <summary>
                /// Дефолтные веб методы для настроек CorsPolicy (<see cref="PolicyName"/>)
                /// </summary>
                public static readonly string[] WebMethods = ["GET", "POST", "PUT", "DELETE", "OPTIONS"];

                /// <summary>
                /// Дефолтные заголовки на ответ для настроек CorsPolicy (<see cref="PolicyName"/>)
                /// </summary>
                public static readonly string[] ExposedHeaders = ["Token-Expired", "Invalid-Token", "Authorization"];
            }
        }
    }
}