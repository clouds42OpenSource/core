﻿using Microsoft.AspNetCore.Mvc;

namespace UploadFileApi.Controllers
{
    /// <summary>
    /// Базовый API Controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
    }
}
