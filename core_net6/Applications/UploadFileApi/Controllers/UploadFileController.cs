﻿using System;
using Clouds42.AccountDatabase.Create.Managers;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UploadFileApi.Extensions;

namespace UploadFileApi.Controllers
{
    /// <summary>
    /// Контроллер загружаемых файлов 
    /// </summary>
    [Authorize]
    public sealed class UploadFileController(UploadFilesManager uploadFilesManager) : BaseApiController
    {
        /// <summary>
        /// Инициировать процесс загрузки файла
        /// </summary>
        /// <param name="model">Модель запроса инициализации загрузки файла</param>
        /// <returns>ID объекта загружаемого файла</returns>
        [HttpPost]
        public IActionResult StartUpload([FromBody] InitUploadFileRequestDto model)
        {
            var result = uploadFilesManager.InitUploadProcess(model);

            return Ok(new InitUploadFileResultDto { UploadedFileId = result.Result });
        }

        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        /// <returns>Результат загрузки части файла</returns>
        [HttpPost]
        public IActionResult UploadChunk([FromForm]UploadChunkDto uploadChunkDto)
        {
            var result =
                uploadFilesManager.UploadChunkOfDbFile(uploadChunkDto);
            if (result.Error)
                return BadRequest();
            
            return Ok();
        }
        
        /// <summary>
        /// Запустить процесс склейки частей в исходный файл
        /// </summary>
        /// <returns>Результат склейки частей файла</returns>
        [HttpPost]
        public IActionResult MergeChunks([FromBody] InitMergeChunksToInitialFileProcessDto initMergeChunksToInitialFileProcessDto)
        {
            var result = uploadFilesManager.InitMergeChunksToInitialFileProcess(initMergeChunksToInitialFileProcessDto.UploadFileId, initMergeChunksToInitialFileProcessDto.CountOfChunks);

            return Ok(new UploadChunkOfFileResultDto { Result = !result.Error, ErrorMessage = result.Message });
        }

        /// <summary>
        /// Проверить статус загрузки бэкапа инф. базы
        /// </summary>
        /// <returns>Cтатус загрузки бэкапа инф. базы</returns>
        [HttpGet]
        public IActionResult UploadStatus(Guid uploadFileId)
        {
            var result = uploadFilesManager.CheckFileUploadStatus(uploadFileId);

            return Ok(new UploadChunkOfFileResultDto { Result = result.Result, ErrorMessage = result.Message });
        }


            #region старые эндпоинты для обратной совместимости
        /// <summary>
        /// Загрузить часть файла
        /// </summary>
        /// <returns>Результат загрузки части файла</returns>
        [HttpPost]
        public IActionResult UploadChunkOfFile()
        {
            var result =
                uploadFilesManager.UploadChunkOfDbFile(HttpContext.Request.CreateThinHttpRequestObject());

            return Ok(new UploadChunkOfFileResultDto { Result = !result.Error, ErrorMessage = result.Message });
        }

        /// <summary>
        /// Проверить загруженный файл на соответствие требованиям
        /// </summary>
        /// <returns>Результат проверки</returns>
        [HttpPost]
        public IActionResult CheckLoadFileValidity()
        {
            var result = uploadFilesManager.CheckLoadFileValidity(HttpContext.Request.CreateThinHttpRequestObject());

            return Ok(new UploadChunkOfFileResultDto { Result = !result.Error, ErrorMessage = result.Message });
        }

        /// <summary>
        /// Инициировать процесс загрузки файла
        /// </summary>
        /// <param name="model">Модель запроса инициализации загрузки файла</param>
        /// <returns>ID объекта загружаемого файла</returns>
        [HttpPost]
        public IActionResult InitUploadProcess([FromBody] InitUploadFileRequestDto model)
        {
            var result = uploadFilesManager.InitUploadProcess(model);

            return Ok(new InitUploadFileResultDto { UploadedFileId = result.Result });
        }

        /// <summary>
        /// Запустить процесс склейки частей в исходный файл
        /// </summary>
        /// <returns>Результат склейки частей файла</returns>
        [HttpPost]
        public IActionResult InitMergeChunksToInitialFileProcess()
        {
            var result = uploadFilesManager.InitMergeChunksToInitialFileProcess(HttpContext.Request.CreateThinHttpRequestObject());

            return Ok(new UploadChunkOfFileResultDto { Result = !result.Error, ErrorMessage = result.Message });
        }

        /// <summary>
        /// Проверить статус загрузки бэкапа инф. базы
        /// </summary>
        /// <returns>Cтатус загрузки бэкапа инф. базы</returns>
        [HttpPost]
        public IActionResult CheckFileUploadStatus()
        {
            var result = uploadFilesManager.CheckFileUploadStatus(HttpContext.Request.CreateThinHttpRequestObject());

            return Ok(new UploadChunkOfFileResultDto { Result = result.Result, ErrorMessage = result.Message });
        }
        #endregion
    }
}
