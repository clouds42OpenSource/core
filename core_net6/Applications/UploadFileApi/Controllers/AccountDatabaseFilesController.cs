﻿using System;
using Clouds42.AccountDatabase.Create.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace UploadFileApi.Controllers
{
    /// <summary>
    /// Контроллер загружаемых файлов баз. 
    /// </summary>
    [Authorize]
    public sealed class AccountDatabaseFilesController(AccountDatabaseFilesManager accountDatabaseFilesManager)
        : BaseApiController
    {
        /// <summary>
        /// Получить информацию из zip файла информационной базы.
        /// Список пользователей, информация о конфигурации, тип шаблона.
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Информацию о базе из Zip</returns>
        [HttpGet]
        public IActionResult GetDatabaseInfoFromZip(Guid uploadedFileId)
        {
            var data = accountDatabaseFilesManager.GetDatabaseInfoFromZip(uploadedFileId);
            return Ok(data);
        }

        /// <summary>
        /// Проверка совместимости релиза шаблона и релиза из zip файла информационной базы
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Результат проверки</returns>
        [HttpGet]
        public IActionResult ValidateConfigVersionInZip(Guid uploadedFileId)
        {
            var data = accountDatabaseFilesManager.ValidateVersionXmlInZipFile(uploadedFileId);
            return Ok(data);
        }
        
        /// <summary>
        /// Ping the upload file api
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Ping()
        {
            return Ok("Pong");
        }
    }
}
