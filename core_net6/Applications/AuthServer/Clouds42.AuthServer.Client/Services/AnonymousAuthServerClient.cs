﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Clouds42.AuthServer.Client.Contracts;
using Clouds42.AuthServer.Client.Responses;
using Clouds42.Configurations.Configurations;

namespace Clouds42.AuthServer.Client.Services
{
    public class AnonymousAuthServerClient : IAnonymousAuthServerClient
    {
        private IHttpClientFactory _httpClientFactory;
        protected string BaseUrl => CloudConfigurationProvider.AuthServer.GetBaseUrl();

        public AnonymousAuthServerClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        /// <summary>
        /// Запросить токен авторизации с помощью логина и пароля юзера
        /// </summary>
        /// <param name="username">Login юзера</param>
        /// <param name="password">Password юзера</param>
        /// <returns>access и refresh токены</returns>
        public async Task<AuthenticateResponse> AuthenticateAsync(string username, string password)
        {
            var path = CloudConfigurationProvider.AuthServer.GetAuthenticatePath();
            var request = new RestRequest(BuildUri(path),Method.Post)
                   .AddJsonBody(new { Username = username, Password = password });
            var response = await RestClient.ExecuteAsync<AuthenticateResponse>(request);
            EnsureOkStatus(response);
            return response.Data;
        }

        /// <summary>
        /// Запросить токен авторизации с помощью refresh токена
        /// </summary>
        /// <param name="jsonWebToken">Устаревший аccess token </param>
        /// <param name="refreshToken">Refresh токен</param>
        /// <returns>Новые access и refresh токены</returns>
        public async Task<RefreshTokenResponse> RefreshTokenAsync(string jsonWebToken, string refreshToken)
        {
            var path = CloudConfigurationProvider.AuthServer.GetRefreshPath();
            var request = new RestRequest(BuildUri(path),Method.Post)
               .AddJsonBody(new { JsonWebToken = jsonWebToken, RefreshToken = refreshToken });
            var response = await RestClient.ExecuteAsync<RefreshTokenResponse>(request);
            EnsureOkStatus(response);
            return response.Data;
        }

        /// <summary>
        /// Метод для проверки статус кода ответа
        /// Вызовет Exception если код ответа отличяется от 200OK
        /// </summary>
        /// <param name="response"></param>
        protected void EnsureOkStatus(RestResponse response)
        {
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
                throw new InvalidOperationException(response.Content);
        }

        /// <summary>
        /// Построить URI эндпоинта
        /// </summary>
        /// <param name="path">Относительный путь к эндпоинту</param>
        /// <returns></returns>
        protected Uri BuildUri(string path)
        {   
            var uri = new UriBuilder(BaseUrl);
            if (string.IsNullOrWhiteSpace(path))
                return uri.Uri;

            path = path.TrimStart('/');
            path = System.IO.Path.Combine(uri.Path, path);
            uri.Path = path;
            return uri.Uri;
        }
            
    }
}


