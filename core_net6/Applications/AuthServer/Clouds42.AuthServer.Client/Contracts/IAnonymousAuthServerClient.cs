﻿using System.Threading.Tasks;
using Clouds42.AuthServer.Client.Responses;

namespace Clouds42.AuthServer.Client.Contracts
{
    /// <summary>
    /// AuthServer клиент для работы с незащищенными (анонимными) эндпоинтами
    /// </summary>
    public interface IAnonymousAuthServerClient
    {
        /// <summary>
        /// Запросить токен авторизации с помощью логина и пароля юзера
        /// </summary>
        /// <param name="username">Login юзера</param>
        /// <param name="password">Password юзера</param>
        /// <returns>access и refresh токены</returns>
        Task<AuthenticateResponse> AuthenticateAsync(string username, string password);

        /// <summary>
        /// Запросить токен авторизации с помощью refresh токена
        /// </summary>
        /// <param name="jsonWebToken">Устаревший аccess token </param>
        /// <param name="refreshToken">Refresh токен</param>
        /// <returns>Новые access и refresh токены</returns>
        Task<RefreshTokenResponse> RefreshTokenAsync(string jsonWebToken, string refreshToken);
    }
}


