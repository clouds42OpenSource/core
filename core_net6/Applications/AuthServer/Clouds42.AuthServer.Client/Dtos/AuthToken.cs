﻿using System;

namespace Clouds42.AuthServer.Client.Dtos
{

    /// <summary>
    /// Токен авторизации юзера
    /// </summary>
    public class AuthToken
    {

        /// <summary>
        /// Access Token для запроса к защищенным эндпоинтам
        /// </summary>
        public string JsonWebToken { get; set; }

        /// <summary>
        /// Refresh токен для запроса нового Access токена
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Срок действия access токена в секундах
        /// </summary>
        public int ExpiresIn { get; set; }

        /// <summary>
        /// Дата когда токен был запрошен
        /// </summary>
        public DateTime RequestedOn { get; set; }


        /// <summary>
        /// Метод для проверки закончится ли срок действия токена через указанный период времени
        /// </summary>
        /// <param name="skew"></param>
        /// <returns></returns>
        public bool WillExpire(TimeSpan skew)
        {
            return DateTime.Now.Add(skew) >= RequestedOn.AddSeconds(ExpiresIn);
        }
    }
}


