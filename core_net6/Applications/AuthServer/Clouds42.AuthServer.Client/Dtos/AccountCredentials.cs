﻿namespace Clouds42.AuthServer.Client.Dtos
{
    /// <summary>
    /// Явки пользователя
    /// </summary>
    public class AccountCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}


