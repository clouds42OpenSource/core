﻿namespace Clouds42.AuthServer.Client.Responses
{
    public class RefreshTokenResponse
    {
        /// <summary>
        /// AccessToken, токен для аутентификации
        /// </summary>
        public string JsonWebToken { get; set; }

        /// <summary>
        /// RefreshToken, токен для обновления пары токенов (JsonWebToken и RefreshToken)
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Срок действия токена в секундах
        /// </summary>
        public int ExpiresIn { get; set; }

        /// <summary>
        /// Если <c>true</c>, то обновление токенв прошло успешно
        /// </summary>
        public bool IsSuccess { get; set; }
    }
}


