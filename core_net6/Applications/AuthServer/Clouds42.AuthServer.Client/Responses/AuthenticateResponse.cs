﻿namespace Clouds42.AuthServer.Client.Responses
{
    public class AuthenticateResponse
    {
        /// <summary>
        /// Json Web Token
        /// </summary>
        public string JsonWebToken { get; set; }

        /// <summary>
        /// Токен для обновления пары токенов (токена аутентификации и refresh токена)
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Период валидности токена в секундах
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}


