﻿using Clouds42.AuthServer.Client.Contracts;
using Clouds42.AuthServer.Client.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AuthServer.Client
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddFrozenAccountBalance(IServiceCollection services)
        {
            services.AddTransient<IAnonymousAuthServerClient, AnonymousAuthServerClient>();
            return services;
        }
    }
}
