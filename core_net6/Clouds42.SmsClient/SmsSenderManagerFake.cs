﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels;

namespace Clouds42.SmsClient
{
    public class SmsSenderManagerFake : ISmsSenderManager
    {
        public ManagerResult NotifyAccountUser(AccountUser accountUser,
            IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword)
        {
            return new ManagerResult{State = ManagerResultState.Ok};
        }

        public int SendNewActivationSms(string userLogin)
        {
            return 0;
        }
    }
}