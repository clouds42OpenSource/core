﻿using System.Threading.Tasks;

namespace Clouds42.SmsClient
{
    /// <summary>
    /// Клиент для отправки SMS
    /// </summary>
    public interface ISmsClient
    {
        /// <summary>
        /// Отправить SMS
        /// </summary>
        /// <param name="phoneNumbers">Номер телефона</param>
        /// <param name="message">отправляемое сообщение</param>
        Task SendSms(string phoneNumbers, string message);
    }
}
