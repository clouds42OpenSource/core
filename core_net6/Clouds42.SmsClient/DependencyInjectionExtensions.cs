﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.SmsClient
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSmsClient(this IServiceCollection services)
        {
            services
                .AddTransient<ISmsClient, SmsClient>()
                .AddTransient<ISmsSenderManager, SmsSenderManager>();

            return services;
        }
    }
}
