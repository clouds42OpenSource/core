﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;

namespace Clouds42.SmsClient
{
    /// <summary>
    /// Клиент для отправки SMS
    /// </summary>
    public class SmsClient(ILogger42 logger) : ISmsClient
    {
        private readonly string _smsClientLogin = CloudConfigurationProvider.SmsClient.GetClientLogin();
        private readonly string _smsClientPassword = CloudConfigurationProvider.SmsClient.GetClientPassword();
        private readonly string _smsClientCharset = CloudConfigurationProvider.SmsClient.GetClientCharset();
        private readonly string _smsServiceUrl = CloudConfigurationProvider.SmsClient.GetServiceUrl();
        private readonly bool _needSendSms = CloudConfigurationProvider.SmsClient.GetNeedSendSms();


        /// <summary>
        /// Отправить SMS
        /// </summary>
        /// <param name="phoneNumbers">Номер телефона</param>
        /// <param name="message">отправляемое сообщение</param>
        public async Task SendSms(string phoneNumbers, string message)
        {
            if (!_needSendSms)
                return;

            await Send(GetRequestArguments(phoneNumbers, message));

            logger.Trace($"SMS отправлено на номер\\а {phoneNumbers} с текстом {message}");
        }

        /// <summary>
        /// Отправить SMS
        /// </summary>
        /// <param name="requestArguments">Параметры для отправки запроса</param>
        /// <returns>Результат выполнения</returns>
        private async Task Send(string requestArguments)
        {
            using var client = new HttpClient();

            var response = await client.GetAsync(GetUrlForSendSms(requestArguments));

            string ret;
            var i = 0;

            do
            {
                if (i > 0)
                    await Task.Delay(2000);

                ret = await response.Content.ReadAsStringAsync();

            } while (ret == "" && ++i < 3);
        }

        /// <summary>
        /// Получить URL для отправки SMS
        /// </summary>
        /// <param name="requestArguments">Параметры для отправки запроса</param>
        /// <returns>URL для отправки SMS</returns>
        private string GetUrlForSendSms(string requestArguments)
            => $"{_smsServiceUrl}?{requestArguments}";

        /// <summary>
        /// Получить строку аргументов для запроса
        /// </summary>
        /// <param name="phoneNumbers">Номера телефонов</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Строка аргументов для запроса</returns>
        private string GetRequestArguments(string phoneNumbers, string message)
            => $"login={UrlEncode(_smsClientLogin)}&psw={UrlEncode(_smsClientPassword)}&fmt=1&charset={_smsClientCharset}" +
               $"&cost=3&phones={UrlEncode(phoneNumbers)}&mes={UrlEncode(message)}&id=0&translit=0";

        /// <summary>
        /// кодирование параметра в http-запросе
        /// </summary>       
        private static string UrlEncode(string str)
            => WebUtility.UrlEncode(str);
    }
}
