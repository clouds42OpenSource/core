﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.DataModels;

namespace Clouds42.SmsClient
{
    /// <summary>
    /// Менеджер управления смс рассылкой
    /// </summary>
    public interface ISmsSenderManager
    {
        /// <summary>
        ///  Отправить сообщение пользователю на телефон и почту
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <param name="registrationSource">Источник регистрации</param>
        /// <param name="generatedPassword">Сгенерированный пароль.</param>
        ManagerResult NotifyAccountUser(AccountUser accountUser,
            IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword);

        /// <summary>
        /// Необходим для промо сайта. Отправка СМС еще раз
        /// </summary>
        int SendNewActivationSms(string userLogin);
    }
}