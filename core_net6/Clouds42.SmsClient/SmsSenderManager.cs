﻿using System;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.SmsClient
{
    /// <summary>
    ///     Менеджер управления смс рассылкой
    /// </summary>
    public class SmsSenderManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountUserRegistrationNotificationProvider accountUserRegistrationNotificationProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException), ISmsSenderManager
    {
        /// <summary>
        /// Необходим для промо сайта. Отправка СМС еще раз
        /// </summary>
        public int SendNewActivationSms(string userLogin)
        {
            AccessProvider.HasAccess(ObjectAction.Accounts_ReSendSms);

            try
            {
                if (string.IsNullOrEmpty(userLogin)) return 1;

                // генерим новый код и шлем смс новый код подтверждения
                accountUserRegistrationNotificationProvider.SendNewActivationSms(userLogin);

                logger.Trace("Повторная отправка СМС пользователю {0}", userLogin);

                return 0;
            }
            catch (Exception ex)
            {
                logger.Warn("Ошибка при повторной отправке смс: {0}", ex.Message);
                handlerException.Handle(ex, "[Ошибка при повторной отправке СМС]");
                return 1;
            }
        }

        /// <summary>
        ///  Отправить сообщение пользователю на телефон и почту
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <param name="registrationSource">Источник регистрации</param>
        /// <param name="generatedPassword">Сгенерированный пароль.</param>
        public ManagerResult NotifyAccountUser(AccountUser accountUser,
            IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword)
        {
            try
            {
                accountUserRegistrationNotificationProvider.NotifyAccountUser(accountUser, registrationSource, generatedPassword);
                return Ok();
            }
            catch (Exception e)
            {
                logger.Warn($"Ошибка при нотификации пользователя: {accountUser.Id}. Exception: {e.GetFullInfo()}");
                handlerException.Handle(e, "[Ошибка при нотификации пользователя]");
                return PreconditionFailed(e.GetFullInfo());
            }
        }
        
    }
}
