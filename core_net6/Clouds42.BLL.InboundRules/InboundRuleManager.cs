﻿using System;
using Clouds42.BLL.InboundRules.Models;
using Clouds42.Logger;
using Microsoft.Web.Administration;
namespace Clouds42.BLL.InboundRules
{
    /// <summary>
    /// Менеджер для взаимодействия с
    /// правилами входящего трафика на IIS
    /// </summary>
    public sealed class InboundRuleManager : IDisposable
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Менеджер для работы с IIS
        /// </summary>
        private readonly ServerManager _serverManager;

        /// <summary>
        /// Путь к секции правил входящего трафика
        /// </summary>
        private const string PathToGlobalRulesSection = "system.webServer/rewrite/globalRules";

        /// <summary>
        /// Коллекция правил входящего трафика
        /// </summary>
        public InboundRuleCollection InboundRules { get; }

        private InboundRuleManager(ServerManager serverManager, ILogger42 logger)
        {
            _serverManager = serverManager;
            InboundRules = new InboundRuleCollection(serverManager.GetApplicationHostConfiguration()
                .GetSection(PathToGlobalRulesSection).GetCollection());
        }

        /// <summary>
        /// Создать новый инстанс
        /// </summary>
        /// <param name="serverName">Адрес подключения</param>
        /// <returns>Менеджер для взаимодействия с
        /// правилами входящего трафика на IIS</returns>
        public static InboundRuleManager CreateNewInstanceFor(string serverName)
        {
            try
            {
                var serverManager = ServerManager.OpenRemote(serverName);
                var count = serverManager.Sites.Count;
                _logger.Trace($"Кол-во сайтов на сервере {serverName} {count}");
                return new InboundRuleManager(serverManager, _logger);
            }
            catch (Exception)
            {
                throw new ServerManagerException($"Не удалось полдключиться к серверу по адресу '{serverName}'");
            }
        }

        /// <summary>
        /// Сохранить изменения
        /// </summary>
        public void CommitChanges()
        {
            _serverManager.CommitChanges();
        }

        /// <summary>
        /// Уничтожить объект
        /// </summary>
        public void Dispose()
        {
            _serverManager.Dispose();
        }
    }
}
