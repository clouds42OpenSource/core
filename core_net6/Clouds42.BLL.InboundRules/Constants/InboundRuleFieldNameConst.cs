﻿namespace Clouds42.BLL.InboundRules.Constants
{
    /// <summary>
    /// Название полей секции правило маршрутизации
    /// </summary>
    public static class InboundRuleFieldNameConst
    {
        /// <summary>
        /// Название правила входящего трафика
        /// </summary>
        public const string Name = "name";

        /// <summary>
        /// Остановить обработку последующих правил
        /// </summary>
        public const string StopProcessing = "stopProcessing";
    }
}
