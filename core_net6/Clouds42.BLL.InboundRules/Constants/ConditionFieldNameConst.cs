﻿namespace Clouds42.BLL.InboundRules.Constants
{
    /// <summary>
    /// Название полей секции условия
    /// </summary>
    public static class ConditionFieldNameConst
    {
        /// <summary>
        /// Не учитывать регистр
        /// </summary>
        public const string IgnoreCase = "ignoreCase";

        /// <summary>
        /// Не соответсвует шаблону
        /// </summary>
        public const string DoesNotMatchPattern = "negate";

        /// <summary>
        /// Ввод
        /// </summary>
        public const string Input = "input";

        /// <summary>
        /// Шаблон
        /// </summary>
        public const string Pattern = "pattern";

        /// <summary>
        /// Логическая группировка
        /// </summary>
        public const string LogicalGrouping = "logicalGrouping";
    }
}
