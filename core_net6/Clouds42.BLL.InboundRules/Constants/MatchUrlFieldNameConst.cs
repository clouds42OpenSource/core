﻿namespace Clouds42.BLL.InboundRules.Constants
{
    /// <summary>
    /// Название полей секции адреса соответствия
    /// </summary>
    public static class MatchUrlFieldNameConst
    {
        /// <summary>
        /// Адрес
        /// </summary>
        public const string Url = "url";

        /// <summary>
        /// Не учитывать регистр
        /// </summary>
        public const string IgnoreCase = "ignoreCase";

    }
}
