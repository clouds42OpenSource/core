﻿namespace Clouds42.BLL.InboundRules.Constants
{
    /// <summary>
    /// Секции в конфиге правил входящего трафика на IIS
    /// </summary>
    public static class InboundRuleSectionConst
    {
        /// <summary>
        /// Секция правила входящего трафика
        /// </summary>
        public const string InboundRuleSection = "rule";

        /// <summary>
        /// Секция адреса соответствия
        /// </summary>
        public const string MatchUrlSection = "match";

        /// <summary>
        /// Секция действия для правил входящего трафика
        /// </summary>
        public const string ActionSection = "action";

        /// <summary>
        /// Секция коллекции условий
        /// </summary>
        public const string ConditionsSection = "conditions";

        /// <summary>
        /// Секция условия
        /// </summary>
        public const string ConditionSection = "add";
    }
}
