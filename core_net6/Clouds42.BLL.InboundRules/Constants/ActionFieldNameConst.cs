﻿namespace Clouds42.BLL.InboundRules.Constants
{
    /// <summary>
    /// Название полей секции действия для правила маршрутизации
    /// </summary>
    public static class ActionFieldNameConst
    {
        /// <summary>
        /// Тип действия
        /// </summary>
        public const string Type = "type";

        /// <summary>
        /// Адрес переадресации
        /// </summary>
        public const string Url = "url";
    }
}
