﻿using Clouds42.BLL.InboundRules.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Коллекция правил входящего трафика
    /// </summary>
    public class InboundRuleCollection
    {
        private readonly ConfigurationElementCollection _collection;

        public InboundRuleCollection(ConfigurationElementCollection collection)
        {
            _collection = collection;
        }

        /// <summary>
        /// Попытаться получить правило входящего трафика
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inboundRule">Правило входящего трафика</param>
        /// <returns>Результат получения</returns>
        public bool TryGetInboundRule(string name, out InboundRule inboundRule)
        {
            var element = GetByName(name);

            if (element == null)
            {
                inboundRule = null;
                return false;
            }

            inboundRule = new InboundRule(element);
            return true;
        }

        /// <summary>
        /// Создать правило входящего трафика
        /// </summary>
        /// <param name="name">Название правила</param>
        /// <returns>Правило</returns>
        public InboundRule CreateInboundRule(string name)
        {
            var existingElement = GetByName(name);

            if (existingElement != null)
                return new InboundRule(existingElement);

            var ruleElement = _collection.CreateElement(InboundRuleSectionConst.InboundRuleSection);
            var rule = new InboundRule(ruleElement)
            {
                Name = name,
                StopProcessing = true
            };

            _collection.Add(ruleElement);
            return rule;
        }

        /// <summary>
        /// Удалить правило входящего трафика
        /// </summary>
        /// <param name="name">Название правила</param>
        public void RemoveInboundRule(string name)
        {
            var element = GetByName(name);
            element?.Delete();
        }

        /// <summary>
        /// Получить правило входящего трафика
        /// </summary>
        /// <param name="name">Название правила</param>
        /// <returns>Правило входящего трафика</returns>
        private ConfigurationElement GetByName(string name)
        {
            foreach (var element in _collection)
                if ((string)element[InboundRuleFieldNameConst.Name] == name)
                    return element;

            return null;
        }
    }
}
