﻿using Clouds42.BLL.InboundRules.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Адрес соответствия
    /// </summary>
    public class MatchUrl
    {
        private readonly ConfigurationElement _conditionElement;
        private const string DefaultUrl = "(.*)";

        public MatchUrl(ConfigurationElement conditionElement)
        {
            _conditionElement = conditionElement;
        }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Url
        {
            get => (string)_conditionElement[MatchUrlFieldNameConst.Url];
            set => _conditionElement[MatchUrlFieldNameConst.Url] = value;
        }

        /// <summary>
        /// Установить дефолтный адрес
        /// </summary>
        public void SetDefaultUrl() => Url = DefaultUrl;

        /// <summary>
        /// Не учитывать регистр
        /// </summary>
        public bool IgnoreCase
        {
            get => (bool)_conditionElement[MatchUrlFieldNameConst.IgnoreCase];
            set => _conditionElement[MatchUrlFieldNameConst.IgnoreCase] = value;
        }
    }
}
