﻿using System;
using Clouds42.BLL.InboundRules.Constants;
using Clouds42.BLL.InboundRules.Enums;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Коллекция условий правила
    /// </summary>
    public class ConditionCollection
    {
        private readonly ConfigurationElement _configurationElement;
        private readonly ConfigurationElementCollection _collection;

        public ConditionCollection(ConfigurationElement configurationElement)
        {
            _configurationElement = configurationElement;
            _collection = configurationElement.GetCollection();
        }

        /// <summary>
        /// Логическая группировка
        /// </summary>
        public LogicalGrouping LogicalGrouping
        {
            get => (LogicalGrouping) Enum.Parse(typeof(LogicalGrouping),
                (string) _configurationElement[ConditionFieldNameConst.LogicalGrouping]);
            set => _configurationElement[ConditionFieldNameConst.LogicalGrouping] = value.ToString();
        }

        /// <summary>
        /// Попытаться получить условие правила
        /// </summary>
        /// <param name="pattern">Шаблон условия</param>
        /// <param name="condition">Условие</param>
        /// <returns>Результат получения</returns>
        public bool TryGetCondition(string pattern, out Condition condition)
        {
            var element = GetByPattern(pattern);

            if (element == null)
            {
                condition = null;
                return false;
            }

            condition = new Condition(element);
            return true;
        }

        /// <summary>
        /// Создать условие
        /// </summary>
        /// <param name="pattern">Шаблон условия</param>
        /// <returns>Условие</returns>
        public Condition CreateCondition(string pattern)
        {
            var existingElement = GetByPattern(pattern);

            if (existingElement != null)
                return new Condition(existingElement);

            var conditionElement = _collection.CreateElement(InboundRuleSectionConst.ConditionSection);
            var condition = new Condition(conditionElement)
            {
                Pattern = pattern,
                IgnoreCase = true
            };

            condition.SetDafaultInput();
            _collection.Add(conditionElement);
            return condition;
        }

        /// <summary>
        /// Удалить условие
        /// </summary>
        /// <param name="pattern">Шаблон условия</param>
        public void RemoveCondition(string pattern)
        {
            var element = GetByPattern(pattern);
            element?.Delete();
        }

        /// <summary>
        /// Получить условие по шаблону
        /// </summary>
        /// <param name="pattern">Шаблон условия</param>
        /// <returns>Условие</returns>
        private ConfigurationElement GetByPattern(string pattern)
        {
            foreach (var element in _collection)
                if ((string)element[ConditionFieldNameConst.Pattern] == pattern)
                    return element;

            return null;
        }
    }
}
