﻿using Clouds42.BLL.InboundRules.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Условие правила
    /// </summary>
    public class Condition
    {
        private readonly ConfigurationElement _conditionElement;
        private const string DefaultInput = "{PATH_INFO}";

        public Condition(ConfigurationElement conditionElement)
        {
            _conditionElement = conditionElement;
        }

        /// <summary>
        /// Не учитывать регистр
        /// </summary>
        public bool IgnoreCase
        {
            get => (bool)_conditionElement[ConditionFieldNameConst.IgnoreCase];
            set => _conditionElement[ConditionFieldNameConst.IgnoreCase] = value;
        }

        /// <summary>
        /// Не соответсвует шаблону
        /// </summary>
        public bool DoesNotMatchPattern
        {
            get => (bool)_conditionElement[ConditionFieldNameConst.DoesNotMatchPattern];
            set => _conditionElement[ConditionFieldNameConst.DoesNotMatchPattern] = value;
        }

        /// <summary>
        /// Ввод
        /// </summary>
        public string Input
        {
            get => (string)_conditionElement[ConditionFieldNameConst.Input];
            set => _conditionElement[ConditionFieldNameConst.Input] = value;
        }

        /// <summary>
        /// Шаблон
        /// </summary>
        public string Pattern
        {
            get => (string)_conditionElement[ConditionFieldNameConst.Pattern];
            set => _conditionElement[ConditionFieldNameConst.Pattern] = value;
        }

        /// <summary>
        /// Установить дефолтное значение ввода
        /// </summary>
        public void SetDafaultInput() => Input = DefaultInput;
    }
}
