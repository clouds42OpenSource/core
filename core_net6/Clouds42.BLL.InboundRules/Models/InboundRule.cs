﻿using Clouds42.BLL.InboundRules.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Правило входящего трафика
    /// </summary>
    public class InboundRule
    {
        private readonly ConfigurationElement _ruleElement;

        /// <summary>
        /// Адрес соответствия
        /// </summary>
        public MatchUrl MatchUrl { get; }

        /// <summary>
        /// Действие для правил входящего трафика
        /// </summary>
        public Action Action { get; }

        /// <summary>
        /// Коллекция условий
        /// </summary>
        public ConditionCollection Conditions { get; }

        public InboundRule(ConfigurationElement ruleElement)
        {
            _ruleElement = ruleElement;
            MatchUrl = new MatchUrl(ruleElement.GetChildElement(InboundRuleSectionConst.MatchUrlSection));
            Action = new Action(ruleElement.GetChildElement(InboundRuleSectionConst.ActionSection));
            Conditions = new ConditionCollection(ruleElement.GetChildElement(InboundRuleSectionConst.ConditionsSection));
        }

        /// <summary>
        /// Название правила входящего трафика
        /// </summary>
        public string Name
        {
            get => (string)_ruleElement[InboundRuleFieldNameConst.Name];
            set => _ruleElement[InboundRuleFieldNameConst.Name] = value;
        }

        /// <summary>
        /// Остановить обработку последующих правил
        /// </summary>
        public bool StopProcessing
        {
            get => (bool)_ruleElement[InboundRuleFieldNameConst.StopProcessing];
            set => _ruleElement[InboundRuleFieldNameConst.StopProcessing] = value;
        }

    }
}
