﻿using Clouds42.BLL.InboundRules.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Действие для правил маршрутизации
    /// </summary>
    public class Action
    {
        private readonly ConfigurationElement _actionElement;
        private const string DefaultType = "Rewrite";
        private const string HttpsScheme = "https://";
        private const string HttpScheme = "http://";

        public Action(ConfigurationElement actionElement)
        {
            _actionElement = actionElement;
        }

        /// <summary>
        /// Тип действия
        /// </summary>
        public string Type
        {
            get => (string)_actionElement[ActionFieldNameConst.Type];
            set => _actionElement[ActionFieldNameConst.Type] = value;
        }

        /// <summary>
        /// Адрес переадресации
        /// </summary>
        public string Url
        {
            get => (string)_actionElement[ActionFieldNameConst.Url];
            set => _actionElement[ActionFieldNameConst.Url] = value;
        }

        /// <summary>
        /// Установить поля дреса
        /// </summary>
        /// <param name="actionUrlProperties">Поля адреса</param>
        public void SetActionUrlProperties(ActionUrlProperties actionUrlProperties)
        {

            var uriScheme = actionUrlProperties.UseHttpsScheme ? HttpsScheme : HttpScheme;
            Url =$"{uriScheme}{actionUrlProperties.WebFarmName}/{actionUrlProperties.Path}";
        }

        /// <summary>
        /// Установить дефолтный тип
        /// </summary>
        public void SetDefaultType() => Type = DefaultType;
    }
}
