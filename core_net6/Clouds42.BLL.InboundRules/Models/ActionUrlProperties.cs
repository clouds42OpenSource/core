﻿namespace Clouds42.BLL.InboundRules.Models
{
    /// <summary>
    /// Поля адреса
    /// </summary>
    public class ActionUrlProperties
    {
        /// <summary>
        /// Использовать https при формировании адреса
        /// </summary>
        public bool UseHttpsScheme { get; set; }

        /// <summary>
        /// Название веб фермы
        /// </summary>
        public string WebFarmName { get; set; }

        /// <summary>
        /// Путь
        /// </summary>
        public string Path { get; set; }
    }
}
