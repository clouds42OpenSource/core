﻿namespace Clouds42.BLL.InboundRules.Enums
{
    /// <summary>
    /// Логическая группировка
    /// </summary>
    public enum LogicalGrouping
    {
        /// <summary>
        /// Совпадение с любым из элементов
        /// </summary>
        MatchAny = 0,

        /// <summary>
        /// Совпадение со всеми элементами
        /// </summary>
        MatchAll = 1
    }
}
