﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace PatchIncreaseRdpCostForRuAccounts.Providers
{
    /// <summary>
    /// Класс для считывания AccountId фильтра из json файла
    /// </summary>
    public static class AccountsFilterReader
    {
        /// <summary>
        /// Получить список аккаунтов из файла
        /// </summary>
        /// <param name="sourceFile">Путь к исходному файлу</param>
        /// <returns>Лист ID аккаунтов</returns>
        public static List<Guid> ReadGetAccountIdsFilter(string sourceFile)
        {
            try
            {
                string json;
                using (var stream = new StreamReader(sourceFile))
                {
                    json = stream.ReadToEnd();
                }
                return JsonConvert.DeserializeObject<List<Guid>>(json);
            }
            catch (Exception ex)
            {
                throw new FileFormatException($"Ошбика при чтении Account ID Filter. {ex.Message}");
            }

        }
    }
}
