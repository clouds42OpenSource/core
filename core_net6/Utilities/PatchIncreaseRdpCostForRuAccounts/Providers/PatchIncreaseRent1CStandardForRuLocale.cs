﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Billing.BillingOperations.Helpers;
using Clouds42.Billing.BillingOperations.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using CommonLib.Enums;
using Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands;
using MediatR;
using Microsoft.Extensions.Configuration;
using PatchIncreaseRdpCostForRuAccounts.Models;
using PatchIncreaseRdpCostForRuAccounts.Selectors;
using Repositories;
using Clouds42.Common.Constants;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Clouds42.DomainContext;


namespace PatchIncreaseRdpCostForRuAccounts.Providers
{
    /// <summary>
    /// Провайдер для изменения стоимости Аренды 1С Web для аккаунтов в ru локали
    /// </summary>
    public partial class PatchIncreaseRent1CStandardForRuLocale(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IConfiguration configuration,
        ILogger42 logger,
        IHandlerException handlerException,
        ISender sender)
    {
        /// <summary>
        /// Флаг запуска патча в тестовом режиме
        /// В тестовом режиме транзакции не будут завершены
        /// </summary>
        public bool NoCommitMode { get; set; }

        /// <summary>
        /// Текущая стоимость Web
        /// </summary>
        public decimal CurrentRate { get; set; }

        /// <summary>
        /// Новая стоимость Web
        /// </summary>
        public decimal NewRate { get; set; }

        /// <summary>
        /// Текущая стоимость Rdp
        /// </summary>
        public decimal CurrentRateRdp { get; set; }

        /// <summary>
        /// Новая стоимость Rdp
        /// </summary>
        public decimal NewRateRdp { get; set; }

        /// <summary>
        /// Признак показывающий как обрабатывать неожиданные значение я таблице тарифов
        /// True: вызвать exception и вся операция будет отменена
        /// False: перезаписать новым значением
        /// </summary>
        public bool ValidateCurrentRate { get; set; }


        /// <summary>
        /// Выполнить патч
        /// </summary>
        /// <param name="accountsFilter">Фильтр аккаунтов для которых нужно пересчитать ресурсы по новым ставкам</param>
        public void Execute(List<Guid> accountsFilter = null)
        {
            LogMessageHelper.PushMessage($"Пересчет стоимости сервиса 'Аренды 1С Web' в RU локали с {CurrentRate} на {NewRate}");
            LogMessageHelper.PushMessage($"Пересчет стоимости сервиса 'Аренды 1С Rdp' в RU локали с {CurrentRateRdp} на {NewRateRdp}");
            if (NoCommitMode) LogMessageHelper.PushMessage("NO COMMIT MODE");
            if (accountsFilter == null) LogMessageHelper.PushMessage("ACCOUNTS FILTER: OFF");
            else { LogMessageHelper.PushMessage($"ACCOUNTS FILTER: ON for {accountsFilter.Count} items"); }
            if (accountsFilter != null)
            {
                UpdateRatesWeb();
                UpdateRatesRdp();
            }

            UpdateCurrentAccounts(configuration, accountsFilter);
        }

        private void UpdateRatesWeb()
        {
            LogMessageHelper.PushMessage("Запущен пересчет тарифов");
            var rates = dbLayer
                .SelectRent1CRatesDataForLocale(LocaleConst.Russia, ResourceType.MyEntUserWeb)
                .ToList();

            int updatedCount = 0;
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                foreach (var rate in rates)
                {
                    if (rate.ResourceType != ResourceType.MyEntUserWeb ||
                        rate.Rate.Cost == NewRate)
                        continue;

                    if (ValidateCurrentRate && rate.Rate.Cost != CurrentRate)
                        throw new InvalidOperationException($"Неожиданное текущее значение тарифа {rate.Rate.Id} {rate.Rate.Cost} руб. Ожидалось {CurrentRate}");

                    var prevValue = rate.Rate.Cost;
                    rate.Rate.Cost = NewRate;
                    dbLayer.RateRepository.Update(rate.Rate);
                    dbLayer.Save();
                    LogMessageHelper.PushMessage($"Тариф {rate.Rate.Id} изменен с {prevValue} на {rate.Rate.Cost}");
                    updatedCount++;
                }
                if (!NoCommitMode)
                {
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
                LogMessageHelper.PushMessage($"Изменение тарифов завершено. Изменено записей: {updatedCount}");
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
        private void UpdateRatesRdp()
        {
            LogMessageHelper.PushMessage("Запущен пересчет тарифов");
            var rates = dbLayer
                .SelectRent1CRatesDataForLocale(LocaleConst.Russia, ResourceType.MyEntUser)
                .ToList();

            int updatedCount = 0;
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                foreach (var rate in rates)
                {
                    if (rate.ResourceType != ResourceType.MyEntUser ||
                        rate.Rate.Cost == NewRateRdp)
                        continue;

                    if (ValidateCurrentRate && rate.Rate.Cost != CurrentRateRdp)
                        throw new InvalidOperationException($"Неожиданное текущее значение тарифа {rate.Rate.Id} {rate.Rate.Cost} руб. Ожидалось {CurrentRateRdp}");

                    var prevValue = rate.Rate.Cost;
                    rate.Rate.Cost = NewRateRdp;
                    dbLayer.RateRepository.Update(rate.Rate);
                    dbLayer.Save();
                    LogMessageHelper.PushMessage($"Тариф {rate.Rate.Id} изменен с {prevValue} на {rate.Rate.Cost}");
                    updatedCount++;
                }
                if (!NoCommitMode)
                {
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
                LogMessageHelper.PushMessage($"Изменение тарифов завершено. Изменено записей: {updatedCount}");
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }
        private void UpdateCurrentAccounts(IConfiguration configuration, List<Guid> accountsFilter = null)
        {
            var accountIds = accountsFilter ?? dbLayer
                .SelectAccountRent1CStandardResourcesForAccount(LocaleConst.Russia)
                .GroupBy(x => x.BillingAccountId)
                .Select(x => x.Key)
                .ToList();
            var accountIdsCount = accountIds.Count;
            LogMessageHelper.PushMessage($"Отобранно всего аккаунтов {accountIdsCount}");
            var optionsBuilder = new DbContextOptionsBuilder<Clouds42DbContext>();
            ;
            if (configuration.GetSection("ConnectionStrings:DatabaseType").Value == ProviderTypeConstants.MsSql)
            {
                optionsBuilder.UseSqlServer(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            else
            {
                optionsBuilder.UseNpgsql(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }
            foreach (var accountId in accountIds)
            {
                var dbLayer = new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options));
                LogMessageHelper.PushMessage($"Обработка аккаунта {accountId}");
                UpdateAccountResources(accountId, dbLayer);
                LogMessageHelper.PushMessage($"Обработка аккаунта {accountId} успешно завершена");
                accountIdsCount--;
                LogMessageHelper.PushMessage($"Осталось обработать аккаунтов {accountIdsCount}");
            }
        }
        private void ValidateAccountData(AccountConfiguration accountConfiguration)
        {
            if (accountConfiguration == null)
                throw new InvalidOperationException("Не удалось найти конфигурацию аккаунта");
            if (accountConfiguration.IsVip)
                throw new InvalidOperationException("Аккаунт является VIP аккаунтом");
            if (accountConfiguration.Locale.Name != LocaleConst.Russia)
                throw new InvalidOperationException($"Локаль аккаунта {accountConfiguration.Locale.Name} не соответствует ожидаемой {LocaleConst.Russia}");
        }
        private void UpdateAccountResources(Guid accountId, UnitOfWork dbLayer)
        {
            var accountData = dbLayer.GetGenericRepository<AccountConfiguration>()
                .AsQueryable()
                .Include(x => x.Locale)
                .FirstOrDefault(x => x.AccountId == accountId);

            ValidateAccountData(accountData);

            var resourcesToBeUpdatedWeb = dbLayer
                .SelectResourceDataForBillingAccount(accountId)
                .Where(x => x.Resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)
                .Where(x => x.Resource.Cost != NewRate && x.Resource.Cost == CurrentRate)
                .ToList();

            var resourcesToBeUpdatedRdp = dbLayer
            .SelectResourceDataForBillingAccount(accountId)
            .Where(x => x.Resource.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
            .Where(x => x.Resource.Cost != NewRateRdp && x.Resource.Cost == CurrentRateRdp)
            .ToList();

            if (!resourcesToBeUpdatedWeb.Any())
            {
                var accountRate = dbLayer.AccountRateRepository.FirstOrDefault(r => r.AccountId == accountId
                && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb && r.Cost == CurrentRate);

                if (accountRate != null)
                {
                    accountRate.Cost = NewRate;
                    LogMessageHelper.PushMessage($"Изменяем запись в индивидуальный тариф для аккаунта {accountId} со стоимостью {accountRate.Cost}");
                    dbLayer.AccountRateRepository.Update(accountRate);
                    dbLayer.Save();
                }

            }
            if (!resourcesToBeUpdatedRdp.Any()) 
            {                
                    var accountRate = dbLayer.AccountRateRepository.FirstOrDefault(r => r.AccountId == accountId
                && r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser && r.Cost == CurrentRateRdp);

                if (accountRate != null)
                {
                    accountRate.Cost = NewRateRdp;
                    LogMessageHelper.PushMessage($"Изменяем запись в индивидуальный тариф для аккаунта {accountId} со стоимостью {accountRate.Cost}");
                    dbLayer.AccountRateRepository.Update(accountRate);
                    dbLayer.Save();
                }

            }

            if (!resourcesToBeUpdatedWeb.Any() && !resourcesToBeUpdatedRdp.Any())
            {
                LogMessageHelper.PushMessage($"Для аккаунта {accountId} нет изменяемых ресурсов");
                return;
            }
            LogMessageHelper.PushMessage($"Для аккаунта {accountId} выявлено Web {resourcesToBeUpdatedWeb.Count}" +
                $" и Rdp {resourcesToBeUpdatedRdp.Count} ресурсов для изменения");
            var updatesTracker = new AccountUpdatesTracker
            {
                InitialTotalBalance = dbLayer.BillingAccountRepository
                    .WhereLazy(x => x.Id == accountId)
                    .Select(x => new { TotalBalance = x.Balance + x.BonusBalance })
                    .FirstOrDefault()?.TotalBalance ?? 0,
                InitialRegularPayment = dbLayer.GetRegularPaymentForAccounts()
                    .FirstOrDefault(rp => rp.AccountId == accountId)?.RegularPaymentAmount ?? 0
            };

            LogMessageHelper.PushMessage($"Изменение стоимости ресурсов для аккаунта {accountId}");


            foreach (var resourceInfo in resourcesToBeUpdatedWeb)
            {
                UpdateAccountResource(resourceInfo.Resource, resourceInfo.ResourceConfiguration, updatesTracker, ResourceType.MyEntUserWeb,
                    NewRate, CurrentRate, dbLayer);
            }
            foreach (var resourceInfo in resourcesToBeUpdatedRdp)
            {
                UpdateAccountResource(resourceInfo.Resource, resourceInfo.ResourceConfiguration, updatesTracker, ResourceType.MyEntUser,
                    NewRateRdp, CurrentRateRdp, dbLayer);
            }
            
            var resourcesForUpdateCost = updatesTracker.UpdatedResources
                .Select(x => x.UpdatedResource.BillingServiceType.Service)
                .Distinct().ToList();

            sender.Send(new RecalculateResourcesConfigurationCostCommand([accountId],
                resourcesForUpdateCost.Select(x => x.Id).ToList(), Clouds42Service.MyEnterprise)).Wait();

            LogMessageHelper.PushMessage(
                $"Изменение стоимости ресурсов для аккаунта {accountId} успешно завершено. Изменено ресурсов: {updatesTracker.UpdatedResources.Count}");

            HandleAccountCompensation(accountId, updatesTracker, dbLayer);

        }

        private void UpdateAccountResource(Resource resource, ResourcesConfiguration resourceConfiguration, 
            AccountUpdatesTracker updatesTracker, ResourceType resourceType, decimal newRate, decimal currentRate, UnitOfWork dbLayer)
        {
            if (resource.BillingServiceType.SystemServiceType != resourceType)
            {
                LogMessageHelper.PushMessage($"Пропускаю ресурс {resource.Id}. Ресурс не является типа {resourceType}");
                return;
            }

            if (resource.Cost == newRate)
            {
                LogMessageHelper.PushMessage($"Пропускаю ресурс {resource.Id}. Стоимость соответствует ожидаемой {resource.Cost}");
                return;
            }

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var trackerItem = new AccountUpdatesTrackerResourceItem
                {
                    ResourceCostBeforeUpdate = resource.Cost,
                    IsDemoResourceBeforeUpdate = resource.DemoExpirePeriod.HasValue && (resource.DemoExpirePeriod.Value - DateTime.Now).Days > 0,
                    ResourceSubjectBeforeUpdate = resource.Subject,
                    IsServiceCostFixedBeforeUpdate = resourceConfiguration == null || resourceConfiguration.CostIsFixed == true,
                    UpdatedResource = resource,
                };
                resource.Cost = newRate;
                dbLayer.ResourceRepository.Update(resource);
                dbLayer.Save();
                updatesTracker.UpdatedResources.Add(trackerItem);

                var accountRate = dbLayer.AccountRateRepository.FirstOrDefault(r => r.AccountId == resourceConfiguration.AccountId
                    && r.BillingServiceTypeId == resource.BillingServiceTypeId && r.Cost == currentRate);

                if (accountRate != null)
                {
                    accountRate.Cost = newRate;
                    LogMessageHelper.PushMessage($"Изменяем ресурс и в индивидуальном тарифе для аккаунта {resource.BillingAccounts.Account.Id} со стоимостью {accountRate.Cost}");
                    dbLayer.AccountRateRepository.Update(accountRate);
                    dbLayer.Save();
                }

                if (!NoCommitMode)
                {
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
                    
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                LogMessageHelper.PushMessage($"ERROR: При обработке аккаунта {resource.BillingAccounts.Account.Id} произошла ошибка. {ex.Message}");
                throw new InvalidOperationException($"ERROR: При обработке аккаунта {resource.BillingAccounts.Account.Id} произошла ошибка. {ex.Message}");
            }
        }

        private void HandleAccountCompensation(Guid accountId,
            AccountUpdatesTracker updatesTracker, UnitOfWork dbLayer)
        {
            LogMessageHelper.PushMessage($"Компенсация аккаунта {accountId}");
            var rateChange = CurrentRateRdp-NewRateRdp;
            var rateChangeWeb = NewRate  - CurrentRate ;
            if (rateChange <= 0)
            {
               LogMessageHelper.PushMessage(
                   $"Компенсация аккаунта {accountId} пропущена: Отрицательное изменение тарифа {NewRate} - {CurrentRate} = {rateChange}");
                return;
            }

            var prepaidPeriod =
                (int)Math.Floor(updatesTracker.InitialRegularPayment > 0 ?
                updatesTracker.InitialTotalBalance / updatesTracker.InitialRegularPayment :
                decimal.Zero);
            prepaidPeriod = Math.Min(12, prepaidPeriod);

            if (prepaidPeriod <= 0)
            {
               LogMessageHelper.PushMessage($"Компенсация аккаунта {accountId} пропущена: У аккаунта нет периода предоплаты: " +
                   $"Баланс: {updatesTracker.InitialTotalBalance} / Ежемесячный платеж {updatesTracker.InitialRegularPayment} = {prepaidPeriod}");
                return;
            }

            var billableResourcesWeb = updatesTracker.UpdatedResources
                .Where(x => !x.IsDemoResourceBeforeUpdate)
                .Where(x => x.ResourceSubjectBeforeUpdate.HasValue)
                .Where(x => !x.IsDemoServiceBeforeUpdate)
                .Where(x => x.UpdatedResource.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb)
                .Where(x => !x.IsServiceCostFixedBeforeUpdate)
                .ToList();

            var billableResourcesRdp = updatesTracker.UpdatedResources
                .Where(x => !x.IsDemoResourceBeforeUpdate)
                .Where(x => x.ResourceSubjectBeforeUpdate.HasValue)
                .Where(x => !x.IsDemoServiceBeforeUpdate)
                .Where(x => x.UpdatedResource.BillingServiceType.SystemServiceType == ResourceType.MyEntUser)
                .Where(x => !x.IsServiceCostFixedBeforeUpdate)
                .ToList();

            var expectedRegularPaymentChange = (billableResourcesWeb.Count * rateChangeWeb) - (billableResourcesRdp.Count * rateChange);

            if (expectedRegularPaymentChange <= 0)
            {
                LogMessageHelper.PushMessage(
                   $"Компенсация аккаунта {accountId} пропущена: Ожидаемое изменение регулярного платежа {billableResourcesRdp.Count} лиц. * {rateChange} руб. " +
                   $"+ ({billableResourcesWeb.Count} * {rateChangeWeb})= {expectedRegularPaymentChange} руб. меньше или равно 0.");
                return;
            }

            var newRegularPayment = dbLayer
                .GetRegularPaymentForAccounts()
                .FirstOrDefault(x => x.AccountId == accountId)?.RegularPaymentAmount ?? 0;

            var actualRegularPaymentChange =  newRegularPayment - updatesTracker.InitialRegularPayment;

            if (actualRegularPaymentChange != expectedRegularPaymentChange)
                throw new InvalidOperationException(
                    $"Фактическое изменение суммы регулярного платежа " + 
                    $"Новый регулярный платеж {newRegularPayment} - Начальный регулярный платеж {updatesTracker.InitialRegularPayment} = {actualRegularPaymentChange} " +
                    $"отличается от ожидаемого {expectedRegularPaymentChange}.");

            var compensationAmount = prepaidPeriod * expectedRegularPaymentChange;
            var countWeb = billableResourcesWeb.Count - billableResourcesRdp.Count;
            var costStandart = rateChangeWeb - rateChange;
            var payment = new PaymentDefinitionDto
            {
                Id = Guid.NewGuid(),
                Account = accountId,
                Date = DateTime.Now,
                OriginDetails = "core-cp",
                OperationType = PaymentType.Inflow,
                System = PaymentSystem.Admin,
                Status = PaymentStatus.Done,
                Description =
                    $"Компенсация в связи с повышением тарифов. Количество оплачиваемых пользователей Web {countWeb} шт. и Стандарт {billableResourcesRdp.Count}" +
                    $"Период предоплаты {prepaidPeriod} мес. " +
                    $"Сумма компенсации ({countWeb} * {rateChangeWeb} +{billableResourcesRdp.Count} * {costStandart})* {prepaidPeriod} = {compensationAmount} руб.",
                Total = compensationAmount,
                TransactionType = TransactionType.Bonus
            };
            var billingPaymentsProvider = new BillingPaymentsProvider(dbLayer, new BillingPaymentsHelper(dbLayer), logger, handlerException);
            var result = billingPaymentsProvider.CreatePayment(payment);
            if (result != PaymentOperationResult.Ok)
                throw new InvalidOperationException($"Не удалось добавить компенсацию для аккаунта {accountId}. {result}");

            LogEventHelper.LogEvent(dbLayer, accountId, accessProvider, LogActions.ManualBalanceEdit,
                $"Добавление бонусных средств на сумму {payment.Total} {dbLayer.AccountsRepository.GetLocale(payment.Account, Guid.Parse(configuration["DefaultLocale"])).Currency}.");
            dbLayer.Save();

           LogMessageHelper.PushMessage($"Аккаунт {accountId} компенсирован на сумму {compensationAmount}");
        }
    }
}

