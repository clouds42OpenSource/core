﻿using System.Collections.Generic;

namespace PatchIncreaseRdpCostForRuAccounts.Models
{
    /// <summary>
    /// Трекер изменений ресурсов аккаунта
    /// </summary>
    public class AccountUpdatesTracker
    {
        /// <summary>
        /// Сумма баланса на момент начала изменений
        /// </summary>
        public decimal InitialTotalBalance { get; set; }

        /// <summary>
        /// Сумма регулярного платежа на момент начала изменений
        /// </summary>
        public decimal InitialRegularPayment { get; set; }

        /// <summary>
        /// Данные измененний ресурсов
        /// </summary>
        public List<AccountUpdatesTrackerResourceItem> UpdatedResources { get; }
            = [];
    }
}

