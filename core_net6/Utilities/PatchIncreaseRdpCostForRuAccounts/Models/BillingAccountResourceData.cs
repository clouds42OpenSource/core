﻿using CommonLib.Enums;
using System;

namespace PatchIncreaseRdpCostForRuAccounts.Models
{
    /// <summary>
    /// Модель данных ресурсов Аренды 1С для аккаунта биллинга
    /// </summary>
    public class BillingAccountResourceData
    {
        /// <summary>
        /// Id аккаунта биллинга
        /// </summary>
        public Guid BillingAccountId { get; set; }

        /// <summary>
        /// ID ресурса
        /// </summary>
        public Guid ResourceId { get; set; }

        /// <summary>
        /// Тип ресурса
        /// </summary>
        public ResourceType? ResourceType { get; set; }
    }
}
