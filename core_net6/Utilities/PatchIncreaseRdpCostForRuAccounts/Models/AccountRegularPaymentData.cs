﻿using System;

namespace PatchIncreaseRdpCostForRuAccounts.Models
{
    /// <summary>
    /// Данные ежемесаячного платежа аккаунта
    /// </summary>
    public class AccountRegularPaymentData
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Сумма ежемесячного платежа
        /// </summary>
        public decimal RegularPaymentAmount { get; set; }
    }
}
