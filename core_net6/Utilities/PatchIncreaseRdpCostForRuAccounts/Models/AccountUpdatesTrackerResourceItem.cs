﻿using System;
using Clouds42.Domain.DataModels.billing;

namespace PatchIncreaseRdpCostForRuAccounts.Models
{
    /// <summary>
    /// Изменение ресурса
    /// </summary>
    public class AccountUpdatesTrackerResourceItem
    {
        /// <summary>
        /// Сервис в Демо режиме на момент начала изменений
        /// </summary>
        public bool IsDemoServiceBeforeUpdate { get; set; }

        /// <summary>
        /// Ресурс в демо режиме на момент начала изменений
        /// </summary>
        public bool IsDemoResourceBeforeUpdate { get; set; }

        /// <summary>
        /// Сервис помечен флагом фиксированной стоимости на момент начала изменений
        /// </summary>
        public bool IsServiceCostFixedBeforeUpdate { get; set; }

        /// <summary>
        /// Целевой объект ресурса на момент начала изменений
        /// </summary>
        public Guid? ResourceSubjectBeforeUpdate { get; set; }

        /// <summary>
        /// Стоимость ресурса на момент начала изменений
        /// </summary>
        public decimal ResourceCostBeforeUpdate { get; set; }

        /// <summary>
        /// Измененный ресурс
        /// </summary>
        public Resource UpdatedResource { get; set; }
    }
}

