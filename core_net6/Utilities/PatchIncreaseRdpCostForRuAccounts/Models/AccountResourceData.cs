﻿using System;
using Clouds42.Domain.DataModels.billing;

namespace PatchIncreaseRdpCostForRuAccounts.Models
{
    public class AccountResourceData
    {
        /// <summary>
        /// ID аккаунта биллинга
        /// </summary>
        public Guid BillingAccountId { get; set; }

        /// <summary>
        /// Ресурс
        /// </summary>
        public Resource Resource { get; set; }

        /// <summary>
        /// Тариф акканта для ресурса
        /// </summary>
        public AccountRate AccountRate { get; set; } 

        /// <summary>
        /// Конфигурация ресурса
        /// </summary>
        public ResourcesConfiguration ResourceConfiguration { get; set; }
    }    

}
