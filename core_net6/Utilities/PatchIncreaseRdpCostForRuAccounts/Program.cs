﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DependencyRegistration;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PatchIncreaseRdpCostForRuAccounts.Providers;
using Repositories;

namespace PatchIncreaseRdpCostForRuAccounts;

static class Program
{
    private static readonly ServiceCollection _serviceCollection = [];
    private static IUnitOfWork _dbLayer;

    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        //"Запуск патча повышения стоимости Web и пересчета стоимости сервиса 'Аренда 1С Web' у существующих аккаунтов с русской локалью"

        IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddEnvironmentVariables()
            .Build();

        _serviceCollection
            .AddGlobalServices(configuration)
            .AddSerilogWithElastic(configuration)
            //.AddTransient<IAccessProvider,ControlPanelAccessProvider>()
            .AddTransient<IHttpContextAccessor, HttpContextAccessor>()
            .AddTelegramBots(configuration)
            .AddScoped<IUnitOfWork, UnitOfWork>()
            .AddSingleton(configuration)
            .AddTransient<ILogger42, SerilogLogger42>();

        var serviceProvider = _serviceCollection.BuildServiceProvider();
        serviceProvider.GetRequiredService<IHttpContextAccessor>();
        var logger = serviceProvider.GetRequiredService<ILogger42>();
        var handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        var accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();
        var sender = serviceProvider.GetRequiredService <ISender>();
        _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        Logger42.Init(serviceProvider);

        var recalculateRent1CCostForAccountsProvider = new PatchIncreaseRent1CStandardForRuLocale
            (_dbLayer, accessProvider, configuration, logger, handlerException, sender)
            {
                CurrentRate = decimal.Parse(configuration["CurrentRate"]),
                NewRate = decimal.Parse(configuration["NewRate"]),
                NoCommitMode = bool.Parse(configuration["NoCommitMode"]),
                ValidateCurrentRate = bool.Parse(configuration["ValidateCurrentRates"]),
                CurrentRateRdp = decimal.Parse(configuration["CurrentRateRdp"]),
                NewRateRdp = decimal.Parse(configuration["NewRateRdp"])
            };

        var accountIds = 
            bool.Parse(configuration["UseAccountsFilter"]) ?
                AccountsFilterReader.ReadGetAccountIdsFilter("filter_accounts.json") :
                default;

        try
        {
            recalculateRent1CCostForAccountsProvider.Execute(accountIds);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Патч завершился с ошибкой. {ex.Message}");
            Console.ReadLine();
            throw;
        }

        // "Патч завершился."
        Console.ReadLine();
        Console.WriteLine("Обновление стоимости закончено");
        Console.ReadLine();
        Console.WriteLine("Сохранить результат?");
        Console.ReadLine();
        Console.ReadLine();
    }
}

public static class DependencyConfiguration
{
    public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
    {
        var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

        var elasticConfig = new ElasticConfigDto(
            conf["Elastic:Uri"],
            conf["Elastic:UserName"],
            conf["Elastic:Password"],
            conf["Elastic:ApiIndex"],
            conf["Logs:AppName"]
        );

        SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
        return collection;
    }
}
