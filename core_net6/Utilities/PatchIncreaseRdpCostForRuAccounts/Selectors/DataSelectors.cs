﻿using CommonLib.Enums;
using PatchIncreaseRdpCostForRuAccounts.Models;
using System;
using System.Linq;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace PatchIncreaseRdpCostForRuAccounts.Selectors
{
    /// <summary>
    /// Класс для выбора данных пересчета стоимости Аренды 1С Стандарт
    /// </summary>
    public static class DataSelectors
    {

        /// <summary>
        /// Выбрать данные для пересчета Аренды 1С Standard, для аккаунтов локали
        /// </summary>
        /// <param name="localeName">Имя локали по которой нужно выбрать аккаунты</param>
        /// <returns>Данные для пересчета стоимости Аренды 1С Standard для аккаунтов</returns>
        public static IQueryable<BillingAccountResourceData> SelectAccountRent1CStandardResourcesForAccount(this IUnitOfWork dbLayer, string localeName) =>
            from resource in dbLayer.ResourceRepository.WhereLazy()
            join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on resource.BillingServiceTypeId
                equals serviceType.Id
            select new
            {
                accountId = resource.AccountSponsorId ?? resource.AccountId,
                resource,
                serviceType
            }
            into rent1CResourcesInfo
            join accountRate in dbLayer.AccountRateRepository.WhereLazy() on
                rent1CResourcesInfo.serviceType.Id equals accountRate.BillingServiceTypeId into accountRates
            from rate in accountRates.Where(ar => ar.AccountId == rent1CResourcesInfo.accountId).Take(1)
                .DefaultIfEmpty()
            join accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy()
                on rent1CResourcesInfo.accountId equals accountConfiguration.AccountId
            join locale in dbLayer.LocaleRepository.WhereLazy()
                on accountConfiguration.LocaleId equals locale.ID
            where 
                locale.Name == localeName &&
                !accountConfiguration.IsVip && rent1CResourcesInfo.serviceType.Service.SystemService == Clouds42Service.MyEnterprise
            select new BillingAccountResourceData
            {
                BillingAccountId = rent1CResourcesInfo.accountId,
                ResourceId = rent1CResourcesInfo.resource.Id,
                ResourceType = rent1CResourcesInfo.serviceType.SystemServiceType
            };

        /// <summary>
        /// Выбрать тарифы Аренды 1С для локали 
        /// </summary>
        /// <returns>Тарифы Аренды 1С Стандарт</returns>
        public static IQueryable<RateData> SelectRent1CRatesDataForLocale(this IUnitOfWork dbLayer, string localeName, ResourceType resourceType) =>
            (from rate in dbLayer.RateRepository.WhereLazy()
             join locale in dbLayer.LocaleRepository.WhereLazy() on rate.LocaleId equals locale.ID
             join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy() on rate.BillingServiceTypeId
                 equals serviceType.Id
             where locale.Name == localeName && serviceType.SystemServiceType == resourceType
             select new RateData
             {
                 Rate = rate,
                 ResourceType = serviceType.SystemServiceType.Value
             });

        /// <summary>
        /// Выбрать данные ресурсов для аккаунта-плательшика
        /// </summary>
        /// <param name="billingAccountId">ID аккаунта плательщика</param>
        /// <returns>Данные ресурсов для аккаунта-плательщика</returns>
        public static IQueryable<AccountResourceData> SelectResourceDataForBillingAccount(this IUnitOfWork dbLayer, Guid billingAccountId)
        {
            return
                from resource in dbLayer.ResourceRepository.WhereLazy()
                join serviceType in dbLayer.BillingServiceTypeRepository.WhereLazy()
                    on resource.BillingServiceTypeId equals serviceType.Id
                where
                    resource.AccountSponsorId == billingAccountId ||
                    (!resource.AccountSponsorId.HasValue && resource.AccountId == billingAccountId)
                select new
                {
                    sponsorAccountId = resource.AccountSponsorId ?? resource.AccountId,
                    resource
                } into rent1CResourcesInfo
                join accountRateInner in dbLayer.AccountRateRepository.WhereLazy()
                    on rent1CResourcesInfo.resource.BillingServiceTypeId equals accountRateInner.BillingServiceTypeId into accountRates
                from accountRate in accountRates.Where(ar => ar.AccountId == rent1CResourcesInfo.sponsorAccountId).Take(1).DefaultIfEmpty()
                join resourcesConfiguration in dbLayer.ResourceConfigurationRepository.WhereLazy()
                    on new
                    {
                        rent1CResourcesInfo.resource.BillingServiceType.ServiceId,
                        AccountId = rent1CResourcesInfo.sponsorAccountId
                    } equals new
                    {
                        ServiceId = resourcesConfiguration.BillingServiceId, resourcesConfiguration.AccountId
                    } into resourceConfigurations
                from resourceConfiguration in resourceConfigurations.Take(1).DefaultIfEmpty()
                select new AccountResourceData
                {
                    BillingAccountId = rent1CResourcesInfo.sponsorAccountId,
                    Resource = rent1CResourcesInfo.resource,
                    AccountRate = accountRate,
                    ResourceConfiguration = resourceConfiguration
                };
        }

        /// <summary>
        /// Выбрать суммы ежемесячного платежа по аккаунтам
        /// </summary>
        /// <returns>Суммы ежемесячного платежа по аккаунтам</returns>
        public static IQueryable<AccountRegularPaymentData> GetRegularPaymentForAccounts(this IUnitOfWork dbLayer) =>
           from resourceConfiguration in dbLayer.ResourceConfigurationRepository.WhereLazy()
           join account in dbLayer.AccountsRepository.WhereLazy() on resourceConfiguration.AccountId equals
               account.Id
           join service in dbLayer.BillingServiceRepository.WhereLazy() on resourceConfiguration.BillingServiceId
               equals service.Id
           where
               service.SystemService != Clouds42Service.Esdl &&
               service.SystemService != Clouds42Service.Recognition &&
               !resourceConfiguration.IsDemoPeriod

           group resourceConfiguration by resourceConfiguration.AccountId
           into groupedData
           select new AccountRegularPaymentData
           {
               AccountId = groupedData.Key,
               RegularPaymentAmount = groupedData.Sum(rc => rc.Cost)
           };
    }
}
