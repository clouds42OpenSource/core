﻿using System;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;

namespace PatchIncreaseRdpCostForRuAccounts
{
    public static class LogMessageHelper
    {
        private static readonly ILogger42 _logger = new SerilogLogger42();

        /// <summary>
        /// Запушить сообщение
        /// </summary>
        /// <param name="message">Сообщение для пользователя, запустившего патч</param>
        public static void PushMessage(string message)
        {
            _logger.Trace(message);
            Console.WriteLine(message);
        }
    }
}

