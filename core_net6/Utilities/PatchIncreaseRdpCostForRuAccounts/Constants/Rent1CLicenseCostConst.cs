﻿namespace PatchIncreaseRdpCostForRuAccounts.Constants
{
    /// <summary>
    /// Константы стоимости лицензий Аренды 1С
    /// </summary>
    public static class Rent1CLicenseCostConst
    {
        /// <summary>
        /// Ожидаемая текущая стоимость Rdp лицензии
        /// </summary>
        public const decimal CurrentRdpCost = 750;

        /// <summary>
        /// Новая стоимость Rdp лицензии
        /// </summary>
        public const decimal NewRdpCost = 870;
    }

}
