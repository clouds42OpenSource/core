﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using EnvDTE;
using DTEProcess = EnvDTE.Process;
using Process = System.Diagnostics.Process;

namespace VisualStudioAttacherLibrary
{
    #region Classes
 
    public static class VisualStudioAttacher
    {
        #region Public Methods
 
        [DllImport("User32")]
        private static extern int ShowWindow(int hwnd, int nCmdShow);
 
        [DllImport("ole32.dll")]
        private static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);
 
        [DllImport("ole32.dll")]
        private static extern int GetRunningObjectTable(int reserved, out IRunningObjectTable prot);
 
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
 
        public static string GetSolutionForVisualStudio(Process visualStudioProcess)
        {
            if (!TryGetVsInstance(visualStudioProcess.Id, out var visualStudioInstance))
            {
                return null;
            }

            try
            {
                return visualStudioInstance.Solution.FullName;
            }
            catch (Exception)
            {
                //ignored
            }

            return null;
        }
 
        public static Process GetAttachedVisualStudio(Process applicationProcess)
        {
            var visualStudios = GetVisualStudioProcesses();
 
            foreach (var visualStudio in visualStudios)
            {
                if (!TryGetVsInstance(visualStudio.Id, out var visualStudioInstance))
                {
                    continue;
                }

                try
                {
                    foreach (DTEProcess debuggedProcess in visualStudioInstance.Debugger.DebuggedProcesses)
                    {
                        if (debuggedProcess.ProcessID == applicationProcess.Id)
                        {
                            return visualStudio;
                        }
                    }
                }
                catch (Exception)
                {
                    //ignored
                }
            }
            return null;
        }

        public static void AttachVisualStudioToProcess(Process applicationProcess, byte attemptsToAttach = 5)
        {
            Process vsProcess = null;
            for (byte i = 0; i < attemptsToAttach; i++)
            {
                try
                {
                    vsProcess = GetAttachedVisualStudio(Process.GetCurrentProcess());
                    if (vsProcess == null)
                    {
                        continue;
                    }
                    break;
                }
                catch 
                {
                    //ignored
                }                
            }

            if (vsProcess == null)
            {
                return;
            }
            
            for (byte i = 0; i < attemptsToAttach; i++)
            {
                try
                {
                    AttachVisualStudioToProcess(vsProcess, applicationProcess);
                    return;
                }
                catch 
                {
                    //ignored
                }                
            }
        }

        public static void AttachVisualStudioToProcess(Process visualStudioProcess, Process applicationProcess)
        {
            if (!TryGetVsInstance(visualStudioProcess.Id, out var visualStudioInstance))
            {
                return;
            }

            //Find the process you want the VS instance to attach to...
            var processToAttachTo = visualStudioInstance.Debugger.LocalProcesses.Cast<DTEProcess>().FirstOrDefault(process => process.ProcessID == applicationProcess.Id);
 
            //Attach to the process.
            if (processToAttachTo != null)
            {
                processToAttachTo.Attach();
                   
                ShowWindow((int)visualStudioProcess.MainWindowHandle, 3);
                SetForegroundWindow(visualStudioProcess.MainWindowHandle);
            }
            else
            {
                throw new InvalidOperationException("Visual Studio process cannot find specified application '" + applicationProcess.Id + "'");
            }
        }
 
        public static Process GetVisualStudioForSolutions(IEnumerable<string> solutionNames)
        {
            foreach (var solution in solutionNames)
            {
                var visualStudioForSolution = GetVisualStudioForSolution(solution);
                if (visualStudioForSolution != null)
                {
                    return visualStudioForSolution;
                }
            }
            return null;
        }
 
        public static Process GetVisualStudioForSolution(string solutionName)
        {
            var visualStudios = GetVisualStudioProcesses();
 
            foreach (var visualStudio in visualStudios)
            {
                if (!TryGetVsInstance(visualStudio.Id, out var visualStudioInstance))
                {
                    continue;
                }

                try
                {
                    var actualSolutionName = Path.GetFileName(visualStudioInstance.Solution.FullName);
 
                    if (string.Compare(actualSolutionName, solutionName, StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        return visualStudio;
                    }
                }
                catch (Exception)
                {
                    //ignored
                }
            }
            return null;
        }
 
        #endregion
 
        #region Private Methods
 
        private static IEnumerable<Process> GetVisualStudioProcesses()
        {
            var processes = Process.GetProcesses();
            return processes.Where(o => o.ProcessName.Contains("devenv"));
        }
 
        private static bool TryGetVsInstance(int processId, out _DTE instance)
        {
            GetRunningObjectTable(0, out var runningObjectTable);
            runningObjectTable.EnumRunning(out var monikerEnumerator);
            monikerEnumerator.Reset();
 
            var numFetched = IntPtr.Zero;
            var monikers = new IMoniker[1];

            while (monikerEnumerator.Next(1, monikers, numFetched) == 0)
            {
                CreateBindCtx(0, out var ctx);

                monikers[0].GetDisplayName(ctx, null, out var runningObjectName);

                runningObjectTable.GetObject(monikers[0], out var runningObjectVal);

                if (runningObjectVal is not _DTE dteInstance || !runningObjectName.StartsWith("!VisualStudio"))
                {
                    continue;
                }

                var currentProcessId = int.Parse(runningObjectName.Split(':')[1]);

                if (currentProcessId != processId)
                {
                    continue;
                }

                instance = dteInstance;
                return true;
            }
 
            instance = null;
            return false;
        }
 
        #endregion
    }
 
    #endregion
}
