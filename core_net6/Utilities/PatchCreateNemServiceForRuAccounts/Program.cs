﻿using System;
using Microsoft.Extensions.Configuration;
using Repositories;
using CommonLib.Constants;

namespace PatchCreateNemServiceForRuAccounts
{
    static class Program
    {
        static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            //"Запуск патча по добавлению нового сервиса у существующих аккаунтов с русской локалью"

            
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables()
                .Build();

            var uow = new UnitOfWork();
            var patchAddNewServiceRuLocale = new PatchAddNewServiceRuLocale(uow);

            var accountList = uow.AccountsRepository.Where(a => a.AccountConfiguration.Locale.Name == LocaleConst.Russia);
            Console.WriteLine($"Получили список пользователей.");
            try
            {
                patchAddNewServiceRuLocale.Execute(accountList);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Патч завершился с ошибкой. {ex.Message}");
                Console.ReadLine();
                throw;
            }
            Console.WriteLine($"Патч завершен");
            // "Патч завершился."
            Console.ReadLine();
        }
    }
}

