﻿using Clouds42.DomainContext.DataModels;
using Clouds42.DomainContext.DataModels.Billing;
using CommonLib.Enums;
using PatchAddNewServiceRuLocale;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PatchCreateNemServiceForRuAccounts
{
    /// <summary>
    /// Провайдер для добавления нового сервиса
    /// </summary>
    public partial class PatchAddNewServiceRuLocale
    {
        readonly IUnitOfWork _dbLayer;

        public PatchAddNewServiceRuLocale(IUnitOfWork dbLayer)
        {
            _dbLayer = dbLayer;
        }



        /// <summary>
        /// Выполнить патч
        /// </summary>
        /// <param name="accounts">Список пользователей кому добавить новый сервис</param>
        public void Execute(IEnumerable<Account> accounts)
        {
            //CreateNewService();
            AddNewServiceForAccount(accounts);
        }

        private void CreateNewService()
        {
            using (var transaction = _dbLayer.SmartTransaction.Get())
            {
                try
                {
                    var services = new BillingService()
                    {
                        Id = Guid.Parse("F83709DC-60BE-4530-8A80-11EBD5502459"),
                        Name = "Дополнительные сеансы",
                        BillingServiceStatus = BillingServiceStatusEnum.IsActive,
                        Key = Guid.Parse("5E89D0FA-5DAC-4ED3-AA5E-B53B1A12740A"),
                        ShortDescription = "Дополнительные сеансы",
                        Opportunities = "Дополнительные сеансы",
                        IsActive = true
                    };
                    _dbLayer.BillingServiceRepository.Insert(services);
                    _dbLayer.Save();
                    LogMessageHelper.PushMessage($"Добавлен новый сервис с Id {services.Id} и именем {services.Name}");

                    var serviceType = new BillingServiceType()
                    {
                        Id = Guid.Parse("4FF22EFC-4636-458B-8423-077818BD15A8"),
                        ServiceId = services.Id,
                        Name = "Дополнительный сеанс",
                        Description = "Дополнительный сеанс",
                        BillingType = BillingTypeEnum.ForAccount,
                        DependServiceTypeId = Guid.Parse("7EBD508B-D866-425D-B484-D6740FFDB3BF"),
                        Key = Guid.Parse("8F6313E1-F2BD-492F-B437-31DBDCD1FC94"),
                        IsDeleted = false
                    };
                    _dbLayer.BillingServiceTypeRepository.Insert(serviceType);
                    _dbLayer.Save();
                    LogMessageHelper.PushMessage($"Добавлен новый тип сервиса с Id {serviceType.Id} и именем {serviceType.Name}");
                    var rate = new Rate()
                    {
                        Id = Guid.NewGuid(),
                        RatePeriod = "Month",
                        Cost = 950,
                        AccountType = "Standart",
                        LocaleId = Guid.Parse("5E9BAE8F-2282-4026-9172-3CF52E7DF843"),
                        BillingServiceTypeId = serviceType.Id
                    };
                    _dbLayer.RateRepository.Insert(rate);
                    _dbLayer.Save();
                    LogMessageHelper.PushMessage($"Добавлена стоимость сервиса с Id {rate.BillingServiceTypeId} и ценой {rate.Cost}");
                    transaction.Commit();

                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
        private void AddNewServiceForAccount(IEnumerable<Account> accounts)
        {
            var service = _dbLayer.BillingServiceRepository.FirstOrDefault(s => s.Name == "Дополнительные сеансы");
            Console.WriteLine($"Получили добавленный сервис {service}.");
            using (var transaction = _dbLayer.SmartTransaction.Get())
            {
                foreach (var account in accounts)
                {
                    try
                    {

                        var resAccount = _dbLayer.ResourceConfigurationRepository.Where(s => s.AccountId == account.Id && s.BillingServiceId == service.Id).FirstOrDefault();
                        Console.WriteLine($"Получили список ресурсов по аккаунту {resAccount}.");
                        if (resAccount != null)
                            continue;

                        var billing = _dbLayer.BillingAccountRepository.FirstOrDefault(b => b.Id == account.Id);
                        Console.WriteLine($"Получили биллинг аккаунта {billing}.");
                        if (billing == null)
                            continue;

                        var newId = Guid.NewGuid();

                        var generaye = true;

                        while (generaye)
                        {
                            var resId = _dbLayer.ResourceConfigurationRepository.Where(s => s.Id == newId).FirstOrDefault();
                            if (resId == null)
                            {
                                generaye = false;
                                break;
                            }

                            newId = Guid.NewGuid();
                        }
                        LogMessageHelper.PushMessage($"Начало добавления новый сервис {service.Name} аккаунту {account.Id} ");
                        var resource = new ResourcesConfiguration()
                        {
                            Id = Guid.NewGuid(),
                            AccountId = account.Id,
                            Cost = 0,
                            CostIsFixed = false,
                            DiscountGroup = 0,
                            CreateDate = DateTime.Now,
                            BillingServiceId = service.Id,
                            IsDemoPeriod = false
                        };
                        _dbLayer.ResourceConfigurationRepository.Insert(resource);
                        _dbLayer.Save();
                        LogMessageHelper.PushMessage($"Добавлен новый сервис {service.Name} аккаунту {account.Id} ");
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        LogMessageHelper.PushMessage($"При добавлении нового сервиса {service.Name} аккаунту {account.Id} произошла ошибка {ex.InnerException}, {ex.Message}");
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

    }
}

