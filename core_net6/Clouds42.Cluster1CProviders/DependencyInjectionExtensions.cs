﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Cluster1CProviders
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddCluster1CProviders(this IServiceCollection services)
        {
            services.AddTransient<ICluster1CProvider, Cluster1CProvider>();
            return services;
        }
    }
}
