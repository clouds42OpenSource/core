﻿using System;
using System.Runtime.ExceptionServices;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.RasClient.Providers.Interfaces;
using Clouds42.Tools1C.Connectors1C.AgentConnector;
using Clouds42.Tools1C.Helpers;
using V82;

namespace Clouds42.Cluster1CProviders
{

    /// <summary>
    /// Провайдер работы с кластером 1С.
    /// </summary>
    internal class Cluster1CProvider(
        IRasClientProvider rasClientProvider,
        ILogger42 logger)
        : ICluster1CProvider
    {
        private readonly Lazy<string> _lockDatabaseMessage = new(CloudConfigurationProvider.Mcob.GetDatabasesLockMessage);
        private readonly Lazy<string> _lockDatabasePassword = new(CloudConfigurationProvider.Mcob.GetDatabasesLockPassword);

        /// <summary>
        /// Завершить рабочие процессы в базе.
        /// </summary>        
        public void DropSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            try
            {
                if (CloudConfigurationProvider.Enterprise1C.RasClient.NeedUseRas())
                {
                    rasClientProvider.DropAccountDatabaseSessions(accountDatabaseEnterprise);
                    return;
                }

            }
            catch (AccessViolationException ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка отключения активных сессий с кластера {ex.GetFullInfo()}");
                throw;
            }
            catch (Exception ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка отключения активных сессий с кластера {ex.GetFullInfo()}");
                throw;
            }            
        }

        
        /// <summary>
        /// Удалить информационную базу м кластера.
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param> 
        public void DropDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            try
            {
                if (CloudConfigurationProvider.Enterprise1C.RasClient.NeedUseRas() && 
                    !Is82Db(accountDatabaseEnterprise.AccountDatabase))
                {
                    rasClientProvider.DropAccountDatabase(accountDatabaseEnterprise);
                    return;
                }

                DropDatabaseFromCluster(accountDatabaseEnterprise);
            }
            catch (Exception ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка удаления базы с клатсера {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Создать базу на кластере.
        /// </summary>        
        public void CreateDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            try
            {
                if (CloudConfigurationProvider.Enterprise1C.RasClient.NeedUseRas())
                    rasClientProvider.CreateAccountDatabase(accountDatabaseEnterprise);

            }
            catch (Exception ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка создания базы на кластере {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Есть рабочие процессы в базе.
        /// </summary>        
        public bool AnyDbSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            try
            {
                if (CloudConfigurationProvider.Enterprise1C.RasClient.NeedUseRas())
                    return rasClientProvider.AccountDatabaseHasActiveSessions(accountDatabaseEnterprise);

                return Is82Db(accountDatabaseEnterprise.AccountDatabase)
                    ? AnyDb82Sessions(accountDatabaseEnterprise)
                    : AnyDb83Sessions(accountDatabaseEnterprise);
            }
            catch (Exception ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка проверки активных сессий {ex.GetFullInfo()}");
                throw;
            }
        }

        
        
        /// <summary>
        /// Изменить состояние блокировки у информационной базы на кластере.
        /// </summary>        
        public void SetLockStatusDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, bool lockValue)
        {
            try
            {
                if (Is82Db(accountDatabaseEnterprise.AccountDatabase))
                {
                    V82SetDatabaseLock(accountDatabaseEnterprise, lockValue);
                }
                else
                {
                    V83SetDatabaseLock(accountDatabaseEnterprise, lockValue);
                }                
            }
            catch (Exception ex)
            {
                TraceError(accountDatabaseEnterprise, $"Ошибка создания базы на кластере {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Создать коннектор работы с кластером 8.2
        /// </summary>        
        private ClusterConnector1C82 Create82ClusterConnector(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
            => new(new ClusterParametersDto
            {
                ConnectionAddress = accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress,
                AdminName = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                AdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword                
            });

        /// <summary>
        /// Проверить является ли информационная база платформы 8.2
        /// </summary>        
        private bool Is82Db(IAccountDatabase accountDatabase)
            => accountDatabase.ApplicationName.Contains("8.2");

        /// <summary>
        /// Проверить наличие активных сессий у ИБ 8.3
        /// </summary>        
        [HandleProcessCorruptedStateExceptions]
        private bool AnyDb83Sessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            using var comConnector = new Cluster1CConnector(Connector1CParamsCreator.CreateClusterConnectionParameters(accountDatabaseEnterprise));
            comConnector.Connect();

            for (var i = 0; i < comConnector.ClusterResult.Sessions.Length; i++)
            {
                var session = (dynamic)comConnector.ClusterResult.Sessions.GetValue(i);
                var currentIbName = (string)session.infoBase.Name;

                if (currentIbName == accountDatabaseEnterprise.AccountDatabase.V82Name)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Проверить наличие активных сессий у ИБ 8.2
        /// </summary>    
        [HandleProcessCorruptedStateExceptions]
        private bool AnyDb82Sessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var connector = Create82ClusterConnector(accountDatabaseEnterprise);
            connector.Connect();

            for (var i = 0; i < connector.ClusterResult.Sessions.Length; i++)
            {
                var session = (V82.ISessionInfo)connector.ClusterResult.Sessions.GetValue(i);
                var currentIbName = session.infoBase.Name;

                if (currentIbName == accountDatabaseEnterprise.AccountDatabase.V82Name)
                    return true;
            }

            return false;
        }

       

        /// <summary>
        /// Изменить состояние блокировки у информационной базы на кластере 8.2
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база.</param>
        /// <param name="lockValue">Признак блокировки.</param>
        [HandleProcessCorruptedStateExceptions]
        private void V82SetDatabaseLock(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, bool lockValue)
        {
            
            Trace(accountDatabaseEnterprise, "Попытка подключения к кластеру");
            var connector = Create82ClusterConnector(accountDatabaseEnterprise);
            connector.Connect();

            Trace(accountDatabaseEnterprise, "Попытка получения рабочих процессов кластера");
            var workingProcesses = connector.ClusterResult.Agent.GetWorkingProcesses(connector.ClusterResult.Cluster);

            var workingProcess = GetWorkingProcess(workingProcesses, connector);

            // Если процессов не запущенно
            if (workingProcess == null)
            {
                Trace(accountDatabaseEnterprise, "Ни одного рабочего процесса на кластере не обнаруженно.");
                return;
            }

            var infoBases = workingProcess.GetInfoBases();
            var length = infoBases.GetLength(0);

            for (var i = 0; i < length; i++)
            {
                var ibName = ((V82.IInfoBaseInfo)infoBases.GetValue(i)).Name;

                var info = (V82.IInfoBaseInfo)infoBases.GetValue(i);
                if (info.Name == ibName)
                {
                    info.PermissionCode = _lockDatabasePassword.Value;
                    info.SessionsDenied = (sbyte)(lockValue ? 1 : 0);
                    info.DeniedFrom = DateTime.Now;
                    info.DeniedTo = DateTime.Now.AddYears(1);
                    info.DeniedMessage = _lockDatabaseMessage.Value;

                    workingProcess.UpdateInfoBase(info);

                    Trace(accountDatabaseEnterprise, "База заблокированна на кластере");

                    return;
                }
            }

        }

        /// <summary>
        /// Изменить состояние блокировки у информационной базы на кластере 8.3
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база.</param>
        /// <param name="lockValue">Признак блокировки.</param>
        [HandleProcessCorruptedStateExceptions]
        private void V83SetDatabaseLock(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, bool lockValue)
        {

            Trace(accountDatabaseEnterprise, "Попытка подключения к кластеру");

            using var comConnector = new Cluster1CConnector(Connector1CParamsCreator.CreateClusterConnectionParameters(accountDatabaseEnterprise));
            comConnector.Connect();

            Trace(accountDatabaseEnterprise, "Попытка получения рабочих процессов кластера");
            var workingProcesses = comConnector.ClusterResult.Agent.GetWorkingProcesses(comConnector.ClusterResult.Cluster);

            var workingProcess = GetWorkingProcess(workingProcesses, comConnector);

            if (workingProcess == null)
            {
                Trace(accountDatabaseEnterprise, "Не запущенно ни одного рабочего процесса");
                return;
            }

            var infoBases = workingProcess.GetInfoBases();
            var length = infoBases.GetLength(0);

            for (var i = 0; i < length; i++)
            {
                var infoBaseInfo = infoBases.GetValue(i);
                var ibName = (string)infoBaseInfo.Name;

                if (ibName == accountDatabaseEnterprise.AccountDatabase.V82Name)
                {

                    infoBaseInfo.PermissionCode = _lockDatabasePassword.Value;
                    infoBaseInfo.SessionsDenied = (sbyte)(lockValue ? 1 : 0);
                    infoBaseInfo.SessionsDenied = lockValue;
                    infoBaseInfo.DeniedFrom = DateTime.Now;
                    infoBaseInfo.DeniedTo = DateTime.Now.AddYears(1);
                    infoBaseInfo.DeniedMessage = _lockDatabaseMessage.Value;

                    workingProcess.UpdateInfoBase(infoBaseInfo);

                    Trace(accountDatabaseEnterprise, "База разблокированна на кластере");
                    return;
                }
            }
        }

        /// <summary>
        /// Удалить инф. базу с кластера
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база с данными о кластере предприятия аккаунта.</param>
        [HandleProcessCorruptedStateExceptions]
        private void DropDatabaseFromCluster(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            using var comConnector = new Cluster1CConnector(
                Connector1CParamsCreator.CreateClusterConnectionParameters(accountDatabaseEnterprise));
            comConnector.Connect();

            Trace(accountDatabaseEnterprise, "Попытка получения рабочих процессов кластера");
            var workingProcesses =
                comConnector.ClusterResult.Agent.GetWorkingProcesses(comConnector.ClusterResult.Cluster);

            var workingProcess = GetWorkingProcess(workingProcesses, comConnector);

            if (workingProcess == null)
            {
                Trace(accountDatabaseEnterprise, "Ни одного рабочего процесса на кластере не обнаруженно.");
                return;
            }

            var infoBases = workingProcess.GetInfoBases();
            var length = infoBases.GetLength(0);

            for (var i = 0; i < length; i++)
            {
                var ibName = (string) infoBases.GetValue(i).Name;

                if (ibName != accountDatabaseEnterprise.AccountDatabase.V82Name)
                    continue;

                workingProcess.DropInfoBase(infoBases.GetValue(i), 0);
                Trace(accountDatabaseEnterprise, "База успешно удалена с кластера");
                break;
            }
        }

        /// <summary>
        /// Полдучить рабочий процесс кластера 8.2
        /// </summary>
        private IWorkingProcessConnection GetWorkingProcess(Array workingProcesses, ClusterConnector1C82 connector)
        {

            IWorkingProcessConnection workingProcess = null;
            // ищем первый запущенный рабочий процесс
            for (var i = 0; i < workingProcesses.Length; i++)
            {
                var workingProcessInfo = (V82.IWorkingProcessInfo)workingProcesses.GetValue(i);

                //	рабочий процесс не запущен, игнорим его
                if (workingProcessInfo.PID.Length == 0)
                    continue;

                // рабочий процесс запущен
                workingProcess = connector.ClusterResult.ComConnector.ConnectWorkingProcess($"{workingProcessInfo.HostName}:{workingProcessInfo.MainPort}"); workingProcess.AddAuthentication("", " ");
                break;
            }

            return workingProcess;
        }

        /// <summary>
        /// Полдучить рабочий процесс кластера 8.3
        /// </summary>
        private dynamic GetWorkingProcess(dynamic workingProcesses, Cluster1CConnector connector)
        {
            dynamic workingProcess = null;

            for (var i = 0; i < workingProcesses.Length; i++)
            {
                var workingProcessInfo = workingProcesses.GetValue(i);

                if (workingProcessInfo.PID.Length == 0)
                    continue;

                workingProcess =
                    connector.ClusterResult.ComConnector.ConnectWorkingProcess(
                        $"{workingProcessInfo.HostName}:{workingProcessInfo.MainPort}");
                workingProcess.AddAuthentication("", " ");

                break;
            }

            return workingProcess;
        }

        /// <summary>
        /// Записать сообщение в лог
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база с данными о кластере предприятия аккаунта</param>
        /// <param name="message">Сообщение</param>
        private void Trace(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, string message)
        {
            logger.Info(
                $"{accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress} :: {accountDatabaseEnterprise.AccountDatabase.V82Name}:: {message}");
        }

        /// <summary>
        /// Записать ошибку в лог
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база с данными о кластере предприятия аккаунта</param>
        /// <param name="message">Сообщение</param>
        private void TraceError(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, string message)
        {
            logger.Warn(
                $"{accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress} :: {accountDatabaseEnterprise.AccountDatabase.V82Name}:: {message}");
        }
    }
}
