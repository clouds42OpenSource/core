﻿using Clouds42.DataContracts.AccountDatabase.Interface;

namespace Clouds42.Cluster1CProviders
{
    /// <summary>
    /// Провайдер работы с кластером 1С.
    /// </summary>
    public interface ICluster1CProvider
    {
        /// <summary>
        /// Есть рабочие процессы в базе
        /// </summary>
        /// <returns></returns>
        bool AnyDbSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Завершить рабочие процессы в базе.
        /// </summary>        
        void DropSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Удалить информационную базу м кластера.
        /// </summary>        
        void DropDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Создать базу на кластере.
        /// </summary>        
        void CreateDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Изменить состояние блокировки у информационной базы на кластере.
        /// </summary>        
        void SetLockStatusDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, bool lockValue);

    }
}
