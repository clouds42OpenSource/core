﻿using Clouds42.Logger;
using Clouds42.TelegramBot.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;

namespace Clouds42.TelegramBot
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddTelegramBots(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var botConfigs = configuration.GetSection("TelegramBots").Get<List<BotConfiguration>>();

            foreach (var botConfig in botConfigs!)
            {
                serviceCollection.AddSingleton<ITelegramBot42>(sp => new TelegramBot42(new TelegramBotClient(botConfig.ApiKey), sp.GetRequiredService<ILogger42>(), botConfig.ChatId, configuration["Logs:AppName"]!, botConfig.Type));
            }

            return serviceCollection;
        }
    }

    public class BotConfiguration
    {
        public string ApiKey { get; set; }
        public string ChatId { get; set; }
        public BotType Type { get; set; }
    }

    public enum BotType
    {
        Alert = 0,
        Notification = 1
    }
}
