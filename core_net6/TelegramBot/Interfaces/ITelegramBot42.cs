﻿namespace Clouds42.TelegramBot.Interfaces
{
    public interface ITelegramBot42
    {
        public Task SendAlert(string message);
        public Task SendNotification(long chatId, string message);
        public BotType Type { get; set; }
    }
}
