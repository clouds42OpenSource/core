﻿using System.Net;
using Clouds42.Logger;
using Clouds42.TelegramBot.Interfaces;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace Clouds42.TelegramBot
{
    public class TelegramBot42(
        ITelegramBotClient telegramBotClient,
        ILogger42 logger,
        string chatId,
        string appName,
        BotType type)
        : ITelegramBot42
    {
        public BotType Type { get; set; } = type;

        public async Task SendAlert(string message)
        {
            try
            {
                var metaData = $"Node: {Dns.GetHostName()} \nApp Name: {appName} \n";

                await telegramBotClient.SendTextMessageAsync(chatId, metaData +  message);
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"[Ошибка отправки сообщения в телеграм] :: {ex.Message}");
            }
        }

        public async Task SendNotification(long chatId, string message)
        {
            try
            {
                await telegramBotClient.SendTextMessageAsync(chatId, message, parseMode:ParseMode.MarkdownV2);
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"[Ошибка отправки уведомления в Telegram] :: {ex.Message}");
            }
        }
    }
}
