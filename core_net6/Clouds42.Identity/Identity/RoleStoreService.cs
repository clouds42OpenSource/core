﻿using Clouds42.Identity.Contracts;
using Microsoft.AspNetCore.Identity;

namespace Clouds42.Identity.Identity
{
    /// <summary>
    /// Предоставляет API для работы с ролями в identity системе
    /// </summary>
    public class RoleStoreService : IRoleStore<ApplicationRole>
    {
        private readonly List<ApplicationRole> _roles;

        /// <summary>
        /// Инициализация класса <see cref="RoleStoreService"/>
        /// </summary>
        public RoleStoreService()
        {
            _roles = [];
        }

        #region IRoleStore

        /// <summary>
        /// Создаёт новую роль
        /// </summary>
        /// <param name="role">Роль, которую нужно добавить</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> CreateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            _roles.Add(role);

            return Task.FromResult(IdentityResult.Success);
        }

        /// <summary>
        /// Обновляет роль
        /// </summary>
        /// <param name="role">Роль, которую нужно обновить</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> UpdateAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            var match = _roles.FirstOrDefault(r => r.Id == role.Id);
            if (match == null)
            {
                return Task.FromResult(IdentityResult.Failed());
            }

            match.Name = role.Name;
            return Task.FromResult(IdentityResult.Success);

        }

        /// <summary>
        /// Удаляет роль
        /// </summary>
        /// <param name="role">Роль которую нужно удалить</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> DeleteAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            var match = _roles.FirstOrDefault(r => r.Id == role.Id);
            if (match == null)
            {
                return Task.FromResult(IdentityResult.Failed());
            }
            
            _roles.Remove(match);
            return Task.FromResult(IdentityResult.Success);

        }

        /// <summary>
        /// Поиск роли
        /// </summary>
        /// <param name="roleId">Id роли которую нужно найти</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<ApplicationRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            var role = _roles.First(r => r.Id == roleId);

            return Task.FromResult(role);
        }

        /// <summary>
        /// Поиск роли
        /// </summary>
        /// <param name="normalizedRoleName">Нормалезированное наименование роли которую нужно найти</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<ApplicationRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            var role = _roles.First(r =>
                string.Equals(r.NormalizedName, normalizedRoleName, StringComparison.OrdinalIgnoreCase));

            return Task.FromResult(role);
        }

        /// <summary>
        /// Возвращает Id роли
        /// </summary>
        /// <param name="role">Роль из которой получить её ID</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetRoleIdAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.Id);
        }

        /// <summary>
        /// Возвращает наименование роли
        /// </summary>
        /// <param name="role">Роль из которой получить её имя</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.NormalizedName);
        }

        /// <summary>
        /// Возвращает нормалезированное наименование роли
        /// </summary>
        /// <param name="role">Роль из которой получить её нормалезированное наименование</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetNormalizedRoleNameAsync(ApplicationRole role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role.NormalizedName);
        }

        /// <summary>
        /// Устанавливает наименование роли
        /// </summary>
        /// <param name="role">Роль в которую установить новое имя</param>
        /// <param name="roleName">Новое наименование роли</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetRoleNameAsync(ApplicationRole role, string roleName, CancellationToken cancellationToken)
        {
            role.Name = roleName;

            return Task.FromResult(true);
        }

        /// <summary>
        /// Устанавливает нормализованное наименование роли
        /// </summary>
        /// <param name="role">Роль в которую установить новое нормализованное имя</param>
        /// <param name="normalizedName">Новое нормализованное наименование роли</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetNormalizedRoleNameAsync(ApplicationRole role, string normalizedName,
            CancellationToken cancellationToken)
        {
            return Task.FromResult(true);
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Освобождение ресурсов
        /// </summary>
        public void Dispose()
        {
            // Освобождение дополнительных ресурсов не требуется в текущей реализации
        }

        #endregion
    }
}
