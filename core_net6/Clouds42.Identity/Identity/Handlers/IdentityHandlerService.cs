﻿using Clouds42.Identity.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Clouds42.Identity.Identity.Handlers
{
    /// <summary>
    /// Обработчик действий связанный с API identity системы
    /// </summary>
    public class IdentityHandlerService : IIdentityHandler
    {
        /// <summary>
        /// Ошибка при неуспешной аутентификации
        /// </summary>
        private const string AUTH_N_ERROR_MSG =
            "Ошибка аутентификации, неверные учетные данные, проверьте имя пользователя и / или пароль и повторите попытку.";

        /// <summary>
        /// Логгер
        /// </summary>
        private readonly ILogger<SignInManager<ApplicationUser>> _logger;
        
        /// <summary>
        /// Менеджер который предоставляет API для аутентификации
        /// </summary>
        private readonly SignInManager<ApplicationUser> _signInManager;

        /// <summary>
        /// Инициализация класса <see cref="IdentityHandlerService"/>
        /// </summary>
        /// <param name="signInManager">Менеджер который предоставляет API для аутентификации</param>
        /// <param name="logger">Логгер</param>
        public IdentityHandlerService(SignInManager<ApplicationUser> signInManager, ILogger<SignInManager<ApplicationUser>> logger
        )
        {
            _signInManager = signInManager;
            _logger = logger;
        }

        /// <summary>
        /// Производит аутентификацию
        /// </summary>
        /// <param name="username">Логин</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        public async Task<ApplicationUser> AuthenticateAsync(string username, string password)
        {
            var user = await _signInManager.UserManager.FindByNameAsync(username) ?? await _signInManager.UserManager.FindByEmailAsync(username);

            if (user == null)
            {
                _logger.LogWarning($"Аутентификация пользователя({username}) не пройдена.");
                throw new UnauthorizedAccessException(AUTH_N_ERROR_MSG);
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, password, false);

            if (result.Succeeded)
            {
                return user;
            }

            _logger.LogWarning($"Аутентификация пользователя({username}) не пройдена.");
            throw new UnauthorizedAccessException(AUTH_N_ERROR_MSG);
        }
    }
}
