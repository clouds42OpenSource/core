﻿using System.Security.Claims;
using Clouds42.Domain.DataModels;
using Clouds42.Identity.Contracts;
using Clouds42.Jwt.Helpers;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Identity;

namespace Clouds42.Identity.Identity
{
    /// <summary>
    /// API для работы с Identity системой
    /// </summary>
    public sealed class UserStoreService(IUnitOfWork unitOfWork) : IUserEmailStore<ApplicationUser>,
        IUserPasswordStore<ApplicationUser>,
        IUserRoleStore<ApplicationUser>,
        IUserClaimStore<ApplicationUser>
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly IUnitOfWork _unitOfWork = unitOfWork;

        /// <summary>
        /// Переводит <see cref="AccountUser"/> в <see cref="ApplicationUser"/>
        /// </summary>
        /// <param name="user">Объект пользователя, который нужно перевести в <see cref="ApplicationUser"/></param>
        private static ApplicationUser CreateApplicationUserFrom(AccountUser user)
        {
            return user == null
                ? new ApplicationUser()
                : new ApplicationUser
                {
                    Id = user.Id,
                    Email = user.Email,
                    PasswordHash = user.Password,
                    UserName = user.Login,
                    NormalizedUserName = user.Login.ToUpper(),
                    NormalizedEmail = user.Email.ToUpper(),
                    PhoneNumber = user.PhoneNumber,
                    Activated = user.Activated,

                    EmailConfirmed = false,
                    PhoneNumberConfirmed = false,
                    LockoutEnabled = false,
                    TwoFactorEnabled = false,

                    SecurityStamp = null,
                    ConcurrencyStamp = null,
                    LockoutEnd = null,
                    AccessFailedCount = 0
                };
        }

        /// <summary>
        /// Поиск пользователя по UserName
        /// </summary>
        /// <param name="normalizedUserName">Искомое имя пользователя</param>
        private ApplicationUser FindUserByUserName(string normalizedUserName)
        {
            var foundUser = _unitOfWork.AccountUsersRepository.GetAccountUserByLogin(normalizedUserName);
            return CreateApplicationUserFrom(foundUser);
        }

        /// <summary>
        /// Поиск пользователя по email
        /// </summary>
        /// <param name="normalizedEmail">Искомый email</param>
        private ApplicationUser? FindUserByEmail(string normalizedEmail)
        {
            var foundUser = _unitOfWork.AccountUsersRepository.GetAccountUserByEmail(normalizedEmail);
            return CreateApplicationUserFrom(foundUser);
        }

        /// <summary>
        /// Поиск пользователя по ID
        /// </summary>
        /// <param name="id">Искомый ID</param>
        private ApplicationUser? FindUserById(string id)
        {
            if (!Guid.TryParse(id, out var idGuid))
            {
                return null;
            }

            var foundUser = _unitOfWork.AccountUsersRepository.GetById(idGuid);
            return CreateApplicationUserFrom(foundUser);
        }


        #region IUserStore

        /// <summary>
        /// Создаёт нового пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            user.Id = Guid.Empty;
            return Task.FromResult(IdentityResult.Success);
        }

        /// <summary>
        /// Обновляет информацию пользователя
        /// </summary>
        /// <param name="user">Новая информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            if (user.Id.Equals(Guid.Empty))
                return Task.FromResult(IdentityResult.Failed(new IdentityError
                {
                    Code = "UserNotFound",
                    Description = $"User ID:{user.Id} is not found."
                }));

            return Task.FromResult(IdentityResult.Success);
        }

        /// <summary>
        /// Удаляет пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(IdentityResult.Failed());
        }

        /// <summary>
        /// Поиск пользователя по ID
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var user = FindUserById(userId);
            return Task.FromResult(user);
        }

        /// <summary>
        /// Поиск пользователя по UserName
        /// </summary>
        /// <param name="normalizedUserName">UserName</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var user = FindUserByUserName(normalizedUserName);
            return Task.FromResult(user);
        }

        /// <summary>
        /// Получить ID пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id.ToString());
        }

        /// <summary>
        /// Получить UserName
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        /// <summary>
        /// Получить нормалезированный UserName
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        /// <summary>
        /// Установить UserName
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="userName">Новый UserName</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Установить нормализованный UserName
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="normalizedName">Новый нормализованный UserName</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName,
            CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            return Task.CompletedTask;
        }

        #endregion

        #region IUserEmailStore

        /// <summary>
        /// Устанавливает email пользователю
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="email">Новый email</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetEmailAsync(ApplicationUser user, string email, CancellationToken cancellationToken)
        {
            user.Email = email;
            return Task.FromResult(true);
        }

        /// <summary>
        /// Получает email пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        /// <summary>
        /// Получает подтверждён ли email пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        /// <summary>
        /// Устанавливает статус подтверждён ли email пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="confirmed">Статус подтверждения email</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed, CancellationToken cancellationToken)
        {
            user.EmailConfirmed = confirmed;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Ищет пользователя по нормализованному email
        /// </summary>
        /// <param name="normalizedEmail">Нормализованный email</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<ApplicationUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            var user = FindUserByEmail(normalizedEmail);
            return Task.FromResult(user);
        }

        /// <summary>
        /// Получает нормализованный email
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetNormalizedEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedEmail);
        }

        /// <summary>
        /// Устанавливает нормализованный email
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="normalizedEmail">Новый нормализованный email</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetNormalizedEmailAsync(ApplicationUser user, string normalizedEmail,
            CancellationToken cancellationToken)
        {
            user.NormalizedEmail = normalizedEmail;
            return Task.CompletedTask;
        }


        #endregion

        #region IUserPasswordStore

        /// <summary>
        /// Получает хэш пароля пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        /// <summary>
        /// Проверяет есть ли хэш пароля у пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        }


        /// <summary>
        /// Устанавливает хэш пароля пользователю
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="passwordHash">Хэш пароля</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }


        #endregion

        #region IUserRoleStore

        /// <summary>
        /// Добавляет пользователя к роли
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="roleName">роль в которую добавить пользователя</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task AddToRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            return Task.FromException(new NotSupportedException());
        }

        /// <summary>
        /// Удаляет пользователя от роли
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="roleName">роль из которой убрать пользователя</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        /// <summary>
        /// Получает роли пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IList<string>> GetRolesAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            var userRoles = new List<string>();
            return Task.FromResult<IList<string>>(userRoles);
        }

        /// <summary>
        /// Проверяет есть ли пользователь в роли
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="roleName">роль которая проверятся, есть ли там пользователь</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            var userRoles = Array.Empty<string>();
            return Task.FromResult(userRoles.Any(role => role.ToString() == roleName));
        }

        /// <summary>
        /// Получает всех пользователей определённой роли
        /// </summary>
        /// <param name="roleName">роль в которую входят пользователи</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            return Task.FromResult<IList<ApplicationUser>>(null);
        }

        #endregion
        
        #region IUserClaimStore

        /// <summary>
        /// Получает claims пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IList<Claim>> GetClaimsAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            IList<Claim> claims = new List<Claim>
            {
                new(PrincipalUserClaims.UsernameClaim, user.UserName),
                new(PrincipalUserClaims.UserIdClaim, user.Id.ToString("B"))
            };

            return Task.FromResult(claims);
        }

        /// <summary>
        /// Добавляет claims пользователю
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="claims">Новые claims</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task AddClaimsAsync(ApplicationUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Заменяет claim пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="newClaim">Claim, на который заменить</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        /// <param name="claim">Claim, который заменить</param>
        public Task ReplaceClaimAsync(ApplicationUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаляет claims у пользователя
        /// </summary>
        /// <param name="user">Информация о пользователе</param>
        /// <param name="claims">Claims, которые нужно удалить</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task RemoveClaimsAsync(ApplicationUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получает пользователей для определённого claim
        /// </summary>
        /// <param name="claim">Claim, для поиска пользователей</param>
        /// <param name="cancellationToken">Токен для отмены действия</param>
        public Task<IList<ApplicationUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        
        #endregion

        #region IDisposable

        /// <summary>
        /// Освобождение ресурсов
        /// </summary>
        public void Dispose()
        {
            // Освобождение дополнительных ресурсов не требуется в текущей реализации
        }

        #endregion
    }
}
