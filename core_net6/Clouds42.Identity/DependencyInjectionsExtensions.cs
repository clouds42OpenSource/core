﻿using Clouds42.Identity.Contracts;
using Clouds42.Identity.Identity.Handlers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Identity
{
    public static class DependencyInjectionsExtensions
    {
        public static IServiceCollection AddIdentity(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IIdentityHandler, IdentityHandlerService>();

            return serviceCollection;
        }
    }
}
