﻿using Clouds42.AccountUsers.AccountUserSession.Helpers;
using Clouds42.Common.Encrypt;
using Clouds42.Domain.DataModels;
using Clouds42.Identity.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Identity
{
    /// <summary>
    /// Класс для получения хэшей пароля в системе Identity
    /// </summary>
    public class CustomPasswordHasher(IEncryptionProvider encryptionProvider, IConfiguration configuration)
        : IPasswordHasher<ApplicationUser>
    {
        /// <summary>
        /// Создаёт хэш для пароля
        /// </summary>
        /// <param name="user">Для какого пользователя создать хэш</param>
        /// <param name="password">Пароль для которого создаётся хэш</param>
        public string HashPassword(ApplicationUser user, string password)
            => encryptionProvider.Encrypt(password);

        /// <summary>
        /// Валидирует хэши паролей
        /// </summary>
        /// <param name="user">Для какого пользователя валидирует</param>
        /// <param name="hashedPassword">С каким хэшем стравнивать</param>
        /// <param name="providedPassword">Какой пароль захэшировать и сравнить</param>
        public PasswordVerificationResult VerifyHashedPassword(ApplicationUser user, string hashedPassword, string providedPassword)
        {
            var validator = new AccountUserSessionValidator(configuration);
            var isPasswordValid = validator.ValidateUserCredentials(new AccountUser
            {
                Login = user.UserName,
                Password = user.PasswordHash,
                Activated = user.Activated
            }, providedPassword);

            return isPasswordValid
                ? PasswordVerificationResult.Success
                : PasswordVerificationResult.Failed;
        }
    }
}
