﻿using System.Collections.Generic;

namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPagedList<out T> : IPagedList, IEnumerable<T>
    {
        ///<summary>
        /// Получает элемент по указанному индексу.
        ///</summary>
        ///<param name="index">Индекс</param>
        T this[int index] { get; }

        ///<summary>
        /// Количество элементов, содержащихся на этой странице.
        ///</summary>
        int Count { get; }

        ///<summary>
        /// Получает неперечислимую копию этого списка.
        ///</summary>
        PagedListMetaData MetaData { get; }
    }

    /// <summary>
    /// Подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
    public interface IPagedList
    {
        /// <summary>
        /// Общее количество подмножеств в множестве
        /// </summary>
        int PageCount { get; }

        /// <summary>
        /// Количество объектов, содержащихся в множестве.
        /// </summary>
        int TotalItemCount { get; }

        /// <summary>
        /// Индекс этого подмножества в множестве, отсчитываемый от 1, и 0, если надмножество пусто.
        /// </summary>
        int PageNumber { get; }

        /// <summary>
        /// Максимальный размер любого отдельного подмножества
        /// </summary>
        int PageSize { get; }

        /// <summary>
        /// Флаг наличия предыдущей страницы
        /// </summary>
        bool HasPreviousPage { get; }

        /// <summary>
        /// Флаг наличия следующей страницы
        /// </summary>
        bool HasNextPage { get; }

        /// <summary>
        /// Флаг нахождения на 1 странице
        /// </summary>
        bool IsFirstPage { get; }

        /// <summary>
        /// Флаг нахождения на последней странице
        /// </summary>
        bool IsLastPage { get; }

        /// <summary>
        /// Отсчитываемый от единицы индекс первого элемента в подмножестве
        /// </summary>
        int FirstItemOnPage { get; }

        /// <summary>
        /// Отсчитываемый от единицы индекс последнего элемента в подмножестве
        /// </summary>
        int LastItemOnPage { get; }
    }
}
