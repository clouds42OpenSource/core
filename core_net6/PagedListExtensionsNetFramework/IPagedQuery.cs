﻿namespace PagedListExtensionsNetFramework
{
    /// <summary>
    ///     Interface to paged query
    /// </summary>
    public interface IPagedQuery
    {
        /// <summary>
        ///     Page number
        /// </summary>
         int? PageNumber { get; set; }

        /// <summary>
        ///     Page size
        /// </summary>
        int? PageSize { get; set; }
    }
}
