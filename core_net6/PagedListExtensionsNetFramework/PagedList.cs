﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TKey"></typeparam>
	public class PagedList<T, TKey> : BasePagedList<T>
    {
        /// <summary>
        /// Инициализирует новый объект, производный от <see cref = "BasePagedList{T}" /> и устанавливает свойства,
        /// необходимые для расчета данных о положении и размере для подмножества и надмножества.
        /// </summary>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="keySelector">Выражение для сортировки</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="pageSize">Максимальный размер любого отдельного подмножества</param>
        public PagedList(IQueryable<T> superset, Expression<Func<T, TKey>> keySelector, int pageNumber, int pageSize)
            : base(pageNumber, pageSize, superset?.Count() ?? 0)
        {
            if (TotalItemCount > 0)
            {
                InitSubset(superset, keySelector.Compile(), pageNumber, pageSize);
            }
        }

        /// <summary>
        /// Инициализирует новый объект, производный от <see cref = "BasePagedList{T}" /> и устанавливает свойства,
        /// необходимые для расчета данных о положении и размере для подмножества и надмножества.
        /// </summary>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="keySelector">Выражение для сортировки</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="pageSize">Максимальный размер любого отдельного подмножества</param>
        public PagedList(IQueryable<T> superset, Func<T, TKey> keySelector, int pageNumber, int pageSize)
            : base(pageNumber, pageSize, superset?.Count() ?? 0)
        {
            if (TotalItemCount > 0)
            {
                InitSubset(superset, keySelector, pageNumber, pageSize);
            }
        }

        private void InitSubset(IEnumerable<T> superset, Func<T, TKey> keySelectorMethod, int pageNumber, int pageSize)
        {
            var items = pageNumber == 1
                ? superset.OrderBy(keySelectorMethod).Take(pageSize).ToList()
                : superset.OrderBy(keySelectorMethod).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            Subset.AddRange(items);
        }
    }

    /// <summary>
    /// Представляет подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
    /// <typeparam name="T">Тип объектов, которые коллекция должна хранить</typeparam>
    public class PagedList<T> : BasePagedList<T>
    {
        /// <summary>
        /// Инициализирует новый экземпляр <see cref="PagedList{T}"/> который делит множества на подмножества размера  pageSize.
        /// </summary>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества </param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="pageSize">Максимальный размер любого отдельного подмножества</param>
        public PagedList(IQueryable<T> superset, int pageNumber, int pageSize)
            : base(pageNumber, pageSize, superset?.Count() ?? 0)
        {
            if (TotalItemCount > 0)
            {
                Subset.AddRange(pageNumber == 1
                    ? superset.Take(pageSize).ToList()
                    : superset.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList()
                );
            }
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="PagedList{T}"/> который делит множества на подмножества размера  pageSize.
        /// </summary>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества </param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="pageSize">Максимальный размер любого отдельного подмножества</param>
		public PagedList(IEnumerable<T> superset, int pageNumber, int pageSize)
            : this(superset.AsQueryable(), pageNumber, pageSize)
        {
        }

        /// <summary>
        /// Клонирование
        /// </summary>
        /// <param name="pagedList">Исходный PagedList</param>
        /// <param name="superset">Исходная коллекция</param>
        public PagedList(IPagedList pagedList, IEnumerable<T> superset)
        {
            TotalItemCount = pagedList.TotalItemCount;
            PageSize = pagedList.PageSize;
            PageNumber = pagedList.PageNumber;
            PageCount = pagedList.PageCount;
            HasPreviousPage = pagedList.HasPreviousPage;
            HasNextPage = pagedList.HasNextPage;
            IsFirstPage = pagedList.IsFirstPage;
            IsLastPage = pagedList.IsLastPage;
            FirstItemOnPage = pagedList.FirstItemOnPage;
            LastItemOnPage = pagedList.LastItemOnPage;

            Subset.AddRange(superset);
        }
    }
}
