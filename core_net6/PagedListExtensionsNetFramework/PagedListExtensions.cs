﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Clouds42.Common.ManagersResults;

namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Контейнер для методов расширения, предназначенных для упрощения создания экземпляров PagedList
    /// </summary>
    public static class PagedListExtensions
    {
        /// <summary>
        /// Разделяет коллекцию объектов на n страниц
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="numberOfPages">Количество страниц, на которые должна быть разделена коллекция</param>
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> superset, int numberOfPages)
        {
            var enumerable = superset.ToList();
            var take = Convert.ToInt32(Math.Ceiling(enumerable.Count / (double)numberOfPages));

            var result = new List<IEnumerable<T>>();

            for (var i = 0; i < numberOfPages; i++)
            {
                var chunk = enumerable.Skip(i * take).Take(take).ToList();

                if (chunk.Any())
                {
                    result.Add(chunk);
                }
            }

            return result;
        }

        /// <summary>
        /// Разделяет коллекцию объектов на неизвестное количество страниц с n элементами на странице
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> superset, int pageSize)
        {
            // Cache this to avoid evaluating it twice
            var enumerable = superset as T[] ?? superset.ToArray();
            var count = enumerable.Length;

            if (count < pageSize)
            {
                yield return enumerable;
            }
            else
            {
                var numberOfPages = Math.Ceiling(count / (double)pageSize);

                for (var i = 0; i < numberOfPages; i++)
                {
                    yield return enumerable.Skip(pageSize * i).Take(pageSize);
                }
            }
        }

        /// <summary>
        /// Создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу
        /// и которые содержат метаданные о коллекции объектов, из которых было создано подмножество.
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <returns>A subset of this collection of objects that can be individually accessed by index and containing metadata about the collection of objects the subset was created from.</returns>
        /// <seealso cref="PagedList{T}"/>
        public static IPagedList<T> ToPagedList<T>(this IEnumerable<T> superset, int pageNumber, int pageSize)
        {
            return new PagedList<T>(superset, pageNumber, pageSize);
        }

        /// <summary>
        /// Создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        public static IPagedList<T> ToPagedList<T>(this IEnumerable<T> superset)
        {
            var enumerable = superset as T[] ?? superset.ToArray();
            var supersetSize = enumerable.Length;
            var pageSize = supersetSize == 0 ? 1 : supersetSize;
            return new PagedList<T>(enumerable, 1, pageSize);
        }

        /// <summary>
        /// Привести к произвольному типу
        /// </summary>
        /// <param name="source">Источник</param>
        /// <param name="selector">Селектор</param>
        /// <typeparam name="TSource">Входной тип</typeparam>
        /// <typeparam name="TResult">Результирующий тип</typeparam>
        public static IPagedList<TResult> Select<TSource, TResult>(this IPagedList<TSource> source, Func<TSource, TResult> selector)
        {
            var subset = ((IEnumerable<TSource>)source).Select(selector);
            return new PagedList<TResult>(source, subset);
        }

        /// <summary>
        /// Создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <typeparam name="TKey">Тип для сравнения</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="keySelector">Выражение для сортировки</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static IPagedList<T> ToPagedList<T, TKey>(this IQueryable<T> superset, Expression<Func<T, TKey>> keySelector, int pageNumber, int pageSize)
        {
            return new PagedList<T, TKey>(superset, keySelector, pageNumber, pageSize);
        }

        /// <summary>
        /// Создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <typeparam name="TKey">Тип для сравнения</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="keySelector">Выражение для сортировки</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static IPagedList<T> ToPagedList<T, TKey>(this IEnumerable<T> superset, Expression<Func<T, TKey>> keySelector, int pageNumber, int pageSize)
        {
            return new PagedList<T, TKey>(superset.AsQueryable(), keySelector, pageNumber, pageSize);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        private static async Task<List<T>> ToListAsync<T>(this IEnumerable<T> superset)
        {
            return await superset.ToListAsync(CancellationToken.None);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="cancellationToken"></param>
        private static async Task<List<T>> ToListAsync<T>(this IEnumerable<T> superset, CancellationToken cancellationToken)
        {
            return await Task.Run(superset.ToList, cancellationToken);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="cancellationToken"></param>
        private static async Task<List<T>> ToListAsync<T>(this IQueryable<T> superset, CancellationToken cancellationToken)
        {
            return await Task.Run(superset.ToList, cancellationToken);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="cancellationToken"></param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            if (pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException($"pageNumber = {pageNumber}. PageNumber cannot be below 1.");
            }

            if (pageSize < 1)
            {
                throw new ArgumentOutOfRangeException($"pageSize = {pageSize}. PageSize cannot be less than 1.");
            }

            var subset = new List<T>();
            var totalCount = 0;

            if (superset == null)
            {
                return new StaticPagedList<T>(subset, pageNumber, pageSize, totalCount);
            }

            totalCount = superset.Count();
            if (totalCount > 0)
            {
                subset.AddRange(
                    pageNumber == 1
                        ? await superset.Skip(0).Take(pageSize).ToListAsync(cancellationToken)
                        : await superset.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync(cancellationToken)
                );
            }

            return new StaticPagedList<T>(subset, pageNumber, pageSize, totalCount);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, int pageNumber, int pageSize)
        {
            return await superset.ToPagedListAsync(pageNumber, pageSize, CancellationToken.None);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IEnumerable<T> superset, int pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            return await superset.AsQueryable().ToPagedListAsync(pageNumber, pageSize, cancellationToken);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IEnumerable<T> superset, int pageNumber, int pageSize)
        {
            return await superset.AsQueryable().ToPagedListAsync(pageNumber, pageSize, CancellationToken.None);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, int? pageNumber, int pageSize, CancellationToken cancellationToken)
        {
            return await superset.AsQueryable().ToPagedListAsync(pageNumber ?? 1, pageSize, cancellationToken);
        }

        /// <summary>
        /// Асинхронно создает подмножество этой коллекции объектов, к которым можно получить индивидуальный доступ по индексу и
        /// которые содержат метаданные о коллекции объектов, из которых было создано подмножество
        /// </summary>
        /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
        /// <param name="superset">Коллекция объектов, которые нужно разделить на подмножества</param>
        /// <param name="pageSize">Максимальное количество элементов, которые может содержать каждая страница</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, int? pageNumber, int pageSize)
        {
            return await superset.AsQueryable().ToPagedListAsync(pageNumber ?? 1, pageSize, CancellationToken.None);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="list"></param>
        /// <param name="sorting"></param>
        /// <param name="desc"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public static PagedDto<T> GetSortedPagedDto<T, TResult>(this IPagedList<T> list, Func<T, TResult> sorting, bool desc) where T : class
        {
            return new PagedDto<T>(desc ? list.OrderByDescending(sorting).ToPagedList() : list.OrderBy(sorting).ToPagedList(), list.MetaData);
        }

        /// <summary>
        /// To paged dto
        /// </summary>
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static PagedDto<T> ToPagedDto<T>(this IPagedList<T> list) where T : class
        {
            return new PagedDto<T>(list, list.MetaData);
        }

        /// <summary>
        /// To paged dto
        /// </summary>
        /// <param name="list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static PagedDto<T> ToPagedDto<T>(this List<T> list, PagedListMetaData metaData) where T : class
        {
            return new PagedDto<T>(list.ToPagedList(), metaData);
        }

        public static ManagerResult<T> ToOkManagerResult<T>(this T result, int? code = null, string message = null)
        {
            return new ManagerResult<T> { Result = result, State = ManagerResultState.Ok, Code = code, Message = message};
        }
        public static ManagerResult<T> ToPreconditionFailedManagerResult<T>(string message, int? code = null)
        {
            return new ManagerResult<T> { State = ManagerResultState.PreconditionFailed, Message = message, Code = code};
        }

        /// <summary>
        /// To paged dto with extra
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TExtra"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static PagedDto<T, TExtra> ToPagedDto<T, TExtra>(this IPagedList<T> list) where T : class where TExtra : class
        {
            return new PagedDto<T, TExtra>(list, list.MetaData);
        }
    }
}
