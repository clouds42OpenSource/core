﻿namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Неперечислимая копия списка
    /// </summary>
    public class PagedListMetaData : IPagedList
    {
        /// <summary>
        /// Конструктор, позволяющий создавать экземпляры, не передавая отдельный список.
        /// </summary>
        protected PagedListMetaData()
        {
        }

        ///<summary>
        /// Неперечислимая копия списка
        ///</summary>
        ///<param name="pagedList">Коллекция из которой берутся метаданные</param>
        public PagedListMetaData(IPagedList pagedList)
        {
            PageCount = pagedList.PageCount;
            TotalItemCount = pagedList.TotalItemCount;
            PageNumber = pagedList.PageNumber;
            PageSize = pagedList.PageSize;
            HasPreviousPage = pagedList.HasPreviousPage;
            HasNextPage = pagedList.HasNextPage;
            IsFirstPage = pagedList.IsFirstPage;
            IsLastPage = pagedList.IsLastPage;
            FirstItemOnPage = pagedList.FirstItemOnPage;
            LastItemOnPage = pagedList.LastItemOnPage;
        }

        public PagedListMetaData(int pageCount, int totalItemCount, int pageNumber, int pageSize)
        {
            PageCount = pageCount;
            TotalItemCount = totalItemCount;
            PageNumber = pageNumber;
            PageSize = pageSize;
        }



        /// <summary>
        /// Общее количество подмножеств в множестве
        /// </summary>
        public int PageCount { get; protected set; }

        /// <summary>
        /// Количество объектов, содержащихся в множестве.
        /// </summary>
        public int TotalItemCount { get; protected set; }

        /// <summary>
        /// Индекс этого подмножества в множестве, отсчитываемый от 1, и 0, если надмножество пусто.
        /// </summary>
        public int PageNumber { get; protected set; }

        /// <summary>
        /// Максимальный размер любого отдельного подмножества.
        /// </summary>
        public int PageSize { get; protected set; }

        /// <summary>
        /// Флаг наличия предыдущей страницы
        /// </summary>
        public bool HasPreviousPage { get; protected set; }

        /// <summary>
        /// Флаг наличия следующей страницы
        /// </summary>
        public bool HasNextPage { get; protected set; }

        /// <summary>
        /// Флаг нахождения на 1 странице
        /// </summary>
        public bool IsFirstPage { get; protected set; }

        /// <summary>
        /// Флаг нахождения на последней странице
        /// </summary>
        public bool IsLastPage { get; protected set; }

        /// <summary>
        /// Отсчитываемый от единицы индекс первого элемента в подмножестве
        /// </summary>
        public int FirstItemOnPage { get; protected set; }

        /// <summary>
        /// Отсчитываемый от единицы индекс последнего элемента в подмножестве
        /// </summary>
        public int LastItemOnPage { get; protected set; }
    }
}
