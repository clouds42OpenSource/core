﻿using System.Collections.Generic;

namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
    /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
    public class StaticPagedList<T> : BasePagedList<T>
    {
        /// <summary>
        /// Инициализирует новый экземпляр <see cref="StaticPagedList{T}"/> который содержит уже разделенное подмножество и
        /// информацию о размере надмножества и положении подмножества в нем.
        /// </summary>
        /// <param name="subset">Единственное подмножество, которое должна представлять эта коллекция</param>
        /// <param name="metaData">Предоставьте свойство ".MetaData" существующего экземпляра IPagedList, чтобы воссоздать его здесь (например,
        /// при создании нового экземпляра PagedList после использования Automapper для преобразования его содержимого в DTO.)</param>
        public StaticPagedList(IEnumerable<T> subset, IPagedList metaData)
            : this(subset, metaData.PageNumber, metaData.PageSize, metaData.TotalItemCount)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="StaticPagedList{T}"/> который содержит уже разделенное подмножество и
        /// информацию о размере надмножества и положении подмножества в нем.
        /// </summary>
        /// <param name="subset">Единственное подмножество, которое должна представлять эта коллекция</param>
        /// <param name="pageNumber">Отсчитываемый от единицы индекс подмножества объектов, содержащихся в этом экземпляре</param>
        /// <param name="pageSize">Максимальный размер любого отдельного подмножества</param>
        /// <param name="totalItemCount">Размер множества</param>
        public StaticPagedList(IEnumerable<T> subset, int pageNumber, int pageSize, int totalItemCount)
            : base(pageNumber, pageSize, totalItemCount)
        {
            Subset.AddRange(subset);
        }
    }
}
