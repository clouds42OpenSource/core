﻿namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Interface of paged dto
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPagedDto<out T> where T : class
    {
        /// <summary>
        /// Коллекция
        /// </summary>
        IPagedList<T> Records { get; }

        /// <summary>
        /// Неперечислимая копия списка
        /// </summary>
        PagedListMetaData Metadata { get; }
    }

    /// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedDto<T> : IPagedDto<T> where T : class
    {
        /// <inheritdoc/>
        public IPagedList<T> Records { get; }

        /// <inheritdoc/>
        public PagedListMetaData Metadata { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public PagedDto(IPagedList<T> data, PagedListMetaData meta)
        {
            Records = data;
            Metadata = meta;
        }
    }

    /// <summary>
    /// Paged dto with extra proprerty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TExtra"></typeparam>
    public class PagedDto<T, TExtra> : PagedDto<T>
        where T : class
        where TExtra : class
    {
        /// <summary>
        /// </summary>
        /// <param name="data"></param>
        /// <param name="meta"></param>
        public PagedDto(IPagedList<T> data, PagedListMetaData meta) : base(data, meta)
        {
        }

        /// <summary>
        /// Extra data
        /// </summary>
        public TExtra Extradata { get; private set; }

        /// <summary>
        /// Set extra data
        /// </summary>
        /// <param name="extraData"></param>
        public void SetExtra(TExtra extraData)
        {
            Extradata = extraData;
        }
    }
}
