﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PagedListExtensionsNetFramework
{
    /// <summary>
    /// Подмножество коллекции объектов, к которой можно получить индивидуальный доступ по индексу,
    /// и содержащую метаданные о надмножестве коллекции объектов, из которой было создано это подмножество.
    /// </summary>
    /// <typeparam name="T">Тип объектов, который должна содержать коллекция</typeparam>
    public abstract class BasePagedList<T> : PagedListMetaData, IPagedList<T>
    {
        /// <summary>
        /// Подмножество
        /// </summary>
        protected readonly List<T> Subset = [];

        /// <summary>
        /// конструктор
        /// </summary>
        protected internal BasePagedList()
        {
        }

        /// <summary>
        /// Инициализирует новый объект, производный от <see cref = "BasePagedList{T}" /> и устанавливает свойства,
        /// необходимые для расчета данных о положении и размере для подмножества и надмножества.
        /// </summary>
        /// <param name = "pageNumber">Отсчитываемый от единицы индекс подмножества объектов</param>
        /// <param name = "pageSize">Максимальный размер любого отдельного подмножества</param>
        /// <param name = "totalItemCount">Размер множества</param>
        protected internal BasePagedList(int pageNumber, int pageSize, int totalItemCount)
        {
            if (pageNumber < 1)
            {
                throw new ArgumentOutOfRangeException($"pageNumber = {pageNumber}. PageNumber cannot be below 1.");
            }

            if (pageSize < 1)
            {
                throw new ArgumentOutOfRangeException($"pageSize = {pageSize}. PageSize cannot be less than 1.");
            }

            if (totalItemCount < 0)
            {
                throw new ArgumentOutOfRangeException($"totalItemCount = {totalItemCount}. TotalItemCount cannot be less than 0.");
            }

            TotalItemCount = totalItemCount;

            PageSize = pageSize;
            PageNumber = pageNumber;
            PageCount = TotalItemCount > 0
                            ? (int)Math.Ceiling(TotalItemCount / (double)PageSize)
                            : 0;

            var pageNumberIsGood = PageCount > 0 && PageNumber <= PageCount;

            HasPreviousPage = pageNumberIsGood && PageNumber > 1;
            HasNextPage = pageNumberIsGood && PageNumber < PageCount;
            IsFirstPage = pageNumberIsGood && PageNumber == 1;
            IsLastPage = pageNumberIsGood && PageNumber == PageCount;

            var numberOfFirstItemOnPage = (PageNumber - 1) * PageSize + 1;

            FirstItemOnPage = pageNumberIsGood ? numberOfFirstItemOnPage : 0;

            var numberOfLastItemOnPage = numberOfFirstItemOnPage + PageSize - 1;

            LastItemOnPage = pageNumberIsGood
                                ? numberOfLastItemOnPage > TotalItemCount
                                   ? TotalItemCount
                                   : numberOfLastItemOnPage
                                : 0;
        }

        /// <summary>
        /// Возвращает перечислитель, который выполняет итерацию по BasePagedList.
        /// </summary>
        public IEnumerator<T> GetEnumerator()
        {
            return Subset.GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель, который выполняет итерацию по BasePagedList.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        ///<summary>
        /// Получает элемент по указанному индексу.
        ///</summary>
        ///<param name="index">Индекс</param>
        public T this[int index] => Subset[index];

        /// <summary>
        /// Получает количество элементов, содержащихся в подмножестве
        /// </summary>
        public virtual int Count => Subset.Count;

        ///<summary>
        /// Получение неперечислимой копии списка
        ///</summary>
        public PagedListMetaData MetaData => new PagedListMetaData(this);
    }
}
