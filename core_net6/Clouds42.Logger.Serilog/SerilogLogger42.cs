﻿using Serilog;

namespace Clouds42.Logger.Serilog
{
    public class SerilogLogger42 : ILogger42
    {
        public void Debug(string message) => Log.Logger.Debug(message);

        /// <inheritdoc/>
        public void Debug(string message, params object[] args) => Log.Logger.Debug($"{message}", args);

        /// <inheritdoc/>
        public void Error(string message) => Log.Logger.Error(message);

        /// <inheritdoc/>
        public void Error(string message, params object[] args) => Log.Logger.Error($"{message}", args);

        /// <inheritdoc/>
        public void Error(Exception exception, string message) => Log.Logger.Error(exception, message);

        /// <inheritdoc/>
        public void Info(string message) => Log.Logger.Information(message);

        /// <inheritdoc/>
        public void Info(string message, params object[] args) => Log.Logger.Information($"{message}", args);
        
        /// <inheritdoc/>
        public void Trace(string message) => Log.Logger.Verbose(message);

        /// <inheritdoc/>
        public void Trace(string message, params object[] args) => Log.Logger.Verbose($"{message}", args);

        /// <inheritdoc/>
        public void Warn(string message) => Log.Logger.Warning(message);

        /// <inheritdoc/>
        public void Warn(string message, params object[] args) => Log.Logger.Warning($"{message}", args);

        public void Warn(Exception ex, string message) => Log.Logger.Warning(ex, message);
    }
}
