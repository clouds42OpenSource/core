﻿using System.Net;
using Clouds42.Logger.Serilog.Models;
using Elastic.CommonSchema.Serilog;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;

namespace Clouds42.Logger.Serilog
{
    /// <summary>
    /// Класс для конфигурации NLog
    /// </summary>
    public static class SerilogConfig
    {
        /// <summary>
        /// Конфигурирование логгера
        /// </summary>
        /// <param name="localLogsDto"></param>
        /// <param name="elasticDto"></param>
        public static void ConfigureSerilog(LocalLogsConfigDto localLogsDto , ElasticConfigDto? elasticDto = null)
        {
            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override(nameof(Microsoft), LogEventLevel.Information)
                .Enrich.WithThreadId()
                .WriteTo.File(
                    Path.Combine(AppContext.BaseDirectory, localLogsDto.FileName),
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] [{ThreadId}] {Message:lj}{NewLine}{Exception}", 
                    rollingInterval: RollingInterval.Day);

            if (elasticDto != null && !string.IsNullOrEmpty(elasticDto.Uri)) // переделать под раздельную инициализацию слокальных логов и серилога
            {
                loggerConfiguration
                
                .WriteTo.Logger(elasticLogger => elasticLogger
                        .Enrich.WithProperty("app_name", $"{elasticDto.AppName}")
                        .Enrich.WithProperty("host_name", $"{Dns.GetHostName()}")
                        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticDto.Uri))
                        {
                            AutoRegisterTemplate = false,
                            TypeName = null,
                            BatchAction = ElasticOpType.Create,
                            CustomFormatter = new EcsTextFormatter(),
                            IndexFormat = elasticDto.Index,
                            ModifyConnectionSettings = (connectionConfig) =>
                            connectionConfig.BasicAuthentication(elasticDto.Username, elasticDto.Password)
                        }),
                        restrictedToMinimumLevel: LogEventLevel.Debug);
            }

            Log.Logger = loggerConfiguration.CreateLogger();
        }
    }
}
