﻿using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Logger.Serilog
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddSerilog(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ILogger42, SerilogLogger42>();

            return serviceCollection;
        }
    }
}
