﻿namespace Clouds42.Logger.Serilog.Models
{
    /// <summary>
    /// Модель конфига Elastic
    /// </summary>
    public class ElasticConfigDto(string uri, string username, string password, string index, string appName)
    {
        /// <summary>
        /// Эндпоинт ElasticSearch
        /// </summary>
        public string Uri { get; set; } = uri;

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Username { get; set; } = username;

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; } = password;

        /// <summary>
        /// Индекс в Elastic
        /// </summary>
        public string Index { get; set; } = index;

        /// <summary>
        /// Шаблон построения логов
        /// </summary>
        public string AppName { get; set; } = appName;
    }
}
