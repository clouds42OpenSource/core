﻿
namespace Clouds42.Logger.Serilog.Models
{
    /// <summary>
    /// Конфигурация локальных логов
    /// </summary>
    public class LocalLogsConfigDto(string fileName)
    {
        /// <summary>
        /// Название файла логов
        /// </summary>
        public string FileName { get; set; } = fileName;
    }
}
