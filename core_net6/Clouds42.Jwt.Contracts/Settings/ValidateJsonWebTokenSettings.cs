﻿namespace Clouds42.Jwt.Contracts.Settings
{
    /// <summary>
    /// Настройки для валидации JsonWebToken
    /// </summary>
    public sealed class ValidateJsonWebTokenSettings
    {
        /// <summary>
        /// Издатель
        /// </summary>
        public string Issuer { get; set; }
        
        /// <summary>
        /// Аудитория
        /// </summary>
        public string[] Audiences { get; set; }
        
        /// <summary>
        /// Секретный ключ
        /// </summary>
        public string SigningKeyBase64 { get; set; }

        /// <summary>
        /// Тип JWT аутентификации
        /// </summary>
        public string AuthenticationType { get; set; }
    }
}
