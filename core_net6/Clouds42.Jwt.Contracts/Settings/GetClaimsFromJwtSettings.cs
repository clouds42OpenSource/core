﻿namespace Clouds42.Jwt.Contracts.Settings
{
    /// <summary>
    /// Настройки для получения claims из JsonWebToken
    /// </summary>
    public class GetClaimsFromJwtSettings
    {
        /// <summary>
        /// Секретный ключ
        /// </summary>
        public string SigningKeyBase64 { get; set; }
    }
}
