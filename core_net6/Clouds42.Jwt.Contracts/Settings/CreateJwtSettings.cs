﻿namespace Clouds42.Jwt.Contracts.Settings
{
    /// <summary>
    /// Настройки для создания JsonWebToken
    /// </summary>
    public sealed class CreateJwtSettings
    {
        /// <summary>
        /// Издатель
        /// </summary>
        public string Issuer { get; set; }
        
        /// <summary>
        /// Аудитория
        /// </summary>
        public string Audience { get; set; }
        
        /// <summary>
        /// Время жизни токена
        /// </summary>
        public TimeSpan TimeToLive { get; set; }
    }
}
