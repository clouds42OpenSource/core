﻿using Clouds42.Identity.Contracts;
using Clouds42.Jwt.Contracts.Simple;

namespace Clouds42.Jwt.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public interface IJwtProvider
    {
        /// <summary>
        /// Метод для создания Json Web Token
        /// </summary>
        /// <param name="password"></param>
        /// <param name="audience">Строковое значение аудитории</param>
        /// <param name="user">Для кого создать Json Web Token</param>
        /// <param name="login"></param>
        /// <returns>Возвращает JsonWebToken и RefreshToken</returns>
        Task<JwtInfo> CreateJsonWebTokenFor(string login, string password, string audience, ApplicationUser user);

        /// <summary>
        /// Метод для создания нового токена JsonWebToken по токену RefreshToken
        /// </summary>
        /// <param name="jsonWebToken">Токен который соответствует RefreshToken</param>
        /// <param name="refreshToken">Токен с помощью которого обновить</param>
        /// <returns>Возвращает JsonWebToken и RefreshToken</returns>
        Task<JwtInfo?> RefreshJsonWebToken(string jsonWebToken, string refreshToken);

        /// <summary>
        /// Проверить валидность текущего токена
        /// </summary>
        /// <param name="jsonWebToken">Токен который соответствует RefreshToken</param>
        /// <param name="refreshToken">Токен с помощью которого обновить</param>
        /// <returns>true - если токен есть и не просрочен</returns>
        Task<bool> CheckCurrentTokenValidity(string jsonWebToken, string refreshToken);
    }
}
