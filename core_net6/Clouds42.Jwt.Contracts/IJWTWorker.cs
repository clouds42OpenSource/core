﻿using System.Security.Claims;
using Clouds42.Jwt.Contracts.Settings;
using Clouds42.Jwt.Contracts.Simple;

namespace Clouds42.Jwt.Contracts
{
    /// <summary>
    /// Объект для работы с JWT
    /// </summary>
    public interface IJwtWorker
    {
        /// <summary>
        /// Метод для создания Json Web Token
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="claims">Claims которые нужно добавить в Json Web Token</param>
        /// <returns>Возвращает Json Web Token и Refresh Token</returns>
        Task<JwtInfo> CreateJsonWebTokenAsync(IEnumerable<Claim> claims, CreateJwtSettings settings);
        
        /// <summary>
        /// Генерирует refresh токен
        /// </summary>
        byte[] GenerateRefreshToken();

        /// <summary>
        /// Получает claims из Json Web Token
        /// </summary>
        /// <param name="token">Json Web Token из которого получить claims</param>
        Task<ClaimsPrincipal> GetPrincipalFromExpiredJsonWebTokenAsync(string token);

        ClaimsPrincipal GetPrincipalFromExpiredJsonWebTokenObsolete(string token, GetClaimsFromJwtSettings settings);

        /// <summary>
        /// Проверяет на валидность Json Web Token и возвращает claims
        /// </summary>
        /// <param name="jsonWebToken">Json Web Token который проверить на валидность и получить claims</param>
        Task<ClaimsPrincipal> ValidateJsonWebTokenAsync(string jsonWebToken);
    }
}
