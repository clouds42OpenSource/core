﻿namespace Clouds42.Jwt.Contracts.Simple
{
    /// <summary>
    /// Содржит информацию о Json Web Token
    /// </summary>
    public class JwtInfo
    {
        /// <summary>
        /// Json Web Token
        /// </summary>
        public string JsonWebToken { get; set; }
        
        /// <summary>
        /// Refresh токен
        /// </summary>
        public byte[] RefreshToken { get; set; }

        /// <summary>
        /// Период валидности токена
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}
