﻿using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace Clouds42.Jwt.Contracts;

public interface IJwtHelper
{
    Task<TokenValidationParameters> GeTokenValidationParameters(bool validateLifeTime = true);
    Task<RSACryptoServiceProvider> GetRsaCryptoProviderWithSettings();
}
