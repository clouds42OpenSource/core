﻿using WebhookApi.Models;

namespace WebhookApi.Worker
{
    public interface IWebhookWorker
    {
        Task HandleUpdateMessage(TalkMeWebHookRequest request);
    }
}
