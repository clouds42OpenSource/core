﻿using Clouds42.Domain.DataModels.Notification;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.TalkMeContext;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using WebhookApi.Models;

namespace WebhookApi.Worker
{
    public class WebhookWorker(
        ITelegramBotClient client,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        ISender sender)
        : IWebhookWorker
    {
        public async Task HandleUpdateMessage(TalkMeWebHookRequest request)
        {
            try
            {
                var type = request?.Data?.Client.Source?.Type.Id;

                switch (type)
                {
                    case Types.Telegram:
                        await HandleTelegramMessageAsync(unitOfWork, request);
                        break;
                    case Types.WhatsApp:
                        await HandleWhatsAppMessageAsync(unitOfWork, request);
                        break;
                }
              
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Ошибка обработки сообщений");
            }
        }

        public async Task HandleTelegramMessageAsync(IUnitOfWork unitOfWork, TalkMeWebHookRequest request)
        {
            var chatId = request?.Data?.Client.Source?.Data?.Id;
            var context = request?.Data?.Client?.Source?.Context;

            var accountUserNotificationSettings = await unitOfWork
                .AccountUserNotificationSettingsRepository
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Code == context && x.NotificationSettings.Type == NotificationType.Telegram);

            if (accountUserNotificationSettings is not { ChatId: null })
            {
                return;
            }

            accountUserNotificationSettings.ChatId = chatId;
            unitOfWork.AccountUserNotificationSettingsRepository.Update(accountUserNotificationSettings);
            await unitOfWork.SaveAsync();

            await client.SendTextMessageAsync(chatId, "Поздравляем \U0001F389 Вы успешно активировали уведомления! \U00002705");
        }

        public async Task HandleWhatsAppMessageAsync(IUnitOfWork unitOfWork, TalkMeWebHookRequest request)
        {
            var phone = request?.Data?.Client?.Phone;
            var message = request?.Data?.Message?.Text;
            var messageTextWithOutEscape = message?.TrimStart('\\');
            var accountUserNotificationSettings = await unitOfWork
                .AccountUserNotificationSettingsRepository
                .AsQueryable()
                .Include(x => x.AccountUser)
                .FirstOrDefaultAsync(x => (x.Code == message || x.Code == messageTextWithOutEscape) && 
                                          x.AccountUser.PhoneNumber == phone && 
                                          x.NotificationSettings.Type == NotificationType.WhatsApp);

            if (accountUserNotificationSettings == null)
            {
                return;
            }

            accountUserNotificationSettings.IsActive = true;
            unitOfWork.AccountUserNotificationSettingsRepository.Update(accountUserNotificationSettings);
            await unitOfWork.SaveAsync();

            await sender.Send(new SendWhatsAppNotificationsCommand
            {
                Notifications =
                [
                    new()
                    {
                        Message = "Поздравляем \U0001F389 Вы успешно активировали уведомления! \U00002705",
                        Phone = accountUserNotificationSettings.AccountUser.PhoneNumber
                    }
                ]
            });
        }
    }
}
