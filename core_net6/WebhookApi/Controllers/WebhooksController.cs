﻿using Microsoft.AspNetCore.Mvc;
using WebhookApi.Models;
using WebhookApi.Worker;

namespace WebhookApi.Controllers
{
    [Route("webhooks")]
    public class WebhooksController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Notifications webhook is running");
        }

        [HttpPost]
        [Route("talk-me")]
        public async Task<IActionResult> Update([FromServices] IWebhookWorker worker, [FromBody] TalkMeWebHookRequest update)
        {
            await worker.HandleUpdateMessage(update);

            return Ok();
        }
    }
}
