﻿using Clouds42.Configurations;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Core42.Application;
using GCP.Extensions.Configuration.SecretManager;
using MediatR;
using Newtonsoft.Json.Serialization;
using Repositories;
using Telegram.Bot;
using WebhookApi.Options;
using WebhookApi.Worker;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddGcpKeyValueSecrets()
    .AddJsonFile("appsettings.json", false, true)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", false, true);

ConfigurationHelper.SetConfiguration(builder.Configuration);
SerilogConfig.ConfigureSerilog(new LocalLogsConfigDto(builder.Configuration["Logs:FileName"]!));

builder.Host.UseDefaultServiceProvider(options => options.ValidateOnBuild = false);
builder.Services.AddSerilog();
builder.Services.AddClouds42DbContext(builder.Configuration);
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();
builder.Services.AddCore42Application();
builder.Services.AddHttpClient();
builder.Services.AddSingleton<IWebhookWorker>(sp =>
{
    var telegramBots = builder.Configuration.GetSection("TelegramBots").Get<List<BotConfiguration>>();
    var notificationBot = telegramBots!.FirstOrDefault(x => x.Type == BotType.Notification);

    return new WebhookWorker(new TelegramBotClient(notificationBot!.ApiKey), sp.GetRequiredService<IUnitOfWork>(), sp.GetRequiredService<ILogger42>(), sp.GetRequiredService<ISender>());
});

builder.Services.AddCors(x =>
{
    x.AddDefaultPolicy(z =>
    {
        var policy = builder.Configuration.GetSection("CorsPolicy").Get<CorsPolicySettings>();

        z.WithMethods(policy!.AllowMethods)
            .WithOrigins(policy!.AllowOrigins);
    });
});

builder.Services
    .AddControllers()
    .AddNewtonsoftJson(options =>
    {
        options.UseMemberCasing();
        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
    });

builder.Services.AddEndpointsApiExplorer();

var app = builder.Build();
app.UseCors();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});


using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<Clouds42DbContext>();
    dbContext.Database.Migrate();
}

app.Run();
