﻿namespace WebhookApi.Options
{
    public class CorsPolicySettings
    {
        public string[] AllowMethods { get; set; }
        public string[] AllowOrigins { get; set; }
    }
}
