﻿namespace WebhookApi.Models
{
    public class TalkMeWebHookRequest
    {
        public Data Data { get; set; }
    }
    public class Client
    {
        public string Phone { get; set; }
        public Source Source { get; set; }
    }

    public class Data
    {
        public Client Client { get; set; }
        public Message Message { get; set; }
    }

    public class Source
    {
        public string Id { get; set; }
        public string Context { get; set; }
        public SourceData Data { get; set; }
        public Type Type { get; set; }
    }

    public class Type
    {
        public string Id { get; set; }
    }

    public class Message
    {
        public string Text { get; set; }
    }

    public class SourceData
    {
        public long Id { get; set; }
    }

    public static class Types
    {
        public const string WhatsApp = "whatsappOfficial";
        public const string Telegram = "telegram";
    }
}
