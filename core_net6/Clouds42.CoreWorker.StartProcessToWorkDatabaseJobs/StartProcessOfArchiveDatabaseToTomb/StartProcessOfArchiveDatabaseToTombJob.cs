﻿using Clouds42.AccountDatabase.Contracts.Processors.Managers;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorker;
using System;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfArchiveDatabaseToTomb
{
    /// <summary>
    /// Задача для запуска процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.StartProcessOfArchiveDatabaseToTombJob)]
    public class StartProcessOfArchiveDatabaseToTombJob(
        IUnitOfWork dbLayer,
        IExecuteProcessForWorkingWithDatabaseManager executeProcessForWorkingWithDatabaseManager)
        : CoreWorkerParamsJobWithRetry<ProcessOfArchiveDatabaseToTombParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(
            ProcessOfArchiveDatabaseToTombParamsDto jobParams, Guid taskId,
            Guid taskQueueId)
        {
            var managerResult =
                executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfArchiveDatabaseToTomb(jobParams);

            return CreateRetryParams(taskQueueId, managerResult.Error);
        }
    }
}