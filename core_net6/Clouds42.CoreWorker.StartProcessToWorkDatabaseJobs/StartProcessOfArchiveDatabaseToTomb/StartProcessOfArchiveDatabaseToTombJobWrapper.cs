﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfArchiveDatabaseToTomb
{
    /// <summary>
    /// Обработчик задачи по запуску процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    public class StartProcessOfArchiveDatabaseToTombJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : TypingParamsJobWrapperBase<
            StartProcessOfArchiveDatabaseToTombJob, ProcessOfArchiveDatabaseToTombParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override
            TypingParamsJobWrapperBase<StartProcessOfArchiveDatabaseToTombJob, ProcessOfArchiveDatabaseToTombParamsDto>
            Start(ProcessOfArchiveDatabaseToTombParamsDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));
            return this;
        }

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(ProcessOfArchiveDatabaseToTombParamsDto paramsJob) =>
            $"Запуск процесса на архивацию базы '{paramsJob.DatabaseId}' в склеп.";
    }
}
