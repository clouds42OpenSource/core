﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfRemoveDatabase
{
    /// <summary>
    /// Обработчик задачи по запуску процесса на удаление инф. базы
    /// </summary>
    public class StartProcessOfRemoveDatabaseJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : TypingParamsJobWrapperBase<
            StartProcessOfRemoveDatabaseJob, ProcessOfRemoveDatabaseParamsDto>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override TypingParamsJobWrapperBase<StartProcessOfRemoveDatabaseJob, ProcessOfRemoveDatabaseParamsDto>
            Start(ProcessOfRemoveDatabaseParamsDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));
            return this;
        }

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(ProcessOfRemoveDatabaseParamsDto paramsJob) =>
            $"Запуск процесса на удаление базы '{paramsJob.DatabaseId}'";
    }
}
