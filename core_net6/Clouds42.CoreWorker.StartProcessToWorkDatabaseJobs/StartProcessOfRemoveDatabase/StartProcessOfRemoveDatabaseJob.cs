﻿using System;
using Clouds42.AccountDatabase.Contracts.Processors.Managers;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfRemoveDatabase
{
    /// <summary>
    /// Задача для запуска процесса на удаление инф. базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.StartProcessOfRemoveDatabaseJob)]
    public class StartProcessOfRemoveDatabaseJob(
        IUnitOfWork dbLayer,
        IExecuteProcessForWorkingWithDatabaseManager executeProcessForWorkingWithDatabaseManager)
        : CoreWorkerParamsJobWithRetry<ProcessOfRemoveDatabaseParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(ProcessOfRemoveDatabaseParamsDto jobParams,
            Guid taskId,
            Guid taskQueueId)
        {
            var managerResult = executeProcessForWorkingWithDatabaseManager.ExecuteProcessOfRemoveDatabase(jobParams);
            return CreateRetryParams(taskQueueId, managerResult.Error);
        }
    }
}