﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.DependencyRegistration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;

namespace PatcheUpdateAccessToAccountDatabase;
//вытащить из базы все базы со State = ready и PublishState = published
//достать пулы и переписать веб конфиг


internal class Program
{
    private static readonly ServiceCollection _serviceCollection = [];
    private static IUnitOfWork _dbLayer;
    private static ISegmentHelper segmentHelper;
    private static IisApplicationDataHelper iisApplicationDataHelper;
    private static IDatabasePlatformVersionHelper databasePlatformVersionHelper;
    private static IWebAccessHelper webAccessHelper;
    private static ILogger42 logger;

    private static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        Console.WriteLine("Введите гуид базы");

        string databaseStringId = Console.ReadLine();

        IConfiguration configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddEnvironmentVariables()
            .Build();
        _serviceCollection
            .AddGlobalServices(configuration)
            .AddSerilogWithElastic(configuration)
            .AddTelegramBots(configuration)
            .AddScoped<IUnitOfWork, UnitOfWork>()
            .AddSingleton(configuration)
            .AddTransient<ILogger42, SerilogLogger42>();

        var serviceProvider = _serviceCollection.BuildServiceProvider();
        segmentHelper = serviceProvider.GetRequiredService<ISegmentHelper>();
        iisApplicationDataHelper = serviceProvider.GetRequiredService<IisApplicationDataHelper>();
        databasePlatformVersionHelper = serviceProvider.GetRequiredService<IDatabasePlatformVersionHelper>();
        webAccessHelper = serviceProvider.GetRequiredService<IWebAccessHelper>();
        logger = serviceProvider.GetRequiredService<ILogger42>();
        _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        Logger42.Init(serviceProvider);
        logger.Info("Начало выполнения патча");

        if (string.IsNullOrEmpty(databaseStringId))
        {
            var publishedDatabases = _dbLayer.DatabasesRepository
                .AsQueryable()
                .Include(i => i.Account)
                .Where(d => d.PublishState == PublishState.Published.ToString() &&
                            d.State == DatabaseState.Ready.ToString() &&
                            d.AccountDatabaseOnDelimiter == null).ToList();
            Console.WriteLine($"Всего баз найдено {publishedDatabases.Count}");
            logger.Info($"Всего баз найдено {publishedDatabases.Count}");

            publishedDatabases.ForEach(pd => 
            {
                try
                {
                    ChangeAccountDatabaseWebConfig(pd);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"ошибкой с базой {pd.V82Name}");
                }
            });

            Console.ReadLine();
            return;
        }

        var databaseId = new Guid(databaseStringId);
        var accountDatabase = _dbLayer.DatabasesRepository
            .FirstOrDefault(d => d.Id == databaseId);
        if (accountDatabase == null)
            Console.WriteLine("База не найдена");
        ChangeAccountDatabaseWebConfig(accountDatabase);

        Console.ReadLine();
    }


    public static void ChangeAccountDatabaseWebConfig(AccountDatabase accountDatabase)
    {
        Console.WriteLine($"база {accountDatabase.V82Name}");

        var pathTo1CModule = GetPathToModule1C(accountDatabase, databasePlatformVersionHelper);
        var accountEncodeId = accountDatabase.Account.GetEncodeAccountId();
        var accessGroupName = DomainCoreGroups.GetDatabaseGroupName(accountDatabase.Account.IndexNumber, accountDatabase.DbNumber);
        Console.WriteLine($"accessGroupName: {accessGroupName}");
        var publishSiteName = segmentHelper.GetPublishSiteName(accountDatabase.Account);
        Console.WriteLine($"publishSiteName: {publishSiteName}");

        var accountContentServer = segmentHelper.GetContentServer(accountDatabase.Account);
        var host = _dbLayer.CloudServicesContentServerNodeRepository
            .FirstOrDefault(w => w.ContentServerId == accountContentServer.ID).PublishNodeReference.Address;
        Console.WriteLine($"host: {host}");
        var site = iisApplicationDataHelper.GetPublishSite(host, publishSiteName);
        Console.WriteLine($"PhysicalPath: {site.PhysicalPath}");

        var pathToWebConfig = segmentHelper.GetPublishedDatabasePhycicalPath
            (site.PhysicalPath, accountEncodeId, accountDatabase.V82Name, accountDatabase.Account);

        var filePath = Path.Combine(pathToWebConfig, "lock_web.config");

        if (!System.IO.File.Exists(filePath))
        {
            filePath = Path.Combine(pathToWebConfig, "web.config");
            if (!System.IO.File.Exists(filePath))
            {
                logger.Warn("Веб конфиг не найден");
                return;
            }
        }
        Console.WriteLine($"filePath: {filePath}");
        logger.Info($"база: {accountDatabase.V82Name}.\nweb.config: {filePath}\nhost: {host}");

        var webConfigText = System.IO.File.ReadAllText(filePath);
        if (webConfigText.Contains("users=\"?\""))
        {
            Console.WriteLine("База с анонимной авторизацией");
            webConfigText = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                          <add name=""1C Web-service Extension"" path=""*"" verb=""*"" modules=""IsapiModule"" scriptProcessor=""{pathTo1CModule}"" resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                            <add accessType=""Allow"" roles=""ad\{accessGroupName}"" />
                                                            <add accessType=""Allow"" users=""?"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";
        }
        else
        {
            Console.WriteLine("База без анонимной авторизации");
            webConfigText = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                          <add name=""1C Web-service Extension"" path=""*"" verb=""*"" modules=""IsapiModule"" scriptProcessor=""{pathTo1CModule}"" resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                            <add accessType=""Allow"" roles=""ad\{accessGroupName}"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";
        }

        Console.WriteLine("записываем веб конфиг");
        System.IO.File.WriteAllText(filePath, webConfigText);

        var externalAndGrandAccesses = _dbLayer.AcDbAccessesRepository
            .WhereLazy(x => x.AccountDatabaseID == accountDatabase.Id &&
                            x.AccountUserID != Guid.Empty)
            .Select(x => x.AccountUser.Login).ToList();

        Console.WriteLine("начинаем выдавать доступы");
        webAccessHelper.ManageAccess([accountDatabase], externalAndGrandAccesses, null);
        Console.WriteLine("доступы выданы\n");

        logger.Info($"c базой {accountDatabase.V82Name} закончил");

    }

    private static string GetPathToModule1C(AccountDatabase accountDatabase, IDatabasePlatformVersionHelper databasePlatformVersionHelper)
        => accountDatabase.IsFile == true
            ? databasePlatformVersionHelper.CreatePathTo1CModuleForFileDb(accountDatabase.AccountId,
                accountDatabase.PlatformType, accountDatabase.DistributionTypeEnum)
            : databasePlatformVersionHelper.CreatePathTo1CModuleForServerDb(accountDatabase.AccountId,
                accountDatabase.PlatformType);

    
}

public static class DependencyConfiguration
{
    public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
    {
        var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

        var elasticConfig = new ElasticConfigDto(
            conf["Elastic:Uri"],
            conf["Elastic:UserName"],
            conf["Elastic:Password"],
            conf["Elastic:ApiIndex"],
            conf["Logs:AppName"]
        );

        SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
        return collection;
    }
}
