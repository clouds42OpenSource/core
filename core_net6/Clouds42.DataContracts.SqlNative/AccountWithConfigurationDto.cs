﻿using dbContext = Clouds42.DomainContext.DataModels.AccountProperties;

namespace Clouds42.DataContracts.SqlNative
{
    /// <summary>
    /// Модель аккаунта с его конфигурацией
    /// </summary>
    public class AccountWithConfigurationDto
    {
        /// <summary>
        /// Аккаунт облака
        /// </summary>
        public DomainContext.DataModels.Account Account { get; set; }

        /// <summary>
        /// Конфигурация аккаунта
        /// </summary>
        public dbContext.AccountConfiguration AccountConfiguration { get; set; }
    }
}
