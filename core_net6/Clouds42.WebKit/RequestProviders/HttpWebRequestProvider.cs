﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Clouds42.Logger;

namespace Clouds42.WebKit.RequestProviders
{
    /// <summary>
    ///     HttpWeb запросы
    /// </summary>
    public class HttpWebRequestProvider(ILogger42 logger) : IHttpWebRequestProvider
    {
        private readonly Guid _session = Guid.NewGuid();

        /// <summary>
        ///     Отправить POST запрос с JSON данными
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="success">Статус</param>
        public string SendPostJson(string url, string jsonData, out bool success)
        {
            return SendPostJson(url, jsonData, null, null, out success);
        }

        /// <summary>
        /// Отправить POST запрос с JSON данными
        /// с указанными заголовками запроса
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="webHeaderCollection">Заголовки запроса</param>
        /// <param name="success">Статус</param>
        /// <returns>Ответ на запрос</returns>
        public string SendPostJsonWithHeaders(string url, string jsonData, WebHeaderCollection webHeaderCollection,
            out bool success)
        {
            return SendPostJson(url, jsonData, null, webHeaderCollection, out success);
        }

        /// <summary>
        ///     Отправить POST запрос с JSON данными
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="webHeaderCollection">Заголовки запроса</param>
        /// <param name="success">Статус</param>
        /// <param name="certificate">Сертификат</param>
        public string SendPostJson(string url, string jsonData, X509Certificate2 certificate, WebHeaderCollection webHeaderCollection, out bool success)
        {
            try
            {
                logger.Info($"[{_session}] :: Начало операции");

                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";

                if (webHeaderCollection != null)
                    webRequest.Headers = webHeaderCollection;

                webRequest.ContentType = "application/json";

                logger.Info($"[{_session}] :: URL: {url}");
                logger.Info($"[{_session}] :: Тело запроса: {jsonData}");

                if (certificate != null)
                {
                    webRequest.ClientCertificates.Add(certificate);

                    logger.Info($"[{_session}] :: Сертификат {certificate.FriendlyName} закреплен к запросу");
                }

                logger.Info($"[{_session}] :: Отправка запроса...");

                var streamWriter = new StreamWriter(webRequest.GetRequestStream());
                streamWriter.Write(jsonData);
                streamWriter.Close();

                logger.Info($"[{_session}] :: Ожидание ответа...");

                using var response = (HttpWebResponse)webRequest.GetResponse();
                var responseStream = response.GetResponseStream() ?? throw new InvalidOperationException("Неудалось получить ответ от сервера");
                var streamReaderData = new StreamReader(responseStream).ReadToEnd();
                var decodeString = System.Web.HttpUtility.HtmlDecode(streamReaderData);

                logger.Info($"[{_session}] :: Ответ: {decodeString}");
                logger.Info($"[{_session}] :: Операция успешно завершена");

                success = true;
                return decodeString;
            }
            catch (Exception e)
            {
                logger.Info($"[{_session}] :: Операция была завершена с ошибкой: {e}");

                success = false;
                return e.ToString();
            }
        }

        /// <summary>
        /// Выполнить запрос с Basic авторизацией.
        /// </summary>
        /// <param name="uri">Урл.</param>
        /// <param name="username">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <param name="success">Признак успешности.</param>
        /// <returns>Результат выполнения запроса.</returns>
        public string HttpRequestByBasicAuth(string uri, string username, string password, out bool success)
        {
            try
            {
                logger.Info($"[{_session}] :: Начало операции");

                logger.Info($"[{_session}] :: Запрос: {uri}");

                var authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

                logger.Info($"[{_session}] :: Логин: {username}");
                logger.Info($"[{_session}] :: Пароль: {password}");

                var request = (HttpWebRequest) WebRequest.Create(uri);

                request.Headers[nameof(Authorization)] = "Basic " + authInfo;
                
                logger.Info($"[{_session}] :: Отправка запроса...");

                var response = (HttpWebResponse) request.GetResponse();
                var responseStream = response.GetResponseStream() ?? throw new InvalidOperationException("Неудалось получить ответ от сервера");

                logger.Info($"[{_session}] :: Ожидание ответа...");

                using var sr = new StreamReader(responseStream);
                var responseString = sr.ReadToEnd();

                logger.Info($"[{_session}] :: Ответ: {responseString}");
                logger.Info($"[{_session}] :: Операция успешно завершена");

                success = true;
                return responseString;
            }
            catch (Exception ex)
            {
                logger.Info($"[{_session}] :: Операция была завершена с ошибкой: {ex}");

                success = false;
                return ex.ToString();
            }
        }
    }
}
