﻿using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Clouds42.WebKit
{
    /// <summary>
    ///     HttpWeb запросы
    /// </summary>
    public interface IHttpWebRequestProvider
    {
        /// <summary>
        ///     Отправить POST запрос с JSON данными
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="success">Статус</param>
        string SendPostJson(string url, string jsonData, out bool success);

        /// <summary>
        ///     Отправить POST запрос с JSON данными
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="webHeaderCollection">Заголовки запроса</param>
        /// <param name="success">Статус</param>
        /// <param name="certificate">Сертификат</param>
        string SendPostJson(string url, string jsonData, X509Certificate2 certificate,
            WebHeaderCollection webHeaderCollection, out bool success);

        /// <summary>
        /// Выполнить запрос с Basic авторизацией.
        /// </summary>
        /// <param name="uri">Урл.</param>
        /// <param name="username">Логин.</param>
        /// <param name="password">Пароль.</param>
        /// <param name="success">Признак успешности.</param>
        /// <returns>Результат выполнения запроса.</returns>
        string HttpRequestByBasicAuth(string uri, string username, string password, out bool success);

        /// <summary>
        /// Отправить POST запрос с JSON данными
        /// с указанными заголовками запроса
        /// </summary>
        /// <param name="url">УРЛ</param>
        /// <param name="jsonData">Данные</param>
        /// <param name="webHeaderCollection">Заголовки запроса</param>
        /// <param name="success">Статус</param>
        /// <returns>Ответ на запрос</returns>
        string SendPostJsonWithHeaders(string url, string jsonData, WebHeaderCollection webHeaderCollection,
            out bool success);
    }
}