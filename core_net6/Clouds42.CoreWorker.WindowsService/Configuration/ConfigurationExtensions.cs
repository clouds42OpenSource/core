﻿
using Clouds42.Logger.Serilog.Models;

namespace Clouds42.CoreWorker.WindowsService.Configuration
{
    public static class ConfigurationExtensions
    {
        public static ElasticConfigDto GetElasticSettings(this IConfiguration configuration)
        {
            return new ElasticConfigDto(
                configuration.GetValue<string>("ElasticUri", string.Empty),
                configuration.GetValue<string>("ElasticUserName", string.Empty),
                configuration.GetValue<string>("ElasticPassword", string.Empty),
                configuration.GetValue<string>("ElasticCoreWorkerIndex", string.Empty),
                configuration.GetValue<string>("Logs:AppName", string.Empty));
        }

        public static LocalLogsConfigDto GetLocalLogsConfig(this IConfiguration configuration)
        {
            return new LocalLogsConfigDto(
                fileName: configuration.GetValue<string>("Logs:FileName", string.Empty));
        }
    }
}



