﻿using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Microsoft.Extensions.Primitives;

namespace Clouds42.CoreWorker.WindowsService.Configuration
{
    public class ElasticLoggingConfigurationManager(IConfiguration configuration, ILogger42 logger)
    {
        public void Initialize()
        {
            SerilogConfig.ConfigureSerilog(configuration.GetLocalLogsConfig(), configuration.GetElasticSettings());
            StartListener();
        }

        readonly object _lock = new();
        bool _isListening;
        IDisposable? _changesListener;

        private void StartListener()
        {
            logger.Info("Start listen");
            lock (_lock)
            {
                if (_isListening)
                    return;
                _changesListener = ChangeToken.OnChange(
                    () => configuration.GetReloadToken(),
                    ReloadElasticTarget);
                _isListening = true;
            }
        }

        private void ReloadElasticTarget()
        {
            logger.Info("Reload Elastic Target");
            SerilogConfig.ConfigureSerilog(configuration.GetLocalLogsConfig(), configuration.GetElasticSettings());
        }

        public void Dispose()
        {
            _changesListener?.Dispose();
        }
    }
}



