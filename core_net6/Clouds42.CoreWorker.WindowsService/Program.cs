﻿using System.Net;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Configurations;
using Clouds42.CoreWorker;
using Clouds42.CoreWorker.JobWrappers;
using Clouds42.CoreWorker.WindowsService.Configuration;
using Clouds42.CoreWorker.WindowsService.Providers;
using Clouds42.CoreWorkerTask;
using Clouds42.DependencyRegistration;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Jwt;
using Clouds42.Logger;
using Clouds42.Logger.Serilog;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.TelegramBot;
using Clouds42.TelegramBot.Interfaces;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.EntityFrameworkCore;
using Repositories;
using static Clouds42.CoreWorker.WindowsService.Providers.WorkerServiceOptionsProvider;

var conf = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

var serviceName = conf.GetValue("ServiceName", "42Clouds CoreWorker");

IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService(options =>
    {
        options.ServiceName = serviceName;
    })
    .UseDefaultServiceProvider(options => options.ValidateScopes = false)
    .ConfigureAppConfiguration(builder =>
    {
        builder
            .AddGcpKeyValueSecrets()
            .AddJsonFile("appsettings.json");
    })
    .ConfigureServices((context, services) =>
    {
        ConfigurationHelper.SetConfiguration(context.Configuration);
        services
            .AddClouds42DbContext(context.Configuration)
            .AddHttpClient()
            .AddHttpClient<IisHttpClient>()
            .ConfigurePrimaryHttpMessageHandler(() =>
            {
                var login = context.Configuration.GetRequiredSection("IISConfiguration:Login")?.Value;
                var password = context.Configuration.GetRequiredSection("IISConfiguration:Password")?.Value;
                var domain = context.Configuration.GetRequiredSection("IISConfiguration:Domain")?.Value;

                return new HttpClientHandler
                {
                    UseDefaultCredentials = false,
                    ServerCertificateCustomValidationCallback =
                        HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                    Credentials = new NetworkCredential(login, password, domain),
                    PreAuthenticate = true
                };
            });

        services
            .AddCoreWorkerJobWrappers()
            .AddSingleton<ElasticLoggingConfigurationManager>()
            .AddTransient<IAuthTokenProvider, CoreWorkerAuthTokenProvider>()
            .AddSingleton<WorkerServiceOptionsProvider>()
            .AddScoped<IUnitOfWork, UnitOfWork>()
            .AddGlobalServices(context.Configuration,true)
            .AddJwt()
            .AddMemoryCache();

            var sp = services.BuildServiceProvider();

            services.AddWorkerHostedService(sp.GetRequiredService<IUnitOfWork>(), sp.GetRequiredService<WorkerServiceOptionsProvider>().GetOptions(), sp.GetRequiredService<ILogger42>());

        var configuration = context.Configuration;
        var connectionString = context.Configuration.GetConnectionString("DefaultConnection");
        
        SerilogConfig.ConfigureSerilog(configuration.GetLocalLogsConfig());

        services.AddDistributedIsolatedExecutor(new DbObjectLockProvider(connectionString!, context.Configuration.GetSection("ConnectionStrings:DatabaseType").Value!));

        services.AddTelegramBots(configuration);

    })
    .Build();

host.Services.GetRequiredService<ElasticLoggingConfigurationManager>().Initialize();
Logger42.Init(host.Services);
HandlerException42.Init(host.Services);
ConfigurationHelper.SetConfiguration(host.Services.GetRequiredService<IConfiguration>());
var logger = Logger42.GetLogger();
var telegramBot = host.Services.GetRequiredService<IEnumerable<ITelegramBot42>>().FirstOrDefault(x => x.Type == BotType.Alert);

await telegramBot!.SendAlert("Worker успешно запущен!!!");

logger.Info("App starting...");

try
{
    using (var scope = host.Services.CreateScope())
    {
        var dbContext = scope.ServiceProvider.GetRequiredService<Clouds42DbContext>();
        dbContext.Database.Migrate();
    }

    await host.RunAsync();
}
catch (Exception e)
{
    logger.Error(e, "В процессе запуска приложения возникла ошибка");
}
