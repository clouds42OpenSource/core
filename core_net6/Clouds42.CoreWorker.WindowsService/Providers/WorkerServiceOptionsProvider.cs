﻿using Clouds42.Configurations.Configurations;
using Microsoft.Win32;

namespace Clouds42.CoreWorker.WindowsService.Providers
{
    internal class WorkerServiceOptionsProvider
    {
        readonly IConfiguration _configuration;
        public WorkerServiceOptionsProvider(IConfiguration configuration)
        {
            _configuration = configuration;
            _optionsLazy = new Lazy<CoreWorkerServiceOptions>(LoadOptions);
        }

        readonly Lazy<CoreWorkerServiceOptions> _optionsLazy;
        public CoreWorkerServiceOptions GetOptions() => _optionsLazy.Value;
        private CoreWorkerServiceOptions LoadOptions()
        {
            var workerId = GetWorkerIdFromRegistry();
            return new CoreWorkerServiceOptions(workerId)
            {
                PullToInternalQueueTimeout = GetPullFromInternalQueueTimeout(),
                PushToInternalQueueTimeout = GetPushToInternalQueueTimeout(),
                NoTaskDelay = GetNoTaskDelay(),
                ProducerPollingRate = GetPollingRate()
            };
        }

        TimeSpan? GetPullFromInternalQueueTimeout()
        {
            var milliseconds = _configuration
                .GetValue<int?>("WorkerService:PullFromInternalQueueTimeoutMs", default);

            return !milliseconds.HasValue ? default(TimeSpan?) : TimeSpan.FromMilliseconds(milliseconds.Value);
        }

        TimeSpan? GetPushToInternalQueueTimeout()
        {
            var milliseconds = _configuration
                .GetValue<int?>("WorkerService:PushToInternalQueueTimeoutMs", default);

            return !milliseconds.HasValue ? default(TimeSpan?) : TimeSpan.FromMilliseconds(milliseconds.Value);
        }

        TimeSpan? GetPollingRate()
        {
            var milliseconds = _configuration
                .GetValue<int?>("WorkerService:PollingRateMs", default);

            return !milliseconds.HasValue ? default(TimeSpan?) : TimeSpan.FromMilliseconds(milliseconds.Value);
        }

        static TimeSpan? GetNoTaskDelay()
        {
            try
            {
                var valueInSeconds = CloudConfigurationProvider.CoreWorker
                    .GetTimeoutInSecondsBetweenCaptureNewTaskRequest();
                if (valueInSeconds >= 0)
                    return TimeSpan.FromSeconds(valueInSeconds);

            }
            catch (Exception)
            {
                //internionally left empty
            }
            return default;
        }

        static short GetWorkerIdFromRegistry()
        {
            using var registry = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            using var key = registry.OpenSubKey(@"SOFTWARE\Clouds42\CoreWorker");
            var keyValue = key?.GetValue("ServiceId")?.ToString() ?? throw new InvalidOperationException(
                @"Не найден ключ 'HKEY_LOCAL_MACHINE\SOFTWARE\Clouds42\CoreWorker\@ServiceId' в реестре!");
          
            if (!short.TryParse(keyValue, out var workerId))
            {
                throw new InvalidOperationException(
                    @"Невозможно распознать значение по ключу 'HKEY_LOCAL_MACHINE\SOFTWARE\Clouds42\CoreWorker\@ServiceId' в реестре!");
            }

            return workerId;

        }

        public class CoreWorkerAuthTokenProvider(IConfiguration configuration) : IAuthTokenProvider
        {
            public string GetAuthToken()
            {
                var token = configuration.GetValue<string?>("Core42Api:AuthToken", default);
                return token ?? throw new InvalidOperationException("Authentication token is not found in configuration");
            }
        }
    }
}
