﻿using Clouds42.Tools1C.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Tools1C
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddTools1CServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<Launch1CHelper>();
            serviceCollection.AddTransient<LogUpdate1CParser>();
            return serviceCollection;
        }
    }
}
