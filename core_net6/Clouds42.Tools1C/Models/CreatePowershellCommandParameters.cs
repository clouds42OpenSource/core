﻿using Clouds42.Tools1C.Helpers;

namespace Clouds42.Tools1C.Models;

public class CreatePowershellCommandParameters 
{
    /// <summary>
    /// Имя сервера 1С:Предприятия
    /// </summary>
    public string ServerName { get; set; }

    /// <summary>
    /// Имя информационной базы на сервере 1С:Предприятия
    /// </summary>
    public string ReferenceName { get; set; }

    /// <summary>
    /// Тип сервера базы данных
    /// </summary>
    public string DatabaseManagementSystem { get; set; }

    /// <summary>
    /// Имя сервера базы данных
    /// </summary>
    public string DatabaseServerName { get; set; }

    /// <summary>
    /// Имя базы данных
    /// </summary>
    public string DatabaseName { get; set; }

    /// <summary>
    /// Имя пользователя базы данных
    /// </summary>
    public string DatabaseUserId { get; set; }

    /// <summary>
    /// Пароль пользователя базы данных
    /// </summary>
    public string DatabaseUserPassword { get; set; }

    /// <summary>
    /// Распространять лицензию через сервер 1С
    /// </summary>
    public bool DistributeLicense { get; set; }

    /// <summary>
    /// Создавать базу данных SQL
    /// </summary>
    public bool CreateSqlDatabase { get; set; }

    /// <summary>
    /// Запретить регламентные задания
    /// </summary>
    public bool SchJobDenied { get; set; }

    /// <summary>
    /// Имя администратора кластера 1С
    /// </summary>
    public string ServerAdminUsername { get; set; }

    /// <summary>
    /// Пароль администратора кластера 1С
    /// </summary>
    public string ServerAdminPassword { get; set; }

    /// <summary>
    /// Локаль (язык)
    /// </summary>
    public string Locale { get; set; }

    /// <summary>
    /// Путь к файлу логирования
    /// </summary>
    public string LogFilePath { get; set; }
    
    /// <summary>
    /// Путь к файлу шаблона (.dt)
    /// </summary>
    public string TemplatePath { get; set; }
    
    /// <summary>
    /// Генерирует команду PowerShell для создания информационной базы
    /// </summary>
    /// <returns>Строка команды PowerShell</returns>
    public string GeneratePowerShellCommand()
    {
        var command =  $"CREATEINFOBASE " +
                       $@"Srvr=""{ServerName}"";Ref=""{ReferenceName}"";DBMS=""{DatabaseManagementSystem}"";" +
                       $@"DBSrvr=""{DatabaseServerName}"";DB=""{DatabaseName}"";DBUID=""{DatabaseUserId}"";DBPwd=""{DatabaseUserPassword}"";" +
                       $@"LicDstr=""{(DistributeLicense ? "Y" : "N")}"";CrSQLDB=""{(CreateSqlDatabase ? "Y" : "N")}"";SchJobDn=""{(SchJobDenied ? "Y" : "N")}"";" +
                       $@"SUsr=""{ServerAdminUsername}"";SPwd=""{ServerAdminPassword}"";Locale=""{Locale}"";";

        if (!string.IsNullOrEmpty(TemplatePath))
        {
            command += $@" /UseTemplate ""{TemplatePath}"" ";
        }

        command += $@"/Out ""{LogFilePath}"" " +
                   $@"{Start1CCommandLineConstants.DisableStartupMessages} " +
                   $@"{Start1CCommandLineConstants.DisableStartupDialogs} " +
                   $@"{Start1CCommandLineConstants.UsePrivilegedMode}";

        return command;
    }
}
