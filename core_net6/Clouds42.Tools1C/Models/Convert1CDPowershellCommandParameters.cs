﻿namespace Clouds42.Tools1C.Models;

public class Convert1CdPowershellCommandParameters
{
    /// <summary>
    /// Путь к информационной базе
    /// </summary>
    public string DatabasePath { get; set; }

    /// <summary>
    /// Путь для выгрузки .dt файла
    /// </summary>
    public string DtFilePath { get; set; }

    /// <summary>
    /// Имя пользователя 1С (если требуется аутентификация)
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// Пароль пользователя 1С
    /// </summary>
    public string UserPassword { get; set; }

    /// <summary>
    /// Путь к файлу логирования
    /// </summary>
    public string LogFilePath { get; set; }

    /// <summary>
    /// Генерирует команду PowerShell для выгрузки базы 1С в формат .dt
    /// </summary>
    /// <returns>Строка команды PowerShell</returns>
    public string GeneratePowerShellCommand()
    {
        var command =  $"DESIGNER /F \"{DatabasePath}\" /DumpIB \"{DtFilePath}\"";

        if (!string.IsNullOrEmpty(UserName))
        {
            command += $" /N \"{UserName}\"";
        }

        if (!string.IsNullOrEmpty(UserPassword))
        {
            command += $" /Pwd \"{UserPassword}\"";
        }

        command += $" /DisableStartupDialogs /UC \"disable\"";

        if (!string.IsNullOrEmpty(LogFilePath))
        {
            command += $" /Out \"{LogFilePath}\"";
        }

        return command;
    }
}
