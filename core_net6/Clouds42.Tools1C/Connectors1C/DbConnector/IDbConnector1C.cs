﻿using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Clouds42.Tools1C.Connectors1C.DbConnector
{
    /// <summary>
    /// Коннектор к информационной базе.
    /// </summary>
    public interface IDbConnector1C
    {

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>        
        ConnectorResultDto<MetadataResultDto> GetMetadataInfo();        

        /// <summary>
        /// Задать права администратора пользователю в 1С.
        /// </summary>        
        void SetAdminRolesForUser(string userName);

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>        
        void ApplyUpdates();

        /// <summary>
        /// Попытаться принять легальность полученных обновлений.
        /// </summary>        
        /// <returns>Признак успешности.</returns>
        bool TryApplyLegalUpdate();
    }
}