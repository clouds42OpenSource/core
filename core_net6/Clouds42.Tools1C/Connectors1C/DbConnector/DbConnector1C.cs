﻿using System;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Logger;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.Tools1C.Connectors1C.DbConnector
{
    /// <summary>
    /// Коннектор к информационной базе.
    /// </summary>
    public sealed class DbConnector1C : IDbConnector1C, IDisposable
    {
        private readonly string _sessionToken;
        private readonly ComConnectorRegistrar _comConnectorRegistrar;
        protected readonly ModelBaseDto Parameters;
        private readonly ILogger42 _logger = Logger42.GetLogger();

        public DbConnector1C(ModelBaseDto parameters)
        {
            Parameters = parameters;
            _sessionToken = Parameters.V82Name;
            _comConnectorRegistrar = new ComConnectorRegistrar();
        }        

        /// <summary>
        /// Получить метаданные информационной базы.
        /// </summary>        
        [HandleProcessCorruptedStateExceptions]
        public ConnectorResultDto<MetadataResultDto> GetMetadataInfo()
        {
            try
            {
                return new ConnectorResultDto<MetadataResultDto>
                {
                    Result = GetMetadataInfoWrap(),
                    ConnectCode = ConnectCodeDto.Success
                };
            }
            catch (Exception ex)
            {
                var errorModel = Connector1CErrorParser.Parse(ex);
                return new ConnectorResultDto<MetadataResultDto>
                {
                    ConnectCode = errorModel.ConnectCode,
                    Message = errorModel.ErorMessage
                };
            }
        }

        /// <summary>
        /// Задать права администратора пользователю в 1С.
        /// </summary>        
        public void SetAdminRolesForUser(string userName)
        {
            SetAdminRolesForUser(userName, Connect());
        }

        /// <summary>
        /// Принять полученные обновления информационной базы.
        /// </summary>        
        public void ApplyUpdates()
        {
            var connector = Connect();

            try
            {
                connector.Обработки.ОбновлениеИнформационнойБазы.Создать().ВыполнитьОбновление();
                Trace($"Успешно приняты обновления для базы '{Parameters.GetArguments()}'");
                SetAdminRolesForUser(Parameters.Login, connector);
                return;
            }
            catch (Exception ex)
            {
                Trace($"Принятие обновлений. Способ 1 - ошибка ? {ex.GetFullInfo()}");
            }

            try
            {
                connector.ОбновлениеИнформационнойБазы.ВыполнитьОбновлениеИнформационнойБазы();
                Trace($"Успешно приняты обновления для базы '{Parameters.GetArguments()}'");
                TryApplyLegalUpdateWrap(connector);
                SetAdminRolesForUser(Parameters.Login, connector);
            }
            catch (Exception ex)
            {
                var message = $"Ошибка принятия обновлений способ 2. {ex.GetFullInfo()}";
                Trace(message);
                throw;
            }
        }

        /// <summary>
        /// Попытаться принять легальность полученных обновлений.
        /// </summary>        
        /// <returns>Признак успешности.</returns>
        public bool TryApplyLegalUpdate()
        {
            try
            {
                return TryApplyLegalUpdateWrap(Connect());
            }
            catch (Exception ex)
            {
                Trace($"Ошибка принятия легальности :: {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Попытаться принять легальность полученных обновлений.
        /// Примечание!!! Метод нужен для корректной обработки GC.Collect по утилизации объектов коннектора
        /// </summary>
        /// <param name="connector">Подключение к базе.</param>
        /// <returns>Признак успешности.</returns>
        private bool TryApplyLegalUpdateWrap(dynamic connector)
        {
            try
            {
                connector.ОбновлениеИнформационнойБазыСлужебный.ЗаписатьПодтверждениеЛегальностиПолученияОбновлений();
                return true;
            }
            catch (Exception ex)
            {
                Trace($"Ошибка вызова ЗаписатьПодтверждениеЛегальностиПолученияОбновлений :: {ex}");
                return false;
            }
        }
        
        /// <summary>
        /// Задать права администратора пользователю в 1С.       
        /// </summary>        
        private void SetAdminRolesForUser(string userName, dynamic connector)
        {
            try
            {                
                var user = connector.ПользователиИнформационнойБазы.НайтиПоИмени(userName);

                if (user == null)
                {
                    var message =
                        $"В информационной базе '{Parameters.GetArguments()}' не удалось найти пользователя '{userName}'";
                    Trace(message);
                    throw new NotFoundException(message);
                }

                TrySetRoleForUser(user, connector.Метаданные.Роли.ПолныеПрава);
                TrySetRoleForUser(user, connector.Метаданные.Роли.АдминистраторСистемы);

                Trace(
                    $"В информационной базе '{Parameters.GetArguments()}' пользователю '{userName}' выданы права 'ПолныеПрава' и 'АдминистраторСистемы'");
            }
            catch (Exception ex)
            {
                var message = $"Ошибка установки прав администратора пользователю '{userName}' в базе '{Parameters.GetArguments()}' :: {ex}";
                Trace(message);                
            }
        }

        /// <summary>
        /// Попытаться добавить роль пользователю.
        /// </summary>
        /// <param name="user">Пользователь 1С.</param>
        /// <param name="role">Роль 1С.</param>
        private void TrySetRoleForUser(dynamic user, dynamic role)
        {
            try
            {
                user.Роли.Добавить(role);
                user.Записать();
            }
            catch (Exception ex)
            {
                Trace($"Не удалось добавить роль {role} пользователю {user} :: {ex}");
            }
        }        

        /// <summary>
        /// Враппер к методу получение метаданных информационной базы.
        /// Примечание!!! Метод нужен для корректной обработки GC.Collect по утилизации объектов коннектора.
        /// </summary>        
        private MetadataResultDto GetMetadataInfoWrap()
        {
            var connector = Connect();
            return new MetadataResultDto(connector.Метаданные.Версия, connector.Метаданные.Синоним);
        }

        /// <summary>
        /// Подключиться к информационной базе.
        /// </summary>        
        [HandleProcessCorruptedStateExceptions]
        private dynamic Connect()
        {
            try
            {
                var connectString = Parameters.GetArguments();
                Trace($"Подключение: {connectString}");

                var comConnectorName = ComConnectorNameBuilder.Build(Parameters);
                Trace($"Полученно имя коннектора {comConnectorName}");

                var logpath = Parameters.GetRegisterLog();
                Trace($"Получили путь до логов : {logpath}");
                if (Directory.Exists(logpath))
                {
                    Trace($"Пытаемся удалить логи 1С : {logpath}");
                    if(File.Exists(logpath + @"\1Cv8.lgd"))
                    {
                        Trace($@"Удаляем старый тип логов 1С : {logpath}\1Cv8.lgd");
                        File.Delete(logpath + @"\1Cv8.lgd");
                    }
                    

                    if(!File.Exists(logpath + @"\1Cv8.lgf"))
                    {
                        Trace($@"Добавляем новый тип логов 1С : {logpath}\1Cv8.lgf");
                        var file = File.Create(logpath + @"\1Cv8.lgf");
                        file.Dispose();
                    }
                   
                }

                var connectorInstance = GetOrCreateConnectorInstance(comConnectorName);

                return connectorInstance.Connect(connectString);
            }
            catch (Exception ex)
            {
                var errorMessage = $"{ex.Message} {ex.InnerException?.Message}";
                Trace($"Ошибка подлючения : {errorMessage}");
                throw;
            }
        }

        /// <summary>
        /// Получить инстанс COM коннектора.
        /// </summary>
        /// <param name="comConnectorName">Имя COM коннектора.</param>       
        private dynamic GetOrCreateConnectorInstance(string comConnectorName)
        {
            try
            {
                return _comConnectorRegistrar.GetComConnectorInstance(Parameters);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Не зарегистрирован COM коннектор '{comConnectorName}' :: {ex.Message}";
                Trace(errorMessage);
                throw new COMException(errorMessage);
            }
        }

        /// <summary>
        /// Написать в лог трассировку.
        /// </summary>
        private void Trace(string message)
            => _logger.Info($"{_sessionToken} :: {message}");

        /// <summary>
        /// Освобождение ресурсов 
        /// </summary>
        public void Dispose()
            => Trace("Освобождение ресурсов");
    }
}
