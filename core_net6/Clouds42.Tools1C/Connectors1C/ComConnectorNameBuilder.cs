﻿using System;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.Tools1C.Connectors1C
{
    /// <summary>
    /// Хэлпер для получения имени COM коннектора
    /// </summary>
    internal static class ComConnectorNameBuilder
    {
        /// <summary>
        /// Сформировать имя COM коннектора для версии платформы.
        /// </summary>
        /// <param name="platform">Платформа.</param>
        /// <returns>Имя COM коннектора.</returns>
        public static string Build(IPlatformDto platform)
        {
            if (platform.PlatformType == PlatformType.V83)
                return string.IsNullOrEmpty(platform.PlatformVersion) 
                    ? "V83.COMConnector.1" 
                    : $"V{platform.PlatformVersion}.COMConnector";

            if (platform.PlatformType == PlatformType.V82)
                return "V82.COMConnector.1";

            throw new InvalidOperationException(
                $"Не удалось получить имя коннектора для платформы {platform.PlatformType} версии {platform.PlatformVersion}");
        }
    }
}
