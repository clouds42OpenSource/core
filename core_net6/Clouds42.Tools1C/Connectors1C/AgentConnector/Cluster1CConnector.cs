﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Logger;
using Clouds42.Tools1C.Helpers;

namespace Clouds42.Tools1C.Connectors1C.AgentConnector
{
    /// <summary>
    /// Коннектор для подключения к кластеру 1С
    /// </summary>
    public sealed class Cluster1CConnector(ClusterParametersDto clusterParameters) : IDisposable
    {
        private readonly ComConnectorRegistrar _comConnectorRegistrar = new();
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Данные по кластеру.
        /// </summary>
        public ClusterConnectionResultDto ClusterResult { get; private set; }

        /// <summary>
        /// Подключиться к кластеру.
        /// </summary>
        public void Connect()
        {
            var comConnectorName = ComConnectorNameBuilder.Build(clusterParameters);
            Trace($"Получено имя КОМ коннектора '{comConnectorName}'.");

            var connectorInstance = GetConnectorInstance(comConnectorName);
            Trace($"Создан инстанс КОМ коннектора '{comConnectorName}'.");

            var agent = ConnectToAgent(connectorInstance);
            Trace("Подключение к агенту 1С.");

            var cluster = agent.GetClusters().GetValue(0);

            Authenticate(agent, cluster);
            Trace("Авторизация в кластер.");

            var sess = (Array)agent.GetSessions(cluster);
            Trace("Получение сессий.");

            ClusterResult = new ClusterConnectionResultDto(connectorInstance, agent, cluster, sess);
        }

        /// <summary>
        /// Получить инстанс коннектора.
        /// </summary>
        /// <param name="comConnectorName">Имя коннектора.</param>
        /// <returns>Инстанс коннектора</returns>
        private dynamic GetConnectorInstance(string comConnectorName)
        {
            try
            {
                return _comConnectorRegistrar.GetComConnectorInstance(clusterParameters);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Не зарегистрирован COM коннектор '{comConnectorName}' :: {ex.Message}";
                Trace(errorMessage);
                throw new COMException(errorMessage);
            }
        }

        /// <summary>
        /// Авторизоваться в кластер.
        /// </summary>
        /// <param name="agent">Агент сервера предприятия</param>
        /// <param name="cluster">Кластер сервера предприятия</param>
        private void Authenticate(dynamic agent, dynamic cluster)
        {
            try
            {
                agent.Authenticate(cluster, clusterParameters.AdminName, clusterParameters.AdminPassword);
            }
            catch (Exception ex)
            {
                var message = $"Ошибка авторизации в кластер {clusterParameters.ConnectionAddress}, Имя '{clusterParameters.AdminName}'  :: {ex.Message} {ex.InnerException?.Message} ";
                Trace(message);
                throw new ClusterAuthenticateException(message);
            }
        }

        /// <summary>
        /// Подключиться к агенту кластера.
        /// </summary>
        /// <param name="connector">Инстанс коннектора</param>
        private dynamic ConnectToAgent(dynamic connector)
        {

            var enterpriseServers = GetEnterpriseServers();
            var catchExceptionMessage = new StringBuilder();
            foreach (var enterpriseServer in enterpriseServers)
            {
                try
                {
                    var cluster =
                        connector.ConnectAgent(enterpriseServer);
                    return cluster;
                }
                catch (Exception ex)
                {
                    catchExceptionMessage.AppendLine($"{enterpriseServer}->{ex.GetFullInfo(false)}");
                }
            }

            var message = $"Не удалось подключиться к кластеру 1С {clusterParameters.ConnectionAddress} :: {catchExceptionMessage}";
            Trace(message);

            throw new ClusterConnectionException(message);
        }

        /// <summary>
        /// Получить список кластеров из строки подключения.        
        /// </summary>
        /// <returns>Список кластеров из строки подключения.</returns> 
        private List<string> GetEnterpriseServers()
            => string.IsNullOrEmpty(clusterParameters.ConnectionAddress)
                ? []
                : clusterParameters.ConnectionAddress.Split(',').ToList();

        /// <summary>
        /// Освободить ресурсы
        /// </summary>
        public void Dispose()
            => _comConnectorRegistrar.DeleteComConnectorRegistration(clusterParameters);

        /// <summary>
        /// Записать трассировку в лог.
        /// </summary>
        private void Trace(string message)
            => _logger.Trace($"{clusterParameters.ConnectionAddress} :: {message}");
    }
}
