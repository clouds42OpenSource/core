﻿using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.HandlerExeption.Contract;
 
using Clouds42.Logger;

namespace Clouds42.Tools1C.Connectors1C.AgentConnector
{
    /// <summary>
    /// Базовый класс подклшючения к кластеру 1С.
    /// </summary>
    public abstract class ClusterConnector1CBase(ClusterParametersDto clusterParameters)
    {                

        protected ClusterParametersDto ClusterParameters = clusterParameters;
        protected readonly ILogger42 Logger = Logger42.GetLogger();
        protected readonly IHandlerException HandlerException = HandlerException42.GetHandler();

        /// <summary>
        /// Подключиться к кластеру.
        /// </summary>
        public abstract void Connect();

        /// <summary>
        /// Полукчить список кластеров из строки подключения.        
        /// </summary>        
        protected List<string> GetEnterpriseServers()
        {
            if (string.IsNullOrEmpty(ClusterParameters.ConnectionAddress))
                return [];

            return ClusterParameters.ConnectionAddress.Split(',').ToList();
        }

        /// <summary>
        /// Записать трассировку в лог.
        /// </summary>
        protected void Trace(string message)
        {
            Logger.Trace($"{ClusterParameters.ConnectionAddress} :: {message}");
        }
    }
}
