﻿using System;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Connectors1C.AgentConnector;

namespace Clouds42.Tools1C.Connectors1C.AgentConnector
{
    /// <summary>
    /// Класс подключения к кластеру 8.2
    /// </summary>
    public class ClusterConnector1C82(ClusterParametersDto clusterParameters)
        : ClusterConnector1CBase(clusterParameters)
    {
        public class Result
        {
            internal Result(
                V82.COMConnector comConnector = null,
                V82.IServerAgentConnection agent = null,
                V82.IClusterInfo cluster = null,
                Array sessions = null)
            {
                ComConnector = comConnector;
                Agent = agent;
                Cluster = cluster;
                Sessions = sessions;
            }
            
            /// <summary>
            /// Ком коннектор.
            /// </summary>
            public V82.COMConnector ComConnector { get; }

            /// <summary>
            /// Агент кластера.
            /// </summary>
            public V82.IServerAgentConnection Agent { get; }

            /// <summary>
            /// Кластер.
            /// </summary>
            public V82.IClusterInfo Cluster { get; }

            /// <summary>
            /// Сессии кластера.
            /// </summary>
            public Array Sessions { get; }
        }

        /// <summary>
        /// Данные по кластеру.
        /// </summary>
        public Result ClusterResult { get; private set; }

        /// <summary>
        /// Подключиться к кластеру 8.2.
        /// </summary>        
        public override void Connect()
        {

            var connector = new V82.COMConnector();

            var agent = ConnectToAgent(connector);

            var cluster = (V82.IClusterInfo)agent.GetClusters().GetValue(0);

            Authenticate(agent, cluster);

            var sess = agent.GetSessions(cluster);

            ClusterResult = new Result(connector, agent, cluster, sess);
        }

        /// <summary>
        /// Авторизоваться в кластер.
        /// </summary>
        private void Authenticate(V82.IServerAgentConnection agent, V82.IClusterInfo cluster)
        {
            try
            {
                agent.Authenticate(cluster, ClusterParameters.AdminName, ClusterParameters.AdminPassword);
            }
            catch (Exception ex)
            {
                throw new ClusterAuthenticateException($"Ошибка авторизации в кластер {ClusterParameters.ConnectionAddress}, Имя '{ClusterParameters.AdminName}' :: {ex.Message} {ex.InnerException?.Message}");
            }
        }

        /// <summary>
        /// Подключиться к агенту кластера.
        /// </summary>        
        private V82.IServerAgentConnection ConnectToAgent(V82.COMConnector connector)
        {            

            var enterpriseServers = GetEnterpriseServers();
            foreach (var enterpriseServer in enterpriseServers)
            {
                try
                {
                    var cluster =
                        connector.ConnectAgent(enterpriseServer);
                    return cluster;
                }
                catch (Exception ex)
                {
                    HandlerException.Handle(ex, $"[Ошибка подключения к серверу предприятия] {enterpriseServer}");
                }
            }

            throw new ClusterConnectionException(
                $"Не удалось подключиться к кластеру 1С {ClusterParameters.ConnectionAddress}");
        }
        
    }
}
