﻿using System;
using System.IO;
using System.Linq;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Link;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Tools1C.Models;
using JetBrains.Annotations;

namespace Clouds42.Tools1C.Helpers
{
    /// <summary>
    /// Хэлпер для запуска 1С
    /// </summary>
    public class Launch1CHelper(IUnitOfWork dbLayer, IAccountDatabaseModelCreator creator, IAccessProvider accessProvider, IAccountDatabasePathHelper pathHelper,  ILogger42 logger)
    {
        /// <summary>
        /// Получить параметры для запуска 1С в режиме конфигурации
        /// </summary>
        /// <param name="accountDatabaseDestinationPath">Путь к инф. базе</param>
        /// <param name="uploadedDtFilePath">Путь к азгруженному файлу Dt</param>
        /// <returns>Параметры для запуска 1С в режиме конфигурации</returns>
        public Launch1CParamsDto GetLaunch1CParamsForConfigMode(string accountDatabaseDestinationPath, string uploadedDtFilePath)
        {
            var platformExecPath = GetPlatformExecPath();
            var logFilePath = GetLogFilePath();
            var connectionString = GetConfigConnectionString(accountDatabaseDestinationPath, uploadedDtFilePath, logFilePath);

            return new Launch1CParamsDto
            {
                ExeParameters = connectionString,
                Client1CPath = platformExecPath,
                LogFilePath = logFilePath
            };
        }
        


        public Launch1CParamsDto GetLaunch1CParamsForCreateMode(Guid accountDatabaseId, [CanBeNull] string uploadedDtFilePath, bool isCopy = false)
        {
            var enterpriseModel = creator.CreateModel(accountDatabaseId);

            if (enterpriseModel == null)
            {
                logger.Warn("EnterpriseModel is null");
                throw new InvalidOperationException("EnterpriseModel cannot be null");
            }
            
            CheckNullWithLogging(enterpriseModel.EnterpriseServer, nameof(enterpriseModel.EnterpriseServer));
            CheckNullWithLogging(enterpriseModel.SqlServer, nameof(enterpriseModel.SqlServer));
            CheckNullWithLogging(enterpriseModel.AccountDatabase, nameof(enterpriseModel.AccountDatabase));
            
            var parameters = new CreatePowershellCommandParameters
            {
                ServerAdminPassword = enterpriseModel.EnterpriseServer.AdminPassword,
                ServerAdminUsername = enterpriseModel.EnterpriseServer.AdminName,
                ServerName = enterpriseModel.EnterpriseServer.ConnectionAddress,
                DatabaseManagementSystem = "MSSQLServer",
                DatabaseServerName = enterpriseModel.SqlServer.ConnectionAddress,
                LogFilePath = GetLogFilePath(),
                DatabaseName = enterpriseModel.AccountDatabase.SqlName,
                ReferenceName = enterpriseModel.AccountDatabase.V82Name,
                DistributeLicense = true,
                CreateSqlDatabase = true,
                SchJobDenied = isCopy,
                TemplatePath = uploadedDtFilePath,
                DatabaseUserId =  CloudConfigurationProvider.AccountDatabase.Cluster.GetSqlLogin(),
                DatabaseUserPassword = CloudConfigurationProvider.AccountDatabase.Cluster.GetSqlPassword(),
                Locale = "ru_RU"
            };

            LogEventHelper.LogEvent(
                dbLayer,
                enterpriseModel.AccountDatabase.AccountId,
                accessProvider,
                LogActions.ClusteredDatabaseCreationStarting, 
                $"Начинаю создание серверной базы по адресам : {parameters.DatabaseServerName} \n " +
                $"{parameters.ServerName} \n регламентные задания включены : {isCopy}");

             return new Launch1CParamsDto
            {
                ExeParameters = GetCreateConnectionString(parameters),
                Client1CPath = GetPlatformExecPath(),
                LogFilePath = GetLogFilePath(),
            };
        }
        
        private void CheckNullWithLogging<T>(T obj, string propertyName) where T : class
        {
            if (obj == null)
            {
                logger.Warn($"{propertyName} is null");
            }
        }

        /// <summary>
        ///  Метод, который получает из параметров строку для запуска в режиме Design
        /// </summary>
        /// <param name="accountDatabaseId"></param>
        /// <param name="pathToDt"></param>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
         public Launch1CParamsDto GetLaunch1CParamsForDesignMode(Guid accountDatabaseId, string pathToDt, string login, string password)
        {
            var database = dbLayer.DatabasesRepository.GetById(accountDatabaseId); 
            var parameters = new Convert1CdPowershellCommandParameters()
            {
                DtFilePath = pathToDt,
                DatabasePath = pathHelper.GetPath(database),
                UserPassword = password,
                UserName = login,
                LogFilePath = GetLogFilePath(),
            };
            
            return new Launch1CParamsDto
            {
                ExeParameters = GetDesignConnectionString(parameters),
                Client1CPath = GetPlatformExecPath(),
                LogFilePath = GetLogFilePath(),
            };        
        }
        
        
        /// <summary>
        /// Получить строку подключения в режиме конфигурации
        /// </summary>
        /// <param name="accountDatabaseDestinationPath">Путь к инф. базе</param>
        /// <param name="uploadedDtFilePath">Путь к азгруженному файлу Dt</param>
        /// <param name="logFilePath">Путь к лог файлу</param>
        /// <returns>Строка подключения в режиме конфигурации</returns>
        private string GetConfigConnectionString(string accountDatabaseDestinationPath, string uploadedDtFilePath, string logFilePath)
            => $@"CONFIG /F ""{accountDatabaseDestinationPath}"" /RestoreIB ""{uploadedDtFilePath}"" /Out ""{logFilePath}"" 
                    {Start1CCommandLineConstants.DisableStartupMessages}
                    {Start1CCommandLineConstants.DisableStartupDialogs}
                    {Start1CCommandLineConstants.UsePrivilegedMode}
                ";


        private static string GetCreateConnectionString(CreatePowershellCommandParameters parameters) =>
            parameters?.GeneratePowerShellCommand();
        
        private static string GetDesignConnectionString(Convert1CdPowershellCommandParameters parameters) => 
            parameters?.GeneratePowerShellCommand(); 

        /// <summary>
        /// Получить путь к платформе 1С
        /// </summary>
        /// <returns>Путь к платформе 1С</returns>
        private string GetPlatformExecPath()
        {
            var availablePlatforms = dbLayer.PlatformVersionReferencesRepository.All().ToList()
                .OrderByDescending(v => v.Version, new VersionComparer());

            foreach (var platform in availablePlatforms)
            {
                var filePath = Get1СExecutionFilePath(platform.PathToPlatform);

                if (!string.IsNullOrEmpty(filePath))
                    return filePath;
            }

            throw new InvalidOperationException("Не найдено ни одной доступной платформы");
        }

        /// <summary>
        /// Получить путь к исполняемому файлу 1С
        /// </summary>
        /// <param name="pathToPlatform">Путь к платформе</param>
        /// <returns>Путь к исполняемому файлу 1С</returns>
        private string Get1СExecutionFilePath(string pathToPlatform)
        {
            var thickClientExePath = Path.Combine(pathToPlatform, Client1CConstants.ThickNameWithExe);
            var thinClientExePath = Path.Combine(pathToPlatform, Client1CConstants.ThinNameWithExe);

            if (File.Exists(thickClientExePath))
                return thickClientExePath;

            if (File.Exists(thinClientExePath))
                return thinClientExePath;

            return null;
        }

        /// <summary>
        /// Получить путь к файлу лога
        /// </summary>
        /// <returns>Путь к файлу лога</returns>
        private string GetLogFilePath()
        {
            var pathToLogFolder1C = CloudConfigurationProvider.Enterprise1C.GetPathToLogFolder1C();
            return Path.Combine(pathToLogFolder1C, Guid.NewGuid().ToString());
        }
    }
}
