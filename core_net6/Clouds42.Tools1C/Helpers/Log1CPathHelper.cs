﻿using System;
using System.IO;
using Clouds42.Configurations;

namespace Clouds42.Tools1C.Helpers
{
    /// <summary>
    /// Класс для определения файла с логами 1С.
    /// </summary>
    public static class Log1CPathHelper
    {
        /// <summary>
        /// Получить путь до файла с логами 1С по премени.        
        /// </summary>
        public static string GetPathOutByTime(DateTime dateTime, string dbName)
        {
            var timeFormat = dateTime.ToString("yyyy-MM-dd_hh-mm-ss");           
            var sharePath = ConfigurationHelper.GetConfigurationValue("TaSLogsSharePath");

            var folderPath = $@"{sharePath}/{dbName}";

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            var filePath = $@"{folderPath}/{timeFormat}.txt";
            return filePath;
        }

        /// <summary>
        /// Получить путь до файла с логами 1С.        
        /// </summary>
        /// <param name="dbName">Название инф. базы</param>
        /// <param name="logfileName">Название файла лога</param>
        /// <returns>Путь до файла с логами 1С</returns>
        public static string GetPathOutByName(string dbName, string logfileName)
        {
            if (string.IsNullOrEmpty(logfileName))
                throw  new ArgumentException($"Ожидается не пустое значение {logfileName}");

            var folderPath = GetLogFolderPath(dbName);

            var filePath = $@"{folderPath}\{logfileName.Replace(".", "_")}.txt";
            return filePath;
        }

        /// <summary>
        /// Получить путь к файлу лога 1С
        /// </summary>
        /// <param name="dbName">Название инф. базы</param>
        /// <param name="logFileName">Название файла лога</param>
        /// <returns>Путь к файлу лога 1С</returns>
        public static string GetPathForLogFile(string dbName, string logFileName = null)
        {
            var logFolderPath = GetLogFolderPath(dbName);

            logFileName = string.IsNullOrEmpty(logFileName)
                ? $"42clouds_{Guid.NewGuid():N}"
                : logFileName;

            return Path.Combine(logFolderPath, $"{logFileName}.txt");
        }

        /// <summary>
        /// Получить путь к файлу метаданных инф. базы
        /// </summary>
        /// <param name="dbName">Название инф. базы</param>
        /// <returns>Путь к файлу метаданных инф. базы</returns>
        public static string GetPathForMetadataJsonFile(string dbName)
        {
            var logFolderPath = GetLogFolderPath(dbName);

            var metadataFileName = $"42clouds_{Guid.NewGuid():N}.json";
            return Path.Combine(logFolderPath, metadataFileName);
        }

        /// <summary>
        /// Получить путь к папке логов
        /// </summary>
        /// <param name="dbName">Номер инф. базы</param>
        /// <returns>Путь к папке логов</returns>
        private static string GetLogFolderPath(string dbName)
        {
            dbName = dbName
                .Replace(" ", "_")
                .Replace(".", "_");
            var sharePath = ConfigurationHelper.GetConfigurationValue("TaSLogsSharePath");

            var logFolderPath = Path.Combine(sharePath, dbName);

            if (!Directory.Exists(logFolderPath))
                Directory.CreateDirectory(logFolderPath);

            return logFolderPath;
        }
    }
}
