﻿using System;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.DataContracts.Connectors1C.DbConnector;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.Tools1C.Helpers
{
    /// <summary>
    /// Хэлпер для создания параметров подлкючения через COM объект
    /// </summary>
    public static class Connector1CParamsCreator
    {
        /// <summary>
        /// Создать модель параметров подключения к файловой базе
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Модель параметров подключения к файловой базе</returns>
        public static FileDatabaseComConnectionParamsDto CreateParamsForConnectToFileDatabase(IUpdateDatabaseDto database)
            => new()
            {
                V82Name = database.UpdateDatabaseName,
                Login = database.AdminLogin,
                Password = database.AdminPassword,
                FilePath = database.ConnectionAddress,
                PlatformVersion = database.PlatformVersion,
                PlatformType = database.PlatformType,
                PathToPlatform = database.PlatformType == PlatformType.V82 ? Configurations.Configurations.CloudConfigurationProvider.Enterprise1C.Get1C82ExecutionPath() : database.FolderPath1C
            };

        /// <summary>
        /// Создать модель параметров подключения к серверной базе
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Модель параметров подключения к серверной базе</returns>
        public static ServerDatabaseComConnectionParamsDto CreateParamsForConnectToServerDatabase(IUpdateDatabaseDto database)
            => new()
            {
                V82Name = database.UpdateDatabaseName,
                Login = database.AdminLogin,
                Password = database.AdminPassword,
                PlatformVersion = database.PlatformVersion,
                PlatformType = database.PlatformType,
                DbName = database.UpdateDatabaseName,
                ServerEnterprise = database.ConnectionAddress,
                PathToPlatform = database.PlatformType == PlatformType.V82 ? Configurations.Configurations.CloudConfigurationProvider.Enterprise1C.Get1C82ExecutionPath() : database.FolderPath1C
            };

        /// <summary>
        /// Создать параметры подключения к кластеру 1С
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база с данными о кластере предприятия аккаунта</param>
        /// <returns>Параметры подключения к кластеру 1С</returns>
        public static ClusterParametersDto CreateClusterConnectionParameters(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
            => new()
            {
                ConnectionAddress = accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress,
                AdminName = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                AdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword,
                PlatformVersion = accountDatabaseEnterprise.AccountDatabase.PlatformType == PlatformType.V82 
                    ? accountDatabaseEnterprise.Segment.Stable82PlatformVersionReference.Version
                    : accountDatabaseEnterprise.Segment.Stable83PlatformVersionReference.Version,
                PlatformType = accountDatabaseEnterprise.AccountDatabase.PlatformType,
                PathToPlatform = GetPathToPlatform(accountDatabaseEnterprise)
            };

        /// <summary>
        /// Получить путь к платформе 1С
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Информационная база с данными о кластере предприятия аккаунта</param>
        /// <returns>Путь к платформе 1С</returns>
        private static string GetPathToPlatform(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            if (accountDatabaseEnterprise.AccountDatabase.PlatformType == PlatformType.V82)
            {
                if (string.IsNullOrEmpty(accountDatabaseEnterprise.Segment.Stable82PlatformVersionReference
                    .PathToPlatform))
                    throw new InvalidOperationException(
                        $"У сегмента '{accountDatabaseEnterprise.Segment.Name}' :: '{accountDatabaseEnterprise.Segment.Description}' не задан путь до 1С 8.2");

                return accountDatabaseEnterprise.Segment.Stable82PlatformVersionReference.PathToPlatform;
            }

            if (accountDatabaseEnterprise.AccountDatabase.PlatformType != PlatformType.V83)
                throw new InvalidOperationException(
                    $"Для типа платформы {accountDatabaseEnterprise.AccountDatabase.PlatformType} не определено откуда брать путь до 1С.");

            if (accountDatabaseEnterprise.AccountDatabase.DistributionType == DistributionType.Alpha.ToString())
            {
                if (string.IsNullOrEmpty(accountDatabaseEnterprise.Segment.Alpha83PlatformVersionReference
                    .PathToPlatform))
                    throw new InvalidOperationException(
                        $"У сегмента '{accountDatabaseEnterprise.Segment.Name}'::'{accountDatabaseEnterprise.Segment.Description}' не задан путь до альфа 1С 8.3");

                return accountDatabaseEnterprise.Segment.Alpha83PlatformVersionReference.PathToPlatform;
            }

            if (string.IsNullOrEmpty(accountDatabaseEnterprise.Segment.Stable83PlatformVersionReference.PathToPlatform))
                throw new InvalidOperationException(
                    $"У сегмента '{accountDatabaseEnterprise.Segment.Name}'::'{accountDatabaseEnterprise.Segment.Description}' не задан путь до стабильной 1С 8.3");

            return accountDatabaseEnterprise.Segment.Stable83PlatformVersionReference.PathToPlatform;
        }
    }
}
