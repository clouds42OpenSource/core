﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Connectors1C.DbConnector;

namespace Clouds42.Tools1C.Helpers
{
    public class Connector1CErrorParser
    {

        public class ErrorModel
        {
            public ConnectCodeDto ConnectCode { get; set; }
            public string ErorMessage { get; set; }
        }

        private static readonly Dictionary<ConnectCodeDto, List<string>> LogDictionary = new()
        {
            {
                ConnectCodeDto.DatabaseDamaged, [
                    "файл базы данных поврежден"
                ]
            },
            {
                ConnectCodeDto.RegistrationLogIncorrect, ["файла журнала регистрации"]
            },
            {
                ConnectCodeDto.PasswordOrLoginIncorrect, [
                    "идентификация пользователя не выполнена",
                    "неправильное имя или пароль пользователя",
                    "(Исключение из HRESULT: 0x80010105 (RPC_E_SERVERFAULT))",
                    "0x80010105 (RPC_E_SERVERFAULT)",
                    "HRESULT: 0x80010105 (RPC_E_SERVERFAULT))"
                ]
            }
        };

        /// <summary>
        /// Провести анализ сообщения об ошибке.
        /// </summary>  
        public static ErrorModel Parse(Exception ex)
        {
            return Parse($"{ex.Message} {ex.InnerException?.Message}");
        }

        /// <summary>
        /// Провести анализ сообщения об ошибке.
        /// </summary>  
        public static ErrorModel Parse(string errorMessage)
        {
            if (MessageContainsErrorCode(errorMessage, ConnectCodeDto.DatabaseDamaged))
                return new ErrorModel
                {
                    ConnectCode = ConnectCodeDto.DatabaseDamaged,
                    ErorMessage = errorMessage
                };
            
            if (MessageContainsErrorCode(errorMessage, ConnectCodeDto.PasswordOrLoginIncorrect))
                return new ErrorModel
                {
                    ConnectCode = ConnectCodeDto.PasswordOrLoginIncorrect,
                    ErorMessage = errorMessage
                };
            if (MessageContainsErrorCode(errorMessage, ConnectCodeDto.RegistrationLogIncorrect))
                return new ErrorModel
                {
                    ConnectCode = ConnectCodeDto.RegistrationLogIncorrect,
                    ErorMessage = errorMessage
                };

            return new ErrorModel
            {
                ConnectCode = ConnectCodeDto.ConnectError,
                ErorMessage = errorMessage
            };
        }

        /// <summary>
        /// Текст ошибки содержит коды ошибки.
        /// </summary>        
        private static bool MessageContainsErrorCode(string message, ConnectCodeDto connectCode)
        {

            if (!LogDictionary.ContainsKey(connectCode))
                return false;

            var logDictionary = LogDictionary[connectCode];

            foreach (var val in logDictionary)
            {
                if (message.IndexOf(val, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
            }
            return false;
        }

    }
}
