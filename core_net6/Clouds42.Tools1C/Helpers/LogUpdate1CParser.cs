﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Clouds42.Common.Extensions;

namespace Clouds42.Tools1C.Helpers
{

    /// <summary>
    /// Парсер лога обновления 1С. 
    /// </summary>
    public class LogUpdate1CParser
    {
        public enum LogState
        {

            /// <summary>
            /// Успешное обновлене.
            /// </summary>
            Success,

            /// <summary>
            /// База содержит доработки.
            /// </summary>
            DbHasModification,

            /// <summary>
            /// Ошибки обновлений.
            /// </summary>
            Error,

            /// <summary>
            /// Поврежден пакет обновлений.
            /// </summary>
            CfuDamaged,

            /// <summary>
            /// В инф. базе есть активные сеансы
            /// </summary>
            AcDbHasActiveSessions,

            /// <summary>
            /// Не верные авторизационные данные
            /// </summary>
            PasswordOrLoginIncorrect,

            /// <summary>
            /// Не найдена лицензия
            /// </summary>
            ErrorNoLisenseFound,

            /// <summary>
            /// Не хватает места на диске
            /// </summary>
            ErrorNotEnoughSpace
        }

        /// <summary>
        /// Результат анализа лога 1С.
        /// </summary>
        public class Result
        {
            /// <summary>
            /// Результат операции обновления.
            /// </summary>
            public LogState LogState { get; set; }

            /// <summary>
            /// Прочитанные данные из лога.
            /// </summary>
            public string[] Lines { get; set; }

            public override string ToString()
            {
                if (Lines == null)
                    return string.Empty;

                var st = new StringBuilder();

                foreach (var line in Lines)
                {
                    st.AppendLine(line);
                }
                return st.ToString();
            }
        }

        private readonly Dictionary<LogState, List<string>> _logDictionary = new()
        {
            {
                LogState.DbHasModification, [
                    "база модифицированна",
                    "база доработана",
                    "невозможно выполнение обновления конфигурации в командном режиме",
                    "Изменение конфигурации запрещено"
                ]
            },
            {
                LogState.Error, [
                    "критическая ошибка",
                    "системная ошибка",
                    "непредвиденная ошибка",
                    "ошибка загрузки компоненты"
                ]
            },
            {
                LogState.CfuDamaged, [
                    "неверный формат хранилища данных",
                    "файл не содержит доступных обновлений"
                ]
            },
            {
                LogState.AcDbHasActiveSessions, [
                    "Ошибка исключительной блокировки информационной базы",
                    "Активны сеансы:"
                ]
            },
            {
                LogState.PasswordOrLoginIncorrect, ["Пользователь ИБ не идентифицирован"]
            },
            {
                LogState.ErrorNoLisenseFound, [
                    "Не найдена лицензия",
                    "Не обнаружен ключ защиты программы"
                ]
            },
            {
                LogState.ErrorNotEnoughSpace, [
                    "Недостаточно места",
                    "загружены не все данные"
                ]
            }
        };

        /// <summary>
        /// Провести анализ лога.
        /// </summary>
        /// <param name="logPath">Путь к файлу лога</param>
        /// <returns>Результат анализа</returns>
        public Result Parse(string logPath, int exitCode = 0 )
        {
            var logContent = ReadLogFile(logPath);

            foreach (var logLine in logContent)
            {
                var handleResult = HandleLogLine(logLine, logContent);
                if (handleResult == null)
                    continue;

                return handleResult;
            }
            if(exitCode != 0)
            {
                return new Result
                {
                    Lines = logContent,
                    LogState = LogState.Error
                };
            }

            return new Result
            {
                Lines = logContent,
                LogState = LogState.Success
            };
        }

        /// <summary>
        /// Обработать строку лога
        /// </summary>
        /// <param name="logLine">Строка лога</param>
        /// <param name="logContent">Содержимое лог файла</param>
        /// <returns>Результат обработки</returns>
        private Result HandleLogLine(string logLine, string[] logContent)
        {
            foreach (LogState logState in Enum.GetValues(typeof(LogState)))
            {
                if (logState == LogState.Success)
                    continue;

                if (LineHasIncludes(logLine, logState))
                    return new Result
                    {
                        Lines = logContent,
                        LogState = logState
                    };
            }

            return null;
        }

        /// <summary>
        /// Линия содержит вхождения по состоянию.
        /// </summary>        
        private bool LineHasIncludes(string line, LogState logState)
        {

            if (!_logDictionary.ContainsKey(logState))
                return false;

            var logDictionary = _logDictionary[logState];

            foreach (var val in logDictionary)
            {
                if (line.IndexOf(val, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Прочитать данные с диска.
        /// </summary>
        private string[] ReadLogFile(string filePath)
        {
            if (!File.Exists(filePath))
                return new List<string>().ToArray();
            string[] res;

            try
            {
                res= File.ReadAllLines(filePath, Encoding.GetEncoding("Windows-1251"));
            }
            catch (Exception e)
            {
                res = [$"В процессе чтения \"{filePath}\" произошла системная ошибка: {e.GetFullInfo()}"];
            }

            return res;
        }

    }
}
