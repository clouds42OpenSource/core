﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.Enums._1C;
using Microsoft.Win32;

namespace Clouds42.Tools1C.Helpers
{
    /// <summary>
    /// Регистратор для COM коннекторов
    /// </summary>
    public class ComConnectorRegistrar
    {
        private readonly Lazy<string> _1CComLibraryName = new(CloudConfigurationProvider.Enterprise1C.Get1CComLibraryName);
        private readonly Lazy<string> _regSvrProcessName = new(CloudConfigurationProvider.Enterprise1C.GetRegSvrProcessName);
        private readonly Lazy<string> _defaultProgramIdForV83ComConnector = new(CloudConfigurationProvider.Enterprise1C.GetDefaultProgramIdForV83ComConnector);
        private readonly Lazy<string> _defaultProgramIdForV82ComConnector = new(CloudConfigurationProvider.Enterprise1C.GetDefaultProgramIdForV82ComConnector);
        private readonly Lazy<string> _registryWow6432NodeName = new(CloudConfigurationProvider.Enterprise1C.GetRegistryWow6432NodeName);
        private readonly Lazy<string> _registryAppIdNodeName = new(CloudConfigurationProvider.Enterprise1C.GetRegistryAppIdNodeName);
        private readonly Lazy<string> _registryDllSurrogateKeyName = new(CloudConfigurationProvider.Enterprise1C.GetRegistryDllSurrogateKeyName);
        private readonly Lazy<string> _registryClsIdNodeName = new(CloudConfigurationProvider.Enterprise1C.GetRegistryClsIdNodeName);
        private readonly Lazy<string> _registrySoftwareNodeName = new(CloudConfigurationProvider.Enterprise1C.GetRegistrySoftwareNodeName);
        private readonly Lazy<string> _registryClassesNodeName = new(CloudConfigurationProvider.Enterprise1C.GetRegistryClassesNodeName);
        private readonly Lazy<string> _dllHostProcessName = new(CloudConfigurationProvider.Enterprise1C.GetDllHostProcessName);


        /// <summary>
        /// Получить экземпляр COM коннектора
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        /// <returns>Экземпляр COM коннектора</returns>
        public dynamic GetComConnectorInstance(IPlatformDto parameters)
        {
            try
            {
                if (NeedToRegisterComObject(parameters))
                    RegisterNewComConnector(parameters);

                var comConnectorType = GetComConnectorType(parameters);
                var connectorInstance = Activator.CreateInstance(comConnectorType);
                return connectorInstance;
            }
            catch (Exception)
            {
                DeleteComConnectorRegistration(parameters);
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать новый COM коннектор
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        private void RegisterNewComConnector(IPlatformDto parameters)
        {
            DeleteComConnectorRegistration(parameters);
            KillComSurrogateProcess();
            RegisterComConnector(parameters);
            CreateRegistryKeysForComConnector(parameters);
        }

        /// <summary>
        /// Удалить регистрацию COM коннектора
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        public void DeleteComConnectorRegistration(IPlatformDto parameters)
        {
            var commandParams = $"/s /u \"{GetComDllPath(parameters)}\"";
            RunRegSvrProcess(commandParams);
        }

        /// <summary>
        /// Зарегистировать COM коннектор
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        private void RegisterComConnector(IPlatformDto parameters)
        {
            var commandParams = $"/s \"{GetComDllPath(parameters)}\"";
            RunRegSvrProcess(commandParams);
        }

        /// <summary>
        /// Получить путь к библиотеке COM от 1С
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        private string GetComDllPath(IPlatformDto parameters)
        {
            if(string.IsNullOrEmpty(parameters.PathToPlatform))
                throw new InvalidOperationException("В модели параметров не указан путь к платформе");

            return Path.Combine(parameters.PathToPlatform, _1CComLibraryName.Value);
        }

        /// <summary>
        /// Запустить процесс regsvr32
        /// </summary>
        /// <param name="parameters">Параметры процесса</param>
        private void RunRegSvrProcess(string parameters)
        {
            var registrationProcess = new Process
            {
                StartInfo =
                {
                    FileName = _regSvrProcessName.Value,
                    Arguments = parameters,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    ErrorDialog = false
                }
            };

            registrationProcess.Start();
            registrationProcess.WaitForExit(180000);

            if (registrationProcess.HasExited)
                return;

            var message = "Принудительное завершения процесса regsvr32.";
            registrationProcess.Kill();
            throw new InvalidOperationException(message);
        }

        /// <summary>
        /// Признак необходимости регистрировать ком объект
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        /// <returns>true - если в реестре прописан путь к comcntr.dll другой версии платформы</returns>
        private bool NeedToRegisterComObject(IPlatformDto parameters)
        {
            try
            {
                var comConnectorType = GetComConnectorType(parameters);

                if (comConnectorType == null)
                    return true;

                var comConnectorId = comConnectorType.GUID;

                var comConnectorRegistryKey = Registry.LocalMachine.OpenSubKey(_registrySoftwareNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                    ?.OpenSubKey(_registryClassesNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                    ?.OpenSubKey(_registryWow6432NodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                    ?.OpenSubKey(_registryClsIdNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                    ?.OpenSubKey(comConnectorId.ToString("B"), RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                    ?.OpenSubKey("InprocServer32", RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl);

                if (comConnectorRegistryKey == null)
                    return true;

                var currentComConnectorPath = GetComDllPath(parameters);
                var existingComConnectorPath = comConnectorRegistryKey.GetValue("").ToString();

                return !existingComConnectorPath.Equals(currentComConnectorPath, StringComparison.InvariantCultureIgnoreCase);
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Получить тип ком коннектора
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        /// <returns>Тип ком коннектора</returns>
        private Type GetComConnectorType(IPlatformDto parameters)
        {
            var comConnectorName = parameters.PlatformType == PlatformType.V83
                ? _defaultProgramIdForV83ComConnector.Value
                : _defaultProgramIdForV82ComConnector.Value;

            return Type.GetTypeFromProgID(comConnectorName);
        }

        /// <summary>
        /// Создать значения в реестре для COM коннектора
        /// </summary>
        /// <param name="parameters">Модель параметров подключения к инф. базе</param>
        private void CreateRegistryKeysForComConnector(IPlatformDto parameters)
        {
            var comConnectorName = parameters.PlatformType == PlatformType.V83
                ? _defaultProgramIdForV83ComConnector.Value
                : _defaultProgramIdForV82ComConnector.Value;

            var comConnectorType = Type.GetTypeFromProgID(comConnectorName);

            var comConnectorId = comConnectorType.GUID;

            CreateClassesRootComConnectorAppIdValue(comConnectorId);
            CreateClassesRootAppIdSubKey(comConnectorId);
            CreateLocalMachineAppIdSubKey(comConnectorId);
        }

        /// <summary>
        /// Создать значение AppID в разделе COM коннектора
        /// </summary>
        /// <param name="comConnectorId">ID COM коннектора</param>
        private void CreateClassesRootComConnectorAppIdValue(Guid comConnectorId)
        {
            var comConnectorRecord = GetClassesRootComConnectorTree(comConnectorId);

            if (comConnectorRecord.GetValue(_registryAppIdNodeName.Value) != null)
            {
                comConnectorRecord.DeleteValue(_registryAppIdNodeName.Value);
            }

            comConnectorRecord.SetValue(_registryAppIdNodeName.Value, comConnectorId.ToString("B"), RegistryValueKind.String);
            comConnectorRecord.Close();
        }

        /// <summary>
        /// Создать ключ AppID в разделе HKEY_CLASSES_ROOT\Wow6432Node\\AppID
        /// </summary>
        /// <param name="comConnectorId">ID COM коннектора</param>
        private void CreateClassesRootAppIdSubKey(Guid comConnectorId)
        {
            var classesRootAppIdTree = GetClassesRootAppIdTree();
            if (classesRootAppIdTree.OpenSubKey(comConnectorId.ToString("B")) != null)
            {
                classesRootAppIdTree.DeleteSubKey(comConnectorId.ToString("B"));
            }

            var classesRootAppIdSubKey = classesRootAppIdTree.CreateSubKey(comConnectorId.ToString("B"))
                                         ?? throw new InvalidOperationException(
                                             $"Ошибка при создании значения рееста по пути HKEY_CLASSES_ROOT\\Wow6432Node\\AppID\\{comConnectorId:B}");

            classesRootAppIdSubKey.SetValue(_registryDllSurrogateKeyName.Value, "", RegistryValueKind.String);
            classesRootAppIdSubKey.Close();
        }

        /// <summary>
        /// Создать ключ AppID в разделе HKEY_LOCAL_MACHINE\\Software\\Classes\\AppID
        /// </summary>
        /// <param name="comConnectorId">ID COM коннектора</param>
        private void CreateLocalMachineAppIdSubKey(Guid comConnectorId)
        {
            var localMachineAppIdTree = GetLocalMachineAppIdTree();

            if (localMachineAppIdTree.OpenSubKey(comConnectorId.ToString("B")) != null)
            {
                localMachineAppIdTree.DeleteSubKey(comConnectorId.ToString("B"));
            }

            var localMachineAppIdSubKey = localMachineAppIdTree.CreateSubKey(comConnectorId.ToString("B"))
                                          ?? throw new InvalidOperationException(
                                              $"Ошибка при создании значения рееста по пути HKEY_LOCAL_MACHINE\\Software\\Classes\\AppID\\{comConnectorId:B}");

            localMachineAppIdSubKey.SetValue(_registryDllSurrogateKeyName.Value, "", RegistryValueKind.String);
            localMachineAppIdSubKey.Close();
        }

        /// <summary>
        /// Получить раздел реестра HKEY_CLASSES_ROOT\Wow6432Node
        /// </summary>
        /// <returns>Раздел реестра HKEY_CLASSES_ROOT\Wow6432Node</returns>
        private RegistryKey GetClassesRootTree()
            => Registry.ClassesRoot.OpenSubKey(_registryWow6432NodeName.Value,
                   RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
               ?? throw new InvalidOperationException("Не найден раздел реестра HKEY_CLASSES_ROOT\\Wow6432Node");

        /// <summary>
        /// Получить раздел реестра для COM коннектора
        /// </summary>
        /// <param name="comConnectorId">ID COM коннектора</param>
        /// <returns>Раздел реестра для COM коннектора</returns>
        private RegistryKey GetClassesRootComConnectorTree(Guid comConnectorId)
            => GetClassesRootTree()
                   .OpenSubKey(_registryClsIdNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                   ?.OpenSubKey(comConnectorId.ToString("B"), RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
               ?? throw new InvalidOperationException($"Не создан раздел реестра для COM коннектора {comConnectorId}");

        /// <summary>
        /// Получить раздел реестра HKEY_CLASSES_ROOT\Wow6432Node\AppID
        /// </summary>
        /// <returns>Раздел реестра HKEY_CLASSES_ROOT\Wow6432Node\AppID</returns>
        private RegistryKey GetClassesRootAppIdTree()
            => GetClassesRootTree().OpenSubKey(_registryAppIdNodeName.Value, true)
               ?? throw new InvalidOperationException("Не найден раздел реестра HKEY_CLASSES_ROOT\\Wow6432Node\\AppID");

        /// <summary>
        /// Получить раздел реестра HKEY_LOCAL_MACHINE\Software\Classes\AppID
        /// </summary>
        /// <returns>Раздел реестра HKEY_LOCAL_MACHINE\Software\Classes\AppID</returns>
        private RegistryKey GetLocalMachineAppIdTree()
            => Registry.LocalMachine.OpenSubKey(_registrySoftwareNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                   ?.OpenSubKey(_registryClassesNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
                   ?.OpenSubKey(_registryAppIdNodeName.Value, RegistryKeyPermissionCheck.ReadWriteSubTree, RegistryRights.FullControl)
               ?? throw new InvalidOperationException("Не найден раздел реестра HKEY_LOCAL_MACHINE\\Software\\Classes\\AppID");

        /// <summary>
        /// Убить процессы COM Surrogate
        /// </summary>
        private void KillComSurrogateProcess()
        {
            var comSurrogateProcesses = Process.GetProcesses().Where(pr =>
                pr.ProcessName.Equals(_dllHostProcessName.Value, StringComparison.InvariantCultureIgnoreCase));

            comSurrogateProcesses.ToList().ForEach(process =>
            {
                try
                {
                    process.Kill();
                }
                catch (Exception)
                {
                   //ignore
                }
            });
        }
    }
}
