﻿namespace Clouds42.Tools1C.Helpers
{
    /// <summary>
    /// Константы строки запуска 1С
    /// </summary>
    public static class Start1CCommandLineConstants
    {
        /// <summary>
        /// Отключить диалоговые окна при запуске
        /// </summary>
        public const string DisableStartupDialogs = "/DisableStartupDialogs";

        /// <summary>
        /// Отключить информационные сообщения при запуске
        /// </summary>
        public const string DisableStartupMessages = "/DisableStartupMessages";

        /// <summary>
        /// Использовать привелигированный режим запуска
        /// ВНИМАНИЕ: работает только при указании логина/пароля пользователя с полными правами
        /// </summary>
        public const string UsePrivilegedMode = "/UsePrivilegedMode";
    }
}
