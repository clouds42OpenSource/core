﻿using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Cp.Helpers
{
    public class ControlPanelAccessProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IHttpContextAccessor httpContextAccessor,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;

        private const string SelectedContextAccount = nameof(SelectedContextAccount);
        private HttpContext _context;
        public override string Name => httpContextAccessor?.HttpContext?.User?.Identity?.Name;

        protected override string GetUserIdentity()
            => Name;

        public override IUserPrincipalDto GetUser()
        {
            return GetUser(Name ?? _context?.User?.Identity?.Name);
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new NotImplementedException();
        }

        public override List<string> GetEmailAdmin()
        {
           var user = GetUser();

           var accAdminsEmail = _dbLayer.AccountsRepository.GetEmails(user.ContextAccountId);
           return accAdminsEmail;
        }
        public override string GetUserAgent()
        {
           return httpContextAccessor?.HttpContext?.Request?.Headers["User-Agent"].ToString() ?? _context?.Request?.Headers["User-Agent"].ToString();
        }

        public override string GetUserHostAddress()
        {
            return httpContextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString() ?? _context?.Connection?.RemoteIpAddress?.ToString();
        }
        public override bool SetContextAccount(Guid accountId)
        {

            var byId = _dbLayer.AccountsRepository.GetById(accountId);
            if (byId == null) return false;
            var cookieOption = new CookieOptions { Expires = DateTime.Now.AddDays(1), HttpOnly = true, Secure = true, Domain = ".42clouds.com", };
            if (httpContextAccessor.HttpContext != null)
            {
                httpContextAccessor.HttpContext?.Response.Cookies
                    .Append(
                        SelectedContextAccount,
                        accountId.ToString(),
                        cookieOption
                    );

            }
            else
            {
                _context.Response?.Cookies.Append(SelectedContextAccount, accountId.ToString(), cookieOption);
            }


            return true;
        }

        public override void SetCurrentContextAccount(IResponseCookies cookies)
        {
            cookies.Delete(SelectedContextAccount, new CookieOptions { Expires = DateTime.Now.AddDays(-1), HttpOnly = true, Secure = true, Domain = ".42clouds.com", });
        }

        public override bool IsCurrentContextAccount()
        {
            var exist = httpContextAccessor.HttpContext?.Request.Cookies[SelectedContextAccount] ?? _context?.Request?.Cookies[SelectedContextAccount];
            return exist == null || string.IsNullOrEmpty(exist);
        }

        private IUserPrincipalDto GetUser(string login)
        {

            if (string.IsNullOrEmpty(login))
                return null;
            
            var accountUser = _dbLayer.AccountUsersRepository.FirstOrDefault(a => a.Login == login);

            if (accountUser == null)
            {
                return null;
            }

            var contextAccountId = GetContextAccountId(httpContextAccessor.HttpContext?.Request.Cookies ?? _context?.Request?.Cookies);
            var userGroups = accountUser.AccountUserRoles.Select(s => s.AccountUserGroup).ToList();

            return new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                RequestAccountId = accountUser.AccountId,
                Token = Guid.Empty,
                Groups = userGroups,
                Name = accountUser.Login,
                PhoneNumber = accountUser.PhoneNumber,
                Email = accountUser.Email,
                ContextAccountId = contextAccountId ?? accountUser.AccountId
            };

        }

        public override Guid? GetContextAccountId(IRequestCookieCollection cookie)
        {
            var exist = cookie[SelectedContextAccount];
            var guid = !string.IsNullOrEmpty(exist)
                ? Guid.Parse(exist)
                : (Guid?) null;
            return guid;
        }

        public override void SetHttpContext(HttpContext context)
        {
            _context = context;
        }

    }
}
