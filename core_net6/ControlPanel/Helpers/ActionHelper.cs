﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Cp.Helpers
{
    public static class ActionHelper
    {
        public static UrlHelper HtmlActionLink(ActionContext context) => new(context);
    }
}
