﻿using System.Net;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Configurations;
using Clouds42.CoreWorkerTask;
using Clouds42.DomainContext;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.RuntimeContext;
using Clouds42.TelegramBot;
using Cp.Infrastructure.Authentication;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.WebEncoders;

namespace Cp
{
    public class Startup(IWebHostEnvironment env)
    {
        public IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", true, true)
            .AddGcpKeyValueSecrets(env.EnvironmentName)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddEnvironmentVariables()
            .Build();

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddClouds42DbContext(Configuration)
                .AddHttpContextAccessor()
                .AddSingleton(Configuration)
                .AddTransient<ICoreExecutionContext, CpCoreExecutionContext>()
                .AddServices(Configuration)
                .AddSerilogWithElastic(Configuration)
                .AddTelegramBots(Configuration)
                .AddAutoMapper(Assembly.GetExecutingAssembly())
                .AddDistributedIsolatedExecutor(
                    new DbObjectLockProvider(Configuration.GetConnectionString("DefaultConnection"), Configuration.GetSection("ConnectionStrings:DatabaseType").Value))
                .AddHttpClient<IisHttpClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    var login = Configuration.GetRequiredSection("IISConfiguration:Login")?.Value;
                    var password = Configuration.GetRequiredSection("IISConfiguration:Password")?.Value;
                    var domain = Configuration.GetRequiredSection("IISConfiguration:Domain")?.Value;

                    return new HttpClientHandler
                    {
                        UseDefaultCredentials = false,
                        ServerCertificateCustomValidationCallback =
                            HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                        Credentials = new NetworkCredential(login, password, domain),
                        PreAuthenticate = true
                    };
                });

            services
                .Configure<WebEncoderOptions>(options =>
                {
                    options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All);
                })
                .AddMvc(options =>
                {
                    options.EnableEndpointRouting = false;
                    options.Filters.Add(new AuthorizeFilter());
                })
                .AddNewtonsoftJson(options =>
                {
                    options.UseMemberCasing();
                });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, opt =>
                {
                    opt.LoginPath = "/signin";
                    opt.TicketDataFormat = new MachineKeySecureDataFormat(
                            decryptionKey: Configuration["MachineKey:DecryptionKey"],
                            validationKey: Configuration["MachineKey:ValidationKey"]);
                    opt.ExpireTimeSpan = TimeSpan.FromDays(1);
                    opt.Cookie.HttpOnly = true;
                })
                .AddClouds42Jwt(Clouds42JwtAuthenticationDefaults.JwtAuthenticationScheme);
            services.AddCors();
            services.AddAuthorization(options =>
            {
                var defaultSchemes = new[] {
                    Clouds42JwtAuthenticationDefaults.JwtAuthenticationScheme,
                    CookieAuthenticationDefaults.AuthenticationScheme
                };

                options.DefaultPolicy = new AuthorizationPolicyBuilder(defaultSchemes)
                    .RequireAuthenticatedUser()
                    .Build();
            });
            services.AddSession();
          
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            Logger42.Init(app.ApplicationServices);
            HandlerException42.Init(app.ApplicationServices);
            ConfigurationHelper.SetConfiguration(app.ApplicationServices.GetRequiredService<IConfiguration>());

            app.UseExceptionHandler(exceptionHandlerApp =>
            {
                exceptionHandlerApp.Run(context =>
                {
                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();
                    var exceptionHandler = app.ApplicationServices.GetRequiredService<IHandlerException>();
                    exceptionHandler.Handle(exceptionHandlerPathFeature.Error, $"Необработанное исключение. {exceptionHandlerPathFeature.Path} {exceptionHandlerPathFeature.Endpoint?.DisplayName}");

                    return Task.CompletedTask;
                });
            });


            //app.UseWebOptimizer();
            app.UseCors(op =>
            {
                op.AllowAnyHeader();
                op.AllowAnyOrigin();
                op.AllowAnyMethod();
            });
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseRouting();
            app.UseSession();
            var path = Path.Combine(env.ContentRootPath, "downloads");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(path),
                RequestPath = "/downloads"
            });
            app.UseMvc(route =>
            {
                route.AddRoutes();
            });
        }
    }
}
