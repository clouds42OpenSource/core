﻿using System.Text.Encodings.Web;
using Clouds42.Jwt.Contracts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;

namespace Cp.Infrastructure.Authentication
{
    public static class Clouds42JwtAuthenticationDefaults
    {
        public const string JwtAuthenticationScheme = "Jwt42Clouds";
    }
    public static class Clouds42JwtAuthentication
    {   
        public static AuthenticationBuilder AddClouds42Jwt(this AuthenticationBuilder builder,
            string scheme,
            Action<Clouds42JwtAuthenticationHandlerOptions> func = null)
        {
            builder.AddScheme<Clouds42JwtAuthenticationHandlerOptions, Clouds42JwtAuthenticationHandler>(scheme, func);
            return builder;
        }
    }
    public class Clouds42JwtAuthenticationHandler(
        IOptionsMonitor<Clouds42JwtAuthenticationHandlerOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock)
        : AuthenticationHandler<Clouds42JwtAuthenticationHandlerOptions>(options, logger, encoder, clock)
    {
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var authorizationHeaders = Context.Request.Headers.Authorization;
            if (!authorizationHeaders.Any())
                return await Task.FromResult(AuthenticateResult.NoResult());

            var jwtWorker = Context.RequestServices.GetRequiredService<IJwtWorker>();

            foreach (var jsonWebToken in from authorizationHeader in authorizationHeaders where !string.IsNullOrEmpty(authorizationHeader) && authorizationHeader.ToLower().StartsWith("bearer ") select authorizationHeader.Split(
                         [" "], StringSplitOptions.RemoveEmptyEntries)[1])
            {
                try
                {
                    var claimsPrincipal = await jwtWorker.ValidateJsonWebTokenAsync(jsonWebToken);

                    return await Task.FromResult(AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name)));
                }

                catch
                {
                    // ignored
                }
            }
            return await Task.FromResult(AuthenticateResult.NoResult());
        }
    }
    public class Clouds42JwtAuthenticationHandlerOptions : AuthenticationSchemeOptions { }
}
