﻿using System.Security.Cryptography;
using System.Text;
using AspNetCore.LegacyAuthCookieCompat;
using Microsoft.AspNetCore.Authentication;

namespace Cp.Infrastructure.Authentication
{
    /// <summary>
    /// Based on the implementation from AspNetCore.LegacyAuthCookieCompat
    /// </summary>
    public class MachineKeyEncryptor
    {
        static class KeyDerivationHelper
        {
            private static readonly UTF8Encoding SecureUtf8Encoding = new(encoderShouldEmitUTF8Identifier: false, throwOnInvalidBytes: true);

            public static byte[] DeriveKey(byte[] keyDerivationKey, CompatibilityMode compatibilityMode)
            {
                if (compatibilityMode == CompatibilityMode.Framework20SP2)
                {
                    return keyDerivationKey;
                }

                using HMACSHA512 hmac = new HMACSHA512(keyDerivationKey);
                GetKeyDerivationParameters(out var label, out var context);
                return DeriveKeyImpl(hmac, label, context, keyDerivationKey.Length * 8);
            }

            private static void GetKeyDerivationParameters(out byte[] label, out byte[] context)
            {
                label = SecureUtf8Encoding.GetBytes("FormsAuthentication.Ticket");
                using MemoryStream memoryStream = new MemoryStream();
                using (new BinaryWriter(memoryStream, SecureUtf8Encoding))
                {
                    context = memoryStream.ToArray();
                }
            }

            private static byte[] DeriveKeyImpl(HMAC hmac, byte[] label, byte[] context, int keyLengthInBits)
            {
                int num = label?.Length ?? 0;
                int num2 = context?.Length ?? 0;
                checked
                {
                    byte[] array = new byte[4 + num + 1 + num2 + 4];
                    if (num != 0)
                    {
                        Buffer.BlockCopy(label, 0, array, 4, num);
                    }

                    if (num2 != 0)
                    {
                        Buffer.BlockCopy(context, 0, array, 5 + num, num2);
                    }

                    WriteUInt32ToByteArrayBigEndian((uint)keyLengthInBits, array, 5 + num + num2);
                    int num3 = 0;
                    int num4 = unchecked(keyLengthInBits / 8);
                    byte[] array2 = new byte[num4];
                    uint num5 = 1u;
                    while (num4 > 0)
                    {
                        WriteUInt32ToByteArrayBigEndian(num5, array, 0);
                        byte[] array3 = hmac.ComputeHash(array);
                        int num6 = Math.Min(num4, array3.Length);
                        Buffer.BlockCopy(array3, 0, array2, num3, num6);
                        num3 += num6;
                        num4 -= num6;
                        num5++;
                    }

                    return array2;
                }
            }

            private static void WriteUInt32ToByteArrayBigEndian(uint value, byte[] buffer, int offset)
            {
                buffer[offset] = (byte)(value >> 24);
                buffer[offset + 1] = (byte)(value >> 16);
                buffer[offset + 2] = (byte)(value >> 8);
                buffer[offset + 3] = (byte)value;
            }
        }

        private static readonly Lazy<RandomNumberGenerator> RandomNumberGeneratorLazy = new(RandomNumberGenerator.Create);
        private byte[] _decryptionKeyBlob;
        private CompatibilityMode _compatibilityMode;
        private HashProvider _hasher;
        private static RandomNumberGenerator RandomNumberGenerator => RandomNumberGeneratorLazy.Value;
        public MachineKeyEncryptor(string decryptionKey, string validationKey, ShaVersion hashAlgorithm = ShaVersion.Sha1, CompatibilityMode compatibilityMode = CompatibilityMode.Framework20SP2)
        {
            byte[] decryptionKey2 = HexUtils.HexToBinary(decryptionKey);
            byte[] validationKey2 = HexUtils.HexToBinary(validationKey);
            Initialize(decryptionKey2, validationKey2, hashAlgorithm, compatibilityMode);
        }

        private void Initialize(byte[] decryptionKey, byte[] validationKey, ShaVersion hashAlgorithm, CompatibilityMode compatibilityMode)
        {
            _compatibilityMode = compatibilityMode;
            _decryptionKeyBlob = KeyDerivationHelper.DeriveKey(decryptionKey, _compatibilityMode);
            _hasher = HashProvider.Create(KeyDerivationHelper.DeriveKey(validationKey, _compatibilityMode), hashAlgorithm);
        }

        public byte[] Decrypt(string cookieString)
        {
            byte[] array = null;
            if (cookieString.Length % 2 == 0)
            {
                try
                {
                    array = HexUtils.HexToBinary(cookieString);
                }
                catch
                {
                    return array;
                }
            }

            if (array == null)
            {
                return array;
            }

            byte[] array2 = Decrypt(array, _hasher, isHashAppended: true);

            if (_compatibilityMode != CompatibilityMode.Framework20SP2)
            {
                return array2;
            }

            var num = array2.Length - _hasher.HashSize;
            if (!_hasher.CheckHash(array2, num))
            {
                throw new ArgumentNullException("Invalid Hash");
            }

            return array2;
        }
        public string Encrypt(byte[] array, bool randomiseUsingHash = false)
        {
            if (array == null)
            {
                throw new ArgumentNullException("Invalid ticket");
            }

            byte[] array2 = array;
            if (_compatibilityMode == CompatibilityMode.Framework20SP2 && _hasher != null)
            {
                byte[] hMacsHash = _hasher.GetHMACSHAHash(array, null, 0, array.Length);
                if (hMacsHash == null)
                {
                    throw new ArgumentNullException("Unable to get HMACSHAHash");
                }

                array2 = new byte[hMacsHash.Length + array.Length];
                Buffer.BlockCopy(array, 0, array2, 0, array.Length);
                Buffer.BlockCopy(hMacsHash, 0, array2, array.Length, hMacsHash.Length);
            }

            byte[] array3 = EncryptData(array2, randomiseUsingHash ? _hasher : null);
            if (array3 == null)
            {
                throw new ArgumentNullException("Unable to encrypt cookie");
            }

            if (_hasher == null)
            {
                return HexUtils.BinaryToHex(array2);
            }

            byte[] hMacsHash2 = _hasher.GetHMACSHAHash(array3, null, 0, array3.Length);
            if (hMacsHash2 == null)
            {
                throw new ArgumentNullException("Unable to sign cookie");
            }

            array2 = new byte[hMacsHash2.Length + array3.Length];
            Buffer.BlockCopy(array3, 0, array2, 0, array3.Length);
            Buffer.BlockCopy(hMacsHash2, 0, array2, array3.Length, hMacsHash2.Length);

            return HexUtils.BinaryToHex(array2);
        }

        private byte[] EncryptData(byte[] blob, HashProvider hasher = null)
        {
            using Aes aes = Aes.Create();
            aes.Key = _decryptionKeyBlob;
            aes.BlockSize = 128;
            aes.GenerateIV();
            if (_compatibilityMode == CompatibilityMode.Framework20SP2)
            {
                aes.IV = new byte[aes.IV.Length];
                aes.Mode = CipherMode.CBC;
            }
            else if (hasher != null)
            {
                aes.IV = hasher.GetIVHash(blob, aes.IV.Length);
            }

            ICryptoTransform transform = aes.CreateEncryptor();
            using MemoryStream memoryStream = new MemoryStream();
            if (_compatibilityMode != 0)
            {
                memoryStream.Write(aes.IV, 0, aes.IV.Length);
            }

            using CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            if (_compatibilityMode == CompatibilityMode.Framework20SP2)
            {
                if (true)
                {
                    int num = RoundupNumBitsToNumBytes(aes.KeySize);
                    byte[] array;
                    if (hasher != null)
                    {
                        array = hasher.GetIVHash(blob, num);
                    }
                    else
                    {
                        array = new byte[num];
                        RandomNumberGenerator.GetBytes(array);
                    }

                    cryptoStream.Write(array, 0, array.Length);
                }
            }

            cryptoStream.Write(blob, 0, blob.Length);
            cryptoStream.FlushFinalBlock();
            byte[] result = memoryStream.ToArray();

            return result;
        }
        private byte[] Decrypt(byte[] cookieBlob, HashProvider hasher, bool isHashAppended)
        {
            if (hasher == null)
            {
                throw new ArgumentNullException("hasher");
            }

            if (isHashAppended)
            {
                cookieBlob = hasher.CheckHashAndRemove(cookieBlob);
                if (cookieBlob == null)
                {
                    throw new CryptographicException("Signature verification failed");
                }
            }

            using Aes aes = Aes.Create();
            aes.Key = _decryptionKeyBlob;
            aes.BlockSize = 128;
            if (_compatibilityMode == CompatibilityMode.Framework20SP2)
            {
                aes.GenerateIV();
                aes.IV = new byte[aes.IV.Length];
                aes.Mode = CipherMode.CBC;
            }
            else
            {
                byte[] array = new byte[aes.IV.Length];
                Buffer.BlockCopy(cookieBlob, 0, array, 0, array.Length);
                aes.IV = array;
            }

            using MemoryStream memoryStream = new MemoryStream();
            using ICryptoTransform transform = aes.CreateDecryptor();
            using CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            if (_compatibilityMode == CompatibilityMode.Framework20SP2)
            {
                cryptoStream.Write(cookieBlob, 0, cookieBlob.Length);
            }
            else
            {
                cryptoStream.Write(cookieBlob, aes.IV.Length, cookieBlob.Length - aes.IV.Length);
            }

            cryptoStream.FlushFinalBlock();
            byte[] array2 = memoryStream.ToArray();
            if (_compatibilityMode != 0)
            {
                return array2;
            }

            int num = RoundupNumBitsToNumBytes(aes.KeySize);
            int num2 = array2.Length - num;
            if (num2 < 0)
            {
                throw new ArgumentException($"Unexpected salt length: {num}. Total: {array2.Length}");
            }

            byte[] array3 = new byte[num2];
            Buffer.BlockCopy(array2, num, array3, 0, num2);
            return array3;
        }

        internal static int RoundupNumBitsToNumBytes(int numBits)
        {
            if (numBits < 0)
            {
                return 0;
            }

            return numBits / 8 + ((((uint)numBits & 7u) != 0) ? 1 : 0);
        }
    }

    /// Based on the implementation from AspNetCore.LegacyAuthCookieCompat
    public class MachineKeySecureDataFormat(string decryptionKey, string validationKey)
        : ISecureDataFormat<AuthenticationTicket>
    {
        readonly TicketSerializer _ticketSerializer = new();
        readonly MachineKeyEncryptor _encryptor = new(decryptionKey, validationKey);

        public string Protect(AuthenticationTicket data)
        {
            var serialized = _ticketSerializer.Serialize(data);
            return _encryptor.Encrypt(serialized);
        }

        public string Protect(AuthenticationTicket data, string purpose)
        {
            return Protect(data);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            var decrypted = _encryptor.Decrypt(protectedText);
            var ticket = _ticketSerializer.Deserialize(decrypted);
            return ticket;
        }

        public AuthenticationTicket Unprotect(string protectedText, string purpose)
        {
            return Unprotect(protectedText);
        }
    }
}
