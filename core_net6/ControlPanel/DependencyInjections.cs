﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DependencyRegistration;
using Clouds42.Jwt;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Helpers;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Cp.enums;
using Cp.Helpers;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Repositories;

namespace Cp
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddServices(this IServiceCollection collection, IConfiguration configuration)
        {
            collection
                .AddScoped<IAccessProvider, ControlPanelAccessProvider>()
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddTransient<IRequestContextTaskDispatcher, RequestContextTaskDispatcher>()
                .AddMemoryCache()
                .AddHttpClient()
                .AddScoped<IJwtHelper, JwtHelper>()
                .AddJwt()
                .AddTransient<ICompositeViewEngine, CompositeViewEngine>()
                .AddScoped<ContextAccess>()
                .AddGlobalServices(configuration);

            return collection;
        }
        
        public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
        {
            var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

            var elasticConfig = new ElasticConfigDto(
                    conf["Elastic:Uri"],
                    conf["Elastic:UserName"],
                    conf["Elastic:Password"],
                    conf["Elastic:CpIndex"],
                    conf["Logs:AppName"]
                );

            SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
            return collection;
        }
        

        public static IRouteBuilder AddRoutes(this IRouteBuilder routeBuilder)
        {
            routeBuilder.MapGet("{resource}.axd/{*pathInfo}", async (context) =>
            {
                context.Response.StatusCode = 404;
            });
            routeBuilder.MapRoute(
                name: "ROUTE TO GET MARKET AS PLUGIN FOR 1C",
                "market/get-plugin-1c",
                defaults: new
                {
                    controller = "ReactSpa",
                    action = "Market1C"
                });

            routeBuilder.MapRoute(
                "MARKET REACT-JS APP", // Имя маршрута
                "{locale}/{app}/{*route}", // URL-адрес с параметрами
                new
                {
                    controller = "ReactSpa",
                    action = nameof(Index)
                });
                routeBuilder.MapRoute(
                "REACT-JS APP", // Имя маршрута
                "{app}/{*route}", // URL-адрес с параметрами
                new
                {
                    controller = "ReactSpa",
                    action = nameof(Index)
                }, // Параметры по умолчанию
                new
                {
                    app = $"^({ReactApp.Spa}|{ReactApp.Market})$"
                });
            routeBuilder.MapRoute(
                "MVC PayboxImportSuccessOrFail", // Имя маршрута
                "BillingAccounts/PayboxImportSuccessOrFail", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "PayboxImportSuccessOrFail"
                });
            routeBuilder.MapRoute(
                "MVC PayboxImportResult", // Имя маршрута
                "BillingAccounts/PayboxImportResult", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "PayboxImportResult"
                });
            routeBuilder.MapRoute(
                "MVC RobokassaImportResult", // Имя маршрута
                "BillingAccounts/RobokassaImportResult", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "RobokassaImportResult"
                });
            routeBuilder.MapRoute(
                "MVC RobokassaImportSuccess", // Имя маршрута
                "BillingAccounts/RobokassaImportSuccess", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "RobokassaImportSuccess"
                });
            routeBuilder.MapRoute(
                "MVC RobokassaImportFail", // Имя маршрута
                "BillingAccounts/RobokassaImportFail", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "RobokassaImportFail"
                });
            routeBuilder.MapRoute(
                "MVC UkrPaysImportResult", // Имя маршрута
                "BillingAccounts/UkrPaysImportResult", // URL-адрес с параметрами
                new
                {
                    controller = "BillingAccounts",
                    action = "UkrPaysImportResult"
                });
            routeBuilder.MapRoute(
                "REACT-JS", // Имя маршрута
                "{*route}", // URL-адрес с параметрами
                new
                {
                    controller = "ReactSpa",
                    action = nameof(Index)
                });


            routeBuilder.MapRoute(
                "Default", // Имя маршрута
                "{*route}", // URL-адрес с параметрами
                new
                {
                    controller = "ReactSpa",
                    action = nameof(Index)
                });

            return routeBuilder;
        }
    }
}
