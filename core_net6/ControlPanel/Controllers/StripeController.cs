﻿using System.Text;
using System.Text.Encodings.Web;
using Clouds42.Accounts.Stripe.Helpers;
using Clouds42.Accounts.Stripe.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.Navigation;
using Cp.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using ActionResult = Microsoft.AspNetCore.Mvc.ActionResult;
using ViewContext = Microsoft.AspNetCore.Mvc.Rendering.ViewContext;

namespace Cp.Controllers
{
    /// <summary>
    /// Контроллер для работы со страйпом
    /// </summary>
    [AllowAnonymous]
    public class StripeController(
        StripeManager stripeManager,
        StripeOptionsMapper stripeOptionsMapper,
        ICompositeViewEngine viewEngine,
        IHttpContextAccessor httpContextAccessor,
        ContextAccess contextAccessor,
        IAccessProvider accessProvider,
        IWebHostEnvironment webHostEnvironment)
        : BaseContentController
    {
        /// <summary>
        /// Вернуть начальньое представление страйпа
        /// </summary>
        /// <returns>Начальное представление страйпа</returns>
        [HttpGet]
        public ActionResult Index()
        {
            return PartialView(stripeManager.GetStripeData(stripeOptionsMapper.MapToUserParametersForStripeModel(
                contextAccessor.IsAuthenticated(),
                contextAccessor.Name, accessProvider.GetContextAccountId(httpContextAccessor.HttpContext?.Request.Cookies))).Result);
        }

        /// <summary>
        /// Сгенерировать код js для страйпа
        /// </summary>
        /// <param name="stripeOptionsDto">Модель параметров для страйпа</param>
        /// <returns>Сгенерированныйы код js</returns>
        [HttpGet]
        public ActionResult GenerateStripeJS(StripeOptionsDto stripeOptionsDto)
        {
            var result = stripeManager.GetStripeData(stripeOptionsMapper.MapToUserParametersForStripeModel(
                contextAccessor.IsAuthenticated(),
                contextAccessor.Name, accessProvider.GetContextAccountId(httpContextAccessor.HttpContext?.Request.Cookies), stripeOptionsDto));
            
            return result.Error ? JsonIsoDataResult(!result.Error, new { ErrorMessage = result.Message }) : Content(GenerateStringForJsFromView(nameof(Index), result.Result), "application/x-javascript", Encoding.UTF8);
        }

        /// <summary>
        /// Получить данные пользователя для страйпа
        /// </summary>
        /// <returns>Данные пользователя для страйпа</returns>
        [HttpGet]
        public ActionResult GetUserDataForStripe()
        {
            var result = stripeManager.GetUserDataForStripe(contextAccessor.IsAuthenticated(), contextAccessor.Name);
            var userDataForStripeProviderScript =
                $"var userDataForStripeProvider=\"{JavaScriptEncoder.Default.Encode(RenderViewToString(nameof(GetUserDataForStripe), result.Result))}\";";

            return new JsonResult(userDataForStripeProviderScript);
        }

        /// <summary>
        /// Визуализировать представление в строку
        /// </summary>
        /// <param name="viewName">Название представления</param>
        /// <param name="model">Модель</param>
        /// <returns>Сгенерированную строку из представления</returns>
        private string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.ActionDescriptor.ActionName;

            using var sw = new StringWriter();
            ViewEngineResult viewResult =
                viewEngine.FindView(ControllerContext, viewName, false);

            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                ViewData,
                TempData,
                sw,
                new HtmlHelperOptions()
            );

            viewResult.View.RenderAsync(viewContext);

            return sw.GetStringBuilder().ToString();
        }

        /// <summary>
        /// Сгенерировать строку для JS из представления
        /// </summary>
        /// <param name="viewName">Название представления</param>
        /// <param name="model">Модель</param>
        /// <returns>Сгенерированная строка для JS</returns>
        private string GenerateStringForJsFromView(string viewName, object model)
        {
            var page = JavaScriptEncoder.Default.Encode(RenderViewToString(viewName, model));
            return " var html = \"" + page + "\"; \r\n" + System.IO.File.ReadAllText(Path.Combine(webHostEnvironment.WebRootPath, "Scripts/stripe.js"));
        }
    }
}
