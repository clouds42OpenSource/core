﻿using System.Security.Claims;
using Clouds42.Configurations.Configurations;
using Clouds42.Jwt.Contracts;
using Clouds42.Jwt.Contracts.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cp.Controllers.Home
{
    [Authorize]
    public class JwtController(IJwtWorker jwtWorker) : BaseContentController
    {
        [HttpGet]
        public async Task<ActionResult> GenerateJwtAccessTokenAsync()
        {
            var userName = User?.Identity?.Name;
            if (string.IsNullOrEmpty(userName))
                return Unauthorized();

            var userId = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

            var issuer = CloudConfigurationProvider.JsonWebToken.GetIssuer();
            var audience = CloudConfigurationProvider.JsonWebToken.GetAudienceCp();
            var timeToLive = TimeSpan.FromMinutes(CloudConfigurationProvider.JsonWebToken.GetTimeToLiveInMinutes());

            var jwtSettings = new CreateJwtSettings
            {
                Issuer = issuer,
                Audience = audience,
                TimeToLive = timeToLive
            };
            var jwtInfo = await jwtWorker.CreateJsonWebTokenAsync(new []
            {
                new Claim(ClaimTypes.Name, userName),
                userId
            }, jwtSettings);

            Response.Headers.Add("Authorization", $"Bearer {jwtInfo.JsonWebToken}");

            return Ok();
        }
    }
}
