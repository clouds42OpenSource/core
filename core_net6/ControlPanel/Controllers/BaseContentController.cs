﻿using System.Text;
using Clouds42.Common.ManagersResults;
using Clouds42.Logger;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Cp.Controllers
{
    /// <summary>
    /// Базовый контроллер для работы с контентом приложения
    /// </summary>
    public class BaseContentController : Controller
    {
        protected readonly ILogger42 Logger = Logger42.GetLogger();
        private ISender _mediator;

        /// <summary>
        ///     Медиатор
        /// </summary>
        protected ISender Mediator => _mediator ??= HttpContext.RequestServices.GetService<ISender>();

        /// <summary>
        /// Результат выполнения запроса в Json
        /// </summary>
        /// <param name="requestStatus">Статус запроса</param>
        /// <param name="option">Доп. параметры</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Результат выполнения запроса в Json</returns>
        protected ActionResult JsonDataStatus(bool requestStatus, object option = null, string message = null)
        {
            return Json(new { success = requestStatus, data = option, message });
        }

        /// <summary>
        /// Модель валидации аргументов запроса в Json
        /// </summary>
        protected ActionResult ModelValidationToJson()
        {
            var modelStateResponse = new List<dynamic>();
            foreach (var item in ModelState)
            {
                if (item.Value.Errors.Count == 0) continue;
                modelStateResponse.Add(new
                {
                    property = item.Key,
                    errors = item.Value.Errors.Select(error => error.ErrorMessage).ToArray()
                });
            }

            return Json(new { success = false, validationState = modelStateResponse });
        }

        /// <summary>
        /// Результат выполнения запроса с данными в Json
        /// </summary>
        /// <param name="requestStatus">Статус запроса</param>
        /// <param name="option">Доп. параметры</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Результат выполнения запроса с данными в Json</returns>
        protected ActionResult JsonDataResult(bool requestStatus, object option = null, string message = null)
        {
            return new JsonResult(new { success = requestStatus, data = option, message });
        }

        /// <summary>
        /// Результат выполнения запроса с данными в формате Json с корректной конвертацией даты
        /// </summary>
        /// <param name="requestStatus">Сатус запроса</param>
        /// <param name="option">Объект для сериализации</param>
        /// <param name="message">Сообщение</param>
        /// <returns>Результат выполнения запроса с данными в формате Json с корректной конвертацией даты</returns>
        protected ActionResult JsonIsoDataResult(bool requestStatus, object option = null, string message = null)
        {
            var resultData = new { success = requestStatus, data = option, message };
            return GetContentResult(resultData);
        }

        /// <summary>
        /// Переложить результат выполнения менеджера в Json
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="managerResult">Результат выполнения менеджера</param>
        /// <returns>Результат выполнения в Json</returns>
        protected ActionResult TransferToJsonDataStatus<T>(ManagerResult<T> managerResult) => managerResult.Error
            ? JsonDataStatus(!managerResult.Error, new { ErrorMessage = managerResult.Message })
            : JsonDataStatus(!managerResult.Error, managerResult.Result);

        /// <summary>
        /// Переложить результат выполнения менеджера в Json
        /// с корректной конвертацией даты
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="managerResult">Результат выполнения менеджера</param>
        /// <returns>Результат выполнения в Json</returns>
        protected ActionResult TransferToJsonIsoDataResult<T>(ManagerResult<T> managerResult) => managerResult.Error
            ? JsonDataStatus(!managerResult.Error, new { ErrorMessage = managerResult.Message })
            : JsonDataStatus(!managerResult.Error, managerResult.Result);

        /// <summary>
        /// Переложить результат выполнения менеджера в Json
        /// </summary>
        /// <param name="managerResult">Результат выполнения менеджера</param>
        /// <returns>Результат выполнения в Json</returns>
        protected ActionResult TransferToJsonDataStatus(ManagerResult managerResult) => managerResult.Error
            ? JsonDataStatus(!managerResult.Error, new { ErrorMessage = managerResult.Message })
            : JsonDataStatus(!managerResult.Error);

        /// <summary>
        /// Получить объект ContentResult
        /// </summary>
        /// <param name="resultData">Содержимое ответа на запрос</param>
        /// <returns>Объект ContentResult</returns>
        private static ContentResult GetContentResult(object resultData)
            => new()
            {
                Content = JsonConvert.SerializeObject(resultData),
                ContentType = "application/json"
            };

        /// <summary>
        /// Сохранить детали HTTP запроса в лог файл
        /// </summary>
        protected async Task<string> LogRequestAndGetNakedBody()
        {
            var body = await GetRequestBodyAsStringOrEmptyAsync();
            Logger.Info($"Request Details: {Request.Method} to {Request.Host} with body {body}");
            return body;
        }


        /// <summary>
        /// Попытаться получить сырое string тело запроса или пустую строку.
        /// </summary>
        /// <returns>Тело запроса</returns>
        private async Task<string> GetRequestBodyAsStringOrEmptyAsync()
        {
            try
            {
                using StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8, false, 1024, true);
                var bodyString = await reader.ReadToEndAsync();

                return bodyString;
            }

            catch (Exception ex)
            {
                Logger.Warn(ex, "Не удалось прочитать тело запроса в string");
                return string.Empty;
            }
        }
    }
}
