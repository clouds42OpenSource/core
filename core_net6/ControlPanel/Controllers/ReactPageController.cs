﻿using System.Net.Http.Headers;
using System.Text;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Cp.enums;
using Cp.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Newtonsoft.Json;
using MediaTypeWithQualityHeaderValue = System.Net.Http.Headers.MediaTypeWithQualityHeaderValue;

namespace Cp.Controllers
{
    /// <summary>
    /// Контролер для обработки запросов от React приложения 
    /// </summary>
    [Authorize]
    public class ReactSpaController(
        IUnitOfWork dbLayer,
        AccountUserSessionLoginManager accountUserSessionLoginManager,
        IAccessProvider accessProvider,
        IAccountUserProvider accountUserProvider,
        ICompositeViewEngine viewEngine,
        IWebHostEnvironment hostingEnvironment,
        ContextAccess contextAccessor)
        : Controller
    {
        /// <summary>
        /// ID для сервиса CORE-42
        /// </summary>
        private static readonly string Clouds42ServiceId = "CORE42";

        /// <summary>
        /// Локаль по умолчанию
        /// </summary>
        private const string DefaultLocale = "ru-ru";

        /// <summary>
        /// Время кеширования HTTP ресурса в секундах.
        /// </summary>
        private readonly int _resourceCacheInSeconds = CloudConfigurationProvider.Resource.GetHttpResourceCacheTimeInSeconds();
        
        /// <summary>
        /// Список расширений ресурса, которые ненужно кешировать
        /// </summary>
        private readonly string[] _extensionsWithoutCacheControl = [".htm", ".html"];

        /// <summary>
        /// URL для отправки запроса в МС на проверку токена
        /// </summary>
        private readonly string _msTokenValidationUrl = CloudConfigurationProvider.Market.GetUrlToCheckToken();

        /// <summary>
        /// Название заголовка для токена в МС
        /// </summary>
        private readonly string _tokenHeaderParameterName = CloudConfigurationProvider.DbTemplateDelimiters.GetTokenHeaderParameterName();

        /// <summary>
        /// Логин для авторизации в МС
        /// </summary>
        private readonly string _msLogin = CloudConfigurationProvider.DbTemplateDelimiters.GetLoginForRegistrationZone();

        /// <summary>
        /// Пароль для авторизации  в МС
        /// </summary>
        private readonly string _msPassword = CloudConfigurationProvider.DbTemplateDelimiters.GetPasswordForRegistrationZone();

        private readonly ICompositeViewEngine _viewEngine = viewEngine;
        readonly ContextAccess _contextAccessor = contextAccessor;

        /// <summary>
        /// Экшен для открытия ReactJS приложения
        /// </summary>
        /// <param name="app">React приложение</param>
        /// <param name="route">Запрашиваемый путь в ReactJS приложении</param>
        /// <param name="returnUrl">URL на который вдальнейшем нужно перейти</param>
        /// <returns>Файлы ReactJS приложения</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index(ReactApp app = ReactApp.Spa, string route = "", string returnUrl = "")
        {
            if (!route.IsNullOrEmpty() && route.Contains("market"))
            {
                route = route.Replace("market","");
                app = ReactApp.Market;
            }
                
            return GetReactAppFiles(app, route, returnUrl);
        }

        /// <summary>
        /// Получить последний активный токен сессии для текущего пользователя в промо
        /// </summary>
        /// <returns>Последний активный токен сессии для текущего пользователя в промо</returns>
        private Guid GetUserSessionTokenForCurrentUserForMarketInPromo()
        {
            var currentUserSessionToken = Guid.Empty;
            try
            {
                var currentUser = accessProvider.GetUser();
                if (currentUser != null)
                {
                    var lastActiveUserSessionToken = dbLayer.AccountUserSessionsRepository.GetLastValidTokenForUser(currentUser.Id, 1);

                    currentUserSessionToken = Guid.Empty.Equals(lastActiveUserSessionToken)
                        ? CreateUserSessionToken(currentUser.Id, currentUser.Name, "Промо сайт.")
                        : lastActiveUserSessionToken;
                }
            }
            catch
            {
                // To do nothing...
            }

            return currentUserSessionToken;
        }


        /// <summary>
        /// Экшен для открытия ReactJS приложения
        /// </summary>
        /// <remarks></remarks>
        /// <param name="zone">Зона в МС</param>
        /// <returns>Файлы ReactJS приложения</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Market1C(
            [FromQuery(Name = "openid.auth.uid")] string openIdAuthUserId,
            [FromQuery(Name = "openid.auth.user")] string userLogin,
            [FromQuery] string zone,
            [FromQuery] bool hybrid,
            [FromQuery] string configurationName
            )
        {
            var locale = DefaultLocale;

            var isValid = await IsUserIdValid(new TokenValidationDto
            {
                User = userLogin,
                Token = openIdAuthUserId
            });

            if (!isValid)
            {
                return RedirectToAction(nameof(Index), new
                {
                    locale,
                    app = ReactApp.Market,
                    route = "init-app",
                    error = "Token not valid"
                });
            }

            var foundUser = dbLayer.AccountUsersRepository.GetAccountUserByLogin(userLogin);
            if (foundUser == null)
            {
                return RedirectToAction(nameof(Index), new
                {
                    locale,
                    app = ReactApp.Market,
                    route = "init-app",
                    error = $"User \"{userLogin}\" not found"
                });
            }

            return RedirectToAction(nameof(Index), new
            {
                locale,
                app = ReactApp.Market,
                route = "init-app",
                authData = CreateAuthenticationDataFor1C(foundUser, zone),
                hybrid,
                configurationName
            });
        }


        /// <summary>
        /// Создаёт аутентификационные данные для маркета в 1С
        /// </summary>
        /// <param name="user">Доменная модель пользователя</param>
        /// <param name="dbZone">Зона базы на разделителях</param>
        /// <returns>Строку в Base64 кодировке, содержащая аутентификационные данные для маркета в 1С</returns>
        private string CreateAuthenticationDataFor1C(AccountUser user, string dbZone)
        {
            var isAccountAdmin =  IsAccountAdmin(user);
            var userSessionToken = CreateUserSessionToken(user.Id, user.Login, "1С клиент");
            var accountAdminContacts = GetAccountAdminContacts(user);

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
            {
                userId = user.Id,
                dbZone,
                isAdmin = isAccountAdmin,
                accountId = user.AccountId,
                coreToken = userSessionToken,
                accountAdminEmail = accountAdminContacts.Email,
                accountAdminPhone = accountAdminContacts.PhoneNumber
            })));
        }

        /// <summary>
        /// Проверка, является ли пользователь админом аккаунта
        /// </summary>
        /// <param name="user">Какого пользователя проверить</param>
        /// <returns>true - если админ аккаунте</returns>
        private bool IsAccountAdmin(AccountUser user) =>
            user != null && user.AccountUserRoles.Any(item => item.AccountUserGroup == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Проверка, является ли пользователь админом аккаунта
        /// </summary>
        /// <param name="user">Какого пользователя проверить</param>
        /// <returns>true - если админ аккаунте</returns>
        private bool IsAccountAdmin(IUserPrincipalDto user) =>
            user != null && user.Groups.Any(g => g == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Получить контакты пользователя
        /// </summary>
        /// <param name="user">У какого пользователя получить контакты</param>
        /// <returns>Контакты пользователя</returns>
        private UserContactsModelDto GetAccountAdminContacts(AccountUser user)
        {
            var isAccountAdmin = IsAccountAdmin(user);

            if (isAccountAdmin)
            {
                return new UserContactsModelDto
                {
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber
                };
            }
            else
            {
                return GetFirstAccountAdminContacts(user.AccountId);
            }
        }

        /// <summary>
        /// Получить контакты пользователя
        /// </summary>
        /// <param name="user">У какого пользователя получить контакты</param>
        /// <returns>Контакты пользователя</returns>
        private UserContactsModelDto GetAccountAdminContacts(IUserPrincipalDto user)
        {
            var isCurrentUserAccountAdmin = IsAccountAdmin(user);
            var currentAccountId = user?.RequestAccountId ?? Guid.Empty;

            if (user != null && isCurrentUserAccountAdmin)
            {
                return new UserContactsModelDto
                {
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber
                };
            }
            else
            {
                return GetFirstAccountAdminContacts(currentAccountId);
            }
        }

        /// <summary>
        /// Получить контакты для первого пользователя, который является админом аккаунта
        /// </summary>
        /// <param name="accountId">Для какого аккаунта найти первого админа аккаунта и получит его контакты</param>
        /// <returns>Контакты пользователя</returns>
        private UserContactsModelDto GetFirstAccountAdminContacts(Guid accountId)
        {
            var adminAccount = accountUserProvider.GetFirstAccountAdminFor(accountId);

            if (adminAccount != null)
            {
                return new UserContactsModelDto
                {
                    Email = adminAccount.Email,
                    PhoneNumber = adminAccount.PhoneNumber
                };
            }

            return new UserContactsModelDto();
        }

        /// <summary>
        /// Создаёт сессию пользователя только для админа аккаунта
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userLogin"></param>
        /// <param name="clientDeviceInfo">Информация о устройстве клиента.</param>
        /// <returns>Токен сессии для администратора аккаунта или <see cref="Guid.Empty"/> - сессия не создана.</returns>
        private Guid CreateUserSessionToken(Guid userId, string userLogin, string clientDeviceInfo) =>
            accountUserSessionLoginManager.CrossServiceLogin(new CrossServiceLoginModelDto
            {
                AccountUserId = userId,
                AccountUserLogin = userLogin,
                ClientDescription = "Открытие маркета",
                ClientDeviceInfo = clientDeviceInfo
            }).Result;

        private async Task<bool> IsUserIdValid(TokenValidationDto model)
        {
            using var httpClient = new HttpClient();
            var response = await SubmitRequestToCheckToken(httpClient, model);
            var resultContent = await response.Content.ReadAsStringAsync();
            var result = resultContent.DeserializeFromJson<TokenValidationResultDto>();

            return result?.IsValid == true;
        }

        /// <summary>
        /// Отправить запрос на проверку токена
        /// </summary>
        /// <param name="httpClient">Каким клиентом отправить запрос</param>
        /// <param name="model">Модель для проверки токена</param>
        /// <returns>HttpResponseMessage</returns>
        /// <exception cref="NotFoundException">Вслучае, если не заполнены данные для сервиса "CORE42"</exception>
        private Task<HttpResponseMessage> SubmitRequestToCheckToken(HttpClient httpClient, TokenValidationDto model)
        {
            var service = dbLayer.CloudServiceRepository.GetCloudService(Clouds42ServiceId);
            if (service?.ServiceToken == null)
                throw new NotFoundException($"Токен службы \"{Clouds42ServiceId}\" не найден");

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
            $"{Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_msLogin}:{_msPassword}"))}");

            var messageContent = new StringContent(model.ToJson());
            messageContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            messageContent.Headers.Add(_tokenHeaderParameterName, service.ServiceToken?.ToString());

            return httpClient.PostAsync($"{_msTokenValidationUrl}", messageContent);
        }

        private string GetAppViewPathFromAppName(ReactApp app)
        {
            var appFolder = app.GetStringValue();
            return string.IsNullOrEmpty(appFolder)
                ? appFolder
                : Path.Combine(hostingEnvironment.ContentRootPath, "Views", "ReactSpa", appFolder);
        }

        /// <summary>
        /// Возвратить ReactJS файлы
        /// </summary>
        /// <param name="app">Приложение React</param>
        /// <param name="route">Запрашиваемый путь в ReactJS приложении</param>
        /// <param name="returnUrl">URL на который вдальнейшем нужно перейти</param>
        /// <returns>Файлы ReactJS приложения</returns>
        private ActionResult GetReactAppFiles(ReactApp app, string route, string returnUrl)
        {
            var appViewPath = GetAppViewPathFromAppName(app);
            if (string.IsNullOrEmpty(appViewPath))
            {
                return NotFound();
            }

            if(string.IsNullOrEmpty(route))
                 route =  String.Empty;

            var previewFilePath = Path.Combine(appViewPath, route);
            var previewFileExtension = Path.GetExtension(previewFilePath);
            var hasPreviewFileExtension = !string.IsNullOrEmpty(previewFileExtension);
            var isPreviewFilePathExist = System.IO.File.Exists(previewFilePath);

            //Если файл не существует, но есть расширение, то возвращаем пустую строку
            if (!isPreviewFilePathExist && hasPreviewFileExtension)
            {
                return new ContentResult
                {
                    Content = string.Empty,
                    ContentType = GetMimeTypeFromFileExtension(previewFileExtension)
                };
            }

            //Если запрашиваеммый файл не существует, то возвращаем стартовую страницу, иначе запрашиваеммый файл 
            var resultFilePath = isPreviewFilePathExist
                ? previewFilePath
                : Path.Combine(appViewPath, "index.html");

            var resultFileExtension = Path.GetExtension(resultFilePath);
            var contentType = GetMimeTypeFromFileExtension(resultFileExtension);

            ResponseWriteExpiresForResourceExtension(resultFileExtension);
            var bytes = System.IO.File.ReadAllBytes(resultFilePath);
            return new FileContentResult(bytes, contentType);
        }

        private void ResponseWriteExpiresForResourceExtension(string resourceExtension)
        {
            if(Array.IndexOf(_extensionsWithoutCacheControl, resourceExtension) >= 0)
                return;


            Response.GetTypedHeaders().CacheControl =
            new Microsoft.Net.Http.Headers.CacheControlHeaderValue
            {
                Public = true,
                MaxAge = TimeSpan.FromSeconds(_resourceCacheInSeconds),
            };

            Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
            new string[] { "Accept-Encoding" };
        }


        /// <summary>
        /// Конвертирует экстеншен файла в mime тип
        /// </summary>
        /// <param name="ext">Экстеншен который нужно сконвертировать</param>
        /// <returns>Mime тип для переданного расширения <paramref name="ext"/>.</returns>
        private static string GetMimeTypeFromFileExtension(string ext)
        {
            switch (ext?.ToLower())
            {
                case ".txt":
                case ".map":
                    return "text/plain";

                case ".svg":
                    return "image/svg+xml";

                case ".eot":
                    return "application/vnd.ms-fontobject";

                case ".woff2":
                    return "application/font-woff2";

                case ".woff":
                    return "font/x-woff";

                case ".ttf":
                    return "application/octet-stream";

                case ".htm":
                case ".html":
                    return "text/html";

                case ".css":
                    return "text/css";

                case ".js":
                    return "application/javascript";

                case ".json":
                    return "application/json";

                case ".png":
                    return "image/png";

                case ".ico":
                    return "image/x-icon";

                case ".gif":
                    return "image/gif";

                default:
                    return "text/plain";
            }
        }
    }
}
