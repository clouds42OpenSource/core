﻿using System.Text;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Billing.Models;
using Clouds42.Billing.Factories.Helpers;
using Clouds42.BLL.Common;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Cp.Helpers;
using Cp.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Cp.Controllers.Management
{
    /// <summary>
    /// Контроллер операций биллинга
    /// </summary>
    public class BillingAccountsController(
        IUnitOfWork dbLayer,
        PaymentReceiptManager manager,
        BillingAccountManager billingAccountManager,
        PaymentsManager paymentsManager,
        ConfirmPaymentManager confirmPaymentManager,
        PayboxPaymentManager payboxPaymentManager,
        IHandlerException handlerException,
        ILogger42 logger,
        ContextAccess contextAccessor)
        : BaseController
    {
        /// <summary>
        ///     Управление коллбэком Paybox, сообщают о результате платежа
        /// </summary>
        /// <param name="resultModel">Параметры</param>
        /// <returns>Ответ в виде xml строки</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> PayboxImportResult(PayboxImportResultModel resultModel)
        {
            var body = await LogRequestAndGetNakedBody();
            
            switch (Request.ContentType)
            {
                case "application/json":
                    resultModel = JsonConvert.DeserializeObject<PayboxImportResultModel>(body);
                    resultModel?.SetSourceDataCollection(RequestSourceDataCollectionHelper.GetSourceDataCollectionFromJson(body));
                    break;
                case "application/x-www-form-urlencoded":
                    resultModel?.SetSourceDataCollection(HttpContext.Request.GetSourceDataCollectionFromForm());
                    break;
            }

            var managerResult = payboxPaymentManager.PayboxImportResultAnonymous(resultModel);

            return Ok(managerResult.Error ? managerResult.Message : managerResult.Result);
        }

        /// <summary>
        ///     Paybox перенаправляет в случае успешного или не успешного онлайн прохождения платежа
        /// </summary>
        /// <param name="model">Параметры</param>
        /// <returns>Redirect на BillingAccounts/Main с выводом сообщения</returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult PayboxImportSuccessOrFail(PayboxImportSuccessOrFailureModel model)
        {
            var payment = dbLayer.PaymentRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .FirstOrDefault(x => x.Number == model.pg_order_id);

            var signature = model.GenerateSignature(payment.Account.AccountConfiguration.SupplierId);

            if (!paymentsManager.IsPayboxSignatureValid(signature, model.pg_sig))
            {
                logger.Trace("(PayboxImportSuccessOrFail) :: Не валидная подпись");
                TempData["Alert"] = "Paybox: ошибка при проверке платежа. Не валидная подпись.";
                return Redirect("/billing");
            }

            logger.Trace($"(PayboxImportSuccessOrFail) :: Запрос платежа {model.pg_order_id} - Валиден!");

            var checkPaymentStatus =
                paymentsManager.CheckPayboxPaymentStatus(model.pg_order_id,
                    model.pg_failure_description);

            var message = checkPaymentStatus;
            TempData["Alert"] = message;
            return Redirect("/billing");
        }

        /// <summary>
        /// Robokassa. Успешная проплата. Коллбэк обработки платежа
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        public void RobokassaImportResult(decimal outSum, int invId, string signatureValue)
        {
            if (!new RobokassaHelper(dbLayer).CheckResponse(outSum, invId, signatureValue))
                return;

            var accountId = dbLayer.PaymentRepository.FirstOrDefault(x => x.Number == invId).AccountId;
            try
            {
                confirmPaymentManager.ConfirmPaymentAnonymous(invId, outSum, true);
                manager.CreateFiscalReceiptTask(invId);
                billingAccountManager.SendRobokassaSuccessImportReport(invId);

                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Пополнение баланса через Робокассу на сумму {outSum:0.00} руб.");

                byte[] bytes = Encoding.ASCII.GetBytes("OK" + invId);
                Response.Body.WriteAsync(bytes);
                logger.Info($"Успешное зачисление платежа {invId} на сумму {outSum:0.00} руб.");
            }
            catch (Exception e)
            {
                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Ошибка пополнения баланса через Робокассу на сумму {outSum:0.00} руб.");
                logger.Info($"Ошибка зачисление платежа {invId} на сумму {outSum:0.00} руб. Ошибка: {e.GetFullInfo()}");
            }
        }

        /// <summary>
        /// Robokassa. Успешная проплата. Страница для отображения клиенту
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult RobokassaImportSuccess(decimal outSum)
        {
            TempData["Success"] = $"Робокасса: платеж успешно импортирован {outSum:0.00} руб.";
            return Redirect("/billing");
        }

        /// <summary>
        /// Robokassa. Неуспешная проплата. Страница для отображения клиенту
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult RobokassaImportFail(decimal outSum, int invId)
        {
            var confirmResult =
                confirmPaymentManager.ConfirmPayment(contextAccessor.ContextAccountId, invId, outSum, false);
            if (!confirmResult.Error && confirmResult.Result == PaymentOperationResult.Ok)
            {
                logger.Info($"Успешная отмена платежа {invId} на сумму {outSum:0.00} руб.");
                TempData["Alert"] = $"Робокасса: платеж аннулирован {outSum:0.00} руб.";
            }
            else
            {
                logger.Info($"Ошибка при отмене платежа {invId} на сумму {outSum:0.00} руб.");
                TempData["Alert"] = $"Робокасса: ошибка при аннулировании платежа {outSum:0.00} руб.";
            }

            return Redirect("/billing");
        }


        [AllowAnonymous]
        [HttpPost]
        public string UkrPaysImportResult(string id_ups, string order, string amount, string date, string hash,
            string system)
        {
            try
            {
                //разделяеи на номер внутрненний номер платежа и номер аккаунта
                logger.Info("(UkrPaysImportResult)Получение данных с параметрами id_ups: {0}, amount: {1}, order: {2}, date: {3}, hash: {4}, system: {5}", 
                    id_ups, amount, order, date, hash, system);
                var clientPayData = order.Split('_');
                var payId = int.Parse(clientPayData[0]);
                var accountIndexNumber = int.Parse(clientPayData[1]);
                var accountId = dbLayer.AccountsRepository.FirstOrDefault(x => x.IndexNumber == accountIndexNumber).Id;
                var sum = decimal.Parse(amount.Replace('.', ','));
                var extNumber = int.Parse(id_ups);

                var existSuccessPayment =
                    dbLayer.PaymentRepository.FirstOrDefault(x => x.PaymentSystemTransactionId == extNumber.ToString());
                if (existSuccessPayment != null)
                    return "duplicate";
                logger.Info("(UkrPaysImportResult)Начало обработки платежа {0} на сумму {1}", payId, amount);

                if (!UkrPaysHelper.IsValidControlKey(id_ups, order, amount, date, hash))
                {
                    //ошибка и отмена платежа
                    confirmPaymentManager.ConfirmPaymentAnonymous(payId, sum, false);
                    logger.Info("UkrPays: ошибка при импорте платежа {0}. Контрольные суммы не совпадают!", payId);
                    LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                        $"Ошибка пополнения баланса через UkrPays на сумму {sum:0.00} грн.");
                    return "Error";
                }

                logger.Info("(UkrPaysImportResult)запрос платежа {0} на сумму {1} Валиден!", payId, amount);

                var systemName = UkrPaysHelper.GetPaySystemValue(system);
                logger.Info(systemName);
                paymentsManager.EditPaySystemDescription(payId, systemName);
                var confirmResult = confirmPaymentManager.ConfirmPaymentAnonymous(payId, sum, true);
                paymentsManager.SetExternalPaymentStatus( payId, extNumber);
                logger.Info("(UkrPaysImportResult)Подтвержение платежа {0} на сумму {1} в статусе : {2}", payId, amount,
                    confirmResult);
                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Пополнение баланса через UkrPays на сумму {sum} грн.");
                return "OK";
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[(UkrPaysImportResult)Ошибка платежа]");
                return "Error";
            }
        }

        
    }

}
