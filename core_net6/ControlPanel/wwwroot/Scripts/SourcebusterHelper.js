﻿// Объект хелпер для определения источника 
var SourcebusterHelper = {

    // Инициализировать источник пользователя
    initUserSource: function (registrableDomain, configuration = this._defaultConfiguration) {

        this._configuration =  configuration;
        
        var currentUserSource = this._getCurrentSourceFromCookies();

        if (currentUserSource) {
            this._callSubscriber(currentUserSource);
            return;
        }

        sbjs.init({
            domain: registrableDomain,
            callback: this._getSourceBusterDataCallback.bind(this),
            session_length: this._configuration.lifetimeSourceBuster
        });
    },

    // Подписать на событие "Инициализация источника пользователя"
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    // Функция вызова подписчика
    _callSubscriber(userSource) {
        console.log('Источник:' + userSource);
    },

    // Функция обратного вызова для получения источника
    _getSourceBusterDataCallback: function (sourceBusterData) {

        var userSource = sourceBusterData.current.src;

        if (userSource === "(direct)")
            userSource = this._configuration.defaultUserSource;
        
        this._setSourceToCookie(userSource);
        this._callSubscriber(userSource);
    },

    // Получить источник из куков
    _getCurrentSourceFromCookies: function () {

        var name = this._configuration.cookieKey;

        if (name.length === 0 || document.cookie.length <= 0)
            return undefined;

        var positionStart = document.cookie.indexOf(name + "=");

        if (positionStart === -1)
            return undefined;

        positionStart = positionStart + name.length + 1;
        var positionEnd = document.cookie.indexOf(";", positionStart);

        if (positionEnd === -1)
            positionEnd = document.cookie.length;

        return unescape(document.cookie.substring(positionStart, positionEnd));
    },

    // Записать источник в куки
    _setSourceToCookie(value) {

        var name = this._configuration.cookieKey;

        var date = new Date();
        date.setTime(date.getTime() + (this._configuration.lifeTimeCookies * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires + "; path=/";
    },

    // Конфигурация для настройки 
    // хелпера по определению источника 
    _configuration: null,

    // Дефолтная конфигурация
    _defaultConfiguration: {

        // Ключ куков
        cookieKey: 'SourceBuster',

        // Дефолтное значение источника
        defaultUserSource: 'Прямой переход по ссылке',

        // Время жизни источника(в минутах)
        lifetimeSourceBuster: 1440,

        // Время жизини кук(в месяцах)
        lifeTimeCookies: 1
    }
}