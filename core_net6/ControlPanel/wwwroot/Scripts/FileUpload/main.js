/*
 * jQuery File Upload Plugin JS Example 6.5.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

function func() {
    window.location = window.location;
}

$(function () {
    'use strict';
    var form;
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();

    $('#fileupload').fileupload('option', {
        autoUpload: true,
        maxNumberOfFiles: 1,
        maxFileSize: 2000000000,
        resizeMaxWidth: 1920,
        resizeMaxHeight: 1200,
        dataType: "json",
        acceptFileTypes: /(\.|\/)(dt)$/i,
        done: function(e, data) {
            var json = data.result;
            window.location = json;
        }
    })

        //Bind event callbacks
        .bind('fileuploadsend', function(e, data) {
            console.log('sent');
            $('div#indicator-process').fadeIn().fadeIn("slow").fadeIn(3000);
        })
        .bind('fileuploadfail', function(e, data) {
            console.log('failed');
            $('div#indicator-fail').fadeIn().fadeIn("slow").fadeIn(3000);
        })
        .bind('add', function(e, data) {
            console.log('add action toggled');
            $.each(data.files, function(index, file) {
                alert('Added file: ' + file.name);
                if (!/(\.|\/)(dt)$/i.match(file.name)) {
                    $("span.fileinput-button").removeClass('disabled');
                    $('div#indicator-fail').fadeIn().fadeIn("slow").fadeIn(3000);
                }
            });
        })
        .bind('change', function(e, data) {
            console.log('change action toggled');
            console.log(e.target.accept === 'dt');
            form = e.target.form;
            if (!e.target.files[0].name.match(/(\.|\/)(dt)$/i)) {
                $("span.fileinput-button").removeClass('disabled');
                $('div#indicator-fail').fadeIn().fadeIn("slow").fadeIn(3000);
                setTimeout(func, 7000);
            }
        }).bind('fileuploaddone', function(e, data) {
            console.log('fileuploaddone');
        });

    /*$('#fileupload').bind('fileuploaddone', function (e, data) {
        console.log('done');
    });
    $('#fileupload').bind('fileuploadalways', function (e, data) {
        console.log(data.result);
    });*/
});
