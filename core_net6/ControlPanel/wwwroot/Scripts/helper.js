﻿String.prototype.replaceAll = function(search, replace) {
    return this.split(search).join(replace);
};

Attention = new function () {
    this.Show = function (title, message) {
        $.gritter.add({
            title: title,
            text: message
        });
    }
}

Percents = new function() {
    this.Round = function(value) {
        var values = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100];
        var percent;

        if (value < 5)
            percent = 0;

        $.each(values, function (i, val) {
            if (value >= val)
                percent = val;
        });

        return percent;
    }
}

DateTime = new function() {
    this.ToString = function(h, m, s) {
        var hours = h < 10 ? "0" + h : h;
        var minutes = m < 10 ? "0" + m : m;
        var seconds = s < 10 ? "0" + s : s;

        return hours + ":" + minutes + ":" + seconds;
    }
}

Search = new function () {
    this.InTable = function (input, table) {
        var $rows = $('#'+table+' tbody tr');
        $('#' + input).keyup(function () {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function () {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
    }
}