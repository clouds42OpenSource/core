﻿DateTimePicker = new function() {
    this.Date = function(instance) {
        $("#" + instance).datetimepicker({
            locale: "ru",
            format: "DD.MM.YYYY"
        });
    }
    this.DateTime = function(instance) {
        $("#" + instance).datetimepicker({
            locale: "ru",
            format: "DD.MM.YYYY HH:mm:ss"
        });
    }
}