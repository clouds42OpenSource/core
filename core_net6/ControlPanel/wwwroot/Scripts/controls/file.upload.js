﻿$(function () {
    var wrapper = $(".file_upload"),
        inp = wrapper.find("input"),
        btn = wrapper.find("button"),
        lbl = wrapper.find("div");

    btn.focus(function () {
        inp.focus();
    });
    
    inp.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    inp.change(function () {

        if (inp[0].files[0] === undefined)
            return;

        var fileName = inp[0].files[0].name;

        if (!fileName.length)
            return;

        var accept = inp.attr('accept');

        if (accept != undefined && accept !== '') {
            var formats = accept.split(',');
            var isValid = false;
            var fileFormat = fileName.split(".").pop();
            formats.forEach(function (element) {
                if (`.${fileFormat}` === element)
                    isValid = true;
            });

            if (isValid === false) {
                lbl.text('Выберите файл');
                inp.val("");
                toastr.error(`Выберите другой файл. Допустимые форматы для файла: ${accept}`);
                return;
            }
                
        }

        if (lbl.is(":visible")) {
            lbl.text(fileName);
            btn.html('<i class="fa fa-folder-open" aria-hidden="true"></i>');
        } else
            btn.html('<i class="fa fa-folder-open" aria-hidden="true"></i>');
    }).change();

});
$(window).resize(function () {
    $(".file_upload input").triggerHandler("change");
});
