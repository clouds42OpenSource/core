﻿// Хэлпер для создания базы из ZIP файла
function CreateAcDbFromZipHelper(vmData) {
    var vm = vmData;
    var helper = this;

    // Данные биллинга
    helper.BillingInfo = {

        // Общая стоимость
        TotalCost: ko.observable(0)
    }

    // Формат для элементов выпадающего списка
    helper.FormatDropDownItem = function (item) {
        if (item.text === "Недействительный")
            return $('<span class="text-danger-alt">' + item.text + '</span>');

        return item.text;
    };

    // Провалидировать версию конфигурации загруженного zip файла
    helper.ValidateConfigVersion = function () {
        MaskHelper.ShowBlockUiBody();
        AjaxHelper.submitWithJwtAccessToken({
                type: "GET",
                url: vm.Config.Urls.ValidateConfigVersionInZip,
                data: { uploadedFileId: vm.UploadZipFileProcess.FileUploader.UploadedFileId() }
            },
            function (data) {
                MaskHelper.HideBlockUIBody();
                if (data.Error === true) {
                    var id = 'configError-confirmation';
                    Modal.Close = function () {
                        $("." + id).find("button[data-dismiss]").click(function () {
                            window.location.href = vm.Config.Urls.IndexAccountDatabases;
                        });
                    };
                    Modal.Open({
                        type: ModalType.Default.Normal,
                        id: id,
                        title: '<p style="margin: 10px; font-size: 18px; text-align: center;">Проверка совместимости релиза и конфигурации</p>',
                        body: '<p style="margin: 10px; font-size: 14px;">Уважаемый пользователь!<br /> ' + data.ErrorObject +
                            ' <a href="js:;" onclick="openSupportWindow()">службу поддержки</a> .</p>',
                        buttons: {
                            yes: {
                                label: '<i class="fa fa-check"></i> OK',
                                className: "btn btn-sm btn-default text-uppercase",
                                callback: function () {
                                    window.location.href = vm.Config.Urls.IndexAccountDatabases;
                                }
                            }
                        }
                    });
                    Modal.Close();
                }
                else {
                    helper.InitRowsZipFile();
                }
            });
    };

    // Инициализация списка из zip файла
    helper.InitRowsZipFile = function () {
        MaskHelper.ShowBlockUiBody();
        AjaxHelper.submitWithJwtAccessToken({
            type: "GET",
            url: vm.Config.Urls.GetDatabaseInfoFromZipUrl,
            data: { uploadedFileId: vm.UploadZipFileProcess.FileUploader.UploadedFileId() }
        },
            function (data) {
                MaskHelper.HideBlockUIBody();
                if (data.Error === true) {
                    vm.UploadAcDbFileHelper.ShowUploadErrorMessage(data.ErrorObject);
                } else {
                    vm.UploadZipFileProcess.UploadDatabaseTemplateId(data.Result.TemplateId);
                    vm.UploadZipFileProcess.MyDatabasesServiceTypeId(data.Result.MyDatabasesServiceTypeId);
                    if (data.Result.Users === null || data.Result.Users.length === 0) {
                        var title =
                            '<p style="margin: 10px; font-size: 18px; text-align: center;">' + vm.No1CUsersFoundPhrase + '</p>';
                        Modal.Open({
                            type: ModalType.Default.Normal,
                            title: title,
                            body: '<p style="margin: 10px; font-size: 14px;">Внимание!<br /><br />' +
                                'Нам не удалось обнаружить пользователей в загружаемой базе. <br /><br />' +
                                'Для корректной работы системы мы можем автоматически создать пользователя с правами администратора внутри вашей базы.</p>',
                            buttons: {
                                no: {
                                    label: '<i class="fa fa-ban"></i> Отменить создание',
                                    className: "btn btn-sm btn-default text-uppercase",
                                    callback: function () { window.location.href = vm.Config.Urls.IndexAccountDatabases; }
                                },
                                yes: {
                                    label: '<i class="fa fa-check"></i> Создать и продолжить',
                                    className: "btn btn-sm btn-info text-uppercase",
                                    callback: function () {
                                        var user = helper.InitZipAccountUser({
                                            FullName: "Администратор",
                                            Uid: null,
                                            AccountUserId: ko.observable('null'),
                                            IsAdmin: true,
                                            AccountUsers: ko.observableArray([]),
                                            AccessCost: ko.observable(0)
                                        });
                                        vm.UploadZipFileProcess.AccountZipUsers.push(user);
                                    }
                                }
                            }
                        });
                    } else {
                        var usersList = [];
                        ko.utils.arrayForEach(data.Result.Users,
                            function (item) {
                                var user = helper.InitZipAccountUser(item);
                                usersList.push(user);
                            });
                        vm.UploadZipFileProcess.AccountZipUsers(usersList);
                    }
                }
            });
    };

    // Инициализировать пользователя из ZIP файла
    helper.InitZipAccountUser = function (zipAccountUser) {
        var user = {
            FullName: zipAccountUser.FullName,
            Uid: zipAccountUser.Uid,
            AccountUserId: ko.observable('null'),
            IsAdmin: zipAccountUser.IsAdmin,
            AccountUsers: ko.observableArray([]),
            AccessCost: ko.observable(0)
        };

        ko.utils.arrayForEach(vm.UploadZipFileProcess.AccountUsersList, function (accountItem) {
            user.AccountUsers.push({ id: accountItem.id, text: accountItem.text });
        });

        user.AccountUserId.subscribeChanged(function (newValue, oldValue) {
            accountUserChanged(user, newValue, oldValue);
        });

        return user;
    };

    // Список пользователей из zip файла
    helper.ZipFileAccountUsers = {
        // Удалить пользователя из выбранного сопоставления
        DeleteSelectedUser: function (obj) {
            obj.AccountUserId("null");
            obj.AccessCost(0);
        },

        // Создать нового пользователя
        CreateNewAccountUser: function (obj) {
            $.ajax({
                url: vm.Config.Urls.AccountUsersAdd,
                beforeSend: function () { MaskHelper.ShowBlockUiBody(); },
                success: function (result) {
                    MaskHelper.HideBlockUIBody();
                    Modal.Open({
                        id: "CreateAccountUser_Fast",
                        title: "Создание нового пользователя",
                        body: result,
                        type: ModalType.Default.Large,
                        buttons: {
                            save: {
                                label: '<i class="fa fa-check"></i> Сохранить',
                                className: "btn btn-sm btn-success text-uppercase",
                                callback: function () {
                                    saveNewAccountUser(obj);
                                    return false;
                                }
                            },
                            close: {
                                label: '<i class="fa fa-ban"></i> Закрыть',
                                className: "btn btn-sm btn-default text-uppercase",
                                callback: function () { return true; }
                            }
                        },
                        callback: function () { $(".custom-header-add-user").remove(); }
                    });
                }
            });
        }
    };

    // Подготовить список аккаунт юзеров для выпадающего списка
    helper.InitAccountUsersForDropDownList = function () {
        $.get(vm.Config.Urls.GetAvailableUsersForGrantAccess).done(function (data) {
            vm.UploadZipFileProcess.AccountUsersList = [{ id: "null", text: "Недействительный" }];
            ko.utils.arrayForEach(data.data, function (item) {
                var userDescription = item.UserLogin;
                if (item.UserFullName !== null && item.UserFullName !== undefined && item.UserFullName.length > 0) {
                    userDescription += " (" + item.UserFullName + ")";
                }
                vm.UploadZipFileProcess.AccountUsersList.push({ id: item.UserId, text: userDescription });
            });
        });
    };

    // Коллбэк на изменение выпадающего списка
    function accountUserChanged(account, newValue, oldValue) {
        var oldAccountUser = ko.utils.arrayFirst(vm.UploadZipFileProcess.AccountUsersList, function (child) { return child.id === oldValue; });

        ko.utils.arrayForEach(vm.UploadZipFileProcess.AccountZipUsers(), function (zipAccount) {
            if (zipAccount.Uid !== account.Uid) {
                if (newValue !== "null") {
                    var accountForRemove = ko.utils.arrayFirst(zipAccount.AccountUsers(), function (child) { return child.id === newValue; });
                    zipAccount.AccountUsers.remove(accountForRemove);
                }

                if (oldAccountUser !== null && oldAccountUser.id !== "null") {
                    zipAccount.AccountUsers.push({ id: oldAccountUser.id, text: oldAccountUser.text });
                }
            }
        });

        if (newValue === null || newValue === "null" || newValue === undefined || newValue === oldValue) {
            account.AccessCost(0);
            recalculateAccessesCost();
            return;
        }

        const dataToSend = {
            accountUserDataModelsList: getAccountUsersModels(),
            serviceTypesIdsList: [vm.UploadZipFileProcess.MyDatabasesServiceTypeId()]
        };

        $.ajax({
            url: vm.Config.Urls.CalculateAccessesCostForConfigurations,
            type: 'post',
            data: dataToSend,
            beforeSend: function () { MaskHelper.ShowBlockUiBody(); },
            success: function (result) {
                MaskHelper.HideBlockUIBody();
                if (result.success === true) {
                    ko.utils.arrayForEach(result.data || [],
                        function (item) {
                            const userAccess = getUserAccess(item.AccountUserId);
                            if (userAccess === null || userAccess === undefined) {
                                return;
                            }
                            userAccess.AccessCost(item.AccessCost);
                        });
                    recalculateAccessesCost();
                }
            }
        });

    };

    // Сохранить нового пользователя
    function saveNewAccountUser(account) {
        MaskHelper.ShowBlockUiBody();
        debugger;
        var form = $('#UserAddForm').serializeArray();
        var FormObject = {};
        $.each(form,
            function (i, v) {
                if (v.name.toLowerCase() === 'roles') {
                    if (FormObject.hasOwnProperty(v.name)) {
                        FormObject[v.name].push(v.value);
                    } else {
                        FormObject[v.name] = [];
                        FormObject[v.name].push(v.value);
                    }
                } else {
                    FormObject[v.name] = v.value;
                }
               
            });
        $.ajax({
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(FormObject),
            url: vm.Config.Urls.AccountUsersAdd
        }).done(function (result) {
            MaskHelper.HideBlockUIBody();

            if (!result.success && result.data.html) {
                ko.cleanNode(document.getElementById("custom-container-add-user"));
                $('#custom-container-add-user').html("");
                $('#custom-container-add-user').html(result.data.html);
                $(".custom-header-add-user").remove();
                return;
            }

            if (!result.success && result.data.message) {
                toastr.warning(result.data.message);
                return;
            }

            if (result.success && result.accountUser) {
                Modal.Close("CreateAccountUser_Fast");
                callbackAfterSaveAccountUser(account, result.accountUser);
                return;
            }
        });
    };

    // Обработчик события после успешного сохранения нового пользователя
    function callbackAfterSaveAccountUser(account, newAccountUser) {
        var parsedAccountUserName = parseNewAccountUserName(newAccountUser);

        account.AccountUsers.push({ id: newAccountUser.Id, text: parsedAccountUserName });
        vm.UploadZipFileProcess.AccountUsersList.push({ id: newAccountUser.Id, text: parsedAccountUserName });

        var addAccountUser = ko.utils.arrayFirst(vm.UploadZipFileProcess.AccountUsersList, function (child) { return child.id === newAccountUser.Id; });
        ko.utils.arrayForEach(vm.UploadZipFileProcess.AccountZipUsers(), function (zipAccount) {
            if (zipAccount.Uid !== account.Uid) {
                if (addAccountUser !== null && addAccountUser.id !== "null") {
                    zipAccount.AccountUsers.push({ id: addAccountUser.id, text: addAccountUser.text });
                }
            }
        });
        account.AccountUserId(newAccountUser.Id);
    };

    // Получить полное имя пользователя
    function parseNewAccountUserName(newAccountUser) {
        var fullName = "";

        if (newAccountUser.FirstName !== "" && newAccountUser.FirstName !== null && newAccountUser.FirstName !== undefined)
            fullName += " " + newAccountUser.FirstName;

        if (newAccountUser.MiddleName !== "" && newAccountUser.MiddleName !== null && newAccountUser.MiddleName !== undefined)
            fullName += " " + newAccountUser.MiddleName;

        if (newAccountUser.LastName !== "" && newAccountUser.LastName !== null && newAccountUser.LastName !== undefined)
            fullName += " " + newAccountUser.LastName;

        var text = newAccountUser.Login + (fullName === "" ? "" : " (" + fullName + ")");

        return text;
    };

    // Получить список моделей пользователей
    function getAccountUsersModels() {
        const accountUsersModels = [];
        ko.utils.arrayForEach(vm.UploadZipFileProcess.AccountUsersList,
            function (user) {
                if (user.id === null || user.id === "null") {
                    return;
                }
                accountUsersModels.push({
                    UserId: user.id,
                    HasAccess: userHasAccess(user.id)
                });
            });
        return accountUsersModels;
    };

    // Признак что пользователь имеет доступ
    function userHasAccess(userId) {
        const userAccess = getUserAccess(userId);
        return userAccess !== null;
    };

    // Получить доступ пользователя
    function getUserAccess(userId) {
        return ko.utils.arrayFirst(vm.UploadZipFileProcess.AccountZipUsers(),
            function (user) { return user.AccountUserId() === userId; });
    };

    // Пересчитать стоимость доступов
    function recalculateAccessesCost() {
        var newTotalCost = 0;
        ko.utils.arrayForEach(vm.UploadZipFileProcess.AccountZipUsers(),
            function (acDbAccess) {
                if (acDbAccess.AccountUserId() === null || acDbAccess.AccountUserId() === "null" || acDbAccess.AccountUserId() === undefined) {
                    return;
                }
                newTotalCost += acDbAccess.AccessCost();
            });
        helper.BillingInfo.TotalCost(newTotalCost);
    }
};