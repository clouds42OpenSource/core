﻿// Хэлпер для загрузки файлов инф. базы
function UploadAcDbFileHelper(vmData) {

    var vm = vmData;
    var helper = this;

    // Создать загрузчик файла
    helper.CreateFileUploader = function (fileType) {
        return {

            // Необходимо показывать кнопку загрузки
            NeedShowUploadButton: ko.observable(true),

            // Запущен процесс загрузки файла
            UploadStarted: ko.observable(false),

            // Файл загружен
            FileUploaded: ko.observable(false),

            // Количество загружаемых частей файла
            CountOfUploadedParts: ko.observable(0),

            // Список уже загруженных частей
            AlreadyLoadedParts: ko.observableArray([]),

            // Прогресс загрузки файла
            UploadFileProgress: ko.observable('0%'),

            // Загруженный файл не соответствует требованиям
            FileIsInvalid: ko.observable(false),

            // Сообщение о результате проверки файла
            ValidationErrorMessage: ko.observable(null),

            // ID загруженного файла
            UploadedFileId: ko.observable(null),

            // Заголовок для ID загруженного файла
            HeaderForUploadedFileId: ko.observable(vm.Config.additionalData.headerForUploadedFileId),

            // Запущена склейка файла
            MergeFileProcessStarted: ko.observable(false),

            // Тип файла
            FileType: ko.observable(fileType)
        }
    };

    // Установить значение процента загрузки файла
    helper.SetUploadProgressValue = function (uploadFileProcess) {
        const totalParts = uploadFileProcess.FileUploader.CountOfUploadedParts();
        const alreadyLoadedParts = uploadFileProcess.FileUploader.AlreadyLoadedParts().length;

        const progressValue = ((alreadyLoadedParts * 100) / totalParts).toFixed(0);
        uploadFileProcess.FileUploader.UploadFileProgress(progressValue + '%');
    };

    // Показать сообщение с ошибкой загрузки
    helper.ShowUploadErrorMessage = function (message) {
        var errorMessage =
            '<p style="margin: 10px; font-size: 14px; word-wrap: break-word;">Во время загрузки файла произошла непредвиденная ошибка.<br/>' +
                'Обратитесь пожалуйста в нашу <a href="js:;" onclick="openSupportWindow()">службу поддержки</a>.<br/>' +
                'Сообщение для службы поддержки: ' +
                message +
                '</p>';

        Modal.Open({
            type: ModalType.Default.Normal,
            title: '<p style="margin: 10px; font-size: 16px;">Ошибка загрузки</p>',
            body: errorMessage,
            buttons: {
                yes: {
                    label: '<i class="fa fa-check"></i> OK',
                    className: "btn btn-sm btn-default text-uppercase",
                    callback: function () {
                        window.location.href = vm.Config.Urls.IndexAccountDatabases;
                    }
                }
            }
        });
    };

    // Показать сообщение об ошибке во время проверки файла
    helper.ShowErrorMessage = function (errorMessage, uploadFileProcess) {
        uploadFileProcess.FileUploader.NeedShowUploadButton(true);
        uploadFileProcess.FileUploader.FileIsInvalid(true);
        uploadFileProcess.FileUploader.ValidationErrorMessage(errorMessage);
    };

    // Проверить статус загруженного файла
    helper.CheckFileUploadStatus = function (uploadFileProcess) {
        AjaxHelper.submitWithJwtAccessToken({
                type: "POST",
                url: vm.Config.Urls.CheckFileUploadStatus,
                contentType: false,
                processData: false,
                headers: {
                    'UploadedFileId': uploadFileProcess.FileUploader.UploadedFileId()
                }
            },
            function (result) {
                if (result.Result) {
                    
                    uploadFileProcess.FileUploader.FileUploaded(true);
                    uploadFileProcess.FileUploader.MergeFileProcessStarted(false);
                    helper.CheckLoadFileValidity(uploadFileProcess);
                } else {
                    if (result.ErrorMessage === null) {
                        setTimeout(function () {
                                helper.CheckFileUploadStatus(uploadFileProcess);
                            },
                            15000);
                        return;
                    }
                    helper.ShowUploadErrorMessage(result.ErrorMessage, uploadFileProcess);
                }
            });
    };

    // Запустить процесс склейки частей в исходный файл
    helper.InitMergeChunksToInitialFileProcess = function (uploadFileProcess) {
        AjaxHelper.submitWithJwtAccessToken({
                type: "POST",
                url: vm.Config.Urls.MergeChunksToInitialFile,
                contentType: false,
                processData: false,
                headers: {
                    'UploadedFileId': uploadFileProcess.FileUploader.UploadedFileId(),
                    'CountOfFileParts': uploadFileProcess.FileUploader.CountOfUploadedParts()
                }
            },
            function (result) {
                if (result.Result) {
                    uploadFileProcess.FileUploader.MergeFileProcessStarted(true);
                    helper.CheckFileUploadStatus(uploadFileProcess);
                } else {
                    helper.ShowErrorMessage(result.ErrorMessage, uploadFileProcess);
                }
            });
    };

    // Обработчик после окончания загрузки части файла
    helper.PartLoadedCallback = function (uploadFileProcess) {
        var countOfLoadedParts = uploadFileProcess.FileUploader.AlreadyLoadedParts().length;
        helper.SetUploadProgressValue(uploadFileProcess);

        if (countOfLoadedParts === uploadFileProcess.FileUploader.CountOfUploadedParts()) {
            helper.InitMergeChunksToInitialFileProcess(uploadFileProcess);
        }
    };

    // Загрузить части файла
    helper.UploadFileChunks = function (fileChunks, partCount, uploadFileProcess) {
        var chunk = fileChunks.shift();
        if (chunk === null || chunk === undefined) {
            return;
        }
        var partTokenOfFileChunk = vm.Config.additionalData.partTokenOfFileChunk;

        partCount++;
        var filePartName = uploadFileProcess.UploadFileName() +
            partTokenOfFileChunk +
            partCount +
            "." +
            uploadFileProcess.FileUploader.CountOfUploadedParts();

        var model = new FormData();
        model.append('file', chunk, filePartName);
        AjaxHelper.submitWithJwtAccessToken({

                type: "POST",
                url: vm.Config.Urls.UploadChunkOfFile,
                contentType: false,
                processData: false,
                data: model,
                headers: { 'UploadedFileId': uploadFileProcess.FileUploader.UploadedFileId() }
            },
            function (result) {
                
                if (result.Result === false) {
                    helper.ShowUploadErrorMessage(result.ErrorMessage);
                    return;
                }
                uploadFileProcess.FileUploader.AlreadyLoadedParts.push({
                    PartName: partCount,
                    Success: result.Result
                });
                helper.PartLoadedCallback(uploadFileProcess);
                helper.UploadFileChunks(fileChunks, partCount, uploadFileProcess);
            });
    };

    // Сбросить состояние загрузчика файлов
    helper.ReInitDbFileUploader = function (fileUploader) {
        fileUploader.FileIsInvalid(false);
        fileUploader.NeedShowUploadButton(false);
        fileUploader.UploadStarted(true);
        fileUploader.FileUploaded(false);
        fileUploader.AlreadyLoadedParts.removeAll();
        fileUploader.UploadFileProgress('0%');
        fileUploader.MergeFileProcessStarted(false);
    };

    // Проверить файл на соответствие требованиям
    helper.CheckLoadFileValidity = function (uploadFileProcess) {
        MaskHelper.ShowBlockUiBody();
        AjaxHelper.submitWithJwtAccessToken({
                type: "POST",
                url: vm.Config.Urls.CheckLoadFileValidity,
                contentType: false,
                processData: false,
                headers: { 'UploadedFileId': uploadFileProcess.FileUploader.UploadedFileId() }
            },
            function (result) {
                MaskHelper.HideBlockUIBody();
                if (result.Result) {
                    uploadFileProcess.CreateAccountDatabase();
                } else {
                    helper.ShowErrorMessage(result.ErrorMessage, uploadFileProcess);
                }
            });
    };
}