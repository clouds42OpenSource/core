﻿// Перечисление типов создания инф. баз
const createAcDbTypeEnum = {

    // Одна инф. база
    "SingleDatabase": 0,

    // Множество инф. баз
    "MultipleDatabases": 1,

    // Не относится к созданию базы
    "NoCreation" : 2
}