﻿// Статус доступа к инф. базе
const acDbAccessStateEnum = {

    // Выдан
    "Done": 0,

    // В процессе выдачи
    "ProcessingGrant": 1,

    // В процессе удаления
    "ProcessingDelete": 2,

    // Ошибка
    "Error": 3,

    // Неизвестное состояние
    "Undefined": 4
}