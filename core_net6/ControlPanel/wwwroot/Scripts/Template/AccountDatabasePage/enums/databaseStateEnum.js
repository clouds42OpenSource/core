﻿// Статус инф. базы
const databaseStateEnum = {

    // База в процессе создания
    "NewItem": 1,

    // База данных готова к работе
    "Ready": 2,

    // База не создана, недостаточно места на диске
    "ErrorNotEnoughSpace": 6,

    // База не создана, ошибка создания
    "ErrorCreate": 7,

    // База не создана, ошибка загрузки из dt
    "ErrorDtFormat": 8,

    // Идет выполнение регламентных операций
    "ProcessingSupport": 9,

    // База в состоянии переноса
    "TransferDb": 10,

    // База в очереди на восстановление
    "RestoringFromTomb": 12,

    // База в очереди на удаление в склеп
    "DelitingToTomb": 13,

    // База в очереди на архивацию
    "DetachingToTomb": 14,

    // База удалена в склеп
    "DeletedToTomb": 15,

    // База в архиве
    "DetachedToTomb": 16,

    // База удалена с облака
    "DeletedFromCloud": 17
}