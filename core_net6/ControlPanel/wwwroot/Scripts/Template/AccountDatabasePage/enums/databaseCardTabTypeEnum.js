﻿
// Тип таба карточки информационной базы
const databaseCardTabTypeEnum = {

    // Дефолтный
    Default: 0,

    // Таб общие
    "General": 1,

    // Аторизация/ТиИ
    "AcDbSupport": 2,

    // Архивные копии
    "DatabaseBackups": 3,

    // Доступы к инф. базе
    DatabaseAccesses: 4,

    // Расширения сервиса
    ServiceExtensions: 5
}