﻿// Тип таба карточки информационной базы
const databaseEditOperationTypeEnum = {

    // Смена файлового хранилища
    "ChangeFileStorage": 0,

    // Публикация инф. базы
    "PublishDatabase": 1,

    // Отмена публикации инф. базы
    "CancelPublishDatabase": 2,

    // Перезапуск пула
    "RestarIisAppPool": 3,

    // Отправка в архив
    "ArchiveDatabaseToTomb": 4,

    // Смена версии платформы
    "ChangeDistributionType": 5,

    // Завершить сессии в информационной базе
    "TerminateSessionInDb": 6
}