﻿// Статус публикации инф. базы
const databasePublishStateEnum = {
    "Published": 0,
    "RestartingPool": 4,
    "PendingPublication": 1,
    "PendingUnpublication": 2,
    "Unpublished": 3
};