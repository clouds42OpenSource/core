﻿
// __proto__ таба карточки инф. базы
const DatabaseTabProto = {

    // Таб проинициализирован
    IsInitialized: ko.observable(false),

    // Данные таба
    Data: null,

    // Специальное действие таблицы для таба
    // (при открытии карточки передается действие для таба)
    SpecialTableAction: null,

    // Установить исходные/начальные данные модели таба
    SetInitialData: function () {
        throw "Не реализована функция по установке исходных данных";
    },

    // Обновить данные
    UpdateData: function (callbackFunc) {
        throw "Не реализована функция по обновлению данных";
    },

    // Подписка на изменение данных таба (Выполняется после обновления данных)
    SubscribeToChanges: function () {
        // Не обязательная функция. Базовой реализации нет
    },

    // Выполнить бизнес процессы над новыми данными (Выполняется после обновления данных)
    PerformBusinessProcessesOnNewData: function() {
        // Не обязательная функция. Базовой реализации нет
    },

    // Выполинть инициализацию DOM элементов (Выполняется до обновления данных)
    PerformInitializationDomElements: function () {
        // Не обязательная функция. Базовой реализации нет
    },

    // Заполнить данные фильтра дефолными значениями (Выполняется до обновления данных)
    FillFilterDataWithDefaultValues: function() {
        // Не обязательная функция. Базовой реализации нет
    },

    // Инициализировать данные таба
    InitializeTabData: function (forceUpdate, callbackFunc) {

        forceUpdate = forceUpdate || false;

        if (!this.IsInitialized() || forceUpdate) {
            this._performTabUpdate(callbackFunc);
            return;
        }

        if (this.IsInitialized() && callbackFunc)
            callbackFunc();
    },

    // Инициализировать данные таба с очищением старых данных
    InitializeTabDataWithClearingOldData: function (forceUpdate, callbackFunc) {

        if (this.IsInitialized())
            this.ResetTabData();

        this.InitializeTabData(forceUpdate, callbackFunc);
    },

    // Сбросить данные таба
    ResetTabData: function () {

        this.IsInitialized(false);
    },

    // Выполнить обновление таба
    _performTabUpdate: function (callbackFunc) {

        const isInitialized = this.IsInitialized.bind(this);
        const subscribeToChanges = this.SubscribeToChanges.bind(this);
        const performBusinessProcessesOnNewData = this.PerformBusinessProcessesOnNewData.bind(this);
        this.PerformInitializationDomElements();
        this.FillFilterDataWithDefaultValues();

        this.UpdateData(function() {
            isInitialized(true);
            subscribeToChanges();
            performBusinessProcessesOnNewData();
            if (callbackFunc) {
                callbackFunc();    
            }
        });
    }
}