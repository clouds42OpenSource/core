﻿
// Хелпер для работы с моделью данных таба "Архивные копии(Бэкапы инф. базы)"
const databaseBackupsTabDataModelHelper = {

    // Обновить данные модели таба "Архивные копии(Бэкапы инф. базы)" (старая модель имеет observable поля)
    updateData: function (oldModel, newModel) {

        oldModel.DbBackupsCount(newModel.DbBackupsCount);
        oldModel.DatabaseBackups.removeAll();

        const dateTimeFormat = this._dateTimeFormat;

        ko.utils.arrayForEach(newModel.DatabaseBackups || [],
            function (item) {
                oldModel.DatabaseBackups.push({

                    // Id бекапа
                    Id: item.Id,

                    // Инициатор
                    Initiator: item.Initiator,

                    // Дата создания бекапа
                    CreationDate: CommonHelper.DateTimeToDefaultFormat(item.CreationDate, dateTimeFormat),

                    // Путь к бекапу
                    BackupPath: item.BackupPath,

                    // Тригер события создания бэкапа
                    EventTrigger: item.EventTrigger,

                    // Описание триггера создания бекапа
                    EventTriggerDescription: item.EventTriggerDescription,

                    // База на разделителях
                    IsDbOnDelimiters: item.IsDbOnDelimiters,

                    // Источник хранилища бэкапа
                    SourceType: item.SourceType
                });
            });
    },

    // Получить исходные/начальные данные модели таба "Архивные копии(Бэкапы инф. базы)"
    getInitialData: function () {

        return {

            // Количество бэкапов базы
            DbBackupsCount: ko.observable(0),

            // Бэкапы инф. базы
            DatabaseBackups: ko.observableArray([])
        };
    },

    // Формат времени
    _dateTimeFormat: 'YYYY.MM.DD HH:mm:ss'
}