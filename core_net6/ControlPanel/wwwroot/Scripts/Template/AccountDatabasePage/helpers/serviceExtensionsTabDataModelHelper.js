﻿
// Хелпер для работы с моделью данных таба "Расширения сервиса для инф. базы"
const serviceExtensionsTabDataModelHelper = {

    // Обновить данные модели таба "Расширения сервиса" (старая модель имеет observable поля)
    updateData: function (oldModel, newModel, databaseId) {

        oldModel.ServiceExtensions.removeAll();

        const getDefaultDateFormat = this.getDefaultDateFormat.bind(this);

        ko.utils.arrayForEach(newModel || [],
            function (item) {

                oldModel.ServiceExtensions.push({

                    // Id сервиса
                    Id: ko.observable(item.Id),

                    // Название сервиса
                    Name: ko.observable(item.Name),

                    // Краткое описание сервиса
                    ShortDescription: ko.observable(item.ShortDescription),

                    // Последняя дата активности расширения
                    ExtensionLastActivityDate: ko.observable(getDefaultDateFormat(item.ExtensionLastActivityDate)),

                    // Состояние расширения
                    ExtensionState: ko.observable(item.ExtensionState),

                    // Признак что расширение установлено
                    IsInstalled: ko.observable(item.IsInstalled),

                    // Признак что сервис активирован
                    IsServiceActivated: ko.observable(item.IsServiceActivated),

                    // Признак что чекбокс доступен
                    IsCheckboxEnabled: ko.observable(false),

                    // Id инф. базы
                    DatabaseId: ko.observable(databaseId)
                });
            });
    },

    // Получить исходные/начальные данные модели таба "Расширения сервиса для инф. базы"
    getInitialData: function () {

        return {

            // Расширения сервиса
            ServiceExtensions: ko.observableArray([]),

            // Интервалы опроса сервера
            ServerPollingIntervals: []
        };
    },

    // Получить дефолтный формат даты
    getDefaultDateFormat: function(date) {

        return CommonHelper.DateTimeToDefaultFormat(
            date,
            this._defaultDateFormat);
    },

    // Дефолтный формат даты
    _defaultDateFormat: 'DD-MM-YY HH:mm'
}