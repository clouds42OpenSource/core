﻿
// Общий хелпер для обновления данных инф. базы
const updateDatabaseDataCommonHelper = {

    // Обновить значение массива
    updateArrayValues: function (oldArray, newArray) {

        oldArray.removeAll();

        ko.utils.arrayForEach(newArray || [],
            function (item) {
                oldArray.push(item);
            });
    }

}