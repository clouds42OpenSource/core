﻿
// Хелпер для работы с моделью данных таба "Доступы к инф. базе"
const databaseAccessesTabDataModelHelper = {

    // Обновить данные модели таба "Доступы к инф. базе" (старая модель имеет observable поля)
    updateData: function (oldModel, newModel) {

        oldModel.DatabaseAccesses.removeAll();

        const getStateDescription = this._getStateDescription.bind(this);
        ko.utils.arrayForEach(newModel.DatabaseAccesses || [],
            function (item) {
                oldModel.DatabaseAccesses.push({

                    // Id пользователя
                    UserId: item.UserId,

                    // Логин пользователя
                    UserLogin: item.UserLogin,

                    // ФИО пользователя
                    UserFullName: item.UserFullName,

                    // Фамилия пользователя
                    UserLastName: item.UserLastName,

                    // Имя пользователя
                    UserFirstName: item.UserFirstName,

                    // Отчество пользователя
                    UserMiddleName: item.UserMiddleName,

                    // Элекстронный адрес пользователя
                    UserEmail: item.UserEmail,

                    // Доступ является внешним(к базе чужого аккаунта)
                    IsExternalAccess: item.IsExternalAccess,

                    // Есть доступ(имеется запись в базе)
                    HasAccess: ko.observable(item.HasAccess),

                    // Статус доступа (AccountDatabaseAccessState Enum)
                    State: ko.observable(item.State),

                    // Причина задержки выдачи доступа
                    DelayReason: ko.observable(item.DelayReason),

                    // Номер аккаунта пользователя
                    AccountIndexNumber: ko.observable(item.AccountIndexNumber),

                    // Название аккаунта пользователя
                    AccountCaption: ko.observable(item.AccountCaption),

                    // Информация об аккаунте пользователя
                    AccountInfo: ko.observable(item.AccountInfo),

                    // Описание статуса
                    StateDescription: ko.observable(getStateDescription(item.State)),

                    // Признак что есть лицензия на конфигурации
                    HasLicenseForConfigurations: ko.observable(false),

                    // Стоимость доступа
                    AccessCost: ko.observable(0)
                });
            });
    },

    // Получить модель внешнего доступа
    getExternalAccessModel: function (accessItem) {
        const getStateDescription = this._getStateDescription.bind(this);
        return {
            // Id пользователя
            UserId: accessItem.UserId,

            // Логин пользователя
            UserLogin: accessItem.UserLogin,

            // ФИО пользователя
            UserFullName: accessItem.UserFullName,

            // Фамилия пользователя
            UserLastName: accessItem.UserLastName,

            // Имя пользователя
            UserFirstName: accessItem.UserFirstName,

            // Отчество пользователя
            UserMiddleName: accessItem.UserMiddleName,

            // Элекстронный адрес пользователя
            UserEmail: accessItem.UserEmail,

            // Доступ является внешним(к базе чужого аккаунта)
            IsExternalAccess: true,

            // Есть доступ(имеется запись в базе)
            HasAccess: ko.observable(accessItem.HasAccess),

            // Статус доступа (AccountDatabaseAccessState Enum)
            State: ko.observable(acDbAccessStateEnum.Undefined),

            // Причина задержки выдачи доступа
            DelayReason: ko.observable(accessItem.DelayReason),

            // Номер аккаунта пользователя
            AccountIndexNumber: ko.observable(accessItem.AccountIndexNumber),

            // Название аккаунта пользователя
            AccountCaption: ko.observable(accessItem.AccountCaption),

            // Информация об аккаунте пользователя
            AccountInfo: ko.observable(accessItem.AccountInfo),

            // Описание статуса
            StateDescription: ko.observable(getStateDescription(acDbAccessStateEnum.Undefined)),

            // Признак что есть лицензия на конфигурации
            HasLicenseForConfigurations: ko.observable(false),

            // Стоимость доступа
            AccessCost: ko.observable(0)
        }
    },

    // Получить исходные/начальные данные модели таба "Доступы к инф. базе"
    getInitialData: function () {

        return {

            // Доступы к инф. базе
            DatabaseAccesses: ko.observableArray([])
        };
    },

    // Сформировать модели пользователя в процессе обработки пользователя
    updateUserInAccessManageProcessingModels: function (databaseAccesses, userInAccessManageProcessingModels, databaseId) {

        userInAccessManageProcessingModels.removeAll();

        ko.utils.arrayForEach(databaseAccesses,
            function (item) {

                var operation = item.State() === 1
                ? 0
                : item.State() === 2
                ? 1
                : null;
                
                userInAccessManageProcessingModels.push({
                    AccountDatabaseId: ko.observable(databaseId),
                    AccountUserId: ko.observable(item.UserId),
                    Operation: ko.observable(operation),
                    IsExternal: ko.observable(item.IsExternalAccess),
                    AccessState: ko.observable(item.State()),
                    DelayReason: ko.observable(item.DelayReason())
                });
            });
    },

    // Обновить данные для доступа
    updateAccessData: function (access, data) {

        access.State(data.AccessState);
        access.DelayReason(data.DelayReason);
    },

    // Получить описание состояния доступа пользователя
    _getStateDescription: function (state) {

        if (state === 0 || state === 4) {
            return null;
        }

        switch (state) {
        case 2:
            return "В процессе удаления";
        case 1:
            return "В процессе выдачи";
        case 3:
            return "Ошибка выполнения операции";
        default:
            return "Идет обработка. Пожалуйста, подождите.";
        }
    }
}