﻿// Хэлпер изменения инф. баз
function ChangeAccountDatabaseHelper(helperConfig, vmData) {
    var vm = vmData;
    var helper = this;
    var needHandleEvents = vm.GeneralTab.IsInitialized;

    // Конфигурация
    helper.Config = helperConfig;

    helper.Intervals = [];

    // Создать редакторы инф. базы
    helper.CreateDbEditors = function () {
        vm.GeneralTab.Data.Editors.CaptionEditor = {
            Value: vm.GeneralTab.Data.Caption,
            InitialValue: ko.observable(vm.GeneralTab.Data.Caption())
        };
        vm.GeneralTab.Data.Editors.V82NameEditor = {
            Value: vm.GeneralTab.Data.V82Name,
            InitialValue: ko.observable(vm.GeneralTab.Data.V82Name())
        };
        vm.GeneralTab.Data.Editors.HasModificationsEditor = {
            Value: vm.GeneralTab.Data.HasModifications,
            InitialValue: ko.observable(vm.GeneralTab.Data.HasModifications())
        };
        vm.GeneralTab.Data.Editors.DbTemplateEditor = {
            Value: vm.GeneralTab.Data.DbTemplate,
            InitialValue: ko.observable(vm.GeneralTab.Data.DbTemplate())
        };
        vm.GeneralTab.Data.Editors.PlatformTypeEditor = {
            Value: vm.GeneralTab.Data.PlatformType,
            InitialValue: ko.observable(vm.GeneralTab.Data.PlatformType())
        };
        vm.GeneralTab.Data.Editors.DistributionTypeEditor = {
            Value: vm.GeneralTab.Data.DistributionType,
            InitialValue: ko.observable(vm.GeneralTab.Data.DistributionType()),
            NewValue: ko.observable(null),
            NeedShowLoader: function () {
                return vm.GeneralTab.Data.Editors.DistributionTypeEditor.NewValue() !== null &&
                    vm.GeneralTab.Data.Editors.DistributionTypeEditor.NewValue() !==
                    vm.GeneralTab.Data.Editors.DistributionTypeEditor.InitialValue();
            }
        };
        vm.GeneralTab.Data.Editors.DbStateEditor = {
            Value: vm.GeneralTab.Data.DatabaseState,
            InitialValue: ko.observable(vm.GeneralTab.Data.DatabaseState())
        };
        vm.GeneralTab.Data.Editors.RestoreModelTypeEditor = {
            Value: vm.GeneralTab.Data.RestoreModelType,
            InitialValue: ko.observable(vm.GeneralTab.Data.RestoreModelType())
        };
        vm.GeneralTab.Data.Editors.UsedWebServicesEditor = {
            Value: vm.GeneralTab.Data.UsedWebServices,
            InitialValue: ko.observable(vm.GeneralTab.Data.UsedWebServices())
        };
        vm.GeneralTab.Data.Editors.FileStorageEditor = {
            Value: vm.GeneralTab.Data.FileStorageId,
            InitialValue: ko.observable(vm.GeneralTab.Data.FileStorageId()),
            NewValue: ko.observable(null),
            NeedShowLoader: function () {
                return this.NewValue() !== null &&
                    this.NewValue() !== this.InitialValue();
            }
        };
        vm.GeneralTab.Data.Editors.IsEditorsInitialized(true);
        helper.InitDbEditors();
    };

    // Обновить редакторы инф. базы
    helper.UpdateDbEditors = function () {
        vm.GeneralTab.Data.Editors.CaptionEditor.InitialValue(vm.GeneralTab.Data.Caption());
        vm.GeneralTab.Data.Editors.V82NameEditor.InitialValue(vm.GeneralTab.Data.V82Name());
        vm.GeneralTab.Data.Editors.HasModificationsEditor.InitialValue(vm.GeneralTab.Data.HasModifications());
        vm.GeneralTab.Data.Editors.DbTemplateEditor.InitialValue(vm.GeneralTab.Data.DbTemplate());
        vm.GeneralTab.Data.Editors.PlatformTypeEditor.InitialValue(vm.GeneralTab.Data.PlatformType());
        vm.GeneralTab.Data.Editors.DistributionTypeEditor.InitialValue(vm.GeneralTab.Data.DistributionType());
        vm.GeneralTab.Data.Editors.DbStateEditor.InitialValue(vm.GeneralTab.Data.DatabaseState());
        vm.GeneralTab.Data.Editors.RestoreModelTypeEditor.InitialValue(vm.GeneralTab.Data.RestoreModelType());
        vm.GeneralTab.Data.Editors.UsedWebServicesEditor.InitialValue(vm.GeneralTab.Data.UsedWebServices());
        vm.GeneralTab.Data.Editors.FileStorageEditor.InitialValue(vm.GeneralTab.Data.FileStorageId());
        helper.ClearValidationMessages();
    };

    // Инициализировать редакторы инф. базы
    helper.InitDbEditors = function () {
        if (!vm.GeneralTab.Data.Editors.IsEditorsInitialized()) {
            return;
        }
        helper.InitCaptionEditor();
        helper.InitV82NameEditor();
        helper.InitDbHasModificationsEditor();
        helper.InitDbUsedWebServicesEditor();
        helper.InitDbStateEditor();
        helper.InitRestoreModelTypeEditor();
        helper.InitDistributionTypeEditor();
        helper.InitDbFileStorageEditor();
        helper.InitPlatformTypeEditor();
        helper.InitDbTemplateEditor();
    };

    // Инициализировать редактор названия инф. базы
    helper.InitCaptionEditor = function () {
        vm.GeneralTab.Data.Editors.CaptionEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.CaptionEditor) === false)
                return;

            const validationResult =
                helper.DbValidators.ValidateDbCaption(oldValue);
            if (validationResult === false)
                return;

            const params = {
                databaseId: vm.TabsCommonData.DatabaseId(),
                caption: vm.GeneralTab.Data.Caption()
            };

            apiHelper.sendBasePostRequest(
                vm.Config.Urls.ChangeDbCaptionUrl,
                vm.ContentId,
                params,
                function () {
                    vm.GeneralTab.ResetTabData();
                    vm.GeneralTab.InitializeTabData();
                    helper.ChangeDbNameInDataGrid();
                    toastr.success("Данные успешно сохранены!");
                });
        });
    };

    // Инициализировать редактор номер инф. базы
    helper.InitV82NameEditor = function () {
        vm.GeneralTab.Data.Editors.V82NameEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.V82NameEditor) === false)
                return;

            const validationResult =
                helper.DbValidators.ValidateV82Name(oldValue);
            if (!validationResult) {
                return;
            }

            const params = {
                databaseId: vm.TabsCommonData.DatabaseId(),
                v82Name: vm.GeneralTab.Data.V82Name()
            };

            apiHelper.sendBasePostRequest(
                vm.Config.Urls.ChangeDbV82NameUrl,
                vm.ContentId,
                params,
                function () {
                    vm.GeneralTab.ResetTabData();
                    vm.GeneralTab.InitializeTabData();
                    helper.ChangeDbNameInDataGrid();
                    toastr.success("Данные успешно сохранены!");
                });
        });
    };

    // Инициализировать редактор флага наличия доработок инф. базы
    helper.InitDbHasModificationsEditor = function () {
        vm.GeneralTab.Data.Editors.HasModificationsEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.HasModificationsEditor) === false)
                return;

            var url = vm.Config.Urls.ChangeDatabaseHasModificationsFlag +
                "?databaseId=" +
                vm.TabsCommonData.DatabaseId() +
                "&hasModifications=" +
                newValue;

            apiHelper.sendBaseGetRequest(
                url,
                vm.ContentId,
                function () {
                    vm.GeneralTab.ResetTabData();
                    vm.GeneralTab.InitializeTabData();
                    toastr.success("Данные успешно сохранены!");
                });
        });
    };

    // Инициализировать редактор флага веб сервисов инф. базы
    helper.InitDbUsedWebServicesEditor = function () {
        vm.GeneralTab.Data.Editors.UsedWebServicesEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.UsedWebServicesEditor) === false)
                return;

            const params = {
                databaseId: vm.TabsCommonData.DatabaseId(),
                usedWebServices: newValue
            };

            apiHelper.sendBasePostRequest(
                vm.Config.Urls.ChangeDbFlagUsedWebServicesUrl,
                vm.ContentId,
                params,
                function () {
                    vm.GeneralTab.ResetTabData();
                    vm.GeneralTab.InitializeTabData();
                    toastr.success("Данные успешно сохранены!");
                });
        });
    };

    // Инициализировать редактор статуса инф. базы
    helper.InitDbStateEditor = function () {
        vm.GeneralTab.Data.Editors.DbStateEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.DbStateEditor) === false)
                return;

            const params = {
                databaseId: vm.TabsCommonData.DatabaseId(),
                databaseState: newValue
            };

            apiHelper.sendBasePostRequest(
                vm.Config.Urls.ChangeDatabaseStateUrl,
                vm.ContentId,
                params,
                function () {
                    vm.GeneralTab.ResetTabData();
                    vm.GeneralTab.InitializeTabData();
                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                    toastr.success("Данные успешно сохранены!");
                });
        });
    };

    // Инициализировать редактор модели восстановления инф. базы
    helper.InitRestoreModelTypeEditor = function () {
        vm.GeneralTab.Data.Editors.RestoreModelTypeEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.RestoreModelTypeEditor) === false)
                return;

            var id = 'restoreModel-change-confirmation';

            Modal.Close = function () {
                $("." + id).find("button[data-dismiss]").click(function () {
                    vm.GeneralTab.Data.RestoreModelType(oldValue);
                });
            };

            Modal.Open({
                type: ModalType.Default.Normal,
                title: "Смена модели восстановления для информационной базы",
                body: "Вы действительно хотите сменить модель восстановления для текущей информационной базы?",
                id: id,
                buttons: {
                    yes: {
                        label: '<i class="fa fa-check"></i> Подтвердить',
                        className: "btn btn-sm btn-success text-uppercase",
                        callback: function () {
                            var url = vm.Config.Urls.ChangeDatabaseRestoreModelType +
                                "?databaseId=" +
                                vm.TabsCommonData.DatabaseId() +
                                "&restoreModelType=" +
                                newValue;

                            apiHelper.sendBaseGetRequest(
                                url,
                                vm.ContentId,
                                function () {
                                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                                    vm.GeneralTab.ResetTabData();
                                    vm.GeneralTab.InitializeTabData();
                                    toastr.success("Данные успешно сохранены!");
                                });
                        }
                    },
                    no: {
                        label: '<i class="fa fa-ban"></i> Отмена',
                        className: "btn btn-sm btn-default text-uppercase",
                        callback: function () {
                            vm.GeneralTab.Data.RestoreModelType(oldValue);
                            return true;
                        }
                    }
                }
            });
            Modal.Close();
        });
    };

    // Инициализировать редактор шаблона инф. базы
    helper.InitDbTemplateEditor = function () {
        vm.GeneralTab.Data.Editors.DbTemplateEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.DbTemplateEditor) === false)
                return;
            var id = 'template-change-confirmation';

            Modal.Close = function () {
                $("." + id).find("button[data-dismiss]").click(function () {
                    vm.GeneralTab.Data.DbTemplate(oldValue);
                });
            };

            Modal.Open({
                type: ModalType.Default.Normal,
                title: "Смена шаблона для информационной базы",
                body: "Вы действительно хотите сменить шаблон для текущей информационной базы?",
                id: id,
                buttons: {
                    yes: {
                        label: '<i class="fa fa-check"></i> Подтвердить',
                        className: "btn btn-sm btn-success text-uppercase",
                        callback: function () {
                            var url = vm.Config.Urls.ChangeTemplate +
                                "?databaseId=" +
                                vm.TabsCommonData.DatabaseId() +
                                "&templateId=" +
                                newValue;

                            apiHelper.sendBaseGetRequest(
                                url,
                                vm.ContentId,
                                function () {
                                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                                    vm.GeneralTab.ResetTabData();
                                    vm.GeneralTab.InitializeTabData();
                                    toastr.success("Данные успешно сохранены!");
                                });
                        }
                    },
                    no: {
                        label: '<i class="fa fa-ban"></i> Отмена',
                        className: "btn btn-sm btn-default text-uppercase",
                        callback: function () {
                            vm.GeneralTab.Data.DbTemplate(oldValue);
                            return true;
                        }
                    }
                }
            });
            Modal.Close();
        });
    };

    // Очистить сообщения валидации
    helper.ClearValidationMessages = function () {
        helper.Alerts.captionAlert.message('');
        helper.Alerts.v82NameAlert.message('');
    };

    // Сменить тип платформы инф. базы
    helper.InitPlatformTypeEditor = function () {
        vm.GeneralTab.Data.Editors.PlatformTypeEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.PlatformTypeEditor) === false)
                return;
            var id = 'platform-change-confirmation';

            Modal.Close = function () {
                $("." + id).find("button[data-dismiss]").click(function () {
                    vm.GeneralTab.Data.PlatformType(oldValue);
                });
            }

            Modal.Open({
                type: ModalType.Default.Normal,
                title: "Смена платформы для информационной базы",
                body: "Вы действительно хотите сменить платформу для текущей информационной базы?",
                id: id,
                buttons: {
                    yes: {
                        label: '<i class="fa fa-check"></i> Подтвердить',
                        className: "btn btn-sm btn-success text-uppercase",
                        callback: function () {
                            var url = vm.Config.Urls.ChangePlatformType +
                                "?databaseId=" +
                                vm.TabsCommonData.DatabaseId() +
                                "&platformType=" +
                                parseInt(newValue);

                            apiHelper.sendBaseGetRequest(
                                url,
                                vm.ContentId,
                                function () {
                                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                                    vm.GeneralTab.ResetTabData();
                                    vm.GeneralTab.InitializeTabData();
                                    toastr.success("Данные успешно сохранены!");
                                });
                        }
                    },
                    no: {
                        label: '<i class="fa fa-ban"></i> Отмена',
                        className: "btn btn-sm btn-default text-uppercase",
                        callback: function () {
                            vm.GeneralTab.Data.PlatformType(oldValue);
                            return true;
                        }
                    }
                }
            });
            Modal.Close();
        });
    };

    // Инициализировать редактор типа распространения
    helper.InitDistributionTypeEditor = function () {
        vm.GeneralTab.Data.Editors.DistributionTypeEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.DistributionTypeEditor) === false)
                return;
            var id = 'platform-version-change-confirmation';

            Modal.Close = function () {
                $("." + id).find("button[data-dismiss]").click(function () {
                    vm.GeneralTab.Data.DistributionType(oldValue);
                });
            };

            Modal.Open({
                type: ModalType.Default.Normal,
                title: "Смена релиза платформы для информационной базы",
                id: id,
                body: '<p>Вы действительно хотите сменить релиз платформы для </p>' +
                    "\'" +
                    vm.GeneralTab.Data.Caption() +
                    "\' ?",
                buttons: {
                    yes: {
                        label: '<i class="fa fa-check"></i> Подтвердить',
                        className: "btn btn-sm btn-success text-uppercase",
                        callback: function () {
                            var url = vm.Config.Urls.ChangePlatformType +
                                "?databaseId=" +
                                vm.TabsCommonData.DatabaseId() +
                                "&platformType=" +
                                parseInt(vm.GeneralTab.Data.PlatformType()) +
                                "&distributionType=" +
                                newValue;

                            apiHelper.sendBaseGetRequest(
                                url,
                                vm.ContentId,
                                function () {
                                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                                    vm.GeneralTab.Data.Editors.DistributionTypeEditor.NewValue(newValue);
                                    helper.CreateInterval(databaseEditOperationTypeEnum.ChangeDistributionType,
                                        function () {
                                            return vm.GeneralTab.Data.Editors.DistributionTypeEditor.NewValue() ===
                                                vm.GeneralTab.Data.Editors.DistributionTypeEditor.InitialValue();
                                        }
                                    );
                                    toastr.success("Данные успешно сохранены!");
                                });
                        }
                    },
                    no: {
                        label: '<i class="fa fa-ban"></i> Отмена',
                        className: "btn btn-sm btn-default text-uppercase",
                        callback: function () {
                            vm.GeneralTab.Data.DistributionType(oldValue);
                            return true;
                        }
                    }
                }
            });
            Modal.Close();
        });
    };

    // Сменить файловое хранилище инф. базы
    helper.InitDbFileStorageEditor = function () {
        vm.GeneralTab.Data.Editors.FileStorageEditor.Value.subscribeChanged(function (newValue, oldValue) {
            if (helper.NeedHandleChangeEvent(newValue, oldValue, vm.GeneralTab.Data.Editors.FileStorageEditor) === false)
                return;
            var id = 'file-storage-change-confirmation';

            Modal.Close = function () {
                $("." + id).find("button[data-dismiss]").click(function () {
                    vm.GeneralTab.Data.FileStorageId(oldValue);
                });
            };

            Modal.Open({
                type: ModalType.Default.Normal,
                title: "Смена хранилища для информационной базы",
                body: "Вы действительно хотите сменить хранилище для текущей информационной базы?",
                id: id,
                buttons: {
                    yes: {
                        label: '<i class="fa fa-check"></i> Подтвердить',
                        className: "btn btn-sm btn-success text-uppercase",
                        callback: function () {
                            var url = vm.Config.Urls.ChangeFileStorage +
                                "?databaseId=" +
                                vm.TabsCommonData.DatabaseId() +
                                "&fileStorageId=" +
                                newValue;

                            apiHelper.sendBaseGetRequest(
                                url,
                                vm.ContentId,
                                function () {
                                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                                    vm.GeneralTab.Data.Editors.FileStorageEditor.NewValue(newValue);
                                    helper.CreateInterval(databaseEditOperationTypeEnum.ChangeFileStorage,
                                        function () {
                                            return vm.GeneralTab.Data.Editors.FileStorageEditor.NewValue() ===
                                                vm.GeneralTab.Data.Editors.FileStorageEditor.InitialValue();
                                        }
                                    );
                                    toastr.success("Данные успешно сохранены!");
                                });
                        }
                    },
                    no: {
                        label: '<i class="fa fa-ban"></i> Отмена',
                        className: "btn btn-sm btn-default text-uppercase",
                        callback: function () {
                            vm.GeneralTab.Data.FileStorageId(oldValue);
                            return true;
                        }
                    }
                }
            });
            Modal.Close();
        });
    };

    // Отменить публикацию инф. базы
    helper.CancelPublishDatabase = function () {
        var delegate = function () {
            var url = vm.Config.Urls.CancelPublishDatabaseUrl +
                "?databaseId=" +
                vm.TabsCommonData.DatabaseId();

            apiHelper.sendBaseGetRequest(
                url,
                vm.ContentId,
                function () {
                    Attention.Show("База поставлена в очередь на снятие с публикации", 'Через некоторое время она будет снята с публикации');
                    helper.ChangeDbPublishState(databasePublishStateEnum.PendingUnpublication);
                    vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                    helper.CreateInterval(databaseEditOperationTypeEnum.CancelPublishDatabase,
                        function () {
                            return vm.GeneralTab.Data.PublishState() === databasePublishStateEnum.Unpublished;
                        }
                    );
                });
        }

        if (vm.GeneralTab.Data.UsedWebServices() === true) {

            var message = document.createElement("p");
            message.innerHTML = 'Уважаемый пользователь. <br/>' +
                "В информационной базе опубликованы веб сервисы. После отмены публикации они будут не доступны. Для повторной публикации веб сервисов обратитесь в техподдержку";

            swal3B({
                title: 'Подтверждение отмены публикации',
                content: message,
                icon: "warning",
                buttons: {
                    close: {
                        text: 'Нет',
                        className: "btn btn-default",
                        value: 'close'
                    },
                    confirm: {
                        text: 'Да',
                        className: "btn btn-success",
                        value: 'confirm'
                    }
                }
            })
                .then(function (value) {
                    switch (value) {
                        case "confirm":
                            delegate();
                            break;
                        default:
                            break;
                    }
                });
        } else {
            delegate();
        }
    };

    // Опубликовать инф. базу
    helper.PublishDatabase = function () {
        const params = {
            databaseId: vm.TabsCommonData.DatabaseId()
        };

        apiHelper.sendBasePostRequest(
            vm.Config.Urls.PublishDatabaseUrl,
            vm.ContentId,
            params,
            function () {
                Attention.Show("База поставлена в очередь на публикацию", 'Через некоторое время она будет опубликована');
                helper.ChangeDbPublishState(databasePublishStateEnum.PendingPublication);
                vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                helper.CreateInterval(databaseEditOperationTypeEnum.PublishDatabase,
                    function () {
                        return vm.GeneralTab.Data.PublishState() === databasePublishStateEnum.Published;
                    }
                );
            });
    };

    // Перезапустить пул на сервере публикаций
    helper.RestartIisAppPool = function () {
        const params = {
            databaseId: vm.TabsCommonData.DatabaseId()
        };

        apiHelper.sendBasePostRequest(
            vm.Config.Urls.RestartAppPoolOnIis,
            vm.ContentId,
            params,
            function () {
                Attention.Show("Задача создана",
                    "Спустя некоторое время пул будет перезапущен.");
                helper.ChangeDbPublishState(databasePublishStateEnum.RestartingPool);
                vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                helper.CreateInterval(databaseEditOperationTypeEnum.RestarIisAppPool,
                    function () {
                        return vm.GeneralTab.Data.PublishState() === databasePublishStateEnum.Published;
                    }
                );
            });
    };

    // Сменить название инф. базы в таблице
    helper.ChangeDbNameInDataGrid = function () {

        var isNameIncludesNumber = vm.TabsCommonData.DbObjectFromTable().Name().includes(vm.GeneralTab.Data.V82Name());

        if (!isNameIncludesNumber) {
            vm.TabsCommonData.DbObjectFromTable().Name(vm.GeneralTab.Data.Caption());
            return;
        }

        const dbName = vm.GeneralTab.Data.Caption() + " (" + vm.GeneralTab.Data.V82Name() + ")";
        vm.TabsCommonData.DbObjectFromTable().Name(dbName);
    };

    // Сменить статус публикации инф. базы
    helper.ChangeDbPublishState = function (publishState) {
        vm.TabsCommonData.DbObjectFromTable().PublishState(publishState);
        const needShowWebLink = publishState === databasePublishStateEnum.Published;
        vm.TabsCommonData.DbObjectFromTable().NeedShowWebLink(needShowWebLink);
        vm.GeneralTab.Data.PublishState(publishState);
    };

    // Сменить статус инф. базы
    helper.ChangeDbState = function (databaseState) {
        vm.GeneralTab.Data.DatabaseState(databaseState);
        vm.TabsCommonData.DbObjectFromTable().State(databaseState);
    };

    // Признак необходимости обрабатывать событие изменения
    helper.NeedHandleChangeEvent = function (newValue, oldValue, editor) {
        if (oldValue === undefined || oldValue === null) {
            editor.InitialValue(newValue);
        }

        if (needHandleEvents() === false)
            return false;

        if (oldValue === null ||
            newValue === oldValue ||
            editor.InitialValue() === newValue) {
            return false;
        }
        return true;
    };

    // Создать интервал
    helper.CreateInterval = function (operationType, successConditionFunc) {
        var interval = setInterval(function () {
            vm.GeneralTab.ResetTabData();
            vm.GeneralTab.InitializeTabData(false,
                function () {
                    if (successConditionFunc() === true) {
                        helper.RemoveInterval(operationType);
                    }
                });
        },
        5000);

        helper.Intervals.push({
            DatabaseId: ko.observable(vm.TabsCommonData.DatabaseId()),
            Data: ko.observable(interval),
            Operation: ko.observable(operationType)
        });
    };

    // Удалить интервал
    helper.RemoveInterval = function (operationType) {
        var existingItem = ko.utils.arrayFirst(helper.Intervals,
            function (item) {
                if (item.DatabaseId() === vm.TabsCommonData.DatabaseId() &&
                    item.Operation() === operationType) {
                    return item;
                }
            });

        if (existingItem === null || existingItem === undefined)
            return;

        clearInterval(existingItem.Data());

        const index = helper.Intervals.indexOf(existingItem);
        if (index > -1) {
            helper.Intervals.splice(index, 1);
        }
    }

    // Удалить все интервалы
    helper.RemoveAllServerPollingIntervals = function () {
        ko.utils.arrayForEach(helper.Intervals,
            function (item) {
                helper.RemoveInterval(item.Operation());
            });
        helper.ResetEditorsNewValues();
    };

    // Сбросить новые значения редакторов
    helper.ResetEditorsNewValues = function () {
        vm.GeneralTab.Data.Editors.DistributionTypeEditor.NewValue(null);
        vm.GeneralTab.Data.Editors.FileStorageEditor.NewValue(null);
    };

    // Завершить сеансы в базе
    helper.TerminateSessionInDb = function (callbackFunc) {
        const url = vm.Config.Urls.StartProcessOfTerminateSessionsInDatabase;
        const params = {
            databaseId: vm.TabsCommonData.DatabaseId()
        }
        
        apiHelper.sendPostRequestWithoutHandleResult(
            url,
            params,
            function (data) {
                if (!data.success) {
                    toastr.warning(data.data.ErrorMessage);
                    return;
                }
                
                callbackFunc();
                Attention.Show("Завершение активных сеансов",
                    "Все активные сеансы в базе “" + vm.GeneralTab.Data.Caption() + "” будут завершены в течение нескольких минут");

                vm.Config.UpdateTableLink(vm.Config.CurrentPage);
                vm.GeneralTab.ResetTabData();
                vm.GeneralTab.InitializeTabData();
            });
    };

    // Переместить базу в архив
    helper.SendDbToArchive = function (callbackFunc) {
        const params = {
            DatabasesId: [vm.TabsCommonData.DatabaseId()]
        };

        apiHelper.sendBasePostRequest(
            vm.Config.Urls.StartProcessOfArchiveDatabasesToTomb,
            vm.Config.ContentId,
            params,
            function () {
                Attention.Show("Задача создана",
                    "База в процессе переноса в архив.");
                callbackFunc();
                vm.CloseDatabaseCard();
                vm.Config.UpdateTableLink(vm.Config.CurrentPage);
            });
    };

    // Валидаторы
    helper.DbValidators = {

        // Провалидировать название инф. базы
        ValidateDbCaption: function (oldValue) {
            var currentEditorValue = vm.GeneralTab.Data.Editors.CaptionEditor.Value().replace(/\s+/g, "");
            if (!vm.GeneralTab.Data.Editors.CaptionEditor.Value || currentEditorValue.length === 0) {

                helper.Alerts.captionAlert.message('Укажите название базы');
                vm.GeneralTab.Data.Editors.CaptionEditor.Value(oldValue);
                return false;
            }
            if (helper.Config.captionRegularExpression.test(currentEditorValue)) {

                helper.Alerts.captionAlert.message('Использовались недопустимые символы');
                vm.GeneralTab.Data.Editors.CaptionEditor.Value(oldValue);
                return false;
            }
            if (currentEditorValue.length > helper.Config.maxDbCaptionLength) {

                helper.Alerts.captionAlert.message("Наименование должно содержать до " +
                    helper.Config.maxDbCaptionLength +
                    " символов");
                vm.GeneralTab.Data.Editors.CaptionEditor.Value(oldValue);
                return false;
            }
            helper.Alerts.captionAlert.message('');
            return true;
        },

        // Провалидировать номер инф. базы
        ValidateV82Name: function (oldValue) {
            var currentValue = vm.GeneralTab.Data.Editors.V82NameEditor.Value().replace(/\s+/g, "");
            if (!vm.GeneralTab.Data.Editors.V82NameEditor.Value || currentValue.length === 0) {
                helper.Alerts.v82NameAlert.message('Укажите номер базы');
                vm.GeneralTab.Data.Editors.V82NameEditor.Value(oldValue);
                return false;
            }
            if (helper.Config.v82NameRegularExpression.test(currentValue)) {
                helper.Alerts.v82NameAlert.message('Использовались недопустимые символы');
                vm.GeneralTab.Data.Editors.V82NameEditor.Value(oldValue);
                return false;
            }
            if (!vm.GeneralTab.Data.Editors.V82NameEditor || currentValue.length > 20) {
                helper.Alerts.v82NameAlert.message('Длина номера не должна превышать 20 символов');
                vm.GeneralTab.Data.Editors.V82NameEditor.Value(oldValue);
                return false;
            }
            helper.Alerts.v82NameAlert.message('');
            return true;
        }
    };

    // Алерты
    helper.Alerts = {

        // Алерт названия инф. базы
        captionAlert: {
            componentId: 'dbCaption-alert',
            message: ko.observable(null)
        },

        // Алерт номера инф. базы
        v82NameAlert: {
            componentId: 'dbV82Name-alert',
            message: ko.observable(null)
        },

        // Показать алерт для блока
        showAlertForBlock: function (block, message) {
            block.message(message);
            helper.Alerts.showAlertBlock(block.componentId);
        },

        // Показать блок с алертом
        showAlertBlock: function (alertBlockId) {
            $('#' + alertBlockId).removeClass('hiddenContainer');
            $('#' + alertBlockId).addClass('visibleContainer');
        },

        // Скрыть блок с алертом
        hideAlertBlock: function (alertBlockId) {
            $('#' + alertBlockId).removeClass('visibleContainer');
            $('#' + alertBlockId).addClass('hiddenContainer');
        }
    };
}