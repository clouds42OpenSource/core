﻿// Хелпер для работы с моделью данных таба "Общее"
const generalTabDataModelHelper = {
    
    // Обновить данные модели таба "Общее" (старая модель имеет observable поля)
    updateData: function(oldModel, newModel) {

        oldModel.Id(newModel.Id);
        oldModel.MyDatabasesServiceTypeId(newModel.MyDatabasesServiceTypeId);
        oldModel.V82Name(newModel.V82Name);
        oldModel.Caption(newModel.Caption);
        oldModel.DbNumber(newModel.DbNumber);
        oldModel.DatabaseState(newModel.DatabaseState);
        oldModel.IsFile(newModel.IsFile);
        oldModel.DbTemplate(newModel.DbTemplate);
        oldModel.TemplateName(newModel.TemplateName);
        oldModel.ConfigurationName(newModel.ConfigurationName);
        oldModel.CanEditDatabase(newModel.CanEditDatabase);
        oldModel.TemplatePlatform(newModel.TemplatePlatform);
        oldModel.PlatformType(newModel.PlatformType);
        oldModel.Stable82Version(newModel.Stable82Version);
        oldModel.V82Server(newModel.V82Server);
        oldModel.SqlServer(newModel.SqlServer);
        oldModel.FilePath(newModel.FilePath);
        oldModel.CanWebPublish(newModel.CanWebPublish);
        oldModel.PublishState(newModel.PublishState);
        oldModel.UsedWebServices(newModel.UsedWebServices);
        oldModel.RestoreModelType(newModel.RestoreModelType);
        oldModel.CanChangeRestoreModel(newModel.CanChangeRestoreModel);
        oldModel.FileStorageName(newModel.FileStorageName);
        oldModel.Version(newModel.Version);
        oldModel.IsDbOnDelimiters(newModel.IsDbOnDelimiters);
        oldModel.IsDemoDelimiters(newModel.IsDemoDelimiters);
        oldModel.ZoneNumber(newModel.ZoneNumber);
        oldModel.HasModifications(newModel.HasModifications);
        oldModel.CreationDate(CommonHelper.DateTimeToDefaultFormat(newModel.CreationDate, this._dateTimeFormat));
        oldModel.DatabaseConnectionPath(newModel.DatabaseConnectionPath);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailableFileStorages,
            newModel.AvailableFileStorages);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailablePlatformVersions,
            [
                { text: newModel.Stable83Version, value: 0 },
                { text: newModel.Alpha83Version, value: 1 }
            ]);
        oldModel.FileStorageId(newModel.FileStorageId);
        oldModel.DistributionType(newModel.DistributionType);
        oldModel.Alpha83Version(newModel.Alpha83Version);
        oldModel.Stable83Version(newModel.Stable83Version);
        oldModel.ActualDatabaseBackupId(newModel.ActualDatabaseBackupId);
        oldModel.IsExistTerminatingSessionsProcessInDatabase(newModel.IsExistTerminatingSessionsProcessInDatabase);
        oldModel.IsStatusErrorCreated(newModel.IsStatusErrorCreated);
    },

    // Получить исходные/начальные данные модели таба "Общее"
    getInitialData: function() {

        return {

            // Id базы
            Id: ko.observable(null),

            // Дата создания базы
            CreationDate: ko.observable(null),

            // Название базы
            V82Name: ko.observable(null),

            //Описание
            Caption: ko.observable(null),

            // Номер базы
            DbNumber: ko.observable(null),

            // Статус базы
            DatabaseState: ko.observable(null),

            // База файловая
            IsFile: ko.observable(null),

            // Шаблон базы
            DbTemplate: ko.observable(null),

            // Название шаблона
            TemplateName: ko.observable(null),

            // Название конфигурации
            ConfigurationName: ko.observable(null),

            // Возможность редактировать базу
            CanEditDatabase: ko.observable(null),

            // Платформа
            TemplatePlatform: ko.observable(null),

            // Тип распространения
            DistributionType: ko.observable(null),

            // Тип платформы
            PlatformType: ko.observable(null),

            // Стабильная версия 82
            Stable82Version: ko.observable(null),

            // Альфа версия 83
            Alpha83Version: ko.observable(null),

            // Стабильная версия 83
            Stable83Version: ko.observable(null),

            // Сервер 82
            V82Server: ko.observable(null),

            // Sql сервер
            SqlServer: ko.observable(null),

            // Путь к файлу
            FilePath: ko.observable(null),

            // Возможность вэб публикации
            CanWebPublish: ko.observable(null),

            // Статус публикации
            PublishState: ko.observable(null),

            // Использование вэб сервиса
            UsedWebServices: ko.observable(null),

            // Id файлового хранилища
            FileStorageId: ko.observable(null),

            // Тип модели восстановления
            RestoreModelType: ko.observable(null),

            // Признак что есть возможность сменить модель восстановления
            CanChangeRestoreModel: ko.observable(null),

            // Наименование файлового хранилища
            FileStorageName: ko.observable(null),

            // Версия
            Version: ko.observable(null),

            // База на разделителях
            IsDbOnDelimiters: ko.observable(null),

            // Признак что база демо на разделителях
            IsDemoDelimiters: ko.observable(null),

            // Номер области (если база на разделителях)
            ZoneNumber: ko.observable(null),

            // Признак, что в базе есть доработки
            HasModifications: ko.observable(null),

            // Путь подключения к инф. базе
            DatabaseConnectionPath: ko.observable(null),

            // Доступные хранилища файлов
            AvailableFileStorages: ko.observableArray([]),

            // Доступные версии платформы
            AvailablePlatformVersions: ko.observableArray([]),

            // ID услуги сервиса "Мои инф. базы"
            MyDatabasesServiceTypeId: ko.observable(null),

            // ID актуального бэкапа инф. базы
            ActualDatabaseBackupId: ko.observable(null),

            // Cуществует ли процесс завершения сеансов в базе данных
            IsExistTerminatingSessionsProcessInDatabase: ko.observable(false),

            // Статус базы "Ошибка создания"
            IsStatusErrorCreated: ko.observable(false),

            // Редакторы
            Editors: {
                // Признак что редакторы инициализированы
                IsEditorsInitialized: ko.observable(false),

                // Редактор названия инф. базы
                DbCaptionEditor: null,

                // Редактор номера инф. базы
                V82NameEditor: null
            }
        };
    },

    // Формат времени
    _dateTimeFormat: 'YYYY-MM-DD'
}