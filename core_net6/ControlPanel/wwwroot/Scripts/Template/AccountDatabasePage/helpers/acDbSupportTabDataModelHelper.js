﻿
// Хелпер для работы с моделью данных таба "Авторизация и ТиИ"
const acDbSupportTabDataModelHelper = {

    // Обновить данные модели таба "Авторизация и ТиИ" (старая модель имеет observable поля)
    updateData: function (oldModel, newModel) {

        oldModel.DatabaseId(newModel.DatabaseId);
        oldModel.IsConnects(newModel.IsConnects);
        oldModel.IsNotAuthorized(newModel.IsNotAuthorized);
        oldModel.SupportState(newModel.SupportState);
        oldModel.SupportStateDescription(newModel.SupportStateDescription);
        oldModel.LastTehSupportDate(newModel.LastTehSupportDate);
        oldModel.Login(newModel.Login);
        oldModel.Password(newModel.Password);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AcDbSupportHistories,
            newModel.AcDbSupportHistories);
    },

    // Получить исходные/начальные данные модели таба "Авторизация и ТиИ"
    getInitialData: function () {

        return {

            // Id информационной базы
            DatabaseId: ko.observable(null),

            // Есть соединение(соединяется) с базой
            IsConnects: ko.observable(null),

            // Пользователь не авторизован в базе
            IsNotAuthorized: ko.observable(null),

            // Состояние авторизации
            SupportState: ko.observable(null),

            // Описание состояния авторизации
            SupportStateDescription: ko.observable(null),

            // Дата последней технической поддержки
            LastTehSupportDate: ko.observable(null),

            // Логин для авторизации пользователя в базу
            Login: ko.observable(null),

            // Пароль для авторизации пользователя в базу
            Password: ko.observable(null),

            // История технической поддержки инф. баз
            AcDbSupportHistories: ko.observableArray([]),

            // Интервалы опроса сервера
            ServerPollingIntervals: []
        };
    }
}