﻿
// Хелпер для генерации модели данных инф. базы
const generateAccountDatabaseDataHelper = {

    // Сформировать модель данных инф. базы
    generateModel: function(data) {
        return {
            Id: ko.observable(data.Id),
            Name: ko.observable(data.Name),
            TemplateCaption: ko.observable(data.TemplateCaption),
            TemplateImageName: ko.observable(data.TemplateImageName),
            SizeDatabeseDescription: ko.observable(this._getSizeDatabeseDescription(data.SizeInMb)),
            SizeInMb: ko.observable(data.SizeInMb),
            LastActivityDate: ko.observable(CommonHelper.DateTimeToDefaultFormat(data.LastActivityDate, this._dateTimeFormat)),
            DatabaseLaunchLink: ko.observable(data.DatabaseLaunchLink),
            IsFile: ko.observable(data.IsFile),
            HasSupport: ko.observable(data.HasSupport),
            HasAutoUpdate: ko.observable(data.HasAutoUpdate),
            HasAcDbSupport: ko.observable(data.HasAcDbSupport),
            State: ko.observable(data.State),
            PublishState: ko.observable(data.PublishState),
            NeedShowWebLink: ko.observable(data.NeedShowWebLink),
            IsDbOnDelimiters: ko.observable(data.IsDbOnDelimiters),
            IsExistSessionTerminationProcess: ko.observable(data.IsExistSessionTerminationProcess),
            CreateAccountDatabaseComment: ko.observable(data.CreateAccountDatabaseComment),
            CreationTimer: {
                TimerType: ko.observable(data.CreationTimer.TimerType),
                Visible: ko.observable(data.CreationTimer.Visible),
                TimeLeft: {
                    Hours: ko.observable(data.CreationTimer.TimeLeft.Hours),
                    Minutes: ko.observable(data.CreationTimer.TimeLeft.Minutes),
                    Seconds: ko.observable(data.CreationTimer.TimeLeft.Seconds),
                    TotalSeconds: ko.observable(data.CreationTimer.TimeLeft.TotalSeconds)
                }
            },
            IsSelected: ko.observable(false)
        };
    },

    // Сформировать параметры запроса, для получения моделей баз
    generateParametersForGetDatabases: function (filter, config, sortingOptions, pageOptions) {
        return {
            Filter: {
                SearchString: filter.SearchString,
                IsServerDatabaseOnly: filter.IsServerDatabaseOnly,
                IsArchievedDatabaseOnly: filter.IsArchievedDatabaseOnly,
                PageSize: pageOptions.PageSize,
                AccountId: config.AccountId,
                AccountUserId: config.AccountUserId,
                AccountDatabaseAffiliationType: config.DatabaseAffiliationType
            },
            PageNumber: pageOptions.PageNumber,
            SortingData: {
                SortKind: sortingOptions.SortType,
                FieldName: sortingOptions.SortFieldName
            }
        };
    },

    // Получить описание размена базы
    _getSizeDatabeseDescription: function(sizeInMb) {
        return sizeInMb > 0 ? CommonHelper.MegabyteToClassicFormat(sizeInMb) : '-';
    },

    // Формат времени
    _dateTimeFormat: 'YYYY-MM-DD HH:mm:ss'

}