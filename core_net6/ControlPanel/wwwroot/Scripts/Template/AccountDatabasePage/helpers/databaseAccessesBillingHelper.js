﻿
// Хэлпер для работы с биллинговой частью доступов в инф. базу
const databaseAccessesBillingHelper = {

    // Получить общую стоимость доступов
    getTotalAccessesCost: function (accesses) {
        var totalAccessesCost = 0;
        ko.utils.arrayForEach(accesses,
            function (access) {
                if (access.hasAccess === true && access.state !== acDbAccessStateEnum.Done) {
                    totalAccessesCost += access.accessCost();
                }
            });
        return totalAccessesCost;
    }
}