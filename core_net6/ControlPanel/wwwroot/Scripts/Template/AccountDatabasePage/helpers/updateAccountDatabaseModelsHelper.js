﻿
// Хелпер для обновления моделей информационных баз
const updateAccountDatabaseModelsHelper = {

    // Обновить модель общих разрешений
    updateCommonPermissions: function(oldModel, newModel) {

        oldModel.HasPermissionForRdp(newModel.HasPermissionForRdp);
        oldModel.HasPermissionForWeb(newModel.HasPermissionForWeb);
        oldModel.HasPermissionForCreateAccountDatabase(newModel.HasPermissionForCreateAccountDatabase);
        oldModel.HasPermissionForDisplaySupportData(newModel.HasPermissionForDisplaySupportData);
        oldModel.HasPermissionForDisplayPayments(newModel.HasPermissionForDisplayPayments);
        oldModel.HasPermissionForDisplayAutoUpdateButton(newModel.HasPermissionForDisplayAutoUpdateButton);
        oldModel.HasPermissionForMultipleActionsWithDb(newModel.HasPermissionForMultipleActionsWithDb);
    },

    // Обновить модель общих данных для работы с информационными базами
    updateCommonData: function(oldModel, newModel) {

        oldModel.AccountId(newModel.AccountId);
        oldModel.AccountUserId(newModel.AccountUserId);
        oldModel.MyDatabaseServiceId(newModel.MyDatabaseServiceId);
        oldModel.AccessToServerDatabaseServiceTypeId(newModel.AccessToServerDatabaseServiceTypeId);
        oldModel.IsMainServiceAllowed(newModel.IsMainServiceAllowed);
        oldModel.IsVipAccount(newModel.IsVipAccount);
        oldModel.AccountLocaleName(newModel.AccountLocaleName);
        oldModel.AccountAdminInfo(newModel.AccountAdminInfo);
        oldModel.AllowAddingDatabaseInfo.IsAllowedCreateDb(newModel.AllowAddingDatabaseInfo.IsAllowedCreateDb);
        oldModel.AllowAddingDatabaseInfo.SurchargeForCreation(newModel.AllowAddingDatabaseInfo.SurchargeForCreation);
        oldModel.AllowAddingDatabaseInfo.ForbiddeningReasonMessage(newModel.AllowAddingDatabaseInfo
            .ForbiddeningReasonMessage);
        oldModel.MainServiceStatusInfo.ServiceIsLocked(newModel.MainServiceStatusInfo.ServiceIsLocked);
        oldModel.MainServiceStatusInfo.ServiceLockReason(newModel.MainServiceStatusInfo.ServiceLockReason);
        oldModel.MainServiceStatusInfo.ServiceLockReasonType(newModel.MainServiceStatusInfo.ServiceLockReasonType);
    },

    // Обновить дополнительные данные для работы с информационными базами
    updateAdditionalData: function (oldModel, newModel) {

        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailableDatabaseStatuses, newModel.AvailableDatabaseStatuses);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailablePlatformTypes, newModel.AvailablePlatformTypes);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailableAccountDatabaseRestoreModelTypes, newModel.AvailableAccountDatabaseRestoreModelTypes);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.AvailableDbTemplates, newModel.AvailableDbTemplates);
        updateDatabaseDataCommonHelper.updateArrayValues(oldModel.ServiceExtensionsForDatabaseTypes, newModel.ServiceExtensionsForDatabaseTypes);
    }
}