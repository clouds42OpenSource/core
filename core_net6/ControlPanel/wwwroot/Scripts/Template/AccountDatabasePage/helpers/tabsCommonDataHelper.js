﻿
// Хелпер для работы с общими данными табов
const tabsCommonDataHelper = {

    // Обновить общие данные табов (старая модель имеет observable поля)
    updateTabsCommonData: function(oldModel, newModel) {
        oldModel.DatabaseId(newModel.DatabaseId);
        oldModel.TemplateImageName(newModel.TemplateImageName);
        oldModel.DbObjectFromTable(newModel.DbObjectFromTable);
        oldModel.DatabaseCardTabType(newModel.DatabaseCardTabType);
        oldModel.CurrencyName(newModel.CurrencyName);
        oldModel.AccessToServerDatabaseServiceTypeId(newModel.AccessToServerDatabaseServiceTypeId);
    },

    // Получить исходные/начальные данные данные табов(общие данные)
    getInitialData: function () {

        return {

            // Id базы
            DatabaseId: ko.observable(null),

            DbObjectFromTable: ko.observable(null),

            // Название изображения шаблона
            TemplateImageName: ko.observable(null),

            // Тип таба карточки информационной базы
            DatabaseCardTabType: ko.observable(databaseCardTabTypeEnum.General),

            // Название валюты
            CurrencyName: ko.observable(null),

            // ID услуги "Доступ в серверную базу"
            AccessToServerDatabaseServiceTypeId: ko.observable(null)
        };
    },

    // Выполнить инициализацию Datetimepicker
    InitializeDatetimepicker: function (id, callbackFunc, defaultDate) {

        const domObj = $('#' + id);

        domObj.datetimepicker({
            format: this._dateTimeFormat,
            locale: 'ru'
        }).on('dp.change', function (e) {
            callbackFunc();
        });

        domObj.val(defaultDate);
    },

    // Формат времени
    _dateTimeFormat: 'DD.MM.YYYY'
}