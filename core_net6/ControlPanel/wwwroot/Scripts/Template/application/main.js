﻿// DOM ready
$(function () {
    var ajaxFormSubmit = function () {
        var $form = $(this);

        var $input = $form.find("input[data-clerable='true']");

        if (!$input.val()) {
            $input.effect("highlight", { color: "#d9edf7" }, 1000);
            return false;
        }

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };
        var $spinner = $form.parents("div.row-fluid:first").find("div#spinner");
        var $alert = $form.parents("div.span6:first").find("div#alert");
        $spinner.show();
        // when we receive data 
        $.ajax(options)
            .done(function (data) {
                if (!data.length) {
                    $alert.show();
                    return;
                }

                var $target = $($form.attr("data-cp-target"));
                var $newHtml = $(data);
                $target.replaceWith($newHtml);
                $newHtml.effect("highlight", 500);
                // clear all inputs (prevent double send!)
                $form.find("input[data-clerable='true']").val("");
                $alert.hide();
            })
            .always(function () {
                $spinner.hide();
            })
            .error(function () {
                $alert.show();
            });

        // prevent default action
        return false;
    };

    var submitAutocomplateForm = function (event, ui) {

        var $input = $(this);

        $input.val(ui.item.label);


        var $form = $input.parents("form:first");
        $form.find("#accountId").val(ui.item.value);
        //$("#accountId").val(ui.item.value);
        $form.submit();

        return false;
    };

    //var createAutocomplete = function () {
    //    var $input = $(this);

    //    var options = {
    //        source: $input.attr("data-cp-autocomplete"),
    //        select: submitAutocomplateForm,
    //        focus: function (event, ui) {
    //            //$input.val(ui.item.label);
    //            return false;
    //        }
    //    };

    //    $input.autocomplete(options).data("uiAutocomplete")._renderItem = function (ul, item) {
    //        return $("<li>")
    //          .append("<a><b>" + item.accountIndex + ", " + item.label + "</b><br><span class=\"muted\">" + "Менеджер: " + item.managerName + ", email: " + item.managerEmail + "</span></a>")
    //          .appendTo(ul);
    //    };;
    //};

    var submitAutocomplateForm2 = function (event, ui) {

        var $input = $(this);

        $input.val(ui.item.label);

        var $form = $input.parents("form:first");
        $form.find("#accountUserId").val(ui.item.value);
        //$("#accountId").val(ui.item.value);
        $form.submit();

        return false;
    };

    var createAutocomplete2 = function () {
        var $input = $(this);

        var options = {
            source: $input.attr("data-cp-user-autocomplete"),
            select: submitAutocomplateForm2,
            focus: function (event, ui) {
                //$input.val(ui.item.label);
                return false;
            }
        };

        $input.autocomplete(options).data("uiAutocomplete")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<a><b>" + item.label + "</b>,<br/><span class=\"muted\">email: " + item.email + "</span></a>")
                .appendTo(ul);
        };;
    };

    // 0..1...many forms selected
    $(document).on("submit", "form[data-cp-ajax='true']", ajaxFormSubmit);

    //// for each autocomplete field
    //$("input[data-cp-autocomplete]").each(createAutocomplete);
    // becouse we need another render form... :(
    $("input[data-cp-user-autocomplete]").each(createAutocomplete2);

    // clear error when edit
    $("form[data-cp-ajax='true']").bind('input', function () {
        var $form = $(this);
        var $alert = $form.parents("div.span6:first").find("div#alert");
        $alert.hide();
    });

    // update tabs content
    $("a[data-tab-url]").on('shown.bs.tab', function (e) {
        var url = $(e.target).attr('data-tab-url');
        if (url == name)
            return;
        var targetDiv = $(e.target).attr("href"); // activated tab

        if ($(targetDiv).find('#stubSpinner').length) {
            $.ajax({
                type: "GET",
                url: url,
                error: function (data) {
                    $(targetDiv).html("В процессе загрузки произошла ошибка. Пожалуйста, обновите страницу и попробуйте снова");
                },
                success: function (data) {
                    $(targetDiv).html(data);
                    refreshBinds();
                }
            });
        }
    });

    var refreshBinds = function () {
        // clear error when edit
        $("form[data-cp-ajax='true']").bind('input', function () {
            var $form = $(this);
            var $alert = $form.parents("div.span6:first").find("div#alert");
            $alert.hide();
        });

        $("input[data-cp-user-autocomplete]").each(createAutocomplete2);
    };

    //$(document).on("click", "a.add-all", function (event) {
    //    event.preventDefault();
    //    var $link = $(this);
    //    var url = $link.attr('href');
    //    if (!url.length)
    //        return;
    //    var targetDiv = $link.attr("data-cp-target"); // target div
    //    var $spinner = $link.parents("div.span6:first").find("div.form").find("div#spinner");
    //    var $alert = $link.parents("div.span6:first").find("div#alert");
    //    $spinner.show();
    //    $.ajax({
    //        type: "POST",
    //        url: url,
    //        complete: function () {
    //            $spinner.hide();
    //        },
    //        success: function (data) {
    //            var $newHtml = $(data);
    //            $(targetDiv).replaceWith($newHtml);
    //            $newHtml.effect("highlight", 500);
    //            refreshBinds();
    //            $alert.hide();
    //        },
    //        error: function () {
    //            $alert.show();
    //        }
    //    });
    //});

    // TABBER
    $("a[pull-tab]").on("click", function () {
        $(this).closest("div.tabs").find("a[pull-tab]").parent("li").removeClass("active");
        $(this).closest("div.tabs").find("div[tab-content]").removeClass("active");

        $(this).parent("li").addClass("active");
        $(this).closest("div.tabs").find("div[tab-content='" + $(this).attr("pull-tab") + "']").addClass("active");
    });

    // SPINNER
    $("a[bind-spinner]").on("click", function () {
        SpinnerControl.OnClick(this);
    });

    $("input[bind-editable-spinner]").on("input", function () {
        SpinnerControl.Notify(this);
    });

    $('#inputDatabaseName').on('input', function (e) {        
    });

    // TEXTAREA AUTO RESIZE FUNCTION
    $.each($('textarea[autoresize]'), function () {
        var offset = this.offsetHeight - this.clientHeight;

        var resizeTextarea = function (el) {
            $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        };

        resizeTextarea(this);
        $(this).on('keyup input', function () { resizeTextarea(this); }).removeAttr('autoresize');
    });
    
    // GROUP CHECKBOXES
    $.each($(".check-group-box"), function (i, item) {
        CheckGroupBox.Execute(item);
    });
    $("button[data-value-check]").on("click", function () {
        if ($(this).attr("disabled") == undefined)
            CheckGroupBox.OnClick(this);
    });
});

CheckGroupBox = new function () {
    this.OnClick = function (item) {
        var container = $(item).parent();
        var input = container.find("input");
        if ($(item).attr("data-value-check") === "true") {
            $(input).prop("checked", true);
        } else {
            $(input).prop("checked", false);
        }
        CheckGroupBox.Execute(container);
    }
    this.Execute = function (container) {
        var falseItem = $(container).find("button[data-value-check='false']");
        var trueItem = $(container).find("button[data-value-check='true']");
        var input = $(container).find("input");
        
        if ($(input).is(":checked")) {
            trueItem.addClass("active");
            falseItem.removeClass("active");
        } else {
            trueItem.removeClass("active");
            falseItem.addClass("active");
        }
    }
}

SpinnerControl = new function() {
    this.Notify = function (input) {
        $(input).change();
    }
    this.OnClick = function (btn) {
        var container = $(btn).parent("span");

        if (container.attr("array") != undefined)
            SpinnerControl.ArrayType(btn);

        if (container.attr("min") != undefined && container.attr("max") != undefined)
            SpinnerControl.SingleType(btn);
   }
    this.SingleType = function (btn) {
        var container = $(btn).parent("span");

        var type = $(btn).attr("bind-type");

        var span = container.find("span");
        var input = container.find("input");

        var min = parseInt(container.attr("min"));
        var max = parseInt(container.attr("max"));

        var currentValue = parseInt($(span).text());

        if (type === "minus") {
            if (currentValue > min) {
                span.text(parseInt(currentValue - 1));
                input.val(parseInt(currentValue - 1)).change();
            }
        } else {
            if (currentValue < max) {
                span.text(parseInt(currentValue + 1));
                input.val(parseInt(currentValue + 1)).change();
            }
        }
    }
    this.ArrayType = function (btn) {
        var container = $(btn).parent("span");

        var type = $(btn).attr("bind-type");

        var span = container.find("span");
        var input = container.find("input");

        var array = JSON.parse(container.attr("array"));
        var currentValue = parseInt($(span).text());
        var position = array.indexOf(currentValue);

        if (type === "minus") {
            if (array[position - 1] !== undefined) {
                span.text(array[position - 1]);
                input.val(array[position - 1]).change();
            }
        } else {
            if (array[position + 1] !== undefined) {
                span.text(array[position + 1]);
                input.val(array[position + 1]).change();
            }
        }
    }
}