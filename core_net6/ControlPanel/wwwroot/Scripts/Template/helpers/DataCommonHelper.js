﻿UIHelper = {
    ShowBlock: function (block) {
        block.removeClass('hiddenContainer');
        block.addClass('visibleContainer');
    },
    HideBlock: function (block) {
        block.removeClass('visibleContainer');
        block.addClass('hiddenContainer');
    }
}

CommonValidator = new function () {
    this.EmailIsValid = function (email) {
        var re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

CommonHelper = new function () {

    this.JsonEscape = function (str) {
        return str.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t");
    };

    this.MegabyteToClassicFormat = function (mb) {
        var gb = Math.round(10 * (mb / 1024)) / 10;
        return gb + ' Гб';
    }

    this.JsDateTimeToString = function (date, format) {
        dateFormat.i18n["monthNames"] = [
            "янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек",
            "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"
        ];
        if (date) {
            var myDateValue = new Date(date.match(/\d+/)[0] * 1);
            if (format)
                return dateFormat(myDateValue, format);
            return myDateValue.format();
        }
        return '---';
    };

    this.DecodeHtml = function (html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    };

    this.GetFormOfject = function (formId) {
        var formData = {};
        var arrayParams = $('#' + formId).serializeArray();
        for (var i = 0; i < arrayParams.length; i++) {
            formData[arrayParams[i].name] = arrayParams[i].value;
        }
        return formData;
    };

    this.alert = function (msg) {
        toastr.warning(msg);
    };
    this.warn = function (msg, title) {
        toastr.warning(msg, title);
    };
    this.Exception = function (model) {
        model.Title = model.Title == undefined ? 'Ошибка' : model.Title;
        model.Message = model.Message == undefined ? 'Неопознанная ошибка' : model.Message;
        model.ErrorDetails = model.ErrorDetails == undefined ? '' : model.ErrorDetails;

        toastr.warning(model.Message, model.Title);
    };

    this.PayServiceWithPrepayment = function (title, msg, callback_GoToPay, callback_PrePayment) {
        var message = document.createElement("p");
        message.innerHTML = msg;
        swal3B({
            title: title || "Внимание",
            content: message,
            icon: "warning",
            buttons: {
                close: {
                    text: 'Отмена',
                    className: "btn btn-default"
                },
                prePayment: {
                    text: 'Взять обещанный платеж',
                    className: "btn btn-success",
                    value: 'prePayment'
                },
                goToPay: {
                    text: 'Оплатить сейчас',
                    className: "btn btn-success",
                    callback: 'goToPay'
                }
            }
        })
            .then(function(value) {
            switch (value) {
                case "prePayment":
                    callback_PrePayment();
                break;
                case "goToPay":
                    callback_GoToPay();
                break;
            }
        
    });
    }

    this.PayService = function (title, msg, callback_GoToPay) {
        var message = document.createElement("p");
        message.innerHTML = msg;
        swal3B({
            title: title || "Внимание",
            content: message,
            icon: "warning",
            buttons: {
                close: {
                    text: 'Отмена',
                    className: "btn btn-default"
                },
                goToPay: {
                    text: 'Оплатить сейчас',
                    className: "btn btn-success",
                    callback: 'goToPay'
                }
            }
        })
            .then(function (value) {
                switch (value) {
                    case "goToPay":
                        callback_GoToPay();
                        break;
                }

            });
    }

    this.toCurrency = function (sum, Locale, withCurrency) {

        var floatValue = parseFloat(sum);
        if (isNaN(floatValue))
            floatValue = 0.00;
        var value = floatValue.toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

        if (withCurrency && withCurrency === true) {
            return value + ' ' + Locale.Currency();
        } else {
            return value;
        }
    }

    this.toStrCurrency = function (sum, currency) {

        var floatValue = parseFloat(sum);
        if (isNaN(floatValue))
            floatValue = 0.00;
        var value = floatValue.toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

        if (currency) {
            return value + ' ' + currency + '.';
        } else {
            return value;
        }
    }

    this.toDocumentNumberFormat = function(value) {
        var numbers = value.split("-");
        var result = numbers[0];
        var contractNumber = addZeros(numbers[1], 2);
        result += '-'+contractNumber;
        if (numbers[2] !== undefined) {
            result += '-'+addZeros(numbers[2], 3);
        };

        function addZeros(num, len) {
            while (("" + num).length < len) num = "0" + num;
            return num.toString();
        }

        return result;
    }

    this.toMoneyFormat = function (sum) {
        var floatValue = parseFloat(sum);
        if (isNaN(floatValue))
            floatValue = 0.00;
        return floatValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').replace('.', ',');
    }

    this.ObjectToFormData = function (obj, rootName, ignoreList) {
        var formData = new FormData();

        function appendFormData(data, root) {
            if (!ignore(root)) {
                root = root || '';
                if (data instanceof File) {
                    formData.append(root, data);
                } else if (data instanceof Blob) {
                    formData.append(root, data);
                } else if (Array.isArray(data)) {
                    for (var i = 0; i < data.length; i++) {
                        appendFormData(data[i], root + '[' + i + ']');
                    }
                } else if (typeof data === 'object' && data) {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            if (root === '') {
                                appendFormData(data[key], key);
                            } else {
                                appendFormData(data[key], root + '.' + key);
                            }
                        }
                    }
                } else {
                    if (data !== null && typeof data !== 'undefined') {
                        formData.append(root, data);
                    }
                }
            }
        }

        function ignore(root) {
            return Array.isArray(ignoreList) && ignoreList.some(function (x) { return x === root; });
        }

        appendFormData(obj, rootName);

        return formData;
    };

    // Привести дату к формату
    this.DateTimeToDefaultFormat = function (date, format) {

        if (date === null || date === undefined) {
            return '---';
        }
        return moment(date).format(format);
    }

    // Сравнить версии
    this.CompareVersions = function (version1, version2, options) {
        var lexicographical = options && options.lexicographical,
            zeroExtend = options && options.zeroExtend,
            v1parts = version1.split('.'),
            v2parts = version2.split('.');

        function isValidPart(x) {
            return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
        }

        if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
            return NaN;
        }

        if (zeroExtend) {
            while (v1parts.length < v2parts.length) v1parts.push("0");
            while (v2parts.length < v1parts.length) v2parts.push("0");
        }

        if (!lexicographical) {
            v1parts = v1parts.map(Number);
            v2parts = v2parts.map(Number);
        }

        for (var i = 0; i < v1parts.length; ++i) {
            if (v2parts.length == i) {
                return 1;
            }

            if (v1parts[i] == v2parts[i]) {
                continue;
            }
            else if (v1parts[i] > v2parts[i]) {
                return 1;
            }
            else {
                return -1;
            }
        }

        if (v1parts.length != v2parts.length) {
            return -1;
        }

        return 0;
    }
};