﻿JWTAccessTokenHelper = (function() {
    var jwtTokenStoreName = 'jwt_access_token';

    function sleep(milliseconds) {
        var date = Date.now();
        var currentDate;
        do {
            currentDate = Date.now();
        } while (currentDate - date < milliseconds);
    }

    var jwtAccessTokenHelper = {
        urlToGetJwtAccessToken: "",
        getJwtAccessToken: null,
        removeJwtTokenFromLocalStorage: null
    }

    function getJwtAccessToken(numberOfAttempts) {
        var token = sessionStorage.getItem(jwtTokenStoreName);
    
        if (token) {
            return token;
        }

        var urlToGetJwt = jwtAccessTokenHelper.urlToGetJwtAccessToken;

        do {
            var res = $.ajax({
                cache: false,
                type: 'GET',
                url: urlToGetJwt,
                async: false
            });

            token = res.getResponseHeader("Authorization");
            if (token) {
                break;
            }

            --numberOfAttempts;
            sleep(2000);
        } while (numberOfAttempts > 0)


        if (token) {
            sessionStorage.setItem(jwtTokenStoreName, token);
        }

        return token;
    }

    function removeJwtTokenFromLocalStorage() {
        sessionStorage.removeItem(jwtTokenStoreName);
    }

    jwtAccessTokenHelper.getJwtAccessToken = getJwtAccessToken;
    jwtAccessTokenHelper.removeJwtTokenFromLocalStorage = removeJwtTokenFromLocalStorage;

    return jwtAccessTokenHelper;
})();