﻿// Хэлпер для определения нажатия комбинации клавиш
KeyCombinationDetector = new function () {

    // Признак что нажата любая комбинация быстрых действий Windows
    this.IsAnyWindowsShortcutsPressed = function (event) {
        return KeyCombinationDetector.IsPressedCtrlPlus(event, KeyCodeTypeEnum.CKey) ||
            KeyCombinationDetector.IsPressedCtrlPlus(event, KeyCodeTypeEnum.VKey) ||
            KeyCombinationDetector.IsPressedCtrlPlus(event, KeyCodeTypeEnum.XKey) ||
            KeyCombinationDetector.IsPressedCtrlPlus(event, KeyCodeTypeEnum.AKey) ||
            KeyCombinationDetector.IsPressedCtrlPlus(event, KeyCodeTypeEnum.ZKey) ||
            KeyCombinationDetector.IsCtrlKey(event) ||
            KeyCombinationDetector.IsAltKey(event) ||
            KeyCombinationDetector.IsShiftKey(event) ||
            KeyCombinationDetector.IsPressedKeyEqualKeyCodeType(event, KeyCodeTypeEnum.EnterKey);
    };

    // Получить код нажатой клавиши
    this.GetPressedKeyCode = function (event) {
        return event.which || event.keyCode;
    };

    // Признак что нажата клавиша Ctrl
    this.IsCtrlKey = function (event) {
        const keyCode = KeyCombinationDetector.GetPressedKeyCode(event);
        return event.ctrlKey ? event.ctrlKey : ((keyCode === KeyCodeTypeEnum.CtrlKey) ? true : false);
    }

    // Признак что нажата клавиша Ctrl
    this.IsAltKey = function (event) {
        const keyCode = KeyCombinationDetector.GetPressedKeyCode(event);
        return keyCode === KeyCodeTypeEnum.AltKey;
    }

    // Признак что нажата клавиша Shift
    this.IsShiftKey = function (event) {
        const keyCode = KeyCombinationDetector.GetPressedKeyCode(event);
        return keyCode === KeyCodeTypeEnum.ShiftKey;
    }

    // Признак что нажатая клавиша является keyCodeType
    this.IsPressedKeyEqualKeyCodeType = function (event, keyCodeType) {
        const keyCode = KeyCombinationDetector.GetPressedKeyCode(event);
        return keyCode === keyCodeType;
    }

    // Признак что нажата комбинация ctrl + KeyCodeTypeEnum
    this.IsPressedCtrlPlus = function (event, keyCodeType) {
        const keyCode = KeyCombinationDetector.GetPressedKeyCode(event);
        if (keyCode === keyCodeType && KeyCombinationDetector.IsCtrlKey(event)) {
            return true;
        }
        return false;
    }
};


// Перечисление кодов клавиш
KeyCodeTypeEnum = {

    // Клавиша V
    'VKey': 86,

    // Клавиша C
    'CKey': 67,

    // Клавиша X
    'XKey': 88,

    // Клавиша A
    'AKey': 65,

    // Клавиша Z
    'ZKey': 90,

    // Клавиша Ctrl
    'CtrlKey': 17,

    // Клавиша Alt
    'AltKey': 18,

    // Клавиша Shift
    'ShiftKey': 16,

    // Клавиша Enter
    'EnterKey': 13
};