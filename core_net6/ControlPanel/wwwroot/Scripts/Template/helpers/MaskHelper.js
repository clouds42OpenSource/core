﻿MaskHelper = new function () {
    var self = this;
    self.ShowBlockUi = function (container) {
        if (container) {
            container.children('.ibox-content').addClass('sk-loading');
        } else {
            var cont = $('.ibox');
            if (cont) {
                cont.children('.ibox-content').addClass('sk-loading');
            } else {
                $('body').children('.ibox-content').addClass('sk-loading');
            }
        }
    };
    self.HideBlockUI = function (container) {
        if (container) {
            container.children('.ibox-content').removeClass('sk-loading');
        } else {
            var cont = $('.ibox');
            if (cont) {
                cont.children('.ibox-content').removeClass('sk-loading');
            } else {
                $('body').children('.ibox-content').removeClass('sk-loading');
            }
        }
    };

    self.ShowLoader = function (container) {
        if (!container) return;

        container.css("position", "relative");

        var mask =
            '<div id="spinner-block" style="position: absolute; width: 100%; top: 0; height: 100%; display: flex; align-items: center; background: #ffffffb8; z-index: 999;">' +
                '<div class="sk-spinner sk-spinner-double-bounce" style="width: 90px; height: 90px;">' +
                    '<div class="sk-double-bounce1"></div>' +
                    '<div class="sk-double-bounce2"></div>' +
                '</div>' +
            '</div>';

        container.append(mask);
    };
    self.HideLoader = function (container) {
        if (!container) return;

        container.css("position", "inherit");
        $("#spinner-block").remove();
    };

    self.ShowBlockUiBody = function () {
        $.blockUI({
            message: '<div id="spinner">' +
                        '<div class="sk-spinner sk-spinner-double-bounce" style="width: 90px; height: 90px;">' +
                            '<div class="sk-double-bounce1"></div>' +
                            '<div class="sk-double-bounce2"></div>' +
                        '</div>' +
                     '</div>',
            css: {
                border: "none",
                color: "#f1f1f1",
                backgroundColor: "transparent"
            },
            overlayCSS: {
                backgroundColor: "#000",
                opacity: 0.5,
                cursor: "wait"
            },
            baseZ: 10000
        });
    };
    self.HideBlockUIBody = function () {
        $.unblockUI();
    };
};