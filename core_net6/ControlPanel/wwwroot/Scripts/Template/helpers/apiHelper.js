﻿
// Хелпер для работы с API(базовая отправка запросов)
const apiHelper = {

    // Базовая отправка GET запроса
    sendBaseGetRequest: function (url, contentId, callbackFunc, isAsyncOperation, needShowErrorInModal) {

        MaskHelper.ShowLoader($("#" + contentId));
        this.sendBaseGetRequestWithoutLoader(url, contentId, callbackFunc, isAsyncOperation, needShowErrorInModal)
    },

    // Отправка GET запроса без загрузчика
    sendBaseGetRequestWithoutLoader: function (url, contentId, callbackFunc, isAsyncOperation, needShowErrorInModal) {

        const handleResult = this._handleResult.bind(this);
        $.ajax({
            url: url,
            method: "GET",
            async: isAsyncOperation,
            error: this._handleError,
            success: function (result) {
                handleResult(contentId, result, callbackFunc, needShowErrorInModal);
            }
        });
    },

    // Базовая отправка GET запроса без обработки результата запроса
    sendBaseGetRequestWithoutHandleResult: function (url, contentId, callbackFunc, isAsyncOperation) {

        MaskHelper.ShowLoader($("#" + contentId));

        $.ajax({
            url: url,
            method: "GET",
            async: isAsyncOperation,
            error: this._handleError,
            success: function (result) {

                MaskHelper.HideLoader($("#" + contentId));
                callbackFunc(result);
            }
        });
    },

    // Базовая отправка POST запроса
    sendBasePostRequest: function (url, contentId, params, callbackFunc) {

        MaskHelper.ShowLoader($("#" + contentId));
        const handleResult = this._handleResult.bind(this);

        $.ajax({
            url: url,
            method: "POST",
            data: params,
            error: this._handleError,
            success: function (result) {
                handleResult(contentId, result, callbackFunc);
            }
        });
    },

    // Базовая отправка POST запроса
    sendJsonBasePostRequest: function (url, contentId, params, callbackFunc) {

        MaskHelper.ShowLoader($("#" + contentId));
        const handleResult = this._handleResult.bind(this);

        $.ajax({
            url: url,
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            data: params,
            error: this._handleError,
            success: function (result) {
                handleResult(contentId, result, callbackFunc);
            }
        });
    },

    // Базовая отправка POST запроса(возвращает промис)
    sendBasePostRequestWithPromise: function (url, params) {

        return new Promise(function (resolve, reject) {

            AjaxHelper.ajaxRequestJsonData(url, params,
                function (result) {
                    resolve(result);
                },
                function () {
                    reject("Ошибка получения списка прав доступа к объекту БД");
                },
                false);
        });
    },

    // Отправить запрос без обработки результата запроса
    sendPostRequestWithoutHandleResult: function (url, params, callbackFunc) {

        $.ajax({
            url: url,
            method: "POST",
            data: params,
            error: this._handleError,
            success: function (result) {
                callbackFunc(result);
            }
        });
    },

    // Обработать результат после вызова метода API
    _handleResult: function (contentId, result, callbackFunc, needShowErrorInModal) {

        MaskHelper.HideLoader($("#" + contentId));

        if (!result.success) {

            if (needShowErrorInModal) {

                var message = document.createElement("p");
                message.innerHTML = result.data.ErrorMessage;
                swal3B({
                    title: "Внимание",
                    content: message,
                    icon: "warning",
                    buttons: {
                        close: {
                            text: 'Закрыть',
                            className: "btn btn-success"
                        }
                    }
                });
                return;
            }

            toastr.warning(result.data.ErrorMessage);
            return;
        }

        callbackFunc(result.data);
    },

    // Обработать ошибку
    _handleError: function (xhr, ajaxOptions, thrownError) {
        const needAuth = xhr.getResponseHeader("NeedAuth");
        if (needAuth) {
            location.reload(true);
        }
    }
}