﻿AjaxHelper = {
    ajaxRequest: function (url, data, success, error, mimeType, showMask, showErrorMessage, contentType) {
        if (showMask) {
            MaskHelper.ShowBlockUi();
        } 
        return jQuery.ajax({
            dataType: mimeType,
            contentType: contentType,
            //timeout: 120000,
            url: url,
            type: 'POST',
            cache: false,
            data: data,
            success: function (jsondata) {
                if (showMask) {
                    MaskHelper.HideBlockUI();
                }
                //console.warn(jsondata);
                if (mimeType == 'json') {
                    if (jsondata.success === false) {
                        if (showErrorMessage)
                            CommonHelper.Exception(jsondata.data);
                        error(jsondata.data);
                    } else {
                        success(jsondata.data);
                    }
                } else {
                    var len = jsondata.length;
                    var isError = len > 0 && jsondata.charAt(0) == '{' && jsondata.charAt(len - 1) == '}';
                    if (isError) {
                        try {
                            var json = JSON.parse(jsondata);
                            if (showErrorMessage)
                                CommonHelper.Exception(json);
                            error(json);
                        } catch (e) {
                            isError = false;
                        }
                    }
                    if (!isError) {
                        success(jsondata);
                    }
                }
                MaskHelper.HideBlockUI();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var needAuth = xhr.getResponseHeader("NeedAuth");
                if (needAuth) {
                    location.reload(true);
                }
                if (showMask) {
                    MaskHelper.HideBlockUI();
                }

                if (error)
                    error({
                        success: false,
                        Title: 'Ошибка',
                        Message: xhr.status + ': ' + xhr.statusText,
                        ErrorDetails: xhr.responseText
                    });
            }
        });
    },

    loadTreeViewModelItem: function (url, dataParams, success, error) {
        return this.loadJsonData(url, dataParams, success, error);
    },

    loadJsonData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'json', true);
    },

    loadJsonData2: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'json');
    },

    loadHtmlData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'html');
    },

    loadHtml: function (url, dataParams, selector, success, error) {
        MaskHelper.ShowBlockUi();
        return this.ajaxRequest(
			url,
			dataParams,
			function (resp) {
			    jQuery(selector).html(resp);
                if (success) success(resp);
			    MaskHelper.HideBlockUI();
			},
			function (resp) {
			    jQuery(selector).html('<div class="alert alert-danger" style="white-space: pre-wrap;" role="alert"></div>');
			    jQuery(selector).find('.alert').text(resp.Message);
                if (error) error(resp);
			    MaskHelper.HideBlockUI();
			},
			'html');
    },

    ajaxRequestJsonData: function (url, dataParams, success, error, showMask) {
        return this.ajaxRequest(url, dataParams, success, error, 'json', showMask);
    },

    ajaxRequestJsonArray: function (url, dataParams, success, error, showMask) {
        return this.ajaxRequest(url, dataParams, success, error, 'json', showMask, null, 'application/json');
    },

    ajaxRequestXmlData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'xml', true);
    },

    SendPost: function(url, formData) {
        return $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(formData),
            dataType: "json"
        });
    },
    submitWithJwtAccessToken: function(ajaxOptions, done, fail) {
        var jwtAccessToken = JWTAccessTokenHelper.getJwtAccessToken(3);

        if (!ajaxOptions.headers) {
            ajaxOptions.headers = {};
        }

        ajaxOptions.headers["Authorization"] = jwtAccessToken;

        $.ajax(ajaxOptions)
            .done(done)
            .fail(function(xhr, textStatus, error) {
                var isInvalidToken = xhr.getResponseHeader("Invalid-Token");
                if (isInvalidToken) {
                    if (fail) {
                        fail(xhr, textStatus, error);
                    }
                    return;
                }
                   
                var isExpiredToken = xhr.getResponseHeader("Token-Expired");

                if (isExpiredToken) {
                    
                    JWTAccessTokenHelper.removeJwtTokenFromLocalStorage();
                    
                    setTimeout(function() {
                            AjaxHelper.submitWithJwtAccessToken(ajaxOptions, done, fail);
                        },
                        1);

                    return;
                }
                
                if (fail) {
                    fail(xhr, textStatus, error);
                }
            });


    }
}