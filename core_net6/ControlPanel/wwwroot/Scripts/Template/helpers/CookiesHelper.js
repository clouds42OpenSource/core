﻿// Хэлпер для проверки куков в браузере клиента
CookiesHelper = {
    // Проверить включены ли куки в браузере клиента
    areCookiesEnabled: function() {
        try {
            document.cookie = 'cookietest=1';
            var cookiesEnabled = document.cookie.indexOf('cookietest=') !== -1;
            document.cookie = 'cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT';
            return cookiesEnabled;
        } catch (e) {
            return false;
        }
    },

    // Показать сообщение о выключенных куках
    showCookiesErrorMessage: function() {
        swal({
            title: "Отключено сохранение файлов cookie",
            text: "Включите файлы cookie в браузере и повторите попытку.",
            icon: "warning",
            buttons: {
                close: {
                    text: 'Ок',
                    className: "btn btn-default"
                }
            }
        });
    }
}