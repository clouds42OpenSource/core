﻿/*
 * jQuery File Upload Plugin Localization Example 6.5
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Файл слишком большого размера",
            "minFileSize": "Файл слишком малого размера",
            "acceptFileTypes": "Тип загруженного файла не поддерживается",
            "maxNumberOfFiles": "Превышено максимальное количество файлов",
            "uploadedBytes": "Загруженные данные превышают размер файла",
            "emptyResult": "Загруженный файл пуст",
            "error": "Ошибка",
            "Request Entity Too Large": "Файл слишком большого размера"
        },
        "error": "Ошибка",
        "start": "Загрузить",
        "cancel": "Отмена",
        "destroy": "Удалить"
    }
};
