﻿
// Тип восстановления информационной базы из бэкапа
const databaseBackupRestoreTypeEnum = {

    // Восстановить с заменой в текущую
    ToCurrent: 0,

    // Восстановить в новую
    ToNew: 1
}