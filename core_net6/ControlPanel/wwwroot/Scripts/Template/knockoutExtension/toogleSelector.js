﻿ko.bindingHandlers.toggleAll = {
    update: function (element, valueAccessor, allBindingsAccessor) {
        var aa = allBindingsAccessor();
        var selector = aa.selector;
        var va = valueAccessor();
        var value = ko.utils.unwrapObservable(va);
        if (value) {
            $(selector).children().removeAttr('disabled');
        }
        else {
            $(selector).children().attr('disabled', 'disabled');
        }
    }
};