﻿ko.bindingHandlers.select2 = {
    after: ["options", "value"],
    init: function (el, valueAccessor, allBindingsAccessor, viewModel) {
        var params = ko.unwrap(valueAccessor());

        var key = $(el).attr("key");

        $(el).addClass("knockout-select2-" + key);
        $(el).select2(params);

        ko.utils.domNodeDisposal.addDisposeCallback(el, function () { $(el).select2('destroy'); });

        $(el).on('select2:select', function (e) {
            ko.utils.arrayForEach($(".knockout-select2-" + key), function (select2Item) {
                $(select2Item).select2("destroy");
                $(select2Item).select2(params);
            });
        });
    },
    update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
        var parameters = ko.unwrap(allBindingsAccessor().select2);
        var newValue = ko.unwrap(allBindingsAccessor().value);
        var key = $(el).attr("key");

        ko.utils.arrayForEach($(".knockout-select2-" + key), function (select2Item) {
            $(select2Item).select2("destroy");
            $(select2Item).select2(parameters);
        });
    }
};

ko.subscribable.fn.subscribeChanged = function (callback) {
    var oldValue;
    this.subscribe(function (val) { oldValue = val; }, this, 'beforeChange');
    this.subscribe(function (val) { callback(val, oldValue); });
};

ko.bindingHandlers.pressEnter = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        var callback = valueAccessor();
        $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                callback.call(viewModel);
                return false;
            }
            return true;
        });
    }
};


var ko_wrapper = function (ko) {
    var data = {};

    data.wrap = function (obj) {
        if (Object.prototype.toString.call(obj) == '[object Array]'
            || Object.prototype.toString.call(obj) == '[object Object]') {
            for (var i in obj) {
                obj[i] = data.wrap(obj[i]);
            }
        }

        if (Object.prototype.toString.call(obj) == '[object Array]') {
            return ko.observableArray(obj);
        }
        else {
            return ko.observable(obj);
        }
    }

    data.unwrap = function (obj) {
        if (Object.prototype.toString.call(obj) == '[object Array]'
            || Object.prototype.toString.call(obj) == '[object Object]') {
            for (var i in obj()) {
                obj[i] = data.unwrap(obj()[i]);
            }
        }

        if (typeof (value) === 'function') {
            return obj();
        }
        else {
            return obj;
        }
    }

    return data;
}

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = {
            format: 'DD.MM.YYYY HH:mm:ss',
            locale: 'ru',
            defaultDate: valueAccessor()()
        };

        if (allBindingsAccessor() !== undefined) {
            if (allBindingsAccessor().datepickerOptions !== undefined) {
                options.format = allBindingsAccessor().datepickerOptions.format !== undefined ? allBindingsAccessor().datepickerOptions.format : options.format;

                options.maxDate = allBindingsAccessor().datepickerOptions.setMaxDateNow !== undefined &&
                    allBindingsAccessor().datepickerOptions.setMaxDateNow !== false
                    ? Date.now()
                    : allBindingsAccessor().datepickerOptions.maxDate;

                options.minDate = allBindingsAccessor().datepickerOptions.setMinDateNow !== undefined &&
                    allBindingsAccessor().datepickerOptions.setMinDateNow !== false
                    ? Date.now()
                    : allBindingsAccessor().datepickerOptions.minDate;
            }
        }

        $(element).datetimepicker(options);

        ko.utils.registerEventHandler(element, "dp.change", function (event) {
            var value = valueAccessor();
            if (ko.isObservable(value)) {
                value(event.date);
            }
        });

        var defaultVal = $(element).val();

        if (defaultVal === undefined || defaultVal === '') {
            defaultVal = new Date();
            element.value = moment(defaultVal).format(options.format);
        }
        
        var value = valueAccessor();
        value(moment(defaultVal, options.format));
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var thisFormat = 'DD.MM.YYYY HH:mm:ss';
        var defaultFormat = 'MM-DD-YYYY';
        var minDate = "01.01.1753";

        if (allBindingsAccessor() !== undefined) {
            if (allBindingsAccessor().datepickerOptions !== undefined) {
                thisFormat = allBindingsAccessor().datepickerOptions.format !== undefined ? allBindingsAccessor().datepickerOptions.format : thisFormat;
            }
        }

        var value = valueAccessor();
        var unwrapped = ko.utils.unwrapObservable(value());

        if (unwrapped === undefined || unwrapped === null || unwrapped === false) {
            element.value = moment(new Date()).format(thisFormat);
        } else {
            element.value = unwrapped.format(thisFormat);
        }

        if (new Date(moment(element.value, thisFormat).format(defaultFormat)) < new Date(moment(minDate, thisFormat).format(defaultFormat))) {
            element.value = moment(minDate).format(thisFormat);
        }
        value(element.value);
    }
};

ko.bindingHandlers.inputmask = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var obj = allBindingsAccessor();
        $(element).inputmask(obj.settings);

        var eventHandlerType = obj.eventHandlerType !== undefined ? obj.eventHandlerType : 'focusout';

        ko.utils.registerEventHandler(element, eventHandlerType, function () {
            var observable = valueAccessor();
            var value = $(element).val();
            var charactersdelete = allBindingsAccessor().charactersToRemove;

            if (value !== null && value !== undefined && charactersdelete !== null && charactersdelete !== undefined) {
                for (var i = 0; i < charactersdelete.length; i++) {
                    value = value.toString().split(charactersdelete[i]).join('');
                }
                value = value.toString().split('_').join('');
            }

            observable(value);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var observable = valueAccessor();
        var charactersdelete = allBindingsAccessor().charactersToRemove;

        if (value !== null && value !== undefined && charactersdelete !== null && charactersdelete !== undefined) {
            for (var i = 0; i < charactersdelete.length; i++) {
                value = value.toString().split(charactersdelete[i]).join('');
            }
            value = value.toString().split('_').join('');
        }

        observable(value);
        $(element).val(value);
    }
};

ko.bindingHandlers.limitCharactersControl = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var obj = allBindingsAccessor();
        var eventHandlerType = obj.eventHandlerType !== undefined ? obj.eventHandlerType : 'input';

        $(element).attr('maxlength', obj.limitCharacters);
        $("<span class='" + obj.cssClass + "' style='display: none;' id='" + obj.id + "'></span>").insertAfter(element);

        ko.utils.registerEventHandler(element, eventHandlerType, function () {
            var observable = valueAccessor();
            var value = $(element).val();
            observable(value);
        });

        $(element).focus(function() {
            $("#" + obj.id).show();
        })
        .blur(function () {
            $("#" + obj.id).hide();
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var obj = allBindingsAccessor();
        var value = ko.utils.unwrapObservable(valueAccessor());
        var valueLength = 0;
        var observable = valueAccessor();

        if (value !== undefined && value !== null)
            valueLength = value.length;

        var residue = obj.limitCharacters - valueLength;
        if (residue < 0)
            residue = 0;

        $("#" + obj.id).html("Осталось символов: " + residue);

        observable(value);
        $(element).val(value);
    }
}

ko.bindingHandlers.summernote = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        var obj = allBindingsAccessor();
        var observable = valueAccessor();
        var value = ko.utils.unwrapObservable(valueAccessor());

        var optionsBinding = obj.Options || {};

        optionsBinding.callbacks = {};
        optionsBinding.callbacks.onChange = function (contents, $editable) {
            observable($(element).summernote('code'));
        }
        $(element).summernote(optionsBinding);
        $(element).summernote("code", value);

        if (obj.CanDisable)
            $(element).summernote('disable');
    },

    update: function (element, valueAccessor, allBindingsAccessor) {

        var obj = allBindingsAccessor();
       

        if (obj.CanDisable)
            $(element).summernote('disable');
        else $(element).summernote('enable');
    }
},


ko.bindingHandlers.tooltip = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).tooltip({
            title: value
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).tooltip({
            title: value
        });
    }
}

ko.validation.rules['checkDate'] = {
    validator: function (val, date) {

        if (typeof val === 'string' || val instanceof String) {
            var dateNow = new Date(moment(date, 'DD-MM-YYYY').format('MM-DD-YYYY'));
            var dateSelected = new Date(moment(val, 'DD-MM-YYYY').format('MM-DD-YYYY'));

            if (dateSelected > dateNow)
                return false;

            return true;
        } 
            return true;
    },
    message: 'Поле даты не валидно. Этот день ещё не наступил'
};

ko.bindingHandlers.unfocusAfterClick = {
    init: function (element, valueAccessor, allBindingsAccessor,
        viewModel, bindingContext) {

        var newValueAccesssor = function () {
            return function () {
                valueAccessor().apply(viewModel);
                $(element).blur();
            }
        }

        ko.bindingHandlers.click.init(element, newValueAccesssor, allBindingsAccessor, viewModel, bindingContext);
    }
};


ko.bindingHandlers.cropperImage = {
    init: function (element, valueAccessor, allBindingsAccessor) {

        var obj = allBindingsAccessor();
        var observable = valueAccessor();
        var imageImputId = obj.settings.imageImputId;

        obj.settings.Options.crop = function (event) {

            $("#" + element.id).cropper('getCroppedCanvas',
                {
                    width: obj.settings.width,
                    height: obj.settings.height,
                    imageSmoothingEnabled: 'true',
                    imageSmoothingQuality: 'high'
                }).toBlob(function (blob) {

                    if (blob !== null) {
                        blob.lastModifiedDate = new Date();
                        blob.name = observable().FileName;
                        observable().Content = blob;
                    }
                });
        }

        if (observable() !== null &&
            observable().FileName !== null &&
            observable().FileName !== undefined &&
            observable().FileName !== "") {

            var options = obj.settings.Options;
            options.data = {
                top: parseFloat(0),
                left: parseFloat(0),
                with: obj.settings.width,
                height: obj.settings.height
            };

            $("#" + element.id).attr('src', "data:" + observable().ContentType + ";base64," + observable().Base64);
            $("#" + element.id).cropper(options);
        }

        $("#" + imageImputId).on('change',
            function () {
                var file = this.files[0];
                var format = file.name.split(".").pop();

                if (obj.settings.accept !== null && obj.settings.accept.length > 0) {

                    var find = ko.utils.arrayFirst(obj.settings.accept,
                        function (e) { return e === format });

                    if (find === undefined || find === null) {
                        Attention.Show("Ошибка", "Логотип вашего сервиса может иметь один из следующих форматов: " + obj.settings.accept.join(', '));
                        return;
                    }
                }

                var img = new Image();
                var url = window.URL || window.webkitURL;
                img.src = url.createObjectURL(file);

                img.onload = function () {
                    if ((obj.settings.minWidth !== null && obj.settings.minHeight !== null) && (this.width < obj.settings.minWidth || this.height < obj.settings.minHeight)) {
                        Attention.Show("Ошибка", "Изображения для логотипа должно быть размером не меньше " + obj.settings.minWidth + "*" + obj.settings.minHeight);
                        return;
                    }

                    obj.settings.onUploadImage(file);

                    observable({
                        FileName: file.name,
                        ContentType: file.type,
                        Content: file
                    });

                    new Promise(function (resolve, reject) {
                        const reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function () { resolve(reader.result); }
                        reader.onerror = function (error) { reject(error); }
                    }).then(
                        function (data) {

                            if ($("#" + element.id).cropper !== undefined) {
                                $("#" + element.id).cropper('destroy');
                                $("#" + element.id).removeAttr('src');
                            }

                            obj.settings.Options.data = null;
                            $("#" + element.id).attr('src', data);
                            $("#" + element.id).cropper(obj.settings.Options);
                        }
                    );
                }
            });

        if (obj.settings.containerId !== null)
            $('#' + obj.settings.containerId).show();
    }
};


ko.bindingHandlers.numeric = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var obj = allBindingsAccessor();
        var eventHandlerType = obj.eventHandlerType !== undefined ? obj.eventHandlerType : 'input';

        ko.utils.registerEventHandler(element, eventHandlerType, function () {
            var observable = valueAccessor();
            var value = $(element).val();

            if (value !== null && value !== undefined && value !== "") {
                value = value.toString().replace(/\D+/g, '');
            }
            
            observable(value);
            $(element).val(value);
        });
    }
    ,
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var observable = valueAccessor();

        if (value !== null && value !== undefined && value !== "") {
            value = value.toString().replace(/\D+/g, '');
        }

        observable(value);
        $(element).val(value);
    }
};

ko.bindingHandlers.clipboard = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var settings = allBindingsAccessor().settings;
        var message = ko.utils.unwrapObservable(settings.message);
        var toCopy = ko.utils.unwrapObservable(valueAccessor());
        var clipboard = new ClipboardJS(element,
            {
                text: function (trigger) {

                    if (message !== null && message !== undefined && message !== "")
                        Attention.Show("Выполнено", message);

                    return toCopy;
                }
            });
    }
};

ko.bindingHandlers.blocksize = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var settings = allBindingsAccessor().settings;
        var initialSize = 80;
        if (settings !== null && settings !== undefined) {
            initialSize = settings.initialSize
        }

        var tabBlocks = [];
        var needCalculate = false;

        $.each($(element).find('.sortTab'),
            function (id, block) {
                tabBlocks.push({
                    Id: id,
                    Block: $(block)
                })
            });

        heightChanged();

        function heightChanged() {
            if ($(window).width() > 1150 || needCalculate === false) {
                $.each(tabBlocks,
                    function (id, blockItem) {
                        if (blockItem.Block.find('ul').height() > 0) {
                            needCalculate = true;
                        }
                        blockItem.Block.css('height', initialSize + '%');
                    });
                setTimeout(heightChanged, 50);
                return;
            }

            var maxHeight = 0;
            var highestBlockId = null;

            $.each(tabBlocks,
                function (id, blockItem) {
                    if (blockItem.Block.height() > maxHeight) {
                        highestBlockId = blockItem.Id;
                        maxHeight = blockItem.Block.height();
                    }
                });
            $.each(tabBlocks,
                function (id, blockItem) {
                    if (blockItem.Id !== highestBlockId) {
                        blockItem.Block.height(maxHeight);
                    }
                });
            setTimeout(heightChanged, 50);
        }
    }
}


var inject_binding = function (allBindings, key, value) {
    return {
        has: function (bindingKey) {
            return (bindingKey === key) || allBindings.has(bindingKey);
        },
        get: function (bindingKey) {
            var binding = allBindings.get(bindingKey);
            if (bindingKey === key) {
                binding = binding ? [].concat(binding, value) : value;
            }
            return binding;
        }
    };
}

ko.bindingHandlers.selectize = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (!allBindingsAccessor.has('optionsText'))
            allBindingsAccessor = inject_binding(allBindingsAccessor, 'optionsText', 'Name');
        if (!allBindingsAccessor.has('optionsValue'))
            allBindingsAccessor = inject_binding(allBindingsAccessor, 'optionsValue', 'Id');
        if (typeof allBindingsAccessor.get('optionsCaption') == 'undefined')
            allBindingsAccessor = inject_binding(allBindingsAccessor, 'optionsCaption', '--Не выбрано--');

        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        var options = {
            valueField: allBindingsAccessor.get('optionsValue'),
            labelField: allBindingsAccessor.get('optionsText'),
            searchField: allBindingsAccessor.get('optionsText')
        }

        if (allBindingsAccessor.has('options')) {
            var passed_options = allBindingsAccessor.get('options');
            for (var attr_name in passed_options) {
                options[attr_name] = passed_options[attr_name];
            }
        }

        var $select = $(element).selectize(options)[0].selectize;

        if (typeof allBindingsAccessor.get('value') == 'function') {
            $select.addItem(allBindingsAccessor.get('value')());
            allBindingsAccessor.get('value').subscribe(function(new_val) {
                $select.addItem(new_val);
            });
        }

        if (typeof allBindingsAccessor.get('selectedOptions') == 'function') {
            allBindingsAccessor.get('selectedOptions').subscribe(function (new_val) {
                var values = $select.getValue();
                var items_to_remove = [];
                for (var k in values) {
                    if (new_val.indexOf(values[k]) == -1) {
                        items_to_remove.push(values[k]);
                    }
                }

                for (var k in items_to_remove) {
                    $select.removeItem(items_to_remove[k]);
                }

                for (var k in new_val) {
                    $select.addItem(new_val[k]);
                }

            });
            var selected = allBindingsAccessor.get('selectedOptions')();
            for (var k in selected) {
                $select.addItem(selected[k]);
            }
        }


        if (typeof init_selectize == 'function') {
            init_selectize($select);
        }

        if (typeof valueAccessor().subscribe == 'function') {
            valueAccessor().subscribe(function (changes) {
                // To avoid having duplicate keys, all delete operations will go first
                var addedItems = new Array();
                changes.forEach(function (change) {
                    switch (change.status) {
                        case 'added':
                            addedItems.push(change.value);
                            break;
                        case 'deleted':
                            var itemId = change.value[options.valueField];
                            if (itemId != null) $select.removeOption(itemId);
                    }
                });
                addedItems.forEach(function (item) {
                    $select.addOption(item);
                });

            }, null, "arrayChange");
        }

    },
    update: function (element, valueAccessor, allBindingsAccessor) {

        if (allBindingsAccessor.has('object')) {
            var optionsValue = allBindingsAccessor.get('optionsValue') || 'Id';
            var value_accessor = valueAccessor();
            var selected_obj = $.grep(value_accessor(), function (i) {
                if (typeof i[optionsValue] == 'function')
                    var id = i[optionsValue];
                else
                    var id = i[optionsValue];
                return id === allBindingsAccessor.get('value')();
            })[0];

            if (selected_obj) {
                allBindingsAccessor.get('object')(selected_obj);
            }
        }
    }
}

ko.bindingHandlers.clickLock = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var wrappedValueAccessor = function () {
            return function (data, event) {

                ko.bindingHandlers.clickLock.preOnClick.call(viewModel, data, event, element);

                var actionAfterClick = function () {
                    element.disabled = false;
                }

                valueAccessor().call(viewModel, data, event, actionAfterClick);
            };

        };

        ko.bindingHandlers.click.init(element, wrappedValueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: ko.bindingHandlers.click.update,
    preOnClick: function (data, event, element ) {
        element.setAttribute("disabled", true);
    }
};

// Отключить автозаполнение
ko.bindingHandlers.disableAutoComplete = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var settings = allBindingsAccessor().disableAutoCompleteSettings;
        if (settings === null || settings === undefined) {
            settings = {
                isPasswordInput: false
            }
        }

        const isPasswordInput = settings.isPasswordInput;
        $(element).attr("autocomplete", "new-password");
        $(element).attr("data-lpignore", "true");
        if (isPasswordInput === true) {
            $(element).addClass("custom-password-input");
        }
    }
};