﻿
// Хелпер для работы с моделью "Данные пользователей аккаунта"
const accountUsersDataModelHelper = {

    // Обновить данные пользователей аккаунта (старые значения являются observable массивом)
    updateUsersData: function (oldValues, newValues) {

        var generateModel = this._generateModel.bind(this);

        oldValues.removeAll();

        ko.utils.arrayForEach(newValues || [],
            function (item) {
                var userData = generateModel(item);
                oldValues.push(userData);
            });
    },

    // Сформировать параметры запроса, для получения моделей пользователей
    generateParametersForGetUsers: function (searchString, accountId, sortingOptions, pageOptions) {
        return {
            Filter: {
                SearchString: searchString,
                PageSize: pageOptions.PageSize,
                AccountId: accountId
            },
            PageNumber: pageOptions.PageNumber,
            SortingData: {
                SortKind: sortingOptions.SortType,
                FieldName: sortingOptions.SortFieldName
            }
        };
    },

    // Сформировать модель данных о пользователе аккаунта
    _generateModel: function(item) {
        return {

            // Id пользователя
            Id: ko.observable(item.Id),

            // Логин пользователя
            Login: ko.observable(item.Login),

            // Фамилия пользователя
            LastName: ko.observable(item.LastName),

            // Имя пользователя
            FirstName: ko.observable(item.FirstName),

            // Отчество пользователя
            MiddleName: ko.observable(item.MiddleName),

            // ФИО пользователя
            FullName: ko.observable(item.FullName),

            // Признак что пользователь админ аккаунта
            IsAccountAdmin: ko.observable(item.IsAccountAdmin),

            // Телефон пользователя
            PhoneNumber: ko.observable(item.PhoneNumber),

            // Электронная почта пользователя
            Email: ko.observable(item.Email),

            // Признак что пользователь активирован
            IsActivated: ko.observable(item.IsActivated)
        }
    }
}