﻿
// Хелпер для работы с моделью общих/базовых данных о пользователях(для работы с пользователями)
const usersCommonDataModelHelper = {

    // Обновить данные модели модели "Общие/базовые данные о пользователях(для работы с пользователями)"
    updateData: function(oldModel, newModel) {

        oldModel.AccountId(newModel.AccountId);
        oldModel.MyDatabaseServiceId(newModel.MyDatabaseServiceId);
        oldModel.PermissionsForWorkingWithUsers.HasPermissionForAddAccountUser(newModel.PermissionsForWorkingWithUsers
            .HasPermissionForAddAccountUser);
    },

    // Получить исходные/начальные данные модели "Общие/базовые данные о пользователях(для работы с пользователями)"
    getInitialData: function() {

        return {

            // Id аккаунта
            AccountId: ko.observable(null),

            // Id сервиса "Мои инф. базы"
            MyDatabaseServiceId: ko.observable(null),

            // Разрешения для работы с пользователями аккаунта
            PermissionsForWorkingWithUsers: {

                // Имеет разрешение на добавление нового пользователя
                HasPermissionForAddAccountUser: ko.observable(false)
            }
        };
    }
}