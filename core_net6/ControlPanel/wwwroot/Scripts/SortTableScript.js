﻿function SortTableScript(config) {
    var vm = this;
    vm.Config = config;
    $(document).on("click",
        ".sort",
        function() {

            ajaxSortDatabase($(this));

        });

    function ajaxSortDatabase(element) {
        var page = 1;
        if (element)
            page = element.data("page");

        var accountId = $("[name='account_id']").val();
        var sortName = element.data('name');
        var sortDir = element.data('dir');
        $.ajax({
            url: vm.Config._url.Filter,
            data: { page: page, accountId: accountId, sortName: sortName, sortDir: sortDir },
            type: "post",
            beforeSend: function() {

            },
            success: function(data) {
                $('#account-movebase-form').html(data);
                $('#orderState').data('name', sortName);
                if (sortDir == "OrderBy") {
                    $(`#${sortName}`).data('dir', 'OrderByDescending');
                    $('#orderState').find("th#" + sortName).data('dir', 'OrderByDescending');
                } else {
                    $(`#${sortName}`).data('dir', 'OrderBy');
                    $('#orderState').find("th#" + sortName).data('dir', 'OrderBy');
                }

            }
        });
    }
}