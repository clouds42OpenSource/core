﻿function SortAccountDatabaseScript(config) {
    var vm = this;
    vm.Config = config;
    $(document).on("click",
        ".sort",
        function () {
            ajaxFilterDatabase($(this));
        });
    $(document).on("click", "#searchInfoBase",
        function() {
            ajaxFilterDatabase($(this)); 
        });

    $(document).on("click", "#searchClearDb",
        function () {
            $('#searchQueryNumber').val("");
            $('#searchQueryPath').val("");
            $('#searchType').val("");
            $('#searchQuery').val("");
            $('#searchPathDrop').val("");
            ajaxFilterDatabase($(this));
        });
        function hideallDropdowns() {
            $(".dropped .drop-menu-main-sub").hide();
            $(".dropped").removeClass('dropped');
            $(".dropped .drop-menu-main-sub .title").unbind("click");
        }
        function hideallDropdownsPath() {
            $(".droppedPath .drop-menu-main-sub-pat").hide();
            $(".droppedPath").removeClass('droppedPath');
            $(".droppedPath .drop-menu-main-sub-path .title").unbind("click");
        }
        function hideallDropdownsType() {
            $(".droppedType .drop-menu-main-sub-type").hide();
            $(".droppedType").removeClass('droppedType');
            $(".droppedType .drop-menu-main-sub-type .title").unbind("click");
        }
        function showDropdown(el) {
            var el_li = $(el).parent().addClass('dropped');
            el_li
                .find('.title')
                .click(function () {
                    hideallDropdowns();
                })
                .html($(el).html());
            el_li.find('.drop-menu-main-sub').show();
        }
        function showDropdownPath(el) {
            var el_li = $(el).parent().addClass('droppedPath');
            el_li
                .find('.title')
                .click(function () {
                    hideallDropdownsPath();
                })
                .html($(el).html());
            el_li.find('.drop-menu-main-sub-path').show();
        }
        function showDropdownType(el) {
            var el_li = $(el).parent().addClass('droppedType');
            el_li
                .find('.title')
                .click(function () {
                    hideallDropdownsType();
                })
                .html($(el).html());
            el_li.find('.drop-menu-main-sub-type').show();
        }
        $(document).on("click", "#drop-down", function () {
            showDropdown(this);
        });
        $(document).on("click", "#drop-down-path", function () { 
            showDropdownPath(this);
        });
        $(document).on("click", "#drop-down-type", function () { 
            showDropdownType(this);
        });


    function ajaxFilterDatabase(element) {
        var page = $('.ajaxlink').data("activ");
        var accountId = $("[name='account_id']").val();
        var sortName = element.data('name');
        var sortDir = element.data('dir');
        var search = $('#searchQuery').val();
        var filterNumber = $('#searchQueryNumber').val();
        var filterPath = $('#searchQueryPath').val();
        var filterType = $('#searchType').val();
        var filterPathDrop = $('#searchPathDrop').val();
        $.ajax({
            url: vm.Config._url.Filter,
            data: { page: page, accountId: accountId, sortName: sortName, sortDir: sortDir, filterNumber: filterNumber, filterPath: filterPath, filterType: filterType, search: search, filterPathDrop: filterPathDrop },
            cache: false,
            beforeSend: function () {
                MaskHelper.ShowBlockUi($('.modal-content'));
            },
            success: function (data) {
                $('#account-movebase-form').html(data);
                $('#searchQuery').val(search);
                $('#searchPathDrop').val(filterPathDrop);
                $('#searchQueryNumber').val(filterNumber);
                $('#searchQueryPath').val(filterPath);
                $('#searchType').val(filterType);
                $('#orderState').data('name', sortName);
                if (sortDir === "OrderBy") {
                    $('#sortName').data('dir', 'OrderByDescending');
                    $('#orderState').find("th#" + sortName).data('dir', 'OrderByDescending');
                } else {
                    $('#sortName').data('dir', 'OrderBy');
                    $('#orderState').find("th#" + sortName).data('dir', 'OrderBy');
                }
                MaskHelper.HideBlockUI($('.modal-content'));
            }
        });
    }

}