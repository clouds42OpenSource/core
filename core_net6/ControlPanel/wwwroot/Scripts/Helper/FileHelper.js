﻿// Объект хелпер для работы с файлами
var FileHelper = {

    // Сконфертировать файл в строку Base64
    getBase64: function(file) {
        return new Promise(function (resolve, reject) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () { resolve(reader.result); }
            reader.onerror = function (error) { reject(error); }
        });
    },

    // Проверить валидность загруженного файла
    validateFormatFile: function(file, validFormats) {
        var format = file.name.split(".").pop().toLowerCase();

        var findFormat = ko.utils.arrayFirst(validFormats,
            function (item) {
                return item === format;
            });

        if (findFormat)
            return {
                Success: true,
                Message: ''
            };

        return {
            Success: false,
            Message: 'Ваш файл ' + file.name + ' имеет не верный формат. Допустимые форматы файла ' +
                validFormats.join(', ')
        }
    },

    // Сгенерировать строку Base64 для отображения
    generateBase64StringToDisplay: function (contentType, base64) {
        return "data:" + contentType + ";base64," + base64;
    }
}