﻿$(document).ready(function () {
    var mutationObserver = new window.MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            saveBlockState(mutation);
        });
    });

    mutationObserver.observe(document.getElementById("side-menu"), {
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
    });

    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
        var item = cookies[i].replace(/\s+/g, '');

        if (item[0] !== '_') {
            var cookieName = getCookieName(item);
            var cookieValue = getCookieValue(item);
            changeElementState(cookieName, cookieValue);
        }
    }
});

//Сохранить состояние блока
function saveBlockState(mutation) {
    var blockName = $(mutation.target).attr('let');
    if (blockName === null || blockName === undefined) {
        return;
    }
    var blockState = getElementState($(mutation.target));
    setMenuItemStateToCookie(blockName, blockState);
}

// Получить имя куки
function getCookieName(cookie) {
    return cookie.split('=')[0].replace(/\s+/g, '');
}

// Получить значение куки
function getCookieValue(cookie) {
    try {
        return JSON.parse(cookie.split('=')[1]);
    }
    catch (err){
        return null;
    }
}

// Сменить состояние блока меню при загрузке страницы
function changeElementState(name, value) {
    var element = getElementByName(name);
    if (value === null || value === undefined) {
        return;
    }
    if (value === false) {
        $(element).removeClass('active');
        return;
    }
    $(element).addClass('active');
}

//Записать состояние блока в куки
function setMenuItemStateToCookie(name, value) {
    document.cookie = name.replace(/\s+/g, '') + "=" + value + "; Max-Age=99999999; path=/";
}

// Получить состояние элемента (true - если развернут)
function getElementState(element) {
    var elementClass = $(element).attr('class');
    if (~elementClass.indexOf("active")) {
        return true;
    }
    return false;
}

// Получить элемент по имени
function getElementByName(name) {
    var matchingElements = [];
    var allElements = document.getElementsByTagName('*');
    for (var i = 0, n = allElements.length; i < n; i++) {
        if (allElements[i].getAttribute('let') !== null) {
            if (!name || allElements[i].getAttribute('let') == name)
                matchingElements.push(allElements[i]);
        }
    }
    return matchingElements[0];
}