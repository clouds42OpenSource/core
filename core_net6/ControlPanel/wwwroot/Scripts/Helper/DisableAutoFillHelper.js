﻿// Хелпер отключение автозаполнения
DisableAutoFillHelper = new function () {

    // Отключение авто заполнения на форме
    this.DisableAutoFillInForm = function () {
        $('form').attr('autocomplete', 'off');
        $('input').attr('autocomplete', 'new-password');
    }

    //  Отключение автозаполнения для поля
    this.DisableAutoFillForField = function (idElement) {
        $('#' + idElement).attr('autocomplete', 'new-password');
    }
}