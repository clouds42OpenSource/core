﻿var ModalType = {
    Default: {
        Small: "small",
        Normal: "normal",
        Large: "large"
    },
    Custom: {
        _10_percent: "modal-10p",
        _20_percent: "modal-20p",
        _30_percent: "modal-30p",
        _40_percent: "modal-40p",
        _50_percent: "modal-50p",
        _60_percent: "modal-60p",
        _70_percent: "modal-70p",
        _80_percent: "modal-80p",
        _90_percent: "modal-90p",
        _100_percent: "modal-100p"
    }
}

Modal = new function () {
    this.Open = function (args) {

        if (!validate())
            return;

        if (args.url == undefined) {
            openModal(args.body);
            return;
        }

        MaskHelper.ShowBlockUiBody();

        $.get(args.url, args.params)
            .done(function(data) {
                MaskHelper.HideBlockUIBody();
                if (!data.success && data.data != undefined) {
                    Attention.Show("Ошибка", data.data.message);
                    return;
                }
                openModal(data.html);
            });

        function validate() {

            let valid = true;

            if (args == undefined) {
                console.log("Exception: args = undefined");
                valid = false;
            }

            if (args.type == undefined) {
                args.type = undefined;
                console.log("Exception: args.type = undefined");
                valid = false;
            }

            if (args.url == undefined && args.body == undefined) {
                args.url = undefined;
                args.body = undefined;
                console.log("Exception: args.url = undefined and args.body = undefined. you must specify one of the parameters");
                valid = false;
            }

            if (args.title == undefined) {
                args.title = undefined;
                console.log("Exception: args.title = undefined");
                valid = false;
            }

            if (args.params == undefined) {
                args.params = {};
            }

            if (args.buttons == undefined) {
                args.buttons = {};
            }

            if (args.id == undefined) {
                args.id = undefined;
            }

            if (args.callback == undefined)
                args.callback = undefined;

            return valid;
        }

        function openModal(html) {
            var box = bootbox.dialog({
                size: args.type,
                className: args.type + " " + args.id,
                title: args.title,
                message: html,
                backdrop: true,
                animate: true,
                onEscape: args.onEscape,
                buttons: args.buttons
            });

            $(".bootbox").removeAttr("tabindex");

            if (args.callback != undefined)
                box.on("shown.bs.modal", args.callback);
        }
    }
    this.Close = function (id) {
        $("." + id).find("button[data-dismiss]").click();
    };
}