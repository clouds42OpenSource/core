﻿UIHelper = {
    ShowBlock: function (block) {
        block.removeClass('hiddenContainer');
        block.addClass('visibleContainer');
    },
    HideBlock: function (block) {
        block.removeClass('visibleContainer');
        block.addClass('hiddenContainer');
    }
}

// Хелпер для отображения пароля
ShowPasswordHelper = new function() {

    // Включить возможность отображать пароль
    this.On = function (id) {

        var search = "#" + id;
        $(search + " span").on('click', function (event) {

            event.preventDefault();

            if ($(search + " input").attr("type") === "text") {
                $(search + " input").attr('type', 'password');
                $(search + " i").addClass("fa-eye-slash");
                $(search + " i").removeClass("fa-eye");
            } else if ($(search + " input").attr("type") === "password") {
                $(search + " input").attr('type', 'text');
                $(search + " i").removeClass("fa-eye-slash");
                $(search + " i").addClass("fa-eye");
            }
        });
    }
}

CommonValidator = new function ()
{
    this.EmailIsValid = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

CommonHelper = new function () {   

    this.JsonEscape = function(str) {
        return str.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
    };

    this.MegabyteToClassicFormat = function(mb) {
        var gb = Math.round(10 * (mb / 1024)) / 10;
        return gb + ' Гб';
    }

    this.JsDateTimeToString = function(date, format) {
        if (date) {
            var myDateValue = new Date(date.match(/\d+/)[0] * 1);
            if (format)
                return dateFormat(myDateValue, format);
            return myDateValue.format();            
        }
        return '---';
    };

    this.DecodeHtml = function(html) {
        var txt = document.createElement("textarea");
        txt.innerHTML = html;
        return txt.value;
    };

    this.GetFormOfject = function(formId) {
        var formData = {};
        var arrayParams = $('#' + formId).serializeArray();
        for (var i = 0; i < arrayParams.length; i++) {
            formData[arrayParams[i].name] = arrayParams[i].value;
        }
        return formData;
    };    

    this.alert = function (msg) {
        bootbox.alert(msg);
    };
    this.warn = function (msg, title) {
        bootbox.dialog({
            title: title || "Внимание",
            message: msg,
            closeButton: true,
            animate: true,
            className: "warning-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });
    };
    this.Exception = function (model) {
        model.Title = model.Title == undefined ? 'Ошибка' : model.Title;
        model.Message = model.Message == undefined ? 'Неопознанная ошибка' : model.Message;
        model.ErrorDetails = model.ErrorDetails == undefined ? '' : model.ErrorDetails;

        bootbox.dialog({
            message: model.Message,
            title: model.Title,
            closeButton: true,
            animate: true,
            className: "error-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });

    };

    this.PayServiceWithPrepayment = function (title, msg, callback_GoToPay, callback_PrePayment) {

        var message = document.createElement("p");
        message.innerHTML = msg;
        swal3B({
                title: title || "Внимание",
                content: message,
                icon: "warning",
                buttons: {
                    close: {
                        text: 'Отмена',
                        className: "btn btn-default"
                    },
                    prePayment: {
                        text: 'Взять обещанный платеж',
                        className: "btn btn-success",
                        value: 'prePayment'
                    },
                    goToPay: {
                        text: 'Оплатить сейчас',
                        className: "btn btn-success",
                        callback: 'goToPay'
                    }
                }
            })
            .then(function (value) {
                switch (value) {
                case "prePayment":
                    callback_PrePayment();
                    break;
                case "goToPay":
                    callback_GoToPay();
                    break;
                }

            });
    }

    this.PayService = function (title, msg, callback_GoToPay) {
        var message = document.createElement("p");
        message.innerHTML = msg;
        swal3B({
                title: title || "Внимание",
                content: message,
                icon: "warning",
                buttons: {
                    close: {
                        text: 'Отмена',
                        className: "btn btn-default"
                    },
                    goToPay: {
                        text: 'Оплатить сейчас',
                        className: "btn btn-success",
                        callback: 'goToPay'
                    }
                }
            })
            .then(function (value) {
                switch (value) {
                case "goToPay":
                    callback_GoToPay();
                    break;
                }

            });
    }

    this.toCurrency = function (sum, Locale, withCurrency) {

        var floatValue = parseFloat(sum);
        if (isNaN(floatValue))
            floatValue = 0.00;
        var value = floatValue.toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');

        if (withCurrency && withCurrency === true) {
            return value + ' ' + Locale.Currency();
        } else {
            return value;
        }
    }

    this.toMoneyFormat = function (sum) {
        var floatValue = parseFloat(sum);
        if (isNaN(floatValue))
            floatValue = 0.00;
        return floatValue.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').replace('.', ',');
    }

};