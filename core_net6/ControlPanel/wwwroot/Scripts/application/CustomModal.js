﻿CustomModal = new function () {
    this.IsLoad = false;
    this.LoadUrl = null;
    this.Instanse = null;

    this.RelodModal = function() {
        if (!CustomModal.IsLoad)
            return;

        AjaxHelper.loadHtml(CustomModal.LoadUrl, {}, $('#custom-modal-dialog-container'),
           function () {               
           }, function () {
              bootbox.alert('Произошла ошибка соединения с сервером, попробуйте еще раз.');
           },
           true
        );

    };

    this.Close = function() {
        if (CustomModal.IsLoad && CustomModal.Instanse)
            CustomModal.Instanse.modal('hide');
    };

    this.Dialog = function (title, loadUrl, saveCallback, closeCallBack, dialogClass) {
        CustomModal.LoadUrl = loadUrl;
        if (!dialogClass)
            dialogClass = 'cutom-dialog-modal';        
        $.ajax({
            url: loadUrl,
            beforeSend: function () {
                CustomModal.Instanse = bootbox.dialog({
                        title: title,
                        message: '<div id="custom-modal-dialog-container"><div id="stubSpinner" class="spinner text-center loading"></div></div>',
                        buttons: {
                            "Сохранить": {
                                className: "btn-success CloseAndSaveBtn",
                                callback: function() {
                                    return saveCallback();                                    
                                }
                            }
                        },
                        className: dialogClass
                    }).
                    on("hide", function() {
                        jQuery('html').css('overflow', 'scroll');
                        CustomModal.IsLoad = false;
                });               
                jQuery('html').css('overflow', 'hidden');
            },
            success: function (result) {
                CustomModal.IsLoad = true;
                $('#custom-modal-dialog-container').html(result);
                $('.CloseAndSaveBtn').removeClass('CloseAndSaveBtn');
            }
        });

        return CustomModal.Instanse;
    };

    this.DialogDetails = function (title, loadUrl, dialogClass) {
        CustomModal.LoadUrl = loadUrl;
        if (!dialogClass)
            dialogClass = 'cutom-dialog-modal';
        $.ajax({
            url: loadUrl,
            beforeSend: function () {
                CustomModal.Instanse = bootbox.dialog({
                    title: title,
                    message: '<div id="custom-modal-dialog-container"><div id="stubSpinner" class="spinner text-center loading"></div></div>',                    
                    className: dialogClass
                }).
                    on("hide", function () {
                        jQuery('html').css('overflow', 'scroll');
                        CustomModal.IsLoad = false;
                    });
                jQuery('html').css('overflow', 'hidden');
            },
            success: function (result) {
                CustomModal.IsLoad = true;
                $('#custom-modal-dialog-container').html(result);                
            }
        });

        return CustomModal.Instanse;
    };

    this.Confirm = function (title, text, callback, labelSuccess, label) {
       var message = text || '';
       CustomModal.Instanse = bootbox.dialog({
            title: title,
            message: '<div>' + message + '</div>',
            className: "confirm-modal-custom",
            buttons: {
                success: {
                    label: labelSuccess || "Ok",
                    className: "btn-success",
                    callback: function () {
                        if (callback)
                            callback();
                    }
                },
                main: {
                    label: label || "Отмена",
                    className: "btn-primary",
                    callback: function () {
                    }
                }
            }
       });
       return CustomModal.Instanse;
    };

    this.Alert = function (title, text, callback) {
        var message = text || '';
        var dialog = bootbox.dialog({
            title: title,
            message: '<div>' + message + '</div>',
            className: "confirm-modal-custom",
            buttons: {
                success: {
                    label: "Ok",
                    className: "btn-default",
                    callback: function () {
                        if (callback)
                            callback();
                    }
                }                
            }
        });
        return dialog;
    };
}