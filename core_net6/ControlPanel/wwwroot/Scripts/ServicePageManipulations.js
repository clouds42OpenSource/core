﻿/*
    Соглашения на использование этого скрипта: 
    1. всегда должен быть input#FreeCount - hidden поле с количеством свободных доступов
    2. всегда должны быть следующие элементы
        div#connectFreeButton
        div#BuyButton
        div#RemoveButton
    3. управляющие элементы
*/

$(function () {
    $('.access-control').click(function () {
        var countFree = $('input#FreeCount').val();
        var container = $(this).parent().parent();
        elseHide(container);
        if (countFree == 0) {
            $(this).parent().parent().find('div#BuyButton').show().css("display","inline");
            $(this).parent().parent().find('div#BuyButton > button').effect("highlight", { color: "#FF7A00" }, 700);
            $(this).parent().parent().find('input#provideAction').val('enable');
        }
    });
    $('.clicker').submit(function () {
        var img = $('<img>');
        img.attr('src', '/Content/img/ajax-loader.gif');
        $(this).find('div').html(img);
    });
});

function elseHide(container) {
    container.find('div#connectFreeButton').hide();
    container.find('div#BuyButton').hide();
    container.find('div#RemoveButton').hide();
}

function toBegin(container) {
    container.find('div#connectFreeButton').show();
    container.find('div#BuyButton').hide();
    container.find('div#RemoveButton').hide();
}