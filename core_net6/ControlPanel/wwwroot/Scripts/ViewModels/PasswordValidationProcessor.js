﻿// Процессор для валидации пароля
function PasswordValidationProcessor(config) {

    var vm = this;

    // Настройки валидации пароля
    vm.PasswordValidationSettings = {

        // Минимальная длина пароля
        MinPasswordLength: 7,

        // Максимальная длина пароля
        MaxPasswordLength: 25,

        // Регулярное выражение
        RegexPassword: /^[\S]+$/
    }

    // Логин
    vm.Login = ko.observable(config.Login);

    // Пароль
    vm.Password = ko.observable(config.Password);

    // Потор пароля
    vm.ConfirmPassword = ko.observable(config.ConfirmPassword);

    // Текущий пароль
    vm.CurrentPassword = ko.observable(config.CurrentPassword);

    // Необходимость подтвержать пароль
    vm.NeedConfirmPassword = ko.observable(config.NeedConfirmPassword);

    // Сообщения валидации пароля
    vm.PasswordValidationMessages = ko.observableArray([]);

    // Флаг указывающий, что пароль будет генерироваться автоматически
    vm.IsAutomaticallyGeneratePassword = ko.observable(config.IsAutomaticallyGeneratePassword);

    // Признак что нужно вводить текущий пароль
    vm.NeedInputCurrentPassword = ko.observable(config.NeedInputCurrentPassword);

    // Поле пароль имеет фокус
    vm.HasPasswordFocus = ko.observable(false);

    // Текущий пароль корректный
    vm.IsCorrectCurrentPassword = ko.observable(false);

    // Изменить флаг указывающий, что пароль будет генерироваться автоматически
    vm.ChangeIsAutomaticallyGeneratePasswordFlag = function() {
        if (vm.IsAutomaticallyGeneratePassword()) {
            vm.Login(null);
            vm.Password(null);
        }
    }

    // Пароль не является null
    vm.PasswordIsNotNull = function() {
        return !vm.StringIsNull(vm.Password());
    }

    // Возможность сохранить пароль
    vm.CanSavePassword = function () {

        if (vm.IsAutomaticallyGeneratePassword())
            return true;

        if (vm.PasswordValidationMessages().length === 0)
            return false;

        var errorValidationFirst = ko.utils.arrayFirst(vm.PasswordValidationMessages(),
            function (element) {
                return !element.IsSuccess();
            });

        return errorValidationFirst === null;
    }
    
    // Выполнить валидацию пароля
    vm.ValidatePassword = function () {

        if (!vm.StringIsNull(vm.Login())) {
            $("#Login-error").text('');
        }
        if (!vm.StringIsNull(vm.Password())) {
            $("#Password-error").text('');
        }
        vm.PasswordValidationMessages.removeAll();

        vm.PasswordValidationMessages.push({
            Message: ko.observable("от " + vm.PasswordValidationSettings.MinPasswordLength
                + " до " + vm.PasswordValidationSettings.MaxPasswordLength + " символов"),
            IsSuccess: ko.observable(vm.IsValidLength(vm.Password()))
        });

        vm.PasswordValidationMessages.push({
            Message: ko.observable("большие латинские буквы"),
            IsSuccess: ko.observable(vm.HasUpperCase(vm.Password()))
        });

        vm.PasswordValidationMessages.push({
            Message: ko.observable("маленькие латинские буквы"),
            IsSuccess: ko.observable(vm.HasLowerCase(vm.Password()))
        });

        var hasDigitOrSpecialSymbols = vm.HasDigit(vm.Password()) || vm.HasSpecialSymbols(vm.Password());

        vm.PasswordValidationMessages.push({
            Message: ko.observable("цифры или спец символы, без пробелов"),
            IsSuccess: ko.observable(hasDigitOrSpecialSymbols && vm.IsValidMask(vm.Password()))
        });
        
        vm.PasswordValidationMessages.push({
            Message: ko.observable("не равен логину"),
            IsSuccess: ko.observable(vm.IsPasswordNotEqualLogin(vm.Login(), vm.Password()))
        });

        var findError = ko.utils.arrayFirst(vm.PasswordValidationMessages(),
            function(element) {
                return element.IsSuccess() === false;
            });

        if (findError === null || findError === undefined) {
            $("[data-valmsg-for='Password']").text('');
            $(".all-message").text('');
        }

        if (vm.NeedConfirmPassword()) {

            var isValidConfirmPassword = vm.ComparePasswords(vm.Password(), vm.ConfirmPassword());

            vm.PasswordValidationMessages.push({
                Message: ko.observable("пароль и его подтверждение совпадают"),
                IsSuccess: ko.observable(isValidConfirmPassword)
            });

            if (isValidConfirmPassword)
                $("[data-valmsg-for='ConfirmPassword']").text('');
        }

        if (vm.NeedInputCurrentPassword()) {

            if (vm.IsCorrectCurrentPassword())
                $("[data-valmsg-for='CurrentPassword']").text('');

            vm.PasswordValidationMessages.push({
                Message: ko.observable("корректно указан текущий пароль"),
                IsSuccess: ko.observable(vm.IsCorrectCurrentPassword())
            });
        }
    }
    
    // Проверить текущий пароль
    vm.CheckCurrentPassword = function() {

        if (!vm.NeedInputCurrentPassword() || vm.StringIsNull(config.UserId))
            return;

        MaskHelper.ShowLoader($('.modal-footer'));

        if (vm.StringIsNull(vm.CurrentPassword())) {

            vm.ProcessCheckResultCurrentPassword(false);
            MaskHelper.HideLoader($('.modal-footer'));
            return;
        }

        $.get(config.CheckCurrentPasswordUrl,
            {
                userId: config.UserId,
                currentPassword: vm.CurrentPassword()
            }).done(function (result) {
                
                MaskHelper.HideLoader($('.modal-footer'));

                if (!result.success) {
                    vm.ProcessCheckResultCurrentPassword(false);
                    Attention.Show("Ошибка", result.data.ErrorMessage);
                    return;
                }

                vm.ProcessCheckResultCurrentPassword(result.data);
        });
    }

    // Обработать результат проверки текущего пароля
    vm.ProcessCheckResultCurrentPassword = function(result) {
        vm.IsCorrectCurrentPassword(result);
        vm.ValidatePassword();
    }

    // Сравнить пароли
    vm.ComparePasswords = function(password, confirmPassword) {
        return password === confirmPassword;
    }
    
    // Пароль содержит спец. символы
    vm.HasSpecialSymbols = function (password) {

        if (vm.StringIsNull(password))
            return false;

        var characters = password.split('');
        var hasSpecialSymbol = false;

        characters.forEach(function(element) {
            if (element.toUpperCase() === element.toLowerCase() &&
                element.charCodeAt(0) < 127 &&
                !vm.HasDigit(element))
                hasSpecialSymbol = true;
        });

        return hasSpecialSymbol;
    }

    // Пароль не равен логину
    vm.IsPasswordNotEqualLogin = function (login, password) {

        if (vm.StringIsNull(login) || vm.StringIsNull(password))
            return true;

        if (login.toUpperCase() !== password.toUpperCase())
            return true;

        return false;
    }

    // Длина пароля валидна
    vm.IsValidLength = function (password) {

        if (!vm.StringIsNull(password) &&
            password.length <= vm.PasswordValidationSettings.MaxPasswordLength &&
            password.length >= vm.PasswordValidationSettings.MinPasswordLength)
            return true;

        return false;
    }

    // Маска пароля валидна
    vm.IsValidMask = function (password) {

        if (!vm.StringIsNull(password) && vm.PasswordValidationSettings.RegexPassword.test(password))
            return true;

        return false;
    }

    // Пароль содержит строчные буквы
    vm.HasLowerCase = function (password) {
        return !vm.StringIsNull(password) && (/[a-z]/.test(password));
    }

    // Пароль содержит заглавные буквы
    vm.HasUpperCase = function (password) {
        return !vm.StringIsNull(password) && (/[A-Z]/.test(password));
    }

    // Пароль содержит цифры
    vm.HasDigit = function (password) {
        return !vm.StringIsNull(password) && (/[0-9]/.test(password));
    }

    // Строка является пустой
    vm.StringIsNull = function (string) {

        if (string === undefined || string === null || string.length === 0)
            return true;

        return false;
    }
    
    function constructor() {
        vm.ValidatePassword();

        if (vm.NeedInputCurrentPassword() && !vm.StringIsNull(vm.CurrentPassword()))
            vm.CheckCurrentPassword();
    }

    constructor();
}