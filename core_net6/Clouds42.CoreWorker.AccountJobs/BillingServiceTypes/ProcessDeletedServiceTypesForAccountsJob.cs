﻿using System;
using Clouds42.Accounts.Contracts.ManageAccountServiceTypes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountJobs.BillingServiceTypes
{
    /// <summary>
    /// Задача по обработке удаленных услуг, для аккаунтов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ProcessDeletedServiceTypesForAccountsJob)]
    public class ProcessDeletedServiceTypesForAccountsJob(
        IUnitOfWork dbLayer,
        IProcessDeletedServiceTypesForAccountsProvider processDeletedServiceTypesProvider)
        : CoreWorkerParamsJob<ProcessDeletedServiceTypesForAccountsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(ProcessDeletedServiceTypesForAccountsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            processDeletedServiceTypesProvider.Process(jobParams);
        }
    }
}
