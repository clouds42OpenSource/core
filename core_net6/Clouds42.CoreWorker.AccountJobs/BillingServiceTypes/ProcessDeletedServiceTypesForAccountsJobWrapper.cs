﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.BillingService;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountJobs.BillingServiceTypes
{
    /// <summary>
    /// Обработчик задачи по обработке удаленных услуг, для аккаунтов
    /// </summary>
    public class ProcessDeletedServiceTypesForAccountsJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<ProcessDeletedServiceTypesForAccountsJob,
            ProcessDeletedServiceTypesForAccountsDto>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override
            TypingParamsJobWrapperBase<ProcessDeletedServiceTypesForAccountsJob, ProcessDeletedServiceTypesForAccountsDto> Start(
                ProcessDeletedServiceTypesForAccountsDto paramsJob)
        {
            StartTask(paramsJob,
                $"Обработка удаленных услуг сервиса {paramsJob.ServiceId} для аккаунтов. Id изменений сервиса = '{paramsJob.BillingServiceChangesId}'");
            return this;
        }
    }
}
