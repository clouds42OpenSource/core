﻿using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExternalProviders
{
    public class CoreWorkerAccessProvider(
        IAuthTokenProvider authTokenProvider,
        IConfiguration configuration,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        readonly IUnitOfWork _dbLayer = dbLayer;
        protected override string GetUserIdentity() => authTokenProvider.GetAuthToken();
        public override IUserPrincipalDto GetUser()
        {
            var token = Guid.Parse(GetUserIdentity());
            var accountUser = _dbLayer.AccountUsersRepository.FirstOrDefault(au => au.AuthToken == token);
            return new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                Groups = [AccountUserGroup.Cloud42Service],
                Token = token
            };
        }

        public override Task<IUserPrincipalDto> GetUserAsync()
        {
            throw new NotImplementedException();
        }

        public override string GetUserAgent()
        {
            return null;
        }

        public override string GetUserHostAddress()
        {
            return null;
        }

        public override string Name
        {
            get { return "Core worker"; }
        }
    }
}
