﻿using Clouds42.RuntimeContext;

namespace Clouds42.CoreWorker.ExternalProviders
{
    public class WorkerCoreExecutionContext(short? workerId) : ICoreWorkerExecutionContext
    {
        public short? WorkerId { get; } = workerId;
    }
}
