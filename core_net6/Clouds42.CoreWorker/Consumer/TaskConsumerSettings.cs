﻿namespace Clouds42.CoreWorker.Consumer
{
    public class TaskConsumerSettings
    {
        public TimeSpan? PullFromQueueTimeout { get; set; }
    }
}
