﻿using Clouds42.DataContracts.CoreWorker;

namespace Clouds42.CoreWorker.Consumer.Abstractions
{
    public interface ICaptureTaskProvider
    {
        Task<Guid?> CaptureNewTask(short coreWorkerId, List<Guid> internalTaskQueue);
        Task UpdateTaskStatus(Guid taskId, JobExecutionResultDto executionResult, string comment);
    }
}
