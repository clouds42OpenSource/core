﻿using Clouds42.CoreWorker.Consumer.Abstractions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.CoreWorker.Consumer
{
    /// <summary>
    /// Хэлпер для захвата задач
    /// </summary>
    public class CaptureTaskProvider(
        ILogger42 logger,
        IHandlerException handlerException,
        ICoreWorkerTasksQueueProvider coreWorkerTasksQueueProvider)
        : ICaptureTaskProvider
    {
        /// <summary>
        /// Захватить новую задачу
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <param name="internalTaskQueue">Внутренняя очередь задач воркера</param>
        /// <returns>ID захваченной задачи</returns>
        public async Task<Guid?> CaptureNewTask(short coreWorkerId, List<Guid> internalTaskQueue)
        {
            try
            {
                return await coreWorkerTasksQueueProvider.CaptureTaskFromQueueAsync(new CaptureTaskRequestDto
                {
                    CoreWorkerId = coreWorkerId,
                    InternalTasksQueue = new InternalTasksQueueDto { List = internalTaskQueue }
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"[Ошибка захвата новой задачи]");

                return null;
            }
        }

        public async Task UpdateTaskStatus(Guid taskId, JobExecutionResultDto executionResult, string comment)
        {
            try
            {
                await coreWorkerTasksQueueProvider.UpdateTaskStatusAsync(
                    new UpdateTaskStatusRequestDto
                    {
                        Comment = comment, ExecutionResult = executionResult, TaskId = taskId
                    });
            }

            catch (Exception e)
            {
                handlerException.Handle(e, "[Ошибка обновления статуса задачи после ее выполнения]");
            }
        }
    }
}
