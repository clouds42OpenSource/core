﻿using System.Collections.Concurrent;
using Clouds42.CoreWorker.Consumer.Abstractions;
using Clouds42.CoreWorker.Processing.Abstractions;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Microsoft.Extensions.Hosting;

namespace Clouds42.CoreWorker.Consumer
{
    /// <summary>
    /// Поток обработки задач
    /// </summary>
    public class TaskConsumer(
        ITaskProcessor taskProcessor,
        ILogger42 logger,
        TaskConsumerSettings settings,
        IHandlerException handlerException,
        ICaptureTaskProvider captureTaskProvider,
        short workerId,
        string name,
        BlockingCollection<Guid> taskCollection) : BackgroundService
    {
        const int DefaultPullFromQueueTimeoutMs = 200;

        private readonly TimeSpan _pullFromQueueTimeout = settings?.PullFromQueueTimeout != null &&
                                                         settings.PullFromQueueTimeout > TimeSpan.Zero ? settings.PullFromQueueTimeout.Value : TimeSpan.FromMilliseconds(DefaultPullFromQueueTimeoutMs);

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.Info($"Worker {workerId} {name} started");
            while (!stoppingToken.IsCancellationRequested || !taskCollection.IsCompleted)
            {
                var taskId = Guid.Empty;
                var comment = string.Empty;
                var executionResult = new JobExecutionResultDto(CloudTaskQueueStatus.New);
                try
                {
                    if (!taskCollection.TryTake(out taskId, _pullFromQueueTimeout))
                        continue;

                    comment = string.Empty;
                    logger.Debug($"TaskConsumer {name} got a task: {taskId}");
                    executionResult = taskProcessor.ProcessTask(taskId, workerId);
                    logger.Debug($"TaskConsumer {name} done processing task: {taskId}");
                }
                catch (Exception ex)
                {
                    comment = ex.Message;
                    executionResult.TaskQueueStatus = CloudTaskQueueStatus.Error;
                    handlerException.Handle(ex, $"[TaskConsumer got an error processing task]: name : {name} id : {taskId}, error msg: {comment}");
                }

                finally
                {
                    if (taskId != Guid.Empty)
                        await captureTaskProvider.UpdateTaskStatus(taskId, executionResult, comment);
                }
            }
            logger.Debug($"TaskConsumer {name} stopped");
        }
    }
}
