﻿using System.Collections.Concurrent;
using System.Configuration;
using Clouds42.CoreWorker.Consumer;
using Clouds42.CoreWorker.Consumer.Abstractions;
using Clouds42.CoreWorker.Processing.Abstractions;
using Clouds42.CoreWorker.Producer;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Clouds42.CoreWorker
{
    public class CoreWorkerService(
        short workerId,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IServiceProvider serviceProvider,
        IHandlerException handlerException,
        CoreWorkerServiceOptions options)
        : IHostedService
    {
        private readonly List<TaskConsumer> _consumers = [];
        private readonly BlockingCollection<Guid>  _tasks = new ();
        private short _threadsCount;
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await RegisterServiceStart();

            var workerInfo = unitOfWork
                .CoreWorkerRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(w => w.CoreWorkerId == workerId) ?? throw new ConfigurationErrorsException($"Worker '{workerId}' info is not found");

            _threadsCount = workerInfo.MaxThreadsCount is null or 0 ? (short)Environment.ProcessorCount : workerInfo.MaxThreadsCount.Value;

            var producer = new TaskProducer(
                    serviceProvider.GetRequiredService<ICaptureTaskProvider>(),
                    serviceProvider.GetRequiredService<ILogger42>(),
                    new TaskProducerSettings
                    {
                        NoTaskDelay = options.NoTaskDelay,
                        PushToInternalQueueTimeout = options.PushToInternalQueueTimeout,
                        TaskPollingRate = options.ProducerPollingRate
                    },
                    serviceProvider.GetRequiredService<IHandlerException>(), options.WorkerId, _tasks);


            Task.Run(() => producer.StartAsync(cancellationToken));


            for (var i = 0; i < _threadsCount; i++)
            {
                var consumer = new TaskConsumer(
                    serviceProvider.GetRequiredService<ITaskProcessor>(),
                    serviceProvider.GetRequiredService<ILogger42>(),
                    new TaskConsumerSettings { PullFromQueueTimeout = options.PushToInternalQueueTimeout },
                    serviceProvider.GetRequiredService<IHandlerException>(),
                    serviceProvider.GetRequiredService<ICaptureTaskProvider>(), workerId, $"TaskConsumer-{i}", _tasks);

                Task.Run(() => consumer.StartAsync(cancellationToken));

                _consumers.Add(consumer);
            }

            Task.Run(async () =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await UpdateMonitoringData();
                    }
                    catch (Exception ex)
                    {
                        handlerException.Handle(ex, "[Error while monitoring threads]");
                    }
                    finally
                    {
                        await Task.Delay(500, cancellationToken);
                    }
                }
            });
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Task.WhenAll(_consumers.Select(x => x.StopAsync(cancellationToken)));
            await RegisterServiceStop();
        }

        private async Task UpdateMonitoringData()
        {
            await UpdateServiceRecord();
        }

        private async Task RegisterServiceStart()
        {
            logger.Info("Registering service #{0} start in db", workerId);
            try
            {
                await UpdateServiceRecord();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Error registering service start in db]");
            }
        }

        private async Task RegisterServiceStop()
        {
            logger.Info("Registering service #{0} start in db", workerId);
            try
            {
                await UpdateServiceRecord(CoreWorkerStatus.OFF);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Error registering service start in db]");
            }
        }

        private async Task UpdateServiceRecord(CoreWorkerStatus status = CoreWorkerStatus.ON)
        {
            await using var scope = serviceProvider.CreateAsyncScope();
            var dbLayer = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

            var updateModel = await dbLayer.CoreWorkerRepository
                .AsQueryable()
                .FirstOrDefaultAsync(cw => cw.CoreWorkerId == workerId) ?? new Domain.DataModels.CoreWorker();

            updateModel.MaxThreadsCount = _threadsCount;
            updateModel.Tick = DateTime.Now;
            updateModel.Status = status;

            if (updateModel.CoreWorkerId == 0)
            {
                updateModel.CoreWorkerId = workerId;
                dbLayer.CoreWorkerRepository.Insert(updateModel);
            }
            else
                dbLayer.CoreWorkerRepository.Update(updateModel);

            await dbLayer.SaveAsync();
        }
    }
}
