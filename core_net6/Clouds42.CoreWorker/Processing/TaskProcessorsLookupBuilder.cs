﻿using Clouds42.CoreWorker.BaseJobs;
using System.Reflection;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.Processing
{
    public class TaskProcessorsLookupBuilder
    {
        readonly Dictionary<string, Type> _map = new();
        public TaskProcessorsLookupBuilder RegisterWithName<TJob>(string taskName)
        {
            _map[taskName] = typeof(TJob);
            return this;
        }
        public TaskProcessorsLookupBuilder RegisterFromAttribute<TJob>() where TJob : CoreWorkerJob
        {
            var type = typeof(TJob);
            var taskName = type.GetCoreWorkerTaskName();
            if (taskName == null)
                throw new InvalidOperationException($"Core Worker Job {typeof(TJob).FullName} has no CoreWorkerTaskAttribute");
            return RegisterWithName<TJob>(taskName);
        }
        public TaskProcessorsLookupBuilder RegisterAssemblyJobs(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes()
                .Where(myType => myType is { IsClass: true, IsAbstract: false } && myType.IsSubclassOf(typeof(CoreWorkerJob))))
            {
                var taskName = type.GetCoreWorkerTaskName();
                if (taskName != null)
                    _map.Add(taskName, type);
            }
            return this;
        }
        public TaskProcessorsLookup Build()
        {
            return new TaskProcessorsLookup(_map);
        }
    }
}