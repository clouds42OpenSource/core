﻿namespace Clouds42.CoreWorker.Processing
{
    public class TaskProcessorsLookup
    {
        readonly Dictionary<string, Type> _map;
        public TaskProcessorsLookup(Dictionary<string, Type> map)
        {
            _map = map.ToDictionary(x => x.Key, x => x.Value);
        }
        public bool TryGetTaskProcessor(string taskName, out Type? jobType)
        {
            return _map.TryGetValue(taskName, out jobType);
        }
    }
}