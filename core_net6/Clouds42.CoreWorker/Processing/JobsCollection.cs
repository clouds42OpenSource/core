﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.Processing.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.Processing
{
    public class JobsCollection : IJobsCollection
    {
        readonly IServiceCollection _serviceCollection;
        readonly TaskProcessorsLookupBuilder _mapBuilder;
        public JobsCollection(IServiceCollection serviceCollection)
        {
            _serviceCollection = serviceCollection;
            _mapBuilder = new TaskProcessorsLookupBuilder();
            _serviceCollection.AddSingleton(_ => _mapBuilder.Build());
        }

        public IJobsCollection RegisterJob<T>() where T : CoreWorkerJob
        {
            _mapBuilder.RegisterFromAttribute<T>();
            _serviceCollection.AddTransient<T>();

            return this;
        }

        public IJobsCollection RegisterJob<T>(string taskName) where T : CoreWorkerJob
        {
            _mapBuilder.RegisterWithName<T>(taskName);
            _serviceCollection.AddTransient<T>();

            return this;
        }
    }
}
