﻿using Clouds42.CoreWorker.BaseJobs;

namespace Clouds42.CoreWorker.Processing.Abstractions
{
    public interface IJobScope : IDisposable
    {
        CoreWorkerJob? Processor { get; }
    }
}