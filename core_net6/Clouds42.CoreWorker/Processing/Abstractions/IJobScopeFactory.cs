﻿namespace Clouds42.CoreWorker.Processing.Abstractions
{
    public interface IJobsProvider
    {
        IJobScope GetJob(string taskName, short workerId);
    }
}