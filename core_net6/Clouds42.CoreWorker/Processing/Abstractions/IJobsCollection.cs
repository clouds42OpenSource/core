﻿using Clouds42.CoreWorker.BaseJobs;

namespace Clouds42.CoreWorker.Processing.Abstractions
{
    public interface IJobsCollection
    {
        IJobsCollection RegisterJob<T>() where T : CoreWorkerJob;
        IJobsCollection RegisterJob<T>(string taskName) where T : CoreWorkerJob;
    }
}