﻿using Clouds42.DataContracts.CoreWorker;

namespace Clouds42.CoreWorker.Processing.Abstractions
{
    /// <summary>
    /// Обработчик задач воркера
    /// </summary>
    public interface ITaskProcessor
    {
        /// <summary>
        /// Обработать задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="workerId"></param>
        /// <returns>Результат выполнения</returns>
        JobExecutionResultDto ProcessTask(Guid taskId, short workerId);
    }
}
