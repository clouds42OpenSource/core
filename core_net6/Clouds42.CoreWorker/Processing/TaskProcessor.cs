﻿using Clouds42.Common.Exceptions;
using Clouds42.CoreWorker.Processing.Abstractions;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.Processing
{
    /// <summary>
    /// Обработчик задач воркера
    /// </summary>
    public class TaskProcessor(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IJobsProvider jobScopeFactory)
        : ITaskProcessor
    {
        /// <summary>
        /// Обработать задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="workerId"></param>
        /// <returns>Результат выполнения</returns>
        public JobExecutionResultDto ProcessTask(Guid taskId, short workerId)
        {
            var queuedTask = unitOfWork.CoreWorkerTasksQueueRepository.GetById(taskId);
            queuedTask.Status = CloudTaskQueueStatus.Processing.ToString();
            queuedTask.StartDate = DateTime.Now;

            unitOfWork.CoreWorkerTasksQueueRepository.Update(queuedTask);

            UpdateQueuedTaskLiveFrom(queuedTask, unitOfWork);

            unitOfWork.Save();

            var coreWorkerTask = unitOfWork.CoreWorkerTaskRepository.GetById(queuedTask.CoreWorkerTaskId) ?? throw new TaskNotFoundException($"Ошибка обработчика задач воркера. Task {taskId} not found!");
            using var jobScope = jobScopeFactory.GetJob(coreWorkerTask.TaskName, workerId);
            if (jobScope?.Processor == null)
                throw new NotImplementedException($"Job implementation for task {taskId} not found");

            logger.Debug("Job processor for task {0} resolved to {1}",
                taskId, jobScope.Processor.GetType());
            jobScope.Processor.Execute(coreWorkerTask.ID, queuedTask.Id);

            return jobScope.Processor.NeedRetry
                ? new JobExecutionResultDto(
                    CloudTaskQueueStatus.NeedRetry,
                    jobScope.Processor.RetryDelayInSeconds)
                : new JobExecutionResultDto(CloudTaskQueueStatus.Ready);
        }

        /// <summary>
        /// Обновить поля для активной задачи в очереди
        /// </summary>
        /// <param name="queuedTask">Задача в очереди</param>
        /// <param name="uow">Контекст базы</param>
        private static void UpdateQueuedTaskLiveFrom(CoreWorkerTasksQueue queuedTask, IUnitOfWork uow)
        {
            var queuedTaskLive = uow.CoreWorkerTasksQueueLiveRepository.GetById(queuedTask.Id);
            queuedTaskLive.Status = queuedTask.QueueStatus;
            queuedTaskLive.StartDate = queuedTask.StartDate;

            uow.CoreWorkerTasksQueueLiveRepository.Update(queuedTaskLive);
        }
    }
}
