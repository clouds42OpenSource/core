﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.Processing.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.Processing
{

    /// <summary>
    /// Factory for jobs scope
    /// </summary>
    public sealed class JobsProvider(IServiceProvider serviceProvider, TaskProcessorsLookup coreWorkerJobsMap)
        : IJobsProvider
    {
        /// <summary>
        /// Создать экземпляр
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="taskName">Имя с типом пространства имен</param>
        /// <param name="workerId">Идентификатор воркера</param>
        /// <returns></returns>
        public IJobScope GetJob(string taskName, short workerId)
        {
            if (!coreWorkerJobsMap.TryGetTaskProcessor(taskName, out var processorType) || processorType == null)
                throw new InvalidOperationException($"Unkown job type {taskName}");
            var scope = serviceProvider.CreateScope();
            return new JobScope(processorType, scope);
        }
    }

    public class JobScope : IJobScope
    {
        readonly IServiceScope _scope;
        readonly Type _type;
        readonly Lazy<CoreWorkerJob?> _jobInstanceLazy;
        public JobScope(Type type, IServiceScope scope)
        {
            _scope = scope;
            _type = type;
            var workerJobType = typeof(CoreWorkerJob);

            if (!_type.IsSubclassOf(workerJobType))
                throw new ArgumentOutOfRangeException(
                    nameof(type),
                    $"Type {type.FullName} is not sublclass of {workerJobType.FullName}"
                );

            _jobInstanceLazy = new Lazy<CoreWorkerJob?>(() => _scope.ServiceProvider.GetRequiredService(_type) as CoreWorkerJob);
        }
        public CoreWorkerJob? Processor => _jobInstanceLazy.Value;

        public void Dispose()
        {
            _scope?.Dispose();
        }
    }
}
