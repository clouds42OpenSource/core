﻿namespace Clouds42.CoreWorker
{
    public interface IAuthTokenProvider
    {
        string GetAuthToken();
    }
}