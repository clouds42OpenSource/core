﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CoreWorker.AccountDatabase.Jobs.BackupFileFetching;
using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromDt;
using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromTemplate;
using Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteAccountDatabase;
using Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManagePublishAcDbRedirect;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs;
using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase;
using Clouds42.CoreWorker.AccountDatabasesOnDelimiters;
using Clouds42.CoreWorker.AccountJobs.BillingServiceTypes;
using Clouds42.CoreWorker.AccountUserJobs.ManageLockState;
using Clouds42.CoreWorker.AccountUserJobs.RemoveObsoleteUserJsonWebTokens;
using Clouds42.CoreWorker.AcDbAccessesJobs.Jobs;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewAccount;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser;
using Clouds42.CoreWorker.BillingJobs.AutoPayJob;
using Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments;
using Clouds42.CoreWorker.BillingJobs.MainServiceResourcesChanges;
using Clouds42.CoreWorker.BillingJobs.MyDiskSizeRecalculate;
using Clouds42.CoreWorker.BillingServiceType.RecalculateServiceTypeCostAfterChange;
using Clouds42.CoreWorker.CommonJobs.FileSystemJobs;
using Clouds42.CoreWorker.CommonJobs.UploadedFileJobs;
using Clouds42.CoreWorker.Consumer;
using Clouds42.CoreWorker.Consumer.Abstractions;
using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountUsersDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportInvoicesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportPaymentsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportResourcesDataToBigQuery;
using Clouds42.CoreWorker.ExternalProviders;
using Clouds42.CoreWorker.Jobs;
using Clouds42.CoreWorker.Jobs._1CJobs;
using Clouds42.CoreWorker.Jobs.AccountDatabase;
using Clouds42.CoreWorker.Jobs.Archivation;
using Clouds42.CoreWorker.Jobs.Audit;
using Clouds42.CoreWorker.Jobs.Cluster1CJobs;
using Clouds42.CoreWorker.Processing;
using Clouds42.CoreWorker.Processing.Abstractions;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfArchiveDatabaseToTomb;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfRemoveDatabase;
using Clouds42.GoogleCloud.SpreadSheet.Jobs;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.RuntimeContext;
using Microsoft.Extensions.DependencyInjection;


namespace Clouds42.CoreWorker
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddWorkerHostedService(this IServiceCollection serviceCollection, IUnitOfWork unitOfWork, CoreWorkerServiceOptions options, ILogger42 logger)
        {
            return serviceCollection
                .AddTransient<ITaskProcessor, TaskProcessor>()
                .AddTransient<IJobsProvider, JobsProvider>()
                .AddTransient<ICaptureTaskProvider, CaptureTaskProvider>()
                .AddTransient<IAccessProvider, CoreWorkerAccessProvider>()
                .AddTransient<ICoreExecutionContext>(sp => new WorkerCoreExecutionContext(sp.GetService<CoreWorkerServiceOptions>()?.WorkerId))
                .ConfigureJobsCollection(jobsCollection =>
                {
                    jobsCollection.RegisterClouds42Jobs();
                })
                .AddHostedService(sp => new CoreWorkerService(
                    options.WorkerId,
                    sp.GetRequiredService<ILogger42>(), 
                    sp.GetRequiredService<IUnitOfWork>(),
                    sp,
                    sp.GetRequiredService<IHandlerException>(),
                    options));
        }

        public static IJobsCollection RegisterClouds42Jobs(this IJobsCollection jobsCollection)
        {
            jobsCollection.RegisterJob<TransformFileBaseToServerJob>()
                .RegisterJob<SetFeedbackNotificationSendJob>()
                //AcDbAccessJobs
                .RegisterJob<RemoveAccessesFromDatabaseJob>()
                //AccountDatabaseJobs
                .RegisterJob<BackupFileFetchingJob>()
                .RegisterJob<CreateDatabaseFromDtJob>()
                .RegisterJob<CreateDatabaseFromTemplateJob>()
                .RegisterJob<DeleteAccountDatabaseJob>()
                .RegisterJob<DeleteInactiveAccountsDataJob>()
                .RegisterJob<ManagePublishedAccountDatabaseRedirectJob>()
                .RegisterJob<SetServiceExtensionDatabaseActivationStatusJob>()
                .RegisterJob<CancelPublishDatabaseJob>()
                .RegisterJob<PublishDatabaseJob>()
                .RegisterJob<RemoveOldPublicationJob>()
                .RegisterJob<RepublishDatabaseJob>()
                .RegisterJob<RepublishWithChangingConfigJob>()
                .RegisterJob<RestartAccountApplicationPoolOnIisJob>()
                .RegisterJob<TerminateSessionsInDatabaseJob>()
                //AccountDatabasesOnDelimiters
                .RegisterJob<UploadAcDbZipFileThroughWebServiceJob>()
                //AccountJobs
                .RegisterJob<ProcessDeletedServiceTypesForAccountsJob>()
                //AccountUserJobs
                .RegisterJob<ManageAccountUserLockStateJob>()
                .RegisterJob<RemoveObsoleteAccountUserJsonWebTokensJob>()
                //ActiveDirectoryJobs
                .RegisterJob<ChangeUserLoginInAdJob>()
                .RegisterJob<ChangeUserPasswordInAdJob>()
                .RegisterJob<DeleteUserInAdJob>()
                .RegisterJob<EditUserInAdJob>()
                .RegisterJob<EditUserGroupsInAdListInAdJob>()
                .RegisterJob<RegisterNewAccountInAdJob>()
                .RegisterJob<RegisterNewUserInAdJob>()
                .RegisterJob<RemoveDirectoryAclForUserInJob>()
                .RegisterJob<SetDirectoryAclInAdJob>()
                .RegisterJob<SetDirectoryAclForUserInAdJob>()
                //BillingJobs
                .RegisterJob<FinalizeYookassaPaymentJob>()
                .RegisterJob<AutoPayJob>()
                .RegisterJob<FinalizeYookassaPaymentsJob>()
                .RegisterJob<NotifyAboutMainServiceResourcesChangesJob>()
                .RegisterJob<MyDiskUsedSizeRecalculationJob>()
                //BillingserviceTypeJobs
                .RegisterJob<RecalculateServiceTypeCostAfterChangeJob>()
                //CommonJobs
                .RegisterJob<CreateDirectoryJob>()
                .RegisterJob<RemoveDirectoryJob>()
                .RegisterJob<RemoveFileJob>()
                .RegisterJob<MergeChunksToInitialFileJob>()
                .RegisterJob<RemoveUploadFileJob>()
                //ExportDataJobs
                .RegisterJob<ExportAccountsDataToBigQueryJob>()
                .RegisterJob<ExportAccountUsersDataToBigQueryJob>()
                .RegisterJob<ExportDatabasesDataToBigQueryJob>()
                .RegisterJob<ExportDataToBigQueryJob>()
                .RegisterJob<ExportInvoicesDataToBigQueryJob>()
                .RegisterJob<ExportPaymentsDataToBigQueryJob>()
                .RegisterJob<ExportResourcesDataToBigQueryJob>()
                //OtherJobs
                .RegisterJob<McobEditUserRolesOfDatabase>()
                .RegisterJob<AccountDatabaseAutoUpdateJob>()
                .RegisterJob<AccountDatabaseMigrationJob>()
                .RegisterJob<AddWebAccessToDatabaseJob>()
                .RegisterJob<Check1CDatabaseFileForExistenceJob>()
                .RegisterJob<RemoveWebAccessToDatabaseJob>()
                .RegisterJob<RepublishSegmentDatabasesJob>()
                .RegisterJob<UpdateLoginForWebAccessDatabaseJob>()
                .RegisterJob<ArchivateAccountDatabaseToTombJob>()
                .RegisterJob<ArchivateAccountFilesToTombJob>()
                .RegisterJob<ArchivDatabaseJob>()
                .RegisterJob<AccountDatabasesAuditAndChangeStateJob>()
                .RegisterJob<AccountServiceDependenciesAuditJob>()
                .RegisterJob<AuditCoreWorkerTasksQueueJob>()
                .RegisterJob<CfuUpdaterJob>()
                .RegisterJob<Regular42CloudServiceProlongationJob>()
                .RegisterJob<CreateDatabaseClusterJob>()
                .RegisterJob<DropDatabaseFromClusterJob>()
                .RegisterJob<DropSessionsFromClusterJob>()
                .RegisterJob<CorpPingJob>()
                .RegisterJob<CreateInvoiceFiscalReceiptJob>()
                .RegisterJob<DatabaseRegularOperationsJob>()
                .RegisterJob<DeleteAccountDatabaseToTombJob>()
                .RegisterJob<DeleteWindowsProfileJob>()
                .RegisterJob<DocLoaderByRent1CProlongationJob>()
                .RegisterJob<InfoBaseLastActiveUpdateJob>()
                .RegisterJob<ManageAccountDatabaseRestoreModelJob>()
                .RegisterJob<MyDiskRegularOperationsJob>()
                .RegisterJob<NotifyBeforeChangeServiceCostJob>()
                .RegisterJob<PlanSupportAccountDatabaseJob>()
                .RegisterJob<PublishingPathsDatabasesAuditJob>()
                .RegisterJob<RecalculateServiceCostAfterChangesJob>()
                .RegisterJob<ReportJob>()
                .RegisterJob<RestoreAccountDatabaseAfterFailedAutoUpdateJob>()
                .RegisterJob<RestoreAccountDatabaseFromTombJob>()
                .RegisterJob<SegmentMigrationJob>()
                .RegisterJob<SendBackupAccountDatabaseToTombJob>()
                .RegisterJob<SupportAuthVerification>()
                .RegisterJob<TehSupportAccountDatabasesJob>()
                .RegisterJob<TemplatesUpdaterJob>()
                .RegisterJob<UpdateAccountDatabasesJob>()
                //StartProcessToWorkDatabaseJobs
                .RegisterJob<StartProcessOfArchiveDatabaseToTombJob>()
                .RegisterJob<StartProcessOfRemoveDatabaseJob>()
                .RegisterJob<AccountDatabaseUpdateJob>()
                .RegisterJob<HandleRequestToChangeAccDbTypeJob>()
                // GoogleSheetJobs
                .RegisterJob<UpdatePartnerClientsGoogleSheetJob>();
            

            return jobsCollection;
        }

        public static IServiceCollection ConfigureJobsCollection(this IServiceCollection serviceCollection,
            Action<IJobsCollection> configure)
        {
            var builder = new JobsCollection(serviceCollection);
            configure.Invoke(builder);

            return serviceCollection;
        }
    }
}
