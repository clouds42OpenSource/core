﻿namespace Clouds42.CoreWorker
{
    public sealed class CoreWorkerServiceOptions(
        short workerId)
    {
        public short WorkerId { get; } = workerId;
        public TimeSpan? ProducerPollingRate { get; set; }
        public TimeSpan? NoTaskDelay { get; set; }
        public TimeSpan? PullToInternalQueueTimeout { get; set; }
        public TimeSpan? PushToInternalQueueTimeout { get; set; }

    }
}
