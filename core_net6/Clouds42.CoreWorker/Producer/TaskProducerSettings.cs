﻿namespace Clouds42.CoreWorker.Producer
{
    public class TaskProducerSettings
    {
        public TimeSpan? NoTaskDelay { get; set; }
        public TimeSpan? TaskPollingRate { get; set; }
        public TimeSpan? PushToInternalQueueTimeout { get; set; }
    }
}

