﻿using System.Collections.Concurrent;
using Clouds42.CoreWorker.Consumer.Abstractions;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Microsoft.Extensions.Hosting;

namespace Clouds42.CoreWorker.Producer
{
    public class TaskProducer(
        ICaptureTaskProvider captureTaskProvider,
        ILogger42 logger,
        TaskProducerSettings settings,
        IHandlerException handlerException,
        short workerId,
        BlockingCollection<Guid> taskCollection) : BackgroundService
    {
        const int DefaultPollingRateMs = 5000;
        const int DefaultNoTaskDelayMs = 30000;
        const int DefaultPushToInternalQueueTimeoutMs = 1000;


        private readonly TimeSpan _pollingRate = settings?.TaskPollingRate != null &&
                                         settings.TaskPollingRate > TimeSpan.Zero ? settings.TaskPollingRate.Value : TimeSpan.FromMilliseconds(DefaultPollingRateMs);

        private readonly TimeSpan _noTaskDelay = settings?.NoTaskDelay != null &&
                                         settings.NoTaskDelay > TimeSpan.Zero ? settings.NoTaskDelay.Value : TimeSpan.FromMilliseconds(DefaultNoTaskDelayMs);

        private readonly TimeSpan _pushToQueueTimeout = settings?.PushToInternalQueueTimeout != null &&
                                                 settings.PushToInternalQueueTimeout > TimeSpan.Zero ? settings.PushToInternalQueueTimeout.Value : TimeSpan.FromMilliseconds(DefaultPushToInternalQueueTimeoutMs);
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            logger.Info("Capture Task Worker started");
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var newTaskId = await captureTaskProvider.CaptureNewTask(workerId, taskCollection.ToList());
                    if (newTaskId == null)
                    {
                        await Task.Delay(_noTaskDelay, stoppingToken);
                        continue;
                    }

                    logger.Info("Wow! I`ve got a new task! Task id: {0}", newTaskId);


                    while (!taskCollection.TryAdd(newTaskId.Value, _pushToQueueTimeout))
                        logger.Debug("Internal task queue is full, retrying in {0} ms...", _pushToQueueTimeout);

                    logger.Debug("Task {0} added to internal task queue", newTaskId);

                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, "[Provide packages exception]");
                }
                finally
                {
                    await Task.Delay(_pollingRate, stoppingToken);
                }
            }

            logger.Info("Capture Task Worker stopped");
        }
    }
}

