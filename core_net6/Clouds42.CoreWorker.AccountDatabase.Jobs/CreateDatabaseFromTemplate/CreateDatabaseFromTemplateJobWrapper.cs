﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromTemplate
{
    public class CreateDatabaseFromTemplateJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : TypingParamsJobWrapperBase<CreateDatabaseFromTemplateJob, RegisterFileDatabaseFromTemplateModelDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<CreateDatabaseFromTemplateJob, RegisterFileDatabaseFromTemplateModelDto> Start(RegisterFileDatabaseFromTemplateModelDto paramsJob)
        {
            StartTask(paramsJob, $"Регистрация базы '{paramsJob.AccountDatabaseId}' из шаблона");
            return this;
        }
    }
}
