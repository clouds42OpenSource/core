﻿using System;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromTemplate
{
    /// <summary>
    /// Задача по созданию файловой базы из шаблона.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CreateDatabaseFromTemplateJob)]
    public class CreateDatabaseFromTemplateJob(
        IUnitOfWork dbLayer,
        IRegisterDatabaseFromTemplateProvider databaseFromTemplateProvider)
        : CoreWorkerParamsJob<RegisterFileDatabaseFromTemplateModelDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RegisterFileDatabaseFromTemplateModelDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                databaseFromTemplateProvider.RegisterDatabaseOnServer(jobParams);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка регистрации базы из шаблона] id='{jobParams.AccountDatabaseId}' ");
                throw;
            }

        }

    }
}
