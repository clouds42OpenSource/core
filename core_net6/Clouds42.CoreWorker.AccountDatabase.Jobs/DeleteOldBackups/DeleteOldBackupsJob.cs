﻿using System;
using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteOldBackups
{
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteOldBackups)]
    public class DeleteOldBackupsJob(IUnitOfWork dbLayer, IBackupProvider backupProvider)
        : CoreWorkerParamsJob<DeleteOldBackupsDto>(dbLayer)
    {
        protected override void Execute(DeleteOldBackupsDto jobParams, Guid taskId, Guid taskQueueId)
        {
           backupProvider.DeleteOldBackups(jobParams);
        }
    }
}
