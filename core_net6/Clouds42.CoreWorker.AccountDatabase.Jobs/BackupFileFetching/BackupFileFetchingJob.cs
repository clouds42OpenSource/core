﻿using Clouds42.CoreWorker.BaseJobs;
using System;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.BackupFileFetching
{
    /// <inheritdoc />
    /// <summary>
    /// Задача по загрузке бэкапа базы на разделителях
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.BackupFileFetchingJob)]
    public class BackupFileFetchingJob(
        IUnitOfWork dbLayer,
        IBackupFileProvider backupFileProvider)
        : CoreWorkerParamsJob<BackupFileFetchingJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">Id таски</param>
        /// <param name="taskQueueId">Id очереди</param>
        protected override void Execute(BackupFileFetchingJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                backupFileProvider.FetchBackupFile(jobParams);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка при загрузке бэкапа базы на разделителях]");
                throw;
            }
        }
    }
}
