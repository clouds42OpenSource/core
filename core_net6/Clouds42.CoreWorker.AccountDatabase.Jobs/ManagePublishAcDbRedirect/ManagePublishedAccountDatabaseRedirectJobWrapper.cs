﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.ManagePublishAcDbRedirect
{
    /// <summary>
    /// Задача для управления редиректом для опубликованной базы
    /// </summary>
    public class ManagePublishedAccountDatabaseRedirectJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<ManagePublishedAccountDatabaseRedirectJob, ManagePublishedAcDbRedirectParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу по управлению доступом к базе на разделителях.
        /// </summary>
        public override TypingParamsJobWrapperBase<ManagePublishedAccountDatabaseRedirectJob, ManagePublishedAcDbRedirectParamsDto> Start(ManagePublishedAcDbRedirectParamsDto paramsJob)
        {
            StartTask(paramsJob, $"Управление редиректом для инф. базы {paramsJob.AccountDatabaseV82Name} на сервере публикаций.");
            return this;
        }
    }
}
