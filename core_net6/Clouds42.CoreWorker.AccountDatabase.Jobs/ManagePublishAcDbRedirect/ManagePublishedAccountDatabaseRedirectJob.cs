﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.ManagePublishAcDbRedirect
{
    /// <summary>
    /// Задача для управления редиректом для опубликованной базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ManagePublishedAccountDatabaseRedirectJob)]
    public class ManagePublishedAccountDatabaseRedirectJob(
        IUnitOfWork dbLayer,
        IManagePublishedAccountDatabaseRedirectProvider managePublishedAccountDatabaseRedirectProvider)
        : CoreWorkerParamsJob<ManagePublishedAcDbRedirectParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(ManagePublishedAcDbRedirectParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            managePublishedAccountDatabaseRedirectProvider.ManageAcDbRedirect(jobParams);
        }
    }
}
