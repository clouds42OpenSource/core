﻿using System;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача по переопубликации с изменением web.config файла
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RepublishWithChangingConfigJob)]
    public class RepublishWithChangingConfigJob(
        IUnitOfWork unitOfWork,
        IPublishDatabaseProvider publishDatabaseProvider)
        : CoreWorkerParamsJob<PublishOrCancelPublishDatabaseJobParamsDto>(unitOfWork)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        protected override void Execute(PublishOrCancelPublishDatabaseJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            publishDatabaseProvider.RepublishWithChangingWebConfig(jobParams.AccountDatabaseId);
        }
    }
}
