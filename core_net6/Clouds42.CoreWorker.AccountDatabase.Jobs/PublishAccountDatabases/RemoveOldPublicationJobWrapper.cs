﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    /// Обработчик задачи удаления старой публикации инф. базы
    /// </summary>
    public class RemoveOldPublicationJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RemoveOldPublicationJob, RemoveOldPublicationJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик задачи</returns>
        public override TypingParamsJobWrapperBase<RemoveOldPublicationJob, RemoveOldPublicationJobParamsDto> Start(RemoveOldPublicationJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Удаление старой публикации инф. базы.");
            return this;
        }
    }
}
