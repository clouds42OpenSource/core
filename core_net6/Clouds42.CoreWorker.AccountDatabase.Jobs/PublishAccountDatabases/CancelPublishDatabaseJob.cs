using System;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Джоба отмены публикации базы на IIS
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CancelPublishDatabaseJob)]
    public class CancelPublishDatabaseJob(
        IUnitOfWork unitOfWork,
        IPublishDatabaseProvider publishDatabaseProvider)
        : CoreWorkerParamsJob<PublishOrCancelPublishDatabaseJobParamsDto>(unitOfWork)
    {
        /// <summary>
        /// Выполнить задачу по отмене публикации базы
        /// </summary>
        protected override void Execute(PublishOrCancelPublishDatabaseJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            publishDatabaseProvider.CancelPublishDatabase(jobParams.AccountDatabaseId);
        }
    }
}
