﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача по повторной публикации базы
    /// </summary>
    public class RepublishDatabaseJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RepublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить таску публикации базы
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<RepublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto> Start(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Переопубликация базы.");
            return this;
        }
    }
}
