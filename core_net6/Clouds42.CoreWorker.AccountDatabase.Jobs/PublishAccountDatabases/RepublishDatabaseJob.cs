﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using System;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <inheritdoc />
    /// <summary>
    ///     Джоба перепубликации базы на IIS
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RepublishDatabaseJob)]
    public class RepublishDatabaseJob(
        IUnitOfWork unitOfWork,
        IPublishDatabaseProvider publishDatabaseProvider)
        : CoreWorkerParamsJob<PublishOrCancelPublishDatabaseJobParamsDto>(unitOfWork)
    {
        /// <inheritdoc />
        /// <summary>
        /// Выполнить задачу по переопубликации базы
        /// </summary>
        protected override void Execute(PublishOrCancelPublishDatabaseJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            publishDatabaseProvider.RepublishDatabase(jobParams.AccountDatabaseId);
        }
    }
}
