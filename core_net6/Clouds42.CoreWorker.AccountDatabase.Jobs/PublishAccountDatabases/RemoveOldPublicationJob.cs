﻿using System;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    /// Джоба удаления старой публикации инф. базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveOldPublicationJob)]
    public class RemoveOldPublicationJob(
        IUnitOfWork dbLayer,
        IPublishDbProvider publishProvider,
        IServiceProvider serviceProvider)
        : CoreWorkerParamsJob<RemoveOldPublicationJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выолнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RemoveOldPublicationJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            publishProvider.RemoveOldDatabasePublication(serviceProvider,jobParams.AccountDatabaseId, jobParams.OldSegmentId);
        }
    }
}
