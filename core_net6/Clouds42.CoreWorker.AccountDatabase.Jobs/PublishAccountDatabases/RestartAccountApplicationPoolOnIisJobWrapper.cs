﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача по перезапуску пула приложения аккаунта на нодах публикаций 
    /// </summary>
    public class RestartAccountApplicationPoolOnIisJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RestartAccountApplicationPoolOnIisJob,
            RestartAccountApplicationPoolOnIisJobParamsDto>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить таску перезапуска пула аккаунта на нодах публикаций
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<RestartAccountApplicationPoolOnIisJob, RestartAccountApplicationPoolOnIisJobParamsDto> Start(RestartAccountApplicationPoolOnIisJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Перезапуск пула на нодах публикаций.");
            return this;
        }
    }
}
