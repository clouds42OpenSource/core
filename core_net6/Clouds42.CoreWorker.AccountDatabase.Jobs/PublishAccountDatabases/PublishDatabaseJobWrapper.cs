﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача публикации базы на IIS
    /// </summary>
    public class PublishDatabaseJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<PublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить таску публикации базы
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<PublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto> Start(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Публикация базы.");
            return this;
        }
    }
}
