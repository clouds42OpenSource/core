﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача переопубликации базы со сменой значения в конфиге
    /// </summary>
    public class RepublishWithChangingConfigJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RepublishWithChangingConfigJob, PublishOrCancelPublishDatabaseJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить таску
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<RepublishWithChangingConfigJob, PublishOrCancelPublishDatabaseJobParamsDto> Start(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Переопубликация базы с изменением в web.config.");
            return this;
        }
    }
}
