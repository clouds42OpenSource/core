﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <summary>
    ///     Задача по отмене публикации базы на IIS
    /// </summary>
    public class CancelPublishDatabaseJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<CancelPublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить таску отмены публикации базы
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override TypingParamsJobWrapperBase<CancelPublishDatabaseJob, PublishOrCancelPublishDatabaseJobParamsDto> Start(PublishOrCancelPublishDatabaseJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Отмена публикации базы.");
            return this;
        }
    }
}
