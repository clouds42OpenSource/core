﻿using System;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases
{
    /// <inheritdoc />
    /// <summary>
    /// Джоба рестарта пула приложения аккаунта на нодах публикаций
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RestartAccountApplicationPoolOnIisJob)]
    public class RestartAccountApplicationPoolOnIisJob(
        IUnitOfWork unitOfWork,
        IRestartDatabaseIisApplicationPoolProvider restartDatabaseIisApplicationPoolProvider)
        : CoreWorkerParamsJob<RestartAccountApplicationPoolOnIisJobParamsDto>(unitOfWork)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RestartAccountApplicationPoolOnIisJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            restartDatabaseIisApplicationPoolProvider.RestartIisApplicationPool(jobParams.AccountDatabaseId);
        }
    }
}
