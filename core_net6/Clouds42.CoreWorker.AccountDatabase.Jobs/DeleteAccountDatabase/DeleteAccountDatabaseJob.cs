﻿using System;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteAccountDatabase
{
    /// <summary>
    /// Задача по удалению инф. базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteAccountDatabaseJob)]
    public class DeleteAccountDatabaseJob(
        IUnitOfWork dbLayer,
        IDeleteAccountDatabaseProvider deleteAccountDatabaseProvider)
        : CoreWorkerParamsJob<DeleteAccountDatabaseParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(DeleteAccountDatabaseParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            deleteAccountDatabaseProvider.DeleteAccountDatabase(jobParams);
        }
    }
}
