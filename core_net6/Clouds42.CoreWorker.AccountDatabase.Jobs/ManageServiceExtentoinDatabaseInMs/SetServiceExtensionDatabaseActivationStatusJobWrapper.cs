﻿using Clouds42.BLL.CoreWorkerTask;
using Clouds42.BLL.CoreWorkerTask.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.ServiceExtensionDatabase;
using Repositories;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs
{
    /// <summary>
    /// Обработчик задач по установке статуса активации
    /// расширения сервиса для инф. базы 
    /// </summary>
    public class SetServiceExtensionDatabaseActivationStatusJobWrapper
        : SynchronizationParamsJobWrapperBase<SetServiceExtensionDatabaseActivationStatusJob, SetServiceExtensionDatabaseActivationStatusDto>
    {
        public SetServiceExtensionDatabaseActivationStatusJobWrapper(IUnitOfWork dbLayer, IRegisterTaskInQueueProvider registerTaskInQueueProvider) : base(dbLayer, registerTaskInQueueProvider)
        {
        }

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override
            TypingParamsJobWrapperBase<SetServiceExtensionDatabaseActivationStatusJob,
                SetServiceExtensionDatabaseActivationStatusDto> Start(
                SetServiceExtensionDatabaseActivationStatusDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));
            return this;
        }

        /// <summary>
        /// Создать ключ синхронизации
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Ключ синхронизации</returns>
        protected override string CreateSynchronizationKey(SetServiceExtensionDatabaseActivationStatusDto paramsJob) =>
            TaskSynchronizationKey.SetServiceExtensionDatabaseActivationStatus.CreateKey(paramsJob.AccountDatabaseId,
                paramsJob.ServiceId);

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(SetServiceExtensionDatabaseActivationStatusDto paramsJob) =>
            $"Установка статуса активации '{paramsJob.IsActive}' расширения сервиса '{paramsJob.ServiceId}' для инф. базы '{paramsJob.AccountDatabaseId}'";
    }
}