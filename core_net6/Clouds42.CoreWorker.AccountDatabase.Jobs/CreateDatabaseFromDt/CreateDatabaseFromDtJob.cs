﻿using System;
using Clouds42.AccountDatabase.Contracts.Register;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromDt
{

    /// <summary>
    /// Задача по созданию базы из dt.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CreateDatabaseFromDtJob)]
    public class CreateDatabaseFromDtJob(
        IUnitOfWork dbLayer,
        IRegisterDbOnServerFromDtProvider registerDbOnServerFromDtProvider)
        : CoreWorkerParamsJob<RegisterDatabaseFromUploadFileModelDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(RegisterDatabaseFromUploadFileModelDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                registerDbOnServerFromDtProvider.RegisterDatabaseOnServer(jobParams);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, $"[Ошибка регистрации базы из dt файла] id='{jobParams.AccountDatabaseId}' файл: '{jobParams.UploadedFileId}'");
            }

        }

    }
}
