﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromDt
{
    public class CreateDatabaseFromDtJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : TypingParamsJobWrapperBase<CreateDatabaseFromDtJob, RegisterDatabaseFromUploadFileModelDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        public override TypingParamsJobWrapperBase<CreateDatabaseFromDtJob, RegisterDatabaseFromUploadFileModelDto> Start(RegisterDatabaseFromUploadFileModelDto paramsJob)
        {
            StartTask(paramsJob, $"Регистрация базы из dt '{paramsJob.UploadedFileId}'");
            return this;
        }
    }
}
