﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs
{
    /// <summary>
    /// Обработчик задач по установке статуса активации
    /// расширения сервиса для инф. базы 
    /// </summary>
    public class SetServiceExtensionDatabaseActivationStatusJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<SetServiceExtensionDatabaseActivationStatusJob,
            SetServiceExtensionDatabaseActivationStatusDto>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override
            TypingParamsJobWrapperBase<SetServiceExtensionDatabaseActivationStatusJob,
                SetServiceExtensionDatabaseActivationStatusDto> Start(
                SetServiceExtensionDatabaseActivationStatusDto paramsJob)
        {
            StartTask(paramsJob, GetTaskComment(paramsJob));
            return this;
        }

        /// <summary>
        /// Получить комментарий для задачи
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Комментарий для задачи</returns>
        private string GetTaskComment(SetServiceExtensionDatabaseActivationStatusDto paramsJob) =>
            $"Установка статуса активации '{paramsJob.IsActive}' расширения сервиса '{paramsJob.ServiceId}' для инф. базы '{paramsJob.AccountId}'";
    }
}
