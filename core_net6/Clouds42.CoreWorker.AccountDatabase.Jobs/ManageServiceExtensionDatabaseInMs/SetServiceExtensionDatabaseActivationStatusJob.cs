﻿using System;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs
{
    /// <summary>
    /// Задача на установку статуса активации
    /// расширения сервиса для инф. базы 
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.SetServiceExtensionDatabaseActivationStatusJob)]
    public class SetServiceExtensionDatabaseActivationStatusJob(
        IUnitOfWork dbLayer,
        ISetServiceExtensionDatabaseActivationStatusCommand serviceExtensionDatabaseActivationStatusCommand)
        : CoreWorkerParamsJobWithRetry<SetServiceExtensionDatabaseActivationStatusDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(SetServiceExtensionDatabaseActivationStatusDto jobParams, Guid taskId,
            Guid taskQueueId)
        {
            try
            {
                serviceExtensionDatabaseActivationStatusCommand.Execute(jobParams);
                return CreateRetryParams(taskQueueId, false);
            }
            catch (Exception)
            {
                return CreateRetryParams(taskQueueId, true);
            }

        }
    }
}
