﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase
{
    /// <summary>
    /// Обработчик задачи по завершению сеансов в инф. базе
    /// </summary>
    public class TerminateSessionsInDatabaseJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : TypingParamsJobWrapperBase<TerminateSessionsInDatabaseJob,
                TerminateSessionsInDatabaseJobParamsDto>(registerTaskInQueueProvider, dbLayer),
            ITerminateSessionsInDatabaseJobWrapper
    {
        /// <summary>
        /// Запустить задачу по завершению сеансов в инф. базе
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи по завершению сеансов в информационной базе</param>
        /// <returns>Прослойка выполнения</returns>
        public override TypingParamsJobWrapperBase<TerminateSessionsInDatabaseJob, TerminateSessionsInDatabaseJobParamsDto>
            Start(TerminateSessionsInDatabaseJobParamsDto paramsJob)
        {
            StartTask(paramsJob,
                $"Завершение сеансов в информационной базе. Id процесса: '{paramsJob.TerminationSessionsInDatabaseId}'");

            return this;
        }
    }
}
