﻿using System;
using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase
{
    /// <summary>
    /// Задача для завершения сеансов в информационной базе 
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.TerminateSessionsInDatabaseJob)]
    public class TerminateSessionsInDatabaseJob(
        IUnitOfWork dbLayer,
        ITerminateSessionsInDatabaseProvider terminateSessionsInDatabasProvider)
        : CoreWorkerParamsJob<TerminateSessionsInDatabaseJobParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу по завершению сеансов в инф. базе
        /// </summary>
        /// <param name="jobParams">Модель параметров задачи по завершению сеансов в информационной базе</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void
            Execute(TerminateSessionsInDatabaseJobParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            terminateSessionsInDatabasProvider.Terminate(jobParams.MapToTerminateSessionsInDatabaseParamsDc());
        }
    }
}