﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase
{
    /// <summary>
    /// Обработчик задачи по завершению сеансов в инф. базе
    /// </summary>
    public interface ITerminateSessionsInDatabaseJobWrapper
    {
        /// <summary>
        /// Запустить задачу по завершению сеансов в инф. базе
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи по завершению сеансов в информационной базе</param>
        /// <returns>Прослойка выполнения</returns>
        TypingParamsJobWrapperBase<TerminateSessionsInDatabaseJob, TerminateSessionsInDatabaseJobParamsDto> Start(
            TerminateSessionsInDatabaseJobParamsDto paramsJob);
    }
}
