﻿using System;
using Clouds42.AccountDatabase.Contracts.Delete.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData
{
    /// <summary>
    /// Задача по удалению данных неактивных аккаунтов
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.DeleteInactiveAccountsDataJob)]
    public class DeleteInactiveAccountsDataJob(
        IDeleteInactiveAccountDataProvider deleteInactiveAccountDataProvider,
        IBeforeDeleteAccountDatabaseProvider beforeDeleteAccountDatabaseProvider)
        : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            deleteInactiveAccountDataProvider.DeleteInactiveAccountData();
            beforeDeleteAccountDatabaseProvider.DeleteBackupAccountDatabase();
        }
    }
}
