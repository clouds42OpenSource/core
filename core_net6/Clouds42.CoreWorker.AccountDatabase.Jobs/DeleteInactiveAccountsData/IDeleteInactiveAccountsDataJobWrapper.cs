﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData
{
    /// <summary>
    /// Задача по удалению данных неактивных аккаунтов
    /// </summary>
    public interface IDeleteInactiveAccountsDataJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        JobWrapperBase Start();
    }
}
