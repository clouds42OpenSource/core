﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData
{
    /// <summary>
    /// Задача по удалению данных неактивных аккаунтов
    /// </summary>
    public class DeleteInactiveAccountsDataJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer), IDeleteInactiveAccountsDataJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        public JobWrapperBase Start()
        {
            StartTask("Удаление данных неактивных аккаунтов.");
            return this;
        }

        /// <summary>
        /// Тип задачи - удаление данных неактивных аккаунтов
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.DeleteInactiveAccountsDataJob;
    }
}
