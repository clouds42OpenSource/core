﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountUsersDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportInvoicesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportPaymentsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportResourcesDataToBigQuery;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportDataToBigQuery
{
    /// <summary>
    /// Задача на экспорт всех необходимых данных в BigQuery
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ExportDataToBigQueryJob)]
    public class ExportDataToBigQueryJob(IServiceProvider serviceProvider) : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            using var dbScope = serviceProvider.GetRequiredService<IUnitOfWork>().SmartTransaction.Get();
            try
            {
                serviceProvider.GetRequiredService<IExportAccountsDataToBigQueryJobWrapper>().Start();
                serviceProvider.GetRequiredService<IExportAccountUsersDataToBigQueryJobWrapper>().Start();
                serviceProvider.GetRequiredService<IExportDatabasesDataToBigQueryJobWrapper>().Start();
                serviceProvider.GetRequiredService<IExportInvoicesDataToBigQueryJobWrapper>().Start();
                serviceProvider.GetRequiredService<IExportPaymentsDataToBigQueryJobWrapper>().Start();
                serviceProvider.GetRequiredService<IExportResourcesDataToBigQueryJobWrapper>().Start();

                dbScope.Commit();
            }
            catch (Exception ex)
            {
                var message = "Экспорт данных в BigQuery завершился с ошибкой";
                HandlerException.Handle(ex, "[Ошибка экспорта данных в BigQuery]");
                dbScope.Rollback();
                serviceProvider.GetRequiredService<IHandlerException>().Handle(ex, message);
                throw;
            }
        }
    }
}
