﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportAccountUsersDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных пользователей в BigQuery
    /// </summary>
    public interface IExportAccountUsersDataToBigQueryJobWrapper : ITypingJobWrapper<ExportAccountUsersDataToBigQueryJob>
    {
    }
}
