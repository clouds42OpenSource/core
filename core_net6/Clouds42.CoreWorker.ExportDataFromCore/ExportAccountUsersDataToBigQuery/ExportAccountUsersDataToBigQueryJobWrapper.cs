﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportAccountUsersDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных пользователей в BigQuery
    /// </summary>
    internal class ExportAccountUsersDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportAccountUsersDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportAccountUsersDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportAccountUsersDataToBigQueryJob> Start()
        {
            StartTask("Экспорт данных пользователей в BigQuery");
            return this;
        }
    }
}
