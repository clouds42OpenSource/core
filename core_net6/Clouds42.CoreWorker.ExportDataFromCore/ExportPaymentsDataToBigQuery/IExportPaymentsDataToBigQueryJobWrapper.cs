﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportPaymentsDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных платежей в BigQuery
    /// </summary>
    public interface IExportPaymentsDataToBigQueryJobWrapper : ITypingJobWrapper<ExportPaymentsDataToBigQueryJob>
    {
    }
}
