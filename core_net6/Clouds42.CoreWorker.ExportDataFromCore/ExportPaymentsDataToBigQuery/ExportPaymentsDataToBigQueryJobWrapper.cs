﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportPaymentsDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных платежей в BigQuery
    /// </summary>
    internal class ExportPaymentsDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportPaymentsDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportPaymentsDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportPaymentsDataToBigQueryJob> Start()
        {
            StartTask("Экспорт данных платежей в BigQuery");
            return this;
        }
    }
}
