﻿using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportAccountUsersDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportInvoicesDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportPaymentsDataToBigQuery;
using Clouds42.CoreWorker.ExportDataFromCore.ExportResourcesDataToBigQuery;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.ExportDataFromCore
{
    /// <summary>
    /// Регистратор зависимостей для сборки задач
    /// по экспорту данных из облака
    /// </summary>
    public static class DependencyInjections
    {
        /// <summary>
        /// Зарегистрировать зависимости
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddCoreWorkerExportDataFromCoreWrappers(this IServiceCollection servicesCollection)
        {
            servicesCollection.AddTransient<IExportAccountUsersDataToBigQueryJobWrapper, ExportAccountUsersDataToBigQueryJobWrapper>();
            servicesCollection.AddTransient<IExportAccountsDataToBigQueryJobWrapper, ExportAccountsDataToBigQueryJobWrapper>();
            servicesCollection.AddTransient<IExportResourcesDataToBigQueryJobWrapper, ExportResourcesDataToBigQueryJobWrapper>();
            servicesCollection.AddTransient<IExportInvoicesDataToBigQueryJobWrapper, ExportInvoicesDataToBigQueryJobWrapper>();
            servicesCollection.AddTransient<IExportPaymentsDataToBigQueryJobWrapper, ExportPaymentsDataToBigQueryJobWrapper>();
            servicesCollection.AddTransient<IExportDatabasesDataToBigQueryJobWrapper, ExportDatabasesDataToBigQueryJobWrapper>();
            
            return servicesCollection;
        }
    }
}