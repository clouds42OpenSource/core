﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportResourcesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных ресурсов в BigQuery
    /// </summary>
    public interface IExportResourcesDataToBigQueryJobWrapper : ITypingJobWrapper<ExportResourcesDataToBigQueryJob>
    {
    }
}
