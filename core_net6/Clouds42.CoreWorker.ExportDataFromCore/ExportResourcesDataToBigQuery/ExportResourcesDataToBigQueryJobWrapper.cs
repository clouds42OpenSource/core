﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportResourcesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных ресурсов в BigQuery
    /// </summary>
    internal class ExportResourcesDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportResourcesDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportResourcesDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportResourcesDataToBigQueryJob> Start()
        {
            StartTask("Экспорт данных ресурсов в BigQuery");
            return this;
        }
    }
}
