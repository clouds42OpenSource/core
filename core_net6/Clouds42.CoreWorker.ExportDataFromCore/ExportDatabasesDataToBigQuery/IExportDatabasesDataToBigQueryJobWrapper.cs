﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных инф. баз в BigQuery
    /// </summary>
    public interface IExportDatabasesDataToBigQueryJobWrapper : ITypingJobWrapper<ExportDatabasesDataToBigQueryJob>
    {
    }
}
