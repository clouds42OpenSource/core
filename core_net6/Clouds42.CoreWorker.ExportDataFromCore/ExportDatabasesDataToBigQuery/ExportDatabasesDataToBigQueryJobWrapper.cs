﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных инф. баз в BigQuery
    /// </summary>
    internal class ExportDatabasesDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportDatabasesDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportDatabasesDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportDatabasesDataToBigQueryJob> Start()
        {   
            StartTask("Экспорт данных инф. баз в BigQuery");
            return this;
        }
    }
}
