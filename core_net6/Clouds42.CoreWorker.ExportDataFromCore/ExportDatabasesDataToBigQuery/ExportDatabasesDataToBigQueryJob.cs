﻿using System;
using Clouds42.BigQuery.Contracts.ExportDataFromCore;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.ExportDataFromCore.Helpers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportDatabasesDataToBigQuery
{
    /// <summary>
    /// Задача на экспорт данных инф. баз в BigQuery
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.ExportDatabasesDataToBigQueryJob)]
    public class ExportDatabasesDataToBigQueryJob(
        IUnitOfWork dbLayer,
        IExportDataToBigQueryManager exportDataToBigQueryManager)
        : CoreWorkerJobWithRetry(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Параметры перезапуска задачи</returns>
        protected override RetryJobParamsDto ExecuteAndDetermineRetryNeed(Guid taskId, Guid taskQueueId)
        {
            var managerResult = exportDataToBigQueryManager.ExportDatabasesData();
            return CreateRetryParams(taskQueueId, managerResult.Error,
                RetryExportDataToBigQueryJobHelper.GetRetryExportDataJobDelayInSeconds());
        }
    }
}