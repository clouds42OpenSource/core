﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportAccountsDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных аккаунтов в BigQuery
    /// </summary>
    internal class ExportAccountsDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportAccountsDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportAccountsDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportAccountsDataToBigQueryJob> Start()
        {
            StartTask("Экспорт данных аккаунтов в BigQuery");
            return this;
        }
    }
}
