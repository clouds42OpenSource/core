﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportAccountsDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных аккаунтов в BigQuery
    /// </summary>
    public interface IExportAccountsDataToBigQueryJobWrapper : ITypingJobWrapper<ExportAccountsDataToBigQueryJob>
    {
    }
}