﻿using System;
using Clouds42.Configurations.Configurations;

namespace Clouds42.CoreWorker.ExportDataFromCore.Helpers
{
    /// <summary>
    /// Хелпер для перезапуска задачи на экспорт данных в BigQuery
    /// </summary>
    public static class RetryExportDataToBigQueryJobHelper
    {
        /// <summary>
        /// Задержка для перезапуска задачи
        /// на экспорт данных в BigQuery(в секундах)
        /// </summary>
        private static readonly Lazy<int> RetryExportDataJobDelayInSeconds =
            new(CloudConfigurationProvider.BigQuery.GetRetryExportDataJobDelayInSeconds);

        /// <summary>
        /// Получить задержку для перезапуска задачи
        /// на экспорт данных в BigQuery(в секундах)
        /// </summary>
        /// <returns>Задержка для перезапуска задачи в секундах</returns>
        public static int GetRetryExportDataJobDelayInSeconds() => RetryExportDataJobDelayInSeconds.Value;
    }
}