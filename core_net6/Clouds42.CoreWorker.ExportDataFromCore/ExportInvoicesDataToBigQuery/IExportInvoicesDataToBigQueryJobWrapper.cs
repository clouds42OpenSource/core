﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportInvoicesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных счетов на оплату в BigQuery
    /// </summary>
    public interface IExportInvoicesDataToBigQueryJobWrapper : ITypingJobWrapper<ExportInvoicesDataToBigQueryJob>
    {
    }
}
