﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.ExportDataFromCore.ExportInvoicesDataToBigQuery
{
    /// <summary>
    /// Обработчик задачи по экспорту данных счетов на оплату в BigQuery
    /// </summary>
    internal class ExportInvoicesDataToBigQueryJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        :
            TypingJobWrapperBase<ExportInvoicesDataToBigQueryJob>(registerTaskInQueueProvider, dbLayer),
            IExportInvoicesDataToBigQueryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        public override TypingJobWrapperBase<ExportInvoicesDataToBigQueryJob> Start()
        {
            StartTask("Экспорт данных счетов на оплату в BigQuery");
            return this;
        }
    }
}
