﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core42.Models.Payments
{
    public class PromisePaymentActionRequest
    {
        [Required]
        public Guid AccountId { get; set; }
    }
}