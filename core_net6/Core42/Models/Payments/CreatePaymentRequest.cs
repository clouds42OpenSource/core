﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.Enums;

namespace Core42.Models.Payments
{
    public class CreatePaymentRequest
    { 
        /// <summary>
        /// Id аккаунта платежа
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>        
        [Required(ErrorMessage = "Сумма обязательна")]
        [RegularExpression(@"((^|, )(^-?\d+(\.\d{2})?$|^-?\d+(\,\d{2})?$))+$", ErrorMessage = "Недопустимое значение.")]
        public decimal Total { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// Дата создания транзакции
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType TransactionType { get; set; }
    }
}