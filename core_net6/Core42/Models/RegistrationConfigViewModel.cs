﻿using System;
using Clouds42.Domain.Enums;

namespace Core42.Models
{
    /// <summary>
    /// Модель регистрации пользователей
    /// </summary>
    public class RegistrationConfigViewModel
    {
        /// <summary>
        /// Имя сервиса.
        /// Примечание только системные сервисы. 
        /// </summary>
        public string CloudService { get; set; }

        /// <summary>
        /// Номепр сервиса.
        /// Примечание только не системные сервисы.
        /// </summary>
        public Guid? CloudServiceId { get; set; }

        /// <summary>
        /// Имя конфигурации которую нужно развернуть.
        /// </summary>
        public string Configuration1C { get; set; }

        /// <summary>
        /// Номер рефферала.
        /// </summary>
        public Guid? ReferralAccountId { get; set; }

        /// <summary>
        /// Источник регистрации.
        /// </summary>
        public ExternalClient RegistrationSource { get; set; }

        /// <summary>
        /// Получить имя системного сервиса из строки.
        /// </summary>        
        public Clouds42Service GetSystemCloudServiceEnumValue()
        {
            if (string.IsNullOrEmpty(CloudService))
                return Clouds42Service.Unknown;

            if (!Enum.TryParse(CloudService, out Clouds42Service cloudService))
                cloudService = Clouds42Service.MyEnterprise;

            return cloudService;
        }

    }
}
