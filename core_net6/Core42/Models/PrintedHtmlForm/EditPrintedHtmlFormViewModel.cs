﻿using System;

namespace Core42.Models.PrintedHtmlForm
{
    /// <summary>
    /// Модель отображения для редактирования печатной формы
    /// </summary>
    public class EditPrintedHtmlFormViewModel : BasePrintedHtmlFormViewModel
    {
        /// <summary>
        /// Id печатной формы
        /// </summary>
        public Guid Id { get; set; }
    }
}
