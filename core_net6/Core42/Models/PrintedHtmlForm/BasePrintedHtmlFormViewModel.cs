﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Clouds42.BLL.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Core42.Models.PrintedHtmlForm
{
    /// <summary>
    /// Базовый модель отображения печатной формы
    /// </summary>
    public class BasePrintedHtmlFormViewModel
    {
        /// <summary>
        /// Название печатной формы Html
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Html разметка
        /// </summary>
        public string HtmlData { get; set; }

        /// <summary>
        /// Тип модели
        /// </summary>
        public string ModelType { get; set; }

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<CloudFileViewModel> Files { get; set; }

        /// <summary>
        /// Новые подгруженные файлы
        /// </summary>
        public List<IFormFile> UploadedFiles { get; set; }

        /// <summary>
        /// Маппинг моделя отображения печатной формы к модели контракта печатной формы
        /// </summary>
        /// <returns>Модель контракта печатной формы</returns>
        public PrintedHtmlFormDto MappingToDataContract()
        {
            return new PrintedHtmlFormDto
            {
                Name = Name,
                HtmlData = HtmlData,
                ModelType = ModelType,
                Files = Files?.Where(f => !f.IsDeleted).Select(f => new CloudFileDto
                {
                    Id = f.Id,
                    FileName = f.FileName,
                    ContentType = f.ContentType,
                    Bytes = f.Bytes,
                    Base64 = f.Base64
                }).ToList()
            };
        }

        /// <summary>
        /// Заполнить массив файлов новыми данными
        /// </summary>
        /// <returns>Модель отображения печатной формы HTML</returns>
        public BasePrintedHtmlFormViewModel FillAttachmentFiles()
        {
            if (Files == null)
                Files = [];

            Files.RemoveAll(f => f.IsDeleted);

            if (UploadedFiles == null)
                return this;

            foreach (var file in UploadedFiles)
            {
                if (file == null)
                    continue;

                var bytes = file.ConvertToByteArray();
                Files.Add(new CloudFileViewModel
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    Bytes = bytes,
                    Base64 = Convert.ToBase64String(bytes)
                });
            }

            return this;
        }
    }
}
