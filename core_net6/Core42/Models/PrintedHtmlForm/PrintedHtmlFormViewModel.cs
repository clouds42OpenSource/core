﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Clouds42.BLL.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Core42.Models.PrintedHtmlForm
{
    /// <summary>
    /// Модель отображения печатной формы HTML
    /// </summary>
    public class PrintedHtmlFormViewModel: IPagedListFilter
    {
        /// <summary>
        /// Текущая страница
        /// (вызов редактирования печатной формы вне справочника)
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Размер страницы
        /// (вызов редактирования печатной формы вне справочника)
        /// </summary>
        public int PageSize { get; set; }


        /// <summary>
        /// Id печатной формы
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// Название печатной формы Html
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Html разметка
        /// </summary>
        public string HtmlData { get; set; }

        /// <summary>
        /// Тип модели
        /// </summary>
        public string ModelType { get; set; }

        /// <summary>
        /// Тип моделей
        /// </summary>
        public List<SelectListItem> ModelTypes { get; set; }

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        public List<CloudFileViewModel> Files { get; set; }

        /// <summary>
        /// Новые подгруженные файлы
        /// </summary>
        public List<IFormFile> UploadedFiles { get; set; }


        /// <summary>
        /// Привести модель к типу представления
        /// </summary>
        /// <param name="dc">Модель контракта печатной формы</param>
        /// <returns>Модель отображения печатной формы HTML</returns>
        public static PrintedHtmlFormViewModel ToViewModel(PrintedHtmlFormDto dc)
        {
            return new PrintedHtmlFormViewModel
            {
                Id = dc.Id,
                HtmlData = dc.HtmlData,
                Name = dc.Name,
                ModelType = dc.ModelType,
                ModelTypes = dc.ModelTypes.Select(w => new SelectListItem{Text = w.Value, Value = w.Key}).ToList(),
                Files = dc.Files?.Select(w => new CloudFileViewModel
                {
                    Id = w.Id,
                    FileName = w.FileName,
                    ContentType = w.ContentType,
                    Bytes = w.Bytes,
                    Base64 = w.Base64
                }).ToList()
            };
        }

        /// <summary>
        /// Привести модель к контракту
        /// </summary>
        /// <returns>Модель контракта печатной формы</returns>
        public PrintedHtmlFormDto ToDataContract()
        {
            return new PrintedHtmlFormDto
            {
                Id = Id,
                Name = Name,
                HtmlData = HtmlData,
                ModelType = ModelType,
                Files = Files?.Where(w => !w.IsDeleted).Select(w => new CloudFileDto
                {
                    Id = w.Id,
                    FileName = w.FileName,
                    ContentType = w.ContentType,
                    Bytes = w.Bytes,
                    Base64 = w.Base64
                }).ToList()
            };
        }

        /// <summary>
        /// Заполнить массив файлов новыми данными
        /// </summary>
        /// <returns>Модель отображения печатной формы HTML</returns>
        public PrintedHtmlFormViewModel FillAttachmentFiles()
        {
            if (Files == null)
                Files = [];

            Files.RemoveAll(w => w.IsDeleted);

            if (UploadedFiles == null)
                return this;

            foreach (var file in UploadedFiles)
            {
                if (file == null)
                    continue;

                var bytes = file.ConvertToByteArray();
                Files.Add(new CloudFileViewModel
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    Bytes = bytes,
                    Base64 = Convert.ToBase64String(bytes)
                });
            }

            return this;
        }
    }
}
