﻿namespace Core42.Models
{
    /// <summary>
    /// Описание модели ключ/значение
    /// </summary>
    public class KeyValuePairModelDescription : ModelDescription
    {
        /// <summary>
        /// Описание модели ключа
        /// </summary>
        public ModelDescription KeyModelDescription { get; set; }

        /// <summary>
        /// Описание модели значения
        /// </summary>
        public ModelDescription ValueModelDescription { get; set; }
    }
}