﻿using System;

namespace Core42.Models
{
    /// <summary>
    /// Описание параметра
    /// </summary>
    public class ParameterDescriptionVm
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип параметра
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// Источник описания модели
        /// </summary>
        public ModelDescription SourceTypeDescription { get; set; }
    }
}