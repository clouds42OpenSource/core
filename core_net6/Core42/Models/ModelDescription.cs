﻿using System;

namespace Core42.Models
{
    /// <summary> 
    /// Описание модели
    /// </summary> 
    public abstract class ModelDescription
    {
        /// <summary>
        /// Документация
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public Type ModelType { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }
}