﻿namespace Core42.Models.CurrentSessionSettings
{
    public class PopupMessageViewModel
    {
        public string Text { get; set; }
        public string Type { get; set; }
    }
}
