﻿namespace Core42.Models
{
    /// <summary>
    /// Описание значения Enum
    /// </summary>
    public class EnumValueDescription
    {
        /// <summary>
        /// Документация
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }
}