﻿using System.Collections.Generic;

namespace Core42.Models
{
    /// <summary>
    /// Информация о контроллере
    /// </summary>
    public class ControllerInfoVm
    {
        /// <summary>
        /// Название контроллера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание контроллера
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Методы контроллера
        /// </summary>
        public List<ActionInfoVm> ActionsInfo { get; set; } = [];
    }
}