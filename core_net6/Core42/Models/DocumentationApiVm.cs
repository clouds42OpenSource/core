﻿using System.Collections.Generic;

namespace Core42.Models
{
    /// <summary>
    /// Документация Api
    /// </summary>
    public class DocumentationApiVm
    {
        /// <summary>
        /// Информация о контроллерах
        /// </summary>
        public List<ControllerInfoVm> ControllersInfo { get; set; } = [];
    }
}