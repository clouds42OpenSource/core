﻿using System.Collections.ObjectModel;

namespace Core42.Models
{
    /// <summary>
    /// Описание модели Enum
    /// </summary>
    public class EnumTypeModelDescription : ModelDescription
    {
        /// <summary>
        /// Значения Enum
        /// </summary>
        public Collection<EnumValueDescription> Values { get; private set; } = [];
    }
}