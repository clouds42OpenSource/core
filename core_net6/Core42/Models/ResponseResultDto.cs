﻿

namespace Core42.Models
{
    public class ResponseResultDto<T>
    {
        public bool Success { get; set; }

        public T Data { get; set; }

        public string Message { get; set; }

        public string Code { get; set; }
    }
}
