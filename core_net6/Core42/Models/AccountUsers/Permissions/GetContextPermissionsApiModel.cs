﻿using System;
using System.Collections.Generic;

namespace Core42.Models.AccountUsers.Permissions
{
    /// <summary>
    /// Модель запроса разрешений пользователя для указанного контекста
    /// </summary>
    public class GetContextPermissionsApiModel
    {
        /// <summary>
        /// Таргеты разрешений
        /// </summary>
        public PermissionContext Context { get; set; }

        /// <summary>
        /// Список проверяемых ращрешений
        /// </summary>
        public ICollection<string> Permissions { get; set; }
    }

    /// <summary>
    /// Таргеты для запроса разрешений пользователя
    /// </summary>
    public class PermissionContext
    {

        /// <summary>
        /// ID таргетируемого аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// ID таргетируемого пользователя 
        /// </summary>
        public Guid? AccountUserId { get; set; }
    }
}