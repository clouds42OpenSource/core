﻿using System;
using System.Collections.Generic;

namespace Core42.Models.AccountUsers
{
    /// <summary>
    /// API модель пользователя
    /// </summary>
    public class AccountUserApiModel
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID Аккаунта пользователя
        /// </summary>        
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>        
        public string MiddleName { get; set; }

        /// <summary>
        /// Полное имя пользователя
        /// </summary>        
        public string FullName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>        
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     Пользователь активирован (подтвержден мобильный или одобрен менеджером)
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        ///     Пользователь создан в домене Active Directory
        /// </summary>
        public bool CreatedInAd { get; set; }


        /// <summary>
        /// Признак что пользователь отписан от рассылки
        /// </summary>
        public bool? Unsubscribed { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Роли пользователя в аккаунте
        /// </summary>
        public List<string> AccountRoles { get; set; }
    }
}