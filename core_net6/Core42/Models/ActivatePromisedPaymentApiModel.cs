﻿using System;

namespace Core42.Models
{
    /// <summary>
    /// Модель запроса на активацию обещанного платежа
    /// </summary>
    public class ActivatePromisedPaymentApiModel
    {
        /// <summary>
        /// ID инвойса на основании которого нужно активировать обещанный платеж
        /// </summary>
        public Guid InvoiceId { get; set; }
    }
}