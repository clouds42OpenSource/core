﻿using System;

namespace Core42.Models.Invoices.Creation
{
    /// <summary>
    /// API модель создания инвойса на произвольную сумму
    /// </summary>
    public class CreateInvoiceFromArbitraryAmountApiModel : ArbitraryAmountInvoiceApiModel
    {
        /// <summary>
        /// ID пользователя которому необходимо отправить уведомление о создании инвойса
        /// </summary>
        public Guid? NotifyAccountUserId { get; set; }
    }
}