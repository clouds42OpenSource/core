﻿using Core42.Models.Invoices.Calculator;
using System;

namespace Core42.Models.Invoices.Creation
{
    /// <summary>
    /// API модель для создания инвойса из расчета
    /// </summary>
    public class CreateInvoiceFromCalculationApiModel : InvoiceCalculationApiModel
    {
        /// <summary>
        /// ID пользователя которому необходимо отправить уведомление о создании инвойса
        /// </summary>
        public Guid? NotifyAccountUserId { get; set; }
    }
}
