﻿using System;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing.ClosingDocuments;
using Clouds42.Domain.Enums;

namespace Core42.Models.Invoices
{
    /// <summary>
    /// API модель инвойса
    /// </summary>
    public class InvoiceInfoApiModel
    {
        /// <summary>
        /// Id счета
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата счета
        /// </summary>
        public DateTime InvoiceDate { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }
        
        /// <summary>
        /// Флаг нужна ли подпись для акта или нет
        /// </summary>

        public bool RequiredSignature { get; set; }
        
        /// <summary>
        /// Статус акта
        /// </summary>

        public Status Status { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Статус счета
        /// </summary>
        public InvoiceStatus State { get; set; }

        /// <summary>
        /// Описание акта
        /// </summary>
        public string ActDescription { get; set; }

        /// <summary>
        /// Id акта
        /// </summary>
        public Guid? ActId { get; set; }

        /// <summary>
        /// Это новый счет
        /// </summary>
        public bool IsNewInvoice { get; set; }

        /// <summary>
        /// Номер фиксального чека
        /// </summary>
        public string ReceiptFiscalNumber { get; set; }

        /// <summary>
        /// Префикс
        /// </summary>
        public string Uniq { get; set; }

        /// <summary>
        /// Создать из DC модели
        /// </summary>
        /// <param name="source">Исходная DC модель</param>
        /// <returns>API модель</returns>
        public static InvoiceInfoApiModel CreateFrom(InvoiceDc source)
        {
            if (source == null) return default;
            return new InvoiceInfoApiModel
            {
                Id = source.Id,
                ActDescription = source.ActDescription,
                ActId = source.ActId,
                Description = source.Description,
                InvoiceDate = source.InvoiceDate,
                InvoiceSum = source.InvoiceSum,
                IsNewInvoice = source.IsNewInvoice,
                ReceiptFiscalNumber = source.ReceiptFiscalNumber,
                State = source.State,
                Uniq = source.Uniq
            };
        }
    }
}
