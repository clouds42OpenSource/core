﻿using System;

namespace Core42.Models.Invoices
{
    /// <summary>
    /// API модель результата создания инвойса
    /// </summary>
    public class CreateInvoiceResultApiModel
    {
        /// <summary>
        /// ID созданного инвойса
        /// </summary>
        public Guid Id { get; set; }
    }
}