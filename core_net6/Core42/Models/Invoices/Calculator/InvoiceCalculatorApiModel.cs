﻿using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Core42.Models.Locale;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Core42.Models.Invoices.Calculator
{
    /// <summary>
    /// API Модель калькулятора инвойсов
    /// </summary>
    public class InvoiceCalculatorApiModel
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Имя покупателя
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// Имя поставщика
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Детали валюты инвойса
        /// </summary>
        public CurrencyApiModel Currency { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Итого до НДС
        /// </summary>
        public decimal TotalBeforeVAT { get; set; }

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Итого после НДС
        /// </summary>
        public decimal TotalAfterVAT { get; set; }

        /// <summary>
        /// Итого бонуснов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Калькуляторы продуктов
        /// </summary>
        public List<InvoiceProductCalculatorApiModel> Products { get; set; }
            = [];

        /// <summary>
        /// Контроллер значений поля "Платежный период" (null если платежный период не требуется)
        /// </summary>
        public InvoiceCalculatorControlApiModel PayPeriodControl { get; set; }

        /// <summary>
        /// Создать из Dc модели калькулятора инвойсов
        /// </summary>
        /// <param name="source">Исходная Dc модель калькулятора инвойса</param>
        /// <returns>API модель калькулятора инвойса</returns>
        public static InvoiceCalculatorApiModel CreateFrom(InvoiceCalculatorDto source)
        {
            if (source == null) return null;
            var result = new InvoiceCalculatorApiModel
            {
                AccountId = source.AccountId,
                BuyerName = source.BuyerName,
                SupplierName = source.SupplierName,
                Currency = CurrencyApiModel.CreateFrom(source.Currency),
                PayPeriod = source.PayPeriod,
                TotalBeforeVAT = source.TotalBeforeVAT,
                VAT = source.VAT,
                TotalAfterVAT = source.TotalAfterVAT,
                TotalBonus = source.TotalBonus,
                Products = source.Products.Select(p => InvoiceProductCalculatorApiModel.CreateFrom(p)).ToList(),
                PayPeriodControl = InvoiceCalculatorControlApiModel.CreateFrom(source.PayPeriodControl)
            };

            return result;
        }
    }

    /// <summary>
    /// API модель калькулятора продукта инвойса
    /// </summary>
    public class InvoiceProductCalculatorApiModel
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Включен ли продукт в инвойсе
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Стоиость одной единицы продукта
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Итого стоимость
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Итого бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Имя Продукта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание продукта
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Контроллер значений поля "Количество"
        /// </summary>
        public InvoiceCalculatorControlApiModel QuantityControl { get; set; }

        /// <summary>
        /// Создать из Dc модель калькулятора продукта инвойса
        /// </summary>
        /// <param name="source">Исходная Dc модель калькулятора продукта</param>
        /// <returns>API модель калькулятора продукта</returns>
        public static InvoiceProductCalculatorApiModel CreateFrom(InvoiceProductCalculatorDc source)
        {
            if (source == null) return null;
            var res = new InvoiceProductCalculatorApiModel
            {
                Description = source.Description,
                Identifier = source.Identifier.ToString(),
                IsActive = source.IsActive,
                Name = source.Name,
                PayPeriod = source.PayPeriod,
                Quantity = source.Quantity,
                Rate = source.Rate,
                TotalBonus = source.TotalBonus,
                TotalPrice = source.TotalPrice,
                QuantityControl = InvoiceCalculatorControlApiModel.CreateFrom(source.QuantityControl)
            };
            return res;
        }
    }

    /// <summary>
    /// API модель контроллера значений поля
    /// </summary>
    public class InvoiceCalculatorControlApiModel
    {

        /// <summary>
        /// Тип контроллера
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Настройки контроллера
        /// </summary>
        public List<KeyValuePairApiModel<string, string>> Settings { get; set; }
            = [];

        /// <summary>
        /// Создать из Dc модели
        /// </summary>
        /// <param name="source">Исходная Dc модель</param>
        /// <returns>Api модель</returns>
        public static InvoiceCalculatorControlApiModel CreateFrom(CalculatorControlDto source)
        {
            if (source == null) return null;
            return new InvoiceCalculatorControlApiModel
            {
                Type = source.Type,
                Settings = source.Settings?
                    .Select(KeyValuePairApiModel<string, string>.CreateFrom)
                    .ToList()
            };
        }
    }

    /// <summary>
    /// API модель пары Ключ-Значени
    /// </summary>
    /// <remarks>По сути - обычный KeyValuePair только ссылочного типа. Нужен просто для корректной XML сериализации</remarks>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    /// <typeparam name="TValue">Тип значения</typeparam>
    public class KeyValuePairApiModel<TKey, TValue>
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        /// Создать из KeyValuePair
        /// </summary>
        /// <param name="source">Исходный KeyValue</param>
        /// <returns>API модель пары Ключ-Значение </returns>
        public static KeyValuePairApiModel<TKey, TValue> CreateFrom(KeyValuePair<TKey, TValue> source)
        {
            return new KeyValuePairApiModel<TKey, TValue>
            {
                Key = source.Key,
                Value = source.Value
            };
        }
    }
}