﻿using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums.Billing;

namespace Core42.Models.Invoices.Calculator
{
    /// <summary>
    /// API модель запроса не перерасчет инвойса
    /// </summary>
    public class RecalculateInvoiceRequestApiModel
    {
        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Продукты инвойса
        /// </summary>
        public List<RecalculateInvoiceProductRequestApiModel> Products { get; set; }
            = [];


        /// <summary>
        /// Конвертировать в DC модель запроса на перерасчет инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого запрашивается перерасчет</param>
        /// <param name="invoicePurposeType">Тип инвойса</param>
        /// <returns>DC модель запроса на перерасчет инвойса</returns>
        public RecalculateInvoiceDto ToRecalculateInvoiceDc(
            Guid accountId,
            InvoicePurposeType invoicePurposeType)
        {
            return new RecalculateInvoiceDto
            {
                AccountId = accountId,
                InvoicePurposeType = invoicePurposeType,
                PayPeriod = PayPeriod,
                Products = Products.Select(x => x.ToRecalculateInvoiceProductDc())
                    .ToList()
            };
        }
    }

    /// <summary>
    /// API модель продукта в запросе на переасчет инвойса
    /// </summary>
    public class RecalculateInvoiceProductRequestApiModel
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public string Identifier { get; set; }


        /// <summary>
        /// Признак того включен ли продукт в инвойс
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }


        /// <summary>
        /// Конвертировать в DC модель продукта в запросе на перерасчет инвойса
        /// </summary>
        /// <returns>DC модель продукта запроса на перерасчет инвойса</returns>
        public RecalculateInvoiceProductCalculatorDc ToRecalculateInvoiceProductDc()
        {
            return new RecalculateInvoiceProductCalculatorDc
            {
                Identifier = Identifier,
                IsActive = IsActive,
                Quantity = Quantity
            };
        }
    }
}