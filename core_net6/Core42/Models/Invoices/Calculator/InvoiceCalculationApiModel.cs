﻿using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums.Billing;

namespace Core42.Models.Invoices.Calculator
{
    /// <summary>
    /// API модель расчета инвойса
    /// </summary>
    public class InvoiceCalculationApiModel
    {
        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Расчеты продуктов инвойса
        /// </summary>
        public List<InvoiceProductCalculationApiModel> Products { get; set; }
            = [];

        /// <summary>
        /// Итого до НДС
        /// </summary>
        public decimal TotalBeforeVAT { get; set; }

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Итого после НДС
        /// </summary>
        public decimal TotalAfterVAT { get; set; }

        /// <summary>
        /// Итого бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        public Guid? AccountAdditionalCompanyId { get; set; }

        /// <summary>
        /// Создать из Dc модели калькулятора
        /// </summary>
        /// <param name="source">Исходная Dc модель калькулятора инвойса</param>
        /// <returns>API модель расчета инвойса</returns>
        public static InvoiceCalculationApiModel CreateFrom(InvoiceCalculatorDto source)
        {
            return new InvoiceCalculationApiModel
            {
                PayPeriod = source.PayPeriod,
                Products = source.Products.Select(InvoiceProductCalculationApiModel.CreateFrom).ToList(),
                TotalAfterVAT = source.TotalAfterVAT,
                TotalBeforeVAT = source.TotalBeforeVAT,
                TotalBonus = source.TotalBonus,
                VAT = source.VAT
            };
        }

        /// <summary>
        /// Конвертировать в Dc модель калькуляции инвойса
        /// Неактивные продукты будут проигнорированы
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого создается Dc модель</param>
        /// <param name="invoicePurposeType">Тип инвойса для которго создается Dc модель</param>
        /// <returns></returns>
        public InvoiceCalculationDto ToInvoiceCalculationDc(Guid accountId,
            InvoicePurposeType invoicePurposeType)
        {
            return new InvoiceCalculationDto
            {
                AccountId = accountId,
                InvoicePurposeType = invoicePurposeType,
                PayPeriod = PayPeriod,
                Products = Products
                    .Where(x => x.IsActive)
                    .Select(x => x.ToInvoiceProductCalculationDc())
                    .ToList(),
                TotalAfterVAT = TotalAfterVAT,
                TotalBeforeVAT = TotalBeforeVAT,
                TotalBonus = TotalBonus,
                VAT = VAT,
                AccountAdditionalCompanyId = AccountAdditionalCompanyId
            };
        }
    }

    /// <summary>
    /// API модель расчета продукта инвойса
    /// </summary>
    public class InvoiceProductCalculationApiModel
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Активен ли продукт
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Стоимость одной единицы продукта
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Общая стоимость указанного количества продукта для выбранного периода
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Общая сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Создать из DC модели калькулятора продукта
        /// </summary>
        /// <param name="source">Исходная Dc модель калькулятора продукта</param>
        /// <returns>API модель расчета продукта</returns>
        public static InvoiceProductCalculationApiModel CreateFrom(InvoiceProductCalculatorDc source)
        {
            return new InvoiceProductCalculationApiModel
            {
                TotalBonus = source.TotalBonus,
                Identifier = source.Identifier,
                PayPeriod = source.PayPeriod,
                Quantity = source.Quantity,
                Rate = source.Rate,
                TotalPrice = source.TotalPrice,
                IsActive = source.IsActive
            };
        }

        /// <summary>
        /// Конвертировать в Dc модель расчета продукта инвойса
        /// </summary>
        /// <returns>Dc модель расчета продукта инвойса</returns>
        public InvoiceProductCalculationDc ToInvoiceProductCalculationDc()
        {
            return new InvoiceProductCalculationDc
            {
                Quantity = Quantity,
                Identifier = Identifier,
                PayPeriod = PayPeriod,
                Rate = Rate,
                TotalBonus = TotalBonus,
                TotalPrice = TotalPrice
            };
        }
    }
}
