﻿using System;
using Clouds42.DataContracts.Billing.Inovice;

namespace Core42.Models.Invoices
{
    /// <summary>
    /// API модель инвойса на произвольную сумму
    /// </summary>
    public class ArbitraryAmountInvoiceApiModel
    {
        /// <summary>
        /// Описание услуги
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сумма инвойса
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Конвертировать в DC модель запроса на создание инвойса
        /// </summary>
        /// <param name="accountId">Id Аккаунта для которого создается запрос</param>
        /// <returns>DataContract модель запроса на создание инвойса</returns>
        public CreateArbitraryAmountInvoiceDto ToCreateArbitraryAmountInvoiceDc(Guid accountId)
        {
            return new CreateArbitraryAmountInvoiceDto
            {
                AccountId = accountId,
                Amount = Amount,
                Description = Description
            };
        }
    }
}