﻿using System;

namespace Core42.Models
{
    /// <summary>
    /// Параметр анотации
    /// </summary>
    public class ParameterAnnotation
    {
        /// <summary>
        /// Атрибут
        /// </summary>
        public Attribute AnnotationAttribute { get; set; }

        /// <summary>
        /// Документация(описание)
        /// </summary>
        public string Documentation { get; set; }
    }
}