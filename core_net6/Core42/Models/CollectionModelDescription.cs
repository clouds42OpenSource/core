﻿namespace Core42.Models
{
    /// <summary>
    /// Описание модели коллекции
    /// </summary>
    public class CollectionModelDescription : ModelDescription
    {
        /// <summary>
        /// Элемент коллекции
        /// </summary>
        public ModelDescription ElementDescription { get; set; }
    }
}