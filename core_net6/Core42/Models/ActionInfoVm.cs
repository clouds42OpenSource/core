﻿using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Core42.Models
{
    /// <summary>
    /// Метод контроллера
    /// </summary>
    public class ActionInfoVm
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Название метода
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание метода
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public string MethodType { get; set; }

        /// <summary>
        /// Относительный путь
        /// </summary>
        public string RelativePath { get; set; }

        /// <summary>
        /// Описание входящих параметров
        /// </summary>
        public List<ParameterDescriptionVm> RequestParametersDescription { get; set; } = [];

        /// <summary>
        /// Описание исходящего параметра
        /// </summary>
        public ParameterDescriptionVm ResponseParameterDescription { get; set; }

        /// <summary>
        /// Пример запроса
        /// </summary>
        public Dictionary<MediaTypeHeaderValue, object> SampleRequests { get; private set; } = new();

        /// <summary>
        /// Пример ответа
        /// </summary>
        public Dictionary<MediaTypeHeaderValue, object> SampleResponses { get; private set; } = new();
    }
}