﻿using System;
using Microsoft.AspNetCore.Http;

namespace Core42.Models
{
    public class CloudFileViewModel
    {
        public IFormFile Base { get; set; }

        public Guid Id { get; set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Хэш данные
        /// </summary>
        public string Base64 { get; set; }

        /// <summary>
        /// Имя файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Данные файла
        /// </summary>
        public byte[] Bytes { get; set; }

        /// <summary>
        /// Файл удален
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
