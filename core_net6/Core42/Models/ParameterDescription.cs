﻿using System.Collections.Generic;

namespace Core42.Models
{
    /// <summary>
    /// Описание параметра
    /// </summary>
    public class ParameterDescription
    {
        /// <summary>
        /// Анотации
        /// </summary>
        public List<ParameterAnnotation> Annotations { get; private set; } = [];

        /// <summary>
        /// Документация
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание типа модели
        /// </summary>
        public ModelDescription TypeDescription { get; set; }
    }
}