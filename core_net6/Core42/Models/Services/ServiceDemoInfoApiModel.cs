﻿using System;

namespace Core42.Models.Services
{
    /// <summary>
    /// Информация о демо сервиса
    /// </summary>
    public class ServiceDemoInfoApiModel
    {
        /// <summary>
        /// Включён ли демо период
        /// </summary>
        public bool IsEnabled { get; set; }
        
        /// <summary>
        /// Дата завершения демо периода
        /// </summary>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Истёк ли срок демо периода
        /// </summary>
        public bool IsExpired { get; set; }
    }

}
