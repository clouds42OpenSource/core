﻿using System;
using Clouds42.Domain.Enums;

namespace Core42.Models.Services
{
    /// <summary>
    /// Структура содержащая информацию об опции сервиса
    /// </summary>
    public class ServiceOptionInfoApiModel
    {
        /// <summary>
        /// Id опции сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// ID пользователя или ID аккаунта, которому назначен ресурс
        /// </summary>
        public Guid Subject { get; set; }
    }

}
