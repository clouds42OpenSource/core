﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.ResourceConfiguration;

namespace Core42.Models.Services
{
    /// <summary>
    /// Информация о сервисе аккаунта
    /// </summary>
    public class AccountServiceInfoApiModel
    {
        /// <summary>
        /// Нужна ли оплата за главный сервис, такой как аренда 1С,...
        /// </summary>
        public bool IsMainServiceNeedPayment { get; set; }

        /// <summary>
        /// Активен ли сервис
        /// </summary>
        public bool IsActive { get; set; }
        
        /// <summary>
        /// Отключён ли сервис
        /// </summary>
        public bool IsDisabled { get; set; }
        
        /// <summary>
        /// Дата завершения оплаченного периода за сервис
        /// </summary>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Информация о демо
        /// </summary>
        public ServiceDemoInfoApiModel Demo { get; set; }

        /// <summary>
        /// Список включённых опций 
        /// </summary>
        public List<ServiceOptionInfoApiModel> Options { get; set; }

        /// <summary>
        /// Создать модель для ответа на получение информации о сервисе аккаунта
        /// </summary>
        /// <param name="accountService">Информация о сервисе аккаунта</param>
        /// <returns>Модель для ответа на получение информации о сервисе аккаунта</returns>
        public static AccountServiceInfoApiModel CreateFrom(AccountServiceDto accountService)
        {
            if(accountService == null)
            {
                return new AccountServiceInfoApiModel
                {
                    Demo = new ServiceDemoInfoApiModel(),
                    Options = []
                };
            }

            var options = new List<ServiceOptionInfoApiModel>();
            foreach(var option in accountService.Options)
            {
                options.Add(new ServiceOptionInfoApiModel
                {
                    Id = option.Id,
                    BillingType = option.BillingType,
                    Subject = option.Subject
                });
            }

            return new AccountServiceInfoApiModel
            {
                IsActive = accountService.IsActive,
                ExpiresOn = accountService.ExpiresOn,
                IsDisabled = accountService.IsDisabled,
                IsMainServiceNeedPayment = accountService.IsMainServiceNeedPayment,
                Demo = new ServiceDemoInfoApiModel
                {
                    ExpiresOn = accountService.Demo.ExpiresOn,
                    IsEnabled = accountService.Demo.IsEnabled,
                    IsExpired = accountService.Demo.IsExpired
                },
                Options = options
            };
        }
    }

}
