﻿using Clouds42.DataContracts.Account.Locale;

namespace Core42.Models.Locale
{
    /// <summary>
    /// API деталей валюты
    /// </summary>
    public class CurrencyApiModel
    {
        /// <summary>
        /// Код валюты
        /// </summary>
        public int? CurrencyCode { get; set; }

        /// <summary>
        /// Имя валюты
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Создать API модель с DC модели
        /// </summary>
        /// <param name="src">Исходная DC модель</param>
        /// <returns>API модель</returns>
        public static CurrencyApiModel CreateFrom(CurrencyDto src)
        {
            if (src == null) return null;
            return new CurrencyApiModel
            {
                Currency = src.Currency,
                CurrencyCode = src.CurrencyCode
            };
        }
    }
}