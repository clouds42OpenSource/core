﻿using System.Collections.ObjectModel;

namespace Core42.Models
{
    /// <summary>
    /// Описание модели сложного типа
    /// </summary>
    public class ComplexTypeModelDescription : ModelDescription
    {
        /// <summary>
        /// Поля модели
        /// </summary>
        public Collection<ParameterDescription> Properties { get; private set; } = [];
    }
}