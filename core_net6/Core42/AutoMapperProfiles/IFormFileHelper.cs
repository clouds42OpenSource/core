﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace Core42.AutoMapperProfiles;

public static class IFormFileHelper
{
    public static byte[] GetBytes(this IFormFile file)
    {
        using var stream = new MemoryStream((int)file.Length);
        file.CopyTo(stream);
        return stream.ToArray();
    }

    public static string GetBase64String(this IFormFile file)
    {
        using var ms = new MemoryStream();
        file.CopyTo(ms);
        var fileBytes = ms.ToArray();
        return Convert.ToBase64String(fileBytes);
    }
}
