﻿using System;
using AutoMapper;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Microsoft.AspNetCore.Http;

namespace Core42.AutoMapperProfiles;

[Obsolete("Класть мапперы рядом с местом, где используются или использовать IMapFrom")]
public class CloudFileInputDtoProfile : Profile
{
    public CloudFileInputDtoProfile()
    {
        CreateMap<CloudFileDataDto<IFormFile>, CloudFileDataDto<byte[]>>()
            .ForMember(output => output.Content,
                mapper => mapper.MapFrom(input => input.Content.GetBytes()))
            .ForMember(output => output.Base64,
                mapper => mapper.MapFrom(input => input.Content.GetBase64String()));
    }


}
