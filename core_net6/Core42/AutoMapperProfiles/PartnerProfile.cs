using System;
using AutoMapper;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.GoogleCloud.SpreadSheet.Models;

namespace Core42.AutoMapperProfiles;

public class PartnerProfile : Profile
{
    [Obsolete("Класть мапперы рядом с местом, где используются или использовать IMapFrom")]
    public PartnerProfile()
    {
        CreateMap<PartnerClientSheetDto, PartnerClientDto>()
            .ForMember(dest => dest.PartnerAccountId, opt => opt.Ignore())
            .ForMember(dest => dest.AccountId, opt => opt.Ignore())
            .ForMember(dest => dest.PartnerAccountCaption, opt => opt.Ignore())
            .ForMember(dest => dest.AccountIndexNumber, opt => opt.Ignore())
            .ForMember(dest => dest.IsVipAccount, opt => opt.Ignore())
            .ReverseMap()
            .ForMember(dest => dest.ClientActivationDate, opt => opt.MapFrom(src => src.ClientActivationDate.ToDateOnlyString()))
            .ForMember(dest => dest.ServiceExpireDate, opt => opt.MapFrom(src => src.ServiceExpireDate.ToDateOnlyString()))
            .ForMember(dest => dest.ServiceIsActiveForClient, opt => opt.MapFrom(src => PartnerClientSheetDtoExtensions.MapServiceIsActive(src.ServiceIsActiveForClient)))
            .ForMember(dest => dest.IsDemoPeriod, opt => opt.MapFrom(src => PartnerClientSheetDtoExtensions.MapDemoPeriod(src.IsDemoPeriod)))
            .ForMember(dest => dest.TypeOfPartnership, opt => opt.MapFrom(src => src.TypeOfPartnership.ToString()))
            .ForMember(dest => dest.TypeOfPartnership, opt => opt.MapFrom(src => PartnerClientSheetDtoExtensions.MapClientType(src.TypeOfPartnership)));


        CreateMap<PartnersClientFilterDto, GoogleSheetDto>()
            .ForMember(dest => dest.AccountId,
                opt => opt.MapFrom(src => src.AccountId))
            .ForMember(dest => dest.PeriodFrom,
                opt => opt.MapFrom(src => src.PeriodFrom))
            .ForMember(dest => dest.PeriodTo,
                opt => opt.MapFrom(src => src.PeriodTo))
            .ForMember(dest => dest.SheetType,
                opt => opt.MapFrom(src => SheetType.PartnerClients))
            .ReverseMap();
    }
}
