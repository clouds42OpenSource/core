﻿using System;
using AutoMapper;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Core42.AutoMapperProfiles 
{
    [Obsolete("Класть мапперы рядом с местом, где используются или использовать IMapFrom")]
    public class CreateAgentRequisitesProfile : Profile
    {
        public CreateAgentRequisitesProfile()
        {
            CreateMap<CreateAgentRequisitesInputDto, CreateAgentRequisitesDto>();
            CreateMap<LegalPersonRequisitesInputDto, LegalPersonRequisitesDto>();
            CreateMap<PhysicalPersonRequisitesInputDto, PhysicalPersonRequisitesDto>();
            CreateMap<SoleProprietorRequisitesInputDto, SoleProprietorRequisitesDto>();

            CreateMap<CreateAgentCashOutRequestInfoDto, CreateAgentCashOutRequestDto>()
                .ForMember(x => x.PaySum, z => z.MapFrom(y => y.PaySumDecimal))
                .ForMember(x => x.TotalSum, z => z.MapFrom(y => y.TotalSumDecimal));

            CreateMap<EditAgentCashOutRequestInputDto, EditAgentCashOutRequestDto>()
                .ForMember(x => x.PaySum, z => z.MapFrom(y => y.PaySumDecimal))
                .ForMember(x => x.TotalSum, z => z.MapFrom(y => y.TotalSumDecimal));

            CreateMap<CreateAgentCashOutRequestInputDto, CreateAgentCashOutRequestDto>();
            CreateMap<EditAgentRequisitesInputDto, EditAgentRequisitesDto>();
        }
    }
}
