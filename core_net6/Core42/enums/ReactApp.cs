﻿using Clouds42.Domain.Attributes;

namespace Core42.enums
{
    /// <summary>
    /// Представляет виды React приложений
    /// </summary>
    public enum ReactApp
    {
        /// <summary>
        /// ЛК42
        /// </summary>
        [StringValue("cp42")]
        Spa,

        /// <summary>
        /// Маркет42
        /// </summary>
        [StringValue("market42")]
        Market
    }
}
