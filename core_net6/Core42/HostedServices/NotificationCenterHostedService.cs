﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clouds42.CoreWorkerTask;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.SignalR.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.HostedServices
{
    public class NotificationCenterHostedService(
        IHubContext<NotificationHub,
        INotificationClient> hubContext,
        IMapper mapper,
        ILogger42 logger,
        IServiceProvider serviceProvider,
        IConfiguration configuration,
        IDbObjectLockProvider dbObjectLockProvider)
        : BackgroundService
    {
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var timer = new PeriodicTimer(TimeSpan.FromSeconds(int.Parse(configuration.GetSection("HostedService:Period").Value)));

            while (await timer.WaitForNextTickAsync(stoppingToken))
            {
                try
                {
                    using var scope = serviceProvider.CreateScope();
                    var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                    using (await dbObjectLockProvider.AcquireLockAsync(nameof(NotificationCenterHostedService),
                               TimeSpan.FromSeconds(10)))
                    {
                        var openedConnections = (await unitOfWork.ConnectionRepository
                                .AsQueryableNoTracking()
                                .Where(x => x.Connected)
                                .ToListAsync(stoppingToken))
                            .ToList();

                        var createdNotifications = await unitOfWork.NotificationRepository
                            .AsQueryableNoTracking()
                            .Where(x => !x.IsDeleted && !x.IsRead && !x.IsSent)
                            .ToListAsync(stoppingToken);

                        if (!createdNotifications.Any())
                        {
                            continue;
                        }

                        openedConnections.GroupJoin(createdNotifications, x => x.AccountUserId, z => z.AccountUserId,
                                (connection, notifications) => (connection, notifications))
                            .ToList()
                            .ForEach(x =>
                            {
                                if (x.notifications.Any())
                                {
                                    x.notifications.ToList().ForEach(async z =>
                                    {
                                        await hubContext.Clients.Client(x.connection.ConnectionId)
                                            .Broadcast(mapper.Map<NotificationDto>(z));

                                        z.IsSent = true;
                                    });
                                }
                            });

                        await unitOfWork.BulkUpdateAsync(createdNotifications.Where(x => x.IsSent));
                    }
                }

                catch (Exception ex)
                {
                    logger.Error(ex, "Ошибка отправки сообщений пользователям в центр уведомлений");
                }
            }
        }
    }
}
