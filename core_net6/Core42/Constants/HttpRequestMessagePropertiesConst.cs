﻿namespace Core42.Constants
{
    /// <summary>
    /// Константы полей Http контекста
    /// </summary>
    public static class HttpRequestMessagePropertiesConst
    {
        /// <summary>
        /// MS_HttpContext
        /// </summary>
        public const string MsHttpContext = "MS_HttpContext";

        /// <summary>
        /// Тип размещения - Вложение
        /// </summary>
        public const string DispositionTypeAttachment = "attachment";

        /// <summary>
        /// Название заголовка от Nginx с оригинальным IP адресом клиента
        /// </summary>
        public const string NginxOriginalIpAddressHeaderName = "x-forwarded-for";
    }
}