﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Clouds42.Common.Jwt;
using Clouds42.Jwt.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using AuthenticationSchemes = Core42.Authentication.Schemes.AuthenticationSchemes;

namespace Core42.Filters
{
    /// <summary>
    /// Фильтр для аутентификация через JsonWebToken
    /// </summary>
    public sealed class JwtAuthenticationFilterAttribute(IJwtWorker jwtWorker) : IAsyncAuthorizationFilter
    {
        /// <summary>
        /// Событие при обработке авторизации
        /// </summary>
        /// <param name="context">Контекст выполнения метода</param>
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {

            if (string.IsNullOrEmpty(context.HttpContext.Request.Headers.Authorization.ToString()) ||
                !context.HttpContext.Request.Headers.Authorization.ToString().Contains(AuthenticationSchemes.JwtBearer))
                return;

            var allowAnonymousAttributes = (context.ActionDescriptor as ControllerActionDescriptor)?.MethodInfo.GetCustomAttributes<AllowAnonymousAttribute>();

            if (allowAnonymousAttributes.Any())
                return;

            var jsonWebToken = context.HttpContext.Request.Headers.Authorization.ToString().Replace("Bearer ", string.Empty);
            
            try
            {
                var claimsPrincipal = await jwtWorker.ValidateJsonWebTokenAsync(jsonWebToken);

                context.HttpContext.User = claimsPrincipal;
            }

            catch (SecurityTokenExpiredException)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.HttpContext.Response.Headers.TryAdd(JsonWebTokenNotValidHttpHeader.TokenExpiredHeader, "true");
                context.Result = new UnauthorizedResult();
            }

            catch
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.HttpContext.Response.Headers.TryAdd(JsonWebTokenNotValidHttpHeader.InvalidTokenHeader, "true");
                context.Result = new UnauthorizedResult();
            }
        }
    }

}
