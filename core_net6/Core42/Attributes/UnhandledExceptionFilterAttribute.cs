﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Clouds42.Common.Extensions;
using Clouds42.Common.Jwt;
using Clouds42.HandlerExeption.Contract;
using Core42.Helpers.Exceptions;
using Core42.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Core42.Attributes
{
    /// <summary>
    /// Represents the an attribute that provides a filter for unhandled exceptions.
    /// </summary>
    public class UnhandledExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// Событие при возникновении исключения
        /// </summary>
        /// <param name="exceptionContext">Контекст выполненного действия</param>
        public override void OnException(ExceptionContext exceptionContext)
        {
            if (exceptionContext.Exception is OperationCanceledException)
                return;

            var handlerException = (IHandlerException)exceptionContext.HttpContext.RequestServices.GetService(typeof(IHandlerException));

            var statusCode = HttpStatusCodeHelper.GetHttpStatusCode(exceptionContext.Exception);

            if (statusCode == HttpStatusCode.Unauthorized && !exceptionContext.HttpContext.Response.Headers.ContainsKey(JsonWebTokenNotValidHttpHeader.TokenExpiredHeader))
                exceptionContext.HttpContext.Response.Headers.Add(JsonWebTokenNotValidHttpHeader.TokenExpiredHeader, "true");

            var errors = exceptionContext.Exception.MapException();

            if (exceptionContext.Exception is ValidationException exc)
            {
                errors = exc.MapException();
            }

            var errorModel = new ResponseResultDto<string>
            {
                Code = "400", Data = null, Success = false, Message = errors.LastOrDefault("Не удалось выполнить операцию")
            };

            var acceptXmlArray = new[] { "application/xml", "text/xml" };

            var acceptContainsOrEmpty = !exceptionContext.HttpContext.Request.Headers.Accept.Any() || exceptionContext.HttpContext.Request.Headers.Accept.Any(x => acceptXmlArray.Contains(x));

            exceptionContext.Result = new ContentResult
            {
                Content = acceptContainsOrEmpty ? errorModel.SerializeToXml() : JsonConvert.SerializeObject(errorModel, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }),
                StatusCode = statusCode.ToInt(),
                ContentType = acceptContainsOrEmpty ? "application/xml" : "application/json"
            };

            if(exceptionContext.Exception is not ValidationException)
                handlerException?.Handle(exceptionContext?.Exception, "[Необработанное исключение API]",$"User request: {Helpers.HttpContextHelper.ExtractTraceInfo(exceptionContext.HttpContext.Request)} :: errorModel :: {errorModel}");
        }
    }

    /// <summary>
    ///     Extensions for Exceptions
    /// </summary>
    public static class ExceptionExtensions
    {
        /// <summary>
        ///     Fluent validation exception map to Dictionary
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static List<string> MapException(this ValidationException ex)
        {
            return ex.Errors.Select(item => item.ErrorMessage).ToList();
        }

        /// <summary>
        ///     Exception map to Dictionary
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static List<string> MapException(this Exception ex)
        {
            return [ex.Message];
        }
    }
}
