﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Enums.Security;

namespace Core42.Attributes
{
    /// <summary>
    /// Атрибут для определения внешнего сервиса
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ExternalServiceAttribute(params ExternalServiceTypeEnum[] externalServiceType) : Attribute
    {
        /// <summary>
        /// Тип внешнего сервиса
        /// </summary>
        public List<ExternalServiceTypeEnum> ExternalServiceType { get; } = externalServiceType.ToList();
    }
}
