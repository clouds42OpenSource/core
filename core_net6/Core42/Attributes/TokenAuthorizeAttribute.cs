﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public sealed class TokenAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        var headerContainsToken = context.HttpContext.Request.Headers.ContainsKey("Token");
        var headerContainsAuth = context.HttpContext.Request.Headers.ContainsKey("Authorization");
        var headerContainsAccessTokenInCookies = context.HttpContext.Request.Cookies.ContainsKey("AccessToken");

        if (!headerContainsToken && !headerContainsAuth && !headerContainsAccessTokenInCookies)
        {
            context.Result = new UnauthorizedObjectResult(null);
        }

        else
        {
            var accessProvider = context.HttpContext.RequestServices.GetService<IAccessProvider>();

            var user = accessProvider.GetUser();
            if (user == null)
            {
                context.Result = new UnauthorizedObjectResult(null);
            }
        }
    }
}
