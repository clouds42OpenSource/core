﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Core42.RazorControls
{
    public static partial class RazorControl
    {
        public static UrlHelper HtmlActionLink(ActionContext context) => new(context);
    }
}
