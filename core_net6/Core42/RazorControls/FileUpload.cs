﻿using System;
using System.Linq.Expressions;
using Clouds42.Common.Extensions;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Core42.RazorControls
{
    public static class FileUploadRazor
    {
        public static HtmlString FileUpload(this IHtmlHelper htmlHelper, string name, CloudFileViewModel model)
        {
            var fileName = !string.IsNullOrEmpty(model.FileName) ? model.FileName : "Выберите файл";

            var button = "<button type=\"button\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i></button>";
            var label = $"<div>{fileName}</div>";
            var file = $"<input type=\"file\" name=\"{name}.Base\" id=\"{name.FieldNameToId()}_Base\"/>";
            var script = $"<script src=\"{ActionHelper.HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/controls/file.upload.min.js")}\"></script>";

            var container = $"<div class=\"file_upload\">{button}{label}{file}{script}</div>";

            return new HtmlString(container);
        }

        public static HtmlString FileUploadFor<TModel>(this IHtmlHelper<TModel> htmlHelper, Expression<Func<TModel, CloudFileViewModel>> expression, string acceptFormats = "")
        {
            var expressionProvider = new ModelExpressionProvider(htmlHelper.MetadataProvider);
            var fieldName = expressionProvider.GetExpressionText(expression);

            var metadata = expressionProvider.CreateModelExpression(htmlHelper.ViewData, expression);
            var model = (CloudFileViewModel)metadata.Model;

            var fileName = !string.IsNullOrEmpty(model.FileName) ? model.FileName : "Выберите файл";
            var formats = !string.IsNullOrEmpty(acceptFormats) ? $"accept=\"{acceptFormats}\"" : "";
            var button = "<button type=\"button\"><i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i></button>";
            var label = $"<div>{fileName}</div>";
            var file = $"<input type=\"file\" name=\"{fieldName}.Base\" id=\"{fieldName.FieldNameToId()}_Base\" {formats} />";
            var script = $"<script src=\"{ActionHelper.HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/controls/file.upload.js")}\"></script>";

            var container = $"<div class=\"file_upload\">{button}{label}{file}{script}</div>";

            return new HtmlString(container);
        }
    }
}
