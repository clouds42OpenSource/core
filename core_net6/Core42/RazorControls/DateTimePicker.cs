﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using System.Text.Encodings.Web;
using Clouds42.Common.Extensions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Core42.RazorControls
{
    public static partial class RazorControl
    {
        public static HtmlString DateTimePickerFor<TModel>(this IHtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime>> expression, string cssClass = null, bool? onlyDate = null, string dataBind = null, string placeholder = null)
        {
            var expressionProvider = new ModelExpressionProvider(htmlHelper.MetadataProvider);
            var fieldName = expressionProvider.GetExpressionText(expression);
            var fieldId = fieldName.FieldNameToId();
            var metadata = expressionProvider.CreateModelExpression(htmlHelper.ViewData, expression);
            var model = (DateTime)metadata.Model;

            var htmlAttrs = new Dictionary<string, object>
            {
                {"class", $"form-control {cssClass}"},
                {nameof(placeholder), placeholder}
            };

            if (!string.IsNullOrEmpty(dataBind))
                htmlAttrs.Add("data-bind", dataBind);

            var textBox = htmlHelper.TextBox(fieldName, model.ToString("dd.MM.yyyy HH:mm:ss"), htmlAttrs);
            var func = onlyDate.HasValue && onlyDate.Value
                ? $"DateTimePicker.Date('{fieldId}');"
                : $"DateTimePicker.DateTime('{fieldId}');";

            var dependencies = $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/moment.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/moment-with-locales.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/bootstrap-datetimepicker.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/controls/DateTimePickerControl.js")}\"></script>";

            var script = $"{dependencies}<script>$(document).ready(function() {{ {func} }});</script>";
            return new HtmlString(textBox.GetString() + script);
        }

        public static HtmlString DateTimePickerFor<TModel>(this IHtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime?>> expression, string cssClass = null, bool? onlyDate = null, string dataBind = null, string placeholder = null)
        {
            var expressionProvider = new ModelExpressionProvider(htmlHelper.MetadataProvider);
            var fieldName = expressionProvider.GetExpressionText(expression);
            var fieldId = fieldName.FieldNameToId();

            var metadata = expressionProvider.CreateModelExpression(htmlHelper.ViewData, expression);

            var model = (DateTime?)metadata.Model;

            var htmlAttrs = new Dictionary<string, object>
            {
                {"class", $"form-control {cssClass}"},
                {nameof(placeholder), placeholder}
            };

            if (!string.IsNullOrEmpty(dataBind))
                htmlAttrs.Add("data-bind", dataBind);

            var textBox = htmlHelper.TextBox(fieldName, model?.ToString("dd.MM.yyyy HH:mm:ss"), htmlAttrs);
            var func = onlyDate.HasValue && onlyDate.Value
                ? $"DateTimePicker.Date('{fieldId}');"
                : $"DateTimePicker.DateTime('{fieldId}');";

            var dependencies = $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/moment.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/moment-with-locales.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/bootstrap-datetimepicker.js")}\"></script>" +
                               $"<script src=\"{HtmlActionLink(htmlHelper.ViewContext).Content("~/Scripts/controls/DateTimePickerControl.js")}\"></script>";

            var script = $"{dependencies}<script>$(document).ready(function() {{ {func} }});</script>";
            return new HtmlString(textBox.GetString() + script);
        }

        public static HtmlString DateTimeRangeFor<TModel>(this IHtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, DateTime>> expressionForStart, Expression<Func<TModel, DateTime>> expressionForEnd,
            string label, string cssClass = null, bool onlyDate = false, string data_bind_1 = null, string data_bind_2 = null)
        {
            IHtmlContent labelFor = null;
            if (!string.IsNullOrEmpty(label))
            {
                labelFor = htmlHelper.LabelFor(expressionForStart, label, new { @class = "control-label" });
            }

            var textBox1For = htmlHelper.DateTimePickerFor(expressionForStart, $"{cssClass} first-child", onlyDate, data_bind_1);
            var textBox2For = htmlHelper.DateTimePickerFor(expressionForEnd, $"{cssClass} last-child", onlyDate, data_bind_2);

            var stringBuilder = new StringBuilder();
            if (labelFor != null)
            {
                stringBuilder.Append(labelFor.GetString());
            }
            stringBuilder.Append("<div class=\"input-group\">");
            stringBuilder.Append(textBox1For.GetString());
            stringBuilder.Append("<span class=\"input-group-addon\">ДО</span>");
            stringBuilder.Append(textBox2For.GetString());
            stringBuilder.Append("</div>");

            return new HtmlString($"<div class=\"form-group\">{stringBuilder}</div>");
        }

        public static string GetString(this IHtmlContent content)
        {
            using var writer = new StringWriter();
            content.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }
    }
}
