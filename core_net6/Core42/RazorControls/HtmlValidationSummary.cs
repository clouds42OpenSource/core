﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Core42.RazorControls
{
    /// <summary>
    /// Контрол Razor для отображения элементов
    /// </summary>
    public static partial class RazorControl
    {
        /// <summary>
        /// Контрол отображения общей информации валидации
        /// с элементами html
        /// </summary>
        /// <param name="htmlHelper">Html хелпер</param>
        /// <returns>Общая информация валидации</returns>
        public static IHtmlContent HtmlValidationSummary(this IHtmlHelper htmlHelper)
        {

            var result = htmlHelper.Raw(System.Net.WebUtility.HtmlDecode(htmlHelper.ValidationSummary().ToString()));
            return result;
        }

    }
}
