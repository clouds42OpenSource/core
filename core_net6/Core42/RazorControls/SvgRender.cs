﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Core42.RazorControls
{
    public static class SvgRenderControl
    {
        /// <summary>
        ///     Отрендерить файл SVG
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="path">Виртуальный путь к файлу</param>
        /// <param name="appendUniqId">Добавить уникальный идентификатор</param>
        public static HtmlString SvgRender(this IHtmlHelper htmlHelper, string path, bool appendUniqId = true)
        {
            try
            {
                var svgFile = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path.TrimStart('~', '/', '\\')));

                if (!appendUniqId)
                    return new HtmlString(svgFile);

                var currentUniqId = Regex.Match(svgFile, "(uniq=\")\\S*\"").Value.Replace("uniq=\"", "").TrimEnd('"');
                if (string.IsNullOrEmpty(currentUniqId))
                    return null;

                var renderContent = svgFile.Replace(currentUniqId, $"SVG-{Guid.NewGuid():N}-{currentUniqId}");
                return new HtmlString(renderContent);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
