﻿
//Get COOKIE by name
getCookie = function (name) {
    var prefix = name + "=";
    var cookieStartIndex = document.cookie.indexOf(prefix);
    if (cookieStartIndex == -1)
        return null;
    var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
    if (cookieEndIndex == -1)
        cookieEndIndex = document.cookie.length;
    return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
};

// Delete COOKIE by name
deleteCookie = function (name) {
    if (getCookie(name))
        document.cookie = name + "=" + ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
};

changeLangScript = function (script, id, lang) {
    var src = "https://42clouds.com/Stripe/ChangeLang?lang=" + lang;
    script.id = id;
    script.type = "text/javascript";
    script.src = src;
    document.getElementsByTagName("head")[0].appendChild(script);
};

jQuery(document).ready(function () {

    jQuery('#stripe').html(html);

    var currentLang;
    var script = document.createElement("script");

    jQuery('a[data-lang]').click(function () {

        currentLang = jQuery(this).attr('data-lang');
        if (jQuery('#changeLang') !== [])
            jQuery('#changeLang').remove();
        changeLangScript(script, "changeLang", currentLang);

        document.cookie = "flag=" + currentLang + ";path=/;";

    });

    if (getCookie("flag") === 'be')
        jQuery('#choosed_lang').addClass('flag ' + getCookie("flag"));
    else if (getCookie("lang") != null)
        jQuery('#choosed_lang').addClass('flag ' + getCookie("lang"));
    else
        jQuery('#choosed_lang').addClass('flag ru');

    var cl_tmp = jQuery('#choosed_lang').attr('class');

    if (cl_tmp != null)
        var classes = cl_tmp.split(' ');

    jQuery('.mmore').hover(function () {
        jQuery(this).addClass('active').find("ul").show();
    }, function () {
        jQuery(this).removeClass('active').find("ul").hide();
    });

});

/* Edit locate path*/
function editLocatePath(element) {
    window.location.pathname = jQuery(element).attr('locate');
    
};