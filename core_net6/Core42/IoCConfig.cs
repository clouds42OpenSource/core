﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Configurations;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Jwt.Contracts;
using Clouds42.Logger;
using Core42.Authentication.Filters;
using Core42.Authentication.Handlers;
using Core42.Authentication.Schemes;
using Core42.Configurations;
using Core42.Helpers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Core42;

public static class IoCConfig
{
    public static void AddStaticInitializers(this IApplicationBuilder builder, IConfiguration configuration)
    {
        Logger42.Init(builder.ApplicationServices);
        HandlerException42.Init(builder.ApplicationServices);
        ConfigurationHelper.SetConfiguration(configuration);
        CacheHelper.Configure(builder.ApplicationServices.GetRequiredService<IMemoryCache>());
    }

    public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddAuthentication(option =>
            {
                option.DefaultScheme = AuthenticationSchemes.Multiple;
                option.DefaultChallengeScheme = AuthenticationSchemes.Multiple;
            })
            .AddJwtBearer(async options =>
            {
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        _ = AuthenticationHeaderValue.TryParse(context.Request.Headers.Authorization.FirstOrDefault(),
                            out var headerValue);

                        context.Token = context.Request.Cookies.ContainsKey("AccessToken")
                            ? context.Request.Cookies["AccessToken"]
                            : headerValue?.Parameter;

                        return Task.CompletedTask;
                    }
                };
                options.RequireHttpsMetadata = true;
                options.SaveToken = true;
                options.TokenValidationParameters = await services
                    .BuildServiceProvider()
                    .GetRequiredService<IJwtHelper>()
                    .GeTokenValidationParameters();
            })
            .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>(AuthenticationSchemes.Basic, null)
            .AddPolicyScheme(AuthenticationSchemes.Multiple, AuthenticationSchemes.Multiple, options =>
            {
                options.ForwardDefaultSelector = context =>
                {
                    string authorization = context.Request.Headers[HeaderNames.Authorization];

                    return !string.IsNullOrEmpty(authorization) &&
                           authorization.StartsWith(AuthenticationSchemes.Basic)
                        ? AuthenticationSchemes.Basic
                        : AuthenticationSchemes.JwtBearer;
                };
            });

        return services;
    }
        
    public static IApplicationBuilder UseOpenFileServer(this IApplicationBuilder builder,
        IConfiguration configuration)
    {
        var settingsPath = configuration.GetSection("FileSettings:PhysicalPath").Value;
        var requestPath = configuration.GetSection("FileSettings:RequestPath").Value;

        var physicalPath = Path.Combine(Directory.GetCurrentDirectory(), settingsPath ?? "StaticFiles");

        if (!Directory.Exists(physicalPath))
        {
            Directory.CreateDirectory(physicalPath);
        }

        builder.UseFileServer(new FileServerOptions
        {
            FileProvider = new PhysicalFileProvider(physicalPath),
            RequestPath = requestPath ?? "",
            EnableDirectoryBrowsing = true
        });

        return builder;
    }

    public static IServiceCollection AddApiVersioning(this IServiceCollection services)
    {
        services
            .AddApiVersioning(o =>
            {
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            })
            .AddApiExplorer(o =>
            {
                o.GroupNameFormat = "'v'VVV";
                o.SubstituteApiVersionInUrl = true;
            });

        return services;
    }

    public static IServiceCollection AddSwaggerGen(this IServiceCollection services)
    {
        services.AddSwaggerGenNewtonsoftSupport()
            .AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerGenOptions>()
            .AddSwaggerGen(o =>
            {
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                o.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

                o.DescribeAllParametersInCamelCase();

                o.AddSecurityDefinition(AuthenticationSchemes.JwtBearer, new OpenApiSecurityScheme
                {
                    Name = "Bearer authorization in header",
                    Type = SecuritySchemeType.Http,
                    Scheme = AuthenticationSchemes.JwtBearer,
                    Description = "Input your JWT-bearer key to access this API",
                    In = ParameterLocation.Header
                });

                o.AddSecurityDefinition(AuthenticationSchemes.Basic, new OpenApiSecurityScheme
                {
                    Name = "Basic authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = AuthenticationSchemes.Basic,
                    Description = "Input your username and password to access this API",
                    In = ParameterLocation.Header
                });

                o.AddSecurityDefinition(AuthenticationSchemes.Token, new OpenApiSecurityScheme
                {
                    Name = "Token authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = AuthenticationSchemes.Token,
                    Description = "Input your token to access this API",
                    In = ParameterLocation.Header
                });

                o.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = AuthenticationSchemes.JwtBearer
                            }
                        },
                        Array.Empty<string>()
                    },
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = AuthenticationSchemes.Basic
                            }
                        },
                        Array.Empty<string>()
                    },
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = AuthenticationSchemes.Token
                            }
                        },
                        Array.Empty<string>()
                    }
                });

                o.CustomSchemaIds(type => type.ToString());
                o.OperationFilter<SecurityRequirementsOperationFilter>();
            });

        return services;
    }
}
