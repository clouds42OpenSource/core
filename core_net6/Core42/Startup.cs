﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using Asp.Versioning.ApiExplorer;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Common.Converters;
using Clouds42.CoreWorkerTask;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Clouds42.Jwt.Contracts;
using Clouds42.TelegramBot;
using Clouds42.TelegramBot.Interfaces;
using Core42.Application.Features.NotificationContext.SignalR.Hubs;
using Core42.Attributes;
using Core42.Authentication;
using Core42.Filters;
using Core42.Helpers;
using Core42.HostedServices;
using GCP.Extensions.Configuration.SecretManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using StackExchange.Profiling.Storage;
using Swashbuckle.AspNetCore.SwaggerUI;
using AuthenticationSchemes = Core42.Authentication.Schemes.AuthenticationSchemes;

namespace Core42
{
    public class Startup(IWebHostEnvironment env)
    {
        public IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true)
            .AddGcpKeyValueSecrets(env.EnvironmentName)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddEnvironmentVariables()
            .Build();

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCors(options =>
                {
                    options.AddDefaultPolicy(builder =>
                        builder.SetIsOriginAllowed(_ => true)
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials());
                })
                .AddSingleton(Configuration)
                .AddHttpClient<IisHttpClient>()
                .ConfigurePrimaryHttpMessageHandler(() =>
                {
                    var login = Configuration.GetRequiredSection("IISConfiguration:Login")?.Value;
                    var password = Configuration.GetRequiredSection("IISConfiguration:Password")?.Value;
                    var domain = Configuration.GetRequiredSection("IISConfiguration:Domain")?.Value;

                    return new HttpClientHandler
                    {
                        UseDefaultCredentials = false,
                        ServerCertificateCustomValidationCallback =
                            HttpClientHandler.DangerousAcceptAnyServerCertificateValidator,
                        Credentials = new NetworkCredential(login, password, domain),
                        PreAuthenticate = true
                    };
                });

            services
                .AddClouds42DbContext(Configuration)
                .AddServices(Configuration)
                .AddHostedService<NotificationCenterHostedService>()
                .AddDistributedIsolatedExecutor(new DbObjectLockProvider(Configuration.GetConnectionString("DefaultConnection"), Configuration.GetSection("ConnectionStrings:DatabaseType").Value))
                .AddScoped<UnhandledExceptionFilterAttribute>()
                .AddHttpContextAccessor()
                .AddSerilogWithElastic(Configuration)
                .AddTelegramBots(Configuration)
                .AddAutoMapper(Assembly.GetExecutingAssembly())
                .AddDistributedMemoryCache()
                .AddSession(options =>
                {
                    options.IdleTimeout = TimeSpan.FromMinutes(60);
                })
                .Configure<ApiBehaviorOptions>(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                    options.SuppressInferBindingSourcesForParameters = true;
                });
            
               services.AddControllers(options =>
                {
                    options.SuppressAsyncSuffixInActionNames = false;

                    //Костыль для работы старых ендпоинтов V2 не используется в новых
                    options.Filters.Add(new JwtAuthenticationFilterAttribute(services.BuildServiceProvider().GetRequiredService<IJwtWorker>()));

                    options.OutputFormatters.Add(new XmlSerializerOutputFormatterNamespace());
                })
                .AddNewtonsoftJson(options =>
                {
                    options.UseMemberCasing();
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    options.SerializerSettings.Converters.Add(new DatetimeConverter());
                })

                .AddXmlSerializerFormatters()
                .AddXmlDataContractSerializerFormatters();

            services
                .AddMiniProfiler(options =>
                {
                    options.RouteBasePath = "/profiler";
                    (options.Storage as MemoryCacheStorage).CacheDuration = TimeSpan.FromMinutes(60);
                })
                .AddEntityFramework();

            services
                .AddSignalR();

            services
                .AddAuthentication(Configuration)
                .AddAuthorization(options =>
                    {
                        options.DefaultPolicy = new AuthorizationPolicyBuilder(AuthenticationSchemes.JwtBearer)
                            .RequireAuthenticatedUser().Build();

                        options.AddPolicy(AuthenticationSchemes.Basic,
                            new AuthorizationPolicyBuilder(AuthenticationSchemes.Basic).RequireAuthenticatedUser()
                                .Build());
                    })
                .AddSwaggerGen()
                .AddApiVersioning();
              
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider versionDescriptionProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDeveloperExceptionPage();
            app.AddStaticInitializers(Configuration);
            app.UseCors();
            app.UseStaticFiles();
            app.UseOpenFileServer(Configuration);
            app.UseRouting();
            app.UseSession();
            app.UseMiniProfiler();
            app.UseAuthentication();
            app.UseAuthorization();
            #if RELEASE
            app.UseMiddleware<SwaggerAuthMiddleware>();
            #endif
            app.UseSwagger();
            app.UseSwaggerUI(o =>
            {
                foreach (var description in versionDescriptionProvider.ApiVersionDescriptions)
                {
                    o.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                        $"42Clouds API - {description.GroupName.ToUpper()}");
                }

                o.RoutePrefix = "swagger";
                o.DocExpansion(DocExpansion.None);
                o.DefaultModelsExpandDepth(-1);
            });
          
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<NotificationHub>("hubs/notification");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            var telegramBot = app.ApplicationServices.GetRequiredService<IEnumerable<ITelegramBot42>>().FirstOrDefault(x => x.Type == BotType.Alert);

            telegramBot!.SendAlert("Ядро успешно запущено!!!");
        }
    }
}
