﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using AppCommon.Models.CSResourcesModels;
using AppCommon.Tools;
using Core42.Filters;
using Core42.Helpers;
using NLog;

namespace Core42.Controllers
{
    public class CommonStorageController : BaseController
    {
        private readonly Logger _log;

        public CommonStorageController() {
            _log = LogManager.GetCurrentClassLogger();
        }

        [HttpGet]
        [Core42Authorize(CoreGroup.CloudAdminGroup, CoreGroup.ManagerGroup)]
        [ResponseType(typeof(MyDiskInfoModelDto))]        
        public IHttpActionResult GetMyDiskInfo(Guid? accountId)          
        {
            if (!accountId.HasValue)
                return PreconditionFailed("accountId is requiredParamentr");

            var myDiskInfo = new CommonStorageDataProvider().GetMyDiskInfo(accountId.Value);
            if (myDiskInfo != null)
                return Ok(myDiskInfo);

            return BadRequest("Can't create 'My disk info' model");
        }

    }
}