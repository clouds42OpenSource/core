﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.DataContracts.Account.AccountsService;
using Clouds42.Accounts.ServiceAccounts.Managers;
using System.Threading.Tasks;
using Asp.Versioning;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.ServiceAccountContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with service accounts
    /// </summary>
    [Route("service-accounts")]
    [ApiVersion("1.0")]
    public class ServiceAccountController(ServiceAccountManager serviceAccountManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get filtered, sorted and paginated service accounts
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<ServiceAccountDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredServiceAccountsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Create a service account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AddServiceAccountDto>))]
        public IActionResult Create([FromBody] AddServiceAccountDto dto)
        {
            var result = serviceAccountManager.AddServiceAccountItem(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs



        #endregion

        #region DELETEs

        /// <summary>
        /// Delete a service account
        /// </summary>
        /// <param name="id">Identifier of the service account</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Remove(Guid id)
        {
            var result = serviceAccountManager.DeleteServiceAccount(id);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
