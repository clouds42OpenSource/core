﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Core42.Application.Features.DelimiterSourceAccountDatabaseContext.Queries;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter;
using Clouds42.Accounts.ServiceAccounts.Managers;
using Clouds42.DataContracts.Account.AccountsService;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing databases on delimiters
    /// </summary>
    [Route("delimiter-source-account-databases")]
    [ApiVersion("1.0")]
    public class AccountDatabasesOnDelimitersController(
        CreateDelimiterSourceAccountDatabaseManager createDelimiterSourceAccountDatabaseManager,
        EditDelimiterSourceAccountDatabaseManager editDelimiterSourceAccountDatabaseManager,
        RemoveDelimiterSourceAccountDatabaseManager removeDelimiterSourceAccountDatabaseManager,
        ServiceAccountManager serviceAccountManager)
        : BaseV1Controller
    {
        #region GETs
        /// <summary>
        /// Get filtered, sorted and paginated delimiter source account databases
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredDelimiterSourceAccountDatabaseQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Create a databse on delimiters
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Create([FromBody] DelimiterSourceAccountDatabaseDto dto)
        {
            var result = createDelimiterSourceAccountDatabaseManager.Create(dto);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Create a mother base 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route ("service-source-databases")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public IActionResult CreateMotherBase([FromBody] MotherAccountDatabaseDto dto)
        {
            var result = createDelimiterSourceAccountDatabaseManager.CreateMotherBase(dto);
            serviceAccountManager.AddServiceAccountItem(new AddServiceAccountDto { AccountId = dto.AccountId});
            return ResponseResultDto(result);
        }
        #endregion

        #region PUTs

        /// <summary>
        /// Edit a database on delimiters
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] DelimiterSourceAccountDatabaseDto dto)
        {
            var result = editDelimiterSourceAccountDatabaseManager.Edit(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Remove a database on delimiters
        /// </summary>
        /// <param name="accountDatabaseId"></param>
        /// <param name="dbTemplateDelimiterCode"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{accountDatabaseId}/{dbTemplateDelimiterCode}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Remove([FromRoute] Guid accountDatabaseId, [FromRoute] string dbTemplateDelimiterCode)
        {
            var result = removeDelimiterSourceAccountDatabaseManager.Remove(accountDatabaseId, dbTemplateDelimiterCode);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
