﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Segment.CoreHosting.Managers;
using Core42.Application.Features.CoreHostingContext.Dtos;
using Core42.Application.Features.CoreHostingContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("core-hosting")]
    public class CoreHostingController(
        CoreHostingManager coreHostingManager,
        CreateAndEditCoreHostingManager createAndEditCoreHostingManager)
        : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted core hosting
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted core hosting</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<CoreHostingDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredCoreHostingQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get core hosting by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CoreHostingDto>))]
        public async Task<IActionResult> ById([FromRoute] GetCoreHostingByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new core hosting
        /// </summary>
        /// <param name="terminalFarmDto">core hosting</param>
        [HttpPost]
        public IActionResult Add([FromBody] Clouds42.DataContracts.Service.CoreHosting.CoreHostingDto terminalFarmDto)
        {
            return ResponseResultDto(createAndEditCoreHostingManager.CreateCoreHosting(terminalFarmDto));

        }

        /// <summary>
        /// Delete core hosting
        /// </summary>
        /// <param name="id">core hosting id</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(coreHostingManager.DeleteCoreHosting(id));
        }

        /// <summary>
        /// Change core hosting
        /// </summary>
        /// <param name="terminalFarmDto">Changed core hosting</param>
        [HttpPut]
        public IActionResult Edit([FromBody] Clouds42.DataContracts.Service.CoreHosting.CoreHostingDto terminalFarmDto)
        {
            return ResponseResultDto(createAndEditCoreHostingManager.EditCoreHosting(terminalFarmDto));
        }
    }
}
