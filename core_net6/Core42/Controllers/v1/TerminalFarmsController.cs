﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Segment.CloudsServicesTerminalFarm.Managers;
using Core42.Application.Features.TerminalFarmContext.Dtos;
using Core42.Application.Features.TerminalFarmContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("terminal-farms")]
    public class TerminalFarmsController(TerminalFarmManager terminalFarmManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted terminal farms
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted terminal farms</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<TerminalFarmDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredTerminalFarmsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get terminal farm by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<TerminalFarmDto>))]
        // ReSharper disable once RouteTemplates.RouteParameterIsNotPassedToMethod
        public async Task<IActionResult> ById([FromRoute] GetTerminalFarmByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new farm
        /// </summary>
        /// <param name="terminalFarmDto">farm</param>
        [HttpPost]
        public IActionResult Add([FromBody] CloudTerminalFarmDto terminalFarmDto)
        {
            return ResponseResultDto(terminalFarmManager.AddNewTerminalFarm(terminalFarmDto));
            
        }

        /// <summary>
        /// Delete farm
        /// </summary>
        /// <param name="id">farm id</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(terminalFarmManager.DeleteTerminalFarm(id));
        }

        /// <summary>
        /// Change farm
        /// </summary>
        /// <param name="terminalFarmDto">Changed farm</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudTerminalFarmDto terminalFarmDto)
        {
            return ResponseResultDto(terminalFarmManager.EditTerminalFarm(terminalFarmDto));
        }
    }
}
