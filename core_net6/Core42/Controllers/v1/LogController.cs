﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.BLL.Common;
using Clouds42.CloudLogging.Contracts.Dtos;
using Core42.Application.Features.CloudChangesActionContext.Dtos;
using Core42.Application.Features.CloudChangesActionContext.Queries;
using Core42.Application.Features.CloudChangesContext.Dtos;
using Core42.Application.Features.CloudChangesContext.Queries;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Dtos;
using Core42.Application.Features.DatabaseLaunchRdpStartHistoryContext.Queries;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with logs
    /// </summary>
    [Route("logs")]
    [ApiVersion("1.0")]
    public class LogController(ContextAccess contextAccess, ICloudChangesProvider cloudChangesProvider)
        : BaseV1Controller
    {
        /// <summary>
        /// Get launch db and start rdp logs with filter, sorting and paging
        /// </summary>
        /// <param name="query">filter</param>
        /// <returns>Paged list of records</returns>
        [HttpGet]
        [Route("launch-db-and-start-rdp")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<LaunchDbAndStartRdpLogRecordDto>>))]
        public async Task<IActionResult> LaunchDbAndStartRdp([FromQuery] LaunchDbAndStartRdpLogQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        ///  Get all system logging with filter, sorting and paging
        /// </summary>
        /// <param name="query">filter</param>
        /// <returns>Paged list with records</returns>
        [HttpGet]
        [Route("cloud-changes")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<CloudChangesLogRecordDto>>))]
        public async Task<IActionResult> CloudChanges([FromQuery] CloudChangesQuery query)
        {
            if (query?.Filter != null)
            {
                query.Filter.IsForeignAccount = !contextAccess.IsCurrentContextAccount;
                query.Filter.CurrentAccountUserId = contextAccess.CurrentAccountUserId;
                query.Filter.ContextAccountId = contextAccess.ContextAccountId;
            }

            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        ///  Get all system logging with filter, sorting and paging
        /// </summary>
        /// <returns>Paged list with records</returns>
        [HttpGet]
        [Route("cloud-changes-action")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<CloudChangesActionDto>>))]
        public async Task<IActionResult> CloudChangesActions()
        {
            return ResponseResultDto(await Mediator.Send(new CloudChangesActionsQuery()));
        }

        /// <summary>
        /// Event log action
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("log-event")]
        public IActionResult LogEvent([FromBody] LogEventDto dto)
        {
            var result = cloudChangesProvider.LogEvent(dto.AccountId, dto.Log, dto.Description);

            return Json(new { success = !result.Error, data = result.Message });
        }

    }
}
