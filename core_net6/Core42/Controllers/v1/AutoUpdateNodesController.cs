﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.AutoUpdateNodesContext.Dtos;
using Core42.Application.Features.AutoUpdateNodesContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing auto update nodes
    /// </summary>
    [ApiVersion("1.0")]
    [Route("auto-update-nodes")]
    public class AutoUpdateNodesController : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get all paged auto update nodes
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AutoUpdateNodeDto>>))]
        public async Task<IActionResult> GetAll([FromQuery] GetAllAutoUpdatesNodesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get an auto update identifier by identfieir
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AutoUpdateNodeDto>))]
        public async Task<IActionResult> GetById([FromRoute] GetAutoUpdateNodeByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }


        #endregion

        #region POSTs

        /// <summary>
        /// Add an auto update node
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public async Task<IActionResult> AddAutoUpdateNode([FromBody] AddAutoUpdateNodeQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit an auto update node
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public async Task<IActionResult> EditAutoUpdateNode([FromBody] EditAutoUpdateNodeQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete an auto update node
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> DeleteAutoUpdateNode([FromQuery]  DeleteAutoUpdateNodeQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion
    }
}
