﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Core42.Application.Features.BackupContext.Queries;
using Clouds42.AccountDatabase.Backup.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.AccountDatabase.Contracts.Backup.Models;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService;
using Core42.Application.Features.BackupContext.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing databases backups
    /// </summary>
    [Route("database-backup")]
    [ApiVersion("1.0")]
    public class BackupController(BackupManager backupManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get backup information
        /// </summary>
        /// <param name="backupId">Backup identifier</param>
        /// <returns></returns>
        [HttpGet("{backupId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<BackupRestoringDto>))]
        public async Task<IActionResult> GetBackupInfoById([FromRoute] Guid backupId)
        {
            return ResponseResultDto(await Mediator.Send(new GetBackupInfoByIdQuery(backupId)));
        }

        /// <summary>
        /// Get uploaded backup file status
        /// </summary>
        /// <param name="query">Uploaded file identifier</param>
        /// <returns>Status with comment</returns>
        [HttpGet("upload-status")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<UploadedFileInfoDto>))]
        public async Task<IActionResult> GetUploadedBackupFileStatus([FromQuery] GetBackupUploadStatusQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get Google-Disk storage path for account
        /// </summary>
        /// <param name="query">Account identifier</param>
        /// <returns></returns>
        [HttpGet("google-storage")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<GoogleBackupDto>))]
        public async Task<IActionResult> GetPathToGoogleDiskStorage([FromQuery] GetGoogleDriveStorageQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Restore infobase from archive
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns>The result of running the recovery task</returns>
        [HttpPost("restore")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<CreateAccountDatabasesResultDto>))]
        public IActionResult RestoreAccountDatabaseFromBackup([FromBody] RestoreAcDbFromBackupDto model)
        {
            return ResponseResultDto(backupManager.RestoreAccountDatabase(model));
        }
        
        #endregion

        #region PUTs

        /// <summary>
        /// Start the process of downloading the backup 
        /// </summary>
        /// <param name="backupId">Backup identifier</param>
        /// <param name="uploadedFileId">Uploaded backup file identifier</param>
        /// <returns></returns>
        [HttpPut("{backupId:guid}/start-fetching")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult StartBackupUploadedFileFetching([FromRoute] Guid backupId, [FromQuery] Guid uploadedFileId)
        {
            return ResponseResultDto(backupManager.StartBackupFileFetching(backupId, uploadedFileId));
        }

        /// <summary>
        /// Pay the service backup for the account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut("pay")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PaymentServiceResultDto>))]
        public IActionResult PayBackup([FromBody] PayBackupModelDto dto)
        {
            return ResponseResultDto(backupManager.PayBackup(dto));
        }

        #endregion
    }
}
