﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Models;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Core42.Application.Features.AccountDatabaseContext.Commands;
using Clouds42.MyDatabaseService.Calculator.Managers;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Queries;
using Core42.Application.Features.AccountDatabaseContext.Dtos;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [Route("account-databases")]
    [ApiVersion("1.0")]
    public class AccountDatabasesController(
        IAccountDatabaseManager accountDatabaseManager,
        AccountDatabasePublishManager accountDatabasePublishManager,
        StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager,
        AccountDatabaseEditManager accountDatabaseEditManager,
        ContextAccess contextAccessor,
        CalculateCostOfAcDbAccessesManager calculateCostOfAcDbAccessesManager,
        CreateAccountDatabasesManager createAccountDatabasesManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get a collection of infobases by filter and order
        /// </summary>
        /// <param name="query">Search filter</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type =
                typeof(ResponseResultDto<
                    PagedDto<Application.Features.AccountDatabaseContext.Dtos.AccountDatabaseItemDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetAccountDatabasesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get infobase card
        /// </summary>
        /// <param name="number">database number</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{number}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountDatabaseCardResultDto>))]
        public async Task<IActionResult> Card([FromRoute] string number)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountDatabaseQuery { AccountDatabaseNumber = number }));
        }

        /// <summary>
        /// Get infobase list by searchLine
        /// </summary>
        /// <param name="query">database name or number</param>
        /// <returns></returns>
        [HttpGet]
        [Route("as-list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountDatabaseInfoDto>>))]
        public async Task<IActionResult> SearchByNameOrNumber([FromQuery] GetAccountDatabaseBySearchLineQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpGet]
        [Route("check-name")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountDatabaseInfoDto>>))]
        public async Task<IActionResult> CheckIfNameExists([FromQuery] string name)
        {
            return ResponseResultDto(await Mediator.Send(new CheckIfDatabaseNameExistsQuery(name)));
        }

        /// <summary>
        /// Get all databases templates for the account
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CreateAcDbFromTemplateDataDto>))]
        public async Task<IActionResult> GetTemplatesForAccount([FromQuery] GetAccountDatabasesTemplatesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Authorization in database
        /// </summary>
        /// <param name="authorizeInDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("authorization")]
        public IActionResult AuthorizeInDatabase([FromBody] SupportDataDto authorizeInDatabaseDto)
        {
            var result = accountDatabaseManager.SaveSupportDataAndAuthorize(authorizeInDatabaseDto);
            if (!result.Result)
                return ResponseResultDto(result);

            return ResponseResultDto(accountDatabaseManager.GetDatabaseSupportSettings(authorizeInDatabaseDto.DatabaseId).ToOkManagerResult());
        }

        [HttpPost]
        [Route("from-file-to-cluster")]
        public async Task<IActionResult> TransformFromFileToCluster([FromBody] CreateClusteredCommand command) 
            => ResponseResultDto(await Mediator.Send(command));

        /// <summary>
        /// DeAuthorizetion in database
        /// </summary>
        /// <param name="deAuthorizationDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("deauthorization")]
        public IActionResult DeAuthorizeInDatabase([FromBody] DeAuthorizationDatabaseDto deAuthorizationDatabaseDto)
        {
            accountDatabaseManager.DisableSupportAuthorization(deAuthorizationDatabaseDto.DatabaseId);
            return ResponseResultDto(accountDatabaseManager.GetDatabaseSupportSettings(deAuthorizationDatabaseDto.DatabaseId).ToOkManagerResult());
        }

        
       
        /// <summary>
        /// Publish database
        /// </summary>
        /// <param name="publishDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("publish")]
        public IActionResult PublishDatabase([FromBody] PublishDatabaseDto publishDatabaseDto)
        {
            if (publishDatabaseDto.Publish)
            {
                var result1 = accountDatabasePublishManager.PublishDatabase(publishDatabaseDto.DatabaseId);
                return ResponseResultDto(result1);
            }

            var model = new PostRequestAccDbIdDto { AccountDatabaseId = publishDatabaseDto.DatabaseId };
            var result = accountDatabasePublishManager.CancelPublishDatabase(model);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// Calculate the cost of accesses in the configuration
        /// </summary>
        /// <param name="calculateAccessesCostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("access/price")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<AccountUserLicenceForConfigurationDto>>))]
        public IActionResult CalculateAccessesCost([FromBody] CalculateAccessesCostDto calculateAccessesCostDto)
        {
            var result = calculateCostOfAcDbAccessesManager.CalculateAccessesCostForConfigurations
                (calculateAccessesCostDto.ServiceTypesIdsList, calculateAccessesCostDto.AccountUserDataModelsList, calculateAccessesCostDto.AccountId);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// Create a database from a template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("from-template")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<CreateAccountDatabasesResultDto>))]
        public IActionResult CreateDatabaseFromTemplate([FromBody] CreateAccountDatabasesFromTemplateDto dto)
        {
            return ResponseResultDto(createAccountDatabasesManager.CreateAccountDatabasesFromTemplate(contextAccessor.ContextAccountId, dto));
        }

        /// <summary>
        /// Добавляет стейт в историю саппорта, что пользователь уведомлен о возможности постановки баз на АО и ТИИ
        /// </summary>
        /// <returns></returns>
        [HttpPost("{accountId}/notification-state")]
        public async Task<IActionResult> AddUserNotifiedState(Guid accountId) =>
           ResponseResultDto(await accountDatabaseManager.AddUserNotifiedState(accountId));
        

        /// <summary>
        /// Create a database from a template
        /// </summary>
        /// <returns></returns>
        [HttpPost("{accountId}/activate-rent-if-need")]
        public IActionResult ActivateRent1CIfNeeded([FromRoute] Guid accountId)
        {
           var result =  createAccountDatabasesManager.ActivateRent1CIfNeeded(accountId);

            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Cancel ifnobase publication
        /// </summary>
        /// <param name="databaseId">Infobase identifier</param>
        /// <returns></returns>
        [HttpPut]
        [Route("cancel-publish/{DatabaseId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult CancelPublishDatabase([FromRoute] Guid databaseId)
        {
            var model = new PostRequestAccDbIdDto { AccountDatabaseId = databaseId };
            var res = accountDatabasePublishManager.CancelPublishDatabase(model);
            return ResponseResultDto(res);
        }

        /// <summary>
        /// Edit infobase info
        /// </summary>
        /// <param name="dto">Infobase editing model</param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] DatabaseCardEditDatabaseInfoDataDto dto)
        {
            var result = accountDatabaseEditManager.EditAccountDatabase(
                AccountDatabaseCartDomainModel.CreateFrom(dto), contextAccessor.ContextAccountId);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete infobase
        /// </summary>
        /// <param name="databaseId">Infobase identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{databaseId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RemoveDatabase([FromRoute] Guid databaseId)
        {
            var result = startProcessToWorkDatabaseManager.StartProcessOfRemoveDatabase(databaseId);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
