﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.StateMachine;
using Clouds42.DataContracts.ProcessFlow;
using System.Threading.Tasks;
using Asp.Versioning;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using Core42.Application.Features.ProcessFlowContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with process flows
    /// </summary>
    [Route("process-flows")]
    [ApiVersion("1.0")]
    public class ProcessFlowController(ProcessFlowRetryManager processFlowRetryManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get details about a process flow
        /// </summary>
        /// <param name="processFlowId">Process flow identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ProcessFlowDto>))]
        [Route("{processFlowId:guid}")]
        public async Task<IActionResult> Details([FromRoute] Guid processFlowId)
        {
            return ResponseResultDto(await Mediator.Send(new GetProcessFlowByIdQuery(processFlowId)));
        }

        /// <summary>
        /// Get all process flows by filter
        /// </summary>
        /// <param name="query">Process flow query filter</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<ProcessFlowDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetProcessFlowFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Retry process flow
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("retry")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Retry([FromBody] ProcessFlowRetryDto dto)
        {
            var result = processFlowRetryManager.Retry(dto.ProcessFlowId);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
