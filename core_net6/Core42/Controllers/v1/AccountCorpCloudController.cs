﻿using System;
using Clouds42.Accounts.Contracts.CorpCloud.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountCorpCloudController(IAccountCorpCloudProvider accountCorpCloudProvider) : BaseV1Controller
    {
        /// <summary>
        /// get corp cloud info
        /// </summary>
        [HttpGet("CorpCloudInfo")]
        public IActionResult GetCorpCloudInfo(Guid accountId) 
        {
            return ResponseResultDto(accountCorpCloudProvider.GetAccountCorpCloudInfo(accountId));
        }

        /// <summary>
        /// update corp cloud info
        /// </summary>
        [HttpPut("CorpCloudInfo")]
        public IActionResult UpdateCorpCloudInfo(Guid accountId)
        {
            return ResponseResultDto(accountCorpCloudProvider.UpdateAccountCorpCloudInfo(accountId));
        }
    }
}
