﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.AccountUsersContext.Queries;
using Core42.Application.Features.NotificationSettingsContext.Commands;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    [Route("notification-settings")]
    [ApiVersion("1.0")]
    public class NotificationSettingsController : BaseV1Controller
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountUserNotificationSettingsDto>>))]
        public async Task<IActionResult> ChangeUserNotificationSettings([FromBody] ChangeUserNotificationSettingsCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }
    }
}
