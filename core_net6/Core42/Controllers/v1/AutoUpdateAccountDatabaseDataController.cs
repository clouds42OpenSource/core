﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountDatabase.AutoUpdate.Managers;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using Core42.Application.Features.AutoUpdateAccountDatabaseDataContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with account database auto update
    /// </summary>
    [Route("auto-update-account-database-data")]
    [ApiVersion("1.0")]
    public class AutoUpdateAccountDatabaseDataController(
        AutoUpdateAccountDatabaseDataManager autoUpdateAccountDatabaseDataManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get queued auto update account database data with pagination, sorting and filtering
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("queued")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<AutoUpdateAccountDatabaseDto>>))]
        public async Task<IActionResult> Queued([FromBody] DatabaseInAutoUpdateQueueQuery query)
        {
            query.AutoUpdateState = AutoUpdateState.Queued;

            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet]
        [Route("solo-update-queue")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AutoUpdateAccountDatabaseDto>>))]
        public async Task<IActionResult> AutoUpdateQueue(bool onlyVip)
        {
            return ResponseResultDto(await Mediator.Send(new DatabaseInAutoUpdateQueueQuery{Filter = new DatabaseInAutoUpdateQueryFilter{IsActiveRent1COnly = onlyVip}, AutoUpdateState = AutoUpdateState.SoloUpdate}));
        }


        /// <summary>
        /// Get performed auto update account database data with pagination, sorting and filtering
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("performed")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<AutoUpdateAccountDatabaseDto>>))]
        public async Task<IActionResult> Performed([FromBody] DatabaseInAutoUpdateQueueQuery query)
        {
            query.AutoUpdateState = AutoUpdateState.Performed;

            return ResponseResultDto(await Mediator.Send(query));
        }


        /// <summary>
        /// Get connected auto update account database data with pagination, sorting and filtering
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("connected")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<AutoUpdateAccountDatabaseDto>>))]
        public async Task<IActionResult> Connected([FromBody] DatabaseInAutoUpdateQueueQuery query)
        {
            query.AutoUpdateState = AutoUpdateState.Connected;

            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get workers that perform auto-update
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("workers")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<short>>))]
        public async Task<IActionResult> Workers()
        {
            return ResponseResultDto(await Mediator.Send(new GetWorkersForAutoUpdateQuery()));
        }

        /// <summary>
        /// Get techinal results
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("technical-results")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<DatabaseAutoUpdateTechnicalResultDataDto>>))]
        public IActionResult TechnicalResults([FromQuery] GetTechnicalResultsQuery query)
        {
            return ResponseResultDto(autoUpdateAccountDatabaseDataManager.GetDatabaseAutoUpdateTechnicalResults(query));
        }

        #endregion
    }
}
