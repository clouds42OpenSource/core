﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.CreateAccount.Interfaces;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [Route("")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class CredentialsController(IServiceProvider serviceProvider) : BaseV1Controller
    {
        readonly AccountUserResetPasswordNotificationManager _accountUserResetPasswordNotificationManager = serviceProvider.GetRequiredService<AccountUserResetPasswordNotificationManager>();
        readonly IAccountUserSessionManager _accountUserSessionManager = serviceProvider.GetRequiredService<IAccountUserSessionManager>();
        private readonly IReCaptchaValidator _captchaValidator = serviceProvider.GetRequiredService<IReCaptchaValidator>();
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();
        private readonly AccountAgreementHelper _accountAgreementHelper = serviceProvider.GetRequiredService<AccountAgreementHelper>();
        private readonly CreateAccountManager _createAccountManager = serviceProvider.GetRequiredService<CreateAccountManager>();
        private readonly IHandlerException _handlerException = serviceProvider.GetRequiredService<IHandlerException>();
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();
        private readonly IAccessProvider _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();

        #region GETs
        /// <summary>
        /// Open supplier offer agreement
        /// </summary>
        [HttpGet]
        [Route("personal/offer-document-file")]
        public IActionResult OpenAgreementFile([FromQuery] RegistrationConfigViewModel registrationConfig, string locale = "ru-ru")
        {
            try
            {
                var agreementFileId = _accountAgreementHelper.GetAgreementFileId(registrationConfig, locale);
                var agreementFile = _accountAgreementHelper.GetAgreementFile(agreementFileId).ToOkManagerResult();

                if (!agreementFile.Error)
                {
                    return File(agreementFile.Result.Content, agreementFile.Result.ContentType);
                }

                ViewBag.ErrorText = agreementFile.Message;

                return NotFound($"Файла для поставщика {agreementFileId} не найден");
            }
            catch (Exception ex)
            {
                return ResponseResultDto(PagedListExtensions.
                    ToPreconditionFailedManagerResult<string>(ex.Message));
            }

        }

        /// <summary>
        /// Изменить пароль
        /// </summary>
        /// <param name="resetCode">Код восстановления пароля</param>
        [HttpGet]
        [Route("change-password-session")]
        public IActionResult Change([FromQuery] string resetCode)
        {
            var tokenResult = _accountUserSessionManager.GetTokenByResetCode(resetCode);

            if (tokenResult.State != ManagerResultState.Ok)
                return ResponseResultDto(tokenResult);
            
            var loginResult = _accountUserSessionManager.GetLoginByToken(tokenResult.Result);
            if (loginResult.Error)
                return ResponseResultDto(loginResult);

            var confirmPasswordModel = new ConfirmPasswordModel { LoginToken = tokenResult.Result, Login = loginResult.Result };
           
            return ResponseResultDto(new ManagerResult<ConfirmPasswordModel> { State = ManagerResultState.Ok, Result = confirmPasswordModel });
        }


        /// <summary>
        /// Очистить контекст для пользователя
        /// </summary>
        /// <returns>Результат установки</returns>
        [HttpGet]
        [Route("clear-session")]
        public IActionResult ClearSession()
        {
            HttpContext.SignOutAsync();
            HttpContext.Session.Clear();
            _accessProvider.SetCurrentContextAccount(HttpContext.Response.Cookies);

            return ResponseResultDto(new ManagerResult { State = ManagerResultState.Ok });
        }
        #endregion

        #region POSTs
        /// <summary>
        /// Востановить пароль
        /// </summary>
        /// <param name="email">Почта пользователя</param>
        [HttpPost]
        [Route("reset-password-letter")]
        public IActionResult ResetPassword([FromBody] string email)
        {
            return ResponseResultDto(_accountUserResetPasswordNotificationManager.SendPasswordChangeRequest(email));
        }

        /// <summary>
        /// Установить новый пароль для пользователя
        /// </summary>
        /// <param name="model">Модель подтверждения пароля</param>
        /// <returns>Результат установки</returns>
        [HttpPost]
        [Route("new-password")]
        public IActionResult SetPassword([FromBody]ConfirmPasswordModel model)
        {
            return !ModelState.IsValid ? ModelValidation() : ResponseResultDto(_accountUserResetPasswordNotificationManager.SetNewPassword(model));
        }

        /// <summary>
        /// Зарегистрировать пользователя
        /// </summary>
        /// <param name="model">Модель регистрации</param>
        /// <returns>Реузльтат регистрации</returns>
        [HttpPost]
        [Route("personal/signUp")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountSignInModelDto>))]
        public async Task<IActionResult> Index([FromBody] RegisterModel model)
        {
            if (string.IsNullOrEmpty(model.CaptchaToken))
                return ResponseResultDto(PagedListExtensions.
                    ToPreconditionFailedManagerResult<AccountSignInModelDto>("Captcha token empty"));

            if (!_captchaValidator.IsCaptchaPassed(model.CaptchaToken))
            {
                return ResponseResultDto(PagedListExtensions.
                    ToPreconditionFailedManagerResult<AccountSignInModelDto>("Captcha validation failed"));
            }
            var domainInfo = new Uri(HttpContext.Request.GetDisplayUrl()).GetDomainInfo();

            var cloudService = model.RegistrationConfig.GetSystemCloudServiceEnumValue();
            var accountRegistrationModel = new AccountRegistrationModelDto
            {
                Email = model.Email,
                FullPhoneNumber = model.PhoneNumber,
                Login = model.IsAutoGeneratedLogin ? string.Empty : model.Login,
                Password = model.IsAutoGeneratedLogin ? null : model.Password,
                UserSource = string.IsNullOrEmpty(model.UserSource)? domainInfo?.RegistrableDomain: model.UserSource,
                RegistrationConfig = new RegistrationConfigDomainModelDto
                {
                    CloudService = cloudService,
                    CloudServiceId = model.RegistrationConfig.CloudServiceId,
                    Configuration1C = model.RegistrationConfig.Configuration1C,
                    RegistrationSource = model.RegistrationConfig.RegistrationSource,
                    ReferralAccountId = model.RegistrationConfig.ReferralAccountId
                },
                LocaleName = model.LocaleName
            };

            try
            {
                // Добавляем аккаунт
                var createAccount = await _createAccountManager.AddAccount(accountRegistrationModel);
                if (createAccount.Error)
                {
                    _logger.Warn($"Ошибка регистрации аккаунта {model.Login} {model.PhoneNumber} {model.Email} :: '{createAccount.Message}'");

                    return ResponseResultDto(PagedListExtensions.ToPreconditionFailedManagerResult<AccountSignInModelDto>(createAccount.Message));
                }
                var registerModel = new AccountSignInModelDto
                {
                    Password = createAccount.Result.AccountUserPassword,
                    Username = createAccount.Result.AccountUserLogin,
                    ReturnUrl = GetRedirectUrl(model, createAccount.Result)
                };

                return ResponseResultDto(registerModel.ToOkManagerResult());
            }

            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Error adding user to company] : {accountRegistrationModel.Login}");

                return ResponseResultDto(PagedListExtensions.ToPreconditionFailedManagerResult<AccountSignInModelDto>(ex.Message));
            }
        }

        /// <summary>
        /// Получить адрес редиректа после авторизации
        /// </summary>
        /// <param name="model">Модель регистрации пользователя</param>
        /// <param name="accountRegistrationResult">Результат активации аккаунта</param>
        /// <returns>Адрес редиректа после авторизации</returns>
        private string GetRedirectUrl(RegisterModel model, AccountRegistrationResultDto accountRegistrationResult)
        {
            if (!accountRegistrationResult.ServiceActivationErrorMessage.IsNullOrEmpty())
                return Url.Action("", "spa",
                    new { errorMessage = accountRegistrationResult.ServiceActivationErrorMessage });

            if (!model.RegistrationConfig.Configuration1C.IsNullOrEmpty())
                return Url.Action("my-databases", "spa");

            if (model.RegistrationConfig.CloudServiceId.HasValue)
            {
                return Url.Action("billing-service", "spa", model.RegistrationConfig.CloudServiceId);
            }

            if (model.RegistrationConfig.ReferralAccountId.HasValue &&
                !model.RegistrationConfig.CloudServiceId.HasValue)
                return Url.Action("my-databases", "spa");

            var cloudService = model.RegistrationConfig.GetSystemCloudServiceEnumValue();
            return cloudService switch
            {
                Clouds42Service.MyEnterprise => Url.Action("create-db", "spa"),
                Clouds42Service.Esdl => Url.Action("esdl", "spa"),
                _ => _accountConfigurationDataProvider.GetAccountLocale(accountRegistrationResult.Account.Id).Name !=
                     LocaleConst.Russia
                    ? Url.Action("rent", "spa")
                    : Url.Action("welcome-page", "spa")
            };
        }

        #endregion
    }
}
