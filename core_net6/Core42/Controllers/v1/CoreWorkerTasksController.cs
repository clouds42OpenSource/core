﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Core42.Application.Features.CoreWorkerTaskContext.Queries;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.CoreWorkerTaskContext.Dtos;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing core workers' tasks
    /// </summary>
    [Route("core-worker-tasks")]
    [ApiVersion("1.0")]
    public class CoreWorkerTasksController(ICoreWorkerTaskManager coreWorkerTaskManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get all worker`s tasks as key-value
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("as-key-value")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<KeyValuePair<Guid, string>>>))]
        public async Task<IActionResult> AsKeyValue()
        {
            return ResponseResultDto(await Mediator.Send(new GetAllCoreWorkerTasksAsKeyValueQuery()));
        }

        /// <summary>
        /// Get all worker`s tasks filtered
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<CoreWorkerTaskDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredCoreWorkerTasksQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get workes task for edit
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CoreWorkerTaskDto>))]
        public async Task<IActionResult> ById([FromRoute] GetCoreWorkerTaskByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get tab visibility flags
        /// </summary>
        /// <returns></returns>
        [HttpGet("visibility")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<CoreWorkerTaskDto>>))]
        public async Task<IActionResult> Visibility()
        {
            return ResponseResultDto(await Mediator.Send(new GetCoreWorkerTabVisibilityQuery()));
        }

        /// <summary>
        /// Get available tasks bag
        /// </summary>
        /// <returns></returns>
        [HttpGet("available-tasks")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AvailableTaskBagDto>>))]
        public async Task<IActionResult> AvailableTasksBag([FromQuery] GetFilteredAvailableTasksBagQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get priority
        /// </summary>
        /// <returns></returns>
        [HttpGet("{workerId}/available-tasks/{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AvailableTaskBagDto>))]
        public async Task<IActionResult> AvailableTasksBagPriority([FromRoute] short workerId, [FromRoute] Guid taskId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAvailableTasksBagPriorityQuery(workerId, taskId)));
        }

        /// <summary>
        /// Get available task by workerId
        /// </summary>
        /// <returns></returns>
        [HttpGet("{workerId}/available-tasks")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AvailableTaskBagDto>))]
        public async Task<IActionResult> AvailableTasksBagByWorkerId([FromRoute] short workerId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAvailableTasksBagByWorkerIdQuery(workerId)));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Edit worker task
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] EditCoreWorkerTaskDto dto)
        {
            var result = coreWorkerTaskManager.EditCoreWorkerTask(dto);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Update priority
        /// </summary>
        /// <returns></returns>
        [HttpPost("available-tasks/priority")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<CoreWorkerTaskDto>>))]
        public IActionResult UpdatePriority([FromBody] ChangeCoreWorkerTaskBagPriorityDto request)
        {
            return ResponseResultDto(coreWorkerTaskManager.ChangeWorkerTaskBagPriority(request));
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Change available worker's tasks
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("available-tasks")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult ChangeWorkerAvailableTasksBags([FromBody] ChangeWorkerAvailableTasksBagsDto dto)
        {
            var result = coreWorkerTaskManager.ChangeWorkerAvailableTasksBags(dto);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
