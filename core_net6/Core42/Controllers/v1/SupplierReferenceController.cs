﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.Suppliers.Managers;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.DataContracts.SupplierReference;
using System.Threading.Tasks;
using Asp.Versioning;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.SupplierContext.Dtos;
using Core42.Application.Features.SupplierContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing suppliers' reference
    /// </summary>
    [Route("suppliers-reference")]
    [ApiVersion("1.0")]
    public class SupplierReferenceController(
        SupplierReferenceManager supplierReferenceManager,
        SupplierReferralAccountManager supplierReferralAccountManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get a paginated collection of suppliers
        /// </summary>
        /// <param name="query">Query of request</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<Application.Features.SupplierContext.Dtos.SupplierDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredSuppliersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        ///  Get supplier by id
        /// </summary>
        /// <param name="id">Supplier id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<Application.Features.SupplierContext.Dtos.SupplierDto>>))]
        public async Task<IActionResult> ById([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetSupplierReferenceByIdQuery(id)));
        }

        /// <summary>
        /// Get items used for initializing the pages for creating/editing a supplier
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("combo-boxes")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<InitSelectListItemsDto>))]
        public async Task<IActionResult> ComboBoxes()
        {
            return ResponseResultDto(await Mediator.Send(new GetSupplierReferencesComboBoxesQuery()));
        }

        /// <summary>
        /// Get supplier referral accounts
        /// </summary>
        /// <param name="supplierId">Identifier of the supplier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{supplierId}/referral-accounts")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<SupplierReferralAccountsDictionaryDto>>))]
        public async Task<IActionResult> ReferralAccounts([FromRoute] Guid supplierId)
        {
            return ResponseResultDto(await Mediator.Send(new GetSupplierReferenceReferralAccountsQuery(supplierId)));
        }

        /// <summary>
        /// Open supplier offer agreement
        /// </summary>
        /// <param name="supplierId">supplier</param>
        [HttpGet]
        [Route("offer-document-file")]
        public IActionResult OpenAgreementFile([FromQuery] Guid supplierId)
        {
            var data = supplierReferenceManager.GetAgreementCloudFileBySupplierId(supplierId);

            if (data.Error)
            {
                ViewBag.ErrorText = data.Message;
                return NotFound($"Файла для поставщика {supplierId} не найден");
            }
            return File(data.Result.Bytes, data.Result.ContentType); 
         }

        #endregion

        #region POSTs

        /// <summary>
        /// Create a supplier
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Add([FromForm] Clouds42.DataContracts.Billing.Supplier.SupplierDto dto)
        {
            var result = supplierReferenceManager.CreateSupplier(dto);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Create a referral account for a supplier
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{supplierId}/referral-accounts/{accountId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RefferalAccount([FromBody] SupplierReferenceAddRefferalAccountDto dto)
        {
            var result = supplierReferralAccountManager.CreateSupplierReferralAccount(dto);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Edit supplier for a account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("account")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult EditSupplierForAccount([FromBody] SupplierReferenceAddRefferalAccountDto dto)
        {
            var result = supplierReferralAccountManager.EditSupplierForAccount(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit a supplier
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("referral-accounts")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RefferalAccount([FromForm] Clouds42.DataContracts.Billing.Supplier.SupplierDto dto)
        {
            var result = supplierReferenceManager.EditSupplier(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete a supllier referral account
        /// </summary>
        /// <param name="supplierId">Identifier of the supplier</param>
        /// <param name="accountId">Identifier of the referral account</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{supplierId}/referral-accounts/{accountId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RemoveReferralAccount([FromRoute] Guid supplierId, Guid accountId)
        {
            var result = supplierReferralAccountManager.DeleteSupplierReferralAccount(supplierId, accountId);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Remove supplier
        /// </summary>
        /// <param name="id">Supplier identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RemoveSupplier([FromRoute] Guid id)
        {
            var result = supplierReferenceManager.DeleteSupplier(id);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
