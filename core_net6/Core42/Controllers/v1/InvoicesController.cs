﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Domain.Enums.Security;
using Core42.Application.Features.InvoiceContext.Dtos;
using Core42.Application.Features.InvoiceContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Attributes;

namespace Core42.Controllers.v1
{
    [Route("invoices")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class InvoicesController : BaseV1Controller
    {

        #region GETs

        /// <summary>
        /// Get invoice by query params
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="uniq"></param>
        /// <param name="sum"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<InvoiceDto>))]
        [ExternalService(ExternalServiceTypeEnum.Corp)]
        [Route("{uniq:long}")]
        [TokenAuthorize]
        public async Task<IActionResult> GetOneByQueryParams([FromQuery] Guid accountId, [FromRoute] long uniq, [FromQuery] decimal sum)
        {
            return ResponseResultDto(await Mediator.Send(new GetInvoiceByFilterQuery { AccountId = accountId, Sum = sum, Uniq = uniq }));
        }
        
        /// <summary>
        /// Get all invoices
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<InvoiceDto>>))]
        [ExternalService(ExternalServiceTypeEnum.Corp)]
        [TokenAuthorize]
        public async Task<IActionResult> GetAll([FromQuery] GetAllInvoicesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }
        #endregion

    }
}
