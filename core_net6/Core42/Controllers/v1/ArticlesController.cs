﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.ArticleBillingContext.Commands;
using Core42.Application.Features.ArticleBillingContext.Dtos;
using Core42.Application.Features.ArticleBillingContext.Queries;
using Core42.Application.Features.ArticleContext.Commands;
using Core42.Application.Features.ArticleContext.Dtos;
using Core42.Application.Features.ArticleContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("articles")]
    public class ArticlesController: BaseV1Controller
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<ArticleDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredArticlesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleExtendedDto>))]
        public async Task<IActionResult> ById([FromRoute] int id)
        {
            return ResponseResultDto(await Mediator.Send(new GetArticleByIdQuery(id)));
        }

        [HttpGet]
        [Route("summary-info/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleSummaryInfoDto>))]
        public async Task<IActionResult> SummaryInfo([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetArticleSummaryInfoQuery(id)));
        }

        [HttpGet]
        [Route("cash-out-to-balance/available-sum")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<decimal>))]
        public async Task<IActionResult> GetTransferBalanceRequest([FromQuery] GetTransferBalanseQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpGet]
        [Route("transaction-info")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleTransactionDto>))]
        public async Task<IActionResult> TransactionInfo([FromQuery] GetTransactionArticlesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpPost]
        [Route("cash-out-to-balance/available-sum")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> CreateTransferBalanceRequest([FromBody] CreateTransferBalanceCommand query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleDto>))]
        public async Task<IActionResult> Create([FromBody] CreateArticleCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        [HttpPost]
        [Route("transaction-сreate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> CreateTransaction([FromBody] CreateInflowTransactionCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        [HttpDelete]
        [Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteArticleCommand(id)));
        }

        [HttpPut]
        [Route("{id:int}/change-status")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleDto>))]
        public async Task<IActionResult> ChangeStatus([FromRoute] int id, [FromBody] ChangeArticleStatusCommand command)
        {
            command.Id = id;

            return ResponseResultDto(await Mediator.Send(command));
        }

        [HttpPut]
        [Route("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ArticleDto>))]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateArticleCommand command)
        {
            command.Id = id;

            return ResponseResultDto(await Mediator.Send(command));
        }
    }
}
