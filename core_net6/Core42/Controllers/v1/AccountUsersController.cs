﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.AccountUsers.Contracts;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountUser.AccountUserBitrixChatInfo;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.Access;
using Core42.Application.Features.AccountUsersContext.Commands;
using Core42.Application.Features.AccountUsersContext.Dtos;
using Core42.Application.Features.AccountUsersContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using AccountUserDto = Clouds42.DataContracts.AccountUser.AccountUserDto;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing account users
    /// </summary>
    [Route("account-users")]
    [ApiVersion("1.0")]
    public class AccountUsersController(
        IAccountUsersManager accountUsersManager,
        AccountDatabaseCardManager accountDatabaseCardManager,
        StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get account users by filter and order.
        /// Returns only the user record if the requesting user only has the <see cref="AccountUserGroup.AccountUser"/> role
        /// </summary>
        /// <param name="query">Filter and order query model</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<Application.Features.AccountUsersContext.Dtos.AccountUserDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredAccountUsersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get account users by filter and order.
        /// Returns only the user record if the requesting user only has the <see cref="AccountUserGroup.AccountUser"/> role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Application.Features.AccountUsersContext.Dtos.AccountUserDto>))]
        [Route("{id:guid}")]
        public async Task<IActionResult> ById(Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountUserByIdQuery(id)));
        }

        [HttpGet]
        [Route("{id:guid}/transitions")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<TransitionDto>>))]
        public async Task<IActionResult> GetTransitionCount([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetTransitionsCountByUserIdQuery(id)));
        }

        /// <summary>
        /// Get user information for Bitrix chat
        /// </summary>
        /// <param name="accountUserId">Id user</param>
        /// <param name="currentUrl">current url</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountUserId:guid}/bitrix-info")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountUserBitrixChatInfoItemDto>>))]
        public IActionResult GetUserInfoForBitrix([FromRoute]Guid accountUserId, [FromQuery] string currentUrl)
        {
            return ResponseResult(accountUsersManager.GetUserInfoForBitrix(accountUserId, currentUrl));
        }

        [HttpGet]
        [Route("verify-email")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<VerifyEmailResultDto>))]
        public async Task<IActionResult> VerifyEmail()
        {
            return ResponseResultDto(await Mediator.Send(new VerifyEmailCommand()));
        }

        [HttpGet]
        [Route("confirm-email")]
        [ProducesResponseType(StatusCodes.Status200OK,  Type = typeof(ResponseResultDto<string>))]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromQuery] string code, [FromQuery] Guid? accountUserId, [FromQuery] bool? isLinkClicked)
        {
            return ResponseResultDto(await Mediator.Send(new ConfirmEmailCommand(code, accountUserId, isLinkClicked)));
        }

        [HttpGet]
        [Route("verify-phone")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> VerifyPhone()
        {
            return ResponseResultDto(await Mediator.Send(new VerifyPhoneCommand()));
        }

        [HttpGet]
        [Route("confirm-phone")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmPhone([FromQuery] string code, [FromQuery] Guid? accountUserId)
        {
            return ResponseResultDto(await Mediator.Send(new ConfirmPhoneCommand(code, accountUserId)));
        }

        /// <summary>
        /// Get user information for access databases
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountUserId:guid}/access")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<UserCardAcDbAccessesDataDto>))]
        public async Task<IActionResult> GetUserAccessDatabases([FromRoute] GetAccountUserDatabaseAccessesQuery query)
        {
            return ResponseResult(await Mediator.Send(query));
        }

        /// <summary>
        /// Get user information for access databases
        /// </summary>
        /// <param name="accountUserId">Id user</param>
        /// <param name="accountId">Id account</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountUserId:guid}/external-access")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<UserCardAcDbAccessesDataDto>))]
        public IActionResult GetUserExternalAccessDatabases([FromRoute] Guid accountUserId, [FromQuery] Guid accountId)
        {
            return ResponseResult(accountDatabaseCardManager.GetUserCardAcDbExternalAccessesDatabases(accountUserId, accountId));
        }
        #endregion

        #region POSTs

        /// <summary>
        /// Give or delete access to database
        /// </summary>
        /// <param name="startAccessesToDbForUserCardDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("access")]
        public IActionResult GiveOrDeleteAccessToAccountDatabase
            ([FromBody] StartProcessesAccessesToDbForUserCardDto startAccessesToDbForUserCardDto)
        {

            if (startAccessesToDbForUserCardDto.GiveAccess)
            {
                var result1 = startProcessToWorkDatabaseManager.StartProcessesOfAccessesforUserCard(startAccessesToDbForUserCardDto);
                return ResponseResult(result1);
            }
            var result = startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccessesForUserCard(startAccessesToDbForUserCardDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Add user to account
        /// </summary>
        /// <param name="command">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountUserDto>))]
        public async Task<IActionResult> Add([FromBody] AccountUserAddCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        [HttpPost]
        [Route("confirm-phone-no-code")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> ConfirmPhone([FromBody] СonfirmUserDataDto data)
        {
            return ResponseResultDto(await Mediator.Send(new ConfirmPhoneNoCodeCommand(data.isVerified, data.AccountUserId)));
        }

        [HttpPost]
        [Route("confirm-email-no-code")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> ConfirmEmail([FromBody] СonfirmUserDataDto data)
        {
            return ResponseResultDto(await Mediator.Send(new ConfirmEmailNoCodeCommand(data.isVerified, data.AccountUserId)));
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit user
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        [Authorize]
        public async Task<IActionResult> Edit([FromBody] AccountUserEditCommand model)
        {
            return ResponseResultDto(await Mediator.Send(model));
        }

        #endregion

    }
}
