﻿using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.AccountUserBitDepthContext;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1;


[Route("bit-depth")]
[ApiVersion("1.0")]
public class AccountUserBitDepthController : BaseV1Controller
{
    [HttpPut] 
    public async Task<IActionResult> SaveUserBitDepth([FromBody] SaveAccountUserBitDepthCommand command) 
        => ResponseResultDto(await Mediator.Send(command));
}
