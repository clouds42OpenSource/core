﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using Asp.Versioning;
using Clouds42.Accounts.Stripe.Helpers;
using Clouds42.Accounts.Stripe.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Navigation;
using Clouds42.Domain.Access;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using ViewContext = Microsoft.AspNetCore.Mvc.Rendering.ViewContext;

namespace Core42.Controllers.v1
{
    [Route("stripe")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class StripeController(
        StripeManager stripeManager,
        StripeOptionsMapper stripeOptionsMapper,
        ICompositeViewEngine viewEngine,
        IHttpContextAccessor httpContextAccessor,
        IAccessProvider accessProvider,
        IWebHostEnvironment webHostEnvironment,
        IAccountUserProvider accountUserProvider)
        : BaseV1Controller
    {
        [HttpGet]
        [Route("js")]
        public IActionResult GetJsStripe([FromQuery] StripeOptionsDto stripeOptionsDto)
        {
            var contextAccountId = accessProvider.GetContextAccountId(httpContextAccessor.HttpContext?.Request.Cookies);
            var currentUser = accessProvider.GetUser();
            var result = stripeManager.GetStripeData(stripeOptionsMapper.MapToUserParametersForStripeModel(
                currentUser != null,
                currentUser?.Name,
                contextAccountId, stripeOptionsDto));

            return result.Error ? Ok(new { success = !result.Error, data = result.Message}) : Content(GenerateStringForJsFromView("Index", result.Result), "application/x-javascript", Encoding.UTF8);
        }


        [HttpGet]
        [Route("account-data")]
        public IActionResult GetModelStripe([FromQuery] StripeOptionsDto stripeOptionsDto)
        {
            var contextAccountId = accessProvider.GetContextAccountId(httpContextAccessor.HttpContext?.Request.Cookies);

            var currentUser = accessProvider.GetUser();
            var result = stripeManager.GetStripeData(stripeOptionsMapper.MapToUserParametersForStripeModel(
                currentUser != null,
                currentUser?.Name,
                contextAccountId, stripeOptionsDto));

            if(currentUser != null)
            {
                result.Result.IsCurrentUserAccountAdmin = IsAccountAdmin(currentUser);
                result.Result.СurrentAccountUserId = currentUser?.Id ?? Guid.Empty;
                var accountAdminContacts = GetAccountAdminContacts(currentUser);

                result.Result.AccountAdminEmail = accountAdminContacts.Email;
                result.Result.AccountAdminPhone = accountAdminContacts.PhoneNumber;
            }

            return Ok(new { success = !result.Error, data = result.Result });
        }

        /// <summary>
        /// Получить контакты пользователя
        /// </summary>
        /// <param name="user">У какого пользователя получить контакты</param>
        /// <returns>Контакты пользователя</returns>
        [HttpGet]
        private UserContactsModelDto GetAccountAdminContacts(IUserPrincipalDto user)
        {
            var isCurrentUserAccountAdmin = IsAccountAdmin(user);
            var currentAccountId = user?.RequestAccountId ?? Guid.Empty;

            if (user != null && isCurrentUserAccountAdmin)
            {
                return new UserContactsModelDto
                {
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber
                };
            }
            else
            {
                return GetFirstAccountAdminContacts(currentAccountId);
            }
        }

        /// <summary>
        /// Проверка, является ли пользователь админом аккаунта
        /// </summary>
        /// <param name="user">Какого пользователя проверить</param>
        /// <returns>true - если админ аккаунте</returns>
        [HttpGet]
        private bool IsAccountAdmin(IUserPrincipalDto user) =>
            user != null && user.Groups.Any(g => g == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Получить контакты для первого пользователя, который является админом аккаунта
        /// </summary>
        /// <param name="accountId">Для какого аккаунта найти первого админа аккаунта и получит его контакты</param>
        /// <returns>Контакты пользователя</returns>
        [HttpGet]
        private UserContactsModelDto GetFirstAccountAdminContacts(Guid accountId)
        {
            var adminAccount = accountUserProvider.GetFirstAccountAdminFor(accountId);

            if (adminAccount != null)
            {
                return new UserContactsModelDto
                {
                    Email = adminAccount.Email,
                    PhoneNumber = adminAccount.PhoneNumber
                };
            }

            return new UserContactsModelDto();
        }
        /// <summary>
        /// Сгенерировать строку для JS из представления
        /// </summary>
        /// <param name="viewName">Название представления</param>
        /// <param name="model">Модель</param>
        /// <returns>Сгенерированная строка для JS</returns>
        private string GenerateStringForJsFromView(string viewName, object model)
        {
            var page = JavaScriptEncoder.Default.Encode(RenderViewToString(viewName, model)).Replace("$", "jQuery");
            return " var html = \"" + page + "\"; \r\n" + System.IO.File.ReadAllText(Path.Combine(webHostEnvironment.WebRootPath, "Scripts/stripe.js"));
        }

        /// <summary>
        /// Визуализировать представление в строку
        /// </summary>
        /// <param name="viewName">Название представления</param>
        /// <param name="model">Модель</param>
        /// <returns>Сгенерированную строку из представления</returns>
        private string RenderViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.ActionDescriptor.ActionName;

            using var sw = new StringWriter();
            ViewEngineResult viewResult =
                viewEngine.FindView(ControllerContext, viewName, false);

            ViewContext viewContext = new ViewContext(
                ControllerContext,
                viewResult.View,
                ViewData,
                TempData,
                sw,
                new HtmlHelperOptions()
            );

            viewResult.View.RenderAsync(viewContext);

            return sw.GetStringBuilder().ToString();
        }
    }
}
