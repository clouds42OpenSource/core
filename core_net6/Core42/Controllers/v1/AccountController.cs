﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums.Security;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.AccountContext.Commands;
using Core42.Application.Features.AccountContext.Dtos;
using Core42.Application.Features.AccountContext.Queries;
using Core42.Attributes;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using GetAccountDatabasesForMigrationQuery = Core42.Application.Features.AccountDatabaseContext.Queries.GetAccountDatabasesForMigrationQuery;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Controller for Accounts
    /// </summary>
    [Route("accounts")]
    [ApiVersion("1.0")]
    public class AccountController(
        EditAccountManager editAccountManager,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        CloudSegmentReferenceManager cloudSegmentReferenceManager,
        IAccountsManager accountsManager,
        IAccessProvider accessProvider)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get info about account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountDetailsDto>))]
        public async Task<IActionResult> ById([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountByIdQuery(id)));
        }

        /// <summary>
        /// Get information about the account to display in the card
        /// </summary>
        /// <param name="index">Index number of the account</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{index:int}/details")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountCardDataDto>))]
        public async Task<IActionResult> ByIndexNumber([FromRoute] int index)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountByIndexNumberQuery(index)));
        }

        /// <summary>
        /// Get account identifier by query params
        /// </summary>
        /// <param name="query">Search line to search account identifier</param>
        /// <returns></returns>
        [HttpGet("id")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        [ExternalService(ExternalServiceTypeEnum.Corp)]
        [AllowAnonymous]
        public async Task<IActionResult> IdByFilters([FromQuery] GetAccountIdByFilterQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get filtered accounts
        /// </summary>
        /// <param name="query">Query params</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AccountDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredAccountsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// get available account segments 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}/segment")]
        public IActionResult Segment(Guid id)
        {
            var segment = accountConfigurationDataProvider.GetAccountSegment(id);

            var result = cloudSegmentReferenceManager.GetAvailableMigrationSegmentsWithCurrent(segment.ID);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// get available account database
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("database-migration")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AccountDatabasesMigrationResultDto>))]
        public async Task<IActionResult> DatabaseMigration([FromQuery] GetAccountDatabasesForMigrationQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// get accounts list by search line
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("as-list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountDictionaryDto>>))]
        public async Task<IActionResult> BySearchLine([FromQuery] GetAccountsDictionaryDtoQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Set context account by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}/set-context")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult<bool>>))]
        public IActionResult SetContextAccountId([FromRoute] Guid id)
        {
            try
            {
                accessProvider.HasAccess(ObjectAction.ControlPanel_ModeOtherAccount, () => id);
                accessProvider.SetContextAccount(id);
                return ResponseResultDto(new ManagerResult { State = ManagerResultState.Ok });
            }

            catch (Exception ex)
            {
                return ResponseResultDto(new ManagerResult { State = ManagerResultState.PreconditionFailed, Message = ex.Message });
            }
        }

        /// <summary>
        /// Remove current context account
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("remove-context")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult<bool>>))]
        public IActionResult RemoveContextAccountId()
        {
            try
            {
                accessProvider.SetCurrentContextAccount(HttpContext.Response.Cookies);
                return ResponseResultDto(new ManagerResult { State = ManagerResultState.Ok });
            }
            catch (Exception ex)
            {
                return ResponseResultDto(new ManagerResult { State = ManagerResultState.PreconditionFailed, Message = ex.Message });
            }
        }

        /// <summary>
        /// Get account connection settings
        /// </summary>
        /// <returns>Account settings model</returns>
        [HttpGet("connection-settings")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountSettingsDataDto>))]
        public async Task<IActionResult> ConnectionSettings()
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountConnectionSettingsQuery
            {
                AccountUserId = (await accessProvider.GetUserAsync()).Id
            }));
        }

        /// <summary>
        /// Get summary statistics about accounts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("footer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountFooterModel>))]
        public IActionResult Summary()
        {
            var result = GetSummaryData("FooterModel");
            return ResponseResultDto(new ManagerResult<AccountFooterModel> { Result = result, State = ManagerResultState.Ok });
        }

        #endregion

        #region PUTs

        /// <summary>
        /// change account info
        /// </summary>
        /// <param name="accountDetails"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("info")]
        public IActionResult Info([FromBody] AccountDetailsModelDto accountDetails)
        {
            if (!ModelState.IsValid)
            {
                var errorMessages = string.Join(", ", ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage));
                return ResponseResult(PagedListExtensions.ToPreconditionFailedManagerResult<bool>(errorMessages));
            }
            var result = editAccountManager.UpdateAccount(accountDetails.MapToEditAccountDc());

            return ResponseResult(result);
        }
        
        /// <summary>
        /// change account segment
        /// </summary>
        /// <param name="changeSegment"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("segment")]
        public IActionResult Segment([FromBody] ChangeSegmentDto changeSegment)
        {
            var result = editAccountManager.ChangeSegment(changeSegment.accountId, changeSegment.segmentId, true);
            return ResponseResult(result);
        }

        /// <summary>
        /// migrate databases to ather storage 
        /// </summary>
        /// <param name="accountDatabaseMigration"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("database-migration")]
        public IActionResult DatabaseMigration([FromBody] AccountDatabaseMigrationDto accountDatabaseMigration)
        {
            if (!accountDatabaseMigration.databases.Any())
            {
                return ResponseResult(new ManagerResult { State = ManagerResultState.NotFound, Message = "No selected bases" });
            }

            var result = accountsManager.MigrateDatabaseToStorage(accountDatabaseMigration.storage, accountDatabaseMigration.databases);

            return ResponseResult(result);
        }
        #endregion

        /// <summary>
        /// Send Delete Account Confirmation Email
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("{accountId:guid}/delete-confirmation")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> SendDeleteAccountConfirmationEmail([FromRoute] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new DeactivateAccountCommand {AccountId = accountId }));
        }

        /// <summary>
        /// confirm account deletion 
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("confirm-deletion")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> ConfirmAccountDeactivation([FromBody] string confirmationCode) 
        {
            return ResponseResultDto(await Mediator.Send(new ConfirmDeactivateAccountCommand { ConfirmationCode = confirmationCode }));
        }

        #region Supporting methods
        /// <summary>
        /// Get account summary data in footer
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        [NonAction]
        private AccountFooterModel GetSummaryData(string cacheKey)
        {
            if (!CacheHelper.Exists(cacheKey))
            {
                var model = accountsManager.GetFooterViewModel();
                CacheHelper.Add(model, cacheKey);
                return model;
            }
            else
            {
                return CacheHelper.Get<AccountFooterModel>(cacheKey);
            }
        }
        #endregion
    }
}

