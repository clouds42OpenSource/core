﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Segment.CloudEnterpriseServer.Managers;
using Core42.Application.Features.EnterpriseServerContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using EnterpriseServerDto = Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer.EnterpriseServerDto;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("enterprise-servers")]
    public class EnterpriseServersController(EnterpriseServersManager enterpriseServersManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted publish node
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted publish node</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<EnterpriseServerDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredEnterpriseServersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get publish node by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<EnterpriseServerDto>))]
        public async Task<IActionResult> ById([FromRoute] GetEnterpriseServerByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new publish node
        /// </summary>
        /// <param name="dto">publish node</param>
        [HttpPost]
        public IActionResult Add([FromBody] EnterpriseServerDto dto)
        {
            return ResponseResultDto(enterpriseServersManager.AddNewEnterpriseServer(dto));
        }

        /// <summary>
        /// Delete publish node
        /// </summary>
        /// <param name="id">publish node</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(enterpriseServersManager.DeleteEnterpriseServer(id));
        }

        /// <summary>
        /// Change publish node
        /// </summary>
        /// <param name="dto">Changed publish node</param>
        [HttpPut]
        public IActionResult Edit([FromBody] EnterpriseServerDto dto)
        {
            return ResponseResultDto(enterpriseServersManager.EditEnterpriseServer(dto));
        }
    }
}
