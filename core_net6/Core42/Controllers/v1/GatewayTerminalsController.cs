﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Segment.CloudsServicesGatewayTerminal.Managers;
using Core42.Application.Features.GatewayTerminalContext.Dtos;
using Core42.Application.Features.GatewayTerminalContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("gateway-terminals")]
    public class GatewayTerminalsController(CloudGatewayTerminalManager cloudGatewayTerminalManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted terminal gateway terminals
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted terminal gateway terminals</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<GatewayTerminalDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredGatewayTerminalsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get terminal gateway terminal by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<GatewayTerminalDto>))]
        // ReSharper disable once RouteTemplates.RouteParameterIsNotPassedToMethod
        public async Task<IActionResult> ById([FromRoute] GetGatewayTerminalByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new gateway terminal
        /// </summary>
        /// <param name="gatewayTerminalDto">gateway terminal</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudGatewayTerminalDto gatewayTerminalDto)
        {
            return ResponseResultDto(cloudGatewayTerminalManager.CreateCloudGatewayTerminal(gatewayTerminalDto));

        }

        /// <summary>
        /// Delete gateway terminal
        /// </summary>
        /// <param name="id">gateway terminal id</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(cloudGatewayTerminalManager.DeleteCloudGatewayTerminal(id));
        }

        /// <summary>
        /// Change gateway terminal
        /// </summary>
        /// <param name="gatewayTerminalDto">Changed gateway terminal</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudGatewayTerminalDto gatewayTerminalDto)
        {
            return ResponseResultDto(cloudGatewayTerminalManager.EditCloudGatewayTerminal(gatewayTerminalDto));
        }
    }
}
