﻿using System;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.AccountFileContext.Queries;
using Core42.Application.Features.AccountFileContext.Commands;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Controller for account files
    /// </summary>
    [Route("account-files")]
    [ApiVersion("1.0")]
    public class AccountFilesController : BaseV1Controller
    {
        /// <summary>
        /// create account file
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAccountFile([FromForm] CreateAccountFileCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// delete account file
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="fileId">Идентификатор файла</param>
        /// <returns></returns>
        [HttpDelete("/accounts/{accountId:guid}/account-files/{fileId:guid}")]
        public async Task<IActionResult> DeleteAccountFile([FromRoute] Guid accountId, [FromRoute] Guid fileId)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteAccountFileCommand(accountId, fileId)));
        }

        /// <summary>
        /// Get account file by id
        /// </summary>
        /// <returns>Account settings model</returns>
        [HttpGet("/accounts/{accountId:guid}/account-files/{fileId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> GetAccountFileById([FromRoute] Guid accountId, [FromRoute] Guid fileId, [FromQuery] bool download)
        {
            var fileManagerResult = await Mediator.Send(new GetAccountFileByIdQuery(accountId, fileId));

            if (fileManagerResult.Error)
            {
                return ResponseResultDto(fileManagerResult);
            }

            Response.Headers.Add("Content-Disposition", $"{(download ? "attachment" : "inline")}; filename={fileManagerResult.Result.FileName}");

            return File(fileManagerResult.Result.Content, fileManagerResult.Result.ContentType);
        }

        /// <summary>
        /// Get account files to confirmation
        /// </summary>
        /// <returns>Account settings model</returns>
        [HttpGet("confirmations")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AccountFileDto>>))]
        public async Task<IActionResult> Files([FromQuery] GetAccountFilesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// change account file status
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("confirmations/status-change")]
        public async Task<IActionResult> ChangeAccountFileStatus([FromBody] ChangeAccountFileStatusCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }
    }
}
