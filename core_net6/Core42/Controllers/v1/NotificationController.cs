﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Contracts.Features.NotificationContext.Commands;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using Core42.Application.Features.NotificationContext.Commands;
using Core42.Application.Features.NotificationContext.Queries;
using Core42.Attributes;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [Route("notifications")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class NotificationController : BaseV1Controller
    {
        /// <summary>
        /// Get filtered notifications
        /// </summary>
        /// <param name="query">Filter and order query model</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<NotificationDto>>))]
        [TokenAuthorize]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredNotificationsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Create notification
        /// </summary>
        /// <param name="command">Command for create notification</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<NotificationDto>>))]
        [TokenAuthorize]
        public async Task<IActionResult> Create([FromBody] GlobalSendNotificationsCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// Mark notification is read
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("mark-is-read")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<NotificationDto>>))]
        [TokenAuthorize]
        public async Task<IActionResult> MarkIsRead([FromBody] MarkIsReadNotificationsCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// Delete notification
        /// </summary>
        /// <param name="messageIds"></param>
        /// <param name="accountUserId"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        [TokenAuthorize]
        public async Task<IActionResult> Delete([FromQuery] List<Guid> messageIds, [FromQuery] Guid? accountUserId = null)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteNotificationCommand(messageIds, accountUserId)));
        }
    }
}
