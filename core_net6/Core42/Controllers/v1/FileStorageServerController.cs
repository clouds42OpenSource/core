﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;
using Core42.Application.Features.FileStorageServerContext.Dtos;
using Core42.Application.Features.FileStorageServerContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("file-storage-servers")]
    public class FileStorageServerController(ICloudFileStorageServerManager fileStorageServerManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted publish node
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted publish node</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<FileStorageServerDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredFileStorageServersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get publish node by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<FileStorageServerDto>))]
        public async Task<IActionResult> ById([FromRoute] GetFileStorageServerByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new publish node
        /// </summary>
        /// <param name="dto">publish node</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudFileStorageServerDto dto)
        {
            return ResponseResultDto(fileStorageServerManager.CreateCloudFileStorageServer(dto));
        }

        /// <summary>
        /// Delete publish node
        /// </summary>
        /// <param name="id">publish node</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(fileStorageServerManager.DeleteCloudFileStorageServer(id));
        }

        /// <summary>
        /// Change publish node
        /// </summary>
        /// <param name="dto">Changed publish node</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudFileStorageServerDto dto)
        {
            return ResponseResultDto(fileStorageServerManager.EditCloudFileStorageServer(dto));
        }
    }
}
