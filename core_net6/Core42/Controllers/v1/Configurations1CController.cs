﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.AccountDatabase._1C.Managers;
using PagedListExtensionsNetFramework;
using System.Threading.Tasks;
using System.Collections.Generic;
using Asp.Versioning;
using Core42.Application.Features.Configuration1CContext.Command;
using Core42.Application.Features.Configuration1CContext.Dtos;
using Core42.Application.Features.Configuration1CContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with configurations
    /// </summary>
    [Route("configuration-1c")]
    [ApiVersion("1.0")]
    public class Configurations1CController(Configurations1CDataManager configurations1CDataManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get 1C configuration filtered
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<Configurations1CDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetConfiguration1CFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get 1C configuration names
        /// </summary>
        /// <returns></returns>
        [HttpGet("names/as-list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<string>>))]
        public async Task<IActionResult> Names()
        {
            return ResponseResultDto(await Mediator.Send(new GetConfiguration1CNamesQuery()));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add 1C configuration
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AddOrUpdateConfiguration1CResultDto>))]
        public IActionResult Add([FromBody] AddOrUpdateConfiguration1CDataItemDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = configurations1CDataManager.AddConfiguration1C(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit 1C configuration
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AddOrUpdateConfiguration1CResultDto>))]
        public  async Task<IActionResult> Edit([FromBody] EditConfiguration1CCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete 1C configuration
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Remove([FromBody] DeleteConfiguration1CDataItemDto dto)
        {
            var result = configurations1CDataManager.DeleteConfiguration(dto);
            return ResponseResultDto(result);
        }

        #endregion

    }
}
