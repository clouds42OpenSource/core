﻿using Core42.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace Core42.Controllers.v1
{
    [Authorize]
    public class BaseV1Controller : BaseController
    {
    }
}
