﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Segment.PublishNode.Managers;
using Core42.Application.Features.BaseContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Dtos;
using Core42.Application.Features.PublishNodeContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("publish-nodes")]
    public class PublishNodesController(PublishNodesManager publishNodesManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted publish node
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted publish node</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<PublishNodeDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredPublishNodesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get publish node by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PublishNodeDto>))]
        public async Task<IActionResult> ById([FromRoute] GetPublishNodeByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new publish node
        /// </summary>
        /// <param name="dto">publish node</param>
        [HttpPost]
        public IActionResult Add([FromBody] Clouds42.DataContracts.CloudServicesSegment.PublishNode.PublishNodeDto dto)
        {
            return ResponseResultDto(publishNodesManager.AddNewPublishNode(dto));
        }

        /// <summary>
        /// Delete publish node
        /// </summary>
        /// <param name="id">publish node</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(publishNodesManager.DeletePublishNode(id));
        }

        /// <summary>
        /// Change publish node
        /// </summary>
        /// <param name="dto">Changed publish node</param>
        [HttpPut]
        public IActionResult Edit([FromBody] Clouds42.DataContracts.CloudServicesSegment.PublishNode.PublishNodeDto dto)
        {
            return ResponseResultDto(publishNodesManager.EditPublishNode(dto));
        }

        /// <summary>
        /// Get available publish nodes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("available")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<BaseAvailableDto<PublishNodeDto, PublishNodeDto>>))]
        public async Task<IActionResult> Available()
        {
            return ResponseResultDto(await Mediator.Send(new GetAvailablePublishNodesQuery()));
        }
    }
}
