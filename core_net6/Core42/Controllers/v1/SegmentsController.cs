﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudSegment;
using Clouds42.Segment.CloudsServicesSegment.Managers;
using Core42.Application.Features.SegmentContext.Dtos;
using Core42.Application.Features.SegmentContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Controller for Segments
    /// </summary>
    [Route("segments")]
    [ApiVersion("1.0")]
    public class SegmentsController(CloudSegmentReferenceManager cloudSegmentReferenceManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted segments
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted segments</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<SegmentDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredSegmentsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get segment by id
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<SegmentExtendedDto>))]
        // ReSharper disable once RouteTemplates.RouteParameterIsNotPassedToMethod
        public async Task<IActionResult> ById([FromRoute] GetSegmentByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get filtered paginated and sorted segment accounts
        /// </summary>
        /// <param name="query.Id">Segment identifier</param>
        /// <param name="query">filters</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{filter.segmentId:guid}/accounts")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<SegmentAccountDto>>))]
        // ReSharper disable once RouteTemplates.RouteParameterIsNotPassedToMethod
        public async Task<IActionResult> GetSegmentAccounts([FromQuery] GetSegmentAccountsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get all drop-downs lists
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("drop-downs")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<SegmentAdditionalDataDto>))]
        public async Task<IActionResult> GetDropDownLists()
        {
            return ResponseResultDto(await Mediator.Send(new GetAllSegmentDropDownsListsQuery()));
        }


        /// <summary>
        /// Get all segments lists
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<SegmentsListDto>>))]
        public async Task<IActionResult> GetSegmentsLists([FromQuery] GetSegmentsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get segment short data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("segment-short-data")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<Dictionary<Guid, string>>>))]
        public async Task<IActionResult> GetSegmentsShortData()
        {
            return ResponseResultDto(await Mediator.Send(new GetSegmentsShortDataQuery()));
        }

        /// <summary>
        /// Create segment
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public Task<IActionResult> Add([FromBody] CreateCloudServicesSegmentDto dto)
        {
            return Task.FromResult(ResponseResultDto(cloudSegmentReferenceManager.AddNewSegment(dto)));
        }

        /// <summary>
        /// Edit segment
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public Task<IActionResult> Edit([FromBody] EditCloudServicesSegmentDto dto)
        {
            return Task.FromResult(ResponseResultDto(cloudSegmentReferenceManager.EditSegment(dto)));
        }

        /// <summary>
        /// Edit platform version for segments
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        [Route("edit-platform")]
        public Task<IActionResult> EditPlatformVersion([FromBody] EditPlatformVersionSegmentDto dto)
        {
            StringBuilder result = new StringBuilder();
            foreach(var segmentId in dto.SegmentIds)
            {
                result.AppendLine($"Редактирование сервиса: {cloudSegmentReferenceManager.EditPlatformVersion(dto.Stable83VersionId, segmentId)},");
            }

            return Task.FromResult(ResponseResultDto(result.ToString().ToOkManagerResult()));
        }

        /// <summary>
        /// Delete segment
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public Task<IActionResult> Delete([FromRoute] Guid id)
        {
            return Task.FromResult(ResponseResultDto(cloudSegmentReferenceManager.DeleteSegment(id)));
        }
    }
}
