﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using AutoMapper;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using Clouds42.AgencyAgreement.AgentRequisites.Managers;
using Clouds42.AgencyAgreement.AgentTransferBalanceRequest.Managers;
using Clouds42.AgencyAgreement.Contracts.Partner;
using Clouds42.AgencyAgreement.Partner;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DaData.Providers;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.DataContracts.Service.Partner.PartnerClients;
using Clouds42.DataContracts.Service.Partner.PartnerTransactions;
using Core42.Application.Features.PartnersContext.Queries;
using Core42.Application.Features.PartnersContext.Queries.Filters;
using Core42.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("partners")]
    public class PartnersController(
        IServiceProvider serviceProvider,
        IMapper mapper,
        IPartnerDataProvider partnerDataProvider)
        : BaseV1Controller
    {
        readonly BillingServiceManager _billingServiceManager = serviceProvider.GetRequiredService<BillingServiceManager>();
        readonly CreateAccountManager _createAccountManager = serviceProvider.GetRequiredService<CreateAccountManager>();
        readonly AgentRequisitesManager _agentRequisitesManager = serviceProvider.GetRequiredService<AgentRequisitesManager>();
        readonly CreateAgentRequisitesManager _createAgentRequisitesManager = serviceProvider.GetRequiredService<CreateAgentRequisitesManager>();
        readonly RemoveAgentRequisitesManager _removeAgentRequisitesManager = serviceProvider.GetRequiredService<RemoveAgentRequisitesManager>();
        readonly ApplyAgencyAgreementManager _applyAgencyAgreementManager = serviceProvider.GetRequiredService<ApplyAgencyAgreementManager>();
        readonly AgentPaymentManager _agentPaymentManager = serviceProvider.GetRequiredService<AgentPaymentManager>();
        readonly PartnerManager _partnerManager = serviceProvider.GetRequiredService<PartnerManager>();
        readonly DaDataManager _daDataManager = serviceProvider.GetRequiredService<DaDataManager>();
        readonly EditAgentRequisitesManager _editAgentRequisitesManager = serviceProvider.GetRequiredService<EditAgentRequisitesManager>();
        readonly AgentCashOutRequestManager _agentCashOutRequestManager = serviceProvider.GetRequiredService<AgentCashOutRequestManager>();
        readonly CreateAgentCashOutRequestManager _createAgentCashOutRequestManager = serviceProvider.GetRequiredService<CreateAgentCashOutRequestManager>();
        readonly ChangeAgentCashOutRequestStatusManager _changeAgentCashOutRequestStatusManager = serviceProvider.GetRequiredService<ChangeAgentCashOutRequestStatusManager>();
        readonly CreateAgentTransferBalanceRequestManager _createAgentTransferBalanseRequestManager = serviceProvider.GetRequiredService<CreateAgentTransferBalanceRequestManager>();
        readonly EditAgentCashOutRequestManager _editAgentCashOutRequestManager = serviceProvider.GetRequiredService<EditAgentCashOutRequestManager>();
        readonly PartnerClientsManager _partnerClientsManager = serviceProvider.GetRequiredService<PartnerClientsManager>();
        readonly ContextAccess _contextAccessor = serviceProvider.GetRequiredService<ContextAccess>();
        readonly RecalculationServiceCostDataManager _recalculationServiceCostDataManager = serviceProvider.GetRequiredService<RecalculationServiceCostDataManager>();
        readonly AgentDocumentFileManager _agentDocumentFileManager = serviceProvider.GetRequiredService<AgentDocumentFileManager>();
        private readonly PrintAgencyAgreementManager _printAgencyAgreementManager = serviceProvider.GetService<PrintAgencyAgreementManager>();

        #region Общий эндпоинт
        /// <summary>
        /// Get summary partner data
        /// </summary>
        [HttpGet]
        [Route("summary-data")]
        public IActionResult GetSummaryPartnerData([FromQuery] Guid accountId)
        {
            var result = _partnerManager.GetSummaryInformationForPartner(accountId);

            return ResponseResult(result);
        }
        /// <summary>
        /// Update general partner information
        /// </summary>
        [HttpGet]
        [Route("monthly-charge")]
        public async Task<IActionResult> GetMonthlyPartnerCharge([FromQuery] Guid accountId)
        {
            return ResponseResult(await partnerDataProvider.GetMonthlyCharge(accountId));
        }


        /// <summary>
        /// Accept the agency agreement for the account
        /// </summary>
        [HttpPost]
        [Route("agency-agreement/accept")]
        public IActionResult ApplyAgencyAgreement([FromBody] Guid accountId)
        {
            var result = _applyAgencyAgreementManager.Apply(accountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// The need to accept the agency agreement
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("agency-agreement/accepted")]
        public IActionResult NeedApplyAgencyAgreement([FromQuery] Guid accountId)
        {
            var result = _applyAgencyAgreementManager.NeedApplyAgencyAgreement(accountId);
            return ResponseResult(result);
        }
        #endregion

        #region Страница  Мои Клиенты
        /// <summary>
        /// Get partner clients
        /// </summary>
        [HttpGet]
        [Route("clients")]
        public IActionResult GetPartnerClients([FromQuery] PartnersClientFilterDto filter)
        {
            var result = _billingServiceManager.GetPartnerClients(filter);
            return ResponseResult(result);
        }

        /// <summary>
        /// Register a new client.
        /// </summary>        
        [HttpPost]
        [Route("client")]
        public async Task<IActionResult> CreateNewClient([FromBody] CreateNewClientDto createNewClient)
        {
            var result = await _createAccountManager.CreateAccountByReferral(createNewClient.Email, createNewClient.PhoneNumber, createNewClient.ReferralAccountId);
            return ResponseResult(result);
        }
        #endregion

        #region Страница  Мои Сервисы
        /// <summary>
        /// Get partner services
        /// </summary>
        [HttpGet]
        [Route("services")]
        public async Task<IActionResult> GetPartnerServices([FromQuery] PartnersBillingServiceFilterDto filter)
        {
            var result = await Mediator.Send(new GetPartnerServicesQuery
            {
                OrderBy =
                    !string.IsNullOrEmpty(filter.SortFieldName) && filter.SortType.HasValue
                        ? $"{filter.SortFieldName}.{filter.SortType!.ToString()!.ToLower()}"
                        : $"{nameof(PartnerBillingServiceDto.ServiceActivationDate)}.desc",
                PageNumber = filter.PageNumber,
                PageSize = filter.PageSize,
                Filter = new PartnerServiceFilter
                {
                    BillingServiceStatus = filter.BillingServiceStatus,
                    From = filter.PeriodFrom,
                    Name = filter.Name,
                    PartnerAccountId = filter.AccountId,
                    To = filter.PeriodTo,
                    SearchLine = filter.PartnerAccountData
                }
            });

            return ResponseResult(result);

        }
        #endregion

        #region Страница Реквизиты
        /// <summary>
        /// Get agent requisites
        /// </summary>
        [HttpGet]
        [Route("agent-requisites")]
        public IActionResult GetAgentRequisites([FromQuery] PartnersAgentRequisitesFilterDto filter)
        {
            var result = _agentRequisitesManager.GetAgentRequisites(filter);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get agent requisites
        /// </summary>
        [HttpGet]
        [Route("agent-requisite")]
        public IActionResult GetAgentRequisite([FromQuery] Guid agentRequisitesId)
        {
            var result = _agentRequisitesManager.GetAgentRequisitesById(agentRequisitesId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get bank details
        /// </summary>
        [HttpGet]
        [Route("bank-details")]
        public IActionResult GetBankDetails([FromQuery] string BIK)
        {
            var result = _daDataManager.GetBankDetails(BIK);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get legal entity details
        /// </summary>
        [HttpGet]
        [Route("legal-entity-details")]
        public IActionResult GetLegalEntityDetails([FromQuery] string INN)
        {
            var result = _daDataManager.GetLegalEntityDetails(INN);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get sole proprietor details
        /// </summary>
        [HttpGet]
        [Route("sole-proprietor-details")]
        public IActionResult GetSoleProprietorDetails([FromQuery] string INN)
        {
            var result = _daDataManager.GetSoleProprietorDetails(INN);
            return ResponseResult(result);
        }

        /// <summary>
        /// Change agent requisite status
        /// </summary>
        [HttpPut]
        [Route("agent-requisite-status")]
        public IActionResult ChangeAgentRequisitesStatus([FromForm] EditAgentRequisitesDto editAgentRequisitesDto)
        {
            var result = _editAgentRequisitesManager.ChangeAgentRequisitesStatus(editAgentRequisitesDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Change agent requisite
        /// </summary>
        [HttpPut]
        [Route("agent-requisite")]
        public IActionResult EditAgentRequisites([FromForm] EditAgentRequisitesInputDto editAgentRequisitesDto)
        {
            var result = _editAgentRequisitesManager.EditAgentRequisites(mapper.Map<EditAgentRequisitesDto>(editAgentRequisitesDto));
            return ResponseResult(result);
        }

        /// <summary>
        /// Creating agent requisite
        /// </summary>
        [HttpPost]
        [Route("agent-requisite")]
        public IActionResult CreateAgentRequisites([FromForm] CreateAgentRequisitesInputDto createAgentRequisitesDto)
        {
            var result = _createAgentRequisitesManager.CreateAgentRequisites(mapper.Map<CreateAgentRequisitesDto>(createAgentRequisitesDto));
            return ResponseResult(result);
        }

        /// <summary>
        /// Delete agent requisite
        /// </summary>
        [HttpDelete]
        [Route("agent-requisite")]
        public IActionResult RemoveAgentRequisites([FromQuery] Guid agentRequisitesId)
        {
            var result = _removeAgentRequisitesManager.RemoveAgentRequisites(agentRequisitesId);
            return ResponseResult(result);
        }
        #endregion

        #region Страница  Транзакции
        /// <summary>
        /// Get partner transactions
        /// </summary>
        [HttpGet]
        [Route("agent-payments")]
        public IActionResult GetPartnerTransactions([FromQuery] PartnerTransactionsFilterDto filter)
        {
            var result = _billingServiceManager.GetPartnerTransactions(filter);
            return ResponseResult(result);
        }

        /// <summary>
        /// Create an agency payment
        /// </summary>
        [HttpGet]
        [Route("agent-payment")]
        public IActionResult CreateAgencyPayment([FromQuery] AgencyPaymentCreationDto agencyPaymentCreation)
        {
            var result = _agentPaymentManager.CreateAgentPayment(agencyPaymentCreation);
            return ResponseResult(result);
        }
        #endregion


        #region Страница Вывод
        /// <summary>
        /// Get agent cash out requests
        /// </summary>
        [HttpGet]
        [Route("agent-cash-out-requests")]
        public IActionResult GetAgentCashOutRequests([FromQuery] AgentCashOutRequestsFilterDto filter)
        {
            var result = _agentCashOutRequestManager.GetAgentCashOutRequests(filter);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get a list of active agency contracts
        /// </summary>
        /// <param name="accountId">Id account</param>
        /// <returns>Active agency contracts</returns>
        [HttpGet]
        [Route("agent-requisite/active-contracts")]
        public IActionResult ActiveAgentContracts([FromQuery] Guid accountId)
        {
            var result = _createAgentCashOutRequestManager.GetActiveAgentRequisites(accountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get aget report file
        /// </summary>
        [HttpGet]
        [Route("agent-report")]
        public IActionResult PrintAgentReport([FromQuery] Guid agentCashOutRequestId)
        {
            var data = _createAgentCashOutRequestManager.PrintAgentReport(agentCashOutRequestId);
            return File(data.Result.Bytes, "application/pdf");
        }

        /// <summary>
        /// Print a new agent report
        /// </summary>
        /// <returns>Pdf file for printing</returns>
        [HttpGet]
        [Route("agent-report-print")]
        public IActionResult PrintAgentReport([FromQuery] Guid accountId, [FromQuery] Guid agentRequisitesId)
        {
            var data = _createAgentCashOutRequestManager.PrintAgentReport(new EditAgentCashOutRequestDto {
                AccountId = accountId, AgentRequisitesId = agentRequisitesId });
            Response.Headers.ContentDisposition = data.Result.FileName;
            return File(data.Result.Bytes, "application/pdf");
        }

        /// <summary>
        /// get an download agent file 
        /// </summary>
        /// <returns>Pdf file for printing</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("agent-document-file")]
        public IActionResult OpenAgentDocumentFile([FromQuery] Guid agentDocumentFileId)
        {
            var data = _agentDocumentFileManager.GetAgentDocumentFile(agentDocumentFileId);

            if (data.Error)
                return NotFound($"Файла с айди {agentDocumentFileId} не найден");

            return File(data.Result.Content, data.Result.ContentType);
        }

        /// <summary>
        /// Open current agency agreement
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        [Route("open-actual-agency-agreement")]
        public IActionResult OpenActualAgencyAgreement()
        {
            var data = _printAgencyAgreementManager.PrintActualAgencyAgreement();

            if (data.Error)
                return NotFound("Файла не найден");

            return File(data.Result.Bytes, "application/pdf");
        }

        /// <summary>
        /// Check the possibility of creating a cash out request
        /// </summary>
        [HttpGet]
        [Route("agent-cash-out-request/available")]
        public IActionResult CheckAbilityToCreateCashOutRequest([FromQuery] Guid accountId)
        {
            var result = _createAgentCashOutRequestManager.CheckAbilityToCreateCashOutRequest(accountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get a model for editing a cash out request
        /// </summary>
        /// <param name="agentCashOutRequestId">ID agency agreement</param>
        /// <returns>cash out request model</returns>
        [HttpGet]
        [Route("agent-cash-out-request")]
        public IActionResult EditAgentCashOutRequest([FromQuery] Guid agentCashOutRequestId)
        {
            var result = _editAgentCashOutRequestManager.GetAgentCashOutRequestById(agentCashOutRequestId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Check the possibility of creating an cash out request to balance 
        /// </summary>
        [HttpGet]
        [Route("cash-out-to-balance/available")]
        public IActionResult CheckAbilityToCreateAgentTransferBalanseRequest([FromQuery] Guid accountId)
        {
            var result = _createAgentTransferBalanseRequestManager.CheckAbilityToCreateAgentTransferBalanceRequest(accountId);
            return ResponseResult(result);

        }

        /// <summary>
        /// Get the available amount to transfer the agent's balance
        /// </summary>
        [HttpGet]
        [Route("cash-out-to-balance/available-sum")]
        public IActionResult GetAvailableSumForAgentTransferBalanceRequest([FromQuery] Guid accountId)
        {
            var result = _createAgentTransferBalanseRequestManager.GetAvailableSumForAgentTransferBalanceRequest(accountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Create a request to transfer the agent's balance
        /// </summary>
        [HttpPost]
        [Route("cash-out-to-balance")]
        public IActionResult CreateAgentTransferBalanceRequest([FromBody] CreateAgentTransferBalanseRequestDto createAgentTransferBalanceRequestDto)
        {
            var result = _createAgentTransferBalanseRequestManager.Create(createAgentTransferBalanceRequestDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Create agent cash out request
        /// </summary>
        [HttpPost]
        [Route("agent-cash-out-request")]
        public IActionResult CreateAgentCashOutRequest([FromForm] CreateAgentCashOutRequestInfoDto createAgentCashOutRequest)
        {
            var dto = mapper.Map<CreateAgentCashOutRequestDto>(createAgentCashOutRequest);
            var result = _createAgentCashOutRequestManager.CreateAgentCashOutRequest(dto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Change cash out reguest status
        /// </summary>
        [HttpPut]
        [Route("agent-cash-out-request/status")]
        public IActionResult ChangeAgentCashOutRequestStatus([FromBody] ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus)
        {
            var result = _changeAgentCashOutRequestStatusManager.ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatus);
            return ResponseResult(result);
        }

        /// <summary>
        /// Edit cash out reguest
        /// </summary>
        /// <param name="editAgentCashOutRequest">cash out reguest mode</param>
        /// <returns>The result of the operation</returns>
        [HttpPut]
        [Route("agent-cash-out-request")]
        public IActionResult EditAgentCashOutRequest([FromForm] EditAgentCashOutRequestInputDto editAgentCashOutRequest)
        {
            var result = _editAgentCashOutRequestManager.EditAgentCashOutRequest(mapper.Map<EditAgentCashOutRequestDto>(editAgentCashOutRequest));
            return ResponseResult(result);
        }

        /// <summary>
        /// Delete cash out reguest
        /// </summary>
        [HttpDelete]
        [Route("agent-cash-out-request")]
        public IActionResult RemoveAgentCashOutRequest([FromQuery] Guid agentCashOutRequestId)
        {
            var result = _agentCashOutRequestManager.RemoveAgentCashOutRequest(agentCashOutRequestId);
            return ResponseResult(result);
        }

        #endregion

        #region Дополнительные поинты для партнерки в админке


        /// <summary>
        /// Search an account to create a partner
        /// </summary>
        [HttpGet]
        [Route("account")]
        public IActionResult SearchAccounts([FromQuery] string searchLine)
        {
            var result = _partnerClientsManager.SearchAccounts(searchLine);
            return ResponseResult(result);
        }

        /// <summary>
        /// Bind Account To Partner
        /// </summary>
        [HttpPost]
        [Route("bind-account")]
        public IActionResult BindAccountWithPartner([FromBody] AccountsBindingDto accountsBinding)
        {
            var result = _partnerClientsManager.BindAccountToPartner(accountsBinding, _contextAccessor.ContextAccountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Unbind Account from Partner
        /// </summary>
        [HttpPost]
        [Route("unbind-account")]
        public IActionResult UnBindAccountFromPartner([FromBody] Guid clintId)
        {
            var result = _partnerClientsManager.UnBindAccountFromPartner(clintId, _contextAccessor.ContextAccountId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Search services by name
        /// </summary>
        [HttpGet]
        [Route("services-by-name")]
        public IActionResult SearchServicesByName([FromQuery] string searchLine)
        {
            var result = _billingServiceManager.SearchServicesByName(searchLine);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get recalculation service cost data
        /// </summary>
        [HttpGet]
        [Route("recalculation-service-cost")]
        public IActionResult GetRecalculationServiceCostData([FromQuery]RecalculationServiceCostFilterDto filter)
        {
            var result = _recalculationServiceCostDataManager.GetData(filter);
            return ResponseResult(result);
        }

        #endregion

    }
}

