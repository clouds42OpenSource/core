﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Core42.Application.Features.TerminalServerContext.Dtos;
using Core42.Application.Features.TerminalServerContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("terminal-servers")]
    public class TerminalServersController(CloudTerminalServerManager terminalServerManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted terminal servers
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted terminal servers</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<TerminalServerDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredTerminalServersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get terminal server by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<TerminalServerDto>))]
        // ReSharper disable once RouteTemplates.RouteParameterIsNotPassedToMethod
        public async Task<IActionResult> ById([FromRoute] GetTerminalServerByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new server
        /// </summary>
        /// <param name="terminalServerDto">server</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudTerminalServerDto terminalServerDto)
        {
            return ResponseResultDto(terminalServerManager.CreateCloudTerminalServer(terminalServerDto));

        }

        /// <summary>
        /// Delete server
        /// </summary>
        /// <param name="id">server id</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(terminalServerManager.DeleteCloudTerminalServer(id));
        }

        /// <summary>
        /// Change server
        /// </summary>
        /// <param name="terminalServerDto">Changed server</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudTerminalServerDto terminalServerDto)
        {
            return ResponseResultDto(terminalServerManager.EditCloudTerminalServer(terminalServerDto));
        }
    }
}
