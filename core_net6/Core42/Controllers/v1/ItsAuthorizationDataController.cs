﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.AccountDatabase.ITS.Managers;
using PagedListExtensionsNetFramework;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.ItsAuthorizationDataContext.Queries;
using Core42.Application.Features.ItsAuthorizationDataContext.Dtos;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with its auth data
    /// </summary>
    [Route("its-authorization-data")]
    [ApiVersion("1.0")]
    public class ItsAuthorizationDataController(ItsAuthorizationDataManager availableItsManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get ITS authorization data
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<ItsAuthorizationDataDto>>))]
        public async Task<IActionResult> ItsAuthorizationData([FromQuery] GetAuthorizationDataFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get ITS authorization data by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ItsAuthorizationDataDto>))]
        public async Task<IActionResult> ById([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetItsAuthorizationByIdQuery(id)));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Create ITS authorization data
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Create([FromBody] Clouds42.DataContracts.Configurations1c.ItsAuthorization.ItsAuthorizationDataDto dto)
        {
            var result = availableItsManager.CreateItsAuthorizationData(dto);
            return ResponseResultDto(result);
        }


        #endregion

        #region PUTs

        /// <summary>
        /// Edit ITS auth data
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] Clouds42.DataContracts.Configurations1c.ItsAuthorization.ItsAuthorizationDataDto dto)
        {
            var result = availableItsManager.EditItsAuthorizationData(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete ITS authorization data
        /// </summary>
        /// <param name="id">Data identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult RemoveData([FromRoute] Guid id)
        {
            var result = availableItsManager.DeleteItsAuthorizationData(id);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
