﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.GoogleCloud.SignedLinks;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1;

[ApiVersion("1.0")]
[Route("signed-urls")]
public class SignedUrlsController : BaseV1Controller
{

    private readonly ITemporaryUrlService _temporaryUrlService;

    public SignedUrlsController(ITemporaryUrlService temporaryUrlService)
    {
        _temporaryUrlService = temporaryUrlService;
    }

    [HttpGet]
    public async Task<IActionResult> GetLink(Guid backupId) => ResponseResult(await _temporaryUrlService.GetSignedUrl(backupId));
           
    
}
