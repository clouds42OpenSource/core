﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Core42.Models.PrintedHtmlForm;
using Clouds42.AgencyAgreement.PrintedHtmlForm.Managers;
using System.Threading.Tasks;
using Core42.Application.Features.PrintedHtmlFormContext.Queries;
using Core42.Application.Features.PrintedHtmlFormContext.Dtos;
using System.Collections.Generic;
using Asp.Versioning;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with printed html form references
    /// </summary>
    [Route("printed-html-form-references")]
    [ApiVersion("1.0")]
    public class PrintedHtmlFormReferenceController(
        PrintedHtmlFormManager printedHtmlFormManager,
        CreateAndEditPrintedHtmlFormManager createAndEditPrintedHtmlFormManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get filtered, sorted and paginated html form references
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PrintedHtmlFormDto>))]
        public async Task<IActionResult> PrintedHtmForms([FromQuery] GetFilteredPrintedHtmlFormsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get HTML print form by identifier
        /// </summary>
        /// <param name="id">Identifier of HTML print form</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PrintedHtmlFormDto>))]
        public async Task<IActionResult> PrintedHtmlForm([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetPrintedHtmlFormByIdQuery(id)));
        }

        /// <summary>
        /// Get all printed html model types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("model-types")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<Dictionary<string, string>>>))]
        public async Task<IActionResult> ModelTypes()
        {
            return ResponseResultDto(await Mediator.Send(new GetPrintedHtmlFormModelTypesQuery()));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Get PDF with test data
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("test-data")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TestData([FromForm] EditPrintedHtmlFormViewModel viewModel)
        {
            var result = printedHtmlFormManager.GetDocumentWithTestDataForPrintedHtmlForm(viewModel.FillAttachmentFiles().MappingToDataContract());

            if (!result.Error)
                return File(result.Result.Bytes, System.Net.Mime.MediaTypeNames.Application.Pdf, result.Result.FileName);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// Create HTML print form
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>Identifier of created HTML print form</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid?>))]
        public IActionResult Create([FromForm] CreatePrintedHtmlFormViewModel viewModel)
        {
            var dataContract = viewModel.FillAttachmentFiles().MappingToDataContract();
            var data = createAndEditPrintedHtmlFormManager.CreateOrChangePrintedHtmlForm(dataContract);
            return ResponseResultDto(data);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit HTML printed form
        /// </summary>
        /// <param name="viewModel">Parameters model</param>
        /// <returns>Edited HTML print form identifier</returns>
        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid?>))]
        public IActionResult Edit([FromForm] EditPrintedHtmlFormViewModel viewModel)
        {
            var dataContract = viewModel.FillAttachmentFiles().MappingToDataContract();
            dataContract.Id = viewModel.Id;
            var data = createAndEditPrintedHtmlFormManager.CreateOrChangePrintedHtmlForm(dataContract);
            return ResponseResultDto(data);
        }

        #endregion

        #region DELETEs



        #endregion
    }
}
