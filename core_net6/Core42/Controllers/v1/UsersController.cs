﻿using System;
using Core42.Application.Features.AccountUsersContext.Commands;
using Core42.Models;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    [Route("users")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class UsersController : BaseV1Controller
    {
        [HttpPost]
        [Route("register")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountUserRegisterDto>))]
        public async Task<IActionResult> Registry([FromBody] RegisterNewUserCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        [HttpDelete]
        [Route("{userId:guid}/accounts/{accountId:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> DeleteUser([FromRoute] Guid userId, [FromRoute] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteUserCommand(userId, accountId)));
        }
    }
}
