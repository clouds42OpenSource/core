﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Domain.Enums.Security;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.InvoiceContext.ActContext.Commands;
using Core42.Application.Features.InvoiceContext.ActContext.Queries;
using Core42.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1;
[Route("closing-documents")]
[TokenAuthorize]
[ApiVersion("1.0")]
public class ClosingDocumentsController(IUnitOfWork uow) : BaseV1Controller
{
    [HttpPost]
    public async Task<IActionResult> UploadClosingDocument( [FromForm] UploadClosingDocumentCommand command) =>
        ResponseResultDto(await Mediator.Send(command));
    
    [HttpPut("change-status")]
    public async Task<IActionResult> ChangeDocumentStatus( [FromBody] ChangeClosingDocumentStatusCommand command)
        => ResponseResultDto(await Mediator.Send(command));
    
    [HttpPatch]
    [AllowAnonymous]
    [ExternalService(ExternalServiceTypeEnum.Corp)]
    [TokenAuthorize]
    public async Task<IActionResult> ChangeDocumentsStatuses([FromBody] ChangeClosingDocumentStatusesCommand command) =>
        ResponseResultDto(await Mediator.Send(command));
    
    
    [HttpGet]
    [AllowAnonymous]
    [ExternalService(ExternalServiceTypeEnum.Corp)]
    [TokenAuthorize]
    public async Task<IActionResult> GetSignedClosingDocuments(GetSignedActsQuery query) =>
        ResponseResultDto(await Mediator.Send(query));


    [HttpGet("file")]
    public async Task<IActionResult> GetUploadedAct(Guid actId) =>
        ResponseResultDto((await uow.CloudFileRepository.AsQueryable().FirstOrDefaultAsync(f => f.Id == actId)).ToOkManagerResult());
}
