﻿using Microsoft.AspNetCore.Mvc;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.CoreWorkerContext.Queries;
using Core42.Application.Features.CoreWorkerContext.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with core workers
    /// </summary>
    [Route("core-workers")]
    [ApiVersion("1.0")]
    public class CoreWorkersController(ICoreWorkersDataManager coreWorkersDataManager) : BaseV1Controller
    {
        /// <summary>
        /// Get worker settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("configuration")]
        [AllowAnonymous]
        public IActionResult GetWorkerConfiguration() => CreateResult(coreWorkersDataManager.GetWorkerConfiguration(), x => x);


        /// <summary>
        /// Get all worker`s tasks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<CoreWorkerDescDto>>))]
        public async Task<IActionResult> AllWorkers()
        {
            return ResponseResultDto(await Mediator.Send(new GetAllWorkersQuery()));
        }
    }
}
