﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Core42.Application.Features.NavigationContext.Queries;
using Microsoft.AspNetCore.Mvc;
using Clouds42.Configurations.Configurations;
using Microsoft.Extensions.DependencyInjection;
using PagedListExtensionsNetFramework;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with navigation 
    /// </summary>
    [ApiVersion("1.0")]
    [Route("navigation")]
    public class NavigationController(IServiceProvider serviceProvider) : BaseV1Controller
    {
        private readonly IAccountConfigurationDataProvider _accountConfigurationDataProvider = serviceProvider.GetRequiredService<IAccountConfigurationDataProvider>();
        private readonly Lazy<string> _ruPromoSiteUrl = new(CloudConfigurationProvider.PromoSite.GetRuPromoSiteUrl);
        private readonly Lazy<string> _uaPromoSiteUrl = new(CloudConfigurationProvider.PromoSite.GetUaPromoSiteUrl);
        private readonly IAccessProvider _accessProvider = serviceProvider.GetRequiredService<IAccessProvider>();

        /// <summary>
        /// Get all left side menus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Menus()
        {
            return ResponseResultDto(await Mediator.Send(new GetMenusQuery()));
        }

        [HttpGet]
        [Route("main-page")]
        public async Task<IActionResult> MainPage()
        {
            return ResponseResultDto(await Mediator.Send(new GetMainPageDataQuery()));
        }


        [HttpGet]
        [Route("promo-page")]
        public IActionResult PromoPage()
        {
            var locale = _accountConfigurationDataProvider.GetAccountLocale(_accessProvider.ContextAccountId);

            var redirectUrl = locale.Name == LocaleConst.Ukraine
                ? _uaPromoSiteUrl.Value
                : _ruPromoSiteUrl.Value;

            return ResponseResult(redirectUrl.ToOkManagerResult());
        }
    }
}
