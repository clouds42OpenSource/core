﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Core42.enums;
using Core42.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using MediaTypeWithQualityHeaderValue = System.Net.Http.Headers.MediaTypeWithQualityHeaderValue;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Контролер для обработки запросов от React приложения 
    /// </summary>
    [Authorize]
    [Route("")]
    public class ReactSpaController(
        IUnitOfWork dbLayer,
        AccountUserSessionLoginManager accountUserSessionLoginManager,
        IAccessProvider accessProvider,
        IAccountUserProvider accountUserProvider,
        ICompositeViewEngine viewEngine,
        IWebHostEnvironment hostingEnvironment)
        : Controller
    {
        /// <summary>
        /// ID для сервиса CORE-42
        /// </summary>
        private static readonly string Clouds42ServiceId = "CORE42";

        /// <summary>
        /// Локаль по умолчанию
        /// </summary>
        private const string DefaultLocale = "ru-ru";

        /// <summary>
        /// Время кеширования HTTP ресурса в секундах.
        /// </summary>
        private readonly int _resourceCacheInSeconds = CloudConfigurationProvider.Resource.GetHttpResourceCacheTimeInSeconds();

        /// <summary>
        /// Время кеширования HTTP ресурса в секундах.
        /// </summary>
        private readonly string _siteUrl = CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();

        /// <summary>
        /// Список расширений ресурса, которые ненужно кешировать
        /// </summary>
        private readonly string[] _extensionsWithoutCacheControl = [".htm", ".html"];

        /// <summary>
        /// URL для отправки запроса в МС на проверку токена
        /// </summary>
        private readonly string _msTokenValidationUrl = CloudConfigurationProvider.Market.GetUrlToCheckToken();

        /// <summary>
        /// Название заголовка для токена в МС
        /// </summary>
        private readonly string _tokenHeaderParameterName = CloudConfigurationProvider.DbTemplateDelimiters.GetTokenHeaderParameterName();

        /// <summary>
        /// Логин для авторизации в МС
        /// </summary>
        private readonly string _msLogin = CloudConfigurationProvider.DbTemplateDelimiters.GetLoginForRegistrationZone();

        /// <summary>
        /// Пароль для авторизации  в МС
        /// </summary>
        private readonly string _msPassword = CloudConfigurationProvider.DbTemplateDelimiters.GetPasswordForRegistrationZone();

        /// <summary>
        /// Сгенерировать строку для JS из представления
        /// </summary>
        /// <returns>Сгенерированная строка для JS</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("market/get-plugin")]
        public IActionResult MarketPlugin()
        {
            const string scriptSrcTemplate = "<(?<type>script)[^<]*?\\ssrc=(?<quot>['\"])(?<result>(?>(?!\\k<quot>).)+)\\k<quot>";
            const string scriptContentTemplate = "<(?<type>script)\\>(?<result>.*?)\\<\\/script\\>";
            const string linkStylesheetTemplate = "<(?<type>link)[^<]*?\\shref=(?<quot>['\"])(?<result>(?>(?!\\k<quot>).)+)\\k<quot>\\srel=\"stylesheet\"";

            var template = $"{linkStylesheetTemplate}|{scriptContentTemplate}|{scriptSrcTemplate}";

            var appViewPath = GetAppViewPathFromAppName(ReactApp.Market);
            var indexFilePath = Path.Combine(appViewPath, "index.html");
            var content = System.IO.File.ReadAllText(indexFilePath);
            var matches = new Regex(template, RegexOptions.IgnoreCase).Matches(content);

            var stylesheets = new List<string>();
            var scripts = new List<string>();
            var scriptContents = new List<string>();
            foreach (Match match in matches)
            {
                var type = match.Groups["type"]?.Value;
                var value = match.Groups["result"]?.Value;

                if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value))
                {
                    continue;
                }

                switch (type)
                {
                    case "link":
                        stylesheets.Add($"{_siteUrl}{value}");
                        continue;

                    case "script" when value.StartsWith("/"):
                        scripts.Add($"{_siteUrl}{value}");
                        break;

                    case "script" when value.StartsWith("http"):
                        scripts.Add(value);
                        break;

                    case "script":
                        {
                            const string scriptContentHrefTemplate = "((\"|')(?<data>/[A-Za-z0-9_]{4,}/)(\"|'))+";

                            var val = new Regex(scriptContentHrefTemplate, RegexOptions.IgnoreCase | RegexOptions.Multiline)
                                .Replace(value, m =>
                                {
                                    var data = m.Groups["data"]?.Value;
                                    return $"\"{_siteUrl}{data}\"";
                                });
                            scriptContents.Add(val);
                            break;
                        }
                }
            }

            ViewBag.Stylesheets = stylesheets;
            ViewBag.ScriptContents = scriptContents;
            ViewBag.Scripts = scripts;

            ViewBag.SessionCoreToken = GetUserSessionTokenForCurrentUserForMarketInPromo();

            var currentUser = accessProvider.GetUser();

            ViewBag.currentAccountId = currentUser?.RequestAccountId ?? Guid.Empty;
            ViewBag.currentAccountUserId = currentUser?.Id ?? Guid.Empty;

            var isCurrentUserAccountAdmin = IsAccountAdmin(currentUser);
            ViewBag.isCurrentUserAccountAdmin = isCurrentUserAccountAdmin;

            var accountAdminContacts = GetAccountAdminContacts(currentUser);
          
            ViewBag.accountAdminEmail = accountAdminContacts.Email;
            ViewBag.accountAdminPhone = accountAdminContacts.PhoneNumber;

            using var sw = new StringWriter();
            var viewResult = viewEngine.GetView("/Views/MarketPromo/Index.cshtml", "/Views/MarketPromo/Index.cshtml", true);
            var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw, new HtmlHelperOptions());
            viewResult.View.RenderAsync(viewContext);

            return Ok(sw.GetStringBuilder().ToString());
        }

        /// <summary>
        /// Получить последний активный токен сессии для текущего пользователя в промо
        /// </summary>
        /// <returns>Последний активный токен сессии для текущего пользователя в промо</returns>
        [HttpGet]
        private Guid GetUserSessionTokenForCurrentUserForMarketInPromo()
        {
            var currentUserSessionToken = Guid.Empty;
            try
            {
                var currentUser = accessProvider.GetUser();
                if (currentUser != null)
                {
                    var lastActiveUserSessionToken = dbLayer.AccountUserSessionsRepository.GetLastValidTokenForUser(currentUser.Id, 1);

                    currentUserSessionToken = Guid.Empty.Equals(lastActiveUserSessionToken)
                        ? CreateUserSessionToken(currentUser.Id, currentUser.Name, "Промо сайт.")
                        : lastActiveUserSessionToken;
                }
            }
            catch
            {
                // To do nothing...
            }

            return currentUserSessionToken;
        }


        /// <summary>
        /// Экшен для открытия ReactJS приложения
        /// </summary>
        /// <remarks></remarks>
        /// <param name="openIdAuthUserId">Токен пользователя от OpenId</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="zone">Зона в МС</param>
        /// <param name="hybrid">если <c>'true'</c>, то сервис гибридный, иначе <c>'false'</c></param>
        /// <param name="configurationName">имя конфигурации</param>
        /// <returns>Файлы ReactJS приложения</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("market/get-plugin-1c")]
        public async Task<IActionResult> Market1C(
            [FromQuery(Name = "openid.auth.uid")] string openIdAuthUserId,
            [FromQuery(Name = "openid.auth.user")] string userLogin,
            [FromQuery] string zone,
            [FromQuery] bool hybrid,
            [FromQuery] string configurationName
            )
        {
            var locale = DefaultLocale;

            var isValid = await IsUserIdValid(new TokenValidationDto
            {
                User = userLogin,
                Token = openIdAuthUserId
            });

            if (!isValid)
            {
                return RedirectToAction("market", new
                {
                    locale,
                    route = "init-app",
                    error = "Token not valid"
                });
            }

            var foundUser = dbLayer.AccountUsersRepository.GetAccountUserByLogin(userLogin);
            if (foundUser == null)
            {
                return RedirectToAction("market", new
                {
                    locale,
                    route = "init-app",
                    error = $"User \"{userLogin}\" not found"
                });
            }

            return RedirectToAction("market", new
            {
                locale,
                route = "init-app",
                authData = CreateAuthenticationDataFor1C(foundUser, zone),
                hybrid,
                configurationName
            });
        }


        /// <summary>
        /// Создаёт аутентификационные данные для маркета в 1С
        /// </summary>
        /// <param name="user">Доменная модель пользователя</param>
        /// <param name="dbZone">Зона базы на разделителях</param>
        /// <returns>Строку в Base64 кодировке, содержащая аутентификационные данные для маркета в 1С</returns>
        [HttpGet]
        private string CreateAuthenticationDataFor1C(AccountUser user, string dbZone)
        {
            var isAccountAdmin =  IsAccountAdmin(user);
            var userSessionToken = CreateUserSessionToken(user.Id, user.Login, "1С клиент");
            var accountAdminContacts = GetAccountAdminContacts(user);

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
            {
                userId = user.Id,
                dbZone,
                isAdmin = isAccountAdmin,
                accountId = user.AccountId,
                coreToken = userSessionToken,
                accountAdminEmail = accountAdminContacts.Email,
                accountAdminPhone = accountAdminContacts.PhoneNumber
            })));
        }

        /// <summary>
        /// Проверка, является ли пользователь админом аккаунта
        /// </summary>
        /// <param name="user">Какого пользователя проверить</param>
        /// <returns>true - если админ аккаунте</returns>
        [HttpGet]
        private bool IsAccountAdmin(AccountUser user) =>
            user != null && user.AccountUserRoles.Any(item => item.AccountUserGroup == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Проверка, является ли пользователь админом аккаунта
        /// </summary>
        /// <param name="user">Какого пользователя проверить</param>
        /// <returns>true - если админ аккаунте</returns>
        [HttpGet]
        private bool IsAccountAdmin(IUserPrincipalDto user) =>
            user != null && user.Groups.Any(g => g == AccountUserGroup.AccountAdmin);

        /// <summary>
        /// Получить контакты пользователя
        /// </summary>
        /// <param name="user">У какого пользователя получить контакты</param>
        /// <returns>Контакты пользователя</returns>
        [HttpGet]
        private UserContactsModelDto GetAccountAdminContacts(AccountUser user)
        {
            var isAccountAdmin = IsAccountAdmin(user);

            if (isAccountAdmin)
            {
                return new UserContactsModelDto
                {
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber
                };
            }
            else
            {
                return GetFirstAccountAdminContacts(user.AccountId);
            }
        }

        /// <summary>
        /// Получить контакты пользователя
        /// </summary>
        /// <param name="user">У какого пользователя получить контакты</param>
        /// <returns>Контакты пользователя</returns>
        [HttpGet]
        private UserContactsModelDto GetAccountAdminContacts(IUserPrincipalDto user)
        {
            var isCurrentUserAccountAdmin = IsAccountAdmin(user);
            var currentAccountId = user?.RequestAccountId ?? Guid.Empty;

            if (user != null && isCurrentUserAccountAdmin)
            {
                return new UserContactsModelDto
                {
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber
                };
            }
            else
            {
                return GetFirstAccountAdminContacts(currentAccountId);
            }
        }

        /// <summary>
        /// Получить контакты для первого пользователя, который является админом аккаунта
        /// </summary>
        /// <param name="accountId">Для какого аккаунта найти первого админа аккаунта и получит его контакты</param>
        /// <returns>Контакты пользователя</returns>
        [HttpGet]
        private UserContactsModelDto GetFirstAccountAdminContacts(Guid accountId)
        {
            var adminAccount = accountUserProvider.GetFirstAccountAdminFor(accountId);

            if (adminAccount != null)
            {
                return new UserContactsModelDto
                {
                    Email = adminAccount.Email,
                    PhoneNumber = adminAccount.PhoneNumber
                };
            }

            return new UserContactsModelDto();
        }

        /// <summary>
        /// Создаёт сессию пользователя только для админа аккаунта
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userLogin"></param>
        /// <param name="clientDeviceInfo">Информация о устройстве клиента.</param>
        /// <returns>Токен сессии для администратора аккаунта или <see cref="Guid.Empty"/> - сессия не создана.</returns>
        [HttpGet]
        private Guid CreateUserSessionToken(Guid userId, string userLogin, string clientDeviceInfo) =>
            accountUserSessionLoginManager.CrossServiceLogin(new CrossServiceLoginModelDto
            {
                AccountUserId = userId,
                AccountUserLogin = userLogin,
                ClientDescription = "Открытие маркета",
                ClientDeviceInfo = clientDeviceInfo
            }).Result;

        [HttpGet]
        private async Task<bool> IsUserIdValid(TokenValidationDto model)
        {
            using var httpClient = new HttpClient();
            var response = await SubmitRequestToCheckToken(httpClient, model);
            var resultContent = await response.Content.ReadAsStringAsync();
            var result = resultContent.DeserializeFromJson<TokenValidationResultDto>();

            return result?.IsValid == true;
        }

        /// <summary>
        /// Отправить запрос на проверку токена
        /// </summary>
        /// <param name="httpClient">Каким клиентом отправить запрос</param>
        /// <param name="model">Модель для проверки токена</param>
        /// <returns>HttpResponseMessage</returns>
        /// <exception cref="NotFoundException">Вслучае, если не заполнены данные для сервиса "CORE42"</exception>
        [HttpGet]
        private Task<HttpResponseMessage> SubmitRequestToCheckToken(HttpClient httpClient, TokenValidationDto model)
        {
            var service = dbLayer.CloudServiceRepository.GetCloudService(Clouds42ServiceId);
            if (service?.ServiceToken == null)
                throw new NotFoundException($"Токен службы \"{Clouds42ServiceId}\" не найден");

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
            $"{Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_msLogin}:{_msPassword}"))}");

            var messageContent = new StringContent(model.ToJson());
            messageContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            messageContent.Headers.Add(_tokenHeaderParameterName, service.ServiceToken?.ToString());

            return httpClient.PostAsync($"{_msTokenValidationUrl}", messageContent);
        }

        [HttpGet]
        private string GetAppViewPathFromAppName(ReactApp app)
        {
            var appFolder = app.GetStringValue();
            return string.IsNullOrEmpty(appFolder)
                ? appFolder
                : Path.Combine(hostingEnvironment.ContentRootPath, "Views", "ReactSpa", appFolder);
        }

        /// <summary>
        /// Возвратить ReactJS файлы
        /// </summary>
        /// <param name="app">Приложение React</param>
        /// <param name="route">Запрашиваемый путь в ReactJS приложении</param>
        /// <param name="returnUrl">URL на который вдальнейшем нужно перейти</param>
        /// <returns>Файлы ReactJS приложения</returns>
        [HttpGet]
        private IActionResult GetReactAppFiles(ReactApp app, string route, string returnUrl)
        {
            var appViewPath = GetAppViewPathFromAppName(app);
            if (string.IsNullOrEmpty(appViewPath))
            {
                return NotFound();
            }

            if(string.IsNullOrEmpty(route))
                 route =  String.Empty;

            var previewFilePath = Path.Combine(appViewPath, route);
            var previewFileExtension = Path.GetExtension(previewFilePath);
            var hasPreviewFileExtension = !string.IsNullOrEmpty(previewFileExtension);
            var isPreviewFilePathExist = System.IO.File.Exists(previewFilePath);

            //Если файл не существует, но есть расширение, то возвращаем пустую строку
            if (!isPreviewFilePathExist && hasPreviewFileExtension)
            {
                return new ContentResult
                {
                    Content = string.Empty,
                    ContentType = GetMimeTypeFromFileExtension(previewFileExtension)
                };
            }

            //Если запрашиваеммый файл не существует, то возвращаем стартовую страницу, иначе запрашиваеммый файл 
            var resultFilePath = isPreviewFilePathExist
                ? previewFilePath
                : Path.Combine(appViewPath, "index.html");

            var resultFileExtension = Path.GetExtension(resultFilePath);
            var contentType = GetMimeTypeFromFileExtension(resultFileExtension);

            if (app == ReactApp.Spa && route.Contains("authentication") && !isPreviewFilePathExist)
            {
                CreateIdentityDataCookie(returnUrl);
            }

            ResponseWriteExpiresForResourceExtension(resultFileExtension);
            var bytes = System.IO.File.ReadAllBytes(resultFilePath);
            return new FileContentResult(bytes, contentType);
        }

        [HttpGet]
        private void ResponseWriteExpiresForResourceExtension(string resourceExtension)
        {
            if(Array.IndexOf(_extensionsWithoutCacheControl, resourceExtension) >= 0)
                return;


            Response.GetTypedHeaders().CacheControl =
            new Microsoft.Net.Http.Headers.CacheControlHeaderValue
            {
                Public = true,
                MaxAge = TimeSpan.FromSeconds(_resourceCacheInSeconds),
            };

            Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
            new string[] { "Accept-Encoding" };
        }

        /// <summary>
        /// Создать Cookie, которая содержит нужжные URL адреса при аутентификации
        /// </summary>
        /// <param name="returnUrl">URL на который нужно вернуться</param>
        [HttpGet]
        private void CreateIdentityDataCookie(string returnUrl)
        {
            var oidProviderUrl = CloudConfigurationProvider.OpenId.Sauri.GetUrlOidProvider();

            var oidReturnUrl = string.Empty;

            if (!string.IsNullOrEmpty(Request.GetDisplayUrl()))
                oidReturnUrl = SignInUrlHelper.GetSignInRedirectUrl(returnUrl, "/signin/OidRemoteServerLogonCallback", HttpContext);

            var authServerUrl = CloudConfigurationProvider.AuthServer.GetAuthServerAuthenticationUrl();

            Response.Cookies.Append(
                "identity-data",
                JsonConvert.SerializeObject(new
                {
                    oidServerUrl = oidProviderUrl,
                    oidReturnUrl,
                    authServerUrl
                }
            ), new CookieOptions { Secure = true });
        }

        /// <summary>
        /// Конвертирует экстеншен файла в mime тип
        /// </summary>
        /// <param name="ext">Экстеншен который нужно сконвертировать</param>
        /// <returns>Mime тип для переданного расширения <paramref name="ext"/>.</returns>
        [HttpGet]
        private static string GetMimeTypeFromFileExtension(string ext)
        {
            switch (ext?.ToLower())
            {
                case ".txt":
                case ".map":
                    return "text/plain";

                case ".svg":
                    return "image/svg+xml";

                case ".eot":
                    return "application/vnd.ms-fontobject";

                case ".woff2":
                    return "application/font-woff2";

                case ".woff":
                    return "font/x-woff";

                case ".ttf":
                    return "application/octet-stream";

                case ".htm":
                case ".html":
                    return "text/html";

                case ".css":
                    return "text/css";

                case ".js":
                    return "application/javascript";

                case ".json":
                    return "application/json";

                case ".png":
                    return "image/png";

                case ".ico":
                    return "image/x-icon";

                case ".gif":
                    return "image/gif";

                default:
                    return "text/plain";
            }
        }
    }
}
