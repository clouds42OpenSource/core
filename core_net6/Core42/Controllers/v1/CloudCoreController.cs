﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.CloudServices.CloudsCore.Managers;
using Core42.Models;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.CloudCore;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.CloudServicesContext.Queries;
using Core42.Application.Features.CloudCoreContext.Commands;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Configuring cloud settings
    /// </summary>
    [Route("cloud-core")]
    [ApiVersion("1.0")]
    public class CloudCoreController(CloudCoreManager cloudCoreManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get default cloud configuration
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CloudCoreDefConfigurationDto>))]
        [Route("default-configuration")]
        public async Task<IActionResult> DefaultConfiguration()
        {
            return ResponseResultDto(await Mediator.Send(new GetDefaultConfigurationQuery()));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Edit content of the notification page
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("page-notification")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult PageNotification([FromBody] CloudCorePageNotificationDto dto)
        {
            var result = cloudCoreManager.EditMainPageNotification(dto.PageNotification);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Edit default segment
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("default-segment")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult DefSegment([FromBody] CloudCoreDefSegmentDto dto)
        {
            var result = cloudCoreManager.EditDefaultSegment(dto.DefSegmentId);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Edit the time difference between Ukraine and Moscow
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("hours-difference-ua-and-msc")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult HoursDiffUaAndMsc([FromBody] CloudeCoreHoursDiffDto dto)
        {
            var result = cloudCoreManager.EditHoursDifferenceOfUkraineAndMoscow(dto.Hours);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Edit payment aggregator for Russian locale
        /// </summary>
        /// <param name="dto">Editing dto model</param>
        /// <returns>Success status (<see cref="bool"/>)</returns>
        [HttpPost]
        [Route("russian-locale-payment-aggregator")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult RussianLocalePaymentAggregator([FromBody] CloudCorePaymentAggregatorDto dto)
        {
            var result = cloudCoreManager.EditRussianLocalePaymentAggregator(dto.Aggregator);
            return ResponseResultDto(result);
        }

        /// <summary>
        /// Edit payment aggregator for Russian locale
        /// </summary>
        /// <param name="bannerImage"></param>
        /// <returns>Success status (<see cref="bool"/>)</returns>
        [HttpPost]
        [Route("banner")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> AddBannerImage(IFormFile bannerImage)
        {
            return ResponseResultDto(await Mediator.Send(new ChangeBannerImageCommand(bannerImage)));
        }

        #endregion
    }
}
