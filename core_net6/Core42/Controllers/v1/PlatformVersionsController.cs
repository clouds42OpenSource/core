﻿using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using Clouds42.Segment.PlatformVersion.Managers;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using Core42.Application.Features.PlatformVersionContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("platform-versions")]
    public class PlatformVersionsController(PlatformVersionManager platformVersionManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted platform version
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted platform version</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<PlatformVersionDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredPlatformVersionsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get platform version by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("one")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PlatformVersionDto>))]
        public async Task<IActionResult> ByVersion([FromQuery] GetPlatformVersionByVersionQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new platform version
        /// </summary>
        /// <param name="dto">platform version</param>
        [HttpPost]
        public IActionResult Add([FromBody] PlatformVersion1CDto dto)
        {
            return ResponseResultDto(platformVersionManager.AddNewPlatformVersionReference(dto));
        }

        /// <summary>
        /// Delete platform version
        /// </summary>
        /// <param name="version">platform version</param>
        [HttpDelete]
        public IActionResult Delete([FromQuery] string version)
        {
            return ResponseResultDto(platformVersionManager.DeletePlatformVersionReference(version));
        }

        /// <summary>
        /// Change platform version
        /// </summary>
        /// <param name="dto">Changed platform version</param>
        [HttpPut]
        public IActionResult Edit([FromBody] PlatformVersion1CDto dto)
        {
            return ResponseResultDto(platformVersionManager.EditExistedPlatformVersionReference(dto));
        }
    }
}
