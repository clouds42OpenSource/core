﻿using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountUsers.Contracts;
using Core42.Application.Features.EmailContext.Commands;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    [Route("emails")]
    [ApiVersion("1.0")]
    [AllowAnonymous]
    public class EmailsController(IAccountUsersManager accountUsersManager) : BaseV1Controller
    {
        [HttpGet("unsubscribe")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> Unsubscribe([FromQuery] string code)
        {
            return ResponseResultDto(await accountUsersManager.Unsubscribed(code, null));
        }

        [HttpPost("send")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<string>))]
        public async Task<IActionResult> Send([FromBody] SendEmailCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }
    }
}
