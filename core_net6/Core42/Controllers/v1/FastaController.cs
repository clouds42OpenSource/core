﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.DataContracts.Fasta;
using Clouds42.DataContracts.Service;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Fasta.Managers;
using Clouds42.Common.ManagersResults;
using Core42.Helpers;
using Core42.Models;
using Core42.Application.Features.FastaContext.Queries;
using Core42.Application.Features.FastaContext.Dtos;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with "Fasta" service
    /// </summary>
    [ApiVersion("1.0")]
    [Route("fasta")]
    public class FastaController(
        ContextAccess contextAccessor,
        ResourcesManager resourcesManager,
        FastaServiceManager fastaServiceManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get info about the "Fasta" service for the context account
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ServiceFastaViewModelDto>))]
        public async Task<IActionResult> Info()
        {
            return ResponseResultDto(await Mediator.Send(new GetFastaInfoQuery(contextAccessor.ContextAccountId)));
        }

        /// <summary>
        /// Get session token for DocLoader for the context user
        /// </summary>
        /// <returns>Account user session token as <see cref="Guid"/></returns>
        [HttpGet("doc-loader/token")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public async Task<IActionResult> GetTokenForDocLoader()
        {
            return ResponseResultDto(await Mediator.Send(new GetFastaTokenForDocLoaderQuery((await contextAccessor.Provider.GetUserAsync()).Id)));
        }

        /// <summary>
        /// Get precalculated "Fasta" service cost
        /// </summary>
        /// <param name="query">Precalculated data query model</param>
        /// <returns></returns>
        [HttpGet("cost")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<FastaPrecalculatedCostDto>))]
        public async Task<IActionResult> CalculateCost([FromQuery] GetFastaPrecalculatedCostQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get info on resources for "Fasta" service
        /// </summary>
        /// <returns></returns>
        [HttpGet("resources")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Recognition42LicensesModelDto>))]
        public async Task<IActionResult> GetResourcesInfo()
        {
            return ResponseResultDto(await Mediator.Send(new GetFastaResourcesQuery(contextAccessor.ContextAccountId)));
        }

        /// <summary>
        /// Get the demo period info of the Fasta service for the context account
        /// </summary>
        /// <returns>Demo period info model</returns>
        [HttpGet("demo-period")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManageFastaDemoPeriodDto>))]
        public async Task<IActionResult> GetFastaDemoPeriod()
        {
            return ResponseResultDto(await Mediator.Send(new GetFastaDemoPeriodQuery(contextAccessor.ContextAccountId)));
        }

        /// <summary>
        /// Get Fasta desktop program
        /// </summary>
        /// <returns></returns>
        [HttpGet("program")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> GerFastaProgram()
        {
            return await Mediator.Send(new GetFastaProgramQuery());
        }

        #endregion

        #region POSTs



        #endregion

        #region PUTs

        /// <summary>
        /// Buy "Fasta" service (pages and/or duration) 
        /// </summary>
        /// <param name="dto">Pages amount and years amount</param>
        /// <returns></returns>
        [HttpPut("buy")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Buy([FromBody] FastaBuyingDto dto)
        {
            var result = resourcesManager.BuyEsdlAndRec42(dto.PagesAmount, dto.YearsAmount);

            return result.Complete ? ResponseResultDto(new ManagerResult<bool> { State = ManagerResultState.Ok, Result = true }) :
                ResponseResultDto(new ManagerResult<bool> { State = ManagerResultState.PreconditionFailed, Result = false, Message = result.Comment });
        }

        /// <summary>
        /// Change resources for "Fasta" service
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut("resources")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> ChangeResources([FromBody] FastaResourcesDto dto)
        {
            return ResponseResultDto(await resourcesManager.ChangeFastaResources(dto));
        }

        /// <summary>
        /// Change the demo period of the Fasta service
        /// </summary>
        /// <param name="dto">Input data model</param>
        /// <returns></returns>
        [HttpPut("demo-period")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult ChangeFastaDemoPeriod([FromBody] ManageFastaDemoPeriodDto dto)
        {
            return ResponseResultDto(fastaServiceManager.ChangeFastaDemoPeriod(dto));
        }

        #endregion

        #region DELETEs



        #endregion
    }
}
