﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using Core42.Models;
using Core42.Application.Features.DbTemplateDelimitersContext.Dtos;
using Core42.Application.Features.DbTemplateDelimitersContext.Queries;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.AccountDatabase.Delimiters.Managers;
using System.Collections.Generic;
using Asp.Versioning;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with dilimiter infobase templates
    /// </summary>
    [Route("db-template-delimiters")]
    [ApiVersion("1.0")]
    public class DbTemplateDelimitersController(DbTemplateDelimitersManager dbTemplateDelimitersManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get a list of delimiter base templates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<DbTemplateDelimiterDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredDbTemplatesDelimitersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get a list of delimiter base templates
        /// </summary>
        /// <returns></returns>
        [HttpGet("as-key-value")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<DbTemplateDelimiterDictionaryDto>>))]
        public async Task<IActionResult> AsKeyValue()
        {
            return ResponseResultDto(await Mediator.Send(new GetDbTemplateDelimitersAsKeyValueQuery()));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add a delimiter infobase template
        /// </summary>
        /// <param name="dto">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Add([FromBody] DbTemplateDelimeterItemDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = dbTemplateDelimitersManager.AddDbTemplateDelimiterItem(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit a delimiter infobase template
        /// </summary>
        /// <param name="dto">Parameters model</param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Edit([FromBody] DbTemplateDelimeterItemDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = dbTemplateDelimitersManager.UpdateDbTemplateDelimiterItem(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete a delimiter infobase template
        /// </summary>
        /// <param name="dto">Configuration identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{configurationId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Remove([FromRoute] DeleteDbTemplateDelimiterDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = dbTemplateDelimitersManager.DeleteDbTemplateDelimiterItem(dto);

            return ResponseResultDto(result);
        }

        #endregion
    }
}
