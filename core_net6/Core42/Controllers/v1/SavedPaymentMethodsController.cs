﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Billing.Contracts.Payment;
using Clouds42.Billing.Payment.Managers;
using Core42.Application.Features.SavedPaymentMethodContext.Queries;
using Core42.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing saved payment methods
    /// </summary>
    [AllowAnonymous]
    [Route("saved-payments-methods")]
    public class SavedPaymentMethodsController(SavedPaymentMethodsManager savedPaymentMethodsManager) : BaseV1Controller
    {
        /// <summary>
        /// Get saved payment methods
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        [HttpGet]
        [TokenAuthorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<SavedPaymentMethodDto>))]
        public async Task<IActionResult> SavedPaymentMethodsInfoAsync([FromQuery] Guid? accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAllSavedPaymentsMethodsByAccountIdQuery(accountId)));
        }

        /// <summary>
        /// Remove saved payment method
        /// </summary>
        /// <param name="id">Payment method identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public async Task<IActionResult> DeletePaymentAsync([FromRoute] Guid id)
        {
            var managerResult = await savedPaymentMethodsManager.DeleteSavedPaymentMethodAsync(id);
            return ResponseResultDto(managerResult);
        }
    }
}
