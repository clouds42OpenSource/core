﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.CloudServices.MyDisk.Managers;
using Clouds42.DataContracts.MyDisk;
using Core42.Application.Features.MyDiskContext.Queries;
using Core42.Helpers;
using Core42.Models;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing "My Disk" service
    /// </summary>
    [Route("my-disk")]
    [ApiVersion("1.0")]
    public class MyDiskController(
        MyDiskManager myDiskManager,
        ContextAccess contextAccessor)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get "My Disk" service info by account
        /// </summary>
        /// <param name="query">Query model</param>
        /// <returns></returns>
        [HttpGet("{accountId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ServiceMyDiskDto>))]
        public async Task<IActionResult> ById([FromRoute] GetMyDiskDataQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Calculate the cost of disk resizing
        /// </summary>
        /// <param name="query">Query model</param>
        /// <returns></returns>
        [HttpGet("size-change-cost")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<MyDiskChangeTariffPreviewDto>))]
        public async Task<IActionResult> CalculateSizeChange([FromQuery] GetCalculationSizeChangeQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Check recalculation job status
        /// </summary>
        /// <param name="query">Task identifier</param>
        /// <returns></returns>
        [HttpGet("recalculation-finished")]
        public async Task<IActionResult> CheckRecalculationJobStatus([FromQuery] CheckRecalculationJobStatusQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get resources info for "My Disk" service
        /// </summary>
        [HttpGet("managment")]
        public async Task<IActionResult> MyDisk()
        {
            return ResponseResultDto(await Mediator.Send(new GetMyDiskManagmentDataQuery(contextAccessor.ContextAccountId)));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Change "My Disk" tariff (size) with a payment
        /// </summary>
        /// <param name="dto">Parameters model</param>
        /// <returns></returns>
        [HttpPost("tariff/payment")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult PayTariff([FromBody] MyDiskChangeTariffDto dto)
        {
            return ResponseResultDto(myDiskManager.ChangeMyDiskTariff(dto));
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Change tariff of My Disk
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="sizeGb">New size of disk in GB</param>
        /// <returns></returns>
        [HttpPut("tariff")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<TryChangeTariffResultDto>))]
        public IActionResult ChangeTariff([FromQuery] Guid accountId, [FromQuery] int sizeGb)
        {
            return ResponseResultDto(myDiskManager.TryChangeMyDiskTariff(accountId, sizeGb));
        }

        /// <summary>
        /// Start recalculation cost job for account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpPut("start-recalculation")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid?>))]
        public IActionResult StartRecalculationTask([FromQuery] Guid accountId)
        {
            return ResponseResultDto(myDiskManager.RecalculateAccountSizeAndTryProlong(accountId));
        }

        /// <summary>
        /// Manage "My Disk" service
        /// </summary>
        /// <param name="model">Service managment model</param>
        /// <returns></returns>
        [HttpPut("managment")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult ManageMyDisk([FromBody] ServiceMyDiskManagerDto model)
        {
            return ResponseResultDto(myDiskManager.ManageMyDisk(contextAccessor.ContextAccountId, model));
        }

        #endregion

        #region DELETEs



        #endregion
    }
}
