﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Billing.Models;
using Clouds42.Billing.Factories.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with billing accounts
    /// </summary>
    [ApiVersion("1.0")]
    [Route("billing-accounts")]
    [AllowAnonymous]
    public class BillingAccountsController(
        InvoiceActManager invoiceActManager,
        PaymentReceiptManager paymentReceiptManager,
        BillingAccountManager billingAccountManager,
        ConfirmPaymentManager confirmPaymentManager,
        PaymentsManager paymentsManager,
        IHandlerException handlerException,
        IUnitOfWork dbLayer,
        PayboxPaymentManager payboxPaymentManager,
        IAccessProvider accessProvider)
        : BaseV1Controller
    {
        #region GETs
        /// <summary>
        /// Get a pdf-file of act
        /// </summary>
        /// <param name="id">Act identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("open-act-pdf/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> OpenActPdfFile(Guid id)
        {
            var result = await invoiceActManager.BuildPdfFileForAct(id);
            if (result.Error)
                return ResponseResultDto(result);

            Response.Headers.Add("Content-Disposition", $"inline; filename={Uri.EscapeDataString(result.Result.fileName)}");

            return File(result.Result.act, System.Net.Mime.MediaTypeNames.Application.Pdf);
        }

        [HttpGet]
        [Route("download-act-pdf/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> DownloadActPdfFile(Guid id)
        {
            var result = await invoiceActManager.BuildPdfFileForAct(id);

            return result.Error ? ResponseResultDto(result) : File(result.Result.act, System.Net.Mime.MediaTypeNames.Application.Pdf, result.Result.fileName);
        }

        /// <summary>
        /// Open the form with a fiscal receipt
        /// </summary>
        /// <param name="id">Payment invoice identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("open-fiscal-receipt-pdf/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public ActionResult OpenFiscalReceiptPdfFile(Guid id)
        {
            var document = paymentReceiptManager.BuildInvoiceReceiptPdfDocument(id);
            Response.Headers.Add("Content-Disposition", $"inline; filename={document.FileName}");
            return File(document.Bytes, System.Net.Mime.MediaTypeNames.Application.Pdf);
        }

        /// <summary>
        /// Download fiscal receipt as PDF
        /// </summary>
        /// <param name="id">Payment invoice identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("download-fiscal-receipt-pdf/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public IActionResult DownloadFiscalReceiptPdfFile(Guid id)
        {
            var document = paymentReceiptManager.BuildInvoiceReceiptPdfDocument(id);
            return File(document.Bytes, System.Net.Mime.MediaTypeNames.Application.Pdf, document.FileName);
        }

        /// <summary>
        /// Open invoice pdf
        /// </summary>
        /// <param name="invoiceId">Payment invoice identifier</param>
        /// <param name="download">dowload or open file</param>
        /// <returns></returns>
        [HttpGet]
        [Route("invoice-pdf/{invoiceId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public IActionResult GetInvoicePDFFile([FromRoute]Guid invoiceId, bool download = false)
        {
            var data = billingAccountManager.BuildInvoicePdfDocument(invoiceId);

            if (data.Error)
                return ResponseResultDto(data);

            return download 
                ? File(data.Result.Bytes, System.Net.Mime.MediaTypeNames.Application.Pdf, data.Result.FileName)
                : File(data.Result.Bytes, System.Net.Mime.MediaTypeNames.Application.Pdf);
        }


        /// <summary>
        ///     Paybox перенаправляет в случае успешного или не успешного онлайн прохождения платежа
        /// </summary>
        /// <param name="model">Параметры</param>
        /// <returns>Redirect на BillingAccounts/Main с выводом сообщения</returns>
        [HttpGet]
        [Route("paybox-import-result")]
        public IActionResult PayboxImportSuccessOrFail(PayboxImportSuccessOrFailureModel model)
        {
            var payment = dbLayer.PaymentRepository
                .AsQueryableNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .FirstOrDefault(x => x.Number == model.pg_order_id);

            var signature = model.GenerateSignature(payment.Account.AccountConfiguration.SupplierId);

            if (!paymentsManager.IsPayboxSignatureValid(signature, model.pg_sig))
            {
                Logger.Trace("(PayboxImportSuccessOrFail) :: Не валидная подпись");
                TempData["Alert"] = "Paybox: ошибка при проверке платежа. Не валидная подпись.";

                return Redirect($"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/billing");
            }

            Logger.Trace($"(PayboxImportSuccessOrFail) :: Запрос платежа {model.pg_order_id} - Валиден!");

            var checkPaymentStatus =
                paymentsManager.CheckPayboxPaymentStatus(model.pg_order_id,
                    model.pg_failure_description);

            var message = checkPaymentStatus;
            TempData["Alert"] = message;

            return Redirect($"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/billing");
        }

        /// <summary>
        /// Robokassa. Успешная проплата. Коллбэк обработки платежа
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        [Route("robokassa-import-result")]
        public void RobokassaImportResult(decimal outSum, int invId, string signatureValue)
        {
            if (!new RobokassaHelper(dbLayer).CheckResponse(outSum, invId, signatureValue))
                return;

            var accountId = dbLayer.PaymentRepository.FirstOrDefault(x => x.Number == invId).AccountId;
            try
            {
                confirmPaymentManager.ConfirmPaymentAnonymous(invId, outSum, true);
                paymentReceiptManager.CreateFiscalReceiptTask(invId);
                billingAccountManager.SendRobokassaSuccessImportReport(invId);

                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Пополнение баланса через Робокассу на сумму {outSum:0.00} руб.");

                byte[] bytes = Encoding.ASCII.GetBytes("OK" + invId);
                Response.Body.WriteAsync(bytes);
                Logger.Info($"Успешное зачисление платежа {invId} на сумму {outSum:0.00} руб.");
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Ошибка пополнения баланса через Робокассу на сумму {outSum:0.00} руб.");
                Logger.Error(ex, $"Ошибка зачисление платежа {invId} на сумму {outSum:0.00} руб. Ошибка: {ex.Message}");
            }
        }

        /// <summary>
        /// Robokassa. Успешная проплата. Страница для отображения клиенту
        /// </summary>
        [HttpGet]
        [Route("robokassa-import-succes")]
        public IActionResult RobokassaImportSuccess(decimal outSum)
        {
            TempData["Success"] = $"Робокасса: платеж успешно импортирован {outSum:0.00} руб.";

            return Redirect($"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/billing");
        }

        /// <summary>
        /// Robokassa. Неуспешная проплата. Страница для отображения клиенту
        /// </summary>
        [HttpGet]
        [Route("robokassa-import-fail")]
        public IActionResult RobokassaImportFail(decimal outSum, int invId)
        {
            var confirmResult =
                confirmPaymentManager.ConfirmPayment(accessProvider.ContextAccountId, invId, outSum, false);
            if (!confirmResult.Error && confirmResult.Result == PaymentOperationResult.Ok)
            {
                Logger.Info($"Успешная отмена платежа {invId} на сумму {outSum:0.00} руб.");
                TempData["Alert"] = $"Робокасса: платеж аннулирован {outSum:0.00} руб.";
            }
            else
            {
                Logger.Info($"Ошибка при отмене платежа {invId} на сумму {outSum:0.00} руб.");
                TempData["Alert"] = $"Робокасса: ошибка при аннулировании платежа {outSum:0.00} руб.";
            }

            return Redirect($"{CloudConfigurationProvider.Cp.GetSiteAuthorityUrl()}/billing");
        }
        #endregion

        #region POSTs
        /// <summary>
        ///     Управление коллбэком Paybox, сообщают о результате платежа
        /// </summary>
        /// <param name="resultModel">Параметры</param>
        /// <returns>Ответ в виде xml строки</returns>
        [HttpPost]
        [Route("paybox-import-result")]
        public async Task<IActionResult> PayboxImportResult(PayboxImportResultModel resultModel)
        {
            var body = await LogRequestAndGetNakedBody();

            switch (Request.ContentType)
            {
                case "application/json":
                    resultModel = JsonConvert.DeserializeObject<PayboxImportResultModel>(body);
                    resultModel?.SetSourceDataCollection(RequestSourceDataCollectionHelper.GetSourceDataCollectionFromJson(body));
                    break;
                case "application/x-www-form-urlencoded":
                    resultModel?.SetSourceDataCollection(HttpContext.Request.GetSourceDataCollectionFromForm());
                    break;
            }

            var managerResult = payboxPaymentManager.PayboxImportResultAnonymous(resultModel);

            return Ok(managerResult.Error ? managerResult.Message : managerResult.Result);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ukrpay-import-result")]
        public string UkrPaysImportResult(string id_ups, string order, string amount, string date, string hash,
            string system)
        {
            try
            {
                //разделяеи на номер внутрненний номер платежа и номер аккаунта
                Logger.Info("(UkrPaysImportResult)Получение данных с параметрами id_ups: {0}, amount: {1}, order: {2}, date: {3}, hash: {4}, system: {5}",
                    id_ups, amount, order, date, hash, system);
                var clientPayData = order.Split('_');
                var payId = int.Parse(clientPayData[0]);
                var accountIndexNumber = int.Parse(clientPayData[1]);
                var accountId = dbLayer.AccountsRepository.FirstOrDefault(x => x.IndexNumber == accountIndexNumber).Id;
                var sum = decimal.Parse(amount.Replace('.', ','));
                var extNumber = int.Parse(id_ups);

                var existSuccessPayment =
                    dbLayer.PaymentRepository.FirstOrDefault(x => x.PaymentSystemTransactionId == extNumber.ToString());
                if (existSuccessPayment != null)
                    return "duplicate";
                Logger.Info("(UkrPaysImportResult)Начало обработки платежа {0} на сумму {1}", payId, amount);

                if (!UkrPaysHelper.IsValidControlKey(id_ups, order, amount, date, hash))
                {
                    //ошибка и отмена платежа
                    confirmPaymentManager.ConfirmPaymentAnonymous(payId, sum, false);
                    Logger.Info("UkrPays: ошибка при импорте платежа {0}. Контрольные суммы не совпадают!", payId);
                    LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                        $"Ошибка пополнения баланса через UkrPays на сумму {sum:0.00} грн.");
                    return "Error";
                }

                Logger.Info("(UkrPaysImportResult)запрос платежа {0} на сумму {1} Валиден!", payId, amount);

                var systemName = UkrPaysHelper.GetPaySystemValue(system);
                Logger.Info(systemName);
                paymentsManager.EditPaySystemDescription(payId, systemName);
                var confirmResult = confirmPaymentManager.ConfirmPaymentAnonymous(payId, sum, true);
                paymentsManager.SetExternalPaymentStatus(payId, extNumber);
                Logger.Info("(UkrPaysImportResult)Подтвержение платежа {0} на сумму {1} в статусе : {2}", payId, amount,
                    confirmResult);
                LogEventHelper.LogEvent(dbLayer, accountId, null, LogActions.RefillByPaymentSystem,
                    $"Пополнение баланса через UkrPays на сумму {sum} грн.");
                return "OK";
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[(UkrPaysImportResult)Ошибка платежа]");
                return "Error";
            }
        }

        /// <summary>
        /// Cancel invoices
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("cancel-invoice/{invoiceId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult CancelInvoice([FromRoute] Guid invoiceId)
        {
            return ResponseResultDto(billingAccountManager.CancelInvoice(invoiceId));
        }
        #endregion


        #region PUTs


        #endregion
    }
}
