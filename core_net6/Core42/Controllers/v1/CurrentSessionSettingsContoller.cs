﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Core42.Models;
using Core42.Models.CurrentSessionSettings;
using Clouds42.Segment.CurrentSessionSettings.Managers;
using Clouds42.DataContracts.Logging.CurrentSessionSettings;
using PagedListExtensionsNetFramework;
using Microsoft.AspNetCore.Authorization;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Interacting with current session settings
    /// </summary>
    [Route("current-session-settings")]
    [ApiVersion("1.0")]
    public class CurrentSessionSettingsController(
        CurrentSessionSettingsManager currentSessionSettingsManager,
        ContextAccess contextAccess)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get current session settings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CurrentSessionSettingsModelDto>))]
        public async Task<IActionResult> CurrentSession()
        {
            var result = await 
                currentSessionSettingsManager.GetCurrentSessionSettings(contextAccess.IsCurrentContextAccount);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// Get saved into TempData messages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("temp-data-messages")]
        public IActionResult TempDataMessages()
        {
            var result = new List<PopupMessageViewModel>();
            TryAddTempdataMessage(result, "Alert");
            TryAddTempdataMessage(result, "Success");
            return ResponseResultDto(result.ToOkManagerResult());
        }

        #endregion

        /// <summary>
        /// Add a message with a specified key into TempData collection, if the key exists
        /// </summary>
        /// <param name="collection">Specified collection</param>
        /// <param name="tempDataKey">TempData's key</param>
        [NonAction]
        private void TryAddTempdataMessage(ICollection<PopupMessageViewModel> collection,
            string tempDataKey)
        {
            var tempDatavalue = TempData[tempDataKey]?.ToString();
            if (string.IsNullOrWhiteSpace(tempDatavalue))
                return;

            collection.Add(new PopupMessageViewModel
            {
                Text = tempDatavalue,
                Type = tempDataKey
            });
        }
    }
}
