﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Core42.Models;
using Core42.Application.Features.AccountUserValidationContext.Queries;
using System.Threading.Tasks;
using Asp.Versioning;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Validating user data
    /// </summary>
    [Route("account-users-validation")]
    [ApiVersion("1.0")]
    public class AccountUsersValidationController : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// ValidateBinding user password
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <param name="passwordHash">Password hash</param>
        /// <returns></returns>
        [HttpGet]
        [Route("check-password")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public async Task<IActionResult> CheckPassword([FromQuery] Guid? userId, [FromQuery] string passwordHash)
        {
            return ResponseResultDto(await Mediator.Send(new CheckPasswordQuery(userId, passwordHash)));
        }

        /// <summary>
        /// Check email validity
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("is-email-available")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> IsEmailAvailable([FromQuery] Guid? userId, [FromQuery] string email)
        {
            return ResponseResultDto(await Mediator.Send(new CheckEmailQuery(userId, email)));
        }

        /// <summary>
        /// Check phone number validity
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <param name="phoneNumber">Phone number</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("is-phone-available")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> IsPhoneAvailable([FromQuery] Guid? userId, [FromQuery]string phoneNumber)
        {
            return ResponseResultDto(await Mediator.Send(new CheckPhoneQuery(userId, phoneNumber)));
        }

        /// <summary>
        /// Check phone number validity
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="login">Login</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("is-login-available")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> IsLoginAvailable([FromQuery] string login, [FromQuery] Guid? id)
        {
            return ResponseResultDto(await Mediator.Send(new CheckLoginQuery(login, id)));
        }

        #endregion
    }
}
