﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels;
using Clouds42.Segment.CloudServicesBackupStorage.Managers;
using Core42.Application.Features.BackupStorageContext.Dtos;
using Core42.Application.Features.BackupStorageContext.Queries;
using Core42.Application.Features.PlatformVersionContext.Dtos;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("backup-storages")]
    public class BackupStoragesController(CloudBackupStorageManager backupStorageManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted backup storage
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted backup storage</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<BackupStorageDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredBackupStoragesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get backup storage by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PlatformVersionDto>))]
        public async Task<IActionResult> ById([FromRoute] GetBackupStorageByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new backup storage
        /// </summary>
        /// <param name="dto">backup storage</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudBackupStorageDto dto)
        {
            return ResponseResultDto(backupStorageManager.CreateCloudBackupStorage(dto));
        }

        /// <summary>
        /// Delete backup storage
        /// </summary>
        /// <param name="id">backup storage</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(backupStorageManager.DeleteCloudBackupStorage(id));
        }

        /// <summary>
        /// Change backup storage
        /// </summary>
        /// <param name="dto">Changed backup storage</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudBackupStorageDto dto)
        {
            return ResponseResultDto(backupStorageManager.EditCloudBackupStorage(dto));
        }
    }
}
