﻿using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.Feedback;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1;
[Route("feedback")]
[ApiVersion("1.0")]
public class FeedbackController : BaseV1Controller
{
    
    [HttpPut]
    public async Task<IActionResult> UpdateUserFeedback([FromBody] UpdateFeedbackCommand command) 
        => ResponseResultDto(await Mediator.Send(command));
    
    
}
