﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer;
using Clouds42.Segment.CloudsServicesSQLServer.Managers;
using Core42.Application.Features.SqlServerContext.Dtos;
using Core42.Application.Features.SqlServerContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("sql-servers")]
    public class SqlServersController(CloudSqlServerManager sqlServerManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted sql server
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted sql server</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<SqlServerDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredSqlServersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get sql server by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<SqlServerDto>))]
        public async Task<IActionResult> ById([FromRoute] GetSqlServerByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new sql server
        /// </summary>
        /// <param name="dto">sql server</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudSqlServerDto dto)
        {
            return ResponseResultDto(sqlServerManager.CreateCloudSqlServer(dto));
        }

        /// <summary>
        /// Delete sql server
        /// </summary>
        /// <param name="id">sql server</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(sqlServerManager.DeleteCloudSqlServer(id));
        }

        /// <summary>
        /// Change sql server
        /// </summary>
        /// <param name="dto">Changed sql server</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudSqlServerDto dto)
        {
            return ResponseResultDto(sqlServerManager.EditCloudSqlServer(dto));
        }
    }
}
