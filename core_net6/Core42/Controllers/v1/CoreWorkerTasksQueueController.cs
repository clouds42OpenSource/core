﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Core42.Application.Features.CoreWorkerTasksQueueContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with core worker's tasks queue
    /// </summary>
    [Route("core-worker-tasks-queue")]
    [ApiVersion("1.0")]
    public class CoreWorkerTasksQueueController(ICoreWorkerTasksQueueManager coreWorkerTasksQueueManager)
        : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get information on a task from the worker's task queue
        /// </summary>
        /// <param name="queuedTaskId">Identifier of the queued task</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{queuedTaskId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<TaskInQueueItemInfoDto>))]
        public async Task<IActionResult> Info([FromRoute] Guid queuedTaskId)
        {
            return ResponseResultDto(await Mediator.Send(new GetCoreWorkerTaskQueueByIdQuery(queuedTaskId)));
        }

        /// <summary>
        /// Get data about the worker task queue
        /// </summary>
        /// <param name="query">Search filter</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<CoreWorkerTaskInQueueItemDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetCoreWorkerTasksQueueFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add task
        /// </summary>
        /// <param name="taskInfo">Task parameters model</param>
        /// <returns>Created task identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CoreWorkerTasksQueueIdDto))]
        public IActionResult Add([FromBody] CoreWorkerTasksDto taskInfo)
        {
            return CreateResult(coreWorkerTasksQueueManager.Add(taskInfo), x => new CoreWorkerTasksQueueIdDto(x));
        }

        /// <summary>
        /// Cancel queued task
        /// </summary>
        /// <param name="cancelWorkerCoreTasksQueue">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        [Route("cancel-queued")]
        public async Task<IActionResult> CancelQueued([FromBody] CancelCoreWorkerTasksQueueDto cancelWorkerCoreTasksQueue)
        {
            var result = await coreWorkerTasksQueueManager.CancelCoreWorkerTasksQueue(
                cancelWorkerCoreTasksQueue.CoreWorkerTasksQueueId,
                cancelWorkerCoreTasksQueue.ReasonCancellation);

            return ResponseResultDto(result);
        }

        #endregion
    }
}
