﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Models;
using Clouds42.AccountDatabase.DbTemplates.Managers;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Core42.Application.Features.DbTemplateContext.Dtos;
using Core42.Application.Features.DbTemplateContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with db templates
    /// </summary>
    [Route("db-templates")]
    [ApiVersion("1.0")]
    public class DbTemplateController(DbTemplatesManager dbTemplatesManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get a list of configuration names
        /// </summary>
        /// <returns>list of configuration names</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<DbTemplateDto>))]
        public async Task<IActionResult> Filtered([FromQuery] GetDbTempalteFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get a db template for edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<DbTemplateDto>))]
        [Route("{id}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetDbTemplateByIdQuery(id)));
        }

        /// <summary>
        /// Get all db templates as key-value pairs (key - template ID, value - description)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<KeyValuePair<Guid, string>[]>))]
        [Route("as-key-value")]
        public async Task<IActionResult> AllAsKeyValue()
        {
            return ResponseResultDto(await Mediator.Send(new GetDbTemplatesAsKeyValueQuery()));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add a db template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AddDbTemplateResultDto>))]
        public IActionResult Add([FromBody] DbTemplateItemDto dto)
        {
            var result = dbTemplatesManager.AddDbTemplateItem(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit a db template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Edit([FromBody] DbTemplateItemDto dto)
        {
            var result = dbTemplatesManager.UpdateDbTemplateItem(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Remove a db template
        /// </summary>
        /// <param name="dbTemplateId">Template identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{dbTemplateId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Remove([FromRoute] Guid dbTemplateId)
        {
            var result = dbTemplatesManager.DeleteDbTemplateItem(dbTemplateId);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
