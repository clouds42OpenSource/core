﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.AgencyAgreement.Industry;
using Clouds42.DataContracts.Service.Industry;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.IndustyContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with industries
    /// </summary>
    [Route("industries")]
    [ApiVersion("1.0")]
    public class IndustryController(IndustryManager industryManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get filtered, sorted and paginated industries
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AddIndustryDataItemDto>))]
        public async Task<IActionResult> Filtered([FromQuery] GetIndustryFilteredQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add industry
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AddIndustryDataItemDto>))]
        public IActionResult Add([FromBody] AddIndustryDataItemDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = industryManager.AddNewIndustry(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit industry
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] UpdateIndustryDataItemDto dto)
        {
            if (!ModelState.IsValid)
                return ModelValidation();

            var result = industryManager.EditExistingIndustry(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs

        /// <summary>
        /// Delete industry
        /// </summary>
        /// <param name="industryId"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Remove(Guid industryId)
        {
            var result = industryManager.DeleteIndustry(industryId);

            return ResponseResultDto(result);
        }

        #endregion
    }
}
