﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using AutoMapper;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.AccountDatabase.ServiceExtensions.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.BillingService.BillingServiceGet;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDatabasesService;
using Clouds42.DataContracts.Service.Partner;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Core42.Application.Features.BillingServiceContext.Commands;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using Core42.Application.Features.BillingServiceExtensionsContext.Queries;
using Core42.Application.Features.ServicesContext.Dtos;
using Core42.Application.Features.ServicesContext.MyDatabases.Queries;
using Core42.Application.Features.ServicesContext.Queries;
using Core42.Attributes;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with billing services
    /// </summary>
    [ApiVersion("1.0")]
    [Route("billing-services")]
    [AllowAnonymous]
    public class BillingServiceController(
        Rent1CConfigurationAccessManager rent1CConfigurationAccessManager,
        IAccessProvider accessProvider,
        IMapper mapper,
        Rent1CDataManagementDataManager rent1CDataManagementDataManager,
        ResourcesManager resourcesManager,
        Rent1CManager rent1CManager,
        IManageServiceExtensionDatabaseManager manageServiceExtensionDatabaseManager,
        BillingServiceManager billingServiceManager,
        RegisterAccountDatabaseByTemplateManager registerAccountDatabaseTemplateManager)
        : BaseV1Controller
    {
        #region Rent 1C
        /// <summary>
        /// Get rent 1C summary info on main page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("rent-1c")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Rent1CInfoDto>))]
        public async Task<IActionResult> GetRent1C()
        {
            return ResponseResultDto(await Mediator.Send(new GetRent1CInfoQuery()));
        }

        /// <summary>
        /// Get rent 1C summary info on main page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("rent-1c/management")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Rent1CManagerDto>))]
        public IActionResult GetRent1CManagement()
        {
            return ResponseResultDto(rent1CDataManagementDataManager.GetManagementData(accessProvider.ContextAccountId));
        }


        /// <summary>
        /// Change user rent 1c management info
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("rent-1c/management")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult ManageRent1C([FromBody] Rent1CServiceManagerDto model)
        {
            rent1CManager.ManageRent1C(accessProvider.ContextAccountId, model);
            return ResponseResultDto(new ManagerResult { State = ManagerResultState.Ok });
        }

        /// <summary>
        /// Get external user for sponsorship
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("rent-1c/external-user")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<LicenseOfRent1CDto>))]
        public async Task<IActionResult> GetExternalUserForRent1C([FromQuery] GetExternalUserForRent1CQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Try pay access for users
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("rent-1c/try-pay-access")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<UpdateAccessRent1CResultDto>))]
        public IActionResult TryPayAccess([FromBody] TryPayRent1CAccessCommand command)
        {
            return ResponseResultDto(rent1CConfigurationAccessManager.ConfigureAccesses(
                accessProvider.ContextAccountId,
                command.Accesses.Select(mapper.Map<UpdaterAccessRent1CRequestDto>).ToList()));
        }

        /// <summary>
        ///Try pay access with promise payment for users
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("rent-1c/try-pay-access-by-promise-payment")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult TryPayAccessByPromisePayment([FromBody] TryPayRent1CAccessCommand command)
        {
            return ResponseResultDto(rent1CConfigurationAccessManager.TryPayAccessByPromisePaymentForRent1C(
                accessProvider.ContextAccountId,
                command.Accesses.Select(mapper.Map<UpdaterAccessRent1CRequestDto>).ToList()));
        }

        /// <summary>
        /// Get 1C demo period data
        /// </summary>
        /// <returns>Форма для изменения демо периода Аренды 1С</returns>
        [HttpGet]
        [Authorize]
        [Route("rent-1c/demo-period")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Rent1CServiceManagerDto>))]
        public IActionResult GetRent1CDemoPeriod()
        {
            return ResponseResultDto(resourcesManager.GetRent1DemoPeriodInfo(accessProvider.ContextAccountId));
        }

        /// <summary>
        /// Change 1C demo period data
        /// </summary>
        /// <param name="model">Модель изменения Аренды 1С</param>
        [HttpPost]
        [Authorize]
        [Route("rent-1c/demo-period")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult ChangeRent1CDemoPeriod([FromBody] Rent1CServiceManagerDto model)
        {
            return ResponseResultDto(resourcesManager.ChangeRent1CDemoPeriod(accessProvider.ContextAccountId, model));
        }

        [HttpGet]
        [Authorize]
        [Route("rent-1c/turn-on")]
        public IActionResult TurnOnRent1CForCurrentAccountUser()
        {
            return ResponseResultDto(rent1CConfigurationAccessManager.TurnOnRent1C(accessProvider.ContextAccountId)); 
        }
        #endregion

        /// <summary>
        /// Get resource by id
        /// </summary>
        /// <param name="id">Resource id</param>
        [HttpGet]
        [Authorize]
        [Route("resources/{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Rent1CUserManagerDto>))]
        public IActionResult GetResource([FromRoute] Guid id)
        {
            return ResponseResultDto(resourcesManager.GetRent1CUserManagerDc(id, accessProvider.ContextAccountId));
        }

        /// <summary>
        /// Change resource
        /// </summary>
        /// <param name="rent1CUserManagerDc">Resource model</param>
        [HttpPost]
        [Authorize]
        [Route("resources")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult EditResource([FromBody] Rent1CUserManagerDto rent1CUserManagerDc)
        {
            return ResponseResultDto(resourcesManager.EditResourceCost(rent1CUserManagerDc, accessProvider.ContextAccountId));
        }

        /// <summary>
        /// Get all databases compatible with the specified billing service extensions for the account
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/extensions/compatible-databases")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<AvailableDatabaseForServiceExtensionDto>>))]
        public async Task<IActionResult> GetDatabasesCompatibleWithBillingServiceExtensions([FromQuery] GetCompatibleTemplatesForExtensionsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get status of extensions of the specified billing service for the specified account database 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/extensions/state")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<ServiceExtensionDatabaseStatusDto>))]
        public async Task<IActionResult> GetBillingServiceExtensionsForDatabaseStatus([FromQuery] GetBillingServiceExtensionsForDatabaseStatusQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Install extensions for the billing service for the databases
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("extensions")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult InstallServiceExtensionForDatabases([FromBody] InstallServiceExtensionForDatabasesDto dto)
        {
            return ResponseResultDto(manageServiceExtensionDatabaseManager.InstallServiceExtensionForDatabases(dto));
        }

        /// <summary>
        /// Remove extensions of the specified billing service from the specified database
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="accountDatabaseId">Database identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route("{id:guid}/extensions")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult RemoveBillingServiceExtensionsFromDatabase([FromRoute] Guid id, [FromQuery] Guid accountDatabaseId)
        {
            return ResponseResultDto(manageServiceExtensionDatabaseManager.DeleteServiceExtensionFromDatabase(new DeleteServiceExtensionFromDatabaseDto
            {
                ServiceId = id,
                DatabaseId = accountDatabaseId
            }));
        }

        /// <summary>
        /// Get info on the specified billing service for the context account.
        /// Using for the billing service main page
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<BillingServiceInfoDto>))]
        public async Task<IActionResult> GetBillingServiceMainInfo([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetBillingServiceForAccountQuery(id, accessProvider.ContextAccountId)));
        }

        /// <summary>
        /// Get activation info on the specified billing service for the context account.
        /// Using for the billing service activation page
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/activation")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> BillingServiceActivation([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetBillingServiceActivationQuery(id, accessProvider.ContextAccountId)));
        }

        /// <summary>
        /// Get services of the billing service for the specified account
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/services")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<IEnumerable<BillingServiceTypeInfoDto>>))]
        public async Task<IActionResult> GetServicesOfBillingService([FromRoute] Guid id, [FromQuery] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetBillingServiceInfoQuery(id, accountId)));
        }

        /// <summary>
        /// Get all active services of the specified billing service for the specified account
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/active-services")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<IEnumerable<Guid>>))]
        public async Task<IActionResult> GetActiveServicesOfBillingServiceForAccount([FromRoute] Guid id, [FromQuery] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetBillingServiceActiveServicesQuery(id, accountId)));
        }

        /// <summary>
        /// Get filtered, paginated and ordered users with active services of the billing service
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [TokenAuthorize]
        [Route("{id:guid}/users")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<PagedDto<AccountUserBillingServiceTypeDto>>))]
        public async Task<IActionResult> GetAccountUsersWithActiveServicesOfBillingService([FromQuery] GetUsersWithBillingServicesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get an account database to run the specified billing service for the specified account
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/database")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AccountDatabaseToRunDto>))]
        public async Task<IActionResult> GetAccountDatabaseToRun([FromRoute] Guid id, [FromQuery] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountDatabaseToRunQuery(id, accountId)));
        }

        /// <summary>
        /// Get an account database on delimiters to run a billing service
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("database-on-delimiters")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AccountDatabaseOnDelimitersToRunDto>))]
        public async Task<IActionResult> GetAccountDatabaseOnDelimitersToRun([FromQuery] GetAccountDatabaseOnDelimitersToRunQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get databases templates compatible with the specified billing service
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/compatible-templates")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<IEnumerable<AvailableDatabaseForServiceExtensionDto>>))]
        public async Task<IActionResult> GetDatabaseTemplatesCompatibleWithService([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetCompatibleTemplatesQuery(id)));
        }

        /// <summary>
        /// Get all user's services of the specified billing service
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="userLogin">Account user login</param>
        /// <returns></returns>
        [HttpGet]
        [TokenAuthorize]
        [Route("{id:guid}/user")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ServiceTypeStateForUserDto>))]
        public async Task<IActionResult> GetServiceStateForUser([FromRoute] Guid id, [FromQuery] string userLogin)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountUserServicesQuery(id, userLogin)));
        }

        /// <summary>
        /// Get activation status of the specified billing service for the context account
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <returns></returns>
        [HttpGet]
        [TokenAuthorize]
        [Route("{id:guid}/activation-status")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> GetBillingServiceActivationStatus([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetBillingServiceActivationStatusQuery(id, accessProvider.ContextAccountId)));
        }

        /// <summary>
        /// Get an account user for sponsorship by the context account
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route("{id:guid}/sponsorship/user")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AccountUserBillingServiceTypeDto>))]
        public async Task<IActionResult> GetUserForSponsorship([FromQuery] GetUserForSponsorshipQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Register/create infobase by template when activating the specified billing service
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("{id:guid}/database")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult RegisterDatabaseWhenActivatingBillingService([FromRoute] Guid id, [FromQuery] RegisterDatabaseUponServiceActivationDto dto)
        {
            dto.ServiceId = id;
            return ResponseResultDto(registerAccountDatabaseTemplateManager.RegisterDatabaseUponServiceActivation(dto));
        }

        /// <summary>
        /// Manage the billing service 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route("management")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult EditBillingServiceOptions([FromBody] BillingServiceOptionsControlDto dto)
        {
            return ResponseResultDto(billingServiceManager.SaveNewOptionsControl(dto));
        }

        /// <summary>
        /// Apply demo-period subscribe for the specified billing service
        /// </summary>
        /// <param name="id">Billing service identifier</param>
        /// <param name="accountId">Account identifier</param>
        /// <param name="usePromisePayment">Need use promise payment</param>
        /// <returns></returns>
        [HttpPut]
        [TokenAuthorize]
        [Route("{id:guid}/activation/demo")]
        public IActionResult ApplyDemoSubscribe([FromRoute] Guid id, [FromQuery] Guid accountId, [FromQuery] bool usePromisePayment = false)
        {
            return ResponseResultDto(billingServiceManager.ApplyDemoServiceSubscribe(id, accountId, usePromisePayment));
        }

        /// <summary>
        /// Precalculate cost of services of the billing service for account users
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [TokenAuthorize]
        [Route("precalculated-cost")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<List<CalculateBillingServiceTypeResultDto>>))]
        public IActionResult GetPrecalculatedCostOfServices([FromBody] PrecalculatedCostOfServiceDto dto)
        {
            return ResponseResultDto(billingServiceManager.CalculateBillingServiceType(dto.AccountId, dto.BillingServiceId,
                dto.ChangedLicense, dto.ChangedUsers));
        }

        /// <summary>
        /// Apply the billing service for the account
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [TokenAuthorize]
        [Route("apply")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<BillingServiceTypeApplyOrPayResultDto>))]
        public IActionResult ApplyBillingService([FromBody] ApplyBillingServiceDto dto)
        {
            return ResponseResultDto(billingServiceManager.ApplyOrPayForAllChangesAccountServiceTypes(
                dto.AccountId, dto.BillingServiceId, dto.UsePromisePayment, dto.ChangedLicenses, dto.ChangedUsers));
        }

        /// <summary>
        /// Get info on the specified service of the "My Databases" billing service for the specified account or the context account
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("my-databases")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<MyDatabasesServiceTypeCardDto>))]
        public async Task<IActionResult> GetById([FromQuery] GetMyDatabasesServiceInfoQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Prolong service demo period 
        /// </summary> 
        /// <param name="prolongDemoService"></param>
        /// <returns></returns>
        [HttpPost("prolong-demo")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult ProlongDemoPeriod([FromBody] ProlongDemoServiceDto prolongDemoService)
        {
            return ResponseResultDto(billingServiceManager.ProlongDemo(prolongDemoService.ServiceId, prolongDemoService.AccountId, prolongDemoService.ProlongDayCount));
        }
    }
}
