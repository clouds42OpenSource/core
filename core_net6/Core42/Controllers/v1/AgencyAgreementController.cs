﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Models;
using Clouds42.AgencyAgreement.AgencyAgreement.Managers;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using PagedListExtensionsNetFramework;
using Core42.Application.Features.AgencyAgreementContext.Dtos;
using Core42.Application.Features.AgencyAgreementContext.Queries;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with agency agreements
    /// </summary>
    [Route("agency-agreements")]
    [ApiVersion("1.0")]
    public class AgencyAgreementController(CreateAgencyAgreementManager createAgencyAgreementManager) : BaseV1Controller
    {
        /// <summary>
        /// Get paginated agency agreements collection by filter and order
        /// </summary>
        /// <param name="query">Filtering and ordering query model</param>
        /// <returns>Paginated agency agreements collection</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<AgencyAgreementDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredAgencyAgreementsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get printed forms for agency agreements
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("printed-html-forms/as-key-value")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<PrintedHtmlFormDictionaryDto>>))]
        public async Task<IActionResult> PrintedHtmlForms()
        {
            return ResponseResultDto(await Mediator.Send(new GetAgencyAgreementPrintedHtmlFormsAsKeyValueQuery()));
        }

        /// <summary>
        /// Get agency agreement as file by id
        /// </summary>
        /// <returns>Agency agreement PDF-file</returns>
        [HttpGet]
        [Route("{id}/print")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> Print([FromRoute] Guid id)
        {
            var managerResult = await Mediator.Send(new GetAgencyAgreementFileQuery(id));
            Response.Headers.Add("Content-Disposition", $"inline; filename={managerResult.Result.FileName}");

            return File(managerResult.Result.Bytes, "application/pdf");
        }

        /// <summary>
        /// Create agency agreement
        /// </summary>
        /// <param name="dto">Data model for creating agency agreement</param>
        /// <returns>Created agency agreement identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public IActionResult Create([FromBody] AddAgencyAgreementItemDto dto)
        {
            var result = createAgencyAgreementManager.AddAgencyAgreementItem(dto);
            return ResponseResultDto(result);
        }
    }
}
