﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.Link42Context.Commands;
using Core42.Application.Features.Link42Context.Dtos;
using Core42.Application.Features.Link42Context.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("link42-configuration")]
    [AllowAnonymous]
    public class Link42ConfigurationController : BaseV1Controller
    {
        [HttpGet]
        [Route("limits")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Link42ConfigurationDto>))]
        public async Task<IActionResult> GetCurrentConfiguration([FromQuery] GetLink42ConfigurationQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Link42ConfigurationDto>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredLink42ConfigurationQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpGet]
        [Route("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Link42ConfigurationDto>))]
        public async Task<IActionResult> ById([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetLink42ConfigurationByIdQuery { Id = id }));
        }

        [HttpPost]
        [Route("counter/increase")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid?>))]
        public async Task<IActionResult> IncreaseCounter([FromBody] IncreaseDownloadCounterCommand query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Link42ConfigurationDto>))]
        public async Task<IActionResult> Add([FromBody] AddLink42ConfigurationCommand query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Link42ConfigurationDto>))]
        public async Task<IActionResult> Update([FromBody] UpdateLink42ConfigurationCommand query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        [HttpDelete]
        [Route("{id:guid}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<Guid>))]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteLink42ConfigurationCommand{ Id = id }));
        }
    }
}
