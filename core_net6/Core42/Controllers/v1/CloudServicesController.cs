﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.CloudServices;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Core42.Application.Contracts.Features.CloudServiceContext.Dtos;
using Core42.Application.Contracts.Features.CloudServiceContext.Queries;
using Core42.Application.Features.CloudServicesContext.Queries;
using Core42.Authentication.Schemes;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with cloud services
    /// </summary>
    [ApiVersion("1.0")]
    [Route("cloud-services")]
    public class CloudServicesController(ICloudServiceManager cloudServiceManager) : BaseV1Controller
    {
        #region GETs
        /// <summary>
        /// Get all cloud users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<Application.Features.CloudServicesContext.Dtos.CloudServiceDto>>))]
        public async Task<IActionResult> All()
        {
            return ResponseResultDto(await Mediator.Send(new GetAllCloudServicesQuery()));
        }

        /// <summary>
        /// Get jwt token by service name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{name}/jwt")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<CloudServiceJwtDto>))]
        public async Task<IActionResult> JwtByServiceId([FromRoute] string name)
        {
            return ResponseResultDto(await Mediator.Send(new GetJwtByServiceIdQuery(name)));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Create a cloud service
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AddCloudServiceModelDto>))]
        public IActionResult Create([FromBody] CloudServiceDto dto)
        {
            var result = cloudServiceManager.AddCloudService(dto);
            return ResponseResultDto(result);
        }


        /// <summary>
        /// Subscription confirmation for Cloud partners
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = AuthenticationSchemes.Basic, Policy = AuthenticationSchemes.Basic)]
        [Route("subscription")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SubscriptionConfirmationForCloud([FromBody] CloudServiceIntegrationDto dto)
        {
            var result = cloudServiceManager.SubscriptionConfirmationForCloud(dto, User.Identity!.Name);

            return ResponseResultDto(result);
        }

        /// <summary>
        /// Subscription confirmation for Cloud partners
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(AuthenticationSchemes = AuthenticationSchemes.Basic, Policy = AuthenticationSchemes.Basic)]
        [Route("info")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CloudServiceInfoDto))]
        public IActionResult GetServiceInfoForCloud([FromQuery] string email)
        {
            var result = cloudServiceManager.GetServiceInfoForCloud(email, User.Identity!.Name);

            return ResponseResultDto(result);
        }

        #endregion

        #region PUTs

        /// <summary>
        /// Edit a cloud service
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool?>))]
        public IActionResult Edit([FromBody] Clouds42.DataContracts.CloudServicesSegment.InnerModels.CloudServiceDto dto)
        {
            var result = cloudServiceManager.UpdateCloudService(dto);
            return ResponseResultDto(result);
        }

        #endregion
    }
}
