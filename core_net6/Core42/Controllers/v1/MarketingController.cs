﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AdvertisingBanner.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Market;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;
using Clouds42.LetterNotification.Managers;
using Core42.Application.Features.MarketingContext.Dtos;
using Core42.Application.Features.MarketingContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AdvertisingBannerTemplateDetailsDto = Core42.Application.Features.MarketingContext.Dtos.AdvertisingBannerTemplateDetailsDto;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing "Marketing" page
    /// </summary>
    [ApiVersion("1.0")]
    [Route("marketing")]
    public class MarketingController(
        AdvertisingBannerTemplateManager advertisingBannerTemplateManager,
        LetterTemplateManager letterTemplateManager,
        CalculateAdvertisingBannerAudienceManager calculateAdvertisingBannerAudienceManager)
        : BaseV1Controller
    {
        #region Advertising banners

        /// <summary>
        /// Get all advertising banners' templates for the context account
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("advertising-banners/templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<AdvertisingBannerTemplateDto>))]
        public async Task<IActionResult> GetAdvertisingBannersTemplates()
        {
            return ResponseResultDto(await Mediator.Send(new GetAdvertisingBannersTemplatesQuery()));
        }

        /// <summary>
        /// Get advertising banner's template details by identifier
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("advertising-banners/templates/details")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<AdvertisingBannerTemplateDetailsDto>))]
        public async Task<IActionResult> GetTemplateDetailsById([FromQuery] GetAdvertisingBannerTemplateDetailsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get an advertising banner's image
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("advertising-banners/image")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        public async Task<IActionResult> GetAdvertisingBannerTemplate([FromQuery] GetAdvertisingBannerImageQuery query)
        {
            var result = await Mediator.Send(query);
            if (result.Error)
                return ResponseResultDto(result);

            return File(result.Result.Content, result.Result.ContentType, Uri.EscapeDataString(result.Result.FileName));
        }

        /// <summary>
        /// Create an advertising banner's template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("advertising-banners/templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult CreateAdvertisingBannerTemplate([FromForm] CreateAdvertisingBannerTemplateDto dto)
        {
            return ResponseResultDto(advertisingBannerTemplateManager.CreateAdvertisingBannerTemplate(dto));
        }

        /// <summary>
        /// Edit an advertising banner's template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("advertising-banners/templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult EditAdvertisingBannerTemplate([FromForm] EditAdvertisingBannerTemplateDto dto)
        {
            return ResponseResultDto(advertisingBannerTemplateManager.EditAdvertisingBannerTemplate(dto));
        }

        /// <summary>
        /// Delete an advertising banner's template
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("advertising-banners/templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ManagerResult>))]
        public IActionResult DeleteAdvertisingBannerTemplate([FromQuery] int templateId)
        {
            return ResponseResultDto(advertisingBannerTemplateManager.DeleteAdvertisingBannerTemplate(templateId));
        }

        #endregion

        #region Advertising letters

        /// <summary>
        /// Get advertising letters' templates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("letters/templates")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<IEnumerable<LetterTemplateDto>>))]
        public async Task<IActionResult> GetLettersTemplates()
        {
            return ResponseResultDto(await Mediator.Send(new GetLetterTemplateQuery()));
        }

        /// <summary>
        /// Get advertising letter template's details by identifier
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("letters/templates/details")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<LetterTemplateDetailsDto>))]
        public async Task<IActionResult> GetLetterTemplateDetailsById([FromQuery] GetLetterTemplateDetailsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Edit a letter template
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("letters/templates")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult EditLetterTemplate([FromBody] EditLetterTemplateDto dto)
        {
            return ResponseResultDto(letterTemplateManager.EditLetterTemplate(dto));
        }

        /// <summary>
        /// Send the test letter
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("letters/send-test")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult SendTestLetter([FromBody] SendTestLetterDto dto)
        {
            return ResponseResultDto(letterTemplateManager.SendTestLetter(dto));
        }


        #endregion

        #region Advertising audience

        /// <summary>
        /// Calculate advertising audience of the banner
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("audience/calculation")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate.
                CalculationAdvertisingBannerAudienceDto>))]
        public IActionResult CalculateAdvertisingAudience([FromBody] Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate.AdvertisingBannerAudienceDto dto)
        {
            return ResponseResultDto(calculateAdvertisingBannerAudienceManager.Calculate(dto));
        }

        #endregion
    }
}
