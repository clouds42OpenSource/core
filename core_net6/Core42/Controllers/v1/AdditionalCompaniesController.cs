﻿using System;
using Core42.Application.Features.AccountAdditionalCompanyContext.Commands;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Core42.Application.Features.AccountAdditionalCompanyContext.Dtos;
using Core42.Application.Features.AccountAdditionalCompanyContext.Queries;
using System.Collections.Generic;
using Asp.Versioning;

namespace Core42.Controllers.v1
{
    [Route("additional-companies")]
    [ApiVersion("1.0")]
    public class AdditionalCompaniesController : BaseV1Controller
    {
        /// <summary>
        /// Add additional company
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> AddAdditionalCompany([FromBody] AddAccountAdditionalCompanyCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// Delete additional company
        /// </summary>
        /// <returns></returns>
        [HttpDelete("/accounts/{accountId:guid}/additional-companies/{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> DeleteAdditionalCompany([FromRoute] Guid accountId, [FromRoute] Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new DeleteAccountAdditionalCompanyCommand { AccountId = accountId, Id = id }));
        }

        /// <summary>
        /// Update additional company
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> UpdateAdditionalCompany([FromBody] UpdateAccountAdditionalCompanyCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// Get additional companies as dictionary
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("as-list")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<AccountAdditionalCompanyDictionaryDto>>))]
        public async Task<IActionResult> AdditionalCompaniesDictionary([FromQuery] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAccountAdditionalCompaniesDictionaryQuery { AccountId = accountId }));
        }
    }
}
