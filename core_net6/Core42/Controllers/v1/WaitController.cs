﻿using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.PerformanceContext.Commands;
using Core42.Application.Features.PerformanceContext.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v1;

[Route("waits")]
[ApiVersion("1.0")]
public class WaitController : BaseV1Controller
{
    [HttpGet]
    public async Task<IActionResult> GetWaitInfosAsync([FromQuery] GetWaitsQuery query) => 
        ResponseResultDto(await Mediator.Send(query));

    [HttpPost]
    public async Task<IActionResult> AddNewWaitInfoAsync([FromBody] CreateWaitInfoCommand command) 
        => ResponseResultDto(await Mediator.Send(command));

    [HttpPut("{type}")]
    public async Task<IActionResult> UpdateWaitInfoAsync([FromRoute] string type, [FromBody] UpdateWaitInfoCommand command)
    {
        command.Type = type;
        return ResponseResultDto(await Mediator.Send(command));
    }
    
    [HttpDelete]
    public async Task<IActionResult> DeleteWaitInfoAsync(DeleteWaitInfoCommand command)
        => ResponseResultDto(await Mediator.Send(command));


    [HttpPost("fill-with-xml")]
    public async Task<IActionResult> FillDatabaseWithXmlFile(FillDatabaseWithXmlCommand command) =>
        ResponseResultDto(await Mediator.Send(command));
}
