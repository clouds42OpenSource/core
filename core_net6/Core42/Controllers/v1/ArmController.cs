﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Core42.Models;
using System.Threading.Tasks;
using Core42.Application.Features.ArmContext.Queries;
using Core42.Application.Features.ArmContext.Dtos;
using System.Collections.Generic;
using Asp.Versioning;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Managing TiI and AO
    /// </summary>
    [ApiVersion("1.0")]
    [Route("arm")]
    public class ArmController(IAccessProvider accessProvider) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get filter model for index page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<BaseArmQuery>))]
        public IActionResult Index()
        {
            accessProvider.HasAccess(ObjectAction.ControlPanel_ManagmentMenu_ARM);
            return ResponseResultDto(new ManagerResult<BaseArmQuery>
            {
                Result = new BaseArmQuery(),
                State = ManagerResultState.Ok
            });
        }

        /// <summary>
        /// Get database new releases' configurations
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("configuration-releases")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(ResponseResultDto<IEnumerable<DbConfigurationDto>>))]
        public async Task<IActionResult> GetDbConfigurationReleases([FromQuery] GetDbConfigurationQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Get info for TiI and AO for specified period
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("period")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<DbSupportDto>))]
        public async Task<IActionResult> GetInfoForPeriod([FromBody] GetInfoForPeriodQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get details of TiI and AO
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("details")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<TiIUpdatesDetailDto>))]
        public async Task<IActionResult> GetDetail([FromBody] GetArmDetailsQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }


        #endregion

        #region PUTs

        /// <summary>
        /// Apply/reject the specified database configuration release
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("configuration-release")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public async Task<IActionResult> ChangeDbConfigurationStatus([FromQuery] ChangeDbConfigurationStatusQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        #endregion

        #region DELETEs



        #endregion
    }
}
