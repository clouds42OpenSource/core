﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Locales.Managers;
using Core42.Application.Contracts.Features.LocaleContext;
using Core42.Application.Features.LocaleContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;
using LocaleDto = Core42.Application.Contracts.Features.LocaleContext.Dtos.LocaleDto;

namespace Core42.Controllers.v1
{
    /// <summary>
    /// Working with locales
    /// </summary>
    [Route("locales")]
    [ApiVersion("1.0")]
    public class LocaleController(LocaleManager localeManager) : BaseV1Controller
    {
        #region GETs

        /// <summary>
        /// Get paginated collection of locales by filter and order
        /// </summary>
        /// <param name="query">Filtering and ordering query model</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<LocaleDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredLocalesQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get all localization by account id and locale id
        /// </summary>
        /// <returns></returns>
        [HttpGet("localizations")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<List<LocaleDto>>))]
        public async Task<IActionResult> Localizations([FromQuery] Guid? localeId, [FromQuery] Guid? accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetAllLocalizationsByCurrentAccountLocaleQuery(localeId, accountId)));
        }

        /// <summary>
        /// Get locale data by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<LocaleDto>))]
        public async Task<IActionResult> ById(Guid id)
        {
            return ResponseResultDto(await Mediator.Send(new GetLocaleByIdQuery(id)));
        }

        #endregion

        #region POSTs



        #endregion

        #region PUTs

        /// <summary>
        /// Edit a locale data
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<bool>))]
        public IActionResult Edit([FromBody] EditLocaleDto dto)
        {
            var result = localeManager.Edit(dto);
            return ResponseResultDto(result);
        }

        #endregion

        #region DELETEs



        #endregion
    }
}
