﻿using Clouds42.DataContracts.Billing.YookassaAggregator;
using Core42.Application.Features.YookassaContext.Commands;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using Asp.Versioning;
using Core42.Application.Features.PayServicesContext.Dtos;
using Core42.Application.Features.PayServicesContext.Queries;

namespace Core42.Controllers.v1
{
    [Route("payments")]
    [ApiVersion("1.0")]
    public class PaymentsController : BaseV1Controller
    {

        /// <summary>
        /// Get paymen by id
        /// </summary>
        /// <param name="id">Parameters model</param>
        /// <param name="accountId">Parameters model</param>
        /// <returns>Paid invoice model</returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentDto))]
        public async Task<IActionResult> PaymentById ([FromRoute] Guid id, [FromQuery] Guid accountId)
        {
            return ResponseResultDto(await Mediator.Send(new GetPaymentByIdQuery { PaymentId = id , AccountId = accountId }));
        }

        /// <summary>
        /// Payment with sbp qr code
        /// </summary>
        /// <param name="command">Parameters model</param>
        /// <returns>Paid invoice model</returns>
        [HttpPost]
        [Route("qr-pay")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentResultDto))]
        public async Task<IActionResult> PayWithQrCode([FromBody] PayWithQrCodeCommand command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }

        /// <summary>
        /// Payment for service alpacaMeet
        /// </summary>
        /// <param name="command">Parameters model</param>
        /// <returns>Paid invoice model</returns>
        [HttpPost]
        [Route("alpacaMeet")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ConfirmYookassaPaymentForClientDataDto))]
        public async Task<IActionResult> PayForServiceAlpacaMeet([FromBody] AlpacaPaymentDto command)
        {
            return ResponseResultDto(await Mediator.Send(command));
        }
    }
}
