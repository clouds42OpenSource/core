﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer;
using Clouds42.Segment.CloudServicesContentServer.Managers;
using Core42.Application.Features.ContentServerContext.Dtos;
using Core42.Application.Features.ContentServerContext.Queries;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PagedListExtensionsNetFramework;

namespace Core42.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("content-servers")]
    public class ContentServersController(CloudContentServerManager contentServerManager) : BaseV1Controller
    {
        /// <summary>
        /// Get filtered paginated and sorted content server
        /// </summary>
        /// <param name="query">query params by filter, paging, and sorted content server</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<PagedDto<ContentServerDto>>))]
        public async Task<IActionResult> Filtered([FromQuery] GetFilteredContentServersQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Get content server by id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<ContentServerDto>))]
        public async Task<IActionResult> ById([FromRoute] GetContentServerByIdQuery query)
        {
            return ResponseResultDto(await Mediator.Send(query));
        }

        /// <summary>
        /// Add new content server
        /// </summary>
        /// <param name="dto">content server</param>
        [HttpPost]
        public IActionResult Add([FromBody] CreateCloudContentServerDto dto)
        {
            return ResponseResultDto(contentServerManager.CreateCloudContentServer(dto));
        }

        /// <summary>
        /// Delete content server
        /// </summary>
        /// <param name="id">content server</param>
        [HttpDelete]
        [Route("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            return ResponseResultDto(contentServerManager.DeleteCloudContentServer(id));
        }

        /// <summary>
        /// Change content server
        /// </summary>
        /// <param name="dto">Changed content server</param>
        [HttpPut]
        public IActionResult Edit([FromBody] CloudContentServerDto dto)
        {
            return ResponseResultDto(contentServerManager.EditCloudContentServer(dto));
        }
    }
}
