﻿using System;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Helpers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Service.Partner;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with partners' services
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class PartnerServicesDataController(
        PartnerBillingServicesDataManager partnerBillingServicesDataManager,
        AgentDocumentFileManager agentDocumentFileManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get information on the status of the service and connected services for the client
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceTypeStateForUserDto))]
        public IActionResult GetServiceTypesStateForUser(Guid serviceId, Guid accountUserId)
        {
            var result = partnerBillingServicesDataManager.GetServiceTypesStateForUserByKey(serviceId, accountUserId);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get information on the status of the service and connected services for the client
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <param name="accountUserLogin">Account user login</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceTypeStateForUserDto))]
        public IActionResult GetServiceTypeStateForUserByLogin(Guid serviceId, string accountUserLogin)
        {
            var result =
                partnerBillingServicesDataManager.GetServiceTypeStateForUserByKey(serviceId, accountUserLogin);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Check the service status of a service for a client.
        /// </summary>
        /// <param name="serviceTypeId">Service if</param>
        /// <param name="accountUserLogin">Account user login</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceTypeStatusForUserDto))]
        public IActionResult CheckServiceTypeStatusForUserByLogin(Guid serviceTypeId, string accountUserLogin)
        {
            var result =
                partnerBillingServicesDataManager.CheckServiceTypeStatusForUser(serviceTypeId, accountUserLogin);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Download billing service file
        /// </summary>
        /// <param name="fileId">Service file identifier</param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DownloadServiceFile(Guid fileId)
        {
            var managerResult = agentDocumentFileManager.GetAgentDocumentFile(fileId);

            if (managerResult.Error)
                return CreateResultEmpty(managerResult, HttpContext);

            return File(managerResult.Result.Content, managerResult.Result.ContentType,
                managerResult.Result.FileName);
        }

        #endregion
    }
}
