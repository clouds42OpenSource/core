﻿using System.Collections.Generic;
using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.AccountDatabase.DbTemplates.Managers;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/dbTemplate")]
    public class DbTemplateController(DbTemplatesManager dbTemplatesManager) : BaseController
    {
        /// <summary>
        /// Get a list of configuration names
        /// </summary>
        /// <returns>list of configuration names</returns>
        [HttpGet]
        [Route("Configuration1CName")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<string>))]
        public IActionResult GetConfiguration1CName()
        {
            var result = dbTemplatesManager.GetConfiguration1CName();
            return CreateResult(result, value => value, HttpContext);
        }
    }
}
