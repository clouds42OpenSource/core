﻿using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.AccountDatabase.Backup;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing infobases copies
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountDatabasesBackupController(AccountDatabaseBackupManager accountDatabaseBackupManager)
        : BaseController
    {
        /// <summary>
        /// Register infobase copy
        /// </summary>
        /// <param name="model">Registration parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult RegisterBackup([FromBody] RegisterAccountDatabaseBackupRequestDto model)
        {
            var result = accountDatabaseBackupManager.RegisterBackup(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Remove infobase copy
        /// </summary>
        /// <param name="model">Removing parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult DeleteBackupRecord([FromBody] DeleteAccountDatabaseBackupRequestDto model)
        {
            var result = accountDatabaseBackupManager.DeleteBackup(model);
            return CreateResultEmpty(result, HttpContext);
        }
    }
}
