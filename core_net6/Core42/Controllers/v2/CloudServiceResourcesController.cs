﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.MyDisk;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]")]
    public class CloudServiceResourcesController(BillingServiceManager billingServiceManager) : BaseController
    {
        #region GETs

        /// <summary>
        /// Get information about the service service for the account
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <param name="accountId">Account identifier</param>
        [HttpGet]
        [Route("accounts/{accountId}/services/{serviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DopSessionServiceDto))]
        public IActionResult GetBillingServiceInfo(Guid serviceId, Guid accountId)
        {
            var result = billingServiceManager.GetBillingServiceTypeInfo(serviceId, accountId);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get Rent1C expire date
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet("accounts/{accountId:guid}/services/rent-1c/expire-date")]
        public async Task<IActionResult> GetRent1CExpireDate([FromRoute] Guid accountId)
        {
            var result = await billingServiceManager.GetRent1CExpireDate(accountId);
            return CreateResult(result, x => x, HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Calculate the cost of services
        /// </summary>
        /// <param name="cloudService">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("accounts")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TryChangeTariffResultDto))]
        public IActionResult CalculateDopSessionService([FromBody] CloudServiceDto cloudService)
        {
            var result = billingServiceManager.CalculateDopSessionServiceType(cloudService.AccountId, cloudService.ServiceId, cloudService.Count, cloudService.AccountUserSessions);

            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Buy service for account
        /// </summary>
        /// <param name="cloudServicePayment">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("payment-service")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentServiceResultDto))]
        public IActionResult PaymentDopSessionService([FromBody] CloudServicePaymentDto cloudServicePayment)
        {
            var result = billingServiceManager.PaymentDopSessionService(cloudServicePayment.ServiceId, cloudServicePayment.AccountId,
                cloudServicePayment.IsPromisePayment, cloudServicePayment.CostOfTariff, cloudServicePayment.Count, cloudServicePayment.AccountUserSessions);

            return CreateResult(result, x => x, HttpContext);
        }

        #endregion
    }
}
