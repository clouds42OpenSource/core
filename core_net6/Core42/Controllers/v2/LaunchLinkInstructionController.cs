﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Helpers;
using Clouds42.DataContracts.Service.Link;
using Clouds42.Link42.Managers;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class LaunchLinkInstructionController(LaunchLinkInstructionManager launchLinkInstructionManager)
        : BaseController
    {
        /// <summary>
        /// Get data about mounting a cloud disk on the terminal
        /// </summary>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MountCloudDiskInfoDto))]
        public async Task<IActionResult> GetMountCloudDiskInfo(Guid accountUserId)
        {
            var result = await launchLinkInstructionManager.GetMountCloudDiskInfo(accountUserId);
            return CreateResult(result, x => x, HttpContext);
        }
    }
}
