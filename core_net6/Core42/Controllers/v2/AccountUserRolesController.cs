﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.Accounts.Account.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Enums.Security;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Attributes;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountUserRolesController(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseController
    {
        private readonly AccountUserRolesManager _manager = new(accessProvider, dbLayer, handlerException);

        /// <summary>
        /// Получить типы ролей пользователя
        /// </summary>
        /// <param name="accountUserId">ИД пользователя аккаунта</param>
        /// <returns>Типы ролей пользователя</returns>
        [HttpGet]
        [ExternalService(ExternalServiceTypeEnum.Corp)]
        public async Task<int[]> Get([FromQuery] Guid accountUserId)
        {
            return await _manager.Get(accountUserId);
        }

    }
}
