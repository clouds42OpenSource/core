﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Helpers;
using Clouds42.AccountDatabase.Access;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing accesses to infobases
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AcDbAccessesController(
        IAcDbAccessesManager manager,
        AcDbAccessManager acDbAccessManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get infobase access properties
        /// </summary>
        /// <param name="accessID">Access identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AcDbAccessPropertiesResultDto))]
        public IActionResult GetProperties(Guid accessID)
        {
            var result = manager.GetProperties(accessID);
            return CreateResult(result, x => new AcDbAccessPropertiesResultDto
            {
                AccountID = x.AccountID,
                AccountUserID = x.AccountUserID ?? Guid.Empty,
                LocalUserID = x.LocalUserID,
                AccountDatabaseID = x.AccountDatabaseID
            }, HttpContext);
        }

        /// <summary>
        /// Get accesses
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="accountUserId">Account user identifier for filtrating (returns all by default)</param>
        /// <param name="includeMine">Enable/disable accesses to my DBs (disable by default)</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AcDbAccessIdsDto))]
        public async Task<IActionResult> FindAccessIDsByAccountID(Guid accountId, Guid? accountUserId = null, bool? includeMine = null)
        {
            var result = await manager.FindAccessIDsByAccountId(accountId, accountUserId, includeMine);
            return CreateResult(result, x => new AcDbAccessIdsDto { AccessIDs = new GuidListItemDto { List = x } }, HttpContext);
        }

        /// <summary>
        /// Get user accesses
        /// </summary>
        /// <param name="accountID">Account identifier</param>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AcDbAccessListResultDto))]
        public IActionResult GetAccessList(Guid accountID, Guid? accountUserId = null)
        {
            var result = manager.GetAccessList(accountID, accountUserId);
            return CreateResult(result, x => new AcDbAccessListResultDto { AcDbAccessList = x }, HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Add infobase access
        /// </summary>
        /// <param name="model">Providing access parameters</param>
        /// <returns>Access identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AcDbAccessesAddResultModelDto))]
        public IActionResult Add([FromBody] AcDbAccessPostAddModelDto model)
        {
            var result = acDbAccessManager.Add(model);
            return CreateResult(result, x => new AcDbAccessesAddResultModelDto(x), HttpContext);
        }

        /// <summary>
        /// Remove infobase access
        /// </summary>
        /// <param name="model">Removing access parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult Delete([FromBody] AcDbAccessPostIDDto model)
        {
            var result = acDbAccessManager.Delete(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Check access state to user infobase
        /// </summary>
        /// <param name="model">Parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult IsLocked([FromBody] AcDbAccessCheckLockModelDto model)
        {
            var result = manager.IsLocked(model);
            return result.State == ManagerResultState.Ok ? CreateResult(result, x => new BoolDto { Result = result.Result }, HttpContext) : CreateResultEmpty(result, HttpContext);
        }

        #endregion
    }
}
