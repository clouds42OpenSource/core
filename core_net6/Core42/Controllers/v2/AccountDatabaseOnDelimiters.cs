﻿using Asp.Versioning;
using Clouds42.AccountDatabase.Delimiters.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountDatabaseOnDelimiters(
        CreateDbOnDelimitersWithoutRegistrationInMsManager createDbOnDelimitersWithoutRegistrationInMsManager)
        : BaseController
    {
        /// <summary>
        /// Create databse on delimiters (without registraion in MS)
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns>Created database identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseIdDto))]
        public IActionResult CreateWithoutRegistrationInMs([FromBody] CreateDbOnDelimitersWithoutRegistrationInMsDto model)
        {
            var result = createDbOnDelimitersWithoutRegistrationInMsManager.Create(model);
            return CreateResult(result, x => new AccountDatabaseIdDto(x), HttpContext);
        }
    }
}
