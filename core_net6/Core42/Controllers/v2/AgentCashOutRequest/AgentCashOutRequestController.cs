﻿using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;

namespace Core42.Controllers.v2.AgentCashOutRequest
{
    /// <summary>
    /// Working with cash out requests for CORP
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AgentCashOutRequestController(
        AgentCashOutRequestDataManager agentCashOutRequestDataManager,
        ChangeAgentCashOutRequestStatusManager changeAgentCashOutRequestStatusManager)
        : BaseController
    {
        /// <summary>
        /// Get cash out reguests with status "New"
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AgentCashOutRequestsInStatusNewDto))]
        public IActionResult GetAgentCashOutRequestsInStatusNew()
        {
            var managerResult = agentCashOutRequestDataManager.GetAgentCashOutRequestsInStatusNew();

            if (managerResult.Error)
                return Ok(managerResult.Message);

            return CreateResult(managerResult, x => x, HttpContext);
        }

        /// <summary>
        /// Change cash out reguest status
        /// </summary>
        /// <param name="changeAgentCashOutRequestStatus">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ChangeAgentCashOutRequestStatus([FromBody]
            ChangeAgentCashOutRequestStatusDto changeAgentCashOutRequestStatus)
        {
            var managerResult =
                changeAgentCashOutRequestStatusManager
                    .ChangeAgentCashOutRequestStatus(changeAgentCashOutRequestStatus);

            if (managerResult.Error)
                return Ok(managerResult.Message);

            return CreateResultEmpty(managerResult, HttpContext);
        }
    }
}
