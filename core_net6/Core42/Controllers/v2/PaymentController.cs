﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers;
using Clouds42.Billing.Invoice.Managers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.Enums;
using Core42.Helpers;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with payment
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class PaymentController(
        PaymentsManager paymentsManager,
        InvoiceActManager invoiceActManager,
        InvoiceConfirmationManager invoiceConfirmationManager,
        InvoiceDataManager invoiceDataManager,
        AggregatorInvoicesDataManager aggregatorInvoicesDataManager,
        IBillingPaymentsProvider billingPaymentsProvider,
        IAbilityToCreatePaymentProvider abilityToCreatePaymentProvider)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get or create invoice by parameters
        /// </summary>
        /// <param name="sum">Pay amount</param>
        /// <param name="isCreateNew">Flag for forced invoice creation</param>
        /// <param name="invoiceNumber">Invoice number</param>
        /// <param name="inn">Account INN</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoicePaymentsDto))]
        public async Task<IActionResult> GetInvoice(decimal sum, bool isCreateNew = false,
            string invoiceNumber = null, string inn = null)
        {
            var result = await invoiceDataManager.GetOrCreateInvoiceAsync(invoiceNumber, isCreateNew, inn, sum);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get or create invoice by parameters
        /// </summary>
        /// <param name="sum">Pay amount</param>
        /// <param name="invoiceNumber">Invoice number</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoicePaymentsDto))]
        public async Task<IActionResult> GetProcessingOrCreateNewInvoice(decimal sum, string invoiceNumber)
        {
            var result = await invoiceDataManager.GetProcessingOrCreateNewInvoiceAsync(invoiceNumber, sum);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get processing invoice by sepcified amount
        /// </summary>
        /// <param name="sum">Invoice amount</param>
        /// <param name="inn">Account INN</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoicePaymentsDto))]
        public async Task<IActionResult> GetProcessingInvoice(decimal sum, string inn)
        {
            var result = await invoiceDataManager.GetProcessingInvoice(inn, sum);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get aggregators payments
        /// </summary>
        /// <param name="dateFrom">Start date</param>
        /// <param name="dateTo">Finish date</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AggregatorInvoiceWithSupplierCodeListDto))]
        public IActionResult GetAggregatorInvoicesList(DateTime dateFrom, DateTime dateTo)
        {
            var result = aggregatorInvoicesDataManager.GetAggregatorInvoicesData(dateFrom, dateTo);
            return CreateResult(result, i => new AggregatorInvoiceWithSupplierCodeListDto(i), HttpContext);
        }

        /// <summary>
        /// Check the possibility of payment by account and amount
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="cost">Amount</param>
        /// <param name="currency">Payment currency</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentServiceResultDto))]
        public IActionResult CheckPaymentOption([FromQuery] Guid accountId, [FromQuery] decimal cost, [FromQuery] string currency)
        {
            var result = billingPaymentsProvider.CanCreatePayment(accountId, cost, out var needMoney);

            return result == PaymentOperationResult.Ok
                ? Ok(new PaymentServiceResultDto { Complete = true })
                : BadRequest(new PaymentServiceResultDto
                {
                    Amount = cost,
                    Currency = currency,
                    NotEnoughMoney = needMoney,
                    ErrorMessage = $"Не удалось провести платеж на {cost} по причине '{result.GetDisplayName()}'",
                    CanUsePromisePayment = abilityToCreatePaymentProvider.CanUsePromisePayment(accountId),
                    CanIncreasePromisePayment = abilityToCreatePaymentProvider.CanIncreasePromisePayment(accountId),
                });
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Make a payment by resolving an invoice for payment.
        /// </summary>
        /// <param name="confirmationInvoice">Invoice data</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult ConfirmationInvoice([FromBody] ConfirmationInvoiceDto confirmationInvoice)
        {
            var result = invoiceConfirmationManager.ConfirmInvoiceViaNewThread(confirmationInvoice);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Cancel invoice
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult CancelInvoice([FromBody] InvoiceCancelDto model)
        {
            var result = paymentsManager.CancelInvoice(model.InvoiceNumber, model.AccountId);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set the act number for the invoice for payment
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public async Task<IActionResult> SetStatement([FromBody] SetActDto actDTO)
        {
            var result = await invoiceActManager.SetAct(
                actDTO.InvoicesId,
                actDTO.ActID,
                actDTO.Description,
                actDTO.RequiredSignature);
            return CreateResultEmpty(result, HttpContext);
        }

        #endregion
    }
}
