﻿using System;
using Asp.Versioning;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.BaseModel;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Handling infobases on delimiters
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AcDbDelimitersController(IAcDbDelimitersManager acDbDelimitersManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get infobase on delimiters number
        /// </summary>
        /// <param name="databaseSourceId">Infobase on delimiters identifer</param>
        /// <param name="zone">Zone number</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SimpleResultModelDto<Guid>))]
        [HttpGet]
        public IActionResult GetAccountDatabaseIdByZone([FromQuery] Guid databaseSourceId, [FromQuery] int zone)
        {
            var result = acDbDelimitersManager.GetAccountDatabaseIdByZone(databaseSourceId, zone);
            return CreateResult(result, r => r, HttpContext);
        }

       

        #endregion

        #region POSTs

        /// <summary>
        /// Set the  infobase status with adding a record in the table
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetStatusAndInsertDataOnDelimiter([FromBody] AccountDatabaseViaDelimiterDto model)
        {
            var result = acDbDelimitersManager.SetStatusAndInsertDataOnDelimiter(model);
            return CreateResultEmpty(result, HttpContext);
        }
        #endregion
    }
}
