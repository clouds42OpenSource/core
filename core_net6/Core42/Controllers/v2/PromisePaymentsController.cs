﻿using Asp.Versioning;
using Core42.Helpers;
using Core42.Models.Payments;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Repositories.Interfaces.Common;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/promisePayments")]
    public class PromisePaymentsController(
        BillingAccountManager billingAccountManager,
        IUnitOfWork dbLayer)
        : BaseController
    {
        /// <summary>
        /// Prolong the promised payment
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("prolong")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public IActionResult Prolong([FromBody] PromisePaymentActionRequest model)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(model.AccountId);
            var managerResult = billingAccountManager.ProlongPromisePayment(account);
            return CreateResult(managerResult, (x) => x, HttpContext);
        }

        /// <summary>
        /// Repay the promised payment
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("repay")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public IActionResult Repay([FromBody] PromisePaymentActionRequest model)
        {
            var account = dbLayer.BillingAccountRepository.GetBillingAccount(model.AccountId);
            var managerResult = billingAccountManager.PromisePaymentEarlyRepay(account);
            return CreateResult(managerResult, (x) => x, HttpContext);
        }
    }
}
