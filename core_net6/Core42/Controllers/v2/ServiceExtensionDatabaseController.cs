﻿using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Helpers;
using Clouds42.AccountDatabase.ServiceExtensions.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with the service extension for the infobase 
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class ServiceExtensionDatabaseController(
        IManageServiceExtensionDatabaseManager manageServiceExtensionDatabaseManager)
        : BaseController
    {
        /// <summary>
        /// Install service extension for infobase
        /// </summary>
        /// <param name="installServiceExtensionModel">Service extension installation model
        /// for infobase</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult InstallServiceExtensionForDatabase(
            [FromBody] InstallServiceExtensionForDatabaseDto installServiceExtensionModel)
        {
            var result =
                manageServiceExtensionDatabaseManager.InstallServiceExtensionForDatabase(installServiceExtensionModel);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Install a service extension for an infobase area
        /// </summary>
        /// <param name="installServiceExtensionModel">Data model for setting the service extension
        /// for area inf. bases</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult InstallServiceExtensionForDatabaseZone(
            [FromBody] InstallServiceExtensionForDatabaseZoneDto installServiceExtensionModel)
        {
            var result =
                manageServiceExtensionDatabaseManager.InstallServiceExtensionForDatabaseZone(
                    installServiceExtensionModel);

            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Remove service extension from infobase
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabase">Service Extension Deletion Model
        /// from the infobase</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult DeleteServiceExtensionFromDatabase(
            [FromBody] DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabase)
        {
            var result =
                manageServiceExtensionDatabaseManager.DeleteServiceExtensionFromDatabase(
                    deleteServiceExtensionFromDatabase);

            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Delete a service extension from the infobase area
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabaseZone">Service Extension Deletion Model
        /// from infobase area</param>
        /// <returns></returns> 
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult DeleteServiceExtensionFromDatabaseZone(
            [FromBody] DeleteServiceExtensionFromDatabaseZoneDto deleteServiceExtensionFromDatabaseZone)
        {
            var result =
                manageServiceExtensionDatabaseManager.DeleteServiceExtensionFromDatabaseZone(
                    deleteServiceExtensionFromDatabaseZone);

            return CreateResultEmpty(result, HttpContext);
        }
    }
}
