﻿using Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers;
using Clouds42.DataContracts.BaseModel;
using Core42.Helpers;
using Core42.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Asp.Versioning;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with promised payments
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class PromisedPaymentsController(IPromisedPaymentActivationManager promisedPaymentActivationManager)
        : BaseController
    {
        /// <summary>
        /// Activate promised payment
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="model">Parameters model</param>
        /// <returns>Activation result</returns>
        [HttpPost]
        [Route("/api_v{version:apiVersion}/accounts/{accountId}/promised-payment/activate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public async Task<IActionResult> ActivatePromisedPayment(
            [FromRoute] Guid accountId,
            [FromBody] ActivatePromisedPaymentApiModel model)
        {
            var result = await promisedPaymentActivationManager
                .ActivatePromisedPaymentAsync(accountId, model.InvoiceId);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Increase promised payment
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="model">Parameters model</param>
        /// <returns>Activation result</returns>
        [HttpPost]
        [Route("/api_v{version:apiVersion}/accounts/{accountId}/promised-payment/increase")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public async Task<IActionResult> IncreasePromisedPayment(
            [FromRoute] Guid accountId,
            [FromBody] ActivatePromisedPaymentApiModel model)
        {
            var result = await promisedPaymentActivationManager
                .IncreasePromisedPaymentAsync(accountId, model.InvoiceId);
            return CreateResultEmpty(result, HttpContext);
        }
    }


}
