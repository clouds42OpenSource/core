﻿using Asp.Versioning;
using Clouds42.CoreWorkerTask.Contracts.Managers;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class CoreWorkerTasksQueueController(ICoreWorkerTasksQueueManager coreWorkerTasksQueueManager)
        : BaseController
    {
        /// <summary>
        /// Add task
        /// </summary>
        /// <param name="taskInfo">Task parameters model</param>
        /// <returns>Created task identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CoreWorkerTasksQueueIdDto))]
        public IActionResult Add([FromBody] CoreWorkerTasksDto taskInfo)
        {
            return CreateResult(coreWorkerTasksQueueManager.Add(taskInfo), x => new CoreWorkerTasksQueueIdDto(x));
        }
    }
}
