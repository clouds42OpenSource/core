﻿using System;
using System.Collections.Generic;
using Asp.Versioning;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.AgencyAgreement.Partner.Managers.PartnerServices;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.ResourceConfiguration;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;
using Core42.Helpers;
using Core42.Models.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}")]
    public class ServicesController(
        BillingServiceCardDataManager billingServiceCardDataManager,
        ReactivateServiceManager reactivateServiceManager,
        PartnerBillingServicesDataManager partnerBillingServicesDataManager,
        CreateBillingServiceManager createBillingServiceManager,
        SetBillingServiceActivityManager setBillingServiceActivityManager,
        EditBillingServiceManager editBillingServiceManager,
        BillingServiceControlManager billingServiceControlManager,
        BillingServiceManager billingServiceManager)
        : BaseController
    {
        /// <summary>
        /// Get service data for account
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("accounts/{accountId}/services/{serviceId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountServiceDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl)]
        public IActionResult GetServiceData([FromRoute] Guid serviceId, [FromRoute] Guid accountId)
        {
            var result = partnerBillingServicesDataManager.GetServiceData(serviceId, accountId);
            return CreateResult(result, r => AccountServiceInfoApiModel.CreateFrom(r), HttpContext);
        }

        /// <summary>
        /// Get service data
        /// </summary>
        /// <param name="serviceId">Service ID for which to get data</param>
        /// <returns>Service Data</returns>
        [HttpGet]
        [Route("services/{serviceId}/service-data")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<BillingServiceCardDto>))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Promo)]
        public IActionResult GetServiceData([FromRoute] Guid serviceId)
        {
            var result = billingServiceCardDataManager.GetData(serviceId);
             
            return result.State == ManagerResultState.Forbidden ? StatusCode(403, result.Message) : CreateResult(result, value => value, HttpContext);
        }

        /// <summary>
        /// Get connection types to 1C infobase
        /// </summary>
        /// <returns>Types of connection to the 1C infobase</returns>
        [HttpGet]
        [Route("services/service-typesOfconnection")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TypesOfConnectionToInformationDbDto))]
        public IActionResult GetTypesOfConnectionToInformationDb()
        {
            var result = billingServiceManager.GetTypesOfConnectionToInformationDb();
            return CreateResult(result, value => value, HttpContext);
        }

        /// <summary>
        /// Create service
        /// </summary>
        /// <param name="createBillingServiceViewModel">Service Creation Model</param>
        /// <returns>Creation result</returns>
        [HttpPost]
        [Route("services/service-create")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BillingServiceIdDto))]
        public IActionResult Create([FromBody] CreateBillingServiceViewModel createBillingServiceViewModel)
        {
            var result = createBillingServiceManager.CreateBillingService(createBillingServiceViewModel.MapToDtoModel());
            return CreateResult(result, value => value, HttpContext);
        }


        /// <summary>
        /// Change service
        /// </summary>
        /// <param name="editBillingServiceViewModel">Service change model</param>
        /// <returns>The result of the operation</returns>
        [HttpPost]
        [Route("services/service-edit")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<EmptyResultDto>))]
        public IActionResult Edit([FromBody] EditBillingServiceViewModel editBillingServiceViewModel)
        {
            var result = editBillingServiceManager.EditBillingService(editBillingServiceViewModel.MapToDtoModel());
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Edit Service Draft
        /// </summary>
        /// <param name="editBillingServiceDraftViewModel">Draft editing model</param>
        /// <returns>The result of the operation</returns>
        [HttpPost]
        [Route("services/service-editDraft")]
        public IActionResult EditDraft([FromBody] EditBillingServiceDraftViewModel editBillingServiceDraftViewModel)
        {
            var result = editBillingServiceManager.EditBillingServiceDraft(editBillingServiceDraftViewModel.MapToDto());
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set service activity
        /// </summary>
        /// <param name="model">Model for setting billing service activity</param>
        [HttpPost]
        [Route("services/service-activity")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolDto))]
        public IActionResult BillingServiceActivity([FromBody] SetBillingServiceActivityDto model)
        {
            var result = setBillingServiceActivityManager.SetActivity(model);

            return CreateResult(result, x => new BoolDto(x), HttpContext);
        }

        /// <summary>
        /// Validation when creating a service
        /// </summary>
        /// <param name="createBillingServiceViewModel">Service Validation Model</param>
        /// <returns>Validation result</returns>
        [HttpPost]
        [Route("services/validate-service")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ValidateResultModelDto))]
        public IActionResult ValidateCreationOfNewBillingService([FromBody] CreateBillingServiceViewModel createBillingServiceViewModel)
        {
            var result = createBillingServiceManager.ValidateCreationOfNewBillingService(createBillingServiceViewModel.MapToDtoModel());
            return CreateResult(result, value => value, HttpContext);
        }

        /// <summary>
        /// Validation when editing a service
        /// </summary>
        /// <param name="editBillingServiceViewModel">Service Validation Model</param>
        /// <returns>Validation result</returns>
        [HttpPost]
        [Route("services/validate-edit-service")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ValidateResultModelDto))]
        public IActionResult ValidateCreationOfNewBillingService([FromBody] EditBillingServiceViewModel editBillingServiceViewModel)
        {
            var result = createBillingServiceManager.ValidateEditBillingService(editBillingServiceViewModel.MapToDtoModel());
            return CreateResult(result, value => value, HttpContext);
        }

        /// <summary>
        /// Service activation for an account
        /// </summary>
        /// <param name="accountId">Account ID for which to activate the service.</param>
        /// <param name="serviceId">Service ID to activate.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("accounts/{accountId}/services/{serviceId}/activate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [ExternalService(ExternalServiceTypeEnum.Promo)]
        public IActionResult ActivateService([FromRoute] Guid accountId, [FromRoute] Guid serviceId)
        {
            var result = reactivateServiceManager.ActivateServiceForExistingAccountWithoutServiceInstallation(accountId, serviceId);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Disabling the service
        /// </summary>
        /// <param name="serviceId">Service ID to disable.</param>
        /// <returns>Execution result</returns>
        [HttpPost]
        [Route("services/{serviceId}/shutdown")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolDto))]
        public IActionResult ShutdownService([FromRoute] Guid serviceId)
        {
            var result =
                setBillingServiceActivityManager
            .ShutdownService(new SetBillingServiceActivityDto { IsActive = false, ServiceId = serviceId });

            return CreateResult(result, x => new BoolDto(x), HttpContext);
        }

        /// <summary>
        /// Service control change
        /// </summary>
        /// <param name="manageBillingServiceDto">Service management model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("services/manage-edit")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<EmptyResultDto>))]
        public IActionResult Manage([FromBody] ManageBillingServiceDto manageBillingServiceDto)
        {
            var result = billingServiceControlManager.Manage(manageBillingServiceDto);
            return CreateResultEmpty(result, HttpContext);
        }
    }
}
