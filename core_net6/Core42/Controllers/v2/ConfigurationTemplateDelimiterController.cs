﻿using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.AccountDatabase.Delimiters.DbTemplateDelimitersReferences;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Access to the configuration update map
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/ConfigurationTemlateDelimiter/[action]")]
    public class ConfigurationTemplateDelimiterController(
        DbTemplateDelimitersReferencesManager dbTemplateDelimitersReferencesManager)
        : BaseController
    {
        /// <summary>
        /// Update configuration version for delimiter infobase template
        /// </summary>
        /// <param name="model">
        /// 1C configuration code:
        /// bp  - "Бухгалтерия предприятия 3.0"
        /// zup - "Зарплата и Управление Персоналом 3.0"
        /// ka  - "Комплексная автоматизация 2.2"
        /// unf - "Управление нашей фирмой 1.6"
        /// ut  - "Управление торговлей 11";
        /// Configuration release version</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult UpdateConfigurationReleaseVersion([FromBody] TemlateDelimiterDto model)
        {
            var result = dbTemplateDelimitersReferencesManager.UpdateConfigurationReleaseVersion(model.СonfigurationCode, model.ReleaseVersion);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Update minimum configuration version for delimiter infobase template
        /// </summary>
        /// <param name="model">
        /// 1C configuration code:
        /// bp  - "Бухгалтерия предприятия 3.0"
        /// zup - "Зарплата и Управление Персоналом 3.0"
        /// ka  - "Комплексная автоматизация 2.2"
        /// unf - "Управление нашей фирмой 1.6"
        /// ut  - "Управление торговлей 11";
        /// Configuration release version</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult UpdateConfigurationMinVersion([FromBody] TemlateDelimiterDto model)
        {
            var result = dbTemplateDelimitersReferencesManager.UpdateConfigurationMinVersion(model.СonfigurationCode, model.ReleaseVersion);
            return CreateResultEmpty(result, HttpContext);
        }
    }
}
