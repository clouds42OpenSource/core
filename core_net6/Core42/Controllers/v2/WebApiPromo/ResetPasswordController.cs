﻿using System;
using System.Collections.Generic;
using Asp.Versioning;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2.WebApiPromo
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class ResetPasswordController(
        IUnitOfWork dbLayer,
        AccountUsersProfileManager accountUsersProfileManager,
        AccountUserResetPasswordNotificationManager accountUserResetPasswordNotificationManager)
        : BaseController
    {
        /// <summary>
        /// Установка нового пароля пользователя
        ///     0 - все ок
        ///     1 - логин не найден в базе
        ///     2 - ошибка при отправке
        ///     3 - недопустимая длина пароля
        ///     4 - недопустимые символы
        ///     
        /// </summary>        
        [HttpPost]
        public int SetNewPassword([FromBody] UserPasswordModelDto model)
        {
            Logger.Trace("Установка нового пароля для пользователя {0} ...", model.Login);

            Logger.Trace("Проверка валидации нового пароля для пользователя {0} ...", model.Login);

            var validator = new PasswordValidation(model.Password);
            var validationResult = validator.PasswordIsValid();
            if (!validationResult.DataIsValid)
                return 4;

            Logger.Trace("Новый пароль валиден для пользователя {0} ...", model.Login);

            try
            {
                accountUsersProfileManager.SetNewUserPassword(model.Login, model.Password);
                Logger.Trace("Новый пароль установлен {0}  для пользователя ", model.Login);
            }
            catch (KeyNotFoundException ex)
            {
                Logger.Warn(ex.Message);
                return 1;
            }
            catch (Exception ex)
            {
                Logger.Warn("Ошибка при установке нового пароля пользователю {0} : {1}", model.Login, ex.Message);
                return 2;
            }
            return 0;
        }



        /// <summary>
        /// Проверка существование e-mail в базе
        ///     0 - все ок, ошибок нету, почта найдена
        ///     1 - ошибка, почта не найдена
        /// </summary>
        [HttpGet]
        public int ValidateEmail(string email)
        {
            var user = dbLayer.AccountUsersRepository.GetAccountUserByEmail(email);
            return user != null ? 0 : 1;
        }


        /// <summary>
        /// Запрос сброса пароля пользователя.
        /// На почту отправляется запрос сброса пароля + ссылка, переход по которой инициирует сброс пароля
        ///     0 - все ок
        ///     1 - почта не найдена в базе
        ///     2 - ошибка при отправке
        ///     3 - почтовый ящик не валиден
        /// </summary>        
        [HttpPost]
        public int ResetPasswordRequest([FromBody] SimpleStringModelDto email)
        {
            Logger.Trace($"Запрос сброса пароля на почту <{email.Value}> ...");

            try
            {
                accountUserResetPasswordNotificationManager.SendPasswordChangeRequest(email.Value);
                Logger.Trace("Запрос сброса пароля на почту {0} отправлен", email);
            }
            catch (KeyNotFoundException ex)
            {
                Logger.Warn(ex.Message);
                return 1;
            }
            catch (Exception ex)
            {
                Logger.Warn("Ошибка при отправке запроса сброса пароля на почту {0} : {1}", email, ex.Message);
                return 2;
            }

            return 0;
        }


        /// <summary>
        /// Получает данные пользователя по его email
        /// Вызывается при восстановлении пароля
        /// </summary>        
        [HttpGet]
        [HttpPost]
        public UserInfoModelDto GetUserInfoByEmail([FromBody] SimpleStringModelDto email)
        {
            var user = dbLayer.AccountUsersRepository.GetAccountUserByEmail(email.Value);

            var userInfo = new UserInfoModelDto
            {
                Email = email.Value,
                Fio = user.FirstName,
                Login = user.Login
            };

            return userInfo;
        }
    }
}
