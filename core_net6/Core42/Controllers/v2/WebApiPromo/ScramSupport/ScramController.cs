﻿using System.Threading.Tasks;
using System.Web.Http;
using AppCommon.DataManagers.Security;
using Core42.Helpers;

namespace Core42.Controllers.WebApiPromo.ScramSupport
{
    public class ScramController : BaseController
    {
        private readonly ScramManager _scramManager;

        public ScramController(
            ScramManager scramManager)
        {
            _scramManager = scramManager;
        }

        //[HttpGet]
        //public async Task<IHttpActionResult> UserCreationChallenge(string secureSessionKey, string encryptedUserName)
        //{
        //    var result = await _scramManager.UserCreationChallenge(secureSessionKey, encryptedUserName);
        //    return CreateResultNoCast(result);
        //}

        //[HttpGet]
        //public async Task<IHttpActionResult> UserCreationResponse(string secureSessionKey, string message)
        //{
        //    var result = await _scramManager.UserCreationResponse(secureSessionKey, message);
        //    return CreateResultEmpty(result);
        //}

        //[HttpGet]
        //public async Task<IHttpActionResult> CreateUser(string userName, string password)
        //{
        //    var result = await _scramManager.CreateUser(userName, password);
        //    return CreateResultEmpty(result);
        //}

        [HttpGet]
        public async Task<IHttpActionResult> GetAuthChallenge(string secureSessionKey, string encryptedMessage)
        {
            var result = await _scramManager.GetAuthChallenge(secureSessionKey, encryptedMessage);
            return CreateResultNoCast(result);
        }

        //[HttpGet]
        //public async Task<IHttpActionResult> DeleteUser(string secureSessionKey, string encryptedUserName)
        //{
        //    var result = await _scramManager.DeleteUser(secureSessionKey, encryptedUserName);
        //    return CreateResultEmpty(result);
        //}

        [HttpGet]
        public async Task<IHttpActionResult> Logout(string secureSessionKey)
        {
            var result = await _scramManager.Logout(secureSessionKey);
            return CreateResultEmpty(result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetServerSignature(string secureSessionKey, string response)
        {
            var result = await _scramManager.GetServerSignature(secureSessionKey, response);
            return CreateResultNoCast(result);
        }
    }
}
