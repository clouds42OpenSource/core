﻿using System.Threading.Tasks;
using System.Web.Http;
using AppCommon.DataManagers.Security;
using Core42.Helpers;

namespace Core42.Controllers.WebApiPromo.ScramSupport
{
    public class P2PSecurityController : BaseController
    {
        private readonly P2PSecurityManager _p2PSecurityManager;

        public P2PSecurityController(
            P2PSecurityManager p2PSecurityManager)
        {
            _p2PSecurityManager = p2PSecurityManager;
        }

        [HttpGet]
        public async Task<IHttpActionResult> SetupSecureSessionKey(string otherPartyPubKey)
        {
            var result = await _p2PSecurityManager.SetupSecureSessionKey(otherPartyPubKey);

            return CreateResultNoCast(result);
        }
    }
}
