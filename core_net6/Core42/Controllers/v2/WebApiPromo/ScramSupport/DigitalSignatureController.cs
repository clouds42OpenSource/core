﻿using System.Threading.Tasks;
using System.Web.Http;
using AppCommon.DataManagers.Security;
using Core42.Helpers;

namespace Core42.Controllers.WebApiPromo.ScramSupport
{
    public class DigitalSignatureController : BaseController
    {
        private readonly DigitalSignatureManager _digitalSignatureManager;

        public DigitalSignatureController(
            DigitalSignatureManager digitalSignatureManager)
        {
            _digitalSignatureManager = digitalSignatureManager;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetKeyBlobBase64(string secureSessionKey)
        {
            var result = await _digitalSignatureManager.GetKeyBlobBase64(secureSessionKey);

            return CreateResultNoCast(result);
        }
    }
}
