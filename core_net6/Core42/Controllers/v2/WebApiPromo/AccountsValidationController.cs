﻿using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.BLL.Common.Attributes;
using Clouds42.Common.DataValidations;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Validators.Account.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2.WebApiPromo
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountsValidationController(IUnitOfWork unitOfWork) : ControllerBase
    {
        /// <summary>
        /// Валидация имени пользователя
        ///     0 - имя валидное          
        ///     2 - имя не соответствует маске
        /// </summary>
        [HttpGet]
        [ActionName("Name")]
        public int ValidateName(string value)
        {
            var validator = new NameValidation(value);

            if (!validator.NameLengthIsValid()) return 1;

            if (!validator.NameMaskIsValid()) return 2;

            return 0;
        }

        /// <summary>
        /// Валидация Email
        ///     0 - почта валидная
        ///     1 - почта уже используется
        ///     2 - почта не соответствует заданной маске
        /// </summary>
        [HttpGet]
        [ActionName("Email")]
        public int ValidateEmail(string value)
        {

            if (string.IsNullOrEmpty(value))
                return 2;

            var validator = new EmailValidation(value);

            if (!validator.EmailMaskIsValid()) return 2;

            return !validator.EmailIsAvailable(unitOfWork) ? 1 : 0;
        }

        /// <summary>
        /// Валидация телефонного номера
        ///     0 - номер валидный
        ///     1 - номер уже используется
        /// </summary>
        [HttpGet]
        [ActionName("PhoneNumber")]
        [PhoneNumber]
        public async Task<int> ValidatePhoneNumber([FromQuery] PhoneNumberModelDto userPhone)
        {
            var validator = new PhoneNumberValidation(userPhone.PhoneNumber);
            return await validator.PhoneNumberIsAvailable(unitOfWork) ? 0 : 1;
        }
    }
}
