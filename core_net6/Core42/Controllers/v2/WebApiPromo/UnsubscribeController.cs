﻿using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountUsers.Contracts;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2.WebApiPromo
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class UnsubscribeController(IAccountUsersManager accountUserManager) : BaseController
    {
        [HttpGet]
        public async Task<bool> CancelSubscription(string code)
        {
            var result = await accountUserManager.Unsubscribed(code, null);

            return !result.Error;
        }

    }
}
