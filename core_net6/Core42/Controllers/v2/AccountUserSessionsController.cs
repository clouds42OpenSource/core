﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Core42.Helpers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.AccountUser.AccountUserSession;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing cloud users sessions by authorization tokens
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountUserSessionsController(
        AccountUserSessionLoginManager userSessionLoginManager,
        IAccountUserSessionManager accountUserSessionManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// ValidateBinding cloud user token
        /// </summary>
        /// <param name="token">Cloud user token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccounUserTokenValidityDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Esdl)]
        public async Task<IActionResult> CheckTokenValidity(Guid token)
        {
            var result = await accountUserSessionManager.CheckTokenValidityAsync(token);
            return CreateResult(result, x => new AccounUserTokenValidityDto(x), HttpContext);
        }

        /// <summary>
        /// Get cloud user identifier by token
        /// </summary>
        /// <param name="token">Cloud user authorization token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserIDDto))]
        [ExternalService(ExternalServiceTypeEnum.Esdl)]
        public IActionResult GetAccountUserIDByToken(Guid token)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = accountUserSessionManager.GetAccountUserIdByToken(token);
            var answer = CreateResult(result, x => new AccountUserIDDto { AccountUserID = x.Data }, HttpContext);
            timer.Stop();

            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, result.Result.Head);
            return answer;

        }

        /// <summary>
        /// Get token by cloud user login
        /// </summary>
        /// <param name="login">Cloud user login</param>
        /// <returns>Токен</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TokenDto))]
        public IActionResult GetTokenByLogin(string login)
        {
            var managerResult = accountUserSessionManager.GetTokenByLogin(login);
            return CreateResult(managerResult, accSession => new TokenDto(managerResult.Result), HttpContext);
        }

        /// <summary>
        /// Get cloud user properties by session token
        /// </summary>
        /// <param name="token">Cloud user session token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserPropertiesChangedModelDto))]
        public async Task<IActionResult> GetUserPropertiesByToken(Guid token)
        {
            var result = await accountUserSessionManager.GetUserPropertiesByTokenAsync(token);

            return CreateResult(result, x => new AccountUserPropertiesChangedModelDto
            {
                AccountUserID = x.Id,
                AccountId = x.AccountId,
                Login = x.Login ?? string.Empty,
                Email = x.Email ?? string.Empty,
                FirstName = x.FirstName ?? string.Empty,
                LastName = x.LastName ?? string.Empty,
                MiddleName = x.MiddleName ?? string.Empty,
                CorpUserID = x.CorpUserID,
                CorpUserSyncStatus = x.CorpUserSyncStatus ?? string.Empty,
                Removed = x.Removed.HasValue && x.Removed.Value,
                FullPhoneNumber = x.PhoneNumber ?? string.Empty,
                CreationDate = x.CreationDate?.ToString("O") ?? string.Empty,
            }, HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Sign in to the cloud using login/password. Creates an entry with a new user session
        /// </summary>
        /// <param name="model">Sign-in parameters model</param>
        /// <returns>Created session token</returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserTokenDto))]
        public IActionResult Login([FromBody] LoginModelDto model)
        {
            var result = userSessionLoginManager.Login(model);
            return CreateResult(result, x => new AccountUserTokenDto(x), HttpContext);
        }

        /// <summary>
        /// Sign in to the cloud using email/password. Creates an entry with a new user session
        /// </summary>
        /// <param name="model">Sign-in parameters model</param>
        /// <returns>Created session token</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountTokenAuthorizationDto))]
        public IActionResult LoginByEmail([FromBody] LoginEmailModelDto model)
        {
            var result = userSessionLoginManager.LoginByEmail(model.Email, model.Password);
            return CreateResult(result, x => new AccountTokenAuthorizationDto
            {
                AccountToken = x
            }, HttpContext);
        }

        #endregion

    }
}
