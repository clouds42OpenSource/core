﻿using System;
using Asp.Versioning;
using Clouds42.BLL.Common;
using Clouds42.Domain.Enums;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class LogController(IServiceProvider serviceProvider) : BaseController
    {
        private readonly ICloudChangesProvider _cloudChangesProvider = serviceProvider.GetRequiredService<ICloudChangesProvider>();

        [HttpGet]
        [Route("/api_v{version:apiVersion}/[action]")]
        public IActionResult LogEvent([FromQuery] Guid accountId, [FromQuery] LogActions log, [FromQuery] string description)
        {
            var result = _cloudChangesProvider.LogEvent(accountId, log, description);

            return Json(new { success = !result.Error, data = result.Message });
        }
    }

}
