﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.AccountDatabase.Activity.Managers;
using Clouds42.AccountDatabase.Backup.Managers;
using Clouds42.AccountDatabase.Card.Manager;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Data.Managers;
using Clouds42.AccountDatabase.Edit.Managers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Managers;
using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.AccountDatabase.ServiceExtensions.Managers;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.Security;
using Clouds42.MyDatabaseService.Calculator.Managers;
using Clouds42.MyDatabaseService.CreateAccountDatabases.Managers;
using CommonLib.Enums;
using Core42.Application.Features.AccountDatabaseContext.Queries;
using Core42.Attributes;
using Core42.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PagedListExtensionsNetFramework;
using AccountDatabaseIdDto = Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels.AccountDatabaseIdDto;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Controller for AccountDatabase entity management
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]")]
    public class AccountDatabasesController(
        AccountDatabasePublishManager accDbPublishManager,
        IAccountDatabaseDataManager accountDatabaseDataManager,
        IAccountDatabaseManager accountDatabaseManager,
        AccountDatabaseEditManager accountDatabaseEditManager,
        AccountDatabaseMcobManager accountDatabaseMcobManager,
        AccountDatabaseTombManager accountDatabaseTombManager,
        CreateAccountDatabasesManager createAccountDatabasesManager,
        CreateDatabaseManager createDatabasesManager,
        AccountDatabasesCommonDataManager accountDatabasesCommonDataManager,
        AccountDatabaseCardManager accountDatabaseCardManager,
        CalculateCostOfAcDbAccessesManager calculateCostOfAcDbAccessesManager,
        AccountDatabaseHelper accountDatabaseHelper,
        IAccountDatabasesDataManager accountDatabasesDataManager,
        StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager,
        ChangeAccountDatabaseManager changeAccountDatabaseManager,
        CreateAccountDatabaseDataManager createAccountDatabaseDataManager,
        CalculateCostOfPlacementDatabaseManager calculateCostOfPlacementDatabaseManager,
        ServiceExtensionDatabaseDataManager serviceExtensionDatabaseDataManager,
        BackupManager backupManager,
        IAccessProvider accessProvider,
        ReactivateServiceManager reactivateServiceManager,
        IManageServiceExtensionDatabaseManager manageServiceExtensionDatabaseManager,
        IFixationAccountDatabaseActivityManager fixationAccountDatabaseActivityManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// get data for the services page
        /// </summary>
        /// <param name="availableServiceExtensionsForDatabaseFilterDto"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Services")]
        public async Task<IActionResult> GetAccDBServicesData([FromQuery] AvailableServiceExtensionsForDatabaseFilterDto availableServiceExtensionsForDatabaseFilterDto)
        {
            var result = await serviceExtensionDatabaseDataManager.GetCompatibleWithDatabaseServiceExtensions(availableServiceExtensionsForDatabaseFilterDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Get service options
        /// </summary>
        /// <param name="serviceId">ID of the service from which to get options</param>
        /// <returns>Service Options</returns>
        [HttpGet]
        [Route("Services/Status")]
        public IActionResult GetServiceStatus([FromQuery] Guid serviceId, [FromQuery] Guid accountDatabaseId)
        {
            var result = serviceExtensionDatabaseDataManager.GetServiceExtensionDatabaseStatus(serviceId, accountDatabaseId);

            return ResponseResult(result);
        }

        /// <summary>
        /// Get a list of account databases on the account management page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountId:guid}/GetAccountDatabases")]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult GetAccountDatabases([FromRoute] Guid accountId, [FromQuery] Guid accountUserId, [FromQuery] AccountDatabaseAffiliationTypeEnum affiliation,
           [FromQuery] string field, [FromQuery] int page, [FromQuery] string search, [FromQuery] int size, [FromQuery] SortType sort, [FromQuery] AccountDatabaseTypeEnum type, bool isNewPage)
        {
            var accountDatabasesFilterParamsDto = accountDatabaseHelper.MapToAccountDatabasesFilterParams(accountId, accountUserId,
                affiliation, field, page, search, size, sort, type, isNewPage);

            var result = accountDatabasesDataManager.GetDatabasesDataByFilter(accountDatabasesFilterParamsDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// get general data for working with account databases
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="accountUserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Availability")]
        public IActionResult GetAvailabilityAccountData([FromQuery] Guid accountId, [FromQuery] Guid accountUserId)
        {
            var result = accountDatabasesCommonDataManager.GetCommonDataForWorkingWithDatabases(accountId, accountUserId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get the general data model of the infobase card
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Items")]
        public IActionResult GetDatabaseCardData([FromQuery] Guid accountID, [FromQuery] Guid databaseId)
        {
            var result = accountDatabaseCardManager.GetDatabaseCardGeneralData(databaseId, accountID);
            return ResponseResult(result);
        }

        /// <summary>
        /// check access to database
        /// </summary>
        /// <param name="objectActions"></param>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Rights")]
        public IActionResult GetAccessToDatabase([FromQuery] List<ObjectAction> objectActions, [FromQuery] Guid databaseId)
        {
            var result = accountDatabaseManager.GetAccountDatabaseAccess(objectActions, databaseId);
            return ResponseResult(result);
        }

        /// <summary>
        /// get database common support data
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SupportData")]
        public IActionResult GetAcDbSupportData([FromQuery] Guid databaseId)
        {
            var result = accountDatabaseCardManager.GetDatabaseCardAcDbSupportData(databaseId);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get account-database access data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Access")]
        public async Task<IActionResult> GetUserListWithAccessToDatabase([FromQuery] GetAccountDatabaseUsersAccessQuery query)
        {
            return ResponseResult(await Mediator.Send(query));
        }

        /// <summary>
        /// Get account-database access data
        /// </summary>
        /// <param name="databaseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("external-access")]
        public async Task<IActionResult> GetUserListWithExternalAccessToDatabase([FromQuery] Guid databaseId)
        {
            try
            {
                var result = new DatabaseCardAcDbAccessesDataDto
                {
                    DatabaseAccesses = await (await Mediator.Send(new GetAccountDatabaseExternalAccessesQuery { AccountDatabaseId = databaseId })).ToListAsync()
                };
                return ResponseResult(result.ToOkManagerResult());
            }
            catch (Exception e)
            {
                Logger.Warn(e, $"[Ошибка получения данных внешних доступов информационной базы]  '{databaseId}'.");

                return ResponseResult(PagedListExtensions.ToPreconditionFailedManagerResult<DatabaseCardAcDbAccessesDataDto>(e.Message));
            }
        }

        /// <summary>
        /// Find an external user to grant access
        /// </summary>
        /// <param name="databaseId"></param>
        /// <param name="accountUserEmail"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Access/External-user")]
        public IActionResult SearchExternalAccountUserForDatabaseAccess([FromQuery] Guid databaseId, [FromQuery] string accountUserEmail)
        {
            var result = accountDatabaseCardManager.SearchExternalAccountUserForGrandAccess(databaseId, accountUserEmail);
            return ResponseResult(result);
        }

        /// <summary>
        /// Calculate the cost of accesses in the configuration
        /// </summary>
        /// <param name="calculateAccessesCostDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Access/Access-price")]
        public IActionResult CalculateAccessesCost([FromBody] CalculateAccessesCostDto calculateAccessesCostDto)
        {
            var result = calculateCostOfAcDbAccessesManager.CalculateAccessesCostForConfigurations
                (calculateAccessesCostDto.ServiceTypesIdsList, calculateAccessesCostDto.AccountUserDataModelsList,
                calculateAccessesCostDto.AccountId, calculateAccessesCostDto.PaidDate);

            return ResponseResult(result);
        }


        /// <summary>
        /// get data for the archiv-copies page
        /// </summary>
        /// <param name="databaseCardDbBackupsFilterDto"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Backups")]
        public IActionResult GetDatabaseCardDbBackupsDataByFilter([FromQuery] DatabaseCardDbBackupsFilterDto databaseCardDbBackupsFilterDto)
        {
            var result = accountDatabaseCardManager.GetDatabaseCardDbBackupsDataByFilter(databaseCardDbBackupsFilterDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Get data about infobase service
        /// </summary>
        /// <param name="accountDatabaseId">Infobase identifier</param>
        /// <returns>Информацию  о сервисе в информационной базе</returns>
        [HttpGet]
        [Route("/api_v{version:apiVersion}/account-databases/{accountDatabaseId}/services")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceExtensionDatabaseInfoDto))]
        public async Task<IActionResult> GetServiceExtensionStatusesForAccountDatabase([FromRoute] Guid accountDatabaseId)
        {
            var result = await accountDatabaseManager.GetServiceExtensionStatusesForAccountDatabase(accountDatabaseId);
            return CreateResultNoCast(result, HttpContext);
        }

        /// <summary>
        /// Get a list of delimiter bases available to the user
        /// </summary>
        /// <param name="accountUserId">User identifier</param>
        /// <param name="configurationCode">Configuration code (optional)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDbsOnDelimetersList")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DelimitersDbsListDto))]
        public async Task<IActionResult> GetDbsOnDelimetersList(Guid accountUserId, string configurationCode = null)
        {
            var managerResult =
                await accountDatabaseDataManager.GetDelimitersDbsByAccUserAsync(accountUserId, configurationCode);

            return CreateResult(managerResult, x => new DelimitersDbsListDto(x), HttpContext);
        }

        /// <summary>
        /// Get infobase status
        /// </summary>
        /// <param name="accountDatabaseId">Infobase identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDatabaseStatusInfo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DatabaseStatusDto))]
        public async Task<IActionResult> GetDatabaseStatusInfo(Guid accountDatabaseId)
        {
            var managerResult = await accountDatabaseDataManager.GetDatabaseStatusInfo(accountDatabaseId);

            return CreateResult(managerResult, x => x, HttpContext);
        }

        /// <summary>
        /// Get infobase data
        /// </summary>
        /// <param name="accountDatabaseId">Infobase number</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetProperties")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasePropertiesDto))]
        public async Task<IActionResult> GetProperties(Guid accountDatabaseId)
        {
            var accountDatabaseMgrResult = await accountDatabaseDataManager.GetAccountDatabasesByIdAsync(accountDatabaseId);
            return CreateResult(accountDatabaseMgrResult, x => x, HttpContext);
        }

        /// <summary>
        /// Get infobase data
        /// </summary>
        /// <param name="accountDatabaseId">Infobase identifier</param>
        /// <param name="accountUserId">Identifier of the user requesting the data</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAccountDatabasesForUserById")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasePropertiesDto))]
        public async Task<IActionResult> GetAccountDatabasesForUserById(Guid accountDatabaseId, Guid accountUserId)
        {
            var accountDatabaseMgrResult = await accountDatabaseDataManager.GetAccountDatabasesForUserById(accountDatabaseId, accountUserId);
            return CreateResult(accountDatabaseMgrResult, x => x, HttpContext);
        }

        /// <summary>
        /// Get infobase data
        /// </summary>
        /// <param name="infoBase">Infobase name</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDatabaseInfo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasePropertiesDto))]
        public async Task<IActionResult> GetDatabaseInfo(string infoBase)
        {
            var accountDatabaseMgrResult = await accountDatabaseDataManager.GetAccountDatabasesByNameAsync(infoBase);
            return CreateResult(accountDatabaseMgrResult, x => x, HttpContext);
        }

        /// <summary>
        /// Get instructions for launching the infobase
        /// </summary>
        /// <param name="accountDatabaseId">Infobase identifier</param>
        /// <param name="accountUserId">Identifier ot the user launching the infobase</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLaunchDatabaseInstructions")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseLaunchInstructionsDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public async Task<IActionResult> GetLaunchDatabaseInstructions(Guid accountDatabaseId, Guid accountUserId)
        {
            return CreateResultNoCast(await Mediator.Send(new GetLaunchDatabaseInstructionsQuery(accountDatabaseId, accountUserId)), HttpContext);
        }

        /// <summary>
        /// Get list of infobase IDs in scope of specific account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetIDs")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasesIDsListDto))]
        public IActionResult GetIDs(Guid accountId)
        {
            var result = accountDatabaseManager.GetIDs(accountId);
            return CreateResult(result, x => new AccountDatabasesIDsListDto { AccountDatabasesIDs = new GuidListItemDto { List = x.ToList() } }, HttpContext);
        }

        /// <summary>
        /// Get a list of users who have access to the infobase
        /// </summary>
        /// <param name="accountDatabaseId">Infobase identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAccountDatabaseUserAccessList")]
        public IActionResult GetAccountDatabaseUserAccessList(Guid accountDatabaseId)
        {
            var result = accountDatabaseDataManager.GetAccountDatabaseUserAccessList(accountDatabaseId);
            return CreateResult(result, x => result.Result, HttpContext);
        }

        /// <summary>
        /// Get remote application settings
        /// </summary>
        /// <param name="accountDatabaseId">Account identifier</param>
        /// <param name="accountUserId">Account user identifier</param>
        /// <param name="launchMode">Launch mode</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRemoteAppParams")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseRemoteParamsDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Promo)]
        public IActionResult GetRemoteAppParams(Guid accountDatabaseId, Guid accountUserId, string launchMode = default)
        {
            var result = accountDatabaseManager.GetRemoteAppParams(accountDatabaseId, accountUserId, launchMode);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Create a RDP link string
        /// </summary>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns>Remote application settings</returns>
        [HttpGet]
        [Route("BuildRemoteDesktopLinkString")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseRemoteParamsDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult BuildRemoteDesktopLinkString(Guid accountUserId)
        {
            var result = accountDatabaseManager.BuildRemoteDesktopLinkString(accountUserId);
            return CreateResult(result, x => new AccountDatabaseRemoteParamsDto(x), HttpContext);
        }

        [HttpPost]
        [Route("SetLaunchType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [AllowAnonymous]
        public IActionResult SetLaunchType([FromBody] PostRequestLaunchTypeDto model)
        {
            var result = accountDatabaseManager.SetLaunchType(model);
            return CreateResultEmpty(result, HttpContext);
        }

        [HttpGet]
        [Route("GetAccountBallanse")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasesBalanceDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult GetAccountBallanse([FromQuery] Guid AccountID)
        {
            var result = accountDatabaseManager.GetAccountBallanse(AccountID);
            return CreateResult(result, x => new AccountDatabasesBalanceDto(x), HttpContext);
        }

        /// <summary>
        /// Get a list of databases available to the user.
        /// </summary>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAccessDatabaseList")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseListDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public async Task<IActionResult> GetAccessDatabaseList(Guid accountUserId)
        {
            var result = await accountDatabaseDataManager.GetAccessDatabaseListAsync(accountUserId);
            return CreateResult(result, x => new AccountDatabaseListDto(x), HttpContext);
        }

        [HttpGet]
        [Route("GetLocalAccountDatabases")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LocalAccountDatabasesListDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult GetLocalAccountDatabases(Guid accountID)
        {
            var result = accountDatabaseManager.GetLocalAccountDatabases(accountID);
            return CreateResult(result, x => new LocalAccountDatabasesListDto(x), HttpContext);
        }

        #endregion

        #region POSTs
        /// <summary>
        /// Get demo period for service
        /// </summary>
        /// <param name="activateDemoServiceDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Services/Demo")]
        public IActionResult ActivateDemoService([FromBody] ActivateDemoServiceDto activateDemoServiceDto)
        {
            var accountAdmins = accessProvider.GetAccountAdmins(activateDemoServiceDto.AccountId);
            if (!accountAdmins.Any())
                return JsonDataStatus(false,
                    new { ErrorMessage = "Невозможно активировать сервис. У аккаунта нет ни одного администратора." });
            var managerResult =
                reactivateServiceManager.ActivateServiceForExistingAccount(activateDemoServiceDto.ServiceId, accountAdmins.First());

            return ResponseResult(managerResult);
        }


        /// <summary>
        /// Activate or deactivate extension service for database
        /// </summary>
        /// <param name="deleteServiceExtensionFromDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Services")]
        public IActionResult ActivateOrDeactivateService([FromBody] DeleteServiceExtensionFromDatabaseDto deleteServiceExtensionFromDatabaseDto)
        {
            if (deleteServiceExtensionFromDatabaseDto.Install)
            {
                var activateModel = new InstallServiceExtensionForDatabasesDto
                {
                    DatabaseIds = [deleteServiceExtensionFromDatabaseDto.DatabaseId],
                    ServiceId = deleteServiceExtensionFromDatabaseDto.ServiceId
                };

                var activateResult = manageServiceExtensionDatabaseManager.InstallServiceExtensionForDatabases(activateModel);
                return ResponseResult(activateResult);
            }

            var deactivateResult =
                manageServiceExtensionDatabaseManager
                    .DeleteServiceExtensionFromDatabase(deleteServiceExtensionFromDatabaseDto);

            return ResponseResult(deactivateResult);
        }

        /// <summary>
        /// end all database sessions
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Session")]
        public IActionResult EndAllSession([FromBody] StartProcessOfTerminateSessionsInDatabaseDto model)
        {
            var result = startProcessToWorkDatabaseManager.StartProcessOfTerminateSessionsInDatabase(model);

            return ResponseResult(result);
        }

        /// <summary>
        /// Clear account database cache
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("ClearCache")]
        public IActionResult DeleteAcDbCache([FromBody] DeleteAcDbCacheDto deleteAcDbCacheDto)
        {
            var result = accountDatabaseDataManager.DeleteAccountDatabaseCache(deleteAcDbCacheDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Reload database iis pool
        /// </summary>
        /// <param name="restartAccountIisApplicationPoolDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("IIS-pool")]
        public IActionResult RestartAccountIisApplicationPool([FromBody] RestartAccountIisApplicationPoolDto restartAccountIisApplicationPoolDto)
        {
            var result = accDbPublishManager.RestartApplicationPoolOnIis(restartAccountIisApplicationPoolDto.databaseId);

            return ResponseResult(result);
        }

        /// <summary>
        /// Authorization in database
        /// </summary>
        /// <param name="authorizeInDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SupportData")]
        public IActionResult UpdateSupportDataAndAuthorize([FromBody] SupportDataDto authorizeInDatabaseDto)
        {
            var result = accountDatabaseManager.SaveSupportDataAndAuthorize(authorizeInDatabaseDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// DeAuthorization in database
        /// </summary>
        /// <param name="deAuthorizationDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Deauthorization")]
        public IActionResult DeAuthorizeInDatabase([FromBody] DeAuthorizationDatabaseDto deAuthorizationDatabaseDto)
        {
            var result = accountDatabaseManager.DisableSupportAuthorization(deAuthorizationDatabaseDto.DatabaseId);

            return ResponseResult(result);
        }

        /// <summary>
        /// Change database type filebase or serverbase
        /// </summary>
        /// <param name="changeAccountDatabaseTypeDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(Type))]
        public IActionResult ChangeAccountDatabaseType([FromBody] ChangeAccountDatabaseTypeDto changeAccountDatabaseTypeDto)
        {
            var result = accountDatabaseEditManager.ChangeAccountDatabaseType(changeAccountDatabaseTypeDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Publish database
        /// </summary>
        /// <param name="publishDatabaseDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Publish")]
        public IActionResult PublishDatabase([FromBody] PublishDatabaseDto publishDatabaseDto)
        {
            if (publishDatabaseDto.Publish)
            {
                var result1 = accDbPublishManager.PublishDatabase(publishDatabaseDto.DatabaseId);
                return ResponseResult(result1);
            }

            var model = new PostRequestAccDbIdDto { AccountDatabaseId = publishDatabaseDto.DatabaseId };
            var result = accDbPublishManager.CancelPublishDatabase(model);

            return ResponseResult(result);
        }

        /// <summary>
        /// Change database release
        /// </summary>
        /// <param name="changePlatformTypeDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Release")]
        public IActionResult ChangePlatformType([FromBody] ChangePlatformTypeDto changePlatformTypeDto)
        {
            var result = accountDatabaseEditManager.ChangeAccountDatabasePlatformType
                (changePlatformTypeDto.DatabaseId, changePlatformTypeDto.PlatformType, changePlatformTypeDto.DistributionType);

            return ResponseResult(result);
        }


        /// <summary>
        /// Make an request for change database type
        /// </summary>
        /// <param name="requestToChangeAccDbTypeDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(ChangeAccDbTypeRequest))]
        public IActionResult ChangeAccDbTypeRequest([FromBody] RequestToChangeAccDbTypeDto requestToChangeAccDbTypeDto)
        {
            var result = accountDatabaseEditManager.ChangeAccDbTypeRequest(requestToChangeAccDbTypeDto);
            return ResponseResult(result);
        }

        /// <summary>
        /// Solo update database Version
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("update-database")]
        public async Task<IActionResult> UpdateDatabase([FromBody] Guid AccountDatabaseId)
        {
            var result = await accountDatabaseEditManager.UpdateDatabase(AccountDatabaseId);
            return ResponseResult(result);
        }
        /// <summary>
        /// Sets last client activity date
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetLastActivityDate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetLastActivityDate([FromBody]PostRequestAccDbLastActivityDateDto model)
        {
            var result = accountDatabaseManager.SetLastActivityDate(model);
            return CreateResultEmpty(result, HttpContext);

        }

        /// <summary>
        /// Delete specific 1C database description from SQL server
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [Obsolete("Use AccountDatabases HttpDelete method instead")]
        public IActionResult Delete([FromBody]PostRequestAccDbIdDto model)
        {
            if (model is not { IsValid: true })
                return CreateResultEmpty(new ManagerResult
                {
                    State = ManagerResultState.PreconditionFailed,
                    Message = "Model is not valid"
                }, HttpContext);

            return CreateResultEmpty(accountDatabaseTombManager.DeleteAccountDatabaseToTomb(model.AccountDatabaseId, false,
                true, CreateBackupAccountDatabaseTrigger.FromApi), HttpContext);
        }

        /// <summary>
        /// Set 1C application name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetApplicationName")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [AllowAnonymous]
        public IActionResult SetApplicationName([FromBody] PostRequestAppNameDto model)
        {
            var result = accountDatabaseManager.SetApplicationName(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set the distribution type of 1C platform
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetDistributiveType")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [AllowAnonymous]
        public IActionResult SetDistributiveType([FromBody] PostRequestSetDistributiveTypeDto model)
        {
            var result = accountDatabaseManager.SetDistributiveType(model);
            return CreateResultEmpty(result, HttpContext);
        }

        [HttpPost]
        [Route("SaveLaunchParameters")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [AllowAnonymous]
        public IActionResult SaveLaunchParameters([FromBody] AccountDatabaseLaunchParametersDto model)
        {
            var result = accountDatabaseManager.SaveLaunchParameters(model.AccountDatabaseID, model.LaunchParameter);
            return CreateResultEmpty(result, HttpContext);
        }

        [HttpPost]
        [Route("PublishOnWeb")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabasePublishPathDto))]
        public IActionResult PublishOnWeb([FromBody] PublishDto model)
        {
            var result = accDbPublishManager.PublishDatabase(model);
            return CreateResult(result, x => new AccountDatabaseWebPublishPathDto(x), HttpContext);
        }

        /// <summary>
        /// Grant access to the user to the database with roles
        /// </summary>
        [HttpPost]
        [Route("McobAddUserToDatabase")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SimpleResultModelDto<string>))]
        public IActionResult McobAddUserToDatabase([FromBody] McobUserAddDto model)
        {
            var result = accountDatabaseMcobManager.McobAddUserToDatabase(model.DataBaseName, model.UserLogin, model.UserName, model.Roles, model.JhoRoles);
            return CreateResult(result, x => new SimpleResultModelDto<string> { Value = x }, HttpContext);
        }

        ///<summary>
        /// Fixate last activity datetime
        ///</summary>
        ///<param name="model"></param>
        ///<returns></returns>
        [HttpPost]
        [Route("FixationLastActivity")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [AllowAnonymous]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult FixationLastActivity([FromBody] PostRequestSetActivityDateDto model)
        {
            if (model.AccountDatabaseID != Guid.Empty)
            {
                fixationAccountDatabaseActivityManager.FixLastActivity(model.AccountDatabaseID);
            }
            fixationAccountDatabaseActivityManager.FixLaunchActivity(model, model.LinkLaunchActionInfo == null ? "IP не определен" : model.LinkLaunchActionInfo.ExternalIpAddress);

            return CreateResultEmpty(new ManagerResult { State = ManagerResultState.Ok }, HttpContext);
        }

        [HttpPost]
        [Route("SaveLocalAccountDatabases")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountDatabaseIdDto))]
        public IActionResult SaveLocalAccountDatabases([FromBody] SaveLocalAccountDatabasesDto model)
        {
            var result = accountDatabaseManager.SaveLocalAccountDatabases(model);
            return CreateResult(result, x => new AccountDatabaseIdDto(x), HttpContext);
        }

        /// <summary>
        /// Create Account Database from DT file
        /// </summary>
        /// <param name="createAcDbFromDtDto"></param>
        /// <returns>Результат создания</returns>
        [HttpPost]
        [Route("/api_v{version:apiVersion}/AccountDatabase/CreateFromDT")]
        public IActionResult CreateDtDatabase([FromBody] CreateAcDbFromDtDto createAcDbFromDtDto)
        {
            var result =
                createAccountDatabasesManager.CreateAccountDatabaseFromDtFile(createAcDbFromDtDto.AccountId,
                    createAcDbFromDtDto);

            return ResponseResult(result);
        }


        /// <summary>
        /// Load Account Database from DT file
        /// </summary>
        /// <param name="createAcDbFromDtDto"></param>
        /// <returns>Результат создания</returns>
        [HttpPost]
        [Route("/api_v{version:apiVersion}/accountDatabase/convert-dt")]
        public IActionResult LoadDtFileDatabase([FromBody] CreateAcDbDtDto createAcDbFromDtDto)
        {
            var result =
                createDatabasesManager.LoadAccountDatabaseАfterСonversion(createAcDbFromDtDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Calculate the cost of the created inf. bases
        /// </summary>
        /// <param name="accountId">identifier of account</param>
        /// <param name="databasesCount">Number of bases</param>
        /// <returns>The cost of the created inf. bases</returns>
        [HttpGet]
        [Route("{accountId}/CostOfCreatingDatabases")]
        public async Task<IActionResult> CalculateCostOfCreatingDatabases([FromRoute] Guid accountId, [FromQuery] int databasesCount)
        {
            var result = await calculateCostOfPlacementDatabaseManager.CalculateCostDatabases(
                new CalculateCostOfPlacementDatabasesDto
                {
                    AccountId = accountId,
                    DatabasesForPlacementCount = databasesCount,
                    SystemServiceType = ResourceType.CountOfDatabasesOverLimit
                });

            return ResponseResult(result);
        }

        /// <summary>
        /// Get databases state for the requested databases
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("States")]
        public IActionResult GetAccountDatabaseStates([FromQuery] AccountDatabasesListIdDTO model)
        {
            var result = createAccountDatabaseDataManager.GetAccountDatabaseStates(model.AccountDatabasesIDs);
            return ResponseResult(result);
        }

        /// <summary>
        /// Calculate the cost of accesses in the configuration for upload file
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="configurationId">Configuration identifier</param>
        /// /// <param name="configurationName">Configuration name</param>
        /// <returns></returns>
        [HttpGet]
        [Route("UploadFile/Access-price")]
        public IActionResult CalculateAccessesCost([FromQuery] Guid accountId, [FromQuery] string configurationId, [FromQuery] string configurationName)
        {
            return ResponseResult(calculateCostOfAcDbAccessesManager.CalculateAccessesCostForUploadFile(accountId, configurationId, configurationName));
        }

        /// <summary>
        /// Create infobase from ZIP-file
        /// </summary>
        /// <param name="createAcDbFromZipDto">Parametes model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ZipDatabase")]
        public IActionResult CreateZipDatabase([FromBody] CreateAcDbFromZipForMSDto createAcDbFromZipDto)
        {
            var result = createAccountDatabasesManager.CreateDatabaseOnDelimitersFromZip(createAcDbFromZipDto.AccountId,
                createAcDbFromZipDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Create infobase from Dt in ZIP
        /// </summary>
        /// <param name="createAcDbFromDtToZipDto">Parametes model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("dtTozipDatabase")]
        public IActionResult CreateDtToZipDatabase([FromBody] CreateAcDbFromDtToZipForMSDto createAcDbFromDtToZipDto)
        {
            var result = createAccountDatabasesManager.CreateDatabaseOnDelimitersFromDtToZip(createAcDbFromDtToZipDto.AccountId,
                createAcDbFromDtToZipDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// Run processes for archiving info bases to the crypt
        /// </summary>
        /// <param name="sendDatabaseToArchiveOrBackupDto">Process model for working with infobases</param>
        [HttpPost]
        [Route("ArchiveOrBackup")]
        public IActionResult StartProcessOfArchiveOrBackupDatabaseToTomb([FromBody] SendDatabaseToArchiveOrBackupDto sendDatabaseToArchiveOrBackupDto)
            => ResponseResult(backupManager.BackupAccountDatabase(sendDatabaseToArchiveOrBackupDto));

        #endregion

        #region PUTs
        /// <summary>
        /// Give or delete access to account-database
        /// </summary>
        /// <param name="startProcessesOfManageAccessesToDbDto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Access")]
        public IActionResult GiveOrDeleteAccesToAccountDatabase
            ([FromBody] StartProcessesOfManageAccessesToDbDto startProcessesOfManageAccessesToDbDto)
        {

            if (startProcessesOfManageAccessesToDbDto.GiveAccess)
            {
                var result1 = startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(startProcessesOfManageAccessesToDbDto);
                return ResponseResult(result1);
            }
            var result = startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(startProcessesOfManageAccessesToDbDto);

            return ResponseResult(result);
        }

        /// <summary>
        /// change some database items
        /// may change only 1 item from ChangeDatabaseInfoDto
        /// </summary>
        /// <param name="changeDatabaseInfoDto"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Items")]
        public IActionResult ChangeDbInfo([FromBody] ChangeDatabaseInfoDto changeDatabaseInfoDto)
        {
            if (changeDatabaseInfoDto.UsedWebServices != null)
            {
                var result = accountDatabaseManager.ChangeDbFlagUsedWebServices
                    (changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.UsedWebServices);
                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.DatabaseState != null)
            {
                var result = accountDatabaseEditManager.ChangeDatabaseState
                    (changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.DatabaseState.Value);
                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.TemplateId != null)
            {
                var result = changeAccountDatabaseManager.ChangeAccountDatabaseTemplate
                    (changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.TemplateId);
                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.Caption != null)
            {
                var result = accountDatabaseEditManager.SetCaption(new PostRequestAccDbCaptionDto
                {
                    AccountDatabaseID = changeDatabaseInfoDto.DatabaseId,
                    Caption = changeDatabaseInfoDto.Caption
                });
                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.V82Name != null)
            {
                var result = accountDatabaseEditManager.SetV82Name(changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.V82Name);
                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.HasModifications != null)
            {
                var result = accountDatabaseEditManager.ChangeDatabaseHasModificationsFlag
                    (changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.HasModifications.Value);

                return ResponseResult(result);
            }

            if (changeDatabaseInfoDto.RestoreModel != null)
            {
                var result = accountDatabaseEditManager.ChangeDatabaseRestoreModelType
                    (changeDatabaseInfoDto.DatabaseId, changeDatabaseInfoDto.RestoreModel.Value);

                return ResponseResult(result);
            }

            return ResponseResult(new ManagerResult { State = ManagerResultState.PreconditionFailed, Message = "не указано ни 1 поле" });
        }


        #endregion

        #region DELETEs
        /// <summary>
        /// Delete one or more bases
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteAccountDatabases")]
        public IActionResult DeleteAccountDatabases([FromBody] ProcessToWorkDatabasesDto processToWorkDatabasesDto)
        {
            var result = startProcessToWorkDatabaseManager.StartProcessOfRemoveDatabases(processToWorkDatabasesDto.DatabasesId);
            return ResponseResult(result);
        }
        #endregion
    }
}     
