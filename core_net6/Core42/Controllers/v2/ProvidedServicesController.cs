﻿using System;
using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Billing.Billing.ProvidedServices;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Account.AccountBilling.ProvidedServices;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]")]
    public class ProvidedServicesController(ProvidedServiceManager providedServiceManager) : BaseController
    {
        /// <summary>
        /// Get rendered services
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="query">Parameters model</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SelectDataResultCommonDto<ProvidedServiceDto>))]
        public IActionResult Get([FromQuery] Guid accountId,
            [FromQuery] SelectDataCommonDto<ProvidedServiceFilterDto> query = null)
        {
            var data = providedServiceManager.GetPaginatedProvidedServicesForAccount(accountId, query);
            return CreateResult(data, (x) => x, HttpContext);
        }

        /// <summary>
        /// Get the rendered services for long-term services for the current user
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="query">Parameters model</param>
        /// <returns></returns>
        [HttpGet]
        [Route("LongServices")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SelectDataResultCommonDto<ProvidedServiceDto>))]
        public IActionResult GetLongServices(
            [FromQuery] Guid accountId,
            [FromQuery] SelectDataCommonDto<ProvidedLongServiceFilterDto> query = null)
        {
            var data = providedServiceManager.GetPaginatedProvidedLongServicesForAccount(accountId,
                query);
            return CreateResult(data, (x) => x, HttpContext);
        }
    }
}
