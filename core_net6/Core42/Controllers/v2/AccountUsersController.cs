﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Core42.Models.AccountUsers;
using Clouds42.CloudServices.Rent1C.Manager;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Managers;
using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.BLL.Common.Attributes;
using Clouds42.AccountUsers.Contracts;
using Clouds42.DataContracts.Account.Registration;
using Microsoft.AspNetCore.Authorization;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Getting information about Clouds42 account users
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountUsersController(
        IAccountUserManager accountUserManager,
        IAccountUsersManager accountUsersManager,
        AccountUsersProfileManager accountUsersProfileManager,
        AccountUsersActivateManager accountUsersActivateManager,
        Rent1CConfigurationAccessManager rent1CConfigurationAccessManager,
        AccountUserResetPasswordNotificationManager accountUserResetPasswordNotificationManager,
        CloudTerminalServerSessionsManager cloudTerminalServerSessionsManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get paginated list of account users
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="query">Query parameters</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api_v{version:apiVersion}/account-users")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SelectDataResultCommonDto<AccountUserApiModel>))]
        public IActionResult GetPaginatedAccountUsers([FromQuery] Guid accountId,
            [FromQuery] PagniationRequestDto<Clouds42.DataContracts.AccountUser.Filters.AccountUsersFilterDto> query = null)
        {
            query ??= new PagniationRequestDto<Clouds42.DataContracts.AccountUser.Filters.AccountUsersFilterDto>();
            query.PageNumber = query.PageNumber <= 0 ? 1 : query.PageNumber;

            var result = accountUserManager.GetPaginatedUsersForAccount(accountId, query);
            return CreatePaginationResult(result, src =>
                new AccountUserApiModel
                {
                    AccountId = src.AccountId,
                    Activated = src.Activated,
                    CreatedInAd = src.CreatedInAd,
                    CreationDate = src.CreationDate,
                    Email = src.Email,
                    FirstName = src.FirstName,
                    FullName = src.FullName,
                    Id = (Guid)src.Id,
                    LastName = src.LastName,
                    Login = src.Login,
                    MiddleName = src.MiddleName,
                    PhoneNumber = src.PhoneNumber,
                    Unsubscribed = src.Unsubscribed,
                    AccountRoles = src.AccountRoles
                });
        }

        /// <summary>
        /// Get cloud user properties
        /// </summary>
        /// <param name="AccountUserID">Cloud user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserPropertiesDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Esdl)]
        public IActionResult GetProperties([FromQuery] Guid AccountUserID)
        {
            var result = accountUsersManager.GetProperties(AccountUserID);

            return CreateResult(result, x => new AccountUserPropertiesDto
            {
                ID = x.Id,
                AccountId = x.AccountId,
                Login = x.Login ?? string.Empty,
                Email = x.Email ?? string.Empty,
                FirstName = x.FirstName ?? string.Empty,
                LastName = x.LastName ?? string.Empty,
                MiddleName = x.MiddleName ?? string.Empty,
                CorpUserID = x.CorpUserID,
                CorpUserSyncStatus = x.CorpUserSyncStatus ?? string.Empty,
                Removed = x.Removed.HasValue && x.Removed.Value,
                FullPhoneNumber = x.PhoneNumber ?? string.Empty,
                CreationDate = x.CreationDate.HasValue ? x.CreationDate.Value.ToString("O") : string.Empty,
                IsManager = accountUsersManager.IsManager(AccountUserID),
                Activated = x.Activated
            }, HttpContext);
        }

        /// <summary>
        /// Get user roles
        /// </summary>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserPropertiesDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Esdl)]
        public IActionResult CheckAdminUserRole(Guid accountUserId)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = accountUsersManager.IsAccountAdmin(accountUserId);
            timer.Stop();

            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, result.Result.Head);
            return CreateResult(result, x => new AdminUserRoleDto(x.Data), HttpContext);
        }

        /// <summary>
        /// Get account identifier
        /// </summary>
        /// <param name="accountUserId">Cloud user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserAccountIDDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Promo)]
        public IActionResult GetAccountId(Guid accountUserId)
        {
            var result = accountUsersManager.GetProperties(accountUserId);
            return CreateResult(result, x => new AccountUserAccountIDDto { AccountID = x.AccountId }, HttpContext);
        }

        /// <summary>
        /// Get cloud user identifier
        /// </summary>
        /// <param name="email">E-mail</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserIdAndVerifireStatusDTO))]
        public IActionResult GetIdByEmail(string email)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = accountUsersManager.GetIdByEmail(email, out var headString);
            timer.Stop();

            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, headString);
            return CreateResult(result, x => new AccountUserIdAndVerifireStatusDTO { AccountUserID = x.AccountUserID, IsVerified = x.IsVerified }, HttpContext);
        }

        /// <summary>
        /// Get cloud user identifier
        /// </summary>
        /// <param name="Login">Cloud user login</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserIDDto))]
        public IActionResult GetIdByLogin([FromQuery] string Login)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = accountUsersManager.GetIdByLogin(Login, out var headString);

            timer.Stop();
            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, headString);

            return CreateResult(result, x => new AccountUserIDDto { AccountUserID = x }, HttpContext);
        }

        /// <summary>
        /// Get cloud user identifier
        /// </summary>
        /// <param name="phoneNumber">Phone number</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserIdAndVerifireStatusDTO))]
        public IActionResult GetIdByPhoneNumber(string phoneNumber)
        {
            var result = accountUsersManager.GetIdByPhoneNumber(phoneNumber);
            return CreateResult(result, x => new AccountUserIdAndVerifireStatusDTO { AccountUserID = x.AccountUserID, IsVerified = x.IsVerified }, HttpContext);
        }

        /// <summary>
        /// Get users identifiers
        /// </summary>
        /// <param name="accountID">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUsersIDsListDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl)]
        public IActionResult GetIDs(Guid accountID)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = accountUsersManager.GetIDs(accountID);

            timer.Stop();

            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, result.Result.Head);

            return CreateResult(result, x => new AccountUsersIDsListDto
            {
                AccountUserIDs = new GuidListItemDto
                {
                    List = x.Data
                }
            }, HttpContext);
        }

        /// <summary>
        /// Get cloud user login
        /// </summary>
        /// <param name="accountUserID">Cloud user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserLoginDto))]
        public IActionResult GetLogin(Guid accountUserID)
        {
            var result = accountUsersManager.GetProperties(accountUserID);
            return CreateResult(result, usr => new AccountUserLoginDto { Login = usr.Login }, HttpContext);
        }

        /// <summary>
        /// Get account users
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserListDto))]
        public IActionResult GetAccountUsers(Guid accountId)
        {
            var result = accountUsersManager.GetAccountUsersList(accountId);
            return CreateResult(result, x => new AccountUserListDto(x), HttpContext);
        }

        // CORP sync

        [HttpGet]
        public IActionResult UserWithRent1C(Guid userId)
        {
            var result = rent1CConfigurationAccessManager.GetUserWithRent1C(userId);
            return ResponseResult(result);
        }


        /// <summary>
        /// Check if the last administrator in the account
        /// </summary>
        /// <param name="accountId">account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        public IActionResult IsLastAdminAccount(Guid accountId)
        {
            var result = accountUsersManager.IsLastAdminAccount(accountId);
            return CreateResult(result, (x) => x, HttpContext);
        }

        /// <summary>
        /// Get all active sessions of users of the specified account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        [HttpGet]
        [Route("/api_v{version:apiVersion}/[controller]/{accountId:guid}/[action]")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserTerminateListDto))]

        public async Task<IActionResult> AccountUserActiveSessions([FromRoute] Guid accountId)
        {
            var result = await cloudTerminalServerSessionsManager.GetAccountUsersConnectionsInfoListAsync(accountId);

            if (result.Error)
                CreateResultEmpty(result, HttpContext);

            return CreateResult(result, x => new AccountUserTerminateListDto(x), HttpContext);
        }


        #endregion

        #region POSTs

        /// <summary>
        /// Add user to account
        /// </summary>
        /// <param name="userInfo">User data</param>
        /// <returns>Added user identifier</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserIDDto))]
        [AllowAnonymous]
        public async Task<IActionResult> AddToAccount([FromBody] AccountUserRegistrationModelDto userInfo)
        {
            var result = await accountUsersProfileManager.AddToAccount(userInfo);
            return CreateResult(result, x => new AccountUserIDDto { AccountUserID = x }, HttpContext);
        }

        /// <summary>
        /// Set user login
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetLogin([FromBody] AccountUsersLoginModelDto model)
        {
            var result = accountUsersProfileManager.SetLogin(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set user phone number
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [PhoneNumber]
        public async Task<IActionResult> SetPhoneNumber([FromBody] PhoneNumberModelDto model)
        {
            var result = await accountUsersManager.SetPhoneNumber(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Request user passrword reset
        /// </summary>
        /// <param name="email">Email to get reset password message</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult ResetPasswordRequest(string email)
        {
            var result = accountUserResetPasswordNotificationManager.SendPasswordChangeRequest(email);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set user first name
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetFirstName([FromBody] AccountUserFirstNameModelDto model)
        {
            var result = accountUsersManager.SetFirstName(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set user last name
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetLastName([FromBody] AccountUserLastNameModelDto model)
        {
            var result = accountUsersManager.SetLastName(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set user middle name
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetMiddleName([FromBody] AccountUserMiddleNameModelDto model)
        {
            var result = accountUsersManager.SetMiddleName(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Set user activated attribute
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult SetActivated([FromBody] AccountUserActivatedModel model)
        {
            var result = accountUsersManager.SetActivated(model);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Enable or disable a user
        /// </summary>
        /// <param name="model">model parameter</param>
        /// <returns>Execution result</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult Activate([FromBody] AccountUserActivateDto model)
        {
            var result = accountUsersActivateManager.Activate(model.accountUserID, model.isActivate);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Remove user
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult Delete([FromBody] AccountUserIdModel model)
        {
            var result = accountUsersProfileManager.Delete(model);

            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Close selected sessions of users of the current account
        /// </summary>
        /// <param name="model">model parameter</param>
        /// <returns>Execution result</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public async Task<IActionResult> AccountUserActiveSessions([FromBody] AccountUsersTerminateSessionDto model)
        {
            var result = await cloudTerminalServerSessionsManager.FindAndCloseUsersRdpSessions(model.accountId, model.userIds);
            return CreateResultEmpty(result, HttpContext);
        }

        #endregion
    }
}
