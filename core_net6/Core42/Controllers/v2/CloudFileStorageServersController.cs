﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Domain.Enums.Security;
using Clouds42.Segment.CloudServicesFileStorageServer.Managers;
using Core42.Attributes;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with file storages
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class CloudFileStorageServersController(ICloudFileStorageServerManager cloudFileStorageServerManager)
        : BaseController
    {
        #region GET methods

        /// <summary>
        /// Get file storage path
        /// </summary>
        /// <param name="FileStorageServerID">File storage identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CloudFileStorageServerPathDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public async Task<IActionResult> GetFileStorageServerPath([FromQuery] Guid FileStorageServerID)
        {
            var result = await cloudFileStorageServerManager.GetFileStorageServerPath(FileStorageServerID);
            return CreateResult(result, x => new CloudFileStorageServerPathDto
            {
                StorageServerPath = x
            }, HttpContext);
        }

        #endregion
    }
}
