﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Asp.Versioning;
using AutoMapper;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.GoogleCloud.SpreadSheet.Interfaces;
using Clouds42.GoogleCloud.SpreadSheet.Jobs;
using Clouds42.GoogleCloud.SpreadSheet.Models;
using Core42.Controllers.v1;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2;

[ApiVersion("1.0")]
[Route("sheets")]
public class SheetsController(
    IGoogleSheetsProvider googleSheetsProvider,
    BillingServiceManager billingServiceManager,
    IMapper mapper,
    IUpdatePartnerClientsGoogleSheetJobWrapper updatePartnerClientsGoogleSheetJobWrapper)
    : BaseV1Controller
{
    [HttpPost("get-or-create-partner-clients-sheet")]
    public async Task<IActionResult> GetOrCreatePartnerClientsSheet(string userEmail, PartnersClientFilterDto filter)
    {
        GoogleSheetDto googleSheetInfo = mapper.Map<GoogleSheetDto>(filter);
        var spreadSheetId = await  googleSheetsProvider
            .GetOrCreateGoogleSheetAsync(userEmail, googleSheetInfo);
        if (spreadSheetId.Error)
        {
            return ResponseResult(spreadSheetId);
        }
        var partnerClients = billingServiceManager
            .GetPartnerClientsConsiderAdditionalMonths(filter)
            .Result
            .ChunkDataOfPagination;

        var partnerClientsSheetsModels = mapper.Map<List<PartnerClientSheetDto>>(partnerClients);

        await googleSheetsProvider
            .UpdateSpreadsheetAsync(partnerClientsSheetsModels, spreadSheetId.Result, PartnerClientGoogleSheetInfo.ColumnNames);

        return ResponseResult(spreadSheetId);
    }

    [HttpPost("run-update-partner-clients")]
    public void RunUpdatePartnerClients() =>  updatePartnerClientsGoogleSheetJobWrapper.Start();
    
}

