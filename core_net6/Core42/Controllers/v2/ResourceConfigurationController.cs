﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums.Security;
using Clouds42.Resources.Managers;
using Core42.Attributes;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing resource configurations
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class ResourceConfigurationController(IResourcesConfigurationManager resourcesConfigurationManager)
        : BaseController
    {
        /// <summary>
        /// Get expiration date of the service for the account
        /// </summary>
        /// <param name="accountId">Account identfier</param>
        /// <param name="service">Service name</param>
        /// <returns></returns>
        [HttpGet]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Esdl)]
        public async Task<IActionResult> GetResourceConfigurationExpireDate(Guid accountId, string service)
        {
            var resConf = await resourcesConfigurationManager.GetServiceExpireDate(accountId, service);

            var result = CreateResult(resConf, (r) => new DateTimeDto { Date = resConf.Result }, HttpContext);

            return result;
        }
    }
}
