﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer;
using Clouds42.DataContracts.BaseModel;
using Clouds42.CloudServices.CloudsTerminalServers.Managers;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing terminal servers
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class CloudTerminalServersController(CloudTerminalServerSessionsManager cloudTerminalServerSessionsManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Find user RDP sessions by account identifier
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="accountUserId">Account user identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(TerminalServerUserRdpSessionsTableDto))]
        // ReSharper disable once InconsistentNaming
        public async Task<IActionResult> FindUserRdpSessionsByAccountID(Guid accountId, Guid? accountUserId = null)
        {
            return CreateResult(await cloudTerminalServerSessionsManager.FindUserRdpSessionsByAccountIdAsync(accountId, accountUserId),
                x => new TerminalServerUserRdpSessionsTableDto { TerminalServerUserRdpSessionsTable = { Rows = x } }, HttpContext
            );
        }

        /// <summary>
        /// Terminate user RDP sessions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public async Task<IActionResult> DropUserRdpSessions()
        {
            return CreateResultEmpty(await cloudTerminalServerSessionsManager.DropUserRdpSessionsAsync(), HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Teminate RDP session
        /// </summary>
        /// <param name="model">Session model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult CloseUserRdpSession([FromBody] CloudTerminalServerPostRequestCloseDto model)
        {
            return CreateResultEmpty(cloudTerminalServerSessionsManager.CloseUserRdpSession(model), HttpContext);
        }

        #endregion
    }
}
