﻿using System;
using Asp.Versioning;
using Clouds42.DataContracts.Account;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Accounts.Account.Managers;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with account manager
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountSaleManagerController(AccountSaleManager saleManager) : BaseController
    {
        /// <summary>
        /// Detach sales manager from account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult RemoveSaleManagerFromAccount(int accountNumber)
        {
            var result = saleManager.RemoveSaleManagerFromAccount(accountNumber);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Attach a sales manager to your account
        /// </summary>
        /// <param name="accountSaleManager">Attached sale manager model</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult AttachSaleManagerToAccount([FromBody] AccountSaleManagerDto accountSaleManager)
        {
            var result = saleManager.AttachSaleManagerToAccount(accountSaleManager);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Get sale manager info for account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountSaleManagerInfoDto))]
        public IActionResult GetAccountSaleManagerInfo(Guid accountId)
        {
            var result = saleManager.GetAccountSaleManagerInfo(accountId);
            return CreateResult(result, x => x, HttpContext);
        }
    }
}
