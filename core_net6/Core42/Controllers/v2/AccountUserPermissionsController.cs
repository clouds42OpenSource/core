﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Core42.Models.AccountUsers.Permissions;
using Clouds42.HandlerExeption.Contract;
using Core42.Helpers;
using System.Linq;
using System.Collections.Generic;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/{accountUserId}/[action]")]
    public class AccountUserPermissionsController(
        IAccountUserPermissionsProvider accountUserPermissionsProvider,
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException exceptionsHandler)
        : BaseController
    {
        readonly AccountUserPermissionsManager _manager = new(accessProvider,
            accountUserPermissionsProvider,
            dbLayer,
            exceptionsHandler);

        [HttpPost]
        public IActionResult GetPermissionsForContext(Guid accountUserId,
            [FromBody] GetContextPermissionsApiModel request)
        {
            var permissionsFilter = request.Permissions != null ?
                ParseValidObjectActions(request.Permissions) : null;

            var result = _manager.GetUserPermissionsForTargets(
                accountUserId,
                request.Context.AccountId,
                request.Context.AccountUserId,
                permissionsFilter);

            return CreateResult(result, permissions => permissions?.Select(p => p.ToString()).ToList(), HttpContext);
        }

        private IEnumerable<ObjectAction> ParseValidObjectActions(IEnumerable<string> source)
        {
            return source
                .Select(x =>
                {
                    var parsed = Enum.TryParse<ObjectAction>(x, out var value);
                    return new { parsed, value };
                })
                .Where(x => x.parsed)
                .Select(x => x.value);
        }
    }
}
