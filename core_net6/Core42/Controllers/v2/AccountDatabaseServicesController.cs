﻿using System;
using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.AccountDatabase.ServiceExtensions.Managers;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}")]
    public class AccountDatabaseServicesController(
        IManageServiceExtensionDatabaseManager manageServiceExtensionDatabaseManager)
        : BaseController
    {
        /// <summary>
        /// Install service in the database
        /// </summary>
        /// <param name="args">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("account-database-services")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Promo)]
        public IActionResult InstallService([FromBody] InstallServiceExtensionToDatabaseDto args)
        {
            var result = manageServiceExtensionDatabaseManager.InstallServiceToDatabase(args);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Uninstall service in the database
        /// </summary>
        /// <param name="serviceId">Service identifier</param>
        /// <param name="accountDatabaseId">Database identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("account-database-services")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [ExternalService(ExternalServiceTypeEnum.Promo)]
        public IActionResult UninstallService([FromQuery] Guid serviceId, [FromQuery] Guid accountDatabaseId)
        {
            var result = manageServiceExtensionDatabaseManager.DeleteServiceFromDatabase(serviceId, accountDatabaseId);
            return CreateResultEmpty(result, HttpContext);
        }
    }
}
