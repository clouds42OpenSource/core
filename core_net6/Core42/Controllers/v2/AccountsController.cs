﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using AutoMapper;
using Clouds42.Accounts.Account.Managers;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;
using Core42.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing cloud accounts
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountsController(
        IAccountsManager accountsManager,
        AccountDataManager accountDataManager,
        ICloud42ServiceHelper cloud42ServiceHelper,
        CreateAccountManager createAccountsManager)
        : BaseController
    {
        private readonly IMapper _mapper = Config.CreateMapper();
        private static readonly MapperConfiguration Config =
            new(cfg => cfg.CreateMap<CloudServicesSegment, CloudServicesSegmentDto>());

        #region GETs

        /// <summary>
        /// Get account data by ID
        /// </summary>
        /// <param name="accountID">Account identifier</param>
        /// <returns>Account data</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountPropertiesDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public async Task<IActionResult> GetProperties(Guid accountID)
        {
            var properties = await accountsManager.GetPropertiesInfoAsync(accountID);
            var result = CreateResult(properties, (account) => new AccountPropertiesDto
            {
                AccountCaption = account.AccountCaption,
                ReferralAccountID = account.ReferralAccountId?.ToString(),
                RegistrationDate = account.RegistrationDate?.ToString("O") ?? string.Empty,
                IndexNumber = account.IndexNumber.ToString(CultureInfo.InvariantCulture),
                Removed = account.Removed.HasValue && account.Removed.Value,
                ManagerId = account.AccountAdminId ?? Guid.Empty,
                Description = account.Description,
                Currency = account.AccountCurrency,
                INN = account.Inn ?? "",
                LocaleId = account.LocaleId
            }, HttpContext);

            return result;
        }

        /// <summary>
        /// Get account caption by account ID
        /// </summary>
        /// <param name="accountID">Account identifier</param>
        /// <returns>Account caption</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountCaptionDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl)]
        public async Task<IActionResult> GetAccountCaption(Guid accountID)
        {
            var result = await accountsManager.GetPropertiesInfoAsync(accountID);
            return CreateResult(result, x => new AccountCaptionDto { AccountCaption = x.AccountCaption }, HttpContext);
        }

        /// <summary>
        /// Get file storage ID by account ID
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns>Storage idenitfier</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CloudFileStorageServerIdDto))]
        public async Task<IActionResult> GetFileStorageServerID(Guid accountId)
        {
            var result = await accountsManager.GetFileStorageServerId(accountId);
            return CreateResult(result, x => new CloudFileStorageServerIdDto { StorageServerId = x }, HttpContext);
        }

        /// <summary>
        /// Find account information records
        /// </summary>
        /// <param name="searchString">Search string</param>
        /// <param name="maxRecordsCount">Max records count</param>
        /// <returns>Account information records</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountInfoTableDto))]
        public async Task<IActionResult> Find(string searchString, int maxRecordsCount = 10)
        {
            var result = await accountsManager.Find(searchString, maxRecordsCount);
            return CreateResult(result, x => new AccountInfoTableDto
            {
                AccountSearchResultTable =
                {
                    Rows = x.Select(ac => new AccountInfoTableRowDto
                    {
                        Id = ac.Id,
                        AccountCaption = ac.AccountCaption,
                        IndexNumber = ac.IndexNumber,
                        Description = ac.Description,
                        DiscSize = cloud42ServiceHelper.GetAvailableSpaceOnDisk(ac.Id)
                    }).ToList()
                }
            }, HttpContext);
        }

        /// <summary>
        /// Get account ID by account number
        /// </summary>
        /// <param name="indexNumber">Account number</param>
        /// <returns>Account identifier</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountIDDto))]
        public IActionResult GetAccountIdByIndexNumber(int indexNumber)
        {
            var result = accountsManager.GetAccountIdByIndexNumber(indexNumber);
            return CreateResult(result, x => new AccountIDDto(x), HttpContext);
        }
        /// <summary>
        /// Get account segment
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns>Account segment</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CloudServicesSegmentDto))]
        public IActionResult GetSegment(Guid accountId)
        {
            var result = accountDataManager.GetAccountSegment(accountId);
            return CreateResult(result, x => _mapper.Map<CloudServicesSegmentDto>(x), HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Find accounts by criteria
        /// </summary>
        /// <param name="criteriaRequest">Find criteria</param>
        /// <returns>Accounts identifiers</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountIdListDto))]
        public IActionResult GetAccountIdsByCriteria([FromBody] AccountFilteredCriteriaRequestDto criteriaRequest)
        {
            var result = accountsManager.GetAccountIdByCriteria(criteriaRequest);
            return CreateResult(result, x => new AccountIdListDto(new AccountIdListItemsDto(x)), HttpContext);
        }

        /// <summary>Создать новую учетную запись</summary>
        /// <param name="accountInfo">Информация о новом аккаунте</param>
        /// <returns>Сессию для доступа по АПИ.</returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountAuthDto))]
        public async Task<IActionResult> Add([FromBody] AccountRegistrationModelDto accountInfo)
        {
            Logger.Info("Регистрация");
            var result = await createAccountsManager.AddAccountAndCreateSessionAsync(accountInfo);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Register a new account without activating services and create an auth session
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api_v{version:apiVersion}/[controller]/registration/no-services")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountAuthDto))]
        public async Task<IActionResult> RegisterAccountWithoutServices([FromBody] AccountRegistrationModelDto dto)
        {
            return CreateResult(await createAccountsManager.AddAccountAndSessionWithoutServices(dto), x=> x, HttpContext);
        }

        #endregion


    }
}
