﻿using System;
using System.Diagnostics;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.MyDisk;
using Clouds42.CloudServices;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Managing cloud services
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class CloudServicesController(ICloudServiceManager cloudServiceManager) : BaseController
    {
        /// <summary>
        /// ValidateBinding token
        /// </summary>
        ///<param name="serviceToken">Service token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceTokenValidityDto))]
        public IActionResult CheckServiceTokenValidity([FromQuery] Guid serviceToken)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var result = CreateResult(cloudServiceManager.CheckServiceTokenValidity(serviceToken, out var headString),
                x => new ServiceTokenValidityDto(x), HttpContext);

            timer.Stop();
            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, headString);

            return result;
        }

        /// <summary>
        /// Get service name by token
        /// </summary>
        /// <param name="token">Service token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceNameDto))]
        public IActionResult GetServiceByToken(Guid token)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var service = cloudServiceManager.GetServiceByToken(token, out var headString);
            var result = CreateResult(service, x => new ServiceNameDto(x), HttpContext);

            timer.Stop();
            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, headString);

            return result;
        }

        /// <summary>
        /// Get information about "MyDisk" service for account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MyDiskInfoModelDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult GetMyDiscByAccount(Guid accountId)
        {
            var methodStartTime = (long)(DateTime.UtcNow - new DateTime(1, 1, 1, 0, 0, 0)).TotalMilliseconds;
            var timer = new Stopwatch();
            timer.Start();

            var myDisk = cloudServiceManager.GetMyDiscByAccount(accountId);

            var result = CreateResult(myDisk, x => new MyDiskInfoModelDto
            {
                AvailableSize = x.Data.AvailableSize,
                ClientFilesSize = x.Data.ClientFilesSize,
                FreeSizeOnDisk = x.Data.FreeSizeOnDisk,  
                ServerDataBaseSize = x.Data.ServerDataBaseSize,
                ClinetFilesCalculateDate = x.Data.ClinetFilesCalculateDate,
                PayedSize = x.Data.PayedSize,
                FileDataBaseSize = x.Data.FileDataBaseSize,
                UsedSizeOnDisk = x.Data.UsedSizeOnDisk
            }, HttpContext);

            timer.Stop();
            TimingsHelper.AddTimingsToResponse(HttpContext, methodStartTime, timer.ElapsedMilliseconds, myDisk.Result.Head);

            return result;
        }
    }
}
