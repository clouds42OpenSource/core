﻿using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using Asp.Versioning;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Accounts.CreateAccount.Managers;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}")]
    public class AccountServiceOptionsController(
        ReactivateServiceManager reactivateServiceManager,
        BillingServiceManager billingServiceManager)
        : BaseController
    {
        #region GETs



        #endregion

        #region POSTs

        /// <summary>
        /// Enable auto-subscription of selected options in billing
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="serviceId">Service identifier</param>
        /// <returns>Результат выполнения</returns>
        [HttpPost]
        [Route("accounts/{accountId}/sevices/{serviceId}/account-service-options/enable-auto-subscribe")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Promo)]
        public IActionResult AccountServiceOptionsSubscribe([FromRoute] Guid accountId, [FromRoute] Guid serviceId)
        {
            var result = billingServiceManager.EnableAutoSubscription(accountId, serviceId);
            return CreateResultEmpty(result, HttpContext);
        }

        #endregion

        #region PUTs



        #endregion

        #region DELETEs

        /// <summary>
        /// Delete all service settings from billing
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="serviceId">Service identifier</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("accounts/{accountId}/sevices/{serviceId}/account-service-options")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult DeleteAllAccountServiceOptions([FromRoute] Guid accountId, [FromRoute] Guid serviceId)
        {
            var result = reactivateServiceManager.DisableAllServiceOptionsFor(accountId, serviceId);
            return CreateResultEmpty(result, HttpContext);
        }

        #endregion
    }
}
