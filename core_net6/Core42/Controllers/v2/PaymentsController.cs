﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Core42.Helpers;
using Core42.Models.Payments;
using Clouds42.Billing.Billing.Managers;
using Clouds42.Billing.BillingOperations.Managers;
using Clouds42.Billing.Contracts.Payment;
using Clouds42.Billing.Contracts.Payment.Interfaces;
using Clouds42.Billing.Payment.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Billing.Contracts.Billing.Models.PaymentSystem;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.Domain.Enums;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Working with payments
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]")]
    public class PaymentsController(
        CreatePaymentManager createPaymentManager,
        BillingDataManager billingDataManger,
        PaymentSystemDataManager paymentSystemDataManager,
        IPayWithSavedPaymentMethodManager payWithSavedPaymentMethodManager,
        PaymentsManager paymentsManager,
        RunTaskForYookassaAggregatorManager runTaskForYookassaAggregatorManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get payments
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="query">Parameterts model</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(SelectDataResultCommonDto<BillingOperationItemDto>))]
        public IActionResult Get([FromQuery] Guid accountId, [FromQuery] SelectDataCommonDto<PaymentsFilterDto> query = null)
        {
            query ??= new SelectDataCommonDto<PaymentsFilterDto>();
            query.PageNumber = query?.PageNumber is null or <= 0 ? 1 : query.PageNumber;
            var data = billingDataManger.GetPaginatedTransactionsForAccount(accountId, query);
            return CreateResult(data, (x) => x, HttpContext);
        }

        /// <summary>
        /// Выполнить оплату
        /// </summary>
        /// <param name="accountId">идентификатор аккаунта</param>
        /// <param name="totalSum">Сумма к оплате</param>
        [HttpGet]
        [Route("{accountId:guid}/pay")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentSystemDataModel))]
        public IActionResult Pay([FromRoute] Guid accountId, [FromQuery] decimal totalSum)
        {
            var model = paymentsManager.GetPaymentSystem(accountId, totalSum);
            return ResponseResultDto(model);
        }
        #endregion

        #region POSTs

        /// <summary>
        /// Create new payment
        /// </summary>
        /// <param name="payment">Model of creating payment</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromBody] CreatePaymentRequest payment)
        {
            if (payment.Total <= 0)
                ModelState.AddModelError("Total", "Сумма должна быть больше 0");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var newPayment = new PaymentDefinitionDto
            {
                Account = payment.AccountId,
                Date = payment.Date,
                OriginDetails = User?.Claims?.FirstOrDefault(x => x.Type == "lin")?.Value ?? "core-cp",
                Id = Guid.NewGuid(),
                OperationType = payment.Type,
                System = PaymentSystem.Admin,
                Status = PaymentStatus.Done,
                Description = string.IsNullOrEmpty(payment.Description) ? "" : payment.Description,
                Total = payment.Total,
                TransactionType = payment.TransactionType
            };

            var result = createPaymentManager.AddPayment(newPayment);
            return CreateResultEmpty(result, HttpContext);
        }

        /// <summary>
        /// Get payment details
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("BeginOnlinePayment")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentServiceAggreagtorDataResult))]
        public async Task<IActionResult> BeginOnlinePaymentAsync([FromBody] PreparePaymentSystemDto model)
        {
            var managerResult = await paymentSystemDataManager.GetPaymentSystemAggregatorDataAsync(model);
            if (!managerResult.Error && managerResult.Result.PaymentSystem == PaymentSystem.Yookassa.ToString() && managerResult.Result.PaymentId.HasValue)
                runTaskForYookassaAggregatorManager.RunTaskToCheckYookassaPaymentStatus(managerResult.Result.PaymentId.Value);
            return CreateResult(managerResult, x => x);
        }

        /// <summary>
        /// Payment without entering data with a saved payment method
        /// </summary>
        /// <param name="model">Parameters model</param>
        /// <returns>Paid invoice model</returns>
        [HttpPost]
        [Route("QuickPay")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PaymentResultDto))]
        public async Task<IActionResult> PayWithSavedPaymentMethodAsync([FromBody] PayWithSavedPaymentMethodDto model)
        {
            var managerResult = await payWithSavedPaymentMethodManager.PayWithSavedPaymentMethodAsync(model);
            if (!managerResult.Error)
                runTaskForYookassaAggregatorManager.RunTaskToCheckYookassaPaymentStatus(managerResult.Result.PaymentId);

            return CreateResult(managerResult, x => x);
        }

        #endregion
    }
}
