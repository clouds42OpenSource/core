﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Account.AccountCSResourceValues;
using Clouds42.CloudServices.AccountCsResourceValues;
using Clouds42.Domain.Enums.Security;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountCSResourceValuesController(
        IAccountCsResourceValuesDataManager accountCsResourceValuesDataManager,
        IAccountCsResourceValuesManager accountCsResourceValuesAccountCsResourceValuesManager)
        : BaseController
    {
        #region GETs

        /// <summary>
        /// Get resource value for cloud service
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="csResourceId">Resource identifier</param>
        /// <param name="accountUserId">Resource user identifier</param>
        /// <param name="dateTime">Period for getting resource value</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceResourceValueDto))]
        public async Task<IActionResult> GetValue([FromQuery] Guid accountId, [FromQuery] Guid csResourceId, [FromQuery] Guid? accountUserId = null, [FromQuery] DateTime? dateTime = null)
        {
            var managerResult = await accountCsResourceValuesAccountCsResourceValuesManager.GetValueAsync(accountId, csResourceId, accountUserId, dateTime);

            return CreateResult(managerResult, x => new ServiceResourceValueDto(x), HttpContext);
        }

        /// <summary>
        /// Get resource value for cloud service
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="csResourceId">Resource identifier</param>
        /// <param name="dateTime">Period for getting resource value</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceResourceValueDto))]
        [ExternalService(ExternalServiceTypeEnum.Adl, ExternalServiceTypeEnum.Esdl)]
        public async Task<IActionResult> GetResourcesValue([FromQuery] Guid accountId, [FromQuery] Guid csResourceId, [FromQuery] DateTime? dateTime = null)
        {
            var managerResult = await accountCsResourceValuesDataManager.GetValueAsync(accountId, csResourceId, dateTime);
            return CreateResult(managerResult, x => new ServiceResourceValueDto(x), HttpContext);
        }

        /// <summary>
        /// Check the expiration date of the demo
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="accountUserId">Account user identifier</param>
        /// <param name="resourcesType">Resource type</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42)]
        public IActionResult IsAssignedResource([FromQuery] Guid accountId, [FromQuery] Guid accountUserId, [FromQuery] string resourcesType)
        {
            var managerResult = accountCsResourceValuesAccountCsResourceValuesManager.IsAssignedResource(accountId, accountUserId, resourcesType);
            return CreateResult(managerResult, x => new BoolDto(x), HttpContext);
        }

        /// <summary>
        /// Check service activity
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="subject">User identifier</param>
        /// <param name="serviceName">Service name</param>
        /// <param name="type">Service type</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BoolDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Promo)]
        public IActionResult IsPaidService([FromQuery] Guid accountId, [FromQuery] Guid subject, [FromQuery] string serviceName, [FromQuery] string type = null)
        {
            var managerResult = accountCsResourceValuesAccountCsResourceValuesManager.IsActiveService(accountId, subject, serviceName, type);
            return CreateResult(managerResult, x => new BoolDto(x), HttpContext);
        }

        #endregion

        #region POSTs

        /// <summary>
        /// Increase resource
        /// </summary>
        /// <param name="model">Parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult IncreaseValue([FromBody] IncreaseDecreaseValueModelDto model)
        {
            var managerResult = accountCsResourceValuesDataManager.IncreaseResource(model);
            return CreateResultEmpty(managerResult, HttpContext);
        }

        /// <summary>
        /// Decrease resource
        /// </summary>
        /// <param name="model">Parameters</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EmptyResultDto))]
        public IActionResult DecreaseValue([FromBody] IncreaseDecreaseValueModelDto model)
        {

            var managerResult = accountCsResourceValuesDataManager.DecreaseResource(model);
            return CreateResultEmpty(managerResult, HttpContext);
        }

        #endregion
    }
}
