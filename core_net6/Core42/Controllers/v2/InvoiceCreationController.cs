﻿using System;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Core42.Models.Invoices;
using Core42.Models.Invoices.Calculator;
using Core42.Models.Invoices.Creation;
using Clouds42.Billing.Contracts.Invoice.Interfaces.Managers;
using Clouds42.Domain.Enums.Billing;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Creating invoices
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/accounts/{accountId}/invoice-creation")]
    public class InvoiceCreationController(
        IInvoiceCreationManager invoiceCreationManager,
        IInvoiceCalculatorManager invoiceCalculatorManager)
        : BaseController
    {
        readonly IInvoiceCreationManager _invoiceCreationManager = invoiceCreationManager ??
                                                                   throw new ArgumentNullException(nameof(invoiceCreationManager));
        readonly IInvoiceCalculatorManager _invoiceCalculatorManager = invoiceCalculatorManager ??
                                                                       throw new ArgumentNullException(nameof(invoiceCalculatorManager));

        #region /subscription-services

        /// <summary>
        /// Get subscription services calculator
        /// </summary>        
        /// <param name="accountId">Account identifier</param>        
        /// <returns></returns>
        [HttpGet]
        [Route("subscription-services/calculator")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceCalculatorApiModel))]
        public IActionResult GetSubscriptionServicesCalculator([FromRoute] Guid accountId)
            => GetCalculator(InvoicePurposeType.BaseBillingServices, accountId);


        /// <summary>
        /// Get subscription services calculation
        /// </summary>        
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns></returns>
        [HttpPost]
        [Route("subscription-services/calculator")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceCalculationApiModel))]
        public IActionResult SubscriptionServicesRecalculate(
            [FromRoute] Guid accountId, [FromBody] RecalculateInvoiceRequestApiModel request) =>
                Recalculate(InvoicePurposeType.BaseBillingServices, accountId, request);

        /// <summary>        
        /// Create invoice by subscription services calculation
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns>Created invoce identifier</returns>
        [HttpPost]
        [Route("subscription-services")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateInvoiceResultApiModel))]
        public IActionResult SubscriptionServicesCreateInvoice([FromRoute] Guid accountId, [FromBody] CreateInvoiceFromCalculationApiModel request) =>
            CreateInvoiceFromCalculation(InvoicePurposeType.BaseBillingServices, accountId, request);

        #endregion

        #region /fasta

        /// <summary>
        /// Get services calculator for "Fasta" service
        /// </summary>        
        /// <param name="accountId">Account identifier</param>        
        /// <returns></returns>
        [HttpGet]
        [Route("fasta/calculator")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceCalculatorApiModel))]
        public IActionResult GetFastaServicesCalculator([FromRoute] Guid accountId)
            => GetCalculator(InvoicePurposeType.AdditionalBillingServices, accountId);


        /// <summary>
        /// Build invoice calculation
        /// </summary>        
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters moel</param>
        /// <returns></returns>
        [HttpPost]
        [Route("fasta/calculator")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceCalculationApiModel))]
        public IActionResult FastaServicesRecalculate(
            [FromRoute] Guid accountId, [FromBody] RecalculateInvoiceRequestApiModel request) =>
                Recalculate(InvoicePurposeType.AdditionalBillingServices, accountId, request);

        /// <summary>        
        /// Create invoice by subscription services calculation
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns>Created invoce identifier</returns>
        [HttpPost]
        [Route("fasta")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateInvoiceResultApiModel))]
        public IActionResult FastaServicesCreateInvoice([FromRoute] Guid accountId, [FromBody] CreateInvoiceFromCalculationApiModel request) =>
            CreateInvoiceFromCalculation(InvoicePurposeType.AdditionalBillingServices, accountId, request);

        #endregion

        #region /custom

        /// <summary>        
        /// Create invoice with arbitrary amount
        /// </summary>        
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns>Created invoice identifier</returns>        
        [HttpPost]
        [Route("custom")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateInvoiceResultApiModel))]
        public IActionResult CustomCreateInvoice([FromRoute] Guid accountId, [FromBody] CreateInvoiceFromArbitraryAmountApiModel request)
        {
            var result = _invoiceCreationManager.CreateArbitraryAmountInvoice(request.ToCreateArbitraryAmountInvoiceDc(accountId), request.NotifyAccountUserId);
            return CreateResult(result, invoiceId => new CreateInvoiceResultApiModel { Id = invoiceId }, HttpContext);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Get invoices calculator
        /// </summary>        
        /// <param name="type">Calculator type</param>
        /// <param name="accountId">Account identifier</param>        
        /// <returns></returns>        
        private IActionResult GetCalculator(InvoicePurposeType type, Guid accountId)
        {
            var calculatorResult = _invoiceCalculatorManager.GetInvoiceCalculatorForAccount(accountId, type);
            return CreateResult(calculatorResult, InvoiceCalculatorApiModel.CreateFrom, HttpContext);
        }


        /// <summary>
        /// Build invoice calculator
        /// </summary>        
        /// <param name="type">Calculator type</param>
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns></returns>        
        private IActionResult Recalculate(InvoicePurposeType type, Guid accountId,
            RecalculateInvoiceRequestApiModel request)
        {
            var result = _invoiceCalculatorManager.RecalculateInvoiceForAccount(
                request.ToRecalculateInvoiceDc(accountId, type));
            return CreateResult(result, InvoiceCalculationApiModel.CreateFrom, HttpContext);
        }

        /// <summary>        
        /// Create invoice by calculation
        /// </summary>
        /// <param name="type">Calculator type</param>
        /// <param name="accountId">Account identifier</param>
        /// <param name="request">Parameters model</param>
        /// <returns></returns>        
        private IActionResult CreateInvoiceFromCalculation(InvoicePurposeType type, Guid accountId, CreateInvoiceFromCalculationApiModel request)
        {
            var result = _invoiceCreationManager
                .CreateInvoiceFromCalculation(request.ToInvoiceCalculationDc(accountId, type), request.NotifyAccountUserId);
            return CreateResult(result, invoiceId => new CreateInvoiceResultApiModel { Id = invoiceId }, HttpContext);
        }

        #endregion Private methods
    }
}
