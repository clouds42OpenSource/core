﻿using System;
using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Core42.Helpers;
using Core42.Models.Invoices;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;

namespace Core42.Controllers.v2
{

    /// <summary>
    /// Working with invoices
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]")]
    public class InvoicesController(BillingAccountManager billingAccountManager) : BaseController
    {
        /// <summary>
        /// Get invoices to pay
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <param name="query">Parameters model</param>
        /// <returns></returns>        
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SelectDataResultCommonDto<InvoiceInfoApiModel>))]
        [Route("")]
        public IActionResult GetPaginated([FromQuery] Guid accountId,
            [FromQuery] SelectDataCommonDto<InvoicesFilterDto> query = null)
        {
            var data = billingAccountManager.GetPaginatedInvoicesForAccount(accountId, query);
            return CreatePaginationResult(data, invoice => new InvoiceInfoApiModel
            {
                Id = invoice.Id,
                ActDescription = invoice.ActDescription,
                ActId = invoice.ActId,
                Description = invoice.Description,
                InvoiceDate = invoice.InvoiceDate,
                InvoiceSum = invoice.InvoiceSum,
                IsNewInvoice = invoice.IsNewInvoice,
                ReceiptFiscalNumber = invoice.ReceiptFiscalNumber,
                State = invoice.State,
                Uniq = invoice.Uniq,
                Status = invoice.Status,
                RequiredSignature = invoice.RequiredSignature,
            });
        }


        /// <summary>
        /// Get invoice
        /// </summary>
        /// <param name="id">Invoice identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(InvoiceInfoApiModel))]
        [Route("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var result = billingAccountManager.GetInvoiceDc(id);
            return CreateResult(result, InvoiceInfoApiModel.CreateFrom, HttpContext);
        }
    }
}
