﻿using System;
using System.Collections.Generic;
using Asp.Versioning;
using Clouds42.Billing.BillingServices.Managers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Controller for work
    /// with a request to change the service for moderation
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/ServiceRequest")]
    public class ChangeServiceRequestController(
        ChangeServiceRequestManager changeServiceRequestManager,
        ModerationResultManager moderationResultManager)
        : BaseController
    {
        /// <summary>
        /// Get a service change request for moderation
        /// </summary>
        /// <param name="ChangeServiceRequestId">Request Id</param>
        /// <returns>Service change request for moderation</returns>
        [HttpGet]
        [Route("{ChangeServiceRequestId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ChangeServiceRequestDto))]
        public IActionResult ChangeServiceRequest(Guid ChangeServiceRequestId)
        {
            var managerResult = changeServiceRequestManager.Get(ChangeServiceRequestId);
            return CreateResult(managerResult, value => value, HttpContext);
        }

        /// <summary>
        /// Process the request moderation result
        /// for service changes
        /// </summary>
        /// <param name="changeServiceRequestModerationResult">Application moderation result</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ModerationResult")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<EmptyResultDto>))]
        public IActionResult ProcessModerationResult([FromBody] ChangeServiceRequestModerationResultDto changeServiceRequestModerationResult)
        {
            var result = moderationResultManager.ProcessModerationResult(changeServiceRequestModerationResult);
            return CreateResultEmpty(result, HttpContext);
        }
    }
}
