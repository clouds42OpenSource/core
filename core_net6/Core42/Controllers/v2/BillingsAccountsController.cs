﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Clouds42.Billing.Billing.Managers;
using Clouds42.DataContracts.Billing;
using Core42.Application.Features.BillingAccountContext.Dtos;
using Core42.Application.Features.BillingAccountContext.Queries;
using Core42.Helpers;
using Core42.Models;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/billingAccounts")]
    [ApiController]
    public class BillingsAccountsController(BillingAccountManager billingAccountManager) : BaseController
    {
        /// <summary>
        /// Get transaction list for account
        /// </summary>
        /// <param name="accountId">Account identifier</param>
        /// <returns>Transaction list</returns>
        [HttpGet]
        [Route("{accountId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BillingDataDto))]
        public IActionResult Get(Guid accountId)
        {
            var data = billingAccountManager.GetBillingDataV2(accountId);
            return CreateResult(data, (x) => x, HttpContext);
        }

        /// <summary>
        /// Get account balance by account identifier
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountId:guid}/balance")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseResultDto<BillingAccountBalanceDto>))]
        public async Task<IActionResult> GetAccountBalance([FromRoute] GetBillingAccountBalanceQuery query)
        {
            return CreateResult(await Mediator.Send(query), (x) => x, HttpContext);
        }
    }
}
