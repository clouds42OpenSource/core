﻿using System;
using System.Globalization;
using Asp.Versioning;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    /// <summary>
    /// Testing service
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class TestController(
        ILogger42 logger,
        IHandlerException handlerException,
        IAutoUpdateAccountDatabasesProvider autoUpdateAccountDatabasesProvider)
        : BaseController
    {
        /// <summary>
        /// Ping service
        /// </summary>
        /// <returns>Current date and time to check the service response</returns>
        [HttpGet]
        public string Ping()
            => DateTime.Now.ToString(CultureInfo.InvariantCulture);
        

        [HttpGet]
        public void TestPlanSupportJob(Guid AccountDatabaseId)
        {
            autoUpdateAccountDatabasesProvider.UpdateDatabase(AccountDatabaseId);
        }


        [HttpGet]
        public void testLogAndIncident()
        {
            logger.Info("SomeTestLog");
            logger.Error("SomeTestError");
            handlerException.Handle(new InvalidOperationException("TestException"), "[Test Incident]");
        }
    }
}
