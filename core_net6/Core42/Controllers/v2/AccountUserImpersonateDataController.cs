﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Clouds42.DataContracts.Service.Link;
using Clouds42.Domain.Enums.Security;
using Clouds42.Link42.Managers;
using Core42.Attributes;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/[action]")]
    public class AccountUserImpersonateDataController(
        IAccountUserImpersonateDataManager accountUserImpersonateDataManager)
        : BaseController
    {
        /// <summary>
        /// Get user impersonation data
        /// </summary>
        /// <param name="login">Cloud user login</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserImpersonateDataDto))]
        public async Task<IActionResult> GetByAccountUserLogin(string login)
        {
            var result = await accountUserImpersonateDataManager.GetByAccountUserLoginAsync(login).ConfigureAwait(false);
            return CreateResult(result, x => x, HttpContext);
        }

        /// <summary>
        /// Get user impersonation data
        /// </summary>
        /// <param name="token">Cloud user session token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AccountUserImpersonateDataDto))]
        [ExternalService(ExternalServiceTypeEnum.Link42, ExternalServiceTypeEnum.Promo)]
        public async Task<IActionResult> GetByToken(Guid token)
        {
            var result = await accountUserImpersonateDataManager.GetByTokenAsync(token).ConfigureAwait(false);
            return CreateResult(result, x => x, HttpContext);
        }
    }
}
