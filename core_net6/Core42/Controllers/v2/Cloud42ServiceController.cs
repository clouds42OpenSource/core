﻿using System;
using System.Threading.Tasks;
using Asp.Versioning;
using Core42.Application.Features.BillingServiceContext.Dtos;
using Core42.Application.Features.BillingServiceContext.Queries;
using Core42.Application.Features.BillingServiceContext.Queries.Filters;
using Core42.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core42.Controllers.v2
{
    [ApiVersion("2.0")]
    [Route("api_v{version:apiVersion}/[controller]/{accountId}/[action]")]
    public class Cloud42ServiceController : BaseController
    {
        /// <summary>
        /// Get a list of services
        /// </summary>
        /// <returns>List of services</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Market42ServiceResultDto))]
        public async Task<IActionResult> ServicesList([FromRoute] Guid accountId, [FromQuery] GetMarker42BillingServicesQuery query)
        {
            query.Filter ??= new Market42ServiceFilter();
            query.Filter.AccountId = accountId;

            return CreateResult(await Mediator.Send(query), x => x, HttpContext);
        }
    }
}
