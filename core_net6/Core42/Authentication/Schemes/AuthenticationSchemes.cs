﻿using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Core42.Authentication.Schemes
{
    public static class AuthenticationSchemes
    {
        public const string JwtBearer = JwtBearerDefaults.AuthenticationScheme;
        public const string Basic = "Basic";
        public const string Multiple = "Multiple";
        public const string Token = "Token";
    }
}
