﻿using System;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using Core42.Authentication.Schemes;
using Microsoft.AspNetCore.Authorization;

namespace Core42.Authentication.Filters
{
    internal class SecurityRequirementsOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (context == null || operation == null)
            {
                return;
            }

            var requiredScopes = context.MethodInfo.DeclaringType
                .GetCustomAttributes(true)
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.AuthenticationSchemes)
                .Distinct()
                .ToList();
            
            var requiredScopes2 = context.MethodInfo
                .GetCustomAttributes(true)
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.AuthenticationSchemes)
                .Distinct()
                .ToList();
            
            string id = AuthenticationSchemes.JwtBearer;
            
            if (requiredScopes.Contains(AuthenticationSchemes.Basic) || requiredScopes2.Contains(AuthenticationSchemes.Basic))
            {
                id = AuthenticationSchemes.Basic;
            }
            
            operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
            
            operation.Security = new List<OpenApiSecurityRequirement>
            {
                new()
                {
                    { 
                        new OpenApiSecurityScheme
                        {
                             Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = id }
                        },

                        Array.Empty<string>()
                    }
                }
            };
            
        }
    }
}
