﻿using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.Access;
using Microsoft.AspNetCore.Http;

namespace Core42.Authentication
{
    public class SwaggerAuthMiddleware(RequestDelegate next)
    {
        public async Task InvokeAsync(HttpContext context)
        {

            if (context.Request.Path.StartsWithSegments("/swagger"))
            {
                var roleClaim = context.User?.Claims?.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role");

                var roleValues = roleClaim?.Value?.Trim('[', ']').Split(',')
                    .Select(value => int.Parse(value.Trim()))
                    .ToArray();

                var roles = roleValues?
                    .Select(value => (AccountUserGroup)value)
                    .ToArray();

                if ((context?.User?.Identity?.IsAuthenticated ?? false) && (roles.Contains(AccountUserGroup.CloudAdmin) || roles.Contains(AccountUserGroup.ExternalService) || roles.Contains(AccountUserGroup.Hotline)))
                {
                    await next(context);
                }

                else
                {
                    context.Response.StatusCode = 401;
                }
              
            }
            else
            {
                await next(context);
            }
        }
    }
}
