﻿using Clouds42.Common.DataModels;

namespace Core42.Providers
{
    /// <summary>
    /// Провайдер настроек
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// Вернуть публичные URL адреса
        /// </summary>
        /// <returns>Публичные URL адреса</returns>
        PublicUrlsDto GetPublicUrls();
    }
}