﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Constants;
using Clouds42.Common.DataModels;
using Clouds42.Configurations.Configurations;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Core42.Providers
{
    /// <summary>
    /// Провайдер настроек
    /// </summary>
    public class SettingsProvider : ISettingsProvider
    {
        /// <summary>
        /// Провайдер доступа
        /// </summary>
        private readonly IAccessProvider _accessProvider;

        /// <summary>
        /// Контекст базы
        /// </summary>
        private readonly IUnitOfWork _dbLayer;

        /// <summary>
        /// промо сайт для RU локали
        /// </summary>
        private readonly Lazy<string> _ruPromoSiteUrl;

        /// <summary>
        /// промо сайт для UA локали
        /// </summary>
        private readonly Lazy<string> _uaPromoSiteUrl;

        /// <summary>
        /// Взаимосвязь между расширением домена и локалью
        /// </summary>
        private readonly Dictionary<string, string> _localeByHostExt = new()
        {
            {HostExtensionName.Promo.RuHostExt, LocaleConst.Russia},
            {HostExtensionName.Promo.UaHostExt, LocaleConst.Ukraine}
        };

        /// <summary>
        /// HTTP Контекст
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Конструктор на инициализацию <see cref="SettingsProvider"/>
        /// </summary>
        /// <param name="dbLayer">Контекст базы</param>
        /// <param name="configuration"></param>
        /// <param name="accessProvider">Провайдер доступа</param>
        /// <param name="httpContextAccessor"></param>
        public SettingsProvider(IUnitOfWork dbLayer,IConfiguration configuration, IAccessProvider accessProvider, IHttpContextAccessor httpContextAccessor)
        {
            _dbLayer = dbLayer;
            _ruPromoSiteUrl = new Lazy<string>(CloudConfigurationProvider.PromoSite.GetRuPromoSiteUrl);
            _uaPromoSiteUrl = new Lazy<string>(CloudConfigurationProvider.PromoSite.GetUaPromoSiteUrl);
            _accessProvider = accessProvider;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        /// <summary>
        /// Вернуть публичные URL адреса
        /// </summary>
        /// <returns>Публичные URL адреса</returns>
        public PublicUrlsDto GetPublicUrls()
        {
            return new PublicUrlsDto
            {
                PromoSiteUrl = GetPromoSiteUrl(GetRequestLocale())
            };
        }

        /// <summary>
        /// Получить название локали для текущего запроса
        /// </summary>
        /// <returns>Название локали</returns>
        private string GetRequestLocale()
        {
            var localeName = _accessProvider.ContextAccountId.Equals(Guid.Empty)
                ? GetLocaleNameFromHeader()
                : GetLocaleNameFromContextAccountId();

            return localeName;
        }

        /// <summary>
        /// Получить название локали из заголовка запроса "Origin" или "Referer", если не может найти, то используется по умолчанию
        /// </summary>
        /// <returns>Название локали</returns>
        private string GetLocaleNameFromHeader()
        {
            var firstHostExt = GetFirstOriginHostExtFromCurrentRequest();

            return _localeByHostExt.TryGetValue(firstHostExt, out var localeName) 
                ? localeName 
                : GetDefaultLocaleName();
        }

        /// <summary>
        /// Получить расширение для первого хоста из заголовка запроса с названием "Origin" или "Referer", если не смог найти, то возвращается <c>null</c>
        /// </summary>
        /// <returns>Расширение для первого хоста</returns>
        private string GetFirstOriginHostExtFromCurrentRequest()
        {
            var origin = _httpContextAccessor.HttpContext.Request.Headers[HttpRequestHeaderName.Origin].ToString();
            if (string.IsNullOrEmpty(origin))
                origin = _httpContextAccessor.HttpContext.Request.Headers[HttpRequestHeaderName.Referer].ToString();

            if (string.IsNullOrEmpty(origin))
                return null;

            var firstOrigin = origin.Split(',').First();

            var firstHostExt = new Uri(firstOrigin).Host.Split('.').Last().ToLower();

            return firstHostExt;
        }

        /// <summary>
        /// Получить название локали по умолчанию
        /// </summary>
        /// <returns>Название локали</returns>
        private string GetDefaultLocaleName()
        {
            var defaultLocale = _dbLayer.AccountsRepository.GetDefaultLocale(Guid.Parse(_configuration["DefaultLocale"]));
            return defaultLocale.Name;
        }

        /// <summary>
        /// Получить название локали из текущего контекста аккаунта, если не может найти, то используется по умолчанию
        /// </summary>
        /// <returns>Название локали</returns>
        private string GetLocaleNameFromContextAccountId()
        {
            var userLocale = _dbLayer.AccountsRepository.GetLocale(_accessProvider.ContextAccountId, Guid.Parse(_configuration["DefaultLocale"]));
            return userLocale.Name;
        }

        /// <summary>
        /// Получить адрес промо сайта
        /// </summary>
        /// <returns>Адрес промо сайта</returns>
        private string GetPromoSiteUrl(string locale)
            => locale == LocaleConst.Ukraine
                ? _uaPromoSiteUrl.Value
                : _ruPromoSiteUrl.Value;
    }
}
