﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Core42.Helpers
{
    public static class ActionHelper
    {
        public static UrlHelper HtmlActionLink(ActionContext context) => new(context);
    }
}
