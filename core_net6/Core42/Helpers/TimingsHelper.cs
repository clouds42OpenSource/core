﻿using Microsoft.AspNetCore.Http;

namespace Core42.Helpers
{
    /// <summary>
    /// Хэлпер для работы с таймингами
    /// </summary>
    public static class TimingsHelper
    {
        /// <summary>
        /// Добавить тайминги в ответ
        /// </summary>
        /// <param name="context">HTTP контекст</param>
        /// <param name="methodStartTime">Время начала выполнения метода</param>
        /// <param name="timingParams">Параметры таймингов</param>
        /// <param name="elapsedMilliseconds">Затраченное время в милисекундах</param>
        public static void AddTimingsToResponse(HttpContext context, long methodStartTime, long elapsedMilliseconds, string timingParams = null)
        {
            context.Response.Headers.Remove("Timings");
            context.Response.Headers.Add("Timings",
                $"Total: {elapsedMilliseconds}&MethodStart:{methodStartTime}{(string.IsNullOrEmpty(timingParams) ? "" : $"&{timingParams}")}");
            context.Response.Headers.Remove("IPAddress");
            context.Response.Headers.Add("IPAddress", System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString());
            context.Response.Headers.Remove("ID");
            context.Response.Headers.Add("ID", context.Request.Headers["ID"].ToString() ?? "");
        }
    }
}
