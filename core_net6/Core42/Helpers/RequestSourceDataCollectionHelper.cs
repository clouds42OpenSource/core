﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace Core42.Helpers
{
    /// <summary>
    /// Класс который занимается построением SourceDataCollection тела Http запроса
    /// Работает только с content-type: application/json и application/x-www-form-urlencoded
    /// </summary>
    public static class RequestSourceDataCollectionHelper
    {
        /// <summary>
        /// Создает коллекцию KeyValuePair с параметрами из json тела запроса
        /// </summary>
        /// <param name="body">Тело запроса(голое)</param>
        /// <returns>Коллекция с параметрами</returns>
        public static IEnumerable<KeyValuePair<string, string>> GetSourceDataCollectionFromJson(string body)
        {
            var obj = (JObject)JToken.Parse(body);

            return obj.GetSourceDataCollection();
        }


        /// <summary>
        /// Создает коллекцию KeyValuePair с параметрами из form-data тела запроса
        /// </summary>
        /// <param name="request">HttpRequest</param>
        /// <returns>Коллекция с параметрами</returns>
        public static IEnumerable<KeyValuePair<string, string>> GetSourceDataCollectionFromForm(this HttpRequest request)
        {
            return request.Form.Keys
                .Where(key => !string.IsNullOrWhiteSpace(key))
                .Select(key => new KeyValuePair<string, string>(key, request.Form[key]))
                .ToList();
        }
    }
}
