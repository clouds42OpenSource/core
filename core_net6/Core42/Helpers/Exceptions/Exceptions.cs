﻿using System;

namespace Core42.Helpers.Exceptions
{
    public class Core42Exception : Exception
    {
        public Core42Exception()
        {
        }

        public Core42Exception(string message)
            : base(message)
        {
        }

        public Core42Exception(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class CloudServiceException : Core42Exception
    {
        public CloudServiceException()
        {
        }

        public CloudServiceException(string message)
            : base(message)
        {
        }
        
        public CloudServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class CSResourcesException : Core42Exception
    {
        public CSResourcesException()
        {
        }

        public CSResourcesException(string message)
            : base(message)
        {
        }


        public CSResourcesException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class AccountCSResourceValueException : Core42Exception
    {
        public AccountCSResourceValueException()
        {
        }

        public AccountCSResourceValueException(string message)
            : base(message)
        {
        }


        public AccountCSResourceValueException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class NotFoundException : Core42Exception
    {
        public NotFoundException()
        {
        }

        public NotFoundException(string message)
            : base(message)
        {
        }


        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class ConflictException : Core42Exception
    {
        public ConflictException()
        {
        }

        public ConflictException(string message)
            : base(message)
        {
        }
        
        public ConflictException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class NotAuthorizedException : Core42Exception
    {
        public NotAuthorizedException()
        {
        }

        public NotAuthorizedException(string message)
            : base(message)
        {
        }

        public NotAuthorizedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
    public class PreconditionException : Core42Exception
    {
        public PreconditionException()
        {
        }

        public PreconditionException(string message)
            : base(message)
        {
        }

        public PreconditionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}