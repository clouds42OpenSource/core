﻿using Clouds42.DataContracts.BaseModel;
using System;
using System.Collections.Generic;
using System.Net;

namespace Core42.Helpers.Exceptions
{
    /// <summary>
    /// Билдер модели ошибки в ответе АПИ
    /// </summary>
    public class ErrorModelBuilder
    {
        private readonly Exception _ex;
        private ErrorStructuredDto _errorStructuredModel;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public ErrorModelBuilder(Exception ex)
        {
            _ex = ex;
        }

        /// <summary>
        /// Построить модель ошибки
        /// </summary>
        /// <param name="httpStatusCode">HTTP код ответа</param>
        /// <param name="errors"></param>
        /// <returns>Модель ошибки</returns>
        public ErrorStructuredDto BuildModel(HttpStatusCode httpStatusCode, List<KeyValuePair<string, List<string>>> errors)
        {
            _errorStructuredModel = new ErrorStructuredDto
            {
                Description = _ex.Message,
                Code = httpStatusCode.ToInt(),
                Errors = errors
            };

            return _errorStructuredModel;
        }

        /// <summary>
        /// Получить трассировку ошибки
        /// </summary>
        /// <returns>Трассировка ошибки</returns>
        public string GetTraceInfo()
            => $"{_errorStructuredModel.Description}, exception: {_ex.Message}";
    }
}
