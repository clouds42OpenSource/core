﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core42.Helpers.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Error")]
    [DataContract(Name = "Error")]
    public class ErrorStucturedDTO
    {
        private string _debugInfo = string.Empty;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Code")]
        [DataMember(Name = "Code")]
        public int Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Description")]
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "DebugInfo")]
        [DataMember(Name = "DebugInfo")]
        public string DebugInfo
        {
            get { return _debugInfo; }
            set { _debugInfo = value; }
        }

        public override string ToString()
        {
            return "\nCode: " + Code + ",\nDescription: " + Description + ",\nDebugInfo: " + DebugInfo;
        }
    }
}