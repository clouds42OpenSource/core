﻿using System;
using System.Linq;
using Core42.Constants;
using Microsoft.AspNetCore.Http;

namespace Core42.Helpers.Exceptions
{
    /// <summary>
    /// Расширение для определения IP-адреса
    /// </summary>
    public static class DetermineIpAddressExtentsion
    {
        /// <summary>
        /// Определить IP-адрес клиента из текущего запроса
        /// </summary>
        /// <param name="request">Текущий запрос</param>
        /// <returns>IP-адрес клиента</returns>
        public static string DetermineClientIpAddress(this HttpRequest request)
        {
            const string originalIpAddressHeaderName = HttpRequestMessagePropertiesConst.NginxOriginalIpAddressHeaderName;

            return request.Headers.Any(key =>
                string.Equals(key.ToString(), originalIpAddressHeaderName, StringComparison.InvariantCultureIgnoreCase))
                ? request.Headers[originalIpAddressHeaderName]
                : request.HttpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}