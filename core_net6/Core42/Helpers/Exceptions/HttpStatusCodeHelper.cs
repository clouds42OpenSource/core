﻿using System;
using System.Net;
using Clouds42.Common.Exceptions;
using FluentValidation;

namespace Core42.Helpers.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    public static class HttpStatusCodeHelper
    {
        /// <summary>
        /// Convert HttpStatusCode to int32 value (HttpStatusCode.OK => 200)
        /// </summary>
        /// <param name="httpStatusCode"></param>
        /// <returns></returns>
        public static int ToInt(this HttpStatusCode httpStatusCode)
        {
            return (int)httpStatusCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static HttpStatusCode GetHttpStatusCode(Exception exception)
        {
            return exception switch
            {
                NotImplementedException => HttpStatusCode.NotImplemented,
                ValidationException => HttpStatusCode.BadRequest,
                NotFoundException => HttpStatusCode.NotFound,
                UnauthorizedAccessException or AccessDeniedException => HttpStatusCode.Unauthorized,
                WebException => HttpStatusCode.Forbidden,
                PreconditionException => HttpStatusCode.PreconditionFailed,
                _ => HttpStatusCode.InternalServerError
            };
        }
    }
}
