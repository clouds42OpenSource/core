﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Core42.Models;
namespace Core42.Helpers.Exceptions
{
    /// <summary>
    /// Генератор текста анотаций
    /// </summary>
    public static class AnnotationTextGenerator
    {
        /// <summary>
        /// Генератор текста анотации
        /// </summary>
        private static readonly IDictionary<Type, Func<object, string>> AnotationTextGenerator = new Dictionary<Type, Func<object, string>>
        {
            { typeof(RequiredAttribute), a => "Обязательное поле." },
            { typeof(RangeAttribute), a =>
                {
                    var range = (RangeAttribute)a;
                    return $"Диапазон: между {range.Minimum} и {range.Minimum}.";
                }
            },
            { typeof(MaxLengthAttribute), a =>
                {
                    var maxLength = (MaxLengthAttribute)a;
                    return $"Максимальная длинна {maxLength.Length}.";
                }
            },
            { typeof(MinLengthAttribute), a =>
                {
                    var minLength = (MinLengthAttribute)a;
                    return $"Минимальная длинна {minLength.Length}.";
                }
            },
            { typeof(StringLengthAttribute), a =>
                {
                    var strLength = (StringLengthAttribute)a;
                    return $"Длинна строки между {strLength.MinimumLength} и {strLength.MaximumLength}.";
                }
            },
            { typeof(RegularExpressionAttribute), a =>
                {
                    RegularExpressionAttribute regularExpression = (RegularExpressionAttribute)a;
                    return $"Соответствие шаблону регулярного выражения {regularExpression.Pattern}.";
                }
            },
        };

        /// <summary>
        /// Сгенерировать анотации
        /// </summary>
        /// <param name="property">Информация о свойстве</param>
        /// <returns>Анотации</returns>
        public static List<ParameterAnnotation> GenerateAnnotations(this MemberInfo property)
        {
            var annotations = new List<ParameterAnnotation>();
            var attributes = property.GetCustomAttributes();

            foreach (var attribute in attributes)
            {
                if (AnotationTextGenerator.TryGetValue(attribute.GetType(), out Func<object, string> textGenerator))
                {
                    annotations.Add(
                        new ParameterAnnotation
                        {
                            AnnotationAttribute = attribute,
                            Documentation = textGenerator(attribute)
                        });
                }
            }

            annotations.Sort((x, y) =>
            {
                if (x.AnnotationAttribute is RequiredAttribute)
                    return -1;

                if (y.AnnotationAttribute is RequiredAttribute)
                    return 1;
                
                return string.Compare(x.Documentation, y.Documentation, StringComparison.OrdinalIgnoreCase);
            });

            return annotations;
        }

        /// <summary>
        /// Сконвертировать в строку
        /// </summary>
        /// <param name="parameterAnnotations">Анотации</param>
        /// <returns>Анотации строкой</returns>
        public static string ToStringAnnotations(this List<ParameterAnnotation> parameterAnnotations)
        {
            if (!parameterAnnotations.Any())
                return string.Empty;

            return string.Join(" ", parameterAnnotations.Select(pa => pa.Documentation));
        } 
    }
}