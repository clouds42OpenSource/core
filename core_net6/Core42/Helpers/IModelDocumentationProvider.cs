﻿using System;
using System.Reflection;

namespace Core42.Helpers
{
    /// <summary>
    /// Провайдер для работы с документацией модели
    /// </summary>
    public interface IModelDocumentationProvider
    {
        /// <summary>
        /// Получить документацию
        /// </summary>
        /// <param name="member">Информация о поле</param>
        /// <returns>Документация</returns>
        string GetDocumentation(MemberInfo member);

        /// <summary>
        /// Получить документацию
        /// </summary>
        /// <param name="type">Тип</param>
        /// <returns>Документация</returns>
        string GetDocumentation(Type type);
    }
}