﻿using Microsoft.AspNetCore.Http;

namespace Core42.Helpers
{
    /// <summary>
    /// Хэлпер для работы с контекстом Http
    /// </summary>
    public static class HttpContextHelper
    {
        /// <summary>
        /// Вытащить информацию трассировки из запроса
        /// </summary>
        /// <param name="request">Запрос</param>
        /// <returns>Информация трассировки из запроса</returns>
        public static string ExtractTraceInfo(HttpRequest request)
        {
            if (request == null)
                return string.Empty;

            var traceInfo = $"Request method: {request.Method}, \nuri: {request.Path}, \nheaders: {request.Headers}";

            return traceInfo;
        }
    }
}
