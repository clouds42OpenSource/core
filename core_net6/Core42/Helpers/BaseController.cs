﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Clouds42.Common.Converters;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Logger;
using Core42.Attributes;
using Core42.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Core42.Helpers
{
    /// <summary>
    /// Базовый контроллер АПИ
    /// </summary>
    [ServiceFilter(typeof(UnhandledExceptionFilterAttribute))]
    [ApiController]
    public class BaseController : Controller
    {
        protected ILogger42 Logger = Logger42.GetLogger();
        private ISender _mediator;
        protected ISender Mediator => _mediator ??= HttpContext.RequestServices.GetService<ISender>();

        #region Error handling


        private readonly Dictionary<ManagerResultState, HttpStatusCode>
            _mapErrorDictionary = new()
            {
                {ManagerResultState.NotFound, HttpStatusCode.NotFound},
                {ManagerResultState.PreconditionFailed, HttpStatusCode.PreconditionFailed},
                {ManagerResultState.BadRequest, HttpStatusCode.BadRequest},
                {ManagerResultState.Forbidden, HttpStatusCode.Forbidden},
                {ManagerResultState.Conflict, HttpStatusCode.Conflict},
                {ManagerResultState.Unauthorized, HttpStatusCode.Unauthorized},
            };

        /// <summary>
        /// Метод для создания результата на основании результата ManagerResult
        /// </summary>
        /// <param name="result"></param>
        /// <param name="func"></param>
        /// <param name="httpContext"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected IActionResult CreateResult<T>(ManagerResult<T> result, Func<T, object> func, HttpContext httpContext = null)
        {
           
            if (result.Error)
            {
                var statusCode = (_mapErrorDictionary?.ContainsKey(result.State) ?? false) ? _mapErrorDictionary[result.State] : HttpStatusCode.BadRequest;
                return CreateErrorResult(statusCode, CreateErrorStructuredDto(result));
            }

            var acceptXmlArray = new[] { "application/xml", "text/xml" };
            var acceptContainsOrEmpty = (!httpContext?.Request.Headers.Accept.Any() ?? false) ||
                                       (httpContext?.Request.Headers.Accept.Any(x => acceptXmlArray.Contains(x)) ??
                                        false);

            return acceptContainsOrEmpty ? Content(func(result.Result).SerializeToXml(), "application/xml", Encoding.UTF8) : Ok(func(result.Result));
        }

        private IActionResult CreateErrorResult(HttpStatusCode httpStatusCode, ErrorStructuredDto model)
        {
            model.Code = model.Code == 0 ? (int)httpStatusCode : model.Code;
            return httpStatusCode switch
            {
                HttpStatusCode.NotFound => NotFound(model),
                HttpStatusCode.Unauthorized => Unauthorized(model),
                HttpStatusCode.Conflict => Conflict(model),
                HttpStatusCode.BadRequest => BadRequest(model),
                _ => BadRequest(model)
            };
        }

        /// <summary>
        /// Метод для создания результата на основании результата ManagerResult пагинации
        /// </summary>
        /// <param name="result"></param>
        /// <param name="func">Функция маппинга исходного типа элемента списка в выходной типа элемента списка</param>
        /// <typeparam name="TSource">Исходный тип элемента пагинированного списка</typeparam>
        /// <typeparam name="TTarget">Выходной тип элемента пагинированного списка</typeparam>
        /// <returns></returns>
        protected IActionResult CreatePaginationResult<TSource, TTarget>(ManagerResult<SelectDataResultCommonDto<TSource>> result,
            Func<TSource, TTarget> func)
            where TSource : class, new()
            where TTarget : class, new()

        {
            Func<SelectDataResultCommonDto<TSource>, SelectDataResultCommonDto<TTarget>> innerFunc =
                paginationResult =>
                {
                    if (paginationResult == null)
                        return null;
                    var transformed = new SelectDataResultCommonDto<TTarget>
                    {
                        Pagination = paginationResult.Pagination
                    };
                    if (paginationResult.Records != null)
                        transformed.Records = paginationResult.Records.Select(func).ToArray();
                    return transformed;
                };

            return CreateResult(result, innerFunc, HttpContext);

        }

        protected IActionResult CreateResultNoCast<T>(ManagerResult<T> result, HttpContext httpContext = null, bool asXml = false)
        {
        
            if (result.Error)
            {
                var statusCode = _mapErrorDictionary.TryGetValue(result.State, out var value) ? value : HttpStatusCode.BadRequest;
                return CreateErrorResult(statusCode, CreateErrorStructuredDto(result));
            }

            if (httpContext == null)
            {
                return asXml
                    ? Content(result.Result.SerializeToXml(), "application/xml", Encoding.UTF8)
                    : Ok(result.Result);
            }

            var httpAccept = httpContext.Request.Headers.Accept;
            if (httpAccept.Contains("application/xml") || httpAccept.Contains("text/xml"))
                return Content(result.Result.SerializeToXml(), "application/xml", Encoding.UTF8);
            return asXml ? Content(result.Result.SerializeToXml(), "application/xml", Encoding.UTF8) : Ok(result.Result);
        }

        private ErrorStructuredDto CreateErrorStructuredDto<T>(ManagerResult<T> result)
        {
            if (result.ErrorObject is ErrorStructuredDto error)
            {
                return new ErrorStructuredDto
                {
                    Description = error.Description,
                    DebugInfo = error.DebugInfo,
                    Code = error.Code
                };
            }
            return new ErrorStructuredDto
            {
                Description = result.Message
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="httpContext"></param>
        /// <param name="asXml"></param>
        /// <returns></returns>
        protected IActionResult CreateResultEmpty(ManagerResult result, HttpContext httpContext = null, bool asXml = false)
        {
            if (result.Error)
            {
               
                return CreateErrorResult(_mapErrorDictionary[result.State], new ErrorStructuredDto
                {
                    Description = result.Message
                });
            }

            if (httpContext == null)
            {
                return asXml
                    ? Content(new EmptyResultDto().SerializeToXml(), "application/xml", Encoding.UTF8)
                    : Ok(new EmptyResultDto());
            }

            var httpAccept = httpContext.Request.Headers.Accept;
            if (httpAccept.Contains("application/xml") || httpAccept.Contains("text/xml"))
                return Content(new EmptyResultDto().SerializeToXml(), "application/xml", Encoding.UTF8);

            return asXml ? Content(new EmptyResultDto().SerializeToXml(), "application/xml", Encoding.UTF8) : Ok(new EmptyResultDto());
        }

        #endregion

        /// <summary>
        /// Результат выполнения запроса в Json
        /// </summary>
        /// <param name="requestStatus">Статус запроса</param>
        /// <param name="option">Доп. параметры</param>
        /// <returns>Результат выполнения запроса в Json</returns>
        protected JsonResult JsonDataStatus(bool requestStatus, object option = null)
        {
            return new JsonResult(option)
            {
                StatusCode = requestStatus ? StatusCodes.Status200OK : StatusCodes.Status400BadRequest
            };
        }

        /// <summary>
        /// Метод для получаеня ResponseResult из ManagerResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <returns></returns>
        protected IActionResult ResponseResult<T>(ManagerResult<T> result)
        {
            switch (result.State)
            {
                case ManagerResultState.PreconditionFailed:
                    break;
                case ManagerResultState.NotFound:
                    break;
                case ManagerResultState.Ok:
                    break;
                case ManagerResultState.Forbidden:
                    return StatusCode(402, result.Message);
                case ManagerResultState.BadRequest:
                    break;
                case ManagerResultState.Conflict:
                    break;
                case ManagerResultState.Unauthorized:
                    break;
                case ManagerResultState.MethodNotAllowed:
                    break;
                default:
                    break;
            }
            return Ok(new ResponseResult<T>(!result.Error, result.Message, result.Result));
        }

        protected IActionResult ResponseResult(ManagerResult result)
        {
            return Ok(!result.Error ? new ResponseResult(true, "Успешно!") : new ResponseResult(false, result.Message));
        }

        protected IActionResult ResponseResultDto(ManagerResult result)
        {
            return Content(JsonConvert.SerializeObject(new ResponseResultDto<object>
            {
                Success = !result.Error,
                Data = null,
                Message = result.Message
            }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new DatetimeConverter() } }), "application/json");
        }

        protected IActionResult ResponseResultDto<T>(ManagerResult<T> result)
        {
            return Content(JsonConvert.SerializeObject(new ResponseResultDto<T>
            {
                Success = !result.Error,
                Data = result.Result,
                Message = result.Message,
                Code = result.Code.ToString()
            }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>{new DatetimeConverter()}}), "application/json");
        }

        protected IActionResult ModelValidation()
        {
            var sb = new StringBuilder("Validation error:\n");

            foreach (var item in ModelState.Where(item => item.Value.Errors.Count != 0))
            {
                sb.Append($"{item.Key}: ");

                foreach (var error in item.Value.Errors)
                    sb.Append(error + ";" );
                    
                sb.AppendLine();
            }

            return ResponseResult(new ManagerResult { State = ManagerResultState.PreconditionFailed, Message = sb.ToString() });
        }
        /// <summary>
        /// Сохранить детали HTTP запроса в лог файл
        /// </summary>
        protected async Task<string> LogRequestAndGetNakedBody()
        {
            var body = await GetRequestBodyAsStringOrEmptyAsync();
            Logger.Info($"Request Details: {Request.Method} to {Request.Host} with body {body}");
            return body;
        }

        /// <summary>
        /// Попытаться получить сырое string тело запроса или пустую строку.
        /// </summary>
        /// <returns>Тело запроса</returns>
        private async Task<string> GetRequestBodyAsStringOrEmptyAsync()
        {
            try
            {
                using StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8, false, 1024, true);
                var bodyString = await reader.ReadToEndAsync();

                return bodyString;
            }

            catch (Exception ex)
            {
                Logger.Warn(ex, "Не удалось прочитать тело запроса в string");
                return string.Empty;
            }
        }
    }

}
