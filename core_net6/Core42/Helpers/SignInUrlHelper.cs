﻿using System;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace Core42.Helpers
{
    /// <summary>
    /// Хэлпер для работы с адресами при авторизации
    /// </summary>
    public static class SignInUrlHelper
    {

        /// <summary>
        /// Получить адрес редиректа при авторизации
        /// </summary>
        /// <param name="returnUrl">Адрес для редиректа после успешной авторизации</param>
        /// <param name="callbackUrl">Адрес колбека при успешной авторизации</param>
        /// <returns>Адрес редиректа при авторизации</returns>
        public static string GetSignInRedirectUrl(string returnUrl, string callbackUrl, HttpContext httpContext)
            => $"{new Uri(httpContext.Request.GetDisplayUrl()).GetLeftPart(UriPartial.Authority)}{callbackUrl}?returnUrl={WebUtility.UrlEncode(returnUrl)}";
    }
}
