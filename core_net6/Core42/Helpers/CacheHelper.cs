﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace Core42.Helpers
{
    /// <summary>
    /// Хэлпер для работы с кешем
    /// </summary>
    public static class CacheHelper
    {
        private static IMemoryCache _memoryCache;

        public static void Configure(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        /// <summary>
        /// Записать значение в кеш
        /// </summary>
        /// <typeparam name="T">Тип элемента</typeparam>
        /// <param name="o">Элемент для записи</param>
        /// <param name="key">Название элемента</param>
        public static void Add<T>(T o, string key) where T : class
        {
            _memoryCache.Set(
                key,
                o,
                DateTime.Now.AddMinutes(120));
        }

        /// <summary>
        /// Удалить элемент из кеша
        /// </summary>
        /// <param name="key">Название элемента</param>
        public static void Clear(string key)
        {
            _memoryCache.Remove(key);
        }

        /// <summary>
        /// Проверить существование элемента в кеше
        /// </summary>
        /// <param name="key">Название элемента</param>
        /// <returns>true - если такой элемент есть</returns>
        public static bool Exists(string key)
        {
            return _memoryCache.TryGetValue(key, out _);
        }

        /// <summary>
        /// Получить элемент из кеша
        /// </summary>
        /// <typeparam name="T">Тип элемента</typeparam>
        /// <param name="key">Название элемента</param>
        /// <returns>Элемент из кеша</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return _memoryCache.Get<T>(key);
            }
            catch
            {
                return null;
            }
        }
    }
}
