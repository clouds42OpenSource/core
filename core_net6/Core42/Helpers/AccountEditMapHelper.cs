﻿using System.Linq;
using Clouds42.DataContracts.Account.AccountModels;

namespace Core42.Helpers
{
    /// <summary>
    /// Хелпер для мэппинга моделей изменений аккаунта
    /// </summary>
    public static class AccountEditMapHelper
    {
        /// <summary>
        /// Сопоставить модель UpdateAccountDataDto с моделью EditAccountDc
        /// </summary>
        /// <param name="updateAccountData">Модель для мэппинга</param>
        /// <returns>Модель изменений аккаунта EditAccountDc</returns>
        public static EditAccountDto MapToEditAccountDc(this UpdateAccountDataDto updateAccountData)
        {
            return new EditAccountDto
            {
                Id = updateAccountData.Id,
                Description = updateAccountData.Description,
                AccountCaption = updateAccountData.AccountCaption,
                Inn = updateAccountData.Inn,
                IsVip = updateAccountData.IsVip,
                LocaleId = updateAccountData.LocaleId,
                AccountEmails = updateAccountData.GetEmails().ToList()
            };
        }

        /// <summary>
        /// Сопоставить модель AccountDetailsDto с моделью EditAccountDc
        /// </summary>
        /// <param name="accountDetails">Модель для мэппинга</param>
        /// <returns>Модель изменений аккаунта EditAccountDc</returns>
        public static EditAccountDto MapToEditAccountDc(this AccountDetailsModelDto accountDetails) => new()
        {
            Id = accountDetails.AccountId,
            Description = accountDetails.AccountDescription,
            AccountCaption = accountDetails.AccountCaption,
            Inn = accountDetails.AccountInn,
            IsVip = accountDetails.IsVip,
            CreateClusterDatabase = accountDetails.CreateClusterDatabase,
            LocaleId = accountDetails.LocaleId,
            AccountEmails = accountDetails.Emails,
            Deployment = accountDetails.Deployment,
            DepartmentRequests = accountDetails.DepartmentRequests,
        };
    }
}
