﻿using System;
using System.Linq;
using Clouds42.Accounts.CreateAccount.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Models;

namespace Core42.Helpers
{
    /// <summary>
    /// Хелпер для работы с договором оферты аккаунта
    /// </summary>
    public class AccountAgreementHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получить договор оферты поставщика
        /// </summary>
        /// <param name="agreementFileId">Id файла договора оферты</param>
        public CloudFile GetAgreementFile(Guid agreementFileId) =>
            dbLayer.CloudFileRepository.FirstOrDefault(cf => cf.Id == agreementFileId) ??
            throw new NotFoundException($"Не удалось найти файл по Id {agreementFileId}");

        /// <summary>
        /// Получить Id файла договора оферты
        /// </summary>
        /// <param name="registrationConfig">Модель регистрации</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Id поставщика</returns>
        public Guid GetAgreementFileId(RegistrationConfigViewModel registrationConfig, string localeName) =>
            FindSupplier(registrationConfig)?.AgreementId ?? GetDefaultSupplierByLocale(localeName).AgreementId;

        /// <summary>
        /// Найти поставщика для аккаунта, для которого идет регистрация
        /// </summary>
        /// <param name="registrationConfig">Модель регистрации</param>
        /// <returns>Поставщик</returns>
        private Supplier FindSupplier(RegistrationConfigViewModel registrationConfig)
        {
            if (registrationConfig.ReferralAccountId.HasValue)
                return dbLayer.SupplierReferralAccountRepository
                    .FirstOrDefault(sp => sp.ReferralAccountId == registrationConfig.ReferralAccountId)?.Supplier;

            if (registrationConfig.CloudServiceId.HasValue)
            {
                var supplier = dbLayer.BillingServiceRepository.AsQueryable()
                    .Join(dbLayer.SupplierReferralAccountRepository.AsQueryable(), s => s.AccountOwnerId, sr => sr.ReferralAccountId,
                        (bs, sr) => new { bs, sr })
                    .Where(s=>s.bs.Id == registrationConfig.CloudServiceId)
                    .Select(s=>s.sr.Supplier).FirstOrDefault();

                return supplier;
            }

            var internalCloudService = AccountRegistrationModelAdapter.SearchInternalCloudService(registrationConfig.RegistrationSource);

            if (internalCloudService == null)
            {
                return null;
            }

            {
                var supplier = dbLayer.BillingServiceRepository.AsQueryable()
                    .Join(dbLayer.SupplierReferralAccountRepository.AsQueryable(), s => s.AccountOwnerId, sr => sr.ReferralAccountId,
                        (bs, sr) => new { bs, sr })
                    .Where(s => s.bs.InternalCloudService == internalCloudService && s.bs.BillingServiceStatus == BillingServiceStatusEnum.IsActive)
                    .Select(s => s.sr.Supplier).FirstOrDefault();

                return supplier;
            }

        }

        /// <summary>
        /// Получить дефолтного поставщика по локали
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Поставщик</returns>
        private Supplier GetDefaultSupplierByLocale(string localeName) =>
            dbLayer.SupplierRepository.FirstOrDefault(sp => sp.Locale.Name == localeName && sp.IsDefault) ??
            throw new NotFoundException($"Не удалось найти дефолтного поставщика для локали {localeName}");
    }
}
