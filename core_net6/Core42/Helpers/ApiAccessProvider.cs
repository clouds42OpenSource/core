﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers;
using Clouds42.Common.DataModels;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Core42.Helpers
{
    public class ApiAccessProvider(
        IUnitOfWork dbLayer,
        IUserPrincipalHelper userPrincipalHelper,
        IHttpContextAccessor httpContextAccessor,
        ILogger42 logger,
        IConfiguration configuration,
        IHandlerException handlerException,
        IAccessMapping accessMapping,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping)
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;
        private readonly IConfiguration _configuration = configuration;
        private readonly IHandlerException _handlerException = handlerException;
        private const string SelectedContextAccount = nameof(SelectedContextAccount);
        public override string Name => GetUser()?.Name;

        /// <summary>
        /// Получить UserIdentity
        /// </summary>
        protected override string GetUserIdentity()
        {
            return TryGetUserIdentityFromHttpContextUser(out var userIdentity)
                ? userIdentity
                : GetUserIdentityFromHeader();
        }

        /// <summary>
        /// Получить UserIdentity из HttpContext.Current.User
        /// </summary>
        /// <param name="userIdentity">Сюда сохраняется UserIdentity</param>
        /// <returns>Возвращает <c>true</c> если получилось взять из HttpContext.Current.User, иначе <c>false</c></returns>
        private bool TryGetUserIdentityFromHttpContextUser(out string userIdentity)
        {
            userIdentity = string.Empty;
            var user = httpContextAccessor?.HttpContext?.User;
            if (user == null || 
                (!user?.Identity?.IsAuthenticated ?? false) || 
                user.Identity is not ClaimsIdentity claimsIdentity)
            {
                return false;
            }

            userIdentity = GetAuthenticationToken(claimsIdentity.Claims);
            return !string.IsNullOrEmpty(userIdentity);
        }

        /// <summary>
        /// Получить токен аутентификации
        /// </summary>
        /// <param name="claimsIdentity">Claims аутентифицируемого пользователя</param>
        /// <returns>Возвращает токен аутентификации</returns>
        private string GetAuthenticationToken(IEnumerable<Claim> claimsIdentity)
        {
            if (httpContextAccessor?.HttpContext?.User?.Identity?.AuthenticationType == CloudConfigurationProvider.JsonWebToken.GetAuthenticationType())
            {
                return claimsIdentity.Where(c => c.Type == ClaimTypes.NameIdentifier)
                    .Select(c => c.Value).SingleOrDefault();
            }

            return claimsIdentity.Where(c => c.Type == ClaimTypes.Name)
                .Select(c => c.Value).SingleOrDefault();
        }

        /// <summary>
        /// Получить UserIdentity из заголовка запроса
        /// </summary>
        private string GetUserIdentityFromHeader()
        {
            var headers = httpContextAccessor?.HttpContext?.Request.Headers;
            var tokenHeader = _configuration["TokenHeader"];

            if (tokenHeader == null)
                return "";

            var token = headers?[tokenHeader].ToString();
            return token ?? "";

        }

        private IUserPrincipalDto _user;
        
        public override IUserPrincipalDto GetUser()
        {
            if (_user != null)
                return _user;

            try
            {
                if (TryGetUserFromHttpContextUser(out var accountUserFromHttpContextUser))
                    return _user = accountUserFromHttpContextUser;

                var tokenItem = GetUserIdentity();

                if (!Guid.TryParse(tokenItem, out var token))
                    return null;

                return _user = userPrincipalHelper.GetUserAndRefreshSession(token);
            }
            
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения юзера из контекста]");

                throw;
            }
        }

        public override async Task<IUserPrincipalDto> GetUserAsync()
        {
             if (_user != null)
                return _user;

             try
             {
                 var userFromContext = await TryGetUserFromHttpContextUserAsync();

                 if (userFromContext != null)
                     return _user = userFromContext;

                 var tokenItem = GetUserIdentityFromHeader();

                 if (!Guid.TryParse(tokenItem, out var token))
                     return null;

                 return _user = userPrincipalHelper.GetUserAndRefreshSession(token);
             }
            
             catch (Exception ex)
             {
                 _handlerException.Handle(ex, "[Ошибка получения юзера из контекста]");

                 throw;
             }
        }

        /// <summary>
        /// Получения AccountUser из HttpContext.Current.User
        /// </summary>
        /// <param name="userFromHttpContextUser">Полученный AccountUser</param>
        private bool TryGetUserFromHttpContextUser(out AccountUserPrincipalDto userFromHttpContextUser)
        {
            userFromHttpContextUser = null;
            var user = httpContextAccessor?.HttpContext?.User;
            if (user == null || 
                (!user?.Identity?.IsAuthenticated ?? false) || 
                user.Identity is not ClaimsIdentity claimsIdentity)
            {
                return false;
            }
            
            var id = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier)
                .Select(c => c.Value).SingleOrDefault();
           
            if (string.IsNullOrEmpty(id) || !Guid.TryParse(id, out var accountUserId))
            {
                return false;
            }

            var accountUser = _dbLayer.AccountUsersRepository.GetById(accountUserId);
            if (accountUser == null)
            {
                return false;
            }

            var contextAccountId = GetContextAccountId(httpContextAccessor.HttpContext.Request.Cookies) ?? accountUser.AccountId;

            userFromHttpContextUser = new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                Name = accountUser.Login,
                ContextAccountId = contextAccountId,
                RequestAccountId = accountUser.AccountId,
                Email = accountUser.Email,
                PhoneNumber = accountUser.PhoneNumber,
                Groups = GetGroups(accountUser.Id),
                IsEmailVerified = accountUser.EmailStatus == "Checked",
                IsPhoneVerified = accountUser.IsPhoneVerified,
                IsNew = accountUser.CreationDate >= DateTime.Now.AddDays(-7),
                Locale = localesConfigurationDataProvider.GetLocaleConfigurationForAccount(contextAccountId)?.LocaleName,
                Deployment = accountUser.Account.Deployment,
                FirstName = accountUser.FirstName
            };

            return true;
        }

        /// <summary>
        /// Получения AccountUser из HttpContext.Current.User
        /// </summary>
        private async Task<AccountUserPrincipalDto> TryGetUserFromHttpContextUserAsync()
        {
            var user = httpContextAccessor?.HttpContext?.User;
            if (user == null ||
                (!user?.Identity?.IsAuthenticated ?? false) ||
                user.Identity is not ClaimsIdentity claimsIdentity)
            {
                return null;
            }

            var id = claimsIdentity.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier)
                .Select(c => c.Value).SingleOrDefault();

            if (string.IsNullOrEmpty(id) || !Guid.TryParse(id, out var accountUserId))
            {
                return null;
            }

            var accountUser = await _dbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.Id == accountUserId);

            if (accountUser == null)
            {
                return null;
            }

            var groups = await _dbLayer.AccountUserRoleRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountUserId == accountUserId)
                .Select(x => x.AccountUserGroup)
                .ToListAsync();

            var accConf = await _dbLayer.AccountConfigurationRepository
                .AsQueryableNoTracking()
                .FirstOrDefaultAsync(x => x.AccountId == accountUser.AccountId);

            var contextAccountId = GetContextAccountId(httpContextAccessor.HttpContext.Request.Cookies) ?? accountUser.AccountId;

            return new AccountUserPrincipalDto
            {
                Id = accountUser.Id,
                Name = accountUser.Login,
                ContextAccountId = contextAccountId,
                RequestAccountId = accountUser.AccountId,
                Email = accountUser.Email,
                PhoneNumber = accountUser.PhoneNumber,
                Groups = groups,
                IsEmailVerified = accountUser.EmailStatus == "Checked",
                IsPhoneVerified = accountUser.IsPhoneVerified,
                IsNew = accountUser.CreationDate >= DateTime.Now.AddDays(-7),
                Locale = localesConfigurationDataProvider.GetLocaleConfigurationForAccount(contextAccountId)?.LocaleName,
                UserSource = accConf.Type,
                IsService = groups.Contains(AccountUserGroup.Cloud42Service),
                FirstName = accountUser.FirstName
            };
        }
        public override Guid? GetContextAccountId(IRequestCookieCollection cookie)
        {
            var exist = cookie[SelectedContextAccount];
            var guid = !string.IsNullOrEmpty(exist)
                ? Guid.Parse(exist)
                : (Guid?)null;
            return guid;
        }

        public override string GetUserAgent()
        {
            var currentRequest = httpContextAccessor?.HttpContext?.Request;

            if (currentRequest == null)
                return string.Empty;

            return currentRequest.Headers.Any(h => h.Key == "UserAgent") ?
                currentRequest.Headers.FirstOrDefault(h => h.Key == "UserAgent").Value : currentRequest.Headers["User-Agent"].ToString();
        }

        public override string GetUserHostAddress() => httpContextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString();

        public override bool IsCurrentContextAccount()
        {
            var exist = httpContextAccessor.HttpContext?.Request.Cookies[SelectedContextAccount];
            return exist == null || string.IsNullOrEmpty(exist);
        }

        public override bool SetContextAccount(Guid accountId)
        {

            var byId = _dbLayer.AccountsRepository.GetById(accountId);
            if (byId == null) return false;
            var cookieOption = new CookieOptions { Expires = DateTime.Now.AddDays(1), HttpOnly = true, Secure = true, Domain = ".42clouds.com", };

            httpContextAccessor.HttpContext?.Response.Cookies
                    .Append(
                        SelectedContextAccount,
                        accountId.ToString(),
                        cookieOption
                    );
            
            return true;
        }

        public override void SetCurrentContextAccount(IResponseCookies cookies)
        {
            cookies.Delete(SelectedContextAccount, new CookieOptions { Expires = DateTime.Now.AddDays(-1), HttpOnly = true, Secure = true, Domain = ".42clouds.com", });
        }

    }

}
