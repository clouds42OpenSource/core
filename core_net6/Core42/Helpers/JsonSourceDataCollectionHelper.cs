﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Core42.Helpers
{
    /// <summary>
    /// Класс для построения SourceDataCollection из Json объектов
    /// </summary>
    public static class JsonSourceDataCollectionHelper
    {
        /// <summary>
        /// Создать коллекцию параметров из JObject
        /// </summary>
        /// <param name="jObject">JObject откуда нужно взять параметры</param>
        /// <returns>Коллекция параметров</returns>
        public static ICollection<KeyValuePair<string, string>> GetSourceDataCollection(this JObject jObject)
        {
            if (jObject == null) throw new ArgumentNullException(nameof(jObject));
            var parameters = new List<KeyValuePair<string, string>>();
            AddParametersFrom(parameters, jObject, []);
            return parameters;
        }

        /// <summary>
        /// Загружает параметры из JObject и добавляет их в коллекцию
        /// </summary>
        /// <param name="collection">Коллекция куда будут добавлены параметры</param>
        /// <param name="jObject">Json объект откуда нужно взять параметры</param>
        /// <param name="path">Базовый путь параметра</param>
        public static void AddParametersFrom(
            this ICollection<KeyValuePair<string, string>> collection,
            JObject jObject,
            IEnumerable<string> path = null)
        {
            if (jObject == null) throw new ArgumentNullException(nameof(jObject));
            if (collection == null) throw new ArgumentNullException(nameof(collection));

            foreach (var property in jObject.Properties())
            {
                if (string.IsNullOrWhiteSpace(property.Name))
                    continue;
                collection.AddParametersFrom(property.Value, AppendPathItem(property.Name, ".", path));
            }
        }

        /// <summary>
        /// Загружает параметры из JToken и добавляет их в коллекцию        
        /// </summary>
        /// <param name="collection">Коллекция куда будут добавлены параметры</param>
        /// <param name="jToken">JToken откуда нужно взять параметры</param>
        /// <param name="path">Базовый путь параметра</param>
        public static void AddParametersFrom(
            this ICollection<KeyValuePair<string, string>> collection,
            JToken jToken,
            IEnumerable<string> path = null)
        {
            if (jToken == null) throw new ArgumentNullException(nameof(jToken));
            if (collection == null) throw new ArgumentNullException(nameof(collection));

            if (jToken is JValue jValue)
            {
                collection.AddParametersFrom(jValue, path); return;
            }

            if (jToken is JObject jObject)
            {
                collection.AddParametersFrom(jObject, path); return;
            }

            if (jToken is JArray jArray)
            {
                collection.AddParametersFrom(jArray, path);
            }
        }

        /// <summary>
        /// Добавить параметры из JValue
        /// </summary>
        /// <param name="collection">Коллекция куда будут добавлены параметры</param>
        /// <param name="jValue">JValue откуда нужно взять параметры</param>
        /// <param name="path">Базовый путь параметра</param>
        public static void AddParametersFrom(this ICollection<KeyValuePair<string, string>> collection,
            JValue jValue,
            IEnumerable<string> path = null)
        {
            if (jValue == null) throw new ArgumentNullException(nameof(jValue));
            if (collection == null) throw new ArgumentNullException(nameof(collection));


            collection.Add(new KeyValuePair<string, string>(string.Join(string.Empty, path), jValue.ToString(null, CultureInfo.InvariantCulture)));
        }

        /// <summary>
        /// Загружает параметры из JArray и добавляет их в коллекцию
        /// </summary>
        /// <param name="collection">Коллекция куда будут добавлены параметры</param>
        /// <param name="jArray">JArray откуда нужно взять параметры</param>
        /// <param name="path">Коллекция базового пути парамерта</param>
        public static void AddParametersFrom(this ICollection<KeyValuePair<string, string>> collection,
            JArray jArray,
            IEnumerable<string> path = null)
        {
            if (jArray == null) throw new ArgumentNullException(nameof(jArray));
            if (collection == null) throw new ArgumentNullException(nameof(collection));

            var index = 0;
            foreach (var item in jArray)
            {
                collection.AddParametersFrom(item, AppendPathItem($"[{index}]", string.Empty, path));
                index++;
            }
        }


        /// <summary>
        /// Добавить элемент к коллекции пути
        /// </summary>
        /// <param name="pathItem">Дотбавляемый элемент</param>
        /// <param name="delimiter">Разделитель (используется только если базовый путь не пуст)</param>
        /// <param name="basePath">Коллекция базового пути парамерта</param>
        /// <returns>Коллекция пути с добавленным элементом</returns>
        private static IEnumerable<string> AppendPathItem(string pathItem, string delimiter, IEnumerable<string> basePath = null) =>
            basePath == null || !basePath.Any() ? new[] { $"{pathItem}" } : basePath.Concat(new[] { $"{delimiter}{pathItem}" });
    }
}
