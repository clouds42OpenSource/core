﻿using System;
using Microsoft.AspNetCore.Http;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Access;
using Clouds42.Locales.Contracts.Interfaces;

namespace Core42.Helpers
{
    /// <summary>
    /// Доступ к контексту
    /// </summary>
    public class ContextAccess(
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IHttpContextAccessor httpContextAccessor,
        IAccessProvider controlPanelAccessProvider)
    {
        /// <summary>
        /// Провайдер доступа к панели управления
        /// </summary>
        public IAccessProvider Provider { get; } = controlPanelAccessProvider;

        /// <summary>
        /// Логин текущего пользователя
        /// </summary>
        public string Name => Provider.Name;

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string ContextAccountName => Provider.ContextAccountName;

        /// <summary>
        /// Порядковый номер аккаунта
        /// </summary>
        public string ContextAccountIndex => Provider.ContextAccountIndex;

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public Guid ContextAccountId => Provider.ContextAccountId;

        /// <summary>
        /// Id текущего пользователя
        /// </summary>
        public Guid CurrentAccountUserId => Provider.GetUser().Id;

        /// <summary>
        /// Пользователь авторизован
        /// </summary>
        public bool IsAuthenticated()
        {
            if (httpContextAccessor == null)
                throw new ArgumentException($"{nameof(IHttpContextAccessor)} was null");

            return httpContextAccessor.HttpContext.User.Identity.IsAuthenticated && Provider.GetUser() != null;
        } 

        /// <summary>
        /// Свой аккаунт
        /// </summary>
        public bool IsCurrentContextAccount => Provider.IsCurrentContextAccount();

        /// <summary>
        /// Адрес сайта личного кабинета
        /// </summary>
        public string CpSiteUrl()
        {
            if (localesConfigurationDataProvider == null)
                throw new ArgumentException($"{nameof(ILocalesConfigurationDataProvider)} was null");

            return Provider.GetUser() != null
                ? localesConfigurationDataProvider.GetCpSiteUrlForAccount(Provider.GetUser().RequestAccountId)
                : CloudConfigurationProvider.Cp.GetSiteAuthorityUrl();
        }
            

        /// <summary>
        /// Установить контекст аккаунта
        /// </summary>
        /// <param name="accountId">Номер аккаунта</param>
        public bool SetContextAccount(Guid accountId)
        {
            Provider.HasAccess(ObjectAction.ControlPanel_ModeOtherAccount, ()=> accountId);
            return Provider.SetContextAccount(accountId);
        }

        /// <summary>
        /// Установить текущий контекст аккаунта
        /// </summary>
        public void SetCurrentContextAccount()
        {
            Provider.SetCurrentContextAccount(httpContextAccessor.HttpContext?.Response.Cookies);
        }
    }
}
