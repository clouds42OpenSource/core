﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DependencyRegistration;
using Clouds42.Jwt;
using Clouds42.Logger.Serilog;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.RuntimeContext;
using Core42.Helpers;
using Core42.Providers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories;

namespace Core42
{
    public static class DependencyConfiguration
    {
        public static IServiceCollection AddServices(this IServiceCollection collection, IConfiguration configuration)
        {
            collection
                .AddScoped<IUnitOfWork, UnitOfWork>()
                .AddTransient<IUserPrincipalHelper, UserPrincipalHelper>()
                .AddTransient<IAccessProvider, ApiAccessProvider>()
                .AddTransient<AccountAgreementHelper>()
                .AddTransient<ICoreExecutionContext, ApiCoreExecutionContext>()
                .AddTransient<IRequestContextTaskDispatcher, RequestContextTaskDispatcher>()
                .AddTransient<ISettingsProvider, SettingsProvider>()
                .AddScoped<ContextAccess>()
                .AddMemoryCache()
                .AddHttpClient()
                .AddJwt()
                .AddGlobalServices(configuration);

            return collection;
        }
        
        public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
        {
            var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

            var elasticConfig = new ElasticConfigDto(
                    conf["Elastic:Uri"],
                    conf["Elastic:UserName"],
                    conf["Elastic:Password"],
                    conf["Elastic:ApiIndex"],
                    conf["Logs:AppName"]
                );

            SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);

            return collection;
        }
    }
}
