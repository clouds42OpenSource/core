﻿using System;
using System.ComponentModel;
using System.Globalization;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Microsoft.Extensions.Configuration;

namespace Clouds42.Configurations
{

    /// <summary>
    /// Хелпер для работы с клауд конфигами
    /// </summary>
    public static class ConfigurationHelper
    {
        static IConfiguration _configuration;
        public static void SetConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        

        /// <summary>
        /// Получить значение клауд конфига
        /// </summary>
        /// <param name="key">Ключ конфигурации</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Значение клауд конфига</returns>
        public static string GetConfigurationValue(string key, Guid? supplierId = null)
        {
            return GetConfigurationValue<string>(key, supplierId);
        }

        /// <summary>
        /// Получить значение клауд конфига
        /// </summary>
        /// <typeparam name="T">Тип значения конфига</typeparam>
        /// <param name="key">Ключ конфигурации</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Значение клауд конфига</returns>
        public static T GetConfigurationValue<T>(string key, Guid? supplierId = null)
        {
            if (supplierId.HasValue)
            {
                key += $"_{supplierId.Value}";
            }

            var value = _configuration.GetSection(key).Value;

            return string.IsNullOrEmpty(value) ? throw new NotFoundException($"Configuration not found by key {key}") : (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(value);
        }
        

        /// <summary>
        /// Получить промежуток времени конфигурации
        /// </summary>
        /// <param name="key">Ключ конфигурации</param>
        /// <param name="format">Формат времени</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Промежуток времени конфигурации</returns>
        public static TimeSpan GetTimeSpanConfiguration(string key, string format = "hh\\:mm\\:ss", Guid? supplierId = null)
        {
            var value = GetConfigurationValue<string>(key, supplierId);
            var timeSpanConfiguration = TimeSpan.ParseExact(value, format, new NumberFormatInfo());
            return timeSpanConfiguration;
        }

        /// <summary>
        /// Получить полный путь до экшена
        /// </summary>
        /// <param name="partialActionPath">Частичный путь до экшена</param>
        /// <returns>Полный путь до экшена</returns>
        public static string GetFullPathToAction(string partialActionPath) => $"{CloudConfigurationProvider.Cp.GetSiteCoreUrl()}{partialActionPath}";
    }
}
