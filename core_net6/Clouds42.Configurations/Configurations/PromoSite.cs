﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Промо сайт
        /// </summary>
        public static class PromoSite
        {
            /// <summary>
            /// Получить адрес промо сайта для русской локали
            /// </summary>
            /// <returns>Адрес промо сайта для русской локали</returns>
            public static string GetRuPromoSiteUrl()
                => ConfigurationHelper.GetConfigurationValue("PromoSite.RuUrl");

            /// <summary>
            /// Получить адрес промо сайта для украинской локали
            /// </summary>
            /// <returns>Адрес промо сайта для украинской локали</returns>
            public static string GetUaPromoSiteUrl()
                => ConfigurationHelper.GetConfigurationValue("PromoSite.UaUrl");
        }
    }
}
