﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Биллинг сервисы.
        /// </summary>
        public static class BillingServices
        {
            /// <summary>
            /// Получить номер сервиса ЕCДЛ
            /// </summary>
            public static Guid GetEsdl42ServiceId()
                => ConfigurationHelper.GetConfigurationValue<Guid>("Esdl42DaysResourceId");

            /// <summary>
            /// Получить номер сервиса Recognition2
            /// </summary>
            public static Guid GetRecognition42ServiceId()
                => ConfigurationHelper.GetConfigurationValue<Guid>("Recognition42LicenseResourceId");

            /// <summary>
            /// Получить значение оптимального отстатока лицензий рек42 для активной аренды. 
            /// </summary>
            public static int GetOptimalLimitRc42ResourceForActiveRent1C()
                => ConfigurationHelper.GetConfigurationValue<int>("BillingServices.OptimalLimitRc42ResourceForActiveRent1C");

            /// <summary>
            /// Получить количество месяцев после которых будет
            /// доступно следующее редактирование стоимости сервиса
            /// </summary>
            public static int GetBeforeNextPossibleEditServiceCostMonthsCount()
                => ConfigurationHelper.GetConfigurationValue<int>("BillingServices.BeforeNextPossibleEditServiceCostMonthsCount");

            /// <summary>
            /// Получить количество месяцев
            /// до пересчета стоимости сервиса
            /// </summary>
            public static int GetBeforeRecalculationCostOfServiceMonthsCount()
                => ConfigurationHelper.GetConfigurationValue<int>("BillingServices.BeforeRecalculationCostOfServiceMonthsCount");

            /// <summary>
            /// Получить количество месяцев
            /// до удаление услуг сервиса для аккаунта
            /// </summary>
            public static int GetBeforeDeletingServiceTypesMonthsCount()
                => ConfigurationHelper.GetConfigurationValue<int>("BillingServices.BeforeDeletingServiceTypesMonthsCount");

            /// <summary>
            /// Получить ссылку на инструкцию по созданию  комплекта поставки
            /// </summary>
            /// <returns>Ссылка на инструкцию по созданию  комплекта поставки</returns>
            public static string GetLinkToInstructionsForCreatingDeliveryKit()
                => ConfigurationHelper.GetConfigurationValue<string>("BillingService.LinkToInstructionsForCreatingDeliveryKit");

            /// <summary>
            /// Мой Диск
            /// </summary>
            public static class MyDisk
            {
                /// <summary>
                /// Кол-во бесплатных Гб по умолчанию
                /// </summary>
                /// <returns></returns>
                public static int GetMyDiskFreeTariffSize()
                    => ConfigurationHelper.GetConfigurationValue<int>("Services.MyDisk.FreeTariffSize");
            }
        }
    }
}
