﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации локали
        /// </summary>
        public static class LocaleConfiguration
        {
            /// <summary>
            /// Получить интервал проверки обновлений в минутах
            /// </summary>
            /// <returns>Интервал проверки обновлений в минутах</returns>
            public static int GetCheckUpdatesIntervalInMinutes()
                => ConfigurationHelper.GetConfigurationValue<int>("LocaleConfiguration.CheckUpdatesIntervalInMinutes");
        }
    }
}
