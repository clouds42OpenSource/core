﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Настройка облака
        /// </summary>
        public static class CloudCore
        {
            /// <summary>
            /// Получить разницу в часах между Украиной и Москвой
            /// </summary>
            /// <returns>Разница в часах между Украиной и Москвой</returns>
            public static int GetHoursDifferenceOfUkraineAndMoscow()
                => ConfigurationHelper.GetConfigurationValue<int>("HoursDifferenceOfUkraineAndMoscow");
        }
    }
}
