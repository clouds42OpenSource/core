﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации аренды 1С
        /// </summary>
        public static class ConfigRental1C
        {
            /// <summary>
            /// Количество бесплатных лицензий на демо период
            /// </summary>
            /// <returns></returns>
            public static int FreeResourceCountAtDemoPeriod()
                => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.FreeResourceCountAtDemoPeriod");

            public static class ServiceExpired
            {

                /// <summary>
                /// Получить количество дней до архивации аккаунта с просроченной арендой.
                /// </summary>
                /// <returns>Количество дней до архивации аккаунта с просроченной арендой</returns>
                public static int GetArchiveDays()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.ArchiveDays");

                /// <summary>
                /// Получить количество дней перед архивацией данных неактивных аккаунтов
                /// </summary>
                /// <returns>Количество дней перед архивацией данных неактивных аккаунтов</returns>
                public static int GetBeforeArchiveDataDaysCount()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.BeforeArchiveDataDaysCount");

                /// <summary>
                /// Получить количество дней перед архивацией данных неактивных демо аккаунтов
                /// </summary>
                /// <returns>Количество дней перед архивацией данных неактивных демо аккаунтов</returns>
                public static int GetBeforeArchiveDataDemoDaysCount()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.BeforeArchiveDataDemoDaysCount");

                /// <summary>
                /// Получить количество дней до архивации демо-аккаунта с просроченной арендой.
                /// </summary>
                /// <returns>Количество дней до архивации демо-аккаунта с просроченной арендой</returns>
                public static int GetArchiveDemoDays()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.ArchiveDemoDays");

                /// <summary>
                /// Получить доступное количество баз на разделителях для архивации
                /// </summary>
                /// <returns>Доступное количество баз на разделителях для архивации</returns>
                public static int GetAvailableCountOfDbOnDelimitersForArchive()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.AvailableCountOfDbOnDelimitersForArchive");

                /// <summary>
                /// Получить количество дней после окончания аренды до удаления данных
                /// </summary>
                /// <returns>Количество дней после окончания аренды до удаления данных</returns>
                public static int GetDaysCountAfterRent1CExpiredForAccountBeforeDeleteData()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.DaysCountAfterRent1CExpiredForAccountBeforeDeleteData");

                /// <summary>
                /// Получить количество аккаунтов для удаления данных
                /// </summary>
                /// <returns>Количество аккаунтов для удаления данных</returns>
                public static int GetAccountsCountForDeleteData()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.GetAccountsCountForDeleteData");

                /// <summary>
                /// Получить время жизни данных аккаунта в склепе(количество дней)
                /// </summary>
                /// <returns>Время жизни данных аккаунта в склепе(количество дней)</returns>
                public static int GetLifetimeAccountDataInTombDaysCount()
                    => ConfigurationHelper.GetConfigurationValue<int>("ConfigRental1C.ServiceExpired.LifetimeAccountDataInTombDaysCount");
            }
        }
    }
}
