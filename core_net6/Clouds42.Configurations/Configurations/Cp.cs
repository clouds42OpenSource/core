﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {

        /// <summary>
        /// Настройки сайта.
        /// </summary>
        public static class Cp
        {
            /// <summary>
            /// Получить полный путь до урл сайта(com)
            /// </summary>
            public static string GetSiteAuthorityUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.SiteAuthorityUrl");

            /// <summary>
            /// Получить полный путь до урл ядра
            /// </summary>
            public static string GetSiteCoreUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreApi.SiteAuthorityUrl");
            
            /// <summary>
            /// Получить полный путь до урл сайта (pro)
            /// </summary>
            /// <returns></returns>
            public static string GetSiteProAuthorityUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.SiteProAuthorityUrl");

            /// <summary>
            /// Получить путь для просмотра документа агента
            /// </summary>
            public static string GetRouteValueForOpenAgentDocument()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.OpenAgentDocumentRouteValue");

            /// <summary>
            /// Получить путь для просмотра файла разработки сервиса
            /// </summary>
            /// <returns></returns>
            public static string GetRouteValueForOpenBillingService1CFile()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForOpenBillingService1CFile");

            /// <summary>
            /// Получить путь для просмотра заявки на вывод средств агента
            /// </summary>
            public static string GetRouteValueForOpenAgentCashOutRequest()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.OpenCashOutRequest");

            /// <summary>
            /// Получить путь для создания реквизитов агента
            /// </summary>
            public static string GetRouteValueForCreateAgentRequisites()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.CreateAgentRequisites");

            /// <summary>
            /// Получить путь для главной страницы
            /// </summary>
            public static string GetRouteValueForIndexPage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForIndexPage");

            /// <summary>
            /// Получить путь для активации сервиса
            /// </summary>
            public static string GetRouteValueForActivateServicePage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForActivateServicePage");

            /// <summary>
            /// Получить путь для просмотра деталей рабочего процесса
            /// </summary>
            public static string GetRouteValueForOpenProcessFlowDetails()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForOpenProcessFlowDetails");

            /// <summary>
            /// Получить путь для актуального агентского соглашения
            /// </summary>
            public static string GetRouteValueForOpenActualAgencyAgreement()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForOpenActualAgencyAgreement");

            /// <summary>
            /// Получить путь для открытия карточки сервиса
            /// </summary>
            public static string GetRouteValueForOpenBillingServiceCard()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForOpenBillingServiceCard");

            /// <summary>
            /// Получить путь для открытия страницы сервиса
            /// </summary>
            public static string GetRouteValueForOpenBillingServicePage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForOpenBillingServicePage");

            /// <summary>
            /// Получить путь для отказа от подписки уведомлений
            /// </summary>
            /// <returns>Путь для отказа от подписки уведомлений</returns>
            public static string GetRouteValueForUnsubscribe()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteValueForUnsubscription");

            /// <summary>
            /// Получить путь для получения изображения рекламного баннера
            /// </summary>
            /// <returns>Путь для получения изображения рекламного баннера</returns>
            public static string GetRouteForGettingAdvertisingBannerImage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForGettingAdvertisingBannerImage");

            /// <summary>
            /// Получить путь для открытия страницы баланса
            /// </summary>
            /// <returns>Путь для открытия страницы баланса</returns>
            public static string GetRouteForOpenBalancePage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForOpenBalancePage");

            /// <summary>
            /// Получить путь для открытия страницы инф. баз
            /// </summary>
            /// <returns>Путь для открытия страницы инф. баз</returns>
            public static string GetRouteForOpenAccountDatabasesPage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForOpenAccountDatabasesPage");

            /// <summary>
            /// Получить путь для открытия страницы Аренды 1С
            /// </summary>
            /// <returns>Путь для открытия страницы Аренды 1С</returns>
            public static string GetRouteForOpenRent1CPage()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForOpenRent1CPage");

            /// <summary>
            /// Получить урл для сброса пароля
            /// </summary>
            /// <returns>Урл для сброса пароля</returns>
            public static string GetRouteForResetPassword()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForResetPassword");

            /// <summary>
            /// Получить урл для оплаты через агрегатор ЮKassa
            /// </summary>
            /// <returns>Урл для оплаты через агрегатор ЮKassa</returns>
            public static string GetRouteForPayViaYookassa()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForPayViaYookassa");

            /// <summary>
            /// Получить урл для запуска задачи
            /// на проверку статуса платежа агрегатора ЮKassa
            /// </summary>
            /// <returns>Урл для запуска задачи
            /// на проверку статуса платежа агрегатора ЮKassa</returns>
            public static string GetRouteForRunTaskToCheckYookassaPaymentStatus()
                => ConfigurationHelper.GetConfigurationValue<string>("Cp.RouteForRunTaskToCheckYookassaPaymentStatus");

        }
    }    
}
