﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        ///     Сегмент
        /// </summary>
        public static class Segment
        {
            /// <summary>
            ///     Путь к модулю IIS для версий 8.2
            /// </summary>
            public static string V82ModulePath()
                => ConfigurationHelper.GetConfigurationValue<string>("Segment.IIS.V82ModulePath");

            /// <summary>
            ///     Путь к модулю IIS для версий 8.3
            /// </summary>
            public static string V83ModulePath()
                => ConfigurationHelper.GetConfigurationValue<string>("Segment.IIS.V83ModulePath");
        }
    }
}
