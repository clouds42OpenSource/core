﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Параметры для консольного приложения работающего с 1С
        /// </summary>
        public static class ConsoleApp1C
        {
            /// <summary>
            /// Время ожидания в секундах подключения к 1С через COM
            /// </summary>
            public static int GetComConnectionTimeoutInSeconds() =>
                ConfigurationHelper.GetConfigurationValue<int>("ConsoleApp.1C.ComConnectionTimeout.Seconds");

            /// <summary>
            /// Количество попыток подключения к 1С через COM
            /// </summary>
            public static byte GetComConnectionAttempts() =>
                ConfigurationHelper.GetConfigurationValue<byte>("ConsoleApp.1C.ComConnection.Attempts");


            /// <summary>
            /// Время ожидания в минутах выполнения применения обновлений к инфобазе 1С
            /// </summary>
            public static int GetApplyUpdatesTimeoutInMinutes() =>
                ConfigurationHelper.GetConfigurationValue<int>("ConsoleApp.1C.ApplyUpdatesTimeout.Minutes");

            /// <summary>
            /// Количество попыток выполнения применения обновлений к инфобазе 1С
            /// </summary>
            public static byte GetApplyUpdatesAttempts() =>
                ConfigurationHelper.GetConfigurationValue<byte>("ConsoleApp.1C.ApplyUpdates.Attempts");


            /// <summary>
            /// Время ожидания в минутах получения метаданных инфобазы 1С
            /// </summary>
            public static int GetRetrieveDatabaseMetadataTimeoutInMinutes() =>
                ConfigurationHelper.GetConfigurationValue<int>("ConsoleApp.1C.RetrieveDatabaseMetadataTimeout.Minutes");

            /// <summary>
            /// Количество попыток получения метаданных инфобазы 1С
            /// </summary>
            public static byte GetRetrieveDatabaseMetadataAttempts() =>
                ConfigurationHelper.GetConfigurationValue<byte>("ConsoleApp.1C.RetrieveDatabaseMetadata.Attempts");


            /// <summary>
            /// Время ожидания для обновления версии конфигурации инфобазы 1С
            /// </summary>
            public static int GetUpdateDatabaseConfigurationTimeoutInMinutes() =>
                ConfigurationHelper.GetConfigurationValue<int>(
                    "ConsoleApp.1C.UpdateDatabaseConfigurationTimeout.Minutes");

            /// <summary>
            /// Количество попыток обновления версии конфигурации инфобазы 1С
            /// </summary>
            public static byte GetUpdateDatabaseConfigurationAttempts() =>
                ConfigurationHelper.GetConfigurationValue<byte>(
                    "ConsoleApp.1C.UpdateDatabaseConfiguration.Attempts");

        }
    }
}
