﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {

        /// <summary>
        /// Тардис
        /// </summary>
        public static class Tardis
        {
            /// <summary>
            /// Получить урл апи Тардис.
            /// </summary>
            public static Uri GetTardisApiUrl()
                => new(ConfigurationHelper.GetConfigurationValue<string>("Tardis.TardisApiUrl"));

            /// <summary>
            /// Получить ID инф. базы Тардис
            /// </summary>
            /// <returns>ID инф. базы Тардис</returns>
            public static Guid GetAccountDatabaseId()
                => ConfigurationHelper.GetConfigurationValue<Guid>("Tardis.AccountDatabaseId");

            /// <summary>
            /// Получить урл апи метода Тардис удаления пользователя по ID ядра.
            /// </summary>
            public static string GetRouteValueForDeleteUser()
                => ConfigurationHelper.GetConfigurationValue<string>("Tardis.RouteValueForDeleteUser");

            /// <summary>
            /// Получить урл апи метода Тардис для установки статуса доступа в информационной базе для пользователя.
            /// </summary>
            public static string GetRouteValueForSetAccess()
                => ConfigurationHelper.GetConfigurationValue<string>("Tardis.RouteValueForSetAccess");

            /// <summary>
            /// Получить урл апи метода Тардис для синхронизации свойств информационной базы.
            /// </summary>
            public static string GetRouteValueForSetBaseProperties()
                => ConfigurationHelper.GetConfigurationValue<string>("Tardis.RouteValueForSetBaseProperties");

            /// <summary>
            /// Получить урл апи метода Тардис для синхронизации свойств пользователя.
            /// </summary>
            public static string GetRouteValueForSetUserProperties()
                => ConfigurationHelper.GetConfigurationValue<string>("Tardis.RouteValueForSetUserProperties");

            /// <summary>
            /// Получить урл апи метода Тардис для синхронизации добавления инф. базы.
            /// </summary>
            public static string GetRouteValueForAddAccountDatabase()
                => ConfigurationHelper.GetConfigurationValue<string>("Tardis.RouteValueForAddAccountDatabase");
        }
    }

}
