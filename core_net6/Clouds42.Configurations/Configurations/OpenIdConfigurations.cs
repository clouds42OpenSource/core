﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Данные OpenId провайдера
        /// </summary>
        public static class OpenId
        {
            /// <summary>
            /// Провайдер OpenId Саюри
            /// </summary>
            public static class Sauri
            {

                /// <summary>
                /// Получить урл для управления состоянием пользователей
                /// </summary>
                public static string GetUrlControlUser()
                    => ConfigurationHelper.GetConfigurationValue<string>("OpenId.Sauri.Url.ControlUser");

                /// <summary>
                /// Получить урл провайдера OpenId
                /// </summary>
                public static string GetUrlOidProvider()
                    => ConfigurationHelper.GetConfigurationValue<string>("OpenId.Sauri.Url.OidProviderUrl");
            }
        }
    }
}