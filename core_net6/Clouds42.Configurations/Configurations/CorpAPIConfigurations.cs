﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Данные CorpAPI
        /// </summary>
        public static class CorpApi
        {
            /// <summary>
            /// Получить логин
            /// </summary>
            public static string GetUsername()
                => ConfigurationHelper.GetConfigurationValue<string>("CorpAPI.Username");

            /// <summary>
            /// Получить пароль
            /// </summary>
            public static string GetPassword()
                => ConfigurationHelper.GetConfigurationValue<string>("CorpAPI.Password");

            /// <summary>
            /// Данные CorpAPI.Url
            /// </summary>
            public static class Url
            {
                /// <summary>
                /// Получить урл для создания пользователя
                /// </summary>
                public static Uri GetUrlPing()
                    => new(ConfigurationHelper.GetConfigurationValue<string>("CorpAPI.Url.Ping"));

                /// <summary>
                /// Получить урл для удаления пользователя
                /// </summary>
                public static Uri GetUrlGetAct()
                    => new(ConfigurationHelper.GetConfigurationValue<string>("CorpAPI.Url.GetAct"));
            }
        }
    }
}