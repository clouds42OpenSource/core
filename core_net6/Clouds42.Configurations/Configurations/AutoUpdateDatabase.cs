﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Автообновление инф. базы
        /// </summary>
        public static class AutoUpdateDatabase
        {
            /// <summary>
            /// Получить Id задачи которая выполняет задачу по АО
            /// </summary>
            /// <returns>Id задачи</returns>
            public static Guid GetCoreWorkerTaskId() =>
                ConfigurationHelper.GetConfigurationValue<Guid>("AutoUpdateDatabase.CoreWorkerTaskId");

            public static string GetObnovlyatorBase64StartString() =>
                ConfigurationHelper.GetConfigurationValue<string>("AutoUpdateDatabase.ObnovlyatorBase64StartString");

            public static string GetObnovlyatorBase64EndString() =>
                ConfigurationHelper.GetConfigurationValue<string>("AutoUpdateDatabase.ObnovlyatorBase64EndString");

            public static string GetObnovlyator1CAccessUserName() =>
                ConfigurationHelper.GetConfigurationValue<string>("AutoUpdateDatabase.GetObnovlyator1CAccessUserName");

            public static string GetObnovlyator1CAccessPassword() =>
                ConfigurationHelper.GetConfigurationValue<string>("AutoUpdateDatabase.GetObnovlyator1CAccessPassword");
        }
    }
}
