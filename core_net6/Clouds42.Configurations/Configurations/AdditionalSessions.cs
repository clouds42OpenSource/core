﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        public static class AdditionalSessions
        {
            public static string GetMsUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("AdditionalSessions.GetMsUrl");
        }
    }
    
}
