﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации сервиса BigQuery
        /// </summary>
        public static class BigQuery
        {
            /// <summary>
            /// Получить Id проекта в сервисе BigQuery
            /// </summary>
            /// <returns>Id проекта в сервисе BigQuery</returns>
            public static string GetProjectId()
                => ConfigurationHelper.GetConfigurationValue<string>("BigQuery.ProjectId");

            /// <summary>
            /// Получить название базы в сервисе BigQuery
            /// </summary>
            /// <returns>Название базы в сервисе BigQuery</returns>
            public static string GetDatabaseName()
                => ConfigurationHelper.GetConfigurationValue<string>("BigQuery.DatabaseName");

            /// <summary>
            /// Получить путь к файлу с учетными данными к сервису BigQuery
            /// </summary>
            /// <returns>Путь к файлу с учетными данными к сервису BigQuery</returns>
            public static string GetPathToCredentialFile()
                => ConfigurationHelper.GetConfigurationValue<string>("BigQuery.PathToCredentialFile");

            /// <summary>
            /// Получить дефолтный размер порции данных для записи/вставки
            /// </summary>
            /// <returns>Дефолтный размер порции данных для записи/вставки</returns>
            public static int GetDefaultDataChunkSizeForInsert()
                => ConfigurationHelper.GetConfigurationValue<int>("BigQuery.DefaultDataChunkSizeForInsert");

            /// <summary>
            /// Получить название таблицы BigQuery для аккаунтов
            /// </summary>
            /// <returns>Название таблицы BigQuery для аккаунтов</returns>
            public static string GetAccountsTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.AccountsTableName");

            /// <summary>
            /// Получить название таблицы BigQuery для пользователей
            /// </summary>
            /// <returns>Название таблицы BigQuery для пользователей</returns>
            public static string GetAccountUsersTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.AccountUsersTableName");

            /// <summary>
            /// Получить название таблицы BigQuery для ресурсов
            /// </summary>
            /// <returns>Название таблицы BigQuery для ресурсов</returns>
            public static string GetResourcesTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.ResourcesTableName");

            /// <summary>
            /// Получить название таблицы BigQuery для счетов на оплату
            /// </summary>
            /// <returns>Название таблицы BigQuery для счетов на оплату</returns>
            public static string GetInvoicesTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.InvoicesTableName");

            /// <summary>
            /// Получить название таблицы BigQuery для информационных баз
            /// </summary>
            /// <returns>Название таблицы BigQuery для информационных баз</returns>
            public static string GetDatabasesTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.DatabasesTableName");

            /// <summary>
            /// Получить задержку для перезапуска задачи
            /// на экспорт данных в BigQuery(в секундах)
            /// </summary>
            /// <returns>Задержка для перезапуска задачи
            /// на экспорт данных</returns>
            public static int GetRetryExportDataJobDelayInSeconds()
                => ConfigurationHelper.GetConfigurationValue<int>("BigQuery.RetryExportDataJobDelayInSeconds");


            /// <summary>
            /// Получить название таблицы BigQuery для платежей
            /// </summary>
            /// <returns>Название таблицы BigQuery для платежей</returns>
            public static string GetPaymentsTableName() =>
                ConfigurationHelper.GetConfigurationValue<string>("BigQuery.PaymentsTableName");
        }
    }
}