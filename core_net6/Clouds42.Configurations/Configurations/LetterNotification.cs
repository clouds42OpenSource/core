﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Уведомление письмом
        /// </summary>
        public static class LetterNotification
        {
            /// <summary>
            /// Получить описание отправителя
            /// </summary>
            /// <returns>Описание отправителя</returns>
            public static string GetSenderDescription()
                => ConfigurationHelper.GetConfigurationValue<string>("LetterNotification.SenderDescription");

            /// <summary>
            /// Получить дефолтное количество допустимых уведомлений
            /// в данном периоде
            /// </summary>
            /// <returns>Дефолтное количество допустимых уведомлений</returns>
            public static int GetDefaultNotificationsInPeriodCount() =>
                ConfigurationHelper.GetConfigurationValue<int>("LetterNotification.DefaultNotificationsInPeriodCount");

            /// <summary>
            /// Получить состояние активности писем
            /// </summary>
            /// <returns>Состояние активности писем</returns>
            public static bool GetLettersEnableState() =>
                ConfigurationHelper.GetConfigurationValue<bool>("LetterNotification.IsEnabledLetters");

            /// <summary>
            /// Получить признак, указывающий на необходимость отправлять письма
            /// </summary>
            /// <returns>Необходимо отправлять письма</returns>
            public static bool GetNeedSendLetters() =>
                ConfigurationHelper.GetConfigurationValue<bool>("LetterNotification.NeedSendLetters");

            /// <summary>
            /// Получить название категории тестового письма
            /// </summary>
            /// <returns>Название категории тестового письма</returns>
            public static string GetTestLetterCategoryName() =>
                ConfigurationHelper.GetConfigurationValue<string>("LetterNotification.TestLetterCategoryName");
        }
    }
}