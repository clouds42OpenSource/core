﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Кастомизация сервиса "Мои инф. базы"
        /// </summary>
        public static class MyDatabasesServiceCustomization
        {
            /// <summary>
            /// Кастомизация для казахской локали
            /// </summary>
            public static class CustomizationForKzLocale
            {
                /// <summary>
                /// Получить лимит на бесплатное создание баз
                /// </summary>
                /// <returns>Лимит на бесплатное создание баз</returns>
                public static int GetLimitOnFreeCreationDb()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "MyDatabasesServiceCustomization.CustomizationForKzlocale.LimitOnFreeCreationDb");

                /// <summary>
                /// Получить cтоимость создания базы свыше бесплатного лимита
                /// </summary>
                /// <returns>Стоимость создания базы свыше бесплатного лимита</returns>
                public static decimal GetCostOfCreatingDbOverFreeLimit()
                    => ConfigurationHelper.GetConfigurationValue<decimal>(
                        "MyDatabasesServiceCustomization.CustomizationForKzlocale.CostOfCreatingDbOverFreeLimit");
            }

            /// <summary>
            /// Кастомизация для казахской локали
            /// </summary>
            public static class CustomizationForUzLocale
            {
                /// <summary>
                /// Получить лимит на бесплатное создание баз
                /// </summary>
                /// <returns>Лимит на бесплатное создание баз</returns>
                public static int GetLimitOnFreeCreationDb()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "MyDatabasesServiceCustomization.CustomizationForUzLocale.LimitOnFreeCreationDb");

                /// <summary>
                /// Получить cтоимость создания базы свыше бесплатного лимита
                /// </summary>
                /// <returns>Стоимость создания базы свыше бесплатного лимита</returns>
                public static decimal GetCostOfCreatingDbOverFreeLimit()
                    => ConfigurationHelper.GetConfigurationValue<decimal>(
                        "MyDatabasesServiceCustomization.CustomizationForUzLocale.CostOfCreatingDbOverFreeLimit");
            }
        }
    }
}
