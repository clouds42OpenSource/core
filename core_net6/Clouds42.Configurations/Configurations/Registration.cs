﻿namespace Clouds42.Configurations.Configurations
{

    public static partial class CloudConfigurationProvider
    {

        public static class Registration
        {

            /// <summary>
            /// Получить имейл менеджера Кладовой.
            /// </summary>                
            public static string GetKladovoyManagerEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.KladovoyManagerEmail");

            /// <summary>
            /// Получить имейл менеджера Delans
            /// </summary>                
            public static string GetDelansManagerEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.DelansManagerEmail");

            /// <summary>
            /// Получить имейл ProfitAccount
            /// </summary>
            /// <returns></returns>
            public static string GetProfitAccountEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.ProfitAccountEmail");

            /// <summary>
            /// Получить имейл DmitriyYashchenko
            /// </summary>
            /// <returns></returns>
            public static string GetDmitriyYashchenkoEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.DmitriyYashchenkoEmail");

            /// <summary>
            /// Получить префикс для авто генерируемого пароля
            /// </summary>
            /// <returns>Префикс для авто генерируемого пароля</returns>
            public static string GetPasswordPrefix()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.PasswordPrefix");

            /// <summary>
            /// Получить имейл менеджера Абонцентра
            /// </summary>
            /// <returns></returns>
            public static string GetAbonCenterEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.AbonCenterEmail");

            /// <summary>
            /// Получить почту менеджера МЦОБ
            /// </summary>
            /// <returns>Почта менеджера МЦОБ</returns>
            public static string GetMcobManagerEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Registration.McobManagerEmail");
        }
    }

}
