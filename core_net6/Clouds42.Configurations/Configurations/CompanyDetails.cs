﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Реквизиты/контактные данные компании 42 облака
        /// </summary>
        public static class CompanyDetails
        {
            /// <summary>
            /// Получить ссылку на телеграм
            /// </summary>
            /// <returns>Ссылка на телеграм</returns>
            public static string GetTelegramLink()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.TelegramLink");

            /// <summary>
            /// Получить ссылку на инстаграм
            /// </summary>
            /// <returns>Ссылка на инстаграм</returns>
            public static string GetInstagramLink()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.InstagramLink");

            /// <summary>
            /// Получить ссылку на фейсбук
            /// </summary>
            /// <returns>Ссылка на фейсбук</returns>
            public static string GetFacebookLink()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.FacebookLink");

            /// <summary>
            /// Получить ссылку на сайт облака
            /// </summary>
            /// <returns>Ссылка на сайт облака</returns>
            public static string GetSiteLink()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.SiteLink");

            /// <summary>
            /// Получить ссылку на сайт облака для украинской локали
            /// </summary>
            /// <returns>Ссылка на сайт облака для украинской локали</returns>
            public static string GetSiteLinkUa()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.SiteLinkUa");

            /// <summary>
            /// Получить электронную почту
            /// </summary>
            /// <returns>Электронная почта</returns>
            public static string GetEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.Email");

            /// <summary>
            /// Получить электронную почту для украинской локали
            /// </summary>
            /// <returns>Электронная почта для украинской локали</returns>
            public static string GetEmailUa()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.EmailUa");

            /// <summary>
            /// Получить номер телефона для русской локали
            /// </summary>
            /// <returns>Номер телефона</returns>
            public static string GetPhoneRu()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.PhoneRu");

            /// <summary>
            /// Получить номер телефона для украинской локали
            /// </summary>
            /// <returns>Номер телефона</returns>
            public static string GetPhoneUa()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.PhoneUa");

            /// <summary>
            /// Получить номер телефона для казахской локали
            /// </summary>
            /// <returns>Номер телефона</returns>
            public static string GetPhoneKz()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.PhoneKz");

            public static string GetPhoneUz()
                => ConfigurationHelper.GetConfigurationValue<string>("CompanyDetails.PhoneUz");
        }
    }
    
}
