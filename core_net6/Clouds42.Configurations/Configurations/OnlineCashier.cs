﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        ///     Параметры онлайн кассы
        /// </summary>
        public static class OnlineCashier
        {
            public static class ChekOnline
            {
                /// <summary>
                ///     Получить ID воркера для выполнения задачи 
                /// </summary>
                public static int GetCoreWorkerId()
                    => ConfigurationHelper.GetConfigurationValue<int>("OnlineCashier.ChekOnline.CoreWorkerId");

                /// <summary>
                ///     Получить URL к сервису 
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetUrlApi(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("OnlineCashier.ChekOnline.UrlApi", supplierId);

                /// <summary>
                ///     Получить данные сертификата 
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetCertificateData(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("OnlineCashier.ChekOnline.CertificateData", supplierId);

                /// <summary>
                ///     Получить пароль сертификата 
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetCertificatePassword(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("OnlineCashier.ChekOnline.CertificatePassword", supplierId);

                /// <summary>
                ///     Получить код налога 
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static int GetTaxCode(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<int>("OnlineCashier.ChekOnline.TaxCode", supplierId);
            }
        }
    }
}