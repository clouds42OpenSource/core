﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Информационная база на разделителях
        /// </summary>
        public static class DbTemplateDelimiters
        {

            /// <summary>
            /// Получить адрес метода который удаляет области в базе.
            /// Например: https://beta-web.sauri.io/saurims-int/.
            /// </summary>
            /// <returns></returns>
            public static string GetUrlForApplications()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.GetUrlForApplications");

            /// <summary>
            /// Получить адрес метода для получения полного пути к бэкапу по ID от МС
            /// </summary>
            /// <returns>Адрес метода для получения полного пути к бэкапу по ID от МС</returns>
            public static string GetUrlForGetBackupFullPathBySmBackupId()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForGetBackupFullPathBySmBackupId");

            /// <summary>
            /// Получить адрес метода для получения полного пути файла DT
            /// </summary>
            /// <returns>Адрес метода для получения полного пути файла DT</returns>
            public static string GetUrlForFullPathOnDt()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForFullPathOnDt");

            /// <summary>
            /// Получить URL для загрузки ZIP файлов
            /// </summary>
            /// <returns>URL для загрузки ZIP файлов</returns>
            public static string GetUrlForUploadZipFiles()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.UrlForUploadZipFiles");

            /// <summary>
            /// Получить признак необходимости использовать новый механизм загрузки ZIP файлов
            /// </summary>
            /// <returns>Признак необходимости использовать новый механизм загрузки ZIP файлов</returns>
            public static bool GetNeedUseNewMechanismForUploadFile()
                => ConfigurationHelper.GetConfigurationValue<bool>(
                    "DbTemplateDelimiters.NeedUseNewMechanismForUploadFile");

            /// <summary>
            /// Получить адрес метода который управляет правами пользователей.
            /// Например: https://beta-web.sauri.io/saurims-int/.
            /// </summary>
            /// <returns></returns>
            public static string GetUrlForAccessToDb()
                => ConfigurationHelper.GetConfigurationValue<string>(
                    "AccessToDbOnDelimeterCommand.GetUrlForAccessToDb");

            public static string GetUrlForProfilesToDb()
                 => ConfigurationHelper.GetConfigurationValue<string>(
                    "AccessToDbOnDelimeterCommand.GetUrlForProfilesToDb");

            /// <summary>
            /// Получить Id служебного аккаунта для
            /// демо баз на разделителях
            /// </summary>
            /// <returns>Id служебного аккаунта</returns>
            public static Guid GetServiceAccountIdForDemoDelimiterDatabases() =>
                ConfigurationHelper.GetConfigurationValue<Guid>(
                    "DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases");

            /// <summary>
            /// Получить адрес метода для управления состоянием блокировки пользователя
            /// </summary>
            /// <returns>Адрес метода для управления состоянием блокировки пользователя</returns>
            public static string GetUrlForChangeAccountUserLockState()
                => ConfigurationHelper.GetConfigurationValue("OpenId.Sauri.Url.ControlUser");


            /// <summary>
            /// Получить логин необходимый для аутинфикации в 1С при регистрации зоны.
            /// </summary>
            /// <returns></returns>
            public static string GetLoginForRegistrationZone()
                => ConfigurationHelper.GetConfigurationValue<string>("DbTemplateDelimiters.LoginForRegistrationZone");

            /// <summary>
            /// Получить пароль необходимый для аутинфикации в 1С при регистрации зоны.
            /// </summary>
            /// <returns></returns>
            public static string GetPasswordForRegistrationZone()
                => ConfigurationHelper.GetConfigurationValue<string>(
                    "DbTemplateDelimiters.PasswordForRegistrationZone");


            /// <summary>
            /// Получить адрес метода для действий по пользователям в МС
            /// </summary>
            /// <returns>Адрес метода для действий по пользователям в МС</returns>
            public static string GetUrlForActivateUser()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForActivateUser");

            /// <summary>
            /// Получить адрес метода для действий с расширениями сервиса в информационную базу
            /// </summary>
            /// <returns>Адрес метода для действий с расширениями сервиса в информационную базу</returns>
            public static string GetUrlForServiceExtensionDatabase()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForServiceExtensionDatabase");

            /// <summary>
            /// Получить адрес метода для получения списка конфигураций информационных баз
            /// </summary>
            /// <returns>Адрес метода дляполучения списка конфигураций информационных баз</returns>
            public static string GetUrlForConfigurationDatabase()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForForConfigurationDatabase");

            /// <summary>
            /// Получить адрес метода для получения списка конфигураций информационных баз
            /// </summary>
            /// <returns>Адрес метода дляполучения списка конфигураций информационных баз</returns>
            public static string GetUrlForServiceExtensionDatabaseStatus()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlForForServiceExtensionDatabaseStatus");

            /// <summary>
            /// Получить адрес метода для получения информации о сервисе
            /// </summary>
            /// <returns>Адрес метода для получения информации о сервисе</returns>
            public static string GetUrlInformationsFromService()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlGetInformationsFromService");

            /// <summary>
            /// Получить адрес метода для получения списка информационных баз для аккаунта
            /// </summary>
            /// <returns>Адрес метода для получения списка информационных баз для аккаунта</returns>
            public static string GetUrlServiceExtensionForAccount()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlServiceExtensionForAccount");

            /// <summary>
            /// Получить адрес метода для отключения сервиса в маркете
            /// </summary>
            /// <returns>Адрес метода для отключения сервиса в маркете</returns>
            public static string GetUrlDisableServiceInMarket()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlDisableServiceInMarket");
            
            /// <summary>
            /// Получить адрес метода для получения списка сервисов доступных для установки в информационную базу
            /// </summary>
            /// <returns>Адрес метода дляполучения списка сервисов доступных для установки в информационную базу</returns>
            public static string GetUrlServicesExtensionForDatabase()
                => ConfigurationHelper.GetConfigurationValue(
                    "DbTemplateDelimiters.UrlServicesExtensionForDatabase");

            /// <summary>
            /// Получить название параметра для сессий загрузки ZIP файлов
            /// </summary>
            /// <returns>Название параметра для сессий загрузки ZIP файлов</returns>
            public static string GetIbSessionParameterName()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.IbSessionParameterName");

            /// <summary>
            /// Получить название параметра для получения URL для загрузки
            /// </summary>
            /// <returns>Название параметра для получения URL для загрузки</returns>
            public static string GetResponseParameterNameForUploadUrl()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.ResponseParameterNameForUploadUrl");

            /// <summary>
            /// Получить название параметра для получения URL для dt файла
            /// </summary>
            /// <returns>Название параметра для получения URL для dt файла</returns>
            public static string GetResponseParameterNameForDtFilel()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.ResponseParameterNameForDtFile");

            /// <summary>
            /// Получить название параметра токена для заголовка запроса
            /// </summary>
            /// <returns>Название параметра токена для заголовка запроса</returns>
            public static string GetTokenHeaderParameterName()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.TokenHeaderParameterName");

            /// <summary>
            /// Получить значение параметра для запуска сессии загрузки файлов
            /// </summary>
            /// <returns>Значение параметра для запуска сессии загрузки файлов</returns>
            public static string GetIbSessionStartParameterValue()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.IbSessionStartParameterValue");

            /// <summary>
            /// Получить значение таймаута для загрузки части файла в МС
            /// </summary>
            /// <returns>Значение таймаута для загрузки части файла в МС</returns>
            public static int GetUploadChunkFileBytesRequestTimeout()
                => ConfigurationHelper.GetConfigurationValue<int>(
                    "DbTemplateDelimiters.UploadChunkFileBytesRequestTimeout");

            /// <summary>
            /// Получить адрес метода который завершает сессии в базе.
            /// </summary>
            /// <returns></returns>
            public static string GetUrlForCloseSession()
                => ConfigurationHelper.GetConfigurationValue("DbTemplateDelimiters.GetUrlForCloseSession");

        }
    }
}
