﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Html шаблон
        /// </summary>
        public static class HtmlTemplate
        {
            /// <summary>
            /// Получить Id шаблона Html для рекламного баннера
            /// </summary>
            public static int GetAdvertisingBannerTemplateId()
                => ConfigurationHelper.GetConfigurationValue<int>("HtmlTemplate.AdvertisingBannerTemplateId");
        }
    }
}
