﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Параметры для AuthServer-а
        /// </summary>
        public static class AuthServer
        {
            /// <summary>
            /// URL для аутентификации
            /// </summary>
            public static string GetAuthServerAuthenticationUrl() =>
                ConfigurationHelper.GetConfigurationValue<string>("AuthServer.Authentication.Url");

            /// <summary>
            /// Базовый URL к Auth серверу
            /// </summary>            
            public static string GetBaseUrl() => ConfigurationHelper.GetConfigurationValue("AuthServer.BaseUrl");

            /// <summary>
            /// Путь к Authenticate эндпоинту AuthServer-а
            /// </summary>            
            public static string GetAuthenticatePath() =>
                ConfigurationHelper.GetConfigurationValue("AuthServer.Paths.Authenticate");

            /// <summary>
            /// Путь к RefreshToken эндпоинту AuthServer-а
            /// </summary>            
            public static string GetRefreshPath() =>
                ConfigurationHelper.GetConfigurationValue("AuthServer.Paths.Refresh");

            /// <summary>
            /// Путь к GetUserToken эндпоинту AuthServer-а
            /// </summary>            
            public static string GetUserTokenPath() =>
                ConfigurationHelper.GetConfigurationValue("AuthServer.Paths.UserToken");
        }
    }
}
