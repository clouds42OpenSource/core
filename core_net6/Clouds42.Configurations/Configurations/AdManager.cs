﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Active Directory
        /// </summary>
        public static class AdManager
        {
            /// <summary>
            /// Режим работы контекста контроллера домена
            /// </summary>
            public static class Mode
            {
                /// <summary>
                /// Получить признак работы контекста AD в режиме разработки
                /// </summary>
                /// <returns>Признак работы контекста AD в режиме разработки</returns>
                public static bool GetIsDevelopMode()
                    => ConfigurationHelper.GetConfigurationValue<bool>("AdManager.Mode.IsDevelopMode");

                /// <summary>
                /// Получить логин авторизации пользователя в АД.
                /// </summary>                
                public static string GetDevelopModeAdUserName()
                    => ConfigurationHelper.GetConfigurationValue<string>("AdManager.Mode.DevelopModeAdUserName");

                /// <summary>
                /// Получить пароль авторизации пользователя в АД.
                /// </summary>                
                public static string GetDevelopModeAdUserPassword()
                    => ConfigurationHelper.GetConfigurationValue<string>("AdManager.Mode.DevelopModeAdUserPassword");
            }

            /// <summary>
            /// параметры подключения к АД
            /// </summary>
            public static class AdServer
            {
                /// <summary>
                /// Получить имя сервера для АД.
                /// </summary>
                public static string GetAdServer()
                    => ConfigurationHelper.GetConfigurationValue(nameof(AdServer));

                /// <summary>
                /// Получить параметры для контейнера для АД.
                /// </summary>                
                public static string GetAdContainer()
                    => ConfigurationHelper.GetConfigurationValue("AdContainer");

                /// <summary>
                /// Получить параметры для root контейнера для АД.
                /// </summary>                
                public static string GetAdContainerRoot()
                    =>  ConfigurationHelper.GetConfigurationValue("AdContainerRoot");

                /// <summary>
                /// Получить параметры для контейнера компании для АД.
                /// </summary>                
                public static string GetAdContainerCompanies()
                    => ConfigurationHelper.GetConfigurationValue("AdContainerCompanies");
            }
        }
    }
}