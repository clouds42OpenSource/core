﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Счета
        /// </summary>
        public static class Invoices
        {
            /// <summary>
            /// Дефолтный период для выставления счета
            /// </summary>
            /// <returns></returns>
            public static int DefaultPeriodForInvoice() =>
                ConfigurationHelper.GetConfigurationValue<int>("Invoices.DefaultPeriodForInvoice");

            /// <summary>
            /// Префикс счета
            /// </summary>
            /// <returns>Префикс счета</returns>
            public static string UniqPrefix() => ConfigurationHelper.GetConfigurationValue<string>("Invoices.UniqPrefix");

            /// <summary>
            /// Получить название прикрепляемого файла для счета на оплату
            /// </summary>
            /// <returns>Название прикрепляемого файла для счета на оплату</returns>
            public static string GetInvoiceAttachFileName()
                => ConfigurationHelper.GetConfigurationValue<string>("Invoices.InvoiceAttachFileName");

            /// <summary>
            /// Получить ставку бонусного вознаграждения для периода оплаты в 3 месяца
            /// </summary>
            /// <returns>Ставка бонусного вознаграждения для периода оплаты в 3 месяца</returns>
            public static int GetBonusRewardFor3MonthPayPeriod()
                => ConfigurationHelper.GetConfigurationValue<int>("Invoices.BonusRewardFor3MonthPayPeriod");

            /// <summary>
            /// Получить ставку бонусного вознаграждения для периода оплаты в 6 месяцев
            /// </summary>
            /// <returns>Ставка бонусного вознаграждения для периода оплаты в 6 месяцев</returns>
            public static int GetBonusRewardFor6MonthPayPeriod()
                => ConfigurationHelper.GetConfigurationValue<int>("Invoices.BonusRewardFor6MonthPayPeriod");

            /// <summary>
            /// Получить ставку бонусного вознаграждения для периода оплаты в 12 месяцев
            /// </summary>
            /// <returns>Ставка бонусного вознаграждения для периода оплаты в 12 месяцев</returns>
            public static int GetBonusRewardFor12MonthPayPeriod()
                => ConfigurationHelper.GetConfigurationValue<int>("Invoices.BonusRewardFor12MonthPayPeriod");
        }
    }
}