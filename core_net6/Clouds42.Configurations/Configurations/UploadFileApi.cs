﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// API для загрузки файлов
        /// </summary>
        public static class UploadFileApi
        {
            /// <summary>
            /// Получить имя заголовка для ID загруженного файла
            /// </summary>
            /// <returns></returns>
            public static string GetHeaderNameForUploadedFileId()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.HeaderNameForUploadedFileId");

            /// <summary>
            /// Получить маршрут для загрузки части файла
            /// </summary>
            /// <returns>Маршрут для загрузки части файла</returns>
            public static string GetRouteValueForUploadChunkOfFile()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.RouteValueForUploadChunkOfFile");

            /// <summary>
            /// Получить маршрут для проверки валидности загруженного файла
            /// </summary>
            /// <returns>Маршрут для проверки валидности загруженного файла</returns>
            public static string GetRouteValueForCheckLoadFileValidity()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.RouteValueForCheckLoadFileValidity");

            /// <summary>
            /// Получить маршрут для инициализации процесса загрузки файла
            /// </summary>
            /// <returns>Маршрут для инициализации процесса загрузки файла</returns>
            public static string GetRouteValueForInitUploadProcess()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.RouteValueForInitUploadProcess");

            /// <summary>
            /// Получить маршрут для получения списка пользователей в загруженной базе данных.
            /// </summary>
            /// <returns>Маршрут для получения списка пользователей в загруженной базе данных.</returns>
            public static string GetDatabaseInfoFromZip()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.GetDatabaseInfoFromZip");

            /// <summary>
            /// Получить маршрут для валидации содержимого zip файла базы.
            /// </summary>
            /// <returns>Маршрут для валидации содержимого zip файла базы.</returns>
            public static string GetRouteValidateConfigVersionInZip()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.ValidateConfigVersionInZip");

            /// <summary>
            /// Получить маршрут для склеивания частей файла.
            /// </summary>
            /// <returns>Маршрут для склеивания частей файла.</returns>
            public static string GetRouteForMergeChunksToInitialFile()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.RouteForMergeChunksToInitialFile");

            /// <summary>
            /// Получить маршрут для проверки статуса загрузки файла.
            /// </summary>
            /// <returns>Маршрут для проверки статуса загрузки файла.</returns>
            public static string GetRouteForCheckFileUploadStatus()
                => ConfigurationHelper.GetConfigurationValue<string>("UploadFileApi.RouteForCheckFileUploadStatus");

        }
    }
}
