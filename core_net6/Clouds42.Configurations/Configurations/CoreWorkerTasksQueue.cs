﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Запущенная задача
        /// </summary>
        public static class CoreWorkerTasksQueue
        {
            /// <summary>
            /// Получить таймаут между захватом новой задачи
            /// </summary>
            /// <returns>Таймаут между захватом новой задачи</returns>
            public static int GetTimeoutInSecondsBetweenCaptureNewTask()
                => ConfigurationHelper.GetConfigurationValue<int>("CoreWorkerTasksQueue.TimeoutInSecondsBetweenCaptureNewTask");
        }
    }
}
