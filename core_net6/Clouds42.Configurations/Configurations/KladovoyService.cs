﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации сервиса Kladovoy
        /// </summary>
        public static class KladovoyService
        {
            /// <summary>
            /// Получить название сайта для сервиса кладовой
            /// </summary>
            /// <returns>Название сайта для сервиса кладовой</returns>
            public static string GetKladovoySiteName()
                => ConfigurationHelper.GetConfigurationValue<string>("KladovoyService.KladovoySiteName");

            /// <summary>
            /// Получить урл сайта для сервиса кладовой
            /// </summary>
            /// <returns>Урл сайта для сервиса кладовой</returns>
            public static string GetKladovoySiteUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("KladovoyService.KladovoySiteUrl");
        }
    }
}
