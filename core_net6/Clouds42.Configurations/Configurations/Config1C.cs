﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Настройки 1С
        /// </summary>
        public static class Enterprise1C
        {
            /// <summary>
            /// Получить полный путь до платформы 1С 8.2
            /// </summary>
            public static string Get1C82ExecutionPath()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.1C82ExecutionPath");

            /// <summary>
            /// Получить путь до хранилаща временных файлов.
            /// </summary>
            /// <returns></returns>
            public static string GetTemporaryStoragePath()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.TemporaryStoragePath");

            /// <summary>
            /// Получить dns хранилаща временных файлов.
            /// </summary>
            /// <returns></returns>
            public static string GetDnsTemporaryStorage()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.DnsTemporaryStorage");

            /// <summary>
            /// Получить путь к папке для сохранения логов
            /// </summary>
            /// <returns></returns>
            public static string GetPathToLogFolder1C()
                => ConfigurationHelper.GetConfigurationValue<string>("LogFolder1C");

            /// <summary>
            /// Получить путь к хранилищу пакетов обновлений.
            /// </summary>
            /// <returns>Путь к хранилищу пакетов обновлений</returns>
            public static string GetCfuStoragePath()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.CfuStoragePath");

            /// <summary>
            /// Получить название библиотеки COM объектов от 1С
            /// </summary>
            /// <returns>Название библиотеки COM объектов от 1С</returns>
            public static string Get1CComLibraryName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.1CComLibraryName");

            /// <summary>
            /// Получить название процесса для регистрации COM объектов
            /// </summary>
            /// <returns>Название процесса для регистрации COM объектов</returns>
            public static string GetRegSvrProcessName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegSvrProcessName");

            /// <summary>
            /// Получить стандартное название COM коннектора 8.3
            /// </summary>
            /// <returns>Стандартное название COM коннектора 8.3</returns>
            public static string GetDefaultProgramIdForV83ComConnector()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.DefaultProgramIdForV83ComConnector");

            /// <summary>
            /// Получить стандартное название COM коннектора 8.2
            /// </summary>
            /// <returns>Стандартное название COM коннектора 8.2</returns>
            public static string GetDefaultProgramIdForV82ComConnector()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.DefaultProgramIdForV82ComConnector");

            /// <summary>
            /// Получить корневой каталог с информацией об обновлениях.
            /// Например: http://downloads.1c.ru/ipp/ITSREPV/V8Update/Configs/.
            /// </summary>
            /// <returns></returns>
            public static string GetCfuDownloadRootCatalogUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.CfuDownloadRootCatalogUrl");

            /// <summary>
            /// Получить название раздела Wow6432Node в реестре
            /// </summary>
            /// <returns>Название раздела Wow6432Node в реестре</returns>
            public static string GetRegistryWow6432NodeName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistryWow6432NodeName");

            /// <summary>
            /// Получить название раздела AppID в реестре
            /// </summary>
            /// <returns>Название раздела AppID в реестре</returns>
            public static string GetRegistryAppIdNodeName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistryAppIdNodeName");

            /// <summary>
            /// Получить название ключа DllSurrogate в реестре
            /// </summary>
            /// <returns>Название ключа DllSurrogate в реестре</returns>
            public static string GetRegistryDllSurrogateKeyName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistryDllSurrogateKeyName");

            /// <summary>
            /// Получить название раздела CLSID в реестре
            /// </summary>
            /// <returns>Название раздела CLSID в реестре</returns>
            public static string GetRegistryClsIdNodeName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistryClsIdNodeName");

            /// <summary>
            /// Получить название раздела Software в реестре
            /// </summary>
            /// <returns>Название раздела Software в реестре</returns>
            public static string GetRegistrySoftwareNodeName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistrySoftwareNodeName");

            /// <summary>
            /// Получить название раздела Classes в реестре
            /// </summary>
            /// <returns>Название раздела Classes в реестре</returns>
            public static string GetRegistryClassesNodeName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.RegistryClassesNodeName");

            /// <summary>
            /// Получить название процесса DllHost
            /// </summary>
            /// <returns>Название процесса DllHost</returns>
            public static string GetDllHostProcessName()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.DllHostProcessName");

            /// <summary>
            /// Получить таймаут операции по восстановлению базы из ДТ файла
            /// </summary>
            /// <returns>Таймаут операции по восстановлению базы из ДТ файла</returns>
            public static int GetRestoreIbFromDtCommandTimeout()
                => ConfigurationHelper.GetConfigurationValue<int>("_1C.RestoreIBFromDtCommandTimeout");

            /// <summary>
            /// Получить строковое значение времени запуска AO для ифн. баз
            /// </summary>
            /// <returns>Строковое значение времени запуска AO для ифн. баз</returns>
            public static string GetAutoUpdateStartDateTimeStringValue()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.AutoUpdateStartDateTimeStringValue");

            /// <summary>
            /// Получить строковое значение времени запуска ТиИ для ифн. баз
            /// </summary>
            /// <returns>Строковое значение времени запуска ТиИ для ифн. баз</returns>
            public static string GetTehSupportStartDateTimeStringValue()
                => ConfigurationHelper.GetConfigurationValue<string>("_1C.TehSupportStartDateTimeStringValue");

            /// <summary>
            /// Клиент RAS
            /// </summary>
            public static class RasClient
            {
                /// <summary>
                /// Признак необходимости использовать RAS
                /// </summary>
                /// <returns>Признак необходимости использовать RAS</returns>
                public static bool NeedUseRas()
                    => ConfigurationHelper.GetConfigurationValue<bool>("_1C.RasClient.NeedUseRas");

                /// <summary>
                /// Получить стандартный порт для ras
                /// </summary>
                /// <returns>Стандартный порт для ras</returns>
                public static string GetDefaultRasWorkingPort()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.DefaultRasWorkingPort");

                /// <summary>
                /// Получить стандартный порт сервера 1С
                /// </summary>
                /// <returns>Стандартный порт сервера 1С</returns>
                public static string GetDefaultServer1CPort()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.DefaultServer1CPort");

                /// <summary>
                /// Получить путь к приложению rac.exe
                /// </summary>
                /// <returns>Путь к приложению rac.exe</returns>
                public static string GetRacApplicationPath()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.RacApplicationPath");

                /// <summary>
                /// Получить путь к приложению ras.exe
                /// </summary>
                /// <returns>Путь к приложению ras.exe</returns>
                public static string GetRasApplicationPath()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.RasApplicationPath");

                /// <summary>
                /// Получить команду для сохранения текущего процесса ras.exe в переменную
                /// </summary>
                /// <returns>Команда для сохранения текущего процесса ras.exe в переменную</returns>
                public static string GetCommandForFetchRasProcess()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.CommandForFetchRasProcess");

                /// <summary>
                /// Получить команду для завершения текущего процесса ras.exe
                /// </summary>
                /// <returns>Команда для завершения текущего процесса ras.exe</returns>
                public static string GetStopRasProcessCommand()
                    => ConfigurationHelper.GetConfigurationValue("_1C.RasClient.StopRasProcessCommand");
            }
        }
    }    
}
