﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Платеж агрегатора ЮKassa
        /// </summary>
        public static class YookassaAggregatorPayment
        {

            /// <summary>
            /// Получить допустимое количество дней для
            /// обработки платежа агрегатором ЮKassa
            /// </summary>
            /// <returns>Допустимое количество дней для
            /// обработки платежа агрегатором ЮKassa</returns>
            public static double GetAllowedPendingPaymentsDaysCount()
                => ConfigurationHelper.GetConfigurationValue<double>("YookassaAgregatorPayment.AllowedPendingPaymentsDaysCount");

            /// <summary>
            /// Получить значение флага - Автоматический прием поступившего платежа
            /// </summary>
            /// <returns>Автоматический прием поступившего платежа</returns>
            public static bool IsCapture()
                => ConfigurationHelper.GetConfigurationValue<bool>("YookassaAgregatorPayment.IsCapture");

            /// <summary>
            /// Получить признак необходимости сохранять платежные данные
            /// </summary>
            /// <returns>Необходимость сохранять платежные данные</returns>
            public static int NeedSavePaymentMethod()
                => ConfigurationHelper.GetConfigurationValue<int>("YookassaAgregatorPayment.NeedSavePaymentMethod");

            /// <summary>
            /// Получить урл для работы с платежами ЮKassa
            /// </summary>
            /// <returns>Урл для работы с платежами ЮKassa</returns>
            public static string GetUrlForWorkingWithPayments()
                => ConfigurationHelper.GetConfigurationValue<string>("YookassaAgregatorPayment.UrlForWorkingWithPayments");

            /// <summary>
            /// Получить Id магазина в ЮKassa
            /// </summary>
            /// <param name="supplierId">Id поставщика</param>
            /// <returns>Id магазина в ЮKassa</returns>
            public static string GetStoreId(Guid supplierId)
                => ConfigurationHelper.GetConfigurationValue<string>("YookassaAgregatorPayment.StoreId", supplierId);

            /// <summary>
            /// Получить секрестный ключ
            /// </summary>
            /// <param name="supplierId">Id поставщика</param>
            /// <returns>Секрестный ключ</returns>
            public static string GetSecretKey(Guid supplierId)
                => ConfigurationHelper.GetConfigurationValue<string>("YookassaAgregatorPayment.SecretKey", supplierId);

            /// <summary>
            /// Получить URL для редиректа юкассой после платежа
            /// </summary>
            /// <returns></returns>
            public static string ReturnUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("YookassaAgregatorPayment.ReturnUrl");
        }
    }
    
}
