﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации агента
        /// </summary>
        public static class AgentConfigurations
        {
            /// <summary>
            /// Путь до файлового хранилища агентов
            /// </summary>
            /// <returns></returns>
            public static string PathToFileStorageAgents()
                => ConfigurationHelper.GetConfigurationValue<string>("AgentConfigurations.PathToFileStorageAgents");
            
            /// <summary>
            /// % ставка агента по привлеченным клиентам.
            /// </summary>
            /// <returns></returns>
            public static int GetAgentBonusPresents()
                => ConfigurationHelper.GetConfigurationValue<int>("AgentConfigurations.AgentBonusPresents");

            /// <summary>
            /// % ставка налога для агента с типом юр.лицо
            /// </summary>
            /// <returns></returns>
            public static int GetTaxRateForLegalPerson()
                => ConfigurationHelper.GetConfigurationValue<int>("AgentConfigurations.LegalPersonTaxRate");

            /// <summary>
            /// % ставка налога для агента с типом физ.лицо
            /// </summary>
            /// <returns></returns>
            public static int GetTaxRateForPhysicalPerson()
                => ConfigurationHelper.GetConfigurationValue<int>("AgentConfigurations.PhysicalPersonTaxRate");

            /// <summary>
            /// % ставка налога для агента с типом ИП
            /// </summary>
            /// <returns></returns>
            public static int GetTaxRateForSoleProprietor()
                => ConfigurationHelper.GetConfigurationValue<int>("AgentConfigurations.SoleProprietorTaxRate");
        }
    }
}
