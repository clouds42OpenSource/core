﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Сервис рассылок SendGrid
        /// </summary>
        public static class SendGrid
        {
            /// <summary>
            /// Получить ключ АПИ для SendGrid
            /// </summary>
            /// <returns>Ключ АПИ для SendGrid</returns>
            public static string GetApiKey()
                => ConfigurationHelper.GetConfigurationValue<string>("SendGrid.ApiKey");

            /// <summary>
            /// Получить путь к приложению обработчика
            /// </summary>
            /// <returns>Путь к приложению обработчика</returns>
            public static string GetWrapperAppPath()
                => ConfigurationHelper.GetConfigurationValue("SendGrid.WrapperAppPath");

            /// <summary>
            /// Получить название агента SendGrid
            /// </summary>
            /// <returns>Название агента SendGrid</returns>
            public static string GetAgentName()
                => ConfigurationHelper.GetConfigurationValue("SendGrid.AgentName");

            /// <summary>
            /// Получить уникальный идентификатор агента SendGrid
            /// </summary>
            /// <returns>Уникальный идентификатор агента SendGrid</returns>
            public static Guid GetAgentUniqueId()
                => ConfigurationHelper.GetConfigurationValue<Guid>("SendGrid.AgentUniqueId");

            public static string GetDefaultTemplateId()
                => ConfigurationHelper.GetConfigurationValue("SendGrid.DefaultTemplateId");

        }
    }
}
