﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Json токен пользователя
        /// </summary>
        public static class AccountUserJsonWebToken
        {
            /// <summary>
            /// Получить срок(период) хранения Json
            /// токена пользователя в минутах
            /// </summary>
            /// <returns>Срок(период) хранения Json
            /// токена в минутах</returns>
            public static int GetTokensStoragePeriodInMinutes()
                => ConfigurationHelper.GetConfigurationValue<int>("AccountUserJsonWebToken.TokensStoragePeriodInMinutes");
        }
    }
    
}
