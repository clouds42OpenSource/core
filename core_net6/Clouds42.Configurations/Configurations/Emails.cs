﻿namespace Clouds42.Configurations.Configurations
{    
    public static partial class CloudConfigurationProvider
    {
        public static class Phones
        {
            public static string GetMinutesToExpireCode() => ConfigurationHelper.GetConfigurationValue("Phones.VerificationCodeExpireMinutes");
            public static string GetMinutesToResendCode() => ConfigurationHelper.GetConfigurationValue("Phones.VerificationCodeResendMinutes");
            public static string GetVerificationCodeMaxSendCountInDay() => ConfigurationHelper.GetConfigurationValue("Phones.VerificationCodeMaxSendCountInDay");
        }
        public static class Emails
        {

            /// <summary>
            /// Получить имейл менедра 42 облаков.
            /// </summary>                
            public static string Get42CloudsManagerEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Emails.42CloudsManagerEmail");

            /// <summary>
            /// Получить имейл менедра EFSOL Dostavka.
            /// </summary>                
            public static string GetEfsolDostavkaEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Emails.EfsolDostavkaEmail");

            /// <summary>
            /// Получить имейл EFSOL Sales.
            /// </summary>                
            public static string GetEfsolSalesEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("Emails.EfsolSalesEmail");
            
            /// <summary>
            /// Получить почту Хотлайна.
            /// </summary>                
            public static string GetHotlineEmail()
                => ConfigurationHelper.GetConfigurationValue<string>("HotlineEmail");

            /// <summary>
            /// Получить почту Деланс инфо
            /// </summary>
            public static string GetDelansInfoEmail() =>
                ConfigurationHelper.GetConfigurationValue<string>("Emails.DelansInfoEmail");

            /// <summary>
            /// Получить почту технической поддержки Vip аккаунтов
            /// </summary>
            /// <returns>Почта технической поддержки Vip аккаунтов</returns>
            public static string GetVipSupportEmail() =>
                ConfigurationHelper.GetConfigurationValue<string>("Emails.VipSupportEmail");

            /// <summary>
            /// Получить почту облачных сервисов
            /// </summary>
            /// <returns>Почта облачных сервисов</returns>
            public static string GetCloudServicesEmail()
                => ConfigurationHelper.GetConfigurationValue("cloud-services");

            /// <summary>
            /// Получить почту для сбора исключений
            /// </summary>
            /// <returns>Почта для сбора исключений</returns>
            public static string GetCoreExceptionEmail()
                => ConfigurationHelper.GetConfigurationValue("GlobalExceptionHandler.CoreExceptionMail");

            public static string GetMinutesToExpireCode() => ConfigurationHelper.GetConfigurationValue("Emails.VerificationCodeExpireMinutes");
            public static string GetMinutesToResendCode() => ConfigurationHelper.GetConfigurationValue("Emails.VerificationCodeResendMinutes");

            public static string GetVerificationCodeMaxSendCountInDay() => ConfigurationHelper.GetConfigurationValue("Emails.VerificationCodeMaxSendCountInDay");
            public static string GetEmailVerificationLink() => ConfigurationHelper.GetConfigurationValue("Emails.VerificationLink");

            public static string GetEmailConfirmDeleteAccountLink() => ConfigurationHelper.GetConfigurationValue("Emails.ConfirmDeleteAccountLink");
            
        }
    }
}
