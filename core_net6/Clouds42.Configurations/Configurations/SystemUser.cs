﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Обще системные настройки.
        /// </summary>
        public static class Common
        {

            /// <summary>
            /// Получить токен безопасности ядра.
            /// </summary>
            /// <returns></returns>
            public static Guid GetCoreSecurityToken()
                => ConfigurationHelper.GetConfigurationValue<Guid>("Common.CoreSecurityToken");

            /// <summary>
            /// Системный пользователь
            /// </summary>
            public static class SystemUser
            {
                /// <summary>
                /// Получить идентификатор системного пользователя
                /// </summary>
                /// <returns></returns>
                public static Guid GetSystemUserIdentifire()
                    => ConfigurationHelper.GetConfigurationValue<Guid>("SystemUser.SystemUserIdentifire");
            }

            /// <summary>
            /// Системный аккаунт
            /// </summary>
            public static class SystemAccount
            {
                /// <summary>
                /// Получить идентификатор системного аккаунта
                /// </summary>
                /// <returns>Идентификатор системного аккаунта</returns>
                public static Guid GetSystemAccountId()
                    => ConfigurationHelper.GetConfigurationValue<Guid>("SystemAccount.SystemAccountId");
            }
        }
    }
}