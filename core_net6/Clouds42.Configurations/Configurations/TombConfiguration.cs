﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        ///     Склеп
        /// </summary>
        public static class Tomb
        {
            /// <summary>
            ///     Получить ID воркера 
            /// </summary>
            public static short GetCoreWorkerId()
                => ConfigurationHelper.GetConfigurationValue<short>("GoogleApi.Drive.Tomb.CoreWorkerId");

            /// <summary>
            ///     Получить секретный ключ 
            /// </summary>
            public static string GetSecretKey()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.SecretKey");

            /// <summary>
            ///     Получить наименование проекта 
            /// </summary>
            public static string GetApplicationName()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.ApplicationName");

            /// <summary>
            ///     Получить токен данные 
            /// </summary>
            public static string GetAuthTokenData()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.AuthTokenData");

            /// <summary>
            ///     Получить шаблон URL файла 
            /// </summary>
            public static string GetShareLink()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.ShareLink");

            /// <summary>
            ///     Получить шаблон URL папки 
            /// </summary>
            public static string GetShareLinkFolder()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.ShareLinkFolder");
            
            /// <summary>
            ///     Получить ID первичного каталога 
            /// </summary>
            public static string GetGlobalFolder()
                => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Drive.Tomb.GlobalFolder");

            public static string GetBucketName() =>
                ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Cloud.BucketName");

            public static string GetCloudLink() => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Cloud.Link");

            public static string GetCredentialsPath() => ConfigurationHelper.GetConfigurationValue<string>("GoogleApi.Cloud.CredentialsPath");
        }
    }
}
