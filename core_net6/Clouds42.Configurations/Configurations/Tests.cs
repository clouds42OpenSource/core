﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Ключи для тестов
        /// </summary>
        public static class Tests
        {
            /// <summary>
            /// IP адрес для подключения к сервису публикации
            /// </summary>
            /// <returns></returns>
            public static string GetCloudServicesContentServer()
                => ConfigurationHelper.GetConfigurationValue<string>("Tests.CloudServicesContentServer.ConnectionAddress");
        }
    }
}
