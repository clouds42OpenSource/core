﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {

        public static class Template
        {
            public static string GetTemplatesDirectory()
                => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Template.TemplateDirectory");

            /// <summary>
            /// Получить путь до хранилища обновлений шаблонов.
            /// </summary>
            public static string GetUpdateTemplateStoragePath()
                => ConfigurationHelper.GetConfigurationValue<string>("Template.UpdateTemplateStoragePath");
        }

    }
}
