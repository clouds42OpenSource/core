﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Ссылки для скачивания
        /// </summary>
        public static class DownloadLinks
        {
            /// <summary>
            /// Получить ссылку на скачивание браузера Google Chrome
            /// </summary>
            /// <returns>Ссылка на скачивание браузера Google Chrome</returns>
            public static string GetChromeDownloadLink() 
                => ConfigurationHelper.GetConfigurationValue<string>("DownloadLink.GoogleChrome");

            /// <summary>
            /// Получить ссылку на скачивание браузера Mozila Firefox
            /// </summary>
            /// <returns>Ссылка на скачивание браузера Mozila Firefox</returns>
            public static string GetFirefoxDownloadLink()
                => ConfigurationHelper.GetConfigurationValue<string>("DownloadLink.MozillaFirefox");

            /// <summary>
            /// Получить ссылку на скачивание Линк42
            /// </summary>
            /// <returns>Ссылка на скачивание Линк42</returns>
            public static string GetLink42DownloadLink() =>
                ConfigurationHelper.GetConfigurationValue<string>("Link42.DownloadLink");

        }
    }
}
