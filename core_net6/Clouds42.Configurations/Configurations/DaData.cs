﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Клиентские данные
        /// </summary>
        public static class DaData
        {
            /// <summary>
            /// Получить токен
            /// </summary>
            public static string GetToken()
                => ConfigurationHelper.GetConfigurationValue<string>("DaData.Token");

            /// <summary>
            /// Получить ключ
            /// </summary>
            public static string GetKey()
                => ConfigurationHelper.GetConfigurationValue<string>("DaData.Key");

            /// <summary>
            /// Получить URL к сервису
            /// для получения банковских реквизитов
            /// </summary>
            public static string GetUrlApiForBankDetails()
                => ConfigurationHelper.GetConfigurationValue<string>("DaData.GetUrlApiForBankDetails");

            /// <summary>
            /// Получить URL к сервису
            /// для получения реквизитов компании
            /// </summary>
            public static string GetUrlApiForCompanyDetails()
                => ConfigurationHelper.GetConfigurationValue<string>("DaData.GetUrlApiForCompanyDetails");
        }
    }
   
}
