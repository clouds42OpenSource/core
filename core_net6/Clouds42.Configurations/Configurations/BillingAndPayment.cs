﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Платежи
        /// </summary>
        public static class BillingAndPayment
        {
            /// <summary>
            /// Константы для Paybox
            /// </summary>
            public static class PayboxConst
            {
                /// <summary>
                ///     Id продавца в системе Paybox
                /// </summary>
                public static int GetMerchantId(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<int>("Paybox.MerchantId", supplierId);

                /// <summary>
                ///     Секретный ключ для приема платежей
                /// </summary>
                public static string GetSecretKey(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.SecretKey", supplierId);

                /// <summary>
                ///     Полный путь для сообщения о результате платежа
                /// </summary>
                public static string GetFullResultUrl(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.FullResultUrl", supplierId);

                /// <summary>
                ///     Полный путь для перенаправления в случае "онлайн" оплаты
                /// </summary>
                public static string GetFullSuccessOrFailUrl(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.FullSuccessAndFailUrl", supplierId);

                /// <summary>
                ///     Ссылка для формирования запроса на создание платежа
                /// </summary>
                public static string GetPayboxPaymentSite(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.PayboxPaymentSite", supplierId);

                /// <summary>
                ///     Скрипт для формирования подписи исходящего платежа
                /// </summary>
                public static string GetPayboxPaymentScript(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.PayboxPaymentScript", supplierId);

                /// <summary>
                ///     Скрипт для формирования подписи при сообщении о результате платежа
                /// </summary>
                public static string GetResultUrlScript(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.ResultUrlScript", supplierId);

                /// <summary>
                ///     Скрипт для формирования подписи при перенаправлении
                /// </summary>
                public static string GetSuccessAndFailScript(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<string>("Paybox.SuccessAndFailScript", supplierId);

                /// <summary>
                ///     Тестовый режим для транзакции
                /// </summary>
                public static int GetPayboxTestingMode(Guid supplierId)
                    => ConfigurationHelper.GetConfigurationValue<int>("Paybox.TestingMode", supplierId);
                
            }

            /// <summary>
            /// Параметры агрегатора Robokassa
            /// </summary>
            public static class Robokassa
            {
                /// <summary>
                /// Получить ссылку на агрегатор
                /// </summary>
                public static string GetUrl() 
                    => ConfigurationHelper.GetConfigurationValue<string>("Robokassa.Url");

                /// <summary>
                /// Получить логин
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetLogin(Guid supplierId) 
                    => ConfigurationHelper.GetConfigurationValue<string>("Robokassa.Login", supplierId);

                /// <summary>
                /// Получить ключ для запросов
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetRequestSecretkey(Guid supplierId) 
                    => ConfigurationHelper.GetConfigurationValue<string>("Robokassa.RequestKey", supplierId);

                /// <summary>
                /// Получить формат строки для суммы платежа
                /// </summary>
                /// <returns>Формат строки для суммы платежа</returns>
                public static string GetStringFormatForPaymentSum()
                    => ConfigurationHelper.GetConfigurationValue<string>("Robokassa.StringFormatForPaymentSum");

                /// <summary>
                /// Получить ключ для ответов
                /// </summary>
                /// <param name="supplierId">ID поставщика</param>
                public static string GetResponseSecretkey(Guid supplierId) 
                    => ConfigurationHelper.GetConfigurationValue<string>("Robokassa.ResponseKey", supplierId);
            }

            /// <summary>
            /// Параметры агрегатора Robokassa
            /// </summary>
            public static class UkrPays
            {
                /// <summary>
                /// Получить ссылку на агрегатор
                /// </summary>
                public static string GetUrl()
                    => ConfigurationHelper.GetConfigurationValue<string>("UkrPays.Url");
            }

            /// <summary>
            /// Обещанный платеж
            /// </summary>
            public static class PromisePayment
            {
                /// <summary>
                /// Получить количество дней для ОП
                /// </summary>
                /// <returns>Количество дней для ОП</returns>
                public static int GetPromisePaymentDays()
                    => ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");
            }
        }
    }
}
