﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        public static class MyDiskRecalculation
        {
            /// <summary>
            /// Получить Id задачи которая выполняет пересчет диска
            /// </summary>
            /// <returns>Id задачи</returns>
            public static Guid GetCoreWorkerTaskId() =>
                ConfigurationHelper.GetConfigurationValue<Guid>("MyDiskRecalculation.CoreWorkerTaskId");
        }
    }
}
