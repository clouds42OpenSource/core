﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// МЦОБ
        /// </summary>
        public static class Mcob
        {
            /// <summary>
            ///  Получить пароль МЦОБ
            /// </summary>
            public static string GetPasswordMcob()
                => ConfigurationHelper.GetConfigurationValue<string>("MCOB.PasswordMCOB");

            /// <summary>
            /// Получить логин МЦОБ
            /// </summary>
            public static string GetLoginMcob()
                => ConfigurationHelper.GetConfigurationValue<string>("MCOB.LoginMCOB");

            /// <summary>
            /// Получить имя процесса МЦОБ
            /// </summary>
            public static string GetProcessorMcob()
                => ConfigurationHelper.GetConfigurationValue<string>("MCOB.ProcessorMCOB");

            /// <summary>
            /// Получить сообщение блокировки базы 
            /// </summary>
            public static string GetDatabasesLockMessage()
                => ConfigurationHelper.GetConfigurationValue<string>("MCOB.DatabasesLockMessage");

            /// <summary>
            /// Получить пароль блокировки базы 
            /// </summary>
            public static string GetDatabasesLockPassword()
                => ConfigurationHelper.GetConfigurationValue<string>("MCOB.DatabasesLockPassword");

            /// <summary>
            /// Получить Id сервиса МЦОБ
            /// </summary>
            /// <returns></returns>
            public static Guid GetMcobBillingServiceId() =>
                ConfigurationHelper.GetConfigurationValue<Guid>("BillingServices.McobBillingServiceId");
        }
    }
}
