﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Параметры для JsonWebToken
        /// </summary>
        public static class JsonWebToken
        {
            /// <summary>
            /// Издатель JsonWebToken
            /// </summary>
            public static string GetIssuer() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Issuer");

            /// <summary>
            /// Аудитория, для кого JsonWebToken
            /// </summary>
            public static string GetAudienceAny() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Audience.Any");
            
            /// <summary>
            /// Приватный ключ
            /// </summary>
            public static string GetIssuerSigningKeyBase64() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Issuer.SigningKey.Base64");
            
            /// <summary>
            /// Время жизни JsonWebToken в минутах
            /// </summary>
            public static int GetTimeToLiveInMinutes() =>
                ConfigurationHelper.GetConfigurationValue<int>("JWT.TimeToLive.Minutes");

            /// <summary>
            /// Тип аутентификации
            /// </summary>
            public static string GetAuthenticationType() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.AuthenticationType");

            /// <summary>
            /// Время жизни JsonWebToken в минутах для ЛК
            /// </summary>
            /// <returns>Количество минут</returns>
            public static int GetTimeToLiveForCpInMinutes() =>
                ConfigurationHelper.GetConfigurationValue<int>("JWT.TimeToLive.CP.Minutes");

            /// <summary>
            /// Аудитория, для ЛК JsonWebToken
            /// </summary>
            /// <returns>Название аудитории</returns>
            public static string GetAudienceCp() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Audience.CP");

            /// <summary>
            /// Аудитория, для ЛК JsonWebToken
            /// </summary>
            /// <returns>Название аудитории</returns>
            public static string GetUrlForGetJwtSettingsFromMsProvider() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Ms.Provider.Settings.Url");

            public static string GetDefaultJwtKeysFromDb() =>
                ConfigurationHelper.GetConfigurationValue<string>("JWT.Ms.Provider.Settings.Keys");
        }
    }
}
