﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        public static class TalkMe
        {
            public static string GetBaseApiUrl() => ConfigurationHelper.GetConfigurationValue<string>("TalkMe.Api.BaseUrl");
            public static string GetSourceName() => ConfigurationHelper.GetConfigurationValue<string>("TalkMe.WhatsApp.SourceName");
            public static string GetToken() => ConfigurationHelper.GetConfigurationValue<string>("TalkMe.XToken");
        }
    }
}
