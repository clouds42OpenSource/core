﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Клиент отправки SMS
        /// </summary>
        public static class SmsClient
        {
            /// <summary>
            /// Получить признак необходимости отправки SMS
            /// </summary>
            /// <returns>Признак работы контекста AD в режиме разработки</returns>
            public static bool GetNeedSendSms()
                => ConfigurationHelper.GetConfigurationValue<bool>("SmsClient.NeedSendSms");

            /// <summary>
            /// Получить логин клиента для отправки SMS
            /// </summary>
            /// <returns>Логин клиента для отправки SMS</returns>
            public static string GetClientLogin()
                => ConfigurationHelper.GetConfigurationValue("SmsClient.Login");

            /// <summary>
            /// Получить пароль клиента для отправки SMS
            /// </summary>
            /// <returns>Пароль клиента для отправки SMS</returns>
            public static string GetClientPassword()
                => ConfigurationHelper.GetConfigurationValue("SmsClient.Password");

            /// <summary>
            /// Получить кодировку клиента для отправки SMS
            /// </summary>
            /// <returns>Кодировка клиента для отправки SMS</returns>
            public static string GetClientCharset()
                => ConfigurationHelper.GetConfigurationValue("SmsClient.Charset");

            /// <summary>
            /// Получить адрес сервиса для отправки SMS
            /// </summary>
            /// <returns>Адрес сервиса для отправки SMS</returns>
            public static string GetServiceUrl()
                => ConfigurationHelper.GetConfigurationValue("SmsClient.ServiceUrl");
        }
    }
}
