﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Настройки воркера.
        /// </summary>
        public static class CoreWorker
        {
            /// <summary>
            /// Получить URL для захвата задачи
            /// </summary>
            /// <returns>URL для захвата задачи</returns>
            public static string GetUrlForCaptureTask()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.UrlForCaptureTask");

            /// <summary>
            /// Получить URL для обновления статуса задачи
            /// </summary>
            /// <returns>URL для захвата задачи</returns>
            public static string GetUrlForUpdateTaskStatus()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.UrlForUpdateTaskStatus");

            /// <summary>
            /// Получить URL для захвата настроек
            /// </summary>
            /// <returns>URL для захвата настроек</returns>
            public static string GetUrlForCaptureConfiguration()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.UrlForCaptureConfiguration");

            /// <summary>
            /// Получить таймаут между запросами на захват новой задачи
            /// </summary>
            /// <returns>Таймаут между запросами на захват новой задачи</returns>
            public static int GetTimeoutInSecondsBetweenCaptureNewTaskRequest()
                => ConfigurationHelper.GetConfigurationValue<int>("CoreWorker.TimeoutInSecondsBetweenCaptureNewTaskRequest");

            /// <summary>
            /// Получить начальный период (в днях) для поиска задач в обработке
            /// </summary>
            /// <returns>Начальный период (в днях) для поиска задач в обработке</returns>
            public static int GetProcessingTaskStartPeriodInDays()
                => ConfigurationHelper.GetConfigurationValue<int>("CoreWorker.ProcessingTaskStartPeriodInDays");

            /// <summary>
            /// Получить максимально допустимое количество задач в очереди
            /// </summary>
            /// <returns>Максимально допустимое количество задач в очереди</returns>
            public static int GetMaxAllowableTasksInQueueCount()
                => ConfigurationHelper.GetConfigurationValue<int>("CoreWorker.MaxAllowableTasksInQueueCount");

			/// <summary>
            /// Получить количество месяцев, через которые удаляются старые пользователи
            /// </summary>
            /// <returns></returns>
            public static string GetMonthToInactiveOldUsers()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreWorker.MonthToInactive");

            /// <summary>
            /// Получить тему сообщения воркера для отправляемого письма
            /// </summary>
            /// <returns></returns>
            public static string GetEmailReportJobTopik()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreWorker.ReportJobTopik");

            /// <summary>
            /// Получить почту для отправки сообщений воркером
            /// </summary>
            /// <returns></returns>
            public static string GetWorkerReportMail()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreWorker.WorkerReportMail");

            /// <summary>
            /// Получить количество дней для удаления неактивных виндовс профилей
            /// </summary>
            /// <returns></returns>
            public static string GetDeleteArchiveDays()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreWorker.DeleteArchiveDays");

            /// <summary>
            ///  Получить количество секунд на которое усыпляется поток
            /// </summary>
            /// <returns></returns>
            public static string GetSecondToWaitAfterDbDelete()
                => ConfigurationHelper.GetConfigurationValue<string>("CoreWorker.WaitAfterDbDelete");

            /// <summary>
            /// Получить номер инициатора операции
            /// </summary>
            /// <returns>Номер инициатора операции</returns>
            public static string GetOperationInitiatorNumber()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.CoreServiceIdGuid");

            /// <summary>
            /// Получить флаг для отправки уведомлений
            /// </summary>
            /// <returns>Флаг для отправки уведомлений</returns>
            public static string GetNotificationsEnabledStatus()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.Notifications.Enabled");

            /// <summary>
            /// Получить флаг для отправки уведомлений на почту
            /// </summary>
            /// <returns>Флаг для отправки уведомлений на почту</returns>
            public static string GetNotificationsUseEmail()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.Notifications.UseEmail");

            /// <summary>
            /// Получить почту клауда для отправки писем воркера
            /// </summary>
            /// <returns></returns>
            public static string GetSenderEmail()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.SenderEmail");

            /// <summary>
            /// Получить отображаемое название отправителя письма
            /// </summary>
            /// <returns>Название отправителя письма</returns>
            public static string Get42CloudCommandSender()
                => ConfigurationHelper.GetConfigurationValue("CoreWorker.FromLableCommand42Clouds");

            /// <summary>
            /// Потеря связи с воркером.
            /// </summary>
            public static class ConnectionLost
            {
                /// <summary>
                /// Количество минут до таймаута доступности воркера.
                /// </summary>
                public static int GetTimeoutMinutes()
                    => ConfigurationHelper.GetConfigurationValue<int>("CoreWorker.ConnectionLost.TimeoutMinutes");
            }
        }
    }
}
