﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Настройки и ключи для капчи
        /// </summary>
        public static class CaptchaValidatorConfiguration
        {
            /// <summary>
            /// Адрес валидации токена пользователя
            /// </summary>
            public static string GetRemoteAddress()
                => ConfigurationHelper.GetConfigurationValue<string>("ReCapcha.RemoteAddress");

            /// <summary>
            /// Уровень валидации токена
            /// </summary>
            public static double GetAcceptableScore()
                => ConfigurationHelper.GetConfigurationValue<double>("ReCapcha.AcceptableScore");

            /// <summary>
            /// Ключ пользователя
            /// </summary>
            public static string GetUserKey()
                => ConfigurationHelper.GetConfigurationValue<string>("ReCapcha.UserKey");


        }
    }
}
