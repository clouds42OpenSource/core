﻿namespace Clouds42.Configurations.Configurations;

public static partial class CloudConfigurationProvider
{
    public static class Neuro
    {
        public static string GetBaseApiUrl()
            => ConfigurationHelper.GetConfigurationValue<string>("Neuro.BaseApiLink");
    }
}
