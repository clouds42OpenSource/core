﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        public static class WebSocketBridge
        {

            /// <summary>
            /// Получить адрес сервиса WebSocketBridge
            /// </summary>
            public static string GetServiceApiUri()
                => ConfigurationHelper.GetConfigurationValue<string>("WebSocketBridge.ServiceAPIUri");

        }
    }
}
