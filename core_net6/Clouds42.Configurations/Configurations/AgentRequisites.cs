﻿using System;

namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Реквизиты агента
        /// </summary>
        public static class AgentRequisites
        {
            /// <summary>
            /// Получить id печатной формы отчета агента
            /// </summary>
            public static Guid GetAgentReportPrintHtmlFormId()
                => ConfigurationHelper.GetConfigurationValue<Guid>("AgencyContract.AgentReportPrintHtmlFormId");
        }
    }
}
