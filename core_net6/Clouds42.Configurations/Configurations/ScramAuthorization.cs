﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Авторизация по Scram
        /// </summary>
        public static class ScramAuthorization
        {
            /// <summary>
            /// Получить количество итераций протокола
            /// </summary>
            public static int GetIc()
                => ConfigurationHelper.GetConfigurationValue<int>("ScramAuthorization.Ic");

            /// <summary>
            /// Получить размер криптографической соли
            /// </summary>
            public static int GetSaltSize()
                => ConfigurationHelper.GetConfigurationValue<int>("ScramAuthorization.SaltSize");

            /// <summary>
            /// Получить размер серверного Nonce
            /// </summary>
            public static int GetServerNonceSize()
                => ConfigurationHelper.GetConfigurationValue<int>("ScramAuthorization.ServerNonceSize");

            /// <summary>
            /// Получить размер посоленного пароля
            /// </summary>
            public static int GetSaltedPwdSize()
                => ConfigurationHelper.GetConfigurationValue<int>("ScramAuthorization.SaltedPwdSize");
        }
    }
}
