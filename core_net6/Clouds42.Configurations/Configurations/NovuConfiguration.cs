﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации ностройки Novu
        /// </summary>
        public static class NovuConfiguration
        {
            /// <summary>
            /// Получить название сайта Api
            /// </summary>
            /// <returns>Название сайта для настройки Novu</returns>
            public static string GetBackendUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("Novu.APPNOVUAPI");

            /// <summary>
            /// Получить название сайта socket
            /// </summary>
            /// <returns>Название сайта для настройки Novu</returns>
            public static string GetSocketUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("Novu.APPNOVUSOCKETURL");

            /// <summary>
            /// Получить идентификатор
            /// </summary>
            /// <returns>идентификатор Novu</returns>
            public static string GetNovuId()
                => ConfigurationHelper.GetConfigurationValue<string>("Novu.APPNOVUID");
        }
    }
}
