﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Файлы
        /// </summary>
        public static class Files
        {
            /// <summary>
            /// Путь к приложению DocLoader
            /// </summary>
            /// <returns></returns>
            public static string PathToDocLoader() => ConfigurationHelper.GetConfigurationValue("Files.PathToDocLoader");

            /// <summary>
            /// Путь к папке с документами для загрузки для тестов
            /// </summary>
            /// <returns></returns>
            public static string FileUploadPath() => ConfigurationHelper.GetConfigurationValue(nameof(FileUploadPath));


            /// <summary>
            /// Путь для создания папки 1CEStart при создании пользователя
            /// </summary>
            /// <returns></returns>
            public static string PathTo1CeStart() => ConfigurationHelper.GetConfigurationValue("Files.PathTo1CEStart");

            /// <summary>
            /// Получить путь для создания папки профиля пользователя
            /// </summary>
            /// <returns>Путь для создания папки профиля пользователя</returns>
            public static string GetAccountUserProfilesDirectory() =>
                ConfigurationHelper.GetConfigurationValue("Files.AccountUserProfilesDirectory");
        }
    }
}
