﻿namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        public static class Article
        {
            public static string GetLinkForArticle()
                => ConfigurationHelper.GetConfigurationValue<string>("Article.Link");
        }
    }
  
}
