﻿namespace Clouds42.Configurations.Configurations
{

    public static partial class CloudConfigurationProvider
    {

        public static class Resource
        {
            /// <summary>
            /// Получить время кеширования HTTP ресурса в секундах.
            /// </summary>
            public static int GetHttpResourceCacheTimeInSeconds()
                => int.Parse(ConfigurationHelper.GetConfigurationValue("HttpResource.CacheTimeInSeconds"));
        }

    }
}
