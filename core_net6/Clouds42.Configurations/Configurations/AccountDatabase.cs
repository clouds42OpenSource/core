﻿using System;

namespace Clouds42.Configurations.Configurations
{
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Информационная база
        /// </summary>
        public static class AccountDatabase
        {
            /// <summary>
            /// Разрешенная длинна для номера ИБ
            /// </summary>
            public static int LengthV82NameAccountDatabase()
                => ConfigurationHelper.GetConfigurationValue<int>("AccountDatabase.LengthV82NameAccountDatabase");
            
            
            /// <summary>
            /// Получить путь для хранения файлов, если в локали есть htznr
            /// </summary>
            /// <returns></returns>
            public static string HtznrSharedStoragePath() => 
                ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.HtznrStoragePath");
            
            /// <summary>
            /// ТиИ АО
            /// </summary>
            public static class Support
            {
                /// <summary>
                /// Получить максимальное количество информационных баз, у которых можно провести ТиИ за одну ночь.
                /// </summary>
                public static int GetAccountDatabasesTehSupportMaxCountPerNigth()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Support.AccountDatabasesTehSupportMaxCountPerNigth");

                /// <summary>
                /// Получить количество дней периодичности проведления ТиИ.
                /// </summary>                
                public static int GetTehSupportPeriodicDaysCount()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Support.TehSupportPeriodicDaysCount");

                /// <summary>
                /// Максимальное количество парралелных выполнений ТиИ
                /// </summary>                
                public static int GetMaxTehSupportParallelsThreads()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Support.MaxTehSupportParralelsThreads");

                /// <summary>
                /// Время до которого разрешено проводить ТиИ и АО. Нпример до 06-30.
                /// </summary>                
                public static TimeSpan GetTimeOfLastUpdateOrTehSupportOperation()
                    => ConfigurationHelper.GetTimeSpanConfiguration(
                        "AccountDatabase.Support.TimeOfLastUpdateOrTehSupportOperation");

                /// <summary>
                /// Получить таймаут проведения ТиИ.
                /// </summary>
                public static int GetTehSupportTimeout()
                    => ConfigurationHelper.GetConfigurationValue<int>("AccountDatabase.Support.TehSupportTimeout");

                /// <summary>
                /// Получить таймаут проведения АО.
                /// </summary>
                public static int GetAutoUpdateTimeout()
                    => ConfigurationHelper.GetConfigurationValue<int>("AccountDatabase.Support.AutoUpdateTimeout");

                /// <summary>
                /// Получить таймаут принятия обновления
                /// </summary>
                /// <returns>Таймаут принятия обновления</returns>
                public static int GetApplyUpdateTimeout()
                    => ConfigurationHelper.GetConfigurationValue<int>("AccountDatabase.Support.ApplyUpdateTimeout");

                /// <summary>
                /// Получить таймаут получения метаданных
                /// </summary>
                /// <returns>Таймаут получения метаданных</returns>
                public static int GetReceiveMetadataCommandTimeout()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Support.ReceiveMetadataCommandTimeout");

                /// <summary>
                /// Получить путь к обработке получения метаданных
                /// </summary>
                /// <returns>Путь к обработке получения метаданных</returns>
                public static string GetReceiveMetadataHandlingPath()
                    => ConfigurationHelper.GetConfigurationValue("AccountDatabase.Support.ReceiveMetadataHandlingPath");

                /// <summary>
                /// Получить путь к файлу конфигурации 1С Предприятие
                /// </summary>
                /// <returns>Путь к файлу конфигурации 1С Предприятие</returns>
                public static string Get1CEnterpiseConfigFilePath()
                    => ConfigurationHelper.GetConfigurationValue("AccountDatabase.Support.1CEnterpiseConfigFilePath");

                /// <summary>
                /// Получить название параметра для включения небезопасного режима работы 1С Предприятие
                /// </summary>
                /// <returns>Название параметра для включения небезопасного режима работы 1С Предприятие</returns>
                public static string Get1CEnterpriseUnsafeModeParamName()
                    => ConfigurationHelper.GetConfigurationValue(
                        "AccountDatabase.Support.1CEnterpriseUnsafeModeParamName");

                /// <summary>
                /// Получить команду для принятия обновления
                /// </summary>
                /// <returns>Команда для принятия обновления</returns>
                public static string GetApplyUpdateCommand()
                    => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Support.ApplyUpdateCommand");

                /// <summary>
                /// Получить признак надо ли крутить не фатальные обработки потдержки в пуле.
                /// </summary>
                public static bool GetNeedEnqueueNotFatalErrorToQueue()
                    => ConfigurationHelper.GetConfigurationValue<bool>(
                        "AccountDatabase.Support.NeedEnqueueNotFanatalErrorToQueue");

                /// <summary>
                /// Получить почту для уведомлений о провдеенных тех. работах.
                /// </summary>
                public static string GetSupportNotificationEmail()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "AccountDatabase.Support.SupportNotificationEmail");

                /// <summary>
                /// Получить номер воркера для обработки задач потдержки инфо баз.
                /// </summary>
                public static short GetSupportWorkerId()
                    => ConfigurationHelper.GetConfigurationValue<short>("AccountDatabase.Support.SupportWorkerId");

                /// <summary>
                /// Максимальное количество баз для демо аккаунта
                /// </summary>
                /// <returns></returns>
                public static int MaxCountAccountDbForDemoAccount() =>
                    ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Support.MaxCountAccountDbForDemoAccount");

                public static string GetTiIDateList()
                    => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Support.TiIDateList");
            }

            /// <summary>
            ///     Публикации баз
            /// </summary>
            public static class Publish
            {
                /// <summary>
                /// Получить название для файла web конфига
                /// </summary>
                /// <returns>Название для файла web конфига</returns>
                public static string GetWebConfigFileName()
                    => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Publish.WebConfigFileName");

                /// <summary>
                /// Получить название для файла web конфига с блокировкой
                /// </summary>
                /// <returns>Название для файла web конфига с блокировкой</returns>
                public static string GetLockWebConfigFileName()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "AccountDatabase.Publish.LockWebConfigFileName");

                /// <summary>
                /// Получить адрес для редиректа
                /// </summary>
                /// <returns>Адрес для редиректа</returns>
                public static string GetRouteValueForHttpRedirect()
                    => ConfigurationHelper.GetConfigurationValue("HttpRedirect");
            }

            /// <summary>
            /// Загрузка файлов инф. базы
            /// </summary>
            public static class UploadFiles
            {
                /// <summary>
                /// Получить название токена для части файла
                /// </summary>
                /// <returns></returns>
                public static string GetPartTokenNameForChunkOfFile()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "AccountDatabase.UploadFiles.PartTokenNameForChunkOfFile");

                /// <summary>
                /// Получить название директории для загружаемых частей файла
                /// </summary>
                /// <returns></returns>
                public static string GetDirectoryNameForChunksOfFile()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "AccountDatabase.UploadFiles.DirectoryNameForChunksOfFile");

                /// <summary>
                /// Получить размер в мб для части файла инф. базы
                /// </summary>
                public static int GetSizeInMbForChunkOfDbFile()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.UploadFiles.SizeInMbForChunkOfDbFile");
            }

            /// <summary>
            /// Восстановление инф. базы
            /// </summary>
            public static class Restore
            {
                /// <summary>
                /// Получить таймаут выполнения запроса на восстановление серверной базы
                /// </summary>
                /// <returns>Таймаут выполнения запроса на восстановление серверной базы</returns>
                public static int GetRestoreDatabaseQueryExecutionTimeOut()
                    => ConfigurationHelper.GetConfigurationValue<int>(
                        "AccountDatabase.Restore.RestoreDatabaseQueryExecutionTimeOut");
            }

            /// <summary>
            /// Кластер 1С.
            /// </summary>
            public static class Cluster
            {
                /// <summary>
                /// Получить логин в СКЛ для кластера.
                /// </summary>                
                public static string GetSqlLogin()
                    => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Cluster.SqlLogin");

                /// <summary>
                /// Получить пароль в СКЛ для кластера.
                /// </summary>                
                public static string GetSqlPassword()
                    => ConfigurationHelper.GetConfigurationValue<string>("AccountDatabase.Cluster.SqlPassword");

                /// <summary>
                /// Получить строку подключения механизма управления сеансами
                /// </summary>
                /// <returns>Строка подключения механизма управления сеансами</returns>
                public static string GetSessionsControlConnectionString()
                    => ConfigurationHelper.GetConfigurationValue(
                        "AccountDatabase.Cluster.SessionsControlConnectionString");

            }
        }
    }
}
