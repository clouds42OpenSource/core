﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями облака
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Магазин приложений
        /// </summary>
        public static class Market
        {
            /// <summary>
            /// Получить урл магазина приложений
            /// с указанием источника
            /// </summary>
            /// <returns>Урл магазина приложений</returns>
            public static string GetUrlWithSource()
                => ConfigurationHelper.GetConfigurationValue<string>("Market.UrlWithSource");

            /// <summary>
            /// Получить урл магазина приложений
            /// </summary>
            /// <returns>Урл магазина приложений</returns>
            public static string GetUrl()
                => ConfigurationHelper.GetConfigurationValue<string>("Market.Url");

            /// <summary>
            /// Получить урл на сервис "Штрихкод-информер" магазина 
            /// </summary>
            /// <returns>Урл на сервис "Штрихкод-информер"</returns>
            public static string GetUrlForInformerBarcodeService() =>
                ConfigurationHelper.GetConfigurationValue<string>("Market.UrlForInformerBarcodeService");

            /// <summary>
            /// Получить урл на сервис "Контроль ввода данных в 1С" магазина 
            /// </summary>
            /// <returns>Урл на сервис "Контроль ввода данных в 1С"</returns>
            public static string GetUrlForDataControlService() =>
                ConfigurationHelper.GetConfigurationValue<string>("Market.UrlForDataControlService");

            /// <summary>
            /// Получить урл на сервис "30 служб доставки" магазина 
            /// </summary>
            /// <returns>Урл на сервис "30 служб доставки"</returns>
            public static string GetUrlForDeliveryService() =>
                ConfigurationHelper.GetConfigurationValue<string>("Market.UrlForDeliveryService");

            /// <summary>
            /// Получить урл на сервис "Чат-боты" магазина 
            /// </summary>
            /// <returns>Урл на сервис "Чат-боты"</returns>
            public static string GetUrlForChatBotsService() =>
                ConfigurationHelper.GetConfigurationValue<string>("Market.UrlForChatBotsService");

            /// <summary>
            /// Получить урл для проверки токена
            /// </summary>
            public static string GetUrlToCheckToken()
                => ConfigurationHelper.GetConfigurationValue<string>("Market.UserTokenValidationUrl");
        }
    }
}
