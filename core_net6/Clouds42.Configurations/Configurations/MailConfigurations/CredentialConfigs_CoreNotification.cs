﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты CoreNotification
        /// </summary>
        public static partial class CoreNotification
        {
            /// <summary>
            /// core-notification@42clouds.com
            /// </summary>
            public static class Credentials
            {
                /// <summary>
                ///     Получить логин
                /// </summary>
                public static string GetLogin()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "MailConfiguration.CoreNotification.CoreNotification.Login");

                /// <summary>
                ///     Получить пароль
                /// </summary>
                public static string GetPassword()
                    => ConfigurationHelper.GetConfigurationValue<string>(
                        "MailConfiguration.CoreNotification.CoreNotification.Password");
            }
        }
    }
}