﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Profit-Account
        /// </summary>
        public static partial class ProfitAccount
        {
            public static class Credentials
            {
                /// <summary>
                ///     support@42clouds.com
                /// </summary>
                public static class Support
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.ProfitAccount.Support.Login");

                    /// <summary>
                    ///     Получить пароль
                    /// </summary>
                    public static string GetPassword()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.ProfitAccount.Support.Password");
                }
            }
        }
    }
    
}
