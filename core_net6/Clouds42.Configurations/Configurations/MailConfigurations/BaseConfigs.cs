﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Efsol
        /// </summary>
        public static partial class Efsol
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.Efsol.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost() 
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.Host");
        }

        /// <summary>
        ///     Учетные данные почты Delans
        /// </summary>
        public static partial class Delans
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.Delans.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Delans.Host");
        }

        /// <summary>
        ///     Учетные данные почты Dostavka
        /// </summary>
        public static partial class Dostavka
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.Dostavka.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Dostavka.Host");
        }

        /// <summary>
        ///     Учетные данные почты Promo
        /// </summary>
        public static partial class Promo
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.Promo.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Promo.Host");
        }

        /// <summary>
        ///     Учетные данные почты Sauri
        /// </summary>
        public static partial class Sauri
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.Sauri.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Sauri.Host");
        }

        /// <summary>
        ///     Учетные данные почты Profit-Account
        /// </summary>
        public static partial class ProfitAccount
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.ProfitAccount.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.ProfitAccount.Host");
        }

        /// <summary>
        ///     Учетные данные почты Онлайн АбонЦентра
        /// </summary>
        public static partial class AbonCenter
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.AbonCenter.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.AbonCenter.Host");
        }

        /// <summary>
        ///     Учетные данные почты CoreNotification
        /// </summary>
        public static partial class CoreNotification
        {
            /// <summary>
            ///     Получить порт
            /// </summary>
            public static int GetPort()
                => ConfigurationHelper.GetConfigurationValue<int>("MailConfiguration.CoreNotification.Port");

            /// <summary>
            ///     Получить хост
            /// </summary>
            public static string GetHost()
                => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.CoreNotification.Host");
        }
    }
}