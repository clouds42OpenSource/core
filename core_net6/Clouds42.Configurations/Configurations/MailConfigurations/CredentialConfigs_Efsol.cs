﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Efsol
        /// </summary>
        public static partial class Efsol
        {
            public static class Credentials
            {
                /// <summary>
                ///     manager@42clouds.com
                /// </summary>
                public static class Manager
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.Manager.Login");

                    /// <summary>
                    ///     Получить пароль
                    /// </summary>
                    public static string GetPassword()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.Manager.Password");
                }

                /// <summary>
                ///     manager@42clouds.pro
                /// </summary>
                public static class ManagerPro
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.ManagerPro.Login");
                }

                /// <summary>
                ///     support@42clouds.com
                /// </summary>
                public static class Support
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.Support.Login");

                    /// <summary>
                    ///     Получить пароль
                    /// </summary>
                    public static string GetPassword()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Efsol.Support.Password");
                }
            }
        }
    }
}