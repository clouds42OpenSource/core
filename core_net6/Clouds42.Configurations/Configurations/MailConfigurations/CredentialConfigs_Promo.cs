﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Promo
        /// </summary>
        public static partial class Promo
        {
            public static class Credentials
            {
                /// <summary>
                ///     support@42clouds.com
                /// </summary>
                public static class Support
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Promo.Support.Login");

                    /// <summary>
                    ///     Получить пароль
                    /// </summary>
                    public static string GetPassword()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Promo.Support.Password");
                }
            }
        }
    }
}