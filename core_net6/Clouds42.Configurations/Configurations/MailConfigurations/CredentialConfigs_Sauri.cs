﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Sauri
        /// </summary>
        public static partial class Sauri
        {
            /// <summary>
            ///     sauri@sauri.io
            /// </summary>
            public static class Credentials
            {
                /// <summary>
                ///     Получить логин
                /// </summary>
                public static string GetLogin()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Sauri.Sauri.Login");

                /// <summary>
                ///     Получить пароль
                /// </summary>
                public static string GetPassword()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Sauri.Sauri.Password");

                /// <summary>
                ///     Получить url для сброса пароля
                /// </summary>
                public static string GetResetUrl()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Sauri.ResetUrl");
            }
        }
    }
}