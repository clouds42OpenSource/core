﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Онлайн АбонЦентра
        /// </summary>
        public static partial class AbonCenter
        {
            /// <summary>
            ///     abc@efsol.ru
            /// </summary>
            public static class Credentials
            {
                /// <summary>
                /// Получить логин
                /// </summary>
                public static string GetLogin()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.AbonCenter.Abc.Login");

                /// <summary>
                ///     Получить пароль
                /// </summary>
                public static string GetPassword()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.AbonCenter.Abc.Password");

                /// <summary>
                ///     Получить url
                /// </summary>
                /// <returns></returns>
                public static string GetResetUrl()
                    => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.AbonCenter.ResetUrl");
            }
        }
    }
    
}
