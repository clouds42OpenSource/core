﻿namespace Clouds42.Configurations.Configurations.MailConfigurations
{
    /// <summary>
    ///     Конфигурация почты
    /// </summary>
    public static partial class MailConfiguration
    {
        /// <summary>
        ///     Учетные данные почты Dostavka
        /// </summary>
        public static partial class Dostavka
        {
            public static class Credentials
            {
                /// <summary>
                ///     dostavka_efsol@efsol.ru
                /// </summary>
                public static class DostavkaEfsol
                {
                    /// <summary>
                    ///     Получить логин
                    /// </summary>
                    public static string GetLogin()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Dostavka.DostavkaEfsol.Login");

                    /// <summary>
                    ///     Получить пароль
                    /// </summary>
                    public static string GetPassword()
                        => ConfigurationHelper.GetConfigurationValue<string>("MailConfiguration.Dostavka.DostavkaEfsol.Password");
                }
            }
        }
    }
}