﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер конфигураций ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Сервис "Мои информационные базы"
        /// </summary>
        public static class MyDatabasesService
        {
            /// <summary>
            /// Получить значение дефолтного лимита,
            /// на бесплатное создание баз
            /// </summary>
            public static int GetDefaultLimitOnFreeCreationDb()
                => ConfigurationHelper.GetConfigurationValue<int>("MyDatabasesService.DefaultLimitOnFreeCreationDb");
        }
    }
}
