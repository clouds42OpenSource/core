﻿namespace Clouds42.Configurations.Configurations
{
    /// <summary>
    /// Провайдер для работы с конфигурациями ядра
    /// </summary>
    public static partial class CloudConfigurationProvider
    {
        /// <summary>
        /// Конфигурации сервиса Fasta
        /// </summary>
        public static class FastaService
        {
            /// <summary>
            /// Получить количество демо дней для сервиса
            /// </summary>
            /// <returns>Количество демо дней</returns>
            public static int GetDemoDaysCount()
                => ConfigurationHelper.GetConfigurationValue<int>("FastaService.DemoDaysCount");
        }
    }
    
}
