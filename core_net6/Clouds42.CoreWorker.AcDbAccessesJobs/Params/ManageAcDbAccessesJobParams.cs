﻿using System;
using System.Collections.Generic;

namespace Clouds42.CoreWorker.AcDbAccessesJobs.Params
{
    /// <summary>
    /// Параметры задачи по выдаче/удалению доступов в инф. базу
    /// </summary>
    public class ManageAcDbAccessesJobParams
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Список Id пользователей
        /// </summary>
        public List<Guid> UsersId { get; set; } = [];
    }
}
