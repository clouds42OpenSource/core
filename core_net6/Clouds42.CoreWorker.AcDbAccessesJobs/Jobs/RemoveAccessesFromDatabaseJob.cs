﻿using System;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.CoreWorker.AcDbAccessesJobs.Params;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AcDbAccessesJobs.Jobs
{
    /// <summary>
    /// Задача для удаления доступов из инф. базы
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveAccessesFromDatabaseJob)]
    public class RemoveAccessesFromDatabaseJob(
        IUnitOfWork dbLayer,
        IAccountDatabaseUserAccessManager accountDatabaseUserAccessManager)
        : CoreWorkerParamsJob<ManageAcDbAccessesJobParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(ManageAcDbAccessesJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            jobParams.UsersId.ForEach(userId =>
            {
                accountDatabaseUserAccessManager.DeleteAccessFromDb(jobParams.DatabaseId, userId);
            });
        }
    }
}
