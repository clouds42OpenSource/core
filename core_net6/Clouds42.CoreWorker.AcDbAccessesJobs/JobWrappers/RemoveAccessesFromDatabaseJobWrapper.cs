﻿using Clouds42.CoreWorker.AcDbAccessesJobs.Jobs;
using Clouds42.CoreWorker.AcDbAccessesJobs.Params;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AcDbAccessesJobs.JobWrappers
{
    /// <summary>
    /// Обработчик задачи для удаления доступов из инф. базы
    /// </summary>
    public class RemoveAccessesFromDatabaseJobWrapper(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<RemoveAccessesFromDatabaseJob, ManageAcDbAccessesJobParams>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик задачи</returns>
        public override TypingParamsJobWrapperBase<RemoveAccessesFromDatabaseJob, ManageAcDbAccessesJobParams> Start(ManageAcDbAccessesJobParams paramsJob)
        {
            StartTask(paramsJob, $"Удаление доступов из инф. базы '{paramsJob.DatabaseId}'.");
            return this;
        }
    }
}
