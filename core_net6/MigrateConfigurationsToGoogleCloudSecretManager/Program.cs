﻿using System.Text;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using Google.Api.Gax.ResourceNames;
using Google.Cloud.SecretManager.V1;
using Google.Protobuf;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Repositories;

namespace MigrateConfigurationsToGoogleCloudSecretManager;

static class Program
{
    static async Task Main()
    {
        Console.OutputEncoding = Encoding.UTF8;
        var configurationRoot = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", true, true).Build();
        
        var standNames = configurationRoot.GetSection("Stand:Names").GetChildren().Select(x => x.Value).ToArray();
        var pathToCredentials = configurationRoot.GetSection("PathToCredentials").Value;
        var projectId = configurationRoot.GetSection("ProjectId").Value;
       
        Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", pathToCredentials);
        SecretManagerServiceClient client = await SecretManagerServiceClient.CreateAsync();
        ProjectName projectName = new ProjectName(projectId);
        
        Secret secret = new Secret
        {
            Replication = new Replication
            {
                Automatic = new Replication.Types.Automatic(),
            }
        };

        var connectionString = configurationRoot.GetConnectionString("DefaultConnection");

        foreach (var standName in standNames)
        {
            await CreateSecrets(connectionString, standName, client, projectName, secret, configurationRoot);
        }

        Console.WriteLine("Work is completed");
        Console.ReadKey();
    }

    private static async Task CreateSecrets(string? connectionString,
        string? standName, 
        SecretManagerServiceClient client, 
        ProjectName projectName, Secret secret, IConfigurationRoot configuration)
    {

        try
        {
            connectionString = standName switch
            {
                "localhost" => String.Format(connectionString?? "", "core"),
                "dev1" => string.Format(connectionString ?? "", "core_dev1"),
                "dev3" => string.Format(connectionString ?? "", "core_dev3"),
                "dev4" => string.Format(connectionString ?? "", "core_dev4"),
                "dev5" => string.Format(connectionString ?? "", "core_dev5"),
                "dev6" => string.Format(connectionString ?? "", "core_dev6"),
                "beta" => string.Format(connectionString ?? "", "core"),
                "prod" => string.Format(connectionString ?? "", "core"),
                _ => connectionString
            };

            var optionsBuilder = new DbContextOptionsBuilder<Clouds42DbContext>();
            ;
            if (configuration.GetSection("ConnectionStrings:DatabaseType").Value == ProviderTypeConstants.MsSql)
            {
                optionsBuilder.UseSqlServer(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            else
            {
                optionsBuilder.UseNpgsql(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            var secrets = await new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options)).CloudConfigurationRepository
                .AsQueryableNoTracking()
                .Where(x => !string.IsNullOrEmpty(x.Value))
                .OrderBy(x => x.Key)
                .ToDictionaryAsync(x => x.ContextSupplierId.HasValue ? $"{x.Key}_{x.ContextSupplierId}" : x.Key,
                    x => x.Value);

            SecretPayload payload = new SecretPayload
            {
                Data = ByteString.CopyFromUtf8(JsonConvert.SerializeObject(secrets, Formatting.Indented))
            };

            var sc = await client.CreateSecretAsync(projectName, standName, secret);

            await client.AddSecretVersionAsync(sc.SecretName, payload);
        }

        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}
