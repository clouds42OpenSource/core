﻿using Microsoft.Extensions.DependencyInjection;
using Repositories;
using Clouds42.Logger.Serilog;
using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using Clouds42.DependencyRegistration;
using Clouds42.TelegramBot;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.Logger.Serilog.Models;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace DeleteAccountDatabaseBackups
{
    static class Program
    {
        private static readonly ServiceCollection ServiceCollection = [];
        private static TombCleanerProvider _tombCleanerProvider;
        private static IUnitOfWork _dbLayer;
        private static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            IConfiguration configuration = new ConfigurationBuilder()
                     .AddJsonFile("appsettings.json", true, true)
                     .AddEnvironmentVariables()
                     .Build();

            ServiceCollection
               .AddGlobalServices(configuration)
               .AddSerilogWithElastic(configuration)
               .AddTelegramBots(configuration)
               .AddScoped<IUnitOfWork, UnitOfWork>()
               .AddSingleton(configuration)
               .AddTransient<ILogger42, SerilogLogger42>();

            var serviceProvider = ServiceCollection.BuildServiceProvider();
            _tombCleanerProvider = serviceProvider.GetRequiredService<TombCleanerProvider>();
            int counter = 0;
            _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();

            var backupMigration = _dbLayer.AccountDatabaseBackupRepository.Where(a => a.EventTrigger == CreateBackupAccountDatabaseTrigger.MigrateAccountDatabase 
            && a.SourceType == AccountDatabaseBackupSourceType.GoogleCloud).ToList();
            Console.WriteLine($"Получили список бекапов после миграции {backupMigration.Count}.");
            DeleteAccountDatabaseBackup(backupMigration);
            Console.WriteLine("Удаление бекапов после МИГРАЦИИ завершено!!!!");
            counter += backupMigration.Count;
            
            var backupAO = _dbLayer.AccountDatabaseBackupRepository.Where(a => a.EventTrigger == CreateBackupAccountDatabaseTrigger.AutoUpdateAccountDatabase
            && a.SourceType == AccountDatabaseBackupSourceType.GoogleCloud).ToList();
            Console.WriteLine($"Получили список бекапов после АО {backupAO.Count}.");
            DeleteAccountDatabaseBackup(backupAO);
            Console.WriteLine("Удаление бекапов после АО завершено!!!!");
            counter += backupAO.Count;
            
            var backupTII = _dbLayer.AccountDatabaseBackupRepository.Where(a => a.EventTrigger == CreateBackupAccountDatabaseTrigger.TehSupportAccountDatabase
            && a.SourceType == AccountDatabaseBackupSourceType.GoogleCloud).ToList();
            var backupTS = backupTII.Where(a => a.CreationBackupDateTime <= DateTime.Now.AddMonths(-2)).ToList();
            Console.WriteLine($"Получили список бекапов после ТИИ {backupTS.Count}.");
            DeleteAccountDatabaseBackup(backupTS);
            Console.WriteLine("Удаление бекапов после ТИИ завершено!!!!");
            counter += backupTS.Count;

            var backupDelimReady = _dbLayer.AccountDatabaseBackupRepository.Where(a => a.EventTrigger == CreateBackupAccountDatabaseTrigger.RegulationBackup
            && a.SourceType == AccountDatabaseBackupSourceType.GoogleCloud &&  
            a.AccountDatabase.State == "Ready").ToList();
            Console.WriteLine($"Получили список бекапов баз на разделителях в статусе READY {backupDelimReady.Count}.");
            DeleteAccountDatabaseBackup(backupDelimReady);
            Console.WriteLine("Удаление бекапов баз на разделителях в статусе READY завершено!!!!");
            counter += backupDelimReady.Count;
            
            var backupDelimDeleteFromCloud = _dbLayer.AccountDatabaseBackupRepository.Where(a => a.EventTrigger == CreateBackupAccountDatabaseTrigger.RegulationBackup
            && a.SourceType == AccountDatabaseBackupSourceType.GoogleCloud &&
            a.AccountDatabase.State == "DeletedFromCloud").ToList();
            Console.WriteLine($"Получили список бекапов баз на разделителях в статусе DeletedFromCloud {backupDelimDeleteFromCloud.Count}.");
            DeleteAccountDatabaseBackup(backupDelimDeleteFromCloud);
            Console.WriteLine("Удаление бекапов баз на разделителях в статусе DeletedFromCloud завершено!!!!");
            counter += backupDelimDeleteFromCloud.Count;
            
            Console.WriteLine($"Всего удалено {counter}");
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
            Console.WriteLine("Патч завершен");
            // "Патч завершился."
            Console.ReadLine();
        }


        /// <summary>
        /// Выполнить удаление бекапа
        /// </summary>
        /// <param name="backupMigration">Список бекапов</param>
        public static void DeleteAccountDatabaseBackup(List<AccountDatabaseBackup> backupMigration)
        {
            try
            {
                foreach (var backup in backupMigration)
                {
                    Console.WriteLine($"Начинаем удалять бекап для базы {backup.AccountDatabase.V82Name} с путем {backup.BackupPath}.");
                    _tombCleanerProvider.DeleteCloudAccountDatabaseBackup(backup.Id, true);
                    Console.WriteLine($"Удален бекап для базы {backup.AccountDatabase.V82Name} с путем {backup.BackupPath}.");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Удаление бекапа завершилось с ошибкой. {ex.Message}");
            }
        }
    }
    public static class DependencyConfiguration
    {
        public static IServiceCollection AddSerilogWithElastic(this IServiceCollection collection, IConfiguration conf)
        {
            var localLogsConfig = new LocalLogsConfigDto(conf["Logs:FileName"]);

            var elasticConfig = new ElasticConfigDto(
                    conf["Elastic:Uri"],
                    conf["Elastic:UserName"],
                    conf["Elastic:Password"],
                    conf["Elastic:ApiIndex"],
                    conf["Logs:AppName"]
                );

            SerilogConfig.ConfigureSerilog(localLogsConfig, elasticConfig);
            return collection;
        }
    }


}
