﻿using Clouds42.CoreWorker.AccountDatabasesOnDelimiters.JobWrappers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.AccountDatabasesOnDelimiters
{
    /// <summary>
    /// Регистратор зависимостей для сборки задач для баз на разделителях
    /// </summary>
    public static class DependencyInjections
    {
        /// <summary>
        /// Зарегистрировать зависимости
        /// </summary>
        /// <param name="services">Коллекция сервисов</param>
        /// <returns>Коллекцию сервисов</returns>
        public static IServiceCollection AddAccountDatabaseOnDelimitersServices(this IServiceCollection servicesCollection)
        {
            servicesCollection.AddTransient<IUploadAcDbZipFileThroughWebServiceJobWrapper, UploadAcDbZipFileThroughWebServiceJobWrapper>();

            return servicesCollection;
        }
    }
}
