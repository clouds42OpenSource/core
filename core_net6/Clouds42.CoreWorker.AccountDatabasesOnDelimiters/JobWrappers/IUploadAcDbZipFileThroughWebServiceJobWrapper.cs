﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.CoreWorker.AccountDatabasesOnDelimiters.JobWrappers
{
    /// <summary>
    /// Обработчик задачи для загрузки файла инф. базы через веб сервис
    /// </summary>
    public interface IUploadAcDbZipFileThroughWebServiceJobWrapper
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        ResultingJobWrapperBase<UploadAcDbZipFileThroughWebServiceParamsDto, UploadAcDbZipFileThroughWebServiceResultDto> Start(UploadAcDbZipFileThroughWebServiceParamsDto paramsJob);
    }
}
