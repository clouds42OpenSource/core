﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabasesOnDelimiters.JobWrappers
{
    /// <summary>
    /// Обработчик задачи для загрузки файла инф. базы через веб сервис
    /// </summary>
    internal class UploadAcDbZipFileThroughWebServiceJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ResultingJobWrapperBase<UploadAcDbZipFileThroughWebServiceParamsDto,
                UploadAcDbZipFileThroughWebServiceResultDto>(registerTaskInQueueProvider, dbLayer),
            IUploadAcDbZipFileThroughWebServiceJobWrapper
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override ResultingJobWrapperBase<UploadAcDbZipFileThroughWebServiceParamsDto, UploadAcDbZipFileThroughWebServiceResultDto> Start(UploadAcDbZipFileThroughWebServiceParamsDto paramsJob)
        {
            StartTask(paramsJob, $"Загрузка файла {paramsJob.UploadedFileId} в МС.");
            return this;
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.UploadAcDbZipFileThroughWebServiceJob;
    }
}
