﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using System;
using Clouds42.AccountDatabase.Contracts.UploadZipAccount.Interfaces;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.AccountDatabasesOnDelimiters
{
    /// <summary>
    /// Задача для загрузки файла инф. базы через веб сервис
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.UploadAcDbZipFileThroughWebServiceJob)]
    public class UploadAcDbZipFileThroughWebServiceJob(
        IUnitOfWork dbLayer,
        IUploadAccountDatabaseZipFileManager uploadAccountDatabaseZipFileManager)
        : CoreWorkerResultsJob<UploadAcDbZipFileThroughWebServiceParamsDto,
            UploadAcDbZipFileThroughWebServiceResultDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        /// <returns>Результат выполнения</returns>
        protected override UploadAcDbZipFileThroughWebServiceResultDto ExecuteResult(UploadAcDbZipFileThroughWebServiceParamsDto jobParams,
            Guid taskId, Guid taskQueueId)
        {
            var result = uploadAccountDatabaseZipFileManager.UploadZipFileByChunks(jobParams.UploadedFileId);

            return new UploadAcDbZipFileThroughWebServiceResultDto
            {
                IsSuccess = !result.Error,
                SmUploadedFileId = result.Result
            };
        }
    }
}
