﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения задач
    /// </summary>
    /// <typeparam name="TJob">Тип задачи.</typeparam>
    public abstract class TypingJobWrapperBase<TJob>(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase<TJob>(registerTaskInQueueProvider, dbLayer)
        where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задач</returns>
        public abstract TypingJobWrapperBase<TJob> Start();

        /// <summary>
        /// Зарегистрировать задачу в очареди для выполнения.
        /// </summary>
        /// <param name="comment">Коментарий к задаче.</param>
        /// <param name="dateTimeDelayOperation">Дата-время отложенного выполнения задачи. Задача не будет выполнена раньше чем установлена отложенная дата.</param>
        /// <param name="createTaskModelFunc">Функция которая выполняет создание модели регистрации задачи</param>
        /// <returns>Id созданной задачи</returns>
        protected Guid RegisterTask(string comment, DateTime? dateTimeDelayOperation,
            Func<ParametrizationModelDto> createTaskModelFunc)
            => registerTaskInQueueProvider.RegisterTask(GetTask().ID, comment, dateTimeDelayOperation,
                createTaskModelFunc());
    }
}
