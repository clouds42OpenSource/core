﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <inheritdoc />
    /// <summary>
    /// Прослойка выполнения параметризованных задач с синхронизацией.
    /// </summary>
    /// <typeparam name="TParams">Тип модели параметров.</typeparam>
    /// <typeparam name="TJob">Тип задачи.</typeparam>
    public abstract class SynchronizationParamsJobWrapperBase<TJob, TParams>(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingParamsJobWrapperBase<TJob, TParams>(registerTaskInQueueProvider, dbLayer)
        where TParams : class
        where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Построить ключ синхронизации.
        /// </summary>
        /// <param name="paramsJob">Входящая модель.</param>
        /// <returns>Ключ синхронизации.</returns>
        protected abstract string CreateSynchronizationKey(TParams paramsJob);

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="taskParams">Параметры задачи.</param>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected override void StartTask(TParams taskParams, string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = RegisterTask(taskParams, comment, dateTimeDelayOperation,
                tp => new ParametrizationModelDto(tp, CreateSynchronizationKey(taskParams)));


    }
}
