﻿using System;
using System.Linq;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения параметризованных задач
    /// </summary>
    /// <typeparam name="TJob">Тип джобы</typeparam>
    public abstract class JobWrapperBase<TJob>(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer)
        where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType
            => throw new NotImplementedException("Not supported");

        /// <summary>
        /// Получить данные по запускаемой задаче.
        /// </summary>
        /// <returns></returns>
        protected override Domain.DataModels.CoreWorkerTask GetTask()
        {
            var taskName = typeof(TJob).GetCoreWorkerTaskName();
            Domain.DataModels.CoreWorkerTask task = default;

            if (taskName!=null)            
                task = dbLayer.CoreWorkerTaskRepository.AsQueryableNoTracking().FirstOrDefault(x => x.TaskName == taskName);

            return task ?? throw new InvalidOperationException($"Не удалось получить данные о задаче {typeof(TJob).FullName}");
        }
    }
}
