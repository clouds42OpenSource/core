﻿using System;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения задач с синхронизацией
    /// </summary>
    /// <typeparam name="TJob">Тип задачи</typeparam>
    public abstract class SynchronizationJobWrapperBase<TJob>(
        IUnitOfWork dbLayer,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider)
        : TypingJobWrapperBase<TJob>(registerTaskInQueueProvider, dbLayer)
        where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Создать ключ синхронизации
        /// </summary>
        /// <returns>Ключ синхронизации</returns>
        protected abstract string CreateSynchronizationKey();

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected override void StartTask(string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = RegisterTask(comment, dateTimeDelayOperation,
                () => new ParametrizationModelDto(new EmptyTaskParametersDto(), CreateSynchronizationKey()));
    }
}
