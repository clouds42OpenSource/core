﻿using Clouds42.CoreWorker.BaseJobs;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения задач
    /// </summary>
    /// <typeparam name="TJob">Тип задачи</typeparam>
    public interface ITypingJobWrapper<TJob> where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения задачи</returns>
        TypingJobWrapperBase<TJob> Start();
    }
}