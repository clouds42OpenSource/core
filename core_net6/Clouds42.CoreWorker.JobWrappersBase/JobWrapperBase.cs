﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения параметризованных задач.
    /// </summary>
    public abstract class JobWrapperBase(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : IWorkerTask
    {

        protected Guid? TaskQueueId;

        /// <summary>
        /// Дождаться выполнения задачи.
        /// </summary>
        public virtual IWorkerTaskResult Wait(int timeoutMinutes = 20)
        {
            var cts = new CancellationTokenSource();
            cts.CancelAfter(TimeSpan.FromMinutes(timeoutMinutes));

            while (!cts.Token.IsCancellationRequested)
            {
                var taskQueue = GetQueueItem();
                if (taskQueue.Status == CloudTaskQueueStatus.Ready.ToString())
                {
                    return taskQueue;
                }

                if (taskQueue.Status == CloudTaskQueueStatus.Error.ToString())
                {
                    throw new InvalidOperationException(taskQueue.Comment);
                }

                Task.Delay(5000, cts.Token).Wait(cts.Token);
            }

            throw new TimeoutException("Время ожидания выполнения задачи истекло.");
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected abstract CoreWorkerTaskType TaskType { get; }

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected virtual void StartTask(string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = registerTaskInQueueProvider
                .RegisterTask(GetTask().ID, comment, dateTimeDelayOperation, null);

        /// <summary>
        /// Получить задачу из пула.
        /// </summary>
        protected CoreWorkerTasksQueue GetQueueItem()
        {
            if (!TaskQueueId.HasValue)
                throw new InvalidOperationException("Невозможно получить результат у незапустившейся задачи.");

            var taskQueue = dbLayer.CoreWorkerTasksQueueRepository
                    .AsQueryableNoTracking()
                    .FirstOrDefault(q => q.Id == TaskQueueId);

            return taskQueue ?? throw new InvalidOperationException($"не удалось получить задачу воркера по номеру {TaskQueueId}");
        }

        /// <summary>
        /// Получить данные по запускаемой задаче.
        /// </summary>
        /// <returns></returns>
        protected virtual Domain.DataModels.CoreWorkerTask GetTask()
        {
            var task = dbLayer.CoreWorkerTaskRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(t => t.TaskName == TaskType.ToString());
            return task ?? throw new InvalidOperationException($"Не удалось получить данные о задаче {TaskType}");
        }
    }

}
