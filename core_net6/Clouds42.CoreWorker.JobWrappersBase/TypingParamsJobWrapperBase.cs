﻿using System;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <inheritdoc />
    /// <summary>
    /// Прослойка выполнения параметризованных задач
    /// </summary>
    /// <typeparam name="TParams">Тип модели параметров.</typeparam>
    /// <typeparam name="TJob">Тип задачи.</typeparam>
    public abstract class TypingParamsJobWrapperBase<TJob, TParams>(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase<TJob>(registerTaskInQueueProvider, dbLayer)
        where TParams : class
        where TJob : CoreWorkerJob
    {
        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="paramsJob">Параметры задачи.</param>
        public abstract TypingParamsJobWrapperBase<TJob, TParams> Start(TParams paramsJob);

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="taskParams">Параметры задачи.</param>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected virtual void StartTask(TParams taskParams, string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = RegisterTask(taskParams, comment, dateTimeDelayOperation,
                tp => new ParametrizationModelDto(tp));

        /// <summary>
        /// Зарегистрировать задачу в очареди для выполнения.
        /// </summary>
        /// <param name="taskParams">Параметры задачи.</param>
        /// <param name="comment">Коментарий к задаче.</param>
        /// <param name="dateTimeDelayOperation">Дата-время отложенного выполнения задачи. Задача не будет выполнена раньше чем установлена отложенная дата.</param>
        /// <param name="createTaskModelFunc">Функция которая выполняет создание модели регистрации задачи.</param>
        /// <returns></returns>
        protected Guid RegisterTask(TParams taskParams, string comment, DateTime? dateTimeDelayOperation,
            Func<TParams, ParametrizationModelDto> createTaskModelFunc)
            => registerTaskInQueueProvider.RegisterTask(GetTask().ID, comment, dateTimeDelayOperation,
                createTaskModelFunc(taskParams));
    }
}
