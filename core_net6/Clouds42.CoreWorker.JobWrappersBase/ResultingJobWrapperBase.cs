﻿using System;
using System.Threading.Tasks;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <summary>
    /// Прослойка выполнения параметризованных задач с результатом.
    /// </summary>
    /// <typeparam name="TParams">Тип модели параметров.</typeparam>
    /// <typeparam name="TResult">Тип модели результата.</typeparam>
    public abstract class ResultingJobWrapperBase<TParams, TResult>(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer), IWorkerTask<TResult>
        where TParams : class
        where TResult : class
    {
        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="paramsJob">Параметры задачи.</param>        
        public abstract ResultingJobWrapperBase<TParams, TResult> Start(TParams paramsJob);

        /// <summary>
        /// Дождаться выполнения задачи.
        /// </summary>
        /// <returns>Результат выполнения задачи.</returns>
        public virtual TResult WaitResult()
        {

            while (true)
            {
                var taskQueue = GetQueueItem();
                if (taskQueue.Status == CloudTaskQueueStatus.Ready.ToString())
                {

                    if (string.IsNullOrEmpty(taskQueue.ExecutionResult))
                        throw new InvalidOperationException("У задачи не удалось получить результат выполнения.");

                    var taskQueueResult = taskQueue.ExecutionResult.DeserializeFromJson<TResult>();
                    return taskQueueResult;
                }

                if (taskQueue.Status == CloudTaskQueueStatus.Error.ToString())
                    throw new InvalidOperationException(taskQueue.Comment);

                Task.Delay(5000).Wait();
            }
        }

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="paramsModel"></param>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected void StartTask(TParams paramsModel, string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = registerTaskInQueueProvider.RegisterTask(GetTask().ID, comment, dateTimeDelayOperation, new ParametrizationModelDto(paramsModel));
    }
}
