﻿using System;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappersBase
{
    /// <inheritdoc />
    /// <summary>
    /// Прослойка выполнения параметризованных задач.
    /// </summary>
    /// <typeparam name="TParams">Тип модели параметров.</typeparam>
    public abstract class ParametrizationJobWrapperBase<TParams>(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer)
        where TParams : class
    {
        /// <summary>
        /// Выполнить задачу.
        /// </summary>
        /// <param name="paramsJob">Параметры задачи.</param>
        public abstract IWorkerTask Start(TParams paramsJob);

        /// <summary>
        /// Запустить задачу.
        /// </summary>
        /// <param name="comment">Комментарий задачи.</param>
        /// <param name="dateTimeDelayOperation">Опционально. Дата и время когда требуется начать выполнение задачи. </param>
        protected virtual void StartTask(TParams paramsModel, string comment, DateTime? dateTimeDelayOperation = null)
            => TaskQueueId = registerTaskInQueueProvider.RegisterTask(GetTask().ID, comment, dateTimeDelayOperation, new ParametrizationModelDto(paramsModel));
    }
}
