﻿using System;
using System.Linq.Expressions;
using Clouds42.Configurations.Configurations;
using CommonLib.Extensions;
using SharpRaven;
using SharpRaven.Data;

namespace Clouds42.SentryProvider
{
    /// <summary>
    /// Провайдер регистрации ошибок в Sentry
    /// </summary>
    public class SentryRegisterProvider
    {

        private readonly Lazy<RavenClient> _ravenClient;

        public SentryRegisterProvider()
        {
            _ravenClient = new Lazy<RavenClient>(() =>
            {

                var sentryActivationUrl = CloudConfigurationProvider.Sentry.GetActivationUrl();
                var ravenClient = new RavenClient(sentryActivationUrl)
                {
                    Timeout = new TimeSpan(0, 0, 5),
                    IgnoreBreadcrumbs = false
                };
                return ravenClient;
            });
        }

        /// <summary>
        /// Захватить исключение
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="operationTitle">Заголовок операции</param>
        /// <param name="parameters">Список параметров</param>
        public void CaptureException(Exception exception, string operationTitle, params Expression<Func<object>>[] parameters)
        {
            var exceptionWrapper = new Exception(operationTitle, exception);
            exceptionWrapper.AttachData(parameters.ToDictionary());
            _ravenClient.Value.Capture(new SentryEvent(exceptionWrapper));
        }

        /// <summary>
        /// Захватить предупреждение
        /// </summary>
        /// <param name="exception">Исключение</param>
        /// <param name="operationTitle">Заголовок операции</param>
        /// <param name="parameters">Список параметров</param>
        public void CaptureWarning(Exception exception, string operationTitle, params Expression<Func<object>>[] parameters)
        {
            var warningMessage = $"{operationTitle}::{exception.GetFullInfo()}";
            _ravenClient.Value.Capture(new SentryEvent(warningMessage));
        }
    }
}
