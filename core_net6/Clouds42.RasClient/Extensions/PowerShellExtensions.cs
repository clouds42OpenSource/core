﻿using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;

namespace Clouds42.RasClient.Extensions
{
    /// <summary>
    /// Расширения для PowerShell
    /// </summary>
    public static class PowerShellExtensionsDto
    {
        /// <summary>
        /// Сконвертировать PowerShell объект в строку
        /// </summary>
        /// <param name="psObject">PowerShell объект</param>
        /// <returns>Строка данных</returns>
        public static string ConvertToString(this PSObject psObject)
            => (string)psObject.BaseObject;

        /// <summary>
        /// Сконвертировать PowerShell объект в пару ключ-значение
        /// </summary>
        /// <param name="psObject">PowerShell объект</param>
        /// <returns>Пара ключ-значение</returns>
        public static KeyValuePair<string, string> ToKeyValuePair(this PSObject psObject)
        {
            var psObjectValue = psObject.ConvertToString();
            var dataArray = psObjectValue.Split(':');

            var value = string.Join(":", dataArray.Skip(1).Take(dataArray.Length));
            return new KeyValuePair<string, string>(dataArray.First().Trim(), value.Trim());
        }
    }
}
