﻿using System.Management.Automation;

namespace Clouds42.RasClient.Models
{
    /// <summary>
    /// Элемент данных результата выполнения запроса
    /// </summary>
    public class ExecutionResultDataRowRasClientDto
    {
        /// <summary>
        /// Индекс
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Элемент данных
        /// </summary>
        public PSObject Element { get; set; }
    }
}
