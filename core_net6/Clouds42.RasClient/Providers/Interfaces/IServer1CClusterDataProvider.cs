﻿using System.Collections.Generic;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для получения списка кластеров на сервере 1С 
    /// </summary>
    public interface IServer1CClusterDataProvider
    {
        /// <summary>
        /// Получить список ID кластеров сервера 1С
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Список ID кластеров сервера 1С</returns>
        List<string> GetServer1CClusterIdList(ManageRasCommandParamsDto commandParams);
    }
}
