﻿using Clouds42.DataContracts.AccountDatabase.Interface;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для работы с RAS
    /// </summary>
    public interface IRasClientProvider
    {
        /// <summary>
        /// Закрыть сессий инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        void DropAccountDatabaseSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Признак что в инф. базе есть активные сеансы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>true - если в инф. базе есть хотя бы один активный сеанс</returns>
        bool AccountDatabaseHasActiveSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        void DropAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);

        /// <summary>
        /// Создать инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        void CreateAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);
    }
}
