﻿using Clouds42.DataContracts.AccountDatabase.Interface;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер создания инф. базы на кластере
    /// </summary>
    public interface ICreateAccountDatabaseOnClusterProvider
    {
        /// <summary>
        /// Создание инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        void CreateAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);
    }
}
