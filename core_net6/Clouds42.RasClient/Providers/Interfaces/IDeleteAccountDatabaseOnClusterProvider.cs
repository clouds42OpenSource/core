﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер удаления инф. базы на кластере
    /// </summary>
    public interface IDeleteAccountDatabaseOnClusterProvider
    {
        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabaseInfo">Информация об инф. базе на кластере</param>
        void DeleteAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            ClusterAccountDatabaseInfoDto clusterAccountDatabaseInfo);
    }
}
