﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using System.Collections.Generic;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для получения инф. баз на сервере предприятия
    /// </summary>
    public interface IAccountDatabaseOnServer1CDataProvider
    {
        /// <summary>
        /// Получить список инф. баз
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список инф. баз</returns>
        List<ClusterAccountDatabaseInfoDto> GetAccountDatabases(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise);
    }
}
