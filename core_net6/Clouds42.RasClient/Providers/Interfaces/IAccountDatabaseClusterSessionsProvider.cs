﻿using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;

namespace Clouds42.RasClient.Providers.Interfaces
{
    /// <summary>
    /// Провайдер сессий инф. баз на сервере 1С
    /// </summary>
    public interface IAccountDatabaseClusterSessionsProvider
    {
        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabase">Инф. база на кластере</param>
        /// <returns>Список сессий инф. базы</returns>
        List<ClusterAccountDatabaseSessionDto> GetAccountDatabaseSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            ClusterAccountDatabaseInfoDto clusterAccountDatabase);

        /// <summary>
        /// Закрыть сессию инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="accountDatabaseSession">Сессия инф. базы</param>
        void DropAccountDatabaseSession(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, 
            ClusterAccountDatabaseSessionDto accountDatabaseSession);
    }
}
