﻿using System.Linq;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Domain.Enums._1C;
using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер создания инф. базы на кластере
    /// </summary>
    internal class CreateAccountDatabaseOnClusterProvider(
        PowerShellExecuteCommandHelper powerShellExecuteCommandHelper,
        IServer1CClusterDataProvider server1CClusterDataProvider)
        : ICreateAccountDatabaseOnClusterProvider
    {
        /// <summary>
        /// Создание инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        public void CreateAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var clusterIdList = server1CClusterDataProvider.GetServer1CClusterIdList(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise, PowerShellCommandType.GetClusterList)
            );

                powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise,
                    PowerShellCommandType.CreateAccountDatabase, clusterIdList.First()));

        }

    }
}
