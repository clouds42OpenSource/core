﻿using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;
using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер для получения инф. баз на сервере предприятия
    /// </summary>
    internal class AccountDatabaseOnServer1CDataProvider(
        IServer1CClusterDataProvider server1CClusterDataProvider,
        PowerShellExecuteCommandHelper powerShellExecuteCommandHelper)
        : IAccountDatabaseOnServer1CDataProvider
    {
        /// <summary>
        /// Получить список инф. баз
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список инф. баз</returns>
        public List<ClusterAccountDatabaseInfoDto> GetAccountDatabases(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var accountDatabases = new List<ClusterAccountDatabaseInfoDto>();

            var clusterIdList = server1CClusterDataProvider.GetServer1CClusterIdList(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise, PowerShellCommandType.GetClusterList)
            );

            foreach (var clusterId in clusterIdList)
            {
                var executeResult =
                    powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(
                        RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise,
                            PowerShellCommandType.GetAccountDatabasesList, clusterId));

                var accountDatabasesList = GetAccountDatabasesList(executeResult.ToList(), clusterId);
                if (accountDatabasesList.Any())
                    accountDatabases.AddRange(accountDatabasesList);
            }

            return accountDatabases;
        }

        /// <summary>
        /// Получить список инф. баз на сервере 1С Предприятие
        /// </summary>
        /// <param name="executionResult">Результат выполнения PoweShell команды</param>
        /// <param name="clusterId">ID кластера</param>
        /// <returns>Список инф. баз на сервере 1С Предприятия</returns>
        private static List<ClusterAccountDatabaseInfoDto> GetAccountDatabasesList(List<PSObject> executionResult, string clusterId)
        {
            var accountDatabasesList = new List<ClusterAccountDatabaseInfoDto>();

            var result = PowerShellExecutionResultParser.GroupExecutionResult(executionResult);

            result.ForEach(dataItem =>
            {
                var clusterAccountDatabase = dataItem.DeserializeFromPowerShell<ClusterAccountDatabaseInfoDto>();
                clusterAccountDatabase.ClusterId = clusterId;

                accountDatabasesList.Add(clusterAccountDatabase);
            });

            return accountDatabasesList;
        }
    }
}
