﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;
using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер удаления инф. базы на кластере
    /// </summary>
    internal class DeleteAccountDatabaseOnClusterProvider(PowerShellExecuteCommandHelper powerShellExecuteCommandHelper)
        : IDeleteAccountDatabaseOnClusterProvider
    {
        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabaseInfo">Информация об инф. базе на кластере</param>
        public void DeleteAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            ClusterAccountDatabaseInfoDto clusterAccountDatabaseInfo)
            => powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise,
                    PowerShellCommandType.DeleteAccountDatabase, clusterAccountDatabaseInfo));
    }
}