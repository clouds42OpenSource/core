﻿using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер для получения списка кластеров на сервере 1С 
    /// </summary>
    internal class Server1CClusterDataProvider(PowerShellExecuteCommandHelper powerShellExecuteCommandHelper)
        : IServer1CClusterDataProvider
    {
        /// <summary>
        /// Получить список ID кластеров сервера 1С
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Список ID кластеров сервера 1С</returns>
        public List<string> GetServer1CClusterIdList(ManageRasCommandParamsDto commandParams)
        {
            var clusterIdList = new List<string>();

            var executeResult = powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(commandParams);

            var clustersList = PowerShellExecutionResultParser.GroupExecutionResult(executeResult.ToList());

            clustersList.ForEach(dataItem =>
            {
                var clusterIdKeyValuePair = PowerShellExecutionResultParser.GetDataRowKeyValuePair("cluster", dataItem);

                clusterIdList.Add(clusterIdKeyValuePair.Value);
            });

            return clusterIdList;
        }
    }
}
