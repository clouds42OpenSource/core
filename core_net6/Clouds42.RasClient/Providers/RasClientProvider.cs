﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер для работы с RAS
    /// </summary>
    public class RasClientProvider(
        IAccountDatabaseOnServer1CDataProvider accountDatabaseOnServer1CDataProvider,
        IAccountDatabaseClusterSessionsProvider accountDatabaseClusterSessionsProvider,
        IDeleteAccountDatabaseOnClusterProvider deleteAccountDatabaseOnClusterProvider,
        ICreateAccountDatabaseOnClusterProvider createAccountDatabaseOnClusterProvider,
        ICloudLocalizer cloudLocalizer,
        ILogger42 logger)
        : IRasClientProvider
    {
        /// <summary>
        /// Закрыть сессий инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        public void DropAccountDatabaseSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var acDbSessions = GetAccountDatabaseSessions(accountDatabaseEnterprise);

            acDbSessions.ForEach(session =>
            {
                accountDatabaseClusterSessionsProvider.DropAccountDatabaseSession(accountDatabaseEnterprise,
                    session);
            });
        }

        /// <summary>
        /// Признак что в инф. базе есть активные сеансы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>true - если в инф. базе есть хотя бы один активный сеанс</returns>
        public bool AccountDatabaseHasActiveSessions(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
            => GetAccountDatabaseSessions(accountDatabaseEnterprise).Any();

        /// <summary>
        /// Удалить инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        public void DropAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var acDbOnClusterInfo = GetAccountDatabaseOnCluster(accountDatabaseEnterprise);
            if (acDbOnClusterInfo == null)
                return;

            DropAccountDatabaseSessions(accountDatabaseEnterprise);
            deleteAccountDatabaseOnClusterProvider.DeleteAccountDatabase(accountDatabaseEnterprise, acDbOnClusterInfo);
        }

        /// <summary>
        /// Создать инф. базу
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        public void CreateAccountDatabase(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var acDbOnClusterInfo = GetAccountDatabaseOnCluster(accountDatabaseEnterprise);
            if (acDbOnClusterInfo != null)
            {
                logger.Trace($"База {accountDatabaseEnterprise.AccountDatabase.V82Name} на кластере уже существует.");
                return;
            }

            logger.Trace($"начало создания базы {accountDatabaseEnterprise.AccountDatabase.V82Name} на кластере.");
            createAccountDatabaseOnClusterProvider.CreateAccountDatabase(accountDatabaseEnterprise);
        }

        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список сессий инф. базы</returns>
        private List<ClusterAccountDatabaseSessionDto> GetAccountDatabaseSessions(
            IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            try
            {
                var on1CEnterpriseServerPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.On1CEnterpriseServer,
                    accountDatabaseEnterprise
                        .AccountDatabase.AccountId);

                var acDbOnClusterInfo = GetAccountDatabaseOnCluster(accountDatabaseEnterprise);
                if (acDbOnClusterInfo == null)
                    throw new NotFoundException(
                        $"Инф. база {accountDatabaseEnterprise.AccountDatabase.V82Name} не найдена" +
                        $" {on1CEnterpriseServerPhrase} {accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress}");

                var clusterAccountDatabase = GetAccountDatabaseOnCluster(accountDatabaseEnterprise);
                var accountDatabaseSessions =
                    accountDatabaseClusterSessionsProvider.GetAccountDatabaseSessions(accountDatabaseEnterprise,
                        clusterAccountDatabase);

                TraceLog(accountDatabaseEnterprise, accountDatabaseSessions);

                return accountDatabaseSessions;
            }
            catch (Exception ex)
            {
                var errorMessage = TraceErrorLog(accountDatabaseEnterprise, ex);
                throw new InvalidOperationException(errorMessage);
            }
        }

        /// <summary>
        /// Получить инф. базу на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Инф. база на кластере</returns>
        private ClusterAccountDatabaseInfoDto GetAccountDatabaseOnCluster(
            IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var accountDatabasesOnServerList = GetAccountDatabasesOnCluster(accountDatabaseEnterprise);

            var clusterAccountDatabase = accountDatabasesOnServerList.FirstOrDefault(acDb =>
                                             acDb.AccountDatabaseV82Name ==
                                             accountDatabaseEnterprise.AccountDatabase.V82Name);

            if (clusterAccountDatabase != null)
            {
                logger.Trace($@"Инф. база {accountDatabaseEnterprise.AccountDatabase.V82Name} найдена
                            на сервере предприятия {accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress}.
                            ID инф. базы на кластере: {clusterAccountDatabase.AccountDatabaseId}, ID кластера: {clusterAccountDatabase.ClusterId}");
            }

            return clusterAccountDatabase;
        }

        /// <summary>
        /// Получить список инф. баз на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список инф. баз на кластере</returns>
        private List<ClusterAccountDatabaseInfoDto> GetAccountDatabasesOnCluster(
            IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
            => accountDatabaseOnServer1CDataProvider.GetAccountDatabases(accountDatabaseEnterprise);

        /// <summary>
        /// Записать ошибку в лог
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="exception">Исключение</param>
        /// <returns>Сообщение об ошибке</returns>
        private string TraceErrorLog(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, Exception exception)
        {
            var on1CEnterpriseServerPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.On1CEnterpriseServer,
                accountDatabaseEnterprise
                    .AccountDatabase.AccountId);

            var errorMessage =
                $@"При получении активных сеансов в инф. базе {accountDatabaseEnterprise.AccountDatabase.V82Name} 
                            {on1CEnterpriseServerPhrase} {accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress} возникла ошибка. 
                            Причина: {exception.GetFullInfo(false)}";

            logger.Warn(errorMessage);
            return errorMessage;
        }

        /// <summary>
        /// Записать результат в лог
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabaseSessions">Список сеансов в инф. базе</param>
        private void TraceLog(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            List<ClusterAccountDatabaseSessionDto> clusterAccountDatabaseSessions)
        {
            var on1CEnterpriseServerPhrase = cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.On1CEnterpriseServer,
                accountDatabaseEnterprise
                    .AccountDatabase.AccountId);

            var message = $@"Активные сеансы в инф. базе {accountDatabaseEnterprise.AccountDatabase.V82Name} 
                            {on1CEnterpriseServerPhrase} {accountDatabaseEnterprise.EnterpriseServer.ConnectionAddress} получены успешно. 
                            Количество: {clusterAccountDatabaseSessions.Count}.
                            Список сеансов: ";

            clusterAccountDatabaseSessions.ForEach(session =>
            {
                message +=
                    $" -- ID сеанса: {session.SessionId}, дата начала: {session.StartDateTime}, дата последней активности: {session.LastActivity};";
            });

            logger.Trace(message);
        }
    }
}
