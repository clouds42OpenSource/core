﻿using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;
using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers.Interfaces;

namespace Clouds42.RasClient.Providers
{
    /// <summary>
    /// Провайдер сессий инф. баз на сервере 1С
    /// </summary>
    internal class AccountDatabaseClusterSessionsProvider(PowerShellExecuteCommandHelper powerShellExecuteCommandHelper)
        : IAccountDatabaseClusterSessionsProvider
    {
        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabase">Инф. база на кластере</param>
        /// <returns>Список сессий инф. базы</returns>
        public List<ClusterAccountDatabaseSessionDto> GetAccountDatabaseSessions(
            IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, ClusterAccountDatabaseInfoDto clusterAccountDatabase)
        {
            var executeResult = powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise,
                    PowerShellCommandType.GetAccountDatabaseSessions, clusterAccountDatabase));

            return GetAccountDatabaseSessionList(executeResult.ToList(), clusterAccountDatabase);
        }

        /// <summary>
        /// Закрыть сессию инф. базы на кластере
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="accountDatabaseSession">Сессия инф. базы</param>
        public void DropAccountDatabaseSession(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, 
            ClusterAccountDatabaseSessionDto accountDatabaseSession)
        {
            powerShellExecuteCommandHelper.ExecuteRasPowerShellScript(
                RasClientCommandParamsBuilder.Build(accountDatabaseEnterprise,
                    PowerShellCommandType.DropAccountDatabaseSession, accountDatabaseSession));
        }

        /// <summary>
        /// Получить список сессий инф. базы
        /// </summary>
        /// <param name="executionResult">Результат выполнения PoweShell команды</param>
        /// <param name="clusterAccountDatabase">Инф. база на кластере</param>
        /// <returns>Список сессий инф. базы</returns>
        private static List<ClusterAccountDatabaseSessionDto> GetAccountDatabaseSessionList(
            List<PSObject> executionResult, ClusterAccountDatabaseInfoDto clusterAccountDatabase)
        {
            var accountDatabaseSessionList = new List<ClusterAccountDatabaseSessionDto>();

            var result = PowerShellExecutionResultParser.GroupExecutionResult(executionResult);

            result.ForEach(dataItem =>
            {
                var acDbSession = dataItem.DeserializeFromPowerShell<ClusterAccountDatabaseSessionDto>();
                acDbSession.ClusterId = clusterAccountDatabase.ClusterId;

                accountDatabaseSessionList.Add(acDbSession);
            });

            return accountDatabaseSessionList;
        }
    }
}
