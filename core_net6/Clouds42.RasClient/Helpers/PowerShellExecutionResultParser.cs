﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Runtime.Serialization;
using Clouds42.Common.Exceptions;
using Clouds42.RasClient.Extensions;
using Clouds42.RasClient.Models;

namespace Clouds42.RasClient.Helpers
{
    /// <summary>
    /// Парсер результата выполнения скрипта PowerShell
    /// </summary>
    internal static class PowerShellExecutionResultParser
    {
        /// <summary>
        /// Получить ключ-значение из объекта данных результата выполнения PowerShell скрипта
        /// </summary>
        /// <param name="keyName">Ключ для поиска</param>
        /// <param name="executionResultDataRow">Элемент данных результата выполнения запроса</param>
        /// <returns>Ключ-значение из объекта данных</returns>
        public static KeyValuePair<string, string> GetDataRowKeyValuePair(string keyName, IGrouping<int, ExecutionResultDataRowRasClientDto> executionResultDataRow)
        {
            var dataRow =
                executionResultDataRow.FirstOrDefault(item => item.Element.ConvertToString().Contains(keyName))
                ?? throw new NotFoundException($"Не удалось получить элемент данных по ключу {keyName}");

            return dataRow.Element.ToKeyValuePair();
        }

        /// <summary>
        /// Сгруппировать результат выполнения PowerShell скрипта
        /// </summary>
        /// <param name="executionResult">Результат выполнения PowerShell скрипта</param>
        /// <returns>Сгруппированный результат</returns>
        public static List<IGrouping<int, ExecutionResultDataRowRasClientDto>> GroupExecutionResult(List<PSObject> executionResult)
            => executionResult.Select((psObject, iterator) =>
                    new ExecutionResultDataRowRasClientDto { Element = psObject, Index = iterator })
                .Where(e => !string.IsNullOrEmpty(e.Element.ConvertToString()))
                .GroupBy(e => executionResult.IndexOf(string.Empty, e.Index)).ToList();

        /// <summary>
        /// Десериализовать результат работы RAS в требуемый тип данных
        /// </summary>
        /// <typeparam name="TOut">Тип выходного параметра</typeparam>
        /// <param name="executionResultDataRow">Результат выполнения PowerShell скрипта</param>
        /// <returns>Требуемый тип данных</returns>
        public static TOut DeserializeFromPowerShell<TOut>(this IGrouping<int, ExecutionResultDataRowRasClientDto> executionResultDataRow) 
            where TOut : class
        {
            var result = Activator.CreateInstance<TOut>();

            var properties = typeof(TOut).GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(DataMemberAttribute)) && !string.IsNullOrEmpty(p.Name)).ToList();

            properties.ForEach(property =>
            {
                var dataMember =
                    ((DataMemberAttribute[]) property.GetCustomAttributes(typeof(DataMemberAttribute), true))
                    .FirstOrDefault();

                if (dataMember == null)
                    return;

                var value = GetDataRowKeyValuePair(dataMember.Name, executionResultDataRow);
                property.SetValue(result, Convert.ChangeType(value.Value, property.PropertyType));
            });

            return result;
        }
    }
}
