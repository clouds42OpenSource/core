﻿using System;
using System.Collections.Generic;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Logger;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.RasClient.Helpers
{
    /// <summary>
    /// Хэлпер для запуска PowerShell команд
    /// </summary>
    public class PowerShellExecuteCommandHelper(
        RasResultParser rasResultParser,
        ILogger42 logger)
    {
        /// <summary>
        /// Команда получения текущего процесса ras.exe
        /// </summary>
        private readonly string _commandForFetchRasProcess = CloudConfigurationProvider.Enterprise1C.RasClient.GetCommandForFetchRasProcess();

        /// <summary>
        /// Команда для завершения текущего процесса ras.exe
        /// </summary>
        private readonly string _stopRasApplicationCommand = CloudConfigurationProvider.Enterprise1C.RasClient.GetStopRasProcessCommand();

        /// <summary>
        /// Выполнить PowerShell скрипт для RAS
        /// </summary>
        /// <param name="commandParamsDc">Параметры команды</param>
        /// <returns>Результат выполнения</returns>
        public IEnumerable<PSObject> ExecuteRasPowerShellScript(ManageRasCommandParamsDto commandParamsDc)
        {
            var exceptionMessageBuilder = new StringBuilder();

            foreach (var server1C in commandParamsDc.Server1CNamesList)
            {
                try
                {
                    commandParamsDc.Server1CName = server1C.Trim();
                    var result = ExecuteScript(commandParamsDc);
                    return result;
                }
                catch (Exception ex)
                {
                    exceptionMessageBuilder.AppendLine($"{server1C}->{ex.GetFullInfo(false)}");
                }
            }

            logger.Warn($"При выполнении PowerShell скрипта произошла ошибка. Причина: {exceptionMessageBuilder}");
            throw new ClusterConnectionException(exceptionMessageBuilder.ToString());
        }

        /// <summary>
        /// Выполнить PowerShell скрипт для RAS
        /// </summary>
        /// <param name="commandParamsDc">Параметры команды</param>
        /// <returns>Результат выполнения</returns>
        private IEnumerable<PSObject> ExecuteScript(ManageRasCommandParamsDto commandParamsDc)
        {
            InitialSessionState runSpaceConfiguration = InitialSessionState.CreateDefault2();
            using Runspace runSpace = RunspaceFactory.CreateRunspace(runSpaceConfiguration);
            runSpace.Open();
            using var powerShell = PowerShell.Create(runSpace);
            try
            {
                var command = PowerShellCommandBuilder.Build(commandParamsDc);
                powerShell.AddScript(GetChangeWorkingDirectoryCommand(commandParamsDc.RunSpance));
                powerShell.AddScript(GetStartRasApplicationCommand(commandParamsDc))
                    .AddScript(_commandForFetchRasProcess)
                    .AddScript(command);

                foreach (var com in powerShell.Commands.Commands)
                    logger.Info($"Команда {com.CommandText} выполняемые powerShell");

                var result = powerShell.Invoke();
                logger.Info($"Ошибка выполнения powerShell {powerShell.HadErrors}");
                HandleExecutionResult(powerShell);
                logger.Info("Обработан результат powerShell");

                return result;
            }
            catch (Exception)
            {
                powerShell.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Получить команду для смены рабочей директории
        /// </summary>
        /// <param name="workingDirectory">Рабочая директория</param>
        /// <returns>Команда для смены рабочей директории</returns>
        private static string GetChangeWorkingDirectoryCommand(string workingDirectory)
            => PowerShellCommandBuilder.Build(new RunRasApplicationCommandParamsDto
            {
                CommandType = PowerShellCommandType.ChangeWorkingDirectory,
                RunSpance = workingDirectory
            });

        /// <summary>
        /// Получить команду для запуска RAS
        /// </summary>
        /// <param name="commandParamsDc">Параметры команды</param>
        /// <returns>Команда для запуска RAS</returns>
        private static string GetStartRasApplicationCommand(ManageRasCommandParamsDto commandParamsDc)
            => PowerShellCommandBuilder.Build(new RunRasApplicationCommandParamsDto
            {
                CommandType = PowerShellCommandType.RunRasApplication,
                RunSpance = commandParamsDc.RunSpance,
                ApplicationPath = CloudConfigurationProvider.Enterprise1C.RasClient.GetRasApplicationPath(),
                RasWorkingPort = commandParamsDc.RasWorkingPort,
                Server1CName = commandParamsDc.Server1CName,
                Server1CPort = commandParamsDc.Server1CPort
            });

        /// <summary>
        /// Обработать результат выполнения команды
        /// </summary>
        /// <param name="powerShell">Экземпляр PowerShell</param>
        private void HandleExecutionResult(PowerShell powerShell)
             {
            if (!powerShell.HadErrors)
            {
                StopRasProcess(powerShell);
                return;
            }

            var resultState = rasResultParser.HandleRasErrorResult(powerShell.Streams.Error);

            if (resultState.State == RasExecutionResultState.Success)
            {
                StopRasProcess(powerShell);
                return;
            }

            StopRasProcess(powerShell);
            rasResultParser.ThrowExceptionByState(resultState.State, resultState.ErrorMessage);
        }

        /// <summary>
        /// Остановить процесс ras.exe
        /// </summary>
        /// <param name="powerShell">Экземпляр PowerShell</param>
        private void StopRasProcess(PowerShell powerShell)
        {
            powerShell.AddScript(_stopRasApplicationCommand);
            powerShell.Invoke();
        }
    }
}
