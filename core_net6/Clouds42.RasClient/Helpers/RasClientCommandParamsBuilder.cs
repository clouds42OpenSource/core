﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.RasClient.Helpers
{
    /// <summary>
    /// Хэлпер для создания параметров выполнения команды RAS
    /// </summary>
    public static class RasClientCommandParamsBuilder
    {
        private static readonly string RacApplicationPath = CloudConfigurationProvider.Enterprise1C.RasClient.GetRacApplicationPath();
        private static readonly string RasWorkingPort = CloudConfigurationProvider.Enterprise1C.RasClient.GetDefaultRasWorkingPort();
        private static readonly string Server1CPort = CloudConfigurationProvider.Enterprise1C.RasClient.GetDefaultServer1CPort();
        private static readonly string DBUser = CloudConfigurationProvider.AccountDatabase.Cluster.GetSqlLogin();
        private static readonly string DBPassword = CloudConfigurationProvider.AccountDatabase.Cluster.GetSqlPassword();
        /// <summary>
        /// Создать параметры выполнения команды
        /// </summary>
        /// <param name="commandType">Тип команды</param>
        /// <param name="clusterId">ID кластера</param>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Параметры выполнения команды</returns>
        public static ManageRasCommandParamsDto Build(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, PowerShellCommandType commandType, string clusterId) 
            => new()
            {
                RunSpance = accountDatabaseEnterprise.PlatformPathX64,
                ApplicationPath = RacApplicationPath,
                RasWorkingPort = RasWorkingPort,
                Server1CPort = Server1CPort,
                CommandType = commandType,
                Server1CAdminLogin = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                Server1CAdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword,
                ClusterId = clusterId,
                Server1CNamesList = GetEnterpriseServers(accountDatabaseEnterprise),
                DatabaseName = accountDatabaseEnterprise.AccountDatabase.V82Name,
                ClusterAdminLogin = DBUser,
                ClusterAdminPassword = DBPassword,
                DBServerName = accountDatabaseEnterprise.SqlServer.ConnectionAddress
            };

        /// <summary>
        /// Создать параметры выполнения команды
        /// </summary>
        /// <param name="commandType">Тип команды</param>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Параметры выполнения команды</returns>
        public static ManageRasCommandParamsDto Build(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, PowerShellCommandType commandType)
            => new()
            {
                RunSpance = accountDatabaseEnterprise.PlatformPathX64,
                ApplicationPath = RacApplicationPath,
                RasWorkingPort = RasWorkingPort,
                Server1CPort = Server1CPort,
                CommandType = commandType,
                Server1CAdminLogin = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                Server1CAdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword,
                Server1CNamesList = GetEnterpriseServers(accountDatabaseEnterprise)
            };

        /// <summary>
        /// Создать параметры выполнения команды
        /// </summary>
        /// <param name="commandType">Тип команды</param>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="clusterAccountDatabase">Инф. база на кластере</param>
        /// <returns>Параметры выполнения команды</returns>
        public static ManageRasCommandParamsDto Build(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise, 
            PowerShellCommandType commandType, ClusterAccountDatabaseInfoDto clusterAccountDatabase)
            => new()
            {
                RunSpance = accountDatabaseEnterprise.PlatformPathX64,
                ApplicationPath = RacApplicationPath,
                RasWorkingPort = RasWorkingPort,
                Server1CPort = Server1CPort,
                Server1CNamesList = GetEnterpriseServers(accountDatabaseEnterprise),
                CommandType = commandType,
                Server1CAdminLogin = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                Server1CAdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword,
                ClusterId = clusterAccountDatabase.ClusterId,
                ClusterAccountDatabaseId = clusterAccountDatabase.AccountDatabaseId
            };

        /// <summary>
        /// Создать параметры выполнения команды
        /// </summary>
        /// <param name="commandType">Тип команды</param>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <param name="accountDatabaseSession">Сессия инф. базы на кластере</param>
        /// <returns>Параметры выполнения команды</returns>
        public static ManageRasCommandParamsDto Build(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise,
            PowerShellCommandType commandType, ClusterAccountDatabaseSessionDto accountDatabaseSession)
            => new()
            {
                RunSpance = accountDatabaseEnterprise.PlatformPathX64,
                ApplicationPath = RacApplicationPath,
                RasWorkingPort = RasWorkingPort,
                Server1CPort = Server1CPort,
                Server1CNamesList = GetEnterpriseServers(accountDatabaseEnterprise),
                CommandType = commandType,
                Server1CAdminLogin = accountDatabaseEnterprise.EnterpriseServer.AdminName,
                Server1CAdminPassword = accountDatabaseEnterprise.EnterpriseServer.AdminPassword,
                ClusterId = accountDatabaseSession.ClusterId,
                ClusterAccountDatabaseId = accountDatabaseSession.AccountDatabaseId,
                SessionId = accountDatabaseSession.SessionId
            };

        /// <summary>
        /// Получить список серверов из строки подключения.        
        /// </summary>
        /// <param name="accountDatabaseEnterprise">Данные серверной инф. базы</param>
        /// <returns>Список серверов из строки подключения</returns>
        private static List<string> GetEnterpriseServers(IAccountDatabaseEnterpriseDto accountDatabaseEnterprise)
        {
            var enterpriseServer = accountDatabaseEnterprise.EnterpriseServer;

            if (string.IsNullOrEmpty(enterpriseServer.ConnectionAddress))
                throw new InvalidOperationException($"Для сервера 1С {enterpriseServer.Name} не указан адрес подключения");

            return enterpriseServer.ConnectionAddress.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
