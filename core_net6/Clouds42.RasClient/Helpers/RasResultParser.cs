﻿using System;
using System.Collections.Generic;
using System.Management.Automation;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;
using Clouds42.Logger;

namespace Clouds42.RasClient.Helpers
{
    /// <summary>
    /// Парсер результата выполнения команды RAS
    /// </summary>
    public class RasResultParser(ILogger42 logger)
    {
        /// <summary>
        /// Справочник сопоставления известных сообщений с результатом выполнения команды
        /// </summary>
        private readonly Dictionary<RasExecutionResultState, List<string>> _logDictionary = new()
        {
            {
                RasExecutionResultState.AuthenticationFailed, ["Администратор кластера не аутентифицирован"]
            },
            {
                RasExecutionResultState.RasNotExistInPlatformPath, [
                    "Не удается выполнить команду из-за следующей ошибки: Не удается найти указанный файл",
                    "не распознано как имя командлета, функции, файла сценария или выполняемой программы"
                ]
            },
            {
                RasExecutionResultState.ConnectionError, [
                    "Ошибка установки соединения с кластером серверов",
                    "Сервис не поддерживается",
                    "Этот хост неизвестен",
                    "конечный компьютер отверг запрос на подключение",
                    "Подключение не установлено"
                ]
            },
            {
                RasExecutionResultState.ExecutionError, ["Не найдено ни одного сервера с размещенным сервисом"]
            }
        };

        /// <summary>
        /// Обработать ошибки выполнения команды RAS
        /// </summary>
        /// <param name="errorRecords">Список ошибок</param>
        /// <returns>Результат обработки</returns>
        public RasExecutionResultDto HandleRasErrorResult(PSDataCollection<ErrorRecord> errorRecords)
        {
            var resultState = new RasExecutionResultDto
            {
                State = RasExecutionResultState.Success
            };

            foreach (var errorRecord in errorRecords)
            {
                resultState = HandleRecord(errorRecord.ToString());
                logger.Trace($"Сообщение в потоке ошибок: {errorRecord}");
            }

            return resultState;
        }

        /// <summary>
        /// Инициализировать исключение на основании результата выполнения команды RAS
        /// </summary>
        /// <param name="resultState">Результат выполнения команды</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public void ThrowExceptionByState(RasExecutionResultState resultState, string errorMessage)
        {
            if (resultState == RasExecutionResultState.AuthenticationFailed)
                throw new ClusterAuthenticateException(errorMessage);

            throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Обработать запись об ошибке
        /// </summary>
        /// <param name="errorRecord">Запись об ошибке</param>
        /// <returns>Результат обработки</returns>
        private RasExecutionResultDto HandleRecord(string errorRecord)
        {
            foreach (RasExecutionResultState resultState in Enum.GetValues(typeof(RasExecutionResultState)))
            {
                if (LineHasIncludes(errorRecord, resultState))
                    return new RasExecutionResultDto
                    {
                        State = resultState,
                        ErrorMessage = errorRecord
                    };
            }

            return new RasExecutionResultDto
            {
                State = RasExecutionResultState.Success
            };
        }

        /// <summary>
        /// Линия содержит вхождения по состоянию.
        /// </summary>
        /// <param name="line">Входная строка</param>
        /// <param name="resultState">Результат выполнения команды</param> 
        private bool LineHasIncludes(string line, RasExecutionResultState resultState)
        {
            if (!_logDictionary.ContainsKey(resultState))
                return false;

            var logDictionary = _logDictionary[resultState];

            foreach (var val in logDictionary)
            {
                if (line.IndexOf(val, StringComparison.OrdinalIgnoreCase) >= 0)
                    return true;
            }
            return false;
        }
    }
}
