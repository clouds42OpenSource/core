﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Cloud42Services.Server1CManagement;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.RasClient.Helpers
{
    /// <summary>
    /// Хэлпер для построения PowerShell команд 
    /// </summary>
    public static class PowerShellCommandBuilder
    {
        private static readonly Dictionary<PowerShellCommandType, Func<IPowerShellExecuteCommandParamsDto, string>> MapTypeToCommandBuildMethod =
            new()
            {
                { PowerShellCommandType.ChangeWorkingDirectory, GetChangeWorkingDirectoryCommand },
                { PowerShellCommandType.RunRasApplication, GetStartRasApplicationCommand },
                { PowerShellCommandType.GetClusterList, GetClusterListCommand },
                { PowerShellCommandType.GetAccountDatabasesList, GetAccountDatabasesListCommand },
                { PowerShellCommandType.GetAccountDatabaseSessions, GetAccountDatabaseSessionsCommand },
                { PowerShellCommandType.DropAccountDatabaseSession, GetDropAccountDatabaseSessionCommand },
                { PowerShellCommandType.DeleteAccountDatabase, GetDeleteAccountDatabaseCommand },
                { PowerShellCommandType.CreateAccountDatabase, GetCreateAccountDatabaseCommand }
            };

        /// <summary>
        /// Построить команду
        /// </summary>
        /// <param name="commandParamsDc">Параметры команды</param>
        /// <returns>Команда</returns>
        public static string Build(IPowerShellExecuteCommandParamsDto commandParamsDc)
        {
            if (!MapTypeToCommandBuildMethod.ContainsKey(commandParamsDc.CommandType))
                throw new InvalidOperationException($"Невозможно определить метод для построения команды с типом {commandParamsDc.CommandType}");

            return MapTypeToCommandBuildMethod[commandParamsDc.CommandType](commandParamsDc);
        }

        /// <summary>
        /// Получить команду для смены рабочей директории
        /// </summary>
        /// <param name="powerShellExecuteCommandParamsDc">Параметры команды</param>
        /// <returns>Команда для смены рабочей директории</returns>
        private static string GetChangeWorkingDirectoryCommand(
            IPowerShellExecuteCommandParamsDto powerShellExecuteCommandParamsDc)
            => $"cd \"{powerShellExecuteCommandParamsDc.RunSpance}\"";

        /// <summary>
        /// Получить команду для запуска RAS
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для запуска RAS</returns>
        private static string GetStartRasApplicationCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<RunRasApplicationCommandParamsDto>();
            return $"Start-Process -NoNewWindow -FilePath \"{rasCommandParams.ApplicationPath}\" " +
                   $"-ArgumentList \"cluster --port={rasCommandParams.RasWorkingPort}" +
                   $" {rasCommandParams.Server1CName}:{rasCommandParams.Server1CPort}\"";
        }

        /// <summary>
        /// Получить команду для вывода списка кластеров
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для вывода списка кластеров</returns>
        private static string GetClusterListCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<RunRasApplicationCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} cluster list";
        }

        /// <summary>
        /// Получить команду для вывода списка инф. баз
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для вывода списка инф. баз</returns>
        private static string GetAccountDatabasesListCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<ManageRasCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} infobase summary list " +
                   $"--cluster={rasCommandParams.ClusterId} " +
                   GetClusterAdminCredetialsParamsString(rasCommandParams);
        }

        /// <summary>
        /// Получить команду для вывода списка сессий инф. базы
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для вывода списка сессий инф. базы</returns>
        private static string GetAccountDatabaseSessionsCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<ManageRasCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} session list " +
                   $"--cluster={rasCommandParams.ClusterId} " +
                   GetClusterAdminCredetialsParamsString(rasCommandParams) +
                   $"--infobase=\"{rasCommandParams.ClusterAccountDatabaseId}\"";
        }

        /// <summary>
        /// Получить команду для завершения сессий инф. базы
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для завершения сессий инф. базы</returns>
        private static string GetDropAccountDatabaseSessionCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<ManageRasCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} session " +
                   $"--cluster={rasCommandParams.ClusterId} " +
                   GetClusterAdminCredetialsParamsString(rasCommandParams) +
                   "terminate " +
                   $"--session={rasCommandParams.SessionId} " +
                   $"--error-message=\"Terminate by {rasCommandParams.Server1CAdminLogin}\" ";
        }

        /// <summary>
        /// Получить команду для удаления инф. базы
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для удаления инф. базы</returns>
        private static string GetDeleteAccountDatabaseCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<ManageRasCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} infobase drop " +
                   $"--cluster={rasCommandParams.ClusterId} " +
                   GetClusterAdminCredetialsParamsString(rasCommandParams) +
                   $"--infobase=\"{rasCommandParams.ClusterAccountDatabaseId}\" ";
        }

        /// <summary>
        /// Получить команду для создания инф. базы
        /// </summary>
        /// <param name="commandParams">Параметры команды</param>
        /// <returns>Команда для удаления инф. базы</returns>
        private static string GetCreateAccountDatabaseCommand(IPowerShellExecuteCommandParamsDto commandParams)
        {
            var rasCommandParams = commandParams.CastToSpecificCommandParams<ManageRasCommandParamsDto>();
            return $"{rasCommandParams.ApplicationPath} infobase " +
                   $"--cluster={rasCommandParams.ClusterId} create " +
                   GetClusterAdminCredetialsParamsString(rasCommandParams) +
                   "--create-database " +
                   $"--name=\"{rasCommandParams.DatabaseName}\" " +
                   "--dbms=\"MSSQLServer\" " +
                   $"--db-server=\"{rasCommandParams.DBServerName}\" " +
                   $"--db-name=\"db_{rasCommandParams.DatabaseName}\"  " +
                   "--locale=\"ru\" " +
                   $"--descr=\"{rasCommandParams.DatabaseName}\" " +
                   "--date-offset=\"2000\" " +
                   "--scheduled-jobs-deny=\"off\" " +
                   $"--db-user=\"{rasCommandParams.ClusterAdminLogin}\" " +
                   $"--db-pwd=\"{rasCommandParams.ClusterAdminPassword}\" " +
                   "--license-distribution=allow";
        }

        /// <summary>
        /// Получить строку параметров
        /// с данными авторизации администратора кластера
        /// </summary>
        /// <param name="rasCommandParams">Параметры команды</param>
        /// <returns>
        /// Строка параметров с данными
        /// авторизации администратора кластера
        /// </returns>
        private static string GetClusterAdminCredetialsParamsString(ManageRasCommandParamsDto rasCommandParams)
            => $"--cluster-user=\"{rasCommandParams.Server1CAdminLogin}\" " +
               $"--cluster-pwd=\"{rasCommandParams.Server1CAdminPassword}\" ";
    }   
}
