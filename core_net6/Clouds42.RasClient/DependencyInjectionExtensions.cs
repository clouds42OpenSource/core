﻿using Clouds42.RasClient.Helpers;
using Clouds42.RasClient.Providers;
using Clouds42.RasClient.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.RasClient
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddRasClient(this IServiceCollection services)
        {
            services.AddTransient<IRasClientProvider, RasClientProvider>();
            services.AddTransient<IServer1CClusterDataProvider, Server1CClusterDataProvider>();
            services.AddTransient<IAccountDatabaseOnServer1CDataProvider, AccountDatabaseOnServer1CDataProvider>();
            services.AddTransient<IAccountDatabaseClusterSessionsProvider, AccountDatabaseClusterSessionsProvider>();
            services.AddTransient<IDeleteAccountDatabaseOnClusterProvider, DeleteAccountDatabaseOnClusterProvider>();
            services.AddTransient<ICreateAccountDatabaseOnClusterProvider, CreateAccountDatabaseOnClusterProvider>();
            services.AddTransient<PowerShellExecuteCommandHelper>();
            services.AddTransient<RasResultParser>();
            return services;
        }
    }
}
