﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.SendBackupAccountDatabaseToTomb
{
    /// <summary>
    /// Джоба по отправке бэкапа информационной базы в склеп.
    /// </summary>
    public class SendBackupAccountDatabaseToTombJobWrapper :
        ParametrizationJobWrapperBase<SendBackupAccountDatabaseToTombJobParamsDto>,
        ISendBackupAccountDatabaseToTombJobWrapper
    {

        /// <summary>
        /// Запустить задачу по отправке бэкапа информационной базы в склеп.
        /// </summary>        
        public SendBackupAccountDatabaseToTombJobWrapper(
            IRegisterTaskInQueueProvider registerTaskInQueueProvider,
            IUnitOfWork dbLayer) : base(registerTaskInQueueProvider, dbLayer)
        {
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.SendBackupAccountDatabaseToTombJob;

        /// <summary>
        /// Отправить бэкап информационной базы в склеп.
        /// </summary>        
        public override ParametrizationJobWrapperBase<SendBackupAccountDatabaseToTombJobParamsDto> Start(SendBackupAccountDatabaseToTombJobParamsDto paramsJob)
        {            
            StartTask(paramsJob, "Отправить бэкап в склеп.");
            return this;
        }

        public ParametrizationJobWrapperBase<SendBackupAccountDatabaseToTombJobParamsDto> Start(SendBackupAccountDatabaseToTombJobParamsDto paramsJob, DateTime dateTimeDelayOperation)
        {            
            StartTask(paramsJob, "Отправить бэкап в склеп.", dateTimeDelayOperation);
            return this;
        }

    }
}
