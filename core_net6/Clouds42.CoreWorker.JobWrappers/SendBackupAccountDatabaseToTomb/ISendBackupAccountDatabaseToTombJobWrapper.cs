﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;

namespace Clouds42.CoreWorker.JobWrappers.SendBackupAccountDatabaseToTomb
{
    public interface ISendBackupAccountDatabaseToTombJobWrapper
    {

        /// <summary>
        /// Отправить бэкап информационной базы в склеп.
        /// </summary>        
        ParametrizationJobWrapperBase<SendBackupAccountDatabaseToTombJobParamsDto> Start(SendBackupAccountDatabaseToTombJobParamsDto paramsJob);

        /// <summary>
        /// Отправить бэкап информационной базы в склеп.
        /// </summary> 
        ParametrizationJobWrapperBase<SendBackupAccountDatabaseToTombJobParamsDto> Start(SendBackupAccountDatabaseToTombJobParamsDto paramsJob, DateTime dateTimeDelayOperation);
    }
}
