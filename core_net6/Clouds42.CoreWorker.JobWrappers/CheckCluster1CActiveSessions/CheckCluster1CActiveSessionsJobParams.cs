﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.CheckCluster1CActiveSessions
{

    /// <summary>
    /// Модель входных параметров джобы по проверке наличия активных сессий в базе 1С на кластере.
    /// </summary>
    public class CheckCluster1CActiveSessionsJobParams
    {
        public Guid AccountDatabaseId { get; set; }
    }
}