﻿namespace Clouds42.CoreWorker.JobWrappers.CheckCluster1CActiveSessions
{

    /// <summary>
    /// Модель результата работы джобы по проверке наличия активных сессий в базе 1С на кластере.
    /// </summary>
    public class CheckCluster1CActiveSessionsJobResult
    {
        public bool HasActiveSessions { get; set; }
    }
}