﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;

namespace Clouds42.CoreWorker.JobWrappers.PublishDatabase
{
    public interface IRepublishSegmentDatabasesJobWrapper
    {        
        ParametrizationJobWrapperBase<RepublishSegmentDatabasesJobParamsDto> Start(
            RepublishSegmentDatabasesJobParamsDto paramsJob);
    }
}
