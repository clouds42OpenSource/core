﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.PublishDatabase
{
    public class RepublishSegmentDatabasesJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<RepublishSegmentDatabasesJobParamsDto>(registerTaskInQueueProvider, dbLayer), IRepublishSegmentDatabasesJobWrapper
    {
        /// <inheritdoc />
        /// <summary>
        /// Тип задачи - Переопубликация баз
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RepublishSegmentDatabasesJob;

        /// <inheritdoc />
        /// <summary>
        /// Запустить таску публикации баз
        /// </summary>
        /// <param name="paramsJob">Модель параметров</param>
        public override ParametrizationJobWrapperBase<RepublishSegmentDatabasesJobParamsDto> Start(RepublishSegmentDatabasesJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Переопубликация баз сегмента.");
            return this;
        }
    }
}
