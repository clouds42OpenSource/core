﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.AccountDatabaseSupport
{
    /// <summary>
    /// Обработчик для задачи автообновления инф. базы
    /// </summary>
    public class AccountDatabaseAutoUpdateJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ResultingJobWrapperBase<AccountDatabaseAutoUpdateParamsDto, AccountDatabaseAutoUpdateResultDto>(
            registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID задачи в очереди
        /// </summary>
        public Guid? AuTaskQueueId { get; set; }

        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Прослойка выполнения</returns>
        public override ResultingJobWrapperBase<AccountDatabaseAutoUpdateParamsDto, AccountDatabaseAutoUpdateResultDto> Start(AccountDatabaseAutoUpdateParamsDto paramsJob)
        {
            StartTask(paramsJob, $"Автообновление инф. базы {paramsJob.AccountDatabaseId}.");
            return this;
        }

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения</returns>
        public void StartTask()
        {
            Start(new AccountDatabaseAutoUpdateParamsDto
            {
                AccountDatabaseId = AccountDatabaseId
            });

            AuTaskQueueId = TaskQueueId;
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType 
            => CoreWorkerTaskType.AccountDatabaseAutoUpdateJob;
    }
}
