﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.AccountDatabaseSupport
{
    /// <summary>
    /// Обработчик задачи для управления моделью восстановления для инф. баз
    /// </summary>
    public class ManageAccountDatabaseRestoreModelJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns>Прослойка выполнения</returns>
        public JobWrapperBase Start()
        {
            StartTask("Управление моделью восстановления для инф. баз.");
            return this;
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.ManageAccountDatabaseRestoreModelJob;
    }
}
