﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.BackupFileFetching
{
    /// <summary>
    /// Задача по загрузке бэкапа базы на разделителях
    /// </summary>
    public class BackupFileFetchingJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<BackupFileFetchingJobParamsDto>(registerTaskInQueueProvider, dbLayer),
            IBackupFileFetchingJobWrapper
    {
        /// <summary>
        /// Тип задачи - загрузка бэкапа базы на разделителях
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.BackupFileFetchingJob;

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи</param>
        public override ParametrizationJobWrapperBase<BackupFileFetchingJobParamsDto> Start(BackupFileFetchingJobParamsDto paramsJob)
        {
            StartTask(paramsJob, "Загрузка файла бэкапа инф. базы");
            return this;
        }
    }
}
