﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;

namespace Clouds42.CoreWorker.JobWrappers.BackupFileFetching
{
    /// <summary>
    /// Задача по загрузке файла бэкапа
    /// </summary>
    public interface IBackupFileFetchingJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи</param>
        ParametrizationJobWrapperBase<BackupFileFetchingJobParamsDto> Start(
            BackupFileFetchingJobParamsDto paramsJob);
    }
}
