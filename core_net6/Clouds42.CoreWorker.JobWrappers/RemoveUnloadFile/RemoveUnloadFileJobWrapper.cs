﻿using Clouds42.CoreWorker.JobWrappers;
using Clouds42.CoreWorker.JobWrappers.RemoveUnloadFile;
using CommonLib.Enums.CoreWorker;
using Repositories;

namespace Clouds42.CoreWorker.JobWrappers.RemoveUnloadFile
{
    /// <summary>
    /// Задача по удалению загруженных файлов.
    /// </summary>
    public class RemoveUnloadFileJobWrapper : JobWrapperBase, IRemoveUnloadFileJobWrapper
    {
        public RemoveUnloadFileJobWrapper(IUnitOfWork dbLayer) : base(dbLayer)
        {
        }


        /// <summary>
        /// Провести удаление загруженных файлов.
        /// </summary>        
        public JobWrapperBase Start()
        {
            StartTask(null, "Провести удаление загруженных файлов.");
            return this;
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RemoveUploadFileJob;
    }
}