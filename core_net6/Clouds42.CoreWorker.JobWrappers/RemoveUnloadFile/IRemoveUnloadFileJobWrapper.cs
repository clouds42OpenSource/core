﻿namespace Clouds42.CoreWorker.JobWrappers.RemoveUnloadFile
{
    /// <summary>
    /// Задача по удалению загруженных файлов.
    /// </summary>
    public interface IRemoveUnloadFileJobWrapper
    {
        /// <summary>
        /// Выполнить удаление загруженных файлов.
        /// </summary>        
        JobWrapperBase Start();
    }
}