﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.RemoveFile
{
    /// <summary>
    /// Задача по удалению файла.
    /// </summary>
    public class RemoveFileJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<RemoveFileJobParams>(registerTaskInQueueProvider, dbLayer),
            IRemoveFileJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RemoveFileJob;

        /// <summary>
        /// Выполнить удаление файла.
        /// </summary>        
        public override ParametrizationJobWrapperBase<RemoveFileJobParams> Start(RemoveFileJobParams paramsJob)
        {                                    
            StartTask(paramsJob, $"Удаление файла '{paramsJob.FilePath}'.");            
            return this;
        }

        /// <summary>
        /// Выполнить удаление файла.
        /// </summary>        
        public ParametrizationJobWrapperBase<RemoveFileJobParams> Start(RemoveFileJobParams paramsJob, DateTime startTaskDelay)
        {
            StartTask(paramsJob, $"Удаление файла '{paramsJob.FilePath}'.", startTaskDelay);
            return this;
        }

    }
}
