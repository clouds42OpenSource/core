﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.RemoveFile
{
    /// <summary>
    /// Задача по удалению файла.
    /// </summary>
    public interface IRemoveFileJobWrapper
    {
        /// <summary>
        /// Выполнить удаление файла.
        /// </summary>        
        ParametrizationJobWrapperBase<RemoveFileJobParams> Start(
            RemoveFileJobParams paramsJob);

        /// <summary>
        /// Выполнить удаление файла.
        /// </summary>        
        ParametrizationJobWrapperBase<RemoveFileJobParams> Start(RemoveFileJobParams paramsJob,
            DateTime startTaskDelay);

    }
}