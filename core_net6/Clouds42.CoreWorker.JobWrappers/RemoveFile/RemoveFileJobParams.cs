﻿namespace Clouds42.CoreWorker.JobWrappers.RemoveFile
{
    /// <summary>
    /// Модель входных параметров джобы по удалению файла.
    /// </summary>
    public class RemoveFileJobParams
    {
        public string FilePath { get; set; }
    }
}
