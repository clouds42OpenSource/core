﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.CreateDatabase
{
    /// <summary>
    /// Модель входных параметров джобы по созданию базы 1С на кластере.
    /// </summary>
    public class CreateDatabaseClusterJobParams
    {
        public Guid AccountDatabaseId { get; set; }
    }
}