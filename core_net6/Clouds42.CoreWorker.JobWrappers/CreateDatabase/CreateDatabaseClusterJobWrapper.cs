﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.CreateDatabase
{
    /// <summary>
    /// Джоба созданию базы 1С на кластере.
    /// </summary>
    public class CreateDatabaseClusterJobWrapper :
        ParametrizationJobWrapperBase<CreateDatabaseClusterJobParams>,
        ICreateDatabaseClusterJobWrapper
    {

        /// <summary>
        /// Запустить задачу по удалению базы с кластера.
        /// </summary>        
        public CreateDatabaseClusterJobWrapper(
            IRegisterTaskInQueueProvider registerTaskInQueueProvider,
            IUnitOfWork dbLayer) : base(registerTaskInQueueProvider, dbLayer)
        {
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.CreateDatabaseClusterJob;

        /// <summary>
        /// Выполнить создание базы 1С на кластере.
        /// </summary>        
        public override ParametrizationJobWrapperBase<CreateDatabaseClusterJobParams> Start(CreateDatabaseClusterJobParams paramsJob)
        {
            StartTask(paramsJob, "Создать информационную базу на кластере.");
            return this;
        }

    }
}
