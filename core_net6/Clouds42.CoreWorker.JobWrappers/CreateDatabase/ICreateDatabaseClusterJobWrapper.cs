﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.CreateDatabase
{
    /// <summary>
    /// Джоба созданию базы 1С на кластере.
    /// </summary>
    public interface ICreateDatabaseClusterJobWrapper
    {

        /// <summary>
        /// Выполнить создание базы 1С на кластере.
        /// </summary>        
        ParametrizationJobWrapperBase<CreateDatabaseClusterJobParams> Start(CreateDatabaseClusterJobParams paramsJob);
    }
}