﻿using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromDt;
using Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManagePublishAcDbRedirect;
using Clouds42.CoreWorker.AccountDatabase.Jobs.ManageServiceExtensionDatabaseInMs;
using Clouds42.CoreWorker.AccountDatabase.Jobs.PublishAccountDatabases;
using Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase;
using Clouds42.CoreWorker.AccountJobs.BillingServiceTypes;
using Clouds42.CoreWorker.AccountUserJobs.ManageLockState;
using Clouds42.CoreWorker.AcDbAccessesJobs.JobWrappers;
using Clouds42.CoreWorker.BillingJobs.FinalizeYokassaPayments;
using Clouds42.CoreWorker.BillingJobs.MyDiskSizeRecalculate;
using Clouds42.CoreWorker.BillingServiceType.RecalculateServiceTypeCostAfterChange;
using Clouds42.CoreWorker.JobWrappers.BackupFileFetching;
using Clouds42.CoreWorker.JobWrappers.CreateDatabase;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.CoreWorker.JobWrappers.DeleteAccountDatabase;
using Clouds42.CoreWorker.JobWrappers.DropDatabase;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;
using Clouds42.CoreWorker.JobWrappers.Invoices;
using Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost;
using Clouds42.CoreWorker.JobWrappers.PublishDatabase;
using Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.CoreWorker.JobWrappers.RemoveFile;
using Clouds42.CoreWorker.JobWrappers.RemoveUploadFile;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.CoreWorker.JobWrappers.SendBackupAccountDatabaseToTomb;
using Clouds42.CoreWorker.JobWrappers.UploadFile;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfArchiveDatabaseToTomb;
using Clouds42.CoreWorker.StartProcessToWorkDatabaseJobs.StartProcessOfRemoveDatabase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.JobWrappers
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddJobWrappersServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapper>();
            serviceCollection.AddTransient<IDropDatabaseFromClusterJobWrapper, DropDatabaseFromClusterJobWrapper>();
            serviceCollection.AddTransient<ICreateDatabaseClusterJobWrapper, CreateDatabaseClusterJobWrapper>();
            serviceCollection.AddTransient<IRemoveDirectoryJobWrapper, RemoveDirectoryJobWrapper>();
            serviceCollection.AddTransient<IRepublishSegmentDatabasesJobWrapper, RepublishSegmentDatabasesJobWrapper>();
            serviceCollection.AddTransient<ISendBackupAccountDatabaseToTombJobWrapper, SendBackupAccountDatabaseToTombJobWrapper>();
            serviceCollection.AddTransient<IRemoveFileJobWrapper, RemoveFileJobWrapper>();
            serviceCollection.AddTransient<IFileBaseToServerTransformationWrapper, FileBaseToServerTransformationWrapper>();
            serviceCollection.AddTransient<IRemoveUploadFileJobWrapper, RemoveUploadFileJobWrapper>();
            serviceCollection.AddTransient<IRemoveWebAccessToDatabaseJobWrapper, RemoveWebAccessToDatabaseJobWrapper>();
            serviceCollection.AddTransient<IAddWebAccessToDatabaseJobWrapper, AddWebAccessToDatabaseJobWrapper>();
            serviceCollection.AddTransient<IBackupFileFetchingJobWrapper, BackupFileFetchingJobWrapper>();
            serviceCollection.AddTransient<IMergeChunksToInitialFileJobWrapper, MergeChunksToInitialFileJobWrapper>();
            serviceCollection.AddTransient<IDeleteInactiveAccountsDataJobWrapper, DeleteInactiveAccountsDataJobWrapper>();
            serviceCollection.AddTransient<IDeleteAccountDatabaseJobWrapper, DeleteAccountDatabaseJobWrapper>();
            serviceCollection.AddTransient<IRestoreAccountDatabaseFromTombJobWrapper, RestoreAccountDatabaseFromTombJobWrapper>();
            serviceCollection.AddTransient<IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper, RestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper>();
            serviceCollection.AddTransient<IRecalculateServiceCostAfterChangesJobWrapper, RecalculateServiceCostAfterChangesJobWrapper>();
            serviceCollection.AddTransient<INotifyBeforeChangeServiceCostJobWrapper, NotifyBeforeChangeServiceCostJobWrapper>();
            serviceCollection.AddTransient<IUpdateLoginForWebAccessDatabaseJobWrapper, UpdateLoginForWebAccessDatabaseJobWrapper>();
            serviceCollection.AddTransient<ITerminateSessionsInDatabaseJobWrapper, TerminateSessionsInDatabaseJobWrapper>();
            serviceCollection.AddTransient<ICreateDirectoryJobWrapper, CreateDirectoryJobWrapper>();
            serviceCollection.AddTransient<ManageAccountUserLockStateJobWrapper>();
            serviceCollection.AddTransient<PublishDatabaseJobWrapper>();
            serviceCollection.AddTransient<CancelPublishDatabaseJobWrapper>();
            serviceCollection.AddTransient<RepublishDatabaseJobWrapper>();
            serviceCollection.AddTransient<RepublishWithChangingConfigJobWrapper>();
            serviceCollection.AddTransient<RestartAccountApplicationPoolOnIisJobWrapper>();
            serviceCollection.AddTransient<RemoveOldPublicationJobWrapper>();
            serviceCollection.AddTransient<SetServiceExtensionDatabaseActivationStatusJobWrapper>();
            serviceCollection.AddTransient<RemoveAccessesFromDatabaseJobWrapper>();
            serviceCollection.AddTransient<RecalculateServiceTypeCostAfterChangeJobWrapper>();
            serviceCollection.AddTransient<StartProcessOfRemoveDatabaseJobWrapper>();
            serviceCollection.AddTransient<StartProcessOfArchiveDatabaseToTombJobWrapper>();
            serviceCollection.AddTransient<CreateDatabaseFromDtJobWrapper>();
            serviceCollection.AddTransient<CreateDirectoryJobWrapper>();
            serviceCollection.AddTransient<FinalizeYookassaPaymentJobWrapper>();
            serviceCollection.AddTransient<ProcessDeletedServiceTypesForAccountsJobWrapper>();
            serviceCollection.AddTransient<MyDiskUsedSizeRecalculationJobWrapper>();
            serviceCollection.AddTransient<ManagePublishedAccountDatabaseRedirectJobWrapper>();
            serviceCollection.AddTransient<CreateInvoiceFiscalReceiptJobWrapper>();
            
            return serviceCollection;
        }
    }
}
