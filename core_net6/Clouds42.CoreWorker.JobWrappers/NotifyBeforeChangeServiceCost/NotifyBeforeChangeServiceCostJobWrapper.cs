﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost
{
    /// <summary>
    /// Джоба по уведомлению аккаунтов
    /// о скором изменении стоимости сервиса
    /// </summary>
    public class NotifyBeforeChangeServiceCostJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<NotifyBeforeChangeServiceCostJobParams>(registerTaskInQueueProvider, dbLayer),
            INotifyBeforeChangeServiceCostJobWrapper
    {
        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.NotifyBeforeChangeServiceCostJob;

        /// <summary>
        /// Уведомить аккаунтов о скором
        /// изменении стоимости сервиса
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по уведомлению о скором изменении стоимости сервиса</param>
        /// </summary> 
        public override ParametrizationJobWrapperBase<NotifyBeforeChangeServiceCostJobParams> Start(
            NotifyBeforeChangeServiceCostJobParams paramsJob)
        {
            StartTask(paramsJob, "Уведомление аккаунтов о скором изменении стоимости сервиса");
            return this;
        }
    }
}
