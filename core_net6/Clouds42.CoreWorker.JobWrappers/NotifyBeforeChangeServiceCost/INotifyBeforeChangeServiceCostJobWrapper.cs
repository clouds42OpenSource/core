﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost
{
    /// <summary>
    /// Джоба по уведомлению аккаунтов
    /// о скором изменении стоимости сервиса
    /// </summary>
    public interface INotifyBeforeChangeServiceCostJobWrapper
    {
        /// <summary>
        /// Уведомить аккаунтов о скором
        /// изменении стоимости сервиса
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по уведомлению о скором изменении стоимости сервиса</param>
        /// </summary> 
        ParametrizationJobWrapperBase<NotifyBeforeChangeServiceCostJobParams> Start(
            NotifyBeforeChangeServiceCostJobParams paramsJob);
    }
}
