﻿using System;
using System.Collections.Generic;

namespace Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost
{
    /// <summary>
    /// Модель входных параметров джобы
    /// по уведомлению о скором изменении стоимости сервиса
    /// </summary>
    public class NotifyBeforeChangeServiceCostJobParams
    {
        /// <summary>
        /// ID изменений по сервису биллинга
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Список Id аккаунтов для которых
        /// нужно сделать уведомление
        /// </summary>
        public List<Guid> AccountIds { get; set; }

        /// <summary>
        /// Дата изменения стоимости сервиса
        /// </summary>
        public DateTime ServiceCostChangeDate { get; set; }
    }
}
