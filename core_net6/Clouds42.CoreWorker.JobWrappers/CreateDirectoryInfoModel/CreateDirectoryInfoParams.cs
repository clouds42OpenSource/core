﻿namespace Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel
{
    /// <summary>
    /// Модель входных параметров джобы по созданию каталога
    /// </summary>
    public class CreateDirectoryInfoParams
    {
        /// <summary>
        /// Путь до каталога
        /// </summary>
        public string DirectoryPath { get; set; }
    }
}
