﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;

namespace Clouds42.CoreWorker.JobWrappers.UploadFile
{
    /// <summary>
    /// Задача по склейке частей в исходный файл
    /// </summary>
    public interface IMergeChunksToInitialFileJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи</param>
        ParametrizationJobWrapperBase<MergeChunksToInitialFIleParamsDto> Start(
            MergeChunksToInitialFIleParamsDto paramsJob);
    }
}
