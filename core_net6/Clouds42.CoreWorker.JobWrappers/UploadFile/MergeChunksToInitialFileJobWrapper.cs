﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.UploadFile
{
    /// <summary>
    /// Задача по склейке частей в исходный файл
    /// </summary>
    public class MergeChunksToInitialFileJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<MergeChunksToInitialFIleParamsDto>(registerTaskInQueueProvider, dbLayer), IMergeChunksToInitialFileJobWrapper
    {
        /// <summary>
        /// Тип задачи - склейка частей в исходный файл
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.MergeChunksToInitialFileJob;

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Модель параметров задачи</param>
        public override ParametrizationJobWrapperBase<MergeChunksToInitialFIleParamsDto> Start(MergeChunksToInitialFIleParamsDto paramsJob)
        {
            StartTask(paramsJob, "Склейка частей в исходный файл.");
            return this;
        }
    }
}
