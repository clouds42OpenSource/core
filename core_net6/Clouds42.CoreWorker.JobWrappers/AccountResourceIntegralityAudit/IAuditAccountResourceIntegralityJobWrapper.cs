﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.AccountResourceIntegralityAudit
{
    /// <summary>
    /// Задача по проверке целостности услуг ресурсов (по зависимостям) у всеех аккаунтов
    /// </summary>
    public interface IAuditAccountResourceIntegralityJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        JobWrapperBase Start();
    }
}
