﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.AccountResourceIntegralityAudit
{
    /// <summary>
    /// Задача по проверке целостности услуг ресурсов (по зависимостям) у всеех аккаунтов
    /// </summary>
    public class AuditAccountResourceIntegralityJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer), IAuditAccountResourceIntegralityJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <returns></returns>
        public JobWrapperBase Start()
        {
            StartTask("Проверка целостности ресурсов аккаунта");
            return this;
        }

        /// <summary>
        /// Тип задачи - проверка целостности услуг ресурсов (по зависимостям) у всех аккаунтов
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.AuditAccountResourceIntegralityJob;
    }
}
