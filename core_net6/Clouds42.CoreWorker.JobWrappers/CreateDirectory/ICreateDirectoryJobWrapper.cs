﻿using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;
using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.CreateDirectory
{
    /// <summary>
    /// Обработчик задачи создания директории
    /// </summary>
    public interface ICreateDirectoryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик</returns>
        ParametrizationJobWrapperBase<CreateDirectoryInfoParams> Start(
            CreateDirectoryInfoParams paramsJob);
    }
}
