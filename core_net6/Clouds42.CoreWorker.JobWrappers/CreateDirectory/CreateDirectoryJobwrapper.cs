﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.CreateDirectory
{
    /// <summary>
    /// Обработчик задачи создания директории
    /// </summary>
    public class CreateDirectoryJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<CreateDirectoryInfoParams>(registerTaskInQueueProvider, dbLayer),
            ICreateDirectoryJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        /// <returns>Обработчик</returns>
        public override ParametrizationJobWrapperBase<CreateDirectoryInfoParams> Start(CreateDirectoryInfoParams paramsJob)
        {
            StartTask(paramsJob, $"Создание директории {paramsJob.DirectoryPath}");
            return this;
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.CreateDirectoryJob;
    }
}
