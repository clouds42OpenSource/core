﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.DeleteAccountDatabase
{
    /// <summary>
    /// Задача по удалению инф. базы
    /// </summary>
    public class DeleteAccountDatabaseJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<DeleteAccountDatabaseParamsDto>(registerTaskInQueueProvider, dbLayer),
            IDeleteAccountDatabaseJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override ParametrizationJobWrapperBase<DeleteAccountDatabaseParamsDto> Start(DeleteAccountDatabaseParamsDto paramsJob)
        {
            StartTask(paramsJob, "Удаление инф. базы");
            return this;
        }

        /// <summary>
        /// Тип задачи - удаление инф. базы
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.DeleteAccountDatabaseJob;
    }
}
