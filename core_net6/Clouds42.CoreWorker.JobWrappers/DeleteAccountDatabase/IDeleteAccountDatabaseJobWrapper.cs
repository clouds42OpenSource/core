﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;

namespace Clouds42.CoreWorker.JobWrappers.DeleteAccountDatabase
{
    /// <summary>
    /// Задача по удалению инф. базы
    /// </summary>
    public interface IDeleteAccountDatabaseJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        ParametrizationJobWrapperBase<DeleteAccountDatabaseParamsDto> Start(DeleteAccountDatabaseParamsDto paramsJob);
    }
}
