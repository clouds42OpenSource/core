﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.CoreWorker.JobWrappers.DropDatabase
{
    /// <summary>
    /// Задача по удалению базы 1С с кластера.
    /// </summary>
    public interface IDropDatabaseFromClusterJobWrapper: IWorkerTask
    {

        /// <summary>
        /// Запустить задачу по удалению базы с кластера.
        /// </summary>        
        IWorkerTask Start(DropDatabaseFromClusterJobParams paramsJob);
    }
}