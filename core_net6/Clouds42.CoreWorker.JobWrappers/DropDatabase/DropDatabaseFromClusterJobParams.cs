﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.DropDatabase
{
    /// <summary>
    /// Модель входных параметров джобы по удалению базы 1С с кластера.
    /// </summary>
    public class DropDatabaseFromClusterJobParams
    {
        public Guid AccountDatabaseId { get; set; }
    }
}