﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.DropDatabase
{

    /// <summary>
    /// Задача по удалению базы 1С с кластера.
    /// </summary>
    public class DropDatabaseFromClusterJobWrapper :
        ParametrizationJobWrapperBase<DropDatabaseFromClusterJobParams>,
        IDropDatabaseFromClusterJobWrapper
    {

        /// <summary>
        /// Запустить задачу по удалению базы с кластера.
        /// </summary>        
        public DropDatabaseFromClusterJobWrapper(
            IRegisterTaskInQueueProvider registerTaskInQueueProvider,
            IUnitOfWork dbLayer) : base(registerTaskInQueueProvider, dbLayer)
        {
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.DropDatabaseFromClusterJob;

        /// <summary>
        /// Выполнить отключение активных сессий.
        /// </summary>        
        public override IWorkerTask Start(DropDatabaseFromClusterJobParams paramsJob)
        {
            StartTask(paramsJob, "Удалить информационную базу с кластера.");
            return this;
        }

    }
}
