﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.DropSessions
{
    /// <summary>
    /// Задача по проверке наличия активных сессий в базе 1С на кластере.
    /// </summary>
    public class DropSessionsFromClusterJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<DropSessionsFromClusterJobParams>(registerTaskInQueueProvider, dbLayer),
            IDropSessionsFromClusterJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.DropSessionsFromClusterJob;

        /// <summary>
        /// Выполнить отключение активных сессий.
        /// </summary>        
        public override ParametrizationJobWrapperBase<DropSessionsFromClusterJobParams> Start(DropSessionsFromClusterJobParams paramsJob)
        {
            StartTask(paramsJob, "Отключение активных соединений с кластера.");            
            return this;
        }

    }
}
