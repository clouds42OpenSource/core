﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.DropSessions
{
    /// <summary>
    /// Модель входных параметров джобы по отключению активных сессий в базе 1С на кластере.
    /// </summary>
    public class DropSessionsFromClusterJobParams
    {
        public Guid AccountDatabaseId { get; set; }
    }
}