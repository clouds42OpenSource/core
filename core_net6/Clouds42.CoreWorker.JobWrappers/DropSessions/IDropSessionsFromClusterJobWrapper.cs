﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.DropSessions
{
    /// <summary>
    /// Задача по проверке наличия активных сессий в базе 1С на кластере.
    /// </summary>
    public interface IDropSessionsFromClusterJobWrapper
    {
        /// <summary>
        /// Выполнить отключение активных сессий.
        /// </summary>        
        ParametrizationJobWrapperBase<DropSessionsFromClusterJobParams> Start(
            DropSessionsFromClusterJobParams paramsJob);
    }
}