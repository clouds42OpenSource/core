﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Предоставления веб прав к базе на IIS.
    /// </summary>
    public interface IAddWebAccessToDatabaseJobWrapper
    {
        /// <summary>
        /// Выполнить предоставления веб прав к базе.
        /// </summary>        
        ParametrizationJobWrapperBase<WebAccessToDatabaseJobParams> Start(
            WebAccessToDatabaseJobParams paramsJob);
    }
}