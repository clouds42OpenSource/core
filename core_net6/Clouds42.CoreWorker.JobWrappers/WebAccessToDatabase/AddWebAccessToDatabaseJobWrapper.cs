﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Задача по предоставления веб прав к базе.
    /// </summary>
    public class AddWebAccessToDatabaseJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<WebAccessToDatabaseJobParams>(registerTaskInQueueProvider, dbLayer),
            IAddWebAccessToDatabaseJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.AddWebAccessToDatabaseJob;

        /// <summary>
        /// Выполнить предоставления веб прав к базе.
        /// </summary>        
        public override ParametrizationJobWrapperBase<WebAccessToDatabaseJobParams> Start(WebAccessToDatabaseJobParams paramsJob)
        {
            StartTask(paramsJob, "Предоставления веб прав к базе.");            
            return this;
        }

    }
}
