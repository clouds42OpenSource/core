﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Модель входных параметров джобы по предоставлению/удалению веб прав к базе.
    /// </summary>
    public class WebAccessToDatabaseJobParams
    {
        /// <summary>
        /// Идентификатор базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }
    }
}