﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Обработчик задачи по обновлению логина
    /// веб доступа базы
    /// </summary>
    public class UpdateLoginForWebAccessDatabaseJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<UpdateLoginForWebAccessDatabaseDto>(registerTaskInQueueProvider, dbLayer),
            IUpdateLoginForWebAccessDatabaseJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.UpdateLoginForWebAccessDatabaseJob;

        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override ParametrizationJobWrapperBase<UpdateLoginForWebAccessDatabaseDto> Start(
            UpdateLoginForWebAccessDatabaseDto paramsJob)
        {
            StartTask(paramsJob, $"Обновление логина {paramsJob.OldLogin}  для веб доступа базам {string.Join(',', paramsJob.AccountDatabasesId)}");
            return this;
        }
    }
}
