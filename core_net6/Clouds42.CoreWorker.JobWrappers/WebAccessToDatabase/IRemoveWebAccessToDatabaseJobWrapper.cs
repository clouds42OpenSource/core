﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Удаление веб прав к базе на IIS.
    /// </summary>
    public interface IRemoveWebAccessToDatabaseJobWrapper
    {
        /// <summary>
        /// Выполнить удаление веб прав к базе.
        /// </summary>        
        ParametrizationJobWrapperBase<WebAccessToDatabaseJobParams> Start(
            WebAccessToDatabaseJobParams paramsJob);
    }
}