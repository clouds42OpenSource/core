﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;

namespace Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase
{
    /// <summary>
    /// Обработчик задачи по обновлению логина
    /// веб доступа базы
    /// </summary>
    public interface IUpdateLoginForWebAccessDatabaseJobWrapper
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        ParametrizationJobWrapperBase<UpdateLoginForWebAccessDatabaseDto> Start(
            UpdateLoginForWebAccessDatabaseDto paramsJob);
    }
}
