﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.Invoices
{
    /// <summary>
    /// Обработчик задачи по созданию фискального чека
    /// </summary>
    public class CreateInvoiceFiscalReceiptJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : ParametrizationJobWrapperBase<CreateInvoiceFiscalReceiptJobParamsDc>(registerTaskInQueueProvider, dbLayer)
    {
        /// <summary>
        /// Запустить задачу
        /// </summary>
        /// <param name="paramsJob">Параметры задачи</param>
        public override ParametrizationJobWrapperBase<CreateInvoiceFiscalReceiptJobParamsDc> Start(CreateInvoiceFiscalReceiptJobParamsDc paramsJob)
        {
            StartTask(paramsJob, "Получение фискального чека.");
            return this;
        }

        /// <summary>
        /// Тип задачи
        /// </summary>
        protected override CoreWorkerTaskType TaskType 
            => CoreWorkerTaskType.CreateInvoiceFiscalReceiptJob;
    }
}
