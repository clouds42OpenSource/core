﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.Invoices
{
    /// <summary>
    /// Параметры создания фискального чека
    /// </summary>
    public class CreateInvoiceFiscalReceiptJobParamsDc
    {
        /// <summary>
        /// ID счета на оплату
        /// </summary>
        public Guid InvoiceId { get; set; }
    }
}
