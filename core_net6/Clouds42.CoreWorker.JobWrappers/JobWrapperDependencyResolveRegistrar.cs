﻿using Clouds42.CoreWorker.AccountDatabase.Jobs.CreateDatabaseFromTemplate;
using Clouds42.CoreWorker.AccountDatabase.Jobs.DeleteInactiveAccountsData;
using Clouds42.CoreWorker.AccountDatabase.Jobs.TerminateSessionsInDatabase;
using Clouds42.CoreWorker.JobWrappers.BackupFileFetching;
using Clouds42.CoreWorker.JobWrappers.CreateDatabase;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.CoreWorker.JobWrappers.DeleteAccountDatabase;
using Clouds42.CoreWorker.JobWrappers.DropDatabase;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;
using Clouds42.CoreWorker.JobWrappers.NotifyBeforeChangeServiceCost;
using Clouds42.CoreWorker.JobWrappers.PublishDatabase;
using Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.CoreWorker.JobWrappers.RemoveFile;
using Clouds42.CoreWorker.JobWrappers.RemoveUploadFile;
using Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase;
using Clouds42.CoreWorker.JobWrappers.SendBackupAccountDatabaseToTomb;
using Clouds42.CoreWorker.JobWrappers.UploadFile;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.CoreWorker.JobWrappers
{
    /// <summary>
    /// Регистратор зависимостей для сборки обработчиков задач воркера
    /// </summary>
    public static class JobWrapperDependencyResolveRegistrar
    {
        /// <summary>
        /// Зарегистрировать зависимости
        /// </summary>
        /// <param name="services">Экземпляр контейнера</param>
        public static IServiceCollection AddCoreWorkerJobWrappers(this IServiceCollection services)
        {
            services.AddTransient<IDropSessionsFromClusterJobWrapper, DropSessionsFromClusterJobWrapper>();
            services.AddTransient<IDropDatabaseFromClusterJobWrapper, DropDatabaseFromClusterJobWrapper>();
            services.AddTransient<ICreateDatabaseClusterJobWrapper, CreateDatabaseClusterJobWrapper>();
            services.AddTransient<IRemoveDirectoryJobWrapper, RemoveDirectoryJobWrapper>();
            services.AddTransient<IRepublishSegmentDatabasesJobWrapper, RepublishSegmentDatabasesJobWrapper>();
            services.AddTransient<ISendBackupAccountDatabaseToTombJobWrapper, SendBackupAccountDatabaseToTombJobWrapper>();
            services.AddTransient<IRemoveFileJobWrapper, RemoveFileJobWrapper>();
            services.AddTransient<IFileBaseToServerTransformationWrapper, FileBaseToServerTransformationWrapper>();
            services.AddTransient<IRemoveUploadFileJobWrapper, RemoveUploadFileJobWrapper>();
            services.AddTransient<IRemoveWebAccessToDatabaseJobWrapper, RemoveWebAccessToDatabaseJobWrapper>();
            services.AddTransient<IAddWebAccessToDatabaseJobWrapper, AddWebAccessToDatabaseJobWrapper>();
            services.AddTransient<IBackupFileFetchingJobWrapper, BackupFileFetchingJobWrapper>();
            services.AddTransient<IMergeChunksToInitialFileJobWrapper, MergeChunksToInitialFileJobWrapper>();
            services.AddTransient<IDeleteInactiveAccountsDataJobWrapper, DeleteInactiveAccountsDataJobWrapper>();
            services.AddTransient<IDeleteAccountDatabaseJobWrapper, DeleteAccountDatabaseJobWrapper>();
            services.AddTransient<IRestoreAccountDatabaseFromTombJobWrapper, RestoreAccountDatabaseFromTombJobWrapper>();
            services.AddTransient<IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper, RestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper>();
            services.AddTransient<IRecalculateServiceCostAfterChangesJobWrapper, RecalculateServiceCostAfterChangesJobWrapper>();
            services.AddTransient<INotifyBeforeChangeServiceCostJobWrapper, NotifyBeforeChangeServiceCostJobWrapper>();
            services.AddTransient<IUpdateLoginForWebAccessDatabaseJobWrapper, UpdateLoginForWebAccessDatabaseJobWrapper>();
            services.AddTransient<ITerminateSessionsInDatabaseJobWrapper, TerminateSessionsInDatabaseJobWrapper>();
            services.AddTransient<ICreateDirectoryJobWrapper, CreateDirectoryJobWrapper>();
            services.AddTransient<CreateDatabaseFromTemplateJobWrapper>();

            return services;
        }
    }
}
