﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.DataContracts.CoreWorkerTasks.Results;

namespace Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase
{
    /// <summary>
    /// Джоба для восстановления инф. базы из бэкапа.
    /// </summary>
    public interface IRestoreAccountDatabaseFromTombJobWrapper
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>        
        ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam, RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob);

        /// <summary>
        /// Восстановить инф. базу из бэкапа
        /// </summary>        
        ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam, RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob, DateTime dateTimeDelayOperation);
    }
}
