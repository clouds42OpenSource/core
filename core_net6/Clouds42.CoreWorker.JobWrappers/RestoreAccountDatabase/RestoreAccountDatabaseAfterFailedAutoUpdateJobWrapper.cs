﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.CoreWorkerTasks.Parameters;
using Clouds42.DataContracts.CoreWorkerTasks.Results;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.RestoreAccountDatabase
{
    /// <summary>
    /// Джоба для восстановления инф. базы из бэкапа после не успешной попытки АО.
    /// </summary>
    public class RestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam,
                RestoreAccountDatabaseFromTombResultDto>(registerTaskInQueueProvider, dbLayer),
            IRestoreAccountDatabaseAfterFailedAutoUpdateJobWrapper
    {
        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        public override ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam,
            RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob)
        {
            StartTask(paramsJob, "Восстановление инф. базы из бэкапа после не успешной попытки АО.");
            return this;
        }

        /// <summary>
        /// Восстановить инф. базу из бэкапа после не успешной попытки АО
        /// </summary>
        public ResultingJobWrapperBase<RestoreAccountDatabaseFromTombWorkerTaskParam,
            RestoreAccountDatabaseFromTombResultDto> Start(RestoreAccountDatabaseFromTombWorkerTaskParam paramsJob, DateTime dateTimeDelayOperation)
        {
            StartTask(paramsJob, "Восстановление инф. базы из бэкапа после не успешной попытки АО.", dateTimeDelayOperation);
            return this;
        }

        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RestoreAccountDatabaseAfterFailedAutoUpdateJob;
    }
}
