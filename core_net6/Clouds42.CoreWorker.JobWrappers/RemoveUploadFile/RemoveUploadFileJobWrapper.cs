﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.RemoveUploadFile
{
    /// <summary>
    /// Задача по удалению загруженных файлов.
    /// </summary>
    public class RemoveUploadFileJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        : JobWrapperBase(registerTaskInQueueProvider, dbLayer), IRemoveUploadFileJobWrapper
    {
        /// <summary>
        /// Провести удаление загруженных файлов.
        /// </summary>        
        public JobWrapperBase Start()
        {
            StartTask("Удалить загруженные файлы.");
            return this;
        }

        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RemoveUploadFileJob;

    }
}
