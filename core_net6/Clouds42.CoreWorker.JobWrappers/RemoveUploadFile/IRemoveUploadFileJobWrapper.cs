﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.RemoveUploadFile
{
    public interface IRemoveUploadFileJobWrapper
    {
        /// <summary>
        ///  Задача по удалению загруженных файлов.
        /// </summary>        
        JobWrapperBase Start();
    }
}