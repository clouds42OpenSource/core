﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges
{
    /// <summary>
    /// Джоба по пересчету стоимости сервиса после изменений
    /// </summary>
    public interface IRecalculateServiceCostAfterChangesJobWrapper
    {

        /// <summary>
        /// Пересчетать стоимость сервиса после изменений
        /// c отложенным выполнением
        /// </summary>
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по пересчету стоимости сервиса после изменений</param>
        /// <param name="dateTimeDelayOperation">Время задержки операции</param>
        ParametrizationJobWrapperBase<RecalculateServiceCostAfterChangesJobParams> Start(
            RecalculateServiceCostAfterChangesJobParams paramsJob, DateTime dateTimeDelayOperation);

        /// <summary>
        /// Пересчетать стоимость сервиса после изменений
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по пересчету стоимости сервиса после изменений</param>
        /// </summary> 
        ParametrizationJobWrapperBase<RecalculateServiceCostAfterChangesJobParams> Start(
            RecalculateServiceCostAfterChangesJobParams paramsJob);

        /// <summary>
        /// Получить Id запущенной задачи
        /// </summary>
        /// <returns>Id запущенной задачи</returns>
        Guid? GetTaskQueueId();
    }
}
