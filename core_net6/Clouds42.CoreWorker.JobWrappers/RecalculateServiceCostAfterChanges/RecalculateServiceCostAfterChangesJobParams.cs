﻿using System;
using System.Collections.Generic;

namespace Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges
{
    /// <summary>
    /// Модель входных параметров джобы
    /// по пересчету стоимости сервиса после изменений
    /// </summary>
    public class RecalculateServiceCostAfterChangesJobParams
    {
        /// <summary>
        /// ID изменений по сервису биллинга
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Список Id аккаунтов которым
        /// нужно сделать пересчет стоимости
        /// </summary>
        public List<Guid> AccountIds { get; set; }
    }
}
