﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using System;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.RecalculateServiceCostAfterChanges
{
    /// <summary>
    /// Джоба по пересчету стоимости сервиса после изменений
    /// </summary>
    public class RecalculateServiceCostAfterChangesJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<RecalculateServiceCostAfterChangesJobParams>(registerTaskInQueueProvider,
                dbLayer),
            IRecalculateServiceCostAfterChangesJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RecalculateServiceCostAfterChangesJob;

        /// <summary>
        /// Пересчетать стоимость сервиса после изменений
        /// c отложенным выполнением
        /// </summary>
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по пересчету стоимости сервиса после изменений</param>
        /// <param name="dateTimeDelayOperation">Время задержки операции</param>
        public ParametrizationJobWrapperBase<RecalculateServiceCostAfterChangesJobParams> Start(RecalculateServiceCostAfterChangesJobParams paramsJob,
            DateTime dateTimeDelayOperation)
        {
            StartTask(paramsJob, "Пересчитать стоимость сервиса после изменений", dateTimeDelayOperation);
            return this;
        }

        /// <summary>
        /// Пересчетать стоимость сервиса после изменений
        /// <param name="paramsJob">Модель входных параметров джобы
        /// по пересчету стоимости сервиса после изменений</param>
        /// </summary> 
        public override ParametrizationJobWrapperBase<RecalculateServiceCostAfterChangesJobParams> Start(RecalculateServiceCostAfterChangesJobParams paramsJob)
        {
            StartTask(paramsJob, "Пересчитать стоимость сервиса после изменений");
            return this;
        }

        /// <summary>
        /// Получить Id запущенной задачи
        /// </summary>
        /// <returns>Id запущенной задачи</returns>
        public Guid? GetTaskQueueId() => TaskQueueId;
    }
}
