﻿namespace Clouds42.CoreWorker.JobWrappers.RemoveDirectory
{
    /// <summary>
    /// Модель входных параметров джобы по удалению директории.
    /// </summary>
    public class RemoveDirectoryJobParams
    {
        public string DerectoryPath { get; set; }
    }
}