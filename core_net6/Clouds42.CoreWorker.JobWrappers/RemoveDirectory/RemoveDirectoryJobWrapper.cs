﻿using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.CoreWorker.JobWrappersBase;
using System;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.RemoveDirectory
{
    /// <summary>
    /// Задача по удалению директории.
    /// </summary>
    public class RemoveDirectoryJobWrapper(
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IUnitOfWork dbLayer)
        :
            ParametrizationJobWrapperBase<RemoveDirectoryJobParams>(registerTaskInQueueProvider, dbLayer),
            IRemoveDirectoryJobWrapper
    {
        /// <summary>
        /// Тип задачи.
        /// </summary>
        protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.RemoveDirectoryJob;

        /// <summary>
        /// Выполнить удаление директории.
        /// </summary>        
        public override ParametrizationJobWrapperBase<RemoveDirectoryJobParams> Start(RemoveDirectoryJobParams paramsJob)
        {                                    
            StartTask(paramsJob, $"Удаление директории '{paramsJob.DerectoryPath}'.");            
            return this;
        }

        /// <summary>
        /// Выполнить удаление директории.
        /// </summary>        
        public ParametrizationJobWrapperBase<RemoveDirectoryJobParams> Start(RemoveDirectoryJobParams paramsJob, DateTime startTaskDelay)
        {
            StartTask(paramsJob, $"Удаление файла '{paramsJob.DerectoryPath}'.", startTaskDelay);
            return this;
        }

    }
}
