﻿using System;
using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.RemoveDirectory
{
    /// <summary>
    /// Задача по удалению директории.
    /// </summary>
    public interface IRemoveDirectoryJobWrapper
    {
        /// <summary>
        /// Выполнить удаление директории.
        /// </summary>        
        ParametrizationJobWrapperBase<RemoveDirectoryJobParams> Start(
            RemoveDirectoryJobParams paramsJob);

        /// <summary>
        /// Выполнить удаление директории.
        /// </summary>        
        ParametrizationJobWrapperBase<RemoveDirectoryJobParams> Start(
            RemoveDirectoryJobParams paramsJob, DateTime startTaskDelay);
            
    }
}
