﻿using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;

public interface IFileBaseToServerTransformationWrapper
{
    /// <summary>
    /// Выполнить трансформацию файловой базы в серверную
    /// </summary>        
    ParametrizationJobWrapperBase<FileBaseToServerTransformationParams> Start(
        FileBaseToServerTransformationParams paramsJob);
}
