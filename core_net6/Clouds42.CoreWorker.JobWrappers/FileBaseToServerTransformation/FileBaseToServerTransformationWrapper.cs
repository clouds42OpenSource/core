﻿using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;

public class FileBaseToServerTransformationWrapper(
    IRegisterTaskInQueueProvider registerTaskInQueueProvider,
    IUnitOfWork dbLayer)
    : ParametrizationJobWrapperBase<FileBaseToServerTransformationParams>(registerTaskInQueueProvider, dbLayer),
        IFileBaseToServerTransformationWrapper
{
    protected override CoreWorkerTaskType TaskType => CoreWorkerTaskType.TransformFileBaseToServerJob;

    public override ParametrizationJobWrapperBase<FileBaseToServerTransformationParams> Start(FileBaseToServerTransformationParams paramsJob)
    {
        StartTask(paramsJob, $"Трансформация базы из файловой в серверную {paramsJob.AccountDatabaseId}");
        return this;
    }
}
