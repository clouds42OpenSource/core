﻿using System;

namespace Clouds42.CoreWorker.JobWrappers.FileBaseToServerTransformation;

public class FileBaseToServerTransformationParams 
{
    /// <summary>
    /// Идентификатор учетной записи.
    /// </summary>
    public Guid AccountId { get; set; }

    /// <summary>
    /// Идентификатор пользователя учетной записи.
    /// </summary>
    public Guid AccountUserId { get; set; }

    /// <summary>
    /// Идентификатор базы данных учетной записи.
    /// </summary>
    public Guid AccountDatabaseId { get; set; }

    /// <summary>
    /// Логин для подключения.
    /// </summary>
    public string Login { get; set; }

    /// <summary>
    /// Пароль для подключения.
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// Номер телефона, связанный с учетной записью.
    /// </summary>
    public string PhoneNumber { get; set; }

    /// <summary>
    /// Указывает, является ли операция копированием.
    /// </summary>
    public bool IsCopy { get; set; }
}
