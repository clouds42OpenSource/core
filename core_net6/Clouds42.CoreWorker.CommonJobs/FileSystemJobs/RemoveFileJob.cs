﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.RemoveFile;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using System.IO;
using System;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.CommonJobs.FileSystemJobs
{

    /// <inheritdoc />
    /// <summary>
    /// Задача по удалению файла.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveFileJob)]
    public class RemoveFileJob(
        IUnitOfWork dbLayer,
        IRemoveFileJobWrapper removeFileJobWrapper,
        IHandlerException handlerException,
        ILogger42 logger)
        : CoreWorkerParamsJob<RemoveFileJobParams>(dbLayer)
    {
        protected override void Execute(RemoveFileJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            logger.Info($"Удаление файла {jobParams.FilePath}");
            try
            {
                if (!File.Exists(jobParams.FilePath))
                    return;

                File.Delete(jobParams.FilePath);

                if (File.Exists(jobParams.FilePath))
                {
                    removeFileJobWrapper.Start(jobParams);
                    logger.Warn($"Не удалось удалить файл {jobParams.FilePath}");
                    return;
                }
                logger.Info($"Файл {jobParams.FilePath} удален");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления файла] {jobParams.FilePath}");
                removeFileJobWrapper.Start(jobParams);
            }
        }
    }
}
