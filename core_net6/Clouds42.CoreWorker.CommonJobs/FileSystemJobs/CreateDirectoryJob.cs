﻿using System;
using System.IO;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;
using Clouds42.Domain.Constants;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.CommonJobs.FileSystemJobs
{
    /// <summary>
    /// Задача по создаю директории в облаке.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.CreateDirectoryJob)]
    public class CreateDirectoryJob(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : CoreWorkerParamsJob<CreateDirectoryInfoParams>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        protected override void Execute(CreateDirectoryInfoParams jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                if (Directory.Exists(jobParams.DirectoryPath))
                    return;

                Directory.CreateDirectory(jobParams.DirectoryPath);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания директории] '{jobParams.DirectoryPath}'");
            }
        }
    }
}
