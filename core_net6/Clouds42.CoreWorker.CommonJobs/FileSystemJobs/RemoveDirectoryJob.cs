﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappers.RemoveDirectory;
using Clouds42.Logger;
using System;
using System.IO;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.CommonJobs.FileSystemJobs
{
    /// <summary>
    /// Задача по удалению директорий.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveDirectoryJob)]
    public class RemoveDirectoryJob(
        IUnitOfWork dbLayer,
        IRemoveDirectoryJobWrapper removeDirectoryJobWrapper,
        ILogger42 logger)
        : CoreWorkerParamsJob<RemoveDirectoryJobParams>(dbLayer)
    {
        protected override void Execute(RemoveDirectoryJobParams jobParams, Guid taskId, Guid taskQueueId)
        {
            logger.Info($"Удаление директории {jobParams.DerectoryPath}");
            try
            {
                if (!Directory.Exists(jobParams.DerectoryPath))
                    return;

                DirectoryHelper.DeleteDirectory(jobParams.DerectoryPath);

                if (Directory.Exists(jobParams.DerectoryPath))
                {
                    removeDirectoryJobWrapper.Start(jobParams);
                    logger.Warn($"Не удалось удалить директорию {jobParams.DerectoryPath}");
                    return;
                }
                logger.Info($"Директория {jobParams.DerectoryPath} удалена");
            }
            catch (IOException ex)
            {
                logger.Warn($"Ошибка удаления директории {jobParams.DerectoryPath} {ex.Message}");
                logger.Info("Задача будет пересоздана");
            }
            catch (UnauthorizedAccessException ex)
            {
                logger.Warn($"Ошибка удаления директории {jobParams.DerectoryPath} {ex.Message}");
                logger.Info("Задача будет пересоздана");
                removeDirectoryJobWrapper.Start(jobParams);
            }
            catch (Exception ex)
            {
                logger.Warn($"Ошибка удаления директории {jobParams.DerectoryPath} {ex.Message}");
            }
        }
    }
}
