﻿using Clouds42.Configurations;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.Logger;
using System;
using System.IO;
using System.Linq;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.CommonJobs.UploadedFileJobs
{
    /// <summary>
    /// Задача по удалению файла.
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.RemoveUploadFileJob)]
    public class RemoveUploadFileJob(
        IUnitOfWork dbLayer,
        ILogger42 logger) : CoreWorkerJob
    {
        readonly string _fileUploadPath = ConfigurationHelper.GetConfigurationValue("FileUploadPath");
        readonly TimeSpan _maxFilesAge = TimeSpan.FromDays(1);

        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <param name="taskQueueId">ID задачи в очереди</param>
        public override void Execute(Guid taskId, Guid taskQueueId)
        {
            //получить список всех пользователей
            var accountUserLoginList =
                dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery().Select(l => l.Login).ToList();

            //проверить для каждого пользователя наличие папки и содержания в ней загруженных файлов
            foreach (var login in accountUserLoginList)
            {
                var sourcePath = Path.Combine(_fileUploadPath, login);
                DirectoryHelper.ClearOldFiles(sourcePath, _maxFilesAge, logger);
            }

            //удаляем файлы в корневом каталоге
            DirectoryHelper.ClearOldFiles(_fileUploadPath, _maxFilesAge, logger);
        }
    }
}