﻿using System;
using Clouds42.AccountDatabase.Contracts.Merge.Interfaces;
using Clouds42.CoreWorker.BaseJobs;
using Clouds42.DataContracts.AccountDatabase.UploadFiles;
using Clouds42.Domain.Constants;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.CoreWorker.CommonJobs.UploadedFileJobs
{
    /// <inheritdoc />
    /// <summary>
    /// Задача по склейке частей в исходный файл
    /// </summary>
    [CoreWorkerJobName(CoreWorkerTasksCatalog.MergeChunksToInitialFileJob)]
    public class MergeChunksToInitialFileJob(IUnitOfWork dbLayer, IMergeFileProvider mergeFileProvider)
        : CoreWorkerParamsJob<MergeChunksToInitialFIleParamsDto>(dbLayer)
    {
        /// <summary>
        /// Выполнить задачу
        /// </summary>
        /// <param name="jobParams">Параметры задачи</param>
        /// <param name="taskId">Id таски</param>
        /// <param name="taskQueueId">Id очереди</param>
        protected override void Execute(MergeChunksToInitialFIleParamsDto jobParams, Guid taskId, Guid taskQueueId)
        {
            try
            {
                mergeFileProvider.MergeChunksToInitialFile(jobParams);
            }
            catch (Exception ex)
            {
                HandlerException.Handle(ex, "[Ошибка при склейке частей в исходный файл]");
                throw;
            }
        }
    }
}
