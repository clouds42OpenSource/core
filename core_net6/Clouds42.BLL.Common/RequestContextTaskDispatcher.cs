﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clouds42.BLL.Common
{
    /// <summary>
    /// Диспетчер задач в рамках контекста запроса.
    /// </summary>
    public class RequestContextTaskDispatcher : IRequestContextTaskDispatcher
    {

        private readonly List<Task> _tasks = [];
        private readonly List<AggregateException> _aggregateExceptionsList = [];

        /// <summary>
        /// Прикрепить к диспетчеру таску.
        /// </summary>
        /// <param name="task">Таска.</param>
        public void Bind(Task task)
        {            
            _tasks.Add(task.ContinueWith(t =>
            {
                if (t.Status == TaskStatus.Faulted)
                    _aggregateExceptionsList.Add(t.Exception);
            }));
        }

        /// <summary>
        /// Дождаться всех прикрпеленных тасок диспетчера.
        /// </summary>
        public void WaitAll()
        {
            if (_tasks.Any())
            {
                Task.WaitAll(_tasks.Where(t=> !t.IsCanceled).ToArray());
                _tasks.Clear();
            }
        }

        /// <summary>
        /// Получить список всех исключений по прикрепленным таскам.
        /// </summary>
        /// <returns></returns>
        public List<AggregateException> GetAggregateExceptionsList()
            => _aggregateExceptionsList;        

    }
}
