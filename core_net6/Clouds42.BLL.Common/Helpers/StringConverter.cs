﻿using System.Globalization;
using System.Linq;

namespace Clouds42.BLL.Common.Helpers
{
    /// <summary>
    /// Конвертер строк
    /// </summary>
    public static class StringConverter
    {
        /// <summary>
        /// Перевести число в строковый вид.
        /// [Example: десять тысяч рублей 67 копеек]; [Example: сто двадцять три гривні 00 копійок]
        /// </summary>
        public static string CurrencyToTxt(decimal amount, string localeCurrency)
        {
            ConvertToStringDecimal(amount, out var beforeDot, out var afterDot);
            var answer = "";
            switch (localeCurrency)
            {
                case "грн":
                {
                    var moneyToStr = new MoneyToStringConverter("UAH", "UKR", "NUMBER");
                    answer = moneyToStr.Convert(int.Parse(beforeDot), int.Parse(afterDot));
                    break;
                }
                case "руб":
                {
                    var moneyToStr = new MoneyToStringConverter("RUR", "RUS", "NUMBER");
                    answer = moneyToStr.Convert(int.Parse(beforeDot), int.Parse(afterDot));
                    break;
                }
                case "сум":
                {
                    var moneyToStr = new MoneyToStringConverter("UZS", "RUS", "NUMBER");
                    answer = moneyToStr.Convert(int.Parse(beforeDot), int.Parse(afterDot));
                    break;
                }
                case "тг":
                {
                    var kztStr = new MoneyToStringConverter("KZT", "RUS", "NUMBER");
                    answer = kztStr.Convert(int.Parse(beforeDot), int.Parse(afterDot));
                    break;
                }
                    
            }

            return answer.First().ToString().ToUpper() + answer[1..];
        }

        /// <summary>
        /// Сконвертировать число типа decimal в строку
        /// </summary>
        /// <param name="value">Число</param>
        /// <param name="beforeDot">Значение до точки</param>
        /// <param name="afterDot">Значение после точки</param>
        private static void ConvertToStringDecimal(decimal value, out string beforeDot, out string afterDot)
        {
            beforeDot = null;
            afterDot = null;

            var round = decimal.Round(value, 2).ToString("0.00", CultureInfo.InvariantCulture);

            if (round.Contains(','))
            {
                beforeDot = round.Split(',')[0];
                afterDot = round.Split(',')[1];
            }

            if (!round.Contains('.'))
            {
                return;
            }

            beforeDot = round.Split('.')[0];
            afterDot = round.Split('.')[1];
        }
    }
}
