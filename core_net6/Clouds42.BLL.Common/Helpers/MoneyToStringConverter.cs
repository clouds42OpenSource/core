﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clouds42.BLL.Common.Helpers
{
    /// <summary>
    /// Конвертер денежной суммы в строковый эквивалент
    /// </summary>
    public class MoneyToStringConverter
    {
        #region CONST
        private const int Num0 = 0;
        private const int Num1 = 1;
        private const int Num2 = 2;
        private const int Num3 = 3;
        private const int Num4 = 4;
        private const int Num10 = 10;
        private const int Num11 = 11;
        private const int Num14 = 14;
        private const int Num100 = 100;
        private const int Num1000 = 1000;
        private const int Index0 = 0;
        private const int Index1 = 1;
        private const int Index2 = 2;
        private const int Index3 = 3;
        #endregion

        #region READONLY
        private readonly string _currencyList = MoneyToStringConverterResources.CurrencyListResource;
        private readonly string _kopFiveUnit;
        private readonly string _kopOneUnit;
        private readonly string _kopSex;
        private readonly string _kopTwoUnit;
        private readonly string _language;
        private readonly Dictionary<string, string[]> _messages = new();
        private readonly string _pennies;
        private readonly string _rubFiveUnit;
        private readonly string _rubOneUnit;
        private readonly string _rubSex;
        private readonly string _rubTwoUnit;
        private readonly List<(Func<long, bool>, Action<long, string, StringBuilder>)> _mapAppendActionForSecondCheckList;
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="currency">Валюта</param>
        /// <param name="language">Язык</param>
        /// <param name="pennies">Тип доли числа после запятой</param>
        public MoneyToStringConverter(string currency, string language, string pennies)
        {
            var currencyType = currency ?? throw new InvalidOperationException("Не указана валюта");
            _language = language ?? throw new InvalidOperationException("Не указан язык");
            _pennies = pennies ?? throw new InvalidOperationException("Не указан тип доли числа после запятой");
            dynamic currencyList = JsonConvert.DeserializeObject<object>(_currencyList);

            var items = currencyList["CurrencyList"][language]["item"];
            foreach (var languageItem in items)
            {
                if (languageItem["-text"] == null)
                {
                    continue;
                }

                string element = languageItem["-text"];
                _messages.Add((string)languageItem["-value"], element.Split(','));
            }
            var currencyItem = currencyList["CurrencyList"][currencyType];
            var theIsoElement = (dynamic)null;
            foreach (var item in currencyItem)
            {
                if (item["-language"] != _language) 
                    continue;
                theIsoElement = item;
                break;
            }
            if (theIsoElement == null)
            {
                throw new ArgumentNullException($"Currency not found {currencyType}");
            }
            _rubOneUnit = theIsoElement["-RubOneUnit"];
            _rubTwoUnit = theIsoElement["-RubTwoUnit"];
            _rubFiveUnit = theIsoElement["-RubFiveUnit"];
            _kopOneUnit = theIsoElement["-KopOneUnit"];
            _kopTwoUnit = theIsoElement["-KopTwoUnit"];
            _kopFiveUnit = theIsoElement["-KopFiveUnit"];
            _rubSex = theIsoElement["-RubSex"];
            _kopSex = theIsoElement["-KopSex"];

            _mapAppendActionForSecondCheckList =
            [
                (range => range == Num1, AppendNum1StringValue),
                (range => range == Num2, AppendNum2StringValue)
            ];
        }

        /// <summary>
        /// Сконвертировать сумму в строку
        /// </summary>
        /// <param name="beforeDecimalPointPart">Сумма до запятой</param>
        /// <param name="afterDecimalPointPart">Сумма после запятой</param>
        /// <returns>Значение суммы строкой</returns>
        public string Convert(long beforeDecimalPointPart, long afterDecimalPointPart)
        {
            var moneyToStringBuilder = new StringBuilder();
            long triadNum = 0;

            var intPart = beforeDecimalPointPart;
            if (intPart == 0)
            {
                moneyToStringBuilder.Append(_messages["0"][0] + " ");
            }
            do
            {
                var theTriad = intPart % Num1000;
                moneyToStringBuilder.Insert(0, TriadToWord(theTriad, triadNum, _rubSex));
                if (triadNum == 0)
                {
                    var range10 = theTriad % Num100 / Num10;
                    var range = theTriad % Num10;

                    ProcessNumberFraction(range, range10, moneyToStringBuilder);
                }
                intPart /= Num1000;
                triadNum += 1;
            } while (intPart > 0);

            ProcessPenniesValue(afterDecimalPointPart, moneyToStringBuilder);
            ProcessAfterDecimalPointPartValue(afterDecimalPointPart, moneyToStringBuilder);
            return moneyToStringBuilder.ToString().Trim();
        }

        /// <summary>
        /// Обработать доли числа
        /// </summary>
        /// <param name="range">Доля числа</param>
        /// <param name="range10">Десятая доля числа</param>
        /// <param name="moneyToStringBuilder">Строковый билдер суммы</param>
        private void ProcessNumberFraction(long range, long range10, StringBuilder moneyToStringBuilder)
        {
            if (range10 == Num1)
            {
                moneyToStringBuilder.Append(_rubFiveUnit);
            }
            else
            {
                switch (range)
                {
                    case Num1:
                        moneyToStringBuilder.Append(_rubOneUnit);
                        break;
                    case Num2:
                    case Num3:
                    case Num4:
                        moneyToStringBuilder.Append(_rubTwoUnit);
                        break;
                    default:
                        moneyToStringBuilder.Append(_rubFiveUnit);
                        break;
                }
            }
        }

        /// <summary>
        /// Обработать значение копеек (суммы после запятой)
        /// </summary>
        /// <param name="afterDecimalPointPart">Сумма после запятой</param>
        /// <param name="moneyToStringBuilder">Строковый билдер суммы</param>
        private void ProcessPenniesValue(long afterDecimalPointPart, StringBuilder moneyToStringBuilder)
        {
            if (_pennies == "TEXT")
            {
                moneyToStringBuilder.Append(_language == "ENG" ? " and " : " ")
                    .Append(afterDecimalPointPart == 0 ? _messages["0"][0] + " " : TriadToWord(afterDecimalPointPart, 0, _kopSex));
            }
            else
            {
                moneyToStringBuilder.Append(" " +
                                            (afterDecimalPointPart < 10 ? "0" + System.Convert.ToString(afterDecimalPointPart) : System.Convert.ToString(afterDecimalPointPart)) +
                                            " ");
            }
        }

        /// <summary>
        /// Обработать значение суммы после запятой
        /// </summary>
        /// <param name="afterDecimalPointPart">Сумма после запятой</param>
        /// <param name="moneyToStringBuilder">Строковый билдер суммы</param>
        private void ProcessAfterDecimalPointPartValue(long afterDecimalPointPart, StringBuilder moneyToStringBuilder)
        {
            if (afterDecimalPointPart is >= Num11 and <= Num14)
            {
                moneyToStringBuilder.Append(_kopFiveUnit);
            }
            else
            {
                switch (afterDecimalPointPart % Num10)
                {
                    case Num1:
                        moneyToStringBuilder.Append(_kopOneUnit);
                        break;
                    case Num2:
                    case Num3:
                    case Num4:
                        moneyToStringBuilder.Append(_kopTwoUnit);
                        break;
                    default:
                        moneyToStringBuilder.Append(_kopFiveUnit);
                        break;
                }
            }
        }

        /// <summary>
        /// Сконвертировать сумму в слово
        /// </summary>
        /// <param name="triad">Сумма</param>
        /// <param name="triadNum">Доля числа</param>
        /// <param name="sex">Род слова</param>
        /// <returns>Слово</returns>
        private string TriadToWord(long triad, long triadNum, string sex)
        {
            var triadWord = new StringBuilder();

            if (triad == 0)
            {
                return "";
            }

            var range = ExecuteFirstCheck(triad, triadWord);
            if (_language == "ENG" && triadWord.Length > 0 && triad % Num10 == 0)
            {
                triadWord = new StringBuilder(triadWord.ToString(0, triadWord.Length - 1));
                triadWord.Append(" ");
            }

            var range10 = range;
            range = triad % Num10;
            ExecuteSecondCheck(triadNum, sex, triadWord, triad, range10);
            switch (triadNum)
            {
                case Num0:
                    break;
                case Num1:
                case Num2:
                case Num3:
                case Num4:
                    if (range10 == Num1)
                    {
                        triadWord.Append(_messages["1000_10"][triadNum - 1] + " ");
                    }
                    else
                    {
                        switch (range)
                        {
                            case Num1:
                                triadWord.Append(_messages["1000_1"][triadNum - 1] + " ");
                                break;
                            case Num2:
                            case Num3:
                            case Num4:
                                triadWord.Append(_messages["1000_234"][triadNum - 1] + " ");
                                break;
                            default:
                                triadWord.Append(_messages["1000_5"][triadNum - 1] + " ");
                                break;
                        }
                    }
                    break;
                default:
                    triadWord.Append("??? ");
                    break;
            }
            return triadWord.ToString();
        }

        /// <summary>
        /// Выполнить вторую проверку
        /// </summary>
        /// <param name="triadNum">Сумма</param>
        /// <param name="sex">Род слова</param>
        /// <param name="triadWordBuilder">Строковый билдер суммы</param>
        /// <param name="triad">Сумма</param>
        /// <param name="range10">Десятая доля числа</param>
        private void ExecuteSecondCheck(long triadNum, string sex, StringBuilder triadWordBuilder, long triad, long range10)
        {
            var range = triad % Num10;
            if (range10 == 1)
            {
                triadWordBuilder.Append(_messages["10_19"][range] + " ");
            }
            else
            {
                var appendAction = _mapAppendActionForSecondCheckList
                    .FirstOrDefault(tuple => tuple.Item1(range))
                    .Item2;

                if (appendAction == null)
                {
                    triadWordBuilder.Append(Concat(["", "", ""], _messages["3_9"])[range] + " ");
                    return;
                }

                appendAction(triadNum, sex, triadWordBuilder);
            }
        }

        /// <summary>
        /// Добавить строковое значение для числа 1
        /// </summary>
        /// <param name="triadNum">Сумма</param>
        /// <param name="sex">Род слова</param>
        /// <param name="triadWordBuilder">Строковый билдер суммы</param>
        private void AppendNum1StringValue(long triadNum, string sex, StringBuilder triadWordBuilder)
        {
            if (triadNum == Num1)
            {
                triadWordBuilder.Append(_messages["1"][Index0] + " ");
            }
            else if (triadNum == Num2 || triadNum == Num3 || triadNum == Num4)
            {
                triadWordBuilder.Append(_messages["1"][Index1] + " ");
            }
            else if ("M" == sex)
            {
                triadWordBuilder.Append(_messages["1"][Index2] + " ");
            }
            else if ("F" == sex)
            {
                triadWordBuilder.Append(_messages["1"][Index3] + " ");
            }
        }

        /// <summary>
        /// Добавить строковое значение для числа 2
        /// </summary>
        /// <param name="triadNum">Сумма</param>
        /// <param name="sex">Род слова</param>
        /// <param name="triadWordBuilder">Строковый билдер суммы</param>
        private void AppendNum2StringValue(long triadNum, string sex, StringBuilder triadWordBuilder)
        {
            if (triadNum == Num1)
            {
                triadWordBuilder.Append(_messages["2"][Index0] + " ");
            }
            else if (triadNum == Num2 || triadNum == Num3 || triadNum == Num4)
            {
                triadWordBuilder.Append(_messages["2"][Index1] + " ");
            }
            else if ("M" == sex)
            {
                triadWordBuilder.Append(_messages["2"][Index2] + " ");
            }
            else if ("F" == sex)
            {
                triadWordBuilder.Append(_messages["2"][Index3] + " ");
            }
        }

        /// <summary>
        /// Выполнить первую проверку
        /// </summary>
        /// <param name="triad">Сумма</param>
        /// <param name="triadWord">Строковый билдер суммы</param>
        /// <returns>Результат первой проверки</returns>
        private long ExecuteFirstCheck(long triad, StringBuilder triadWord)
        {
            var range = triad / Num100;
            triadWord.Append(Concat([""], _messages["100_900"])[range]);

            range = triad % Num100 / Num10;
            triadWord.Append(Concat(["", ""], _messages["20_90"])[range]);
            return range;
        }

        /// <summary>
        /// Соеденить 2 массива в один
        /// </summary>
        /// <param name="first">Первый массив</param>
        /// <param name="second">Второй массив</param>
        /// <returns>Результирующий массив</returns>
        private static string[] Concat(string[] first, string[] second)
        {
            var result = new string[first.Length + second.Length];
            first.CopyTo(result, 0);
            second.CopyTo(result, first.Length);
            return result;
        }
    }
}
