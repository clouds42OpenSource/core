﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Constants;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.BLL.Common.Helpers
{
    /// <summary>
    /// Калькулятор для расчета частичной стоимости сервиса
    /// </summary>
    public static class BillingServicePartialCostCalculator
    {
        /// <summary>
        /// Сопоставление названия локали аккаунта и коэффициента округления стоимости
        /// </summary>
        private static readonly IDictionary<string, decimal> MapLocaleNameToRoundingFactor =
            new Dictionary<string, decimal>
            {
                {LocaleConst.Russia, (decimal) LocaleRoundCost.Rus },
                {LocaleConst.Ukraine, (decimal) LocaleRoundCost.Ukr },
                {LocaleConst.Kazakhstan, (decimal) LocaleRoundCost.Kaz },
                {LocaleConst.Uzbekistan, (decimal) LocaleRoundCost.Uzb },
            };

        /// <summary>
        /// Расчитать частичную стоимость
        /// до конца периода оплаты (с округлением в большую сторону)
        /// </summary>
        /// <param name="cost">Полная стоимость</param>
        /// <param name="expireDate">Дата истечения срока
        /// (дата, до которой оплачен сервис)</param>
        /// <returns>Частичная стоимость
        /// до конца периода оплаты </returns>
        public static decimal CalculateUntilExpireDateWithRoundedUp(decimal cost, DateTime expireDate, DateTime? paidDate = null) =>
            Math.Ceiling(CalculateUntilExpireDateWithoutRound(cost, expireDate, paidDate));

        /// <summary>
        /// Расчитать частичную стоимость
        /// до конца периода оплаты (с округлением стоимости)
        /// </summary>
        /// <param name="cost">Полная стоимость</param>
        /// <param name="expireDate">Дата истечения срока
        /// (дата, до которой оплачен сервис)</param>
        /// <param name="localeName">Название локали</param>
        /// <returns>Частичная стоимость
        /// до конца периода оплаты </returns>
        public static decimal CalculateUntilExpireDate(decimal cost, DateTime expireDate,
            string localeName = LocaleConst.Russia)
        {
            var roundValue = GetRoundingFactorByLocaleName(localeName);

            var partialCost = CalculateUntilExpireDateWithoutRound(cost, expireDate);

            if (partialCost % roundValue > 0m)
                partialCost = (int)(partialCost / roundValue) * roundValue + roundValue;
            else
                partialCost = (int)(partialCost / roundValue) * roundValue;

            return Math.Min(cost, decimal.Round(partialCost, 0, MidpointRounding.AwayFromZero));
        }

        /// <summary>
        /// Расчитать частичную стоимость
        /// до конца периода оплаты (без округления стоимости)
        /// </summary>
        /// <param name="cost">Полная стоимость</param>
        /// <param name="expireDate">Дата истечения срока
        /// (дата, до которой оплачен сервис)</param>
        /// <returns>Частичная стоимость
        /// до конца периода оплаты</returns>
        public static decimal CalculateUntilExpireDateWithoutRound(decimal cost, DateTime expireDate, DateTime? paidDate = null)
        {
            if (expireDate < DateTime.Now)
                return 0m;

            if (cost < 0m)
                return 0m;

            if (DateTime.Now.AddMonths(1).Date == expireDate.Date)
                return cost;

            var previousExpireDate = new DateTime(expireDate.Year, expireDate.Month, expireDate.Day).AddMonths(-1);
            var daysCountPast = DateTime.DaysInMonth(previousExpireDate.Year, previousExpireDate.Month);

            var diff = expireDate.Subtract(paidDate ?? DateTime.Now).Days + 1;

            return cost * ((decimal)diff / daysCountPast);
        }

        /// <summary>
        /// Получить коэффициент округления стоимости сервиса по локали
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Коэффициент округления стоимости сервиса по локали</returns>
        private static decimal GetRoundingFactorByLocaleName(string localeName)
        {
            if (!MapLocaleNameToRoundingFactor.TryGetValue(localeName ?? "", out var roundValue))
                return (decimal)LocaleRoundCost.Rus;

            return roundValue;
        }
    }
}
