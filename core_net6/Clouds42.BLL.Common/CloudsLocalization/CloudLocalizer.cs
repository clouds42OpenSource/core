﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.DataModels.History;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.BLL.Common.CloudsLocalization
{
    public interface ICloudLocalizer
    {
        /// <summary>
        /// Получить значение(фраза/слово) на языке локали по аккаунту
        /// </summary>
        /// <param name="localizationKey">Ключ локализации облака</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Значениена языке локали</returns>
        string GetValueByAccount(CloudLocalizationKeyEnum localizationKey, Guid accountId);

        List<CloudLocalizationDto> GetAllLocalizationsByAccount(Guid accountId, Guid? localeId);

        /// <summary>
        /// Инициализировать локали аккаунтов
        /// </summary>
        void InitializeAccountLocales();
    }

    /// <summary>
    /// Локализатор облака
    /// </summary>
    public class CloudLocalizer : ICloudLocalizer
    {
        public CloudLocalizer(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _cloudLocalizationsLazy = new Lazy<List<CloudLocalizationDto>>(GetCloudLocalizationsList);
            InitializeAccountLocales();
        }

        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Lazy список локализаций облака
        /// </summary>
        private readonly Lazy<List<CloudLocalizationDto>> _cloudLocalizationsLazy;

        /// <summary>
        /// Lazy значение интервала проверки обновлений в минутах
        /// </summary>
        private readonly Lazy<int> _checkUpdatesIntervalInMinutesLazy =
            new(CloudConfigurationProvider.CloudLocalization.GetCheckUpdatesIntervalInMinutes);

        /// <summary>
        /// Список локализаций облака
        /// </summary>
        private List<CloudLocalizationDto> CloudLocalizations => _cloudLocalizationsLazy.Value;

        /// <summary>
        /// Значение интервала проверки обновлений в минутах
        /// </summary>
        private int CheckUpdatesIntervalInMinutes => _checkUpdatesIntervalInMinutesLazy.Value;

        /// <summary>
        /// Дата проверки обновлений
        /// </summary>
        private DateTime CheckUpdatesDate { get; set; }

        /// <summary>
        /// Локали аккаунтов
        /// </summary>
        private List<AccountLocaleDto> AccountLocales { get; set; }

        /// <summary>
        /// Получить значение(фраза/слово) на языке локали по аккаунту
        /// </summary>
        /// <param name="localizationKey">Ключ локализации облака</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Значениена языке локали</returns>
        public string GetValueByAccount(CloudLocalizationKeyEnum localizationKey, Guid accountId)
        {
            UpdateAccountLocales();
            return DefineLocalizationValue(localizationKey, DefineLocaleIdForAccount(accountId));
        }

        public List<CloudLocalizationDto> GetAllLocalizationsByAccount(Guid accountId, Guid? localeId)
        {
            UpdateAccountLocales();
            var accountLocaleId = DefineLocaleIdForAccount(accountId);
            return CloudLocalizations.Where(x => x.LocaleId == (localeId ?? accountLocaleId)).ToList();
        }

        /// <summary>
        /// Определить значение локализации по ключу для локали
        /// </summary>
        /// <param name="localizationKey">Ключ локализации облака</param>
        /// <param name="localeId">Id локали</param>
        /// <returns>Значение локализации</returns>
        private string DefineLocalizationValue(CloudLocalizationKeyEnum localizationKey, Guid? localeId)
        {
            if (localeId.IsNullOrEmpty())
                return localizationKey.Description();

            return CloudLocalizations.FirstOrDefault(localization =>
                       localization.Key == localizationKey && localization.LocaleId == localeId)?.Value ??
                   localizationKey.Description();
        }

        /// <summary>
        /// Определить Id локали для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Id локали</returns>
        private Guid DefineLocaleIdForAccount(Guid accountId)
        {
            var localeId = AccountLocales
                .FirstOrDefault(accountLocale => accountLocale.AccountId == accountId)?.LocaleId;

            if (localeId.HasValue)
                return localeId.Value;

            var accountLocaleFromDb = GetAccountLocaleFromDatabase(accountId);

            if (accountLocaleFromDb == null)
                return Guid.Empty;

            UpdateAccountLocale(accountLocaleFromDb);
            return accountLocaleFromDb.LocaleId;
        }

        /// <summary>
        /// Получить список моделей локализаций облака
        /// </summary>
        /// <returns>Список моделей локализаций облака</returns>
        private List<CloudLocalizationDto> GetCloudLocalizationsList() => ExecuteFuncOfGettingDataInDatabase(
            dbLayer => dbLayer.GetGenericRepository<CloudLocalization>()
                .WhereLazy().Select(localization =>
                    new CloudLocalizationDto
                    {
                        Key = localization.Key,
                        Value = localization.Value,
                        LocaleId = localization.LocaleId
                    }).ToList());

        /// <summary>
        /// Инициализировать локали аккаунтов
        /// </summary>
        public void InitializeAccountLocales()
        {
            CheckUpdatesDate = DateTime.Now;
            AccountLocales = GetAccountLocalesList();
        }

        /// <summary>
        /// Обновить список локалей аккаунтов
        /// </summary>
        private void UpdateAccountLocales()
        {
            if (DateTime.Now.AddMinutes(-CheckUpdatesIntervalInMinutes) < CheckUpdatesDate)
                return;

            var accountLocalesToUpdate = SelectAccountLocalesToUpdate();

            if (!accountLocalesToUpdate.Any())
                return;

            accountLocalesToUpdate.ForEach(UpdateAccountLocale);
            CheckUpdatesDate = DateTime.Now;
        }

        /// <summary>
        /// Обновить локаль аккаунта в списке
        /// (если записи нет-> запись будет добавленна)
        /// </summary>
        /// <param name="updatedAccountLocale">Обновленная локаль аккаунта</param>
        private void UpdateAccountLocale(AccountLocaleDto updatedAccountLocale)
        {
            var currentAccountLocaleRow = AccountLocales.FirstOrDefault(accountLocale =>
                accountLocale.AccountId == updatedAccountLocale.AccountId);

            if (currentAccountLocaleRow == null)
            {
                AccountLocales.Add(updatedAccountLocale);
                return;
            }

            currentAccountLocaleRow.LocaleId = updatedAccountLocale.LocaleId;
        }

        /// <summary>
        /// Получить локаль аккаунта из базы
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель локали аккаунта</returns>
        private  AccountLocaleDto GetAccountLocaleFromDatabase(Guid accountId)
        {
            return (from accountConfiguration in _unitOfWork.GetGenericRepository<AccountConfiguration>().WhereLazy()
                    where accountConfiguration.AccountId == accountId
                    select new AccountLocaleDto
                    {
                        AccountId = accountConfiguration.AccountId,
                        LocaleId = accountConfiguration.LocaleId
                    })
                .FirstOrDefault();
        }

        /// <summary>
        /// Выбрать локали аккаунтов на обновление в списке
        /// </summary>
        /// <returns>Локали аккаунтов на обновление в списке</returns>
        private List<AccountLocaleDto> SelectAccountLocalesToUpdate() =>
            ExecuteFuncOfGettingDataInDatabase(dbLayer =>
            {
                var accountIdsWithUpdatedLocale = dbLayer
                    .GetGenericRepository<AccountLocaleChangesHistory>()
                    .WhereLazy(change => change.ChangesDate >= CheckUpdatesDate).Select(change => change.AccountId)
                    .ToList();

                if (!accountIdsWithUpdatedLocale.Any())
                    return [];

                return (from accountConfiguration in dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy()
                        join accountId in accountIdsWithUpdatedLocale on accountConfiguration.AccountId equals accountId
                        select new AccountLocaleDto
                        {
                            AccountId = accountConfiguration.AccountId,
                            LocaleId = accountConfiguration.LocaleId
                        }).ToList();
            });

        /// <summary>
        /// Получить модели локалей аккаунтов
        /// </summary>
        /// <returns>Модели локалей аккаунтов</returns>
        private List<AccountLocaleDto> GetAccountLocalesList() => ExecuteFuncOfGettingDataInDatabase(dbLayer =>
            dbLayer.GetGenericRepository<AccountConfiguration>().WhereLazy().Select(accountConfiguration =>
                new AccountLocaleDto
                {
                    AccountId = accountConfiguration.AccountId,
                    LocaleId = accountConfiguration.LocaleId
                }).ToList());

        /// <summary>
        /// Выполнить функцию на получения данных в базе
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="getDataFunc">Функция на получения данных в базе</param>
        /// <returns>Список данных, полученных функцией</returns>
        private List<T> ExecuteFuncOfGettingDataInDatabase<T>(Func<IUnitOfWork, List<T>> getDataFunc)
        {
            return getDataFunc(_unitOfWork);
        }
    }
}
