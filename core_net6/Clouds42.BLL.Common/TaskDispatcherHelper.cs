﻿using System;
using System.Linq;
using System.Text;
using Clouds42.Common.Extensions;

namespace Clouds42.BLL.Common
{
    /// <summary>
    /// Хелпер управления диспетчером дочерних задач.
    /// </summary>
    public class TaskDispatcherHelper(IRequestContextTaskDispatcher requestContextTaskDispatcher)
    {
        /// <summary>
        /// Дождаться выполнения всех бэкграуд задач.
        /// </summary>
        /// <param name="throwExceptionIfExist">Нужно ли генерировать исключение если они есть.</param>
        public void WaitAllBackgroundTasks(bool throwExceptionIfExist = false)
        {
            requestContextTaskDispatcher.WaitAll();

            var exceptions = requestContextTaskDispatcher.GetAggregateExceptionsList();

            if (!throwExceptionIfExist || !exceptions.Any())
            {
                return;
            }

            var exceptionResult = new StringBuilder();
            foreach (var exception in exceptions)
            {
                exceptionResult.AppendLine(exception.GetFullInfo(needStackTrace: false));
            }

            throw new InvalidOperationException(exceptionResult.ToString());
        }
    }
}
