﻿using System;
using Clouds42.Common.Cache;
using Clouds42.Domain.Access;

namespace Clouds42.BLL.Common.Access
{
    internal static class AccessProviderCache
    {        

        private static readonly object Locker = new();
        internal static bool CheckAccessInCache(CacheItem cacheItem)
        {
            if (string.IsNullOrEmpty(cacheItem?.UserIdentity))
                return false;
            
            var keyCache = GetKeyCache(cacheItem);

            lock (Locker)
            {
                return CacheProviderSelector.Current.Exist(keyCache, cacheItem.UserIdentity);
            }
            
        }        

        internal static void AddAccessToCache(CacheItem cacheItem)
        {
            if (string.IsNullOrEmpty(cacheItem?.UserIdentity))
                return;

            var keyCache = GetKeyCache(cacheItem);

            lock (Locker)
            {
                CacheProviderSelector.Current.PutObject(keyCache, true, cacheItem.UserIdentity);
            }
            
        }

        private static string GetKeyCache(CacheItem cacheItem)
        {
            string keyCache = $"{(int)cacheItem.ObjectAction}";
            if (cacheItem.AccountOwnerId.HasValue)
                keyCache += $"_{cacheItem.AccountOwnerId}";

            if (cacheItem.AccountUserOwnerId.HasValue)
                keyCache += $"_{cacheItem.AccountUserOwnerId}";
            return keyCache;
        }

        internal class CacheItem
        {

            internal CacheItem(string userIdentity, 
                               ObjectAction objectAction,
                               Guid? accountOwnerId,
                               Guid? accountUserOwnerId)
            {
                UserIdentity = userIdentity;
                ObjectAction = objectAction;
                AccountOwnerId = accountOwnerId;
                AccountUserOwnerId = accountUserOwnerId;
            }

            /// <summary>
            /// Идентификатор пользователя выполнившего запрос (субъект которому проверяется доступ к выполнению операции)
            /// </summary>
            internal string UserIdentity { get; }

            /// <summary>
            /// Операция которую инициировал пользователь
            /// </summary>
            internal ObjectAction ObjectAction { get; }

            /// <summary>
            /// Номер аккаунта - владельца запрашиваемого объекта.
            /// </summary>
            internal Guid? AccountOwnerId { get; }

            /// <summary>
            /// Номер аккаунт юзера - владельца запрашиваемого объекта 
            /// </summary>
            internal Guid? AccountUserOwnerId { get; }
        }

    }    

}
