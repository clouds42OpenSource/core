﻿using System;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.Access;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.BLL.Common.Access
{
    public class UserPrincipalHelper(IUnitOfWork dbLayer, IConfiguration configuration) : IUserPrincipalHelper
    {
        public IUserPrincipalDto GetUserAndRefreshSession(Guid sessionToken)
        {

            var sessionLifeTime = configuration["SessionLifeTime"];

            if (!int.TryParse(sessionLifeTime, out var expDays)) 
                expDays = 365;

            var accUsrData = dbLayer.AccountUserSessionsRepository.GetValidAccountUserSessionDataForToken(sessionToken, expDays);

            if (accUsrData == null)
                return null;

            var userPrincipal = new AccountUserPrincipalDto
            {
                Id = accUsrData.Id,
                Token = accUsrData.Token,
                RequestAccountId = accUsrData.RequestAccountId,
                Name = accUsrData.Name,
                Groups = accUsrData.Groups,
                ContextAccountId = accUsrData.RequestAccountId,
                IsService = true,
                UserSource = accUsrData.UserSource
            };

            if (accUsrData.SessionId.HasValue)
                dbLayer.AccountUserSessionsRepository.RefreshTokenCreationTime(accUsrData.SessionId.Value);

            return userPrincipal;
        }

    }
}
