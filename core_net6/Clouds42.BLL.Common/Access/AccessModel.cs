﻿using Clouds42.Domain.Access;

namespace Clouds42.BLL.Common.Access
{
    /// <summary>
    /// Модель доступа
    /// </summary>
    public sealed class AccessModel
    {
        /// <summary>
        /// Для какого действия доступ
        /// </summary>
        public ObjectAction Action { set; get; }
        /// <summary>
        /// Для какой группы доступ
        /// </summary>
        public AccountUserGroup Group { set; get; }
        
        /// <summary>
        /// Уровень доступности
        /// </summary>
        public AccessLevel Level { set; get; }
    }
}