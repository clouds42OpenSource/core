﻿using Clouds42.Logger;
using Microsoft.Extensions.Configuration;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.BLL.Common.Access.Providers
{
    /// <summary>
    /// Базовый системный провайдер.
    /// </summary>
    public abstract class CommonSystemAccessProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : BaseAccessProvider(dbLayer, logger, configuration, handlerException, accessMapping);
}
