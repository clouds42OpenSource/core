﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clouds42.Common.DataModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Microsoft.AspNetCore.Http;

namespace Clouds42.BLL.Common.Access.Providers.Interfaces
{
    /// <summary>
    /// Провайдер доступ
    /// </summary>
    public interface IAccessProvider
    {
        /// <summary>
        /// Получить аутентифицированного пользователя
        /// </summary>
        /// <returns>Аутентифицированный пользователь</returns>
        IUserPrincipalDto GetUser();

        /// <summary>
        /// Получить аутентифицированного пользователя
        /// </summary>
        /// <returns>Аутентифицированный пользователь</returns>
        Task<IUserPrincipalDto> GetUserAsync();

        /// <summary>
        /// Получить от какого клиента пришёл запрос
        /// </summary>
        /// <returns>Клиент от которого пришёл запрос</returns>
        string GetUserAgent();

        /// <summary>
        /// Получить адрес запроса
        /// </summary>
        /// <returns>Адрес запроса</returns>
        string GetUserHostAddress();

        /// <summary>
        /// Получить сейлс менеджеров
        /// </summary>
        /// <returns>Сейлс менеджеры</returns>
        List<AccountUser> GetSalesManagers();

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает исключение AccessDeniedException если доступа нет</returns>
        void HasAccess(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null);

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает <code>true</code> если доступ есть</returns>
        bool HasAccessBool(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null);

        Task HasAccessAsync(ObjectAction objectAction, Guid? getAccountId = null,
            Func<Task<Guid?>> getAccountUserId = null, Func<Task<bool>> optionalCheck = null);

        /// <summary>
        /// ID аккаунта текущего контекста
        /// </summary>
        Guid ContextAccountId { get; }

        /// <summary>
        /// Название аккаунта текущего контекста
        /// </summary>
        string ContextAccountName { get; }

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает <code>true</code>, если доступ есть</returns>
        bool HasAccessForMultiAccountsBool(ObjectAction objectAction);

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает исключение AccessDeniedException если доступа нет</returns>
        void HasAccessForMultiAccounts(ObjectAction objectAction);

        /// <summary>
        /// Получить админов AccountUserId для переданного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого получить админов</param>
        /// <returns>AccountUserId полученных админов</returns>
        List<Guid> GetAccountAdmins(Guid accountId);

        /// <summary>
        /// Получить группы <see cref="AccountUserGroup"/> для переданного пользователя
        /// </summary>
        /// <param name="accountUserId">Для какого пользователя получить группы <see cref="AccountUserGroup"/> </param>
        /// <returns>Группы <see cref="AccountUserGroup"/> для переданного пользователя</returns>
        List<AccountUserGroup> GetGroups(Guid accountUserId);

        /// <summary>
        /// Получение имени аутентифицированного пользователя
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Номер аккаунта текущего контекста
        /// </summary>
        string ContextAccountIndex { get; }

        bool IsCurrentContextAccount();

        void SetCurrentContextAccount(IResponseCookies cookies);
        Guid? GetContextAccountId(IRequestCookieCollection cookie);
        bool SetContextAccount(Guid accountId);
        Guid? AccountIdByAccountDatabase(Guid accountDatabaseId);
        List<string> GetEmailAdmin();
        void SetHttpContext(HttpContext context);
    }
}
