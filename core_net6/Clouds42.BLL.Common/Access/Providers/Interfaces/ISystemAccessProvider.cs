﻿namespace Clouds42.BLL.Common.Access.Providers.Interfaces
{
    /// <summary>
    /// Системный провайдер доступа
    /// </summary>
    public interface ISystemAccessProvider : IAccessProvider
    {

    }
}
