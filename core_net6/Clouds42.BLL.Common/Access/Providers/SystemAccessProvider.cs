﻿using System;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Access;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Clouds42.BLL.Common.Access.Providers
{
    /// <summary>
    /// Системный провайдер доступа.
    /// </summary>
    public class SystemAccessProvider(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IAccessMapping accessMapping,
        IHandlerException handlerException)
        : CommonSystemAccessProvider(dbLayer, logger, configuration, accessMapping, handlerException), ISystemAccessProvider
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Получить данные для идентификации пользователя
        /// </summary>
        /// <returns>Данные для идентификации пользователя</returns>
        protected override string GetUserIdentity()
            => null;

        /// <summary>
        /// Получить аутентифицированного пользователя
        /// </summary>
        /// <returns>Аутентифицированный пользователь</returns>
        public override IUserPrincipalDto GetUser()
        {
            var systemIdent = CloudConfigurationProvider.Common.SystemUser.GetSystemUserIdentifire();
            if (systemIdent == Guid.Empty)
                throw new InvalidOperationException("Неверно сконфигурирован системный пользователь. Проверьте настройку конфигурации SystemUser.SystemUserIdentifire.");

            var user = _dbLayer.AccountUsersRepository.GetAccountUser(systemIdent)
                       ?? throw new NotFoundException("Не удалось получить системного пользователя. Проверьте настройку конфигурации SystemUser.SystemUserIdentifire.");

            return new AccountUserPrincipalDto
            {
                Id = user.Id,
                Name = Name,
                Groups = []
            };
        }

        public override async Task<IUserPrincipalDto> GetUserAsync()
        {
            var systemIdent = CloudConfigurationProvider.Common.SystemUser.GetSystemUserIdentifire();
            if (systemIdent == Guid.Empty)
                throw new InvalidOperationException("Неверно сконфигурирован системный пользователь. Проверьте настройку конфигурации SystemUser.SystemUserIdentifire.");

            var user = await _dbLayer.AccountUsersRepository.AsQueryableNoTracking().FirstOrDefaultAsync(x => x.Id == systemIdent)
                       ?? throw new NotFoundException("Не удалось получить системного пользователя. Проверьте настройку конфигурации SystemUser.SystemUserIdentifire.");

            return new AccountUserPrincipalDto
            {
                Id = user.Id,
                Name = Name,
                Groups = []
            };
        }

        /// <summary>
        /// Получить от какого клиента пришёл запрос
        /// </summary>
        /// <returns>Клиент от которого пришёл запрос</returns>
        public override string GetUserAgent()
            => null;

        /// <summary>
        /// Получить адрес запроса
        /// </summary>
        /// <returns>Адрес запроса</returns>
        public override string GetUserHostAddress()
            => null;

        /// <summary>
        /// Имя аутентифицированного пользователя
        /// </summary>
        public override string Name => "Core system";

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public override void HasAccess(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null,
            Func<bool> optionalCheck = null)
        {
            // Метод не предусмотрен для системного провайдера доступа
        }

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает <code>true</code> если доступ есть</returns>
        public override bool HasAccessBool(ObjectAction objectAction, Func<Guid?> getAccountId = null,
            Func<Guid?> getAccountUserId = null,
            Func<bool> optionalCheck = null)
            => true;

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public override void HasAccessForMultiAccounts(ObjectAction objectAction)
        {
            // Метод не предусмотрен для системного провайдера доступа
        }
    }
}
