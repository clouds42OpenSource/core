﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Http;

using Microsoft.Extensions.Configuration;

namespace Clouds42.BLL.Common.Access.Providers
{
    /// <summary>
    /// Базовый провайдер доступа
    /// </summary>
    public abstract class BaseAccessProvider : IAccessProvider
    {
        protected readonly IUnitOfWork DbLayer;
        protected readonly ILogger42 Logger;
        private readonly IConfiguration _configuration;
        private readonly IHandlerException _handlerException;
        private readonly IDictionary<AccessLevel, Func<IUserPrincipalDto, Guid?, Guid?, bool>> _mapAccessLevelToCheckAccessAction;
        private readonly IAccessMapping _accessMapping;

        protected BaseAccessProvider(IUnitOfWork dbLayer, ILogger42 logger, 
            IConfiguration configuration, IHandlerException handlerException, IAccessMapping accessMapping)
        {
            DbLayer = dbLayer;

            _mapAccessLevelToCheckAccessAction = new Dictionary<AccessLevel, Func<IUserPrincipalDto, Guid?, Guid?, bool>>
            {
                {AccessLevel.Allow, (_, _, _) => true},
                {AccessLevel.HimSelfAccount, CheckAccessForHimSelfAccount},
                {AccessLevel.HimSelfUser, (principalAccountUser, _, accountUserId) => CheckAccessForHimSelfUser(principalAccountUser, accountUserId)},
                {AccessLevel.ControlledAccounts, CheckAccessForControlledAccounts},
            };

            Logger = logger;
            _configuration = configuration;
            _handlerException = handlerException;
            _accessMapping = accessMapping;
        }

        /// <summary>
        /// Получить данные для идентификации пользователя
        /// </summary>
        /// <returns>Данные для идентификации пользователя</returns>
        protected abstract string GetUserIdentity();

        /// <summary>
        /// Получить аутентифицированного пользователя
        /// </summary>
        /// <returns>Аутентифицированный пользователь</returns>
        public abstract IUserPrincipalDto GetUser();

        public abstract Task<IUserPrincipalDto> GetUserAsync();

        /// <summary>
        /// Получить от какого клиента пришёл запрос
        /// </summary>
        /// <returns>Клиент от которого пришёл запрос</returns>
        public abstract string GetUserAgent();

        /// <summary>
        /// Получить адрес запроса
        /// </summary>
        /// <returns>Адрес запроса</returns>
        public abstract string GetUserHostAddress();

        /// <summary>
        /// ID аккаунта текущего контекста
        /// </summary>
        public Guid ContextAccountId => GetUser()?.ContextAccountId ?? Guid.Empty;

        /// <summary>
        /// Название аккаунта текущего контекста
        /// </summary>
        public string ContextAccountName
        {
            get
            {
                var account = DbLayer.AccountsRepository.GetById(GetUser()?.ContextAccountId ?? Guid.Empty);
                return account != null ? account.AccountCaption : "";
            }
        }

        /// <summary>
        /// Номер аккаунта текущего контекста
        /// </summary>
        public string ContextAccountIndex
        {
            get
            {
                var account = DbLayer.AccountsRepository.GetById(GetUser()?.ContextAccountId ?? Guid.Empty);
                return account?.IndexNumber.ToString() ?? "";
            }
        }

        public virtual bool IsCurrentContextAccount()
        {
            throw new NotImplementedException();
        }

        public virtual void SetCurrentContextAccount(IResponseCookies cookies)
        {
            throw new NotImplementedException();
        }

        public virtual Guid? GetContextAccountId(IRequestCookieCollection cookie)
        {
            throw new NotImplementedException();
        }

        public virtual bool SetContextAccount(Guid accountId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получить сейлс менеджеров
        /// </summary>
        /// <returns>Сейлс менеджеры</returns>
        public List<AccountUser> GetSalesManagers()
            => DbLayer.AccountUserRoleRepository
                    .Where(role => role.AccountUserGroup == AccountUserGroup.AccountSaleManager)
                    .Select(role => role.AccountUser).ToList();

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public virtual void HasAccess(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null)
        {
            if (HasAccessBool(objectAction, getAccountId, getAccountUserId, optionalCheck)) return;
            var user = GetUser();

            var accountUserId = (getAccountUserId != null ? "accountUser:" + getAccountUserId() : "");
            var ownerObject = getAccountId != null ? "account:  " + getAccountId() : accountUserId;

            var userIdentity = user != null ? user.Id.ToString() : "Анонимный запрос";
            var message = $"Отказано в доступе. Запрашивается объект ({objectAction}) принадлежит аккаунту : '{ownerObject}'. Запрашивает пользователь '{userIdentity}' info:'{GetUserAgent()}'";
            throw new AccessDeniedException(objectAction, message, user);
        }

        public virtual async Task HasAccessAsync(ObjectAction objectAction, Guid? accountId = null, Func<Task<Guid?>> getAccountUserId = null, Func<Task<bool>> optionalCheck = null)
        {
            var accountUser =  getAccountUserId != null ? await getAccountUserId() : null;
            var optCheck = optionalCheck != null && await optionalCheck();
            if (await HasAccessBool(objectAction, accountId, accountUser, optCheck)) return;
            var user = await GetUserAsync();

            var accountUserId = (getAccountUserId != null ? "accountUser:" + accountUser : "");
            var ownerObject = accountId != null ? "account:  " + accountId : accountUserId;

            var userIdentity = user != null ? user.Id.ToString() : "Анонимный запрос";
            var message = $"Отказано в доступе. Запрашивается объект ({objectAction}) принадлежит аккаунту : '{ownerObject}'. Запрашивает пользователь '{userIdentity}' info:'{GetUserAgent()}'";
            throw new AccessDeniedException(objectAction, message, user);
        }

        public virtual async Task<bool> HasAccessBool(ObjectAction objectAction, Guid? accountId = null,
            Guid? accountUserId = null, bool? optionalCheck = null)
        {
            try
            {
                var accessList = _accessMapping.GetAccess(objectAction,
                    [AccountUserGroup.Anonymous]);

                if (accessList.Any())
                    return true;

                var boolParsed = bool.TryParse(_configuration["EnableCache"], out var cacheEnabled);
                var userIdentity = GetUserIdentity();
                if (boolParsed && cacheEnabled && AccessProviderCache.CheckAccessInCache(new AccessProviderCache.CacheItem(userIdentity, objectAction, accountId, accountUserId)))
                    return true;

                var hasAccess = await HasAccessForAction(objectAction, accountId, accountUserId, optionalCheck);
                if (hasAccess && boolParsed && cacheEnabled)
                    AccessProviderCache.AddAccessToCache(new AccessProviderCache.CacheItem(userIdentity, objectAction, accountId, accountUserId));

                return hasAccess;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка проверки доступа HasAccessBool]");
                return false;
            }
        }

        /// <summary>
        /// Проверить, есть ли доступ
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="getAccountId">Функция возвращающая аккаунт ID для которого проверить доступ</param>
        /// <param name="getAccountUserId">Функция возвращающая ID пользователя для которого проверить доступ</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает <code>true</code> если доступ есть</returns>
        public virtual bool HasAccessBool(ObjectAction objectAction, Func<Guid?> getAccountId = null, Func<Guid?> getAccountUserId = null, Func<bool> optionalCheck = null)
        {
            try
            {
                var accessList = _accessMapping.GetAccess(objectAction,
                    [AccountUserGroup.Anonymous]);

                if (accessList.Any())
                    return true;

                var accountId = getAccountId?.Invoke();
                var accountUserId = getAccountUserId?.Invoke();
                var boolParsed = bool.TryParse(_configuration["EnableCache"], out var cacheEnabled);
                var userIdentity = GetUserIdentity();
                if (boolParsed && cacheEnabled && AccessProviderCache.CheckAccessInCache(new AccessProviderCache.CacheItem(userIdentity, objectAction, accountId, accountUserId)))
                    return true;

                var hasAccess = HasAccessForAction(objectAction, accountId, accountUserId, optionalCheck);
                if (hasAccess && boolParsed && cacheEnabled)
                    AccessProviderCache.AddAccessToCache(new AccessProviderCache.CacheItem(userIdentity, objectAction, accountId, accountUserId));

                return hasAccess;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка проверки доступа HasAccessBool]");
                return false;
            }
        }

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает <code>true</code>, если доступ есть</returns>
        public bool HasAccessForMultiAccountsBool(ObjectAction objectAction)
        {
            var principalAccountUser = GetUser();

            if (principalAccountUser == null)
            {
                var accessModels = _accessMapping.GetAccess(objectAction,
                    [AccountUserGroup.Anonymous]);

                return accessModels.Any();
            }

            var accessList = _accessMapping.GetAccess(objectAction, principalAccountUser.Groups);

            return accessList.Any(x => x.Level is AccessLevel.Allow or AccessLevel.ControlledAccounts);
        }

        /// <summary>
        /// Получить доступ на переданное действие для текущего пользователя
        /// </summary>
        /// <param name="objectAction">Переданное действие на которое получить доступ</param>
        /// <returns>Возвращает исключение <see cref="AccessDeniedException"/> если доступа нет</returns>
        public virtual void HasAccessForMultiAccounts(ObjectAction objectAction)
        {
            var user = GetUser();

            if (!HasAccessForMultiAccountsBool(objectAction))
                throw new AccessDeniedException(objectAction, "Отказано в доступе", user);
        }

        /// <summary>
        /// Имя аутентифицированного пользователя
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Получить админов AccountUserId для переданного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта для которого получить админов</param>
        /// <returns>AccountUserId полученных админов</returns>
        public List<Guid> GetAccountAdmins(Guid accountId)
            => DbLayer.AccountsRepository.GetAccountAdminIds(accountId);

        /// <summary>
        /// Получить группы <see cref="AccountUserGroup"/> для переданного пользователя
        /// </summary>
        /// <param name="accountUserId">Для какого пользователя получить группы <see cref="AccountUserGroup"/> </param>
        /// <returns>Группы <see cref="AccountUserGroup"/> для переданного пользователя</returns>
        public List<AccountUserGroup> GetGroups(Guid accountUserId)
            => DbLayer.AccountUserRoleRepository
                .Where(au => au.AccountUserId == accountUserId)
                .Select(r => r.AccountUserGroup).ToList();

        /// <summary>
        /// Получить ID аккаунта по ID инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>ID аккаунта</returns>
        public Guid? AccountIdByAccountDatabase(Guid accountDatabaseId)
        {
            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.Id == accountDatabaseId);
            return accountDatabase?.AccountId;
        }

        public virtual List<string> GetEmailAdmin()
        {
            throw new NotImplementedException();
        }

        public virtual void SetHttpContext(HttpContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверить, есть ли доступ к указанному действию
        /// </summary>
        /// <param name="objectAction">Действие, на которое нужно проверить доступ</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="optionalCheck">Дополнительная проверка доступа</param>
        /// <returns>Возвращает <code>true</code> если доступ есть</returns>
        private bool HasAccessForAction(ObjectAction objectAction, Guid? accountId, Guid? accountUserId,
            Func<bool> optionalCheck)
        {
            var principalAccountUser = GetUser();

            if (principalAccountUser == null)
                return false;

            var accessList = _accessMapping.GetAccess(objectAction, principalAccountUser.Groups);

            if (!accessList.Any())
                return false;

            foreach (var level in accessList.Select(ac=>ac.Level))
            {
                if (!_mapAccessLevelToCheckAccessAction.TryGetValue(level, out var value))
                    continue;

                var hasAccess = value(principalAccountUser, accountId, accountUserId);
                if (hasAccess)
                    return true;
            }

            return optionalCheck != null && optionalCheck();
        }

        private async Task<bool> HasAccessForAction(ObjectAction objectAction, Guid? accountId, Guid? accountUserId,
            bool? optionalCheck)
        {
            var principalAccountUser = await GetUserAsync();

            if (principalAccountUser == null)
                return false;

            var accessList = _accessMapping.GetAccess(objectAction, principalAccountUser.Groups);

            if (!accessList.Any())
                return false;

            foreach (var level in accessList.Select(ac => ac.Level))
            {
                if (!_mapAccessLevelToCheckAccessAction.TryGetValue(level, out var value))
                    continue;

                var hasAccess = value(principalAccountUser, accountId, accountUserId);
                if (hasAccess)
                    return true;
            }

            return optionalCheck.HasValue && optionalCheck.Value;
        }

        /// <summary>
        /// Проверить доступ для собственного аккаунта
        /// </summary>
        /// <param name="principalAccountUser">Аутентифицированный пользователь</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если доступ есть</returns>
        private bool CheckAccessForHimSelfAccount(IUserPrincipalDto principalAccountUser, Guid? accountId, Guid? accountUserId)
        {
            if (accountId.HasValue && principalAccountUser.RequestAccountId == accountId)
                return true;

            return accountUserId.HasValue &&
                   (accountUserId == principalAccountUser.Id ||
                    IsUserBelongsToAccount(principalAccountUser.RequestAccountId, accountUserId.Value));
        }

        /// <summary>
        /// Проверить доступ для cамого себя
        /// </summary>
        /// <param name="principalAccountUser">Аутентифицированный пользователь</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если доступ есть</returns>
        private static bool CheckAccessForHimSelfUser(IUserPrincipalDto principalAccountUser, Guid? accountUserId)
            => accountUserId.HasValue && accountUserId == principalAccountUser.Id;

        /// <summary>
        /// Проверить доступ для подконтрольных аккаунтов
        /// </summary>
        /// <param name="principalAccountUser">Аутентифицированный пользователь</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если доступ есть</returns>
        private bool CheckAccessForControlledAccounts(IUserPrincipalDto principalAccountUser, Guid? accountId, Guid? accountUserId)
        {
            if (accountId.HasValue && CheckUserIsAccountSaleManager(principalAccountUser.Id, accountId.Value))
                return true;
            
            if (accountUserId.HasValue)
            {
                var contextAccountUser = DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == accountUserId);
                if (contextAccountUser != null && CheckUserIsAccountSaleManager(principalAccountUser.Id, contextAccountUser.AccountId))
                    return true;
            }

            if (accountId.HasValue && principalAccountUser.RequestAccountId == accountId)
                return true;

            return accountUserId.HasValue &&
                   (accountUserId == principalAccountUser.Id || IsUserBelongsToAccount(principalAccountUser.RequestAccountId, accountUserId.Value));
        }

        /// <summary>
        /// Проверить что пользователь принадлежит аккаунты
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если в указанном аккаунте есть пользователь с указанным ID</returns>
        private bool IsUserBelongsToAccount(Guid accountId, Guid accountUserId)
            => DbLayer.AccountUsersRepository.Any(acUs => acUs.AccountId == accountId && acUs.Id == accountUserId);

        /// <summary>
        /// Проверить что указанный пользователь является сейл менеджером аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>true - если пользователь закреплен за аккаунтом как сейл менеджер</returns>
        private bool CheckUserIsAccountSaleManager(Guid accountUserId, Guid accountId)
            => DbLayer.AccountsRepository.Any(ac =>
                ac.AccountSaleManager != null && ac.AccountSaleManager.SaleManagerId == accountUserId &&
                ac.Id == accountId);
    }
}
