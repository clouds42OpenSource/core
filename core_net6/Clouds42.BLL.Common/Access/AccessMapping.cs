﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.AccessMapping;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.BLL.Common.Access
{
    public interface IAccessMapping
    {
        /// <summary>
        /// Получает доступы для действия <see cref="objectAction"/> и переданных групп <see cref="groups"/>
        /// </summary>
        /// <param name="objectAction">действие над которым получить доступ</param>
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        List<AccessModel> GetAccess(ObjectAction objectAction, List<AccountUserGroup> groups);

        /// <summary>
        /// Получает доступы для действий <see cref="objectActions"/> и переданных групп <see cref="groups"/>
        /// </summary>        
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        /// /// <param name="objectActions">действия для которых нужно получить доступ</param>
        List<AccessModel> GetAccess(List<AccountUserGroup> groups, List<ObjectAction> objectActions);

        /// <summary>
        /// Получает доступы для действий по переданным группам <see cref="groups"/>
        /// </summary>                
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        List<AccessModel> GetAccess(List<AccountUserGroup> groups);
    }

    /// <summary>
    /// Класс для проверки доступов
    /// </summary>
    public class AccessMapping : IAccessMapping
    {
        public AccessMapping(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _accessMappingListLazy = new Lazy<List<AccessModel>>(GetAccessMappingList);
        }
        /// <summary>
        /// Список доступов
        /// </summary>
        private readonly Lazy<List<AccessModel>> _accessMappingListLazy;

        /// <summary>
        /// Список доступов
        /// </summary>
        private List<AccessModel> AccessMappingList => _accessMappingListLazy.Value;

        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Получает доступпы из базы данных
        /// </summary>
        private List<AccessModel> GetAccessMappingList()
        {
            var accessMappingRuleRepository = _unitOfWork.GetGenericRepository<AccessMappingRule>();

            return accessMappingRuleRepository.AsQueryableNoTracking().Select(rule => new AccessModel
            {
                Action = rule.Action,
                Level = rule.Level,
                Group = rule.Group
            }).ToList();
        }

        /// <summary>
        /// Получает доступы для действия <see cref="objectAction"/> и переданных групп <see cref="groups"/>
        /// </summary>
        /// <param name="objectAction">действие над которым получить доступ</param>
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        public List<AccessModel> GetAccess(ObjectAction objectAction, List<AccountUserGroup> groups)
        {
            if ((groups?.Count ?? 0) == 0)
                return [];

            return AccessMappingList.Where(m =>
                m.Action == objectAction && m.Level != AccessLevel.Denide && groups.Contains(m.Group)).ToList();
        }

        /// <summary>
        /// Получает доступы для действий <see cref="objectActions"/> и переданных групп <see cref="groups"/>
        /// </summary>        
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        /// /// <param name="objectActions">действия для которых нужно получить доступ</param>
        public List<AccessModel> GetAccess(List<AccountUserGroup> groups, List<ObjectAction> objectActions)
        {
            if ((groups?.Count ?? 0) == 0 || (objectActions?.Count ?? 0) == 0)
                return [];

            return AccessMappingList.Where(m =>
                objectActions.Contains(m.Action) && m.Level != AccessLevel.Denide && groups.Contains(m.Group)).ToList();
        }


        /// <summary>
        /// Получает доступы для действий по переданным группам <see cref="groups"/>
        /// </summary>                
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        public List<AccessModel> GetAccess(List<AccountUserGroup> groups)
        {
            return (groups?.Count ?? 0) == 0 ? [] : AccessMappingList.Where(m => m.Level != AccessLevel.Denide && groups.Contains(m.Group)).ToList();
        }
    }
}
