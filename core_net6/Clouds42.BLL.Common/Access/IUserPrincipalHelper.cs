﻿using System;
using Clouds42.Common.DataModels;

namespace Clouds42.BLL.Common.Access
{
    public interface IUserPrincipalHelper
    {
        IUserPrincipalDto GetUserAndRefreshSession(Guid sessionToken);
    }
}