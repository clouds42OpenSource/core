﻿using System.Collections.Generic;
using CommonLib.Enums.Access;

namespace Clouds42.BLL.Common.Access
{
    public static partial class AccessMapping
    {
        /// <summary>
        /// Доступы для действий с информационными базами
        /// </summary>
        /// <param name="mapping">лист прав</param>
        private static void AccountDatabasesInit(List<AccessModel> mapping)
        {
            mapping.AddRange(
                new List<AccessModel>
                {
                        new AccessModel{Action = ObjectAction.AccountDatabases_AccessToAllAccountUsers, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AccessToAllAccountUsers, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AccessToAllAccountUsers, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AccessToAllAccountUsers, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AccessToAllAccountUsers, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},

                        new AccessModel{Action = ObjectAction.AccountDatabases_RemoveUserAccess, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RemoveUserAccess, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RemoveUserAccess, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RemoveUserAccess, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RemoveUserAccess, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},

                        new AccessModel{Action = ObjectAction.AccountDatabases_GrantAccessToUser, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_GrantAccessToUser, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_GrantAccessToUser, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_GrantAccessToUser, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_GrantAccessToUser, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},

                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.AccountUser, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Detail, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabase_GetUserAccessList, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_GetUserAccessList, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Add, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditFull, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditFull, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditFull, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditFull, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditFull, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabase_Change, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Change, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Change, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Change, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Change, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountFilesToTomb, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountFilesToTomb, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountFilesToTomb, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountFilesToTomb, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditCaption, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditCaption, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditCaption, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditCaption, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditV82Name, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditV82Name, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditV82Name, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditTemplate, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditTemplate, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditTemplate, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditTemplate, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditStatus, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditStatus, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditStatus, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangeDbFlagUsedWebServices, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangeDbFlagUsedWebServices, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangeDbFlagUsedWebServices, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangeDbFlagUsedWebServices, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLinkInfo, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLinkInfo, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLinkInfo, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLinkInfo, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditPlatform, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditPlatform, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditPlatform, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditPlatform, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangePlatformFrom83To82, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangePlatformFrom83To82, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ChangePlatformFrom83To82, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                        
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewSqlServerInfo, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewSqlServerInfo, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewSqlServerInfo, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewSqlServerInfo, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabase_DeleteToTomb, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DeleteToTomb, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DeleteToTomb, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DeleteToTomb, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_RestartIisAppPool, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RestartIisAppPool, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RestartIisAppPool, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RestartIisAppPool, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_Publish, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Publish, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Publish, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Publish, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_Republish, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Republish, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Republish, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Republish, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishWithChangingWebConfig, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishWithChangingWebConfig, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishWithChangingWebConfig, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishWithChangingWebConfig, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishSegmentDatabases, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishSegmentDatabases, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_RepublishSegmentDatabases, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_WebAccess, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_WebAccess, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_WebAccess, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_WebAccess, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditUsedWebServices, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditUsedWebServices, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditUsedWebServices, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLaunchType, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLaunchType, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLaunchType, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLaunchType, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditLaunchType, Group = AccountUserGroup.AccountUser, Level = AccessLevel.HimSelfUser},

                        new AccessModel{Action = ObjectAction.AccountDatabases_AddDbInTemplateForDelimiter, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AddDbInTemplateForDelimiter, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AddDbInTemplateForDelimiter, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_AddDbInTemplateForDelimiter, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_DisableDomainAuthentication, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DisableDomainAuthentication, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DisableDomainAuthentication, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DisableDomainAuthentication, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        
                        new AccessModel{Action = ObjectAction.AccountDatabases_Unpublish, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Unpublish, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Unpublish, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_Unpublish, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        
                        new AccessModel{Action = ObjectAction.AccountDatabases_LockUnlock, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_LockUnlock, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_LockUnlock, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_LockUnlock, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},

                        new AccessModel{Action = ObjectAction.AccountDatabase_Restore, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Restore, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Restore, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_Restore, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabase_DownloadFromTomb, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DownloadFromTomb, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DownloadFromTomb, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabase_DownloadFromTomb, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_LinkedExecuteQuery, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_LinkedExecuteQuery, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_McobRemoveUserRoles, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_McobRemoveUserRoles, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_McobEditRolesOfDatabase, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_McobEditRolesOfDatabase, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_DropSessions, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DropSessions, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DropSessions, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_DropSessions, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditSupport, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditSupport, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditSupport, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditSupport, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_EditDbType, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditDbType, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditDbType, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_EditDbType, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},

                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.AccountUser, Level = AccessLevel.HimSelfUser},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewDelimitersInfo, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},

                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                        new AccessModel{Action = ObjectAction.AccountDatabases_ViewAccessDatabaseList, Group = AccountUserGroup.AccountUser, Level = AccessLevel.HimSelfAccount},

                });
            mapping.AddRange(
                new List<AccessModel>
                {
                    new AccessModel{Action = ObjectAction.AccountDatabase_MigrationListView, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabase_MigrationListView, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabase_MigrationListView, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow}
                });

            mapping.AddRange(
                new List<AccessModel>
                {
                    new AccessModel{Action = ObjectAction.AccountDatabases_ViewInfoDeteil, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ViewInfoDeteil, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ViewInfoDeteil, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ViewInfoDeteil, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.ControlledAccounts},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ViewInfoDeteil, Group = AccountUserGroup.CloudSE, Level = AccessLevel.Allow},
                });

            mapping.AddRange(
                new List<AccessModel>
                {
                    new AccessModel{Action = ObjectAction.AccountDatabases_ParseUsersXml, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ParseUsersXml, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ParseUsersXml, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ParseUsersXml, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ParseUsersXml, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},

                    new AccessModel{Action = ObjectAction.AccountDatabases_ValidateVersionXml, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ValidateVersionXml, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ValidateVersionXml, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ValidateVersionXml, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                    new AccessModel{Action = ObjectAction.AccountDatabases_ValidateVersionXml, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow},

                    new AccessModel{Action = ObjectAction.AccountDatabases_UploadDbFile, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_UploadDbFile, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_UploadDbFile, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow},
                    new AccessModel{Action = ObjectAction.AccountDatabases_UploadDbFile, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount},
                    new AccessModel{Action = ObjectAction.AccountDatabases_UploadDbFile, Group = AccountUserGroup.AccountSaleManager, Level = AccessLevel.Allow}
                });

            mapping.AddRange(
                new List<AccessModel>
                {
                    new AccessModel { Action = ObjectAction.InstallExtension, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow },
                    new AccessModel { Action = ObjectAction.InstallExtension, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow },
                    new AccessModel { Action = ObjectAction.InstallExtension, Group = AccountUserGroup.Hotline, Level = AccessLevel.Allow },
                    new AccessModel { Action = ObjectAction.InstallExtension, Group = AccountUserGroup.AccountAdmin, Level = AccessLevel.HimSelfAccount },
                    new AccessModel { Action = ObjectAction.InstallExtension, Group = AccountUserGroup.AccountUser, Level = AccessLevel.HimSelfAccount },
                });

            mapping.AddRange(
                new List<AccessModel>
                {
                    new AccessModel { Action = ObjectAction.AccountDatabase_ManageRestoreModel, Group = AccountUserGroup.Cloud42Service, Level = AccessLevel.Allow },
                    new AccessModel { Action = ObjectAction.AccountDatabase_ManageRestoreModel, Group = AccountUserGroup.CloudAdmin, Level = AccessLevel.Allow }
                });

        }
    }
}
