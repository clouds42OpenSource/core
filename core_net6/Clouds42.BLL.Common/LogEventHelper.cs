﻿using System;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.BLL.Common
{
    public static class LogEventHelper
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        public static void LogEvent(IUnitOfWork dbLayer, Guid accountId, IAccessProvider accessProvider, 
            LogActions log, string description)
        {
            
            try
            {

                if (accountId == Guid.Empty)
                {
                    _logger.Warn($"Попытка залогировать действие {log.Description()} с описанием {description} не указав номер контекста аккаунта accountId='{accountId}'");
                    return;
                }

                var action = dbLayer.CloudChangesActionRepository.FirstOrDefault(x => x.Index == (int)log);
                if (action == null)
                {
                    _logger.Warn($"Попытка залогировать действие {log.Description()} с описанием {description} не удачна из за отсутсвия в таблице CloudChangesAction'");
                    return;
                }
                _logger.Info($"Логирую действией {(int)log}");
                
                dbLayer.CloudChangesRepository.Insert(new CloudChanges
                    {
                        Id = Guid.NewGuid(),
                        AccountId = accountId,
                        Initiator = accessProvider?.GetUser()?.Id,
                        Action = action.Id,
                        Date = DateTime.Now,
                        Description = description
                });

                dbLayer.Save();   
            }
            catch (Exception ex)
            {
                _logger.Warn($"Ошибка логирования действия {accessProvider} : {ex} \naccountid: {accountId} \naction: {log} ");
            }
        }

        public static void LogEventInitiator(IUnitOfWork dbLayer, Guid accountId, LogActions log, 
            string description, Guid initiatorId, IHandlerException handlerException)
        {

            if (accountId == Guid.Empty)
            {
                _logger.Warn($"Попытка залогировать действие {log.Description()} с описанием {description} не указав номер контекста аккаунта accountId='{accountId}'");
                return;
            }

            try
            {
                var action = dbLayer.CloudChangesActionRepository.FirstOrDefault(x => x.Index == (int)log);
                dbLayer.CloudChangesRepository.Insert(new CloudChanges
                {
                    Id = Guid.NewGuid(),
                    AccountId = accountId,
                    Initiator = initiatorId,
                    Action = action.Id,
                    Date = DateTime.Now,
                    Description = description
                });

                dbLayer.Save();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка инициализации LogEventa]");
            }

        }
    }

    public interface ICloudChangesProvider
    {
        ManagerResult<bool> LogEvent(Guid accountId, LogActions log, string description);
    }

    public class CloudChangesProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : ICloudChangesProvider
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();

        public ManagerResult<bool> LogEvent(Guid accountId, LogActions log, string description)
        {
            _logger.Info($"Логирую действией {(int)log}");
            if (accountId == Guid.Empty)
            {
                _logger.Warn($"Попытка залогировать действие {log.Description()} с описанием {description} не указав номер контекста аккаунта accountId='{accountId}'");
                return new ManagerResult<bool>
                {
                    Result = false,
                    Message = "accountId равен нулю",
                    State = ManagerResultState.PreconditionFailed
                };
            }

            try
            {
                var action = dbLayer.CloudChangesActionRepository.FirstOrDefault(x => x.Index == (int)log);

                if (action == null)
                {
                    handlerException.Handle(new Exception(),$"[Попытка залогировать действие {log.Description()} с описанием {description} но в базе он не найден]");
                    return new ManagerResult<bool>
                    {
                        Result = false,
                        Message = "action равен нулю",
                        State = ManagerResultState.PreconditionFailed
                    };
                }
                dbLayer.CloudChangesRepository.Insert(new CloudChanges
                {
                    Id = Guid.NewGuid(),
                    AccountId = accountId,
                    Initiator = accessProvider?.GetUser()?.Id,
                    Action = action.Id,
                    Date = DateTime.Now,
                    Description = description
                });

                dbLayer.Save();

                return new ManagerResult<bool> {Result = true, State = ManagerResultState.Ok };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка логирования действия]");
                return new ManagerResult<bool>
                {
                    Result = false,
                    Message = ex.Message,
                    State = ManagerResultState.PreconditionFailed
                };
            }
        }
    }

}
