﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Clouds42.BLL.Common
{
    /// <summary>
    /// Диспетчер задач в рамках контекста запроса.
    /// </summary>
    public interface IRequestContextTaskDispatcher
    {
        /// <summary>
        /// Прикрепить к диспетчеру таску.
        /// </summary>
        /// <param name="task">Таска.</param>
        void Bind(Task task);

        /// <summary>
        /// Дождаться всех прикрпеленных тасок диспетчера.
        /// </summary>
        void WaitAll();

        /// <summary>
        /// Получить список всех исключений по прикрепленным таскам.
        /// </summary>
        /// <returns></returns>
        List<AggregateException> GetAggregateExceptionsList();
    }
}
