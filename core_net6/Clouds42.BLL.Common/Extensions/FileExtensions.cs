﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace Clouds42.BLL.Common.Extensions
{
    /// <summary>
    /// Хелпер для работы с файлом
    /// </summary>
    public static class FormFileExtensions
    {
        /// <summary>
        /// Сконвертировать файл в массив байтов
        /// </summary>
        /// <param name="file">Файл</param>
        /// <returns>Массив байтов</returns>
        public static byte[] ConvertToByteArray(this IFormFile file)
        {
            using var memoryStream = new MemoryStream();
            file.OpenReadStream().CopyTo(memoryStream);
            return memoryStream.ToArray();
        }
    }
}