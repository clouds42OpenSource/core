﻿using Clouds42.Configurations;
using System;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.BLL.Common.Extensions
{
    public static class BillingAccountExtensions
    {
        private static readonly int PpDays = ConfigurationHelper.GetConfigurationValue<int>("PromisePaymentDays");

        /// <summary>
        /// Проверка просроченности оп
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
        public static bool IsPromisedPaymentExpired(this BillingAccount billingAccount)
        {
            return billingAccount.PromisePaymentSum > 0 &&
                   billingAccount.PromisePaymentDate.HasValue &&
                   (DateTime.Now.Date - billingAccount.PromisePaymentDate.Value.Date).Days > PpDays;
        }

        /// <summary>
        /// Проверка на наличие ОП
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
	    public static bool HasPromisedPayment(this BillingAccount billingAccount)
        {
            return billingAccount.PromisePaymentSum > 0 &&
                   billingAccount.PromisePaymentDate.HasValue;
        }
    }
}
