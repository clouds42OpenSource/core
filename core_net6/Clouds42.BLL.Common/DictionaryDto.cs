﻿namespace Clouds42.BLL.Common
{
    /// <summary>
    ///     Dictionary dto
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class DictionaryDto<TKey, TValue>
    {
        /// <summary>
        ///     generic key
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        ///     generic value
        /// </summary>
        public TValue Value { get; set; }
    }
}
