﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.DataValidations;

namespace Clouds42.BLL.Common.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class PhoneNumberAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Bad value for PhoneNumber";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
            var phoneNumber = (string)value;
            if (string.IsNullOrWhiteSpace(phoneNumber))
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);

            var phoneNumberValidation = new PhoneNumberValidation(phoneNumber);

            return !phoneNumberValidation.PhoneNumberMaskIsValid() ? new ValidationResult(ErrorMessage ?? DefaultErrorMessage) : ValidationResult.Success;
        }
    }
}
