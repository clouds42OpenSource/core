﻿using System.ComponentModel.DataAnnotations;
using Clouds42.Common.DataValidations;

namespace Clouds42.BLL.Common.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class EmailAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Bad value for email";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
            var email = (string)value;
            if (string.IsNullOrWhiteSpace(email))
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);

            var emailValidator = new EmailValidation(email);

            if (!emailValidator.EmailMaskIsValid())
                return new ValidationResult(ErrorMessage ?? DefaultErrorMessage);
            return  ValidationResult.Success;
        }
    }
}
