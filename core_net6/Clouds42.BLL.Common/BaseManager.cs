﻿using System;
using System.Linq;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing.BillingChanges;
using Clouds42.Domain.Enums;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.BLL.Common
{
    /// <summary>
    /// Базовый класс менеджера
    /// </summary>
    public abstract class BaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
    {
        protected readonly IAccessProvider AccessProvider = accessProvider;
        protected readonly IUnitOfWork DbLayer = dbLayer;
        protected readonly ILogger42 Logger = Logger42.GetLogger();


        /// <summary>
        /// Выполнить действие с фиксацией трудозатрат.
        /// </summary>
        /// <param name="tag">Имя фиксируемого блока.</param>
        /// <param name="action">Выполняемое действие.</param>
        protected void ProcessWithTimingFixation(string tag, Action action)
        {
            action();
        }

        /// <summary>
        /// Выполнить действие с фиксацией трудозатрат.
        /// </summary>
        /// <param name="tag">Имя фиксируемого блока.</param>
        /// <param name="action">Выполняемое действие.</param>
        protected TResult ProcessWithTimingFixation<TResult>(string tag, Func<TResult> action)
        {
            var result = action();
            return result;
        }


        #region Конструкторы моделей, возвращаемых из менеджеров

        protected ManagerResult Ok()
        {
            return new ManagerResult
            {
                State = ManagerResultState.Ok
            };
        }

        protected ManagerResult<T> Ok<T>(T result, string message = null)
        {
            return new ManagerResult<T>
            {
                State = ManagerResultState.Ok,
                Result = result,
                Message = message
            };
        }

        protected ManagerResult<T> PreconditionFailed<T>(string msg)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.PreconditionFailed,
            };
        }

        protected ManagerResult<T> PreconditionFailed<T>(string msg, Exception exception)
        {
            var result = PreconditionFailed<T>(msg);
            result.Exception = exception;
            return result;
        }

        protected ManagerResult<T> PreconditionFailedError<T>(T obj, string error)
        {
            return new ManagerResult<T>
            {
                Message = error,
                ErrorObject = error,
                Result = obj,
                State = ManagerResultState.PreconditionFailed
            };
        }

        protected ManagerResult PreconditionFailed(string msg)
        {
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.PreconditionFailed
            };
        }

        protected ManagerResult PreconditionFailedError(object errorObject)
        {
            return new ManagerResult
            {
                Message = string.Empty,
                ErrorObject = errorObject,
                State = ManagerResultState.PreconditionFailed
            };
        }

        protected ManagerResult<T> NotFound<T>(string msg)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.NotFound
            };
        }

        protected ManagerResult NotFound(string msg)
        {
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.NotFound
            };
        }

        protected ManagerResult<T> Conflict<T>(string msg)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.Conflict
            };
        }

        protected ManagerResult Conflict(string msg)
        {
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.Conflict
            };
        }

        protected ManagerResult<T> ConflictError<T>(object errorObject)
        {
            return new ManagerResult<T>
            {
                Message = string.Empty,
                State = ManagerResultState.Conflict,
                ErrorObject = errorObject
            };
        }

        protected ManagerResult ConflictError(object errorObject)
        {
            return new ManagerResult
            {
                Message = string.Empty,
                State = ManagerResultState.Conflict,
                ErrorObject = errorObject
            };
        }

        protected ManagerResult<T> Forbidden<T>(string msg)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.Forbidden
            };
        }

        protected ManagerResult Forbidden(string msg)
        {
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.Forbidden
            };
        }

        protected ManagerResult<T> BadRequest<T>(string msg = null)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.BadRequest
            };
        }

        protected ManagerResult BadRequest(string msg = null)
        {
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.BadRequest
            };
        }

        protected ManagerResult Unauthorized(string msg)
        {
          
            return new ManagerResult
            {
                Message = msg,
                State = ManagerResultState.Unauthorized
            };
        }

        protected ManagerResult<T> Unauthorized<T>(string msg)
        {
          
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.Unauthorized
            };
        }

        protected ManagerResult<T> MethodNotAllowed<T>(string msg)
        {
            return new ManagerResult<T>
            {
                Message = msg,
                State = ManagerResultState.MethodNotAllowed
            };
        }

        protected ManagerResult ValidationError(string message)
        {
          
            return new ManagerResult
            {
                State = ManagerResultState.PreconditionFailed,
                Message = message
            };
        }

        protected ManagerResult<T> ValidationError<T>(string message)
        {
            return new ManagerResult<T>
            {
                Message = message,
                State = ManagerResultState.PreconditionFailed
            };
        }

        #endregion

        /// <summary>
        /// Получить Id аккаунта инициатора 
        /// </summary>
        /// <returns>Id аккаунта инициатора</returns>
        protected Guid? GetInitiatorAccountId() =>
            AccessProvider.GetUser()?.RequestAccountId;

        /// <summary>
        /// Получить объект пользователя аккаунт админа.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Объект пользователя.</returns>
        protected IAccountUser GetAccountAdminFirstOrDefault(Guid accountId)
        {
            var accountAdmins = AccessProvider.GetAccountAdmins(accountId);
            if (!accountAdmins.Any())
            {
                return null;
            }

            var accountAdminId = accountAdmins.First();
            return DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountAdminId);
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору значения ресурса облачного сервиса
        /// </summary>
        /// <param name="id">Идентификатор AccountCSResourceValue - значения ресурса облачного сервиса</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        // ReSharper disable once InconsistentNaming
        protected Guid? AccountIdByAccountCSResourceValue(Guid id)
        {
            var csResourceValue = DbLayer.AccountCsResourceValueRepository.FirstOrDefault(a => a.Id == id);
            return csResourceValue?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору базы данных аккаунта
        /// </summary>
        /// <param name="id">Идентификатор AccountDatabase - базы данных аккаунта</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountDatabase(Guid id)
        {
            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(a => a.Id == id);
            return accountDatabase?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору базы данных аккаунта
        /// </summary>
        /// <param name="id">Идентификатор AccountDatabase - базы данных аккаунта</param>
        /// <returns>Идентификатор аккаунта</returns>
        protected Guid GetAccountIdByDatabase(Guid id) =>
            AccountIdByAccountDatabase(id) ?? AccessProvider.ContextAccountId;

        /// <summary>
        /// Получить ID аккаунта по ID бэкапа инф. базы от МС
        /// </summary>
        /// <param name="serviceManagerAcDbBackupId">ID бэкапа инф. базы от МС</param>
        /// <returns>ID аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdBySmAcDbBackupId(Guid serviceManagerAcDbBackupId)
        {
            var accountDatabaseBackup = DbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                backup.ServiceManagerAcDbBackup.ServiceManagerAcDbBackupId == serviceManagerAcDbBackupId);
            return accountDatabaseBackup?.AccountDatabase?.AccountId;
        }

        /// <summary>
        /// Получить ID аккаунта по ID бэкапа инф. базы
        /// </summary>
        /// <param name="backupId">ID бэкапа инф. базы</param>
        /// <returns>ID аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountDatabaseBackup(Guid backupId)
        {
            var accountDatabaseBackup = DbLayer.AccountDatabaseBackupRepository.FirstOrDefault(w => w.Id == backupId);
            return accountDatabaseBackup?.AccountDatabase?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по имени базы данных аккаунта
        /// </summary>
        /// <param name="dbName">Имя AccountDatabase - базы данных аккаунта</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountDatabase(string dbName)
        {
            var accountDatabase = DbLayer.DatabasesRepository.FirstOrDefault(a => a.V82Name == dbName);
            return accountDatabase?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору пользователя аккаунта
        /// </summary>
        /// <param name="id">Идентифиатор AccountUser - пользователя аккаунта</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountUser(Guid id)
        {
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Id == id);
            return accountUser?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по логину пользователя аккаунта
        /// </summary>
        /// <param name="login">Логин AccountUser - пользователя аккаунта</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountUser(string login)
        {
            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Login == login);
            return accountUser?.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору сессии пользователя
        /// </summary>
        /// <param name="id">Идентификатор сессии</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAccountUserSession(Guid id)
        {
            var accountUserSession = DbLayer.AccountUserSessionsRepository.FirstOrDefault(a => a.Id == id);
            return accountUserSession?.AccountUser.AccountId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору сессии пользователя
        /// </summary>
        /// <param name="id">Идентификатор сессии</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAcDbAccess(Guid id)
        {
            var accountUserSession = DbLayer.AcDbAccessesRepository.FirstOrDefault(a => a.ID == id);
            return accountUserSession?.AccountID;
        }

        /// <summary>
        /// Признак что аккаунт является владельцем сервиса
        /// </summary>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <returns>true - если текущий аккаунт является владельцем сервиса</returns>
        protected bool AccountIsServiceOwner(Guid billingServiceId)
        {
            var billingService =
                DbLayer.BillingServiceRepository.FirstOrDefault(service =>
                    (service.Id == billingServiceId || service.Key == billingServiceId) &&
                    service.AccountOwnerId == AccessProvider.ContextAccountId);

            return billingService != null;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору сервиса
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByServiceId(Guid serviceId)
        {
            var billingService = DbLayer.BillingServiceRepository.FirstOrDefault(a => a.Id == serviceId);
            return billingService?.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по заявке на изменение сервиса
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByChangeServiceRequestId(Guid changeServiceRequestId)
        {
            var billingService = DbLayer.GetGenericRepository<ChangeServiceRequest>()
                .FirstOrDefault(a => a.Id == changeServiceRequestId)?.BillingService;
            return billingService?.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по ключу сервиса
        /// </summary>
        /// <param name="serviceKey">Ключ сервиса</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByServiceKey(Guid serviceKey)
        {
            var billingService = DbLayer.BillingServiceRepository.FirstOrDefault(a => a.Key == serviceKey);
            return billingService.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору реквизитов агента
        /// </summary>
        /// <param name="agentRequisitesId">Идентификатор реквизитов агента</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAgentRequisitesId(Guid agentRequisitesId)
        {
            var agentRequisites = DbLayer.AgentRequisitesRepository.FirstOrDefault(a => a.Id == agentRequisitesId);
            return agentRequisites?.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по идентификатору заявки на вывод средств
        /// </summary>
        /// <param name="agentCashOutRequestId">Идентификатор заявки на вывод средств</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByAgentCashOutRequestId(Guid agentCashOutRequestId)
        {
            var agentCashOutRequest =
                DbLayer.AgentCashOutRequestRepository.FirstOrDefault(a => a.Id == agentCashOutRequestId);
            return agentCashOutRequest?.AgentRequisites.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по ID загруженного файла
        /// </summary>
        /// <param name="uploadedFileId">ID загруженного файла</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByUploadedFile(Guid uploadedFileId)
        {
            var uploadedFile = DbLayer.UploadedFileRepository.FirstOrDefault(w => w.Id == uploadedFileId);
            return uploadedFile?.AccountId;
        }

        /// <summary>
        /// Получить ID аккаунта по ID файла разработки сервиса
        /// </summary>
        /// <param name="billingService1CFileId"></param>
        /// <returns></returns>
        protected Guid? AccountIdByBillingService1CFile(Guid billingService1CFileId)
        {
            var billingService1CFile =
                DbLayer.BillingService1CFileRepository.FirstOrDefault(w => w.Id == billingService1CFileId);

            return billingService1CFile?.Service?.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по ключу услуги
        /// </summary>
        /// <param name="key">Ключ услуги</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByServiceTypeKey(Guid key)
        {
            var billingServiceType = DbLayer.BillingServiceTypeRepository.AsQueryableNoTracking().Include(i => i.Service)
                .FirstOrDefault(a => a.Key == key);
            return billingServiceType?.Service?.AccountOwnerId;
        }

        /// <summary>
        /// Получить идентификатор аккаунта по номеру
        /// </summary>
        /// <param name="indexNumber">Номер аккаунта</param>
        /// <returns>Идентификатор аккаунта или Null, если значение не обнаружено или не установлено</returns>
        protected Guid? AccountIdByIndexNumber(int indexNumber) =>
            DbLayer.AccountsRepository.FirstOrDefault(a => a.IndexNumber == indexNumber)?.Id;

        /// <summary>
        /// Получить Id аккаунта по почте пользователя
        /// </summary>
        /// <param name="email">Почта пользователя</param>
        /// <returns>Id аккаунта</returns>
        protected Guid? GetAccountIdByUserEmail(string email) =>
            DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Email == email && au.EmailStatus  == "Checked")?.AccountId;


        /// <summary>
        /// Определить, имеет ли аккаунт доступ к базе данных
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="databaseId">Идентификатор базы данных</param>
        /// <returns>true - есть доступ, false - нет доступа</returns>
        public bool AccountHasAccessToDataBase(Guid accountId, Guid databaseId)
        {
            var account = DbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            if (account == null)
                return false;

            var database = DbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == databaseId);
            if (database == null)
                return false;

            if (database.AccountId == accountId)
                return true;

            var accountUsersOfAccount = account.AccountUsers.Select(a => a.Id);
            var accessList = DbLayer.AcDbAccessesRepository.Where(
                a =>
                    a.AccountDatabaseID == databaseId && a.AccountUserID != null &&
                    accountUsersOfAccount.Contains(a.AccountUserID.Value));

            return accessList.Any();
        }

        /// <summary>
        ///     Получить информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">ID информационной базы</param>
        /// <returns></returns>
        public AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
        {
            var database = DbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);
            if (database == null)
                return null;

            AccessProvider.HasAccess(ObjectAction.AccountDatabase_Detail, () => database.AccountId);
            return database;
        }

        /// <summary>
        /// Версия метода для логирования роботы корворкера когда в инициаторах нужно записывать
        /// пользователя, поставившего задачу на выполнение
        /// </summary>
        /// <param name="getAccountId">Функтор получения идентификатора аккаунта</param>
        /// <param name="log">Действие для записи в лог</param>
        /// <param name="description">Описание события</param>
        /// <param name="accountUserInitiatorId">Идентификатор пользователя, инициатора записи в лог</param>
        public void LogEvent(Func<Guid?> getAccountId, LogActions log, string description, Guid accountUserInitiatorId)
        {
            try
            {
                var accountId = getAccountId.Invoke();

                if (accountId == null)
                {
                    Logger.Warn($"анонимная операция: '{description}' ");
                    return;
                }

                LogEventHelper.LogEventInitiator(DbLayer, accountId.Value, log, description, accountUserInitiatorId, handlerException);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка логирования воркера]");
            }
        }

        /// <summary>
        /// Метод для логирования роботы корворкера
        /// </summary>
        /// <param name="getAccountId">Функтор получения идентификатора аккаунта</param>
        /// <param name="log">Действие для записи в лог</param>
        /// <param name="description">Описание события</param>
        public void LogEvent(Func<Guid?> getAccountId, LogActions log, string description)
        {
            try
            {
                var accountId = getAccountId.Invoke();

                if (accountId == null)
                {
                    Logger.Warn($"анонимная операция: '{description}' ");
                    return;
                }

                LogEventHelper.LogEvent(DbLayer, accountId.Value, AccessProvider, log, description);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка логирования воркера]");
            }

        }
    }
}
