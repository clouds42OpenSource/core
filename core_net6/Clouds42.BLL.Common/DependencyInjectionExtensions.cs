﻿using Clouds42.BLL.Common.Access.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Encrypt;
using Clouds42.PowerShellClient;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.BLL.Common
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddCommonBll(this IServiceCollection services)
        {
            services.AddTransient<ISystemAccessProvider, SystemAccessProvider>();
            services.AddTransient<IHashProvider, Md5HashProvider>();
            services.AddTransient<DesEncryptionProvider>();
            services.AddTransient<AesEncryptionProvider>();
            services.AddTransient<PowerShellClientWrapper>();
            services.AddScoped<ICloudLocalizer, CloudLocalizer>();
            return services;
        }
    }
}
