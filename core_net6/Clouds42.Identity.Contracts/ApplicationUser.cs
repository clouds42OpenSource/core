﻿using Microsoft.AspNetCore.Identity;

namespace Clouds42.Identity.Contracts
{
    /// <summary>
    /// Тип пользователя в identity системе
    /// </summary>
    public class ApplicationUser: IdentityUser<Guid>
    {
        /// <summary>
        /// Пользователь активирован (подтвержден мобильный или одобрен менеджером)
        /// </summary>
        public bool Activated { get; set; }

    }
}
