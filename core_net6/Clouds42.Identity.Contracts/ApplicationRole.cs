﻿using Microsoft.AspNetCore.Identity;

namespace Clouds42.Identity.Contracts
{
    /// <summary>
    /// Тип для хранения роли в identity системе
    /// </summary>
    public class ApplicationRole: IdentityRole<string>
    {
        
    }
}
