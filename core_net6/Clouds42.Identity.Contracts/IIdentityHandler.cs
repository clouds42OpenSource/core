﻿namespace Clouds42.Identity.Contracts
{
    /// <summary>
    /// Обработчик действий связанный с API identity системы
    /// </summary>
    public interface IIdentityHandler
    {
        /// <summary>
        /// Производит аутентификацию
        /// </summary>
        /// <param name="username">Логин</param>
        /// <param name="password">Пароль</param>
        /// <returns></returns>
        Task<ApplicationUser> AuthenticateAsync(string username, string password);
    }
}
