﻿using System;
using System.Data;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore.Storage;

namespace Repositories
{
    public class SmartTransaction(Clouds42DbContext context) : ISmartTransaction
    {
        private int CountActiveTransaction { set; get; }

        private IDbContextTransaction _dbTransaction;
        public ISmartTransaction Get(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (CountActiveTransaction == 0)
                _dbTransaction = context.Database.BeginTransaction();

            CountActiveTransaction++;
            return this;
        }

        public void Commit()
        {
            if (_dbTransaction == null)
                throw new InvalidOperationException("Коммит не активной транзакции");

            CountActiveTransaction--;

            if (CountActiveTransaction == 0)
                _dbTransaction.Commit();
        }


        public void Rollback()
        {
            if (_dbTransaction == null)
                throw new InvalidOperationException("Откатывание не активной транзакции");
            CountActiveTransaction--;

            if (CountActiveTransaction <= 0)
                _dbTransaction.Rollback();
        }

        public void Dispose() {
            if (CountActiveTransaction <= 0)
                _dbTransaction.Dispose();
        }
    }
}
