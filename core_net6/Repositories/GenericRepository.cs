﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, new()
    {
        protected readonly Clouds42DbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        /// <summary>Инициализирует стандартный репозиторий</summary>
        public GenericRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        {
            Context = context;
            SmartTransaction = smartTransaction;
            DbSet = context.Set<TEntity>();
        }

        public ISmartTransaction SmartTransaction { get; }

        /// <summary> Возвращает список сущностей. Аналог LINQ Where </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Where(Expression<Func<TEntity, bool>> filter = null)
        {

            return filter != null ?  DbSet.Where(filter).ToList() : DbSet.ToList();
        }

        public virtual IQueryable<TEntity> WhereLazy(Expression<Func<TEntity, bool>> filter = null)
        {

            return filter != null ? DbSet.Where(filter) : DbSet;
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return DbSet;
        }

        /// <summary> Возвращает сущность. Аналог LINQ FirstOrDefault </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter != null ?  DbSet.AsQueryable().FirstOrDefault(filter) : DbSet.AsQueryable().FirstOrDefault();
        }

        /// <summary> Возвращает сущность. Аналог LINQ FirstOrDefault </summary>
        /// <param name="filter">Условие отбора сущностей</param>
        /// <param name="exceptionMessage">Текст исключения.</param>
        public TEntity FirstOrThrowException(Expression<Func<TEntity, bool>> filter = null, string exceptionMessage = null)
        {
            var item = filter == null ? null : DbSet.AsQueryable().FirstOrDefault(filter);

            if (item == null)
                throw new NotFoundException(exceptionMessage ?? $"Item {typeof(TEntity)} not found in collection.");

            return item;
        }

        public virtual TEntity GetFirst(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.AsQueryable().FirstOrDefault(predicate);
        }

        public virtual Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            return filter != null ? DbSet.AsQueryable().FirstOrDefaultAsync(filter) : DbSet.AsQueryable().FirstOrDefaultAsync();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public virtual IQueryable<TEntity> All()
        {
            return DbSet;
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.AsQueryable().Where(predicate);
        }

        /// <summary> Получает сущность по первичным ключам </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity GetById(params object[] id)
        {
            return id == null ? null : DbSet.Find(id);
        }

        public virtual async Task<TEntity> GetByIdAsync(params object[] id)
        {
            if (id == null) return null;

            return await DbSet.FindAsync(id);
        }

        /// <summary> Вставляет новую сущность в контекст. </summary>
        /// <param name="entity"></param>
        public virtual void Insert(TEntity entity)
        {
            if (entity == null) return;

            DbSet.Add(entity);
        }

        /// <summary> Вставляет массив новых сущностей в контекст. </summary>
        /// <param name="entities"></param>
        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        /// <summary> Удаляет сущность по первичным ключам </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            if (id == null) return;

            var entityToDelete = DbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary> Удаляет сущность </summary>
        /// <param name="entityToDelete"></param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (entityToDelete == null) return;

            Context.Entry(entityToDelete).State = EntityState.Detached;
            DbSet.Attach(entityToDelete);
            DbSet.Remove(entityToDelete);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> toDelete)
        {
            DbSet.RemoveRange(toDelete);
        }

        /// <summary> Обновляет сущность в репозитории </summary>
        /// <param name="entityToUpdate"></param>
        public virtual void Update(TEntity entityToUpdate)
        {
            if (entityToUpdate == null) return;

            Context.Entry(entityToUpdate).State = EntityState.Detached;
            DbSet.Attach(entityToUpdate);
            Context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual void Reload(TEntity entity)
        {
            Context.Entry(entity).Reload();
        }

        public virtual bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Any(predicate);
        }

        public virtual IEnumerable<T> Select<T>(Expression<Func<TEntity, T>> selector)
        {
            return DbSet.Select(selector);
        }

        public virtual IOrderedQueryable<TEntity> OrderBy<T>(Expression<Func<TEntity, T>> selector)
        {
            return DbSet.OrderBy(selector);
        }

        public virtual IOrderedQueryable<TEntity> OrderByDescending<T>(Expression<Func<TEntity, T>> selector)
        {
            return DbSet.OrderByDescending(selector);
        }

        public virtual int Count()
        {
            return DbSet.Count();
        }

        public virtual IQueryable<TEntity> AsQueryableNoTracking()
        {
            return DbSet.AsNoTracking();
        }
    }
}
