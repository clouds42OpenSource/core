﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.Domain;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;
using Clouds42.Domain.DataModels.Its;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.IDataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Repositories.Implementations;

namespace Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        #region  Repositories
        private ICoreWorkerRepository _coreWorker;
        private ICoreWorkerTasksQueueRepository _coreWorkerTasksQueue;
        private ICoreWorkerTasksQueueLiveRepository _coreWorkerTasksQueueLive;
        private ICsResourceRepository _csResources;
        private IAccountDatabaseRepository _databases;
        private IAccountConfigurationRepository _accountConfigurationRepository;
        private IInvoiceRepository _invoices;
        private IPartnersRepository _partner;
        private IPaymentRepository _payment;
        private IResourcesRepository _resource;
        private IResourcesConfigurationRepository _resourceConfiguration;
        private IPlatformVersionReferencesRepository _platformVersionReferencesRepository;
        private IAgentRequisitesRepository _agentRequisitesRepository;
        private IAccountUserRepository _users;
        private IAgentWalletRepository _agentWalletRepository;
        private IAgentPaymentRepository _agentPaymentRepository;
        private IAgentCashOutRequestRepository _agentCashOutRequestRepository;
        private IAccountCsResourceValueRepository _accountsResourcesValue;
        private IAccountDatabaseBackupsRepository _accountDatabaseBackupRepository;
        private ICloudServicesSegmentRepository _cloudServicesSegmentRepository;
        private ICloudServicesSegmentStorageRepository _cloudServicesSegmentStorageRepository;
        private ICloudServicesEnterpriseServerRepository _cloudServicesEnterpriseServerRepository;
        private ICloudServicesFileStorageServerRepository _cloudServicesFileStorageServerRepository;
        private ICloudServiceRepository _cloudServiceRepository;
        private IBillingAccountRepository _billingAccount;
        private ICloudChangesRepository _cloudChanges;
        private IAcDbSupportRepository _acDbSupportRepository;
        private IAccountUserSessionRepository _accountUserSessions;
        private IAccountRepository _accounts;
        private IWebSocketConnectionRepository _webSocketConnectionRepository;
        private IBillingServiceRepository _billingServiceRepository;
        private IUploadedFileRepository _uploadedFileRepository;
        private IGenericRepository<AcDbAccess> _accesses;
        private IGenericRepository<AccountDatabaseUpdateVersionMapping> _accountDatabaseUpdateVersionMappingRepository;
        private IGenericRepository<AccountEmail> _accountEmailRepository;
        private IGenericRepository<AccountRate> _accountRateRepository;
        private IAccountUserRoleRepository _accountUserRole;
        private IGenericRepository<AcDbAccRolesJho> _acDbAccRolesJhoRepository;
        private IGenericRepository<AcDbSupportHistory> _acDbSupportHistoryRepository;
        private IGenericRepository<AvailableMigration> _availableMigrationRepository;
        private IGenericRepository<CloudChangesAction> _cloudChangesAction;
        private IGenericRepository<CloudChangesGroup> _cloudChangesGroup;
        private IGenericRepository<CloudConfiguration> _cloudConfigurationRepository;
        private IGenericRepository<CloudCore> _cloudCore;
        private IGenericRepository<CloudServicesBackupStorage> _cloudServicesBackupStorageRepository;
        private IGenericRepository<CloudServicesContentServer> _cloudServicesContentServerRepository;
        private IGenericRepository<CloudServicesGatewayTerminal> _cloudServicesGatewayTerminalRepository;
        private IGenericRepository<CloudServicesSegmentTerminalServer> _cloudServicesSegmentTerminalServerRepository;
        private IGenericRepository<CloudServicesSqlServer> _cloudServicesSqlServerRepository;
        private IGenericRepository<CloudServicesTerminalFarm> _cloudServicesTerminalFarmRepository;
        private IGenericRepository<CloudTerminalServer> _cloudTerminalServer;
        private IGenericRepository<PublishNodeReference> _publishNodeReference;
        private IGenericRepository<CloudServicesContentServerNode> _cloudServicesContentServerNode;
        private IGenericRepository<CoreWorkerTask> _coreWorkerTask;
        private IGenericRepository<DbTemplate> _dbTemplate;
        private IGenericRepository<DbTemplateUpdate> _dbTemplateUpdateRepository;
        private IGenericRepository<Configurations1C> _configurations1CRepository;
        private IGenericRepository<ConfigurationNameVariation> _configurationNameVariationRepository;
        private IGenericRepository<ConfigurationsCfu> _configurationsCfuRepository;
        private IGenericRepository<DbTemplateDelimiters> _dbTemplateDelimitersRepository;
        private IGenericRepository<AccountDatabaseOnDelimiters> _accountDatabaseDelimitersRepository;
        private IGenericRepository<AccountDatabaseBackupHistory> _accountDatabaseBackupHistoryRepository;
        private IGenericRepository<InvoiceProduct> _invoiceProduct;
        private IGenericRepository<InvoiceReceipt> _invoiceReceipt;
        private IGenericRepository<FlowResourcesScope> _flowResourcesScopeRepository;
        private IGenericRepository<Locale> _locale;
        private IGenericRepository<MigrationAccountDatabaseHistory> _migrationAccountDatabaseHistoryRepository;
        private IGenericRepository<MigrationAccountHistory> _migrationAccountHistoryRepository;
        private IGenericRepository<NotificationBuffer> _notificationBuffer;
        private IGenericRepository<ProvidedService> _providedServiceRepository;
        private IGenericRepository<Rate> _rateRepository;
        private IGenericRepository<DiskResource> _diskResourceRepository;
        private IGenericRepository<SessionResource> _sessionResourceRepository;
        private IGenericRepository<BillingServiceType> _billingServiceTypeRepository;
        private IGenericRepository<Industry> _industryRepository;
        private IGenericRepository<CloudFile> _cloudFileRepository;
        private IGenericRepository<IndustryDependencyBillingService> _industryDependencyBillingServiceRepository;
        private IGenericRepository<BillingServiceTypeRelation> _billingServiceTypeRelationRepository;
        private IGenericRepository<BillingService1CFile> _billingService1CFileRepository;
        private IGenericRepository<Supplier> _supplierRepository;
        private IGenericRepository<SupplierReferralAccount> _supplierReferralAccountRepository;
        private IGenericRepository<PhysicalPersonRequisites> _physicalPersonRequisitesRepository;
        private IGenericRepository<LegalPersonRequisites> _legalPersonRequisitesRepository;
        private IGenericRepository<SoleProprietorRequisites> _soleProprietorRequisitesRepository;
        private IGenericRepository<AgentRequisitesFile> _agentRequisitesFileRepository;
        private IGenericRepository<PrintedHtmlForm> _printedHtmlFormRepository;
        private IGenericRepository<PrintedHtmlFormFile> _printedHtmlFormFileRepository;
        private IGenericRepository<ProcessFlow> _processFlowRepository;
        private IGenericRepository<ActionFlow> _actionFlowRepository;
        private IGenericRepository<AgentCashOutRequestFile> _agentCashOutRequestFileRepository;
        private IGenericRepository<CoreHosting> _coreHostingRepository;
        private IAccountSaleManagerRepository _accountSaleManagerRepository;
        private IGenericRepository<AgentPaymentRelation> _agentPaymentRelationRepository;
        private IGenericRepository<ServiceAccount> _serviceAccountRepository;
        private IGenericRepository<ManageDbOnDelimitersAccess> _manageDbOnDelimitersAccessRepository;
        private IGenericRepository<AgencyAgreement> _agencyAgreementRepository;
        private IGenericRepository<AgentTransferBalanseRequest> _agentTransferBalanceRequestRepository;
        private IGenericRepository<DelimiterSourceAccountDatabase> _delimiterSourceAccountDatabaseRepository;
        private IGenericRepository<Incident> _incidentRepository;
        private IGenericRepository<Department> _departmentRepository;
        private IGenericRepository<AccountUserDepartment> _accountUserDepartmentRepository;
        private IGenericRepository<AutoUpdateNode> _autoUpdateNodeRepository;
        private IBillingServiceTypeActivityForAccountRepository _billingServiceTypeActivityForAccountRepository;
        private ISavedPaymentMethodRepository _accountPaymentMethodRepository;
        private ISavedPaymentMethodBankCardRepository _accountPaymentMethodBankCardRepository;
        private IAccountAutoPayConfigurationRepository _accountAutoPayConfigurationRepository;
        private ILink42ConfigurationsRepository _link42ConfigurationsRepository;
        private IConnectionRepository _connectionRepository;
        private IGoogleSheetRepository _googleSheetRepository;
        private IWaitsInfoRepository _waitsInfoRepository;
        private INotificationRepository _notificationRepository;
        private IFeedbackRepository _feedbackRepository;
        private IAccountUserBitDepthRepository _accountUserBitDepthRepository;
        private ILink42ConfigurationBitDepthRepository _link42ConfigurationsBitDepthRepository;
        private IEmailSmsVerificationRepository _emailSmsVerificationRepository;
        private IAccountUserNotificationSettingsRepository _accountUserNotificationSettingsRepository;
        private INotificationSettingsRepository _notificationSettingsRepository;
        private IArticleRepository _articleRepository;
        private IGenericRepository<UpdateNodeQueue> _updateNodeQueueRepository;
        private IArticleTransactionRepository _articleTransactionRepository;
        private IArticleWalletsRepository _articleWalletsRepository;
        private IAccountFileRepository _accountFileRepository;
        private IAccountAdditionalCompanyRepository _accountAdditionalCompanyRepository;
        #endregion
        #region  RepositoriesImplementations
        public ICloudServicesSegmentRepository CloudServicesSegmentRepository =>
          _cloudServicesSegmentRepository ??= new CloudServicesSegmentRepository(Context, SmartTransaction);

        public ICloudServicesSegmentStorageRepository CloudServicesSegmentStorageRepository =>
            _cloudServicesSegmentStorageRepository ??= new CloudServicesSegmentStorageRepository(Context, SmartTransaction);

        public ICloudServicesEnterpriseServerRepository CloudServicesEnterpriseServerRepository =>
            _cloudServicesEnterpriseServerRepository ??= new CloudServicesEnterpriseServerRepository(Context, SmartTransaction);

        public ICloudServicesFileStorageServerRepository CloudServicesFileStorageServerRepository =>
            _cloudServicesFileStorageServerRepository ??= new CloudServicesFileStorageServerRepository(Context, SmartTransaction);

        public IAcDbSupportRepository AcDbSupportRepository =>
            _acDbSupportRepository ??= new AcDbSupportRepository(Context, SmartTransaction);

        public ICloudChangesRepository CloudChangesRepository =>
            _cloudChanges ??= new CloudChangesRepository(Context, SmartTransaction);

        public IPlatformVersionReferencesRepository PlatformVersionReferencesRepository =>
            _platformVersionReferencesRepository ??= new PlatformVersionReferencesRepository(Context, SmartTransaction);

        public IPartnersRepository PartnerRepository =>
            _partner ??= new PartnerRepository(Context, SmartTransaction);

        public IPaymentRepository PaymentRepository =>
            _payment ??= new PaymentRepository(Context, SmartTransaction);

        public IAgentWalletRepository AgentWalletRepository =>
            _agentWalletRepository ??= new AgentWalletRepository(Context, SmartTransaction);

        public IAgentPaymentRepository AgentPaymentRepository =>
            _agentPaymentRepository ??= new AgentPaymentRepository(Context, SmartTransaction);

        public IAgentRequisitesRepository AgentRequisitesRepository =>
            _agentRequisitesRepository ??= new AgentRequisitesRepository(Context, SmartTransaction);

        public IAccountUserSessionRepository AccountUserSessionsRepository =>
            _accountUserSessions ??= new AccountUserSessionRepository(Context, SmartTransaction);

        public IInvoiceRepository InvoiceRepository =>
            _invoices ??= new InvoiceRepository(Context, SmartTransaction);
        public IAccountUserRepository AccountUsersRepository =>
            _users ??= new AccountUserRepository(Context, SmartTransaction);

        public IAccountRepository AccountsRepository =>
            _accounts ??= new AccountRepository(Context, SmartTransaction);

        public IAccountDatabaseRepository DatabasesRepository =>
            _databases ??= new AccountDatabaseRepository(Context, SmartTransaction);
        public IAccountConfigurationRepository AccountConfigurationRepository =>
            _accountConfigurationRepository ??= new AccountConfigurationRepository(Context, SmartTransaction);

        public IAccountDatabaseBackupsRepository AccountDatabaseBackupRepository =>
            _accountDatabaseBackupRepository ??= new AccountDatabaseBackupsRepository(Context, SmartTransaction);

        public ICloudServiceRepository CloudServiceRepository =>
            _cloudServiceRepository ??= new CloudServiceRepository(Context, SmartTransaction);

        public ICsResourceRepository CsResourceRepository =>
            _csResources ??= new CsResourceRepository(Context, SmartTransaction);

        public IAccountCsResourceValueRepository AccountCsResourceValueRepository =>
            _accountsResourcesValue ??= new AccountCsResourceValueRepository(Context, SmartTransaction);

        public IBillingAccountRepository BillingAccountRepository =>
            _billingAccount ??= new BillingAccountRepository(Context, SmartTransaction);

        public IResourcesRepository ResourceRepository =>
            _resource ??= new ResourcesRepository(Context, SmartTransaction);

        public IAgentCashOutRequestRepository AgentCashOutRequestRepository =>
            _agentCashOutRequestRepository ??= new AgentCashOutRequestRepository(Context, SmartTransaction);

        public IResourcesConfigurationRepository ResourceConfigurationRepository =>
            _resourceConfiguration ??= new ResourcesConfigurationRepository(Context, SmartTransaction);

        public ICoreWorkerRepository CoreWorkerRepository =>
            _coreWorker ??= new CoreWorkerRepository(Context, SmartTransaction);

        public ICoreWorkerTasksQueueRepository CoreWorkerTasksQueueRepository =>
            _coreWorkerTasksQueue ??= new CoreWorkerTasksQueueRepository(Context, SmartTransaction);

        public ICoreWorkerTasksQueueLiveRepository CoreWorkerTasksQueueLiveRepository =>
            _coreWorkerTasksQueueLive ??= new CoreWorkerTasksQueueLiveRepository(Context, SmartTransaction);

        public IWebSocketConnectionRepository WebSocketConnectionRepository =>
            _webSocketConnectionRepository ??= new WebSocketConnectionRepository(Context, SmartTransaction);

        public IBillingServiceRepository BillingServiceRepository =>
            _billingServiceRepository ??= new BillingServiceRepository(Context, SmartTransaction);

        public IUploadedFileRepository UploadedFileRepository =>
            _uploadedFileRepository ??= new UploadedFileRepository(Context, SmartTransaction);

        public IGenericRepository<NotificationBuffer> NotificationBufferRepository =>
            _notificationBuffer ??= new GenericRepository<NotificationBuffer>(Context, SmartTransaction);

        public IGenericRepository<DbTemplateDelimiters> DbTemplateDelimitersReferencesRepository =>
            _dbTemplateDelimitersRepository ??= new GenericRepository<DbTemplateDelimiters>(Context, SmartTransaction);

        public IGenericRepository<AccountDatabaseOnDelimiters> AccountDatabaseDelimitersRepository =>
            _accountDatabaseDelimitersRepository ??= new GenericRepository<AccountDatabaseOnDelimiters>(Context, SmartTransaction);

        public IGenericRepository<AccountDatabaseBackupHistory> AccountDatabaseBackupHistoryRepository =>
            _accountDatabaseBackupHistoryRepository ??= new GenericRepository<AccountDatabaseBackupHistory>(Context, SmartTransaction);

        public IGenericRepository<CloudConfiguration> CloudConfigurationRepository =>
            _cloudConfigurationRepository ??= new GenericRepository<CloudConfiguration>(Context, SmartTransaction);

        public IGenericRepository<Supplier> SupplierRepository =>
            _supplierRepository ??= new GenericRepository<Supplier>(Context, SmartTransaction);

        public IGenericRepository<SupplierReferralAccount> SupplierReferralAccountRepository =>
            _supplierReferralAccountRepository ??= new GenericRepository<SupplierReferralAccount>(Context, SmartTransaction);

        public IGenericRepository<PhysicalPersonRequisites> PhysicalPersonRequisitesRepository =>
            _physicalPersonRequisitesRepository ??= new GenericRepository<PhysicalPersonRequisites>(Context, SmartTransaction);

        public IGenericRepository<LegalPersonRequisites> LegalPersonRequisitesRepository =>
            _legalPersonRequisitesRepository ??= new GenericRepository<LegalPersonRequisites>(Context, SmartTransaction);

        public IGenericRepository<SoleProprietorRequisites> SoleProprietorRequisitesRepository =>
            _soleProprietorRequisitesRepository ??= new GenericRepository<SoleProprietorRequisites>(Context, SmartTransaction);

        public IGenericRepository<AgentRequisitesFile> AgentRequisitesFileRepository =>
            _agentRequisitesFileRepository ??= new GenericRepository<AgentRequisitesFile>(Context, SmartTransaction);

        public IGenericRepository<PrintedHtmlForm> PrintedHtmlFormRepository =>
            _printedHtmlFormRepository ??= new GenericRepository<PrintedHtmlForm>(Context, SmartTransaction);

        public IGenericRepository<PrintedHtmlFormFile> PrintedHtmlFormFileRepository =>
            _printedHtmlFormFileRepository ??= new GenericRepository<PrintedHtmlFormFile>(Context, SmartTransaction);

        public IGenericRepository<AccountEmail> AccountEmailRepository =>
            _accountEmailRepository ??= new GenericRepository<AccountEmail>(Context, SmartTransaction);

        public IGenericRepository<Rate> RateRepository =>
            _rateRepository ??= new GenericRepository<Rate>(Context, SmartTransaction);

        public IGenericRepository<AccountRate> AccountRateRepository =>
            _accountRateRepository ??= new GenericRepository<AccountRate>(Context, SmartTransaction);

        public IAccountUserRoleRepository AccountUserRoleRepository =>
            _accountUserRole ??= new AccountUserRoleRepository(Context, SmartTransaction);

        public IGenericRepository<InvoiceProduct> InvoiceProductRepository =>
            _invoiceProduct ??= new GenericRepository<InvoiceProduct>(Context, SmartTransaction);

        public IGenericRepository<InvoiceReceipt> InvoiceReceiptRepository =>
            _invoiceReceipt ??= new GenericRepository<InvoiceReceipt>(Context, SmartTransaction);

        public IGenericRepository<FlowResourcesScope> FlowResourcesScopeRepository =>
            _flowResourcesScopeRepository ??= new GenericRepository<FlowResourcesScope>(Context, SmartTransaction);

        public IGenericRepository<AcDbAccess> AcDbAccessesRepository =>
            _accesses ??= new GenericRepository<AcDbAccess>(Context, SmartTransaction);

        public IGenericRepository<CloudTerminalServer> CloudTerminalServerRepository =>
            _cloudTerminalServer ??= new GenericRepository<CloudTerminalServer>(Context, SmartTransaction);

        public IGenericRepository<PublishNodeReference> PublishNodeReferenceRepository =>
            _publishNodeReference ??= new GenericRepository<PublishNodeReference>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesContentServerNode> CloudServicesContentServerNodeRepository =>
            _cloudServicesContentServerNode ??= new GenericRepository<CloudServicesContentServerNode>(Context, SmartTransaction);

        public IGenericRepository<DiskResource> DiskResourceRepository =>
            _diskResourceRepository ??= new GenericRepository<DiskResource>(Context, SmartTransaction);

        public IGenericRepository<SessionResource> SessionResourceRepository =>
    _sessionResourceRepository ??= new GenericRepository<SessionResource>(Context, SmartTransaction);

        public IGenericRepository<BillingServiceType> BillingServiceTypeRepository =>
            _billingServiceTypeRepository ??= new GenericRepository<BillingServiceType>(Context, SmartTransaction);

        public IGenericRepository<DbTemplate> DbTemplateRepository =>
            _dbTemplate ??= new GenericRepository<DbTemplate>(Context, SmartTransaction);

        public IGenericRepository<DbTemplateUpdate> DbTemplateUpdateRepository =>
            _dbTemplateUpdateRepository ??= new GenericRepository<DbTemplateUpdate>(Context, SmartTransaction);

        public IGenericRepository<Configurations1C> Configurations1CRepository =>
            _configurations1CRepository ??= new GenericRepository<Configurations1C>(Context, SmartTransaction);

        public IGenericRepository<ConfigurationNameVariation> ConfigurationNameVariationRepository =>
            _configurationNameVariationRepository ??= new GenericRepository<ConfigurationNameVariation>(Context, SmartTransaction);

        public IGenericRepository<CoreWorkerTask> CoreWorkerTaskRepository =>
            _coreWorkerTask ??= new GenericRepository<CoreWorkerTask>(Context, SmartTransaction);

        public IGenericRepository<CloudCore> CloudCoreRepository =>
            _cloudCore ??= new GenericRepository<CloudCore>(Context, SmartTransaction);

        public IGenericRepository<AgentCashOutRequestFile> AgentCashOutRequestFileRepository =>
            _agentCashOutRequestFileRepository ??= new GenericRepository<AgentCashOutRequestFile>(Context, SmartTransaction);

        public IGenericRepository<AcDbAccRolesJho> AcDbAccRolesJhoRepository =>
            _acDbAccRolesJhoRepository ??= new GenericRepository<AcDbAccRolesJho>(Context, SmartTransaction);

        public IGenericRepository<Locale> LocaleRepository =>
            _locale ??= new GenericRepository<Locale>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesGatewayTerminal> CloudServicesGatewayTerminalRepository =>
            _cloudServicesGatewayTerminalRepository ??= new GenericRepository<CloudServicesGatewayTerminal>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesTerminalFarm> CloudServicesTerminalFarmRepository =>
            _cloudServicesTerminalFarmRepository ??= new GenericRepository<CloudServicesTerminalFarm>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesContentServer> CloudServicesContentServerRepository =>
            _cloudServicesContentServerRepository ??= new GenericRepository<CloudServicesContentServer>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesSqlServer> CloudServicesSqlServerRepository =>
            _cloudServicesSqlServerRepository ??= new GenericRepository<CloudServicesSqlServer>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesBackupStorage> CloudServicesBackupStorageRepository =>
            _cloudServicesBackupStorageRepository ??= new GenericRepository<CloudServicesBackupStorage>(Context, SmartTransaction);

        public IGenericRepository<AvailableMigration> AvailableMigrationRepository =>
            _availableMigrationRepository ??= new GenericRepository<AvailableMigration>(Context, SmartTransaction);

        public IGenericRepository<AcDbSupportHistory> AcDbSupportHistoryRepository =>
            _acDbSupportHistoryRepository ??= new GenericRepository<AcDbSupportHistory>(Context, SmartTransaction);

        public IGenericRepository<CloudChangesAction> CloudChangesActionRepository =>
            _cloudChangesAction ??= new GenericRepository<CloudChangesAction>(Context, SmartTransaction);

        public IGenericRepository<CloudChangesGroup> CloudChangesGroupRepository =>
            _cloudChangesGroup ??= new GenericRepository<CloudChangesGroup>(Context, SmartTransaction);

        public IGenericRepository<ProvidedService> ProvidedServiceRepository =>
            _providedServiceRepository ??= new GenericRepository<ProvidedService>(Context, SmartTransaction);

        public IGenericRepository<CloudServicesSegmentTerminalServer> CloudServicesSegmentTerminalServerRepository =>
            _cloudServicesSegmentTerminalServerRepository ??= new GenericRepository<CloudServicesSegmentTerminalServer>(Context, SmartTransaction);

        public IGenericRepository<ConfigurationsCfu> ConfigurationsCfuRepository =>
            _configurationsCfuRepository ??= new GenericRepository<ConfigurationsCfu>(Context, SmartTransaction);

        public IGenericRepository<AccountDatabaseUpdateVersionMapping> AccountDatabaseUpdateVersionMappingRepository =>
            _accountDatabaseUpdateVersionMappingRepository ??= new GenericRepository<AccountDatabaseUpdateVersionMapping>(Context, SmartTransaction);

        public IGenericRepository<MigrationAccountHistory> MigrationAccountHistoryRepository =>
            _migrationAccountHistoryRepository ??= new GenericRepository<MigrationAccountHistory>(Context, SmartTransaction);

        public IGenericRepository<MigrationAccountDatabaseHistory> MigrationAccountDatabaseHistoryRepository =>
            _migrationAccountDatabaseHistoryRepository ??= new GenericRepository<MigrationAccountDatabaseHistory>(Context, SmartTransaction);

        public IGenericRepository<Industry> IndustryRepository =>
            _industryRepository ??= new GenericRepository<Industry>(Context, SmartTransaction);

        public IGenericRepository<CloudFile> CloudFileRepository =>
            _cloudFileRepository ??= new GenericRepository<CloudFile>(Context, SmartTransaction);

        public IGenericRepository<IndustryDependencyBillingService> IndustryDependencyBillingServiceRepository =>
            _industryDependencyBillingServiceRepository ??= new GenericRepository<IndustryDependencyBillingService>(Context, SmartTransaction);

        public IGenericRepository<BillingServiceTypeRelation> BillingServiceTypeRelationRepository =>
            _billingServiceTypeRelationRepository ??= new GenericRepository<BillingServiceTypeRelation>(Context, SmartTransaction);

        public IGenericRepository<BillingService1CFile> BillingService1CFileRepository =>
            _billingService1CFileRepository ??= new GenericRepository<BillingService1CFile>(Context, SmartTransaction);

        public IGenericRepository<ProcessFlow> ProcessFlowRepository =>
            _processFlowRepository ??= new GenericRepository<ProcessFlow>(Context, SmartTransaction);

        public IGenericRepository<ActionFlow> ActionFlowRepository =>
            _actionFlowRepository ??= new GenericRepository<ActionFlow>(Context, SmartTransaction);

        public IGenericRepository<CoreHosting> CoreHostingRepository =>
            _coreHostingRepository ??= new GenericRepository<CoreHosting>(Context, SmartTransaction);

        public IAccountSaleManagerRepository AccountSaleManagerRepository =>
            _accountSaleManagerRepository ??= new AccountSaleManagerRepository(Context, SmartTransaction);

        public IGenericRepository<AgentPaymentRelation> AgentPaymentRelationRepository =>
            _agentPaymentRelationRepository ??= new GenericRepository<AgentPaymentRelation>(Context, SmartTransaction);

        public IGenericRepository<ServiceAccount> ServiceAccountRepository =>
            _serviceAccountRepository ??= new GenericRepository<ServiceAccount>(Context, SmartTransaction);

        public IGenericRepository<ManageDbOnDelimitersAccess> ManageDbOnDelimitersAccessRepository =>
            _manageDbOnDelimitersAccessRepository ??= new GenericRepository<ManageDbOnDelimitersAccess>(Context, SmartTransaction);

        public IGenericRepository<AgencyAgreement> AgencyAgreementRepository =>
            _agencyAgreementRepository ??= new GenericRepository<AgencyAgreement>(Context, SmartTransaction);

        public IGenericRepository<AgentTransferBalanseRequest> AgentTransferBalanceRequestRepository =>
            _agentTransferBalanceRequestRepository ??= new GenericRepository<AgentTransferBalanseRequest>(Context, SmartTransaction);

        public IGenericRepository<DelimiterSourceAccountDatabase> DelimiterSourceAccountDatabaseRepository =>
            _delimiterSourceAccountDatabaseRepository ??= new GenericRepository<DelimiterSourceAccountDatabase>(Context, SmartTransaction);

        public IGenericRepository<Incident> IncidentRepository =>
            _incidentRepository ??= new GenericRepository<Incident>(Context, SmartTransaction);

        public IGenericRepository<Department> DepartmentRepository =>
            _departmentRepository ??= new GenericRepository<Department>(Context, SmartTransaction);

        public IGenericRepository<AccountUserDepartment> AccountUserDepartmentRepository =>
        _accountUserDepartmentRepository ??= new GenericRepository<AccountUserDepartment>(Context, SmartTransaction);

        public IGenericRepository<AutoUpdateNode> AutoUpdateNodeRepository =>
            _autoUpdateNodeRepository ??= new GenericRepository<AutoUpdateNode>(Context, SmartTransaction);

        public IBillingServiceTypeActivityForAccountRepository BillingServiceTypeActivityForAccountRepository =>
            _billingServiceTypeActivityForAccountRepository ??= new BillingServiceTypeActivityForAccountRepository(Context, SmartTransaction);

        public ISavedPaymentMethodRepository SavedPaymentMethodRepository =>
            _accountPaymentMethodRepository ??= new SavedPaymentMethodRepository(Context, SmartTransaction);

        public ISavedPaymentMethodBankCardRepository SavedPaymentMethodBankCardRepository =>
            _accountPaymentMethodBankCardRepository ??= new SavedPaymentMethodBankCardRepository(Context, SmartTransaction);

        public IAccountAutoPayConfigurationRepository AccountAutoPayConfigurationRepository =>
            _accountAutoPayConfigurationRepository ??= new AccountAutoPayConfigurationRepository(Context, SmartTransaction);

        public ILink42ConfigurationsRepository Link42ConfigurationsRepository => _link42ConfigurationsRepository ??=
            new Link42ConfigurationRepository(Context, SmartTransaction);

        public ILink42ConfigurationBitDepthRepository Link42ConfigurationBitDepthRepository => _link42ConfigurationsBitDepthRepository ??=
            new Link42ConfigurationBitDepthRepository(Context, SmartTransaction);

        public IConnectionRepository ConnectionRepository => _connectionRepository ??=
            new ConnectionRepository(Context, SmartTransaction);

        public IGoogleSheetRepository GoogleSheetRepository =>
            _googleSheetRepository ??= new GoogleSheetsRepository(Context, SmartTransaction);

        public IWaitsInfoRepository WaitsInfoRepository =>
            _waitsInfoRepository ??= new WaitsInfoRepository(Context, SmartTransaction);

        public INotificationRepository NotificationRepository => _notificationRepository ??=
            new NotificationRepository(Context, SmartTransaction);
        public IFeedbackRepository FeedbackRepository => _feedbackRepository ??=
            new FeedbackRepository(Context, SmartTransaction);

        public IAccountUserBitDepthRepository AccountUserBitDepthRepository => _accountUserBitDepthRepository ??=
            new AccountUserBitDepthRepository(Context, SmartTransaction);


        public IGenericRepository<UpdateNodeQueue> UpdateNodeQueueRepository => _updateNodeQueueRepository ??=
            new GenericRepository<UpdateNodeQueue>(Context, SmartTransaction);

        public IEmailSmsVerificationRepository EmailSmsVerificationRepository => _emailSmsVerificationRepository ??= new EmailSmsVerificationRepository(Context, SmartTransaction);

        public INotificationSettingsRepository NotificationSettingsRepository => _notificationSettingsRepository ??= new NotificationSettingsRepository(Context, SmartTransaction);

        public IAccountUserNotificationSettingsRepository AccountUserNotificationSettingsRepository => _accountUserNotificationSettingsRepository ??= new AccountUserNotificationSettingsRepository(Context, SmartTransaction);
        public IArticleRepository ArticleRepository => _articleRepository ??=
            new ArticleRepository(Context, SmartTransaction);

        public IArticleTransactionRepository ArticleTransactionRepository => _articleTransactionRepository ??=
            new ArticleTransactionRepository(Context, SmartTransaction);

        public IArticleWalletsRepository ArticleWalletsRepository => _articleWalletsRepository ??=
            new ArticleWalletsRepository(Context, SmartTransaction);
        public IAccountFileRepository AccountFileRepository =>
            _accountFileRepository ??= new AccountFileRepository(Context, SmartTransaction);

        public IAccountAdditionalCompanyRepository AccountAdditionalCompanyRepository =>
            _accountAdditionalCompanyRepository ??= new AccountAdditionalCompanyRepository(Context, SmartTransaction);
        #endregion

        private bool _disposed;
        public UnitOfWork(Clouds42DbContext clouds42DbContext)
        {
            Context = clouds42DbContext;
            SmartTransaction = new SmartTransaction(Context);
        }

        public Clouds42DbContext Context { get; }
        public ISmartTransaction SmartTransaction { get; }

        private static readonly BulkConfig BulkConfig = new()
        {
            PreserveInsertOrder = false, SetOutputIdentity = false
        };

        public void Save()
        {
            try
            {
                UpdatePersistentEntities();
                Context.SaveChanges();
            }

            catch (DbUpdateException e)
            {
                throw new ValidateException(e.ToString());
            }
        }

        private void UpdatePersistentEntities()
        {
            var modifiedEntities = Context.ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(s => s.Entity)
                .OfType<IPersistentObject>()
                .ToList();

            var addedEntities = Context.ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(s => s.Entity)
                .OfType<IPersistentObject>()
                .ToList();

            foreach (var persistentObject in modifiedEntities)
                persistentObject.LastEditedDateTime = DateTime.Now;

            foreach (var persistentObject in addedEntities)
            {
                persistentObject.LastEditedDateTime = DateTime.Now;
                persistentObject.CreationDate = DateTime.Now;
            }
        }

        public async Task SaveAsync()
        {
            UpdatePersistentEntities();
            await Context.SaveChangesAsync();
        }

        public async Task BulkUpdateAsync<T>(IEnumerable<T> entities) where T : class
        {
            await Context.BulkUpdateAsync(entities, BulkConfig);
        }

        public void BulkUpdate<T>(IEnumerable<T> entities) where T : class
        {
            Context.BulkUpdate(entities, BulkConfig);
        }

        public async Task BulkDeleteAsync<T>(IEnumerable<T> entities) where T : class
        {
            await Context.BulkDeleteAsync(entities, BulkConfig);
        }

        public void BulkDelete<T>(IEnumerable<T> entities) where T : class
        {
            Context.BulkDelete(entities, BulkConfig);
        }

        public void BulkInsert<T>(IEnumerable<T> entities) where T : class
        {
            Context.BulkInsert(entities, BulkConfig);
        }

        public async Task BulkInsertAsync<T>(IEnumerable<T> entities) where T : class
        {
            await Context.BulkInsertAsync(entities, BulkConfig);
        }

        public IGenericRepository<TEntity> GetGenericRepository<TEntity>() where TEntity : class, new()
        {
            return new GenericRepository<TEntity>(Context, SmartTransaction);
        }

        public List<TResult> ExecuteSqlQueryList<TResult>(string query)
        {
            return Context.Database.SqlQueryRaw<TResult>(query).ToList();
        }

        public async Task<TResult> ExecuteSqlQueryAsync<TResult>(string query)
        {
            return (await Context.Database.SqlQueryRaw<TResult>(query).ToListAsync()).FirstOrDefault();
        }

        /// <summary>
        /// Выполнить sql запрос в базу 1С
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <param name="serverName">Название сервера</param>
        /// <param name="timeout">Таймаут</param>
        public void Execute1CSqlQuery(string query, string serverName, int timeout = 5 * 60)
        {
           
            var connectionString = string.Format(SqlQueries.DatabaseCore.ConnectionStringToSql1CFormat, serverName);

            using var connection =
                new SqlConnection(connectionString);
            var command = new SqlCommand(query, connection) { CommandTimeout = timeout };
            

            connection.Open();

            var reader = command.ExecuteReader();

            reader.Read();
            reader.Close();
        }

        public void SetCommandTimeout(int timeout)
        {
            Context.Database.SetCommandTimeout(timeout);
        }

        /// <summary>
        ///     Выбирает из SQL 1С первый столбик результатов
        /// </summary>
        /// <param name="query">строка запроса</param>
        /// <param name="serverName">SQL сервер сегмента</param>
        /// <returns>Размер базы</returns>
        public IEnumerable Execute1CSqlReader(string query, string serverName)
        {
            var connectionString = string.Format(SqlQueries.DatabaseCore.ConnectionStringToSql1CFormat, serverName);

            using var connection =
                new SqlConnection(connectionString);
            var command = new SqlCommand(query, connection);
            var res = new List<object>();
            connection.Open();
            var reader = command.ExecuteReader();
            while (reader.Read()) res.Add(reader[0]);
            reader.Close();
            return res;
        }

        public async Task RefreshAllAsync()
        {
            var refreshableObjects = Context.ChangeTracker.Entries()
                .Where(entry => entry.State is EntityState.Deleted or EntityState.Modified or EntityState.Unchanged)
                .Select(entry => entry.Entity)
                .ToList();

            foreach (var entity in refreshableObjects)
            {
                await Context.Entry(entity).ReloadAsync();
            }
        }

        public void RefreshAll()
        {
            var refreshableObjects = Context.ChangeTracker.Entries()
                .Where(entry => entry.State is EntityState.Deleted or EntityState.Modified or EntityState.Unchanged or EntityState.Detached or EntityState.Added)
                .Select(entry => entry.Entity)
                .ToList();

            foreach (var entity in refreshableObjects)
            {
                Context.Entry(entity).Reload();
            }
        }
        

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
                Context.Dispose();
            _disposed = true;
        }
    }
}
