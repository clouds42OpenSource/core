﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий по работе с сохраненными платежными способами пользователя
    /// </summary>
    public class SavedPaymentMethodRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<SavedPaymentMethod>(context, smartTransaction), ISavedPaymentMethodRepository
    {
        public SavedPaymentMethod GetById(Guid id)
            => DbSet.FirstOrDefault(spm => spm.Id == id);
        public Task<SavedPaymentMethod> GetByIdAsync(Guid id)
            => DbSet.FirstOrDefaultAsync(spm => spm.Id == id);

        public Task<List<SavedPaymentMethod>> GetAllAsync()
            => DbSet.ToListAsync();
        public Task<List<SavedPaymentMethod>> GetAllByAccountIdAsync(Guid accountId)
            => DbSet.Where(spm => spm.AccountId == accountId).ToListAsync();

        public SavedPaymentMethod GetByAccountId(Guid accountId)
            => DbSet.FirstOrDefault(spm => spm.AccountId == accountId);

        public SavedPaymentMethod GetByToken(string token)
            => DbSet.FirstOrDefault(spm => spm.Token == token);

    }
}
