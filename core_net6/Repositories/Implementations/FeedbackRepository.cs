﻿using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations;

public class FeedbackRepository(Clouds42DbContext context, ISmartTransaction smartTransaction) : GenericRepository<Feedback>(context, smartTransaction) , IFeedbackRepository
{
}
