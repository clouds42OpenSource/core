﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.WebSockets;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class WebSocketConnectionRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<WebSocketConnection>(context, smartTransaction), IWebSocketConnectionRepository
    {
        public async Task<List<string>> GetAllOpenConnections()
        {
            return await DbSet
                .AsQueryable()
                .AsNoTracking()
                .Where(x => !x.ConnectionCloseeDateTime.HasValue)
                .Select(x => x.WebSocketId)
                .ToListAsync();
        }

        public async Task<List<string>> GetOpenUserConnectionsWhichAvailableDatabase(Guid accountUserId)
        {
            return await DbSet
                .AsQueryable()
                .AsNoTracking()
                .Where(x => !x.ConnectionCloseeDateTime.HasValue && x.AccountUserId == accountUserId)
                .Select(x => x.WebSocketId)
                .ToListAsync();
        }

        public async Task<List<string>> GetOpenConnectionsWhichAvailableDatabase(Guid accountDatabaseId)
        {

            return await DbSet
                .AsQueryable()
                .AsNoTracking()
                .Join(Context.AcDbAccesses.AsQueryable(), z => z.AccountUserId, x => x.AccountUserID, (connection, access) => new {connection, access})
                .Where(x => !x.connection.ConnectionCloseeDateTime.HasValue && x.access.AccountDatabaseID == accountDatabaseId)
                .Select(x => x.connection.WebSocketId)
                .ToListAsync();
        }

    }
}
