﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class InvoiceRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Invoice>(context, smartTransaction), IInvoiceRepository
    {
        public List<Invoice> GetInvoices(Guid accountId)
        {
            var company = Context.Accounts.Include(account => account.Invoices).FirstOrDefault(c => c.Id == accountId);
            return company == null ? [] : company.Invoices.ToList();
        }

        public IEnumerable<Invoice> GetAccountInvoices(Guid accountId)
        {
            return DbSet.Where(inv => inv.AccountId == accountId);
        }

        public Invoice GetInvoiceById(Guid id)
        {
            return DbSet.Include(x => x.Account).Include(x => x.Payment).Include(x => x.InvoiceReceipt)
                .Include(x => x.InvoiceProducts).FirstOrDefault(inv => inv.Id == id);
        }

        public void AddInvoice(Invoice invoice)
        {
            Context.Entry(invoice).State = EntityState.Detached;
            invoice.Id = Guid.NewGuid();
            DbSet.Add(invoice);
            Context.SaveChanges();
        }

        public void DeleteInvoice(Guid invoiceId)
        {
            DbSet.Remove(GetInvoiceById(invoiceId));
        }

        public void DeleteInvoice(Invoice invoice)
        {
            DbSet.Remove(invoice);
        }

        /// <summary>
        /// Получить счет на оплату или создать по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="invoiceNmbPrefix">Префикс номера инвойса.</param>
        /// <param name="uniq">Номер счета на оплату.</param>
        public async Task<Invoice> GetProcessingOrCreateNewInvoiceAsync(decimal sum, long uniq, string invoiceNmbPrefix)
        {
            var invoice = await Context.Invoices
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Uniq == uniq) ?? throw new NotFoundException($"Счет по номеру {invoiceNmbPrefix}{uniq} не найден");

            if (invoice.Sum != sum)
            {
                throw new InvalidOperationException($"Сумму счета {sum} не соответствует сумме платежа");
            }

            switch (invoice.StateEnum)
            {
                case InvoiceStatus.Processing:
                    return invoice;

                case InvoiceStatus.Processed:
                {
                    var newInvoice = new Invoice
                    {
                        AccountId = invoice.AccountId,
                        Date = DateTime.Now,
                        Sum = sum,
                        State = InvoiceStatus.Processing.ToString(),
                        Id = Guid.NewGuid(),
                        Period = invoice.Period,
                        Requisite = invoice.Requisite
                    };

                    Context.Invoices.Add(newInvoice);

                    var invoiceProducts = await Context.InvoiceProducts
                        .AsQueryable()
                        .AsNoTracking()
                        .Where(x => x.InvoiceId == invoice.Id)
                        .ToListAsync();

                    Context.InvoiceProducts.AddRange(invoiceProducts.Select(x => new InvoiceProduct
                    {
                        Id = Guid.NewGuid(),
                        Count = x.Count,
                        Description = x.Description,
                        InvoiceId = newInvoice.Id,
                        ServiceName = x.ServiceName,
                        ServiceSum = x.ServiceSum,
                        ServiceTypeId = x.ServiceTypeId
                    }));

                    await Context.SaveChangesAsync();

                    return newInvoice;
                }

                default:
                    throw new InvalidOperationException($"Необработанная ситуация. КОД ошибки 201916071336. Счета на оплату с номером {invoiceNmbPrefix}{uniq} находится в статусе {invoice.StateEnum}");
               
            }
        }


        /// <summary>
        /// Получить необработанный инвойс на заданную сумму.
        /// </summary>
        /// <param name="sum">Сумма счёта.</param>
        /// <param name="inn">ИНН аккаунта.</param>
        public async Task<Invoice> GetProcessingInvoiceAsync(string inn, decimal sum)
        {
            return await Context.Invoices
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountAdditionalCompany)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountRequisites)
                .OrderByDescending(x => x.Date)
                .FirstOrDefaultAsync(x => x.State == InvoiceStatus.Processing.ToString() &&
                                          x.Sum == sum &&
                                          (x.Account.AccountRequisites.Inn == inn ||
                                           x.AccountAdditionalCompany.Inn == inn));
        }

        /// <summary>
        /// Установить номер акт счету
        /// </summary>
        /// <param name="invoiceId">Номер счета.</param>
        /// <param name="actId">Номер присваемого акта.</param>
        /// <param name="description">Коментарий к прикрепленному акту.</param>
        /// <param name="requiresSignature">Флаг нужна подпись или нет</param>
        public async Task SetActId(Guid invoiceId, Guid? actId, string description, bool requiresSignature)
        {
            var invoice = await Context.Invoices
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Id == invoiceId) ?? throw new NotFoundException($"Счет не найден по номеру {invoiceId}");
            
            invoice.ActDescription = description;
            invoice.ActID = actId;
            invoice.RequiredSignature = requiresSignature;
           

            Context.Entry(invoice).State = EntityState.Modified;

            await Context.SaveChangesAsync();
        }

        /// <summary>
        /// Получить или создать счёт по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="forceCreate">Флаг принудительного создания счёта</param>
        /// <param name="inn">ИНН аккаунта для которого будет сформирован или получен счёт</param>
        /// <param name="uniq">Номер счета.</param>
        public async Task<Invoice> GetOrCreateInvoiceAsync(bool forceCreate, string inn, decimal sum, long uniq)
        {
            var firstInvoice = await Context.Invoices
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Uniq == uniq && (x.State != InvoiceStatus.Processed.ToString() || !forceCreate));

            if (firstInvoice != null)
            {
                return firstInvoice;
            }

            var secondInvoice = await Context.Invoices
                .Include(x => x.AccountAdditionalCompany)
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountAdditionalCompany)
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountRequisites)
                .OrderByDescending(x => x.Date)
                .FirstOrDefaultAsync(x => x.Sum == sum && 
                                          (x.State == InvoiceStatus.Processed.ToString() || !forceCreate) && 
                                          (x.Account.AccountRequisites.Inn == inn || x.AccountAdditionalCompany.Inn == inn));

            if (secondInvoice != null)
            {
                return secondInvoice;
            }

            if (!forceCreate)
            {
                throw new NotFoundException($"Не удалось получить даные по счету '{uniq}' так как он не существует");
            }

            var accountRequisites = await Context.AccountRequisites
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Inn == inn);

            var accountAdditionalCompany = await Context.AccountAdditionalCompanies.AsQueryable().AsNoTracking()
                .FirstOrDefaultAsync(x => x.Inn == inn);

            var entity = new Invoice
            {
                Id = Guid.NewGuid(),
                AccountId = accountRequisites?.AccountId ?? accountAdditionalCompany.AccountId,
                Requisite = inn,
                State = InvoiceStatus.Processing.ToString(),
                Date = DateTime.Now,
                Sum = sum,
                AccountAdditionalCompanyId = accountAdditionalCompany?.Id,
            };

            Context.Invoices.Add(entity);
            await Context.SaveChangesAsync();

            return entity;

        }

        public Task<List<Invoice>> GetAllProcessing()
            => DbSet.Where(i => i.State == InvoiceStatus.Processing.ToString()).ToListAsync();
    }
}
