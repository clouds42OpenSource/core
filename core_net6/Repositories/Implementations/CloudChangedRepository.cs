﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CloudChangesRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudChanges>(context, smartTransaction), ICloudChangesRepository
    {
        public void AddLogging(CloudChanges model)
        {
            model.Date = DateTime.Now;
            model.Id = Guid.NewGuid();
            Context.Entry(model).State = EntityState.Added;

        }
    }
}
