﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class AccountUserSessionRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountUserSession>(context, smartTransaction), IAccountUserSessionRepository
    {
        public AccountUserSession GetRecentSessionByUserId(Guid accounUserId)
        {

            return Context.AccountUserSessions
                .OrderByDescending(usrS => usrS.TokenCreationTime)
                .FirstOrDefault(usrS => usrS.AccountUserId == accounUserId);
        }

        public AccountUserSession GetAccountUserSessionByToken(Guid token)
        {
            return Context.AccountUserSessions
                .OrderByDescending(usrS => usrS.TokenCreationTime)
                .FirstOrDefault(usrS => usrS.Token == token);
        }

        public AccountUserSession GetAccountUserSession(Guid sessionId)
        {
            return Context.AccountUserSessions
                .FirstOrDefault(usrS => usrS.Id == sessionId);
        }

        public IEnumerable<AccountUserSession> GetAccountUserSessionByUserId(Guid userId)
        {
            return Context.AccountUserSessions
                .OrderByDescending(usrS => usrS.TokenCreationTime)
                .Where(usrS => usrS.AccountUserId == userId).ToList();
        }

        public void InsertOrUpdateAccountUserSession(AccountUserSession accountUserSession)
        {
            if (accountUserSession.Id == Guid.Empty)
            {
                accountUserSession.Id = Guid.NewGuid();
                Context.Entry(accountUserSession).State = EntityState.Added;
                Context.SaveChanges();

                return;
            }

            Context.Entry(accountUserSession).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public bool DeleteAccounUserSession(Guid sessionId)
        {

            var session = Context.AccountUserSessions.FirstOrDefault(usrS => usrS.Id == sessionId);
            if (session == null)
                return false;
            Context.AccountUserSessions.Remove(session);
            Context.SaveChanges();
            return true;
        }

        public async Task<AccountUser> GetAccountUserForTokenAsync(Guid token) 
            => await Context.AccountUsers
                .Where(au => au.Activated)
                .GroupJoin(
                    Context.AccountUserSessions
                        .GroupBy(aus => aus.AccountUserId)
                        .Select(g => new
                        {
                            AccountUserId = g.Key, LastTokenCreationTime = g.Max(aus => aus.TokenCreationTime)
                        }),
                    au => au.Id,
                    ausLast => ausLast.AccountUserId,
                    (au, ausLast) => new { au, ausLast = ausLast.FirstOrDefault() }
                )
                .SelectMany(
                    x => Context.AccountUserSessions.Where(aus =>
                        aus.AccountUserId == x.au.Id &&
                        aus.TokenCreationTime == x.ausLast.LastTokenCreationTime),
                    (x, aus) => new { x.au, aus }
                )
                .Where(result => result.aus.Token == token)
                .Select(result => result.au)
                .FirstOrDefaultAsync();

        /// <summary>
        /// Выбирает для заданного токена параметры пользователя или службы.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="expirationDays">Дни перед тем, как сессия будет считаться устаревшей</param>
        /// <param name="cloud42ServiceId">Идентификатор группы для облачной службы</param>
        /// <returns>AccountUserSessionQueryData</returns>
        /// <seealso cref="AccountUserSessionQueryDataDto"/>
        public AccountUserSessionQueryDataDto GetValidAccountUserSessionDataForToken(Guid token, int expirationDays, AccountUserGroup cloud42ServiceId = AccountUserGroup.Cloud42Service)
        {
            var now = DateTime.Now;
            var query =
              (from au in Context.AccountUsers
               join accountConfiguration in Context.AccountConfigurations on au.AccountId equals accountConfiguration.AccountId
               join aus in Context.AccountUserSessions on au.Id equals aus.AccountUserId
               where aus.Token == token && aus.TokenCreationTime > now.AddDays(-expirationDays)
               select new AccountUserSessionQueryDataDto
               {
                   IsCloudService = false,
                   IsUser = true,
                   SessionId = aus.Id,
                   Name = au.Login,
                   UserSource = accountConfiguration.Type,
                   Id = au.Id,
                   RequestAccountId = au.AccountId,
                   Token = aus.Token,
                   TokenCreationTime = aus.TokenCreationTime
               })
            .Union(
            from accountUser in Context.AccountUsers
            join accountConfiguration in Context.AccountConfigurations on accountUser.AccountId equals accountConfiguration.AccountId
            where accountUser.AuthToken == token
            select new AccountUserSessionQueryDataDto
            {
                IsCloudService = true,
                IsUser = false,
                SessionId = null,
                Name = accountUser.Login,
                UserSource = accountConfiguration.Type,
                Id = accountUser.Id,
                RequestAccountId = accountUser.AccountId,
                Token = accountUser.AuthToken ?? Guid.Empty,
                TokenCreationTime = null
            })
            .Union(Context.CloudServices
                .Include(x => x.AccountUser)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .Where(x => x.Id == token)
                .Select(x => new AccountUserSessionQueryDataDto
                {
                    IsCloudService = true,
                    IsUser = false,
                    SessionId = null,
                    Name = x.AccountUser != null ? x.AccountUser.Login : null,
                    UserSource = x.AccountUser != null ? x.AccountUser.Account.AccountConfiguration.Type : null,
                    Id = x.AccountUserId ?? Guid.Empty,
                    RequestAccountId = x.AccountUser != null ? x.AccountUser.AccountId : Guid.Empty,
                    Token = x.Id,
                    TokenCreationTime = null
                }))
            .OrderByDescending(x => x.TokenCreationTime)
            .Take(1);

            var result = query.FirstOrDefault();

            if (result == null)
            {
                return result;
            }

            if (result.IsCloudService)
            {
                result.Groups = [cloud42ServiceId];

                return result;
            }
            
            var roles = Context.AccountUserRoles
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountUserId == result.Id)
                .Select(x => x.AccountUserGroup)
                .ToList();

            result.Groups = roles;

            return result;
        }
        

        /// <summary>
        /// Выбирает последний активный токен сессии для пользователя.
        /// </summary>
        /// <param name="accountUserId">Id польтзователя, для которого вытащить последний токен сессии</param>
        /// <param name="expirationDays">Дни перед тем, как сессия будет считаться устаревшей</param>
        /// <returns>Последний активный токен сессии.</returns>
        [Obsolete("Удалить после внедрения нового механизма аутентификации.")]
        public Guid GetLastValidTokenForUser(Guid accountUserId, int expirationDays)
        {
            var dateToCompare = DateTime.Now.AddDays(-expirationDays);
            return (
                from au in Context.AccountUsers
                join aus in Context.AccountUserSessions on au.Id equals aus.AccountUserId
                where  au.Id == accountUserId && aus.TokenCreationTime > dateToCompare
                orderby aus.TokenCreationTime descending
                select aus.Token
              ).FirstOrDefault();
        }

        /// <summary>
        /// Обновляет поле TokenCreationTime для указанной сессии, устанавливая текущее дату-время.
        /// </summary>
        /// <param name="sessionId">Id сессии, для которой требуется обновить TokenCreationTime</param>
        public void RefreshTokenCreationTime(Guid sessionId)
        {
            var session = Context.AccountUserSessions
                .FirstOrDefault(s => s.Id == sessionId);

            if (session != null)
            {
                session.TokenCreationTime = DateTime.Now;
                Context.SaveChanges();
            }
        }

        /// <summary>
        /// Получить данные сессии по токену.
        /// </summary>
        /// <param name="token">Токен.</param>
        /// <param name="expDays">Количество дней жизни токена.</param>
        /// <returns>AccountUserSessionInfoQueryData</returns>
        /// <seealso cref="AccountUserSessionInfoQueryDataDto"/>
        public async Task<AccountUserSessionInfoQueryDataDto> GetAccountUserSessionInfoAsync(Guid token, int expDays)
        {
            var now = DateTime.Now;
            var expirationDate = now.AddDays(-expDays);

            var sessionInfo = await (from aus in Context.AccountUserSessions
                    join au in Context.AccountUsers on aus.AccountUserId equals au.Id
                    where aus.Token == token && aus.TokenCreationTime > expirationDate
                    orderby aus.TokenCreationTime descending
                    select new AccountUserSessionInfoQueryDataDto
                    {
                        SessionId = aus.Id,
                        AccountId = au.AccountId,
                        IsService = false,
                        TokenCreationTime = aus.TokenCreationTime
                    })
                .FirstOrDefaultAsync();

            return sessionInfo;
        }
        
        /// <summary>
        /// Получить идентификатор аккаунта по токену сессии.
        /// </summary>
        /// <param name="token">Токен сессии.</param>
        public async Task<Guid> GetAccountIdBySessionTokenAsync(Guid token)
        {
            var accountId = await (from s in Context.AccountUserSessions
                join au in Context.AccountUsers on s.AccountUserId equals au.Id
                where s.Token == token
                select au.AccountId).FirstOrDefaultAsync();

            return accountId;
        }
    }
}
