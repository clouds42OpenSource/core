﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    ///     Репозиторий бекапов баз
    /// </summary>
    public class AccountDatabaseBackupsRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountDatabaseBackup>(context, smartTransaction), IAccountDatabaseBackupsRepository
    {
        /// <summary>
        ///     Получить количество бекапов для определенной базы
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public int GetCountOfDatabaseBackups(Guid accountDatabaseId)
        {
            return Context.AccountDatabaseBackups
                .Count(w => w.AccountDatabaseId == accountDatabaseId);
        }

        /// <summary>
        ///     Получить список баз отсортированных по дате создания
        /// (Первые в списке это новые бэкапы)
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор базы</param>
        public IOrderedQueryable<AccountDatabaseBackup> GetBackupsSortedByDate(Guid accountDatabaseId)
        {
            return Context.AccountDatabaseBackups
                .Where(w => w.AccountDatabaseId == accountDatabaseId)
                .OrderByDescending(w => w.CreateDateTime);
        }

        /// <summary>
        /// Получить путь до бекапа информационной базы
        /// </summary>
        /// <param name="backupId">Id бекапа информационной базы</param>
        /// <returns>Путь до бекапа информационной базы</returns>
        public string GetAccountDatabaseBackupPath(Guid backupId) =>
            Context.AccountDatabaseBackups.FirstOrDefault(b => b.Id == backupId)?.BackupPath;
    }
}
