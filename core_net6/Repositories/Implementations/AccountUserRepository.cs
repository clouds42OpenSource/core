﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Repositories.Implementations
{
    public class AccountUserRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountUser>(context, smartTransaction), IAccountUserRepository
    {
        private readonly IHashProvider _hashProvider = new Md5HashProvider();

        public IQueryable<AccountUser> GetNotDeletedAccountUsersQuery()
        {
            return
                Context.AccountUsers.Where(
                    u => u.CorpUserSyncStatus != "Deleted" && u.CorpUserSyncStatus != "SyncDeleted");
        }

        /// <inheritdoc/>
        public IQueryable<AccountUser> GetValidUsersQuery()
        {
            return Context.AccountUsers
                .Include(usr => usr.AccountUserRoles)
                .Where(usr => (!usr.Removed.HasValue || !usr.Removed.Value) && !usr.CorpUserSyncStatus.Contains("Deleted"));
        }

        public List<AccountUser> GetAccountUsers(Guid accountId, bool includeDeleted = false)
        {
            var usrs = Context.AccountUsers.Where(usr => usr.AccountId == accountId).ToList();

            if (includeDeleted)
                return usrs;

            return usrs.Where(ValidUser).ToList();
        }

        public async Task<List<AccountUser>> GetAccountUsersAsync(Guid accountId)
        {
            return await Context.AccountUsers
                .AsQueryable()
                .Where(x => x.AccountId == accountId && x.Removed != true && !x.CorpUserSyncStatus.Contains("Deleted"))
                .ToListAsync();
        }

        /// <summary>
        ///     Возвращает пользователя по токену
        /// </summary>
        /// <param name="resetToken">Зашифрованное поле ResetCode</param>
        /// <returns></returns>
        public AccountUser GetAccountUserByResetToken(string resetToken)
        {
            var allActiveUsers = Context.AccountUsers.Where(usr =>
                usr.CorpUserSyncStatus != "Deleted" && usr.CorpUserSyncStatus != "SyncDeleted" &&
                usr.ResetCode.HasValue).AsEnumerable();

            return allActiveUsers.FirstOrDefault(usr =>
                _hashProvider.VerifyHash(usr.ResetCode.ToString(), resetToken));
        }

        private static bool ValidUser(AccountUser usr)
        {
            return (!usr.Removed.HasValue || !usr.Removed.Value) && !usr.CorpUserSyncStatus.Contains("Deleted");
        }

        public AccountUser GetManager(Guid managerId)
        {
            return DbSet.FirstOrDefault(man => man.Id == managerId);
        }

        public AccountUser GetByLogin(string login)
        {
            var usr = Context.AccountUsers.Include(v => v.Account).FirstOrDefault(u =>
                u.Login == login && u.CorpUserSyncStatus != "Deleted" && u.CorpUserSyncStatus != "SyncDeleted");
            ReloadContext(usr);
            return usr;
        }

        public AccountUser IsUserExist(string login)
        {
            var usr = Context.AccountUsers.Include(v => v.Account).FirstOrDefault(u => u.Login == login);
            ReloadContext(usr);
            return usr;
        }

        public AccountUser GetByEmail(string email)
        {
            var usr = Context.AccountUsers.Include(v => v.Account).FirstOrDefault(u =>
                u.Email == email && u.CorpUserSyncStatus != "Deleted" && u.CorpUserSyncStatus != "SyncDeleted" && u.EmailStatus == "Checked");
            ReloadContext(usr);
            return usr;
        }

        public AccountUser GetByPhoneNumber(string phone)
        {
            var usr = Context.AccountUsers.FirstOrDefault(u =>
                u.PhoneNumber == phone && u.CorpUserSyncStatus != "Deleted" && u.CorpUserSyncStatus != "SyncDeleted" && u.IsPhoneVerified.HasValue && !u.IsPhoneVerified.Value);
            ReloadContext(usr);
            return usr;
        }

        /// <summary>
        /// Установить пароль пользователя в базе данных
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="notEncryptNewPassword">Пароль в чистом виде</param>
        /// <param name="configuration">Конфиг приложения</param>
        public void SetUserPassword(AccountUser user, string notEncryptNewPassword, IConfiguration configuration)
        {
            user.Password = new DesEncryptionProvider(configuration).Encrypt(notEncryptNewPassword);
            user.PasswordHash = new AesEncryptionProvider(configuration).Encrypt(notEncryptNewPassword);
            user.DateTimeOfLastChangePasswordOrLogin = DateTime.Now;
            user.AuthGuid = Guid.NewGuid();
        }

        public void DeleteUser(AccountUser user)
        {
            Context.AccountUsers.Remove(user);
            Context.SaveChanges();
        }

        /// <summary>
        /// Отписать пользователя от рассылок
        /// <param name="accountUserId">Id пользователя</param>
        /// </summary>
        public async Task<AccountUser> Unsubscribe(Guid accountUserId)
        {
            var accountUser = await Context.AccountUsers.FirstOrDefaultAsync(x => x.Id == accountUserId) ?? throw new NotFoundException($"По номеру {accountUserId} не найден пользователь.");
            
            accountUser.Unsubscribed = true;
            Context.Entry(accountUser).State = EntityState.Modified;

            await Context.SaveChangesAsync();

            return accountUser;
        }

        public AccountUser GetById(Guid id)
        {
            return Context.AccountUsers
                .Include(x => x.Account)
                .FirstOrDefault(x => x.Id == id);
        }

        private void ReloadContext(object o)
        {
            if (o != null)
            {
                Context.Entry(o).Reload();
            }
        }

        public AccountUser GetAccountUser(Guid accountUserId)
        {
            return DbSet.FirstOrDefault(au => au.Id == accountUserId);
        }

        public AccountUser GetAccountUserByCorpId(Guid corpUserId)
        {
            return DbSet.FirstOrDefault(au => au.CorpUserID == corpUserId);
        }

        public AccountUser GetAccountUserByEmail(string email)
        {
            return DbSet.FirstOrDefault(au => au.Email == email);
        }

        public AccountUser GetAccountUserByVerifiedEmail(string email)
        {
            return DbSet.FirstOrDefault(au => au.Email == email && au.EmailStatus == "Checked");
        }
        public AccountUser GetAccountUserByNoVerifiedEmail(string email)
        {
            return DbSet.OrderByDescending(au => au.LastLoggedIn)
                .FirstOrDefault(au => au.Email == email && au.EmailStatus != "Checked");
        }

        public AccountUser GetAccountUserByLogin(string login)
        {
            return DbSet.FirstOrDefault(au => au.Login == login);
        }

        public AccountUser GetAccountUserByeVerifiedPhoneNumber(string phoneNomber)
        {
            return DbSet.FirstOrDefault(au => au.PhoneNumber == phoneNomber && au.IsPhoneVerified.HasValue && au.IsPhoneVerified.Value);
        }

        public AccountUser GetAccountUserByeNoVerifiedPhoneNumber(string phoneNomber)
        {
            return DbSet.OrderByDescending(au=> au.LastLoggedIn).
                FirstOrDefault(au => au.PhoneNumber == phoneNomber && (!au.IsPhoneVerified.HasValue || !au.IsPhoneVerified.Value));
        }

        public List<AccountUser> GetAllAccountUsers(Guid accountId)
        {
            return DbSet.Where(au => au.AccountId == accountId).ToList();
        }

        public void InsertOrUpdateAccountUser(AccountUser accountUser)
        {
            Context.Entry(accountUser).State = accountUser.Id == Guid.Empty
                ? EntityState.Added
                : EntityState.Modified;
            Context.SaveChanges();
        }

        public void InsertAccountUser(AccountUser accountUser)
        {
            try
            {
                Context.Entry(accountUser).State = EntityState.Added;
                Context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                var outputLines = new System.Text.StringBuilder();
                foreach (var eve in e.Entries)
                {
                    outputLines.AppendLine(
                        $@"{DateTime.Now}: Entity of type ""{eve.Entity.GetType().Name}"" in state ""{eve.State}"" has the following validation:");
                }

                throw new ValidateException(outputLines.ToString());
            }
        }

        /// <summary>
        /// Получить данные имперсонации пользователя по Id
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Данные имперсонации пользователя</returns>
        public async Task<AccountUserImpersonateQueryDataModelDto> GetAccountUserImpersonateDataByAccountUserIdAsync(
            Guid accountUserId) => 
            await Context.AccountUsers
            .Where(au => au.Id == accountUserId)
            .Select(au => new AccountUserImpersonateQueryDataModelDto
            {
                AccountUserId = au.Id,
                AccountId = au.AccountId,
                Login = au.Login,
                Email = au.Email,
                PhoneNumber = au.PhoneNumber,
                FirstName = au.FirstName,
                LastName = au.LastName,
                MiddleName = au.MiddleName,
                CorpUserID = au.CorpUserID,
                CorpUserSyncStatus = au.CorpUserSyncStatus,
                AccountUserRemoved = au.Removed,
                AccountUserCreationDate = au.CreationDate,
                AccountCaption = au.Account.AccountCaption,
                ReferralAccountId = au.Account.ReferralAccountID,
                AccountRegistrationDate = au.Account.RegistrationDate,
                AccountIndexNumber = au.Account.IndexNumber,
                AccountRemoved = au.Account.Removed,
                AccountDescription = au.Account.Description,
                AccountInn = au.Account.AccountRequisites.Inn,
                AccountLocaleId = au.Account.AccountConfiguration.LocaleId,
                AccountCurrency = au.Account.AccountConfiguration.Locale.Currency,
                AccountBalance = au.Account.BillingAccount.Balance,
                SegmentId = au.Account.AccountConfiguration.Segment.ID,
                SegmentStable82Version = au.Account.AccountConfiguration.Segment.Stable82Version,
                SegmentAlpha83Version = au.Account.AccountConfiguration.Segment.Alpha83Version,
                SegmentStable83Version = au.Account.AccountConfiguration.Segment.Stable83Version,
            })
            .SingleOrDefaultAsync();
    }
}
