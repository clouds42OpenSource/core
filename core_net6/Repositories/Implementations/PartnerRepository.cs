using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations {
    public class PartnerRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Partner>(context, smartTransaction), IPartnersRepository
    {
        public IEnumerable<Partner> GetReferrerList(Guid referrerId)
        {
            return Context.Partners.Where(x => x.PartnerId == referrerId);
        }

        public IEnumerable<Partner> GetAllReferrerHierarchy()
        {
            throw new NotImplementedException();
        }

        public bool IsPartner(Guid companyId)
        {
            return Context.Partners.FirstOrDefault(x => x.PartnerId == companyId) != null;
        }
    }
}