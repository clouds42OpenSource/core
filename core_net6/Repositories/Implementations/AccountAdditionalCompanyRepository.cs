﻿using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class AccountAdditionalCompanyRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountAdditionalCompany>(context, smartTransaction), IAccountAdditionalCompanyRepository;
}
