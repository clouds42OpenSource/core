﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Расширение для репозитория кошельков агентов
    /// </summary>
    public class AgentWalletRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AgentWallet>(context, smartTransaction), IAgentWalletRepository
    {
        /// <summary>
        /// Получить кошелек агента
        /// </summary>
        /// <param name="accountAgentId">ID агента</param>
        /// <returns>кошелек агента</returns>
        public AgentWallet GetAgentWalletOrCreateNew(Guid accountAgentId)
        {
            var agentWallet = DbSet.FirstOrDefault(w => w.AccountOwnerId == accountAgentId);
            if (agentWallet != null)
            {
                return agentWallet;
            }

            agentWallet = new AgentWallet
            {
                AccountOwnerId = accountAgentId,
                AvailableSum = 0
            };

            DbSet.Add(agentWallet);
            Context.SaveChanges();

            return agentWallet;
        }
    }
}
