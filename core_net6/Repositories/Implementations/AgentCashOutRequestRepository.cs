﻿using System.Linq;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для агентских заявок на вывод средств
    /// </summary>
    public class AgentCashOutRequestRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AgentCashOutRequest>(context, smartTransaction), IAgentCashOutRequestRepository
    {
        /// <summary>
        /// Вставить новую запись заявки на вывод средств
        /// </summary>
        /// <param name="cashOutRequest">Заявка на вывод средств</param>
        public void InsertAgentCashOutRequest(AgentCashOutRequest cashOutRequest)
        {
            var existsRequest = Context.AgentCashOutRequests
                .AsQueryable()
                .AsNoTracking()
                .OrderByDescending(x => x.Number)
                .FirstOrDefault();

            var existsRequisites = Context.AgentRequisites
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountOwner)
                .FirstOrDefault(x => x.Id == cashOutRequest.AgentRequisitesId);

            Context.AgentCashOutRequests.Add(new AgentCashOutRequest
            {
                Id = cashOutRequest.Id,
                RequestNumber = $"{existsRequisites!.AccountOwner.IndexNumber}-{existsRequisites.Number}-{(existsRequest?.Number ?? 0) + 1}",
                Number = (existsRequest?.Number ?? 0) + 1,
                CreationDateTime = cashOutRequest.CreationDateTime,
                RequestStatus = cashOutRequest.RequestStatus,
                AgentRequisitesId = cashOutRequest.AgentRequisitesId,
                RequestedSum = cashOutRequest.RequestedSum
            });

            Context.SaveChanges();
        }
    }
}
