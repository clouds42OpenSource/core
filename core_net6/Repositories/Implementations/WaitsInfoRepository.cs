﻿using Clouds42.Domain.DataModels.WaitInfos;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations;

public class WaitsInfoRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
    : GenericRepository<WaitInfo>(context, smartTransaction), IWaitsInfoRepository;
