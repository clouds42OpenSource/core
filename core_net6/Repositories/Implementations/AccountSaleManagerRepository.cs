﻿using System;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для работы с Менеджерами Аккаунтов
    /// </summary>
    public class AccountSaleManagerRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountSaleManager>(context, smartTransaction), IAccountSaleManagerRepository
    {
        /// <summary>
        /// Получить Менеджера Аккаунта по ID аккаунта
        /// </summary>
        /// <param name="accountId">ИД аккаунта</param>
        /// <returns>Менеджер аккаунта</returns>
        public AccountSaleManager GetForAccount(Guid accountId) =>
            DbSet.FirstOrDefault(x => x.AccountId == accountId);
    }
}