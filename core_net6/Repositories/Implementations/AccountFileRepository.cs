﻿using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class AccountFileRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountFile>(context, smartTransaction), IAccountFileRepository;
}
