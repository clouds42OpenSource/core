﻿using System;
using System.Linq;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для платежей агента
    /// </summary>
    public class AgentPaymentRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AgentPayment>(context, smartTransaction), IAgentPaymentRepository
    {
        private static readonly ILogger42 Logger = Logger42.GetLogger();

        /// <summary>
        /// Вставить запись платежа агента и обновить сумму его кошелька
        /// </summary>
        /// <param name="accountId">ID аккаунта агента</param>
        /// <param name="clientPaymentId">ID платежа клиента</param>
        /// <param name="amount">Сумма платежа</param>
        /// <param name="paymentType">Тип платежа - начисления/снятие.</param>
        /// <param name="date">Дата платежа</param>
        /// <param name="comment">Комментарий к платежу.</param>
        /// <param name="agentPaymentSourceType">Тип источника агентского платежа</param>
        /// <param name="clientAccountId">Id аккаунта клиента</param>
        /// <param name="clientPaymentSum">Сумма платежа клиента</param>
        public void InsertAgentPaymentAndUpdateAgentWallet(Guid accountId, decimal amount, Guid? clientPaymentId = null,
            PaymentType paymentType = PaymentType.Inflow, DateTime? date = null, string comment = null,
            AgentPaymentSourceTypeEnum agentPaymentSourceType = AgentPaymentSourceTypeEnum.AgentReward,
            Guid? clientAccountId = null, decimal? clientPaymentSum = null)
        {
            try
            {
                Guid? agentPaymentRelationId = null;
                DateTime? paymentEntryDateTime = null;
                var nowDate = DateTime.Now;
                if (clientAccountId != null)
                {
                    agentPaymentRelationId = Guid.NewGuid();
                    paymentEntryDateTime = nowDate;

                    var agentPaymentRelation = new AgentPaymentRelation
                    {
                        Id = agentPaymentRelationId.Value,
                        AccountId = clientAccountId.Value,
                        Sum = clientPaymentSum ?? 0
                    };

                    Context.AgentPaymentRelations.Add(agentPaymentRelation);
                    Context.SaveChanges();
                }

                var totalSum = Context.AgentPayments
                    .Where(ap => ap.AccountOwnerId == accountId && ap.PaymentDateTime <= nowDate)
                    .Sum(ap => ap.PaymentType == PaymentType.Inflow ? ap.Sum : -ap.Sum);

                var agentPayment = new AgentPayment
                {
                    Id = Guid.NewGuid(),
                    AccountOwnerId = accountId,
                    ClientPaymentId = clientPaymentId,
                    PaymentType = paymentType,
                    PaymentDateTime = date ?? nowDate,
                    Sum = Math.Round(amount, 2),
                    Comment = comment,
                    AgentPaymentSourceType = agentPaymentSourceType,
                    AgentPaymentRelationId = agentPaymentRelationId,
                    PaymentEntryDateTime = paymentEntryDateTime
                };

                Context.AgentPayments.Add(agentPayment);
                Context.SaveChanges();

                var incomeSum = paymentType == PaymentType.Inflow ? Math.Round(amount, 2) : -Math.Round(amount, 2);

                var agentWallet = Context.AgentWallets
                    .FirstOrDefault(aw => aw.AccountOwnerId == accountId);

                if (agentWallet != null)
                {
                    agentWallet.AvailableSum = totalSum + incomeSum;
                    Context.AgentWallets.Update(agentWallet);
                    Context.SaveChanges();
                }

                Logger.Trace($"Сумма агентского кошелька {accountId} увеличена на {amount} на основании клиентского платежа {clientPaymentId}.");
            }
            catch (Exception ex)
            {
                Logger.Trace($"Не удалось увеличить сумму агентского кошелька {accountId} на {amount} на основании клиентского платежа {clientPaymentId}. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
