﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Clouds42.Common.Constants;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using LinqExtensionsNetFramework;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public static class AccountDatabaseSpecification
    {
        public static Spec<AccountDatabase> ByConfigurationCode(string code)
        {
            return new Spec<AccountDatabase>(x =>
                string.IsNullOrEmpty(code) || x.DbTemplate.Delimiters.Any(z => z.ConfigurationId == code));
        }
    }

    public class AccountDatabaseRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountDatabase>(context, smartTransaction), IAccountDatabaseRepository
    {

        static readonly List<string> States =
        [
            DatabaseState.DelitingToTomb.ToString(),
            DatabaseState.DetachingToTomb.ToString(),
            DatabaseState.DeletedFromCloud.ToString(),
            DatabaseState.DeletedToTomb.ToString(),
            DatabaseState.DetachedToTomb.ToString(),
        ];

        public override IEnumerable<AccountDatabase> GetAll()
        {
            return DbSet;
        }

        public override IQueryable<AccountDatabase> All()
        {
            return DbSet;
        }

        public override IEnumerable<AccountDatabase> Get(Expression<Func<AccountDatabase, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public override bool Any(Expression<Func<AccountDatabase, bool>> predicate)
        {
            return DbSet.Any(predicate);
        }

        public override int Count()
        {
            return DbSet.Count();
        }

        public AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
        {
            return DbSet.FirstOrDefault(ad => ad.Id == accountDatabaseId);
        }

        public void InsertOrUpdateAccountDatabaseValue(AccountDatabase accountDatabaseValue)
        {
            Context.Entry(accountDatabaseValue).State = accountDatabaseValue.Id == Guid.Empty
                ? EntityState.Added
                : EntityState.Modified;
            Context.SaveChanges();
        }
        

        /// <inheritdoc />
        /// <summary>
        ///     Получить список опубликованных баз в сегменте
        /// по платформе и дистрибутиву
        /// </summary>
        /// <param name="segmentId">Идентификатор сегмента</param>
        /// <param name="platformType">Версия платформы (8.2 или 8.3)</param>
        /// <param name="distributionType">Тип дистрибутива</param>
        public List<AccountDatabase> GetPublishedDbs(Guid segmentId,
            PlatformType platformType,
            DistributionType distributionType)
        {
            var platform = platformType.GetDisplayName();

            return Context.Accounts
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountConfiguration)
                .Include(x => x.AccountDatabases)
                .Where(x => x.AccountConfiguration.SegmentId == segmentId)
                .SelectMany(x => x.AccountDatabases)
                .Where(x => x.PublishState == PublishState.Published.ToString() &&
                            x.State == DatabaseState.Ready.ToString() &&
                            x.ApplicationName == platform &&
                            x.DistributionType == distributionType.ToString() &&
                            x.AccountDatabaseOnDelimiter == null)
                .ToList();
        }
        
        public bool AnyPublishedDbs(Guid segmentId,
            PlatformType platformType,
            DistributionType distributionType)
        {
            var platform = platformType.GetDisplayName();

            return Context.Accounts
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountConfiguration)
                .Include(x => x.AccountDatabases)
                .Where(x => x.AccountConfiguration.SegmentId == segmentId)
                .SelectMany(x => x.AccountDatabases)
                .Any(x => x.PublishState == PublishState.Published.ToString() &&
                          x.State == DatabaseState.Ready.ToString() &&
                          x.ApplicationName == platform &&
                          x.DistributionType == distributionType.ToString() &&
                          x.AccountDatabaseOnDelimiter == null);
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="fileStorage">Файловое хранилище куда создается база.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="account">Аккаунт в который создается база.</param>
        /// <param name="isFile">Файловая база.</param>
        public AccountDatabase CreateNewRecord(
            DbTemplate template,
            Account account,
            PlatformType platformType,
            string caption,
            CloudServicesFileStorageServer fileStorage,
            bool isFile)
        {
            var dbNumber = GetNextDbNumber(account);
            var v82Name = $"{DatabaseConst.AllLocalesFileDatabasePrefix}{account.IndexNumber}_{dbNumber}";
            return CreateAccountDatabaseRecord(template, account, platformType, caption, fileStorage, isFile, dbNumber,
                v82Name);
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="template">Шаблон.</param>
        /// <param name="platformType">Тип платформы.</param>
        /// <param name="caption">Название.</param>
        /// <param name="account">Аккаунт в который создается база.</param>
        public AccountDatabase CreateNewRecord(
            DbTemplate template,
            Account account,
            PlatformType platformType,
            string caption)
        {
            var dbNumber = GetNextDbNumber(account);
            var v82Name = $"42_{account.IndexNumber}_{dbNumber}";
            return CreateAccountDatabaseRecord(template, account, platformType, caption, null, false, dbNumber,
                v82Name);
        }

        /// <summary>
        ///     Получить список всех баз на разделителях для пользователя
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        public async Task<List<DelimitersDatabaseQueryDataDto>> GetDelimitersDbsForAccUserAsync(Guid accountUserId)
        {
            return await GetDelimitersDbs(accountUserId, null);
        }

        /// <summary>
        /// Получить спискок свойств доступных баз данных для пользователя аккаунта.
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя аккаунта</param>
        /// <param name="serviceAccountId"></param>
        public async Task<List<AccessDatabaseListQueryDataDto>> GetAccountDatabasesForAccountUserAsync(Guid accountUserId, Guid serviceAccountId)
        {
            var serviceAccountIdForDemoDelimiterDatabases = Guid.Parse(await Context.CloudConfigurations
                .AsQueryable()
                .AsNoTracking()
                .Where(cc => cc.Key == "DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases")
                .Select(cc => cc.Value)
                .FirstOrDefaultAsync());

            var platformVersionForDemoDelimiterDb = await Context.Accounts
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .Where(acc => acc.Id == serviceAccountIdForDemoDelimiterDatabases)
                .Select(acc => acc.AccountConfiguration.Segment.Stable83Version)
                .FirstOrDefaultAsync();

            var filteredAccountDatabases = await Context.AcDbAccesses
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountUserID == accountUserId)
                .Select(x => new
                {
                    AccountDatabasesId = x.AccountDatabaseID,
                    AcDbAccessId = x.ID,
                    AcDbAccessIsLock = x.IsLock,
                    AcDbAccessLaunchType = x.LaunchType
                })
                .ToListAsync();

            var databaseIds = filteredAccountDatabases.Select(x => x.AccountDatabasesId).ToList();
            var acDbAccessIds = filteredAccountDatabases.Select(x => x.AcDbAccessId).ToList();

            var acDbAccRolesJhOes = await Context.AcDbAccRolesJhOes
                .AsQueryable()
                .AsNoTracking()
                .Where(x => acDbAccessIds.Contains(x.AcDbAccessesID))
                .ToListAsync();

            var data = await Context.AccountDatabases
                .AsQueryable()
                .AsNoTracking()
                .AsSplitQuery()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesContentServer)
                .Include(x => x.AccountDatabaseOnDelimiter)
                .ThenInclude(x => x.SourceAccountDatabase)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesFileStorageServer)
                .Include(x => x.AccountDatabaseOnDelimiter)
                .ThenInclude(x => x.SourceAccountDatabase)
                .ThenInclude(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.Segment)
                .ThenInclude(x => x.CloudServicesEnterpriseServer83)
                .Include(x => x.AccountDatabaseOnDelimiter)
                .ThenInclude(x => x.DbTemplateDelimiter)
                .Include(x => x.AccountDatabaseOnDelimiter)
                .ThenInclude(x => x.SourceAccountDatabase)
                .ThenInclude(x => x.DelimiterSourceAccountDatabases)
                .Select(x => new AccessDatabaseListQueryDataDto
                    {
                        Id = x.Id,
                        AccountUserId = accountUserId,
                        CreationDate = x.CreationDate,
                        LastActivityDate = x.LastActivityDate,
                        Caption = x.Caption,
                        ApplicationName = x.ApplicationName,
                        PublishState = x.PublishState,
                        DbName = x.V82Name,
                        DbNumber = x.DbNumber,
                        State = x.State,
                        SqlName = x.SqlName,
                        V82Server = x.V82Server,
                        Platform = x.ApplicationName,
                        LockedState = x.LockedState,
                        IsFile = x.IsFile ?? false,
                        SizeInMb = x.SizeInMB,
                        TemplateId = x.TemplateId,
                        ServiceName = x.ServiceName,
                        LaunchParameters = x.LaunchParameters,
                        DistributionType = x.DistributionType,
                        AccountId = x.AccountId,
                        AccountCaption = x.Account.AccountCaption,
                        IndexNumber = x.Account.IndexNumber,
                        SiteName = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.DelimiterSourceAccountDatabases.FirstOrDefault(z => z.DbTemplateDelimiterCode == x.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode) != null ?
                            x.AccountDatabaseOnDelimiter.SourceAccountDatabase.DelimiterSourceAccountDatabases.FirstOrDefault(z => z.DbTemplateDelimiterCode == x.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode).DatabaseOnDelimitersPublicationAddress : null,
                        DemoSiteName = x.AccountDatabaseOnDelimiter.DbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress,
                        DelimiterPlatformVersion = x.AccountDatabaseOnDelimiter.IsDemo == true
                            ? platformVersionForDemoDelimiterDb
                            : x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.Stable83Version,
                        DbSourceName = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.V82Name,
                        SourceDatabaseClusterName = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress,
                        Zone = x.AccountDatabaseOnDelimiter.Zone,
                        IsDemo = x.AccountDatabaseOnDelimiter.IsDemo,
                        DelimiterDatabaseId = x.AccountDatabaseOnDelimiter.AccountDatabaseId,
                        IsDemoExpired = false,
                        ConnectionAddress = x.Account.AccountConfiguration.Segment.CloudServicesFileStorageServer.ConnectionAddress,
                        ConnectionAddressV82 = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress,
                        ConnectionAddressV83 = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress,
                        SegmentAlpha83Version = x.Account.AccountConfiguration.Segment.Alpha83Version,
                        SegmentStable83Version = x.Account.AccountConfiguration.Segment.Stable82Version,
                        SegmentStable82Version = x.Account.AccountConfiguration.Segment.Stable82Version,
                        PublishSiteName = x.Account.AccountConfiguration.Segment.CloudServicesContentServer.PublishSiteName,
                        GroupByAccount = x.Account.AccountConfiguration.Segment.CloudServicesContentServer.GroupByAccount
                    }
                )
                .Where(x => !States.Contains(x.State) && databaseIds.Contains(x.Id))
                .ToListAsync();

            data
                .GroupJoin(filteredAccountDatabases, z => z.Id, x => x.AccountDatabasesId, (database, accesses) => (database, accesses))
                .ToList()
                .ForEach(x =>
                {
                    var access = x.accesses.FirstOrDefault();

                    x.database.IsLock = access?.AcDbAccessIsLock;
                    x.database.LaunchType = access?.AcDbAccessLaunchType;
                    x.database.RolesJhoArray = acDbAccRolesJhOes
                        .Where(y => y.AcDbAccessesID == access?.AcDbAccessId)
                        .Select(y => y.RoleJHO)
                        .ToList();
                });

            return data;
        }

        public IQueryable<AccessDatabaseListQueryDataDto> GetAccessDatabase()
        {
            var serviceAccountIdForDemoDelimiterDatabases = Guid.Parse(Context.CloudConfigurations
                        .Where(cc => cc.Key == "DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases")
                        .Select(cc => cc.Value)
                        .FirstOrDefault());

            var platformVersionForDemoDelimiterDb = Context.Accounts
                .Join(Context.AccountConfigurations, a => a.Id, accConf => accConf.AccountId, (a, accConf) => new { a, accConf })
                .Join(Context.CloudServicesSegments, acc => acc.accConf.SegmentId, css => css.ID, (acc, css) => new { acc.a, css })
                .Where(acc => acc.a.Id == serviceAccountIdForDemoDelimiterDatabases)
                .Select(acc => acc.css.Stable83Version)
                .FirstOrDefault();

            var query = Context.AccountDatabases
                .AsQueryable()
                .AsNoTracking()
                .Select(x => new AccessDatabaseListQueryDataDto
                {
                    Id = x.Id,
                    AccountId = x.AccountId,
                    AccountUserId = x.AcDbAccesses.FirstOrDefault().AccountUserID ?? Guid.Empty,
                    CreationDate = x.CreationDate,
                    LastActivityDate = x.LastActivityDate,
                    Caption = x.Caption,
                    ApplicationName = x.ApplicationName,
                    PublishState = x.PublishState,
                    DbName = x.V82Name,
                    DbNumber = x.DbNumber,
                    State = x.State,
                    SqlName = x.SqlName,
                    LockedState = x.LockedState,
                    IsFile = x.IsFile,
                    SizeInMb = x.SizeInMB,
                    TemplateId = x.TemplateId,
                    ServiceName = x.ServiceName,
                    LaunchType = x.AcDbAccesses.FirstOrDefault().LaunchType,
                    SiteName = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.SourceAccountDatabase.DelimiterSourceAccountDatabases.FirstOrDefault() != null ?  x.AccountDatabaseOnDelimiter.SourceAccountDatabase.DelimiterSourceAccountDatabases.FirstOrDefault().DatabaseOnDelimitersPublicationAddress : null : null,
                    DemoSiteName = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.DbTemplateDelimiter.DemoDatabaseOnDelimitersPublicationAddress : null,
                    DelimiterPlatformVersion = x.AccountDatabaseOnDelimiter != null &&
                                               x.AccountDatabaseOnDelimiter.IsDemo == true
                        ? platformVersionForDemoDelimiterDb
                        : x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment
                            .Stable83Version,
                    DbSourceName = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.SourceAccountDatabase.V82Name : null,
                    SourceDatabaseClusterName = x.AccountDatabaseOnDelimiter != null ?
                        x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment
                            .CloudServicesEnterpriseServer83.ConnectionAddress : null,
                    Zone = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.Zone : null,
                    IsDemo = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.IsDemo : null,
                    DelimiterDatabaseId = x.AccountDatabaseOnDelimiter != null ? x.AccountDatabaseOnDelimiter.AccountDatabaseId : null,
                    IsLock = x.AcDbAccesses.FirstOrDefault() != null ? x.AcDbAccesses.FirstOrDefault().IsLock : null,
                    IsDemoExpired = false,
                    LaunchParameters = x.LaunchParameters,
                    RolesJhoArray = x.AcDbAccesses.FirstOrDefault() != null ? x.AcDbAccesses.FirstOrDefault().AcDbAccRolesJHOes.Select(z => z.RoleJHO).ToList() : null,
                    AccountCaption = x.Account.AccountCaption,
                    IndexNumber = x.Account.IndexNumber,
                    ConnectionAddress = x.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer82.ConnectionAddress,
                    V82Server = x.V82Server,
                    Platform = x.ApplicationName,
                    ConnectionAddressV82 = x.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer82.ConnectionAddress,
                    ConnectionAddressV83 = x.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress,
                    SegmentAlpha83Version = x.Account.AccountConfiguration.Segment.Alpha83Version,
                    SegmentStable83Version = x.Account.AccountConfiguration.Segment.Stable82Version,
                    SegmentStable82Version = x.Account.AccountConfiguration.Segment.Stable82Version,
                    PublishSiteName = x.Account.AccountConfiguration.Segment.CloudServicesContentServer.PublishSiteName,
                    GroupByAccount = x.Account.AccountConfiguration.Segment.CloudServicesContentServer.GroupByAccount,
                    DistributionType = x.DistributionType,
                    DelimiterDatabaseMustUseWebService = x.AccountDatabaseOnDelimiter != null && x.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.DelimiterDatabaseMustUseWebService
                });

            return query;
        }

        /// <summary>
        ///     Получить список баз на разделителях для пользователя
        /// с указанием кода конфигурации конкретного разделителя
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <param name="configurationCode">Код конфигурации</param>
        public async Task<List<DelimitersDatabaseQueryDataDto>> GetDelimitersDbsForAccUserAsync(Guid accountUserId,
            string configurationCode)
        {
            return await GetDelimitersDbs(accountUserId, configurationCode);
        }

        public async Task<List<DelimitersDatabaseQueryDataDto>> GetDelimitersDbs(Guid accountUserId,
            string configurationCode)
        {
            var query = DbSet
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AcDbAccesses.Any(z => z.AccountUserID == accountUserId) &&
                            x.AccountDatabaseOnDelimiter.Zone.HasValue && !x.AccountDatabaseOnDelimiter.IsDemo &&
                            !States.Contains(x.State))
                .Where(AccountDatabaseSpecification.ByConfigurationCode(configurationCode))
                .Select(x => new DelimitersDatabaseQueryDataDto
                {
                    Id = x.Id,
                    Caption = x.Caption,
                    DbName = x.V82Name,
                    DbSourceId = x.AccountDatabaseOnDelimiter.DatabaseSourceId.Value,
                    DbSourceName = x.AccountDatabaseOnDelimiter.SourceAccountDatabase.V82Name,
                    SiteName = x.DelimiterSourceAccountDatabases
                        .FirstOrDefault(z =>
                            string.IsNullOrEmpty(configurationCode) ||
                            z.DbTemplateDelimiterCode == configurationCode).DatabaseOnDelimitersPublicationAddress ?? x.AccountDatabaseOnDelimiter.SourceAccountDatabase.DelimiterSourceAccountDatabases.FirstOrDefault(z =>
                        string.IsNullOrEmpty(configurationCode) ||
                        z.DbTemplateDelimiterCode == configurationCode).DatabaseOnDelimitersPublicationAddress,
                    Zone = x.AccountDatabaseOnDelimiter.Zone
                });

            return await query.ToListAsync();
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        public async Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesById(Guid accountDatabaseId)
        {
            return await GetAccessDatabase().FirstOrDefaultAsync(x =>
                x.Id == accountDatabaseId && !States.Contains(x.State));
        }

        /// <summary>
        /// Получить данные информационной базы по имени базы.
        /// </summary>
        /// <param name="v82Name">Имя информационной базы.</param>
        public async Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesByNameAsync(string v82Name)
        {
            return await GetAccessDatabase().FirstOrDefaultAsync(x =>
                x.DbName == v82Name && !States.Contains(x.State));
        }

        /// <summary>
        /// Получить данные информационной базы по номеру.
        /// </summary>
        /// <param name="accountDatabaseId">Идентификатор информационной базы.</param>
        /// <param name="accountUserId">Идентификатор пользователя который запрашивает сведения о базе.</param>
        public async Task<AccessDatabaseListQueryDataDto> GetAccountDatabasesForUserById(Guid accountDatabaseId,
            Guid accountUserId)
        {
            return await GetAccessDatabase().FirstOrDefaultAsync(x =>
                x.Id == accountDatabaseId && x.AccountUserId == accountUserId && !States.Contains(x.State));
        }

        /// <summary>
        /// Получить следующий номер базы.
        /// </summary>
        private int GetNextDbNumber(Account account)
            => (Context.AccountDatabases
                    .Where(db => db.AccountId == account.Id)
                    .Max(db => (int?) db.DbNumber) ?? 0) + 1;

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        private AccountDatabase CreateAccountDatabaseRecord(DbTemplate template, Account account,
            PlatformType platformType,
            string caption, CloudServicesFileStorageServer fileStorage, bool isFile, int dbNumber, string v82Name)
        {
            if (string.IsNullOrWhiteSpace(caption))
                caption = "Моя база №" + dbNumber;

            var database = new AccountDatabase
            {
                Id = Guid.NewGuid(),
                Caption = caption.TrimStart().TrimEnd(),
                CreationDate = DateTime.Now,
                DbNumber = dbNumber,
                LastActivityDate = DateTime.Now,
                SizeInMB = 0,
                TemplateId = template?.Id,
                AccountId = account.Id,
                V82Name = v82Name,
                SqlName = $"db_{DatabaseConst.AllLocalesFileDatabasePrefix}{account.IndexNumber}_{dbNumber}",
                ServiceName = Clouds42Service.MyEnterprise.ToString(),
                StateEnum = DatabaseState.NewItem,
                IsFile = isFile,
                ApplicationName = platformType.GetDisplayName(),
                FileStorageID = fileStorage?.ID
            };

            Insert(database);
            Context.SaveChanges();

            return database;
        }
    }
}
