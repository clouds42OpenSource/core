﻿using Clouds42.Domain.DataModels.billing.AgentRequisites;
using System.Linq;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для агентских договоров
    /// </summary>
    public class AgentRequisitesRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AgentRequisites>(context, smartTransaction), IAgentRequisitesRepository
    {
        /// <summary>
        /// Вставить новую запись реквизитов агента
        /// </summary>
        /// <param name="agentRequisites">Реквизиты агента</param>
        public void InsertAgentRequisites(AgentRequisites agentRequisites)
        {
            // Определяем следующий номер
            var lastNumber = Context.AgentRequisites
                .Where(ar => ar.AccountOwnerId == agentRequisites.AccountOwnerId)
                .OrderByDescending(ar => ar.Number)
                .Select(ar => (int?)ar.Number)
                .FirstOrDefault() ?? 0;
            
            var newAgentRequisites = new AgentRequisites
            {
                Id = agentRequisites.Id,
                Number = lastNumber + 1,
                AgentRequisitesStatus = agentRequisites.AgentRequisitesStatus,
                StatusDateTime = null, 
                LegalPersonRequisitesId = agentRequisites.LegalPersonRequisitesId,
                PhysicalPersonRequisitesId = agentRequisites.PhysicalPersonRequisitesId,
                SoleProprietorRequisitesId = agentRequisites.SoleProprietorRequisitesId,
                AccountOwnerId = agentRequisites.AccountOwnerId,
                CreationDate = agentRequisites.CreationDate
            };

            Context.AgentRequisites.Add(newAgentRequisites);

            Context.SaveChanges();
        }
    }
}
