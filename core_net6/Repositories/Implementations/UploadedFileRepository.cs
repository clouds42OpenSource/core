﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.Files;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для работы с загружаемыми файлами
    /// </summary>
    public class UploadedFileRepository(
        Clouds42DbContext context,
        ISmartTransaction smartTransaction)
        : GenericRepository<UploadedFile>(context, smartTransaction), IUploadedFileRepository
    {
        /// <summary>
        /// Получить статус загружаемого файла
        /// </summary>
        /// <param name="uploadedFileId">Идентификатор загружаемого файла</param>
        /// <returns>Статус загружаемого файла</returns>
        public UploadedFileStatus GetUploadedFileStatus(Guid uploadedFileId)
        {
            return FirstOrThrowException(x => x.Id == uploadedFileId).Status;
        }
    }
}
