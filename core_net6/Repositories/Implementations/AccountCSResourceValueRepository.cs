﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class AccountCsResourceValueRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountCSResourceValue>(context, smartTransaction), IAccountCsResourceValueRepository
    {
        public int GetAccountCsResourceValuesModifyCount(Guid accountId, Guid csResourceId, DateTime dateTime)
        {
            return DbSet
                .Where(x => x.AccountId == accountId &&
                            x.CSResourceId == csResourceId &&
                            x.ModifyResourceDateTime <= dateTime)
                .Sum(x => x.ModifyResourceValue);
        }

        public async Task<int> GetAccountCsResourceValuesModifyCountAsync(Guid accountId, Guid csResourceId, DateTime dateTime)
        {
            return await DbSet
                .Where(x =>  x.AccountId == accountId && 
                             x.CSResourceId == csResourceId && 
                             x.ModifyResourceDateTime <= dateTime)
                .SumAsync(x => x.ModifyResourceValue);

        }

        public IEnumerable<AccountCSResourceValue> GetAccountCsResourceValues(Guid accountId, Guid csResourceId)
        {
            return DbSet
                .Where(x => x.AccountId == accountId && x.CSResourceId == csResourceId);
        }

        public int GetAccountCsResourceValuesCount(Guid accountId, Guid csResourceId)
        {
            return DbSet
                .Count(x => x.AccountId == accountId &&
                                x.CSResourceId == csResourceId);
        }

        public AccountCSResourceValue GetAccountCsResourceValue(Guid accountCsResourceValueId)
        {
            return DbSet.FirstOrDefault(x => x.Id == accountCsResourceValueId);
        }
    }
}

