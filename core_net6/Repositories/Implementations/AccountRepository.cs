﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий по работе с сущностью аккаунта
    /// </summary>
    public class AccountRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Account>(context, smartTransaction), IAccountRepository
    {
        public Account GetAccountAndAccountUsers(Guid id)
        {
            return DbSet.Include(x => x.AccountUsers).FirstOrDefault(c => c.Id == id);
        }

        public Account GetAccountByUserId(Guid userId) 
        {
            var user = Context.AccountUsers.Include(accountUser => accountUser.Account).FirstOrDefault(u => u.Id == userId);
            if (user == null)
                throw new NotFoundException($"Аккаунт по идентификатору пользователя {userId} не найден");
            return user.Account;
        }

        public Account GetAccount(Guid accountId)
        {
            return Context.Accounts.FirstOrDefault(a => a.Id == accountId);
        }

        /// <inheritdoc/>
        public Task<Account> GetAccountAsync(Guid accountId)
        {
            return Context.Accounts.FirstOrDefaultAsync(a => a.Id == accountId);
        }

        public void InsertAccount(Account newAccount)
        {
            Context.Entry(newAccount).State = EntityState.Added;
            Context.SaveChanges();
        }

        /// <summary>
        /// Получить количество сервисов у аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Количество сервисов у аккаунта</returns>
        public int GetCountBillingServicesForAccount(Guid accountId)
        {
            var countServices = Context.Services.Count(w => w.AccountOwnerId == accountId);
            return countServices;
        }

        /// <inheritdoc/>
        public Locale GetLocale(Guid accountId, Guid localeId)
        {
            var locale = Context.AccountConfigurations.Include(accountConfiguration => accountConfiguration.Locale).FirstOrDefault(x => x.AccountId == accountId)?.Locale;
            return locale ?? GetDefaultLocale(localeId);
        }

        /// <inheritdoc/>
        public async Task<Locale> GetLocaleAsync(Guid accountId, Guid localeId)
        {
            var locale = await Context.AccountConfigurations
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountId == accountId)
                .Select(x => x.Locale)
                .FirstOrDefaultAsync();

            return locale ?? GetDefaultLocale(localeId);
        }

        /// <inheritdoc/>
        public async Task<Locale> GetLocaleOrThrowAsync(Guid accountId, Guid localeId)
        {
            var locale = await GetLocaleAsync(accountId, localeId);
            if (locale == null)
                throw new NotFoundException($"Локаль для аккаунта {accountId} не найдена");
            return locale;
        }

        /// <summary>
        /// Получить локаль по умолчанию
        /// </summary>
        /// <returns>Локаль по умолчанию</returns>
        public Locale GetDefaultLocale(Guid defaultLocaleId)
        {
            return Context.Locale.FirstOrDefault(x => x.ID == defaultLocaleId);
        }

        public List<Guid> GetAccountAdminIds(Guid accountId)
        {
            return Context.AccountUsers
                .AsQueryable()
                .AsNoTracking()
                .Where(x => x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin && x.AccountId == accountId))
                .Select(x => x.Id)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Получает список AccountUser с правами админа и активной арендой
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<AccountUser> GetAccountAdminsWithActivatedRent1C(Guid accountId)
        {
            var serviceTypes = new List<ResourceType> { ResourceType.MyEntUser, ResourceType.MyEntUserWeb };

            return Context
                .AccountUsers
                .AsQueryable()
                .Where(x => x.AccountId == accountId &&
                            x.AccountUserRoles.Any(y => y.AccountUserGroup == AccountUserGroup.AccountAdmin) &&
                            x.Account.BillingAccount.Resources.Any(y =>
                                serviceTypes.Contains(y.BillingServiceType.SystemServiceType.Value)))
                .ToList();
        }

        /// <summary>
        ///     Получить доступные почты для рассылки у аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        public List<string> GetEmails(Guid accountId)
        {
            return Context.AccountUsers
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountUserRoles)
                .Where(x => x.AccountId == accountId && x.AccountUserRoles.Any(y => y.AccountUserGroup == AccountUserGroup.AccountAdmin))
                .Select(x => x.Email)
                .Union(Context.Accounts
                        .AsQueryable()
                        .AsNoTracking()
                        .Where(a => a.Id == accountId)
                        .SelectMany(a => a.AccountEmails.Select(e => e.Email))
                )
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Получить данные аккаунта.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <returns></returns>
        public async Task<AccountInfoQueryDataDto> GetAccountInfoByIdAsync(Guid accountId)
        {
            return await DbSet
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.AccountConfiguration)
                .ThenInclude(x => x.Locale)
                .Include(x => x.AccountUsers)
                .ThenInclude(x => x.AccountUserRoles)
                .Select(x => new AccountInfoQueryDataDto
                {
                    AccountId = x.Id,
                    IndexNumber = x.IndexNumber,
                    AccountCaption = x.AccountCaption,
                    RegistrationDate = x.RegistrationDate,
                    ReferralAccountId = x.ReferralAccountID,
                    Removed = x.Removed,
                    Description = x.Description,
                    LocaleId = x.AccountConfiguration.LocaleId,
                    Inn = x.AccountRequisites.Inn,
                    AccountCurrency = x.AccountConfiguration.Locale.Currency,
                    AccountAdminId = x.AccountUsers.FirstOrDefault(z =>
                        z.AccountUserRoles.Any(y => y.AccountUserGroup == AccountUserGroup.AccountAdmin)).Id
                })
                .FirstOrDefaultAsync(x => x.AccountId == accountId);
        }


        /// <summary>
        /// Необходимость аккаунту принять
        /// агентское соглашение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Необходимость принять агентское соглашение</returns>
        public bool NeedApplyAgencyAgreement(Guid accountId)
        {
            return Context.AgentWallets.Any(x => !x.ApplyAgencyAgreementDate.HasValue && x.AccountOwnerId == accountId);
        }

        /// <summary>
        /// Получить Id всех аккаунтов
        /// </summary>
        /// <returns>Id аккаунтов</returns>
        public List<Guid> GetAllAccountIds()
        {
            return Context.Accounts.Select(account => account.Id).ToList();
        }

        /// <summary>
        /// Получить аккаунт по номеру акккаунта
        /// </summary>
        /// <param name="indexNumber">Номер аккаунта</param>
        /// <returns>Аккаунт</returns>
        public Account GetAccountByIndexNumber(int indexNumber)
        {
            return Context.Accounts.FirstOrDefault(a => a.IndexNumber == indexNumber);
        }
    }
}
