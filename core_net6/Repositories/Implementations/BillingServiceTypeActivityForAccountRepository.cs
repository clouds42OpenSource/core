﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для работы с моделями активности услуги сервиса для аккаунта
    /// </summary>
    public class BillingServiceTypeActivityForAccountRepository(
        Clouds42DbContext context,
        ISmartTransaction smartTransaction)
        : GenericRepository<BillingServiceTypeActivityForAccount>(context, smartTransaction),
            IBillingServiceTypeActivityForAccountRepository
    {
        /// <summary>
        /// Получить список ID удаленных, но еще доступных услуг для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список ID удаленных, но еще доступных услуг для аккаунта</returns>
        public List<Guid> GetDeletedButStillAvailableServiceTypesIdForAccount(Guid billingServiceId, Guid accountId)
            => Context.BillingServiceTypeActivityForAccounts.Where(
                bst =>
                    bst.BillingServiceType.Service.Id == billingServiceId &&
                    bst.AccountId == accountId &&
                    (bst.AvailabilityDateTime - DateTime.Now).TotalDays > 0
            ).Select(bst => bst.BillingServiceTypeId).ToList();
    }
}
