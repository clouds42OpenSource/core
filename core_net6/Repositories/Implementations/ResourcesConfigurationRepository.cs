﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class ResourcesConfigurationRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<ResourcesConfiguration>(context, smartTransaction), IResourcesConfigurationRepository
    {
        /// <summary>
        /// Получить дату окончания срока действия для сервиса, привязанного к указанному аккаунту.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="service">Сервис</param>
        /// <param name="promisePaymentDays">Количество дней активности обещанного платежа.</param>
        /// <returns>Если дата устаревания сервиса для аккаунта найдена и она не заморожена (Frozen), то возвращается она,
        /// если же она существует и заморожена, то возвращается параметр dateTime, если он установлен, или текущая дата/время
        /// если не установлен. В других случаях возвращается NULL.</returns>
        public async Task<DateTime?> GetServiceExpirationDate(Guid accountId, Clouds42Service service, int promisePaymentDays)
        {
            var dateTimeVar = DateTime.Now;

            var result = await
                (from a in Context.Accounts
                    join rc in Context.ResourcesConfigurations on a.Id equals rc.AccountId
                    join s in Context.Services on rc.BillingServiceId equals s.Id
                    join ba in Context.BillingAccounts on a.Id equals ba.Id
                    join st in Context.ServiceTypes on s.Id equals st.ServiceId
                    join depSt in Context.ServiceTypes on st.DependServiceTypeId equals depSt.Id into depStGroup
                    from depSt in depStGroup.DefaultIfEmpty()
                    join depRc in Context.ResourcesConfigurations on new { AccountId = a.Id, BillingServiceId = depSt.ServiceId } equals new { depRc.AccountId, depRc.BillingServiceId } into depRcGroup
                    from depRc in depRcGroup.DefaultIfEmpty()
                    where s.SystemService == service && a.Id == accountId
                    orderby rc.Id
                    select new
                    {
                        ExpireDate =
                            (ba.PromisePaymentSum != null && ba.PromisePaymentSum > 0 &&
                             ba.PromisePaymentDate != null &&
                             (dateTimeVar - ba.PromisePaymentDate.Value).Days > promisePaymentDays)
                                ? dateTimeVar
                                : ((depSt != null ? depRc.ExpireDate : rc.ExpireDate) ?? dateTimeVar)
                    })
                .FirstOrDefaultAsync();

            return result?.ExpireDate;
        }

        /// <summary>
        /// Получить список активных не дочерних сервисов, которые можно продлить или нужно заблокировать.       
        /// </summary>        
        public List<ResourcesConfiguration> GetActiveMainServicesForProlongOrLock()
        {
            var dateTimeNow = DateTime.Now;
            
            var query = from rc in Context.ResourcesConfigurations
                join ser in Context.Services on rc.BillingServiceId equals ser.Id
                join serAc in Context.ServiceAccounts on rc.AccountId equals serAc.Id into serAcGroup
                from serAc in serAcGroup.DefaultIfEmpty()
                where rc.ExpireDate != null
                      && rc.IsDemoPeriod == false
                      && ser.BillingServiceTypes.All(x => !x.DependServiceTypeId.HasValue)
                      && (rc.ExpireDate.Value - dateTimeNow).Days <= 0
                      && ser.SystemService != Clouds42Service.Recognition
                      && ser.SystemService != Clouds42Service.Esdl
                      && rc.Frozen == false
                      && serAc == null
                select rc;

            return query.ToList();
        }

        /// <summary>
        /// Получить список сервисов, которые можно продлить или заблокировать.
        /// </summary>        
        public List<ResourcesConfiguration> GetSoonExpiredMainServices(int beforeLockDays)
        {
            var dateTimeNow = DateTime.Now;
          
            return (from rc in Context.ResourcesConfigurations
                join ser in Context.Services on rc.BillingServiceId equals ser.Id
                join serAc in Context.ServiceAccounts on rc.AccountId equals serAc.Id into serAcGroup
                from serAc in serAcGroup.DefaultIfEmpty()
                where rc.ExpireDate != null
                      && rc.IsDemoPeriod == false
                      && (rc.Frozen == false || rc.Frozen == null)
                      && rc.ExpireDate >= dateTimeNow
                      && ser.BillingServiceTypes.All(x => !x.DependServiceTypeId.HasValue)
                      && Context.Payments.Any(p => p.AccountId == rc.AccountId && p.OperationType == PaymentType.Inflow.ToString() &&
                                                   p.Status == PaymentStatus.Done.ToString() &&
                                                   p.Sum > 0)
                      && (dateTimeNow - rc.ExpireDate.Value).Days <= beforeLockDays
                      && rc.Cost > 0
                      && serAc == null
                select rc).ToList();
        }

        /// <summary>
        /// Получить список для разблокирования платных сервисов, которые заблокированны, на которые есть деньги что бы разблокировать и у аккаунтов которых нет просроченного обещанного платежа.
        /// </summary>        
        public List<Guid> GetPaidLockedServicesWithAvailableBalanceOnAccount(int promisePaymentDays)
        {
            DateTime dateTimeNow = DateTime.Now;

            var result = (from rc in Context.ResourcesConfigurations
                          join ser in Context.Services on rc.BillingServiceId equals ser.Id
                          join ba in Context.BillingAccounts on rc.AccountId equals ba.Id
                          join a in Context.Accounts on rc.AccountId equals a.Id
                          join serAc in Context.ServiceAccounts on a.Id equals serAc.Id into gServiceAccount
                          from serAc in gServiceAccount.DefaultIfEmpty()
                          where ser.BillingServiceTypes.All(x => !x.DependServiceTypeId.HasValue)
                                && rc.Frozen == true              
                                && rc.Cost > 0                   
                                && rc.IsDemoPeriod == false
                                && (ser.SystemService == null || (ser.SystemService != Clouds42Service.Recognition && ser.SystemService != Clouds42Service.Esdl))
                                && (ba.Balance + ba.BonusBalance >= rc.Cost) 
                                && (a.Removed == null || a.Removed == false)
                                && (ba.PromisePaymentSum == null || ba.PromisePaymentSum == 0 || ba.PromisePaymentDate.Value.AddDays(promisePaymentDays) > dateTimeNow)
                                && serAc == null                          
                          select rc.Id).ToList();

            return result;
        }

        /// <summary>
        /// Получить список заблокированных сервисов у которых 
        /// цена = 0 и обязательно есть спонсируемый пользователь,
        /// у аккаунта который спонсирует Аренда активна
        /// </summary>
        public List<ResourcesConfiguration> GetLockedSponsoredServiceConfigurations(int promisePaymentDays)
        {
            var sqlQuery = string.Format(SqlQueries.ResourceConfiguration.LockedSponsoredServiceConfigurations,
                promisePaymentDays, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            var result = Context.ResourcesConfigurations.FromSqlRaw(sqlQuery).ToList();

            return result;
        }

        /// <summary>
        /// Признак, что основной сервис аккаунта имеет активные спонсируемые лицензии
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса</param>
        /// <returns>true - если есть спонсируемые спонсируемые лицензии и у спонсора сервис активен</returns>
        public bool IsMainServiceHasActiveSponsoredLicenses(ResourcesConfiguration configuration)
        {
            return Context.ResourcesConfigurations
                .AsQueryable()
                .AsNoTracking()
                .Any(x => x.BillingServiceId == configuration.BillingServiceId &&
                          x.BillingService.BillingServiceTypes.Any(z =>
                              z.Resources.Any(y => y.AccountSponsorId.HasValue && y.Subject.HasValue)) &&
                          x.Frozen.HasValue && !x.Frozen.Value);
        }

        /// <summary>
        /// Получить список всех платных родительских сервисов
        /// </summary>
        public List<ResourcesConfiguration> GetAllPaidActiveMainServices(Guid accountId)
        {
            return Context.ResourcesConfigurations
                .Where(res =>
                    res.AccountId == accountId &&
                    res.Cost > 0 &&
                    res.Frozen == false &&
                    res.BillingService.BillingServiceTypes.All(w => w.DependServiceTypeId == null) &&
                    res.BillingService.SystemService != Clouds42Service.Recognition).ToList();
        }
        
        /// <summary>
        /// Получить список всех сервисов с просроченным демо периодом
        /// </summary>
        public List<ResourcesConfiguration> GetAllExpiredDemoServices()
        {
            var nowDate = DateTime.Now;

            var query = DbSet
                .AsQueryable()
                .Where(x => x.BillingAccounts.Account.ServiceAccount == null &&
                            (!x.Frozen.HasValue || !x.Frozen.Value) &&
                            (x.ExpireDate.HasValue && (x.ExpireDate.Value - nowDate).Days <= 0) &&
                            x.IsDemoPeriod);

            return query.ToList();
        }

        /// <summary>
        /// Получить список всех гибридных сервисов с просроченным периодом
        /// </summary>
        public List<ResourcesConfiguration> GetAllExpiredHybridServices()
        {
            var nowDate = DateTime.Now;

            return DbSet
                .AsQueryable()
                .Where(x => (x.ExpireDate.HasValue && (x.ExpireDate.Value - nowDate).Days <= 0) && 
                            !x.IsDemoPeriod && 
                            (!x.Frozen.HasValue || !x.Frozen.Value) && 
                            x.BillingService.IsHybridService &&
                            x.Cost > 0 &&
                            x.BillingAccounts.Account.ServiceAccount == null
                )
                .ToList();
        }

        /// <summary>
        /// Получить список всех конфигураций, у которых осталось количество дней до завершения демо = <paramref name="numberOfRemainingDays"/>
        /// </summary>
        /// <returns>Список найденных конфигураций у которых осталось количество дней до завершения демо = <paramref name="numberOfRemainingDays"/></returns>
        public List<ResourcesConfiguration> GetAllDemoResourcesConfigurationsWithNumberOfRemainingDays(int numberOfRemainingDays)
        {
            var currentDate = DateTime.Now;

            var query =
                from rc in Context.ResourcesConfigurations
                join sa in Context.ServiceAccounts on rc.AccountId equals sa.Id into saG
                from subSaG in saG.DefaultIfEmpty()
                where (rc.Frozen == null || rc.Frozen == false)
                   && rc.IsDemoPeriod
                   && rc.ExpireDate != null
                   && (currentDate -  rc.ExpireDate.Value).Days == numberOfRemainingDays
                   && subSaG == null
                select rc;

            return query.ToList();
        }

        /// <summary>
        /// Получить список всех конфигураций, у которых истёк демо период, заблокирован и есть автоподписка
        /// </summary>
        /// <param name="accountId">Для какого аккаунта</param>
        /// <returns>Список всех конфигураций, у которых истёк демо период, заблокирован и есть автоподписка</returns>
        public List<ResourcesConfiguration> GetAllFrozenDemoResourcesConfigurationsWithAutoSubscription(Guid accountId)
        {
            var currentDate = DateTime.Now;

            return (
                from rc in Context.ResourcesConfigurations
                join st in Context.ServiceTypes on rc.BillingServiceId equals st.ServiceId
                join sa in Context.ServiceAccounts on rc.AccountId equals sa.Id into saG
                from subSaG in saG.DefaultIfEmpty()
                join r in Context.Resources.Where(item => item.AccountId == accountId && item.Subject != null) on st.Id equals r.BillingServiceTypeId into res
                where rc.AccountId == accountId
                   && rc.Frozen == true
                   && rc.IsDemoPeriod
                   && rc.ExpireDate != null
                   && (rc.ExpireDate.Value - currentDate).Days <= 0
                   && rc.IsAutoSubscriptionEnabled == true
                   && res.Any()
                   && subSaG == null
                select rc).ToList();
        }
    }
}
