﻿using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations;

public class AccountUserBitDepthRepository : GenericRepository<AccountUserBitDepth>, IAccountUserBitDepthRepository
{
    public AccountUserBitDepthRepository(Clouds42DbContext context, ISmartTransaction smartTransaction) : base(context, smartTransaction)
    {
    }
}
