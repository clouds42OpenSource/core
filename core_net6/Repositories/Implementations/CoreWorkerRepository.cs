﻿using System;
using System.Linq;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CoreWorkerRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CoreWorker>(context, smartTransaction), ICoreWorkerRepository
    {
        public bool CoreWorkerCanProcessTask(Type jobType, short? coreWorkerId)
        {
            var taskName = jobType.GetCoreWorkerTaskName();
            var validTick = DateTime.Now.AddMinutes(-2);

            return Context.CoreWorkers.AsNoTracking()
                .Include(x => x.CoreWorkerAvailableTasksBags)
                .ThenInclude(x => x.CoreWorkerTask)
                .Any(x => (!coreWorkerId.HasValue || x.CoreWorkerId == coreWorkerId) &&
                          x.Status == CoreWorkerStatus.ON && 
                          x.Tick.HasValue && 
                          x.Tick.Value > validTick && 
                          taskName != null && 
                          x.CoreWorkerAvailableTasksBags.Any(z => z.CoreWorkerTask.TaskName.ToLower() == taskName.ToLower()));
        }

    }
}
