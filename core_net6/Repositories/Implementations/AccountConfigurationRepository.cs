﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountConfiguration;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.IDataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class AccountConfigurationRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountConfiguration>(context, smartTransaction), IAccountConfigurationRepository
    {
        private static readonly object Obj = new();

        /// <summary>
        /// Получить конфигурацию аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Конфигурация аккаунта</returns>
        public AccountConfiguration GetAccountConfiguration(Guid accountId)
        {
            lock (Obj)
            {
                return Context.AccountConfigurations.FirstOrDefault(configuration => configuration.AccountId == accountId);
            }
        }

        /// <summary>
        /// Получить модель конфигурации аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель конфигурации аккаунта</returns>
        public AccountConfigurationDto GetAccountConfigurationDc(Guid accountId) =>
            MapToAccountConfigurationDc(GetAccountConfiguration(accountId));

        /// <summary>
        /// Получить модель аккаунта с конфигурацией
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель аккаунта с конфигурацией</returns>
        public AccountWithConfigurationDto GetAccountWithConfiguration(Guid accountId) =>
            GetAccountsWithConfiguration()
                .FirstOrDefault(accountWithConfig => accountWithConfig.Account.Id == accountId);

        /// <summary>
        /// Получить данные конфигурации аккаунта по локали
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по локали</returns>
        public AccountConfigurationByLocaleDataDto GetAccountConfigurationByLocaleData(Guid accountId) =>
            GetAccountConfigurationsDataByLocale()
                .FirstOrDefault(accountConfig => accountConfig.Account.Id == accountId) ??
            throw new NotFoundException(
                $"Не удалось получить конфигурацию аккаунта по локали. Id аккаунта '{accountId}'");

        /// <summary>
        /// Получить локаль пользователя аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        /// <returns>Локаль пользователя аккаунта</returns>
        public ILocale GetAccountUserLocale(Guid accountUserId) =>
            (from accountUser in Context.AccountUsers
             join accountConfigurationByLocaleData in GetAccountConfigurationsDataByLocale() on accountUser.AccountId
                 equals accountConfigurationByLocaleData.Account.Id
             where accountUser.Id == accountUserId
             select accountConfigurationByLocaleData.Locale).FirstOrDefault();

        /// <summary>
        /// Получить модели аккаунтов с конфигурацией
        /// </summary>
        /// <returns>Модели аккаунтов с конфигурацией</returns>
        public IQueryable<AccountWithConfigurationDto> GetAccountsWithConfiguration() =>
            from account in Context.Accounts
            join accountConfiguration in Context.AccountConfigurations on
                account.Id equals accountConfiguration.AccountId
            select new AccountWithConfigurationDto
            {
                Account = account,
                AccountConfiguration = accountConfiguration
            };

        public IQueryable<AccountConfigurationByLocaleDataDto> GetAccountConfigurationsDataByLocale() =>
            from accountWithConfiguration in GetAccountsWithConfiguration()
            join locale in Context.Locale on accountWithConfiguration
                    .AccountConfiguration.LocaleId equals
                locale.ID
            select new AccountConfigurationByLocaleDataDto
            {
                Account = accountWithConfiguration.Account,
                Locale = locale
            };

        /// <summary>
        /// Получить конфигурации аккаунтов по сегменту
        /// </summary>
        /// <returns>Конфигурации аккаунтов по сегменту</returns>
        public IQueryable<AccountConfigurationBySegmentDataDto> GetAccountConfigurationsDataBySegment() =>
            from accountWithConfiguration in GetAccountsWithConfiguration()
            join segment in Context.CloudServicesSegments on accountWithConfiguration
                    .AccountConfiguration.SegmentId equals
                segment.ID
            select new AccountConfigurationBySegmentDataDto
            {
                Account = accountWithConfiguration.Account,
                Segment = segment
            };

        /// <summary>
        /// Получить данные конфигурации аккаунта по сегменту
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные конфигурации аккаунта по сегменту</returns>
        public AccountConfigurationBySegmentDataDto GetAccountConfigurationBySegmentData(Guid accountId) =>
            GetAccountConfigurationsDataBySegment()
                .FirstOrDefault(accountConfig => accountConfig.Account.Id == accountId);

        /// <summary>
        /// Получить хранилище бэкапов по сегменту аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Хранилище бэкапов по сегменту аккаунта</returns>
        public async Task<CloudServicesBackupStorage> GetBackupStorageByAccountSegment(Guid accountId) =>
            await (from accountConfigurationData in GetAccountConfigurationsDataBySegment()
                   join backupStorage in Context.CloudServicesBackupStorages on
                       accountConfigurationData.Segment.BackupStorageID equals backupStorage.ID
                   where accountConfigurationData.Account.Id == accountId
                   select backupStorage).FirstOrDefaultAsync();

        /// <summary>
        /// Получить конфигурации аккаунтов по поставщику
        /// </summary>
        /// <returns>Конфигурации аккаунтов по поставщику</returns>
        public IQueryable<AccountConfigurationBySupplierDataDto> GetAccountConfigurationsDataBySupplier() =>
            from accountWithConfiguration in GetAccountsWithConfiguration()
            join supplier in Context.Suppliers on accountWithConfiguration.AccountConfiguration
                    .SupplierId equals
                supplier.Id
            select new AccountConfigurationBySupplierDataDto
            {
                Account = accountWithConfiguration.Account,
                Supplier = supplier
            };

        /// <summary>
        /// Выполнить маппинг к модели конфигурации аккаунта
        /// </summary>
        /// <param name="accountConfiguration">Конфигурация аккаунта</param>
        /// <returns>Модель конфигурации аккаунта</returns>
        private static AccountConfigurationDto MapToAccountConfigurationDc(AccountConfiguration accountConfiguration) 
            => new()
            {
                    AccountId = accountConfiguration.AccountId,
                    FileStorageId = accountConfiguration.FileStorageId,
                    SegmentId = accountConfiguration.SegmentId,
                    SupplierId = accountConfiguration.SupplierId,
                    LocaleId = accountConfiguration.LocaleId,
                    IsVip = accountConfiguration.IsVip,
                    Type = accountConfiguration.Type,
                    CloudStorageWebLink = accountConfiguration.CloudStorageWebLink
                };
    }
}
