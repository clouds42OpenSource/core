﻿using Clouds42.Domain.DataModels.Notification;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class ConnectionRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Connection>(context, smartTransaction), IConnectionRepository;
}
