﻿using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для поддержки инф. базы
    /// </summary>
    public class AcDbSupportRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AcDbSupport>(context, smartTransaction), IAcDbSupportRepository
    {
        /// <summary>
        /// Установить конфигурацию инф. базы
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <param name="configuration">Конфигурация 1С</param>
        public void SetConfiguration(AcDbSupport acDbSupport, IConfigurations1C configuration)
        {
            Context.AcDbSupports
                .Where(x => x.AccountDatabasesID == acDbSupport.AccountDatabasesID)
                .ExecuteUpdate(x => x.SetProperty(y => y.ConfigurationName, configuration.Name));
           
            Reload(acDbSupport);
        }
    }
}
