﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class ArticleTransactionRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<ArticleTransaction>(context, smartTransaction), IArticleTransactionRepository;
}
