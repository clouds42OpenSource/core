﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class AccountAutoPayConfigurationRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<AccountAutoPayConfiguration>(context, smartTransaction),
            IAccountAutoPayConfigurationRepository
    {
        /// <summary>
        /// Получить по методу оплаты
        /// </summary>
        /// <param name="paymentMethodId">Идентификатор метода оплаты</param>
        public Task<AccountAutoPayConfiguration> GetByPaymentMethodIdAsync(Guid paymentMethodId)
            => DbSet.FirstOrDefaultAsync(aapc => aapc.SavedPaymentMethodId == paymentMethodId);

        /// <summary>
        /// Получить по методу оплаты и аккаунту
        /// </summary>
        /// <param name="accountId"></param>
        public Task<AccountAutoPayConfiguration> GetByAccountIdAsync(Guid accountId)
            => DbSet.FirstOrDefaultAsync(aapc => aapc.AccountId == accountId);

        public AccountAutoPayConfiguration GetByAccountId(Guid accountId)
            => DbSet.FirstOrDefault(aacp => aacp.AccountId == accountId);
    }
}
