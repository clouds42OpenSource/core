﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class CloudServicesEnterpriseServerRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudServicesEnterpriseServer>(context, smartTransaction),
            ICloudServicesEnterpriseServerRepository
    {
        public CloudServicesEnterpriseServer GetEnterpriseServer(Guid cloudServicesEnterpriseServerId)
        {
            return FirstOrDefault(x => x.ID == cloudServicesEnterpriseServerId);
        }
    }
}