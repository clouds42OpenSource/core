﻿using System;
using System.Linq;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CloudServiceRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudService>(context, smartTransaction), ICloudServiceRepository
    {
        public void UpdateCloudService(CloudService cloudService)
        {
            try
            {
                Context.Entry(cloudService).State = EntityState.Modified;
                Context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                var outputLines = new System.Text.StringBuilder();
                foreach (var eve in e.Entries)
                {
                    outputLines.AppendLine(
                        $"{DateTime.Now}: Entity of type \"{eve.Entity.GetType().Name}\" in state \"{eve.State}\" has the following validation errors");
                }

                throw new ValidateException(outputLines.ToString());
            }
           
        }
	    public void InsertCloudService(CloudService cloudService)
	    {
		    try
		    {
			    Context.Entry(cloudService).State =  EntityState.Added;
			    Context.SaveChanges();
		    }
		    catch (DbUpdateException e)
		    {
			    var outputLines = new System.Text.StringBuilder();
			    foreach (var eve in e.Entries)
			    {
				    outputLines.AppendLine(
                        $"{DateTime.Now}: Entity of type \"{eve.Entity.GetType().Name}\" in state \"{eve.State}\" has the following validation errors");
			    }

			    throw new ValidateException(outputLines.ToString());
		    }

	    }

		public IQueryable<string> GetCloudServices()
        {
            return DbSet.Select(x => x.CloudServiceId);
        }
        public int GetCloudServicesCount()
        {
            return DbSet.Count();
        }
        public CloudService GetCloudService(string cloudServiceId)
        {
            return DbSet.FirstOrDefault(x => x.CloudServiceId.ToLower() == cloudServiceId.ToLower());
        }

        public CloudService GetCloudServiceByToken(Guid cloudServiceToken)
        {
            return DbSet.FirstOrDefault(cs => cs.ServiceToken == cloudServiceToken);
        }

        public void DeleteCloudService(CloudService cloudService)
        {
          Context.Entry(cloudService).State = EntityState.Deleted;
          Context.SaveChanges();
          
        }
    }
}
