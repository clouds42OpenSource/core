﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CsResourceRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CsResource>(context, smartTransaction), ICsResourceRepository
    {
        public void InsertCsResource(CsResource csResource)
        {
	        Context.Entry(csResource).State = EntityState.Added;
			Context.SaveChanges();
        }

	    public void UpdateCsResource(CsResource csResource)
	    {
		    Context.Entry(csResource).State = EntityState.Modified;
			Context.SaveChanges();
	    }
		public CsResource GetCsResourceByCloudServiceIdAndResourceName(string cloudServiceId, string resourceName)
        {
            return DbSet.FirstOrDefault(csr => csr.CloudServiceId == cloudServiceId &&
                                                                 csr.ResourcesName == resourceName);
        }
        public CsResource GetCsResourceByResourceName(string resourcesName)
        {
            return DbSet.FirstOrDefault(csr => csr.ResourcesName == resourcesName);
        }
        public CsResource GetCsResourceById(Guid csResourceId)
        {
            return DbSet.FirstOrDefault(csr => csr.Id == csResourceId);
        }
        public CsResource GetCsResourceFromServiceByName(string cloudServiceId, string csResourceName)
        {
            return DbSet.FirstOrDefault(csr => csr.CloudServiceId == cloudServiceId && csr.ResourcesName == csResourceName);
        }
        public List<CsResource> GetCsResources(string cloudServiceId)
        {
            return DbSet.Where(csr => csr.CloudServiceId == cloudServiceId).ToList();
        }
        public void DeleteCsResource(CsResource csResource)
        {
            Context.Entry(csResource).State = EntityState.Deleted;
            Context.SaveChanges();
        }
    }
}
