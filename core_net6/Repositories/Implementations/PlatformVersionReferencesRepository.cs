﻿using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    ///     Основная имплементация IPlatformVersionReferencesRepository
    /// </summary>
    public class PlatformVersionReferencesRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<PlatformVersionReference>(context, smartTransaction), IPlatformVersionReferencesRepository
    {
        /// <inheritdoc />
        /// <summary>
        ///     Получить список стабильных версий 8.2
        /// </summary>
        public IQueryable<string> GetStable82Versions()
        {
            return Context.PlatformVersionReferences.Where(x => x.Version.StartsWith("8.2"))
                .OrderBy(x => x.Version)
                .Select(x => x.Version);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Получить список стабильных версий 8.3
        /// </summary>
        public IQueryable<string> GetStable83Versions()
        {
            return Context.PlatformVersionReferences.Where(x => !x.Version.StartsWith("8.2"))
                .OrderBy(x => x.Version)
                .Select(x => x.Version);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Получить список альфа версий 8.3
        /// </summary>
        public IQueryable<string> GetAlpha83Versions()
        {
            return Context.PlatformVersionReferences.Where(x => !x.Version.StartsWith("8.2"))
                .OrderBy(x => x.Version)
                .Select(x => x.Version);
        }
    }
}
