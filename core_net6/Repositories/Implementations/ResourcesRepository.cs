﻿using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Repositories.Implementations
{
    public class ResourcesRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Resource>(context,
            smartTransaction), IResourcesRepository
    {
        /// <summary>
        /// Получить ресурсы Аренды 1С (Вэб или RDP)
        /// </summary>
        /// <returns>Ресурсы Аренды 1С</returns>
        public IQueryable<Resource> GetRent1CResources() => Context.Resources.Where(r =>
            r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb ||
            r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser);
    }
}
