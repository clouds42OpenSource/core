﻿using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для работы с очередью задач
    /// </summary>
    public class CoreWorkerTasksQueueRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CoreWorkerTasksQueue>(context, smartTransaction), ICoreWorkerTasksQueueRepository;
}
