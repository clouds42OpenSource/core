﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class SavedPaymentMethodBankCardRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<SavedPaymentMethodBankCard>(context, smartTransaction),
            ISavedPaymentMethodBankCardRepository
    {
        public SavedPaymentMethodBankCard GetByPaymentMethodId(Guid paymentMethodId)
            => DbSet.FirstOrDefault(spmb => spmb.SavedPaymentMethodId == paymentMethodId);

        public Task<SavedPaymentMethodBankCard> GetByPaymentMethodIdAsync(Guid paymentMethodId)
            => DbSet.FirstOrDefaultAsync(spmb => spmb.SavedPaymentMethodId == paymentMethodId);
    }
}
