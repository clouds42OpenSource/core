﻿using Clouds42.Domain.DataModels.Notification;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations;

public class NotificationSettingsRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
    : GenericRepository<NotificationSettings>(context, smartTransaction), INotificationSettingsRepository;
