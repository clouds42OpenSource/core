﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class CloudServicesSegmentRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudServicesSegment>(context, smartTransaction), ICloudServicesSegmentRepository
    {
        /// <summary>
        ///     Получить сегмент
        /// </summary>
        /// <param name="segmentId">Идентификатор сегмента</param>
        public CloudServicesSegment GetSegment(Guid segmentId)
        {
            return FirstOrDefault(x => x.ID == segmentId);
        }
    }
}
