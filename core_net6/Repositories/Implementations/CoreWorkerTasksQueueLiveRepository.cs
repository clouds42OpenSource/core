﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    /// <summary>
    /// Репозиторий для работы с очередью задач
    /// </summary>
    public class CoreWorkerTasksQueueLiveRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CoreWorkerTasksQueueLive>(context, smartTransaction), ICoreWorkerTasksQueueLiveRepository
    {
        /// <summary>
        /// Получить все взаимосвязанные задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные задачи</param>
        /// <returns>все взаимосвязанные задачи</returns>
        public async Task<List<CoreWorkerTasksQueueLive>> GetAllDependentTasks(Guid coreWorkerTasksQueueIdFromToSearch)
        {
            var allDependTasks = await DbSet.AsQueryable()
                .OrderBy(x => x.CreateDate)
                .GroupJoin(DbSet.AsQueryable(), z => z.CoreWorkerTasksQueueId, z => z.DependTaskId,
                    (current, depended) => new { current, depended })
                .SelectMany(x => x.depended.DefaultIfEmpty(), (cur, d1) => new { cur.current, d1 })
                .GroupJoin(DbSet.AsQueryable(), z => z.d1.CoreWorkerTasksQueueId, x => x.DependTaskId,
                    (curDep, d2) => new { curDep.current, curDep.d1, d2 })
                .SelectMany(x => x.d2.DefaultIfEmpty(), (x, d2) => new { x.current, x.d1, d2 })
                .GroupJoin(DbSet.AsQueryable(), z => z.d2.CoreWorkerTasksQueueId, x => x.DependTaskId,
                    (curDep, d3) => new { curDep.current, curDep.d1, curDep.d2, d3 })
                .SelectMany(x => x.d3.DefaultIfEmpty(), (x, d3) => new { x.current, x.d1, x.d2, d3 })
                .Where(x => x.current.CoreWorkerTasksQueueId == coreWorkerTasksQueueIdFromToSearch)
                .ToListAsync();


            return allDependTasks
                .SelectMany(x => new List<CoreWorkerTasksQueueLive> { x.current, x.d1, x.d2, x.d3 })
                .Where(x => x != null)
                .DistinctBy(x => x.CoreWorkerTasksQueueId)
                .ToList();
        }

        /// <summary>
        /// Проверяет, все ли задачи завершины начиная с переданной, находит все взаимосвязанные и смотрит на состояние
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные</param>
        /// <returns>Если все состояния в задачах <see cref="CloudTaskQueueStatus.Ready"/> или есть хотя бы одна с <see cref="CloudTaskQueueStatus.Error"/>, то возвращает (<c>true</c>, список взаимосвязанных задач), иначе (<c>false</c>, [])</returns>
        public async Task<ValueTuple<bool, List<CoreWorkerTasksQueueLive>>> GetMultiTaskCompletedInfo(Guid coreWorkerTasksQueueIdFromToSearch)
        {
            var allDependentTasks = await GetAllDependentTasks(coreWorkerTasksQueueIdFromToSearch);
            var readyCount = allDependentTasks.Count(item => item.Status == CloudTaskQueueStatus.Ready);
            var errorCount = allDependentTasks.Count(item => item.Status == CloudTaskQueueStatus.Error);
            var isCompleted = errorCount > 0 || readyCount == allDependentTasks.Count;

            return new ValueTuple<bool, List<CoreWorkerTasksQueueLive>>(isCompleted, allDependentTasks);
        }

        /// <summary>
        /// Проверяет, все ли задачи завершины начиная с переданной, находит все взаимосвязанные и смотрит на состояние, если считается завершённой:(<see cref="GetMultiTaskCompletedInfo"/>), то удаляет все взаимосвязанные задачи
        /// </summary>
        /// <param name="coreWorkerTasksQueueIdFromToSearch">Очередь задачи от которой найти все взаимосвязанные</param>
        public async Task DeleteAllMultiTasksWhenCompleted(Guid coreWorkerTasksQueueIdFromToSearch)
        {
            var (isMultiTaskCompleted, allDependentTasks) = await GetMultiTaskCompletedInfo(coreWorkerTasksQueueIdFromToSearch);

            if (!isMultiTaskCompleted)
            {
                return;
            }

            foreach (var task in allDependentTasks)
            {
                Delete(task);
            }
        }
    }
}
