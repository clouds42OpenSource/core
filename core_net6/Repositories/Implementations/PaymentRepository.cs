﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class PaymentRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<Payment>(context, smartTransaction), IPaymentRepository
    {
        public Payment GetById(Guid id) => DbSet.FirstOrDefault(p => p.Id == id);
        public List<Payment> GetPendingPaymentsByPaymentSystemAndLessSomeDate(string paymentSystem, DateTime date)
            => DbSet
                .Where(p => (p.Date - date).Days == 0 && p.PaymentSystem == paymentSystem && p.Status == PaymentStatus.Waiting.ToString() && !string.IsNullOrEmpty(p.PaymentSystemTransactionId))
                .ToList();

        public Payment GetByPaymentSystemTransactionId(string transactionId)
            => DbSet.FirstOrDefault(p => p.PaymentSystemTransactionId == transactionId);

        public Task<Payment> GetByPaymentSystemTransactionIdAsync(string transactionId)
            => DbSet.FirstOrDefaultAsync(p => p.PaymentSystemTransactionId == transactionId);
        public int PreparePayment(Payment payment)
        {
            payment.Id = Guid.NewGuid();
            DbSet.Add(payment);
            Context.SaveChanges();

            return payment.Number;
        }

        public IEnumerable<Payment> GetPayments(Guid accountId)
        {
            return DbSet.Where(p => p.AccountId == accountId);
        }

        public IQueryable<Payment> GetPayments()
        {
            return DbSet;
        }

        public Payment GetPaymentByExchangeId(Guid exchangeIdentifier)
        {
            return DbSet.FirstOrDefault(p => p.ExchangeDataId == exchangeIdentifier);
        }
        
        public Payment FindPayment(Guid paymentId)
        {
            return DbSet.FirstOrDefault(p => p.Id == paymentId);
        }

        public Payment FindPayment(int number)
        {
            return DbSet.FirstOrDefault(p => p.Number == number);
        }                

        public void RemovePaymentByNumber(int number)
        {
            DbSet.Remove(FindPayment(number));
        }
    }
}
