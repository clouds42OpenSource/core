﻿using System;
using System.Collections.Generic;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class AccountUserRoleRepository(
        Clouds42DbContext context,
        ISmartTransaction smartTransaction)
        : GenericRepository<AccountUserRole>(context, smartTransaction), IAccountUserRoleRepository
    {
        /// <summary>
        /// Получить роли пользователя по ID пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Лист ролей пользователя</returns>
        public ICollection<AccountUserRole> GetUserRoles(Guid accountUserId)
            => DbSet.Where(au => au.AccountUserId == accountUserId).ToList();
        
    }
}