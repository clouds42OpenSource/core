﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.DataModels.billing;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class BillingServiceRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<BillingService>(context, smartTransaction), IBillingServiceRepository
    {
        /// <summary>
        /// Получить список Id аккаунтов
        /// которые используют сервис
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список Id аккаунтов</returns>
        public List<Guid> GetAccountIdsThatUseService(Guid serviceId)
        {
            return DbSet
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.ResourcesConfigurations)
                .Where(x => x.Id == serviceId)
                .SelectMany(x => x.ResourcesConfigurations.Select(z => z.AccountId))
                .Distinct()
                .ToList();
        }
    }
}
