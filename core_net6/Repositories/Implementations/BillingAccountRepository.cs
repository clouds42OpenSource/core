﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class BillingAccountRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<BillingAccount>(context, smartTransaction), IBillingAccountRepository
    {
        /// <summary>
        /// Получить аккаунт биллинга
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт биллинга</returns>
        public BillingAccount GetBillingAccount(Guid accountId)
        {
            return DbSet.FirstOrDefault(acc => acc.Id == accountId);
        }

        /// <inheritdoc/>
        public async Task<BillingAccount> GetBillingAccountOrThrowAsync(Guid accountId)
        {
            var entity = await DbSet.FirstOrDefaultAsync(acc => acc.Id == accountId);
            if (entity == null)
                throw new NotFoundException($"Аккаунт Биллинга с id: {accountId} не найден");
            return entity;
        }

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        public decimal GetBalance(string login, TransactionType transactionType = TransactionType.Money)
        {
            var accountUser = Context.AccountUsers.FirstOrDefault(x => x.Login == login);
            return accountUser != null ? GetBalance(accountUser.AccountId, transactionType) : decimal.Zero;
        }

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        public decimal GetBalance(Guid accountId, TransactionType transactionType = TransactionType.Money)
        {
            var account = GetBillingAccount(accountId);

            if (transactionType == TransactionType.Bonus)
                return account?.BonusBalance ?? 0;

            return account?.Balance ?? 0;
        }

        /// <summary>
        /// Получить список аккаунтов у которых просрочен обещанный платеж.
        /// </summary>
        /// <param name="promisePaymentDays">Кол-во дней активности ОП.</param>        
        public List<BillingAccount> GetAccountsWithExpiredPromise(int promisePaymentDays)
        {
            var now = DateTime.Now;

            var billingAccounts = Context.BillingAccounts.Where(
                b =>
                    b.PromisePaymentSum != null &&
                    b.PromisePaymentSum > 0 &&
                    b.PromisePaymentDate != null &&
                    (now - b.PromisePaymentDate.Value).Days > promisePaymentDays &&
                    !Context.ServiceAccounts.Select(w => w.Id).Contains(b.Id));

            return billingAccounts.ToList();
        }

    }
}
