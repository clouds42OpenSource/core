﻿using System.Linq;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class CloudServicesSegmentStorageRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudServicesSegmentStorage>(context, smartTransaction),
            ICloudServicesSegmentStorageRepository
    {
        public CloudServicesSegmentStorage GetByAccountDatabase(AccountDatabase database)
        {
            return Context.CloudServicesSegmentStorages.FirstOrDefault(x => x.FileStorageID == database.FileStorageID);
        }
    }
}

