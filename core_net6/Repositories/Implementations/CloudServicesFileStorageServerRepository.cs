﻿using System;
using System.Threading.Tasks;
using Clouds42.Domain.DataModels;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Implementations
{
    public class CloudServicesFileStorageServerRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<CloudServicesFileStorageServer>(context, smartTransaction),
            ICloudServicesFileStorageServerRepository
    {
        /// <summary>
        /// Получить данные по файловому хранилищу пользователя.
        /// </summary>
        /// <param name="accountUserId">Номер пользователя.</param>        
        public async Task<CloudServicesFileStorageServer> GetFileStorageData(Guid accountUserId)
        {
            return (await Context.AccountUsers
                .AsQueryable()
                .AsNoTracking()
                .Include(x => x.Account)
                .ThenInclude(x => x.AccountConfiguration)
                .ThenInclude(x => x.FileStorageServer)
                .FirstOrDefaultAsync(x => x.Id == accountUserId))?.Account?.AccountConfiguration?.FileStorageServer;

        }
    }
}
