﻿using Clouds42.Domain.DataModels.Security;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations
{
    public class EmailSmsVerificationRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
        : GenericRepository<EmailSmsVerification>(context, smartTransaction), IEmailSmsVerificationRepository;
}
