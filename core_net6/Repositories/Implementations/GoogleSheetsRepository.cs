﻿using Clouds42.Domain.DataModels.GoogleSheets;
using Clouds42.DomainContext.Context;
using Clouds42.Repositories.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Repositories.Implementations;

public class GoogleSheetsRepository(Clouds42DbContext context, ISmartTransaction smartTransaction)
    : GenericRepository<GoogleSheet>(context, smartTransaction), IGoogleSheetRepository;
