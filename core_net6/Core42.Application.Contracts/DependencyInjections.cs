﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Core42.Application.Contracts
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddCore42ApplicationContracts(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();

            services.AddAutoMapper(assembly);

            return services;
        }
    }
}
