﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Contracts.Features.CloudServiceContext.Dtos;
using MediatR;

namespace Core42.Application.Contracts.Features.CloudServiceContext.Queries
{
    /// <summary>
    /// Get jwt by service id query
    /// </summary>
    public class GetJwtByServiceIdQuery(string id) : IRequest<ManagerResult<CloudServiceJwtDto>>
    {
        /// <summary>
        /// Service id
        /// </summary>
        public string Id { get; set; } = id;
    }
}
