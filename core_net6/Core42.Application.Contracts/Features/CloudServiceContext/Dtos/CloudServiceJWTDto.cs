﻿namespace Core42.Application.Contracts.Features.CloudServiceContext.Dtos
{
    /// <summary>
    /// Clouds service jwt dto
    /// </summary>
    public class CloudServiceJwtDto
    {
        /// <summary>
        /// JWT-token
        /// </summary>
        public string JsonWebToken { get; set; }
    }
}
