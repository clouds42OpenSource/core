﻿using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Contracts.Features.ResourceConfigurationContext.Commands
{
    public class RecalculateResourcesConfigurationCostCommand(
        List<Guid> accountIds,
        List<Guid> billingServiceIds,
        Clouds42Service? billingServiceType)
        : IRequest
    {
        public List<Guid> AccountIds { get; set; } = accountIds;
        public List<Guid> BillingServiceIds { get; set; } = billingServiceIds;
        public Clouds42Service? BillingServiceType { get;set; } = billingServiceType;
    }
}
