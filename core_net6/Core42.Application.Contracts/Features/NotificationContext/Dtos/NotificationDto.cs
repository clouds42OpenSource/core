﻿using AutoMapper;
using Clouds42.Domain.DataModels.Notification;
using MappingNetStandart.Mappings;

namespace Core42.Application.Contracts.Features.NotificationContext.Dtos
{
    public class NotificationDto : IMapFrom<Notification>
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid AccountUserId { get; set; }
        public string Login { get; set; }
        public bool IsRead { get; set; }
        public NotificationState State { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Notification, NotificationDto>()
                .ForMember(x => x.Login, z => z.MapFrom(y => y.AccountUser.Login));
        }
    }
}
