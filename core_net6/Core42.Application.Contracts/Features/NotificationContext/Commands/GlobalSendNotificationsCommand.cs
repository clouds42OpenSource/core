﻿using Clouds42.Common.ManagersResults;
using Clouds42.Domain.DataModels.Notification;
using Core42.Application.Contracts.Features.NotificationContext.Dtos;
using MediatR;

namespace Core42.Application.Contracts.Features.NotificationContext.Commands
{
    public class GlobalSendNotificationsCommand : IRequest<ManagerResult<List<NotificationDto>>>
    {
        public bool IgnoreMailSending { get; set; } = false;
        public List<Guid> UserIds { get; set; }
        public string Message { get; set; }
        public string? Header { get; set; }
        public string? Subject { get; set; }
        public NotificationState State { get; set; } = NotificationState.Information;
        public NotificationSendType NotificationSendType { get; set; } = NotificationSendType.ByUsersIdsList;
    }

    public enum NotificationSendType
    {
        OnlyActiveUsers = 1,
        UsersWithConnectedService,
        OnlyAccountAdminsOrManagers,
        ByUsersIdsList
    }
}
