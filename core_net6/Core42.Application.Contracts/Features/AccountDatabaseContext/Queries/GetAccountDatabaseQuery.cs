﻿using Clouds42.Common.ManagersResults;
using Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos;
using MediatR;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Queries
{
    /// <summary>
    /// Get account database by number query
    /// </summary>
    public class GetAccountDatabaseQuery : IRequest<ManagerResult<AccountDatabaseCardResultDto>>
    {
        /// <summary>
        /// Account database number
        /// </summary>
        public string AccountDatabaseNumber { get; set; }

        public Guid? AccountDatabaseId { get; set; }
    }
}
