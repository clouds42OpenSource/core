﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database card main dto
    /// </summary>
    public class AccountDatabaseCardResultDto
    {
        
        /// <summary>
        /// Account database additional data dto
        /// </summary>
        public AccountDatabaseAdditionalDataDto Database { get; set; }

        /// <summary>
        /// Uri for launch published db
        /// </summary>
        public string LaunchPublishedDbUrl { get; set; }

        /// <summary>
        /// Fields access read model dto
        /// </summary>
        public AccountDatabaseReadModeFieldAccessDto FieldsAccessReadModeInfo { get; set; }

        /// <summary>
        /// Database backups
        /// </summary>
        public List<DatabaseBackupDataDto> Backups { get; set; }

        /// <summary>
        /// Teh support db info
        /// </summary>
        public AccountDatabaseTehSupportInfoDto? TehSupportInfo { get; set; }

        /// <summary>
        /// Db tabs visibility dto
        /// </summary>
        public DatabaseCardTabVisibilityDto TabVisibility { get; set; }

        /// <summary>
        /// Db command visibility dto
        /// </summary>
        public DatabaseCardCommandVisibilityDto CommandVisibility { get; set; }

        /// <summary>
        /// Dictionaries in edit mode db card
        /// </summary>
        public AccountDatabaseDictionariesDto AccountCardEditModeDictionaries { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        ///  Код конфигурации
        /// </summary>
        public string ConfigurationCode { get; set; }
        /// <summary>
        /// Return <c>true</c>, if current auth user has privilegies to edit db <see cref="Database"/>, else <c>false</c>
        /// </summary>
        public bool CanEditDatabase { get; set; }
    }
}
