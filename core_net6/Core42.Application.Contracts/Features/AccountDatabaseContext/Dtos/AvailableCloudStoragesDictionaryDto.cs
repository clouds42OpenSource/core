﻿using AutoMapper;
using Clouds42.BLL.Common;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Available cloud storage key-value dto
    /// </summary>
    public class AvailableCloudStoragesDictionaryDto : DictionaryDto<Guid, string>, IMapFrom<CloudServicesFileStorageServer>
    {
        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<CloudServicesFileStorageServer, AvailableCloudStoragesDictionaryDto>()
                .ForMember(x => x.Key, z => z.MapFrom(x => x.ID))
                .ForMember(x => x.Value, z => z.MapFrom(x => x.Name));
        }
    }
}
