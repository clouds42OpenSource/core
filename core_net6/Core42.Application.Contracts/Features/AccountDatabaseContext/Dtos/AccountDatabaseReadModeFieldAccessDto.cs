﻿namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Модель видимости полей для карточки информационной базы в режиме чтения
    /// </summary>
    public class AccountDatabaseReadModeFieldAccessDto
    {
        /// <summary>
        /// Может ли видить поле пути публикации 
        /// </summary>
        public bool CanShowWebPublishPath { get; set; }

        /// <summary>
        /// Может ли видить поле опубликованы сервисы или нет
        /// </summary>
        public bool CanShowUsedWebServices { get; set; }

        /// <summary>
        /// Может ли видить общую информацию о базе данных
        /// </summary>
        public bool CanShowDatabaseDetails { get; set; }

        /// <summary>
        /// Может ли видить информацию о Sql сервере
        /// </summary>
        public bool CanShowSqlServer { get; set; }

        /// <summary>
        /// Может ли видить информацию о состоянии базы
        /// </summary>
        public bool CanShowDatabaseState { get; set; }

        /// <summary>
        /// Может ли видить информацию о хранилище
        /// </summary>
        public bool CanShowDatabaseFileStorage { get; set; }
    }
}
