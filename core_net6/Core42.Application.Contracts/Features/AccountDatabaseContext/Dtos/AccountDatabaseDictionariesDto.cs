﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database dictionaries dto 
    /// </summary>
    public class AccountDatabaseDictionariesDto
    {
        /// <summary>
        /// Available platform types
        /// </summary>
        public List<PlatformType> AvailablePlatformTypes { get; set; }

        /// <summary>
        /// Available distribution types
        /// </summary>
        public List<KeyValuePair<DistributionType, string>> AvailableDestributionTypes { get; set; }

        /// <summary>
        /// Database states
        /// </summary>
        public List<DatabaseState> AvailableDatabaseStates { get; set; }

        /// <summary>
        /// Available file storages
        /// </summary>
        public List<AvailableCloudStoragesDictionaryDto> AvailableFileStorages { get; set; }

        /// <summary>
        /// Available db templates
        /// </summary>
        public List<KeyValueDto<Guid>> AvailableDatabaseTemplates { get; set; }
    }
}
