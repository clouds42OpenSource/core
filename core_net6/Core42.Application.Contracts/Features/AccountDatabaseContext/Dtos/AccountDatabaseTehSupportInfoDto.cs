﻿using Clouds42.Domain.Enums._1C;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Модель для информации о подключении информационной базы к тех обслуживанию
    /// </summary>
    public class AccountDatabaseTehSupportInfoDto
    {
        /// <summary>
        /// Флаг о том что соединение прошло
        /// </summary>
        public bool IsConnects { get; set; }

        /// <summary>
        /// Последняя дата ТиИ
        /// </summary>
        public DateTime? LastHistoryDate { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public SupportState SupportState { get; set; }
        
        /// <summary>
        /// Описание статуса базы
        /// </summary>
        public string SupportStateDescription { get; set; }

    }
}
