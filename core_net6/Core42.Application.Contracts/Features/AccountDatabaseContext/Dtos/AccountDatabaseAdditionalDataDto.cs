﻿using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;
using MappingNetStandart.Mappings;
using Newtonsoft.Json;

namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Account database additional data dto
    /// </summary>
    public class AccountDatabaseAdditionalDataDto : IMapFrom<AccountDatabase>
    {
        /// <summary>
        /// Is external db flag
        /// </summary>
        public bool IsExternalDb { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Backup date
        /// </summary>
        public DateTime? BackupDate { get; set; }

        /// <summary>
        /// Backup date string
        /// </summary>
        public string BackupDateString { get; set; }

        /// <summary>
        /// Is exist backup path flag
        /// </summary>
        public bool ExistBackUpPath { get; set; }

        /// <summary>
        /// Calculate size date
        /// </summary>
        public DateTime? CalculateSizeDateTime { get; set; }

        /// <summary>
        /// Db template platform enum
        /// </summary>
        [NotMapped]
        public PlatformType TemplatePlatform { get { return TemplatePlatformString.ToPlatformTypeEnum(); } }

        /// <summary>
        /// Db template platform string
        /// </summary>
        [JsonIgnore]
        public string TemplatePlatformString { get; set; }

        /// <summary>
        /// Distibution type enum
        /// </summary>
        [NotMapped]
        public DistributionType DistributionType { get { return DistributionTypeString.ToEnum<DistributionType>(); } }

        /// <summary>
        /// Distibution type string
        /// </summary>
        [JsonIgnore]
        public string DistributionTypeString { get; set; }

        /// <summary>
        /// Stable 82 version
        /// </summary>
        public string Stable82Version { get; set; }

        /// <summary>
        /// Alpha 83 version
        /// </summary>
        public string Alpha83Version { get; set; }

        /// <summary>
        /// Stable 83 version
        /// </summary>
        public string? Stable83Version { get; set; }

        /// <summary>
        /// Db number
        /// </summary>
        public int DbNumber { get; set; }

        /// <summary>
        /// Is  file db flag
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// File path
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Db is ready flag
        /// </summary>
        public bool IsReady { get; set; }

        /// <summary>
        /// Db template 
        /// </summary>
        public string DbTemplate { get; set; }

        /// <summary>
        /// Account caption
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// 82 server
        /// </summary>
        public string V82Server { get; set; }

        /// <summary>
        /// Sql server
        /// </summary>
        public string SqlServer { get; set; }

        /// <summary>
        /// Archive path
        /// </summary>
        public string ArchivePath { get; set; }
        
        /// <summary>
        /// Операция с базой данных (Бекапирование, Архивация и тд)
        /// </summary>
        public List<DatabaseOperation> DatabaseOperations { get; set; }
        
        /// <summary>
        /// Service name
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Locked state
        /// </summary>
        public string LockedState { get; set; }

        /// <summary>
        /// Platform type enum
        /// </summary>
        [NotMapped]
        public PlatformType PlatformType { get { return PlatformTypeString.ToPlatformTypeEnum(); } }

        /// <summary>
        /// Platform type string
        /// </summary>
        [JsonIgnore]
        public string PlatformTypeString { get; set; }

        /// <summary>
        /// Can web publish flag
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Used web services flag
        /// </summary>
        public bool UsedWebServices { get; set; }

        /// <summary>
        /// Cloud storage web link
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// File storage identifier
        /// </summary>
        public Guid? FileStorageId { get; set; }

        /// <summary>
        /// Restore model type
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }

        /// <summary>
        /// Can change restore model flag
        /// </summary>
        public bool CanChangeRestoreModel { get; set; }

        /// <summary>
        /// File storage name
        /// </summary>
        public string FileStorageName { get; set; }

        /// <summary>
        /// Actual db backup identifier
        /// </summary>
        public Guid? ActualDatabaseBackupId { get; set; }

        /// <summary>
        /// Db identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Db 82 name
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Db caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Db size in MB
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Db state enum
        /// </summary>
        [NotMapped]
        public DatabaseState State { get { return DatabaseStateString.ToEnum<DatabaseState>(); } }
        
        /// <summary>
        /// Db state string
        /// </summary>
        [JsonIgnore]
        public string DatabaseStateString { get; set; }

        /// <summary>
        /// Create db comment
        /// </summary>
        public string CreateAccountDatabaseComment { get; set; }

        /// <summary>
        /// Template name
        /// </summary>
        [NotMapped]
        public string TemplateName { get { return !string.IsNullOrEmpty(DefaultTemplateCaption) ? DefaultTemplateCaption : ""; } }

        /// <summary>
        /// Template img uri
        /// </summary>
        public string TemplateImgUrl { get; set; }

        /// <summary>
        /// Publish state enum
        /// </summary>
        public PublishState PublishState { get { return PublishStateString.ToEnum<PublishState>(); } }


        /// <summary>
        /// Publish state string
        /// </summary>
        [JsonIgnore]
        public string PublishStateString { get; set; }

        /// <summary>
        /// Web publish path
        /// </summary>
        public string WebPublishPath { get; set; }

        /// <summary>
        /// Need show web link flag
        /// </summary>
        public bool NeedShowWebLink { get; set; }

        /// <summary>
        /// Last activity date
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Timezone name
        /// </summary>
        public string TimezoneName { get; set; }

        /// <summary>
        /// Нas support flag
        /// </summary>
        public bool HasSupport { get; set; }

        /// <summary>
        /// Has auto update flag
        /// </summary>
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Has AcDb support flag
        /// </summary>
        public bool HasAcDbSupport { get; set; }

        /// <summary>
        /// Is deleted flag
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Is Db on delimiters flag
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Is Demo on delimiters flag
        /// </summary>
        public bool IsDemoDelimiters { get; set; }

        /// <summary>
        /// Zone number if db on delimiters
        /// </summary>
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Comnfiguration name
        /// </summary>
        public string ConfigurationName { get; set; }
        
        /// <summary>
        /// configuration code
        /// </summary>
        public string ConfigurationCode { get; set; }

        /// <summary>
        /// Flag if has modifications
        /// </summary>
        public bool HasModifications { get; set; }

        /// <summary>
        /// Connection path
        /// </summary>
        public string DatabaseConnectionPath { get; set; }

        /// <summary>
        /// My database service type identifier
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }


        /// <summary>
        /// Available file storages
        /// </summary>
        [JsonIgnore]
        public List<AvailableCloudStoragesDictionaryDto> AvailableFileStorages { get; set; }

        /// <summary>
        /// Is exists terminating sessions process in db
        /// </summary>
        public bool IsExistTerminatingSessionsProcessInDatabase { get; set; }

        /// <summary>
        /// Last edit date
        /// </summary>
        public DateTime LastEditedDateTime { get; set; }


        /// <summary>
        /// Defailt template caption
        /// </summary>
        [JsonIgnore]
        public string DefaultTemplateCaption { get; set; }

        /// <summary>
        /// Is create error status 
        /// </summary>
        [NotMapped]
        public bool IsStatusErrorCreated
        {
            get
            {
                return State is DatabaseState.ErrorCreate or DatabaseState.ErrorDtFormat or DatabaseState.ErrorNotEnoughSpace;
            }
        }

        /// <summary>
        /// Can edit db flag
        /// </summary>
        public bool CanEditDatabase { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        public string DbTemplateDelimiterName { get; set; }

        /// <summary>
        /// Common data for working with db
        /// </summary>
        public AdditionalDataForWorkingWithDatabasesDto CommonDataForWorkingWithDB { get; set; }

        /// <summary>
        /// Mapping congiguration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            var errorStates = new List<string> { DatabaseState.ErrorCreate.ToString(), DatabaseState.ErrorDtFormat.ToString(), DatabaseState.ErrorNotEnoughSpace.ToString() };
            var v83Platforms = new List<string> { "8.3", "83", "V83" };

            profile.CreateMap<AccountDatabase, AccountDatabaseAdditionalDataDto>()
                .ForMember(x => x.TemplateImgUrl, z => z.MapFrom(y => y.DbTemplate != null ? y.DbTemplate.ImageUrl : ""))
                .ForMember(x => x.V82Name, z => z.MapFrom(y => y.V82Name))
                .ForMember(x => x.AccountId, z => z.MapFrom(y => y.AccountId))
                .ForMember(x => x.V82Server, z => z.MapFrom(y => errorStates.Contains(y.State) && y.AccountDatabaseOnDelimiter != null && 
                !y.AccountDatabaseOnDelimiter.IsDemo ? string.Empty : !string.IsNullOrEmpty(y.V82Server) ? y.V82Server : y.AccountDatabaseOnDelimiter != null &&
                !y.AccountDatabaseOnDelimiter.IsDemo ? v83Platforms.Contains(y.ApplicationName.Trim()) ? 
                 y.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress : 
                 y.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer82.ConnectionAddress : 
                 v83Platforms.Contains(y.ApplicationName.Trim()) ? y.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer83.ConnectionAddress :
                 y.Account.AccountConfiguration.Segment.CloudServicesEnterpriseServer82.ConnectionAddress))
                .ForMember(x => x.IsFile, z => z.MapFrom(y => y.IsFile))
                .ForMember(x => x.Alpha83Version, z => z.MapFrom(y => y.Account.AccountConfiguration.Segment.Alpha83PlatformVersionReference.Version))
                .ForMember(x => x.Stable82Version, z => z.MapFrom(y => y.Account.AccountConfiguration.Segment.Stable82PlatformVersionReference.Version))
                .ForMember(x => x.PlatformTypeString, z => z.MapFrom(y => y.ApplicationName))
                .ForMember(x => x.DbNumber, z => z.MapFrom(y => y.DbNumber))
                .ForMember(x => x.SqlServer, z => z.MapFrom(y => y.AccountDatabaseOnDelimiter != null
                        ? y.AccountDatabaseOnDelimiter.SourceAccountDatabase.Account.AccountConfiguration.Segment.CloudServicesSQLServer.ConnectionAddress
                        : y.Account.AccountConfiguration.Segment.CloudServicesSQLServer.ConnectionAddress))
                .ForMember(x => x.DbTemplate, z => z.MapFrom(y => y.DbTemplate != null ? y.DbTemplate.Id.ToString() : ""))
                .ForMember(x => x.CanWebPublish, z => z.MapFrom(y => y.DbTemplate != null && y.DbTemplate.CanWebPublish))
                .ForMember(x => x.UsedWebServices, z => z.MapFrom(y => y.UsedWebServices ?? false))
                .ForMember(x => x.FileStorageId, z => z.MapFrom(y => y.FileStorageID))
                .ForMember(x => x.IsDbOnDelimiters, z => z.MapFrom(y => y.AccountDatabaseOnDelimiter != null))
                .ForMember(x => x.IsDemoDelimiters, z => z.MapFrom(y => y.AccountDatabaseOnDelimiter != null && y.AccountDatabaseOnDelimiter.IsDemo))
                .ForMember(x => x.ZoneNumber, z => z.MapFrom(y => y.AccountDatabaseOnDelimiter != null ? y.AccountDatabaseOnDelimiter.Zone : null))
                .ForMember(x => x.RestoreModelType, z => z.MapFrom(y => y.RestoreModelType))
                .ForMember(x => x.CanChangeRestoreModel, z => z.MapFrom(y => y.RestoreModelType == AccountDatabaseRestoreModelTypeEnum.Mixed))
                .ForMember(x => x.Id, z => z.MapFrom(y => y.Id))
                .ForMember(x => x.Caption, z => z.MapFrom(y => y.Caption))
                .ForMember(x => x.Version, z => z.MapFrom(y => y.AcDbSupport.CurrentVersion))
                .ForMember(x => x.ConfigurationName, z => z.MapFrom(y => y.DbTemplate.DefaultCaption == "Чистый шаблон" ? "Своя конфигурация" : y.AcDbSupport.ConfigurationName ?? y.DbTemplate.DefaultCaption))
                .ForMember(x => x.HasModifications, z => z.MapFrom(y => y.AcDbSupport != null && y.AcDbSupport.DatabaseHasModifications))
                .ForMember(x => x.FileStorageName, z => z.MapFrom(y => y.CloudServicesFileStorageServer.Name))
                .ForMember(x => x.AvailableFileStorages, z => z.MapFrom(y => y.Account.AccountConfiguration.Segment.CloudServicesSegmentStorages
                                                                            .Select(ss => ss.CloudServicesFileStorageServer)))
                .ForMember(x => x.CreationDate, z => z.MapFrom(y => y.CreationDate))
                .ForMember(x => x.ActualDatabaseBackupId, z => z.MapFrom(y =>
                                                            y.AccountDatabaseBackups.OrderByDescending(x => x.CreationBackupDateTime).FirstOrDefault() != null ?
                                                            y.AccountDatabaseBackups.OrderByDescending(x => x.CreationBackupDateTime).FirstOrDefault().Id : (Guid?)null))
                 .ForMember(x => x.IsExistTerminatingSessionsProcessInDatabase, z => z.MapFrom(y => y.TerminationDatabaseSessions.Any(v => v.TerminateSessionsInDbStatus == TerminateSessionsInDbStatusEnum.InProcess)))
                 .ForMember(x => x.SizeInMB, z => z.MapFrom(y => y.SizeInMB))
                 .ForMember(x => x.CloudStorageWebLink, z => z.MapFrom(y => y.CloudStorageWebLink))
                 .ForMember(x => x.LastEditedDateTime, z => z.MapFrom(y => y.LastEditedDateTime))
                 .ForMember(x => x.AccountCaption, z => z.MapFrom(y => y.Account.AccountCaption))
                 .ForMember(x => x.BackupDate, z => z.MapFrom(y => y.AccountDatabaseBackups.OrderByDescending(x => x.CreateDateTime).FirstOrDefault() != null ?
                                                            y.AccountDatabaseBackups.OrderByDescending(x => x.CreateDateTime).FirstOrDefault().CreateDateTime : (DateTime?)null))
                 .ForMember(x => x.CalculateSizeDateTime, z => z.MapFrom(y => y.CalculateSizeDateTime))
                 .ForMember(x => x.ArchivePath, z => z.MapFrom(y => y.ArchivePath))
                 .ForMember(x => x.DatabaseStateString, z => z.MapFrom(y => y.State))
                 .ForMember(x => x.DistributionTypeString, z => z.MapFrom(y => y.DistributionType))
                 .ForMember(x => x.PublishStateString, z => z.MapFrom(y => y.PublishState))
                 .ForMember(x => x.DefaultTemplateCaption, m => m.MapFrom(z => z.DbTemplate.DefaultCaption))
                 .ForMember(x => x.ConfigurationCode, m => m.MapFrom(z => z.AccountDatabaseOnDelimiter != null ? z.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode : ""))
                 .ForMember(x => x.DbTemplateDelimiterName, m => m.MapFrom(z => z.AccountDatabaseOnDelimiter != null && z.AccountDatabaseOnDelimiter.DbTemplateDelimiter != null ? z.AccountDatabaseOnDelimiter.DbTemplateDelimiter.Name : null))
                 .ForMember(x => x.TemplatePlatformString, z => z.MapFrom(y => y.DbTemplate.Platform));
        }
    }
}
