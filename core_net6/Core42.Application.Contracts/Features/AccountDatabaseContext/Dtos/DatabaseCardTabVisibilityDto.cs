﻿namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Модель видимости табов в карточке информационной базы
    /// </summary>
    public class DatabaseCardTabVisibilityDto
    {
        /// <summary>
        /// Показывать ли таб с бекапами
        /// </summary>
        public bool IsBackupTabVisible { get; set; }
        
        /// <summary>
        /// Показывать ли таб с подключением базы к ТиИ
        /// </summary>
        public bool IsTehSupportTabVisible { get; set; }
    }
}
