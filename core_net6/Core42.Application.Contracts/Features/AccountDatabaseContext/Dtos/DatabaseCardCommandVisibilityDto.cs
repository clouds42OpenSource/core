﻿namespace Core42.Application.Contracts.Features.AccountDatabaseContext.Dtos
{
    /// <summary>
    /// Модель видимости комманд над базой данных
    /// </summary>
    public class DatabaseCardCommandVisibilityDto
    {
        /// <summary>
        /// Видна ли команда публикации информационной базы
        /// </summary>
        public bool IsPublishDatabaseCommandVisible { get; set; }
        /// <summary>
        /// Видна ли команда отмены публикации информационной базы
        /// </summary>
        public bool IsCancelPublishDatabaseCommandVisible { get; set; }

        /// <summary>
        /// Видна ли команда удаления информационной базы
        /// </summary>
        public bool IsDeleteDatabaseCommandVisible { get; set; }

        /// <summary>
        /// Видна ли информация о публикации информационной базы
        /// </summary>
        public bool IsDatabasePublishingInfoVisible { get; set; }
        /// <summary>
        /// Видна ли информация о отмене публикации информационной базы
        /// </summary>
        public bool IsCanacelingDatabasePublishingInfoVisible { get; set; }

    }
}
