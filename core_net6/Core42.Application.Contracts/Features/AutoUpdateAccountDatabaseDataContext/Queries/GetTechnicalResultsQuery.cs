﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries
{
    /// <summary>
    /// Get technical results query
    /// </summary>
    public class GetTechnicalResultsQuery : IHasFilter<GetTechnicalResultsQueryFilter>, IPagedQuery, ISortedQuery
    {
        /// <summary>
        /// Auto update technical results filter
        /// </summary>
        public GetTechnicalResultsQueryFilter Filter { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set;}

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }

        public string OrderBy { get; set; } = $"{nameof(DatabaseAutoUpdateTechnicalResultDataDto.AutoUpdateStartDate)}.desc";
    }

    public class GetTechnicalResultsQueryFilter : IQueryFilter
    {
        /// <summary>
        /// Id базы
        /// </summary>
        public Guid? AccountDatabaseId { get; set; }

        /// <summary>
        /// Id задачи из очереди
        /// </summary>
        public Guid? CoreWorkerTasksQueueId { get; set; }
    }
}
