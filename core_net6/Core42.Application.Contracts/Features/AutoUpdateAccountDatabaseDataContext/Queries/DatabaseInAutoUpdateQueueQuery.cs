﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums._1C;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries
{
    /// <summary>
    /// Модель фильтра базы которая в очереди на АО
    /// </summary>
    public class DatabaseInAutoUpdateQueueQuery : ISortedQuery, IPagedQuery, IHasFilter<DatabaseInAutoUpdateQueryFilter>, IRequest<ManagerResult<PagedDto<AutoUpdateAccountDatabaseDto>>>
    {
        public string? OrderBy { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public AutoUpdateState? AutoUpdateState { get; set; }
        public DatabaseInAutoUpdateQueryFilter Filter { get; set; }
    }

    public class DatabaseInAutoUpdateQueryFilter: IHasSpecificSearch, IQueryFilter
    {
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchLine { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> Configurations { get; set; }

        /// <summary>
        /// Статус АО
        /// </summary>
        public SupportHistoryState? Status { get; set; }

        /// <summary>
        /// Время начала АО
        /// </summary>
        public DateTime? AutoUpdatePeriodFrom { get; set; }

        /// <summary>
        /// Время завершения АО
        /// </summary>
        public DateTime? AutoUpdatePeriodTo { get; set; }

        private bool? _isNeedAutoUpdateOnly;

        /// <summary>
        /// Только те, которым необходимо авто обновление
        /// </summary>
        public bool? NeedAutoUpdateOnly
        {
            get => _isNeedAutoUpdateOnly;
            set => _isNeedAutoUpdateOnly = value == false ? null : value;
        }

        private bool? _isActiveRent1COnly;

        /// <summary>
        /// Только те, у которых активна Аренда 1С
        /// </summary>
        public bool? IsActiveRent1COnly
        {
            get => _isActiveRent1COnly;
            set => _isActiveRent1COnly = value == false ? null : value;
        }

        private bool? _isVipAccountsOnly;

        /// <summary>
        /// Только вип аккаунты
        /// </summary>
        public bool? IsVipAccountsOnly
        {
            get => _isVipAccountsOnly;
            set => _isVipAccountsOnly = value == false ? null : value;
        }
    }

    public enum AutoUpdateState
    {
        Queued = 1,
        Connected = 2,
        Performed = 3,
        SoloUpdate = 4
    }
}
