﻿using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using LinqExtensionsNetFramework;

namespace Core42.Application.Contracts.Features.AutoUpdateAccountDatabaseDataContext.Queries.Specifications;

public static class AutoUpdateDatabasesSpecification
{
    public static Spec<AutoUpdateAccountDatabaseDto> BySearchLine(string searchLine)
    {
        var searchLineToLower = searchLine?.ToLower();

        return new Spec<AutoUpdateAccountDatabaseDto>(x => string.IsNullOrEmpty(searchLineToLower) ||
                                                           x.V82Name.ToLower()
                                                               .Contains(searchLineToLower) ||
                                                           x.Caption.ToLower()
                                                               .Contains(searchLineToLower) ||
                                                           x.ConfigurationName.ToLower()
                                                               .Contains(searchLineToLower) ||
                                                           x.CurrentVersion.ToLower()
                                                               .Contains(searchLineToLower) ||
                                                           x.ActualVersion.ToLower()
                                                               .Contains(searchLineToLower) ||
                                                           x.PlatformVersion.ToLower()
                                                               .Contains(searchLineToLower));
    }

    public static Spec<AutoUpdateAccountDatabaseDto> ByVersionCompare(bool? needOnlyAutoUpdate)
    {
        return new Spec<AutoUpdateAccountDatabaseDto>(x => !needOnlyAutoUpdate.HasValue || 
                                                            x.CurrentVersionBuild < x.ActualVersionBuild || 
                                                            x.CurrentVersionMajor < x.ActualVersionMajor ||
                                                            x.CurrentVersionMinor < x.ActualVersionMinor ||
                                                            x.CurrentVersionPatch < x.ActualVersionPatch);
    }

    public static Spec<AutoUpdateAccountDatabaseDto> ByConfigurationNames(List<string> configurationNames)
    {
        return new Spec<AutoUpdateAccountDatabaseDto>(x =>
            !configurationNames.Any() || configurationNames.Contains(x.ConfigurationName));

    }

    public static Spec<AcDbSupportHistory> InAutoUpdateStatus()
    {
        return new Spec<AcDbSupportHistory>(x => x.Operation == DatabaseSupportOperation.AutoUpdate &&
                                                 x.AcDbSupport.HasAutoUpdate && x.AcDbSupport.State == 2 &&
                                                 x.AcDbSupport.AccountDatabase.State ==
                                                 DatabaseState.Ready.ToString());
    }

    public static Spec<AcDbSupportHistory> ByDate(DateTime? from, DateTime? to)
    {
        return new Spec<AcDbSupportHistory>(x => (!from.HasValue || x.EditDate >= from.Value) && (!to.HasValue || x.EditDate <= to.Value));
    }

    public static Spec<AcDbSupportHistory> BySupportSearchLine(string searchLine)
    {
        var searchLineToLower = searchLine.ToLower();

        return new Spec<AcDbSupportHistory>(x => string.IsNullOrEmpty(searchLine) ||
                                                 x.AcDbSupport.AccountDatabase.V82Name.ToLower()
                                                     .Contains(searchLineToLower) ||
                                                 x.AcDbSupport.AccountDatabase.Caption.ToLower()
                                                     .Contains(searchLineToLower) ||
                                                 x.AcDbSupport.ConfigurationName.ToLower()
                                                     .Contains(searchLineToLower) ||
                                                 x.Description.ToLower()
                                                     .Contains(searchLineToLower));
    }

    public static Spec<AcDbSupportHistory> BySupportConfigurationNames(List<string> configurationNames)
    {
        return new Spec<AcDbSupportHistory>(x => !configurationNames.Any() || configurationNames.Contains(x.AcDbSupport.ConfigurationName ?? x.AcDbSupport.SynonymConfiguration));
    }

    public static Spec<AcDbSupport> ByNeedDbWithProcessingSupport(bool needDbWithProcessingSupport)
    {
        return new Spec<AcDbSupport>(x => (needDbWithProcessingSupport &&
                                           (x.AccountDatabase.State == DatabaseState.Ready.ToString() ||
                                            x.AccountDatabase.State ==
                                            DatabaseState.ProcessingSupport.ToString())) ||
                                          x.AccountDatabase.State == DatabaseState.Ready.ToString());
    }

    public static Spec<AcDbSupport> ByNeedPreparedForUpdate(bool needPreparedForUpdate)
    {
        return new Spec<AcDbSupport>(x => !needPreparedForUpdate || x.PrepearedForUpdate);
    }
}
