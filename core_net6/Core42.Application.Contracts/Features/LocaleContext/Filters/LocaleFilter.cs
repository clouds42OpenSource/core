﻿using LinqExtensionsNetFramework;

namespace Core42.Application.Contracts.Features.LocaleContext.Filters
{
    /// <summary>
    /// Locale filter
    /// </summary>
    public class LocaleFilter : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Search line
        /// </summary>
        public string? SearchLine { get; set; }
    }
}
