﻿using AutoMapper;
using Clouds42.Domain.DataModels;
using MappingNetStandart.Mappings;

namespace Core42.Application.Contracts.Features.LocaleContext.Dtos
{
    /// <summary>
    /// Locale dto
    /// </summary>
    public class LocaleDto : IMapFrom<Locale>
    {
        /// <summary>
        /// Locale identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Locale name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Locale country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Currency code
        /// </summary>
        public int? CurrencyCode { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Cp site url
        /// </summary>
        public string CpSiteUrl { get; set; }

        /// <summary>
        /// Default segment id
        /// </summary>
        public Guid DefaultSegmentId { get; set; }

        /// <summary>
        /// Mapping configuration
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Locale, LocaleDto>()
                .ForMember(x => x.DefaultSegmentId, z => z.MapFrom(c => c.LocaleConfiguration.DefaultSegmentId))
                .ForMember(x => x.CpSiteUrl, z => z.MapFrom(c => c.LocaleConfiguration.CpSiteUrl));
        }
    }
}
