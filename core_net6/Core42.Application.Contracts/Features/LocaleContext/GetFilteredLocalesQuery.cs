﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;
using Core42.Application.Contracts.Features.LocaleContext.Dtos;
using Core42.Application.Contracts.Features.LocaleContext.Filters;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Core42.Application.Contracts.Features.LocaleContext
{
    /// <summary>
    /// Get filtered locales query
    /// </summary>
    public class GetFilteredLocalesQuery : IRequest<ManagerResult<PagedDto<LocaleDto>>>, IPagedQuery, ISortedQuery, IHasFilter<LocaleFilter>
    {
        /// <summary>
        /// Order by sorting
        /// </summary>
        public string OrderBy { get; set; } = $"{nameof(Locale.Country)}.desc";

        /// <summary>
        /// Locale filter
        /// </summary>
        public LocaleFilter? Filter { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Page size
        /// </summary>
        public int? PageSize { get; set; }
    }
}
