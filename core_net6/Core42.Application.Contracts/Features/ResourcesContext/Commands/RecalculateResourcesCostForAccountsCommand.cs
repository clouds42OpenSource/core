﻿using Clouds42.Domain.Enums;
using MediatR;

namespace Core42.Application.Contracts.Features.ResourcesContext.Commands
{
    public class RecalculateResourcesCostForAccountsCommand: IRequest
    {
        public List<Guid> AccountsIds { get; set; }
        public Guid? BillingServiceChangesId { get; set; }
        public Guid? ServiceTypeId { get; set; }
        public Guid? ServiceId { get; set; }
        public Clouds42Service? ServiceType { get; set; }
        public decimal? Cost { get; set; }
    }
}
