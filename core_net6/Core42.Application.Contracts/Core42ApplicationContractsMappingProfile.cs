﻿using AutoMapper;
using MappingNetStandart.Mappings;

namespace Core42.Application.Contracts
{
    /// <summary>
    /// Core42 Application mappings profiles
    /// </summary>
    public class Core42ApplicationContractsMappingProfile : Profile, IMappingProfile
    {
        /// <summary>
        /// Constuctor, recursive search mappings in current assembly and registry
        /// </summary>
        public Core42ApplicationContractsMappingProfile()
        {
            var self = this as IMappingProfile;
            self.ApplyMappingsFromAssembly(typeof(Core42ApplicationContractsMappingProfile).Assembly);
        }
    }
}
