﻿using System.Collections.Concurrent;
using System.Text;
using Clouds42.DomainContext;
using Clouds42.DomainContext.Context;
using GCP.Extensions.Configuration.SecretManager;
using Google.Api.Gax.ResourceNames;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.SecretManager.V1;
using Google.Protobuf;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Repositories;

namespace PatheCreateServiceToAccount
{
    static class Program
    {
        static async Task Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            //"Запуск патча по добавлению нового сервиса у существующих аккаунтов с русской локалью"


            var configuration = new ConfigurationBuilder()
             .AddJsonFile("appsettings.json", true, true)
                .AddGcpKeyValueSecrets("dev_1_", await GoogleCredential.GetApplicationDefaultAsync())
                .AddEnvironmentVariables()
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<Clouds42DbContext>();
            ;
            if (configuration.GetSection("ConnectionStrings:DatabaseType").Value == ProviderTypeConstants.MsSql)
            {
                optionsBuilder.UseSqlServer(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            else
            {
                optionsBuilder.UseNpgsql(configuration.GetSection("ConnectionStrings:DefaultConnection").Value);
            }

            var uow = new UnitOfWork(new Clouds42DbContext(optionsBuilder.Options));

            var cloudConfigurations = await uow.CloudConfigurationRepository
                .AsQueryableNoTracking()
                .ToListAsync();

            // Create the client.
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "C:\\GoogleCloud\\barakamarket-1682669740387-4e36387af886.json");
            SecretManagerServiceClient client = await SecretManagerServiceClient.CreateAsync();

            // Build the parent project name.
            ProjectName projectName = new ProjectName("barakamarket-1682669740387");

            // Build the secret to create.
            Secret secret = new Secret
            {
                Replication = new Replication
                {
                    Automatic = new Replication.Types.Automatic(),
                },
            };

            BlockingCollection<string> errorKeys = new BlockingCollection<string>();

            await Parallel.ForEachAsync(cloudConfigurations, async (x, token) =>
            {
                if (!string.IsNullOrEmpty(x.Value))
                {
                    var secretId = "dev_1_" + x.Key.Replace(".", "__");

                    if (x.ContextSupplierId.HasValue)
                    {
                        secretId += $"_{x.ContextSupplierId}";
                    }

                    try
                    {
                        Secret createdSecret = await client.CreateSecretAsync(projectName, secretId, secret);

                        // Build a payload.
                        SecretPayload payload = new SecretPayload
                        {
                            Data = ByteString.CopyFrom(x.Value, Encoding.UTF8),
                        };

                        // Add a secret version.
                        SecretVersion createdVersion =
                            await client.AddSecretVersionAsync(createdSecret.SecretName, payload);

                        // Access the secret version.
                        AccessSecretVersionResponse result =
                            await client.AccessSecretVersionAsync(createdVersion.SecretVersionName);

                        string data = result.Payload.Data.ToStringUtf8();
                        Console.WriteLine($"Plaintext: {data}");
                    }

                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine($"Error key {secretId}");
                        errorKeys.Add($"{secretId}:{x.Value}", token);
                    }
                   
                }
            });

            await File.WriteAllTextAsync("errorKeys.txt", string.Join("\n", errorKeys));
        }
    }
}

    
