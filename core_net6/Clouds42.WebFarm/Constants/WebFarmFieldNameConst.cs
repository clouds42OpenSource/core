﻿namespace Clouds42.WebFarm.Constants
{
    /// <summary>
    /// Названия полей в секции веб фермы
    /// </summary>
    public static class WebFarmFieldNameConst
    {
        /// <summary>
        /// Имя фермы
        /// </summary>
        public const string Name = "name";

        /// <summary>
        /// Ферма включена
        /// </summary>
        public const string Enabled = "enabled";

        /// <summary>
        /// Агент авто включение
        /// </summary>
        public const string AutoEnableAgent = "autoEnableAgent";

        /// <summary>
        /// Логин админа
        /// </summary>
        public const string AdminUserName = "adminUserName";

        /// <summary>
        /// Пароль админа
        /// </summary>
        public const string AdminPwd = "adminPassword";

        /// <summary>
        /// Минимальное количество серверов
        /// </summary>
        public const string MinimumServers = "minimumServers";

        /// <summary>
        /// Максимальное количество остановленных серверов
        /// </summary>
        public const string MaximumStoppedServers = "maximumStoppedServers";
    }
}
