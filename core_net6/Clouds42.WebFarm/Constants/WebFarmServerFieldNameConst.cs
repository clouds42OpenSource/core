﻿namespace Clouds42.WebFarm.Constants
{
    /// <summary>
    /// Название полей вэб сервера
    /// </summary>
    public static class WebFarmServerFieldNameConst
    {
        /// <summary>
        /// Адрес
        /// </summary>
        public const string Address = "address";

        /// <summary>
        /// Сервер включен
        /// </summary>
        public const string Enabled = "enabled";
    }
}
