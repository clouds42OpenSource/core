﻿namespace Clouds42.WebFarm.Constants
{
    /// <summary>
    /// Секции в конфиге фермы серверов
    /// </summary>
    public static class FarmServerSectionConst
    {
        /// <summary>
        /// Секция вэб ферм
        /// </summary>
        public const string WebFarmsSection = "webFarms";

        /// <summary>
        /// Секция веб фермы
        /// </summary>
        public const string WebFarmSection = "webFarm";

        /// <summary>
        /// Секция сервера
        /// </summary>
        public const string ServerSection = "server";
    }
}
