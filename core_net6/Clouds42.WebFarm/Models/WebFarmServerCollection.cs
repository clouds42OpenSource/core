﻿using System.Collections.Generic;
using Clouds42.WebFarm.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.WebFarm.Models
{
    /// <summary>
    /// Коллекция серверов фермы
    /// </summary>
    public sealed class WebFarmServerCollection(ConfigurationElementCollection collection)
    {
        /// <summary>
        /// Создать сервера фермы
        /// </summary>
        /// <param name="adresss">Адреса подключения</param>
        /// <returns>Сервера ферм</returns>
        public void CreateWebFarmServers(List<string> adresss)
        {
            adresss.ForEach(a => CreateWebFarmServer(a));
        }

        /// <summary>
        /// Создать новый сервер фермы
        /// </summary>
        /// <returns>Сервер фермы</returns>
        public WebFarmServer CreateWebFarmServer(string adress)
        {
            var existingElement = GetByAddress(adress);

            if (existingElement != null)
                return new WebFarmServer(existingElement);

            var serverElement = collection.CreateElement(FarmServerSectionConst.ServerSection);
            var server = new WebFarmServer(serverElement)
            {
                Address = adress,
                Enabled = true
            };
            collection.Add(serverElement);
            return server;
        }

        /// <summary>
        /// Удалить сервера фермы
        /// </summary>
        /// <param name="addresses">Адреса серверов</param>
        public void RemoveWebFarmServers(List<string> addresses)
        {
            addresses.ForEach(RemoveWebFarmServer);
        }

        /// <summary>
        /// Удалить сервер фермы
        /// </summary>
        /// <param name="address">Адрес сервера</param>
        public void RemoveWebFarmServer(string address)
        {
            var element = GetByAddress(address);
            element?.Delete();
        }

        /// <summary>
        /// Попытаться получить сервер фермы
        /// </summary>
        /// <param name="address">Адрес</param>
        /// <param name="webFarmServer">Сервер фермы</param>
        /// <returns>Результат получения</returns>
        public bool TryGetWebFarmServer(string address, out WebFarmServer webFarmServer)
        {
            var element = GetByAddress(address);

            if (element == null)
            {
                webFarmServer = null;
                return false;
            }

            webFarmServer = new WebFarmServer(element);
            return true;
        }

        /// <summary>
        /// Получить сервер фермы по адресу
        /// </summary>
        /// <param name="address">Адрес</param>
        /// <returns>Элемент сервера</returns>
        private ConfigurationElement GetByAddress(string address)
        {
            foreach (var element in collection)
                if ((string)element[WebFarmServerFieldNameConst.Address] == address)
                    return element;

            return null;
        }
    }
}
