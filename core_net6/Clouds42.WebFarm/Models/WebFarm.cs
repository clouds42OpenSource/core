﻿using Clouds42.WebFarm.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.WebFarm.Models
{
    /// <summary>
    /// Ферма серверов
    /// </summary>
    public sealed class WebFarm
    {
        private readonly ConfigurationElement _farmElement;

        public WebFarm(ConfigurationElement farmElement)
        {
            _farmElement = farmElement;
            WebFarmServers = new WebFarmServerCollection(_farmElement.GetCollection());
        }

        /// <summary>
        /// Сервера фермы
        /// </summary>
        public readonly WebFarmServerCollection WebFarmServers;

        /// <summary>
        /// Имя фермы
        /// </summary>
        public string Name
        {
            get => (string)_farmElement[WebFarmFieldNameConst.Name];
            set => _farmElement[WebFarmFieldNameConst.Name] = value;
        }

        /// <summary>
        /// Ферма включена
        /// </summary>
        public bool Enabled
        {
            get => (bool)_farmElement[WebFarmFieldNameConst.Enabled];
            set => _farmElement[WebFarmFieldNameConst.Enabled] = value;
        }

        /// <summary>
        /// Агент авто включение
        /// </summary>
        public bool AutoEnableAgent
        {
            get => (bool)_farmElement[WebFarmFieldNameConst.AutoEnableAgent];
            set => _farmElement[WebFarmFieldNameConst.AutoEnableAgent] = value;
        }

        /// <summary>
        /// Логин админа
        /// </summary>
        public string AdminUserName
        {
            get => (string)_farmElement[WebFarmFieldNameConst.AdminUserName];
            set => _farmElement[WebFarmFieldNameConst.AdminUserName] = value;
        }

        /// <summary>
        /// Пароль админа
        /// </summary>
        public string AdminPassword
        {
            get => (string)_farmElement[WebFarmFieldNameConst.AdminPwd];
            set => _farmElement[WebFarmFieldNameConst.AdminPwd] = value;
        }

        /// <summary>
        /// Минимальное количество серверов
        /// </summary>
        public int MinimumServers
        {
            get => (int)_farmElement[WebFarmFieldNameConst.MinimumServers];
            set => _farmElement[WebFarmFieldNameConst.MinimumServers] = value;
        }

        /// <summary>
        /// Максимальное количество остановленных серверов
        /// </summary>
        public int MaximumStoppedServers
        {
            get => (int)_farmElement[WebFarmFieldNameConst.MaximumStoppedServers];
            set => _farmElement[WebFarmFieldNameConst.MaximumStoppedServers] = value;
        }
    }
}
