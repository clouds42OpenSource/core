﻿using System.Collections.Generic;
using Clouds42.WebFarm.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.WebFarm.Models
{
    /// <summary>
    /// Коллекция ферм
    /// </summary>
    public sealed class WebFarmCollection(ConfigurationElementCollection collection)
    {
        /// <summary>
        /// Создать новую ферму
        /// </summary>
        public WebFarm CreateNewWebFarm(string webFarmName)
        {
            var existingElement = GetByName(webFarmName);

            if (existingElement != null)
                return new WebFarm(existingElement);

            var webFarmElement = collection.CreateElement(FarmServerSectionConst.WebFarmSection);
            var webFarm = new WebFarm(webFarmElement)
            {
                Name = webFarmName,
                Enabled = true
            };

            collection.Add(webFarmElement);
            return webFarm;
        }

        /// <summary>
        /// Удалить фермы
        /// </summary>
        /// <param name="farmNames">Список названий ферм</param>
        public void RemoveWebFarms(List<string> farmNames)
        {
            farmNames.ForEach(RemoveWebFarm);
        }

        /// <summary>
        /// Удалить ферму
        /// </summary>
        /// <param name="webFarmName">Имя фермы</param>
        public void RemoveWebFarm(string webFarmName)
        {
            var farmElement = GetByName(webFarmName);
            farmElement?.Delete();
        }

        /// <summary>
        /// Попытаться получить ферму
        /// </summary>
        /// <param name="name">Имя фермы</param>
        /// <param name="webFarm">Ферма</param>
        /// <returns>Результат получения</returns>
        public bool TryGetWebFarm(string name, out WebFarm webFarm)
        {
            var farmElement = GetByName(name);

            if (farmElement == null)
            {
                webFarm = null;
                return false;
            }

            webFarm = new WebFarm(farmElement);
            return true;
        }

        /// <summary>
        /// Получить ферму по названию
        /// </summary>
        /// <param name="webFarmName">Имя фермы</param>
        /// <returns>Элемент фермы</returns>
        private ConfigurationElement GetByName(string webFarmName)
        {
            foreach (var element in collection)
                if ((string)element[WebFarmFieldNameConst.Name] == webFarmName)
                    return element;

            return null;
        }
    }
}
