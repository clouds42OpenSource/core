﻿using Clouds42.WebFarm.Constants;
using Microsoft.Web.Administration;

namespace Clouds42.WebFarm.Models
{
    /// <summary>
    /// Сервер фермы
    /// </summary>
    public sealed class WebFarmServer(ConfigurationElement serverElement)
    {
        /// <summary>
        /// Адрес
        /// </summary>
        public string Address
        {
            get => (string)serverElement[WebFarmServerFieldNameConst.Address];
            set => serverElement[WebFarmServerFieldNameConst.Address] = value;
        }

        /// <summary>
        /// Включен
        /// </summary>
        public bool Enabled
        {
            get => (bool)serverElement[WebFarmServerFieldNameConst.Enabled];
            set => serverElement[WebFarmServerFieldNameConst.Enabled] = value;
        }
    }
}
