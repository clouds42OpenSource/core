﻿SELECT
acc.IndexNumber,
acc.Name,
users.Login,
IIF(users.FullName IS NOT NULL, users.FullName, CONCAT(users.LastName, ' ', users.FirstName, ' ', users.MiddleName)) AS FullName,
users.Email AS Email
FROM dbo.Accounts AS acc
INNER JOIN
	(
		SELECT resConf.AccountId
		FROM billing.ResourcesConfigurations AS resConf
		INNER JOIN billing.Services AS ser ON ser.Id = resConf.BillingServiceId
		WHERE ExpireDate > '2020-21-04' AND ExpireDate < '2021-31-12'
		Group By resConf.AccountId
	) AS accByDate ON accByDate.AccountId = acc.Id
INNER JOIN dbo.AccountUsers AS users ON users.AccountId = acc.Id
WHERE users.Removed IS NULL AND users.Activated = '1' AND users.CorpUserSyncStatus != 'Deleted'