﻿SELECT 
DISTINCT u.Login,
IIF(bpUsers.bpCount IS NULL, 0, bpUsers.bpCount) BpCount,
IIF(zupUsers.zupCount IS NULL, 0, zupUsers.zupCount) ZupCount,
IIF(kaUsers.kaCount IS NULL, 0, kaUsers.kaCount) KaCount,
IIF(utUsers.utCount IS NULL, 0, utUsers.utCount) UtCount,
IIF(unfUsers.unfCount IS NULL, 0, unfUsers.unfCount) UnfCount
FROM AccountUsers u
INNER JOIN AcDbAccesses acc on acc.AccountUserID = u.Id
INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
LEFT join (
	SELECT 
	acc.AccountUserID userId, COUNT(acc.AccountUserID) bpCount
	FROM AcDbAccesses acc
	INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
	INNER JOIN DbTemplates t on t.Id = db.TemplateId
	WHERE t.DefaultCaption = 'Бухгалтерия предприятия 3.0'
	GROUP BY acc.AccountUserID
) bpUsers on bpUsers.userId = u.Id
LEFT JOIN (
	SELECT 
	acc.AccountUserID userId, COUNT(acc.AccountUserID) zupCount
	FROM AcDbAccesses acc
	INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
	INNER JOIN DbTemplates t on t.Id = db.TemplateId
	WHERE t.DefaultCaption = 'Зарплата и Управление Персоналом 3.0'
	GROUP BY acc.AccountUserID
) zupUsers on zupUsers.userId = u.Id
LEFT JOIN (
	SELECT 
	acc.AccountUserID userId, COUNT(acc.AccountUserID) kaCount
	FROM AcDbAccesses acc
	INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
	INNER JOIN DbTemplates t on t.Id = db.TemplateId
	WHERE t.DefaultCaption = 'Комплексная автоматизация 2.4'
	GROUP BY acc.AccountUserID
) kaUsers on kaUsers.userId = u.Id
LEFT JOIN (
	SELECT 
	acc.AccountUserID userId, COUNT(acc.AccountUserID) utCount
	FROM AcDbAccesses acc
	INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
	INNER JOIN DbTemplates t on t.Id = db.TemplateId
	WHERE t.DefaultCaption = 'Управление торговлей 11'
	GROUP BY acc.AccountUserID
) utUsers on utUsers.userId = u.Id
LEFT JOIN (
	SELECT 
	acc.AccountUserID userId, COUNT(acc.AccountUserID) unfCount
	FROM AcDbAccesses acc
	INNER JOIN AccountDatabases db on db.Id = acc.AccountDatabaseID
	INNER JOIN DbTemplates t on t.Id = db.TemplateId
	WHERE t.DefaultCaption = 'Управление нашей фирмой 1.6'
	GROUP BY acc.AccountUserID 
) unfUsers on unfUsers.userId = u.Id
WHERE (u.Removed is null or u.Removed = 0) and db.State = 'Ready' 
ORDER BY u.Login