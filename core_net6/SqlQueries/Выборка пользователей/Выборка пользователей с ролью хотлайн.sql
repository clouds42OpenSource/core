﻿select a.IndexNumber, u.Login
from Accounts a 
inner join AccountUsers u  on u.AccountId = a.Id
inner join AccountUserRoles r on r.AccountUserId = u.Id
where r.AccountUserGroup = 5
order by a.IndexNumber