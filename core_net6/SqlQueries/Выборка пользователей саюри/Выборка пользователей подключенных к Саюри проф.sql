﻿declare @accountSaleManagers table(Id uniqueidentifier, Login nvarchar (MAX))

insert into @accountSaleManagers
select sm.AccountId as Id, au.Login 
from AccountSaleManagers sm 
inner join AccountUsers au on au.Id = sm.SaleManagerId

select 
a.IndexNumber, 
au.Login, 
au.Email,
sm.Login as SalesManager,
IIF(r.AccountSponsorId is not null, 'Спонсируется аккаунтом ' + CAST(acc.IndexNumber as nvarchar) , ' ') as Sponsor
from Accounts a
inner join billing.Resources r on r.AccountId = a.Id
inner join AccountUsers au on au.Id = r.Subject
left join @accountSaleManagers sm on sm.Id = a.Id
left join Accounts acc on acc.Id = r.AccountSponsorId
where r.BillingServiceTypeId = 'C621895D-F4B1-4BBD-BC6C-3256E29B3F43'
order by a.IndexNumber