﻿SELECT DISTINCT au.Email
FROM AccountUsers au INNER JOIN
     billing.Resources AS r ON  r.[Subject] = au.Id inner join
	 billing.ServiceTypes st ON st.Id = r.BillingServiceTypeId INNER JOIN
	 billing.[Services] AS s ON s.Id = st.ServiceId
WHERE s.[Name] = 'Sauri' and r.[Subject] is not null