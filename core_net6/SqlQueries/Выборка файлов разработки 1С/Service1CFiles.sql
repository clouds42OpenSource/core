﻿
DECLARE @pathToFileStorageAgents NVARCHAR(MAX)
SELECT @pathToFileStorageAgents = (SELECT Value FROM dbo.CloudConfiguration WHERE [Key]='AgentConfigurations.PathToFileStorageAgents')


SELECT s.Id AS ServiceId, s.[Key] AS ServiceKey, s.Name AS ServiceName, ISNULL(resConf.countUsers, 0) AS CountUsers,
sf.Id AS FileId, sf.OriginalName, configs1C.Configurations1C, CONCAT(@pathToFileStorageAgents, '\', 'company_', CONVERT(NVARCHAR(max), a.IndexNumber), '\', sf.FileName) AS FilePath,
'Файл опубликован' AS FileStatus
FROM billing.BillingService1CFiles sf
INNER JOIN billing.BillingService1CFileVersions ver ON ver.Id = sf.BillingService1CFileVersionId
INNER JOIN billing.Services s ON s.Id = sf.ServiceId
INNER JOIN dbo.Accounts AS a ON a.Id = s.AccountOwnerId
INNER JOIN (
SELECT t.BillingServiceId,
stuff(
(
    SELECT ','+ DbTemplateDelimitersId FROM billing.BillingServiceOfConfigurationDependencies WHERE BillingServiceId = t.BillingServiceId FOR XML PATH('')
),1,1,'') AS Configurations1C 
FROM (SELECT DISTINCT BillingServiceId FROM billing.BillingServiceOfConfigurationDependencies ) t
) AS configs1C ON configs1C.BillingServiceId = s.Id
LEFT JOIN (
SELECT BillingServiceId, COUNT(Id) countUsers
FROM billing.ResourcesConfigurations
GROUP BY BillingServiceId
) resConf ON resConf.BillingServiceId = s.Id




SELECT s.Id AS ServiceId, s.[Key] AS ServiceKey, s.Name AS ServiceName, ISNULL(resConf.countUsers, 0) AS CountUsers,
sf.Id AS FileId, sf.OriginalName, configs1C.Configurations1C, CONCAT(@pathToFileStorageAgents, '\', 'company_', CONVERT(NVARCHAR(max), a.IndexNumber), '\', sf.FileName) AS FilePath,
'Файл на модерации' AS FileStatus
FROM billingCommits.BillingService1CFileChanges sf
INNER JOIN billingCommits.BillingService1CFileVersionChanges ver ON ver.Id = sf.BillingService1CFileVersionChangeId
INNER JOIN billingCommits.BillingServiceChanges sc ON sc.Id = sf.BillingServiceChangesId
INNER JOIN billingCommits.ChangeServiceRequests csr ON csr.BillingServiceChangesId = sc.Id
INNER JOIN billing.Services s ON s.Id = csr.BillingServiceId
INNER JOIN dbo.Accounts AS a ON a.Id = s.AccountOwnerId
INNER JOIN (
SELECT t.BillingServiceChangesId,
stuff(
(
    SELECT ','+ DbTemplateDelimitersId FROM billingCommits.BillingServiceOfConfigurationDependencyChanges WHERE BillingServiceChangesId = t.BillingServiceChangesId FOR XML PATH('')
),1,1,'') AS Configurations1C 
FROM (SELECT DISTINCT BillingServiceChangesId FROM billingCommits.BillingServiceOfConfigurationDependencyChanges ) t
) AS configs1C ON configs1C.BillingServiceChangesId = sc.Id
LEFT JOIN (
SELECT BillingServiceId, COUNT(Id) countUsers
FROM billing.ResourcesConfigurations
GROUP BY BillingServiceId
) resConf ON resConf.BillingServiceId = s.Id
WHERE csr.Status = 1
