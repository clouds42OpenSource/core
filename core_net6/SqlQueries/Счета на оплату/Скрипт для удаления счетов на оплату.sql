﻿declare @invoicesIds table (Id uniqueidentifier)

insert into @invoicesIds 
select Id 
from billing.Invoices 
where Uniq in ('68612',
'68715',
'68626',
'68621',
'68618',
'68614',
'68713',
'68710',
'68709',
'68708',
'68707',
'68705',
'68704',
'68703',
'68702',
'68700',
'68695',
'68693',
'68633',
'68615',
'68631',
'68706',
'68716',
'68712',
'68613',
'68718',
'68637',
'68625',
'68699',
'68696',
'68692',
'68629',
'68636',
'68635',
'68627',
'68622',
'68620',
'68619',
'68617',
'68616',
'68720',
'68719',
'68717',
'68711',
'68701',
'68698',
'68697',
'68630',
'68714',
'68628',
'69122')

delete billing.InvoiceProducts
where InvoiceId in (select *
from @invoicesIds)

delete billing.Invoices
where Id in (select *
from @invoicesIds)