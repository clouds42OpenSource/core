﻿SELECT 
s.Id serviceId,
s.Name serviceName,
f.Id fileId,
f.Name fileName,
f.Synonym fileSynonym,
v.Id versionId,
v.Version1CFile fileVersion
FROM billing.Services s 
INNER JOIN billing.BillingService1CFiles f ON f.ServiceId = s.Id
INNER JOIN billing.Service1CFileVersion v ON v.Id = f.Service1CFileVersionId
WHERE s.Name = 'Delans'