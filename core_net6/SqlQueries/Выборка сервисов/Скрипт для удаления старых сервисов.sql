﻿-- Удаление старых сервисов. Доработать при необходимости с учетом заявок на модерацию. 

BEGIN TRY
BEGIN TRANSACTION 
       
DECLARE @ServiceIds TABLE(Id UNIQUEIDENTIFIER)
DECLARE @ServiceTypeIds TABLE(Id UNIQUEIDENTIFIER)

INSERT @ServiceIds
(
    Id
)
VALUES
('490BD1EA-A7FB-4544-AF34-E1833E2651D8')


INSERT @ServiceTypeIds
(
    Id
)
SELECT DISTINCT st.Id
FROM billing.ServiceTypes AS st
INNER JOIN @ServiceIds AS sr ON sr.Id = st.ServiceId

--Invoices
DELETE ir
FROM billing.InvoiceProducts AS ip
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = ip.ServiceTypeId
INNER JOIN billing.Invoices AS i ON i.Id = ip.InvoiceId
INNER JOIN billing.InvoiceReceipts ir ON ir.InvoiceId = i.Id

DECLARE @deletedInvoiceProductInvoiceIds table (Id UNIQUEIDENTIFIER)
DELETE FROM ip
OUTPUT deleted.InvoiceId into @deletedInvoiceProductInvoiceIds
FROM billing.InvoiceProducts as ip
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = ip.ServiceTypeId

DELETE i
FROM billing.Invoices AS i
INNER JOIN @deletedInvoiceProductInvoiceIds AS diip ON diip.Id = i.Id
--Invoices

--Rates
DELETE r
FROM billing.Rates AS r
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = r.BillingServiceTypeId

DELETE ar
FROM billing.AccountRates AS ar
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = ar.BillingServiceTypeId
--Rates

--ServiceTypeRelations
DELETE sertr
FROM billing.ServiceTypeRelations AS sertr
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = sertr.MainServiceTypeId
--ServiceTypeRelations

--Resources
DELETE r
FROM billing.Resources AS r
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = r.BillingServiceTypeId
--Resources

-- BillingServiceTypeActivityForAccounts
DELETE stActive
FROM billing.BillingServiceTypeActivityForAccounts stActive
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = stActive.BillingServiceTypeId
--

--BillingServiceTypeCostChanges
DELETE serviceTypeCostChanges
FROM history.BillingServiceTypeCostChanges serviceTypeCostChanges
INNER JOIN billingCommits.BillingServiceTypeChanges serviceTypeChanges ON serviceTypeChanges.Id = serviceTypeCostChanges.BillingServiceTypeChangeId
INNER JOIN billingCommits.BillingServiceChanges serviceChanges ON serviceChanges.Id = serviceTypeChanges.BillingServiceChangesId
INNER JOIN billingCommits.ChangeServiceRequests changeServiceRequests ON  changeServiceRequests.BillingServiceChangesId = serviceChanges.Id
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = changeServiceRequests.BillingServiceId

DELETE serviceTypeCostChanges
FROM history.BillingServiceTypeCostChanges serviceTypeCostChanges
INNER JOIN billingCommits.BillingServiceTypeChanges serviceTypeChanges ON serviceTypeChanges.Id = serviceTypeCostChanges.BillingServiceTypeChangeId
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = serviceTypeChanges.BillingServiceTypeId
--BillingServiceTypeCostChanges

--BillingServiceTypeChange
DELETE serviceTypeChanges
FROM billingCommits.BillingServiceTypeChanges serviceTypeChanges
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = serviceTypeChanges.BillingServiceTypeId

DELETE serviceTypeChanges
FROM billingCommits.BillingServiceTypeChanges serviceTypeChanges
INNER JOIN billingCommits.BillingServiceChanges serviceChanges ON serviceChanges.Id = serviceTypeChanges.BillingServiceChangesId
INNER JOIN billingCommits.ChangeServiceRequests changeServiceRequests ON  changeServiceRequests.BillingServiceChangesId = serviceChanges.Id
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = changeServiceRequests.BillingServiceId
--BillingServiceTypeChange

--ServiceTypes
DELETE st
FROM billing.ServiceTypes AS st
INNER JOIN @ServiceTypeIds AS stpId ON stpId.Id = st.Id
--ServiceTypes

--IndustriesDependencyBillingServices
DELETE ids
FROM billing.IndustriesDependencyBillingServices AS ids
INNER JOIN @ServiceIds AS srIds ON srIds.Id=ids.BillingServiceId
--IndustriesDependencyBillingServices

--BillingService1CFiles
DELETE serviceExtension
FROM billing.BillingServiceExtension serviceExtension
INNER JOIN billing.BillingService1CFiles s1CFile ON s1CFile.Name = serviceExtension.Service1CFileName
INNER JOIN @ServiceIds AS srIds ON srIds.Id = s1CFile.ServiceId

DELETE service1CFileDbTemplateRelation
FROM billing.BillingService1CFiles AS s1CFile
INNER JOIN @ServiceIds AS srIds ON srIds.Id = s1CFile.ServiceId
INNER JOIN billing.Service1CFileDbTemplateRelation service1CFileDbTemplateRelation ON service1CFileDbTemplateRelation.BillingService1CFileId = s1CFile.Id

DELETE serviceManager1CFileRelation
FROM billing.BillingService1CFiles AS s1CFile
INNER JOIN @ServiceIds AS srIds ON srIds.Id = s1CFile.ServiceId
INNER JOIN billing.ServiceManager1CFileRelation serviceManager1CFileRelation ON serviceManager1CFileRelation.BillingService1CFileId = s1CFile.Id

DECLARE @deletedService1CFileVersionIds table (Id UNIQUEIDENTIFIER)
DELETE s1CFile
OUTPUT deleted.Service1CFileVersionId into @deletedService1CFileVersionIds
FROM billing.BillingService1CFiles AS s1CFile
INNER JOIN @ServiceIds AS srIds ON srIds.Id = s1CFile.ServiceId

DELETE fileVersionRollback
FROM billing.Service1CFileVersionRollback fileVersionRollback
INNER JOIN @deletedService1CFileVersionIds deletedService1CFileVersionIds ON deletedService1CFileVersionIds.Id = fileVersionRollback.VersionFromId

DELETE fileVersionRollback
FROM billing.Service1CFileVersionRollback fileVersionRollback
INNER JOIN @deletedService1CFileVersionIds deletedService1CFileVersionIds ON deletedService1CFileVersionIds.Id = fileVersionRollback.TargetVersionId

DELETE version1CFile
FROM billing.Service1CFileVersion version1CFile
INNER JOIN @deletedService1CFileVersionIds AS version1CFileIds ON version1CFileIds.Id = version1CFile.Id
--BillingService1CFiles

--ResourcesConfigurations
DELETE rc
FROM billing.ResourcesConfigurations AS rc
INNER JOIN @ServiceIds AS srIds ON srIds.Id = rc.BillingServiceId
--ResourcesConfigurations

--AgentPayments
DECLARE @deletedAgentPaymentAgentPaymentRelationIds table (Id UNIQUEIDENTIFIER)
DELETE FROM ap
OUTPUT deleted.AgentPaymentRelationId into @deletedAgentPaymentAgentPaymentRelationIds
FROM billing.Payments AS p
INNER JOIN billing.AgentPayments AS ap ON ap.ClientPaymentId = p.Id
INNER JOIN @ServiceIds AS srIds ON srIds.Id = p.BillingServiceId
--AgentPayments

--AgentPaymentRelations
DELETE apr
FROM billing.AgentPaymentRelations AS apr
INNER JOIN @deletedAgentPaymentAgentPaymentRelationIds AS agentPaymentRelationIds ON agentPaymentRelationIds.Id = apr.Id
--AgentPaymentRelations

--Payments
DELETE p
FROM billing.Payments AS p
INNER JOIN @ServiceIds AS srIds ON srIds.Id = p.BillingServiceId
--Payments

--ChangeServiceRequests
DELETE changeServiceRequests
FROM billingCommits.ChangeServiceRequests changeServiceRequests
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = changeServiceRequests.BillingServiceId
WHERE changeServiceRequests.BillingServiceChangesId IS NULL

DECLARE @billingServiceChangeIds table (Id UNIQUEIDENTIFIER)
DELETE changeServiceRequests
OUTPUT deleted.BillingServiceChangesId into @billingServiceChangeIds
FROM billingCommits.ChangeServiceRequests changeServiceRequests
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = changeServiceRequests.BillingServiceId
WHERE changeServiceRequests.BillingServiceChangesId IS NOT NULL
--ChangeServiceRequests

--ServiceExtensionDatabase
DELETE serviceExtensionDatabase
FROM dbo.ServiceExtensionDatabases serviceExtensionDatabase
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = serviceExtensionDatabase.ServiceId
--ServiceExtensionDatabase

--BillingServiceScreenshot
DECLARE @screenshotChangesCloudFileIds table (Id UNIQUEIDENTIFIER)
DELETE serviceScreenshotChanges
OUTPUT deleted.CloudFileId into @screenshotChangesCloudFileIds
FROM billingCommits.BillingServiceChanges serviceChanges 
INNER JOIN @billingServiceChangeIds serviceChangeIds ON serviceChangeIds.Id = serviceChanges.Id
INNER JOIN billingCommits.BillingServiceScreenshotChanges serviceScreenshotChanges ON serviceScreenshotChanges.BillingServiceChangesId = serviceChanges.Id

DECLARE @screenshotCloudFileIds table (Id UNIQUEIDENTIFIER)
DELETE serviceScreenshots
OUTPUT deleted.CloudFileId into @screenshotCloudFileIds
FROM billing.Services service
INNER JOIN billing.ServiceScreenshots ON serviceScreenshots.ServiceId = service.Id
INNER JOIN @ServiceIds serviceIds ON serviceIds.Id = service.Id
--BillingServiceScreenshot

--BillingServiceScreenshotCloudFiles
DELETE screenshotChangesCloudFile
FROM dbo.CloudFiles screenshotChangesCloudFile
INNER JOIN @screenshotChangesCloudFileIds screenshotChangesCloudFileIds ON screenshotChangesCloudFileIds.Id = screenshotChangesCloudFile.Id

DELETE screenshotCloudFile
FROM dbo.CloudFiles screenshotCloudFile
INNER JOIN @screenshotCloudFileIds screenshotFileIds ON screenshotFileIds.Id = screenshotCloudFile.Id
--BillingServiceScreenshotCloudFiles

--IndustryDependencyBillingServiceChange
DELETE industryDependencyBillingServiceChanges
FROM billingCommits.IndustryDependencyBillingServiceChanges industryDependencyBillingServiceChanges
INNER JOIN @billingServiceChangeIds serviceChangeIds ON serviceChangeIds.Id = industryDependencyBillingServiceChanges.BillingServiceChangesId
--IndustryDependencyBillingServiceChange

--RecalculationServiceCostAfterChange
DELETE recalculationServiceCost
FROM billing.RecalculationServiceCostAfterChanges recalculationServiceCost
INNER JOIN billingCommits.BillingServiceChanges serviceChanges ON serviceChanges.Id = recalculationServiceCost.BillingServiceChangesId
INNER JOIN @billingServiceChangeIds serviceChangeIds ON serviceChangeIds.Id = serviceChanges.Id
--RecalculationServiceCostAfterChange

--BillingServiceChanges
DECLARE @deletedServiceChangesCloudFiles table (IconCloudFileId UNIQUEIDENTIFIER, InstructionServiceCloudFileId UNIQUEIDENTIFIER)
DELETE serviceChanges
OUTPUT deleted.IconCloudFileId, deleted.InstructionServiceCloudFileId into @deletedServiceChangesCloudFiles
FROM billingCommits.BillingServiceChanges serviceChanges
INNER JOIN @billingServiceChangeIds serviceChangeIds ON serviceChangeIds.Id = serviceChanges.Id
--BillingServiceChanges

--Services
DECLARE @deletedServiceCloudFiles table (IconCloudFileId UNIQUEIDENTIFIER, InstructionServiceCloudFileId UNIQUEIDENTIFIER)
DELETE FROM s
OUTPUT deleted.IconCloudFileId, deleted.InstructionServiceCloudFileId into @deletedServiceCloudFiles
FROM billing.Services AS s
INNER JOIN @ServiceIds AS srIds ON srIds.Id = s.Id
--Services

--IconCloudFile
DELETE cf
FROM dbo.CloudFiles AS cf 
INNER JOIN @deletedServiceCloudFiles AS dsfile ON dsfile.IconCloudFileId = cf.Id

DELETE cf
FROM dbo.CloudFiles AS cf 
INNER JOIN @deletedServiceChangesCloudFiles AS dsfile ON dsfile.IconCloudFileId = cf.Id
--IconCloudFile

--InstructionServiceCloudFile
DELETE cf
FROM dbo.CloudFiles AS cf 
INNER JOIN @deletedServiceCloudFiles AS dsfile ON dsfile.InstructionServiceCloudFileId = cf.Id

DELETE cf
FROM dbo.CloudFiles AS cf 
INNER JOIN @deletedServiceChangesCloudFiles AS dsfile ON dsfile.InstructionServiceCloudFileId = cf.Id
--InstructionServiceCloudFile

COMMIT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
	BEGIN
        ROLLBACK
		SELECT ERROR_MESSAGE() as Msg, ERROR_NUMBER() as Num
	END
END CATCH