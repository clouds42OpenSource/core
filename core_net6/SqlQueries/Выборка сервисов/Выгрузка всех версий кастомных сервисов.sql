﻿SELECT 
f.Name, 
sfv.Version1CFile FileVersion,
(CASE
    WHEN sfv.AuditResult = 0 THEN 'Ожидает аудита'
    WHEN sfv.AuditResult = 1 THEN 'Аудит пройден'
    WHEN sfv.AuditResult = 2 THEN 'Отклонена'
	WHEN sfv.AuditResult = 3 THEN 'Опубликована'
END
) AuditResult,
f.CreationDate FileCreationDate
FROM billing.Services s
INNER JOIN billing.BillingService1CFiles f ON f.ServiceId = s.Id
INNER JOIN billing.Service1CFileVersion sfv ON sfv.Id = f.Service1CFileVersionId
ORDER BY f.CreationDate