﻿-- Запись и расчет данных биллинга 
DECLARE @DbOverLimitPlacementCost  DECIMAL = (
SELECT TOP(1) r.Cost 
FROM billing.Rates r 
INNER JOIN billing.ServiceTypes st ON st.Id = r.BillingServiceTypeId
WHERE st.SystemServiceType = 9)

DECLARE @ServerDbPlacement  DECIMAL = (
SELECT TOP(1) r.Cost 
FROM billing.Rates r 
INNER JOIN billing.ServiceTypes st ON st.Id = r.BillingServiceTypeId
WHERE st.SystemServiceType = 10)

DECLARE @DefaultLimitOnFreeCreationDb INT = (CONVERT(INT, (SELECT Value FROM dbo.CloudConfiguration WHERE [Key]='MyDatabasesService.DefaultLimitOnFreeCreationDb')))

DECLARE @AccountsBillingData TABLE (
AccountId UNIQUEIDENTIFIER, 
LimitOnFreeCreationDb INT NOT NULL, 
DbOverLimitPlacementCost MONEY NOT NULL, 
ServerDbPlacementCost MONEY NOT NULL, 
PRIMARY KEY(AccountId)
)

INSERT @AccountsBillingData
(
    AccountId,
    LimitOnFreeCreationDb,
    DbOverLimitPlacementCost,
    ServerDbPlacementCost
)
SELECT DISTINCT a.Id, 
(CASE WHEN  limitAccount.AccountId IS NULL THEN @DefaultLimitOnFreeCreationDb ELSE limitAccount.LimitedQuantity END) AS LimitOnFreeCreationDb,
(CASE WHEN dbOverLimitPlacementRate.AccountId IS NULL THEN @DbOverLimitPlacementCost ELSE dbOverLimitPlacementRate.Cost END) AS DbOverLimitPlacementCost,
(CASE WHEN serverDbPlacementRate.AccountId IS NULL THEN @ServerDbPlacement ELSE serverDbPlacementRate.Cost END) AS ServerDbPlacementCost
FROM dbo.Accounts a
LEFT JOIN billing.LimitOnFreeCreationDbForAccounts limitAccount ON a.Id = limitAccount.AccountId
LEFT JOIN (SELECT ar.* FROM billing.AccountRates ar 
INNER JOIN billing.ServiceTypes st ON st.Id = ar.BillingServiceTypeId
WHERE st.SystemServiceType = 9) AS dbOverLimitPlacementRate ON dbOverLimitPlacementRate.AccountId = a.Id
LEFT JOIN (SELECT ar.* FROM billing.AccountRates ar 
INNER JOIN billing.ServiceTypes st ON st.Id = ar.BillingServiceTypeId
WHERE st.SystemServiceType = 10) AS serverDbPlacementRate ON serverDbPlacementRate.AccountId = a.Id
-- Запись и расчет данных биллинга 


-- Выбор актуальных значений по базам 
DECLARE @AccountDatabases TABLE (Id UNIQUEIDENTIFIER, AccountId UNIQUEIDENTIFIER, IsServerDb BIT NOT NULL, PRIMARY KEY(Id))
INSERT @AccountDatabases
(
    Id,
    AccountId,
    IsServerDb
)
SELECT db.Id, db.OwnerId, CASE WHEN db.IsFile IS NULL OR db.IsFile = 0 THEN 1 ELSE 0 END 
FROM dbo.AccountDatabases db
WHERE db.State IN ('Ready', 'NewItem', 'ProcessingSupport', 'TransferDb', 'RestoringFromTomb')


DECLARE @MyDatabasesResourcesActualValues TABLE (
AccountId UNIQUEIDENTIFIER, 
DatabasesCount INT NOT NULL, 
ServerDatabasesCount INT NOT NULL, 
PRIMARY KEY(AccountId)
)

INSERT @MyDatabasesResourcesActualValues
(
    AccountId,
    DatabasesCount,
    ServerDatabasesCount
)
SELECT a.Id, groupedDatabases.DatabasesCount, ISNULL(groupedServerDatabases.ServerDatabasesCount, 0) AS ServerDatabasesCount
FROM dbo.Accounts a
INNER JOIN (SELECT db.AccountId, COUNT(db.Id) AS DatabasesCount
FROM @AccountDatabases db
GROUP BY db.AccountId
) AS groupedDatabases ON a.Id = groupedDatabases.AccountId
LEFT JOIN (
SELECT db.AccountId, COUNT(db.Id) AS ServerDatabasesCount
FROM @AccountDatabases db
LEFT JOIN dbo.AccountDatabaseOnDelimiters dbOnDelimiter ON dbOnDelimiter.AccountDatabaseId = db.Id
WHERE db.IsServerDb = 1 AND dbOnDelimiter.AccountDatabaseId IS NULL
GROUP BY db.AccountId
) AS groupedServerDatabases ON a.Id = groupedServerDatabases.AccountId
-- Выбор актуальных значений по базам 


-- Выбор данных по ресурсам для сравнения
DECLARE @ResourcesData TABLE (
AccountId UNIQUEIDENTIFIER,
ServiceTypeName NVARCHAR(MAX) NOT NULL,
ResourceCost MONEY NOT NULL, 
DatabasesCount INT NOT NULL,
LimitOnFreeCreationDb INT NOT NULL,
RateCost MONEY NOT NULL
)

INSERT @ResourcesData
(
    AccountId,
    ServiceTypeName,
    ResourceCost,
    DatabasesCount,
    LimitOnFreeCreationDb,
    RateCost
)
SELECT r.AccountId, st.Name AS ServiceTypeName,
r.Cost ResourceCost,
(CASE 
WHEN st.SystemServiceType = 9 THEN actualValues.DatabasesCount 
ELSE actualValues.ServerDatabasesCount END) AS DatabasesCount,
(CASE WHEN st.SystemServiceType = 9 THEN billingData.LimitOnFreeCreationDb ELSE 0 END) AS LimitOnFreeCreationDb, 
(CASE WHEN st.SystemServiceType = 9 THEN billingData.DbOverLimitPlacementCost ELSE billingData.ServerDbPlacementCost END) AS  RateCost
FROM billing.MyDatabasesResources mr
INNER JOIN billing.Resources r ON r.Id = mr.ResourceId
INNER JOIN @AccountsBillingData billingData ON billingData.AccountId = r.AccountId
INNER JOIN billing.ServiceTypes st ON st.Id = r.BillingServiceTypeId
INNER JOIN @MyDatabasesResourcesActualValues actualValues ON actualValues.AccountId = r.AccountId
-- Выбор данных по ресурсам для сравнения


-- Проверяем базы по количеству
SELECT r.AccountId, st.Name AS ServiceTypeName, 
(CASE 
WHEN st.SystemServiceType = 9 THEN actualValues.DatabasesCount 
ELSE actualValues.ServerDatabasesCount END) AS ShouldBeLikeThis,
mr.ActualDatabasesCount AS Actually
FROM billing.MyDatabasesResources mr
INNER JOIN billing.Resources r ON r.Id = mr.ResourceId
INNER JOIN billing.ServiceTypes st ON st.Id = r.BillingServiceTypeId
INNER JOIN @MyDatabasesResourcesActualValues actualValues ON actualValues.AccountId = r.AccountId
WHERE (st.SystemServiceType = 9 AND mr.ActualDatabasesCount <> actualValues.DatabasesCount) OR (st.SystemServiceType = 10 AND mr.ActualDatabasesCount <> actualValues.ServerDatabasesCount)
-- Проверяем базы по количеству


-- Проверяем стоимость ресурсов
SELECT data.AccountId, data.ServiceTypeName, data.ResourceCost, data.ActualCost 
FROM 
(
SELECT rd.AccountId, rd.ServiceTypeName, rd.ResourceCost,
(CASE WHEN rd.DatabasesCount = 0 OR rd.DatabasesCount <= rd.LimitOnFreeCreationDb THEN 0 ELSE rd.DatabasesCount - rd.LimitOnFreeCreationDb END) * rd.RateCost AS ActualCost
FROM @ResourcesData rd) AS data
WHERE data.ResourceCost <> data.ActualCost
-- Проверяем стоимость ресурсов