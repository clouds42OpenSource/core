﻿select a.IndexNumber, st.Name, r.Cost
from Accounts a
inner join billing.AccountRates r on r.AccountId = a.Id
inner join billing.ServiceTypes st on st.Id = r.BillingServiceTypeId
inner join billing.Services s on s.Id = st.ServiceId
where s.SystemService = 2 and r.Cost = 0
order by a.IndexNumber