﻿UPDATE ad
		SET ad.State='DeletedFromCloud'
		FROM dbo.AccountDatabases ad
		INNER JOIN dbo.AccountDatabaseOnDelimiters delimiterDb ON delimiterDb.AccountDatabaseId = ad.Id
		WHERE delimiterDb.IsDemo = 0 AND ad.State IN ('DelitingToTomb', 'DeletedToTomb') 
		AND (delimiterDb.DatabaseSourceId IS NULL OR delimiterDb.Zone IS NULL)