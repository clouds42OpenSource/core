﻿DECLARE @SaleManagers TABLE (AccountId UNIQUEIDENTIFIER, Login NVARCHAR(MAX))
INSERT INTO @SaleManagers (AccountId, Login)
SELECT sm.AccountId, u.Login  
FROM AccountUsers u 
INNER JOIN AccountSaleManagers sm ON sm.SaleManagerId = u.Id

SELECT 
a.IndexNumber,
IIF(del.AccountDatabaseId IS NOT NULL, CAST(del.Zone AS nvarchar), db.V82Name) AS DbNumber,
db.StateDateTime,
sm.Login
FROM AccountDatabases db
INNER JOIN Accounts a ON a.Id = db.OwnerId 
INNER JOIN @SaleManagers sm ON sm.AccountId = a.Id
LEFT JOIN AccountDatabaseOnDelimiters del ON del.AccountDatabaseId = db.Id
WHERE db.State = 'DeletedFromCloud' AND db.StateDateTime > '2021-09-06T00:00:00.00' AND db.StateDateTime  < '2021-09-13T00:00:00.00'
ORDER BY a.IndexNumber, db.StateDateTime