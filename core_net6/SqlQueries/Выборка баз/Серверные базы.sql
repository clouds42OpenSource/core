﻿declare @inernalUserAccesses table (DatabaseId uniqueidentifier, AccessCount int)
declare @externalUserAccesses table (DatabaseId uniqueidentifier, AccessCount int)

insert into @inernalUserAccesses 
select a.AccountDatabaseID, COUNT(a.AccountID)
from AccountDatabases db
inner join AcDbAccesses a on a.AccountDatabaseID = db.Id
where db.OwnerId = a.AccountID
Group By a.AccountDatabaseID 

insert into @externalUserAccesses 
select a.AccountDatabaseID, COUNT(a.AccountID)
from AccountDatabases db
inner join AcDbAccesses a on a.AccountDatabaseID = db.Id
where db.OwnerId <> a.AccountID
Group By a.AccountDatabaseID

select a.IndexNumber,
IIF(a.IsVip IS NULL or a.IsVip = 0, N'Не ВИП', N'ВИП') VIP,
db.V82Name,
IIF(db.Platform =  '8.2', s.Stable82Version, IIF(db.DistributionType = 'Alpha', s.Alpha83Version, s.Stable83Version)) PlatformVersion,
IIF(db.Platform = '8.2', es2.ConnectionAddress, es.ConnectionAddress) ConnectionAddress,
db.Platform,
db.SizeInMb,
IIF(intAccesses.AccessCount is null, 0, intAccesses.AccessCount) InernalUsers,
IIF(extAccesses.AccessCount is null, 0, extAccesses.AccessCount) ExternalUsers
from CloudServicesEnterpriseServer es 
inner join CloudServicesSegment s on es.ID = s.EnterpriseServer83ID 
inner join CloudServicesEnterpriseServer es2 on es2.ID = s.EnterpriseServer82ID 
inner join Accounts a on a.SegmentID = s.Id
inner join AccountDatabases db on db.OwnerId = a.Id
left join @inernalUserAccesses intAccesses on intAccesses.DatabaseId = db.Id
left join @externalUserAccesses extAccesses on extAccesses.DatabaseId = db.Id
left join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = db.Id
where (db.IsFile is null or db.IsFile = 0) and del.AccountDatabaseId is null and db.State = 'Ready' 
order by a.IndexNumber


