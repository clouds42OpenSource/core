﻿DECLARE @BillingServiceId UNIQUEIDENTIFIER = '2991183C-8E1B-42C1-A782-BDA309B28F67'
DECLARE @Rent1CBillingServiceId  UNIQUEIDENTIFIER = 'E746E1D0-2FBC-4E43-9F12-01A20CDC04F1'

DECLARE @AccountsWithActiveService TABLE 
(
	Id UNIQUEIDENTIFIER, 	
	PRIMARY KEY(Id)
)

DECLARE @BillingServiceConfigs TABLE 
(
	ID UNIQUEIDENTIFIER,
	TemplateCode NVARCHAR(MAX),
	PRIMARY KEY(ID)
)

INSERT @BillingServiceConfigs (ID,TemplateCode)
SELECT NEWID(), DbTemplateDelimitersId FROM billing.BillingServiceOfConfigurationDependencies WHERE BillingServiceId = @BillingServiceId

INSERT @AccountsWithActiveService (Id)
SELECT 
ac.Id
FROM Accounts ac
INNER JOIN billing.ResourcesConfigurations rentConfig ON rentConfig.AccountId = ac.Id AND rentConfig.BillingServiceId = @Rent1CBillingServiceId
INNER JOIN billing.ResourcesConfigurations servConfig ON servConfig.AccountId = ac.Id AND servConfig.BillingServiceId = @BillingServiceId
WHERE rentConfig.ExpireDate > GETDATE() AND (servConfig.ExpireDate IS NULL OR (servConfig.ExpireDate > GETDATE() AND servConfig.IsDemoPeriod = 1))

SELECT 
	acDb.Id,
	acDb.V82Name,
	del.Zone
FROM AccountDatabases acDb
INNER JOIN AccountDatabaseOnDelimiters del on del.AccountDatabaseId = acDb.Id
WHERE 
acDb.State = 'Ready' AND 
acDb.OwnerId IN (
SELECT * FROM @AccountsWithActiveService
) AND
del.IsDemo = 0 AND
del.DbTemplateDelimiterCode IN 
(
SELECT TemplateCode FROM @BillingServiceConfigs
)