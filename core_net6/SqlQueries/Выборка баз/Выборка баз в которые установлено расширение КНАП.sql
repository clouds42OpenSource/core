﻿declare @accountSaleManagers table(Id uniqueidentifier, Login nvarchar (MAX), Division nvarchar (MAX))

insert into @accountSaleManagers
select sm.AccountId as Id, au.Login, sm.Division
from AccountSaleManagers sm 
inner join AccountUsers au on au.Id = sm.SaleManagerId


select 
a.IndexNumber, 
db.V82Name,
accSM.Login,
accSM.Division
from Accounts a
inner join AccountDatabases db on db.OwnerId = a.Id
inner join ServiceExtensionDatabases servExt on servExt.AccountDatabaseId = db.Id
inner join billing.Services s on s.Id = servExt.ServiceId
inner join @accountSaleManagers accSM on accSM.Id = a.Id
where s.Name = 'Интеграция с Кнап' and db.State = 'Ready'
order by IndexNumber