﻿SELECT
acdb.V82Name,
acdb.Caption,
IIF(sup.ConfigurationName IS NOT NULL, sup.ConfigurationName, sup.SynonymConfiguration) AS ConfigurationName,
sup.CurrentVersion
FROM AcDbSupport AS sup
INNER JOIN dbo.AccountDatabases AS acdb ON acdb.Id = sup.AccountDatabasesID
INNER JOIN dbo.Accounts AS acc ON acc.Id = acdb.OwnerId
INNER JOIN (Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
FROM billing.ResourcesConfigurations AS rc
INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
WHERE s.SystemService = 2 AND rc.Frozen = 0) AS resConf ON acDb.OwnerId = resConf.AccountId
where acDb.State = 'Ready' and sup.State = 2 and sup.HasAutoUpdate = 1 AND sup.PrepearedForUpdate = 1
order by acDb.V82Name