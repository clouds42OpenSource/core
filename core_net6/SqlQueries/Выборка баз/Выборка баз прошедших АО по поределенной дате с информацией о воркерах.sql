﻿SELECT
ad.V82Name,
ad.Caption,
IIF(sup.ConfigurationName IS NOT NULL, sup.ConfigurationName, sup.SynonymConfiguration) AS ConfigurationName,
workerInfo.CapturedWorkerId,
workerInfo.WorkerStartTask,
workerInfo.WorkerEndTask
FROM dbo.AcDbSupport AS sup
INNER JOIN dbo.AccountDatabases AS ad ON ad.Id = sup.AccountDatabasesID
INNER JOIN
	(
		SELECT
		cwtq.CreateDate AS WorkerStartTask,
		cwtq.EditDate AS WorkerEndTask,
		SUBSTRING(cwtq.Comment, 26, 36) AS DatabaseId,
		cwtq.CapturedWorkerId
		FROM dbo.CoreWorkerTask AS cwt
		INNER JOIN dbo.CoreWorkerTasksQueue AS cwtq ON cwtq.CoreWorkerTaskId = cwt.ID
		WHERE cwt.TaskName = 'AccountDatabaseAutoUpdateJob'
	) AS workerInfo ON workerInfo.DatabaseId = sup.AccountDatabasesID
WHERE workerInfo.WorkerStartTask > DATEADD(HOUR, 0, '2020-16-07 21:00:00')