﻿select db.V82Name, s.ConnectDate
from AccountDatabases db 
inner join AcDbSupport s on s.AccountDatabasesID = db.Id
where db.State = 'Ready' and s.ConnectDate is not null and s.ConnectDate >= '2020-01-12' and HasAutoUpdate = 1
order by db.V82Name