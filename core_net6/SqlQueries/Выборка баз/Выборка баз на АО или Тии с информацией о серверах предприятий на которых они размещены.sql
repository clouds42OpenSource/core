﻿SELECT
IIF (ad.Platform = '8.3',
	(SELECT cses.Name FROM dbo.CloudServicesEnterpriseServer AS cses WHERE cses.Id = css.EnterpriseServer83ID),
	(SELECT cses.Name FROM dbo.CloudServicesEnterpriseServer AS cses WHERE cses.Id = css.EnterpriseServer82ID)
    ) AS EnterpriseServerName,
a.IndexNumber AS AccountNumber,
ad.V82Name AS DatabaseNumber,
ISNULL(internalAccesses.InternalAccessesToDatabaseCount, 0) AS InternalAccessesToDatabaseCount,
ISNULL(externalAccesses.ExternalAccessesToDatabaseCount, 0) AS ExternalAccessesToDatabaseCount,
sup.Login,
sup.Password
FROM dbo.AcDbSupport AS sup
INNER JOIN dbo.AccountDatabases AS ad ON ad.Id = sup.AccountDatabasesID
INNER JOIN dbo.Accounts AS a ON a.Id = ad.OwnerId
LEFT JOIN(SELECT COUNT(*) AS ExternalAccessesToDatabaseCount, dbAccess.AccountDatabaseID FROM dbo.AcDbAccesses dbAccess
INNER JOIN dbo.AccountDatabases AS ad ON ad.Id = dbAccess.AccountDatabaseID
INNER JOIN dbo.AccountUsers AS au ON au.Id = dbAccess.AccountUserID
WHERE ad.OwnerId <> au.AccountID
GROUP BY dbAccess.AccountDatabaseID
) AS externalAccesses ON externalAccesses.AccountDatabaseID = ad.Id
LEFT JOIN(SELECT COUNT(*) AS InternalAccessesToDatabaseCount, dbAccess.AccountDatabaseID FROM dbo.AcDbAccesses dbAccess
INNER JOIN dbo.AccountDatabases AS ad ON ad.Id = dbAccess.AccountDatabaseID
INNER JOIN dbo.AccountUsers AS au ON au.Id = dbAccess.AccountUserID
WHERE ad.OwnerId = au.AccountID
GROUP BY dbAccess.AccountDatabaseID
) AS internalAccesses ON internalAccesses.AccountDatabaseID = ad.Id
INNER JOIN dbo.CloudServicesSegment AS css ON css.ID = a.SegmentID
WHERE (sup.HasAutoUpdate = 1 OR sup.HasSupport = 1) AND (ad.IsFile IS NULL OR ad.IsFile = 0) AND ad.State = 'Ready' AND sup.State = 2