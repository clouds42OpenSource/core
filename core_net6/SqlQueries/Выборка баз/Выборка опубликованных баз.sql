﻿SELECT
a.IndexNumber AS AccountIndex,
dt.V82Name AS DtBase,
dt.Caption AS DatabaseName,
CASE
	WHEN dt.IsFile IS NOT NULL AND dt.IsFile = 1
    THEN 'Файловая база' ELSE 'Серверная база'
END AS DatabaseType,
csfss.Name AS FileStorage,
cstf.Name AS TerminalFarmName,
CONCAT
	(
		'https://',
		cscs.PublishSiteName,
		'/',
		LOWER(SUBSTRING(CAST(a.Id AS varchar(38)), 25, 12)),
		LOWER(SUBSTRING(CAST(a.Id AS varchar(38)), 20, 4)),
		LOWER(SUBSTRING(CAST(a.Id AS varchar(38)), 15, 4)),
		LOWER(SUBSTRING(CAST(a.Id AS varchar(38)), 10, 4)),
		'/',
		dt.V82Name
	) AS WebLink
FROM dbo.Accounts AS a
INNER JOIN
	(
		SELECT ad.Id, ad.OwnerId, ad.FileStorageID, ad.V82Name, ad.Caption, ad.IsFile
		FROM dbo.AccountDatabases AS ad
		LEFT JOIN dbo.AccountDatabaseOnDelimiters AS adod ON adod.AccountDatabaseId = ad.Id
		WHERE adod.AccountDatabaseId IS NULL AND ad.State = 'Ready' AND ad.PublishState = 'Published'
	) AS dt ON dt.OwnerId = a.Id
LEFT JOIN dbo.CloudServicesFileStorageServers AS csfss ON csfss.ID = dt.FileStorageID
INNER JOIN dbo.CloudServicesSegment AS cds ON cds.ID = a.SegmentID
INNER JOIN dbo.CloudServicesTerminalFarm AS cstf ON cstf.ID = cds.ServicesTerminalFarmID
INNER JOIN dbo.CloudServicesContentServer AS cscs ON cscs.Id = cds.ContentServerID