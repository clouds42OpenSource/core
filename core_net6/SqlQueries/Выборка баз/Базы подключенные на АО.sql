﻿select
acDb.V82Name,
sup.ConfigurationName,
sup.CurrentVersion,
(select
	top 1 hist.EditDate
from AcDbSupportHistory hist
where hist.AccountDatabaseId = acDb.Id and hist.Operation = 0
order by hist.EditDate desc
),
(select
	top 1 hist.EditDate
from AcDbSupportHistory hist
where hist.AccountDatabaseId = acDb.Id and hist.State = 1 and hist.Operation = 0
order by hist.EditDate desc
)
from AcDbSupport sup
inner join AccountDatabases acDb on acDb.Id = sup.AccountDatabasesID
INNER JOIN (Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
FROM billing.ResourcesConfigurations AS rc
INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
WHERE s.SystemService = 2 AND rc.Frozen = 0) AS resConf ON acDb.OwnerId = resConf.AccountId
where acDb.State = 'Ready' and sup.State = 2 and sup.HasAutoUpdate = 1
order by acDb.V82Name