﻿select d.V82Name 
from Accounts a 
inner join AccountDatabases d on d.OwnerId = a.Id
left join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = d.Id
where d.DistributionType = 'Alpha' and a.IsVip = 0 and d.State = 'Ready' and del.AccountDatabaseId is null