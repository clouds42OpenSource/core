﻿select 
db.State,
db.Caption,
db.V82Name,
db.SizeInMB,
db.CreationDate,
db.LastActivityDate,
db.OwnerId
from Accounts a
inner join AccountDatabases db on db.OwnerId = a.Id
where a.IndexNumber = 298 and db.State = 'Ready'