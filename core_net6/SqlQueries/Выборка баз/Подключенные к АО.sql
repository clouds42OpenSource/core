﻿SELECT
supDate.supportDate,
acdb.V82Name,
acdb.Caption,
IIF(sup.ConfigurationName IS NOT NULL, sup.ConfigurationName, sup.SynonymConfiguration) AS ConfigurationName,
sup.CurrentVersion,
IIF(acdb.DistributionType = 'Alpha', css.Alpha83Version, IIF(acdb.Platform = '8.3', css.Stable83Version, css.Stable82Version)) AS ConfigurationVersion
FROM AcDbSupport AS sup
INNER JOIN dbo.AccountDatabases AS acdb ON acdb.Id = sup.AccountDatabasesID
INNER JOIN dbo.Accounts AS acc ON acc.Id = acdb.OwnerId
INNER JOIN dbo.CloudServicesSegment AS css ON css.ID = acc.SegmentID
INNER JOIN
	(
		Select
		cc.Date AS supportDate,	acdb.Id, ROW_NUMBER() OVER (PARTITION BY acdb.Id ORDER BY cc.Date DESC) AS rowNumber
		FROM dbo.CloudChanges AS cc
		INNER JOIN dbo.AccountDatabases AS acdb ON cc.Description = CONCAT('База ', acdb.V82Name, ' подключена к АО')
	) AS supDate ON supDate.Id = acdb.Id AND supDate.rowNumber = 1
INNER JOIN (Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
FROM billing.ResourcesConfigurations AS rc
INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
WHERE s.SystemService = 2 AND rc.Frozen = 0) AS resConf ON acDb.OwnerId = resConf.AccountId
where acDb.State = 'Ready' and sup.State = 2 and sup.HasAutoUpdate = 1
order by acDb.V82Name