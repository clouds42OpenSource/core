﻿select 
db.V82Name,
sup.Login,
sup.Password
from AccountDatabases db 
inner join AcDbSupport sup on sup.AccountDatabasesID = db.Id
where sup.HasAutoUpdate = 1
order by db.V82Name