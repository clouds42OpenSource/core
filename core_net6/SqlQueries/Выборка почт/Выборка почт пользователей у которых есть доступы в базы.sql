﻿select 
distinct u.Email
from AccountUsers u
inner join AcDbAccesses acc on acc.AccountUserID = u.Id
inner join AccountDatabases db on db.Id = acc.AccountDatabaseID
left join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = db.Id
inner join Accounts a on a.Id = db.OwnerId
where del.AccountDatabaseId is null and (u.Removed is null or u.Removed = 0) and db.State = 'Ready' and (a.IsVip is null or a.IsVip = 0) and a.IndexNumber not in ( 2319, 620)
order by u.Email