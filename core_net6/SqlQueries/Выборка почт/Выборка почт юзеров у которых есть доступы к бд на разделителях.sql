﻿select distinct
u.Email
from AccountUsers u 
inner join AcDbAccesses a on a.AccountUserID = u.Id
inner join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = a.AccountDatabaseID
inner join AccountDatabases db on db.Id = del.AccountDatabaseId
inner join DbTemplateDelimiters t on t.ConfigurationId = del.DbTemplateDelimiterCode
where del.Zone is not null and t.ConfigurationId = 'bp' and db.State = 'Ready'