﻿declare @accountDatabases table(Id uniqueidentifier, OwnerId uniqueidentifier)
declare @resourcesConfigurations table (AccountId uniqueidentifier, ExpireDate datetime)
declare @resources table (AccountUserId uniqueidentifier, AccountId uniqueidentifier)

insert into @accountDatabases
select Id, OwnerId
from AccountDatabases db 
left join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = db.Id
where del.AccountDatabaseId is null


insert into @resourcesConfigurations
select AccountId, ExpireDate
from billing.ResourcesConfigurations
where BillingServiceId = 'E746E1D0-2FBC-4E43-9F12-01A20CDC04F1'

insert into @resources
select Subject, AccountId 
from billing.Resources
where BillingServiceTypeId  in ('FCA01AEA-C02D-4D3F-B99C-44A19F11050B', 'B1D98B72-A029-4C30-B373-528C8B5ECD0A')

select distinct au.Email
from Accounts a
inner join AccountUsers au on au.AccountId = a.Id
inner join AccountUserRoles aur on  aur.AccountUserId = au.Id and aur.AccountUserGroup in (1,2)
inner join @accountDatabases db on db.OwnerId = a.Id
inner join AcDbAccesses aa on aa.AccountDatabaseID = db.Id and aa.AccountUserId = au.Id and aa.IsLock = 0 
inner join @resourcesConfigurations rc on rc.AccountId = a.Id 
inner join @resources r on r.AccountId = a.Id and r.AccountUserId = au.Id 
where rc.ExpireDate >= convert(date,  GetDate())  and (a.IsVip is null or a.IsVip = 0)
order by au.Email