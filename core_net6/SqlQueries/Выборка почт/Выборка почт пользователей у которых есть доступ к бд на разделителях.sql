﻿select 
distinct u.Email 
from AccountUsers u 
inner join AcDbAccesses acc on acc.AccountUserID = u.Id 
inner join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = acc.AccountDatabaseID
inner join AccountDatabases db on db.Id = del.AccountDatabaseID
where (u.Removed is null or u.Removed = 0) and db.State = 'Ready' 
order by u.Email 