﻿SELECT
accAdmin.Email AS AccountAdminEmail,
accAdmin.PhoneNumber,
a.IndexNumber AS AccountIndex
FROM dbo.Accounts AS a
INNER JOIN
	(
		SELECT au.Id, au.AccountId, au.Login, au.Email, au.PhoneNumber, IIF(au.FullName IS NOT NULL, au.FullName, CONCAT(au.LastName, ' ', au.FirstName, ' ', au.MiddleName)) AS UserFullName
		FROM dbo.AccountUsers AS au
		INNER JOIN dbo.AccountUserRoles AS aur ON aur.AccountUserId = au.Id
		WHERE aur.AccountUserGroup = 2
	) AS accAdmin ON accAdmin.AccountId = a.Id
INNER JOIN
	(
		Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
		FROM billing.ResourcesConfigurations AS rc
		INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
		WHERE s.SystemService = 2 AND rc.Frozen = 1 AND rc.ExpireDate < DATEADD(MONTH, -2, GETDATE())
	) AS conf1С ON a.Id = Conf1С.AccountId
WHERE a.Id NOT IN (SELECT asm.AccountId FROM dbo.AccountSaleManagers AS asm)