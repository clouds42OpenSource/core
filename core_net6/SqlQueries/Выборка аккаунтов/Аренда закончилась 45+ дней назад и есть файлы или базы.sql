﻿SELECT a.Name, 
a.IndexNumber,
a.MyDiskFilesSizeInMb,
rc.ExpireDate
FROM Accounts AS a INNER JOIN 
	 billing.ResourcesConfigurations AS rc ON rc.AccountId = a.Id 
WHERE rc.BillingServiceId = 'E746E1D0-2FBC-4E43-9F12-01A20CDC04F1' AND rc.ExpireDate < DATEADD(DAY, -45, GETDATE()) AND a.MyDiskFilesSizeInMb > 0