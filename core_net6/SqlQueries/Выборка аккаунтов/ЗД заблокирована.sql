﻿ SELECT DISTINCT au.Email
FROM billing.ResourcesConfigurations rc inner join
     billing.[Services] s ON s.Id = rc.BillingServiceId inner join
	 AccountUsers au ON au.AccountId = rc.AccountId inner join
	 AccountUserRoles r ON r.AccountUserId = au.Id and r.AccountUserGroup = 2
WHERE s.[Name] = 'Fasta' and rc.[ExpireDate] <= getdate() AND rc.Cost > 0