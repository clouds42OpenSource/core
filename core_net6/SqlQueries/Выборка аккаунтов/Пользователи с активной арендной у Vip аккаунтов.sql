﻿SELECT
acc.IndexNumber AS Account,
users.CountUsers As CountUsers
FROM dbo.Accounts acc
INNER JOIN (Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
FROM billing.ResourcesConfigurations AS rc
INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
WHERE s.SystemService = 2 AND rc.Frozen = 0) AS resConf ON acc.Id = resConf.AccountId
INNER JOIN (SELECT COUNT(DISTINCT au.Id) AS CountUsers, au.AccountId
FROM dbo.AccountUsers AS au
INNER JOIN billing.Resources AS r ON r.Subject = au.Id
INNER JOIN billing.ServiceTypes AS st ON st.Id = r.BillingServiceTypeId
Where st.SystemServiceType IN (2,3)
Group by au.AccountId) AS users ON users.AccountId = acc.Id
where acc.IsVip = '1'