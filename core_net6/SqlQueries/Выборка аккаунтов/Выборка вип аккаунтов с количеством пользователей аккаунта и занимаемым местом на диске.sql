﻿SELECT
acc.IndexNumber AS Account,
acc.Name,
users.CountUsers As CountUsers,
ISNULL(CAST(cast(dbSize.AccountDbSize + acc.MyDiskFilesSizeInMb as float) / 1024 AS DECIMAL(10,1)), 0) AS UsedSizeOnDisk
FROM dbo.Accounts acc
	INNER JOIN (Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
				FROM billing.ResourcesConfigurations AS rc
					INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
					WHERE s.SystemService = 2 AND rc.Frozen = 0) AS resConf ON acc.Id = resConf.AccountId
	INNER JOIN (SELECT COUNT(DISTINCT au.Id) AS CountUsers, au.AccountId
				FROM dbo.AccountUsers AS au
					INNER JOIN billing.Resources AS r ON r.Subject = au.Id
					INNER JOIN billing.ServiceTypes AS st ON st.Id = r.BillingServiceTypeId
				Where st.SystemServiceType IN (2,3)
					Group by au.AccountId) AS users ON users.AccountId = acc.Id
	LEFT JOIN (SELECT ISNULL(SUM(db.SizeInMB), 0) AS AccountDbSize, db.OwnerId
                        FROM dbo.AccountDatabases AS db
                        WHERE db.State <> '{DatabaseState.DeletedToTomb.ToString()}' AND
                        db.State <> '{DatabaseState.DelitingToTomb.ToString()}'
                        AND db.State <> '{DatabaseState.DetachingToTomb.ToString()}'
                        GROUP BY db.OwnerId
                        ) AS dbSize ON dbSize.OwnerId = acc.Id
where acc.IsVip = '1'