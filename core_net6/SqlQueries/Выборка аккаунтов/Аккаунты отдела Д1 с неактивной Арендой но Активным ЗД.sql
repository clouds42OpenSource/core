﻿SELECT
accAdmin.Email AS AdminAccountEmail,
a.IndexNumber AccountIndex,
asm.Division AS SaleManagerDivision,
asm.FullName AS SaleManagerFullName,
frs.ScopeValue AS PagesNumber,
ldConf.ExpireDate AS ExpireDate,
payment.Date AS LastPaymentDate
FROM dbo.Accounts AS a
INNER JOIN
	(
		SELECT au.Id, au.AccountId, au.Login, au.Email, IIF(au.FullName IS NOT NULL, au.FullName, CONCAT(au.LastName, ' ', au.FirstName, ' ', au.MiddleName)) AS UserFullName
		FROM dbo.AccountUsers AS au
		INNER JOIN dbo.AccountUserRoles AS aur ON aur.AccountUserId = au.Id
		WHERE aur.AccountUserGroup = 2
	) AS accAdmin ON accAdmin.AccountId = a.Id
INNER JOIN
	(
		SELECT IIF(au.FullName IS NOT NULL, au.FullName, CONCAT(au.LastName, ' ', au.FirstName, ' ', au.MiddleName)) AS FullName, asm.*
		FROM dbo.AccountSaleManagers asm
		INNER JOIN dbo.AccountUsers au on asm.SaleManagerId = au.Id
	) AS asm ON (a.Id = asm.AccountId)
INNER JOIN
	(
		Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod , st.Id AS ServiceTypeId
		FROM billing.ResourcesConfigurations AS rc
		INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
		INNER JOIN billing.ServiceTypes AS st ON st.ServiceId = s.Id
		WHERE s.SystemService = 4 AND rc.Frozen = 0 AND rc.ExpireDate > GETDATE()
	) AS ldConf ON a.Id = ldConf.AccountId
INNER JOIN
	(
		Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
		FROM billing.ResourcesConfigurations AS rc
		INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
		WHERE s.SystemService = 2 AND rc.Frozen = 1
	) AS conf1С ON asm.AccountId = Conf1С.AccountId
INNER JOIN billing.FlowResourcesScope AS frs ON frs.AccountId = asm.AccountId
INNER JOIN
	(
		Select p.Account, p.Date, ROW_NUMBER() OVER (PARTITION BY p.Account ORDER BY p.Date DESC) AS RowNumber
		FROM billing.Payments AS p
		INNER JOIN billing.Services AS s ON s.Id = p.BillingServiceId
		Where s.SystemService = 4
	) AS payment ON payment.Account = a.Id AND payment.RowNumber = 1
WHERE asm.Division like '%Д1.%'
ORDER BY SaleManagerId