﻿select distinct a.IndexNumber 
from Accounts a
inner join AccountDatabases db on db.OwnerId = a.Id
inner join AccountDatabaseOnDelimiters del on del.AccountDatabaseId = db.Id
where a.IsVip = 1
order by a.IndexNumber