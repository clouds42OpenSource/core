﻿SELECT
a.IndexNumber,
a.MyDiskFilesSizeInMb,
a.IsVip,
a.CloudStorageWebLink,
conf1С.ExpireDate
FROM dbo.Accounts as a
INNER JOIN
	(
		Select rc.AccountId, rc.ExpireDate, rc.Frozen, rc.IsDemoPeriod
		FROM billing.ResourcesConfigurations AS rc
		INNER JOIN billing.Services AS s ON s.Id = rc.BillingServiceId
		WHERE s.SystemService = 2 AND rc.Frozen = 1 AND rc.ExpireDate >= DATEADD(DAY, -60, '2020-01-03') AND rc.ExpireDate <= DATEADD(DAY, -60, GETDATE())
	) AS conf1С ON a.Id = Conf1С.AccountId
Order By ExpireDate