﻿select 
ac.IndexNumber as 'Номер аккаунта',
IIF(ac.UserSource is null, 'Не указан', ac.UserSource) as 'Рекламный источник',
CONCAT(monthPayment.PaymentSum, ' ', acLoc.Currency) as 'Сумма ежемесячного платежа',
Concat(smUs.FirstName, ' ', smUs.LastName) as 'Ведущий менеджер',
ac.Type as 'Тип регистрации'
from Accounts ac
inner join Locale acLoc on acLoc.ID = ac.LocaleId
left join AccountSaleManagers sm on sm.AccountId = ac.Id
left join AccountUsers smUs on smUs.Id = sm.SaleManagerId
left join (
select resConf.AccountId, SUM(resConf.Cost) as PaymentSum from billing.ResourcesConfigurations resConf
inner join billing.Services serv on serv.Id = resConf.BillingServiceId
where (serv.SystemService is null or serv.SystemService not in (4, 3)) and resConf.IsDemoPeriod = 0
group by resConf.AccountId
) monthPayment on monthPayment.AccountId = ac.Id
where ac.RegistrationDate BETWEEN '2021-01-07 00:00:00' AND '2021-31-07 23:59:59'
