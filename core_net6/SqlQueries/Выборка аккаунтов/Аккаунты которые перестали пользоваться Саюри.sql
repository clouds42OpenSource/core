﻿declare @accountIds table(Id uniqueidentifier, Number nvarchar(max), Login nvarchar(max))
declare @AccountsMonthlyPayments table(Id uniqueidentifier, Sum money)

insert into @accountIds
Select 
a.Id,
a.IndexNumber,
au.Login
from Accounts a 
inner join AccountSaleManagers asm on asm.AccountId = a.Id
inner join AccountUsers au on au.Id = asm.SaleManagerId
inner join billing.ResourcesConfigurations rc on rc.AccountId = a.Id and rc.BillingServiceId = '55F179AF-548C-42FE-8143-6A8AA94DC9B1'
inner join billing.ResourcesConfigurations rentRc on rentRc.AccountId = a.Id and rentRc.BillingServiceId = 'E746E1D0-2FBC-4E43-9F12-01A20CDC04F1'
where asm.Division like '%Д9%' 
and ((rentRc.IsDemoPeriod = 0 and rentRc.Frozen = 0 and rc.Cost = 0 and rc.IsDemoPeriod = 0 ) or (rentRc.Frozen = 1 and rc.Cost > 0 and rc.IsDemoPeriod = 0))

insert into @AccountsMonthlyPayments
select a.Id, Sum(rc.Cost)
from @accountIds a 
inner join billing.ResourcesConfigurations rc on rc.AccountId = a.Id
where rc.IsDemoPeriod = 0
Group by a.Id

select a.Number, a.Login, ss.Date, m.Sum
from @accountIds a 
inner join (
	select a.Id, p.Date, ROW_NUMBER() over (PARTITION BY a.Id order by p.Date desc) as rowNumber
	from Accounts a 
	inner join  billing.Payments p on p.Account = a.Id
	where p.BillingServiceId = 'E746E1D0-2FBC-4E43-9F12-01A20CDC04F1') ss on a.Id = ss.Id and ss.rowNumber = 1
inner join @AccountsMonthlyPayments m on m.Id = a.ID
order by a.Number

