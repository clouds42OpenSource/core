﻿using Clouds42.HandlerExeption.Contract;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.HandlerException
{
    public static class DependencyInjections
    {
        public static IServiceCollection AddHandlerException(this IServiceCollection services)
        {
            services.AddTransient<IHandlerException, HandlerException>();
            services.AddTransient<IIncidentProvider, IncidentProvider>();

            return services;
        }
    }
}
