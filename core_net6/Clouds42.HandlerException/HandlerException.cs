﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Clouds42.Billing.Contracts.Exceptions;
using Clouds42.Common.Exceptions;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Exceptions;
using Clouds42.TelegramBot;
using Clouds42.TelegramBot.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.HandlerException
{
    /// <summary>
    /// Обработчк исключения.    
    /// </summary>
    public class HandlerException : ErrorHandler, IHandlerException
    {
        private readonly ILogger42 _logger;
        private readonly IIncidentProvider _incidentProvider;
        private readonly IServiceProvider _serviceProvider;
        private readonly ITelegramBot42 _telegramBot42;

        public HandlerException(
            ILogger42 logger,
            IIncidentProvider incidentProvider,
            IEnumerable<ITelegramBot42> telegramBots,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _incidentProvider = incidentProvider;
            _serviceProvider = serviceProvider;
            _telegramBot42 = telegramBots.FirstOrDefault(x => x.Type == BotType.Alert);
        }

        /// <summary>
        /// Базовый метод отправки сообщения об ошибке
        /// </summary>
        /// <param name="exception">ошибка</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        public void Handle(Exception exception, string operationTitle, params Expression<Func<object>>[] parameters)
            => Handle(exception, operationTitle, null, parameters);

        /// <summary>
        /// Базовый метод отправки сообщения об ошибке
        /// </summary>
        /// <param name="exception">ошибка</param>
        /// <param name="incidentMessage">заголовок операции вызвавшей ошибку.</param>
        /// <param name="extraErrorMessage">Дополнителные сведения об ошибке.</param>
        /// <param name="parameters">Входящие параметры.</param>
        public void Handle(Exception exception, string incidentMessage, string extraErrorMessage, params Expression<Func<object>>[] parameters)
        {
            switch (exception)
            {
                case AccessDeniedException ex:
                    var userId = ex.User?.Id ?? Guid.Empty;
                    _logger.Warn(ex, $"Ошибка доступа, у пользователя {userId} нет доступа к {ex.ObjectAction} ");
                    return;
                case CfuDamagedException ex:
                    _logger.Warn(ex, "Поврежден CFU пакет");
                    return;
                case FailedResourceConfigurationErrorsException ex:
                    _logger.Warn(ex, $"Неверный ресурс конфигурации сервиса. Сервис : {ex.ResourcesConfiguration.BillingService.Name}");
                    return;
                case NotFoundException ex:
                    _logger.Warn(ex, "Отсутствует запрашиваемая сущность");
                    return;
                case RequestModelValidationException ex:
                    _logger.Warn(ex, "Модель запроса не валидна");
                    return;
                case NotAuthorizedException ex:
                    _logger.Warn(ex, "Ошибка авторизации");
                    return;
                case TaskNotFoundException ex:
                    _logger.Warn(ex, "Задача не найдена");
                    return;
                case RepositoryException ex:
                    _logger.Error(ex, "Ошибка захвата задачи воркером ");
                    return;
                case DbHasModificationException ex:
                    _logger.Warn(ex, $"База содержит доработки {ex.Message}");
                    return;
                case PreconditionException ex:
                    _logger.Warn(ex, $"Ошибка выполнения условия. {ex.Message}");
                    return;
                case UserNotActivatedException ex:
                    _logger.Warn(ex, "Ошибка актвиации пользователя");
                    return;
                case ValidateException ex:
                    _logger.Warn(ex, "Ошибка валидации");
                    return;
                case ActionExecutionException ex:
                    _logger.Warn(ex, "Ошибка выполнения действия процесса");
                    return;
                case RetryObsoleteProcessFlowException ex:
                    _logger.Warn(ex, "Ошибка перезапуска устаревшего процесса");
                    return;
                case RetryProcessFlowException ex:
                    _logger.Warn(ex, "Ошибка перезапуска ошибочного процесса");
                    return;
                case ClusterAuthenticateException ex:
                    _logger.Warn(ex, "Ошибка авторизации в кластер 1с");
                    return;
                case ClusterConnectionException ex:
                    _logger.Warn(ex, "Ошибка подключения к кластеру 1с");
                    return;
                case WebSocketAuthenticationException ex:
                    _logger.Warn(ex, "Ошибка подключениячерез вебсокет");
                    return;
            }
            _logger.Error(exception, incidentMessage);
            
            if (!_incidentProvider.TryInitializeIncident(ref incidentMessage))
            {
                _logger.Error(exception, $"Не удалось инициализировать инцидент!!! message: {incidentMessage}");
                return;
            }

            using var scope = _serviceProvider.CreateScope();
            var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
            var incident = unitOfWork.IncidentRepository.FirstOrDefault(i => i.Name == incidentMessage);

            if (incident == null)
            {
                _incidentProvider.AddNewIncident(incidentMessage);
                _telegramBot42.SendAlert(incidentMessage);
                return;
            }

            if (incident.DateOfLastDiscover > DateTime.Now.AddHours(-24) && incident.DateOfLastDiscover.Day == DateTime.Now.Day) 
            {
                incident.CountOfIncedentToday++;
                incident.DateOfLastDiscover = DateTime.Now;
                _incidentProvider.UpdateIncident(incident);
                return;
            }

            _telegramBot42.SendAlert(incidentMessage);
            incident.DateOfLastDiscover = DateTime.Now;
            incident.CountOfIncedentToday = 1;
            _incidentProvider.UpdateIncident(incident);
        }

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">Текст предупреждения.</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        public void HandleWarning(string errorMessage, string operationTitle)
            => HandleWarningByEmail(errorMessage, operationTitle);

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="errorMessage">Текст предупреждения.</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        public void HandleWarning(string errorMessage, string operationTitle, params Expression<Func<object>>[] parameters)
            => HandleWarningByEmail(errorMessage, operationTitle, parameters);
    }
}


