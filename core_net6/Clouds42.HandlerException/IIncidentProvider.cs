﻿using Clouds42.Domain.DataModels;

namespace Clouds42.HandlerException
{
    public interface IIncidentProvider
    {
        void AddNewIncident(string incidentName);
        void UpdateIncident(Incident incident);
        bool TryInitializeIncident(ref string message);

    }
}
