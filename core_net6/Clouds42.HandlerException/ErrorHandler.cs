﻿using System;
using System.Linq.Expressions;

namespace Clouds42.HandlerException
{
    /// <summary>
    /// Обработчик ошибок.
    /// </summary>
    public abstract class ErrorHandler
    {

        /// <summary>
        /// Отправка письма ядро предупреждение
        /// </summary>
        /// <param name="warningMsg">полный стек ошибки</param>
        /// <param name="operationTitle">заголовок операции вызвавшей ошибку</param>
        /// <param name="parameters">Входящие параметры.</param>
        protected void HandleWarningByEmail(string warningMsg, string operationTitle, params Expression<Func<object>>[] parameters)
        {
            
        }

    }
}
