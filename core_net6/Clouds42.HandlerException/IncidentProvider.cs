﻿using System;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.HandlerException
{
    public class IncidentProvider(IServiceProvider serviceProvider, ILogger42 logger) : IIncidentProvider
    {
        public bool TryInitializeIncident(ref string message)
        {
            if (!message.Contains('[') || !message.Contains(']'))
                return false;

            var firstIndex = message.IndexOf("[");
            var secondIndex = message.IndexOf("]");

            message = message.Substring(firstIndex + 1, secondIndex - firstIndex - 1);

            return true;
        }


        public void AddNewIncident(string incidentName)
        {
            using var scope = serviceProvider.CreateScope();
            var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

            var incident = new Incident
            {
                Id = Guid.NewGuid(),
                Name = incidentName,
                DateOfLastDiscover = DateTime.Now,
                DateOfDiscover = DateTime.Now,
                CountOfIncedentToday = 1
            };

            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                unitOfWork.IncidentRepository.Insert(incident);
                unitOfWork.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "[Ошибка создания инцидента в базе]");
                transaction.Rollback();
            }
        }

        public void UpdateIncident(Incident incident)
        {
            if (incident == null)
                return;

            using var scope = serviceProvider.CreateScope();
            var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
            using var transaction = unitOfWork.SmartTransaction.Get();
            try
            {
                unitOfWork.IncidentRepository.Update(incident);
                unitOfWork.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "[Ошибка обновления инцидента в базе]");
                transaction.Rollback();
            }
        }
    }

}
