﻿using Clouds42.YookassaAggregator.Providers;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.YookassaAggregator
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddYookassaAgregator(this IServiceCollection services)
        {
            services.AddTransient<ICreateYookassaPaymentObjectProvider, CreateYookassaPaymentObjectProvider>();
            services.AddTransient<IGetYookassaPaymentObjectInfoProvider, GetYookassaPaymentObjectInfoProvider>();
            return services;
        }
    }
}
