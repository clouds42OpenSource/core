﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;
using Clouds42.YookassaAggregator.Сonstants;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.YookassaAggregator.Providers
{
    /// <summary>
    /// Базовый провайдер для работы с объектом платежа в агрегаторе ЮKassa по API
    /// </summary>
    public abstract class YookassaPaymentObjectBaseProvider(IServiceProvider serviceProvider)
    {
        private readonly IHttpClientFactory _httpClientFactory = serviceProvider.GetRequiredService<IHttpClientFactory>();
        private readonly Lazy<string> _urlForWorkingWithPayments = new(CloudConfigurationProvider.YookassaAggregatorPayment.GetUrlForWorkingWithPayments);
        private readonly ILogger42 _logger = serviceProvider.GetRequiredService<ILogger42>();

        /// <summary>
        /// Отправить POST запрос агрегатору ЮKassa
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="json">Тело запроса json</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <param name="idempotencyKey">Ключ идэнтпотентности</param>
        /// <returns>Полученные данные при отправке запроса</returns>
        protected T SendPostRequest<T>(string json, Guid supplierId, string idempotencyKey) where T : class
            => SendRequest<T>(CreatePostRequest(idempotencyKey, json), supplierId);

        protected async Task<T> SendPostRequestAsync<T>(string json, Guid supplierId, string idempotencyKey) where T : class
            => await SendRequestAsync<T>(CreatePostRequest(idempotencyKey, json), supplierId);

        /// <summary>
        /// Отправить GET запрос агрегатору ЮKassa
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="yookassaPaymentId">Id платежа ЮKassa
        /// (будет указан в качестве параметра запроса)</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <param name="idempotencyKey">Ключ идэнтпотентности</param>
        /// <returns>Полученные данные при отправке запроса</returns>
        protected T SendGetRequest<T>(Guid yookassaPaymentId, Guid supplierId, string idempotencyKey) where T : class
            => SendRequest<T>(CreateGetRequest(yookassaPaymentId, idempotencyKey), supplierId);
        
        /// <summary>
        /// Получить тип операции
        /// </summary>
        /// <returns>Тип операции</returns>
        protected abstract string GetOperationType();

        /// <summary>
        /// Отправить GET запрос агрегатору ЮKassa
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="request">Объект запроса</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Полученные данные при отправке запроса</returns>
        private T SendRequest<T>(HttpRequestMessage request, Guid supplierId) where T : class
        {
            try
            {
                var client = _httpClientFactory.CreateClient();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                    "Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(
                            $"{GetStoreId(supplierId)}:{GetSecretKey(supplierId)}")));

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));

                var bodyOrUrl = request.Method == HttpMethod.Post
                    ? request.Content?.ReadAsStringAsync().Result
                    : request.RequestUri?.ToString();

                _logger.Info($"Отправка {request.Method} запроса на Юкассу. Содержимое: {bodyOrUrl}");

                var response = client.Send(request);

                HandleExecutionResult(response);

                var result = response.Content.ReadAsStringAsync().Result;

                _logger.Info($"Получили ответ на {request.Method} запрос на Юкассу. Содержимое: {result}");

                return GetDataFromJson<T>(result);
            }

            catch (Exception ex)
            {
                _logger.Warn(ex, $"[Ошибка отправки {request.Method} запроса агрегатору ЮKassa]");
                throw;
            }
        }

        /// <summary>
        /// Отправить GET запрос агрегатору ЮKassa
        /// </summary>
        /// <typeparam name="T">Тип возвращаемых данных</typeparam>
        /// <param name="request">Объект запроса</param>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Полученные данные при отправке запроса</returns>
        private async Task<T> SendRequestAsync<T>(HttpRequestMessage request, Guid supplierId) where T : class
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));

                client.DefaultRequestHeaders
                        .Authorization = new AuthenticationHeaderValue(
                    "Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(
                        $"{GetStoreId(supplierId)}:{GetSecretKey(supplierId)}")));

                var bodyOrUrl = request.Method == HttpMethod.Post
                    ? await request.Content?.ReadAsStringAsync()!
                    : request.RequestUri?.ToString();

                _logger.Info($"Отправка {request.Method} запроса на Юкассу. Содержимое: {bodyOrUrl}");

                var response = await client.SendAsync(request);

                HandleExecutionResult(response);

                _logger.Info($"Получили ответ на {request.Method} запрос на Юкассу. Содержимое: {response.Content}");

                return GetDataFromJson<T>(await response.Content.ReadAsStringAsync());
            }

            catch (Exception ex)
            {
                _logger.Warn(ex, $"[Ошибка отправки {request.Method} запроса агрегатору ЮKassa]");

                throw;
            }
        }

        /// <summary>
        /// Создать POST запрос
        /// </summary>
        /// <param name="idempotencyKey">Ключ идэнтпотентности</param>
        /// <param name="json">Тело запроса json</param>
        /// <returns>Объект запроса</returns>
        private HttpRequestMessage CreatePostRequest(string idempotencyKey, string json)
        {
            var request = new HttpRequestMessage { Method = HttpMethod.Post, RequestUri = new Uri(_urlForWorkingWithPayments.Value) };
          
            request.Headers.Add(YookassaAggregatorConst.IdempotenceKey, idempotencyKey);
            request.Content = new StringContent(json, Encoding.UTF8, "application/json");
            request.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);

            return request;
        }

        /// <summary>
        /// Создать GET запрос
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа ЮKassa</param>
        /// <param name="idempotencyKey">Ключ идэнтпотентности</param>
        /// <returns>Объект запроса</returns>
        private HttpRequestMessage CreateGetRequest(Guid yookassaPaymentId, string idempotencyKey)
        {
            var url = $"{_urlForWorkingWithPayments.Value}/{yookassaPaymentId}";
            var request = new HttpRequestMessage { Method = HttpMethod.Get, RequestUri = new Uri(url) };

            request.Headers.Add(YookassaAggregatorConst.IdempotenceKey, idempotencyKey);

            return request;
        }

        /// <summary>
        /// Обработать результат выполнения
        /// </summary>
        /// <param name="response">Ответ</param>
        private void HandleExecutionResult(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
                return;

            var error = response.Content.ReadAsStringAsync().Result;

            throw new InvalidOperationException(
                $"""
                 Не удалось выполнить операцию: '{GetOperationType()}'. 
                                         Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, адрес {_urlForWorkingWithPayments.Value}, ошибка : {error}
                 """);
        }

        /// <summary>
        /// Получить данные из json
        /// </summary>
        /// <typeparam name="T">Тип данных</typeparam>
        /// <param name="json">json</param>
        /// <returns>Данные полученные из json</returns>
        private static T GetDataFromJson<T>(string json) where T : class
            => JsonCustomSerializer.DeserializeFromJson<T>(json) ??
               throw new InvalidOperationException($"Не удалось десериализовать json= '{json}' в модель '{nameof(T)}'");

        /// <summary>
        /// Получить Id магазина
        /// </summary>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Id магазина</returns>
        private static string GetStoreId(Guid supplierId) =>
            CloudConfigurationProvider.YookassaAggregatorPayment.GetStoreId(supplierId);

        /// <summary>
        /// Получить секретный ключ
        /// </summary>
        /// <param name="supplierId">Id поставщика</param>
        /// <returns>Секретный ключ</returns>
        private static string GetSecretKey(Guid supplierId) =>
            CloudConfigurationProvider.YookassaAggregatorPayment.GetSecretKey(supplierId);
    }
}
