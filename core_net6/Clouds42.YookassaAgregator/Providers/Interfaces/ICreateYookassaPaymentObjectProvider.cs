﻿using System.Threading.Tasks;
using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.YookassaAggregator.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для создания объекта платежа
    /// в агрегаторе ЮKassa
    /// </summary>
    public interface ICreateYookassaPaymentObjectProvider
    {
        /// <summary>
        /// Создать объект платежа в агрегаторе ЮKassa
        /// </summary>
        /// <param name="creationData">Модель данных для создания объекта платежа в ЮKassa</param>
        /// <returns>Объект платежа ЮKassa</returns>
        YookassaPaymentObjectDto Create(YookassaPaymentObjectCreationDataDto creationData);

        Task<YookassaPaymentObjectDto> CreateQrAsync(YookassaPaymentObjectCreationDataDto creationData);
    }
}
