﻿using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.YookassaAggregator.Providers.Interfaces
{
    /// <summary>
    /// Провайдер для получения информации
    /// об объекте платежа агрегатора ЮKassa
    /// </summary>
    public interface IGetYookassaPaymentObjectInfoProvider
    {
        /// <summary>
        /// Получить информацию об объекте платежа
        /// агрегатора ЮKassa
        /// </summary>
        /// <param name="model">Модель для получения информации
        /// об объекте платежа агрегатора ЮKassa</param>
        /// <returns>Финальный объект платежа Юкасса</returns>
        YookassaFinalPaymentObjectDto GetPaymentObjectInfo(GetYookassaPaymentObjectInfoDto model);
    }
}
