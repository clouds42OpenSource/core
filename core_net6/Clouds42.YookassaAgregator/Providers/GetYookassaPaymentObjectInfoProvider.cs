﻿using System;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.YookassaAggregator.Providers.Interfaces;

namespace Clouds42.YookassaAggregator.Providers
{
    /// <summary>
    /// Провайдер для получения информации
    /// об объекте платежа агрегатора ЮKassa
    /// </summary>
    internal class GetYookassaPaymentObjectInfoProvider(IServiceProvider serviceProvider)
        : YookassaPaymentObjectBaseProvider(serviceProvider),
            IGetYookassaPaymentObjectInfoProvider
    {
        /// <summary>
        /// Получить информацию об объекте платежа
        /// агрегатора ЮKassa
        /// </summary>
        /// <param name="model">Модель для получения информации
        /// об объекте платежа агрегатора ЮKassa</param>
        /// <returns>Финальный объект платежа Юкасса</returns>
        public YookassaFinalPaymentObjectDto GetPaymentObjectInfo(GetYookassaPaymentObjectInfoDto model) =>
            SendGetRequest<YookassaFinalPaymentObjectDto>(model.YookassaPaymentId, model.SupplierId,
                model.IdempotenceKey);

        /// <summary>
        /// Получить тип операции
        /// </summary>
        /// <returns>Тип операции</returns>
        protected override string GetOperationType() => "Получение информации об объекте платежа агрегатора ЮKassa";
    }
}
