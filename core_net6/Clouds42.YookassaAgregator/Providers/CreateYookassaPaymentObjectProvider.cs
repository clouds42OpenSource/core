﻿using System;
using System.Threading.Tasks;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.YookassaAggregator.Providers.Interfaces;
using Clouds42.YookassaAggregator.Сonstants;
using Newtonsoft.Json;

namespace Clouds42.YookassaAggregator.Providers
{
    /// <summary>
    /// Провайдер для создания объекта платежа
    /// в агрегаторе ЮKassa
    /// </summary>
    internal class CreateYookassaPaymentObjectProvider(IServiceProvider serviceProvider)
        : YookassaPaymentObjectBaseProvider(serviceProvider),
            ICreateYookassaPaymentObjectProvider
    {
        private readonly Lazy<bool> _isCaptureYookassaPayment = new(CloudConfigurationProvider.YookassaAggregatorPayment.IsCapture);
        private readonly Lazy<string> _returnUrl = new(CloudConfigurationProvider.YookassaAggregatorPayment.ReturnUrl);

        /// <summary>
        /// Создать объект платежа в агрегаторе ЮKassa
        /// </summary>
        /// <param name="creationData">Модель данных для создания объекта платежа в ЮKassa</param>
        /// <returns>Объект платежа ЮKassa</returns>
        public YookassaPaymentObjectDto Create(YookassaPaymentObjectCreationDataDto creationData) =>
            SendPostRequest<YookassaPaymentObjectDto>(
                JsonCustomSerializer.SerializeToJson(GenerateCreateYookassaPaymentObjectDto(creationData)),
                creationData.SupplierId,
                creationData.IdempotencyKey);
        
        public async Task<YookassaPaymentObjectDto> CreateQrAsync(YookassaPaymentObjectCreationDataDto creationData) =>
            await SendPostRequestAsync<YookassaPaymentObjectDto>(
                JsonCustomSerializer.SerializeToJson(new CreateYookassaPaymentWithQrDto
                {
                    Description = creationData.Description, 
                    IsCapture = _isCaptureYookassaPayment.Value, 
                    PaymentAmountData = new YookassaPaymentAmountDataDto
                    {
                        Amount = creationData.PaymentSum,
                        Currency = YookassaAggregatorConst.CurrencyRub
                    },
                    PaymentConfirmationData = new YookassaPaymentConfirmationDataDto
                    {
                        Type = YookassaAggregatorConst.RedirectConfirmationType,
                        ReturnUrl = _returnUrl.Value
                    },
                    PaymentMethodData = new YookassaPaymentMethodDataDto
                    {
                        Type = "sbp"
                    }
                }),
                creationData.SupplierId,
                creationData.IdempotencyKey);

        /// <summary>
        /// Получить тип операции
        /// </summary>
        /// <returns>Тип операции</returns>
        protected override string GetOperationType() => "Создание объекта платежа в агрегаторе ЮKassa";

        /// <summary>
        /// Сформировать модель создания объекта платежа ЮKassa
        /// </summary>
        /// <param name="creationData">Модель данных для создания объекта платежа в ЮKassa</param>
        /// <returns>Модель создания объекта платежа ЮKassa</returns>
        private CreateYookassaPaymentObjectDto GenerateCreateYookassaPaymentObjectDto(
            YookassaPaymentObjectCreationDataDto creationData) => new()
            {
                Description = creationData.Description,
                IsCapture = _isCaptureYookassaPayment.Value,
                PaymentAmountData = new YookassaPaymentAmountDataDto
                {
                    Currency = YookassaAggregatorConst.CurrencyRub,
                    Amount = creationData.PaymentSum
                },
                PaymentConfirmationData = string.IsNullOrEmpty(creationData.AggregatorPaymentMethodId)
                    ? new YookassaPaymentConfirmationDataDto { Type = YookassaAggregatorConst.EmbeddedConfirmationType, ReturnUrl = _returnUrl.Value }
                    : new YookassaPaymentConfirmationDataDto { Type = YookassaAggregatorConst.RedirectConfirmationType, ReturnUrl = _returnUrl.Value },

                NeedSavePaymentMethod = creationData.IsAutoPay,
                PaymentMethodId = creationData.AggregatorPaymentMethodId
            };
    }

    public class CreateYookassaPaymentWithQrDto
    {
        /// <summary>
        /// Данные о сумме платежа
        /// </summary>
        [JsonProperty("amount")]
        public YookassaPaymentAmountDataDto PaymentAmountData { get; set; }

        /// <summary>
        /// Автоматический прием поступившего платежа
        /// </summary>
        [JsonProperty("capture")]
        public bool IsCapture { get; set; }

        /// <summary>
        /// Данные о подтверждении платежа
        /// </summary>
        [JsonProperty("confirmation")]
        public YookassaPaymentConfirmationDataDto PaymentConfirmationData { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Тип оплаты
        /// </summary>
        [JsonProperty("payment_method_data")]
        public YookassaPaymentMethodDataDto PaymentMethodData { get; set; }
    }

    public class YookassaPaymentMethodDataDto
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
