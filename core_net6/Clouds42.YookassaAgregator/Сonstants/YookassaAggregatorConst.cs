﻿namespace Clouds42.YookassaAggregator.Сonstants
{
    /// <summary>
    /// Константы сборки по взаимодействию с API ЮKassa
    /// </summary>
    public static class YookassaAggregatorConst
    {
        /// <summary>
        /// Обозначение валюты в рублях
        /// </summary>
        public const string CurrencyRub = "RUB";

        /// <summary>
        /// Встроенный тип подтверждения платежа
        /// </summary>
        public const string EmbeddedConfirmationType = "embedded";

        /// <summary>
        /// Встроенный тип подтверждения платежа
        /// </summary>
        public const string RedirectConfirmationType = "redirect";

        /// <summary>
        /// Ключ идэнтпотентности
        /// </summary>
        public const string IdempotenceKey = "Idempotence-Key";
    }
}
