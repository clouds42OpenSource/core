﻿namespace Clouds42.YookassaAggregator.Сonstants
{
    public enum YookassaAutoWriteOffSettings
    {
        /// <summary>
        /// Неопределено
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Без автосписаний
        /// </summary>
        WithoutAutoWriteOff = 1,

        /// <summary>
        /// Принудительное автосписание
        /// </summary>
        ForcedAutoWriteOff = 2,

        /// <summary>
        /// Условное автосписание
        /// </summary>
        ConditionalAutoWriteOff = 3
    }
}
