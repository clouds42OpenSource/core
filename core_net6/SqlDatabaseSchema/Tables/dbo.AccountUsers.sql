CREATE TABLE [dbo].[AccountUsers]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Login] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[FirstName] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[Password] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[Email] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NOT NULL,
[PhoneNumber] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Activated] [bit] NOT NULL,
[CreatedInAd] [bit] NOT NULL,
[ConfirmationCode] [nvarchar] (6) COLLATE Cyrillic_General_CI_AS NULL,
[LastLoggedIn] [datetime] NULL,
[Unsubscribed] [bit] NULL,
[CorpUserID] [uniqueidentifier] NULL,
[CorpUserSyncStatus] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Comment] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[LastName] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[MiddleName] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[FullName] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[CreationDate] [datetime] NULL,
[ConfirmationAttemptsLeft] [tinyint] NULL,
[Removed] [bit] NULL,
[EditDate] [datetime] NULL,
[ResetCode] [uniqueidentifier] NULL,
[AuthGuid] [uniqueidentifier] NULL,
[AuthToken] [uniqueidentifier] NULL,
[EmailStatus] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NOT NULL,
[LoginCode] [nvarchar] (6) COLLATE Cyrillic_General_CI_AS NULL,
[DateLoginCode] [datetime] NULL,
[ReferralId] [uniqueidentifier] NULL,
[PrimaryPhone] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AccountUsers] ADD 
CONSTRAINT [PK_dbo.AccountUsers] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [dbo].[AccountUsers] ([AccountId])

CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountUserLogin] ON [dbo].[AccountUsers] ([Login])

ALTER TABLE [dbo].[AccountUsers] ADD
CONSTRAINT [FK_dbo.AccountUsers_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
CREATE NONCLUSTERED INDEX [IX_AuthToken] ON [dbo].[AccountUsers] ([AuthToken])
GO
