CREATE TABLE [dbo].[CSPerformanceCounters]
(
[Id] [uniqueidentifier] NOT NULL,
[CloudServiceId] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[AccountUserId] [uniqueidentifier] NOT NULL,
[PerformanceCounterId] [nvarchar] (150) COLLATE Cyrillic_General_CI_AS NOT NULL,
[PerformanceCounterValue] [float] NOT NULL,
[FixationDateTime] [datetime] NOT NULL,
[UserAgent] [nchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Apdex] [float] NULL,
[CountersAmount] [int] NULL
)
ALTER TABLE [dbo].[CSPerformanceCounters] ADD 
CONSTRAINT [PK_dbo.CSPerformanceCounters] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_CloudServiceId] ON [dbo].[CSPerformanceCounters] ([CloudServiceId])

ALTER TABLE [dbo].[CSPerformanceCounters] ADD
CONSTRAINT [FK_dbo.CSPerformanceCounters_dbo.CloudServices_CloudServiceId] FOREIGN KEY ([CloudServiceId]) REFERENCES [dbo].[CloudServices] ([CloudServiceId])
GO
