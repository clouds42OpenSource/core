CREATE TABLE [dbo].[AccountDomainUsers]
(
[ID] [uniqueidentifier] NOT NULL,
[AccountDomainID] [uniqueidentifier] NOT NULL,
[DomainUserName] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NULL,
[AccountUserID] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[AccountDomainUsers] ADD 
CONSTRAINT [PK_dbo.AccountDomainUsers] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountDomainID] ON [dbo].[AccountDomainUsers] ([AccountDomainID])

CREATE NONCLUSTERED INDEX [IX_AccountUserID] ON [dbo].[AccountDomainUsers] ([AccountUserID])

ALTER TABLE [dbo].[AccountDomainUsers] ADD
CONSTRAINT [FK_dbo.AccountDomainUsers_dbo.AccountDomains_AccountDomainID] FOREIGN KEY ([AccountDomainID]) REFERENCES [dbo].[AccountDomains] ([ID])
ALTER TABLE [dbo].[AccountDomainUsers] ADD
CONSTRAINT [FK_dbo.AccountDomainUsers_dbo.AccountUsers_AccountUserID] FOREIGN KEY ([AccountUserID]) REFERENCES [dbo].[AccountUsers] ([Id])
GO
