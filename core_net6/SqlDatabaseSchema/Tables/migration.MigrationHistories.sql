CREATE TABLE [migration].[MigrationHistories]
(
[HistoryId] [uniqueidentifier] NOT NULL,
[StartDateTime] [datetime] NOT NULL,
[FinishDateTime] [datetime] NOT NULL,
[Status] [tinyint] NOT NULL,
[InitiatorId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [migration].[MigrationHistories] ADD 
CONSTRAINT [PK_migration.MigrationHistories] PRIMARY KEY CLUSTERED  ([HistoryId])
CREATE NONCLUSTERED INDEX [IX_InitiatorId] ON [migration].[MigrationHistories] ([InitiatorId])

ALTER TABLE [migration].[MigrationHistories] ADD
CONSTRAINT [FK_migration.MigrationHistories_dbo.AccountUsers_InitiatorId] FOREIGN KEY ([InitiatorId]) REFERENCES [dbo].[AccountUsers] ([Id])
GO
