CREATE TABLE [dbo].[AcDbLinkTasks]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountDatabase_id] [uniqueidentifier] NOT NULL,
[LocalUser_id] [uniqueidentifier] NOT NULL,
[TaskCommand] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[TaskResult] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[TaskStatus] [int] NOT NULL,
[DateTimeSetTask] [datetime] NULL,
[DateTimeGetTask] [datetime] NULL,
[DateTimeDBStartTask] [datetime] NULL,
[DateTimeDBEndTask] [datetime] NULL,
[DateTimeSetResultTask] [datetime] NULL,
[ActionType] [int] NULL,
[Parameters] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[LinkID] [uniqueidentifier] NULL
)
ALTER TABLE [dbo].[AcDbLinkTasks] ADD 
CONSTRAINT [PK_dbo.AcDbLinkTasks] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountDatabase_id] ON [dbo].[AcDbLinkTasks] ([AccountDatabase_id])

ALTER TABLE [dbo].[AcDbLinkTasks] ADD
CONSTRAINT [FK_dbo.AcDbLinkTasks_dbo.AccountDatabases_AccountDatabase_id] FOREIGN KEY ([AccountDatabase_id]) REFERENCES [dbo].[AccountDatabases] ([Id]) ON DELETE CASCADE
GO
