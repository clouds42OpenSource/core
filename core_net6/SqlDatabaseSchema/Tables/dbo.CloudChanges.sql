CREATE TABLE [dbo].[CloudChanges]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Date] [datetime] NOT NULL,
[Initiator] [uniqueidentifier] NULL,
[Action] [uniqueidentifier] NOT NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudChanges] ADD 
CONSTRAINT [PK_dbo.CloudChanges] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [dbo].[CloudChanges] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_Action] ON [dbo].[CloudChanges] ([Action])

CREATE NONCLUSTERED INDEX [IX_Initiator] ON [dbo].[CloudChanges] ([Initiator])

ALTER TABLE [dbo].[CloudChanges] ADD
CONSTRAINT [FK_dbo.CloudChanges_dbo.AccountUsers_Initiator] FOREIGN KEY ([Initiator]) REFERENCES [dbo].[AccountUsers] ([Id])
ALTER TABLE [dbo].[CloudChanges] ADD
CONSTRAINT [FK_dbo.CloudChanges_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[CloudChanges] ADD
CONSTRAINT [FK_dbo.CloudChanges_dbo.CloudChangesActions_Action] FOREIGN KEY ([Action]) REFERENCES [dbo].[CloudChangesActions] ([Id]) ON DELETE CASCADE
GO
