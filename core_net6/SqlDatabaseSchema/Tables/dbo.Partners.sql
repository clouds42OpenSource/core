CREATE TABLE [dbo].[Partners]
(
[PartnerId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[Partners] ADD 
CONSTRAINT [PK_dbo.Partners] PRIMARY KEY CLUSTERED  ([PartnerId])
GO
