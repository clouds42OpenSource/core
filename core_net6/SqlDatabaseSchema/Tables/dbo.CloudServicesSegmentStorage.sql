CREATE TABLE [dbo].[CloudServicesSegmentStorage]
(
[ID] [uniqueidentifier] NOT NULL,
[SegmentID] [uniqueidentifier] NOT NULL,
[FileStorageID] [uniqueidentifier] NOT NULL,
[IsDefault] [bit] NOT NULL
)
ALTER TABLE [dbo].[CloudServicesSegmentStorage] ADD 
CONSTRAINT [PK_dbo.CloudServicesSegmentStorage] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_FileStorageID] ON [dbo].[CloudServicesSegmentStorage] ([FileStorageID])

CREATE NONCLUSTERED INDEX [IX_SegmentID] ON [dbo].[CloudServicesSegmentStorage] ([SegmentID])

ALTER TABLE [dbo].[CloudServicesSegmentStorage] ADD
CONSTRAINT [FK_dbo.CloudServicesSegmentStorage_dbo.CloudServicesFileStorageServers_FileStorageID] FOREIGN KEY ([FileStorageID]) REFERENCES [dbo].[CloudServicesFileStorageServers] ([ID])
ALTER TABLE [dbo].[CloudServicesSegmentStorage] ADD
CONSTRAINT [FK_dbo.CloudServicesSegmentStorage_dbo.CloudServicesSegment_SegmentID] FOREIGN KEY ([SegmentID]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
GO
