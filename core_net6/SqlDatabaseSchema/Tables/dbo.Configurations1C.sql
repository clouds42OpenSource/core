CREATE TABLE [dbo].[Configurations1C]
(
[Name] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ConfigurationCatalog] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL,
[RedactionCatalog] [nvarchar] (4) COLLATE Cyrillic_General_CI_AS NOT NULL,
[PlatformCatalog] [nvarchar] (3) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[Configurations1C] ADD 
CONSTRAINT [PK_dbo.Configurations1C] PRIMARY KEY CLUSTERED  ([Name])
GO
