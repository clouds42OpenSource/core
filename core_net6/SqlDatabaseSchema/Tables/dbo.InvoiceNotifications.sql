CREATE TABLE [dbo].[InvoiceNotifications]
(
[Id] [uniqueidentifier] NOT NULL,
[Date] [datetime] NOT NULL,
[From] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Content] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL,
[AttachFileContent] [varbinary] (max) NULL,
[AttachFileName] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[InvoiceId] [uniqueidentifier] NOT NULL,
[Status] [bit] NOT NULL,
[To] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[InvoiceNotifications] ADD 
CONSTRAINT [PK_dbo.InvoiceNotifications] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_InvoiceId] ON [dbo].[InvoiceNotifications] ([InvoiceId])

ALTER TABLE [dbo].[InvoiceNotifications] ADD
CONSTRAINT [FK_dbo.InvoiceNotifications_billing.Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [billing].[Invoices] ([Id])
GO
