CREATE TABLE [dbo].[CloudServicesSegmentTerminalServers]
(
[Id] [uniqueidentifier] NOT NULL,
[SegmentId] [uniqueidentifier] NOT NULL,
[TerminalServerId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[CloudServicesSegmentTerminalServers] ADD 
CONSTRAINT [PK_dbo.CloudServicesSegmentTerminalServers] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_SegmentId] ON [dbo].[CloudServicesSegmentTerminalServers] ([SegmentId])

CREATE NONCLUSTERED INDEX [IX_TerminalServerId] ON [dbo].[CloudServicesSegmentTerminalServers] ([TerminalServerId])

ALTER TABLE [dbo].[CloudServicesSegmentTerminalServers] ADD
CONSTRAINT [FK_dbo.CloudServicesSegmentTerminalServers_dbo.CloudServicesSegment_SegmentId] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[CloudServicesSegment] ([ID]) ON DELETE CASCADE
ALTER TABLE [dbo].[CloudServicesSegmentTerminalServers] ADD
CONSTRAINT [FK_dbo.CloudServicesSegmentTerminalServers_dbo.CloudTerminalServers_TerminalServerId] FOREIGN KEY ([TerminalServerId]) REFERENCES [dbo].[CloudTerminalServers] ([ID]) ON DELETE CASCADE
GO
