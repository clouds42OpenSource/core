CREATE TABLE [dbo].[TemplateUpdates]
(
[Id] [uniqueidentifier] NOT NULL,
[Title] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[TemplateID] [uniqueidentifier] NOT NULL,
[UpdateDate] [datetime] NULL,
[UpdateVersion] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[TemplateUpdates] ADD CONSTRAINT [PK_TemplateUpdates] PRIMARY KEY CLUSTERED  ([Id])
GO
