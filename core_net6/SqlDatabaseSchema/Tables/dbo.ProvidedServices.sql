CREATE TABLE [dbo].[ProvidedServices]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Rate] [decimal] (18, 0) NOT NULL,
[Service] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ServiceType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[ActNumber] [int] NULL,
[DateFrom] [datetime] NULL,
[DateTo] [datetime] NULL,
[Count] [int] NOT NULL,
[KindOfServiceProvision] [int] NOT NULL,
[KorpUniquaNumber] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NULL,
[CorpAct] [bit] NOT NULL,
[Comment] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NULL,
[SponsoredAccountId] [uniqueidentifier] NULL,
[KorpDate] [datetime] NULL,
[IsManagedAccounting] [bit] NULL,
[IsBookerAccounting] [bit] NULL
)
ALTER TABLE [dbo].[ProvidedServices] ADD 
CONSTRAINT [PK_dbo.ProvidedServices] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [dbo].[ProvidedServices] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_SponsoredAccountId] ON [dbo].[ProvidedServices] ([SponsoredAccountId])

ALTER TABLE [dbo].[ProvidedServices] ADD
CONSTRAINT [FK_dbo.ProvidedServices_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
ALTER TABLE [dbo].[ProvidedServices] ADD
CONSTRAINT [FK_dbo.ProvidedServices_dbo.Accounts_SponsoredAccountId] FOREIGN KEY ([SponsoredAccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
