CREATE TABLE [mailing].[UsersMessages]
(
[Id] [uniqueidentifier] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL,
[MessageId] [uniqueidentifier] NOT NULL,
[Status] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [mailing].[UsersMessages] ADD 
CONSTRAINT [PK_mailing.UsersMessages] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_MessageId] ON [mailing].[UsersMessages] ([MessageId])

ALTER TABLE [mailing].[UsersMessages] ADD
CONSTRAINT [FK_mailing.UsersMessages_mailing.Messages_MessageId] FOREIGN KEY ([MessageId]) REFERENCES [mailing].[Messages] ([Id])
GO
