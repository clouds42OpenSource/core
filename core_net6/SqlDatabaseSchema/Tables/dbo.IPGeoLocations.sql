CREATE TABLE [dbo].[IPGeoLocations]
(
[ip_from] [float] NOT NULL,
[ip_to] [float] NOT NULL,
[country_code] [nvarchar] (2) COLLATE Cyrillic_General_CI_AS NOT NULL,
[country_name] [nvarchar] (64) COLLATE Cyrillic_General_CI_AS NOT NULL,
[region_name] [nvarchar] (128) COLLATE Cyrillic_General_CI_AS NOT NULL,
[city_name] [nvarchar] (128) COLLATE Cyrillic_General_CI_AS NOT NULL,
[latitude] [float] NOT NULL,
[longitude] [float] NOT NULL,
[zip_code] [nvarchar] (30) COLLATE Cyrillic_General_CI_AS NOT NULL,
[time_zone] [nvarchar] (8) COLLATE Cyrillic_General_CI_AS NOT NULL,
[branch_cityId] [int] NULL
)
GO
