CREATE TABLE [billing].[BillingAccounts]
(
[Id] [uniqueidentifier] NOT NULL,
[Balance] [money] NOT NULL,
[MonthlyPaymentDate] [datetime2] NULL,
[Testing] [bit] NULL,
[AccountType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[DefaultDbType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Removed] [bit] NULL,
[PromisePaymentDate] [datetime] NULL,
[PromisePaymentSum] [money] NULL
)
ALTER TABLE [billing].[BillingAccounts] ADD 
CONSTRAINT [PK_billing.BillingAccounts] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_Id] ON [billing].[BillingAccounts] ([Id])

ALTER TABLE [billing].[BillingAccounts] ADD
CONSTRAINT [FK_billing.BillingAccounts_dbo.Accounts_Id] FOREIGN KEY ([Id]) REFERENCES [dbo].[Accounts] ([Id])
GO
