CREATE TABLE [dbo].[NotificationBuffer]
(
[Id] [uniqueidentifier] NOT NULL,
[Account] [uniqueidentifier] NOT NULL,
[NotifyKey] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[ActualPeriod] [datetime] NULL,
[Counter] [int] NULL
)
ALTER TABLE [dbo].[NotificationBuffer] ADD 
CONSTRAINT [PK_dbo.NotificationBuffer] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_Account] ON [dbo].[NotificationBuffer] ([Account])

ALTER TABLE [dbo].[NotificationBuffer] ADD
CONSTRAINT [FK_dbo.NotificationBuffer_dbo.Accounts_Account] FOREIGN KEY ([Account]) REFERENCES [dbo].[Accounts] ([Id])
GO
