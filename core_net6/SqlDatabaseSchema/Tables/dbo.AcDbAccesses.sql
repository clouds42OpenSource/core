CREATE TABLE [dbo].[AcDbAccesses]
(
[ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_AcDbAccesses_ID] DEFAULT (newid()),
[AccountDatabaseID] [uniqueidentifier] NOT NULL,
[LocalUserID] [uniqueidentifier] NOT NULL,
[AccountID] [uniqueidentifier] NOT NULL,
[AccountUserID] [uniqueidentifier] NULL,
[IsLock] [bit] NOT NULL,
[LaunchType] [int] NULL
)
ALTER TABLE [dbo].[AcDbAccesses] ADD 
CONSTRAINT [PK_dbo.AcDbAccesses] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountDatabaseID] ON [dbo].[AcDbAccesses] ([AccountDatabaseID])

CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[AcDbAccesses] ([AccountID])

CREATE NONCLUSTERED INDEX [IX_AccountUserID] ON [dbo].[AcDbAccesses] ([AccountUserID])

ALTER TABLE [dbo].[AcDbAccesses] ADD
CONSTRAINT [FK_dbo.AcDbAccesses_dbo.AccountDatabases_AccountDatabaseID] FOREIGN KEY ([AccountDatabaseID]) REFERENCES [dbo].[AccountDatabases] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AcDbAccesses] ADD
CONSTRAINT [FK_dbo.AcDbAccesses_dbo.Accounts_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AcDbAccesses] ADD
CONSTRAINT [FK_dbo.AcDbAccesses_dbo.AccountUsers_AccountUserID] FOREIGN KEY ([AccountUserID]) REFERENCES [dbo].[AccountUsers] ([Id])



GO
