CREATE TABLE [migration].[MigrationAccountDatabaseHistories]
(
[HistoryId] [uniqueidentifier] NOT NULL,
[AccountDatabaseId] [uniqueidentifier] NOT NULL,
[SourceFileStorageId] [uniqueidentifier] NULL,
[TargetFileStorageId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [migration].[MigrationAccountDatabaseHistories] ADD 
CONSTRAINT [PK_migration.MigrationAccountDatabaseHistories] PRIMARY KEY CLUSTERED  ([HistoryId])
CREATE NONCLUSTERED INDEX [IX_AccountDatabaseId] ON [migration].[MigrationAccountDatabaseHistories] ([AccountDatabaseId])

CREATE NONCLUSTERED INDEX [IX_HistoryId] ON [migration].[MigrationAccountDatabaseHistories] ([HistoryId])

CREATE NONCLUSTERED INDEX [IX_SourceFileStorageId] ON [migration].[MigrationAccountDatabaseHistories] ([SourceFileStorageId])

CREATE NONCLUSTERED INDEX [IX_TargetFileStorageId] ON [migration].[MigrationAccountDatabaseHistories] ([TargetFileStorageId])

ALTER TABLE [migration].[MigrationAccountDatabaseHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountDatabaseHistories_dbo.AccountDatabases_AccountDatabaseId] FOREIGN KEY ([AccountDatabaseId]) REFERENCES [dbo].[AccountDatabases] ([Id]) ON DELETE CASCADE
ALTER TABLE [migration].[MigrationAccountDatabaseHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountDatabaseHistories_dbo.CloudServicesFileStorageServers_SourceFileStorageId] FOREIGN KEY ([SourceFileStorageId]) REFERENCES [dbo].[CloudServicesFileStorageServers] ([ID])
ALTER TABLE [migration].[MigrationAccountDatabaseHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountDatabaseHistories_dbo.CloudServicesFileStorageServers_TargetFileStorageId] FOREIGN KEY ([TargetFileStorageId]) REFERENCES [dbo].[CloudServicesFileStorageServers] ([ID])
ALTER TABLE [migration].[MigrationAccountDatabaseHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountDatabaseHistories_migration.MigrationHistories_HistoryId] FOREIGN KEY ([HistoryId]) REFERENCES [migration].[MigrationHistories] ([HistoryId])
GO
