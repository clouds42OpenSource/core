CREATE TABLE [dbo].[CloudCore]
(
[MainPageNotification] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[PrimaryKey] [uniqueidentifier] NOT NULL,
[DefaultSaleManagerId] [uniqueidentifier] NULL,
[HourseDifferenceOfUkraineAndMoscow] [int] NULL
)
ALTER TABLE [dbo].[CloudCore] ADD 
CONSTRAINT [PK_dbo.CloudCore] PRIMARY KEY CLUSTERED  ([PrimaryKey])
CREATE NONCLUSTERED INDEX [IX_DefaultSaleManagerId] ON [dbo].[CloudCore] ([DefaultSaleManagerId])

ALTER TABLE [dbo].[CloudCore] ADD
CONSTRAINT [FK_dbo.CloudCore_dbo.AccountUsers_DefaultSaleManagerId] FOREIGN KEY ([DefaultSaleManagerId]) REFERENCES [dbo].[AccountUsers] ([Id])
GO
