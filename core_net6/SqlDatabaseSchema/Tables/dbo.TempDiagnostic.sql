CREATE TABLE [dbo].[TempDiagnostic]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [varchar] (8000) COLLATE Cyrillic_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TempDiagnostic] ADD CONSTRAINT [PK__TempDiag__3213E83FBE8410D8] PRIMARY KEY CLUSTERED  ([id])
GO
