CREATE TABLE [billing].[InvoiceReceipts]
(
[InvoiceId] [uniqueidentifier] NOT NULL,
[DocumentDateTime] [datetime2] NULL,
[ReceiptInfo] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL,
[QrCodeData] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[QrCodeFormat] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NULL,
[FiscalNumber] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[FiscalSerial] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[FiscalSign] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [billing].[InvoiceReceipts] ADD 
CONSTRAINT [PK_billing.InvoiceReceipts] PRIMARY KEY CLUSTERED  ([InvoiceId])
CREATE NONCLUSTERED INDEX [IX_InvoiceId] ON [billing].[InvoiceReceipts] ([InvoiceId])

ALTER TABLE [billing].[InvoiceReceipts] ADD
CONSTRAINT [FK_billing.InvoiceReceipts_billing.Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [billing].[Invoices] ([Id])
GO
