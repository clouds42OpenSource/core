CREATE TABLE [dbo].[AvailableMigration]
(
[Id] [uniqueidentifier] NOT NULL,
[SegmentIdFrom] [uniqueidentifier] NOT NULL,
[SegmentIdTo] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[AvailableMigration] ADD 
CONSTRAINT [PK_dbo.AvailableMigration] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_SegmentIdFrom] ON [dbo].[AvailableMigration] ([SegmentIdFrom])

CREATE NONCLUSTERED INDEX [IX_SegmentIdTo] ON [dbo].[AvailableMigration] ([SegmentIdTo])

ALTER TABLE [dbo].[AvailableMigration] ADD
CONSTRAINT [FK_dbo.AvailableMigration_dbo.CloudServicesSegment_SegmentIdFrom] FOREIGN KEY ([SegmentIdFrom]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
ALTER TABLE [dbo].[AvailableMigration] ADD
CONSTRAINT [FK_dbo.AvailableMigration_dbo.CloudServicesSegment_SegmentIdTo] FOREIGN KEY ([SegmentIdTo]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
GO
