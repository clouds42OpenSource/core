CREATE TABLE [dbo].[CloudServicesSQLServer]
(
[ID] [uniqueidentifier] NOT NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudServicesSQLServer] ADD 
CONSTRAINT [PK_dbo.CloudServicesSQLServer] PRIMARY KEY CLUSTERED  ([ID])
GO
