CREATE TABLE [dbo].[CloudServicesSegment]
(
[ID] [uniqueidentifier] NOT NULL,
[GatewayTerminalsID] [uniqueidentifier] NULL,
[ServicesTerminalFarmID] [uniqueidentifier] NOT NULL,
[ContentServerID] [uniqueidentifier] NOT NULL,
[SQLServerID] [uniqueidentifier] NOT NULL,
[BackupStorageID] [uniqueidentifier] NOT NULL,
[IsDefault] [bit] NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[EnterpriseServer82ID] [uniqueidentifier] NOT NULL,
[EnterpriseServer83ID] [uniqueidentifier] NOT NULL,
[FileStorageServersID] [uniqueidentifier] NOT NULL,
[WindowsAccountsPath] [nvarchar] (500) COLLATE Cyrillic_General_CI_AS NULL,
[CustomFileStoragePath] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[FolderPath1CV83] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[FolderPath1CV82] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[PlatformVersion1C83] [nvarchar] (25) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CloudServicesSegment] ADD 
CONSTRAINT [PK_dbo.CloudServicesSegment] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_BackupStorageID] ON [dbo].[CloudServicesSegment] ([BackupStorageID])

CREATE NONCLUSTERED INDEX [IX_ContentServerID] ON [dbo].[CloudServicesSegment] ([ContentServerID])

CREATE NONCLUSTERED INDEX [IX_EnterpriseServer82ID] ON [dbo].[CloudServicesSegment] ([EnterpriseServer82ID])

CREATE NONCLUSTERED INDEX [IX_EnterpriseServer83ID] ON [dbo].[CloudServicesSegment] ([EnterpriseServer83ID])

CREATE NONCLUSTERED INDEX [IX_FileStorageServersID] ON [dbo].[CloudServicesSegment] ([FileStorageServersID])

CREATE NONCLUSTERED INDEX [IX_GatewayTerminalsID] ON [dbo].[CloudServicesSegment] ([GatewayTerminalsID])

CREATE NONCLUSTERED INDEX [IX_ServicesTerminalFarmID] ON [dbo].[CloudServicesSegment] ([ServicesTerminalFarmID])

CREATE NONCLUSTERED INDEX [IX_SQLServerID] ON [dbo].[CloudServicesSegment] ([SQLServerID])

ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesBackupStorage_BackupStorageID] FOREIGN KEY ([BackupStorageID]) REFERENCES [dbo].[CloudServicesBackupStorage] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesContentServer_ContentServerID] FOREIGN KEY ([ContentServerID]) REFERENCES [dbo].[CloudServicesContentServer] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesEnterpriseServer_EnterpriseServer82ID] FOREIGN KEY ([EnterpriseServer82ID]) REFERENCES [dbo].[CloudServicesEnterpriseServer] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesEnterpriseServer_EnterpriseServer83ID] FOREIGN KEY ([EnterpriseServer83ID]) REFERENCES [dbo].[CloudServicesEnterpriseServer] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesFileStorageServers_FileStorageServersID] FOREIGN KEY ([FileStorageServersID]) REFERENCES [dbo].[CloudServicesFileStorageServers] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesGatewayTerminals_GatewayTerminalsID] FOREIGN KEY ([GatewayTerminalsID]) REFERENCES [dbo].[CloudServicesGatewayTerminals] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesSQLServer_SQLServerID] FOREIGN KEY ([SQLServerID]) REFERENCES [dbo].[CloudServicesSQLServer] ([ID])
ALTER TABLE [dbo].[CloudServicesSegment] ADD
CONSTRAINT [FK_dbo.CloudServicesSegment_dbo.CloudServicesTerminalFarm_ServicesTerminalFarmID] FOREIGN KEY ([ServicesTerminalFarmID]) REFERENCES [dbo].[CloudServicesTerminalFarm] ([ID])
GO
