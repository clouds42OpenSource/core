CREATE TABLE [dbo].[AccountDatabases]
(
[Id] [uniqueidentifier] NOT NULL,
[OwnerId] [uniqueidentifier] NOT NULL,
[LastActivityDate] [datetime2] NOT NULL,
[Caption] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[State] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[TemplateId] [uniqueidentifier] NULL,
[DbNumber] [int] NOT NULL,
[ServiceName] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[SqlName] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[V82Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[V82Server] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[SizeInMB] [int] NOT NULL,
[CreationDate] [datetime2] NOT NULL,
[IsFile] [bit] NULL,
[ArchivePath] [nvarchar] (300) COLLATE Cyrillic_General_CI_AS NULL,
[IsPublished] [bit] NULL,
[WebPublishPath] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AS NULL,
[Platform] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NULL,
[LinkLastPingDatetime] [datetime] NULL,
[LinkLastConnectionError] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AS NULL,
[V82Confname] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AS NULL,
[V82ConfVersion] [nvarchar] (32) COLLATE Cyrillic_General_CI_AS NULL,
[AccountDomainID] [uniqueidentifier] NULL,
[LaunchType] [int] NULL,
[CalculateSizeDateTime] [datetime] NULL,
[ActiveLink] [bit] NULL,
[LinkServicePath] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AS NULL,
[FileStorageID] [uniqueidentifier] NULL,
[LaunchParameters] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[UsedWebServices] [bit] NULL,
[TemplateRelease] [varchar] (300) COLLATE Cyrillic_General_CI_AS NULL,
[CfuId] [uniqueidentifier] NULL,
[LockedState] [nchar] (10) COLLATE Cyrillic_General_CI_AS NULL,
[LastEditedDateTime] [datetime2] NOT NULL,
[CloudStorageWebLink] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AccountDatabases] ADD 
CONSTRAINT [PK_dbo.AccountDatabases] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountDomainID] ON [dbo].[AccountDatabases] ([AccountDomainID])

CREATE NONCLUSTERED INDEX [IX_OwnerId] ON [dbo].[AccountDatabases] ([OwnerId])

CREATE NONCLUSTERED INDEX [IX_TemplateId] ON [dbo].[AccountDatabases] ([TemplateId])

ALTER TABLE [dbo].[AccountDatabases] ADD
CONSTRAINT [FK_dbo.AccountDatabases_dbo.AccountDomains_AccountDomainID] FOREIGN KEY ([AccountDomainID]) REFERENCES [dbo].[AccountDomains] ([ID])
ALTER TABLE [dbo].[AccountDatabases] ADD
CONSTRAINT [FK_dbo.AccountDatabases_dbo.Accounts_OwnerId] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Accounts] ([Id])
ALTER TABLE [dbo].[AccountDatabases] ADD
CONSTRAINT [FK_dbo.AccountDatabases_dbo.DbTemplates_TemplateId] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[DbTemplates] ([Id])
GO
