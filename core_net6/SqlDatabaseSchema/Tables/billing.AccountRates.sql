CREATE TABLE [billing].[AccountRates]
(
[Id] [uniqueidentifier] NOT NULL,
[Service] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[ResourceType] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Cost] [money] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [billing].[AccountRates] ADD 
CONSTRAINT [PK_billing.AccountRates] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [billing].[AccountRates] ([AccountId])

ALTER TABLE [billing].[AccountRates] ADD
CONSTRAINT [FK_billing.AccountRates_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
