CREATE TABLE [dbo].[CloudChangesActions]
(
[Id] [uniqueidentifier] NOT NULL,
[GroupId] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Index] [smallint] NOT NULL
)
ALTER TABLE [dbo].[CloudChangesActions] ADD 
CONSTRAINT [PK_dbo.CloudChangesActions] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_GroupId] ON [dbo].[CloudChangesActions] ([GroupId])

ALTER TABLE [dbo].[CloudChangesActions] ADD
CONSTRAINT [FK_dbo.CloudChangesActions_dbo.CloudChangesGroups_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[CloudChangesGroups] ([Id]) ON DELETE CASCADE
GO
