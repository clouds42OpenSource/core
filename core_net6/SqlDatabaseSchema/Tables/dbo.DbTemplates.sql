CREATE TABLE [dbo].[DbTemplates]
(
[Id] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[DefaultCaption] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[Order] [int] NOT NULL,
[ImageUrl] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[Remark] [nvarchar] (30) COLLATE Cyrillic_General_CI_AS NULL,
[CurrentUpdateVersion] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[ActualUpdateVersion] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[TemplatePath] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[UpdatePath] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[LastUpdateDate] [datetime] NULL,
[UpdatesCount] [int] NULL,
[Platform] [nchar] (10) COLLATE Cyrillic_General_CI_AS NULL,
[LocaleId] [uniqueidentifier] NULL,
[DemoTemplateId] [uniqueidentifier] NULL,
[CanWebPublish] [bit] NULL,
[UpdatesListXmlFilePath] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[CurrectCfuId] [uniqueidentifier] NULL,
[ActualCfuId] [uniqueidentifier] NULL,
[FreeUsageDays] [int] NULL,
[DefaultRateName] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[DbTemplates] ADD 
CONSTRAINT [PK_dbo.DbTemplates] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_DemoTemplateId] ON [dbo].[DbTemplates] ([DemoTemplateId])

CREATE NONCLUSTERED INDEX [IX_LocaleId] ON [dbo].[DbTemplates] ([LocaleId])

CREATE NONCLUSTERED INDEX [IX_DbTemplates] ON [dbo].[DbTemplates] ([Name])

ALTER TABLE [dbo].[DbTemplates] ADD
CONSTRAINT [FK_dbo.DbTemplates_dbo.DbTemplates_DemoTemplateId] FOREIGN KEY ([DemoTemplateId]) REFERENCES [dbo].[DbTemplates] ([Id])
ALTER TABLE [dbo].[DbTemplates] ADD
CONSTRAINT [FK_dbo.DbTemplates_dbo.Locale_LocaleId] FOREIGN KEY ([LocaleId]) REFERENCES [dbo].[Locale] ([ID])

GO
