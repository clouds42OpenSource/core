CREATE TABLE [dbo].[ConfigurationsCfu]
(
[Id] [uniqueidentifier] NOT NULL,
[Version] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Platform1CVersion] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[DownloadDate] [datetime] NULL,
[ValidateState] [bit] NULL,
[FilePath] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[InitiatorId] [uniqueidentifier] NULL,
[ConfigurationName] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[ConfigurationsCfu] ADD 
CONSTRAINT [PK_dbo.ConfigurationsCfu] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_ConfigurationName] ON [dbo].[ConfigurationsCfu] ([ConfigurationName])

CREATE NONCLUSTERED INDEX [IX_InitiatorId] ON [dbo].[ConfigurationsCfu] ([InitiatorId])

ALTER TABLE [dbo].[ConfigurationsCfu] ADD
CONSTRAINT [FK_dbo.ConfigurationsCfu_dbo.AccountUsers_InitiatorId] FOREIGN KEY ([InitiatorId]) REFERENCES [dbo].[AccountUsers] ([Id])
ALTER TABLE [dbo].[ConfigurationsCfu] ADD
CONSTRAINT [FK_dbo.ConfigurationsCfu_dbo.Configurations1C_ConfigurationName] FOREIGN KEY ([ConfigurationName]) REFERENCES [dbo].[Configurations1C] ([Name])
GO
