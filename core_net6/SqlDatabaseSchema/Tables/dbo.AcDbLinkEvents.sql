CREATE TABLE [dbo].[AcDbLinkEvents]
(
[ID] [uniqueidentifier] NOT NULL,
[AccountID] [uniqueidentifier] NOT NULL,
[AcDbLocalUserID] [uniqueidentifier] NULL,
[DateEvent] [datetime] NOT NULL,
[Event] [int] NOT NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Parameters] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AcDbLinkEvents] ADD 
CONSTRAINT [PK_dbo.AcDbLinkEvents] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[AcDbLinkEvents] ([AccountID])

CREATE NONCLUSTERED INDEX [IX_AcDbLocalUserID] ON [dbo].[AcDbLinkEvents] ([AcDbLocalUserID])

ALTER TABLE [dbo].[AcDbLinkEvents] ADD
CONSTRAINT [FK_dbo.AcDbLinkEvents_dbo.Accounts_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([Id])
ALTER TABLE [dbo].[AcDbLinkEvents] ADD
CONSTRAINT [FK_dbo.AcDbLinkEvents_dbo.AcDbLocalUsers_AcDbLocalUserID] FOREIGN KEY ([AcDbLocalUserID]) REFERENCES [dbo].[AcDbLocalUsers] ([Id]) ON DELETE CASCADE
GO
