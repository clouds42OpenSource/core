CREATE TABLE [dbo].[CloudServicesTerminalFarm]
(
[ID] [uniqueidentifier] NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudServicesTerminalFarm] ADD 
CONSTRAINT [PK_dbo.CloudServicesTerminalFarm] PRIMARY KEY CLUSTERED  ([ID])
GO
