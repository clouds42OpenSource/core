CREATE TABLE [dbo].[CloudServices]
(
[Id] [uniqueidentifier] NOT NULL,
[CloudServiceId] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ServiceCaption] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[ServiceToken] [uniqueidentifier] NULL
)
ALTER TABLE [dbo].[CloudServices] ADD 
CONSTRAINT [PK_dbo.CloudServices] PRIMARY KEY CLUSTERED  ([CloudServiceId])
GO
