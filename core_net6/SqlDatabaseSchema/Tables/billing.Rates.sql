CREATE TABLE [billing].[Rates]
(
[Id] [uniqueidentifier] NOT NULL,
[Service] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[ResourceType] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[RatePeriod] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Cost] [money] NOT NULL,
[AccountType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[DiscountGroup] [int] NULL,
[DemoPeriod] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Remark] [nvarchar] (30) COLLATE Cyrillic_General_CI_AS NULL,
[LocaleId] [uniqueidentifier] NULL,
[UpperBound] [int] NULL,
[LowerBound] [int] NULL,
[IsHistorical] [bit] NOT NULL
)
ALTER TABLE [billing].[Rates] ADD 
CONSTRAINT [PK_billing.Rates] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_LocaleId] ON [billing].[Rates] ([LocaleId])

ALTER TABLE [billing].[Rates] ADD
CONSTRAINT [FK_billing.Rates_dbo.Locale_LocaleId] FOREIGN KEY ([LocaleId]) REFERENCES [dbo].[Locale] ([ID])
GO
