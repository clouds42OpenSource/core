CREATE TABLE [dbo].[AccountEmails]
(
[Id] [uniqueidentifier] NOT NULL,
[Email] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[AccountEmails] ADD 
CONSTRAINT [PK_dbo.AccountEmails] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [dbo].[AccountEmails] ([AccountId])

ALTER TABLE [dbo].[AccountEmails] ADD
CONSTRAINT [FK_dbo.AccountEmails_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
