CREATE TABLE [mailing].[Messages]
(
[Id] [uniqueidentifier] NOT NULL,
[Topic] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Body] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Date] [datetime] NULL,
[Type] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[AddUnsubscribeLink] [bit] NOT NULL,
[AddFooter] [bit] NOT NULL,
[Email] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[FromLabel] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [mailing].[Messages] ADD 
CONSTRAINT [PK_mailing.Messages] PRIMARY KEY CLUSTERED  ([Id])
GO
