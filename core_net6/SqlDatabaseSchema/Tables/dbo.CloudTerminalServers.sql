CREATE TABLE [dbo].[CloudTerminalServers]
(
[ID] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (2056) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudTerminalServers] ADD 
CONSTRAINT [PK_dbo.CloudTerminalServers] PRIMARY KEY CLUSTERED  ([ID])
GO
