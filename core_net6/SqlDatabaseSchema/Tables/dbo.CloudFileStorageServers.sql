CREATE TABLE [dbo].[CloudFileStorageServers]
(
[ID] [uniqueidentifier] NOT NULL,
[FileStoragePath] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NOT NULL,
[IsDefaultStorage] [bit] NULL,
[PhysicalPath] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CloudFileStorageServers] ADD 
CONSTRAINT [PK_dbo.CloudFileStorageServers] PRIMARY KEY CLUSTERED  ([ID])
GO
