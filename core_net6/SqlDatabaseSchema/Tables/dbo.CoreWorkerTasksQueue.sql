CREATE TABLE [dbo].[CoreWorkerTasksQueue]
(
[Id] [uniqueidentifier] NOT NULL,
[CoreWorkerTaskId] [uniqueidentifier] NOT NULL,
[Status] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[CreateDate] [datetime] NOT NULL,
[EditDate] [datetime] NULL,
[CoreWorkerId] [smallint] NOT NULL,
[Comment] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[TaskParams] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[DateTimeDelayOperation] [datetime] NULL,
[ExecutionResult] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CoreWorkerTasksQueue] ADD 
CONSTRAINT [PK_dbo.CoreWorkerTasksQueue] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_CoreWorkerId] ON [dbo].[CoreWorkerTasksQueue] ([CoreWorkerId])

CREATE NONCLUSTERED INDEX [IX_CoreWorkerTaskId] ON [dbo].[CoreWorkerTasksQueue] ([CoreWorkerTaskId])

ALTER TABLE [dbo].[CoreWorkerTasksQueue] ADD
CONSTRAINT [FK_dbo.CoreWorkerTasksQueue_dbo.CoreWorkerTask_CoreWorkerTaskId] FOREIGN KEY ([CoreWorkerTaskId]) REFERENCES [dbo].[CoreWorkerTask] ([ID])
ALTER TABLE [dbo].[CoreWorkerTasksQueue] ADD
CONSTRAINT [FK_dbo.CoreWorkerTasksQueue_dbo.CoreWorkers_CoreWorkerId] FOREIGN KEY ([CoreWorkerId]) REFERENCES [dbo].[CoreWorkers] ([Id])
GO
