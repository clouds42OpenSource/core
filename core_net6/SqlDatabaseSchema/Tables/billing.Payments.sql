CREATE TABLE [billing].[Payments]
(
[Id] [uniqueidentifier] NOT NULL,
[Number] [int] NOT NULL IDENTITY(1, 1),
[Account] [uniqueidentifier] NOT NULL,
[Sum] [money] NOT NULL,
[Date] [datetime] NOT NULL,
[Type] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Status] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL,
[PaymentSystem] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[OriginDetails] [nvarchar] (150) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ExchangeDataId] [uniqueidentifier] NULL,
[Service] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[ExternalPaymentNumber] [int] NULL
)
ALTER TABLE [billing].[Payments] ADD 
CONSTRAINT [PK_billing.Payments] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_Account] ON [billing].[Payments] ([Account])

ALTER TABLE [billing].[Payments] ADD
CONSTRAINT [FK_billing.Payments_dbo.Accounts_Account] FOREIGN KEY ([Account]) REFERENCES [dbo].[Accounts] ([Id])
GO
