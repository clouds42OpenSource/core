CREATE TABLE [billing].[InvoiceProducts]
(
[Id] [uniqueidentifier] NOT NULL,
[InvoiceId] [uniqueidentifier] NOT NULL,
[ServiceName] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ServiceSum] [money] NOT NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Count] [int] NOT NULL
)
ALTER TABLE [billing].[InvoiceProducts] ADD 
CONSTRAINT [PK_billing.InvoiceProducts] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_InvoiceId] ON [billing].[InvoiceProducts] ([InvoiceId])

ALTER TABLE [billing].[InvoiceProducts] ADD
CONSTRAINT [FK_billing.InvoiceProducts_billing.Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [billing].[Invoices] ([Id]) ON DELETE CASCADE
GO
