CREATE TABLE [integration].[McobJko]
(
[Id] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_McobJko_Id] DEFAULT (newid()),
[InfobaseName] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL
)
GO
ALTER TABLE [integration].[McobJko] ADD CONSTRAINT [PK_McobJko] PRIMARY KEY CLUSTERED  ([Id])
GO
