CREATE TABLE [dbo].[CloudChangesGroups]
(
[Id] [uniqueidentifier] NOT NULL,
[GroupName] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudChangesGroups] ADD 
CONSTRAINT [PK_dbo.CloudChangesGroups] PRIMARY KEY CLUSTERED  ([Id])
GO
