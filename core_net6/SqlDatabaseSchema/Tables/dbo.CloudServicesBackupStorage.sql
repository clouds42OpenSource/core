CREATE TABLE [dbo].[CloudServicesBackupStorage]
(
[ID] [uniqueidentifier] NOT NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[PhysicalPath] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CloudServicesBackupStorage] ADD 
CONSTRAINT [PK_dbo.CloudServicesBackupStorage] PRIMARY KEY CLUSTERED  ([ID])
GO
