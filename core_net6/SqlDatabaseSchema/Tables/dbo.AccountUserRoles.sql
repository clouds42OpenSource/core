CREATE TABLE [dbo].[AccountUserRoles]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountUserId] [uniqueidentifier] NOT NULL,
[AccountUserGroup] [int] NOT NULL
)
ALTER TABLE [dbo].[AccountUserRoles] ADD 
CONSTRAINT [PK_dbo.AccountUserRoles] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountUserId] ON [dbo].[AccountUserRoles] ([AccountUserId])

ALTER TABLE [dbo].[AccountUserRoles] ADD
CONSTRAINT [FK_dbo.AccountUserRoles_dbo.AccountUsers_AccountUserId] FOREIGN KEY ([AccountUserId]) REFERENCES [dbo].[AccountUsers] ([Id]) ON DELETE CASCADE
GO
