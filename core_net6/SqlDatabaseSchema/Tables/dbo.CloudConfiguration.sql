CREATE TABLE [dbo].[CloudConfiguration]
(
[Key] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudConfiguration] ADD 
CONSTRAINT [PK_dbo.CloudConfiguration] PRIMARY KEY CLUSTERED  ([Key])

GO
