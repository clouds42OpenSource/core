CREATE TABLE [dbo].[AcDbLocalUsers]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountDatabase_id] [uniqueidentifier] NOT NULL,
[AccountUser_id] [uniqueidentifier] NOT NULL,
[Login] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Password] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL,
[LinkLastPingDatetime] [datetime] NULL,
[LinkLastConnectionError] [nvarchar] (1024) COLLATE Cyrillic_General_CI_AS NULL,
[ConnectionState] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[IsDefault] [bit] NULL
)
ALTER TABLE [dbo].[AcDbLocalUsers] ADD 
CONSTRAINT [PK_dbo.AcDbLocalUsers] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountDatabase_id] ON [dbo].[AcDbLocalUsers] ([AccountDatabase_id])

CREATE NONCLUSTERED INDEX [IX_AccountUser_id] ON [dbo].[AcDbLocalUsers] ([AccountUser_id])

ALTER TABLE [dbo].[AcDbLocalUsers] ADD
CONSTRAINT [FK_dbo.AcDbLocalUsers_dbo.AccountDatabases_AccountDatabase_id] FOREIGN KEY ([AccountDatabase_id]) REFERENCES [dbo].[AccountDatabases] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AcDbLocalUsers] ADD
CONSTRAINT [FK_dbo.AcDbLocalUsers_dbo.AccountUsers_AccountUser_id] FOREIGN KEY ([AccountUser_id]) REFERENCES [dbo].[AccountUsers] ([Id])
GO
