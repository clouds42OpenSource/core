CREATE TABLE [dbo].[Locale]
(
[ID] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Currency] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NOT NULL,
[CurrencyCode] [int] NULL
)
ALTER TABLE [dbo].[Locale] ADD 
CONSTRAINT [PK_dbo.Locale] PRIMARY KEY CLUSTERED  ([ID])
GO
