CREATE TABLE [dbo].[CoreWorkers]
(
[Id] [smallint] NOT NULL IDENTITY(1, 1),
[Status] [smallint] NULL,
[Tick] [datetime] NULL,
[MaxThreadsCount] [smallint] NULL,
[BusyThreadsCount] [smallint] NULL
)
ALTER TABLE [dbo].[CoreWorkers] ADD 
CONSTRAINT [PK_dbo.CoreWorkers] PRIMARY KEY CLUSTERED  ([Id])
GO
