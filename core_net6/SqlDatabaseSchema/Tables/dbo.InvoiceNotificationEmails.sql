CREATE TABLE [dbo].[InvoiceNotificationEmails]
(
[Id] [uniqueidentifier] NOT NULL,
[Email] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[InvoiceNotificationId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[InvoiceNotificationEmails] ADD 
CONSTRAINT [PK_dbo.InvoiceNotificationEmails] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_InvoiceNotificationId] ON [dbo].[InvoiceNotificationEmails] ([InvoiceNotificationId])

ALTER TABLE [dbo].[InvoiceNotificationEmails] ADD
CONSTRAINT [FK_dbo.InvoiceNotificationEmails_dbo.InvoiceNotifications_InvoiceNotificationId] FOREIGN KEY ([InvoiceNotificationId]) REFERENCES [dbo].[InvoiceNotifications] ([Id])
GO
