CREATE TABLE [billing].[Invoices]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Date] [datetime2] NOT NULL,
[Sum] [money] NOT NULL,
[Requisite] [nvarchar] (500) COLLATE Cyrillic_General_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Comment] [nvarchar] (500) COLLATE Cyrillic_General_CI_AS NULL,
[State] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Uniq] [bigint] NOT NULL IDENTITY(1, 1),
[ActID] [uniqueidentifier] NULL,
[ActDescription] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[PaymentID] [uniqueidentifier] NULL,
[Period] [int] NULL
)
ALTER TABLE [billing].[Invoices] ADD 
CONSTRAINT [PK_billing.Invoices] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [billing].[Invoices] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_PaymentID] ON [billing].[Invoices] ([PaymentID])

ALTER TABLE [billing].[Invoices] ADD
CONSTRAINT [FK_billing.Invoices_billing.Payments_PaymentID] FOREIGN KEY ([PaymentID]) REFERENCES [billing].[Payments] ([Id])
ALTER TABLE [billing].[Invoices] ADD
CONSTRAINT [FK_billing.Invoices_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])



GO
