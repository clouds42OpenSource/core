CREATE TABLE [dbo].[CSResources]
(
[Id] [uniqueidentifier] NOT NULL,
[CloudServiceId] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[ResourcesName] [nvarchar] (100) COLLATE Cyrillic_General_CI_AS NULL,
[DaysAutoDecrease] [bit] NULL
)
ALTER TABLE [dbo].[CSResources] ADD 
CONSTRAINT [PK_dbo.CSResources] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_CloudServiceId] ON [dbo].[CSResources] ([CloudServiceId])

ALTER TABLE [dbo].[CSResources] ADD
CONSTRAINT [FK_dbo.CSResources_dbo.CloudServices_CloudServiceId] FOREIGN KEY ([CloudServiceId]) REFERENCES [dbo].[CloudServices] ([CloudServiceId])
GO
