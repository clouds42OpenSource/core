CREATE TABLE [dbo].[ApplyUpdateVersions]
(
[Id] [uniqueidentifier] NOT NULL,
[UpdateId] [uniqueidentifier] NOT NULL,
[ApplyVersion] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[ApplyUpdateVersions] ADD 
CONSTRAINT [PK_dbo.ApplyUpdateVersions] PRIMARY KEY CLUSTERED  ([Id])
GO
