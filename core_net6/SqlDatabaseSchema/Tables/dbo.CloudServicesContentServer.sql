CREATE TABLE [dbo].[CloudServicesContentServer]
(
[ID] [uniqueidentifier] NOT NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[CloudServicesContentServer] ADD 
CONSTRAINT [PK_dbo.CloudServicesContentServer] PRIMARY KEY CLUSTERED  ([ID])
GO
