CREATE TABLE [dbo].[Accounts]
(
[Id] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Inn] [nvarchar] (12) COLLATE Cyrillic_General_CI_AS NULL,
[ReferrerId] [uniqueidentifier] NULL,
[IndexNumber] [int] NOT NULL IDENTITY(1, 1),
[RegistrationDate] [datetime] NULL,
[Description] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Type] [nvarchar] (15) COLLATE Cyrillic_General_CI_AS NULL,
[Status] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[Removed] [bit] NULL,
[ProvidedServiceType] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[EаmployeesNumber] [int] NULL,
[WorkAverageCost] [int] NULL,
[WorkExperience] [int] NULL,
[ProvidedServiceVolume] [int] NULL,
[ActiveLink] [bit] NULL,
[MyDiskFilesSizeInMb] [bigint] NULL,
[MyDiskFilesSizeActualDateTime] [datetime] NULL,
[SaleManagerId] [uniqueidentifier] NULL,
[LocaleId] [uniqueidentifier] NULL,
[FreeSizePercent] [smallint] NULL,
[SegmentID] [uniqueidentifier] NULL,
[FileStorageID] [uniqueidentifier] NULL,
[IsVip] [bit] NULL,
[AdditionalResourceName] [nvarchar] (500) COLLATE Cyrillic_General_CI_AS NULL,
[AdditionalResourceCost] [decimal] (18, 0) NULL,
[Emails] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[FileStorageServerID] [uniqueidentifier] NULL,
[CloudStorageWebLink] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[Accounts] ADD 
CONSTRAINT [PK_dbo.Accounts] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_FileStorageID] ON [dbo].[Accounts] ([FileStorageID])

CREATE NONCLUSTERED INDEX [IX_LocaleId] ON [dbo].[Accounts] ([LocaleId])

CREATE NONCLUSTERED INDEX [IX_SegmentID] ON [dbo].[Accounts] ([SegmentID])

ALTER TABLE [dbo].[Accounts] ADD
CONSTRAINT [FK_dbo.Accounts_dbo.CloudServicesFileStorageServers_FileStorageID] FOREIGN KEY ([FileStorageID]) REFERENCES [dbo].[CloudServicesFileStorageServers] ([ID])
ALTER TABLE [dbo].[Accounts] ADD
CONSTRAINT [FK_dbo.Accounts_dbo.CloudServicesSegment_SegmentID] FOREIGN KEY ([SegmentID]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
ALTER TABLE [dbo].[Accounts] ADD
CONSTRAINT [FK_dbo.Accounts_dbo.Locale_LocaleId] FOREIGN KEY ([LocaleId]) REFERENCES [dbo].[Locale] ([ID])
GO
