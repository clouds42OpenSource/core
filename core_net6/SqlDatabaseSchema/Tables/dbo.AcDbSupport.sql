CREATE TABLE [dbo].[AcDbSupport]
(
[AccountDatabasesID] [uniqueidentifier] NOT NULL,
[Login] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Password] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[State] [int] NOT NULL,
[PrepearedForUpdate] [bit] NOT NULL,
[HasSupport] [bit] NOT NULL,
[HasAutoUpdate] [bit] NOT NULL,
[OldVersion] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[CurrentVersion] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[UpdateVersion] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[IsModificationDb] [bit] NULL,
[PreparedForTehSupport] [bit] NOT NULL,
[TehSupportDate] [date] NULL,
[UpdateVersionDate] [date] NULL,
[ConfigurationName] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AcDbSupport] ADD 
CONSTRAINT [PK_dbo.AcDbSupport] PRIMARY KEY CLUSTERED  ([AccountDatabasesID])
CREATE NONCLUSTERED INDEX [IX_AccountDatabasesID] ON [dbo].[AcDbSupport] ([AccountDatabasesID])

CREATE NONCLUSTERED INDEX [IX_ConfigurationName] ON [dbo].[AcDbSupport] ([ConfigurationName])

ALTER TABLE [dbo].[AcDbSupport] ADD
CONSTRAINT [FK_dbo.AcDbSupport_dbo.AccountDatabases_AccountDatabasesID] FOREIGN KEY ([AccountDatabasesID]) REFERENCES [dbo].[AccountDatabases] ([Id])
ALTER TABLE [dbo].[AcDbSupport] ADD
CONSTRAINT [FK_dbo.AcDbSupport_dbo.Configurations1C_ConfigurationName] FOREIGN KEY ([ConfigurationName]) REFERENCES [dbo].[Configurations1C] ([Name])



GO
