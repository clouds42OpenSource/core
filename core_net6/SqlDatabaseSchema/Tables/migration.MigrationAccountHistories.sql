CREATE TABLE [migration].[MigrationAccountHistories]
(
[HistoryId] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[SourceSegmentId] [uniqueidentifier] NOT NULL,
[TargetSegmentId] [uniqueidentifier] NOT NULL
)
ALTER TABLE [migration].[MigrationAccountHistories] ADD 
CONSTRAINT [PK_migration.MigrationAccountHistories] PRIMARY KEY CLUSTERED  ([HistoryId])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [migration].[MigrationAccountHistories] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_HistoryId] ON [migration].[MigrationAccountHistories] ([HistoryId])

CREATE NONCLUSTERED INDEX [IX_SourceSegmentId] ON [migration].[MigrationAccountHistories] ([SourceSegmentId])

CREATE NONCLUSTERED INDEX [IX_TargetSegmentId] ON [migration].[MigrationAccountHistories] ([TargetSegmentId])

ALTER TABLE [migration].[MigrationAccountHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountHistories_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
ALTER TABLE [migration].[MigrationAccountHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountHistories_dbo.CloudServicesSegment_SourceSegmentId] FOREIGN KEY ([SourceSegmentId]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
ALTER TABLE [migration].[MigrationAccountHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountHistories_dbo.CloudServicesSegment_TargetSegmentId] FOREIGN KEY ([TargetSegmentId]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
ALTER TABLE [migration].[MigrationAccountHistories] ADD
CONSTRAINT [FK_migration.MigrationAccountHistories_migration.MigrationHistories_HistoryId] FOREIGN KEY ([HistoryId]) REFERENCES [migration].[MigrationHistories] ([HistoryId])
GO
