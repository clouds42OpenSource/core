CREATE TABLE [dbo].[SystemServicesSegmentAvailableMigration]
(
[ID] [uniqueidentifier] NOT NULL,
[MasterSegmentID] [uniqueidentifier] NOT NULL,
[AvailableSegmentID] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[SystemServicesSegmentAvailableMigration] ADD 
CONSTRAINT [PK_dbo.SystemServicesSegmentAvailableMigration] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AvailableSegmentID] ON [dbo].[SystemServicesSegmentAvailableMigration] ([AvailableSegmentID])

CREATE NONCLUSTERED INDEX [IX_MasterSegmentID] ON [dbo].[SystemServicesSegmentAvailableMigration] ([MasterSegmentID])

ALTER TABLE [dbo].[SystemServicesSegmentAvailableMigration] ADD
CONSTRAINT [FK_dbo.SystemServicesSegmentAvailableMigration_dbo.CloudServicesSegment_AvailableSegmentID] FOREIGN KEY ([AvailableSegmentID]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
ALTER TABLE [dbo].[SystemServicesSegmentAvailableMigration] ADD
CONSTRAINT [FK_dbo.SystemServicesSegmentAvailableMigration_dbo.CloudServicesSegment_MasterSegmentID] FOREIGN KEY ([MasterSegmentID]) REFERENCES [dbo].[CloudServicesSegment] ([ID])
GO
