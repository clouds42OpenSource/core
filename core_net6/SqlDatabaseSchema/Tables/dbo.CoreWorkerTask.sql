CREATE TABLE [dbo].[CoreWorkerTask]
(
[ID] [uniqueidentifier] NOT NULL,
[TaskName] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Status] [bit] NULL,
[TaskType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[MethodName] [nvarchar] (400) COLLATE Cyrillic_General_CI_AS NULL,
[TaskParams] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CoreWorkerTask] ADD 
CONSTRAINT [PK_dbo.CoreWorkerTask] PRIMARY KEY CLUSTERED  ([ID])
GO
