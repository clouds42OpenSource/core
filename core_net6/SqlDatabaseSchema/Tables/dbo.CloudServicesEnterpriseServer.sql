CREATE TABLE [dbo].[CloudServicesEnterpriseServer]
(
[ID] [uniqueidentifier] NOT NULL,
[ConnectionAddress] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[Version] [nvarchar] (10) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Name] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[AdminName] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL,
[AdminPassword] [nvarchar] (250) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CloudServicesEnterpriseServer] ADD 
CONSTRAINT [PK_dbo.CloudServicesEnterpriseServer] PRIMARY KEY CLUSTERED  ([ID])
GO
