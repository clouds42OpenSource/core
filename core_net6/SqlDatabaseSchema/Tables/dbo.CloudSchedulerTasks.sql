CREATE TABLE [dbo].[CloudSchedulerTasks]
(
[ID] [uniqueidentifier] NOT NULL,
[TaskName] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[Status] [bit] NULL,
[CronDateTimePeriod] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[MethodType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL,
[MethodName] [nvarchar] (400) COLLATE Cyrillic_General_CI_AS NULL,
[MethodParams] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[CloudSchedulerTasks] ADD 
CONSTRAINT [PK_dbo.CloudSchedulerTasks] PRIMARY KEY CLUSTERED  ([ID])
GO
