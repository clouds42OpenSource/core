CREATE TABLE [dbo].[AccountCSResourceValues]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[CSResourceId] [uniqueidentifier] NOT NULL,
[InitiatorCloudService] [uniqueidentifier] NOT NULL,
[ModifyResourceValue] [int] NOT NULL,
[ModifyResourceDateTime] [datetime] NOT NULL,
[ModifyResourceComment] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[AccountCSResourceValues] ADD 
CONSTRAINT [PK_dbo.AccountCSResourceValues] PRIMARY KEY CLUSTERED  ([Id])
ALTER TABLE [dbo].[AccountCSResourceValues] ADD
CONSTRAINT [FK_dbo.AccountCSResourceValues_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
ALTER TABLE [dbo].[AccountCSResourceValues] ADD
CONSTRAINT [FK_dbo.AccountCSResourceValues_dbo.CSResources_CSResourceId] FOREIGN KEY ([CSResourceId]) REFERENCES [dbo].[CSResources] ([Id])
GO
CREATE NONCLUSTERED INDEX [Index_AccountId] ON [dbo].[AccountCSResourceValues] ([AccountId], [CSResourceId])
GO
