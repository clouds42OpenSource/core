CREATE TABLE [billing].[AccountRateTariffs]
(
[AccountId] [uniqueidentifier] NOT NULL,
[Service] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ResourceType] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NOT NULL,
[RateId] [uniqueidentifier] NOT NULL,
[TestRow] [nvarchar] (140) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [billing].[AccountRateTariffs] ADD 
CONSTRAINT [PK_billing.AccountRateTariffs] PRIMARY KEY CLUSTERED  ([AccountId], [Service], [ResourceType])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [billing].[AccountRateTariffs] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_RateId] ON [billing].[AccountRateTariffs] ([RateId])

ALTER TABLE [billing].[AccountRateTariffs] ADD
CONSTRAINT [FK_billing.AccountRateTariffs_billing.Rates_RateId] FOREIGN KEY ([RateId]) REFERENCES [billing].[Rates] ([Id])
ALTER TABLE [billing].[AccountRateTariffs] ADD
CONSTRAINT [FK_billing.AccountRateTariffs_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
