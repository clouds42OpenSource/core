CREATE TABLE [dbo].[LinkActivitySchedules]
(
[ID] [uniqueidentifier] NOT NULL,
[TimeToSleep] [float] NOT NULL,
[Downtime] [float] NOT NULL,
[AccountID] [uniqueidentifier] NULL
)
ALTER TABLE [dbo].[LinkActivitySchedules] ADD 
CONSTRAINT [PK_dbo.LinkActivitySchedules] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[LinkActivitySchedules] ([AccountID])

ALTER TABLE [dbo].[LinkActivitySchedules] ADD
CONSTRAINT [FK_dbo.LinkActivitySchedules_dbo.Accounts_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([Id])
GO
