CREATE TABLE [billing].[Resources]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Service] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Cost] [money] NOT NULL,
[Subject] [uniqueidentifier] NULL,
[Type] [nvarchar] (30) COLLATE Cyrillic_General_CI_AS NULL,
[DemoExpirePeriod] [datetime] NULL,
[IsFree] [bit] NULL,
[Tag] [int] NULL,
[AccountSponsorId] [uniqueidentifier] NULL
)
ALTER TABLE [billing].[Resources] ADD 
CONSTRAINT [PK_billing.Resources] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [billing].[Resources] ([AccountId])

CREATE NONCLUSTERED INDEX [IX_AccountSponsorId] ON [billing].[Resources] ([AccountSponsorId])

ALTER TABLE [billing].[Resources] ADD
CONSTRAINT [FK_billing.Resources_billing.BillingAccounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [billing].[BillingAccounts] ([Id])
ALTER TABLE [billing].[Resources] ADD
CONSTRAINT [FK_billing.Resources_dbo.Accounts_AccountSponsorId] FOREIGN KEY ([AccountSponsorId]) REFERENCES [dbo].[Accounts] ([Id])


GO
