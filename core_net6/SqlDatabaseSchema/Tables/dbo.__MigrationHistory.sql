CREATE TABLE [dbo].[__MigrationHistory]
(
[MigrationId] [nvarchar] (150) COLLATE Cyrillic_General_CI_AS NOT NULL,
[ContextKey] [nvarchar] (300) COLLATE Cyrillic_General_CI_AS NOT NULL,
[Model] [varbinary] (max) NOT NULL,
[ProductVersion] [nvarchar] (32) COLLATE Cyrillic_General_CI_AS NOT NULL,
[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF___MigrationHistory_CreatedOn] DEFAULT (sysutcdatetime())
)
GO
ALTER TABLE [dbo].[__MigrationHistory] ADD CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED  ([MigrationId], [ContextKey])
GO
