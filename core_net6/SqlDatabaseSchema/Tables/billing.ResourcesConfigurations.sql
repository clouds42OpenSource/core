CREATE TABLE [billing].[ResourcesConfigurations]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountId] [uniqueidentifier] NOT NULL,
[Service] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NULL,
[Cost] [money] NOT NULL,
[CostIsFixed] [bit] NULL,
[ExpireDate] [datetime] NULL,
[Frozen] [bit] NULL,
[DiscountGroup] [int] NULL,
[CreateDate] [datetime] NULL
)
ALTER TABLE [billing].[ResourcesConfigurations] ADD 
CONSTRAINT [PK_billing.ResourcesConfigurations] PRIMARY KEY CLUSTERED  ([Id])
ALTER TABLE [billing].[ResourcesConfigurations] ADD
CONSTRAINT [FK_billing.ResourcesConfigurations_billing.BillingAccounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [billing].[BillingAccounts] ([Id])
ALTER TABLE [billing].[ResourcesConfigurations] ADD
CONSTRAINT [FK_billing.ResourcesConfigurations_dbo.Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Accounts] ([Id])
GO
CREATE NONCLUSTERED INDEX [IX_AccountId] ON [billing].[ResourcesConfigurations] ([AccountId])
GO
