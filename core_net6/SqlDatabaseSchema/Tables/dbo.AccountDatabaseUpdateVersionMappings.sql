CREATE TABLE [dbo].[AccountDatabaseUpdateVersionMappings]
(
[ConfigurationName] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NOT NULL,
[CfuId] [uniqueidentifier] NOT NULL,
[TargetVersion] [nvarchar] (20) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[AccountDatabaseUpdateVersionMappings] ADD 
CONSTRAINT [PK_dbo.AccountDatabaseUpdateVersionMappings] PRIMARY KEY CLUSTERED  ([ConfigurationName], [CfuId], [TargetVersion])
CREATE NONCLUSTERED INDEX [IX_CfuId] ON [dbo].[AccountDatabaseUpdateVersionMappings] ([CfuId])

CREATE NONCLUSTERED INDEX [IX_ConfigurationName] ON [dbo].[AccountDatabaseUpdateVersionMappings] ([ConfigurationName])

ALTER TABLE [dbo].[AccountDatabaseUpdateVersionMappings] ADD
CONSTRAINT [FK_dbo.AccountDatabaseUpdateVersionMappings_dbo.Configurations1C_ConfigurationName] FOREIGN KEY ([ConfigurationName]) REFERENCES [dbo].[Configurations1C] ([Name])
ALTER TABLE [dbo].[AccountDatabaseUpdateVersionMappings] ADD
CONSTRAINT [FK_dbo.AccountDatabaseUpdateVersionMappings_dbo.ConfigurationsCfu_CfuId] FOREIGN KEY ([CfuId]) REFERENCES [dbo].[ConfigurationsCfu] ([Id])

GO
