CREATE TABLE [dbo].[AccountUserSessions]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountUserId] [uniqueidentifier] NOT NULL,
[Token] [uniqueidentifier] NOT NULL,
[StaticToken] [bit] NULL,
[TokenCreationTime] [datetime] NULL,
[ClientDescription] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[ClientDeviceInfo] [nvarchar] (200) COLLATE Cyrillic_General_CI_AS NULL,
[ClientIPAddress] [nvarchar] (50) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AccountUserSessions] ADD 
CONSTRAINT [PK_dbo.AccountUserSessions] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AccountUserId] ON [dbo].[AccountUserSessions] ([AccountUserId])

CREATE NONCLUSTERED INDEX [IX_AccountUserSessions_Token] ON [dbo].[AccountUserSessions] ([Token])

ALTER TABLE [dbo].[AccountUserSessions] ADD
CONSTRAINT [FK_dbo.AccountUserSessions_dbo.AccountUsers_AccountUserId] FOREIGN KEY ([AccountUserId]) REFERENCES [dbo].[AccountUsers] ([Id])


GO
