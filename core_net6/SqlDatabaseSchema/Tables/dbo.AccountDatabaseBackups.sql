CREATE TABLE [dbo].[AccountDatabaseBackups]
(
[Id] [uniqueidentifier] NOT NULL,
[AccountDatabaseId] [uniqueidentifier] NOT NULL,
[CreateDateTime] [datetime] NOT NULL,
[BackupPath] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NOT NULL,
[EventTrigger] [int] NOT NULL,
[InitiatorId] [uniqueidentifier] NOT NULL,
[SourceType] [int] NOT NULL
)
ALTER TABLE [dbo].[AccountDatabaseBackups] ADD 
CONSTRAINT [PK_dbo.AccountDatabaseBackups] PRIMARY KEY CLUSTERED  ([Id])
CREATE UNIQUE NONCLUSTERED INDEX [IX_Backup_CreateDateTime] ON [dbo].[AccountDatabaseBackups] ([AccountDatabaseId], [CreateDateTime])

CREATE NONCLUSTERED INDEX [IX_InitiatorId] ON [dbo].[AccountDatabaseBackups] ([InitiatorId])

ALTER TABLE [dbo].[AccountDatabaseBackups] ADD
CONSTRAINT [FK_dbo.AccountDatabaseBackups_dbo.AccountDatabases_AccountDatabaseId] FOREIGN KEY ([AccountDatabaseId]) REFERENCES [dbo].[AccountDatabases] ([Id])
ALTER TABLE [dbo].[AccountDatabaseBackups] ADD
CONSTRAINT [FK_dbo.AccountDatabaseBackups_dbo.AccountUsers_InitiatorId] FOREIGN KEY ([InitiatorId]) REFERENCES [dbo].[AccountUsers] ([Id])


GO
