CREATE TABLE [dbo].[AccountDomains]
(
[ID] [uniqueidentifier] NOT NULL,
[AccountID] [uniqueidentifier] NOT NULL,
[DomainName] [nvarchar] (255) COLLATE Cyrillic_General_CI_AS NULL,
[LinkID] [uniqueidentifier] NOT NULL
)
ALTER TABLE [dbo].[AccountDomains] ADD 
CONSTRAINT [PK_dbo.AccountDomains] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountID] ON [dbo].[AccountDomains] ([AccountID])

ALTER TABLE [dbo].[AccountDomains] ADD
CONSTRAINT [FK_dbo.AccountDomains_dbo.Accounts_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Accounts] ([Id]) ON DELETE CASCADE
GO
