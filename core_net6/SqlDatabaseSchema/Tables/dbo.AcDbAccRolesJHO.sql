CREATE TABLE [dbo].[AcDbAccRolesJHO]
(
[Id] [uniqueidentifier] NOT NULL,
[AcDbAccessesID] [uniqueidentifier] NOT NULL,
[RoleJHO] [nchar] (2500) COLLATE Cyrillic_General_CI_AS NOT NULL
)
ALTER TABLE [dbo].[AcDbAccRolesJHO] ADD 
CONSTRAINT [PK_dbo.AcDbAccRolesJHO] PRIMARY KEY CLUSTERED  ([Id])
CREATE NONCLUSTERED INDEX [IX_AcDbAccessesID] ON [dbo].[AcDbAccRolesJHO] ([AcDbAccessesID])

ALTER TABLE [dbo].[AcDbAccRolesJHO] ADD
CONSTRAINT [FK_dbo.AcDbAccRolesJHO_dbo.AcDbAccesses_AcDbAccessesID] FOREIGN KEY ([AcDbAccessesID]) REFERENCES [dbo].[AcDbAccesses] ([ID]) ON DELETE CASCADE


GO
