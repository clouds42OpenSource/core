CREATE TABLE [dbo].[AcDbSupportHistory]
(
[ID] [uniqueidentifier] NOT NULL,
[AccountDatabaseId] [uniqueidentifier] NOT NULL,
[State] [int] NOT NULL,
[Description] [nvarchar] (1000) COLLATE Cyrillic_General_CI_AS NOT NULL,
[EditDate] [datetime] NOT NULL,
[Operation] [int] NULL,
[CurrentVersionCfu] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL,
[OldVersionCfu] [nvarchar] (max) COLLATE Cyrillic_General_CI_AS NULL
)
ALTER TABLE [dbo].[AcDbSupportHistory] ADD 
CONSTRAINT [PK_dbo.AcDbSupportHistory] PRIMARY KEY CLUSTERED  ([ID])
CREATE NONCLUSTERED INDEX [IX_AccountDatabaseId] ON [dbo].[AcDbSupportHistory] ([AccountDatabaseId])

ALTER TABLE [dbo].[AcDbSupportHistory] ADD
CONSTRAINT [FK_dbo.AcDbSupportHistory_dbo.AcDbSupport_AccountDatabaseId] FOREIGN KEY ([AccountDatabaseId]) REFERENCES [dbo].[AcDbSupport] ([AccountDatabasesID]) ON DELETE CASCADE

GO
