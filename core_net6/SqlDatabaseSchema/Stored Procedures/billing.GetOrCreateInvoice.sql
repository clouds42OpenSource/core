SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        Владимир Шеин
-- Create date: 16.05.2018
-- Description:    Процедура получения существующего или создания нового счёта по указанным параметрам.
-- =============================================
CREATE PROCEDURE [billing].[GetOrCreateInvoice]
(
    @ExceptingState NVARCHAR(20),   -- Состояние счёта, с которым счета не попадут в выборку. По умолчанию равен "Processed"
    @ProcessingState NVARCHAR(20),  -- Состояние счёта, которое будет установлено после создания счёта. По умолчанию равен "Processing"
    @Sum Money,                     -- Сумма счёта
    @Now DATETIME,                  -- Текущие дата-время, передаваемые извне (поскольку могут не совпадать с часами на сервере, на котором запущен SQL-сервер)
    @Uniq BIGINT,                   -- Уникальный номер счёта. По нему осуществляется поиск в первую очередь
    @ForceCreate BIT,               -- Флаг (1 или 0), принудительного создания счёта (1 - принудительно (без проверки на существование), 0 - штатно, только если счёт не найден).
    @Inn NVARCHAR(12)               -- ИНН аккаунта по которому будет осуществляться поиск в случае, если поиск по Uniq будет неудачен. Кроме того, аккаунт с этим ИНН используется 
                                    -- при создании нового счёта
)
AS
BEGIN
    
    -- Табличная переменная с полями, совпадающими с полями таблицы billing.Invoices
    DECLARE @tInvoice TABLE 
    (
        [Id][uniqueidentifier],
        [AccountId][uniqueidentifier],
        [Date][datetime2](7),
        [Sum][money],
        [Requisite][nvarchar](500),
        [Description][nvarchar](max),
        [Comment][nvarchar](500),
        [State][nvarchar](20),
        [Uniq][bigint],
        [ActID][uniqueidentifier],
        [ActDescription][nvarchar](250),
        [PaymentID][uniqueidentifier],
        [Period][int]
    )

    -- Заполнение табличной переменной одной записью по результатам поиска по Uniq.
    INSERT INTO @tInvoice(
        [Id],
        [AccountId],
        [Date],
        [Sum],
        [Requisite],
        [Description],
        [Comment],
        [State],
        [Uniq],
        [ActID],
        [ActDescription],
        [PaymentID],
        [Period]
    )
    -- запрос данных по счетам по совпадающему Uniq среди всех записей, у которых bi.State не равно @ExceptingState.
    -- таблица billing.invoices имеет уникальный индекс IX_Invoices_Uniq, поэтому запрос всегда возвратит либо одну запись либо 
    -- ни одной
    SELECT bi.*
    FROM billing.invoices bi
    WHERE bi.Uniq = @Uniq AND (bi.State <> @ExceptingState OR @ForceCreate = 0)

    -- если по первому запросу записей не найдено
    IF NOT EXISTS (select * from @tInvoice)
    BEGIN
        -- Второй запрос по ИНН и сумме. Выбирается самая свежая запись (последняя по дате).
        INSERT INTO @tInvoice(
            [Id],
            [AccountId],
            [Date],
            [Sum],
            [Requisite],
            [Description],
            [Comment],
            [State],
            [Uniq],
            [ActID],
            [ActDescription],
            [PaymentID],
            [Period]
        )
        -- запрос одной строки данных по счетам по совпадающей сумме счёта среди всех записей, у которых bi.State не равно @ExceptingState,
        -- аккаунт совпадает с аккаунтом у которого ИНН равен @Inn. Сортировка выполняется по убыванию, поэтому первой
        -- записью из совпадающих будет самая свежая.
        SELECT TOP 1 bi.*
        FROM billing.invoices bi
        INNER JOIN Accounts a
        ON a.Id = bi.AccountId
        AND a.Inn = @Inn
        WHERE bi.[Sum] = @Sum AND (bi.State <> @ExceptingState OR @ForceCreate = 0)
        ORDER BY bi.Date DESC
    END

    -- если запись по параметрам в результате выполнения одного или двух запросов была найдена, то возвращаем её
    IF EXISTS(SELECT * FROM @tInvoice)
    BEGIN
        SELECT 
            ti.*
        FROM @tInvoice ti
            
        -- запись найдена, выходим
        RETURN
    END
    

	IF @ForceCreate = 0
    BEGIN
        DECLARE @msgError NVARCHAR(1024) = 'Не удалось получить даные по счету' + @Uniq + ' так как он не сущесвует';
        THROW 50000, @msgError, 1
		RETURN
    END

    -- Табличная переменная для сохранения id таблицы billing.Invoices после выполнения INSERT
    DECLARE @IDTABLE TABLE(ID UNIQUEIDENTIFIER)

    -- если запрошено форсированное создание счёта (параметр @ForceCreate=1) или
    -- на предыдущем шаге не было обнаружено счёта, соответствующего параметрам,
    -- пытаемся внести новую запись в таблицу Invoices, у которой 
    -- аккаунт равен аккаунту с ИНН равным параметру Inn, сумма счёта равна Sum
    -- поле Requisite заполняется параметром Inn, и дата выставляется 
    -- равной параметру @Now (поскольку дата на сервере БД может не совпадать с датой
    -- на сервере IIS), состояние счёта (поле State) выставляется равной параметру @ProcessingState.
    INSERT INTO billing.Invoices (
        [AccountId],
        [Date],
        [Sum],
        [Requisite],
        [State]
    )
    -- В таблицу @IDTABLE будет возвращён ID вставленной записи
    OUTPUT Inserted.Id INTO @IDTABLE
    SELECT
        a.Id As [Account],
        @Now As [Date],
        @Sum As [Sum],
        @Inn As [Requisite],
        @ProcessingState As [State]
    FROM Accounts a
    WHERE a.Inn = @Inn
    
    IF NOT EXISTS (SELECT * FROM @IDTABLE)
    BEGIN;
        DECLARE @msg NVARCHAR(1024) = 'Создание нового счёта прошло неудачно. Пожалуйста проверьте, что аккаунт для указанного ИНН=' + @Inn + ' существует';
        THROW 50000, @msg, 1
    END

    -- Возвращаем в выборке только что вставленную запись из таблицы billing.Invoices
    SELECT
        bi.*
    FROM billing.Invoices bi
    WHERE bi.Id = ANY(SELECT ID FROM @IDTABLE)
END
GO
