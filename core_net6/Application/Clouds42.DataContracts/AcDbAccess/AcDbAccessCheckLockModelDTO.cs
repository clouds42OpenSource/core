﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessCheckLockModelDto
    {
        [XmlElement(ElementName = nameof(AccountUserId))]
        [DataMember(Name = nameof(AccountUserId))]
        public Guid AccountUserId { get; set; }

        [XmlElement(ElementName = nameof(DatabaseId))]
        [DataMember(Name = nameof(DatabaseId))]
        public Guid DatabaseId { get; set; }
    }
}
