﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessPostSetAccIdDto
    {
        [XmlElement(ElementName = nameof(AccessID))]
        [DataMember(Name = nameof(AccessID))]
        public Guid AccessID { get; set; }

        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        [XmlElement(ElementName = nameof(SendNotification))]
        [DataMember(Name = nameof(SendNotification))]
        public bool? SendNotification { get; set; }

        public bool IsValid
        {
            get { return AccessID != Guid.Empty && AccountID != Guid.Empty; }
        }
    }
}


