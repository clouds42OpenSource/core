﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessPostIDDto
    {
        [XmlElement(ElementName = nameof(AccessID))]
        [DataMember(Name = nameof(AccessID))]
        public Guid AccessID { get; set; }

        public bool IsValid()
        {
            return AccessID != Guid.Empty;
        }
    }
}
