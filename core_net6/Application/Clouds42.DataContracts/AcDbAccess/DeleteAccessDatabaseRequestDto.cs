﻿using System;

namespace Clouds42.DataContracts.AcDbAccess
{

    /// <summary>
    /// Модель на заявку удаление доступа у базы.
    /// </summary>
    public class DeleteAccessDatabaseRequestDto
    {
        /// <summary>
        /// Идентификатор базы.
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public Guid AccountUserId { get; set; }

    }
}