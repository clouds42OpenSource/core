﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessAccountUserIDPostDto
    {
        [XmlElement(ElementName = nameof(AccessID))]
        [DataMember(Name = nameof(AccessID))]
        public Guid AccessID { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        public bool IsValid
        {
            get { return AccessID != Guid.Empty && AccountUserID != Guid.Empty; }
        }
    }
}

