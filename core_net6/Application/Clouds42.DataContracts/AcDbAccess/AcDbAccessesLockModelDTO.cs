﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessesLockModelDto
    {
        [XmlElement(ElementName = nameof(DbName))]
        [DataMember(Name = nameof(DbName))]
        public string DbName { get; set; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

    }
}