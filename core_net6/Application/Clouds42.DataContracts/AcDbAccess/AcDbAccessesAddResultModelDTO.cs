﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccessesAddResultModelDto
    {
        [XmlElement(ElementName = nameof(AccessID))]
        [DataMember(Name = nameof(AccessID))]
        public Guid AccessID { get; set; }

        public AcDbAccessesAddResultModelDto()
        {
        }

        public AcDbAccessesAddResultModelDto(Guid databaseAccessID)
        {
            AccessID = databaseAccessID;
        }
    }
}
