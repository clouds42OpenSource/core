﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccessLocalUserIdResultDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(LocalUserID))]
        [DataMember(Name = nameof(LocalUserID))]
        public Guid LocalUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbAccessLocalUserIdResultDto()
        {
            
        }

        public AcDbAccessLocalUserIdResultDto(Guid localUserID)
        {
            LocalUserID = localUserID;
        }
    }
}
