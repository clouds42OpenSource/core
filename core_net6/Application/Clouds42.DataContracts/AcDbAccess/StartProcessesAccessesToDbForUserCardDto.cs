﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// Модель для запуска процессов
    /// по управлению доступами в инф. базе
    /// </summary>
    public class StartProcessesAccessesToDbForUserCardDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid UsersId { get; set; }

        /// <summary>
        /// Список Id инф. баз 
        /// </summary>
        public List<Guid> DatabaseIds { get; set; } = [];

        /// <summary>
        /// Выдать доступл или забрать
        /// </summary>
        public bool GiveAccess { get; set; }
    }
}
