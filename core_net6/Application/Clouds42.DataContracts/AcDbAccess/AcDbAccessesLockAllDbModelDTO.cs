﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessesLockAllDbModelDto
    {
        [XmlElement(ElementName = nameof(DbName))]
        [DataMember(Name = nameof(DbName))]
        public string DbName { get; set; }
    }
}
