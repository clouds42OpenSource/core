﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccessAccountIdResultDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbAccessAccountIdResultDto()
        {
            
        }

        public AcDbAccessAccountIdResultDto(Guid accountID)
        {
            AccountID = accountID;
        }
    }
}

