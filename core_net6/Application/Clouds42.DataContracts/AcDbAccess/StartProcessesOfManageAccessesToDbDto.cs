﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// Модель для запуска процессов
    /// по управлению доступами в инф. базе
    /// </summary>
    public class StartProcessesOfManageAccessesToDbDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Список Id пользователей
        /// </summary>
        public List<Guid> UsersId { get; set; } = [];

        /// <summary>
        /// Выдать доступл или забрать
        /// </summary>
        public bool GiveAccess { get; set; }
    }
}
