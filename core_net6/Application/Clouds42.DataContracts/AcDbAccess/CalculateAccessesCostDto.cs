﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// Calculate cost of accesses in a configuration dto
    /// </summary>
    public class CalculateAccessesCostDto
    {
        /// <summary>
        /// Identifiers of services
        /// </summary>
        public List<Guid> ServiceTypesIdsList { get; set; }

        /// <summary>
        /// Account users
        /// </summary>
        public List<AccountUserAccessDto> AccountUserDataModelsList { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Service Paid Date
        /// </summary>
        public DateTime? PaidDate { get; set; }
    }
}
