﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class DeleteAccessDto
    {
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        [Required]
        public Guid? AccountId { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        [Required]
        public Guid? AccountDatabaseID { get; set; }
    }
}
