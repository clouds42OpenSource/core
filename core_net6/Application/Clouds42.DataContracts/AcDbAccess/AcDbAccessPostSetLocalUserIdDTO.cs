﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccessPostSetLocalUserIdDto
    {
        [XmlElement(ElementName = nameof(AccessID))]
        [DataMember(Name = nameof(AccessID))]
        public Guid AccessID { get; set; }

        [XmlElement(ElementName = nameof(LocalUserID))]
        [DataMember(Name = nameof(LocalUserID))]
        public Guid LocalUserID { get; set; }

        public bool IsValid
        {
            get { return AccessID != Guid.Empty && LocalUserID != Guid.Empty; }
        }
    }
}

