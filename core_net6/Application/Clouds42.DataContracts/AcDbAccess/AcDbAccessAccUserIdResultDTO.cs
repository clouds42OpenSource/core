﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccessAccUserIdResultDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbAccessAccUserIdResultDto()
        {
            
        }

        public AcDbAccessAccUserIdResultDto(Guid accountUserID)
        {
            AccountUserID = accountUserID;
        }
    }
}
