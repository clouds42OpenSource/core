﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AcDbAccess
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccessIdsDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccessIDs))]
        [DataMember(Name = nameof(AccessIDs))]
        public GuidListItemDto AccessIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbAccessIdsDto()
        {
            AccessIDs = new GuidListItemDto();
        }
    }
}
