﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Segment
{
    /// <summary>
    /// Параметры смены сегмента для аккаунтов
    /// </summary>
    [DataContract]
    public class SegmentMigrationParamsDto
    {
        /// <summary>
        /// Список аккаунтов которые нужно мигрировать.
        /// </summary>
        [DataMember]
        private List<Guid> _accountIds;

        /// <summary>
        /// Номер сегмента куда нужно мигрировать.
        /// </summary>
        [DataMember]
        private Guid _segmentToId;

        [DataMember]
        private Guid _initiatorId;

        /// <summary>
        /// Признак что необходимо использовать новый механизм
        /// </summary>
        [DataMember]
        public bool UseNewMechanism { get; set; }

        /// <summary>
        /// Инициатор миграции.
        /// </summary>
        
        public Guid InitiatorId
        {
            get => _initiatorId;
            set => _initiatorId = value;
        }

        /// <summary>
        /// Список аккаунтов которые нужно мигрировать.
        /// </summary>
        public List<Guid> AccountIds
        {
            get => _accountIds;
            set => _accountIds = value;
        }

        /// <summary>
        /// Номер сегмента куда нужно мигрировать.
        /// </summary>
        public Guid SegmentToId
        {
            get => _segmentToId;
            set => _segmentToId = value;
        }
    }
}
