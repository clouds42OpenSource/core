﻿using System;

namespace Clouds42.DataContracts.Segment
{
    /// <summary>
    ///  Структура для баз которые мигрировали
    /// </summary>
    public struct RollbackDataDto
    {
        public Guid DbId { get; private set; }
        public string OldDbPath { get; private set; }
        public Guid? OldStorageId { get; private set; }

        /// <summary>
        /// Данные ИБ при миграции необходимые для возврата в случае неудачной миграции
        /// </summary>
        /// <param name="dbId">индификатор базы</param>
        /// <param name="oldDbPath">старый путь до директории ИБ</param>
        /// <param name="oldStorageId">старый индификатор хранилища</param>
        public RollbackDataDto(Guid dbId, string oldDbPath, Guid? oldStorageId)
        {
            DbId = dbId;
            OldDbPath = oldDbPath;
            OldStorageId = oldStorageId;
        }
    }
}
