﻿namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Footer для казахской локали
    /// </summary>
    public class FooterKZDto : FooterDto
    {

        public FooterKZDto()
        {
            Phone = "+7 (727) 349-67-20";
        }
    }

    /// <summary>
    /// Footer для узбекской локали
    /// </summary>
    public class FooterUZDto : FooterDto
    {

        public FooterUZDto()
        {
            Phone = "+998 (71) 200-05-53";
        }
    }
}
