﻿namespace Clouds42.DataContracts.Navigation
{

    /// <summary>
    /// Footer для русской локали
    /// </summary>
    public class FooterRUDto : FooterDto
    {

        public FooterRUDto() 
        {
            Phone = "+7 (495) 252-70-42";
        }
    }
}
