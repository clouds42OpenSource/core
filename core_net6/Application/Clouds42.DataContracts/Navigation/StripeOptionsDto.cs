﻿using Clouds42.Common.Constants;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Модель параметров для страйпа
    /// </summary>
    public class StripeOptionsDto
    {
        /// <summary>
        /// Локаль
        /// </summary>
        public string Local { get; set; } = LocaleConst.Ukraine;

        /// <summary>
        /// Признак показывать флаги
        /// </summary>
        public bool ShowFlags { get; set; } = false;

        /// <summary>
        /// Признак Битрикс
        /// </summary>
        public bool IsBitrix { get; set; } = false;
    }
}
