﻿namespace Clouds42.DataContracts.Navigation
{

    /// <summary>
    /// Footer для украинской локали
    /// </summary>
    public class FooterUADto : FooterDto
    {

        public FooterUADto()
        {
            Phone = "+38 (095) 46-56-280";
        }

    }
}
