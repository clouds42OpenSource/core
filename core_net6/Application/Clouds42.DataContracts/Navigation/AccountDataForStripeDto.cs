﻿using System;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Модель данных аккаунта для страйпа
    /// </summary>
    public class AccountDataForStripeDto
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Индекс аккаунта
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Баланс
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserLogin { get; set; }
    }
}