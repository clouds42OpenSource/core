﻿namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Модель данных пользователя для страйпа
    /// </summary>
    public class UserDataForStripeDto
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserLogin { get; set; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public decimal Balance { get; set; }
    }
}