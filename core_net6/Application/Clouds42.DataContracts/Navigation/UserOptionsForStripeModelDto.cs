﻿using System;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Модель параметров пользователя для страйпа
    /// </summary>
    public class UserOptionsForStripeModelDto
    {
        /// <summary>
        /// Признак пользовать аутентифицирован
        /// </summary>
        public bool IsUserAuthenticated { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserLogin { get; set; }

        /// <summary>
        /// Id аккаунта из контекста
        /// </summary>
        public Guid? ContextAccountId { get; set; }

        /// <summary>
        /// Модель параметров для страйпа
        /// </summary>
        public StripeOptionsDto StripeOptionsDto { get; set; }
    }
}
