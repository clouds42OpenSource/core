﻿using System;
using System.Text;
using Clouds42.Common.Encrypt.Hashes;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Нижняя часть письма (подвал)
    /// </summary>
    public abstract class FooterDto
    {
        protected string Email = "support@42clouds.com";
        protected string UserHash;
        protected string Link;
        protected string CPMainPage = "https://42clouds.com/";
        protected string MainPage = "http://www.efsol.ru/";
        protected string Phone;

        /// <summary>
        /// Шаблон письма
        /// </summary>
        /// <param name="showUnsubscribe">Признак необходимости показывать ссылку на отказ от рассылки</param>
        /// <returns></returns>
        private string FooterTemplate(string siteAuthorityUrl, bool? showUnsubscribe)
        { 

            StringBuilder footer = new StringBuilder();
            footer.Append("<table style=\"margin: 0pt; padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; height: 100% !important; width: 100% !important; background: url('{0}') 0% 50% repeat scroll #f7f6f4;\" bgcolor=\"#f7f6f4\">");
            footer.Append("<tbody>");
            footer.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            footer.Append("<td align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">");
            footer.Append("<table style=\"padding: 0pt; width: 100%; border-collapse: collapse; vertical-align: top; text-align: left;\">");
            footer.Append("<tbody>");
            footer.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            footer.Append("<td height=\"50\" align=\"left\" style=\"padding: 0pt; height: 50px; vertical-align: top; text-align: left; border-collapse: collapse !important;\">&nbsp;</td>");
            footer.Append("</tr>");
            footer.Append("</tbody>");
            footer.Append("</table>");
            footer.Append("<center>");
            footer.Append("<table style=\"padding: 0pt; border-collapse: collapse; vertical-align: top; text-align: left; width: 699px; color: #6c6c6c; font-size: 12px; font-family: Arial, Helvetica, sans-serif; height: 92px;\">");
            footer.Append("<tbody>");
            footer.Append("<tr>");
            footer.Append("<td style=\"padding: 0pt; vertical-align: top; border-collapse: collapse !important; width: 300px;\">");
            footer.Append("<p style=\"margin-top: 0px;\" align=\"left\">Это автоматическая рассылка.<br />Пожалуйста, не отвечайте на нее.<br /><br />");
            if (showUnsubscribe == true)
                footer.Append("<a class=\"daria-goto-anchor\" style=\"color: #0f6fb3; text-decoration: none;\" href=\"{2}\" target=\"_blank\">Отписаться от рассылки</a></p>");
            footer.Append("</td>");
            footer.Append("<td style=\"width: 100px;\">");
            footer.Append("<table align=\"center\">");
            footer.Append("<tbody>");
            footer.Append("<tr>");
            footer.Append($"<td><a href=\"http://tele.click/H42CloudsBot\"><img src=\"{siteAuthorityUrl}/Content/img/upload/telegram.png\" alt=\"\" width=\"35\" height=\"35\" /> </a></td>");
            footer.Append("</tr>");
            footer.Append("</tbody>");
            footer.Append("</table>");
            footer.Append("</td>");
            footer.Append("<td align=\"right\" style=\"padding: 0pt; text-align: right; vertical-align: top; border-collapse: collapse !important;\">");
            footer.Append("<p style=\"margin-top: 0px;\"><a href=\"{0}\">www.42clouds.com</a><br />  phone: {3}<br /> e-mail: <a href=\"mailto: {4}\">support@42clouds.com</a><br />telegram: <a href=\"http://tele.click/H42CloudsBot\">@H42CloudsBot</a></p>");
            footer.Append("</td>");
            footer.Append("</tr>");
            footer.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            footer.Append("<td height=\"10\" align=\"left\" style=\"padding: 0pt; vertical-align: top; text-align: left; border-collapse: collapse !important;\">&nbsp;</td>");
            footer.Append("</tr>");
            footer.Append("<tr style=\"padding: 0pt; vertical-align: top; text-align: left;\" align=\"left\">");
            footer.Append("<td></td>");
            footer.Append("<td align=\"right\" style=\"padding: 0pt; vertical-align: top; border-collapse: collapse !important; text-align: center;\">&copy; 42Clouds</td>");
            footer.Append("<td></td>");
            footer.Append("</tr>");
            footer.Append("</tbody>");
            footer.Append("</table>");
            footer.Append("<p>&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>");
            footer.Append("<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>");
            footer.Append("</center></td>");
            footer.Append("</tr>");
            footer.Append("</tbody>");
            footer.Append("</table>");

            return footer.ToString();
        }

        /// <summary>
        /// Получить footer письма
        /// </summary>
        /// <param name="userGuid">ID пользователя</param>
        /// <param name="showUnsubscribe">Признак необходимости показывать ссылку на отказ от рассылки</param>
        /// <returns></returns>
        public virtual string GetFooter(Guid userGuid, bool? showUnsubscribe, string siteAuthorityUrl)
        {
            UserHash = SimpleIdHash.GetHashById(userGuid);
            Link = "https://cp.42clouds.com/unsubscription/?code=" + UserHash;

            return string.Format(FooterTemplate(siteAuthorityUrl, showUnsubscribe), CPMainPage, MainPage, Link, Phone, Email);
        }
    }
}
