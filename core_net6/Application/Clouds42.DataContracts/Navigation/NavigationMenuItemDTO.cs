﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Элемент меню 
    /// </summary>
    public sealed class NavigationMenuItemDto
    {
        /// <summary>
        /// Иконка для меню
        /// </summary>
        [XmlElement(ElementName = nameof(FaIcon))]
        [DataMember(Name = nameof(FaIcon))]
        public string FaIcon { get; set; }

        /// <summary>
        /// Текст меню
        /// </summary>
        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }

        /// <summary>
        /// Ссылка для меню
        /// </summary>
        [XmlElement(ElementName = nameof(Href))]
        [DataMember(Name = nameof(Href))]
        public string Href { get; set; }
    }

    /// <summary>
    /// Результат с меню
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public sealed class NavigationMenuDto
    {
        /// <summary>
        /// Начальное меню
        /// </summary>
        [XmlElement(ElementName = nameof(StartMenu))]
        [DataMember(Name = nameof(StartMenu))]
        public NavigationMenuItemDto StartMenu{ get; set; }

        /// <summary>
        /// Категория меню, используется в Cookies для раскрытия/скрытия меню 
        /// </summary>
        [XmlElement(ElementName = "CategoryKey ")]
        [DataMember(Name = nameof(CategoryKey))]
        public string CategoryKey { get; set; }

        /// <summary>
        /// Под меню
        /// </summary>
        [XmlElement(ElementName = nameof(Children))]
        [DataMember(Name = nameof(Children))]
        public NavigationMenuItemDto[] Children { get; set; }
    }
}