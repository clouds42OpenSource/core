﻿using System;

namespace Clouds42.DataContracts.Navigation
{
    /// <summary>
    /// Модель страйпа
    /// </summary>
    public class StripeDto
    {
        /// <summary>
        /// Признак пользователь залогинен
        /// </summary>
        public bool IsLogin { get; set; }

        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public string CurrentUser { get; set; }

        /// <summary>
        /// Текущий баланс пользователя
        /// </summary>
        public decimal CurrentUserBalance { get; set; }

        /// <summary>
        /// Признак показывать флаг
        /// </summary>
        public bool ShowFlag { get; set; }

        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Валюта локали
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Признак Битрикс
        /// </summary>
        public bool IsBitrix { get; set; }

        /// <summary>
        /// Название выбранного аккаунта
        /// </summary>
        public string SelectedAccName { get; set; }

        /// <summary>
        /// Индекс выбранного аккаунта
        /// </summary>
        public string SelectedAccIndex { get; set; }

        /// <summary>
        /// Id аккаунта из контекста
        /// </summary>
        public Guid ContextAccountId { get; set; }

        /// <summary>
        /// Id пользователя из контекста
        /// </summary>
        public Guid СurrentAccountUserId { get; set; }

        /// <summary>
        /// Email админа
        /// </summary>
        public string AccountAdminEmail { get; set; }

        /// <summary>
        /// Телефон админа
        /// </summary>
        public string AccountAdminPhone { get; set; }

        /// <summary>
        /// Признак админа
        /// </summary>
        public bool IsCurrentUserAccountAdmin { get; set; }

        
    }
}
