﻿using System;

namespace Clouds42.DataContracts.SupplierReference
{
    public class SupplierReferenceAddRefferalAccountDto
    {
        public Guid SupplierId { get; set; }

        public Guid AccountId { get; set; }
    }
}
