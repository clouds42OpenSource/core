﻿namespace Clouds42.DataContracts.Account
{
    public class RegistrationOptionsDto
    {
        public bool Activated { get; set; }
        public bool CreateInAd { get; set; }
        public bool CreateInCorp { get; set; }
    }
}
