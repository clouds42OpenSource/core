﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель реквизитов аккаунта
    /// </summary>
    public class AccountRequisitesDto
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежат реквизиты
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }
    }
}
