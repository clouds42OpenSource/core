﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling.Fillers
{
    /// <summary>
    /// Модель фильра списка транзакци и оказанных услуг
    /// </summary>
    public class ProvidedServiceAndTransactionsFilterDto
    {    
        /// <summary>
        /// Тип сервиса
        /// </summary>
        public Clouds42Service? Service { get; set; }

        /// <summary>
        /// Начальный период
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Конечный период
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }
    }
}