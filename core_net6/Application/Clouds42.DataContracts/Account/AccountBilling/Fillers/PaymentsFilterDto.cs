﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling.Fillers
{
    public class PaymentsFilterDto
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public Clouds42Service? Service { get; set; }
    }
}