﻿namespace Clouds42.DataContracts.Account.AccountBilling.Fillers
{
    /// <summary>
    /// Заполнитель модели фиксального чека для билдера
    /// </summary>
    internal static class InvoiceReceiptDocumentBuilderDto
    {
        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="invoiceReceiptDocumentBuilder">Модели фиксального чека для билдера</param>
        internal static void FillTestData(this InvoiceReceiptDocumentBuilderModel invoiceReceiptDocumentBuilder)
        {
            invoiceReceiptDocumentBuilder.QrData =
                "iVBORw0KGgoAAAANSUhEUgAAAFgAAAAkCAYAAAANdf2OAAAAAXNSR0IArs4c6QAAAARnQU1BAAC" +
                "xjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAKESURBVGhD7ZexceQwDEWvQffhIlyCK3ADzp" +
                "07durUoUNn7oDHN6c/i+MRJNYrzCV4MxytJJD4+CIp7a9WpFIGJ1MGJ1MGJ1MGJ1MGJ1MGJ1MGJ" +
                "1MGJ1MGJ1MGJ1MGJ1MGJxMz+O2ttcfHS3t+bu3z87hp+P5u7enpEsfv19d5bIQxL+cet2p8fz8C" +
                "BsZ421Z6DtYGf321dnfXo3oYA768/DnqGucWCuL6w8NFhBe7grz393+PxZFzrlusRmJGjRhtWWn" +
                "k+PFxBB7M4tVuNpiEtNlMUMFWkMSM8YplNkSQuWOxmk2WiEY7Oz2NnDPOeM+LD9J7OvB0RnEWiu" +
                "U+RQhPDEswKlJ56bPjTI3AauCeXSWr+AC9pwNLgIFXjDGeGJZtVGQkrzhTo2CFcB+zYRe/wVfHo" +
                "IhbMc7MmRiWOUvPzqIV9L8m9gyNFi+e1cJvNT2ADb2nwy3ixza+aFYQv8srIrE/NZgjeDUxaQL0" +
                "SAcG+al4njYzV1vDOCPZG+lrm8aI5BW3aNT5yLXxG3pPB72dV2i/EjMxsxcRywtjbNMXQySvOEu" +
                "jRZMifQ+WsNWnFcVF3rhj3ArljRR0pkZBrH1ou/gNvacDM46BKWIG+yr3I9+Y13xFaAzvRWe/jc" +
                "/UCIpnmxCr+AC95wIlZAkjkiQcOef6OCs9Mfoe9YwYUV7G1/7MVqO8lp1G7lukkeuMTWMCkCsar" +
                "zb+EZrQe25gIJYMSdQQo+VJIVqiEsNxZGbOCgxV0WqMMRvb08j1Ef2ZsI2+1DMbexavFpgwPSoI" +
                "JiLA7nfMFBU2K+YsZoXPmGn8z8QNXoG5LLPiH84xuHApg5Mpg5Mpg5Mpg5Mpg5Mpg5Mpg1Np7Tf" +
                "k032uLlmuTQAAAABJRU5ErkJggg==";

            invoiceReceiptDocumentBuilder.QrFormat = "png";
            invoiceReceiptDocumentBuilder.Rows =
            [
                new("КАССОВЫЙ ЧЕК/ПРИХОД", "10-10-10 10:10"),
                new("ООО ТЕСТОВАЯ КОМПАНИЯ", ""),
                new("Россия, город Москва, улица Тестовая, дом 10, строение 5", ""),
                new("МЕСТО РАСЧЕТОВ", "https://company.com"),
                new("АВТОМАТ", "720760"),
                new("MartService - Big", ""),
                new("ПРЕДОПЛАТА 100%", "~80.00"),
                new("УСЛУГА", ""),
                new("Кладовой_Тестовый - Мобильное ПО", ""),
                new("ПРЕДОПЛАТА 100%", "~800.00"),
                new("УСЛУГА", ""),
                new("Аренда 1С - Стандарт", ""),
                new("ПРЕДОПЛАТА 100%", "~27000.00"),
                new("УСЛУГА", ""),
                new("##BIG##ИТОГ", "~37230.00"),
                new("ВСЕГО ПОЛУЧЕНО", "~37230.00"),
                new("БЕЗНАЛИЧНЫМИ", "~37230.00"),
                new("СУММА БЕЗ НДС", "~37230.00"),
                new("ЭЛ.АДР.ПОКУПАТЕЛЯ", "grad@mail.ru"),
                new("ИНН", "1234567890"),
                new("СНО", "ОСН"),
                new("ЧЕК: 475", "СМЕНА: 877"),
                new("САЙТ ФНС", "www.nalog.ru"),
                new("РН ККТ: 2505480089011247", "ФН: 9999999999999242"),
                new("ФД: 470380", "ФП: 0957137350")
            ];
        }
    }
}
