﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Constants;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Billing.Supplier;

namespace Clouds42.DataContracts.Account.AccountBilling.Fillers
{
    /// <summary>
    /// Заполнитель модели счета
    /// </summary>
    internal static class InvoiceFillerDto
    {
        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="invoiceDc">Модели счета</param>
        /// <param name="localeName">Название локали</param>
        internal static void Fill(this InvoiceDc invoiceDc, string localeName = null)
        {
            if (string.IsNullOrEmpty(localeName))
            {
                Russian(invoiceDc);
                return;
            }

            Builders[localeName](invoiceDc);
        }

        /// <summary>
        /// Билдер тестовых данных
        /// </summary>
        private static readonly IDictionary<string, Action<InvoiceDc>> Builders = new Dictionary<string, Action<InvoiceDc>>
        {
            {LocaleConst.Russia, Russian},
            {LocaleConst.Ukraine, Ukraine},
            {LocaleConst.Kazakhstan, Kazakhstan},
            {LocaleConst.Uzbekistan, Uzbekistan}
        };

        /// <summary>
        /// Тестовая модель счета на оплату для России
        /// </summary>
        private static void Russian(InvoiceDc invoiceDc)
        {
            invoiceDc.Uniq = "Тест";
            invoiceDc.InvoiceDate = DateTime.Now;
            invoiceDc.BuyerDescription = "Иванов Иван Иванович";
            invoiceDc.AdditionalDataDescription = "Товар 1;Товар 2;Товар 3";
            invoiceDc.InvoiceSum = 1000;
            invoiceDc.Locale = new LocaleDto
            {
                Name = "ru-ru",
                Currency = "руб"
            };
            invoiceDc.SupplierRazorTemplate = new SupplierRazorTemplateDto();
        }

        /// <summary>
        /// Тестовая модель счета на оплату для Украины
        /// </summary>
        private static void Ukraine(InvoiceDc invoiceDc)
        {
            invoiceDc.Uniq = "Тест";
            invoiceDc.InvoiceDate = DateTime.Now;
            invoiceDc.BuyerDescription = "Петров Петро Петрович";
            invoiceDc.AdditionalDataDescription = "Оренда товару 1;Оренда товару 2;Оренда товару 3";
            invoiceDc.InvoiceSum = 1000;
            invoiceDc.Locale = new LocaleDto
            {
                Name = "ru-ua",
                Currency = "грн"
            };
            invoiceDc.SupplierRazorTemplate = new SupplierRazorTemplateDto();
        }

        /// <summary>
        /// Тестовая модель счета на оплату для Казахстана
        /// </summary>
        private static void Kazakhstan(InvoiceDc invoiceDc)
        {
            invoiceDc.Uniq = "Тестілеу";
            invoiceDc.InvoiceDate = DateTime.Now;
            invoiceDc.BuyerDescription = "Мырдаев Еражан Кульбитович";
            invoiceDc.AdditionalDataDescription = "Өнім 1;Өнім 2;Өнім 3";
            invoiceDc.InvoiceSum = 1000;
            invoiceDc.Locale = new LocaleDto
            {
                Name = "ru-kz",
                Currency = "тг"
            };
            invoiceDc.SupplierRazorTemplate = new SupplierRazorTemplateDto();
        }

        /// <summary>
        /// Тестовая модель счета на оплату для Узбекистана
        /// </summary>
        private static void Uzbekistan(InvoiceDc invoiceDc)
        {
            invoiceDc.Uniq = "Sinov";
            invoiceDc.InvoiceDate = DateTime.Now;
            invoiceDc.BuyerDescription = "Мырдаев Еражан Кульбитович";
            invoiceDc.AdditionalDataDescription = "1-mahsulot; Mahsulot 2;";
            invoiceDc.InvoiceSum = 1000;
            invoiceDc.Locale = new LocaleDto
            {
                Name = "ru-uz",
                Currency = "сум"
            };
            invoiceDc.SupplierRazorTemplate = new SupplierRazorTemplateDto();
        }

    }
}
