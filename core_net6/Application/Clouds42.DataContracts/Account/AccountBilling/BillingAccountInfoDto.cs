﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Информация об аккаунте биллинга
    /// </summary>
    public class BillingAccountInfoDto
    {
        /// <summary>
        /// Данные биллинга
        /// </summary>
        public BillingDataDto BillingData { get; set; }

        /// <summary>
        /// Счета
        /// </summary>
        public PagitationCollection<InvoiceInfoDto> Invoices { get; set; }
    }
}