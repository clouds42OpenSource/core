﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель создания нового ресурса
    /// </summary>
    public class CreateResourceDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id аккаунта спонсора
        /// </summary>
        public Guid? AccountSponsorId { get; set; }

        /// <summary>
        /// Id субъекта, который использует ресурс
        /// </summary>
        public Guid? SubjectId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }

        /// <summary>
        /// Ресурс свободен
        /// </summary>
        public bool? IsFree { get; set; }

        /// <summary>
        /// Стоимость ресурса
        /// </summary>
        public decimal Cost { get; set; }
    }
}
