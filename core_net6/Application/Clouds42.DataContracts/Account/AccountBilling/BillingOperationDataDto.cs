﻿using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель операций биллинга
    /// </summary>
    public class BillingOperationDataDto
    {    
        /// <summary>
        /// Список транзакций аккаунта
        /// </summary>
        public PagitationCollection<BillingOperationItemDto> Transactions { get; set; }

        /// <summary>
        /// Текущий баланс
        /// </summary>
        public decimal CurrentBalance { get; set; }

        /// <summary>
        /// Баланс бонусов
        /// </summary>
        public decimal BonusBalance { get; set; }

        /// <summary>
        /// Локаль аккаунта
        /// </summary>
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Признак необходимости показывать баланс
        /// </summary>
        public bool ShowBalance { get; set; }
    }
}