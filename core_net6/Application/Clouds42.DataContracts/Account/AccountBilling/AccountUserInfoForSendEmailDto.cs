﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Информация о пользователе для отправки письма
    /// </summary>
    public class AccountUserInfoForSendEmailDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Адрес пользователя
        /// </summary>
        public string AccountUserEmail { get; set; }

        /// <summary>
        /// Признак что пользователь является администратором аккаунта
        /// </summary>
        public bool IsAccountAdmin { get; set; }
    }
}
