﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    ///     Данные фискализации
    /// </summary>
    [DataContract]
    public class InvoiceReceiptFiscalDataDto
    {
        /// <summary>
        ///     Статус
        /// </summary>
        [DataMember]
        public bool IsSuccess { get; set; }

        /// <summary>
        ///     Ошибки
        /// </summary>
        [DataMember]
        public string Errors { get; set; }

        /// <summary>
        ///     Время создания документа
        /// </summary>
        [DataMember]
        public DateTime? DocumentDateTime { get; set; }

        /// <summary>
        ///     Полная инфомация о чеке
        /// </summary>
        [DataMember]
        public string ReceiptInfo { get; set; }

        /// <summary>
        ///     Данные QR кода (Base64)
        /// </summary>
        [DataMember]
        public string QrCodeData { get; set; }

        /// <summary>
        ///     Формат QR кода
        /// </summary>
        [DataMember]
        public string QrCodeFormat { get; set; }

        /// <summary>
        ///     Номер фискального документа
        /// </summary>
        [DataMember]
        public string FiscalNumber { get; set; }

        /// <summary>
        ///     Номер фискального накопителя, в котором был сформирован документ
        /// </summary>
        [DataMember]
        public string FiscalSerial { get; set; }

        /// <summary>
        ///     Фискальный признак документа
        /// </summary>
        [DataMember]
        public string FiscalSign { get; set; }
    }
}