﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель операции биллинга
    /// </summary>
    public class BillingOperationItemDto 
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Название операции
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaySum { get; set; }
        
        /// <summary>
        /// Остаток после проведенной транзакции
        /// </summary>
        public decimal? Remains { get; set; }

        /// <summary>
        /// Статус платежа
        /// </summary>
        public PaymentStatus Status { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType TransactionType { get; set; }
    }
}
