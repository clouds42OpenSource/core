﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Данные о ресурсах по услуге
    /// </summary>
    public class ResourcesDataByServiceTypeDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Общая стоимость ресурсов
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Объем(количество ресурсов) 
        /// </summary>
        public int VolumeInQuantity { get; set; }
    }
}
