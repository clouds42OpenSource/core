﻿
namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Информация о периоде оплаты
    /// </summary>
    public class PayPeriodInfoDto
    {
        /// <summary>
        /// Описание периода
        /// </summary>
        public string PeriodDescription { get; set; }

        /// <summary>
        /// Бонусное вознаграждение
        /// </summary>
        public int BonusReward { get; set; }

        /// <summary>
        /// Период оплаты
        /// </summary>
        public int Period { get; set; }
    }
}
