﻿
namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель данных баланса аккаунта
    /// </summary>
    public class AccountBalanceDataDto
    {
        /// <summary>
        /// Денежный баланс
        /// </summary>
        public decimal MoneyBalance { get; set; }
        
        /// <summary>
        /// Бонусный баланс
        /// </summary>
        public decimal BonusBalance { get; set; }

        /// <summary>
        /// Замороженный баланс
        /// </summary>
        public decimal FrozenBalance { get; set; }

        /// <summary>
        /// Доступный баланс для расходования
        /// </summary>
        public decimal AvailableBalance => MoneyBalance + BonusBalance - FrozenBalance;
    }
}
