﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Сгрупированные ресурсы по аккаунту
    /// </summary>
    public class GroupedResourcesByAccountDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Ресурсы услуг
        /// </summary>
        public List<Resource> Resources { get; set; }
    }
}
