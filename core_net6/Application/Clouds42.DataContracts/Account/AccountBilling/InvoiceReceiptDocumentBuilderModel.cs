﻿using System.Collections.Generic;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    ///     Модель фиксального чека для билдера PDF
    /// </summary>
    public class InvoiceReceiptDocumentBuilderModel : IPrintedHtmlFormModelDto
    {
        /// <summary>
        ///     Строки чека
        /// </summary>
        public List<InvoiceReceiptDocumentBuilderModelRowDto> Rows { get; set; }

        /// <summary>
        ///     Формат QR кода
        /// </summary>
        public string QrFormat { get; set; }

        /// <summary>
        ///     Данные QR кода
        /// </summary>
        public string QrData { get; set; }

        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="siteAuthorityUrl">url авторизации</param>
        /// <param name="localeName">Название локали</param>
        public void FillTestData(string siteAuthorityUrl = null, string localeName = null, string supplierReferenceName = null)
        {
            InvoiceReceiptDocumentBuilderDto.FillTestData(this);
        }

        /// <summary>
        /// Получить название модели
        /// </summary>
        /// <returns>Название модели</returns>
        public string GetModelName() => "Фиксальный чек счета";
    }
}
