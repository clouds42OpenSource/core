﻿using System;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Маппер модели создания ресурса услуги
    /// </summary>
    public static class CreateResourceMapperDto
    {
        /// <summary>
        /// Выполнить маппинг к модели ресурса
        /// </summary>
        /// <param name="createResourceDc">Модель создания нового ресурса</param>
        /// <returns>Ресурс услуги</returns>
        public static Resource MapToResource(this CreateResourceDto createResourceDc) =>
            new()
            {
                Id = Guid.NewGuid(),
                AccountId = createResourceDc.AccountId,
                AccountSponsorId = createResourceDc.AccountSponsorId,
                BillingServiceTypeId = createResourceDc.BillingServiceTypeId,
                IsFree = createResourceDc.IsFree,
                Subject = createResourceDc.SubjectId,
                Cost = createResourceDc.Cost
            };
    }
}