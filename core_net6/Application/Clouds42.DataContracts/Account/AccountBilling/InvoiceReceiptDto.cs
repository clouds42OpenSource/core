﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Clouds42.DataContracts.Billing.Supplier;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    ///     Фискальный чек для счета
    /// </summary>
    [DataContract]
    public class InvoiceReceiptDto
    {
        /// <summary>
        ///     ID счета
        /// </summary>
        [DataMember]
        public Guid InvoiceId { get; set; }

        /// <summary>
        ///     Локаль
        /// </summary>
        [DataMember]
        public string LocaleName { get; set; }

        /// <summary>
        ///     Продукты
        /// </summary>
        [DataMember]
        public List<InvoiceProductDto> Products { get; set; }

        /// <summary>
        ///     Клиентский email
        /// </summary>
        [DataMember]
        public string ClientEmail { get; set; }

        /// <summary>
        ///     Наличие метода создания чека для текущей модели
        /// </summary>
        [DataMember]
        public bool FiscalAnyBuilder { get; set; }

        /// <summary>
        ///     Данные о фискализации
        /// </summary>
        [DataMember]
        public InvoiceReceiptFiscalDataDto FiscalData { get; set; }

        /// <summary>
        /// Данные о поставщике
        /// </summary>
        [DataMember]
        public SupplierRazorTemplateDto SupplierRazorTemplate { get; set; }
    }
}