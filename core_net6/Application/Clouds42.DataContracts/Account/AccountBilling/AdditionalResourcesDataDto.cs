﻿
namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель данных о дополнительных ресурсах аккаунта
    /// </summary>
    public class AdditionalResourcesDataDto
    {
        /// <summary>
        /// Описание доп. ресурсов
        /// </summary>
        public string AdditionalResourceName { get; set; }

        /// <summary>
        /// Стоимость доп. ресурсов
        /// </summary>
        public decimal? AdditionalResourceCost { get; set; }

        /// <summary>
        /// Признак, указывающий что аккаунт VIP
        /// (для определения наличия доп. ресурсов)
        /// </summary>
        public bool IsVipAccount { get; set; }

        /// <summary>
        /// Наличие дополнительных ресурсов для аккаунта
        /// </summary>
        public bool IsAvailabilityAdditionalResources => IsVipAccount && !string.IsNullOrEmpty(AdditionalResourceName);
    }
}
