﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Регулярный/ежемесечный платеж аккаунта
    /// </summary>
    public class AccountRegularPaymentDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Сумма регулярного платежа
        /// </summary>
        public decimal RegularPaymentSum { get; set; }
    }
}
