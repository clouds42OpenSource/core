﻿using System.Collections.Generic;
using Clouds42.DataContracts.Billing;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель информации для формы пополнения баланса
    /// </summary>
    public class ReplenishBalanceInfoDto
    {
        /// <summary>
        /// Название поставщика
        /// </summary>
        public string SupplierName { get; set; }
        
        /// <summary>
        /// Название покупателя
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Основные сервисы биллинга
        /// </summary>
        public ViewPageBaseBillingServicesDto BaseBillingServices { get; set; }

        /// <summary>
        /// Дополнительные сервисы биллинга
        /// </summary>
        public EsdlAvailableRatesDto AdditionalBillingServices { get; set; }

        /// <summary>
        /// Список пользователей для отправки письма
        /// </summary>
        public List<AccountUserInfoForSendEmailDto> ListOfUsersForSendEmail { get; set; } = [];

        /// <summary>
        /// Список периодов оплаты
        /// </summary>
        public List<PayPeriodInfoDto> PayPeriodsList { get; set; }

        /// <summary>
        /// Признак, что используется ОП
        /// </summary>
        public bool IsPromisePayment { get; set; }

        /// <summary>
        /// Тип счета на оплату
        /// </summary>
        public InvoicePurposeType? InvoiceType { get; set; }

        /// <summary>
        /// Модель предлагаемого платежа
        /// </summary>
        public SuggestedPaymentModelDto SuggestedPayment { get; set; }

        /// <summary>
        /// Дополнительные ресурсы для Аренды 1С
        /// </summary>
        public BillingServiceTypeResourceDto AdditionalResourcesForRent1C { get; set; }

        /// <summary>
        /// Аккаунт является вип
        /// </summary>
        public bool AccountIsVip { get; set; }
    }
}
