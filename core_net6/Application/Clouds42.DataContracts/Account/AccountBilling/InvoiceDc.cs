﻿using System;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Billing.Supplier;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель счета
    /// </summary>
    public class InvoiceDc: IPrintedHtmlFormModelDto
    {
        /// <summary>
        /// Id счета
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Модель шаблона счета на оплату
        /// </summary>
        public SupplierRazorTemplateDto SupplierRazorTemplate { get; set; }

        /// <summary>
        /// Дата счета
        /// </summary>
        public DateTime InvoiceDate { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        public string Requisites { get; set; }

        /// <summary>
        /// Статус счета
        /// </summary>
        public InvoiceStatus State { get; set; }

        /// <summary>
        /// Возможность перезаписи
        /// </summary>
        public bool Overwrite { get; set; }

        /// <summary>
        /// Первый за период
        /// </summary>
        public bool FirstInPeriod { get; set; }

        /// <summary>
        /// Месяц
        /// </summary>
        public int Months { get; set; }

        /// <summary>
        /// Префикс
        /// </summary>
        public string Uniq { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public LocaleDto Locale { get; set;}

        /// <summary>
        /// Описание акта
        /// </summary>
        public string ActDescription { get; set; }

        /// <summary>
        /// Id акта
        /// </summary>
        public Guid? ActId { get; set; }

        /// <summary>
        /// Описание покупателя
        /// </summary>
        public string BuyerDescription { get; set; }

        /// <summary>
        /// Это новый счет
        /// </summary>
        public bool IsNewInvoice { get; set; }

        /// <summary>
        /// Дополнительное описание
        /// </summary>
        public string AdditionalDataDescription { get; set; }

        /// <summary>
        /// Номер фиксального чека
        /// </summary>
        public string ReceiptFiscalNumber { get; set; }

        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Тестовые данные</returns>
        public void FillTestData(string siteAuthorityUrl = null, string localeName = null, string supplierReferenceName = null)
        {
            this.Fill(localeName: localeName);
        }

        /// <summary>
        /// Получить название модели
        /// </summary>
        /// <returns>Название модели</returns>
        public string GetModelName() => "Счет на оплату";
    }
}
