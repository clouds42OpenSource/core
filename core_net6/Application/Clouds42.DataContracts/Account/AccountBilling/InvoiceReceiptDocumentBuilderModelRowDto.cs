﻿namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    ///     Строка чека
    /// </summary>
    public class InvoiceReceiptDocumentBuilderModelRowDto(string key, string value)
    {
        /// <summary>
        ///     Ключ
        /// </summary>
        public string Key { get; set; } = key;

        /// <summary>
        ///     Значение
        /// </summary>
        public string Value { get; set; } = value;
    }
}