﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель ресурсов сгруппированных по сервису
    /// </summary>
    public class GroupedResourcesByServiceDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Список ресурсов
        /// </summary>
        public List<Resource> Resources { get; set; } = [];
    }
}
