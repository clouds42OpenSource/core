﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель для обновления дополнительных ресурсов аккаунта
    /// </summary>
    public class UpdateAdditionalResourcesForAccountDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Описание доп. ресурсов
        /// </summary>
        public string AdditionalResourceName { get; set; }

        /// <summary>
        /// Стоимость доп. ресурсов
        /// </summary>
        public decimal? AdditionalResourceCost { get; set; }
    }
}
