﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель данных ресурса для отчета
    /// </summary>
    public class ResourceReportDataDto
    {
        /// <summary>
        /// Id ресурса
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Название лицензии
        /// </summary>
        [DataMember]
        public string LicenseName { get; set; }

        /// <summary>
        /// Тип лицензии
        /// </summary>
        [DataMember]
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [DataMember]
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [DataMember]
        public string BillingServiceName { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        [DataMember]
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        [DataMember]
        public decimal Cost { get; set; }

        /// <summary>
        /// Номер аккаунта спонсора (если спонсируется)
        /// </summary>
        [DataMember]
        public int? SponsorAccountIndexNumber { get; set; }

        /// <summary>
        /// ФИО сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerFullName
        {
            get
            {
                return SaleManager?.AccountUser?.FullName ??
                       $"{SaleManager?.AccountUser?.LastName} {SaleManager?.AccountUser?.FirstName} {SaleManager?.AccountUser?.MiddleName}";
            }
        }

        /// <summary>
        /// Отдел сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerDivision
        {
            get
            {
                return SaleManager?.Division;
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        public AccountSaleManager SaleManager { get; set; }
    }
}
