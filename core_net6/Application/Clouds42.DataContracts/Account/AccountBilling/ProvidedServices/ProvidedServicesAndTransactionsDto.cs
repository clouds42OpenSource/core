﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Модель транзакций и оказанных услуг
    /// </summary>
    public class ProvidedServicesAndTransactionsDto
    {
        /// <summary>
        /// Модель операций биллинга
        /// </summary>
        public BillingOperationDataDto TransactionsData { get; set; }

        /// <summary>
        /// Список оказанных услуг
        /// </summary>
        public PagitationCollection<ProvidedServiceDto> ProvidedServices { get; set; }

        /// <summary>
        /// Список длительных оказанных услуг (ЗД и рекогнишен)
        /// </summary>
        public PagitationCollection<ProvidedServiceDto> ProvidedLongServices { get; set; }
    }
}