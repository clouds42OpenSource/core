﻿namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Модель регистрации пролонгации сервиса
    /// </summary>
    public class RegisterProlongServiceItemDto : ServiceItemDto
    {
        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int Count { get; set; }
    }
}
