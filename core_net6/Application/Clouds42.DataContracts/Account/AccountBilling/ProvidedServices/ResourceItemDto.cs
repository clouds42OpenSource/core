﻿using System;
using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Модель представления ресурса
    /// </summary>
    public class ResourceItemDto(Resource resource)
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; } = resource.AccountId;

        /// <summary>
        /// ID аккаунта спонсора
        /// </summary>
        public Guid? AccountSponsorId { get; set; } = resource.AccountSponsorId;

        /// <summary>
        /// Стоимость лицензии
        /// </summary>
        public decimal Cost { get; set; } = resource.Cost;

        /// <summary>
        /// Тип лицензии
        /// </summary>
        public ResourceType ResourceType { get; set; } = resource.BillingServiceType.SystemServiceType.GetValueOrDefault();
    }
}
