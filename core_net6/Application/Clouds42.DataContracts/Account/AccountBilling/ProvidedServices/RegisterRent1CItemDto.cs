﻿namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Модель регистрации покупки лицензии Аренды 1С
    /// </summary>
    public class RegisterRent1CItemDto : ServiceItemDto
    {
        /// <summary>
        /// Старая модель регистрации
        /// </summary>
        public RegisterRent1CItemDto Old { get; set; }
    }
}
