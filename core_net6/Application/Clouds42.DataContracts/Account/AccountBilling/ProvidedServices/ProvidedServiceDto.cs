﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Модель представления оказанной услуги
    /// </summary>
    public class ProvidedServiceDto
    {
        /// <summary>
        /// Признак что запись для истории
        /// </summary>
        public bool IsHistoricalRow { get; set; }

        /// <summary>
        /// Описание оказанной услуги
        /// </summary>
        public string ServiceDescription { get; set; }

        /// <summary>
        /// Тип сервиса
        /// </summary>
        public Clouds42Service Service { get; set; }

        /// <summary>
        /// Тип услуги сервиса
        /// </summary>
        public ResourceType ResourceType { get; set; }

        /// <summary>
        /// Начальный период действия
        /// </summary>
        public DateTime? From { get; set; }

        /// <summary>
        /// Конечный период действия
        /// </summary>
        public DateTime? To { get; set; }

        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Тариф
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Источник платежа
        /// </summary>
        public bool FromCorp { get; set; }

        /// <summary>
        /// Аккаунт спонсора
        /// </summary>
        public string SposoredAccountName { get; set; }
    }
}