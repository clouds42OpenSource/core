﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Account.AccountBilling.ProvidedServices
{
    /// <summary>
    /// Элемент сервиса
    /// </summary>
    public abstract class ServiceItemDto
    {
        /// <summary>
        /// Тип ресурса
        /// </summary>
        public ResourceType ResourceType { get; set; }

        /// <summary>
        /// Тариф
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// ID аккаунта спонсора
        /// </summary>
        public Guid? SponsoredAccountId { get; set; }
    }
}
