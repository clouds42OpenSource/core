﻿using System;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    /// Модель создания тарифа на аккаунт
    /// </summary>
    public class CreateAccountRateDto
    {
        /// <summary>
        /// Id услуги сервиса
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Стоимость тарифа на аккаунт
        /// </summary>
        public decimal Cost { get; set; }
    }
}
