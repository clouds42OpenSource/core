﻿namespace Clouds42.DataContracts.Account.AccountBilling.Extensions
{
    /// <summary>
    /// Расширения для модели BillingAccount
    /// </summary>
    public static class BillingAccountExtensionsDto
    {
        /// <summary>
        /// Получить сумму обещанного платежа аккаунта
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>Сумма обещанного платежа</returns>
        public static decimal GetPromisePaymentSum(this Domain.DataModels.billing.BillingAccount billingAccount)
            => billingAccount.PromisePaymentSum ?? decimal.Zero;

        /// <summary>
        /// Получить доступный баланс аккаунта
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>Доступный баланс аккаунта</returns>
        public static decimal GetAvailableBalance(this Domain.DataModels.billing.BillingAccount billingAccount)
            => billingAccount.Balance + billingAccount.BonusBalance;
    }
}
