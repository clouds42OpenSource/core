﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Account.AccountBilling
{
    /// <summary>
    ///     Продукт счета
    /// </summary>
    [DataContract]
    public class InvoiceProductDto
    {
        /// <summary>
        ///     ID счета
        /// </summary>
        [DataMember]
        public Guid InvoiceId { get; set; }

        /// <summary>
        ///     Название товара
        /// </summary>
        [DataMember]
        public string ServiceName { get; set; }

        /// <summary>
        ///     Общая сумма товара
        /// </summary>
        [DataMember]
        public decimal ServiceSum { get; set; }

        /// <summary>
        ///     Описание товара
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        ///     Количество товара
        /// </summary>
        [DataMember]
        public int Count { get; set; }
    }
}