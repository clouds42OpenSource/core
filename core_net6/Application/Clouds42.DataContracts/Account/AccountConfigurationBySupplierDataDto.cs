﻿using Clouds42.Domain.DataModels.Supplier;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель данных конфигурации аккаунта по поставщику
    /// </summary>
    public class AccountConfigurationBySupplierDataDto
    {
        /// <summary>
        /// Аккаунт 
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Поставщик аккаунта
        /// </summary>
        public Supplier Supplier { get; set; }
    }
}
