﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель сгруппированных администраторов аккаунта
    /// </summary>
    public class GroupedAdministratorsByAccountDto
    {
        /// <inheritdoc />
        public GroupedAdministratorsByAccountDto()
        {
            AccountAdministrators = [];
        }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Администраторы аккаунта
        /// </summary>
        public List<IAccountUser> AccountAdministrators { get; set; }
    }
}
