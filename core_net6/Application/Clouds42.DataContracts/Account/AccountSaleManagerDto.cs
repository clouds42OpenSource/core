﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель менеджера прикрепленного к аккаунту
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountSaleManagerDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountNumber))]
        [DataMember(Name = nameof(AccountNumber))]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Логин менеджера
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        /// <summary>
        /// Отдел менеджера
        /// </summary>
        [XmlElement(ElementName = nameof(Division))]
        [DataMember(Name = nameof(Division))]
        public string Division { get; set; }
    }
}
