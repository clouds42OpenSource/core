﻿using Clouds42.Domain.IDataModels;
using ServicesSegment = Clouds42.Domain.DataModels.CloudServicesSegment;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель данных конфигурации аккаунта по сегменту
    /// </summary>
    public class AccountConfigurationBySegmentDataDto
    {
        /// <summary>
        /// Аккаунт 
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Сегмент аккаунта
        /// </summary>
        public ServicesSegment Segment { get; set; }
    }
}
