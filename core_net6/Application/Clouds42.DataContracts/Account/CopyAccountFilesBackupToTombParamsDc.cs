﻿using System;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель параметров для копирования
    /// бекапа файлов аккаунта в склеп
    /// </summary>
    public class CopyAccountFilesBackupToTombParamsDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Путь к файловому хранилищу аккаунта(Источник)
        /// </summary>
        public string AccountFilesStoragePath { get; set; }

        /// <summary>
        /// Путь к бекапу файлов аккаунта
        /// </summary>
        public string AccountFilesBackupFilePath { get; set; }

        /// <summary>
        /// ID файла бэкапа на гугл диске
        /// </summary>
        public string AccountFilesBackupFileId { get; set; }
    }
}
