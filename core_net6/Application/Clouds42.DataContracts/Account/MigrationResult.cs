﻿using System;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель результата миграции аккаунта между сегментами
    /// </summary>
    public class MigrationResultDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
        
        /// <summary>
        /// Результат выполнения миграции
        /// </summary>
        public bool Successful { get; set; }
        
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}