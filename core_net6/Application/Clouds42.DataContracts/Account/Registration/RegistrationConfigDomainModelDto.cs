﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.Registration
{
    /// <summary>
    /// Модель настроек при регистрации аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RegistrationConfigDomainModelDto
    {
        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(CloudService))]
        [DataMember(Name = nameof(CloudService))]
        public Clouds42Service CloudService { get; set; }

        /// <summary>
        /// ID облачного сервиса для активации
        /// </summary>
        [XmlElement(ElementName = nameof(CloudServiceId))]
        [DataMember(Name = nameof(CloudServiceId))]
        public Guid? CloudServiceId { get; set; }

        /// <summary>
        /// Код совместимой с облачным сервисом конфигурации для создания инф. базы 
        /// </summary>
        [XmlElement(ElementName = nameof(CloudServiceCompatibleConfig1CForCreateDatabase))]
        [DataMember(Name = nameof(CloudServiceCompatibleConfig1CForCreateDatabase))]
        public string CloudServiceCompatibleConfig1CForCreateDatabase { get; set; }

        /// <summary>
        /// Код конфигурации для создания инф. базы при регистрации
        /// </summary>
        [XmlElement(ElementName = nameof(Configuration1C))]
        [DataMember(Name = nameof(Configuration1C))]
        public string Configuration1C { get; set; }

        /// <summary>
        /// Тип источника регистрации пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(RegistrationSource))]
        [DataMember(Name = nameof(RegistrationSource))]
        public ExternalClient RegistrationSource { get; set; }

        /// <summary>
        /// ID реферального аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(ReferralAccountId))]
        [DataMember(Name = nameof(ReferralAccountId))]
        public Guid? ReferralAccountId { get; set; }
    }
}