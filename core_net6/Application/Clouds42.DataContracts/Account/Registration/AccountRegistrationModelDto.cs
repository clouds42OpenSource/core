﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.Registration
{

    public interface IRegistrationSourceInfoAdapter
    {
        string UserSource { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        string Password { get; set; }

        /// <summary>
        /// Электронная почта Email
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        string FullPhoneNumber { get; set; }

        ExternalClient GetExternalClientInfo();
    }

    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountRegistrationModelDto : IRegistrationSourceInfoAdapter
    {
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }
        
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        [XmlElement(ElementName = nameof(Inn))]
        [DataMember(Name = nameof(Inn))]
        public string Inn { get; set; }

        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        public string FirstName { get; set; }

        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        public string FullPhoneNumber { get; set; }

        [XmlElement(ElementName = nameof(RegistrationConfig))]
        [DataMember(Name = nameof(RegistrationConfig))]
        public RegistrationConfigDomainModelDto RegistrationConfig { get; set; }

        [XmlElement(ElementName = nameof(LocaleName))]
        [DataMember(Name = nameof(LocaleName))]
        public string LocaleName { get; set; }

        [XmlIgnore]
        public bool PasswordWasGenerated { get; set; }

        [XmlIgnore]
        public string UserSource { get; set; }

        [XmlIgnore]
        public bool SkipOpenIdCreating { get; set; }
        [XmlIgnore]
        public bool PhoneNumberVerified { get; set; }

        [XmlIgnore]
        public long? TelegramChatId { get; set; }

        [XmlIgnore] public bool IsEmailVerified { get; set; } = true;
        public ExternalClient GetExternalClientInfo()
            => RegistrationConfig?.RegistrationSource ?? ExternalClient.Promo;

    }
}
