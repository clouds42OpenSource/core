﻿
using System;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account.Registration
{
    /// <summary>
    /// Результат активации аккаунта
    /// </summary>
    public class AccountRegistrationResultDto
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string AccountUserPassword { get; set; }
        
        /// <summary>
        /// Созданный аккаунт
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Ошибка активации сервиса
        /// </summary>
        public string ServiceActivationErrorMessage { get; set; }

        public Guid? AccountUserId { get; set; }
    }
}
