﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.Registration
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class ResultModelDto
    {
        [XmlElement(ElementName = nameof(Error))]
        [DataMember(Name = nameof(Error))]
        public bool Error { get; set; }

        [XmlElement(ElementName = nameof(Message))]
        [DataMember(Name = nameof(Message))]
        public string Message { get; set; }
    }
}