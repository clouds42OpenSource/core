﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.Registration
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserRegistrationModelDto : IRegistrationSourceInfoAdapter
    {

        /// <summary>
        /// Номер аккаунта.        
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        /// <summary>
        /// Электронная почта Email
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }        

        /// <summary>
        /// Имя
        /// </summary>
        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        /// <summary>
        /// Номер телефона.
        /// </summary>
        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        public string FullPhoneNumber { get; set; }

        /// <summary>
        /// Источник регистрации.
        /// </summary>
        [XmlElement(ElementName = nameof(RegistrationSource))]
        [DataMember(Name = nameof(RegistrationSource))]
        public ExternalClient? RegistrationSource { get; set; }

        public string UserSource { get; set; }

        public ExternalClient GetExternalClientInfo()
        {
            return RegistrationSource ?? ExternalClient.Promo;
        }
    }

    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserRegistrationToAccountModel : AccountUserRegistrationModelDto
    {
        [XmlElement(ElementName = nameof(AccountIdString))]
        [DataMember(Name = nameof(AccountIdString))]
        public string AccountIdString { get; set; }
    }
}