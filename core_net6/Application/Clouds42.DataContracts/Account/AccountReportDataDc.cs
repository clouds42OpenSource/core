﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель данных по аккаунту для отчета
    /// </summary>
    [DataContract]
    public class AccountReportDataDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        [DataMember]
        public string AccountType { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Аккаунт вип
        /// </summary>
        [DataMember]
        public bool IsVip { get; set; }

        /// <summary>
        /// Название компании
        /// </summary>
        [DataMember]
        public string CompanyName { get; set; }

        /// <summary>
        /// Источник регистрации
        /// </summary>
        [DataMember]
        public string UserSource { get; set; }

        /// <summary>
        /// Логин аккаунт админа
        /// </summary>
        [DataMember]
        public string AccountAdminLogin { get; set; }

        /// <summary>
        /// Почта аккаунт админа
        /// </summary>
        [DataMember]
        public string AccountAdminEmail { get; set; }

        /// <summary>
        /// Дата регистрации аккаунта
        /// </summary>
        [DataMember]
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        [DataMember]
        public int AccountUserCount { get; set; }

        /// <summary>
        /// Количество информационных баз
        /// </summary>
        [DataMember]
        public int AccountDatabaseCount { get; set; }

        /// <summary>
        /// Дата окончания Аренды 1С
        /// </summary>
        [DataMember]
        public DateTime? ExpireDateRent1C { get; set; }

        /// <summary>
        /// Используемое место на диске (ГБ)
        /// </summary>
        [DataMember]
        public decimal UsedSizeOnDisk { get; set; }

        /// <summary>
        /// Баланс
        /// </summary>
        [DataMember]
        public decimal Balance { get; set; }

        /// <summary>
        /// Регулярный/ежемесячный платеж
        /// </summary>
        [DataMember]
        public decimal RegularPayment { get; set; }

        /// <summary>
        /// Наличие входящей транзакции
        /// </summary>
        [DataMember]
        public bool HasInflowPayment { get; set; }

        /// <summary>
        /// Количество лицензий на ЗД
        /// </summary>
        [DataMember]
        public int EsdlLicenseCount { get; set; }

        /// <summary>
        /// Дата обещанного платежа
        /// </summary>
        [DataMember]
        public DateTime? PromisePaymentDate { get; set; }

        /// <summary>
        /// Сумма обещанного платежа
        /// </summary>
        [DataMember]
        public decimal? PromisePaymentSum { get; set; }

        /// <summary>
        /// ФИО сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerFullName { get; set; }

        /// <summary>
        /// Отдел сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerDivision { get; set; }

        /// <summary>
        /// Баланс бонусов
        /// </summary>
        public decimal BonusBalance { get; set; }
    }
}
