﻿namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель информации аккаунта для аудита
    /// </summary>
    public class AccountAuditInfoDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Путь к файловому хранилищу сегмента
        /// </summary>
        public string SegmentFileStoragePath { get; set; }
    }
}
