﻿using Clouds42.DataContracts.Configurations1c.PlatformVersion;
using System.Collections.Generic;
using System.ComponentModel;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Account settings model
    /// </summary>
    public class AccountSettingsDataDto
    {
        /// <summary>
        /// Terminal gateway address
        /// </summary>
        [DisplayName(@"Адрес шлюза терминалов")]
        public string GatewayTerminalsName { get; set; }

        /// <summary>
        /// Connection address
        /// </summary>
        [DisplayName(@"Адрес подключения")]
        public string ServicesTerminalFarmName { get; set; }

        /// <summary>
        /// User login
        /// </summary>
        [DisplayName(@"Логин")]
        public string Login { get; set; }

        /// <summary>
        /// Locale name
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// List of thin client versions
        /// </summary>
        public List<PlatformThinClientVersionDto> PlatformThinClientVersions { get; set; } = [];
    }
}
