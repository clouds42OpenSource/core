﻿namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель данных для определения локали,
    /// для регистрации аккаунта
    /// </summary>
    public class DetermineLocaleNameForRegistrationDto
    {
        /// <summary>
        /// Название локали из параметров регистрации
        /// </summary>
        public string LocaleNameFromParameters { get; set; }

        /// <summary>
        /// Название локали из реферальной ссылки
        /// </summary>
        public string LocaleNameFromReferralLink { get; set; }

        /// <summary>
        /// Название хоста сайта личного кабинета
        /// </summary>
        public string CpSiteHostName { get; set; }
    }
}
