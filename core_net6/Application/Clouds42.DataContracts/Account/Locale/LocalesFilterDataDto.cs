﻿namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Данные фильтра для получения локалей
    /// </summary>
    public class LocalesFilterDataDto
    {
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }
    }
}
