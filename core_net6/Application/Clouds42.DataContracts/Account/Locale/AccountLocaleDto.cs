﻿using System;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель локали аккаунта
    /// </summary>
    public class AccountLocaleDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id локали
        /// </summary>
        public Guid LocaleId { get; set; }
    }
}
