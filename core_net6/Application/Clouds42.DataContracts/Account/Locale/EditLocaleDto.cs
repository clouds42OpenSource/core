﻿using System;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель редактирования локали
    /// </summary>
    public class EditLocaleDto
    {
        /// <summary>
        /// ID локали
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Адрес сайта личного кабинета
        /// </summary>
        public string CpSiteUrl { get; set; }

        /// <summary>
        /// ID сегмента по умолчанию
        /// </summary>
        public Guid DefaultSegmentId { get; set; }
    }
}
