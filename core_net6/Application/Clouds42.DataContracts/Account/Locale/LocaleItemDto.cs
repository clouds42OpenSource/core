﻿using System;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель данных локали
    /// </summary>
    public class LocaleItemDto : LocaleDataDto
    {
        /// <summary>
        /// Код валюты
        /// </summary>
        public int? CurrencyCode { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Адрес сайта личного кабинета
        /// </summary>
        public string CpSiteUrl { get; set; }

        /// <summary>
        /// ID сегмента по умолчанию
        /// </summary>
        public Guid DefaultSegmentId { get; set; }
    }
}
