﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель локализации облака
    /// </summary>
    public class CloudLocalizationDto
    {
        /// <summary>
        /// Ключ(ключевое значение для поиска)
        /// </summary>
        public CloudLocalizationKeyEnum Key { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Фраза/слово на языке локали
        /// </summary>
        public string Value { get; set; }
    }
}
