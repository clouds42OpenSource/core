﻿namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Константы локалей аккаунта
    /// </summary>
    public static class LocaleConstantsDto
    {
        /// <summary>
        /// Русская локаль
        /// </summary>
        public const string Rus = "ru-ru";

        /// <summary>
        /// Украинская локаль
        /// </summary>
        public const string Ua = "ru-ua";

        /// <summary>
        /// Казахская локаль
        /// </summary>
        public const string Kz = "ru-kz";

        /// <summary>
        /// Узбекская локаль
        /// </summary>

        public const string Uz = "ru-uz";
    }

}

