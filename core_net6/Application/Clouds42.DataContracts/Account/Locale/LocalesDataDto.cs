﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель данных локалей
    /// </summary>
    public class LocalesDataDto : SelectDataResultCommonDto<LocaleItemDto>
    {
    }
}
