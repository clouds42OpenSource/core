namespace Clouds42.DataContracts.Account.Locale
{
    public class LocaleDto
    {
        public string Name { get; set; }
        public string Currency { get; set; }
    }
}