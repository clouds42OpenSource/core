﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Маппер моделей локали аккаунта
    /// </summary>
    public static class LocaleModelsMapperDto
    {
        /// <summary>
        /// Выполнить маппинг к модели локали
        /// </summary>
        /// <param name="locale">Локаль аккаунта</param>
        /// <returns>Модель локали</returns>
        public static LocaleDto MapToLocaleDc(this ILocale locale) =>
            new()
            {
                Name = locale.Name,
                Currency = locale.Currency
            };
    }
}