﻿namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Dc модель информации о валюте
    /// </summary>
    public class CurrencyDto
    {
        /// <summary>
        /// Имя валюты
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Код калюты
        /// </summary>
        public int? CurrencyCode { get; set; }
    }
}
