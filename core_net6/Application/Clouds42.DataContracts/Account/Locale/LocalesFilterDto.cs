﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Фильтр для получения локалей
    /// </summary>
    public class LocalesFilterDto : SelectDataCommonDto<LocalesFilterDataDto>
    {
    }
}
