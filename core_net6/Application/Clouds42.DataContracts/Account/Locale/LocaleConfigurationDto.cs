﻿using System;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель конфигурации локали
    /// </summary>
    public class LocaleConfigurationDto
    {
        /// <summary>
        /// ID локали
        /// </summary>
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// ID сегмента по умолчанию
        /// </summary>
        public Guid DefaultSegmentId { get; set; }

        /// <summary>
        /// Адрес сайта личного кабинета
        /// </summary>
        public string CpSiteUrl { get; set; }
    }
}
