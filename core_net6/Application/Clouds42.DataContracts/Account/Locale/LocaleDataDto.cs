﻿using System;

namespace Clouds42.DataContracts.Account.Locale
{
    /// <summary>
    /// Модель данных локали
    /// </summary>
    public class LocaleDataDto
    {
        /// <summary>
        /// Id локали
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public string Country { get; set; }
    }
}
