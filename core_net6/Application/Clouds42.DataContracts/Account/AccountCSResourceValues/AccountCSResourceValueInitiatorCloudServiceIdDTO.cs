﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValueInitiatorCloudServiceIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(InitiatorCloudServiceId))]
        [DataMember(Name = nameof(InitiatorCloudServiceId))]
        public Guid InitiatorCloudServiceId { get; set; }
    }
}