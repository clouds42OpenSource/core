﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using System;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class IncreaseDecreaseValueModelDto
    {
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        [Required]
        public Guid? AccountId { get; set; }

        [XmlElement(ElementName = "CSResourceID")]
        [DataMember(Name = "CSResourceID")]
        [Required]
        public Guid? CSResourceId { get; set; }

        [XmlElement(ElementName = nameof(ModifyResourceValue))]
        [DataMember(Name = nameof(ModifyResourceValue))]
        [Range(1, int.MaxValue, ErrorMessage = "ModifyResourceValue must be greater than zero")]
        public int? ModifyResourceValue { get; set; }

        [XmlElement(ElementName = nameof(ModifyResourceComment))]
        [DataMember(Name = nameof(ModifyResourceComment))]
        [StringNonNullAndEmpty]
        [Required]
        public string ModifyResourceComment { get; set; }

        [XmlIgnore]
        public Guid AccountIdValue
        {
            get
            {
                return AccountId.Value;
            }
        }

        [XmlIgnore]
        public Guid CSResourceIdValue
        {
            get
            {
                return CSResourceId.Value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder("IncreaseDecreaseValueModel");
            sb.AppendLine("AccountId: " + AccountId);
            sb.AppendLine("CSResourceID: " + CSResourceId);
            sb.AppendLine("ModifyResourceValue: " + ModifyResourceValue);
            sb.AppendLine("ModifyResourceComment: " + ModifyResourceComment);
            return sb.ToString();
        }
    }
}