﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValueModifyResourceDateTimeDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "InitiatorCloudServiceId")]
        [DataMember(Name = "InitiatorCloudServiceId")]
        public DateTime ModifyResourceDateTime { get; set; }
    }
}