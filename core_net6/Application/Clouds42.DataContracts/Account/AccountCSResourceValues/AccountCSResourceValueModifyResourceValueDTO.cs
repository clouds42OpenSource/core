﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValueModifyResourceValueDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ModifyResourceValue))]
        [DataMember(Name = nameof(ModifyResourceValue))]
        public int ModifyResourceValue { get; set; }
    }
}