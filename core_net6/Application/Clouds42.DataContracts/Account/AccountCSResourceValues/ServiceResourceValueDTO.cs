﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceResourceValueDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceResourceValue))]
        [DataMember(Name = nameof(ServiceResourceValue))]
        public int ServiceResourceValue { get; set; }
        public ServiceResourceValueDto()
        {
        }

        public ServiceResourceValueDto(int? serviceResourceValue)
        {
            ServiceResourceValue = serviceResourceValue ?? 0;
        }
    }
}
