﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValuePropertiesDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(CSResourceID))]
        [DataMember(Name = nameof(CSResourceID))]
        public Guid CSResourceID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(InitiatorCloudServiceID))]
        [DataMember(Name = nameof(InitiatorCloudServiceID))]
        public Guid InitiatorCloudServiceID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ModifyResourceDateTime))]
        [DataMember(Name = nameof(ModifyResourceDateTime))]
        public DateTime ModifyResourceDateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ModifyResourceValue))]
        [DataMember(Name = nameof(ModifyResourceValue))]
        public int ModifyResourceValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ModifyResourceComment))]
        [DataMember(Name = nameof(ModifyResourceComment))]
        public string ModifyResourceComment { get; set; }
    }
}