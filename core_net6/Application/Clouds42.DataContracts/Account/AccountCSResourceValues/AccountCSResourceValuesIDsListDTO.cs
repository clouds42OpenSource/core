﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValuesIDsListDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AcountCSResourceValueIDs))]
        [DataMember(Name = nameof(AcountCSResourceValueIDs))]
        public GuidListItemDto AcountCSResourceValueIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AccountCSResourceValuesIDsListDto()
        {
            AcountCSResourceValueIDs = new GuidListItemDto();
        }
    }

}