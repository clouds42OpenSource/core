﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValueModifyResourceCommentDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ModifyResourceComment))]
        [DataMember(Name = nameof(ModifyResourceComment))]
        public string ModifyResourceComment { get; set; }
    }
}