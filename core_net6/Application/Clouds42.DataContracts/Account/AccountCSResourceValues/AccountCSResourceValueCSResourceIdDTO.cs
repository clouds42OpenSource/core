﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountCSResourceValues
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCSResourceValueCSResourceIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(CSResourceId))]
        [DataMember(Name = nameof(CSResourceId))]
        public Guid CSResourceId { get; set; }
    }
}