﻿using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Информация аккаунта для уведомления
    /// </summary>
    public class AccountInfoForNotifyDto
    {
        /// <inheritdoc />
        public AccountInfoForNotifyDto()
        {
            Emails = [];
        }

        /// <summary>
        /// Почтовые адреса
        /// </summary>
        public List<string> Emails { get; set; }

        /// <summary>
        /// Администратор аккаунта
        /// </summary>
        public IAccountUser AccountAdmin { get; set; }
    }
}
