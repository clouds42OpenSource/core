﻿using System;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Результат активации сервиса
    /// </summary>
    public class ServiceActivationResultDto
    {
        /// <summary>
        /// Признак что сервис уже активирован
        /// </summary>
        public bool IsAlreadyActivated { get; set; }

        /// <summary>
        /// Дата окончания демо периода
        /// </summary>
        public DateTime DemoPeriodEndDate { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }
    }
}
