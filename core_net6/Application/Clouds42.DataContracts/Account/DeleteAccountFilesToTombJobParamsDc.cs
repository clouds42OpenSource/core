﻿using System;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    ///     Модель параметров воркера для таски
    ///     'Архивация в склеп файлов аккаунта'
    /// </summary>
    public class DeleteAccountFilesToTombJobParamsDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
