﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель информации менеджера прикрепленного к аккаунту
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    [DataContract(Name = "Response")]
    public class AccountSaleManagerInfoDto
    {
        /// <summary>
        /// Логин менеджера
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }
        
        /// <summary>
        /// Email менеджера
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }
    }
}
