﻿using System;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Параметры смены сегмента для аккаунта
    /// </summary>
    public class ChangeAccountSegmentParamsDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID cегмента, с которого нужно мигрировать
        /// </summary>
        public Guid SegmentFromId { get; set; }

        /// <summary>
        /// Целевой ID сегмента
        /// </summary>
        public Guid SegmentToId { get; set; }

        /// <summary>
        /// Инициатор миграции
        /// </summary>
        public Guid InitiatorId { get; set; }

        /// <summary>
        /// Дата начала миграции
        /// </summary>
        public DateTime MigrationStartDateTime { get; set; }

        /// <summary>
        /// Ключ основного рабочего процесса
        /// </summary>
        public string ParentProcessFlowKey { get; set; }
    }
}
