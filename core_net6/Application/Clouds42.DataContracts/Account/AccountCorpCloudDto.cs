﻿using System;

namespace Clouds42.DataContracts.Account
{
    public class AccountCorpCloudDto
    {
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество кликов
        /// </summary>
        public int CountOfClicks { get; set; }

        /// <summary>
        /// Дата последнего клика
        /// </summary>
        public DateTime LastUseDate { get; set; }
    }
}
