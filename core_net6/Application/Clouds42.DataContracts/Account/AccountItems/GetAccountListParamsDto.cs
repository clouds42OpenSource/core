﻿using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.Account.AccountItems
{
    /// <summary>
    /// Модель выбора записей списка аккаунтов
    /// </summary>
    public class GetAccountListParamsDto : ISortedQuery, IHasFilter<AccountItemsFilterParamsDto>, IPagedQuery
    {
        public string OrderBy { get; set; }
        public AccountItemsFilterParamsDto Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
