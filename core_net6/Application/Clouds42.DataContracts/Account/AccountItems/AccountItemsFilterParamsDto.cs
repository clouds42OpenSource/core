﻿using System;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.Account.AccountItems
{
    /// <summary>
    /// Фильтр для получения списка аккаунтов
    /// </summary>
    public sealed class AccountItemsFilterParamsDto : IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Строка поиска списка аккаунтов
        /// </summary>
        public string SearchLine { get; set; }

        /// <summary>
        /// Начальная дата регистрации аккаунта
        /// </summary>
        public DateTime? RegisteredFrom { get; set; }

        /// <summary>
        /// Конечная дата регистрации аккаунта
        /// </summary>
        public DateTime? RegisteredTo { get; set; }

        /// <summary>
        /// Индикатор что бы показать только мои аккаунты
        /// </summary>
        public bool? OnlyMine { get; set; }
    }
}
