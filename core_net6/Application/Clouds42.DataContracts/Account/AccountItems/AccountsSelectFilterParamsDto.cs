﻿using System;

namespace Clouds42.DataContracts.Account.AccountItems
{
    /// <summary>
    /// Модель фильтрации выбора аккаунтов
    /// </summary>
    public sealed class AccountsSelectFilterParamsDto
    {
        /// <summary>
        /// Фильтр для получения списка аккаунтов
        /// </summary>
        public AccountItemsFilterParamsDto Filter { get; set; }
        /// <summary>
        /// ID аутентифицированного пользователя
        /// </summary>
        public Guid UserPrincipalId { get; set; }
        
        /// <summary>
        /// Фильтр по счёту
        /// </summary>
        public string SearchInvoice { get; set; }
    }
}