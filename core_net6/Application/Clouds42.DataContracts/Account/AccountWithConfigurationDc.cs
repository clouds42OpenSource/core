﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Account
{
    /// <summary>
    /// Модель аккаунта с его конфигурацией
    /// </summary>
    public class AccountWithConfigurationDto
    {
        /// <summary>
        /// Аккаунт облака
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Конфигурация аккаунта
        /// </summary>
        public Domain.DataModels.AccountProperties.AccountConfiguration AccountConfiguration { get; set; }
    }
}
