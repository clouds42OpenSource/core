﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель названия аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountCaptionDto
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }
        
        /// <summary></summary>
        public AccountCaptionDto()
        {
        }
    }
}