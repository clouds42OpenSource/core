﻿using Clouds42.DataContracts.Account.Registration;
using System;
using Clouds42.Domain.IDataModels;


namespace Clouds42.DataContracts.Account.AccountModels
{
    
    public class AccountRegistrationMailModelDto
    {
        public Guid Id { get; set; }

        public Guid? AccountId { get; set; }

        public bool? Unsubscribe { get; set; } 

        public string Email { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string ResetCode { get; set; }       

        public IRegistrationSourceInfoAdapter RegistrationSource { get; set; }

        public ILocale Locale { get; set; }

        public string FullPhoneNumber { get; set; }

        public bool IsVisiblePassword { get; set; }

    }
}