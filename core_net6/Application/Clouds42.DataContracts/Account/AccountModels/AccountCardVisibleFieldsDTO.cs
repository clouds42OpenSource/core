﻿namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Содержит список полей которые видимы или нет
    /// </summary>
    public sealed class AccountCardVisibleFieldsDto
    {
        /// <summary>
        /// Можно ли показывать полную информацио о аккаунте или нет
        /// </summary>
        public bool CanShowFullInfo { get; set; }

        /// <summary>
        /// Можно ли показывать информацию о сейл менеджере или нет
        /// </summary>
        public bool CanShowSaleManagerInfo { get; set; }
    }
}