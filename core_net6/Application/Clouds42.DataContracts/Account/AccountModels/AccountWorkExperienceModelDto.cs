﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountWorkExperienceModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(WorkExperience))]
        [DataMember(Name = nameof(WorkExperience))]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int WorkExperience { get; set; }

        public AccountWorkExperienceModelDto()
        {
        }
    }
}