﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class WorkAverageCostDto
    {
        /// <summary> </summary>
        [XmlElement(ElementName = nameof(WorkAverageCost))]
        [DataMember(Name = nameof(WorkAverageCost))]
        public int WorkAverageCost { get; set; }
        
        /// <summary></summary>
        public WorkAverageCostDto()
        {
        }
    }
}