﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountFilteredCriteriaRequestDto
    {
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        [XmlElement(ElementName = nameof(Inn))]
        [DataMember(Name = nameof(Inn))]
        public string Inn { get; set; }


        [XmlElement(ElementName = nameof(ManagerPhone))]
        [DataMember(Name = nameof(ManagerPhone))]
        public string ManagerPhone { get; set; }

    }
}
