﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountCaptionModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }

        /// <summary></summary>
        public AccountCaptionModelDto()
        {
        }
    }
}