﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class WorkExperienceDto
    {
        /// <summary> </summary>
        [XmlElement(ElementName = nameof(WorkExperience))]
        [DataMember(Name = nameof(WorkExperience))]
        public int WorkExperience { get; set; }
        
        /// <summary></summary>
        public WorkExperienceDto()
        {
        }
    }
}