﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Таблица с информацией об аккаунтах
    /// </summary>
    public class AccountInfoTableModelDto
    {

        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type ="Table"; 
        /// <summary>
        /// Записи с информацией об аккаунтах
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<AccountInfoTableRowDto> Rows { get; set; } = [];
    }
}
