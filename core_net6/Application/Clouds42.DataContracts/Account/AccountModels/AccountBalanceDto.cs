﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountBalanceDto
    {
        [XmlElement(ElementName = nameof(AccountBalance))]
        [DataMember(Name = nameof(AccountBalance))]
        public decimal AccountBalance { get; set; }

    }
}