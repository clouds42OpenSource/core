﻿using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Информация о акккаунте
    /// </summary>
    public sealed class AccountCardAccountInfoDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID сегмента
        /// </summary>
        public Guid? SegmentID { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Источник, откуда пришёл пользователь
        /// </summary>
        public string UserSource { get; set; }

        /// <summary>
        /// Наименование денежной еденицы относительно локали
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// ID локали
        /// </summary>
        public Guid? LocaleId { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool IsVip { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int IndexNumber { get; set; }

        /// <summary>
        /// Ссылка на склеп
        /// </summary>
        public string CloudStorageWebLink { get; set; }
    }
}