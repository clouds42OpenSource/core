﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountStorageIdModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(FileStorageServerID))]
        [DataMember(Name = nameof(FileStorageServerID))]
        [Required]
        public Guid? FileStorageServerID { get; set; }

        /// <summary></summary>
        public AccountStorageIdModelDto()
        {
        }
    }
}