﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountProvidedServiceTypeModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(ProvidedServiceType))]
        [DataMember(Name = nameof(ProvidedServiceType))]
        [StringNonNullAndEmpty]
        public string ProvidedServiceType { get; set; }

        /// <summary></summary>
        public AccountProvidedServiceTypeModelDto()
        {
        }
    }
}