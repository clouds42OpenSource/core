﻿using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Общая информация для карточки аккаунта
    /// </summary>
    public sealed class AccountCardCommonDataDto
    {
        /// <summary>
        /// Используемое место на диске аккаунта
        /// </summary>
        public long? UsedSizeOnDisk { get; set; }

        /// <summary>
        /// Полная сумма стоимости сервисов аккаунта
        /// </summary>
        public decimal ServiceTotalSum { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Название сейл менеджера
        /// </summary>
        public string AccountSaleManagerCaption { get; set; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Последняя активность
        /// </summary>
        public DateTime? LastActivity { get; set; }

        /// <summary>
        /// Имя сегмента
        /// </summary>
        public string SegmentName { get; set; }

        /// <summary>
        /// Письма для рассылок
        /// </summary>
        public string[] Emails { get; set; }
    }
}