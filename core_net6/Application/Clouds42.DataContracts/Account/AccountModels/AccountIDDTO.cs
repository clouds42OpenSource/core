﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель идентификатора аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountIDDto
    {
        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid? AccountId { get; set; }

        /// <summary></summary>
        public AccountIDDto()
        {
        }

        /// <summary></summary>
        public AccountIDDto(Guid accountId)
        {
            AccountId = accountId;
        }
    }
}