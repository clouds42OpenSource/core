﻿namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Содержит информацию о видимости кнопок  
    /// </summary>
    public sealed class AccountCardButtonsVisibilityDto {
        /// <summary>
        /// Если <c>true</c>, то кнопка перейти в аккаунт видна
        /// </summary>
        public bool IsGotoToAccountButtonVisible { get; set; }
    }
}