﻿using System;

namespace Clouds42.DataContracts.Account.AccountModels
{

    public class ChangeSegmentDto
    {
        /// <summary>
        /// id аккаунта для смены сегмента
        /// </summary>
        public Guid accountId { get; set; }

        /// <summary>
        /// id сегмента для смены
        /// </summary>
        public Guid segmentId { get; set; }
    }
}
