﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Данные аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountPropertiesDto
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }

        /// <summary>
        /// Идентификатор рефферала
        /// </summary>
        [XmlElement(ElementName = nameof(ReferralAccountID))]
        [DataMember(Name = nameof(ReferralAccountID))]
        public string ReferralAccountID { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [XmlElement(ElementName = nameof(RegistrationDate))]
        [DataMember(Name = nameof(RegistrationDate))]
        public string RegistrationDate { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(IndexNumber))]
        [DataMember(Name = nameof(IndexNumber))]
        public string IndexNumber { get; set; }

        /// <summary>
        /// Признак удаленности
        /// </summary>
        [XmlElement(ElementName = nameof(Removed))]
        [DataMember(Name = nameof(Removed))]
        public bool Removed { get; set; }

        /// <summary>
        /// Описание аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [XmlElement(ElementName = nameof(Currency))]
        [DataMember(Name = nameof(Currency))]
        public string Currency { get; set; }

        /// <summary>
        /// Инн аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(INN))]
        [DataMember(Name = nameof(INN))]
        public string INN { get; set; }

        /// <summary>
        /// Идентификатор локали аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(LocaleId))]
        [DataMember(Name = nameof(LocaleId))]
        public Guid? LocaleId { get; set; }

        /// <summary>
        /// Идентификатор менеджера аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(ManagerId))]
        [DataMember(Name = nameof(ManagerId))]
        public Guid ManagerId { get; set; }

    }
}