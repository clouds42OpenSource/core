﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountIdModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        /// <summary></summary>
        public AccountIdModelDto()
        {
        }
    }
}