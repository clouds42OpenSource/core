﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountProvidedServiceVolumeModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(ProvidedServiceVolume))]
        [DataMember(Name = nameof(ProvidedServiceVolume))]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int ProvidedServiceVolume { get; set; }

        /// <summary></summary>
        public AccountProvidedServiceVolumeModelDto()
        {
        }
    }
}