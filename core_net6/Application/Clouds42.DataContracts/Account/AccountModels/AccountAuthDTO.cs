﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountAuthDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid? AccountId { get; set; }

        [XmlElement(ElementName = nameof(Token))]
        [DataMember(Name = nameof(Token))]
        public Guid? Token { get; set; }

        /// <summary></summary>
        public AccountAuthDto()
        {

        }

        /// <summary></summary>
        public AccountAuthDto(Guid accountId, Guid token)
        {
            AccountId = accountId;
            Token = token;
        }
    }
}