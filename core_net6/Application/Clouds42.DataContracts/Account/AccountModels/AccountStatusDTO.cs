﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель статуса аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountStatusDto
    {
        /// <summary>
        /// Статус аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status { get; set; }
        
        /// <summary></summary>
        public AccountStatusDto()
        {
        }
    }
}