﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountIDsListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountIDs))]
        [DataMember(Name = nameof(AccountIDs))]
        public GuidListItemDto AccountIDs { get; set; }

        /// <summary></summary>
        public AccountIDsListDto()
        {
            AccountIDs = new GuidListItemDto();
        }
    }
}