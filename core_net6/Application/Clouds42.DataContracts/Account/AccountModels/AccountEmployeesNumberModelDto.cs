﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountEmployeesNumberModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(EmployeesNumber))]
        [DataMember(Name = nameof(EmployeesNumber))]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int EmployeesNumber { get; set; }

        /// <summary></summary>
        public AccountEmployeesNumberModelDto()
        {
        }
    }
}