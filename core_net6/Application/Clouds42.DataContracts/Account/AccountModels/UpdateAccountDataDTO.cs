﻿using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель параметров для сохранения данных о аккаунте
    /// </summary>
    public sealed class UpdateAccountDataDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// ID локали
        /// </summary>
        public Guid? LocaleId { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool IsVip { get; set; }

        /// <summary>
        /// Письма для рассылок
        /// </summary>
        public string[] Emails { get; set; }

        /// <summary>
        /// Получает список email-ов
        /// </summary>
        /// <returns>Возвращает список email-ов, если <see cref="Emails"/> = <code>null</code>, то возвращается пустой массив.</returns>
        public string[] GetEmails() => Emails ?? [];
    }
}