﻿namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Содержит информацию о видемости табов карточки аккаунта
    /// </summary>
    public sealed class AccountCardTabVisibilityDto
    {
        /// <summary>
        /// Можно ли показывать таб по общей информации аккаунта
        /// </summary>
        public bool IsAccountDetailsTabVisible { get; set; }

        /// <summary>
        /// Можно ли показывать таб по смене сегмента или нет
        /// </summary>
        public bool IsChangeSegmentTabVisible { get; set; }

        /// <summary>
        /// Можно ли показывать таб по миграции баз по хранилищам
        /// </summary>
        public bool IsMigrateDatabaseTabVisible { get; set; }
    }
}