﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountResourceModelDto
    {
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        [Required]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(ServiceName))]
        [DataMember(Name = nameof(ServiceName))]
        [Required]
        public string ServiceName { get; set; }

        public AccountResourceModelDto()
        {
        }
    }
}