﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель таблицы с информацией об аккаунтах
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountInfoTableDto
    {
        /// <summary>
        /// Таблица с информацией об аккаунтах
        /// </summary>
        [XmlElement(ElementName = nameof(AccountSearchResultTable))]
        [DataMember(Name = nameof(AccountSearchResultTable))]
        public AccountInfoTableModelDto AccountSearchResultTable { get; set; } = new();
    }
}
