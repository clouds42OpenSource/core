﻿namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Информация о локали
    /// </summary>
    public sealed class AccountCardKeyValueDataDto<TKey>
    {
        /// <summary>
        /// ID 
        /// </summary>
        public TKey Id { get; set; }
        
        /// <summary>
        /// Название 
        /// </summary>
        public string Value { get; set; }
    }
}