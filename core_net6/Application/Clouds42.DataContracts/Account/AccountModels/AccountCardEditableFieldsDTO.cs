﻿namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Содержит список полей которые можно редактировать или нет
    /// </summary>
    public sealed class AccountCardEditableFieldsDto
    {
        /// <summary>
        /// Можно ли редактировать название аккаунта
        /// </summary>
        public bool IsAccountCaptionEditable { get; set; }
        /// <summary>
        /// Можно ли редактировать Инн
        /// </summary>
        public bool IsInnEditable { get; set; }

        /// <summary>
        /// Можно ли редактировать локаль
        /// </summary>
        public bool IsLocaleEditable { get; set; }
    }
}