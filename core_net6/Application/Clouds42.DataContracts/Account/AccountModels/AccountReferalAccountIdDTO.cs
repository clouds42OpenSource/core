﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель идентификатора рефферала
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountReferalAccountIdDto
    {
        /// <summary>
        /// Идентификатор рефферала
        /// </summary>
        [XmlElement(ElementName = nameof(ReferralAccountID))]
        [DataMember(Name = nameof(ReferralAccountID))]
        public string ReferralAccountID { get; set; }

        /// <summary></summary>
        public AccountReferalAccountIdDto()
        {
        }
    }
    
}