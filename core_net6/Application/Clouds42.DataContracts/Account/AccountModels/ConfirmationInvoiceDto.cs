﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель запроса для принятия счета на оплату
    /// </summary>
    [XmlRoot("Request")]
    [DataContract(Name = "Request")]
    public class ConfirmationInvoiceDto
    { 
        /// <summary>
        /// ID платежа
        /// </summary>
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        /// <summary>
        /// Аккаунт
        /// </summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        #region sum

        /// <summary>
        /// Сумма счёта
        /// </summary>
        [XmlElement(ElementName = nameof(sum))]
        [DataMember(Name = nameof(sum))]
        public string SumString
        {
            get { return sum.ToString(CultureInfo.CurrentUICulture); }
            set { sum = Parse(value); }
        }

        /// <summary>
        /// Сумма счёта
        /// </summary>
        [XmlIgnore]
        [IgnoreDataMember]
        public decimal sum { get; set; }

        #endregion

        /// <summary>
        /// Источник платежа
        /// </summary>
        [XmlElement(ElementName = nameof(OriginDetails))]
        [DataMember(Name = nameof(OriginDetails))]
        public string OriginDetails { get; set; }

        /// <summary>
        /// Платёжная система
        /// </summary>
        [XmlElement(ElementName = nameof(System))]
        [DataMember(Name = nameof(System))]
        public string System { get; set; }

        /// <summary>
        /// Дата совершения платежа
        /// </summary>
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Назначение платежа
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Получает <see cref="PaymentSystem"/> из строки свойства <see cref="System"/>
        /// </summary>
        public PaymentSystem GetPaymentSystem()
        {
            if (Enum.TryParse(System, out PaymentSystem paymentSystem))
                return paymentSystem;

            throw new ValidationException($"У платежа {System} не известный тип платежной системы.");
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ConfirmationInvoiceDto()
        {
        }

        public static decimal Parse(string val)
        {
            string wantedSeperator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            string alternateSeperator = (wantedSeperator == "," ? "." : ",");

            if (val.IndexOf(wantedSeperator) == -1 && val.IndexOf(alternateSeperator) != -1)
            {
                val = val.Replace(alternateSeperator, wantedSeperator);
            }
            return decimal.Parse(val, NumberStyles.Any);
        }
    }
}
