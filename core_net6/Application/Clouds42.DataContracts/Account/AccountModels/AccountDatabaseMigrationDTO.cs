﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// модель миграции баз в карточке аккаунта 
    /// </summary>
    public class AccountDatabaseMigrationDto
    {
        /// <summary>
        /// Хранилище в которое нужно перенести
        /// </summary>
        public Guid storage { get; set; }

        /// <summary>
        /// Базы для переноса
        /// </summary>
        public List<string> databases { get; set; }
    }
}
