﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountStatusModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        [Required]
        public Guid? AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        [StringNonNullAndEmpty]
        public string Status { get; set; }

        /// <summary></summary>
        public AccountStatusModelDto()
        {
        }
    }
}