﻿using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Данные карточки аккаунта
    /// </summary>
    public sealed class AccountCardDataDto
    {
        /// <summary>
        /// Информация о видимости кнопок
        /// </summary>
        public AccountCardButtonsVisibilityDto ButtonsVisibility { get; set; }

        /// <summary>
        /// Общая информация о аккаунте
        /// </summary>
        public AccountCardCommonDataDto CommonData { get; set; }

        /// <summary>
        /// Информация о аккаунте
        /// </summary>
        public AccountCardAccountInfoDto AccountInfo { get; set; }

        /// <summary>
        /// Массив доступных локалей
        /// </summary>
        public AccountCardKeyValueDataDto<Guid>[] Locales{ get; set; }


        /// <summary>
        /// Массив доступных сегментов
        /// </summary>
        public AccountCardKeyValueDataDto<Guid>[] Segments{ get; set; }

        /// <summary>
        /// Список полей которые настраеваемы как видимы или нет
        /// </summary>
        public AccountCardVisibleFieldsDto VisibleFields { get; set; }

        /// <summary>
        /// Список полей которые настраеваемы как редактируемые или нет
        /// </summary>
        public AccountCardEditableFieldsDto EditableFields { get; set; }

        /// <summary>
        /// Содержит информацию о видимости табов карточки аккаунта
        /// </summary>
        public AccountCardTabVisibilityDto TabVisibility { get; set; } 


    }
}