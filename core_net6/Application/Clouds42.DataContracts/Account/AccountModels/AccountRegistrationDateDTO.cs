﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель даты регистрации аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountRegistrationDateDto
    {
        /// <summary>
        /// Дата регистрации аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(RegistrationDate))]
        [DataMember(Name = nameof(RegistrationDate))]
        public string RegistrationDate { get; set; }

        public AccountRegistrationDateDto()
        {
        }
    }
}