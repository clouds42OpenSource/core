﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель номера аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountIndexNumberDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(IndexNumber))]
        [DataMember(Name = nameof(IndexNumber))]
        public int IndexNumber { get; set; }
        public AccountIndexNumberDto()
        {
        }
    }
}