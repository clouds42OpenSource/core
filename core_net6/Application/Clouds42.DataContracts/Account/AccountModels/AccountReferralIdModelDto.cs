﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountReferralIdModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = "AccountCaption")]
        [DataMember(Name = "AccountCaption")]
        public Guid ReferralAccountID { get; set; }

        /// <summary></summary>
        public AccountReferralIdModelDto()
        {
        }
    }
}