﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Department;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель редактирования аккаунта
    /// </summary>
    public sealed class EditAccountDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool? IsVip { get; set; }
        
        /// <summary>
        /// Признак того, что нужно создавать серверную базу по умолчанию
        /// </summary>

        public bool CreateClusterDatabase { get; set; }

        /// <summary>
        /// ID локали
        /// </summary>
        public Guid? LocaleId { get; set; }

        public int? Deployment { get; set; }

        /// <summary>
        /// Список почт аккаунта
        /// </summary>
        public List<string> AccountEmails { get; set; }

        
        /// <summary>
        /// Запросы на изменение департаментов аккаунта
        /// </summary>
        public List<DepartmentRequest>  DepartmentRequests { get; set; }
    }
}
