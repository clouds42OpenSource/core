﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class EmployeesNumberDto
    {
        /// <summary> </summary>
        [XmlElement(ElementName = nameof(EmployeesNumber))]
        [DataMember(Name = nameof(EmployeesNumber))]
        public int EmployeesNumber { get; set; }
        
        /// <summary></summary>
        public EmployeesNumberDto()
        {
        }
    }
}