﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.Account.AccountModels
{
    public class AccountCommonDataDto
    {
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }

        [XmlElement(ElementName = "ReferralAccountID")]
        [DataMember(Name = "ReferralAccountID")]
        public Guid? ReferralAccountId { get; set; }

        [XmlElement(ElementName = nameof(RegistrationDate))]
        [DataMember(Name = nameof(RegistrationDate))]
        public DateTime? RegistrationDate { get; set; }

        [XmlElement(ElementName = nameof(IndexNumber))]
        [DataMember(Name = nameof(IndexNumber))]
        public int IndexNumber { get; set; }

        [XmlElement(ElementName = nameof(Removed))]
        [DataMember(Name = nameof(Removed))]
        public bool Removed { get; set; }

        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        [XmlElement(ElementName = nameof(Currency))]
        [DataMember(Name = nameof(Currency))]
        public string Currency { get; set; }

        [XmlElement(ElementName = "INN")]
        [DataMember(Name = "INN")]
        public string Inn { get; set; }

        [XmlElement(ElementName = nameof(LocaleId))]
        [DataMember(Name = nameof(LocaleId))]
        public Guid? LocaleId { get; set; }

    }
}