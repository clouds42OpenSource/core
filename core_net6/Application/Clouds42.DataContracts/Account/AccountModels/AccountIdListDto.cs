﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountIdListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "Items")]
        [DataMember(Name = "Items")]
        public AccountIdListItemsDto AccountIdsList { get; set; }

        /// <summary></summary>
        public AccountIdListDto()
        {
        }

        /// <summary></summary>
        public AccountIdListDto(AccountIdListItemsDto accountIdsList)
        {
            AccountIdsList = accountIdsList;            
        }
    }

    /// <summary></summary>
    [XmlRoot(ElementName = "Items")]
    [DataContract(Name = "Items")]
    public class AccountIdListItemsDto
    {   

        /// <summary></summary>
        [XmlAttribute(AttributeName = nameof(Type))]
        [DataMember(Name = nameof(Type))]
        public string Type { get; set; }

        /// <summary></summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public List<Guid> AccountIds { get; set; }

        /// <summary></summary>
        public AccountIdListItemsDto()
        {
            Type = "List";
        }

        /// <summary></summary>
        public AccountIdListItemsDto(List<Guid> accountIds) : this()
        {
            AccountIds = accountIds;
        }
    }

}