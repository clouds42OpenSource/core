﻿using Clouds42.DataContracts.AccountUser.AccountUser;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Таблица пользователей аккаунта
    /// </summary>
    public class AccountUsersTableModelDto
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type= "Table"; 

        /// <summary>
        /// Свойства пользователей аккаунта
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<AccountUserPropertiesDto> Rows { get; set; } = [];
    }
}
