﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    ///  Запись информации об аккаунте
    /// </summary>
    public class AccountInfoTableRowDto
    {
        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        [XmlElement(ElementName = "AccountId")]
        [DataMember(Name = "AccountId")]
        public Guid Id { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(IndexNumber))]
        [DataMember(Name = nameof(IndexNumber))]
        public int IndexNumber { get; set; }

        /// <summary>
        /// Название пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(OwnerName))]
        [DataMember(Name = nameof(OwnerName))]
        public string OwnerName { get; set; }

        /// <summary>
        /// Почта пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(OwnerEmail))]
        [DataMember(Name = nameof(OwnerEmail))]
        public string OwnerEmail { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(OwnerId))]
        [DataMember(Name = nameof(OwnerId))]
        public Guid OwnerId { get; set; }

        /// <summary>
        /// Описание аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Размер диска
        /// </summary>
        [XmlElement(ElementName = nameof(DiscSize))]
        [DataMember(Name = nameof(DiscSize))]
        public long DiscSize { get; set; }
    }
}
