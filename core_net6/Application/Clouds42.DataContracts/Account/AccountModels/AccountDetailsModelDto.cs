﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.DataContracts.Department;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Хранит подробные данные о аккаунте
    /// </summary>
    public class AccountDetailsModelDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int IndexNumber { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        [RegularExpression(@"^\d{14}$", ErrorMessage = "ИНН должен содержать ровно 14 цифр")]
        public string AccountInn { get; set; }

        /// <summary>
        /// Признак что аккаунт является ВИП
        /// </summary>
        public bool IsVip { get; set; }
        
        /// <summary>
        /// Признак того, что нужно создавать серверную базу по умолчанию
        /// </summary>

        public bool CreateClusterDatabase { get; set; }

        /// <summary>
        /// Описание аккаунта
        /// </summary>
        public string AccountDescription { get; set; }

        /// <summary>
        /// ID сегмента
        /// </summary>
        public Guid? SegmentId { get; set; }

        /// <summary>
        /// ID локали
        /// </summary>
        public Guid? LocaleId { get; set; }
        
        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Валюта аккаунта
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Ссылка на склеп
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// Источник, откуда пришёл пользователь
        /// </summary>
        public string UserSource { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Используемое место на диске аккаунта
        /// </summary>
        public long? UsedSizeOnDisk { get; set; }

        /// <summary>
        /// Название сейл менеджера
        /// </summary>
        public string AccountSaleManagerCaption { get; set; }

        /// <summary>
        /// Письма для рассылок
        /// </summary>
        public List<string> Emails { get; set; } = [];

        /// <summary>
        /// Имя сегмента
        /// </summary>
        public string SegmentName { get; set; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Последняя активность
        /// </summary>
        public DateTime? LastActivity { get; set; }

        /// <summary>
        /// Полная сумма стоимости сервисов аккаунта
        /// </summary>
        public decimal ServiceTotalSum { get; set; }

        /// <summary>
        /// Возможность сменить локаль
        /// </summary>
        public bool CanChangeLocale { get; set; }
        
        /// <summary>
        /// Запросы на изменение департаментов аккаунта
        /// </summary>
        public List<DepartmentRequest>  DepartmentRequests { get; set; }

        public int? Deployment { get; set; }

        /// <summary>
        /// Все существующие локали
        /// </summary>
        public AccountCardKeyValueDataDto<Guid>[] Locales { get; set; }


    }
}
