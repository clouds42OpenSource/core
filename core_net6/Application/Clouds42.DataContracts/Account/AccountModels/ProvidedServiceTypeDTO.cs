﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель типа оказанной услуги
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ProvidedServiceTypeDto
    {
        /// <summary>
        /// Тип оказанной услуги
        /// </summary>
        [XmlElement(ElementName = nameof(ProvidedServiceType))]
        [DataMember(Name = nameof(ProvidedServiceType))]
        public string ProvidedServiceType { get; set; }
        
        /// <summary></summary>
        public ProvidedServiceTypeDto()
        {
        }
    }
}