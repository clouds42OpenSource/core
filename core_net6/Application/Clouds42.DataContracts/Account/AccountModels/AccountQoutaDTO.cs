﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountQoutaDto
    {
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        [XmlElement(ElementName = nameof(Limit))]
        [DataMember(Name = nameof(Limit))]
        public long Limit { get; set; }

        [XmlElement(ElementName = nameof(StorageID))]
        [DataMember(Name = nameof(StorageID))]
        public Guid StorageID { get; set; }
    }
}
