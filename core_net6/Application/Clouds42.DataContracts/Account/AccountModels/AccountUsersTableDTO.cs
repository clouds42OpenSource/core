﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary>
    /// Модель таблицы пользователей аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUsersTableDto
    {
        /// <summary>
        /// Таблица пользователей аккаунта
        /// </summary>
        [XmlElement(ElementName = "AccountSearchResultTable")]
        [DataMember(Name = "AccountSearchResultTable")]
        public AccountUsersTableModelDto AccountUserSearchResultTable { get; set; } = new();
    }
}
