﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Account.AccountModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ProvidedServiceVolumeDto
    {
        /// <summary> </summary>
        [XmlElement(ElementName = nameof(ProvidedServiceVolume))]
        [DataMember(Name = nameof(ProvidedServiceVolume))]
        public int ProvidedServiceVolume { get; set; }
        
        /// <summary></summary>
        public ProvidedServiceVolumeDto()
        {
        }
    }
}