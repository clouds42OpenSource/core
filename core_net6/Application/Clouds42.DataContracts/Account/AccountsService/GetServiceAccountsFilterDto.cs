﻿namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Фильтр выбора служебных аккаунтов
    /// </summary>
    public sealed class GetServiceAccountsFilterDto
    {
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchLine { get; set; }
    }
}