﻿using System;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Модель создания служебного аккаунта
    /// </summary>
    public class CreateServiceAccountDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID пользователя, инициировавшего создание
        /// </summary>
        public Guid AccountUserInitiatorId { get; set; }


        /// <summary>
        /// Логин пользователя, который создал служебный аккаунт
        /// </summary>
        public string AccountUserInitiatorName { get; set; }


        public static CreateServiceAccountDto CreateWith(AddServiceAccountDto item, Guid accountUserInitiatorId, string accountUserInitiatorName)
        {
            return new CreateServiceAccountDto
            {
                AccountId = item.AccountId,
                AccountUserInitiatorId = accountUserInitiatorId,
                AccountUserInitiatorName = accountUserInitiatorName
            };
        }
    }
}
