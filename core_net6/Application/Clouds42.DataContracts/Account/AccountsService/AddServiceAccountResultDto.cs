﻿using System;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Модель ответа на создание служебного аккаунта
    /// </summary>
    public sealed class AddServiceAccountResultDto
    {
        /// <summary>
        /// Логин пользователя, который создал служебный аккаунт
        /// </summary>
        public string AccountUserInitiatorName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }
    }
}
