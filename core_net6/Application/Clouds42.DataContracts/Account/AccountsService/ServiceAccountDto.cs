﻿using System;

using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Модель контракта сервисного аккаунта
    /// </summary>
    public class ServiceAccountDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        [Required(ErrorMessage = "Номер аккаунта - обязательное поле")]
        [Display(Name = "Номер аккаунта")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ID пользователя, который создал служебный аккаунт
        /// </summary>
        public Guid AccountUserInitiatorId { get; set; }

        /// <summary>
        /// Логин пользователя, который создал служебный аккаунт
        /// </summary>
        public string AccountUserInitiatorName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }
    }
}
