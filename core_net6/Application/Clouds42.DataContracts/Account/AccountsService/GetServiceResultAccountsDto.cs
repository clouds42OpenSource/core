﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Класс предаставляющий данные выборанных служебных аккаунтов
    /// </summary>
    public sealed class GetServiceResultAccountsDto : SelectDataResultCommonDto<ServiceAccountItemDto>
    {
    }
}