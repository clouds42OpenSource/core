﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Класс предаставляющий критерей выбора служебных аккаунтов
    /// </summary>
    public sealed class GetServiceAccountsDto: SelectDataCommonDto<GetServiceAccountsFilterDto>
    {
    }
}