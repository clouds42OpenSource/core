﻿using System;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Модель создания служебного аккаунта
    /// </summary>
    public sealed class AddServiceAccountDto
    {
        /// <summary>
        /// ID аккаунта, который добавить как служебный
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
