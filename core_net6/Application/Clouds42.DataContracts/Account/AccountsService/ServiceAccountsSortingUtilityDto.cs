﻿using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Утилита для сортиовки записей служебных аккаунтов
    /// </summary>
    public static class ServiceAccountsSortingUtilityDto
    {
        /// <summary>
        /// Mapping поля сортировки с действием сортировки
        /// </summary>
        private static readonly Dictionary<string, SortingDelegateQueryable<ServiceAccount>> SortingActions = new()
        {
            {ServiceAccountsSortFieldNamesDto.AccountIndexNumber, SortByAccountIndexNumber},
            {ServiceAccountsSortFieldNamesDto.AccountCaption, SortByAccountCaption},
            {ServiceAccountsSortFieldNamesDto.AccountUserInitiatorName, SortByAccountUserInitiatorName},
            {ServiceAccountsSortFieldNamesDto.CreationDateTime, SortByCreationDateTime}
        };

        /// <summary>
        /// Сортитровать записи служебных аккаунтов по убыванию даты создания
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <returns>Отсортированные записи служебных аккаунтов </returns>
        private static IOrderedQueryable<ServiceAccount> SortByDefault(IQueryable<ServiceAccount> records) 
            => records.OrderByDescending(row => row.CreationDateTime);


        /// <summary>
        /// Сортитровать записи служебных аккаунтов по полю Account.IndexNumber
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи служебных аккаунтов по полю Account.IndexNumber</returns>
        private static IOrderedQueryable<ServiceAccount> SortByAccountIndexNumber(IQueryable<ServiceAccount> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Account.IndexNumber),
                SortType.Desc => records.OrderByDescending(row => row.Account.IndexNumber),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать записи служебных аккаунтов по полю Account.AccountCaption
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи служебных аккаунтов по полю Account.AccountCaption</returns>
        private static IOrderedQueryable<ServiceAccount> SortByAccountCaption(IQueryable<ServiceAccount> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.Account.AccountCaption),
                SortType.Desc => records.OrderByDescending(row => row.Account.IndexNumber),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать записи служебных аккаунтов по полю AccountUser.Name
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи служебных аккаунтов по полю AccountUser.Name</returns>
        private static IOrderedQueryable<ServiceAccount> SortByAccountUserInitiatorName(IQueryable<ServiceAccount> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.AccountUser.LastName),
                SortType.Desc => records.OrderByDescending(row => row.AccountUser.LastName),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Сортитровать записи служебных аккаунтов по полю CreationDateTime
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortType">направление сортировки</param>
        /// <returns>Отсортированные записи служебных аккаунтов по полю CreationDateTime</returns>
        private static IOrderedQueryable<ServiceAccount> SortByCreationDateTime(IQueryable<ServiceAccount> records, SortType sortType)
        {
            return sortType switch
            {
                SortType.Asc => records.OrderBy(row => row.CreationDateTime),
                SortType.Desc => records.OrderByDescending(row => row.CreationDateTime),
                _ => SortByDefault(records)
            };
        }

        /// <summary>
        /// Производит сортировку выбранных записей служебных аккаунтов
        /// </summary>
        /// <param name="records">Записи для сортировки</param>
        /// <param name="sortingData">Информация о сортировке, как сортировать</param>
        /// <returns>Отсортированные записи служебных аккаунтов</returns>
        public static IOrderedQueryable<ServiceAccount> MakeSorting(IQueryable<ServiceAccount> records, SortingDataDto sortingData)
        {
            if (sortingData == null)
            {
                return SortByDefault(records);
            }

            var sortFieldName = (sortingData.FieldName ?? string.Empty).ToLower();
            
            return SortingActions.TryGetValue(sortFieldName, out var sortingDelegate) 
                ? sortingDelegate(records, sortingData.SortKind)
                : SortByDefault(records);
        }
    }
}