﻿using System;

namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Модель контракта сервисного аккаунта
    /// </summary>
    public sealed class ServiceAccountItemDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Логин пользователя, который создал служебный аккаунт
        /// </summary>
        public string AccountUserInitiatorName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }
    }
}
