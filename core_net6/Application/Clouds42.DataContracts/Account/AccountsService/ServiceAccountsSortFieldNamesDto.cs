﻿namespace Clouds42.DataContracts.Account.AccountsService
{
    /// <summary>
    /// Класс, предаставляющий наименование полей для сортировки служебных аккаунтов
    /// </summary>
    public static class ServiceAccountsSortFieldNamesDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public const string AccountIndexNumber = "accountindexnumber";

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public const string AccountCaption = "accountcaption";

        /// <summary>
        ///  Логин пользователя, который создал служебный аккаунт
        /// </summary>
        public const string AccountUserInitiatorName = "accountuserinitiatorname";

        /// <summary>
        ///   Дата создания служебного аккаунта
        /// </summary>
        public const string CreationDateTime = "creationdatetime";
    }
}