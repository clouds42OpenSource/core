﻿using System;

namespace Clouds42.DataContracts.Account.AccountAudit
{
    /// <summary>
    /// Модель аккаунта с неверным сегментом после миграции
    /// </summary>
    public class AccountWithInvalidSegmentAfterMigrationDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Дата операции
        /// </summary>
        public DateTime OperationDateTime { get; set; }

        /// <summary>
        /// Название целевого сегмента при миграции
        /// </summary>
        public string TargetSegmentName { get; set; }

        /// <summary>
        /// Название текущего сегмента аккаунта
        /// </summary>
        public string CurrentAccountSegmentName { get; set; }
    }
}
