﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueDateDto
    {
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date{ get; set; }

        public CoreWorkerTasksQueueDateDto()
        {

        }

        public CoreWorkerTasksQueueDateDto(DateTime date)
        {
            Date = date;
        }

    }
}
