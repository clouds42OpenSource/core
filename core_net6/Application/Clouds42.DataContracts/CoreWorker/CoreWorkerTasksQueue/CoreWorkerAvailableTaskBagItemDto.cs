﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель элемента данных по доступным задачам воркеров
    /// </summary>
    public class CoreWorkerAvailableTaskBagItemDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short WorkerId { get; set; }

        /// <summary>
        /// Название воркера
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// ID задачи
        /// </summary>
        public Guid TaskId { get; set; }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Приоритет задачи
        /// </summary>
        public int TaskPriority { get; set; }
    }
}
