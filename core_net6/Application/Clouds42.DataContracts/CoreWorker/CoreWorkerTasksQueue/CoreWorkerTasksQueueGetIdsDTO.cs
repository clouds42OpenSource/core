﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueGetIdsDto
    {
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status{ get; set; }


        [XmlElement(ElementName = nameof(maxRecordsCount))]
        [DataMember(Name = nameof(maxRecordsCount))]
        public int maxRecordsCount { get; set; }

        public CoreWorkerTasksQueueGetIdsDto()
        {

        }

    }
}
