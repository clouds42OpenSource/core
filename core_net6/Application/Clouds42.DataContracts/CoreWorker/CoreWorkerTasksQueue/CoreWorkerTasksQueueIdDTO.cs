﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueIdDto
    {
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public Guid Id { get; set; }

        public CoreWorkerTasksQueueIdDto() {
            
        }

        public CoreWorkerTasksQueueIdDto(Guid _id)
        {
            Id = _id;
        }
    }
}
