﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель доступных задач воркера
    /// </summary>
    public class CoreWorkerTasksBagsDataDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short WorkerId { get; set; }

        /// <summary>
        /// Название воркера
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// Доступные задачи для добавления
        /// </summary>
        public List<KeyValuePair<Guid, string>> AvailableTasksForAdd { get; set; }

        /// <summary>
        /// Собственные задачи воркера
        /// </summary>
        public List<KeyValuePair<Guid, string>> WorkerTasksBags { get; set; }
    }
}
