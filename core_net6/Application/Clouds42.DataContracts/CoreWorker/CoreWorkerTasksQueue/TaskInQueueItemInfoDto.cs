﻿using System;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель информации по задаче из очереди задач воркера
    /// </summary>
    public class TaskInQueueItemInfoDto
    {
        /// <summary>
        /// Id запущенной задачи
        /// </summary>
        public Guid TaskInQueueItemId { get; set; }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Параметры задачи
        /// </summary>

        public string TaskParams { get; set; }

        /// <summary>
        /// Дата запуска задачи
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        /// Приоритет
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// Тип задачи
        /// </summary>
        public WorkerTaskTypeEnum TaskType { get; set; }

        /// <summary>
        /// Комментарий к задаче
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        public string Status { get; set; }
    }
}
