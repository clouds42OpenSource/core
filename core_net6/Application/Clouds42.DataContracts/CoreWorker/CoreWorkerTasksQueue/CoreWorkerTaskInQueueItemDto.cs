﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель задачи из очереди воркеров
    /// </summary>
    public class CoreWorkerTaskInQueueItemDto
    {
        /// <summary>
        /// ID задачи
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Дата и время запуска
        /// </summary>
        public DateTime CreateDateTime { get; set; }

        /// <summary>
        /// Дата и время выполнения
        /// </summary>
        public DateTime? FinishDateTime { get; set; }

        /// <summary>
        /// ID воркера, который захватил задачу
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// Комментарий к задаче
        /// </summary>
        public string Comment { get; set; }
    }
}
