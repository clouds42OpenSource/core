﻿namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Результат аудита очереди задач воркера
    /// </summary>
    public class AuditCoreWorkerTasksQueueResultDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short CoreWorkerId { get; set; }

        /// <summary>
        /// Название воркера
        /// </summary>
        public string CoreWorkerName { get; set; }

        /// <summary>
        /// Количество задач в очереди
        /// </summary>
        public int TasksInQueueCount { get; set; }
    }

}
