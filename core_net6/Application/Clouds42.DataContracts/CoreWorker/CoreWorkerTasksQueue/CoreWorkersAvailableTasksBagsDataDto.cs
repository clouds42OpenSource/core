﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель данных по доступным задачам воркеров
    /// </summary>
    public sealed class CoreWorkersAvailableTasksBagsDataDto : SelectDataResultCommonDto<CoreWorkerAvailableTaskBagItemDto>
    {

    }
}
