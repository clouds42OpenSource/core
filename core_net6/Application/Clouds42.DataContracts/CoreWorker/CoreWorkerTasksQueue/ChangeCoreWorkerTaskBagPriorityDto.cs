﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель для изменения приоритета доступной задачи воркера
    /// </summary>
    public class ChangeCoreWorkerTaskBagPriorityDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short WorkerId { get; set; }

        /// <summary>
        /// ID задачи
        /// </summary>
        public Guid TaskId { get; set; }

        /// <summary>
        /// Приоритет задачи
        /// </summary>
        public int TaskPriority { get; set; }
    }
}
