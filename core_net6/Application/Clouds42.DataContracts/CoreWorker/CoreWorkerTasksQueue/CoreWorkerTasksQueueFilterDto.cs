﻿using System;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Фильтр поиска задач в очереди воркеров
    /// </summary>
    public class CoreWorkerTasksQueueFilterDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// ID задачи
        /// </summary>
        public Guid? TaskId { get; set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        public CloudTaskQueueStatus? Status { get; set; }

        /// <summary>
        /// Начальный период для поиска задач
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Конечный период для поиска задач
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }
    }
}
