﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Параметры фильтра для поиска списка задач в очереди воркеров
    /// </summary>
    public class CoreWorkerTasksQueueFilterParamsDto : SelectDataCommonDto<CoreWorkerTasksQueueFilterDto>
    {
    }
}
