﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель данных по очереди задач воркера
    /// </summary>
    public class CoreWorkerTasksInQueueDataDto : SelectDataResultCommonDto<CoreWorkerTaskInQueueItemDto>
    {
    }
}
