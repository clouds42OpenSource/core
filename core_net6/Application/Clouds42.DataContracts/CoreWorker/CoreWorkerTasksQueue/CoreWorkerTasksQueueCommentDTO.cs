﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueCommentDto
    {
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        public CoreWorkerTasksQueueCommentDto()
        {

        }
        public CoreWorkerTasksQueueCommentDto(string comment)        
        {
            Comment = comment;
        }
    }
}
