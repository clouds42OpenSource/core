﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerIdDto
    {
        [XmlElement(ElementName = nameof(CoreWorkerId))]
        [DataMember(Name = nameof(CoreWorkerId))]
        public int? CoreWorkerId { get; set; }

        public CoreWorkerIdDto()
        {

        }

        public CoreWorkerIdDto(int? coreWorkerId)
        {
            CoreWorkerId = coreWorkerId;
        }
    }
}
