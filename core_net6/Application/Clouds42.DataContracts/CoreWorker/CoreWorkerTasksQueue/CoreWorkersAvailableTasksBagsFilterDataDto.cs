﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Фильтр для получения данных по доступным задачам воркеров
    /// </summary>
    public sealed class CoreWorkersAvailableTasksBagsFilterDataDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short? WorkerId { get; set; }

        /// <summary>
        /// ID задачи
        /// </summary>
        public Guid? TaskId { get; set; }

        /// <summary>
        /// Приоритет задачи
        /// </summary>
        public int? TaskPriority { get; set; }
    }
}