﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTaskIdDto
    {
        [XmlElement(ElementName = nameof(CoreWorkerTaskTaskId))]
        [DataMember(Name = nameof(CoreWorkerTaskTaskId))]
        public Guid CoreWorkerTaskTaskId { get; set; }

        public CoreWorkerTaskIdDto()
        {

        }

        public CoreWorkerTaskIdDto(Guid id)
        {
            CoreWorkerTaskTaskId = id;
        }
    }
}
