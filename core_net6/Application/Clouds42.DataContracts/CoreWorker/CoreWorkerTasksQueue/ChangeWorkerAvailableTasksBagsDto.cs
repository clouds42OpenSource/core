﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель изменения доступных задач воркера
    /// </summary>
    public class ChangeWorkerAvailableTasksBagsDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short CoreWorkerId { get; set; }

        /// <summary>
        /// Список ID доступных задач воркера
        /// </summary>
        public List<Guid> WorkerTasksBagsIds { get; set; } = [];
    }
}
