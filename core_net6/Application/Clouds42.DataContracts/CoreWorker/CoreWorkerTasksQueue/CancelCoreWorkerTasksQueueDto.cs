﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    public class CancelCoreWorkerTasksQueueDto
    {
        /// <summary>
        /// Core worker's task in queue identifier
        /// </summary>
        public Guid CoreWorkerTasksQueueId { get; set; }

        /// <summary>
        /// Cancellation reason
        /// </summary>
        public string ReasonCancellation { get; set; }
    }
}
