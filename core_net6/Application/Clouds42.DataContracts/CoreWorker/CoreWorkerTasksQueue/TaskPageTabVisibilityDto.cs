﻿namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель видимости вкладок страницы "Задачи"
    /// </summary>
    public class TaskPageTabVisibilityDto
    {
        /// <summary>
        /// Признак, что кнопка "Добавить задачу" видна
        /// </summary>
        public bool IsAddTaskButtonVisible { get; set; }

        /// <summary>
        /// Признак, что вкладка "Управление" видна
        /// </summary>
        public bool IsControlTaskTabVisible { get; set; }

        /// <summary>
        /// Признак, что вкладка "Взаимодействие воркера и задач" видна
        /// </summary>
        public bool IsWorkerAvailableTasksBagsTabVisible { get; set; }

        /// <summary>
        /// Признак, что можно отменять запущенную задачу
        /// </summary>
        public bool CanCancelCoreWorkerTasksQueue { get; set; }
    }
}
