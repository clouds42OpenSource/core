﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Фильтр для получения данных по доступным задачам воркеров
    /// </summary>
    public sealed class CoreWorkersAvailableTasksBagsFilterDto : SelectDataCommonDto<CoreWorkersAvailableTasksBagsFilterDataDto>
    {
    }
}
