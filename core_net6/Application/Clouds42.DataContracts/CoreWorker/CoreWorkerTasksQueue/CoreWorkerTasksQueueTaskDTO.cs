﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueTaskDto
    {
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public Guid Id { get; set; }

        [XmlElement(ElementName = nameof(CoreWorkerTasksTaskId))]
        [DataMember(Name = nameof(CoreWorkerTasksTaskId))]
        public Guid CoreWorkerTasksTaskId { get; set; }

        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status { get; set; }

        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date { get; set; }

        [XmlElement(ElementName = nameof(CoreWorkerId))]
        [DataMember(Name = nameof(CoreWorkerId))]
        public Guid CoreWorkerId { get; set; }

        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }


        public CoreWorkerTasksQueueTaskDto()
        {

        }
                    
    }
}
