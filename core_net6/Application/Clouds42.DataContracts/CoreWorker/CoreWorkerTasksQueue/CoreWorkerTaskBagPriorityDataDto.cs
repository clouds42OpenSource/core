﻿namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    /// <summary>
    /// Модель приоритета доступной задачи воркера
    /// </summary>
    public class CoreWorkerTaskBagPriorityDataDto : ChangeCoreWorkerTaskBagPriorityDto
    {
        /// <summary>
        /// Название воркера
        /// </summary>
        public string WorkerName { get; set; }

        /// <summary>
        /// Название задачи
        /// </summary>
        public string TaskName { get; set; }
    }
}
