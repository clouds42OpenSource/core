﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksQueueStatusDto
    {
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status { get; set; }

        public CoreWorkerTasksQueueStatusDto()
        {

        }

        public CoreWorkerTasksQueueStatusDto(string status)
        {
            Status = status;
        }
    }
}
