﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public abstract class CoreWorkerTasksBaseDto
    {
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }

        [XmlElement(ElementName = nameof(CoreWorkersId))]
        [DataMember(Name = nameof(CoreWorkersId))]
        public int CoreWorkersId { get; set; }

        /// <summary>
        /// Дата и время отложенной операции.
        /// </summary>
        [XmlElement(ElementName = nameof(DateTimeDelayOperation))]
        [DataMember(Name = nameof(DateTimeDelayOperation))]
        public DateTime? DateTimeDelayOperation { get; set; }        

    }

    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksDto : CoreWorkerTasksBaseDto
    {
        [XmlElement(ElementName = nameof(CloudTaskId))]
        [DataMember(Name = nameof(CloudTaskId))]
        public Guid CloudTaskId { get; set; }     

    }


    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTaskByTypeDto : CoreWorkerTasksBaseDto
    {
        [XmlElement(ElementName = nameof(CoreWorkerTaskType))]
        [DataMember(Name = nameof(CoreWorkerTaskType))]
        public CoreWorkerTaskType CoreWorkerTaskType { get; set; }     

    }
}
