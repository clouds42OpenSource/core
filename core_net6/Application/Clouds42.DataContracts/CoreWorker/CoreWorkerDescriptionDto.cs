﻿namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Модель описания воркера
    /// </summary>
    public class CoreWorkerDescriptionDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Название воркера
        /// </summary>
        public string Name { get; set; }
    }
}
