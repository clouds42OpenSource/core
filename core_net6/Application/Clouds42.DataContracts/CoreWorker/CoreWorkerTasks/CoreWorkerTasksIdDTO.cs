﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksIdDto
    {
        [XmlElement(ElementName = nameof(SchedulerTaskID))]
        [DataMember(Name = nameof(SchedulerTaskID))]
        public Guid SchedulerTaskID { get; set; }

        public CoreWorkerTasksIdDto()
        {
           
        }
        public CoreWorkerTasksIdDto(Guid _ID)
        {
           SchedulerTaskID = _ID;
        }

    }
}
