﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksMethodNameDto
    {
        [XmlElement(ElementName = nameof(MethodName))]
        [DataMember(Name = nameof(MethodName))]
        public string MethodName  { get; set; }

        public CoreWorkerTasksMethodNameDto()
        {
           
        }
        public CoreWorkerTasksMethodNameDto(string methodName)
        {
            MethodName = methodName;
        }

    }
}
