﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksTypeDto
    {
        [XmlElement(ElementName = nameof(TaskType))]
        [DataMember(Name = nameof(TaskType))]
        public string TaskType { get; set; }

        public CoreWorkerTasksTypeDto()
        {
           
        }

        public CoreWorkerTasksTypeDto(string taskType)
        {
            TaskType = taskType;
        }
    }
}
