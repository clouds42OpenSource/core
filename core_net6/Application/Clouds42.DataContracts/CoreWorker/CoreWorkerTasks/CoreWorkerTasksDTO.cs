﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksDto
    {
        [XmlElement(ElementName = nameof(TaskType))]
        [DataMember(Name = nameof(TaskType))]
        public string TaskType { get; set; }

        [XmlElement(ElementName = nameof(MethodName))]
        [DataMember(Name = nameof(MethodName))]
        public string MethodName { get; set; }

        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }

        [XmlElement(ElementName = nameof(TaskName))]
        [DataMember(Name = nameof(TaskName))]
        public string TaskName { get; set; }

        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public bool Status { get; set; }


        public CoreWorkerTasksDto()
        {

        }

    }
}
