﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    /// <summary>
    /// Модель фильтра задач воркера
    /// </summary>
    public sealed class CoreWorkerTaskFilterDto: SelectDataCommonDto<CoreWorkerTaskFilterInfoDto>
    {
    }
}
