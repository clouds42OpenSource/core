﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksCountDto
    {
        [XmlElement(ElementName = nameof(Count))]
        [DataMember(Name = nameof(Count))]
        public int Count { get; set; }

        public CoreWorkerTasksCountDto()
        {
           
        }

        public CoreWorkerTasksCountDto(int _count)
        {
            Count = _count;
        }
    }
}
