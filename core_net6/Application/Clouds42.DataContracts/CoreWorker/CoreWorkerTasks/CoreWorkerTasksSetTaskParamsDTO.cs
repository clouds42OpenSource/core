﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksSetTaskParamsDto
    {
        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }

        [XmlElement(ElementName = nameof(SchedulerTaskID))]
        [DataMember(Name = nameof(SchedulerTaskID))]
        public Guid SchedulerTaskID  { get; set; }


        public CoreWorkerTasksSetTaskParamsDto()
        {

        }
    }
}
