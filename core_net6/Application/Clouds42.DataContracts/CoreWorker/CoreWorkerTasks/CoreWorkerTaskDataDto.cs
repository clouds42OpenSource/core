﻿namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    /// <summary>
    /// Модель данных задачи воркера
    /// </summary>
    public class CoreWorkerTaskDataDto : EditCoreWorkerTaskDto
    {
        /// <summary>
        /// Название задачи
        /// </summary>
        public string Name { get; set; }
    }
}
