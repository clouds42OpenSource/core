﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksSetMethodNameDto
    {
        [XmlElement(ElementName = nameof(MethodName))]
        [DataMember(Name = nameof(MethodName))]
        public string MethodName { get; set; }

        [XmlElement(ElementName = nameof(SchedulerTaskID))]
        [DataMember(Name = nameof(SchedulerTaskID))]
        public Guid SchedulerTaskID { get; set; }


        public CoreWorkerTasksSetMethodNameDto()
        {

        }
    }
}
