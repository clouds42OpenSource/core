﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksSetTaskTypeDto
    {
        [XmlElement(ElementName = nameof(TaskType))]
        [DataMember(Name = nameof(TaskType))]
        public string TaskType { get; set; }

        [XmlElement(ElementName = nameof(SchedulerTaskID))]
        [DataMember(Name = nameof(SchedulerTaskID))]
        public Guid SchedulerTaskID  { get; set; }


        public CoreWorkerTasksSetTaskTypeDto()
        {

        }
    }
}
