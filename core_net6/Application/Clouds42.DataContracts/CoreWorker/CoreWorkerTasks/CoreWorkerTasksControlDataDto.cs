﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    /// <summary>
    /// Модель информации об элементах управления задачами воркера
    /// </summary>
    public sealed class CoreWorkerTasksControlDataDto: SelectDataResultCommonDto<CoreWorkerTaskDataDto>
    {
    }
}
