﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksParamsDto
    {
        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }

        public CoreWorkerTasksParamsDto()
        { 
        }

        public CoreWorkerTasksParamsDto(string taskParams)
        {
            TaskParams = taskParams;
        }
    }
}
