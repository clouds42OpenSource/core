﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CoreWorkerTasksListIdDto 
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = nameof(List);


        [XmlElement(ElementName = "SchedulerTaskIDs")]
        [DataMember(Name = "SchedulerTaskIDs")]
        public List<Guid> List { get; set; } = [];
    }
}
