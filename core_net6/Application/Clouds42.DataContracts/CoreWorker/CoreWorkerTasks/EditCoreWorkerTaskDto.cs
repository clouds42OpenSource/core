﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    /// <summary>
    /// Модель редактирования задачи воркера
    /// </summary>
    public class EditCoreWorkerTaskDto
    {
        /// <summary>
        /// Id задачи
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Время жизни выполнения задачи в минутах
        /// </summary>
        public int TaskExecutionLifetimeInMinutes { get; set; }
    }
}
