﻿using System;

namespace Clouds42.DataContracts.CoreWorker.CoreWorkerTasks
{
    /// <summary>
    /// Фильтр для получения задач воркера
    /// </summary>
    public sealed class CoreWorkerTaskFilterInfoDto
    {
        /// <summary>
        /// Id задачи
        /// </summary>
        public Guid? TaskId { get; set; }

    }
}