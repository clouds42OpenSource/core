﻿namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Параметры перезапуска задачи
    /// </summary>
    public class RetryJobParamsDto
    {
        /// <summary>
        /// Признак необходимости перезапускать задачу
        /// </summary>
        public bool NeedRetry { get; set; }

        /// <summary>
        /// Задержка перед повторным перезапуском задачи
        /// </summary>
        public int RetryDelayInSeconds { get; set; } = 30;
    }
}
