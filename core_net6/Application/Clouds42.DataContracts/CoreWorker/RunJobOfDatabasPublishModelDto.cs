﻿using System;

namespace Clouds42.DataContracts.CoreWorker
{
    public class RunJobOfDatabasPublishModelDto
    {
        public Guid AccountDatabaseID { set; get; }
        public Guid AccountUsersId { set; get; }
        public string Login { set; get; }
        public string Pass { set; get; }
        public string ExtensionName { set; get; }
        public bool NeedPublishExtension { set; get; }
        public string LinkServicePath { set; get; }
    }
}
