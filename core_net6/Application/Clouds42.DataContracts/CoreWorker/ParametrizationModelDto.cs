﻿using JetBrains.Annotations;

namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Модель параметров задачи
    /// </summary>
    public class ParametrizationModelDto([NotNull] object taskParams)
    {
        public ParametrizationModelDto([NotNull] object taskParams, [NotNull] string synchronizationKey) : this(taskParams)
        {
            SynchronizationKey = synchronizationKey;
        }

        /// <summary>
        /// Параметры
        /// </summary>
        public object TaskParams { get; } = taskParams;

        /// <summary>
        /// Ключ синхронизации
        /// </summary>
        public string SynchronizationKey { get; }
    }
}
