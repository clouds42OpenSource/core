﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CoreWorker
{
    [XmlInclude(typeof(Dictionary<string,string>))]
    public class CoreWorkerSettingsDto
    {
        public Dictionary<string, string> Secrets { get; } = new();
        public Dictionary<string, string> Settings { get; }
    }
}
