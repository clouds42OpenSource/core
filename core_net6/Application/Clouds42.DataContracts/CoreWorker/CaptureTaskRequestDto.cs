﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Модель запроса для захвата задачи воркером
    /// </summary>
    [XmlRoot("Request")]
    public class CaptureTaskRequestDto
    {
        /// <summary>
        /// ID воркера
        /// </summary>
        [XmlElement(ElementName = nameof(CoreWorkerId))]
        [JsonProperty(nameof(CoreWorkerId))]
        public short CoreWorkerId { get; set; }

        /// <summary>
        /// Внутренняя очередь задач
        /// </summary>
        [XmlElement(ElementName = nameof(InternalTasksQueue))]
        [JsonProperty(nameof(InternalTasksQueue))]
        public InternalTasksQueueDto InternalTasksQueue { get; set; } = new();
    }

    /// <summary>
    /// Модель внутренней очереди
    /// </summary>
    public class InternalTasksQueueDto
    {
        /// <summary>
        /// Внутренняя очередь задач
        /// </summary>
        [XmlElement(ElementName = "TaskId")]
        [JsonProperty("TaskId")]
        public List<Guid> List { get; set; } = [];
    }

    public class UpdateTaskStatusRequestDto
    {
        public Guid TaskId { get; set; }
        public JobExecutionResultDto ExecutionResult { get; set; }
        public string Comment { get; set; }
    }
}
