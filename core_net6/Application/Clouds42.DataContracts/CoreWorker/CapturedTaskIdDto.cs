﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Модель ID захваченной задачи воркера
    /// </summary>
    [XmlRoot(ElementName = "Response")]
    [DataContract(Name = "Response")]
    public class CapturedTaskIdDto
    {
        public CapturedTaskIdDto()
        {

        }

        public CapturedTaskIdDto(Guid? id)
        {
            CapturedTaskId = id;
        }

        /// <summary>
        /// ID захваченной задачи
        /// </summary>
        [XmlElement(ElementName = nameof(CapturedTaskId))]
        [DataMember(Name = nameof(CapturedTaskId))]
        [JsonProperty(nameof(CapturedTaskId))]
        public Guid? CapturedTaskId { get; set; }
    }
}
