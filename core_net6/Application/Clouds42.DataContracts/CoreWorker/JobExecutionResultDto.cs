﻿using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.CoreWorker
{
    /// <summary>
    /// Результат выполнения задачи
    /// </summary>
    public class JobExecutionResultDto
    {
        public JobExecutionResultDto()
        {

        }

        public JobExecutionResultDto(CloudTaskQueueStatus executionStatus, int? retryDelayInSeconds = null)
        {
            TaskQueueStatus = executionStatus;

            if (executionStatus == CloudTaskQueueStatus.NeedRetry)
            {
                RetryJobParams = new RetryJobParamsDto
                {
                    NeedRetry = true,
                    RetryDelayInSeconds = retryDelayInSeconds ?? 30
                };
            }
        }

        /// <summary>
        /// Статус выполнения задачи
        /// </summary>
        public CloudTaskQueueStatus TaskQueueStatus { get; set; }

        /// <summary>
        /// Параметры перезапуска задачи
        /// </summary>
        public RetryJobParamsDto RetryJobParams { get; set; }
    }
}
