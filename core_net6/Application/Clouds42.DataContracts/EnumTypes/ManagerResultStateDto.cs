﻿namespace Clouds42.DataContracts.EnumTypes
{
    public enum ManagerResultStateDto
    {
        PreconditionFailed,
        NotFound,
        Ok,
        Forbidden,
        BadRequest,
        Conflict,
        Unauthorized,
        MethodNotAllowed
    }
}