﻿namespace Clouds42.DataContracts.EnumTypes
{
    public enum ErpDBTypesDto
    {
        CloudDB, 
        MyLocalDB, 
        AvailableCloudDB
    }
}