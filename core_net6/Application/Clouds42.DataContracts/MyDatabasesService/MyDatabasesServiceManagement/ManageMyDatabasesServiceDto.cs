﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement
{
    /// <summary>
    /// Модель для управления сервисом "Мои информационные базы"
    /// </summary>
    public class ManageMyDatabasesServiceDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Лимит на бесплатное создание баз
        /// </summary>
        public int? LimitOnFreeCreationDb { get; set; }

        /// <summary>
        /// Стоимость создания базы, свыше бесплатного тарифа
        /// </summary>
        public decimal? CostOfCreatingDbOverFreeLimit { get; set; }

        /// <summary>
        /// Стоимость размещения серверной базы
        /// </summary>
        public decimal? ServerDatabasePlacementCost { get; set; }
    }
}
