﻿namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement
{
    /// <summary>
    /// Модель данных по управлению
    /// сервисом "Мои информационные базы"
    /// </summary>
    public class MyDatabasesServiceManagementDataDto
    {
        /// <summary>
        /// Лимит на бесплатное создание баз
        /// </summary>
        public int LimitOnFreeCreationDb { get; set; }

        /// <summary>
        /// Стоимость создания базы, свыше бесплатного тарифа
        /// </summary>
        public decimal CostOfCreatingDbOverFreeLimit { get; set; }

        /// <summary>
        /// Стоимость размещения серверной базы
        /// </summary>
        public decimal ServerDatabasePlacementCost { get; set; }
    }
}
