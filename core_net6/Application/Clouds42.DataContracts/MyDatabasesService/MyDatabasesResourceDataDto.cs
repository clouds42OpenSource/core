﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService
{
    /// <summary>
    /// Модель данных ресурса услуги сервиса "Мои ифн. базы"
    /// </summary>
    public class MyDatabasesResourceDataDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Стоимость ресурса
        /// </summary>
        public decimal ResourceCost { get; set; }

        /// <summary>
        /// Количество оплаченных информационных баз
        /// </summary>
        public int PaidDatabasesCount { get; set; }

        /// <summary>
        /// Актуальное количество инф. баз
        /// </summary>
        public int ActualDatabasesCount { get; set; }
    }
}
