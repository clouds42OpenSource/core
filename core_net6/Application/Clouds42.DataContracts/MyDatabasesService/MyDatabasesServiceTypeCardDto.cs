﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService
{
    /// <summary>
    /// Модель карточки услуги сервиса "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceTypeCardDto
    {
        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID сервиса
        /// </summary>
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Название услуги сервиса
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Отображаемое название
        /// </summary>
        public string DisplayedName { get; set; }

        /// <summary>
        /// Дата окончания срока действия услуги сервиса
        /// </summary>
        public DateTime ServiceTypeExpireDate { get; set; }

        /// <summary>
        /// Отображаемая дата окончания
        /// </summary>
        public DateTime DisplayedExpireDate { get; set; }

        /// <summary>
        /// Статус услуги сервиса
        /// </summary>
        public ServiceStatusModelDto ServiceTypeStatus { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Кол-во инф. баз
        /// </summary>
        public int DatabasesCount { get; set; }

        /// <summary>
        /// Стоимость за одну инф. базу
        /// </summary>
        public decimal CostPerOneDatabase { get; set; }

        /// <summary>
        /// Стоимость за одну лицензию
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Общая стоимость баз
        /// </summary>
        public decimal TotalDatabasesCost { get; set; }

        /// <summary>
        /// Тип системной услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Общая стоимость услуги сервиса
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Количество используемых лицензий
        /// </summary>
        public int UsedLicenses { get; set; }

        /// <summary>
        /// Количество спонсирских лицензий (Я спонсирую)
        /// </summary>
        public int IamSponsorLicenses { get; set; }

        /// <summary>
        /// Количество спонсирских лицензий (Меня спонсируют)
        /// </summary>
        public int MeSponsorLicenses { get; set; }
    }
}
