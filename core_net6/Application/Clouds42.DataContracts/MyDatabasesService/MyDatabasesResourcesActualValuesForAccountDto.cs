﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService
{
    /// <summary>
    /// Актуальные значения ресурсов сервиса "Мои инф. базы"
    /// для аккаунта
    /// </summary>
    public class MyDatabasesResourcesActualValuesForAccountDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество информационных баз
        /// </summary>
        public int DatabasesCount { get; set; }

        /// <summary>
        /// Количество сервреных баз
        /// </summary>
        public int ServerDatabasesCount { get; set; }
    }
}
