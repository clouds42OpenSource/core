﻿namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель кратких данных по сервису "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceShortDataDto
    {
        /// <summary>
        /// Лимит на бесплатное кол-во баз
        /// </summary>
        public int LimitOnFreeCreationDb { get; set; }

        /// <summary>
        /// Кол-во баз свыше лимита
        /// </summary>
        public int CountDatabasesOverLimit { get; set; }

        /// <summary>
        /// Кол-во серверных баз
        /// </summary>
        public int ServerDatabasesCount { get; set; }

        /// <summary>
        /// Стоимость размещения инф. базы
        /// </summary>
        public decimal DatabasePlacementCost { get; set; }

        /// <summary>
        /// Стоимость размещения серверной инф. базы
        /// </summary>
        public decimal ServerDatabasePlacementCost { get; set; }

        /// <summary>
        /// Сумма оплаты за серверные инф. базы
        /// </summary>
        public decimal TotalAmountForServerDatabases { get; set; }

        /// <summary>
        /// Сумма оплаты за инф. базы
        /// </summary>
        public decimal TotalAmountForDatabases { get; set; }
    }
}
