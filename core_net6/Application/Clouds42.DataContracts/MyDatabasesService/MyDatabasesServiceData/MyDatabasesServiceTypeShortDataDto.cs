﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных с краткой информацией
    /// по услуге сервиса "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceTypeShortDataDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Тип системной услуги
        /// </summary>
        public ResourceType SystemServiceType { get; set; }
    }
}
