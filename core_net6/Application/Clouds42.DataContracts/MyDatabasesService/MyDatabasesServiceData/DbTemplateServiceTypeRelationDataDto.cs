﻿using System;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных связи шаблона базы и услуги сервиса "Мои информационные базы"
    /// </summary>
    public class DbTemplateServiceTypeRelationDataDto
    {
        /// <summary>
        /// Id шаблона информационной базы
        /// </summary>
        public Guid DbTemplateId { get; set; }

        /// <summary>
        /// Услуга сервиса "Мои информационные базы"
        /// </summary>
        public BillingServiceType ServiceType { get; set; }
    }
}
