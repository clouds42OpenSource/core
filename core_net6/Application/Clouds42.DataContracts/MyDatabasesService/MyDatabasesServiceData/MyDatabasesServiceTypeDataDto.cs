﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных услуги сервиса "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceTypeDataDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal ServiceTypeCost { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }
    }
}
