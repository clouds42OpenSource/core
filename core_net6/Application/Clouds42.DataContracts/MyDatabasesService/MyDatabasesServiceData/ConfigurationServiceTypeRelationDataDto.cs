﻿using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных связи конфигурации и услуги сервиса "Мои информационные базы"
    /// </summary>
    public class ConfigurationServiceTypeRelationDataDto
    {
        /// <summary>
        /// Конфигурация 1С
        /// </summary>
        public Configurations1C Configuration1C { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1С
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];

        /// <summary>
        /// Услуга сервиса "Мои информационные базы"
        /// </summary>
        public BillingServiceType ServiceType { get; set; }
    }
}
