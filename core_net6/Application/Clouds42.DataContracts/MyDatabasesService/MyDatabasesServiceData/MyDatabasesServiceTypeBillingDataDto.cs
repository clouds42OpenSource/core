﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных биллинга услуги сервиса "Мои инф. базы"
    /// </summary>
    public class MyDatabasesServiceTypeBillingDataDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Описание услуги сервиса
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Описание услуги сервиса
        /// </summary>
        public string ServiceTypeDescription { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType SystemServiceType { get; set; }

        /// <summary>
        /// Тип системного сервиса 42clouds
        /// </summary>
        public Clouds42Service? Clouds42ServiceType { get; set; }

        /// <summary>
        /// Признак что сервис услуги активен
        /// </summary>
        public bool IsActiveService { get; set; }

        /// <summary>
        /// Стоимость одной лицензии услуги
        /// </summary>
        public decimal CostPerOneLicense { get; set; }

        /// <summary>
        /// Лимит на бесплатное количество ресурсов
        /// </summary>
        public int LimitOnFreeLicenses { get; set; }

        /// <summary>
        /// Объем(количество ресурсов) 
        /// </summary>
        public int VolumeInQuantity { get; set; }

        /// <summary>
        /// Общая стоимость ресурсов
        /// </summary>
        public decimal TotalAmount { get; set; }
    }
}
