﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData
{
    /// <summary>
    /// Модель данных стоимости услуги для инф. базы(по конфигурации базы)
    /// </summary>
    public class ServiceTypeCostDataForDatabaseDto
    {
        /// <summary>
        /// Id аккаунта(владелец базы по которому определяли тариф)
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal ServiceTypeCost { get; set; }

        /// <summary>
        /// Флаг указывающий что база является файловой
        /// </summary>
        public bool IsFileDatabase { get; set; }
    }
}
