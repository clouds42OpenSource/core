﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers
{
    /// <summary>
    /// Модель триггера на отключение Аренды 1С у пользователей
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public class OnDisabledRent1CForUsersTriggerDto
    {
        /// <summary>
        /// Список Id пользователей
        /// у которых отключили Аренду 1С
        /// </summary>
        public List<Guid> AccountUserIds { get; set; } = [];
    }
}
