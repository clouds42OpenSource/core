﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers
{
    /// <summary>
    /// Модель триггера на изменение шаблона у инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public class OnChangeDatabaseTemplateTriggerDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }
    }
}
