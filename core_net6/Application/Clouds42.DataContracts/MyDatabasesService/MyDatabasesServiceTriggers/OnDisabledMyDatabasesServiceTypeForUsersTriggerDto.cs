﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers
{
    /// <summary>
    /// Модель триггера на отключение лицензии по услуге сервиса
    /// "Мои информационные базы" у пользователей
    /// </summary>
    public class OnDisabledMyDatabasesServiceTypeForUsersTriggerDto
    {
        /// <summary>
        /// Id услуги по которой отключили лицензии
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Список Id пользователей
        /// у которых отключили лицензии
        /// </summary>
        public List<Guid> AccountUserIds { get; set; } = [];
    }
}
