﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService
{
    /// <summary>
    /// Модель создания ресурса "Мои информационные базы"
    /// </summary>
    public class CreateMyDatabasesResourceDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id аккаунта спонсора
        /// </summary>
        public Guid? AccountSponsorId { get; set; }

        /// <summary>
        /// Стоимость ресурса
        /// </summary>
        public decimal ResourceCost { get; set; }

        /// <summary>
        /// Тип системной услуги
        /// </summary>
        public ResourceType SystemServiceType { get; set; }

        /// <summary>
        /// Количество оплаченных информационных баз
        /// </summary>
        public int PaidDatabasesCount { get; set; }

        /// <summary>
        /// Актуальное количество инф. баз
        /// </summary>
        public int ActualDatabasesCount { get; set; }
    }
}
