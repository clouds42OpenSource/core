﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель доступа пользователя для загруженного файла
    /// </summary>
    public class AccountUserLicenceForUploudFileDto
    {
        /// <summary>
        /// ID сервиса
        /// </summary>
        public Guid ServiceTypesId { get; set; }

        /// <summary>
        /// Список пользователей с ценой доступа к конфигурации 
        /// </summary>
        public List<AccountUserLicenceForConfigurationDto> AccountUserLicence { get; set; } = [];
    }
}
