﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель доступа пользователя в конфигурацию
    /// </summary>
    public class AccountUserLicenceForConfigurationDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Признак наличия лицензии
        /// </summary>
        public bool HasLicense { get; set; }

        /// <summary>
        /// Стоимость доступа
        /// </summary>
        public decimal AccessCost { get; set; }
    }
}
