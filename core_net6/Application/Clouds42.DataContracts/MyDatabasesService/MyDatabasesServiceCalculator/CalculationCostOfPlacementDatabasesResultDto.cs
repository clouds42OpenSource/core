﻿namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Model of the result of calculating the cost of placing an infobase for an account
    /// </summary>
    public class CalculationCostOfPlacementDatabasesResultDto
    {
        /// <summary>
        /// Total cost
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Patial cost
        /// </summary>
        public decimal PartialCost { get; set; }

        /// <summary>
        /// Количество инф. баз за которые посчитана стоимость(исключая уже оплаченные базы)
        /// Количество баз для ресурса
        /// </summary>
        public int DatabasesForWhichCostCalculatedCount { get; set; }

        /// <summary>
        /// Кол-во баз за которые необходимо оплатить
        /// </summary>
        public int DatabasesCountForWhichYouNeedPayment { get; set; }

        /// <summary>
        /// Актуальное количество инф. баз
        /// С учетом новых, которые нужно разместить
        /// </summary>
        public int ActualDatabasesCount { get; set; }
    }
}
