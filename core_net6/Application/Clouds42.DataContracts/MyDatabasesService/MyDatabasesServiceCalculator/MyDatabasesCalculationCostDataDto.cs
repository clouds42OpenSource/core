﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель данных для калькуляции
    /// услуги сервиса "Мои информационные базы"
    /// </summary>
    public class MyDatabasesCalculationCostDataDto
    {
        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Актуальная стоимость услуги
        /// </summary>
        public decimal ActualCost { get; set; }

        /// <summary>
        /// Дата пролонгации главного сервиса
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Количество инф. баз купленных
        /// </summary>
        public int PaidDatabasesCount { get; set; }

        /// <summary>
        /// Количество уже размещенных(созданных) инф. баз
        /// </summary>
        public int PlacementDatabasesCount { get; set; }
    }
}
