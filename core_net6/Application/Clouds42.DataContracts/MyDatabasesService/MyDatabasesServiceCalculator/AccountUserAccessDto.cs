﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Account user access dto
    /// </summary>
    public class AccountUserAccessDto
    {
        /// <summary>
        /// Account user identifier
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Has access flag
        /// </summary>
        public bool HasAccess { get; set; }
    }
}
