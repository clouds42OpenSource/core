﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель подсчета стоимости размещения инф. баз для аккаунта
    /// </summary>
    public class CalculateCostOfPlacementDatabasesDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество баз для размещения
        /// </summary>
        public int DatabasesForPlacementCount { get; set; }

        /// <summary>
        /// Тип системной услуги(услуги на аккаунт)
        /// </summary>
        public ResourceType SystemServiceType { get; set; }

        /// <summary>
        /// Дата оплаты
        /// </summary>
        public DateTime? PaidDate { get; set; }
    }
}
