﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель данных для рассчета стоимости доступа в конфигурацию
    /// </summary>
    public class CalculationDataForAccessToConfiguration1CDto
    {
        /// <summary>
        /// Дата окончания действия сервиса "Мои инф. базы"
        /// </summary>
        public DateTime MyDatabasesServiceExpireDate { get; set; }

        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Количество свободных ресурсов
        /// </summary>
        public int FreeResourcesCount { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Cost { get; set; }
    }
}
