﻿using System;

namespace Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator
{
    /// <summary>
    /// Модель стоимости доступа к услуге сервиса
    /// </summary>
    public class AccessForServiceTypeCostDto
    {
        /// <summary>
        /// ID услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Частичная стоимость
        /// </summary>
        public decimal PartialCost { get; set; }

        /// <summary>
        /// Кол-во свободных ресурсов
        /// </summary>
        public int FreeResourcesCount { get; set; }
    }
}
