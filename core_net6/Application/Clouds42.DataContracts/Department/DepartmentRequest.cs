﻿using System;

namespace Clouds42.DataContracts.Department;


/// <summary>
/// Класс запроса на изменение департамента
/// </summary>
public class DepartmentRequest
{
    public Guid DepartmentId { get; set; }
    
    /// <summary>
    /// Название департамента
    /// </summary>
    public string Name { get; set; }
    
    /// <summary>
    /// Тип действия
    /// </summary>
    public DepartmentAction ActionType { get; set; }
}
