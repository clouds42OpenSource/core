﻿namespace Clouds42.DataContracts.Department;

public enum DepartmentAction {   
    Create = 1,
    Update = 2,
    Remove = 3,
}
