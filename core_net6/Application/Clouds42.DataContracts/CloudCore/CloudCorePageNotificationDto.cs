﻿namespace Clouds42.DataContracts.CloudCore
{
    public class CloudCorePageNotificationDto
    {
        public string PageNotification { get; set; }
    }
}
