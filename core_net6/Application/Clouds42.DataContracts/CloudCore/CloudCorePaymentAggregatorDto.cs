﻿using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudCore
{
    public class CloudCorePaymentAggregatorDto
    {
        /// <summary>
        /// Aggregator type for Russian locale
        /// </summary>
        public RussianLocalePaymentAggregatorEnum Aggregator { get; set; }
    }
}
