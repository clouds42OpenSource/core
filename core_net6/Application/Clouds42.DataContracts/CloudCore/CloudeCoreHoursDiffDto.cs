﻿namespace Clouds42.DataContracts.CloudCore
{
    public class CloudeCoreHoursDiffDto
    {
        public int Hours { get; set; }
    }
}
