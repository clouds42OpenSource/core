﻿using System;

namespace Clouds42.DataContracts.CloudCore
{
    public class CloudCoreDefSegmentDto
    {
        public Guid DefSegmentId { get; set; }
    }
}
