﻿using System;

namespace Clouds42.DataContracts.AccountConfiguration
{
    /// <summary>
    /// Модель конфигурации аккаунта
    /// </summary>
    public class AccountConfigurationDto
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежит конфигурация
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id поставщика аккаунта
        /// </summary>
        public Guid SupplierId { get; set; }

        /// <summary>
        /// Id сегмента аккаунта
        /// </summary>
        public Guid SegmentId { get; set; }

        /// <summary>
        /// Id файлового хранилища аккаунта
        /// </summary>
        public Guid FileStorageId { get; set; }

        /// <summary>
        /// Id локали аккаунта
        /// </summary>
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Тип аккаунта
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool IsVip { get; set; }

        /// <summary>
        /// Ссылка на склеп аккаунта
        /// </summary>
        public string CloudStorageWebLink { get; set; }
    }
}
