﻿namespace Clouds42.DataContracts.AccountConfiguration
{
    /// <summary>
    /// Модель данных конфигурации аккаунта по сегменту
    /// </summary>
    public class AccountConfigurationBySegmentDataDto
    {
        /// <summary>
        /// Аккаунт 
        /// </summary>
        public Domain.DataModels.Account Account { get; set; }

        /// <summary>
        /// Сегмент аккаунта
        /// </summary>
        public Domain.DataModels.CloudServicesSegment Segment { get; set; }
    }
}
