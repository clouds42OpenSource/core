﻿namespace Clouds42.DataContracts.AccountConfiguration
{
    /// <summary>
    /// Модель аккаунта с его конфигурацией
    /// </summary>
    public class AccountWithConfigurationDto
    {
        /// <summary>
        /// Аккаунт облака
        /// </summary>
        public Domain.DataModels.Account Account { get; set; }

        /// <summary>
        /// Конфигурация аккаунта
        /// </summary>
        public Domain.DataModels.AccountProperties.AccountConfiguration AccountConfiguration { get; set; }
    }
}
