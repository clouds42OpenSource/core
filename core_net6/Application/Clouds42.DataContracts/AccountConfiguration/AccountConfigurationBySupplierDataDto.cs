﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountConfiguration
{
    /// <summary>
    /// Модель данных конфигурации аккаунта по поставщику
    /// </summary>
    public class AccountConfigurationBySupplierDataDto
    {
        /// <summary>
        /// Аккаунт 
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Поставщик аккаунта
        /// </summary>
        public ISupplier Supplier { get; set; }
    }
}
