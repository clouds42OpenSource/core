﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountConfiguration
{
    /// <summary>
    ///  Модель данных конфигурации аккаунта по локали
    /// </summary>
    public class AccountConfigurationByLocaleDataDto
    {
        /// <summary>
        /// Аккаунт 
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Локаль аккаунта
        /// </summary>
        public ILocale Locale { get; set; }
    }
}
