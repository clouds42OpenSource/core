﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель данных планирования ТиИ/АО информационных баз.
    /// </summary>
    public class BeforeSupportAccountDatabaseModelDto
    {

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Список информационных баз, в которых будет проведено ТиИ или АО.
        /// </summary>
        public ICollection<DbItem> SupportAccountDatabases { get; set; }

        /// <summary>
        /// Модель инф. базы с данными для уведомления
        /// </summary>
        public class DbItem
        {

            /// <summary>
            /// Системное имя базы.
            /// </summary>
            public string V82Name { get; set; }

            /// <summary>
            /// Именование базы.
            /// </summary>
            public string Caption { get; set; }

            /// <summary>
            /// Текущая версия конфигурации.
            /// </summary>
            public string CurrentVersion { get; set; }

            /// <summary>
            /// Целевая версия конфигурации.
            /// </summary>
            public string UpdateVersion { get; set; }

            /// <summary>
            /// Тип операции.
            /// </summary>
            public DatabaseSupportOperation Operation { get; set; }

            /// <summary>
            /// ID аккаунта
            /// </summary>
            public Guid AccountId { get; set; }

            public AutoUpdateTimeEnum TimeOfOperation { get; set; }
        }

    }
}
