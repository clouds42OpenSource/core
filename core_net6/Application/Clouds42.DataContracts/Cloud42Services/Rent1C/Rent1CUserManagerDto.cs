using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Модель управления ресурсом Аренды 1С
    /// </summary>
    public class Rent1CUserManagerDto
    {
        /// <summary>
        /// Логин
        /// </summary>
        [Display(Name = "Логин")]
        public string Login { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        [Display(Name = "Полное имя")]
        public string Name { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        [Display(Name = "Стоимость")]
        public decimal Cost { get; set; }

        /// <summary>
        /// Id ресурса
        /// </summary>
        public Guid ResourceId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        [Display(Name = "Тип лицензии")]
        public string ServiceTypeName { get; set; }
    }
}