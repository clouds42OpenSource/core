using System;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class UpdaterAccessRent1CRequestDto
    {
        public Guid AccountUserId { get; set; }
        public Guid? WebResourceId { get; set; }
        public Guid? RdpResourceId { get; set; }
        public bool WebResource { get; set; }
        public bool StandartResource { get; set; }
        public bool SponsorAccountUser { get; set; }
    }
}