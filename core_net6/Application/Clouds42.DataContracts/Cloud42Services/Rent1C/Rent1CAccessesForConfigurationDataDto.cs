﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Данные для конфигурации лицензий Аренды 1С
    /// </summary>
    public class Rent1CAccessesForConfigurationDataDto
    {
        /// <summary>
        /// Аккаунт облака
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Конфигурация ресурсов сервиса Аренда 1С
        /// </summary>
        public ResourcesConfiguration ResourcesConfiguration { get; set; }

        /// <summary>
        /// Список Id пользователей аккаунта
        /// </summary>
        public List<Guid> UsersIdList { get; set; } = [];
    }
}
