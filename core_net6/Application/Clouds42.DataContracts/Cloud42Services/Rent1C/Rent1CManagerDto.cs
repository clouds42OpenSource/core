﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Модель для управления Арендой 1С
    /// </summary>
    public class Rent1CManagerDto
    {
        /// <summary>
        /// Модели управления ресурсами Аренды 1С
        /// </summary>
        public List<Rent1CUserManagerDto> Users { get; set; } = [];

        /// <summary>
        /// Модель управления сервисом Аренда 1С
        /// </summary>
        public Rent1CServiceManagerDto Service { get; set; }
    }
}