﻿using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class DbControlOptionsDomainModel
    {
        public Guid Id { get; set; }
        public PublishState PublishState { get; set; }
        public bool Detached { get; set; }
        public bool IsEnable { get; set; }
        public string WebPublishPath { get; set; }
        public string Caption { get; set; }
        public DatabaseState State { get; set; }
        public PlatformType TemplatePlatform { get; set; }
        public PlatformType PlatformType { get; set; }
        public int DbNumber { get; set; }
        public string LockedState { get; set; }
        public bool CanWebPublish { get; set; }
        public bool IsDbOnDelimiters { get; set; }
    }
}