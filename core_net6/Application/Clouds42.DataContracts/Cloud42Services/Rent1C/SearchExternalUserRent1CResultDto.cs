namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class SearchExternalUserRent1CResultDto
    {
        public string ErrorMessage { get; set; }
        public LicenseOfRent1CDm User { get; set; }
    }
}