﻿namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Содержит информацию о видах разрешений на аренду 1С 
    /// </summary>
    public class Rent1CPermissionDto
    {
        /// <summary>
        /// Признак того что RDP разрешение включено или отключено
        /// </summary>
        public bool HasPermissionForRdp { get; set; }

        /// <summary>
        /// Признак того что Web разрешение включено или отключено
        /// </summary>
        public bool HasPermissionForWeb { get; set; }
    }
}