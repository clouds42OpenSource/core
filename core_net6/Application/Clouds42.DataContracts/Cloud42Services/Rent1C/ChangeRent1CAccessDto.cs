﻿namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Модель изменения доступа к Аренде 1С
    /// </summary>
    public class ChangeRent1CAccessDto
    {
        /// <summary>
        /// Данные для изменения доступа
        /// </summary>
        public UpdaterAccessRent1CRequestDto AccessData { get; set; }

        /// <summary>
        /// Данные биллинга аккаунта для покупки сервиса Аренда 1С
        /// </summary>
        public AccountBillingDataForBuyRent1CDto BuyRent1CData { get; set; }
    }
}
