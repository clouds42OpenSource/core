﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Модель лицензий распознования документов
    /// </summary>
    public class Recognition42LicensesModelDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { set; get; }

        /// <summary>
        /// Осталось страниц
        /// </summary>
        public int PagesLeft { get; set; }

        /// <summary>
        /// Текущая дата окончания работы сервиса
        /// </summary>
        public DateTime CurrentExpireDate { get; set; }

        /// <summary>
        /// Дата окончания работы сервиса
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Id ресурса страниц
        /// </summary>
        public Guid PagesResourceId { set; get; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        [Range(0, int.MaxValue)]
        public int PagesAmount { set; get; }

        /// <summary>
        /// Сервис Esdl заблокирован
        /// </summary>
        public bool IsFrozenEsdl { get; set; }

        /// <summary>
        /// Сервис "Распознование документов" заблокирован
        /// </summary>
        public bool IsFrozenRec42 { get; set; }

        /// <summary>
        /// Сервис в режиме демо
        /// </summary>
        public bool IsDemoPeriod { get; set; }
    }
}
