﻿using System;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class Rent1CServiceManagerDto
    {                
        /// <summary>
        /// Дата окончания работы сервиса
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Аккаунт является ВИПом
        /// </summary>
        public bool AccountIsVip { get; set; }
        
        /// <summary>
        /// Описание дополнительных ресурсов аккаунта (процессоры/ссд диски и тд)
        /// </summary>
        public string AdditionalResourceName { get; set; }

        /// <summary>
        /// Стоимость дополнительных ресурсов.
        /// </summary>
        public decimal? AdditionalResourceCost { get; set; }
        
        /// <summary>
        /// Стоимость веб лицензии для аккаунта
        /// </summary>
        public decimal? CostOfWebLicense { get; set; } 

        /// <summary>
        /// Стоимость РДП лицензии для аккаунта.
        /// </summary>
        public decimal? CostOfRdpLicense { get; set; } 

        /// <summary>
        /// Сервис в режиме демо
        /// </summary>
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Максимально возможный срок действия демо периода
        /// </summary>
        public DateTime? MaxDemoExpiredDate { get; set; }

        /// <summary>
        /// Лимит на бесплатное создание баз
        /// </summary>
        public int? LimitOnFreeCreationDb { get; set; }

        /// <summary>
        /// Стоимость создания базы, свыше бесплатного тарифа
        /// </summary>
        public decimal? CostOfCreatingDbOverFreeLimit { get; set; }

        /// <summary>
        /// Стоимость размещения серверной базы
        /// </summary>
        public decimal? ServerDatabasePlacementCost { get; set; }
    }
}
