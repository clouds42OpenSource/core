﻿namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Данные биллинга аккаунта для покупки сервиса Аренда 1С
    /// </summary>
    public class AccountBillingDataForBuyRent1CDto
    {
        /// <summary>
        /// Частичная стоимость лицензии вэб
        /// </summary>
        public decimal PartialCostOfWebLicense { get; set; }

        /// <summary>
        /// Частичная стоимость лицензии РДП
        /// </summary>
        public decimal PartialCostOfRdpLicense { get; set; }

        /// <summary>
        /// Частичная стоимость лицензии стандарт
        /// </summary>
        public decimal PartialCostOfStandartLicense { get; set; }

        /// <summary>
        /// Стоимость тарифа РДП
        /// </summary>
        public decimal RdpRateCost { get; set; }

        /// <summary>
        /// Стоимость тарифа вэб
        /// </summary>
        public decimal WebRateCost { get; set; }
    }
}
