﻿using System;
using Clouds42.DataContracts.BaseModel;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{

    public class AccessBuyingViewModelDto
    {
        public ServiceNameRepresentationDto ServiceNameRepresentation { get; set; }

        public Guid SubjectId { get; set; }

        public decimal AccountBalance { get; set; }

        public decimal PartialCost { get; set; }

        public ResourceType ResourceType { get; set; }

    }
}