﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class CloudCoreDomainModelDto
    {
        public string MainPageNotification { set; get; }
        public Domain.DataModels.CloudServicesSegment DefaultSegment { set; get; }
        public List<Domain.DataModels.CloudServicesSegment> SegmentList { set; get; }
        public int? HourseDifferenceOfUkraineAndMoscow { get; set; }

    }
}
