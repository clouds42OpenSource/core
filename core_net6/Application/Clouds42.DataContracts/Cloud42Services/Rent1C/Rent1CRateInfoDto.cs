﻿using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.DataModels.billing;


namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// Информация о тарифах аренды 1С для аккаунта
    /// </summary>
    public class Rent1CRateInfoDto
    {
        /// <summary>
        /// Услуга Аренда 1С Стандарт
        /// </summary>
        public IBillingServiceType Rent1CStandartServiceType { get; set; }

        /// <summary>
        /// Услуга Аренда 1С WEB
        /// </summary>
        public IBillingServiceType Rent1CWebServiceType { get; set; }

        /// <summary>
        /// Тариф для Аренды 1С Стандарт
        /// </summary>
        public RateInfoBaseDc Rent1CStandartRate { get; set; }

        /// <summary>
        /// Тариф для Аренды 1С WEB
        /// </summary>
        public RateInfoBaseDc Rent1CWebRate { get; set; }
    }
}
