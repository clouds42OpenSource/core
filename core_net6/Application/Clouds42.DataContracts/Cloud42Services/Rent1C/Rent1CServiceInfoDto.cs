﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.DataContracts.ResourceConfiguration;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class Rent1CServiceInfoDto
    {
        /// <summary>
        /// Дата следующего списания средств.
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Признак что аккаунт ВИП
        /// </summary>
        public bool AccountIsVip { get; set; }

        /// <summary>
        /// Данные по локали
        /// </summary>
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Ежемесячная оплата за сервис
        /// </summary>
        public decimal MonthlyCostTotal { get; set; }

        /// <summary>
        /// Ежемесячная оплата за вэб лицензии
        /// </summary>
        public decimal MonthlyCostWeb { get; set; }

        /// <summary>
        /// Ежемесячная оплата за стандартные лицензии
        /// </summary>
        public decimal MonthlyCostStandart { get; set; }

        /// <summary>
        /// Тариф за вэб лицензию.
        /// </summary>
        public decimal RateWeb { get; set; }

        /// <summary>
        /// Тариф за рдп лицензию.
        /// </summary>
        public decimal RateRdp { get; set; }

        /// <summary>
        /// Кол-во веб лицензий
        /// </summary>
        public decimal WebResourcesCount { get; set; }

        /// <summary>
        /// Кол-во стандартных лицензий
        /// </summary>
        public decimal StandartResourcesCount { get; set; }

        /// <summary>
        /// Кол-во спонсируемых мной веб лицензий
        /// </summary>
        public decimal SponsoredWebCount { get; set; }

        /// <summary>
        /// Кол-во спонсируемых мной стандартных лицензий
        /// </summary>
        public decimal SponsoredStandartCount { get; set; }

        /// <summary>
        /// Кол-во спонсрских веб лицензий
        /// </summary>
        public decimal SponsorshipWebCount { get; set; }

        /// <summary>
        /// Кол-во спонсрских стандартных лицензий
        /// </summary>
        public decimal SponsorshipStandartCount { get; set; }

        /// <summary>
        /// Свободные веб лицензии
        /// </summary>
        public List<Guid> FreeWebResources { get; set; } = [];

        /// <summary>
        /// Свободные рдп лицензии
        /// </summary>
        public List<Guid> FreeRdpResources { get; set; } = [];

        /// <summary>
        /// Список пользователей аккаунта
        /// </summary>
        public List<LicenseOfRent1CDm> UserList { get; set; } = [];

        /// <summary>
        /// Информация заблокирован сервис или нет и причина блокировки
        /// </summary>
        public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// Краткие данные по сервису "Мои инф. базы"
        /// </summary>
        public MyDatabasesServiceShortDataDto MyDatabasesServiceData { get; set; }

        /// <summary>
        /// Дополнительные ресурсы аккаунта
        /// </summary>
        public AdditionalAccountResourcesModelDto AdditionalAccountResources { get; set; }
    }

    public class LicenseOfRent1CDm
    {
        public Guid Id { get; set; }

        public Guid AccountId { get; set; }

        public string Login { get; set; }

        public string FullUserName { get; set; }

        public bool IsActive { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public Guid? WebResourceId { get; set; }

        public Guid? RdpResourceId { get; set; }

        /// <summary>
        /// Имя спонсируемого мной аккаунта. 
        /// </summary>
        public string SponsoredLicenseAccountName { get; set; }

        /// <summary>
        /// Имя аккаунта проспонмировавшего мою лицензию
        /// </summary>
        public string SponsorshipLicenAccountName { get; set; }

        /// <summary>
        /// Стоимость стандартной лицензии до конца расчетного месяца
        /// </summary>
        public decimal PartialUserCostWeb { get; set; }

        /// <summary>
        /// Стоимость рдп лицензии до конца расчетного месяца
        /// </summary>
        public decimal PartialUserCostRdp { get; set; }

        /// <summary>
        /// Стоимость вэб лицензии до конца расчетного месяца
        /// </summary>
        public decimal PartialUserCostStandart { get; set; }

        /// <summary>
        ///     Названия зависимых активных услуг
        /// </summary>
        public List<KeyValuePair<ResourceType, List<string>>> NamesOfDependentActiveServices { get; set; } = [];
    }
}