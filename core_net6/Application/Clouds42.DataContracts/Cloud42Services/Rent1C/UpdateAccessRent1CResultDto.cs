﻿namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    public class UpdateAccessRent1CResultDto
    {
        public bool Complete { get; set; }
        public decimal EnoughMoney { get; set; }
        public string ErrorMessage { get; set; }
        public bool CanGetPromisePayment { get; set; }
        public decimal CostForPay { get; set; }
    }
}
