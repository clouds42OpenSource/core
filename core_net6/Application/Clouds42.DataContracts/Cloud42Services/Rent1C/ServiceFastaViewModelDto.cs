﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.DataContracts.Cloud42Services.Rent1C
{
    /// <summary>
    /// "Fasta" service info model dto
    /// </summary>
    public class ServiceFastaViewModelDto
    {
        /// <summary>
        /// Service name representation model
        /// </summary>
        public ServiceNameRepresentationDto ServiceNameRepresentation { get; set; }

        /// <summary>
        /// Remaining pages
        /// </summary>
        public int PagesRemain { get; set; }

        /// <summary>
        /// License period
        /// </summary>
        public int LicensePeriod { get; set; } = 1;

        /// <summary>
        /// Default pages tariff (pages count)
        /// </summary>
        public int DefaultPagesTariff { get; set; }

        /// <summary>
        /// Service expire date
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Is service blocked
        /// </summary>
        public bool IsFrozen { get; set; }

        /// <summary>
        /// List of pages tarrif
        /// </summary>
        public List<int> PagesTariffList { get; set; } = [0];

        /// <summary>
        /// Current page tariff
        /// </summary>
        public int CurrentPagesTariff { get; set; }

        /// <summary>
        /// Is allowed to increase the pages of loading documents for 1CRent (Standard)
        /// </summary>
        public bool AllowIncreaseDocLoaderByStandardRent1C { get; set; }

        /// <summary>
        /// Expire date of the Rent 1C service
        /// </summary>
        public DateTime? Rent1CExpiredDate { get; set; }

        /// <summary>
        /// Account plan
        /// </summary>
        public AccountLevel AccountLevel { get; set; }

        /// <summary>
        /// Account locale
        /// </summary>
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Model for service blocking information
        /// </summary>
        public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// No more pages left
        /// </summary>
        private bool NoMorePagesLeft => PagesRemain < 1;

        /// <summary>
        /// Is ESDL service blocked
        /// </summary>
        public bool IsEsdlLocked => NoMorePagesLeft || IsFrozen;
    }
}
