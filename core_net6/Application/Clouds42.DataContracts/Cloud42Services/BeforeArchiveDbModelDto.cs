﻿using System;

namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель данных перед архивацией неактивного аккаунта
    /// </summary>
    public class BeforeArchiveDbModelDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { set; get; }

        /// <summary>
        /// Количество дней до архивации
        /// </summary>
        public int DaysToArchive { get; set; }        

        /// <summary>
        /// Дата окончания Аренды 1С
        /// </summary>
        public DateTime Rent1CExpireDate { get; set; }
    }
}
