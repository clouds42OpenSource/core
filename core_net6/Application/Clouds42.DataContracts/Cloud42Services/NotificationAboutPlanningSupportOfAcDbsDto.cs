﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель уведомления аккаунта о запланированном проведении поддержки инф. баз
    /// </summary>
    public class NotificationAboutPlanningSupportOfAcDbsDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Список инф. баз для выполнения поддержки
        /// </summary>
        public List<BeforeSupportAccountDatabaseModelDto.DbItem> AccountDatabasesForPerformingSupport { get; set; } =
            [];
    }
}
