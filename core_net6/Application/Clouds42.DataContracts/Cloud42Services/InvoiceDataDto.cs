﻿namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель счета
    /// </summary>
    public class InvoiceDataDto
    {
        /// <summary>
        /// Номер счета
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Массив байтов
        /// </summary>
        public byte[] InvoiceDocBytes { get; set; }

        /// <summary>
        /// Название счета
        /// </summary>
        public string InvoiceDocFileName { get; set; }
    }
}
