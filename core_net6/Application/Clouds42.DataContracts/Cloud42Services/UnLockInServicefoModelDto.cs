using System;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class UnLockInServicefoModelDto
    {
        /// <summary>
        /// Срок действия сервиса.
        /// </summary>
        public DateTime ExpireDate { set; get; }
        /// <summary>
        /// Нужно ли отправлять письмо с оповещением
        /// </summary>
        public bool NeedSendNotification { get; set; }
    }
}