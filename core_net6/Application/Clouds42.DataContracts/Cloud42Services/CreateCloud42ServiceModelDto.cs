﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class CreateCloud42ServiceModelDto
    {
	    public static CreateCloud42ServiceModelDto operator +(CreateCloud42ServiceModelDto total, CreateCloud42ServiceModelDto part)
	    {
		    return new CreateCloud42ServiceModelDto
		    {
                Complete = part.Complete,
			    Comment = total.Comment + part.Comment,
				Ids = new List<Guid>(total.Ids).Concat(part.Ids).ToList(),
                CreatedDatabases = total.CreatedDatabases.Concat(part.CreatedDatabases)
                    .GroupBy(x => x.Key)
                    .ToDictionary(g => g.Key, g => g.First().Value)
			};
	    }

	    /// <summary>
	    /// Подключение успешно.
	    /// </summary>          
	    public bool Complete { get; set; } 

        /// <summary>
        /// Нужно денег
        /// </summary>
        public decimal? NeedMoney { get; set; }

        public string Comment { get; set; } = string.Empty;

        public Guid? SubjectId { set; get; }

        /// <summary>
        /// Требуемый дисковый объем
        /// </summary>
        public long? RequiredDiskSpaceMb { get; set; } 

        public List<Guid> Ids { get; set; } = [];

        /// <summary>
        /// Ресурс для редиректа
        /// </summary>
        public ResourceType? ResourceForRedirect { get; set; }
        
        /// <summary>
        /// Словарь созданных баз данных: 
        /// ключ - идентификатор базы, 
        /// значение - признак демо-базы
        /// </summary>
        public Dictionary<Guid, bool> CreatedDatabases { get; set; }

        public CreateCloud42ServiceModelDto()
        {
            Ids = new List<Guid>();
            CreatedDatabases = new Dictionary<Guid, bool>();

        }
	}
}
