﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Результат планирования поддержки инф. баз
    /// </summary>
    public class PlanSupportAccountDatabasesResultDto
    {
        /// <summary>
        /// Список инф. баз с ошибкой планирования
        /// </summary>
        public List<KeyValuePair<Guid, string>> AccountDatabasesWithErrors { get; set; } = [];

        /// <summary>
        /// Список инф. баз для выполнения поддержки
        /// </summary>
        public List<BeforeSupportAccountDatabaseModelDto.DbItem> AccountDatabasesForPerformingSupport { get; set; } =
            [];
    }
}
