﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class CloudServicePaymentDto
    {

        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Идентификатор сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Использовать обещанный платеж или нет
        /// </summary>
        public bool IsPromisePayment { get; set; }

        /// <summary>
        /// Стоимость которую нужно доплатить для активации тарифа.
        /// </summary>
        public decimal CostOfTariff { get; set; }

        /// <summary>
        /// количество купленных сеансов на аккаунт
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// количество сеансов по пользователям
        /// </summary>
        public List<AccountUserSession> AccountUserSessions { get; set; }
    }

    public class AccountUserSession
    {
        public string Login { get; set; }
        public string UserId { get; set; }
        public int Sessions { get; set; }
    }
}
