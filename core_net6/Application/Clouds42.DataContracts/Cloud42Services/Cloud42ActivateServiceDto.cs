﻿using System;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class Cloud42ActivateServiceDto
    {
        public Guid ServiceId { get; set; }
        public string ServiceName { get; set; }

    }
}
