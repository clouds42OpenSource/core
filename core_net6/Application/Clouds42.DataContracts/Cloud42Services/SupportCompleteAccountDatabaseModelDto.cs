﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель данных результат проведения ТиИ/АО информационных баз.
    /// </summary>
    public class SupportCompleteAccountDatabaseModelDto
    {

        public enum SupportStatus
        {
            Success,
            Error
        }

        public class SupportAccountDatabaseResultModel(
            string caption,
            SupportStatus supportStatus,
            string message,
            string description)
        {
            /// <summary>
            /// Заголовок информационной базы.
            /// </summary>
            public string Caption { get; set; } = caption;

            /// <summary>
            /// Статус операции ТиИ.
            /// </summary>
            public SupportStatus SupportStatus { get; set; } = supportStatus;

            /// <summary>
            /// Сообщение
            /// </summary>
            public string Message { get; set; } = message;

            /// <summary>
            /// Детальная информация.
            /// </summary>
            public string Description { get; set; } = description;
        }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Информационные базы аккаунта со статусом результата выполнения ТиИ.
        /// </summary>
        public List<SupportAccountDatabaseResultModel> AccountDatabases { get; set; }
    }
}