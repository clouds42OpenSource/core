﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class CloudServiceDto
    {

        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Идентификатор сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// количество купленных сеансов
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// количество сеансов по пользователям
        /// </summary>
        public List<AccountUserSession> AccountUserSessions { get; set; }

    }
}
