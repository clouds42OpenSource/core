﻿using System;

namespace Clouds42.DataContracts.Cloud42Services
{

    /// <summary>
    /// Модель уведомления о скорой блокировки сервиса
    /// </summary>
    public class BeforeLockInServicefoModelDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string BillingServiceName { get; set; }

        /// <summary>
        /// Стоимсоть сервиса
        /// </summary>
        public decimal Cost { set; get; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public decimal AccountBalance { set; get; }

        /// <summary>
        /// Баланс бонусов аккаунта
        /// </summary>
        public decimal AccountBonusBalance { get; set; }

        /// <summary>
        /// Текст причины
        /// </summary>
        public string ReasonText { set; get; }

        /// <summary>
        /// Период счета
        /// </summary>
        public int? InvoicePeriod { get; set; }

        /// <summary>
        /// Дата блокировки
        /// </summary>
        public DateTime LockDate { set; get; }        

        /// <summary>
        /// Счет
        /// </summary>
        public InvoiceDataDto Invoice { get; set; }
    }
}