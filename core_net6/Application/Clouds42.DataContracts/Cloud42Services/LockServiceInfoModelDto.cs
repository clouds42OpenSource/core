﻿
namespace Clouds42.DataContracts.Cloud42Services
{
    /// <summary>
    /// Модель уведомления блокировки сервиса.
    /// </summary>
    public class LockServiceInfoModelDto
    {
        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Стоимсоть
        /// </summary>
        public decimal NeedCost { set; get; }

        /// <summary>
        /// Баланс
        /// </summary>
        public decimal AccountBalance { set; get; }

        /// <summary>
        /// Текст блокировки
        /// </summary>
        public string ReasonText { get; set; }

        /// <summary>
        /// Нужно ли отправлять письмо с оповещением
        /// </summary>
        public bool NeedSendNotification { get; set;}

        /// <summary>
        /// Счет
        /// </summary>
        public InvoiceDataDto Invoice { get; set; }
    }

}
