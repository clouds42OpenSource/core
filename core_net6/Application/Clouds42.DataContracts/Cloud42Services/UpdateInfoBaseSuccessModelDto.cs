﻿using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class UpdateInfoBaseSuccessModelDto
    {
        public IAccount AccountInfo { get; set; }

        public List<AcDbSupportHistory> ListHistory { get; set;}

    }
}