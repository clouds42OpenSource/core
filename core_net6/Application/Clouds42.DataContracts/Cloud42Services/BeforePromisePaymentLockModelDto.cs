﻿using System;

namespace Clouds42.DataContracts.Cloud42Services
{
    public class BeforePromisePaymentLockModelDto
    {
        public Guid AccountId { get; set; }
    }
}
