﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Результат выполнения команды RAS
    /// </summary>
    public class RasExecutionResultDto
    {
        /// <summary>
        /// Результат
        /// </summary>
        public RasExecutionResultState State { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
