﻿namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Параметры команды для управления сервером 1С Предприятия
    /// </summary>
    public class ManageRasCommandParamsDto : RunRasApplicationCommandParamsDto
    {
        /// <summary>
        /// ID кластера
        /// </summary>
        public string ClusterId { get; set; }

        /// <summary>
        /// ID сессии
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// ID инф. базы на кластере
        ///     <para>
        ///         ID базы на кластере не совпадает с ID в БД
        ///     </para>
        /// </summary>
        public string ClusterAccountDatabaseId { get; set; }

        /// <summary>
        /// Логин администратора сервера 1С
        ///     <para>
        ///         Можно достать из сегментов в ЛК
        ///     </para>
        /// </summary>
        public string Server1CAdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора сервера 1С
        ///     <para>
        ///         Можно достать из сегментов в ЛК
        ///     </para>
        /// </summary>
        public string Server1CAdminPassword { get; set; }

        /// <summary>
        /// Имя базы
        /// </summary>
        public string DatabaseName { get; set; }


        /// <summary>
        /// Имя сервера баз данных
        /// </summary>
        public string DBServerName { get; set; }


        /// <summary>
        /// Логин администратора кластера
        /// </summary>
        public string ClusterAdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора кластера
        /// </summary>
        public string ClusterAdminPassword { get; set; }

        
        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string UseTemplatePath { get; set; }
    }
}
