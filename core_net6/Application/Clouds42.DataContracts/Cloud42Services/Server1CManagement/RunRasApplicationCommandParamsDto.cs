﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Параметры команды для запуска ras.exe (оснастки управления сервером 1С Предприятие)
    /// </summary>
    public class RunRasApplicationCommandParamsDto : PowerShellExecuteCommandParamsDto
    {
        /// <summary>
        /// Порт для работы RAS.exe
        ///     <para>
        ///         Стандартный порт 1545
        ///     </para>
        /// </summary>
        public string RasWorkingPort { get; set; }

        /// <summary>
        /// Название сервера 1С
        /// </summary>
        public string Server1CName { get; set; }

        /// <summary>
        /// Список названий серверов 1С
        /// </summary>
        public List<string> Server1CNamesList { get; set; }

        /// <summary>
        /// Порт на котором работает сервер 1С
        ///     <para>
        ///         Стандартный порт 1540
        ///     </para>
        /// </summary>
        public string Server1CPort { get; set; }
    }
}
