﻿using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Информация об инф. базе на кластере
    /// </summary>
    public class ClusterAccountDatabaseInfoDto
    {
        /// <summary>
        /// ID кластера
        /// </summary>
        public string ClusterId { get; set; }

        /// <summary>
        /// Номер инф. базы
        /// </summary>
        [DataMember(Name = "name")]
        public string AccountDatabaseV82Name { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        [DataMember(Name = "infobase")]
        public string AccountDatabaseId { get; set; }
    }
}
