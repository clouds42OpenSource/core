﻿namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Расширения для параметров команды RAS
    /// </summary>
    public static class RasCommandParamsExtensionsDto
    {
        /// <summary>
        /// Привести параметры команды Ras к требуемому типу параметров команды
        /// </summary>
        /// <typeparam name="T">Требуемый тип параметров команды</typeparam>
        /// <param name="commandParams">Параметры команды Ras</param>
        /// <returns>Требуемые параметры команды</returns>
        public static T CastToSpecificCommandParams<T>(this IPowerShellExecuteCommandParamsDto commandParams)
            where T : IPowerShellExecuteCommandParamsDto
            => (T)commandParams;
    }
}