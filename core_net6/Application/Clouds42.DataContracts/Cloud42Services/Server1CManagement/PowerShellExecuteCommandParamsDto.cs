﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Параметры для выполнения PowerShell команды 
    /// </summary>
    public abstract class PowerShellExecuteCommandParamsDto : IPowerShellExecuteCommandParamsDto
    {
        /// <summary>
        /// Директория для выполнения команды
        /// </summary>
        public string RunSpance { get; set; }

        /// <summary>
        /// Путь к запускаемому приложению
        ///     <para>
        ///         Пример: .\notepad.exe
        ///     </para>
        /// </summary>
        public string ApplicationPath { get; set; }

        /// <summary>
        /// Тип команды
        /// </summary>
        public PowerShellCommandType CommandType { get; set; }
    }
}
