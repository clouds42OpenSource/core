﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Сессия инф. базы
    /// </summary>
    public class ClusterAccountDatabaseSessionDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        [DataMember(Name = "infobase")]
        public string AccountDatabaseId { get; set; }

        /// <summary>
        /// ID сессии
        /// </summary>
        [DataMember(Name = "session")]
        public string SessionId { get; set; }

        /// <summary>
        /// Дата начала сессии
        /// </summary>
        [DataMember(Name = "started-at")]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
        [DataMember(Name = "last-active-at")]
        public DateTime LastActivity { get; set; }

        /// <summary>
        /// ID кластера
        /// </summary>
        public string ClusterId { get; set; }
    }
}
