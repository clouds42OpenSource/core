﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Cloud42Services.Server1CManagement
{
    /// <summary>
    /// Параметры для выполнения PowerShell команды 
    /// </summary>
    public interface IPowerShellExecuteCommandParamsDto
    {
        /// <summary>
        /// Директория для выполнения команды
        /// </summary>
        string RunSpance { get; set; }

        /// <summary>
        /// Путь к запускаемому приложению
        /// </summary>
        string ApplicationPath { get; set; }

        /// <summary>
        /// Тип команды
        /// </summary>
        PowerShellCommandType CommandType { get; set; }
    }
}
