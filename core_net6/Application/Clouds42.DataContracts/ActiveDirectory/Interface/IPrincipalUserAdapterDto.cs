﻿using System;

namespace Clouds42.DataContracts.ActiveDirectory.Interface
{
    public interface IPrincipalUserAdapterDto
    {
        /// <summary>
        /// Получить имя пользователя.
        /// </summary>
        string GetUserName();

        /// <summary>
        /// Пароль
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Имя
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Фамилия
        /// </summary>
        string LastName { get; }

        /// <summary>
        /// Отчество
        /// </summary>
        string MiddleName { get; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        string PhoneNumber { get; }

        string Email { get; }
    }

    public interface ICreateNewUserModel : IPrincipalUserAdapterDto
    {
        /// <summary>
        /// Идентификатор аккаунта.
        /// </summary>
        Guid AccountId { get; }

        Guid GetAccountUserId();

        public string ClientFilesPath { get; set; }
    }

    public interface ICreateNewAccountModel : ICreateNewUserModel
    {
    }

}
