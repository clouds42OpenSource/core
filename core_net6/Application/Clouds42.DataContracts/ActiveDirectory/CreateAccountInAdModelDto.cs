﻿using System;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.DataContracts.ActiveDirectory
{
    public class CreateAccountInAdModelDto : ICreateNewAccountModel
    {

        /// <summary>
        /// Текущий логин
        /// </summary>
        public string OldLogin { get; set; }

        /// <summary>
        /// Новый логин
        /// </summary>
        public string NewLogin { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        /// <summary>
        /// Идентификатор аккаунта.
        /// </summary>
        public Guid AccountId { get; set; }

        public Guid AccountUserId { get; set; }

        public string GetUserName()
            => NewLogin;

        public Guid GetAccountUserId()
            => AccountUserId;

        public string ClientFilesPath{ get; set; }
    }
}
