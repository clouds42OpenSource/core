﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.ActiveDirectory
{

    public class EditUserGroupsAdListModel
    {
        public Guid AccountId { get; set; }
        public List<EditUserGroupsAdModelDto> EditUserGroupsModel { get; set; } = [];
    }

    public class EditUserGroupsAdModelDto
    {
        public List<Item> Items { get; set; } = [];

        public EditUserGroupsAdModelDto Append(string userName, string groupName, OperationEnum operation)
        {
            Items.Add(new Item
            {
                UserName = userName,
                GroupName = groupName,
                Operation = operation
            });
            return this;
        }

        public EditUserGroupsAdModelDto AppendRange(List<Item> items)
        {
            Items.AddRange(items);

            return this;
        }

        public class Item
        {
            public string UserName { get; set; }
            public string GroupName { get; set; }

            public OperationEnum Operation { get; set; }
        }

        public enum OperationEnum
        {
            Exclude,
            Include
        }
    }
}
