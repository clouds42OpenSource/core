﻿
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.DataContracts.ActiveDirectory
{
    public class CreateDomainUserModelDto: IPrincipalUserAdapterDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string GetUserName() => UserName;

    }
}
