﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Fasta
{
    /// <summary>
    /// "Fasta" service resources dto
    /// </summary>
    public class FastaResourcesDto
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Pages resource identifier
        /// </summary>
        [Required]
        public Guid PagesResourceId { get; set; }

        /// <summary>
        /// New service expire date
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// New total pages amount
        /// </summary>
        public int? PagesAmount { get; set; }

        /// <summary>
        /// Comment for changing resources
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Comment { get; set; }
    }
}
