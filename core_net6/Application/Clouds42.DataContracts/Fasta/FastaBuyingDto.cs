﻿namespace Clouds42.DataContracts.Fasta
{
    /// <summary>
    /// DTO for buying "Fasta" service licenses
    /// </summary>
    public class FastaBuyingDto
    {
        /// <summary>
        /// Total pages amount
        /// </summary>
        public int PagesAmount { get; set; }

        /// <summary>
        /// Total years amount
        /// </summary>
        public int YearsAmount { get; set; }
    }
}
