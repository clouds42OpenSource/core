﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных терминальных серверов для сегмента
    /// </summary>
    public class SegmentTerminalServersDataDto
    {
        /// <summary>
        /// Терминальные сервера сегмента
        /// </summary>
        public List<SegmentTerminalServerDto> SegmentTerminalServers { get; set; } = [];

        /// <summary>
        /// Доступные терминальные сервера для сегмента
        /// </summary>
        public List<SegmentTerminalServerDto> AvailableSegmentTerminalServers { get; set; } = [];
    }
}
