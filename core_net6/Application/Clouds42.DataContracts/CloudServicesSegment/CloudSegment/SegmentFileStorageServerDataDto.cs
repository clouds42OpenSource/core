﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных файлового хранилища для сегмента
    /// </summary>
    public class SegmentFileStorageServerDataDto : ISegmentElementDto
    {
        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название файлового хранилища
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Флаг показывающий доступно ли хранилище
        /// для сегмента(Доступно для добавления)
        /// </summary>
        public bool AvailableFileStorage { get; set; }
    }
}
