﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель сегмента
    /// </summary>
    public class CloudServicesSegmentDto
    {
        /// <summary>
        /// ID элемента сегмента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Базовые данные сегмента
        /// </summary>
        public CloudServicesSegmentBaseDataDto SegmentBaseData { get; set; }

        /// <summary>
        /// Данные сегментов для миграции сегмента
        /// </summary>
        public MigrationSegmentsDataDto MigrationSegmentsData { get; set; }

        /// <summary>
        /// Данные файловых хранилищ для сегмента
        /// </summary>
        public SegmentFileStorageServersDataDto SegmentFileStorageServersData { get; set; }

        /// <summary>
        /// Данные по терминальным серверам для сегмента
        /// </summary>
        public SegmentTerminalServersDataDto SegmentTerminalServersData { get; set; }

        /// <summary>
        /// Элементы сегмента
        /// </summary>
        public SegmentElementsDto SegmentElements { get; set; }
    }
}
