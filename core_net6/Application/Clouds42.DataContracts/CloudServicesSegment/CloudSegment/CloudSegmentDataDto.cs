﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных сегмента
    /// </summary>
    public class CloudSegmentDataDto
    {
        /// <summary>
        /// ID сегмента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя сегмента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание сегмента
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Имя шлюза
        /// </summary>
        public string GatewayTerminalName { get; set; }

        /// <summary>
        /// Имя фермы ТС
        /// </summary>
        public string TerminalFarmName { get; set; }

        /// <summary>
        /// Имя сервера Предприятия 8.2
        /// </summary>
        public string EnterpriseServer82Name { get; set; }

        /// <summary>
        /// Имя сервера Предприятия 8.3
        /// </summary>
        public string EnterpriseServer83Name { get; set; }

        /// <summary>
        /// Имя сервера публикаций
        /// </summary>
        public string ContentServerName { get; set; }

        /// <summary>
        /// Имя файлового хранилища
        /// </summary>
        public string FileStorageServerName { get; set; }

        /// <summary>
        /// Имя SQL сервера
        /// </summary>
        public string SqlServerName { get; set; }

        /// <summary>
        /// Имя сервера бэкапов
        /// </summary>
        public string BackupStorageName { get; set; }

        /// <summary>
        /// Путь к файлам пользователей
        /// </summary>
        public string CustomFileStoragePath { get; set; }

        /// <summary>
        /// Имя файлового хранилища по умолчанию
        /// </summary>
        public string DefaultFileStorageName { get; set; }

        /// <summary>
        /// Стабильная версия 8.3
        /// </summary>
        public string Stable83Version { get; set; }
    }
}
