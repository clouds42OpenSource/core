﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель создания сегмента
    /// </summary>
    public class CreateCloudServicesSegmentDto : CloudServicesSegmentBaseDataDto
    {
        /// <summary>
        /// ID хостинга
        /// </summary>
        public Guid CoreHostingId { get; set; }

        /// <summary>
        /// Сегмент по умолчанию
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
