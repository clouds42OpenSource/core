﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных файловых хранилищ для сегмента
    /// </summary>
    public class SegmentFileStorageServersDataDto
    {
        /// <summary>
        /// Файловые хранилища сегмента
        /// </summary>
        public List<SegmentElementDto> SegmentFileStorageServers { get; set; } = [];

        /// <summary>
        /// Доступные файловые хранилища для сегмента
        /// </summary>
        public List<SegmentElementDto> AvailableSegmentFileStorageServers { get; set; } = [];
    }
}
