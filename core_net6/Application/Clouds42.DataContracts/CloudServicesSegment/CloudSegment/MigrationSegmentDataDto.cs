﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных сегмента для миграции
    /// </summary>
    public class MigrationSegmentDataDto : ISegmentElementDto
    {
        /// <summary>
        /// Id сегмента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сегмента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Признак что сегмент доступен
        /// </summary>
        public bool IsAvailable { get; set; }

        /// <summary>
        /// Признак что сегмент по параметрам похож на текущий
        /// </summary>
        public bool IsSimilar { get; set; }
    }
}
