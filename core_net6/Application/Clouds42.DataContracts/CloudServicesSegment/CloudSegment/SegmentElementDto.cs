﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель элемента сегмента
    /// </summary>
    public class SegmentElementDto : ISegmentElementDto
    {
        /// <summary>
        /// Id элемента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название элемента
        /// </summary>
        public string Name { get; set; }
    }
}
