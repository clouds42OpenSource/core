﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель терминального шлюза
    /// </summary>
    public class CloudGatewayTerminalDto : CreateCloudGatewayTerminalDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public Guid Id { get; set; }
    }
}
