﻿using System;
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Терминальный сервер сегмента
    /// </summary>
    public class SegmentTerminalServerDto : ISegmentElementDto
    {
        /// <summary>
        /// ID элемента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название элемента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Флаг показывающий доступен ли терминальный сервер
        /// для сегмента(Доступен для добавления)
        /// </summary>
        public bool Available { get; set; }
    }
}
