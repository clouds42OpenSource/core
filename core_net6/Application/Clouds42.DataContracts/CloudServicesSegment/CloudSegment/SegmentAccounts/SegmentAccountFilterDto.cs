﻿using System;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment.SegmentAccounts
{
    /// <summary>
    /// Фильтр поиска аккаунтов сегмента
    /// </summary>
    public class SegmentAccountFilterDto : ISortingDto
    {
        /// <summary>
        /// ID сегмента
        /// </summary>
        public Guid SegmentId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int? AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
