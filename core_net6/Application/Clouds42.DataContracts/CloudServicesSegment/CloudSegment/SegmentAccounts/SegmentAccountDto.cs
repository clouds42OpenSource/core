﻿namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment.SegmentAccounts
{
    /// <summary>
    /// Модель аккаунта сегмента
    /// </summary>
    public class SegmentAccountDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }
        
        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }
        
        /// <summary>
        /// Размер файловых баз в мб
        /// </summary>
        public int SizeOfFileAccountDatabases { get; set; }

        /// <summary>
        /// Размер серверных баз в мб
        /// </summary>
        public int SizeOfServerAccountDatabases { get; set; }

        /// <summary>
        /// Размер клиентских файлов в мб
        /// </summary>
        public long SizeOfClientFiles { get; set; }
    }
}
