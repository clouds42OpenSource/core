﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных сегментов для миграции
    /// </summary>
    public class MigrationSegmentsDataDto
    {
        /// <summary>
        /// Доступные сегменты для миграции
        /// </summary>
        public List<MigrationSegmentDataDto> AvailableMigrationSegment { get; set; } = [];

        /// <summary>
        /// Сегменты для миграции
        /// </summary>
        public List<MigrationSegmentDataDto> SegmentsForMigration { get; set; } = [];
    }
}
