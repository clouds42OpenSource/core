﻿using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель данных элемента сегмента
    /// </summary>
    public class SegmentElementDataDto : SegmentElementDto
    {
        /// <summary>
        /// Тип элемента сегмента
        /// </summary>
        public CloudSegmentElementType CloudSegmentElementType { get; set; }
    }
}
