﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель редактирования сегмента
    /// </summary>
    public class EditCloudServicesSegmentDto : CloudServicesSegmentBaseDataDto
    {
        /// <summary>
        /// ID элемента сегмента
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Доступные сегменты для миграции
        /// </summary>
        public List<SegmentElementDto> AvailableMigrationSegment { get; set; } = [];

        /// <summary>
        /// Файловые хранилища сегмента
        /// </summary>
        public List<SegmentElementDto> SegmentFileStorageServers { get; set; } = [];

        /// <summary>
        /// Терминальные сервера сегмента
        /// </summary>
        public List<SegmentTerminalServerDto> SegmentTerminalServers { get; set; } = [];
    }
}
