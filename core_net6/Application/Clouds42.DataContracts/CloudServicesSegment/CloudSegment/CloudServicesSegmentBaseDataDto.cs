﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Базовые данные сегмента
    /// </summary>
    public class CloudServicesSegmentBaseDataDto
    {
        /// <summary>
        /// Имя элемента сегмента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID сервера бэкапов
        /// </summary>
        public Guid BackupStorageID { get; set; }

        /// <summary>
        /// ID файлового хранилища
        /// </summary>
        public Guid FileStorageServersID { get; set; }

        /// <summary>
        /// ID сервера Предприятие 8.2
        /// </summary>
        public Guid EnterpriseServer82ID { get; set; }
        
        /// <summary>
        /// ID сервера Предприятие 8.3
        /// </summary>
        public Guid EnterpriseServer83ID { get; set; }
        
        /// <summary>
        /// ID сервера публикаций
        /// </summary>
        public Guid ContentServerID { get; set; }
        
        /// <summary>
        /// ID SQL сервера
        /// </summary>
        public Guid SQLServerID { get; set; }
        
        /// <summary>
        /// ID фермы ТС
        /// </summary>
        public Guid ServicesTerminalFarmID { get; set; }
        
        /// <summary>
        /// ID шлюза
        /// </summary>
        public Guid? GatewayTerminalsID { get; set; }
        
        /// <summary>
        /// ID стабильной версии 8.2
        /// </summary>
        public string Stable82VersionId { get; set; }
        
        /// <summary>
        /// ID стабильной версии 8.3
        /// </summary>
        public string Stable83VersionId { get; set; }
        
        /// <summary>
        /// ID альфа версии 8.3
        /// </summary>
        public string Alpha83VersionId { get; set; }

        /// <summary>
        /// Путь к файлам пользователей
        /// </summary>
        public string CustomFileStoragePath { get; set; }

        /// <summary>
        /// Название хостинга
        /// </summary>
        public string HostingName { get; set; }

        /// <summary>
        /// Описание элемента сегмента
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Открывать базы в тонком клиенте по http ссылке
        /// </summary>
        public bool DelimiterDatabaseMustUseWebService { get; set; }

        /// <summary>
        /// Id дефолтного файлового хранилища
        /// </summary>
        public Guid DefaultSegmentStorageId { get; set; }

        /// <summary>
        /// Нода автообновления
        /// </summary>
        public Guid? AutoUpdateNodeId { get; set; }

        public bool NotMountDiskR { get; set; }
    }
}
