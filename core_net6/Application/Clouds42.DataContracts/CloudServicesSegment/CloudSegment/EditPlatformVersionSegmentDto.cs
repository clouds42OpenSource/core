﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель редактирования версии платформы для списка сегментов
    /// </summary>
    public class EditPlatformVersionSegmentDto 
    {
        /// <summary>
        /// список ID сегментов
        /// </summary>
        public List<Guid> SegmentIds { get; set; } = [];

        /// <summary>
        /// ID сервера Предприятие 8.3
        /// </summary>
        public string Stable83VersionId { get; set; }

    }
}
