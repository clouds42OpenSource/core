﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Элементы сегмента
    /// </summary>
    public class SegmentElementsDto
    {
        /// <summary>
        /// Список бэкап серверов
        /// </summary>
        public List<SegmentElementDto> BackupStorages { get; set; } = [];

        /// <summary>
        /// Список файловых хранилищ
        /// </summary>
        public List<SegmentElementDto> FileStorageServers { get; set; } = [];

        /// <summary>
        /// Список серверов Предприятия 8.3
        /// </summary>
        public List<SegmentElementDto> Enterprise82Servers { get; set; } = [];

        /// <summary>
        /// Список серверов Предприятия 8.3
        /// </summary>
        public List<SegmentElementDto> Enterprise83Servers { get; set; } = [];

        /// <summary>
        /// Список серверов публикаций
        /// </summary>
        public List<SegmentElementDto> ContentServers { get; set; } = [];

        /// <summary>
        /// Список SQL серверов
        /// </summary>
        public List<SegmentElementDto> SqlServers { get; set; } = [];

        /// <summary>
        /// Список ферм ТС
        /// </summary>
        public List<SegmentElementDto> TerminalFarms { get; set; } = [];

        /// <summary>
        /// Список шлюзов
        /// </summary>
        public List<SegmentElementDto> GatewayTerminals { get; set; } = [];

        /// <summary>
        /// Список стабильных версий 8.2
        /// </summary>
        public List<string> Stable82Versions { get; set; } = [];

        /// <summary>
        /// Список стабильных версий 8.3
        /// </summary>
        public List<string> Stable83Versions { get; set; } = [];

        /// <summary>
        /// Список альфа версий 8.3
        /// </summary>
        public List<string> Alpha83Versions { get; set; } = [];

        /// <summary>
        /// Список хостингов облака
        /// </summary>
        public List<SegmentElementDto> CoreHostingList { get; set; } = [];
    }
}
