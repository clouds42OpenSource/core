﻿using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudSegment
{
    /// <summary>
    /// Модель создания терминального шлюза
    /// </summary>
    public class CreateCloudGatewayTerminalDto : ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }
    }
}
