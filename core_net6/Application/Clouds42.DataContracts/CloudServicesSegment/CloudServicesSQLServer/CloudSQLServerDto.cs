﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer
{
    /// <summary>
    /// Модель Sql сервера
    /// </summary>
    public class CloudSqlServerDto : CreateCloudSqlServerDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public Guid Id { get; set; }
    }
}
