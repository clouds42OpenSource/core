﻿using Clouds42.DataContracts.CloudServicesSegment.Interface;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesSQLServer
{
    /// <summary>
    /// Модель создания Sql сервера
    /// </summary>
    public class CreateCloudSqlServerDto : ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum RestoreModelType { get; set; }
    }
}
