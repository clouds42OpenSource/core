﻿using System;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer
{
    /// <summary>
    /// Сервер 1С:Предприятие
    /// </summary>
    public class EnterpriseServerDto
    {
        /// <summary>
        /// Идентификатор сервера
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Путь к настройкам кластера
        /// </summary>
        public string ClusterSettingsPath { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public PlatformType VersionEnum => (PlatformType)Enum.Parse(typeof(PlatformType), Version);

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Логин администратора
        /// </summary>
        public string AdminName { get; set; }

        /// <summary>
        /// Пароль администратора
        /// </summary>
        public string AdminPassword { get; set; }
    }
}
