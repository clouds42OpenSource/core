﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesEnterpriseServer
{
    /// <summary>
    /// Фильтр поиска серверов 1С:Предприятие
    /// </summary>
    public class EnterpriseServerFilterDto : ISortingDto
    {
        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
