﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer
{
    /// <summary>
    /// Модель сервера публикации
    /// </summary>
    public class CloudContentServerDto : CreateCloudContentServerDto
    {
        public CloudContentServerDto()
        {
            AvailablePublishNodes = [];
            PublishNodes = [];
        }

        /// <summary>
        /// Id сервера публикации
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Доступные ноды публикации
        /// </summary>
        public List<PublishNodeDto> AvailablePublishNodes { get; set; }
    }
}
