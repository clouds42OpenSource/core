﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer
{
    /// <summary>
    /// Модель фильтра сервера публикации
    /// </summary>
    public class CloudContentServerFilterDto : ISortingDto
    {
        /// <summary>
        /// Название сервера публикации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание сервера публикации
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сайт публикации
        /// </summary>
        public string PublishSiteName { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
