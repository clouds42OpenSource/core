﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer
{
    /// <summary>
    /// Модель сгруппированных данных адресов публикации,
    /// по серверу публикации
    /// </summary>
    public class GroupedPublishNodeAddressesByContentServerDto
    {
        /// <summary>
        /// Сервер публикации
        /// </summary>
        public Guid ContentServerId { get; set; }

        /// <summary>
        /// Адреса нод публикации
        /// </summary>
        public List<string> PublishNodeAddresses { get; set; } = [];
    }
}
