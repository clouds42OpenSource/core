﻿using Clouds42.DataContracts.CloudServicesSegment.PublishNode;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudServicesContentServer
{
    /// <summary>
    /// Модель создания сервера публикации
    /// </summary>
    public class CreateCloudContentServerDto
    {
        /// <summary>
        /// Название сервера публикации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание сервера публикации
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сайт публикации
        /// </summary>
        public string PublishSiteName { get; set; }

        /// <summary>
        /// Сгрупирован по аккаунту
        /// </summary>
        public bool GroupByAccount { get; set; }
        
        /// <summary>
        /// Выбранные ноды публикации
        /// </summary>
        public List<PublishNodeDto> PublishNodes { get; set; } = [];
    }
}
