﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Модель создания записи завершения сеансов в информационной базе 
    /// </summary>
    public class CreateTerminationSessionsInDatabaseDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Статус завершения сеансов в инф. базе
        /// </summary>
        public TerminateSessionsInDbStatusEnum TerminateSessionsInDbStatus { get; set; } =
            TerminateSessionsInDbStatusEnum.InProcess;
    }
}
