﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Модель регистрации результата завершения
    /// сеансов в информационной базе 
    /// </summary>
    public class RegisterTerminationSessionsInDatabaseResultDto
    {
        /// <summary>
        /// Id записи/процесса завершения сеансов в инф. базе 
        /// </summary>
        public Guid TerminationSessionsInDatabaseId { get; set; }

        /// <summary>
        /// Статус завершения сеансов в инф. базе
        /// </summary>
        public TerminateSessionsInDbStatusEnum TerminateSessionsInDbStatus { get; set; }
    }
}
