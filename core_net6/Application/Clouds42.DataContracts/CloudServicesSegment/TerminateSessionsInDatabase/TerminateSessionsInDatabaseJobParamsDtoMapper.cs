﻿
namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Маппер модели параметров задачи по завершению сеансов в информационной базе 
    /// </summary>
    public static class TerminateSessionsInDatabaseJobParamsDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели параметров
        /// для завершения сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель параметров задачи по завершению сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public static TerminateSessionsInDatabaseJobParamsDto MapToTerminateSessionsInDatabaseParamsDc(
            this TerminateSessionsInDatabaseJobParamsDto model) => new()
        {
                TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                InitiatorId = model.InitiatorId
            };
    }
}
