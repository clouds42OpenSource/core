﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Модель параметров задачи по завершению сеансов в информационной базе
    /// </summary>
    public class TerminateSessionsInDatabaseJobParamsDto
    {
        /// <summary>
        /// Id записи/процесса завершения сеансов в инф. базе 
        /// </summary>
        public Guid TerminationSessionsInDatabaseId { get; set; }

        /// <summary>
        /// Id инициатора
        /// </summary>
        public Guid InitiatorId { get; set; }
    }
}
