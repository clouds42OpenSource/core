﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Модель данных завершения сеансов в информационной базе
    /// </summary>
    public class TerminationSessionsInDatabaseDataDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Cуществует ли процесс завершения сеансов в базе данных
        /// Запись со статусом "В процессе завершения сеансов"
        /// </summary>
        public bool IsExistSessionTerminationProcess { get; set; }
    }
}
