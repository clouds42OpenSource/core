﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase
{
    /// <summary>
    /// Модель для запуска процесса
    /// по завершению сеансов в информационной базе
    /// </summary>
    public class StartProcessOfTerminateSessionsInDatabaseDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }
    }
}
