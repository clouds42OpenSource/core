﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.Interface
{
    /// <summary>
    /// Элемент сегмента
    /// </summary>
    public interface ISegmentElementDto
    {
        /// <summary>
        /// ID элемента
        /// </summary>
        Guid Id { get; set; }

        /// <summary>
        /// Название элемента
        /// </summary>
        string Name { get; set; }
    }
}
