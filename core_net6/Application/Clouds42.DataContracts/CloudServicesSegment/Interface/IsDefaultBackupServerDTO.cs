﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.Interface
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class IsDefaultBackupServerDto
    {
        [XmlElement(ElementName = nameof(IsDefaultBackupServer))]
        [DataMember(Name = nameof(IsDefaultBackupServer))]
        public bool IsDefaultBackupServer { set; get; }
    }
}
