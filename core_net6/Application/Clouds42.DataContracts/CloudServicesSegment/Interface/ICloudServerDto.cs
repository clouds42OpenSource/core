﻿namespace Clouds42.DataContracts.CloudServicesSegment.Interface
{
    /// <summary>
    /// Интерфейс модели сервера
    /// с базовыми полями
    /// </summary>
    public interface ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        string ConnectionAddress { get; set; }
    }
}
