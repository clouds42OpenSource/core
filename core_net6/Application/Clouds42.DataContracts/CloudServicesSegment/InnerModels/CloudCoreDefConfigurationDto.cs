﻿using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Dto обьект для получения дефолтных конфигураций
    /// </summary>
    public class CloudCoreDefConfigurationDto
    {
        /// <summary>
        /// Разница часов между Украиной и Москвой
        /// </summary>
        public int HoursDifferenceOfUkraineAndMoscow { get; set; }

        /// <summary>
        /// Обьект дефолтного сегмента
        /// </summary>
        public CloudCoreSegmentConfigDto DefaultSegment { get; set; }

        /// <summary>
        /// Контент страницы уведомления
        /// </summary>
        public string MainPageNotification { get; set; }

        /// <summary>
        /// Тип аггрегатора
        /// </summary>
        public RussianLocalePaymentAggregatorEnum RussianLocalePaymentAggregator { get; set; }

        /// <summary>
        /// Коллекция сегментов
        /// </summary>
        public List<CloudCoreSegmentConfigDto> Segments { get; set; }
    }
}