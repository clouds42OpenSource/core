﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceTokenValidityDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceTokenValidity))]
        [DataMember(Name = nameof(ServiceTokenValidity))]
        public bool ServiceTokenValidity { get; set; }
        public ServiceTokenValidityDto()
        {
        }

        public ServiceTokenValidityDto(bool serviceTokenValidity)
        {
            ServiceTokenValidity = serviceTokenValidity;
        }
    }
}