﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Модель ответа на добавление нового CloudService
    /// </summary>
    public sealed class AddCloudServiceResponseDto
    {
        /// <summary>
        /// Значение для поля Id в таблице CloudService
        /// </summary>
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public Guid Id { get; set; }
    }
}