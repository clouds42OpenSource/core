﻿namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Cloud service integration to Cloud dto
    /// </summary>
    public class CloudServiceIntegrationDto
    {
        /// <summary>
        /// Email user identifier
        /// </summary>
        public string Email { get; set; }
    }
}
