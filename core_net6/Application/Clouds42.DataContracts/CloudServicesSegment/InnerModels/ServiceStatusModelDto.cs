﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Модель для информация заблокирован сервис или нет и причина блокировки
    /// </summary>
    public class ServiceStatusModelDto
    {
        /// <summary>
        /// Заблокирован сервис или нет
        /// </summary>
        public bool ServiceIsLocked { get; set; }

        /// <summary>
        /// Причина блокировки
        /// </summary>
        public string ServiceLockReason { get; set; }

        /// <summary>
        /// Тип причины блокировки сервиса
        /// </summary>
        public ServiceLockReasonType ServiceLockReasonType { get; set; }

        /// <summary>
        /// Признак наличия не погашенного ОП
        /// </summary>
        public bool PromisedPaymentIsActive { get; set; }

        /// <summary>
        /// Сумма ОП
        /// </summary>
        public string PromisedPaymentSum { get; set; }

        /// <summary>
        /// Граничная дата оплаты ОП
        /// </summary>
        public DateTime PromisedPaymentExpireDate { get; set; }
    }
}