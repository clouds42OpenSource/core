﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class DeleteCloudServiceModelDto
    {
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }
    }
}