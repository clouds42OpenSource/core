﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Cloud service integretion to Cloud dto
    /// </summary>
    public class CloudServiceInfoDto
    {
        /// <summary>
        /// Баланс
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Баланс
        /// </summary>
        public List<ServiceInfoDto> Services { get; set; } = [];
    }
}
