﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetServiceTokenModelDto
    {
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }

        [XmlElement(ElementName = nameof(ServiceToken))]
        [DataMember(Name = nameof(ServiceToken))]
        public Guid? ServiceToken { get; set; }
    }
}