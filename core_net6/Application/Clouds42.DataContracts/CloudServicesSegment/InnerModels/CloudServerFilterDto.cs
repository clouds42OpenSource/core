﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.CloudServicesSegment.Interface;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Модель фильтра сервера облака
    /// </summary>
    public class CloudServerFilterDto : ICloudServerDto, ISortingDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
