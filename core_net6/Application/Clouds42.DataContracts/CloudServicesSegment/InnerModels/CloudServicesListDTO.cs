﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Модель ответа со списком записей из таблицы CloudService
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public sealed class CloudServicesListDto
    {
        /// <summary>
        /// Id записи в таблице CloudService
        /// </summary>
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public Guid Id { get; set; }

        /// <summary>
        /// Значение поля ServiceId
        /// </summary>
        [XmlElement(ElementName = nameof(CloudServiceId))]
        [DataMember(Name = nameof(CloudServiceId))]
        public string CloudServiceId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceCaption))]
        [DataMember(Name = nameof(ServiceCaption))]
        public string ServiceCaption { get; set; }

        /// <summary>
        /// JsonWebToken доступа
        /// </summary>
        [XmlElement(ElementName = nameof(JsonWebToken))]
        [DataMember(Name = nameof(JsonWebToken))]
        public string JsonWebToken { get; set; }
    }
}