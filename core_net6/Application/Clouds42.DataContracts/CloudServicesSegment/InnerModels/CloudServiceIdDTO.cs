﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudServiceIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }
    }
}