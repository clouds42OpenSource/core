﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudServiceCaptionDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }
    }
}