﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Модель для обновления записи в таблице CloudService
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CloudServiceDto
    {
        /// <summary>
        /// Значение для поля ServiceId в таблице CloudService
        /// </summary>
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }

        /// <summary>
        /// Название службы
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceCaption))]
        [DataMember(Name = nameof(ServiceCaption))]
        public string ServiceCaption { get; set; } 
    }
}
