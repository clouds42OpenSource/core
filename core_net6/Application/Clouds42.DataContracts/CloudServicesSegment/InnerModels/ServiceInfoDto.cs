﻿using System;
using System.ComponentModel;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Service info to Cloud dto
    /// </summary>
    public class ServiceInfoDto
    {
        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Дата окончания сервиса
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Признак демо периода.
        /// </summary>
        [DefaultValue(false)]
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Стоимость сервиса
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Признак заблокирован или нет сервис
        /// </summary>
        public bool Frozen { get; set; }

    }
}
