﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AddCloudServiceModelDto
    {
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }

        [XmlElement(ElementName = nameof(ServiceCaption))]
        [DataMember(Name = nameof(ServiceCaption))]
        public string ServiceCaption { get; set; }
    }
}