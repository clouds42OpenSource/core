﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// Обьект дефолтного сегмента
    /// </summary>
    public class CloudCoreSegmentConfigDto
    {
        /// <summary>
        /// Имя дефолтного сегмента
        /// </summary>
        public string DefaultSegmentName { get; set; }

        /// <summary>
        /// ID дефолтного сегмента
        /// </summary>
        public Guid SegmentId { get; set; }
    }
}