﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.InnerModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudServicesIDsListDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(CloudServiceIDs))]
        [DataMember(Name = nameof(CloudServiceIDs))]
        public CloudServicesList CloudServiceIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CloudServicesIDsListDto()
        {
            CloudServiceIDs = new CloudServicesList();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CloudServicesList
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = "Type")] [IgnoreDataMember]
        public string type = nameof(List);
 
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<string> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CloudServicesList()
        {
            List = [];
        }
    }
}
