﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class FileStorageServerModelDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(FileStorageServerPath))]
        [DataMember(Name = nameof(FileStorageServerPath))]
        [StringNonNullAndEmpty(ErrorMessage = "FileStorageServerPath required")]
        public string FileStorageServerPath { get; set; }

        [XmlElement(ElementName = nameof(IsDefaultStorage))]
        [DataMember(Name = nameof(IsDefaultStorage))]
        [Required(ErrorMessage = "IsDefaultStorage required")]
        public bool? IsDefaultStorage { get; set; }

        /// <summary></summary>
        public FileStorageServerModelDto()
        {
        }
    }
}