﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary>
    /// Модель файлового хранилища по умолчанию
    /// </summary>
    public class SegmentFileStorageDataDto
    {
        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        public Guid FileStorageId { get; set; }

        /// <summary>
        /// Id сегмента
        /// </summary>
        public Guid SegmentId { get; set; }

        /// <summary>
        /// Наименование файлового хранилища
        /// </summary>
        public string FileStorageName { get; set; }
    }
}