﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class FileStorageServerPathModelDto
    {
        [XmlElement(ElementName = "FileStorageServerID")]
        [DataMember(Name = "FileStorageServerID")]
        [Required]
        public Guid? StorageServerId { get; set; }

        [XmlElement(ElementName = nameof(FileStorageServerPath))]
        [DataMember(Name = nameof(FileStorageServerPath))]
        [Required(ErrorMessage = "IsDefaultStorage required")]
        public string FileStorageServerPath { get; set; }

        public FileStorageServerPathModelDto()
        {
        }
    }
}