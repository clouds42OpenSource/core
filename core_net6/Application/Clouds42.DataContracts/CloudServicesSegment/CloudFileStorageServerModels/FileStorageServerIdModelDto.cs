﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class FileStorageServerIdModelDto
    {
        [XmlElement(ElementName = "FileStorageServerID")]
        [DataMember(Name = "FileStorageServerID")]
        [Required]
        public Guid? StorageServerId { get; set; }

        /// <summary></summary>
        public FileStorageServerIdModelDto()
        {
        }
    }
}