﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class IsDefaultStorageDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(IsDefaultStorage))]
        [DataMember(Name = nameof(IsDefaultStorage))]
        public bool IsDefaultStorage { get; set; }
        public IsDefaultStorageDto()
        {
        }
    }
}