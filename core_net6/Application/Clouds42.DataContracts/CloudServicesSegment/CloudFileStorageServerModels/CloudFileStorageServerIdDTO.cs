﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary>
    /// Модель идентификатора файлового хранилища
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudFileStorageServerIdDto
    {
        /// <summary>
        /// Идентификатор файлового хранилища
        /// </summary>
        [XmlElement(ElementName = "FileStorageServerID")]
        [DataMember(Name = "FileStorageServerID")]
        public Guid StorageServerId { get; set; }

        /// <summary></summary>
        public CloudFileStorageServerIdDto()
        {
        }
    }
}