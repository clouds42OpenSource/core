﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class FileStorageServerDefaultModelDto
    {
        [XmlElement(ElementName = "FileStorageServerID")]
        [DataMember(Name = "FileStorageServerID")]
        [Required]
        public Guid? StorageServerId { get; set; }

        [XmlElement(ElementName = nameof(IsDefaultStorage))]
        [DataMember(Name = nameof(IsDefaultStorage))]
        [Required(ErrorMessage = "IsDefaultStorage required")]
        public bool? IsDefaultStorage { get; set; }

        public FileStorageServerDefaultModelDto()
        {
        }
    }
}