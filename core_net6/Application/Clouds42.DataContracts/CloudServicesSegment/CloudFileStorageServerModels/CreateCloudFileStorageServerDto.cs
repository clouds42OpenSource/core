﻿
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary>
    /// Модель создания файлового хранилища
    /// </summary>
    public class CreateCloudFileStorageServerDto : ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// DNS имя хранилища
        /// </summary>
        public string DnsName { get; set; }
    }
}
