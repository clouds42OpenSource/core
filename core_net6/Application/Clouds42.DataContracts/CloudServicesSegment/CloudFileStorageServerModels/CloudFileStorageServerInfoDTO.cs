﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudFileStorageServerInfoDto
    {
        [XmlElement(ElementName = nameof(AccountUser))]
        [DataMember(Name = nameof(AccountUser))]
        public Guid AccountUser { get; set; }

        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(StorageId))]
        [DataMember(Name = nameof(StorageId))]
        public Guid StorageId { get; set; }

        [XmlElement(ElementName = nameof(CompanyNumber))]
        [DataMember(Name = nameof(CompanyNumber))]
        public int CompanyNumber { get; set; }

        public CloudFileStorageServerInfoDto()
        {

        }
    }
}
