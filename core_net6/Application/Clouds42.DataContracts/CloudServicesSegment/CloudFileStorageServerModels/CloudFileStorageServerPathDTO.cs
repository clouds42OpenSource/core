﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudFileStorageServerPathDto
    {
         /// <summary></summary>
         [XmlElement(ElementName = "CloudFileStorageServerPath")]
         [DataMember(Name = "CloudFileStorageServerPath")]
         public string StorageServerPath { get; set; }

        /// <summary></summary>
        public CloudFileStorageServerPathDto()
        {
        }
    }
}