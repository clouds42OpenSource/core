﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary>
    /// Модель файлового хранилища
    /// </summary>
    public class CloudFileStorageServerDto : CreateCloudFileStorageServerDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public Guid Id { get; set; }
    }
}
