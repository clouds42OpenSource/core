﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudFileStorageServerModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class FileStorageServerIDsListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "FileStorageServerIDsList")]
        [DataMember(Name = "FileStorageServerIDsList")]
        public GuidListItemDto StorageServerIDs { get; set; }

        /// <summary></summary>
        public FileStorageServerIDsListDto()
        {
            StorageServerIDs = new GuidListItemDto();
        }
    }
}