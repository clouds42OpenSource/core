﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudTerminalServersIDListDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(TerminalServerIDs))]
        [DataMember(Name = nameof(TerminalServerIDs))]
        public GuidListItemDto TerminalServerIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CloudTerminalServersIDListDto()
        {
            TerminalServerIDs = new GuidListItemDto();
        }
    }
}
