﻿namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// Результат завершения RDP сессий
    /// </summary>
    public class CloseRdpSessionsDto
    {
        /// <summary>
        /// Признак успешности операции
        /// </summary>
        public bool IsSuccess { get; set; }
        
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
