﻿
using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// Модель создания терминального сервера
    /// </summary>
    public class CreateCloudTerminalServerDto : ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }
    }
}
