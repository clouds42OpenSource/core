﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CloudTerminalServersPostRequestSetNameModelDto
    {
        [XmlElement(ElementName = nameof(TerminalServerID))]
        [DataMember(Name = nameof(TerminalServerID))]
        public Guid TerminalServerID { get; set; }

        [XmlElement(ElementName = "Name")]
        [DataMember(Name = "Name")]
        public string TerminalServerName { get; set; }

        public CloudTerminalServersPostRequestSetNameModelDto()
        {

        }

        public bool IsValid
        {
            get { return TerminalServerID!=Guid.Empty && !string.IsNullOrEmpty(TerminalServerName); }
        }
    }
}

