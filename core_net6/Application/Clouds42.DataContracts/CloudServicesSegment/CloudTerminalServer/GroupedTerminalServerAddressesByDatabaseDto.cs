﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// Модель сгруппированных адресов терминальных серверов,
    /// по инф. базе
    /// </summary>
    public class GroupedTerminalServerAddressesByDatabaseDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Адреса подключения терминальных серверов
        /// </summary>
        public List<string> ConnectionAddresses { get; set; } = [];
    }
}
