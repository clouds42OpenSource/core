﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudTerminalServersTerminalServerIDResultDto
    {
        [XmlElement(ElementName = nameof(TerminalServerID))]
        [DataMember(Name = nameof(TerminalServerID))]
        public Guid TerminalServerID { get; set; }

        public CloudTerminalServersTerminalServerIDResultDto()
        {

        }

        public CloudTerminalServersTerminalServerIDResultDto(Guid terminalServerID)
        {
            TerminalServerID = terminalServerID;
        }
    }
}
