﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudTerminalServerNameResultDto
    {
        [XmlElement(ElementName = "Name")]
        [DataMember(Name = "Name")]
        public string TerminalServerName { get; set; }

        public CloudTerminalServerNameResultDto()
        {

        }

        public CloudTerminalServerNameResultDto(string terminalServerName)
        {
            TerminalServerName = terminalServerName;
        }
    }
}

