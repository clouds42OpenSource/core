﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// Модель терминального сервера
    /// </summary>
    public class CloudTerminalServerDto : CreateCloudTerminalServerDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public Guid Id { get; set; }
    }
}
