﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    /// <summary>
    /// Модель адресов терминальных серверов для информационной базы
    /// </summary>
    public class TerminalServerAddressesForDatabaseDto
    {
        /// <summary>
        /// Id аккаунта владельца базы
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер инф. базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Адреса терминальных серверов
        /// </summary>
        public List<string> TerminalServerAddresses { get; set; } = [];
    }
}
