﻿using Clouds42.DataContracts.BaseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    // ReSharper disable once InconsistentNaming
    public class UserRdpSessionsDto : INotifyPropertyChanged
    {
        [XmlElement(ElementName = nameof(UserRdpSessionID))]
        [DataMember(Name = nameof(UserRdpSessionID))]
        public int UserRdpSessionID { get; set; }

        [XmlElement(ElementName = nameof(SessionName))]
        [DataMember(Name = nameof(SessionName))]
        public string SessionName { get; set; }

        [XmlElement(ElementName = nameof(UserName))]
        [DataMember(Name = nameof(UserName))]
        public string UserName { get; set; }

        [XmlElement(ElementName = nameof(SessionStatus))]
        [DataMember(Name = nameof(SessionStatus))]
        public string SessionStatus { get; set; }

        [XmlElement(ElementName = nameof(Downtime))]
        [DataMember(Name = nameof(Downtime))]
        public int Downtime { get; set; }

        [XmlElement(ElementName = nameof(EntryTime))]
        [DataMember(Name = nameof(EntryTime))]
        public DateTime EntryTime { get; set; }

        [XmlElement(ElementName = nameof(TerminalServerID))]
        [DataMember(Name = nameof(TerminalServerID))]
        // ReSharper disable once InconsistentNaming
        public Guid TerminalServerID { get; set; }

        [IgnoreDataMember]
        private bool _checked;

        [IgnoreDataMember]
        public bool Checked { get { return _checked; } set { _checked = value; NotifyPropertyChanged(nameof(Checked)); } }

        [IgnoreDataMember]
        public double IdleInMinutes => Downtime / 60.0;

        public UserRdpSessionsDto() { }

        public UserRdpSessionsDto(RdpSessionParams param, Guid serverId)
        {
            UserRdpSessionID = param.UserRdpSessionID;
            SessionName = param.SessionName;
            UserName = param.UserName;
            SessionStatus = param.SessionStatus;
            Downtime = param.Downtime.Second;
            EntryTime = param.EntryTime;
            TerminalServerID = serverId;
        }

        #region PropertyChangedEventHandler
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }

    public class TerminalServerUserRdpSessionsTable
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(type))] [IgnoreDataMember]
        // ReSharper disable once InconsistentNaming
        public string type = "table";

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<UserRdpSessionsDto> Rows { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TerminalServerUserRdpSessionsTable()
        {
            Rows = [];
        }
    }

    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    // ReSharper disable once InconsistentNaming
    public class TerminalServerUserRdpSessionsTableDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(TerminalServerUserRdpSessionsTable))]
        [DataMember(Name = nameof(TerminalServerUserRdpSessionsTable))]
        public TerminalServerUserRdpSessionsTable TerminalServerUserRdpSessionsTable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public TerminalServerUserRdpSessionsTableDto()
        {
            TerminalServerUserRdpSessionsTable = new TerminalServerUserRdpSessionsTable();
        }
    }
}
