﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudTerminalServer
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CloudTerminalServerPostRequestCloseDto
    {
        [XmlElement(ElementName = nameof(TerminalServerID))]
        [DataMember(Name = nameof(TerminalServerID))]
        public Guid TerminalServerID { get; set; }

        [XmlElement(ElementName = nameof(UserRdpSessionID))]
        [DataMember(Name = nameof(UserRdpSessionID))]
        public int UserRdpSessionID { get; set; }

        public CloudTerminalServerPostRequestCloseDto()
        {

        }

        public bool IsValid
        {
            get { return TerminalServerID!=Guid.Empty && UserRdpSessionID >= 0; }
        }
    }
}


