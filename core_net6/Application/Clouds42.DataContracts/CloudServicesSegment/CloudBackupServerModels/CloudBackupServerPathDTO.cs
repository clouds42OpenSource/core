﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudBackupServerPathDto
    {
        [XmlElement(ElementName = nameof(BackupServerPath))]
        [DataMember(Name = nameof(BackupServerPath))]
        public string BackupServerPath { set; get; }
    }
}
