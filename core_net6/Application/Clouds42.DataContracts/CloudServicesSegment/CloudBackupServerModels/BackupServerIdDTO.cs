﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class BackupServerIdDto
    {
        [XmlElement(ElementName = nameof(BackupServerID))]
        [DataMember(Name = nameof(BackupServerID))]
        public Guid BackupServerID {get; set;}
    }
}
