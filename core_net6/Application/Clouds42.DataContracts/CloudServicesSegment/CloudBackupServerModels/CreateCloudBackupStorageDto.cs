﻿using Clouds42.DataContracts.CloudServicesSegment.Interface;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    /// <summary>
    /// Модель создания хранилища бэкапов
    /// </summary>
    public class CreateCloudBackupStorageDto : ICloudServerDto
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Физический путь
        /// </summary>
        public string PhysicalPath { get; set; }
    }
}
