﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CloudBackupServerDto
    {
        [XmlElement(ElementName = "CloudBackupServerID")]
        [DataMember(Name = "CloudBackupServerID")]
        public Guid ID {get; set;}

        [XmlElement(ElementName = nameof(BackupServerPath))]
        [DataMember(Name = nameof(BackupServerPath))]
        public string BackupServerPath { get; set; }

        [XmlElement(ElementName = nameof(IsDefaultBackupServer))]
        [DataMember(Name = nameof(IsDefaultBackupServer))]
        public bool IsDefaultBackupServer { get; set; }
    }
}