﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CloudBackupServerIdDto
    {
        [XmlElement(ElementName = "CloudBackupServerID")]
        [DataMember(Name = "CloudBackupServerID")]
        public Guid ID { get; set; }

    }
}
