﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudBackupServersIDsListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "CloudBackupServersIDsList")]
        [DataMember(Name = "CloudBackupServersIDsList")]
        public GuidListItemDto CloudBackupServersIDs { get; set; }

        /// <summary></summary>
        public CloudBackupServersIDsListDto()
        {
            CloudBackupServersIDs = new GuidListItemDto();
        }
    }
}
