﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.CloudBackupServerModels
{
    /// <summary>
    /// Модель хранилища бэкапов
    /// </summary>
    public class CloudBackupStorageDto : CreateCloudBackupStorageDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        public Guid Id { get; set; }
    }
}
