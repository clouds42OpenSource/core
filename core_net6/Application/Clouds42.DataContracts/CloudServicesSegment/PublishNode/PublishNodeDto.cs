﻿using System;

namespace Clouds42.DataContracts.CloudServicesSegment.PublishNode
{
    /// <summary>
    /// Нода публикаций
    /// </summary>
    public class PublishNodeDto
    {
        /// <summary>
        /// Идентификатор ноды
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        public string Address { get; set; }
    }
}
