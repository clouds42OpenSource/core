﻿using System;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Краткая информация о пользователе
    /// </summary>
    public class AccountUserShortInfoDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }
    }
}
