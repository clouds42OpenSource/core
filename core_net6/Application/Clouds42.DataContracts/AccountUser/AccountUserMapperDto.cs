﻿using System.Linq;
using User = Clouds42.Domain.DataModels.AccountUser;


namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Маппер для моделей пользователя
    /// </summary>
    public static class AccountUserMapperDto
    {
        /// <summary>
        /// Смапить доменную модель к модели представления
        /// </summary>
        /// <param name="accountUser">Доменная модель</param>
        /// <returns>Модель представления</returns>
        public static AccountUserDto MapToAccountUserDc(this User accountUser)
            => new()
            {
                AccountId = accountUser.AccountId,
                Id = accountUser.Id,
                MiddleName = accountUser.MiddleName,
                Login = accountUser.Login,
                Password = accountUser.Password,
                Activated = accountUser.Activated,
                AuthToken = accountUser.AuthToken,
                Email = accountUser.Email,
                FirstName = accountUser.FirstName,
                Roles = accountUser.AccountUserRoles.Select(s => s.AccountUserGroup).ToList(),
                LastName = accountUser.LastName,
                PhoneNumber = accountUser.PhoneNumber,
                CorpUserSyncStatus = accountUser.CorpUserSyncStatus,
                AccountCaption = accountUser.Account.AccountCaption
            };
    }
}

