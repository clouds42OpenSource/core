﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Модель данных по пользователю для отчета
    /// </summary>
    [DataContract]
    public class AccountUserReportDataDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [DataMember]
        public string Login { get; set; }

        /// <summary>
        /// Почтовый адрес пользователя
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ФИО
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [DataMember]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [DataMember]
        public string Status { get; set; }

        /// <summary>
        /// Количество внутренних доступов к базам
        /// у пользователя
        /// </summary>
        [DataMember]
        public int CountInternalAccessesToDatabases { get; set; }

        /// <summary>
        /// Количество внешних доступов к базам
        /// у пользователя
        /// </summary>
        [DataMember]
        public int CountExternalAccessesToDatabases { get; set; }

        /// <summary>
        /// Доступные роли пользователю
        /// </summary>
        [DataMember]
        public string AccountUserRoles { get; set; }
    }
}
