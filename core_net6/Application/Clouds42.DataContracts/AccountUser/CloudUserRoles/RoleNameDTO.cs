﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class RoleNameDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(RoleName))]
        [DataMember(Name = nameof(RoleName))]
        public string RoleName { get; set; }

        /// <summary></summary>
        public RoleNameDto()
        {
        }
    }
}