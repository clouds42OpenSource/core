﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RoleIDModelDto
    {
        [XmlElement(ElementName = nameof(CloudUserRoleID))]
        [DataMember(Name = nameof(CloudUserRoleID))]
        [Required]
        public int? CloudUserRoleID { get; set; }

        public RoleIDModelDto()
        {
        }
    }
}