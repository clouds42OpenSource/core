﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class NewRoleModelDto
    {
        [XmlElement(ElementName = nameof(RoleName))]
        [DataMember(Name = nameof(RoleName))]
        [StringNonNullAndEmpty(ErrorMessage = "Role name is required")]
        public string RoleName { get; set; }

        [XmlElement(ElementName = "RoleDisplayName")]
        [DataMember(Name = "RoleDisplayName")]
        [StringNonNullAndEmpty(ErrorMessage = "Role display name is required")]
        public string RoleDispleyName { get; set; }

        public NewRoleModelDto()
        {
        }
    }
}