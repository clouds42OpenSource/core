﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RoleDisplayNameModelDto
    {
        [XmlElement(ElementName = nameof(CloudUserRoleID))]
        [DataMember(Name = nameof(CloudUserRoleID))]
        [Required]
        public int? CloudUserRoleID { get; set; }

        [XmlElement(ElementName = nameof(RoleDisplayName))]
        [DataMember(Name = nameof(RoleDisplayName))]
        [StringNonNullAndEmpty(ErrorMessage = "Role display name is required")]
        public string RoleDisplayName { get; set; }

        public RoleDisplayNameModelDto()
        {
        }
    }
}