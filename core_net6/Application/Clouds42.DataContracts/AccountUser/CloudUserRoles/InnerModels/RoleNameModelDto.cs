﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RoleNameModelDto
    {
        [XmlElement(ElementName = nameof(CloudUserRoleID))]
        [DataMember(Name = nameof(CloudUserRoleID))]
        [Required]
        public int? CloudUserRoleID { get; set; }

        [XmlElement(ElementName = nameof(RoleName))]
        [DataMember(Name = nameof(RoleName))]
        [StringNonNullAndEmpty(ErrorMessage = "Role name is required")]
        public string RoleName { get; set; }

        public RoleNameModelDto()
        {
        }
    }
}