﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class RoleDisplayNameDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(RoleDisplayName))]
        [DataMember(Name = nameof(RoleDisplayName))]
        public string RoleDisplayName { get; set; }

        /// <summary></summary>
        public RoleDisplayNameDto()
        {
        }
    }
}