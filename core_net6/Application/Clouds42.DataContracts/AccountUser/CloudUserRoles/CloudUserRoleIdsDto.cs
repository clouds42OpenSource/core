﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudUserRoleIdsDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(CloudUserRoleIDs))]
        [DataMember(Name = nameof(CloudUserRoleIDs))]
        public IntListItemDto CloudUserRoleIDs { get; set; }

        /// <summary></summary>
        public CloudUserRoleIdsDto()
        {
            CloudUserRoleIDs = new IntListItemDto();
        }
    }
}