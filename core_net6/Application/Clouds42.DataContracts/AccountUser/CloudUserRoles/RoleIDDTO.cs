﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.CloudUserRoles
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class RoleIDDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(CloudUserRoleID))]
        [DataMember(Name = "AccouCloudUserRoleIDntID")]
        public int CloudUserRoleID { get; set; }

        /// <summary></summary>
        public RoleIDDto()
        {
        }
    }
}