﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Расширения для модели пользователя
    /// </summary>
    public static class AccountUserExtensionsDto
    {
        /// <summary>
        /// Получить полное имя пользователя
        /// </summary>
        /// <param name="accountUser">Модель пользователя</param>
        /// <returns>Полное имя пользователя</returns>
        public static string GetFullName(this IAccountUser accountUser)
            => $"{accountUser.LastName} {accountUser.FirstName} {accountUser.MiddleName}".Trim();
    }
}
