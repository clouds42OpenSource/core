﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель параметров входа
    /// по электронной почте
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class LoginEmailModelDto
    {
        /// <summary>
        /// Электронная почта
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        public LoginEmailModelDto()
        {

        }

        public LoginEmailModelDto(string email, string pass)
        {
            Email = email;
            Password = pass;
        }
    }
}