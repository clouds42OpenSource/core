﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель списка пользовательских сессий
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserSessionIDsListDto
    {
        /// <summary>
        /// Пользовательские сессии
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserSessionIDs))]
        [DataMember(Name = nameof(AccountUserSessionIDs))]
        public GuidListItemDto AccountUserSessionIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AccountUserSessionIDsListDto()
        {
            AccountUserSessionIDs = new GuidListItemDto();
        }
    }
}