﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class LoginPhoneNumberDto
    {
        [XmlElement(ElementName = nameof(PhoneNumber))]
        [DataMember(Name = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

        public LoginPhoneNumberDto()
        {

        }

    }
}