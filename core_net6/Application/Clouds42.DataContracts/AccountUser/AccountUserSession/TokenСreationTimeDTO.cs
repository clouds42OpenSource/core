﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель даты создания токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class TokenСreationTimeDto
    {
        /// <summary>
        /// Дата создания токена
        /// </summary>
        [XmlElement(ElementName = nameof(TokenСreationTime))]
        [DataMember(Name = nameof(TokenСreationTime))]
        public DateTime TokenСreationTime { get; set; }
        public TokenСreationTimeDto()
        {
        }

        public TokenСreationTimeDto(DateTime tokenСreationTime)
        {
            TokenСreationTime = tokenСreationTime;
        }
    }
}