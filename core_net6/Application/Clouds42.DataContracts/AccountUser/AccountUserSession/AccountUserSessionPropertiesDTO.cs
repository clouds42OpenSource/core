﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Свойства пользовательской сессии
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserSessionPropertiesDto
    {
        /// <summary>
        /// Идентификатор пользователя облака
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        /// <summary>
        /// Токен
        /// </summary>
        [XmlElement(ElementName = nameof(Token))]
        [DataMember(Name = nameof(Token))]
        public Guid Token { get; set; }

        /// <summary>
        /// Статичный токен
        /// </summary>
        [XmlElement(ElementName = nameof(StaticToken))]
        [DataMember(Name = nameof(StaticToken))]
        public bool StaticToken { get; set; }

        /// <summary>
        /// Дата создания токена
        /// </summary>
        [XmlElement(ElementName = nameof(TokenСreationTime))]
        [DataMember(Name = nameof(TokenСreationTime))]
        public DateTime? TokenСreationTime { get; set; }

        /// <summary>
        /// Описание клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDescription))]
        [DataMember(Name = nameof(ClientDescription))]
        public string ClientDescription { get; set; }

        /// <summary>
        /// Описание устройства клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDeviceInfo))]
        [DataMember(Name = nameof(ClientDeviceInfo))]
        public string ClientDeviceInfo { get; set; }

        /// <summary>
        /// Ip адрес клиента
        /// </summary>
        [XmlElement(ElementName = "ClientIPAddress")]
        [DataMember(Name = "ClientIPAddress")]
        public string ClientIpAddress { get; set; }
    }
}