﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель описания клиента
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ClientDescriptionDto
    {
        /// <summary>
        /// Описание клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDescription))]
        [DataMember(Name = nameof(ClientDescription))]
        public string ClientDescription { get; set; }
        public ClientDescriptionDto()
        {
        }
    }
}