﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Return accoun user session id
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ClientIpAddressDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "ClientIPAddress")]
        [DataMember(Name = "ClientIPAddress")]
        public string ClientIpAddress { get; set; }
        public ClientIpAddressDto()
        {
        }
    }
}