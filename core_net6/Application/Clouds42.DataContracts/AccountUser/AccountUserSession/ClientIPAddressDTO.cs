﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель статичного токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class StaticTokenDto
    {
        /// <summary>
        /// Статичный токен
        /// </summary>
        [XmlElement(ElementName = nameof(StaticToken))]
        [DataMember(Name = nameof(StaticToken))]
        public bool StaticToken { get; set; }
        public StaticTokenDto()
        {
        }
    }
}