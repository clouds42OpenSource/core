﻿using System;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels
{
    /// <summary>
    /// Модель параметров входа
    /// по логину
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class LoginModelDto
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserLogin))]
        [DataMember(Name = nameof(AccountUserLogin))]
        [StringNonNullAndEmpty]
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserPassword))]
        [DataMember(Name = nameof(AccountUserPassword))]
        public string AccountUserPassword { get; set; }

        /// <summary>
        /// Описание клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDescription))]
        [DataMember(Name = nameof(ClientDescription))]
        [StringNonNullAndEmpty]
        public string ClientDescription { get; set; }

        /// <summary>
        /// Описание устройства клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDeviceInfo))]
        [DataMember(Name = nameof(ClientDeviceInfo))]
        public string ClientDeviceInfo { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("AccountUserLogin: "+ AccountUserLogin);
            sb.AppendLine("AccountUserPassword: " + AccountUserPassword);
            sb.AppendLine("ClientDescription: " + ClientDescription);
            sb.AppendLine("ClientDeviceInfo: " + ClientDeviceInfo);
            return sb.ToString();
        }
    }
}