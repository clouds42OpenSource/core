﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels
{
    /// <summary>
    /// Модель параметров входа
    /// по логину
    /// </summary>
    public class CrossServiceLoginModelDto
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Описание клиента
        /// </summary>
        public string ClientDescription { get; set; }

        /// <summary>
        /// Описание устройства клиента
        /// </summary>
        public string ClientDeviceInfo { get; set; }
    }
}