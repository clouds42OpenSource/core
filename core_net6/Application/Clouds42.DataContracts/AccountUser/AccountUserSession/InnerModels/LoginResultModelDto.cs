﻿using System;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels
{
    [Serializable]
    [XmlRoot(ElementName = @"Result")]
    public class LoginResultModelDto
    {
        [XmlElement] public Guid Token { get; set; }
    }
}