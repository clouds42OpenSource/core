﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserTokenDto
    {
        /// <summary>
        /// Токен досутупа.
        /// </summary>
        [XmlElement(ElementName = nameof(Token))]
        [DataMember(Name = nameof(Token))]
        public Guid Token { get; set; }
        
        public AccountUserTokenDto()
        {
        }

        public AccountUserTokenDto(Guid token)
        {
            Token = token;
        }
    }
}
