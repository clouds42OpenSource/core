﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUserSession
{
    /// <summary>
    /// Модель информации об устройстве клиента
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ClientDeviceInfoDto
    {
        /// <summary>
        /// Информация об устройстве клиента
        /// </summary>
        [XmlElement(ElementName = nameof(ClientDeviceInfo))]
        [DataMember(Name = nameof(ClientDeviceInfo))]
        public string ClientDeviceInfo { get; set; }
        public ClientDeviceInfoDto()
        {
        }
    }
}