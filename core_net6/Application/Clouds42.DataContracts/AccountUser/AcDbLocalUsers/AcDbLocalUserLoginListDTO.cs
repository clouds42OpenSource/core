﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUserLoginListDto
    {
        [XmlElement(ElementName = "AcDbLocalUser")]
        [DataMember(Name = "AcDbLocalUser")]
        public List<LocalUserIdAndStringDto> AcDbLocalUserIDsList { get; set; } = [];
    }
}
