﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbAccountUserIDDto
    {
        [XmlElement(ElementName = "AccountUserID")]
        [DataMember(Name = "DatabAccountUserIdaseID")]
        public Guid AccountUserId { get; set; }

        public AcDbAccountUserIDDto()
        {
        }

        public AcDbAccountUserIDDto(Guid accountUserId)
        {
            AccountUserId = accountUserId;
        }
    }
}
