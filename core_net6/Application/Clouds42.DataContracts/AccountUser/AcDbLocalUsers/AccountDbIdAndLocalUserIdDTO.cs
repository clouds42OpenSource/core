﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDbIdAndAccountUserIdDto
    {
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        [XmlElement(ElementName = "AccountUserID")]
        [DataMember(Name = "AccountUserID")]
        public Guid AccountUserId { get; set; }
    }
}