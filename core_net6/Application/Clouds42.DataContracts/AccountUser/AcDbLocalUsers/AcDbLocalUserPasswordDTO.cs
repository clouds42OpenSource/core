﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUserPasswordDto
    {
        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        public AcDbLocalUserPasswordDto()
        {
        }

        public AcDbLocalUserPasswordDto(string password)
        {
            Password = password;
        }
    }
}

