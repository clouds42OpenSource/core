﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLocalUserIdDto
    {
        [XmlElement(ElementName = "LocalUserID")]
        [DataMember(Name = "LocalUserID")]
        public Guid LocalUserId { get; set; }
    }
}