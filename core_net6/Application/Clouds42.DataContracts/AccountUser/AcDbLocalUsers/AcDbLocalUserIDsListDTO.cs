﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUserIDsListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = "AcDbLocalUserIDs")]
        [DataMember(Name = "AcDbLocalUserIDs")]
        public GuidListItemDto AcDbLocalUserIDsList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbLocalUserIDsListDto()
        {
            AcDbLocalUserIDsList = new GuidListItemDto();
        }
    }
}