﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestSetIdDto
    {
        [XmlElement(ElementName = nameof(LocalUserID))]
        [DataMember(Name = nameof(LocalUserID))]
        public Guid LocalUserID { get; set; }

        [XmlElement(ElementName = nameof(NewLocalUserID))]
        [DataMember(Name = nameof(NewLocalUserID))]
        public Guid NewLocalUserID { get; set; }

        public bool IsValid()
        {
            return LocalUserID != Guid.Empty && NewLocalUserID != Guid.Empty;
        }
    }

}

