﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUserIdDto
    {
        [XmlElement(ElementName = "LocalUserID")]
        [DataMember(Name = "LocalUserID")]
        public Guid LocalUserId { get; set; }

        public AcDbLocalUserIdDto()
        {
        }

        public AcDbLocalUserIdDto(Guid localUserId)
        {
            LocalUserId = localUserId;
        }
    }
}