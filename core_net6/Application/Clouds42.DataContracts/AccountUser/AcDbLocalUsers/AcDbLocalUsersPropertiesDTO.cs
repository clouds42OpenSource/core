﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUsersPropertiesDto
    {
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        [XmlElement(ElementName = nameof(LinkLastPingDatetime))]
        [DataMember(Name = nameof(LinkLastPingDatetime))]
        public DateTime LinkLastPingDatetime { get; set; }

        [XmlElement(ElementName = nameof(LinkLastConnectionError))]
        [DataMember(Name = nameof(LinkLastConnectionError))]
        public string LinkLastConnectionError { get; set; }

    }
}
