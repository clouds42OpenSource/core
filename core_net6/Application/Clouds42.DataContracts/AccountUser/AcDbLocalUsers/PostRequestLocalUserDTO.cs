﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLocalUserDto
    {
	    /// <summary>
	    /// Id информационной базы аккаунта
	    /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

	    /// <summary>
	    /// Логин пользователя
	    /// </summary>
        [XmlElement(ElementName = nameof(LocalUserLogin))]
        [DataMember(Name = nameof(LocalUserLogin))]
        public string LocalUserLogin { get; set; }

		/// <summary>
		/// Пароль пользователя
		/// </summary>
        [XmlElement(ElementName = nameof(LocalUserPassword))]
        [DataMember(Name = nameof(LocalUserPassword))]
        public string LocalUserPassword { get; set; }

	    /// <summary>
	    /// Id пользователя
	    /// </summary>
        [XmlElement(ElementName = "AccountUserID")]
        [DataMember(Name = "AccountUserID")]
        public Guid AccountUserId { get; set; }

	    /// <summary>
	    /// Номер области для баз с разделителями
	    /// </summary>
        [XmlElement(ElementName = "Zone")]
        [DataMember(Name = "Zone")]
        public int AccountDatabaseSection { get; set; }
	}
}
