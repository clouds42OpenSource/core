﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AcDbAccountUserDto
    {
        [XmlElement(ElementName = nameof(LocalUserID))]
        [DataMember(Name = nameof(LocalUserID))]
        public Guid LocalUserID { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Password))]
        [DataMember(Name = nameof(Password))]
        public string Password { get; set; }

        [XmlElement(ElementName = nameof(LinkLastPingDatetime))]
        [DataMember(Name = nameof(LinkLastPingDatetime))]
        public DateTime LinkLastPingDatetime { get; set; }

        [XmlElement(ElementName = nameof(LinkLastConnectionError))]
        [DataMember(Name = nameof(LinkLastConnectionError))]
        public string LinkLastConnectionError { get; set; }

        [XmlElement(ElementName = nameof(ConnectionState))]
        [DataMember(Name = nameof(ConnectionState))]
        public string ConnectionState { get; set; }

        [XmlElement(ElementName = nameof(IsDefault))]
        [DataMember(Name = nameof(IsDefault))]
        public bool IsDefault { get; set; }

        public AcDbAccountUserDto()
        {

        }

    }
}
