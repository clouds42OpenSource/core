﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
	/// <summary>
	/// Модель для добавления нового пользователя 1с с доступом к информационной базе аккаунта
	/// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestAddUserDto
    {
		/// <summary>
		/// Id информационной базы аккаунта
		/// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }
		/// <summary>
		/// Id пользователя
		/// </summary>
        [XmlElement(ElementName = "AccountUserID")]
        [DataMember(Name = "AccountUserID")]
        public Guid AccountUserId { get; set; }
		/// <summary>
		/// Номер области для баз с разделителями
		/// </summary>
        [XmlElement(ElementName = "Zone")]
        [DataMember(Name = "Zone")]
        public int AccountDatabaseSection { get; set; }
	}
}