﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLastPingDto
    {
        [XmlElement(ElementName = nameof(LocalUserID))]
        [DataMember(Name = nameof(LocalUserID))]
        public Guid LocalUserID { get; set; }

        [XmlElement(ElementName = nameof(LinkLastPingDatetime))]
        [DataMember(Name = nameof(LinkLastPingDatetime))]
        public DateTime LinkLastPingDatetime { get; set; }
    }
}