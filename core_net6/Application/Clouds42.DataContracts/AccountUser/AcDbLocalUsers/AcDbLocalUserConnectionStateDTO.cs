﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLocalUserConnectionStateDto
    {
        [XmlElement(ElementName = nameof(ConnectionState))]
        [DataMember(Name = nameof(ConnectionState))]
        public string ConnectionState { get; set; }

        public AcDbLocalUserConnectionStateDto()
        { 
        }
        public AcDbLocalUserConnectionStateDto(string connectionState)
        {
            ConnectionState = connectionState;
        }

    }
}
