﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    public class UserConnectionStatesRowsModelDto
    {
        [XmlElement(ElementName = "LocalUserID")]
        [DataMember(Name = "LocalUserID")]
        public Guid LocalUserId { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseId))]
        [DataMember(Name = nameof(AccountDatabaseId))]
        public Guid AccountDatabaseId { get; set; }

        [XmlElement(ElementName = nameof(ConnectionState))]
        [DataMember(Name = nameof(ConnectionState))]
        public string ConnectionState { get; set; }

        public UserConnectionStatesRowsModelDto()
        {
        }

        public UserConnectionStatesRowsModelDto(Guid Id, Guid accountDatabaseId, string connectionState)
        {
            ConnectionState = connectionState;
            LocalUserId = Id;
            AccountDatabaseId = accountDatabaseId;
        }

    }
    
    
    public class UserConnectionStatesTable
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string type= "table";

        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]

        public List<UserConnectionStatesRowsModelDto> Rows { get; set; } = [];
    }

    

}
