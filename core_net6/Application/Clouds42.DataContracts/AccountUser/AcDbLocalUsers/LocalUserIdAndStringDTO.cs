﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AcDbLocalUsers
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class LocalUserIdAndStringDto
    {
        [XmlElement(ElementName = "LocalUserID")]
        [DataMember(Name = "LocalUserID")]
        public Guid LocalUserId { get; set; }

        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public string Value { get; set; }
    }
}