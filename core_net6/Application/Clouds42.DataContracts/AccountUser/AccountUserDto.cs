﻿using Clouds42.DataContracts.ActiveDirectory.Interface;
using System;
using System.Collections.Generic;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Data contract пользователя
    /// </summary>
    public class AccountUserDto : ICreateNewUserModel
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// ID Аккаунта пользователя
        /// </summary>        
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>        
        public string MiddleName { get; set; }

        /// <summary>
        /// Полное имя пользователя
        /// </summary>        
        public string FullName { get; set; }

        /// <summary>
        /// Эл. почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>        
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Пользователь активирован (подтвержден мобильный или одобрен менеджером)
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Пользователь создан в домене Active Directory
        /// </summary>
        public bool CreatedInAd { get; set; }


        /// <summary>
        /// Признак что пользователь отписан от рассылки
        /// </summary>
        public bool? Unsubscribed { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Роли пользователя в аккаунте
        /// </summary>
        public List<string> AccountRoles { get; set; } = [];
        public string Password { get;  set; }
        public Guid? AuthToken { get;  set; }
        public List<AccountUserGroup> Roles { get; set; }
        public string CorpUserSyncStatus { get;  set; }
        public string AccountCaption { get;  set; }

        public Guid? ResetCode { get; set; }

        public Guid GetAccountUserId()
        {
            if (Id.HasValue)
                return Id.Value;

            throw new ArgumentException(nameof(Id));
        }

        public string GetUserName()
            => Login;

        public string ClientFilesPath { get; set; }
    }
}
