﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Информация о доступе пользователя
    /// </summary>
    public class AccessUserInfoDc
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid UserId { get; set; }
        
        /// <summary>
        /// Email пользователя
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// Статус доступа
        /// </summary>
        public AccountDatabaseAccessState State { get; set; }

        /// <summary>
        /// Причина задержки
        /// </summary>
        public string DelayReason { get; set; }
    }
}