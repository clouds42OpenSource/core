﻿using System;

namespace Clouds42.DataContracts.AccountUser
{
    /// <summary>
    /// Параметры управления состоянием блокировки пользователя в МС
    /// </summary>
    public class ManageAccountUserLockStateParamsDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Признак доступности
        /// </summary>
        public bool IsAvailable { get; set; }

        /// <summary>
        /// Время начала операции
        /// </summary>
        public DateTime OperationStartDateTime { get; set; } = DateTime.Now.ToLocalTime();
    }
}
