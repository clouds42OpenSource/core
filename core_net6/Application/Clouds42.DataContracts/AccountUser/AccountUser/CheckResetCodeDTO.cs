﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CheckResetCodeDto
    {
        /// <summary>
        ///     Валидность токена
        /// </summary>
        [XmlElement(ElementName = nameof(TokenValidity))]
        [DataMember(Name = nameof(TokenValidity))]
        public bool TokenValidity { get; set; }

        public CheckResetCodeDto()
        {
        }

        public CheckResetCodeDto(bool isTokenValid)
        {
            TokenValidity = isTokenValid;
        }
    }
}
