﻿namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель разрешений для работы с пользователями аккаунта
    /// </summary>
    public class PermissionsForWorkingWithUsersDto
    {
        /// <summary>
        /// Имеет разрешение на добавление нового пользователя
        /// </summary>
        public bool HasPermissionForAddAccountUser { get; set; }
    }
}
