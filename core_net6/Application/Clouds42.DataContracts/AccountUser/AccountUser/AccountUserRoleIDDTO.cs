﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserRoleIDDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(RoleID))]
        [DataMember(Name = nameof(RoleID))]
        public int? RoleID { get; set; }
    }
}