﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель даты регистрации пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserCreationDateDto

    {
        /// <summary>
        /// Дата регистрации пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public DateTime? CreationDate { get; set; }
    }
}