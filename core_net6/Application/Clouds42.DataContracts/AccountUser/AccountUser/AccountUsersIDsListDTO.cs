﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель списка идентификаторов пользователей
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUsersIDsListDto
    {
        /// <summary>
        /// Модель списка Guid
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserIDs))]
        [DataMember(Name = nameof(AccountUserIDs))]
        public GuidListItemDto AccountUserIDs { get; set; }

        /// <summary></summary>
        public AccountUsersIDsListDto()
        {
            AccountUserIDs = new GuidListItemDto();
        }
    }
}