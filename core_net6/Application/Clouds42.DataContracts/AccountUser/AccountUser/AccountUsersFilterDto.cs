﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель фильтра пользователей аккаунта
    /// </summary>
    public class AccountUsersFilterDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; }
    }
}
