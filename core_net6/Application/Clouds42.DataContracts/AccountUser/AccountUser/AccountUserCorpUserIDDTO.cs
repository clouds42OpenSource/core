﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель номера пользователя по учетной системе КОРП
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserCorpUserIDDto
    {
        /// <summary>
        /// Номер пользователя по учетной системе КОРП
        /// </summary>
        [XmlElement(ElementName = nameof(CorpUserID))]
        [DataMember(Name = nameof(CorpUserID))]
        public Guid? CorpUserID { get; set; }
    }
}