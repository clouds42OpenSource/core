﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель для получения информации при синхронизации пользователь с Саюри.
    /// </summary>
    [XmlRoot]
    public class UserDataForSauriDto
    {
        [XmlElement]
        public Guid Id { get; set; }

        [XmlElement]
        public string Login { get; set; }
        
        [XmlElement]
        public string Password { get; set; }
        
        [XmlElement]
        public string LocalLogin { get; set; }

        [XmlElement]
        public string LocalDbName { get; set; }
    }
    
    [XmlRoot]
    public class UserDataForSauriListDto(List<UserDataForSauriDto> list)
    {
        [XmlAttribute] public string Type { set; get; } = @"Table";

        [XmlElement] public List<UserDataForSauriDto> Row { get; set; } = list;

        public UserDataForSauriListDto() : this([])
        {
        }
    }

    [XmlRoot(ElementName = "Result")]
    public class UserDataForSauriResultDto(UserDataForSauriListDto list)
    {
        [XmlElement] public UserDataForSauriListDto UserData { get; set; } = list;

        public UserDataForSauriResultDto() : this(new UserDataForSauriListDto())
        {
        }
    }

}