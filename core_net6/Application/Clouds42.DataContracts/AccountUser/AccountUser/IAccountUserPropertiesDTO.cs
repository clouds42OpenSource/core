﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Интерфейс для DTO представления свойств пользователей аккаунта.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IAccountUserPropertiesDto
    {
        /// <summary>
        /// Идентификатор пользователя аккаунта
        /// </summary>
        // ReSharper disable once InconsistentNaming
        Guid ID { get; set; }

        /// <summary>
        /// Идентификатор связанного аккаунта
        /// </summary>
        Guid AccountId { get; set; }

        /// <summary>
        /// Login (имя пользователя для входа)
        /// </summary>
        string Login { get; set; }

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Телефонный номер в полном формате
        /// </summary>
        string FullPhoneNumber { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        string LastName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        string MiddleName { get; set; }

        /// <summary>
        /// Идентификатор пользователя в системе КОРП.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        Guid? CorpUserID { get; set; }

        /// <summary>
        /// Статус синхронизации пользователя с системой КОРП
        /// </summary>
        string CorpUserSyncStatus { get; set; }

        /// <summary>
        /// Удалён пользователь или нет?
        /// </summary>
        bool Removed { get; set; }

        /// <summary>
        /// Дата создания пользователя
        /// </summary>
        string CreationDate { get; set; }

        /// <summary>
        /// Является ли пользователь менеджером (администратором аккаунта)?
        /// </summary>
        bool IsManager { get; set; }
    }
}