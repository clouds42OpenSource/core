﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AdminUserRoleDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AdminUserRole))]
        [DataMember(Name = nameof(AdminUserRole))]
        public bool AdminUserRole { get; set; }
        public AdminUserRoleDto()
        {
        }

        public AdminUserRoleDto(bool adminUserRole)
        {
            AdminUserRole = adminUserRole;
        }
    }
}