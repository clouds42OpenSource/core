﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель пользователей аккаунта
    /// </summary>
    public class AccountUserTerminateListDto(List<AccountUserDto> list)
    {
        /// <summary>
        /// Список терминальных сесий пользователей
        /// </summary>
        public List<AccountUserDto> UserTerminateList { get; set; } = list;

        public AccountUserTerminateListDto() : this([])
        {
        }
    }
}
