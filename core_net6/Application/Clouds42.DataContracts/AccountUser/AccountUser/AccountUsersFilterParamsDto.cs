﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Параметры фильтра для поиска пользователей аккаунта
    /// </summary>
    public class AccountUsersFilterParamsDto : SelectDataCommonDto<AccountUsersFilterDto>
    {
    }
}
