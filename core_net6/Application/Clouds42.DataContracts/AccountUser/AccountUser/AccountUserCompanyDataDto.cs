﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Данные о компании пользователя аккаунта
    /// </summary>
    public class AccountUserCompanyDataDto
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ИНН аккаунта
        /// </summary>
        public string AccountInn { get; set; }

        /// <summary>
        /// Название локали аккаунта
        /// </summary>
        public string AccountLocaleName { get; set; }

        /// <summary>
        /// Данные по администраторам аккаунта
        /// </summary>
        public List<AccountAdministratorInfoDto> AccountAdministrators { get; set; } = [];
    }
}
