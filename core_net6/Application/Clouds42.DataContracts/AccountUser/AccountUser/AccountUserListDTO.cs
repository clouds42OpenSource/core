﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель пользователей аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserListDto(List<AccountUserPropertiesDto> list)
    {
        /// <summary>
        /// Пользователи аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserList))]
        [DataMember(Name = nameof(AccountUserList))]
        public List<AccountUserPropertiesDto> AccountUserList { get; set; } = list;

        public AccountUserListDto() : this([])
        {
        }
    }
}
