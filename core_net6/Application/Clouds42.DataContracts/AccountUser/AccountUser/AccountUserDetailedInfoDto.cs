﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель подробной/детальной информации о пользователе аккаунта 
    /// </summary>
    public class AccountUserDetailedInfoDto
    {
        /// <summary>
        /// Id аккаунта пользователя
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Электронная почта пользователя
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Дополнительная информация для пользователя аккаунта
        /// </summary>
        public AdditionalInfoForAccountUserDto AdditionalInfo { get; set; }

        /// <summary>
        /// Данные о компании пользователя аккаунта
        /// </summary>
        public AccountUserCompanyDataDto AccountUserCompanyData { get; set; }
    }
}
