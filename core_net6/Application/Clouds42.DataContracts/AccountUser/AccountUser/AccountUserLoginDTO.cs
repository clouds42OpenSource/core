﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель логина пользователя облака
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserLoginDto
    {
        /// <summary>
        /// Логин пользователя облака
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }
        public AccountUserLoginDto()
        {
        }

        public AccountUserLoginDto(string login)
        {
            Login = login;
        }
    }
}