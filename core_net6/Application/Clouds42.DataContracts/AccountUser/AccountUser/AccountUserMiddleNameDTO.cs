﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель Отчества пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserMiddleNameDto
    {
        /// <summary>
        /// Отчество пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        public AccountUserMiddleNameDto()
        {
        }
    }
}