﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Общие/базовые данные для работы с пользователями аккаунта
    /// </summary>
    public class CommonDataForWorkingWithUsersDto
    {
        /// <summary>
        /// Id аккаунта 
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса "Мои инф. базы"
        /// </summary>
        public Guid MyDatabaseServiceId { get; set; }

        /// <summary>
        /// Разрешения для работы с пользователями аккаунта
        /// </summary>
        public PermissionsForWorkingWithUsersDto PermissionsForWorkingWithUsers { get; set; }
    }
}
