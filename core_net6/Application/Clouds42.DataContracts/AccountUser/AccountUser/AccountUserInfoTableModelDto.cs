﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Таблица с информацией о пользователях
    /// </summary>
    public class AccountUserInfoTableModelDto
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = "Table"; 

        /// <summary>
        /// Информация о пользователе
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<AccountUserInfoRowDto> Rows { get; set; } = [];
    }
}
