﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель номера телефона пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserPhoneNumberDto
    {
        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(PhoneNumber))]
        [DataMember(Name = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }
        public AccountUserPhoneNumberDto()
        {
        }
    }
}