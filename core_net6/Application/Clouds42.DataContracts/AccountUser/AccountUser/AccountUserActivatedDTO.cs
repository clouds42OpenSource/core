﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель признака активности пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserActivatedDto
    {
        /// <summary>
        /// Признак активности пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(Activated))]
        [DataMember(Name = nameof(Activated))]
        public bool Activated { get; set; }
    }
}