﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{    /// <summary>
     /// Модель идентификатора аккаунта
     /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
        public class AccountUserIdAndVerifireStatusDTO
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        /// <summary>
        /// Статус из верификации
        /// </summary>
        [XmlElement(ElementName = nameof(IsVerified))]
        [DataMember(Name = nameof(IsVerified))]
        public bool IsVerified { get; set; }
    }
}
