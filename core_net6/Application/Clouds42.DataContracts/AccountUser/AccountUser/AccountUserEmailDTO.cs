﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель электронной почты
    /// пользователя облака
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserEmailDto
    {
        /// <summary>
        /// Элекктронная почта пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }
        public AccountUserEmailDto()
        {
        }
    }
}