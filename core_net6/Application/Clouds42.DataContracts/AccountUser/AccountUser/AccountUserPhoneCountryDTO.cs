﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserPhoneCountryDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(PhoneCountry))]
        [DataMember(Name = nameof(PhoneCountry))]
        public string PhoneCountry { get; set; }

        public AccountUserPhoneCountryDto()
        {
        }
    }
}