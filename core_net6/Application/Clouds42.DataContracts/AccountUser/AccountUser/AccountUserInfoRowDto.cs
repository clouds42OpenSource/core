﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Информация о пользователе
    /// </summary>
    public class AccountUserInfoRowDto
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [XmlElement(ElementName = "AccountUserId")]
        [DataMember(Name = "AccountUserId")]
        public Guid Id { get; set; }

        /// <summary>
        /// Название пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserName))]
        [DataMember(Name = nameof(AccountUserName))]
        public string AccountUserName { get; set; }

        /// <summary>
        /// Элекстронная почта пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }
    }
}
