﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserUnsubscribedDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Unsubscribed))]
        [DataMember(Name = nameof(Unsubscribed))]
        public bool Unsubscribed { get; set; }
    }
}