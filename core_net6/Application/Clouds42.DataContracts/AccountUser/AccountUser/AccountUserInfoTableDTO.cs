﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель таблицы с информацией о пользователях
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserInfoTableDto
    {
        /// <summary>
        /// Таблица с информацией о пользователях
        /// </summary>
        [XmlElement(ElementName = "AccountSearchResultTable")]
        [DataMember(Name = "AccountSearchResultTable")]
        public AccountUserInfoTableModelDto AccountUserSearchResultTable { get; set; } = new();
    }
}
