﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель идентификатора аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserAccountIDDto
    {
        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }
    }
}