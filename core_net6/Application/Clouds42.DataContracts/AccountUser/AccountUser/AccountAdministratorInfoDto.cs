﻿namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель информации об администраторе аккаунта
    /// </summary>
    public class AccountAdministratorInfoDto
    {
        /// <summary>
        /// Имя пользователя(администратора)
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя(администратора)
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество пользователя(администратора)
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Электронная почта пользователя(администратора)
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона пользователя(администратора)
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
