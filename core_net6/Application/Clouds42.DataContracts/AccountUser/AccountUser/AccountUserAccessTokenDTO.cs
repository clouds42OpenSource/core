﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель представления токен доступа пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class AccountUserAccessTokenDto
    {
        /// <summary>
        /// Токен доступа
        /// </summary>
        [XmlElement(ElementName = nameof(AccessToken))]
        [DataMember(Name = nameof(AccessToken))]
        public string AccessToken { get; set; }
    }
}
