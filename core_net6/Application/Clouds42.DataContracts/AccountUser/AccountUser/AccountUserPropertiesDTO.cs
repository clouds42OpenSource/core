﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <inheritdoc />
    /// <summary>
    /// DTO для представления свойств пользователей аккаунта.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class AccountUserPropertiesDto : IAccountUserPropertiesDto
    {
        /// <inheritdoc />
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Эл. почта пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        public string FullPhoneNumber { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Имя пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        public string FirstName { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Отчество пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        /// <inheritdoc />
        [XmlElement(ElementName = nameof(CorpUserID))]
        [DataMember(Name = nameof(CorpUserID))]
        public Guid? CorpUserID { get; set; }

        /// <inheritdoc />
        [XmlElement(ElementName = nameof(CorpUserSyncStatus))]
        [DataMember(Name = nameof(CorpUserSyncStatus))]
        public string CorpUserSyncStatus { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Пользовтель удален
        /// </summary>
        [XmlElement(ElementName = nameof(Removed))]
        [DataMember(Name = nameof(Removed))]
        public bool Removed { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Дата создания
        /// </summary>
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public string CreationDate { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Пользователь является менеджером
        /// </summary>
        [XmlElement(ElementName = nameof(IsManager))]
        [DataMember(Name = nameof(IsManager))]
        public bool IsManager { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Пользователь активорован
        /// </summary>
        [XmlElement(ElementName = nameof(Activated))]
        [DataMember(Name = nameof(Activated))]
        public bool Activated { get; set; }
    }
}