﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель валидности токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result", Namespace = "")]
    public class AccounUserTokenValidityDto
    {
        /// <summary>
        /// Валидность токена
        /// </summary>
        [XmlElement(ElementName = nameof(TokenValidity))]
        [DataMember(Name = nameof(TokenValidity))]
        public bool TokenValidity { get; set; }
        public AccounUserTokenValidityDto()
        {
        }

        public AccounUserTokenValidityDto(bool isTokenValid)
        {
            TokenValidity = isTokenValid;
        }
    }
}