﻿namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    public class AccountSignInModelDto
    {
        /// <summary>
        /// Login Username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Login Password
        /// </summary>
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        /// <summary>
        /// Login ReturnUrl
        /// </summary>
        public string ReturnUrl { get; set; } = string.Empty;
    }
}
