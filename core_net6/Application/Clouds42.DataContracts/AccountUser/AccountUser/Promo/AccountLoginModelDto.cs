﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    [XmlRoot(ElementName = "Request")]
    public class AccountLoginModelDto
    {
        [XmlElement]
        public string Username { get; set; }


        [XmlElement]
        public string Password { get; set; }


        [XmlElement]
        public bool RememberMe { get; set; }


        [XmlElement]
        public string ReturnUrl { get; set; } = string.Empty;
    }
}
