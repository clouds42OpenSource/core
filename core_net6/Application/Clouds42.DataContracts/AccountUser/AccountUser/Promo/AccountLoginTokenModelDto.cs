﻿using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
      [XmlRoot(ElementName = "Request")]
    public class AccountLoginTokenModelDto
    {
          [XmlElement(ElementName = nameof(Token))]
          public Guid Token { get; set; }
    }
}
