﻿namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    public class AuthorizationResultDto
    {
        public int Status { get; set; }

        public string Id { get; set; }

        public AuthorizationResultDto()
        {
            
        }
        public AuthorizationResultDto(int status)
        {
            Status = status;
            Id = null;
        }


        public AuthorizationResultDto(int status, string id)
        {
            Status = status;
            Id = id;
        }
    }
}