﻿namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    public class UserInfoModelDto
    {
        public string Login { get; set; }

        public string Email { get; set; }

        public string Fio { get; set; }
    }
}