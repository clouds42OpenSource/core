﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    [XmlRoot(ElementName = "Request")]
    public class UserPasswordModelDto
    {
        [XmlElement(ElementName = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Password))]
        public string Password { get; set; }
        
    }
}