﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    /// <summary>
    /// Модель токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountTokenAuthorizationDto
    {
        /// <summary>
        /// Токен
        /// </summary>
        [XmlElement(ElementName = nameof(AccountToken))]
        [DataMember(Name = nameof(AccountToken))]
        public Guid? AccountToken { get; set; }
    }
}