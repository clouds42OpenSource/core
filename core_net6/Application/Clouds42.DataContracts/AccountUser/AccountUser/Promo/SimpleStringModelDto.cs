﻿namespace Clouds42.DataContracts.AccountUser.AccountUser.Promo
{
    public class SimpleStringModelDto
    {
        public string Value { get; set; }
    }
}
