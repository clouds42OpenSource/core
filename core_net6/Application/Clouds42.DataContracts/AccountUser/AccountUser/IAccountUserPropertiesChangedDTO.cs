﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    // ReSharper disable once InconsistentNaming
    public interface IAccountUserPropertiesChangedDto
    {
        // ReSharper disable once InconsistentNaming
        Guid AccountUserID { get; set; }

        Guid AccountId { get; set; }

        string Login { get; set; }

        string Email { get; set; }

        string FullPhoneNumber { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string MiddleName { get; set; }

        // ReSharper disable once InconsistentNaming
        Guid? CorpUserID { get; set; }

        string CorpUserSyncStatus { get; set; }

        bool Removed { get; set; }

        string CreationDate { get; set; }
    }
}