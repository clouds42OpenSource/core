﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Модель данных(записей) пользователей аккаунта
    /// </summary>
    public class AccountUsersDataDto : SelectDataResultCommonDto<AccountUserDataItemDto>
    {
    }
}
