﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    // ReSharper disable once InconsistentNaming
    public class AccountUserPropertiesChangedModelDto: IAccountUserPropertiesChangedDto
    {
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }

        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        public string FullPhoneNumber { get; set; }

        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        public string FirstName { get; set; }

        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        [XmlElement(ElementName = nameof(CorpUserID))]
        [DataMember(Name = nameof(CorpUserID))]
        public Guid? CorpUserID { get; set; }

        [XmlElement(ElementName = nameof(CorpUserSyncStatus))]
        [DataMember(Name = nameof(CorpUserSyncStatus))]
        public string CorpUserSyncStatus { get; set; }

        [XmlElement(ElementName = nameof(Removed))]
        [DataMember(Name = nameof(Removed))]
        public bool Removed { get; set; }

        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public string CreationDate { get; set; }

    }
}