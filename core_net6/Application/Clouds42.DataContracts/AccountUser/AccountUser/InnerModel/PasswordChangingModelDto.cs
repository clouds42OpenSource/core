﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    /// <summary>
    /// Модель параметров для смены пароля
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PasswordChangingModelDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid? AccountUserID { get; set; }

        /// <summary>
        /// Новый пароль
        /// </summary>
        [XmlElement(ElementName = nameof(NewPassword))]
        [DataMember(Name = nameof(NewPassword))]
        public string NewPassword { get; set; }

        /// <summary>
        /// Старый пароль
        /// </summary>
        [XmlElement(ElementName = nameof(OldPassword))]
        [DataMember(Name = nameof(OldPassword))]
        public string OldPassword { get; set; }
    }
}