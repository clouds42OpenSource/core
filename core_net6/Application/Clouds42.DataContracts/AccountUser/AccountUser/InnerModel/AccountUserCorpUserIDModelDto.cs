﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserCorpUserIDModelDto
    {
        [XmlElement(ElementName = nameof(CorpUserID))]
        [DataMember(Name = nameof(CorpUserID))]
        [Required]
        public Guid? CorpUserID { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        [Required]
        public Guid AccountUserID { get; set; }
    }
}