﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    public class AccountUsersTerminateSessionDto
    {
        public Guid accountId { get; set; }

        public Guid[] userIds { get; set; }

    }
}
