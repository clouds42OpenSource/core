﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserCreationDateModelDto
    {
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        [Required]
        public DateTime? CreationDate { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid AccountUserID { get; set; }
    }
}