﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserActivatedModel
    {
        [XmlElement(ElementName = nameof(Activated))]
        [DataMember(Name = nameof(Activated))]
        [JsonProperty(PropertyName = nameof(Activated))]
        [Required]
        public bool Activated { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [JsonProperty(PropertyName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        [Required]
        public Guid AccountUserID { get; set; }

        public AccountUserActivatedModel()
        {
        }
    }
}
