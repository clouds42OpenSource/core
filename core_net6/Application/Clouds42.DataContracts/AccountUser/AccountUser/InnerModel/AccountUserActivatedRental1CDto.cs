﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    /// <summary>
    /// Модель конфигурации аренды
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserActivatedRental1CDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserId))]
        [DataMember(Name = nameof(AccountUserId))]
        [Required]
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Флаг активности аренды 1С для web
        /// </summary>
        [XmlElement(ElementName = nameof(WebResource))]
        [DataMember(Name = nameof(WebResource))]
        [Required]
        public bool WebResource { get; set; }

        /// <summary>
        /// Флаг активности аренды 1С для standart
        /// </summary>
        [XmlElement(ElementName = nameof(StandartResource))]
        [DataMember(Name = nameof(StandartResource))]
        [Required]
        public bool StandartResource { get; set; }
    }
}
