﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PhoneNumberModelDto
    {
        [XmlElement(ElementName = nameof(PhoneNumber))]
        [DataMember(Name = nameof(PhoneNumber))]
        public string PhoneNumber { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        public Guid? AccountUserID { get; set; }

    }
}