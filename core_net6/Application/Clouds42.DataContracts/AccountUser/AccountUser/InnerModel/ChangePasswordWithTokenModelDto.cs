﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    /// <summary>
    /// Модель для смены пароля по токену
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class ChangePasswordWithTokenModelDto
    {
        /// <summary>
        /// Токен
        /// </summary>
        [XmlElement(ElementName = nameof(ResetToken))]
        [DataMember(Name = nameof(ResetToken))]
        public string ResetToken { get; set; }

        /// <summary>
        /// Новый пароль
        /// </summary>
        [XmlElement(ElementName = nameof(NewPassword))]
        [DataMember(Name = nameof(NewPassword))]
        public string NewPassword { get; set; }
    }
}
