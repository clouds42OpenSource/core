﻿using System;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    public class AccountUserActivateDto
    {
        public bool isActivate { get; set; }

        public Guid accountUserID { get; set; }

        public AccountUserActivateDto()
        {
        }
    }
}
