﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserRoleIdModelDto
    {
        [XmlElement(ElementName = nameof(RoleId))]
        [DataMember(Name = nameof(RoleId))]
        [Required]
        [Range(0, 2)]
        public int? RoleId { get; set; }

        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        [Required]
        public Guid AccountUserID { get; set; }
    }
}