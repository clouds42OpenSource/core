﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Attributes;

namespace Clouds42.DataContracts.AccountUser.AccountUser.InnerModel
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserUpdateModelDto
    {
        [XmlElement(ElementName = nameof(AccountUserID))]
        [DataMember(Name = nameof(AccountUserID))]
        [Required]
        public Guid AccountUserID { get; set; }

        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        [StringNonNullAndEmpty(ErrorMessage = "FirstName is empty")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        [XmlElement(ElementName = nameof(PhoneNumber))]
        [DataMember(Name = nameof(PhoneNumber))]
        [StringNonNullAndEmpty(ErrorMessage = "PhoneNumber is empty")]
        public string PhoneNumber { get; set; }

    }
}