﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserCommonDataDto
    {
        [XmlElement(ElementName = "ID")]
        [DataMember(Name = "ID")]
        public Guid Id { get; set; }

        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        [XmlElement(ElementName = nameof(Email))]
        [DataMember(Name = nameof(Email))]
        public string Email { get; set; }

        [XmlElement(ElementName = nameof(FullPhoneNumber))]
        [DataMember(Name = nameof(FullPhoneNumber))]
        public string FullPhoneNumber { get; set; }

        [XmlElement(ElementName = nameof(FirstName))]
        [DataMember(Name = nameof(FirstName))]
        public string FirstName { get; set; }

        [XmlElement(ElementName = nameof(LastName))]
        [DataMember(Name = nameof(LastName))]
        public string LastName { get; set; }

        [XmlElement(ElementName = nameof(MiddleName))]
        [DataMember(Name = nameof(MiddleName))]
        public string MiddleName { get; set; }

        [XmlElement(ElementName = "CorpUserID")]
        [DataMember(Name = "CorpUserID")]
        public Guid? CorpUserId { get; set; }

        [XmlElement(ElementName = nameof(CorpUserSyncStatus))]
        [DataMember(Name = nameof(CorpUserSyncStatus))]
        public string CorpUserSyncStatus { get; set; }

        [XmlElement(ElementName = nameof(Removed))]
        [DataMember(Name = nameof(Removed))]
        public bool Removed { get; set; }

        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public DateTime? CreationDate { get; set; }
    }
}