﻿namespace Clouds42.DataContracts.AccountUser.AccountUser
{
    /// <summary>
    /// Дополнительная информация для пользователя аккаунта
    /// </summary>
    public class AdditionalInfoForAccountUserDto
    {
        /// <summary>
        /// Урл магазина приложений
        /// с указанием источника
        /// </summary>
        public string MarketUrlWithSource { get; set; }

        /// <summary>
        /// Уведомление для главной страницы
        /// </summary>
        public string MainPageNotification { get; set; }
    }
}
