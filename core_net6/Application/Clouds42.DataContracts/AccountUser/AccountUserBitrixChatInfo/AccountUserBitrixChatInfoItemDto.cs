﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountUser.AccountUserBitrixChatInfo
{
    /// <summary>
    /// Модель элемента информации о пользователе для чата Битрикс24
    /// </summary>
    public class AccountUserBitrixChatInfoItemDto
    {
        /// <summary>
        /// Название ключа элемента
        /// </summary>
        [JsonProperty(PropertyName = "NAME")]
        public string KeyName { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        [JsonProperty(PropertyName = "VALUE")]
        public string Value { get; set; }

        /// <summary>
        /// Тип отображения
        /// </summary>
        [JsonProperty(PropertyName = "DISPLAY")]
        public string DisplayType { get; set; }
    }
}
