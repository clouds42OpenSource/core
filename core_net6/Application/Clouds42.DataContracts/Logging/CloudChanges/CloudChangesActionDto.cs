﻿using System;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Модель с действиями изменений в приложении
    /// </summary>
    public sealed class CloudChangesActionDto: KeyValueGroupDto<Guid, string>
    {
    }
}