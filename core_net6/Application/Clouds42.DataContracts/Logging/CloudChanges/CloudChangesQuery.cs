﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Фильтр для получения лога с изменениями в приложении
    /// </summary>
    public class CloudChangesQuery : ISortedQuery, IPagedQuery, IHasFilter<CloudChangesFilterDto>, IRequest<ManagerResult<PagedDto<CloudChangesLogRecordDto>>>
    {
        public string OrderBy { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public CloudChangesFilterDto Filter { get; set; }
    }
}
