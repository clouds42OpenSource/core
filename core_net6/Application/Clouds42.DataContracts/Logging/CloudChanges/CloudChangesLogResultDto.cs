﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Модель записей логирования изменений в приложении
    /// </summary>
    public sealed class CloudChangesLogResultDto: SelectDataResultCommonDto<CloudChangesLogRecordDto>
    {
    }
}