﻿using System;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Фильтр для получения лога с изменениями в приложении
    /// </summary>
    public class CloudChangesInputFilterDto: IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// ID лога с изменениями
        /// </summary>
        public Guid? CloudChangesActionId { get; set; }

        /// <summary>
        /// Строка для поиска
        /// </summary>
        public string SearchLine { get; set; }

        /// <summary>
        /// Начальная дата создания лога в приложении
        /// </summary>
        public DateTime? CreateCloudChangeDateFrom { get; set; }

        /// <summary>
        /// Конечная дата создания лога в приложении
        /// </summary>
        public DateTime? CreateCloudChangeDateTo { get; set; }

        /// <summary>
        /// Показывать логи аккаунтов только для текущего пользователя
        /// </summary>
        public bool? ShowMyAccountsOnly { get; set; }

        /// <summary>
        /// Показывать логи для VIP аккаунтов 
        /// </summary>
        public bool? ShowVipAccountsOnly { get; set; }
    }
}
