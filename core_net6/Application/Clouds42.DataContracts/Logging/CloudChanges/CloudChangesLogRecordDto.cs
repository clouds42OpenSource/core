﻿using System;
using AutoMapper;
using MappingNetStandart.Mappings;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Модель записи логирования изменения в приложении
    /// </summary>
    public sealed class CloudChangesLogRecordDto : IMapFrom<Domain.DataModels.CloudChanges>
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// Id действия лога
        /// </summary>
        public Guid CloudChangeId { get; set; }

        /// <summary>
        /// Дата создания лога 
        /// </summary>
        public DateTime CreateCloudChangeDate { get; set; }

        /// <summary>
        /// Название действия
        /// </summary>
        public string CloudChangesActionName { get; set; }

        /// <summary>
        /// Описание действия
        /// </summary>
        public string CloudChangesActionDescription { get; set; }

        /// <summary>
        /// Инициатор действия
        /// </summary>
        public string InitiatorLogin { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Domain.DataModels.CloudChanges, CloudChangesLogRecordDto>();
        }
    }
}
