﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Фильтр для получения лога с изменениями в приложении
    /// </summary>
    public sealed class CloudChangesInputFilterParamsDto : SelectDataCommonDto<CloudChangesInputFilterDto>
    {
    }
}