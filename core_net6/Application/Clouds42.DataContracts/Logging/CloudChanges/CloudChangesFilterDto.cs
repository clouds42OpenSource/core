﻿using System;

namespace Clouds42.DataContracts.Logging.CloudChanges
{
    /// <summary>
    /// Фильтр для получения лога с изменениями в приложении
    /// </summary>
    public sealed class CloudChangesFilterDto: CloudChangesInputFilterDto
    {
        /// <summary>
        /// Признак что пользователь находится в контексте другого аккаунта
        /// </summary>
        public bool? IsForeignAccount { get; set; }

        /// <summary>
        /// ID аккаунта, в контексте которого находится пользователь
        /// </summary>
        public Guid? ContextAccountId { get; set; }

        /// <summary>
        /// ID текещего пользователя
        /// </summary>
        public Guid? CurrentAccountUserId { get; set; }
    }
}
