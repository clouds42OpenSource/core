﻿namespace Clouds42.DataContracts.Logging.CurrentSessionSettings
{
    /// <summary>
    /// Содержит настройки текущей сессии
    /// </summary>
    public sealed class CurrentSessionSettingsModelDto
    {
        /// <summary>
        /// Содержит информацию о контексте текущей сессии
        /// </summary>
        public CurrentSessionContextInfoDto CurrentContextInfo { get; set; }
    }
}
