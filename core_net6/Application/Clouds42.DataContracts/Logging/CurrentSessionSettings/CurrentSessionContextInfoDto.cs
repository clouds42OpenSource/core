﻿using System;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.Logging.CurrentSessionSettings
{
    /// <summary>
    /// Содержит информацию о контексте текущей сессии
    /// </summary>
    public sealed class CurrentSessionContextInfoDto
    {
        /// <summary>
        /// Если <code>false</code>, то находится в чужом контексте
        /// </summary>
        public bool IsDifferentContextAccount { get; set; }

        /// <summary>
        /// Название аккаунта текущего контекста
        /// </summary>
        public string ContextAccountName { get; set; }

        /// <summary>
        /// Аккаунт номер текущего контекста
        /// </summary>
        public string ContextAccountIndex { get; set; }

        /// <summary>
        /// Группы текущего пользователя
        /// </summary>
        public AccountUserGroup[] CurrentUserGroups { get; set; }

        public bool? IsPhoneVerified { get; set; }

        public bool? IsEmailVerified { get; set; }
        
        /// <summary>
        /// Показывать уведомление о том, что можно подключить АО и ТИИ или нет
        /// </summary>
        public bool? ShowSupportMessage { get; set; }

        public bool IsPhoneExists { get; set; }

        public bool IsEmailExists { get; set; }
        public bool IsNew { get; set; }

        public Guid UserId { get; set; }
        public Guid AccountId { get; set; }
        public Guid ContextAccountId { get; set; }

        public string Locale { get; set; }
        public string UserSource { get; set; }
    }
}
