﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Logging
{
    /// <summary>
    /// Список записей в лог
    /// </summary>
    public class LogRecordsListDto
    {
        /// <summary>
        /// Список записей
        /// </summary>
        public List<LogRecordDto> LogRecords { get; set; }
        
        /// <summary>
        /// Фильтр поиска
        /// </summary>
        public LoggingFilterDto Filter { get; set; }
        
        /// <summary>
        /// Пагинация списка
        /// </summary>
        public PaginationBaseDto Pagination { get; set; }
        
        /// <summary>
        /// Список действий
        /// </summary>
        public Dictionary<Guid, string> ActionList { get; set; }
    }
}
