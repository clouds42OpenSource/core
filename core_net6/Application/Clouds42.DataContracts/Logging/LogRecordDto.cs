﻿using System;

namespace Clouds42.DataContracts.Logging
{
    /// <summary>
    /// Модель записи в лог
    /// </summary>
    public class LogRecordDto
    {
        /// <summary>
        /// Аккаунт
        /// </summary>
        public Domain.DataModels.Account Account { get; set; }
        
        /// <summary>
        /// Дата операции
        /// </summary>
        public DateTime OperationDateTime { get; set; }
        
        /// <summary>
        /// Инициатор операции
        /// </summary>
        public string Initiator { get; set; }
        
        /// <summary>
        /// Действие
        /// </summary>
        public string Action { get; set; }
        
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}
