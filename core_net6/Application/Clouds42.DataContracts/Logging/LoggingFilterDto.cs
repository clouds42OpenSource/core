﻿using System;

namespace Clouds42.DataContracts.Logging
{
    /// <summary>
    /// Модель фильтра записей логирования
    /// </summary>
    public class LoggingFilterDto
    {
        /// <summary>
        /// Начальный период
        /// </summary>
        public DateTime PeriodFrom { get; set; }
        
        /// <summary>
        /// Конечный период
        /// </summary>
        public DateTime PeriodTo { get; set; }
        
        /// <summary>
        /// ID действия
        /// </summary>
        public Guid ActionId { get; set; }
        
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Текущая страница
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Признак что пользователь находится в контексте другого аккаунта
        /// </summary>
        public bool IsForeignAccount { get; set; }

        /// <summary>
        /// ID аккаунта, в контексте которого находится пользователь
        /// </summary>
        public Guid ContextAccountId { get; set; }
    }
}
