﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.Logging.LaunchDbAndStartRdp
{
    /// <summary>
    /// Logging entry filter model for launching databases and opening RDP
    /// </summary>
    public class LaunchDbAndStartRdpLogQuery : 
        IPagedQuery, 
        ISortedQuery, 
        IHasFilter<LaunchDbAndStartRdpLogFilterDto>, 
        IRequest<ManagerResult<PagedDto<LaunchDbAndStartRdpLogRecordDto>>>
    {
        /// <summary>
        /// Filter for receiving logging of launching databases and RDP
        /// </summary>
        public LaunchDbAndStartRdpLogFilterDto Filter { get; set; }

        /// <summary>
        /// Records on page
        /// </summary>
        public int? PageSize { get; set; }

        /// <summary>
        /// Number of page
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Sorting, etc. Name.ASC, Name.DESC
        /// </summary>
        public string OrderBy { get; set; }
    }
}
