﻿using System;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.DataContracts.Logging.LaunchDbAndStartRdp
{
    /// <summary>
    /// Модель записи логирования запуска баз и RDP
    /// </summary>
    public class LaunchDbAndStartRdpLogRecordDto
    {
        /// <summary>
        /// ID записи
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Индекс/Номер аккаунта
        /// </summary>
        public int AccountNumber { get; set; }
        /// <summary>
        ///  Дата создания записи
        /// </summary>
        public DateTime ActionCreated { get; set; }
        /// <summary>
        /// Id действия логирования (запуск базы, открытия RDP, ...)
        /// </summary>
        public LinkActionType Action { get; set; }
        /// <summary>
        /// Логин кто выполнял действие
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Версия Линка в котором выполняли действие
        /// </summary>
        public string LinkAppVersion { get; set; }
        /// <summary>
        /// Тип Линка
        /// </summary>
        public AppModes LinkAppType { get; set; }
        /// <summary>
        ///  Номер базы
        /// </summary>
        public string V82Name { get; set; }
        /// <summary>
        /// Тип запуска базы (Тонкий клиент, толстый клиент, ...)
        /// </summary>
        public LaunchType LaunchType { get; set; }

        /// <summary>
        /// Внешний IP адрес
        /// </summary>
        public string ExternalIpAddress { get; set; }

        /// <summary>
        /// Внутренний IP адрес
        /// </summary>
        public string InternalIpAddress { get; set; }
    }
}
