﻿using Clouds42.Domain.Enums.Link42;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.Logging.LaunchDbAndStartRdp
{
    /// <summary>
    /// Модель фильтра записей логирования заппуска баз и открытия RDP
    /// </summary>
    public sealed class LaunchDbAndStartRdpLogFilterDto : IQueryFilter
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int? AccountNumber { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Версия линка
        /// </summary>
        public string LinkAppVersion { get; set; }

        /// <summary>
        /// Внешний IP адрес
        /// </summary>
        public string ExternalIpAddress { get; set; }

        /// <summary>
        /// Внутренний IP адрес
        /// </summary>
        public string InternalIpAddress { get; set; }


        /// <summary>
        /// Фильтр по типу линка
        /// </summary>
        public AppModes? LinkAppType { get; set; }
    }
}
