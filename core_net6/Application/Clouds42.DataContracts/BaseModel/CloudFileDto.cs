﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель файла
    /// </summary>
    [DataContract]
    public class CloudFileDto
    {
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Тип файла
        /// </summary>
        [DataMember]
        public string ContentType { get; set; }

        /// <summary>
        /// Хэш данные
        /// </summary>
        [DataMember]
        public string Base64 { get; set; }

        /// <summary>
        /// Имя файла
        /// </summary>
        [DataMember]
        public string FileName { get; set; }

        /// <summary>
        /// Данные файла
        /// </summary>
        [DataMember]
        public byte[] Bytes { get; set; }
    }
}