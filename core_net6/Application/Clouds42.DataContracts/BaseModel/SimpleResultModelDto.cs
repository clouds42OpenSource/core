﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class SimpleResultModelDto<T>
    {
        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public T Value { get; set; }

        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; } 

    }
}