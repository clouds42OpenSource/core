﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Данные документа
    /// </summary>
    public class DocumentDataDto
    {
        /// <summary>
        /// Массив байтов содержимого
        /// </summary>
        public byte[] Bytes { get; set; }
        
        /// <summary>
        /// Название файла
        /// </summary>
        public string FileName { get; set; }
    }
}
