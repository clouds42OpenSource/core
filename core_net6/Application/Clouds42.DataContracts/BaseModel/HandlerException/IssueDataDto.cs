﻿namespace Clouds42.DataContracts.BaseModel.HandlerException
{
    /// <summary>
    /// Данные проблемы
    /// </summary>
    public class IssueDataDto
    {
        /// <summary>
        /// Заголовок операции вызвавшей ошибку
        /// </summary>
        public string OperationTitle { get; set; }

        /// <summary>
        /// Данные аккаунта
        /// </summary>
        public IssueAccountInfoDto AccountInfo { get; set; }
    }
}
