﻿using System;

namespace Clouds42.DataContracts.BaseModel.HandlerException
{
    /// <summary>
    /// Информация об аккаунте в контексте проблемы
    /// </summary>
    public class IssueAccountInfoDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid? AccountUserId { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Роли пользователя
        /// </summary>
        public string AccountUserRoles { get; set; }
    }
}
