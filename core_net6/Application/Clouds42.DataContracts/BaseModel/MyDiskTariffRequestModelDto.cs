﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot("Request")]
    [DataContract(Name = "Request")]
    public class MyDiskTariffRequestModelDto
    {
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(TariffPlan))]
        [DataMember(Name = nameof(TariffPlan))]
        public string TariffPlan { get; set; }

    }
}