﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CloudServicesSegmentDto
    {
        [XmlElement] [DataMember] public Guid ID { get; set; }

        [XmlElement] [DataMember] public string Stable82Version { get; set; }

        [XmlElement] [DataMember] public string Alpha83Version { get; set; }

        [XmlElement] [DataMember] public string Stable83Version { get; set; }
    }
}