﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class BuyingEsdlResultModelDto
    {
        /// <summary>
        /// Тариф установлен.
        /// </summary>
        [XmlElement(ElementName = nameof(Complete))]
        [DataMember(Name = nameof(Complete))]
        public bool Complete { get; set; }

        [XmlElement(ElementName = "NeedMoney")]
        [DataMember(Name = "NeedMoney")]
        public decimal? NeedModey { get; set; }


        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }
    }
}
