﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Пара ключ-значение
    /// </summary>
    /// <typeparam name="TKey">Тип ключа</typeparam>
    public class KeyValueDto<TKey>(TKey key, string value)
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public TKey Key { get; set; } = key;

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; } = value;
    }
}
