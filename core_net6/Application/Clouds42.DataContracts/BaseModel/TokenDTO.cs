﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель токена
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class TokenDto
    {
        /// <summary>
        /// Токен
        /// </summary>
        [XmlElement(ElementName = nameof(Token))]
        [DataMember(Name = nameof(Token))]
        public string Token { get; set; }
        public TokenDto()
        {
        }

        public TokenDto(Guid? token)
        {
            Token = token.HasValue ? token.ToString() : "No token";
        }
    }
}