﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Валидация объекта
    /// </summary>
    public interface IValidateDto
    {
        /// <summary>
        /// Попытаться выполнить валидацию
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат валидации</returns>
        bool TryValidate(out string errorMessage);
    }
}
