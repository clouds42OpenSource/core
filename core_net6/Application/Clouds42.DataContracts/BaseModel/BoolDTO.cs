﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot(ElementName = nameof(Result))]
    [DataContract(Name = nameof(Result))]
    public class BoolDto
    {
        [XmlElement(ElementName = nameof(Result))]
        [DataMember(Name = nameof(Result))]

        public bool Result { get; set; }

        public BoolDto()
        {
        }

        public BoolDto(bool result)
        {
            Result = result;
        }
    }
}