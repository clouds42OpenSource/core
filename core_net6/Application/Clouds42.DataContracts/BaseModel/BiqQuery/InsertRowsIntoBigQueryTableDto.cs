﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.BaseModel.BiqQuery
{
    /// <summary>
    /// Модель записи/вставки строк
    /// в таблицу BigQuery 
    /// </summary>
    public class InsertRowsIntoBigQueryTableDto : BigQueryBaseOperationDto
    {
        /// <summary>
        /// Строки для записи/вставки
        /// </summary>
        public List<BigQueryInsertRowDto> Rows { get; set; } = [];
    }
}
