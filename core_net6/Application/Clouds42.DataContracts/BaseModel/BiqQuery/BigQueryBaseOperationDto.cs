﻿namespace Clouds42.DataContracts.BaseModel.BiqQuery
{
    /// <summary>
    /// Модель базовой операции BigQuery
    /// </summary>
    public abstract class BigQueryBaseOperationDto
    {
        /// <summary>
        /// Название таблицы
        /// </summary>
        public string TableName { get; set; }
    }
}
