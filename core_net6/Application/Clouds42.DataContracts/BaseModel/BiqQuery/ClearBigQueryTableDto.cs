﻿namespace Clouds42.DataContracts.BaseModel.BiqQuery
{
    /// <summary>
    /// Модель для очистки таблицы
    /// в БД BigQuery
    /// </summary>
    public class ClearBigQueryTableDto : BigQueryBaseOperationDto
    {
    }
}
