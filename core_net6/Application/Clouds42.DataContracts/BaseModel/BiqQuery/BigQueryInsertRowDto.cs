﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.BaseModel.BiqQuery
{
    /// <summary>
    /// Модель записи/вставки строки
    /// в таблицу BigQuery
    /// </summary>
    public class BigQueryInsertRowDto
    {
        /// <summary>
        /// Уникальный ключ вставки,
        /// идентифицирующий строку в таблице
        /// </summary>
        public string InsertKey { get; set; }

        /// <summary>
        /// Элементы записи(строки)
        /// </summary>
        public IDictionary<string, object> RowElements { get; set; }
    }
}
