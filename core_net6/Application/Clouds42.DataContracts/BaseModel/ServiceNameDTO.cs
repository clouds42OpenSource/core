﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceNameDto
    {
        [XmlElement(ElementName = "serviceNameResult")]
        [DataMember(Name = "serviceNameResult")]
        public string ServiceNameResult { get; set; }
        public ServiceNameDto(string ServiceNameResult)
        {
            this.ServiceNameResult = ServiceNameResult;
        }
        public ServiceNameDto()
        {

        }
    }
}
