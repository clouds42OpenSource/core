﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Хэлпер для выполнения сортировки
    /// </summary>
    public static class SortingHelperDto
    {
        /// <summary>
        /// Количество парамаметров метода сортировки списка
        /// </summary>
        private const int OrderByMethodParamsCount = 2;

        /// <summary>
        /// Выполнить сортировку
        /// </summary>
        /// <typeparam name="TModel">Тип входящей модели</typeparam>
        /// <param name="dataList">Список данных</param>
        /// <param name="sortingData">Модель сортировки</param>
        /// <returns>Отсортированный список</returns>
        public static IQueryable<TModel> MakeSorting<TModel>(this IQueryable<TModel> dataList, SortingDataDto sortingData) where TModel : class
        {
            var propertyInfo = GetPropertyInfoByName(typeof(TModel), sortingData?.FieldName);
            if (propertyInfo == null)
                return dataList;

            var orderExpression = GetOrderExpression(typeof(TModel), propertyInfo);
            var orderByMethod = GetOrderByMethod(sortingData);
            if (orderByMethod == null)
                return dataList;

            var genericMethod = orderByMethod.MakeGenericMethod(typeof(TModel), propertyInfo.PropertyType);
            return (IQueryable<TModel>)genericMethod.Invoke(null, [dataList, orderExpression]);
        }


        /// <summary>
        /// Попытаться выполнить сортировку или отсортировать по дефолтному полю
        /// </summary>
        /// <typeparam name="TModel">Тип входящей модели</typeparam>        
        /// <param name="dataList">Список данных</param>
        /// <param name="trySorting">Модель сортировки</param>
        /// <param name="defaultSorting">Дефолтная модель сортировки</param>
        /// <returns>Отсортированный список</returns>
        public static IQueryable<TModel> MakeSortOrDefault<TModel>(
            this IQueryable<TModel> dataList,
            SortingDataDto trySorting,
            SortingDataDto defaultSorting) where TModel : class
        {
            if (defaultSorting == null) throw new ArgumentNullException(nameof(defaultSorting));
            var propertyInfo = trySorting?.FieldName != null ? GetPropertyInfoByName(typeof(TModel), trySorting.FieldName) : null;
            if (propertyInfo == null)
                return dataList.MakeSorting(defaultSorting);

            var orderExpression = GetOrderExpression(typeof(TModel), propertyInfo);
            var orderByMethod = GetOrderByMethod(trySorting);
            var genericMethod = orderByMethod.MakeGenericMethod(typeof(TModel), propertyInfo.PropertyType);
            return (IQueryable<TModel>)genericMethod.Invoke(null, [dataList, orderExpression]);
        }

        /// <summary>
        /// Получить информацию о свойстве объекта по названию
        /// </summary>
        /// <param name="objectType">Тип объекта</param>
        /// <param name="fieldName">Название свойства</param>
        /// <returns>>Информация о свойстве объекта</returns>
        private static PropertyInfo GetPropertyInfoByName(Type objectType, string fieldName)
        {
            var properties = objectType.GetProperties();
            var matchedProperty =
                properties.FirstOrDefault(p => p.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
            return matchedProperty;
        }

        /// <summary>
        /// Получить лямбда выражения для сортировки списка
        /// </summary>
        /// <param name="objectType">Тип объекта</param>
        /// <param name="propertyInfo">Информация о свойстве объекта</param>
        /// <returns>Лямбда выражения для сортировки списка</returns>
        private static LambdaExpression GetOrderExpression(Type objectType, PropertyInfo propertyInfo)
        {
            var paramExpr = Expression.Parameter(objectType);
            var propAccess = Expression.PropertyOrField(paramExpr, propertyInfo.Name);
            var expr = Expression.Lambda(propAccess, paramExpr);
            return expr;
        }

        /// <summary>
        /// Получить метод для выполнения сортировки списка
        /// </summary>
        /// <param name="sortingData">Модель сортировки</param>
        /// <returns>Метод для выполнения сортировки списка</returns>
        private static MethodInfo GetOrderByMethod(SortingDataDto sortingData)
        {
            var orderByMethodName = sortingData.SortKind == SortType.Asc
                ? nameof(Queryable.OrderBy)
                : nameof(Queryable.OrderByDescending);

            return typeof(Queryable).GetMethods()
                .FirstOrDefault(
                    m => m.Name == orderByMethodName && m.GetParameters().Length == OrderByMethodParamsCount);
        }
    }
}
