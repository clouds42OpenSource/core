﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель списка
    /// </summary>
    public class GenericListItemDto<TItem>
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = "Type")] [IgnoreDataMember]
        public string type  = nameof(List);

        /// <summary>
        /// Список элементов
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<TItem> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public GenericListItemDto()
        {
            List = [];
        }
    }
}
