﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.Extensions;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Расширения для работы с моделью Dto "Пара ключ-значение"
    /// </summary>
    public static class KeyValueDtoExtensions
    {
        /// <summary>
        /// Сформировать список моделей "Пара ключ-значение"
        /// по перечислению(Enum)
        /// </summary>
        /// <param name="enumType">Тип перечисления(Enum)</param>
        /// <param name="ignoreElements">Игнорируемые значения</param>
        /// <returns>Список моделей "Пара ключ-значение"</returns>
        public static List<KeyValueDto<int>> GenerateKeyValueDtoList(this Type enumType, params System.Enum[] ignoreElements)
        {
            if (!enumType.IsEnum)
                throw new InvalidCastException("Ожидается значение Enum");

            var values = System.Enum.GetValues(enumType);

            var items = new List<KeyValueDto<int>>();

            foreach (var value in values)
            {
                if (ignoreElements != null && ignoreElements.Contains(value))
                    continue;

                items.Add(new KeyValueDto<int>((int) value, EnumExtensions.GetEnumDescription(value)));
            }

            return items;
        }
    }
}