﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель описывающая количество сущностей
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CountDto
    {
        /// <summary>
        /// Количество
        /// </summary>
        [XmlElement(ElementName = nameof(Count))]
        [DataMember(Name = nameof(Count))]
        public int Count { get; set; }
        public CountDto()
        {
        }

        public CountDto(int count)
        {
            Count = count;
        }
    }
}