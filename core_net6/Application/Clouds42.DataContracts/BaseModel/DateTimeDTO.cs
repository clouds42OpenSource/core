﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeDto
    {
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date { get; set; }
    }
}
