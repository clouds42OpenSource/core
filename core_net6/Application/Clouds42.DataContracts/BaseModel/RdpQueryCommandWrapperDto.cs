﻿using System;
using System.Xml.Linq;

namespace Clouds42.DataContracts.BaseModel
{
    public class RdpSessionParams
    {        
        public int UserRdpSessionID { get; set; }
        public string SessionName { get; set; }
        public string UserName { get; set; }
        public string SessionStatus { get; set; }
        public DateTime Downtime { get; set; }
        public DateTime EntryTime { get; set; }

        public static RdpSessionParams Get(string value)
        {
            var str = value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (str.Length != 7) return null;
            var result = new RdpSessionParams { UserName = str[0], SessionName = str[1] };
            int.TryParse(str[2], out var userId);
            result.UserRdpSessionID = userId;
            result.SessionStatus = str[3];
            DateTime.TryParse(str[4], out var idle);
            result.Downtime = idle;
            DateTime.TryParse($"{str[5]} {str[6]}", out var enter);
            result.EntryTime = enter;
            return result;
        }

        public static XElement GetAsXml(string value)
        {
            var p = Get(value);
            var result = new XElement(nameof(RdpSessionParams));
            result.Add(new XElement(nameof(UserName), p.UserName));
            result.Add(new XElement(nameof(UserRdpSessionID), p.UserRdpSessionID));
            result.Add(new XElement(nameof(EntryTime), p.EntryTime));
            result.Add(new XElement(nameof(SessionName), p.SessionName));
            result.Add(new XElement(nameof(SessionStatus), p.SessionStatus));
            result.Add(new XElement(nameof(Downtime), p.Downtime));
            return result;
        }

        public XElement GetAsXml(RdpSessionParams p)
        {
            var result = new XElement(nameof(RdpSessionParams));
            result.Add(new XElement(nameof(UserName), p.UserName));
            result.Add(new XElement(nameof(UserRdpSessionID), p.UserRdpSessionID));
            result.Add(new XElement(nameof(EntryTime), p.EntryTime));
            result.Add(new XElement(nameof(SessionName), p.SessionName));
            result.Add(new XElement(nameof(SessionStatus), p.SessionStatus));
            result.Add(new XElement(nameof(Downtime), p.Downtime));
            return result;
        }
    }

}
