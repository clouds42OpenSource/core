﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Constants;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BaseModel
{
    public class ServiceNameRepresentationDto
    {
        protected static readonly Dictionary<Clouds42Service, ServiceNameRepresentationDto> ServiceList = new()
        {
                {
                    Clouds42Service.Esdl, 
                    new ServiceNameRepresentationDto("Esdl", NomenclaturesNameConstants.Esdl)
                        {
                            Summary = $@"{NomenclaturesNameConstants.Esdl} – незаменимый инструмент, позволяющий максимально ускорить процесс ввода документов в 1С.",
                            ImageUrl = "~/Content/img/services/esdl.png",
                        }
                },                                
                {
                    Clouds42Service.MyDisk, 
                    new ServiceNameRepresentationDto(nameof(MyDisk), "Мой диск")
                        {
                            Summary = @"Сервис «Мой диск» позволяет хранить Ваши информационные базы, любые  файлы и данные",
                            ImageUrl = "~/Content/img/services/myDisk.png",
                        }
                },
                {
                    Clouds42Service.MyEnterprise, 
                    new ServiceNameRepresentationDto("Rent1C", "Аренда 1С")
                        {
                            Summary = @"Вы можете подключить своих пользователей к сервису «Аренда 1С». В рамках доступных лицензий можно переназначать своих пользователей. В момент очередного автоматического продления сервиса неиспользованные лицензии удаляются",
                            ImageUrl = "~/Content/img/services/1c_my_enterprise.png",
                        }
                }
            };

        private ServiceNameRepresentationDto(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Summary { get; set; }
        public string ImageUrl { get; set; }

        public static ServiceNameRepresentationDto GetService(Clouds42Service service)
        {
            return ServiceList[service];
        }
        
        public static ServiceNameRepresentationDto GetService(string service) {
            Clouds42Service ser = (Clouds42Service)Enum.Parse(typeof(Clouds42Service), service);
            return ServiceList[ser];
        }
        
    }
}
