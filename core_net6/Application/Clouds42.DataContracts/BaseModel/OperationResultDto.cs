﻿namespace Clouds42.DataContracts.BaseModel
{
    public class OperationResultDto(bool operationSuccess, string operationMessage = null)
    {
        public bool OperationSuccess { get; set; } = operationSuccess;
        public string OperationMessage { get; set; } = operationMessage;
    }

    public class OperationResult<TOut>(bool operationSuccess, TOut model, string operationMessage = null)
        : OperationResultDto(operationSuccess, operationMessage)
    {
        public TOut Model { get; set; } = model;
    }
}
