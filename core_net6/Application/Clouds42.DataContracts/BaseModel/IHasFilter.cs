﻿using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Interface with filter property
    /// </summary>
    public interface IHasFilter<TFilter> where TFilter : class, IQueryFilter
    {
        /// <summary>
        /// Filter property
        /// </summary>
        public TFilter Filter { get; set; }
    }
}
