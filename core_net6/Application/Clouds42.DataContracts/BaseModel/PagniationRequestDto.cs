﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Базовая модель запроса для пагинированной выборки записей
    /// </summary>
    /// <typeparam name="TFilter">Модель фильтрации записей</typeparam>
    public class PagniationRequestDto<TFilter> where TFilter : class, new()
    {
        /// <summary>
        /// Какую страницу выбрать
        /// </summary>
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Количество записей на странице
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Фильтр отбора
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Информация о сортировке
        /// </summary>
        public SortingDataDto SortingData { get; set; }
    }
}
