﻿using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Сортировка
    /// </summary>
    public interface ISortingDto
    {
        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        SortType? SortType { get; set; }
    }
}
