﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.BaseModel
{
    public class PaginationBaseDto
    {
        public int CurrentPage { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }


        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public int FirstNumber { get; set; }
        public int LastNumber { get; set; }
        public int PageCount { get; set; }

        private int _extra;
        private readonly int _num = 2;


        public PaginationBaseDto() { }
        public PaginationBaseDto(int page, int count, int size)
        {
            Page = page;
            TotalCount = count;
            Size = size;
            PageSize = GetPageSize(size, count);
            PageCount = GetPageCount();
            CurrentPage = GetCurrentPage(page);
            FirstNumber = GetFirstNumber(page);
            LastNumber = GetLastNumber(page);
        }
        

        #region PrivateMethods
        private int GetPageSize(int size, int count)
        {
            if (size == 0)
            {
                return count;
            }
            else
            {
                return size;
            }
        }
        private int GetPageCount()
        {
            if (PageSize != 0 && TotalCount % PageSize == 0)
            {
                return (TotalCount / PageSize);
            }
            else if (PageSize != 0)
            {
                return (TotalCount / PageSize) + 1;
            }
            else return 0;
        }
        private int GetCurrentPage(int page)
        {
            if (page > PageCount)
            {
                return 1;
            }
            else
            {
                return page;
            }
        }
        private int GetFirstNumber(int page)
        {
            int fn;
            if (CurrentPage <= _num)
            {
                fn = 1;
                _extra = _num - page + 1;
            }
            else if (PageCount - CurrentPage < _num)
            {
                fn = page - _num - _num + (PageCount - CurrentPage);

            }
            else
            {
                fn = page - _num;
            }
            if (fn < 1)
            {
                fn = 1;
            }
            return fn;
        }
        private int GetLastNumber(int page)
        {
            int ln;
            if (page + 3 >= PageCount)
            {
                ln = PageCount;
            }
            else
            {
                if (_extra > PageCount - CurrentPage)
                {
                    _extra = PageCount - CurrentPage;
                }
                ln = page + _num + _extra;
            }

            if (ln > PageCount)
            {
                ln = PageCount;
            }
            return ln;
        }
        #endregion
    }

    public class PagitationCollection<TItem>
    {
        public PagitationCollection() { }
        public PagitationCollection(List<TItem> collection, PaginationBaseDto paginationData)
        {
            PaginationData = paginationData;
            Collection = collection;
        }
        public PaginationBaseDto PaginationData { get; set; }
        public List<TItem> Collection { get; set; }
    }
}