﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class EmptyResultDto
    {
    }
}