﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Файл облака
    /// </summary>
    public class FileDataDto<TContent>
    {
        /// <summary>
        /// Название файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Контент файла
        /// </summary>
        public TContent Content { get; set; }

        /// <summary>
        /// Формат файла
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Строка base 64
        /// </summary>
        public string Base64 { get; set; }
    }
}
