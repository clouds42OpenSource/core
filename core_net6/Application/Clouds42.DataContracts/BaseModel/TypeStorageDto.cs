﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.BaseModel
{
    public enum TypeStorage
    {
        [Display(Name = "Все типы хранилищ")] [Description("Все типы хранилищ")]
        AllDatabases,

        [Display(Name = "Файловые")] [Description("Файловые хранилища")]
        FileDatabases,

        [Display(Name = "Серверные")] [Description("Серверные хранилища")]
        ServDatabases
    }
}
