﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary></summary>
    public class IntListItemDto
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = nameof(List);

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<int> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IntListItemDto()
        {
            List = [];
        }
    }
}
