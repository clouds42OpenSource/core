﻿using PagedList.Core;

namespace Clouds42.DataContracts.BaseModel
{
    public class PagedListVmDto<TViewModel, TFilter>
        where TFilter : class, new()
    {
        public IPagedList<TViewModel> PagedList { get; set; }

        public TFilter Filter { get; set; }

        public int TotalCount => PagedList?.TotalItemCount ?? 0;
        public int PageSize => PagedList?.PageSize ?? 0;
    }

    public class PagedListFilter : IPagedListFilter
    {
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public interface IPagedListFilter
    {
        int CurrentPage { get; set; }

        int PageSize { get; set; }
    }
}