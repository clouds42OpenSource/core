﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Хелпер для валидации моделей
    /// </summary>
    public static class ValidateModelHelperDto
    {
        /// <summary>
        /// Сформировать сообщение об обязательном поле
        /// </summary>
        /// <param name="fieldName">Название поля</param>
        /// <returns>Сообщение об обязательном поле</returns>
        public static string GenerateErrorMessageAboutRequiredField(string fieldName) =>
            $"Заполните поле '{fieldName}'";

        /// <summary>
        /// Сформировать сообщение
        /// о максимальном количестве символов поля
        /// </summary>
        /// <param name="fieldName">Название поля</param>
        /// <param name="maxLengt">Максимальная длина</param>
        /// <returns>Сообщение о максимальном количестве символов поля</returns>
        public static string GenerateErrorMessageAboutMaxLengtField(string fieldName, int maxLengt) =>
            $"Максимальная длина поля '{fieldName}' - {maxLengt}";
    }
}