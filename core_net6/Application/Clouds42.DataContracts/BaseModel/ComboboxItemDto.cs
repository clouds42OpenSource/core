﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Тип объекта списка элементов для ComboBox
    /// </summary>
    public class ComboboxItemDto<TValue>
    {
        /// <summary>
        /// Значение элемента
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        /// Текст элемента
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Группировка элемента
        /// </summary>
        public string Group { get; set; }
    }
}