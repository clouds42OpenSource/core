﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Информация о сортировке
    /// </summary>
    public  class SortingDataDto: ICloneable
    {
        /// <summary>
        /// Вид сортировки(по возрастанию, по убыванию)
        /// </summary>
        public SortType SortKind { get; set; }
        
        /// <summary>
        /// Какое поле сортировать
        /// </summary>
        public string FieldName { get; set; }

        public object Clone()
        {
            return new SortingDataDto
            {
                FieldName = FieldName,
                SortKind = SortKind
            };
        }
    }
}
