﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель для хранения ключа, значения и группировки
    /// </summary>
    public class KeyValueGroupDto<TKey, TValue>
    {
        /// <summary>
        /// Ключ этого объекта
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        /// Значение этого объекта
        /// </summary>
        public TValue Value { get; set; }
        
        /// <summary>
        /// Группировка этого объекта
        /// </summary>
        public string Group { get; set; }
    }
}