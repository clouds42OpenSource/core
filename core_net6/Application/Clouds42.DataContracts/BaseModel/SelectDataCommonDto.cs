﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Общий класс для выбора записей
    /// </summary>
    /// <typeparam name="TFilter">Тип фильтра отбора</typeparam>
    public class SelectDataCommonDto<TFilter>
        where TFilter : class, new()
    {
        /// <summary>
        /// Фильтр отбора
        /// </summary>
        public TFilter Filter { get; set; }

        public SortingDataDto SortingData { get; set; }

        public int? PageNumber { get; set; }
    }
}
