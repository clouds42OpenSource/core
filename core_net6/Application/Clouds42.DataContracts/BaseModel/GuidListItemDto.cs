﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель списка Guid
    /// </summary>
    public class GuidListItemDto
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))]
        [IgnoreDataMember]
        public string type { get; set; }
        
        /// <summary>
        /// Список Guid
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<Guid> List { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public GuidListItemDto()
        {
            List = [];
            type = nameof(List);
}
    }
}