﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Error", Namespace = "")]
    [DataContract(Name = "Error", Namespace = "")]
    public class ErrorStructuredDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Code), Namespace = "")]
        [DataMember(Name = nameof(Code))]
        public int Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Description), Namespace = "")]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(DebugInfo), Namespace = "")]
        [DataMember(Name = nameof(DebugInfo))]
        public string DebugInfo { get;set; }

        /// <summary>
        ///   
        /// </summary>
        [XmlElement(ElementName = nameof(Errors), Namespace = "")]
        [DataMember(Name = nameof(Errors))]
        public List<KeyValuePair<string, List<string>>> Errors { get; set; }

        public override string ToString()
        {
            return "\nCode: " + Code + ",\nDescription: " + Description + ",\nDebugInfo: " + DebugInfo;
        }
    }
}
