﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Общий класс для выборанных записей
    /// </summary>
    /// <typeparam name="TResult">Тип результирующего объекта</typeparam>
    public class SelectDataResultCommonDto<TResult>
        where TResult: class, new()
    {  
        /// <summary>
       /// Список записей.
       /// </summary>
        public TResult[] Records { get; set; }

        /// <summary>
        /// Пагинация списка <see cref="Records"/>.
        /// </summary>
        public PaginationBaseDto Pagination { get; set; }
    }
}