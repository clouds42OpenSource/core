﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.BaseModel
{
    public class PaginationDataResultDto<TModel, TFilter>(
        IEnumerable<TModel> chunkDataOfPagination,
        int totalCount,
        int pageSize,
        int pageNumber,
        TFilter filter)
        : PaginationDataResultDto<TModel>(chunkDataOfPagination, totalCount, pageSize, pageNumber)
        where TFilter : class, new()
    {
        /// <summary>
        /// Данные полученны на основание фильтра.
        /// </summary>
        public TFilter Filter { get; } = filter;
    }
    
    public class PaginationDataResultDto<TModel>(
        IEnumerable<TModel> chunkDataOfPagination,
        int totalCount,
        int pageSize,
        int pageNumber)
    {
        /// <summary>
        /// Порция данных пейджера.
        /// </summary>
        public IEnumerable<TModel> ChunkDataOfPagination { get; } = chunkDataOfPagination;

        /// <summary>
        /// Общее количество всех данных паггинации.
        /// </summary>
        public int TotalCount { get; } = totalCount;

        /// <summary>
        /// Количество строк страницы.
        /// </summary>
        public int PageSize { get; } = pageSize;

        /// <summary>
        /// Номер страницы данных.
        /// </summary>
        public int PageNumber { get; } = pageNumber;
    }

}
