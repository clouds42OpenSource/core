﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Описание выполнения некоторой операции (вызов веб-апи метода)
    /// Класс необходим для передачи 1-го аргумента простого типа в ПОСТ-запрос
    /// </summary> 
    [XmlRoot("Request")]
    [DataContract(Name = "Request")]
    public class SimpleRequestModelDto<T>
    {
        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public T Value { get; set; }

    }
}