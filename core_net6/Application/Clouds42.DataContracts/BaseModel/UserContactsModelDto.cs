﻿namespace Clouds42.DataContracts.BaseModel
{
    /// <summary>
    /// Модель, для представления информации о контактах пользователя
    /// </summary>
    public class UserContactsModelDto
    {
        /// <summary>
        /// Email адрес пользователя
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
