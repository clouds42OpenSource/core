﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BaseModel

{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CountriesListResponseModelDto
    {

        /// <summary>
        /// Alpha-2 код
        /// https://en.wikipedia.org/wiki/ISO_3166-1
        /// </summary>
        [XmlElement(ElementName = nameof(Code))]
        [DataMember(Name = nameof(Code))]
        public string Code { set; get; }

        [XmlElement(ElementName = nameof(CountryName))]
        [DataMember(Name = nameof(CountryName))]
        public string CountryName { set; get; }

        [XmlElement(ElementName = nameof(CountryPhoneCode))]
        [DataMember(Name = nameof(CountryPhoneCode))]
        public string CountryPhoneCode { set; get; }
    }
}
