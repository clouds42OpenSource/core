﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels
{
    /// <summary>
    ///     Модель
    /// </summary>
    public class DelimitersDatabaseQueryDataDto : IPublishDatabaseDataDto
    {
        public Guid Id { get; set; }
        public string Caption { get; set; }
        public string DbName { get; set; }
        public string DbSourceName { get; set; }
        public string SiteName { get; set; }
        public int? Zone { get; set; }
        public Guid DbSourceId { get; set; }
    }
}
