﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels
{
    /// <summary>
    /// Краткая информация об аккаунте
    /// </summary>
    public class AccountSimpleInfoDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Заголовок аккаунта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Номер индекса
        /// </summary>
        public int IndexNumber { get; set; }
    }
}
