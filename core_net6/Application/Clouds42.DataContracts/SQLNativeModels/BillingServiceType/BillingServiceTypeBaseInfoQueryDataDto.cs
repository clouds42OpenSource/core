﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels.BillingServiceType
{
    /// <summary>
    /// Нативная модель для СКЛ запроса на получение списка услуг сервиса
    /// </summary>
    public class BillingServiceTypeBaseInfoQueryDataDto
    {
        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID родительской услуги сервиса
        /// </summary>
        public Guid? ParentServiceTypeId { get; set; }

        /// <summary>
        /// Название услуги сервиса
        /// </summary>
        public string Name { get; set; }
    }
}