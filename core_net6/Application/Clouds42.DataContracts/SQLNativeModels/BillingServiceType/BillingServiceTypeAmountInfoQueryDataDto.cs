﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels.BillingServiceType
{
    /// <summary>
    /// Нативная модель для СКЛ запроса на получение списка услуг сервиса со стоимостью для аккаунта и количеством лицензий
    /// </summary>
    public class BillingServiceTypeAmountInfoQueryDataDto
    {
        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Общая стоимость услуги сервиса
        /// </summary>
        public decimal? Amount { get; set; }

        /// <summary>
        /// Цена за услугу сервиса
        /// </summary>
        public decimal? Cost { get; set; }

        /// <summary>
        /// Количество используемых купленных лицензий на услугу
        /// </summary>
        public int? UsedLicenses { get; set; }
    }
}