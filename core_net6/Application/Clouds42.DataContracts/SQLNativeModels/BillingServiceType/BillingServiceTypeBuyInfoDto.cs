﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.SQLNativeModels.BillingServiceType
{
    /// <summary>
    /// Модель списка услуг сервиса с вычесленной суммой и лицензиями для аккаунта
    /// </summary>
    public class BillingServiceTypeBuyInfoDto
    {
        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Родитель услуги сервиса
        /// </summary>
        public IEnumerable<Guid> ParentServiceTypes { get; set; }

        /// <summary>
        /// Наследники услуги сервиса
        /// </summary>
        public IEnumerable<Guid> ChildServiceTypes { get; set; }

        /// <summary>
        /// Название услуги сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Стоимость услуги сервиса
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Цена за услугу сервиса
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Количество используемых купленных лицензий на услугу
        /// </summary>
        public int UsedLicenses { get; set; }
    }
}