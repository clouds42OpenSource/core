﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.SQLNativeModels
{
    [Serializable]
    [XmlRoot("Groups")]
    public class AccountGroupsXmlModelDto
    {
        [XmlElement(nameof(AccountUserGroup))]
        public List<int> AccountGroupsList { set; get; } = [];
        public List<AccountUserGroup> AccountUserGroupList { set; get; } 
        public AccountGroupsXmlModelDto()
        {

            AccountUserGroupList = AccountGroupsList.Cast<AccountUserGroup>().ToList();

        }
        private static readonly XmlSerializer Xs = new(typeof(AccountGroupsXmlModelDto));
        private string _xmlModel = string.Empty;

        public static AccountGroupsXmlModelDto FromEnum(List<AccountUserGroup> groups)
        {
            const string str = "<Groups>{0}</Groups>";
            var list = string.Join("", groups.Select(x => $"<AccountUserGroup>{(int)x}</AccountUserGroup>"));
            var xmlModel = string.Format(str, list);
            var newModel = (AccountGroupsXmlModelDto)Xs.Deserialize(new StringReader(xmlModel));
            newModel._xmlModel = xmlModel;
            return newModel;
        }

        public override string ToString()
        {
            return _xmlModel;
        }
    }
}
