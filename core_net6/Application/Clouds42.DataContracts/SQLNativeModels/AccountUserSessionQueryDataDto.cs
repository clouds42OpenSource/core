﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.SQLNativeModels
{
    /// <summary>
    /// Класс, представляющий результаты запроса данных по заданному токену для пользователя или службы.
    /// </summary>
    /// <remarks>
    /// Класс имеет незначительные отличия от UserPrincipal.
    /// </remarks>
    public class AccountUserSessionQueryDataDto
    {
        public bool IsCloudService { set; get; }
        public bool IsUser { set; get; }
        public Guid? SessionId { set; get; }
        public string Name { set; get; }
        public Guid Id { set; get; }
        public Guid RequestAccountId { set; get; }
        public Guid Token { set; get; }
        public string UserSource { set; get; }
        
        public DateTime? TokenCreationTime { get; set; }

        private AccountGroupsXmlModelDto _accountGroupsXml = new();

        public string AccountGroups
        {
            set
            {
                _accountGroupsXml = AccountGroupsXmlModelDto.FromEnum(Groups);
            }
            get
            {
                return _accountGroupsXml.ToString();
            }
        }

        [NotMapped]
        public List<AccountUserGroup> Groups { get; set; }

    }
}
