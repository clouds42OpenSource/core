﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels
{
    public class AccountUserSessionInfoQueryDataDto
    {

        /// <summary>
        /// Идентификатор сессии.
        /// </summary>
        public Guid? SessionId { get; set; }

        /// <summary>
        /// Идентификатор аккаунта.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата создания токена.
        /// </summary>
        public DateTime? TokenCreationTime { get; set; }
        
        /// <summary>
        /// Служебный токен.
        /// </summary>
        public bool IsService { get; set; }

    }
}