﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels
{
    public class AccountInfoQueryDataDto
    {
        /// <summary>
        /// Идентификатор аккаунта.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название аккаунта.
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Идентификатор рефферала.
        /// </summary>
        public Guid? ReferralAccountId { get; set; }

        /// <summary>
        /// Дата регистрации.
        /// </summary>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        public int IndexNumber { get; set; }

        /// <summary>
        /// Признак удаленности.
        /// </summary>
        public bool? Removed { get; set; }

        /// <summary>
        /// Описание аккаунта.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ИНН.
        /// </summary>
        public string Inn { get; set; }


        /// <summary>
        /// Идентификатор админа аккаунта.
        /// </summary>
        public Guid? AccountAdminId { get; set; }

        /// <summary>
        /// Валюта аккаунта.
        /// </summary>
        public string AccountCurrency { get; set; }

        /// <summary>
        /// Идентификатор локали аккаунта.
        /// </summary>
        public Guid? LocaleId { get; set; }

    }
}