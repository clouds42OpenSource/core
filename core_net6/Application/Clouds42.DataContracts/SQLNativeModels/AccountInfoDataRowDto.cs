﻿using System;

namespace Clouds42.DataContracts.SQLNativeModels
{
    /// <summary>
    /// Информация об аккаунте
    /// </summary>
    public class AccountInfoDataRowDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Заголовок аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Дата регистрации аккаунта
        /// </summary>
        public DateTime? AccountRegistrationDate { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public int? AccountUsersCount { get; set; }

        /// <summary>
        /// Номер индекса
        /// </summary>
        public int IndexNumber { get; set; }

        /// <summary>
        /// Флаг, показывающий является ли аккаунт VIP
        /// </summary>
        public bool? IsVip { get; set; }

        /// <summary>
        /// Id Админ аккаунта
        /// </summary>
        public Guid? AccountAdminId { get; set; }

        /// <summary>
        ///Название Админ аккаунта
        /// </summary>
        public string AccountAdminName { get; set; }

        /// <summary>
        /// Почта Админ аккаунта
        /// </summary>
        public string AccountAdminEmail { get; set; }

        /// <summary>
        /// Логин Админ аккаунта
        /// </summary>
        public string AccountAdminLogin { get; set; }

        /// <summary>
        /// Номер телефона Админ аккаунта
        /// </summary>
        public string AccountAdminPhoneNumber { get; set; }

        /// <summary>
        /// Истечение срока действия
        /// </summary>
        public DateTime? Rent1CExpiredDate { get; set; }
    }
}
