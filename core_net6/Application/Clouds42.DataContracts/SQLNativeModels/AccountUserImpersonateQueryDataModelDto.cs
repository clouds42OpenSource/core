﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.SQLNativeModels
{
    public class AccountUserImpersonateQueryDataModelDto
    {
        public Guid AccountUserId { get; set; }
        public Guid AccountId { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Guid? CorpUserID { get; set; }
        public string CorpUserSyncStatus { get; set; }
        public bool? AccountUserRemoved { get; set; }
        public DateTime? AccountUserCreationDate { get; set; }

        public string AccountCaption { get; set; }
        public Guid? ReferralAccountId { get; set; }
        public DateTime? AccountRegistrationDate { get; set; }
        public int AccountIndexNumber { get; set; }
        public bool? AccountRemoved { get; set; }
        public string AccountDescription { get; set; }
        public string AccountInn { get; set; }
        public Guid? AccountLocaleId { get; set; }
        public string AccountCurrency { get; set; }
        public decimal AccountBalance { get; set; }

        public Guid SegmentId { get; set; }

        public string SegmentStable82Version { get; set; }

        public string SegmentAlpha83Version { get; set; }

        public string SegmentStable83Version { get; set; }

        private AccountGroupsXmlModelDto _accountGroupsXml { set; get; } = new();

        public List<AccountUserGroup> Groups => _accountGroupsXml.AccountGroupsList.Cast<AccountUserGroup>().ToList();

    }
}
