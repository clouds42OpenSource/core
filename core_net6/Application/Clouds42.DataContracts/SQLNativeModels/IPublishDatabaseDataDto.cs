﻿namespace Clouds42.DataContracts.SQLNativeModels
{
    public interface IPublishDatabaseDataDto
    {
        string SiteName { get; }
        string DbSourceName { get; }
        int? Zone { get; }
    }
}