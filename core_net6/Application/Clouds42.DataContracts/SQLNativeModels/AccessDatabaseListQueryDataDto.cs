﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.SQLNativeModels
{
    /// <summary>
    /// Модель свойств информационной базы.
    /// </summary>    
    [Serializable]
    public class AccessDatabaseListQueryDataDto : IPublishDatabaseDataDto
    {
        public Guid Id { set; get; }
        public Guid AccountId { set; get; }
        public DateTime CreationDate { set; get; }
        public DateTime LastActivityDate { set; get; }
        public string Caption { set; get; }
        public string ApplicationName { set; get; }
        public string PublishState { get; set; }
        public string DbName { set; get; }
        public int DbNumber { set; get; }
        public string State { set; get; }
        public string ConfigurationName { set; get; }
        public string ConfigurationVersion { set; get; }
        public string SqlName { set; get; }
        public string LockedState { set; get; }
        public bool? IsFile { set; get; }
        public int SizeInMb { set; get; }
        public Guid? TemplateId { set; get; }
        public string ServiceName { set; get; }
        public int? LaunchType { set; get; }
        public bool? IsLock { set; get; }
        public string AccountCaption { set; get; }
        public bool IsDemoExpired { set; get; }
        public string LaunchParameters { set; get; }
        public string ConnectionAddress { set; get; }
        public int IndexNumber { set; get; }
        public string V82Server { set; get; }
        public string Platform { set; get; }
        public string ConnectionAddressV82 { set; get; }
        public string ConnectionAddressV83 { set; get; }
        public string SegmentAlpha83Version { set; get; }
        public string SegmentStable83Version { set; get; }
        public string SegmentStable82Version { set; get; }
        public string PublishSiteName { set; get; }
        public bool GroupByAccount { set; get; }
        public int? Zone { set; get; }
        public bool? IsDemo { set; get; }
        public Guid? DelimiterDatabaseId { set; get; }
        public string DelimiterPlatformVersion { get; set; }
        public string SourceDatabaseClusterName { set; get; }
        public string DbSourceName { get; set; }
        public string SiteName { get; set; }
        public string DemoSiteName { get; set; }
        public string LinkServicePath { get; set; }
        public string DistributionType { get; set; }
        public Guid AccountUserId { get; set; }
        public bool DelimiterDatabaseMustUseWebService { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public List<string> RolesJhoArray { get; set; } = [];
        public string RolesJhoString { get => string.Join(";", RolesJhoArray);}
    }
}
