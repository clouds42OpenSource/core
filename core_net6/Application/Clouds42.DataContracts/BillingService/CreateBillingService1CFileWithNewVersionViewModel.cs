﻿using System;
using Clouds42.DataContracts.BillingService.BillingService1C;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель создания новой версии файла разработки 1C
    /// </summary>
    public class CreateBillingService1CFileWithNewVersionViewModel<TContent> : Service1CFileDto<TContent>
    {
        public CreateBillingService1CFileWithNewVersionViewModel()
        {
            Configurations1C = [];
        }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Новая версия файла разработки 1C
        /// </summary>
        public CreateNewService1CFileVersionViewModel NewService1CFileVersion { get; set; }

        /// <summary>
        /// Выполнить маппинг к модели Dto
        /// </summary>
        /// <returns>Модель создания файла разработки 1С с новой версией</returns>
        public CreateBillingService1CFileWithNewVersionDto<TContent> MapToBillingService1CFileWithNewVersionDto() =>
            new()
            {
                ServiceId = ServiceId,
                Name = Name,
                AccountId = AccountId,
                FileName = FileName,
                Content = Content,
                ContentType = ContentType,
                Configurations1C = Configurations1C,
                Base64 = Base64,
                Synonym = Synonym,
                NewService1CFileVersion = new CreateNewService1CFileVersionDto
                {
                    Version1CFile = NewService1CFileVersion.Version1CFile,
                    CommentForModerator = NewService1CFileVersion.CommentForModerator,
                    DescriptionOf1CFileChanges = NewService1CFileVersion.DescriptionOf1CFileChanges,
                    NeedNotifyAboutVersionChanges = NewService1CFileVersion.NeedNotifyAboutVersionChanges
                }
            };
    }
}