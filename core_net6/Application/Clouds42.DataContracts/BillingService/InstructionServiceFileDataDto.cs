﻿using System;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Инструкция сервиса
    /// </summary>
    public class InstructionServiceFileDataDto<TContent> : FileDataDto<TContent>
    {
        /// <summary>
        /// Файл облака
        /// </summary>
        public Guid CloudFileId { get; set; }
    }
}
