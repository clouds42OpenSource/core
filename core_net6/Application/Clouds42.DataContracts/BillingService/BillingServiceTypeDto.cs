﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Услуга сервиса
    /// </summary>
    public class BillingServiceTypeDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ключ услуги
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Тип работы биллинга.
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal ServiceTypeCost { get; set; }

        /// <summary>
        /// Услуга от которой зависит сервис
        /// </summary>
        public Guid? DependServiceTypeId { get; set; }

        /// <summary>
        /// Связи услуги
        /// </summary>
        public List<Guid> ServiceTypeRelations { get; set; } = [];

        /// <summary>
        /// Тип подключения к информационной базе
        /// </summary>
        public ConnectionToDatabaseTypeEnum ConnectionToDatabaseType { get; set; }

        /// <summary>
        /// Новая услуга
        /// </summary>
        public bool IsNew { get; set; }
    }
}
