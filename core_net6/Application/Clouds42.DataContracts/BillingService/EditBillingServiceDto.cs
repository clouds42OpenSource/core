﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель редактирования сервиса
    /// </summary>
    public class EditBillingServiceDto : BaseOperationBillingServiceDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

    }
}
