﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель отображения редактирования сервиса
    /// </summary>
    public class EditBillingServiceViewModel
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ключ сервиса
        /// </summary>
        public Guid Key { get; set; }


        /// <summary>
        /// Id заявки на модерацию
        /// </summary>
        public Guid CreateServiceRequestId { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Услуги сервиса
        /// </summary>
        public List<BillingServiceTypeDto> BillingServiceTypes { get; set; } = [];

        /// <summary>
        /// признак гибридного сервиса
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// Привести модель отображения к Dto
        /// </summary>
        /// <returns>Модель редактирования сервиса</returns>
        public EditBillingServiceDto MapToDtoModel()
            => new()
            {
                Id = Id,
                Name = Name,
                AccountId = AccountId,
                Key = Key,
                BillingServiceStatus = BillingServiceStatus,
                BillingServiceTypes = BillingServiceTypes,
                CreateServiceRequestId = CreateServiceRequestId,
                IsHybridService = IsHybridService,
            };
    }
}
