﻿using System;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель отрасли
    /// </summary>
    public class IndustryDto : IIndustry
    {
        /// <summary>
        /// Id отрасли
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название отрасли
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Описание отрасли
        /// </summary>
        public string Description { get; set; }
    }
}
