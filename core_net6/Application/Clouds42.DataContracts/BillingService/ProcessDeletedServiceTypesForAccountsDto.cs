﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель обработки удаленных услуг для аккаунтов
    /// </summary>
    public class ProcessDeletedServiceTypesForAccountsDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Список Id услуг сервиса
        /// </summary>
        public List<Guid> ServiceTypeIds { get; set; } = [];

        /// <summary>
        /// Id изменений сервиса
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }
    }
}
