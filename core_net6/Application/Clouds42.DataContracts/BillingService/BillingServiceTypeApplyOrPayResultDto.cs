﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Результат операции применения\оплаты
    /// </summary>
    public class BillingServiceTypeApplyOrPayResultDto : CheckAbilityToPayForServiceTypeResultDto
    {
        /// <summary>
        /// Список платежей
        /// </summary>
        public List<Guid> PaymentIds { get; set; } = [];
    }
}