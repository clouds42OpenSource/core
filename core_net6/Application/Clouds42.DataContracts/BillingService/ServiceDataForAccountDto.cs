﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель данных сервиса для аккаунта
    /// </summary>
    public class ServiceDataForAccountDto
    {
        /// <summary>
        /// Сервис
        /// </summary>
        public Domain.DataModels.billing.BillingService Service { get; set; }

        /// <summary>
        /// Ресурс конфигурация кастомного сервиса
        /// </summary>
        public ResourcesConfiguration ServiceResourcesConfiguration { get; set; }

        /// <summary>
        /// Ресурс конфигурация сервиса Аренда 1C
        /// </summary>
        public ResourcesConfiguration Rent1CResourcesConfiguration { get; set; }

        /// <summary>
        /// Список ресурсов
        /// </summary>
        public List<Resource> Resources { get; set; }

        /// <summary>
        /// Список Id информационных баз
        /// </summary>
        public List<Guid> DatabasesIds { get; set; }

    }
}
