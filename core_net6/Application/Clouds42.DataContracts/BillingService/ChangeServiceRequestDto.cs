﻿using System;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Заявка изменения сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequestDto
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public ChangeRequestStatusEnum Status { get; set; }

        /// <summary>
        /// Тип заявки на модерацию
        /// </summary>
        public ChangeRequestTypeEnum ChangeRequestType { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Текущие данные сервиса
        /// </summary>
        public BillingServiceCardDto CurrentServiceData { get; set; }

        /// <summary>
        /// Новые данные сервиса
        /// </summary>
        public BillingServiceChangesDto NewServiceData { get; set; }
    }
}
