﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель спонсирования для услуги сервиса
    /// </summary>
    public class AccountUserBillingServiceTypeSponsorshipDto
    {
        /// <summary>
        /// Я спонсирую
        /// </summary>
        public bool I { get; set; }

        /// <summary>
        /// Меня спонсируют
        /// </summary>
        public bool Me { get; set; }

        /// <summary>
        /// Метка (Название аккаунта)
        /// </summary>
        public string Label { get; set; }
    }
}