﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель результата проверки возможности оплаты за услугу сервиса
    /// </summary>
    public class CheckAbilityToPayForServiceTypeResultDto
    {
        /// <summary>
        /// Операция успешно выполнена
        /// </summary>
        public bool IsComplete { get; set; }

        /// <summary>
        /// Сумма нехватки денег на счету
        /// </summary>
        public decimal NotEnoughMoney { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Возможность сделать ОП
        /// </summary>
        public bool CanUsePromisePayment { get; set; }
    }
}
