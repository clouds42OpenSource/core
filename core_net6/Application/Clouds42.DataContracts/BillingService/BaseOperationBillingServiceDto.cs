﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель базовой операции над сервисом
    /// </summary>
    public class BaseOperationBillingServiceDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ключ сервиса
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Id заявки на модерацию
        /// </summary>
        public Guid CreateServiceRequestId { get; set; }

        /// <summary>
        /// признак гибридного сервиса
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Услуги сервиса
        /// </summary>
        public List<BillingServiceTypeDto> BillingServiceTypes { get; set; }

    }
}
