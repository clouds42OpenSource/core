﻿using System;
using Clouds42.Domain.Enums.Billing.BillingService;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Результат модерации заявки
    /// на изменения сервиса
    /// </summary>
    public class ChangeServiceRequestModerationResultDto
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        [JsonProperty ("ServiceRequestId")]
        public Guid Id { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public ChangeRequestStatusEnum Status { get; set; }
    }
}
