﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Услуга сервиса
    /// </summary>
    public class BillingServiceTypeInfoDto
    {
        /// <summary>
        /// ID услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Родитель услуги сервиса
        /// </summary>
        public List<Guid> ParentServiceTypes { get; set; }

        /// <summary>
        /// Наследники услуги сервиса
        /// </summary>
        public List<Guid> ChildServiceTypes { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Общая стоимость услуги сервиса
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Валюта сервиса
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Цена услуги сервиса
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Цена одной лицензии
        /// </summary>
        public decimal CostPerOneLicense { get; set; }

        /// <summary>
        /// Количество используемых лицензий
        /// </summary>
        public int UsedLicenses { get; set; }

        /// <summary>
        /// Количество спонсирских лицензий (Я спонсирую)
        /// </summary>
        public int IamSponsorLicenses { get; set; }

        /// <summary>
        /// Количество спонсирских лицензий (Меня спонсируют)
        /// </summary>
        public int MeSponsorLicenses { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Тип системной услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Имя сервиса биллинга
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Тим системного сервиса облака
        /// </summary>
        public Clouds42Service? Clouds42Service { get; set; }

        /// <summary>
        /// Is service active
        /// </summary>
        public bool IsActive { get; set; }
    }
}
