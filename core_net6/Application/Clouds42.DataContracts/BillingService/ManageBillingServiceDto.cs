﻿using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель управления сервисом 
    /// </summary>
    public class ManageBillingServiceDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Внутренний облачный сервис
        /// </summary>
        public InternalCloudServiceEnum? InternalCloudService { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }
    }
}
