﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель для переключения
    /// </summary>
    public class CalculateBillingServiceTypeDto
    {
        /// <summary>
        /// ID субъекта (Аккаунт\Пользователь)
        /// </summary>
        public Guid Subject { get; set; }

        /// <summary>
        /// ID услуги
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }

        /// <summary>
        /// Выбранный статус
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Данные о спонсировании
        /// </summary>
        public AccountUserBillingServiceTypeSponsorshipDto Sponsorship { get; set; } = new();
    }
}