﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель карточки сервиса биллинга
    /// </summary>
    public class BillingServiceCardDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ключ сервиса
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Услуги сервиса
        /// </summary>
        public List<BillingServiceTypeDto> BillingServiceTypes { get; set; } = [];

        /// <summary>
        /// Внутренний облачный сервис
        /// </summary>
        public InternalCloudServiceEnum? InternalCloudService { get; set; }

        /// <summary>
        /// Реферальная ссылка для маркета
        /// </summary>
        public string ReferralLinkForMarket { get; set; }

        /// <summary>
        /// Cсылка на активацию демо периода
        /// </summary>
        public string LinkToActivateDemoPeriod { get; set; }

        /// <summary>
        /// Наличие заявок на модерации
        /// </summary>
        public bool AvailabilityChangeServiceRequestOnModeration { get; set; }

        /// <summary>
        /// Флаг показывающий, что существует заявка на изменение/создание сервиса биллинга
        /// </summary>
        public bool IsExistsChangeServiceRequest { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Сообщение о том что сервис заблокирован
        /// </summary>
        public string MessageStatingThatServiceIsBlocked { get; set; }

        /// <summary>
        /// Дата следующего доступного
        /// изменения стоимости сервиса
        /// </summary>
        public DateTime? NextPossibleEditServiceCostDate { get; set; }

        /// <summary>
        /// Флаг показывающий можно ли редактировать стоимость сервиса
        /// </summary>
        public bool CanEditServiceCost => NextPossibleEditServiceCostDate == null ||
                                          NextPossibleEditServiceCostDate <= DateTime.Now;

        /// <summary>
        /// Количество месяцев до следующего редактирования
        /// </summary>
        public int BeforeNextEditServiceCostMonthsCount { get; set; }

        /// <summary>
        /// Количество месяцев до удаление услуг сервиса для аккаунта
        /// </summary>
        public int BeforeDeletingServiceTypesMonthsCount { get; set; }
        
        /// <summary>
        /// Электронная почта владельца сервиса
        /// </summary>
        public string ServiceOwnerEmail { get; set; }

        /// <summary>
        /// признак гибридного сервиса
        /// </summary>
        public bool IsHybridService { get; set; }

    }
}
