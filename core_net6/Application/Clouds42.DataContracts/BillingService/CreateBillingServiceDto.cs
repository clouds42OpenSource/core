﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель создания сервиса
    /// </summary>
    public class CreateBillingServiceDto : BaseOperationBillingServiceDto
    {
        public CreateBillingServiceDto()
        {
            BillingServiceTypes = [];
        }
    }
}
