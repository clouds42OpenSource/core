﻿using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Расширения для сервиса биллинга
    /// </summary>
    public static class BillingServiceExtensionDto
    {
        /// <summary>
        /// Признак возможности устанавливать расширение сервиса
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <returns>Признак возможности устанавливать расширение сервиса</returns>
        public static bool CanInstallServiceExtension(this Domain.DataModels.billing.BillingService billingService)
            => billingService.InternalCloudService is not InternalCloudServiceEnum.Sauri;
    }
}
