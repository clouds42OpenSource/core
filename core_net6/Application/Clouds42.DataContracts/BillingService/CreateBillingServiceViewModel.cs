﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель отображения создания сервиса
    /// </summary>
    public class CreateBillingServiceViewModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ключ сервиса
        /// </summary>
        public Guid Key { get; set; }

        /// <summary>
        /// Признак означающий что сервис - гибридный
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// Id заявки на модерацию
        /// </summary>
        public Guid? CreateServiceRequestId { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Услуги сервиса
        /// </summary>
        public List<BillingServiceTypeDto> BillingServiceTypes { get; set; } = [];

        /// <summary>
        /// Привести модель отображения к Dto
        /// </summary>
        /// <returns>Модель создания сервиса</returns>
        public CreateBillingServiceDto MapToDtoModel()
            => new()
            {
                Name = Name,
                AccountId = AccountId,
                Key = Key,
                BillingServiceStatus = BillingServiceStatus,
                BillingServiceTypes = BillingServiceTypes,
                CreateServiceRequestId = CreateServiceRequestId ?? Guid.NewGuid(),
                IsHybridService = IsHybridService
            };
    }
}
