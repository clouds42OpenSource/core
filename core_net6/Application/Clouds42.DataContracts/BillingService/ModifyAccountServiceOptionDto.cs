﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    public class ModifyAccountServiceOptionDto
    {
        /// <summary>
        /// ID опции сервиса
        /// </summary>
        public Guid ServiceOptionId { get; set; }
     
        /// <summary>
        /// ID связки с сервисом опции, ID аккаунта или ID пользователя
        /// </summary>
        public Guid Subject { get; set; }

        /// <summary>
        /// Данные о спонсировании
        /// </summary>
        public AccountUserBillingServiceTypeSponsorshipDto Sponsorship { get; set; }

        /// <summary>
        /// Статус опции сервиса, true - включить, false - отключить
        /// </summary>
        public bool IsEnabled { get; set; }
    }
}
