﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Данные сервиса биллинга для аккаунта
    /// </summary>
    public class AccountBillingServiceDataDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Состояние активности сервиса для аккаунта
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        public Clouds42Service? SystemService { get; set; }
    }
}