﻿namespace Clouds42.DataContracts.BillingService
{

    /// <summary>
    /// Результат потдтверждения подписки демо сервиса.
    /// </summary>
    public class ApplyDemoServiceSubscribeResultDto
    {

        /// <summary>
        /// Операция успешно завершена.
        /// </summary>
        public bool Complete { get; set; }

        /// <summary>
        /// Требуемая сумма для возможности принять подписку.
        /// </summary>
        public decimal? NeedMoney { get; set; }

        /// <summary>
        /// Есть возможность использвать обещанный платеж.
        /// </summary>
        public bool? CanUsePromisePayment { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string ErrorMessage { get; set; }
    }

}
