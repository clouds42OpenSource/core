﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель для открытия оригинальной иконки сервиса
    /// </summary>
    public class OpenOriginalIconServiceDto
    {
        public OpenOriginalIconServiceDto(string linkToOpenIcon)
        {
            LinkToOpenIcon = linkToOpenIcon;
            CanOpenIcon = true;
        }

        public OpenOriginalIconServiceDto()
        {
            CanOpenIcon = false;
        }

        /// <summary>
        /// Ссылка на открытие иконки
        /// </summary>
        public string LinkToOpenIcon { get; }

        /// <summary>
        /// Флаг, указывающий на возможность открыть иконку
        /// </summary>
        public bool CanOpenIcon { get; }
    }
}
