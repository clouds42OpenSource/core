﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    ///  Связь Id услуги с ключом подключения
    /// </summary>
    public class ServiceTypeIdOfKeyDependencyDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Ключ подключения
        /// </summary>
        public Guid Key { get; set; }
    }
}
