﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Данные для аудита версии файла разработки 1С
    /// </summary>
    public class Service1CFileVersionAuditDataDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id версии
        /// </summary>
        public Guid VersionId { get; set; }

        /// <summary>
        /// Id владельца сервиса
        /// </summary>
        public Guid ServiceOwnerId { get; set; }

        /// <summary>
        /// Id файла разработки 1С
        /// </summary>
        public Guid Service1CFileId { get; set; }

        /// <summary>
        /// Id расширения сервиса
        /// </summary>
        public Guid ServiceExtentionId { get; set; }

        /// <summary>
        /// Версия сервиса
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Конфигурациями на разделителях файла 1С
        /// </summary>
        public IEnumerable<string> Service1CFileDbTemplates { get; set; } = new List<string>();

        /// <summary>
        /// Название файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Название файла разработки
        /// </summary>
        public string Service1CFileName { get; set; }

        /// <summary>
        /// Синоним файла разработки
        /// </summary>
        public string Synonym { get; set; }

        /// <summary>
        /// Комментарий для проверяющего
        /// </summary>
        public string CommentForModerator { get; set; }
    }
}
