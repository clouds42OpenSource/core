﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Метаданные файла разработки 1С
    /// </summary>
    public class Service1CFileMetadataDto
    {
        /// <summary>
        /// Название файла разработки 1С
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Синоним файла разработки 1С
        /// </summary>
        public string Synonym { get; set; }

        /// <summary>
        /// Версия файла разработки 1С
        /// </summary>
        public string Version { get; set; }
    }
}
