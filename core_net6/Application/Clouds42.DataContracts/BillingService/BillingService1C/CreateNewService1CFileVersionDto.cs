﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель создания новой версии файла разработки 1C
    /// </summary>
    public class CreateNewService1CFileVersionDto : CreateService1CFileVersionDto
    {
        /// <summary>
        /// Описание изменений файла 1С
        /// </summary>
        public string DescriptionOf1CFileChanges { get; set; }
        
        /// <summary>
        /// Уведомить пользователей сервиса об изменениях
        /// </summary>
        public bool NeedNotifyAboutVersionChanges { get; set; }
    }
}
