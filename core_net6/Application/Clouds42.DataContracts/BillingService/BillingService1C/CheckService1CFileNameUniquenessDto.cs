﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель для проверки уникальности имени файла разработки
    /// </summary>
    public class CheckService1CFileNameUniquenessDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Название файла разработки 1С
        /// </summary>
        public string Name { get; set; }
    }
}
