﻿using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель файла 1С
    /// </summary>
    public class Service1CFileDto<TContent> : FileDataDto<TContent>
    {
        /// <summary>
        /// Названия файла разработки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Синоним файла разработки
        /// </summary>
        public string Synonym { get; set; }

        /// <summary>
        /// Конфигурации 1С
        /// </summary>
        public List<string> Configurations1C { get; set; }

    }
}
