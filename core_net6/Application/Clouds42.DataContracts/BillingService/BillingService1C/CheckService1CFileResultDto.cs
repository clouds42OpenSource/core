﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Результат проверки файла разработки 1С
    /// </summary>
    public class CheckService1CFileResultDto
    {
        /// <summary>
        /// Результат проверки успешен
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
