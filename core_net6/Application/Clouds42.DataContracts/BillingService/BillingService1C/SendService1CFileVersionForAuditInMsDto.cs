﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель для отправки версии файла разработки на аудит в МС
    /// </summary>
    public class SendService1CFileVersionForAuditInMsDto
    {

        /// <summary>
        /// Id владельца сервиса
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id загруженного файла в МС
        /// </summary>
        [JsonProperty("file-id")]
        public Guid FileId { get; set; }

        /// <summary>
        /// Id расширения сервиса
        /// </summary>
        [JsonProperty("extension-id")]
        public Guid ServiceExtentionId { get; set; }

        /// <summary>
        /// Версия сервиса
        /// </summary>
        [JsonProperty("version")]
        public string Version { get; set; }

        /// <summary>
        /// Список конфигураций на разделителях
        /// </summary>
        [JsonProperty("configurations")]
        public List<string> ConfigCompatibility { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [JsonProperty("full-name")]
        public string Service1CFileName { get; set; }

        /// <summary>
        /// Комментарий для проверяющего
        /// </summary>
        [JsonProperty("comment")]
        public string CommentForModerator { get; set; }

        /// <summary>
        /// ID пользователя, инициировавшего отправку файла
        /// </summary>
        [JsonProperty("user-id")]
        public Guid InitiatorUserId { get; set; }
    }
}
