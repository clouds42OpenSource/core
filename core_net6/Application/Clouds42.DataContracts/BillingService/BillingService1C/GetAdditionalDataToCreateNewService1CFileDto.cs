﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель для получения дополнительных данных
    /// для создания нового файла разработки 1С
    /// </summary>
    public class GetAdditionalDataToCreateNewService1CFileDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Название разработки файла
        /// </summary>
        public string Service1CFileName { get; set; }

        /// <summary>
        /// Версия файла разработки
        /// </summary>
        public string FileVersion { get; set; }
    }
}
