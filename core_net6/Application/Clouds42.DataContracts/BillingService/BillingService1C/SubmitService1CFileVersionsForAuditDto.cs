﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель для отправки версии файла разработки 1C на аудит
    /// </summary>
    public class SubmitService1CFileVersionForAuditDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id версии файла
        /// </summary>
        public Guid Service1CFileVersionId { get; set; }

        /// <summary>
        /// ID пользователя, иницировавшего загрузку
        /// </summary>
        public Guid InitiatorUserId { get; set; }
    }
}
