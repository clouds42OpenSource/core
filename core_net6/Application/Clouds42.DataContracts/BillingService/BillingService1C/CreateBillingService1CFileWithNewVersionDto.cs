﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель создания файла разработки 1С с новой версией
    /// (обновление предыдущего файла)
    /// </summary>
    public class CreateBillingService1CFileWithNewVersionDto<TContent> : Service1CFileDto<TContent>
    {
        public CreateBillingService1CFileWithNewVersionDto()
        {
            Configurations1C = [];
        }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Новая версия файла разработки 1C
        /// </summary>
        public CreateNewService1CFileVersionDto NewService1CFileVersion { get; set; }
    }
}
