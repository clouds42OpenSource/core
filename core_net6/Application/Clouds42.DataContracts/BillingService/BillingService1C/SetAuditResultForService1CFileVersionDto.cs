﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель для установки результата аудита
    /// версии файла разработки сервиса биллинга
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetAuditResultForService1CFileVersionDto
    {
        /// <summary>
        /// Id версии файла
        /// </summary>
        [XmlElement(ElementName = nameof(FileVersionId))]
        [DataMember(Name = nameof(FileVersionId))]
        public Guid FileVersionId { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        [XmlElement(ElementName = nameof(ModeratorComment))]
        [DataMember(Name = nameof(ModeratorComment))]
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Результат аудита файла 1С
        /// </summary>
        [XmlElement(ElementName = nameof(AuditResult))]
        [DataMember(Name = nameof(AuditResult))]
        public AuditService1CFileResultEnum AuditResult { get; set; }
    }
}
