﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Параметры задачи для уведомления
    /// о результате аудита версии файла разработки 1С
    /// </summary>
    public class NotifyAboutService1CFileVersionAuditResultJobParamsDto
    {
        /// <summary>
        /// Id версии файла разработки 1С
        /// </summary>
        public Guid Service1CFileVersionId { get; set; }
    }
}
