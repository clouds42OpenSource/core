﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель данных файлов разработок 1С
    /// </summary>
    public class Service1CFilesDataDto
    {
        /// <summary>
        /// Файлы разработок 1С
        /// </summary>
        public List<Service1CFileDataDto<IFormFile>> Service1CFiles { get; set; } = [];

        /// <summary>
        /// Флаг показывающий можно ли добавить
        /// новую версию файла разработки 1С
        /// </summary>
        public bool CanCreateNewService1CFileVersion { get; set; }
    }
}
