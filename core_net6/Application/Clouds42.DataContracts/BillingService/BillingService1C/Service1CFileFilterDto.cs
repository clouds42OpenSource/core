﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель фильтра данных файлов разработки 1С
    /// </summary>
    public class Service1CFileFilterDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Результат аудита файла 1С
        /// </summary>
        public AuditService1CFileResultEnum? AuditResult { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }
    }
}
