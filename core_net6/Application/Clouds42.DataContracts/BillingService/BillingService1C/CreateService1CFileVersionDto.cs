﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель создания версии файла 1С
    /// </summary>
    public class CreateService1CFileVersionDto 
    {
        /// <summary>
        /// Версия 1С файла
        /// </summary>
        public string Version1CFile { get; set; }

        /// <summary>
        /// Комментарий для модератора
        /// </summary>
        public string CommentForModerator { get; set; }

    }
}
