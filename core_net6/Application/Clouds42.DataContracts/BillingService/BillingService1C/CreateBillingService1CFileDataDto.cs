﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель создания 1С файл сервиса
    /// </summary>
    /// <typeparam name="TContent">Контент файла</typeparam>
    public class CreateBillingService1CFileDataDto<TContent> : Service1CFileDto<TContent>
    {
        public CreateBillingService1CFileDataDto()
        {
            Configurations1C = [];
        }

        /// <summary>
        /// Версия файла разработки
        /// </summary>
        public CreateService1CFileVersionDto Service1CFileVersionDto { get; set; }
    }
}
