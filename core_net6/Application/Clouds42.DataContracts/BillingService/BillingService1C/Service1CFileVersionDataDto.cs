﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Модель данных версии файла разработки 1С
    /// </summary>
    public class Service1CFileVersionDataDto : CreateNewService1CFileVersionDto
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Результат аудита файла 1С
        /// </summary>
        public AuditService1CFileResultEnum AuditResult { get; set; }

        /// <summary>
        /// Версия файла не используется.
        /// true, если файл опубликован
        /// и есть опубликованный файл с версией выше
        /// </summary>
        public bool IsNotUsedFileVersion { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Версия файла находится в процессе отката
        /// на предыдущую(более раннюю) версию
        /// </summary>
        public bool IsFileVersionInRollback { get; set; }
    }
}
