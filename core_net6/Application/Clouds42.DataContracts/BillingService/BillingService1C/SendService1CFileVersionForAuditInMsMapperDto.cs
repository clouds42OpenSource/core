﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Маппер модели для отправки версии файла разработки на аудит в МС
    /// </summary>
    public static class SendService1CFileVersionForAuditInMsMapperDto
    {
        /// <summary>
        /// Выполнить маппинг модели
        /// </summary>
        /// <param name="model">Данные для аудита версии файла разработки 1С</param>
        /// <param name="uploaded1CFileId">Id загруженного файла в МС</param>
        /// <param name="initatorUserId">ID пользователя-инициатора</param>
        /// <returns>Модель для отправки версии файла разработки на аудит в МС</returns>
        public static SendService1CFileVersionForAuditInMsDto PerformMapping(
            this Service1CFileVersionAuditDataDto model, Guid uploaded1CFileId, Guid initatorUserId) =>
            new()
            {
                AccountId = model.ServiceOwnerId,
                Service1CFileName = model.Service1CFileName,
                Version = model.Version,
                FileId = uploaded1CFileId,
                ServiceExtentionId = model.ServiceExtentionId,
                ConfigCompatibility = GenerateConfigCompatibility(model.Service1CFileDbTemplates),
                CommentForModerator = model.CommentForModerator,
                InitiatorUserId = initatorUserId
            };

        /// <summary>
        /// Сформировать строку конфигураций на разделителях через запятую
        /// </summary>
        /// <param name="service1CFileDbTemplates">Конфигурациями на разделителях файла 1С</param>
        /// <returns>Конфигурации на разделителях через запятую</returns>
        private static List<string> GenerateConfigCompatibility(IEnumerable<string> service1CFileDbTemplates)
        {
            var elements = service1CFileDbTemplates.ToArray();

            if (!elements.Any())
                return [];

            return [..elements];
        }
    }
}
