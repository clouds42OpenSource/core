﻿namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Входящая модель процесса для получения метаданных 
    /// </summary>
    public class IncomingModelOfMetadataGettingProcessDto
    {
        /// <summary>
        /// Файл считанный в байты
        /// </summary>
        public byte[] File { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        public string FileName { get; set; }
    }
}
