﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingService1C
{
    /// <summary>
    /// Данные файла разработки 1С сервиса
    /// </summary>
    public class Service1CFileDataDto<TContent> : Service1CFileDto<TContent>
    {
        public Service1CFileDataDto()
        {
            Configurations1C = [];
        }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Модель данных версии файла разработки 1С
        /// </summary>
        public Service1CFileVersionDataDto Service1CFileVersionData { get; set; }
    }
}
