﻿using System.Collections.Generic;
namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель отложенного удаления услуг сервиса для аккаунтов
    /// </summary>
    public class DelayedServiceTypesDeletionForAccountsDto : ProcessDeletedServiceTypesForAccountsDto
    {
        public DelayedServiceTypesDeletionForAccountsDto()
        {
            ServiceTypeIds = [];
            AccountsForSetAvailabilityDateTime = [];
        }

        /// <summary>
        /// Список аккаунтов для установки времени, по отложенному удалению
        /// </summary>
        public List<AccountWithDelayedDateOfRemoveServiceTypesDto> AccountsForSetAvailabilityDateTime { get; set; }
    }
}
