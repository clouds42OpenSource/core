﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель аккаунта который использует удаленную услугу
    /// </summary>
    public class AccountWhichUsesDeletedServiceTypesDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата окончания аренды
        /// </summary>
        public DateTime Rent1CExpireDate { get; set; }

        /// <summary>
        /// Необходимо отложенное удаление
        /// </summary>
        public bool NeedDelayedDeletion { get; set; }
    }
}
