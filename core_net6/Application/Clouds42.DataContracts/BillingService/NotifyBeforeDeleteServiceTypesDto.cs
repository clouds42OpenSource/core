﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель уведомления о скором удалении услуг сервиса
    /// </summary>
    public class NotifyBeforeDeleteServiceTypesDto
    {
        /// <summary>
        /// Id изменений сервиса
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Админ аккаунта
        /// владельца сервиса
        /// </summary>
        public IAccountUser AccountAdminOwnerOfService { get; set; }

        /// <summary>
        /// Названия услуг которые будут удалены
        /// </summary>
        public List<string> ServiceTypeNames { get; set; } = [];

        /// <summary>
        /// Ссылка для открытия страницы сервиса
        /// </summary>
        public string UrlForOpenBillingServicePage { get; set; }

        /// <summary>
        /// Дата удаления услуги(отложенная)
        /// </summary>
        public DateTime DelayedDateForDeletingServiceTypes { get; set; }
    }
}
