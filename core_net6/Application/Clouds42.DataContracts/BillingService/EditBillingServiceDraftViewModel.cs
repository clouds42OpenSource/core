﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель редактирования черновика
    /// </summary>
    public class EditBillingServiceDraftViewModel : EditBillingServiceViewModel
    {
        public EditBillingServiceDraftViewModel()
        {
            BillingServiceTypes = [];
        }

        /// <summary>
        /// Привести модель отображения к Dto
        /// </summary>
        /// <returns>Модель редактирования черновика</returns>
        public EditBillingServiceDraftDto MapToDto()
            => new()
            {
                Id = Id,
                Name = Name,
                AccountId = AccountId,
                Key = Key,
                BillingServiceStatus = BillingServiceStatus,
                BillingServiceTypes = BillingServiceTypes
            };
    }
}
