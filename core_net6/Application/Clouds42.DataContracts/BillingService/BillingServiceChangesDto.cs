﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BaseModel;
using Microsoft.AspNetCore.Http;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель изменений по сервису биллинга
    /// </summary>
    public class BillingServiceChangesDto
    {
        /// <summary>
        /// Название сервиса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// признак гибридного сервиса
        /// </summary>
        public bool IsHybridService { get; set; }

        /// <summary>
        /// Отрасли
        /// </summary>
        public List<Guid> Industries { get; set; }

        /// <summary>
        /// Иконка сервиса
        /// </summary>
        public FileDataDto<IFormFile> IconService { get; set; }

        /// <summary>
        /// Инструкция сервиса
        /// </summary>
        public InstructionServiceFileDataDto<IFormFile> InstructionService { get; set; }

        /// <summary>
        /// Скриншоты сервиса
        /// </summary>
        public List<ScreenshotFileDataDto<IFormFile>> BillingServiceScreenshots { get; set; }

        /// <summary>
        /// Список изменений по услугам сервиса
        /// </summary>
        public List<BillingServiceTypeChangeDto> BillingServiceTypes { get; set; }
    }
}
