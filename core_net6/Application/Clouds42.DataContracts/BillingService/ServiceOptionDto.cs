﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Опция сервиса
    /// </summary>
    public class ServiceOptionDto
    {
        /// <summary>
        /// ID опции сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Зависимость от другой услуги
        /// </summary>
        public Guid? DependServiceTypeId { get; set; }
    }
}
