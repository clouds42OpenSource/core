﻿using System;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Файл скриншота
    /// </summary>
    public class ScreenshotFileDataDto<TContent> : FileDataDto<TContent>
    {
        /// <summary>
        /// Файл облака
        /// </summary>
        public Guid? CloudFileId { get; set; }

        /// <summary>
        /// Номер скриншота
        /// </summary>
        public int Order { get; set; }
    }
}
