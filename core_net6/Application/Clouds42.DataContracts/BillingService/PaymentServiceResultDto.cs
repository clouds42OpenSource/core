﻿namespace Clouds42.DataContracts.BillingService
{

    /// <summary>
    /// Результат покупки сервиса.
    /// </summary>
    public class PaymentServiceResultDto
    {
        /// <summary>
        /// Операция успешно завершена.
        /// </summary>
        public bool Complete { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Сумма нехватки денег на счету
        /// </summary>
        public decimal NotEnoughMoney { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Возможность сделать ОП
        /// </summary>
        public bool CanUsePromisePayment { get; set; }

        /// <summary>
        /// Возможность сделать ОП
        /// </summary>
        public bool CanIncreasePromisePayment { get; set; }
    }

}
