﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Параметры активации Аренды 1С
    /// </summary>
    public class Rent1CActivationParamsDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid? AccountUserId { get; set; }

        /// <summary>
        /// Список ID сервисов биллинга
        /// </summary>
        public List<Guid> BillingServicesIdList { get; set; } = [];

        /// <summary>
        /// Тип активации аренды
        /// </summary>
        public Rent1CActivationType Rent1CActivationType { get; set; }
    }
}
