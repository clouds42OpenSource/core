﻿using Clouds42.DataContracts.BillingService.BillingService1C;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель создания новой версии файла разработки 1C
    /// </summary>
    public class CreateNewService1CFileVersionViewModel : CreateService1CFileVersionDto
    {
        /// <summary>
        /// Описание изменений файла 1С
        /// </summary>
        public string DescriptionOf1CFileChanges { get; set; }

        /// <summary>
        /// Уведомить пользователей сервиса об изменениях
        /// </summary>
        public bool NeedNotifyAboutVersionChanges { get; set; }
    }
}