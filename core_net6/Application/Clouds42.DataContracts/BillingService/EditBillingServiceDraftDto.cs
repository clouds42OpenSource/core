﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель редактирования черновика
    /// </summary>
    public class EditBillingServiceDraftDto : BaseOperationBillingServiceDto
    {

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

    }
}
