﻿using System;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель фильтра заявок сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequestsFilterDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid? ServiceId { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public ChangeRequestStatusEnum? Status { get; set; }

        /// <summary>
        /// Тип заявки на модерацию
        /// </summary>
        public ChangeRequestTypeEnum? ChangeRequestType { get; set; }

        /// <summary>
        /// Период с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;
    }
}
