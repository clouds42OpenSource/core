﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель входных параметров задачи по пересчету стоимости услуги сервиса
    /// после изменения(Пересчет стоимости ресурсов услуги и стоимости сервиса)
    /// </summary>
    public class RecalculateServiceTypeCostAfterChangeJobParamsDto
    {
        public Guid ServiceTypeId { get; set; }

        public decimal Cost { get; set; }

        public Clouds42Service? ServiceType { get; set; }

        public Guid ServiceId { get; set; }
    }
}
