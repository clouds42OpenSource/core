﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Service cost data model
    /// </summary>
    public class BillingServiceAmountDataModelDto
    {
        /// <summary>
        /// The amount of the monthly payment for the service
        /// </summary>
        public decimal ServiceAmount { get; set; }

        /// <summary>
        /// The required amount for the service to work until the end of the billing period
        /// </summary>
        public decimal? ServicePartialAmount { get; set; }

        /// <summary>
        /// Required amount for one day
        /// </summary>
        public decimal AmountForOneDay { get; set; }

        /// <summary>
        /// Currency info
        /// </summary>
        public ICurrency Currency { get; set; }
    }
}
