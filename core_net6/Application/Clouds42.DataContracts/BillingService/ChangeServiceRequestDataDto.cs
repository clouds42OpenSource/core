﻿using System;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Данные заявки сервиса на модерацию
    /// </summary>
    public class ChangeServiceRequestDataDto
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string BillingServiceName { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public DateTime StatusDateTime { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public ChangeRequestStatusEnum Status { get; set; }

        /// <summary>
        /// Комментарий модератора
        /// </summary>
        public string ModeratorComment { get; set; }

        /// <summary>
        /// Тип заявки на модерацию
        /// </summary>
        public ChangeRequestTypeEnum ChangeRequestType { get; set; }
    }
}
