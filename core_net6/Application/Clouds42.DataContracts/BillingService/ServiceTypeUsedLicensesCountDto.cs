﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель количества используемых лицензий услуги
    /// </summary>
    public class ServiceTypeUsedLicensesCountDto
    {
        /// <summary>
        /// Общая сумма
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Кол-во используемых лицензий
        /// </summary>
        public int UsedLicenses { get; set; }

        /// <summary>
        /// Кол-во лицензий которые я спонсирую
        /// </summary>
        public int IamSponsorLicenses { get; set; }

        /// <summary>
        /// Кол-во лицензий которые мне спонсируют
        /// </summary>
        public int MeSponsorLicenses { get; set; }
    }
}
