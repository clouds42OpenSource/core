﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель пересчета занимаемого места аккаунта
    /// </summary>
    public class RecalculateAccountMyDiskSizeDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
