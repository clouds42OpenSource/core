﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель аккаунта с отложенной датой удаления услуг
    /// </summary>
    public class AccountWithDelayedDateOfRemoveServiceTypesDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата удаления услуг
        /// </summary>
        public DateTime DelayedDateForDeletingServiceTypes { get; set; }
    }
}
