﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Расширения для моделей биллинга
    /// </summary>
    public static class BillingServiceDtoExtensions
    {
        /// <summary>
        /// Попробовать получить Id аккаунта
        /// </summary>
        /// <param name="billingServiceDto">Модель базовой операции над сервисом</param>
        /// <returns>Id аккаунта</returns>
        public static Guid TryGetAccountId(this BaseOperationBillingServiceDto billingServiceDto)
        {
            if (billingServiceDto.AccountId == null || billingServiceDto.AccountId == Guid.Empty)
                throw new InvalidOperationException("Ошибка. Отсутсвует Id аккаунта");

            return (Guid)billingServiceDto.AccountId;
        }
    }
}
