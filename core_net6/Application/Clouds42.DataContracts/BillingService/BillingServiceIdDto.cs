﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель ответа при создании сервиса биллинга
    /// </summary>
    public class BillingServiceIdDto
    {
        
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

    }
}
