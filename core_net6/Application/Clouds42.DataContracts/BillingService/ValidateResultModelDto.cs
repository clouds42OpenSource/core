﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель результата валидации
    /// </summary>
    public class ValidateResultModelDto
    {
        /// <summary>
        /// Успешность
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
