﻿namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Выполинть оперцию над услугой сервиса (Мои информационные базы)
    /// по конфигурации 1С
    /// </summary>
    public class PerformOperationOnServiceTypeByConfig1CDto
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Стоимость конфигурации
        /// </summary>
        public decimal ConfigurationCost { get; set; }
    }
}
