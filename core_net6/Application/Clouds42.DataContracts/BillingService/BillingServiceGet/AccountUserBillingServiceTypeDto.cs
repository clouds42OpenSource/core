﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    /// <summary>
    /// Модель пагинации. Пользователь и его активные услуги сервиса
    /// </summary>
    public class AccountUserBillingServiceTypePagedDto
    {
        /// <summary>
        /// Всего элементов
        /// </summary>
        public int TotalItemCount { get; set; }

        /// <summary>
        /// Всего страниц
        /// </summary>
        public int PageCount { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public IEnumerable<AccountUserBillingServiceTypeDto> Items { get; set; }

        /// <summary>
        /// Общее количество пользователей у аккаунта
        /// </summary>
        public int TotalCountAccountUsers { get; set; }
    }

    /// <summary>
    /// Account user with active services of a billing service dto
    /// </summary>
    public class AccountUserBillingServiceTypeDto
    {
        /// <summary>
        /// Account user identifier
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Full account user name
        /// </summary>
        public string AccountUserName { get; set; }

        /// <summary>
        /// Sponsoring model
        /// </summary>
        public AccountUserBillingServiceTypeSponsorshipDto Sponsorship { get; set; } = new();

        /// <summary>
        /// Active user's services
        /// </summary>
        public List<Guid> Services { get; set; } = [];

        /// <summary>
        /// Services sponsored by user's account
        /// </summary>
        public List<Guid> SponsoredServiceTypesByMyAccount { get; set; } = [];
    }

}
