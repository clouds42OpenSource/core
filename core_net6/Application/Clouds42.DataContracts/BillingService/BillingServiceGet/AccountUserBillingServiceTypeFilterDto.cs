﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    /// <summary>
    /// Модель фильтра для получения списка аккаунтов и активных сервисов
    /// </summary>
    public class AccountUserBillingServiceTypeFilterDto
    {
        /// <summary>
        /// ID сервиса
        /// </summary>
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Поле сортировки
        /// </summary>
        public Guid? SortRowId { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public string AccountUserName { get; set; }

        /// <summary>
        /// Номер страницы пагинатора
        /// </summary>
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Количество отображаемых записей в таблице
        /// </summary>
        public int PageSize { get; set; } = 10;
    }
}