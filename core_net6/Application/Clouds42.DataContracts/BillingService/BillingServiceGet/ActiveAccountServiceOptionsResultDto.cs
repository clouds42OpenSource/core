﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    public class ActiveAccountServiceOptionsResultDto
    {
        /// <summary>
        /// Опция сервиса
        /// </summary>
        public Guid ServiceoptionId { get; set; }

        /// <summary>
        /// ID объекта, AccountUsers.Id или Accounts.Id 
        /// </summary>
        public Guid Subject { get; set; }
    }
}
