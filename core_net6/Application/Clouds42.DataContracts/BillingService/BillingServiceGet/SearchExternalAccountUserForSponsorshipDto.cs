﻿using System;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    /// <summary>
    /// Модель поиска внешнего пользователя для спонсирования
    /// </summary>
    public class SearchExternalAccountUserForSponsorshipDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id услуги(для работы с карточкой услуги)
        /// </summary>
        public Guid? ServiceTypeId { get; set; }

        /// <summary>
        /// Электронная почта пользователя
        /// </summary>
        public string Email { get; set; }
    }
}
