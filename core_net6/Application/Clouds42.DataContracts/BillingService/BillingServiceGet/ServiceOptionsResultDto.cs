﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    public class ServiceOptionsResultDto
    {
        /// <summary>
        /// Опция сервиса
        /// </summary>
        public Guid ServiceOptionId { get; set; }

        /// <summary>
        /// Тип биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }
    }
}
