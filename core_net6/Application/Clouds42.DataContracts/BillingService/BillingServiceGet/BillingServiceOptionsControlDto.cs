﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    /// <summary>
    /// Billing service management model
    /// </summary>
    public class BillingServiceOptionsControlDto
    {
        public BillingServiceOptionsControlDto()
        {
        }

        public BillingServiceOptionsControlDto(Guid accountId, Guid billingServiceId, IEnumerable<KeyValuePair<Guid, decimal>> newCosts,
            DateTime newExpireDate)
        {
            AccountId = accountId;
            BillingServiceId = billingServiceId;
            NewCosts = newCosts.ToDictionary(x => x.Key, x => x.Value);
            NewExpireDate = newExpireDate;
        }

        /// <summary>
        /// Account identifier
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Billing service identifier
        /// </summary>
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// New costs for services of the billing service
        /// </summary>
        public Dictionary<Guid, decimal> NewCosts { get; set; }

        /// <summary>
        /// New billing service expire date
        /// </summary>
        public DateTime NewExpireDate { get; set; }

        

    }
}
