﻿using System.Collections.Generic;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.BillingService.BillingServiceGet
{
    /// <summary>
    /// Результат активации услуги сервиса
    /// </summary>
    public class CalculateBillingServiceTypeResultDto : CalculateBillingServiceTypeDto
    {
        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// Ошибки
        /// </summary>
        public IEnumerable<CalculateBillingServiceTypeErrorDto> Errors { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string ServiceTypeName { get; set; }
    }

    /// <summary>
    /// Результат активации услуги сервиса. Ошибка активации 
    /// </summary>
    public class CalculateBillingServiceTypeErrorDto
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        public CalculateBillingServiceTypeErrorCode Code { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}
