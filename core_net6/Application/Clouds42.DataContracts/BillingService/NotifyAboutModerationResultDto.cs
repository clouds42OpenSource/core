﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель для уведомления о результате модерации сервиса
    /// </summary>
    public class NotifyAboutModerationResultDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Результат модерации
        /// </summary>
        public ChangeServiceRequestModerationResultDto ModerationResult { get; set; }

        /// <summary>
        /// Ссылка для открытия карточки сервиса
        /// </summary>
        public string UrlForOpenBillingServiceCard { get; set; }
    }
}
