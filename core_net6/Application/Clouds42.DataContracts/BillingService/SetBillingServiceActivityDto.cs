﻿using System;

namespace Clouds42.DataContracts.BillingService
{
    /// <summary>
    /// Модель для установки активности сервиса биллинга
    /// </summary>
    public class SetBillingServiceActivityDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Флаг активности сервиса
        /// </summary>
        public bool IsActive { get; set; }
    }
}
