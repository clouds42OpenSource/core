﻿namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель результата после обновления или добавления конфигурации 1С
    /// </summary>
    public sealed class AddOrUpdateConfiguration1CResultDto
    {
        /// <summary>
        /// Полные пути к архиву с информацией по обновлениям конфигурации.
        /// </summary>
        public ConfigurationRedactionModelDto[] Reductions { get; set; }
    }
}