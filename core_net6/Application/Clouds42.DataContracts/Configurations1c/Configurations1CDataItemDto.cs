﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель свойств элемента конфигурации 1С 
    /// </summary>
    public sealed class Configurations1CDataItemDto : IConfigurations1C
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Каталог конфигурации на ИТС
        /// </summary>
        public string ConfigurationCatalog { get; set; }

        /// <summary>
        /// Редакция
        /// </summary>
        public string RedactionCatalogs { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public string PlatformCatalog { get; set; }

        /// <summary>
        /// Адрес каталога обновлений.
        /// </summary>
        public string UpdateCatalogUrl { get; set; }

        /// <summary>
        /// Краткий код конфигурации, например bp, zup и тд.
        /// </summary>
        public string ShortCode { get; set; }

        /// <summary>
        /// Признак что необходимо использовать COM соединения для принятия обновлений в базе
        /// </summary>
        public bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Признак необходимости проверять наличие обновлений
        /// </summary>
        public bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// ID данных авторизации в ИТС
        /// </summary>
        public Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Стоимость конфигурации
        /// </summary>
        public decimal ConfigurationCost { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1С
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];

        /// <summary>
        /// Полные пути к архиву с информацией по обновлениям конфигурации.
        /// </summary>
        public ConfigurationRedactionModelDto[] Reductions { get; set; }
    }
}