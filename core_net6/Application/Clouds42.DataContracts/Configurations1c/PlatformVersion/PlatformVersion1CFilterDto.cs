﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;


namespace Clouds42.DataContracts.Configurations1c.PlatformVersion
{
    /// <summary>
    /// Фильтер версии платформы 1С
    /// </summary>
    public class PlatformVersion1CFilterDto: ISortingDto
    {
        /// <summary>
        ///     Версия платформы
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        ///     Путь к платформе
        /// </summary>
        public string PathToPlatform { get; set; }

        /// <summary>
        ///     Путь к платформе x64
        /// </summary>
        public string PathToPlatformX64 { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
