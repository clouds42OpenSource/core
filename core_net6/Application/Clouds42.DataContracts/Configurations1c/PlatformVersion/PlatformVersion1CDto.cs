﻿namespace Clouds42.DataContracts.Configurations1c.PlatformVersion
{
    /// <summary>
    /// Версия платформы 1С
    /// </summary>
    public class PlatformVersion1CDto
    {
        /// <summary>
        ///     Версия платформы
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        ///     Путь к платформе
        /// </summary>
        public string PathToPlatform { get; set; }

        /// <summary>
        ///     Путь к платформе x64
        /// </summary>
        public string PathToPlatformX64 { get; set; }

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для MacOs
        /// </summary>
        public string MacOsThinClientDownloadLink { get; set; }

        /// <summary>
        /// Ссылка на скачивание тонкого клиента для Windows
        /// </summary>
        public string WindowsThinClientDownloadLink { get; set; }
    }
}
