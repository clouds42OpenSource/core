﻿namespace Clouds42.DataContracts.Configurations1c.PlatformVersion
{
    /// <summary>
    /// Модель версии тонкого клиента платформы 1С
    /// </summary>
    public class PlatformThinClientVersionDto
    {
        /// <summary>
        /// Версия
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Ссылка на скачивание для MacOS
        /// </summary>
        public string MacOsDownloadLink { get; set; }

        /// <summary>
        /// Ссылка на скачивание для Windows
        /// </summary>
        public string WindowsDownloadLink { get; set; }
    }
}
