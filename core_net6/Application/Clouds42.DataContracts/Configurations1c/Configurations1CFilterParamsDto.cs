﻿namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Фильтр для выбора конфигураций 1С 
    /// </summary>
    public sealed class Configurations1CFilterParamsDto
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Каталог конфигурации на ИТС
        /// </summary>
        public string ConfigurationCatalog { get; set; }
    }
}