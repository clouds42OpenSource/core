﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Configurations1c
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class Configuration1CReleaseMappingResultDto
    {
        [XmlElement(ElementName = nameof(TableReleases))]
        [DataMember(Name = nameof(TableReleases))]
        public TableRelease TableReleases { get; set; }

    }

    public class TableRelease
    {
        [XmlAttribute(nameof(Type))]
        [DataMember(Name = nameof(Type))]
        public string Type { get; set; } = "Table";

        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<Configuration1CReleaseItem> Rows { get; set; }

    }

    public class Configuration1CReleaseItem
    {
        [XmlElement(nameof(UpdateItem))]
        [DataMember(Name = nameof(UpdateItem))]
        public string UpdateItem { get; set; }

        [XmlElement(ElementName = nameof(Versions))]
        [DataMember(Name = nameof(Versions))]
        public string Versions { get; set; }

    }
}
