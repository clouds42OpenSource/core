﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель ответа на запрос получения конфигураций 1С
    /// </summary>
    public sealed class Configurations1CDataResultDto: SelectDataResultCommonDto<Configurations1CDataItemDto>
    {
    }
}