﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Configurations1c.CfuPackage
{
    [XmlRoot(Namespace = @"http://v8.1c.ru/configuration-updates", ElementName = "updateList")]
    [DataContract(Name = "updateList")]
    public class Configurations1CList
    {
        [XmlElement("update")]
        public List<CfuConfigurations> List { get; set; } = [];
    }

    public class VersionCfu
    {
        [XmlAttribute("platform")]
        public string Platform1C { get; set; }

        [XmlText]
        public string VersionValue { get; set; }
    }

    [XmlRoot(ElementName = "update")]
    [DataContract(Name = "update")]
    public class CfuConfigurations : ICfuSource
    {
        //v8u:file
        [XmlElement(ElementName = "file")]
        [DataMember(Name = "file")]
        public string FilePath { get; set; }

        [XmlElement(ElementName = "version")]
        [DataMember(Name = "version")]
        public VersionCfu VersionCfu { get; set; }

        [XmlIgnore]
        public string Version => VersionCfu.VersionValue;

        [XmlElement(ElementName = "size")]
        [DataMember(Name = "size")]
        public string Size { get; set; }

        [XmlElement("target")]
        public List<string> TargetList { get; set; }
    }

}
