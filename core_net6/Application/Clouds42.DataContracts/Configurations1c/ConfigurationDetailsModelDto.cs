﻿using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Configurations1c
{

    /// <summary>
    /// Модель конфигурации 1С с редакциями.
    /// </summary>
    public class ConfigurationDetailsModelDto
    {
        /// <summary>
        /// Детали конфигурации.
        /// </summary>
        public IConfigurations1C Configuration { get; set; }

        /// <summary>
        /// Информация о редакциях.
        /// </summary>
        public List<ConfigurationRedactionModelDto> Redactions { get; set; } = [];

        /// <summary>
        /// Вариации имени конфигурации 1C
        /// </summary>
        public List<string> ConfigurationNameVariations { get; set; } = [];
    }
}
