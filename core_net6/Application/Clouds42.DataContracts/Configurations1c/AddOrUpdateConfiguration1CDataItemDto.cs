﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель свойств элемента конфигурации 1С для обновления или добавления
    /// </summary>
    public sealed class AddOrUpdateConfiguration1CDataItemDto: IConfigurations1C
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        [Required(ErrorMessage = "Название - обязательное поле")]
        [StringLength(255, ErrorMessage = "Длина не должна превышать 255 символов")]
        public string Name { get; set; }

        /// <summary>
        /// Каталог конфигурации на ИТС
        /// </summary>
        [Required(ErrorMessage = "Каталог конфигурации обязательное поле")]
        [StringLength(255, ErrorMessage = "Длина не должна превышать 250 символов")]
        public string ConfigurationCatalog { get; set; }

        /// <summary>
        /// Редакция
        /// </summary>
        [Required(ErrorMessage = "Укажите хотя бы одну редакцию.")]
        [StringLength(250, ErrorMessage = "Длина не должна превышать 250 символов")]
        public string RedactionCatalogs { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        [Required(ErrorMessage = "Платформа обязательное поле")]
        [StringLength(3, ErrorMessage = "Длина поля 'Платформа' не должна превышать 3 символа")]
        public string PlatformCatalog { get; set; }

        /// <summary>
        /// Адрес каталога обновлений.
        /// </summary>
        [StringLength(255)]
        [Required(ErrorMessage = "Адрес каталога обновлений обязательное поле")]
        public string UpdateCatalogUrl { get; set; }

        /// <summary>
        /// Краткий код конфигурации, например bp, zup и тд.
        /// </summary>
        [StringLength(5, ErrorMessage = "Длина поля 'Код конфигурации' не должна превышать 5 символов")]
        public string ShortCode { get; set; }

        /// <summary>
        /// Признак что необходимо использовать COM соединения для принятия обновлений в базе
        /// </summary>
        public bool UseComConnectionForApplyUpdates { get; set; }

        /// <summary>
        /// Признак необходимости проверять наличие обновлений
        /// </summary>
        public bool NeedCheckUpdates { get; set; }

        /// <summary>
        /// Стоимость конфигурации
        /// </summary>
        [Required(ErrorMessage = "Стоимость конфигурации обязательное поле")]
        public decimal ConfigurationCost { get; set; }

        /// <summary>
        /// ID данных авторизации в ИТС
        /// </summary>
        public Guid ItsAuthorizationDataId { get; set; }

        /// <summary>
        /// Вариации имени конфигурации 1С
        /// </summary>
        public List<string> ConfigurationVariations { get; set; } = [];
    }
}