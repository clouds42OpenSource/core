﻿namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель редакции конйигурации.
    /// </summary>
    public class ConfigurationRedactionModelDto
    {
        /// <summary>
        /// Каталоги редакции.
        /// </summary>
        public string RedactionCatalog { get; set; }

        /// <summary>
        /// Адрес по скачиванию карты обновлений редакций.
        /// </summary>
        public string UrlOfMapping { get; set; }
    }
}