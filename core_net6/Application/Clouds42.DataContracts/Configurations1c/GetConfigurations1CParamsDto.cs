﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель выбора записей конфигураций 1С
    /// </summary>
    public sealed class GetConfigurations1CParamsDto: SelectDataCommonDto<Configurations1CFilterParamsDto>
    {
    }
}