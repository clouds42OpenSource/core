﻿namespace Clouds42.DataContracts.Configurations1c.ItsAuthorization
{
    /// <summary>
    /// Данные фильтра для получения данных авторизации ИТС
    /// </summary>
    public class ItsAuthorizationFilterDataDto
    {
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }
    }
}