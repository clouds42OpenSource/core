﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Configurations1c.ItsAuthorization
{
    /// <summary>
    /// Модель списка авторизации данных ИТС с пагинацией
    /// </summary>
    public class ItsAuthorizationDataPaginationDto : SelectDataResultCommonDto<ItsAuthorizationDataDto>
    {
    }
}