﻿
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Configurations1c.ItsAuthorization
{
    /// <summary>
    /// Модель фильтра для получения списка данных авторизации ИТС
    /// </summary>
    public class ItsAuthorizationFilterDto : SelectDataCommonDto<ItsAuthorizationFilterDataDto>
    {
    }
}