﻿using System;

namespace Clouds42.DataContracts.Configurations1c.ItsAuthorization
{
    /// <summary>
    /// Модель авторизации данных ИТС
    /// </summary>
    public class ItsAuthorizationDataDto
    {
        /// <summary>
        /// ID записи
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Логин 
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Хэш пароля
        /// </summary>
        public string PasswordHash { get; set; }
    }
}