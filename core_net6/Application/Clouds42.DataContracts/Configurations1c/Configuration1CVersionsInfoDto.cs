﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель информации о версиях релизов конфигурации 1С
    /// </summary>
    public class Configuration1CVersionsInfoDto
    {
        /// <summary>
        /// Название конфигурации 1С
        /// </summary>
        public string Configuration1CName { get; set; }

        /// <summary>
        /// Версии релизов конфигурации 1С
        /// </summary>
        public List<string> ReleasesVersions { get; set; } = [];
    }
}
