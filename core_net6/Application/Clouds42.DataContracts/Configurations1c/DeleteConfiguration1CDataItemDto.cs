﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Configurations1c
{
    /// <summary>
    /// Модель свойств элемента конфигурации 1С для удаления
    /// </summary>
    public sealed class DeleteConfiguration1CDataItemDto
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        [Required(ErrorMessage = "Название конфигурации - обязательное поле")]
        public string ConfigurationName { get; set; }
    }
}