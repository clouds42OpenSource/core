﻿using System;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Идентификатор пользователя облака.
    /// </summary>
    public interface IAccountUserIdDto
    {

        /// <summary>
        /// Идентификатор пользователя облака.
        /// </summary>
        Guid AccountUserId { get; set; }
    }
}