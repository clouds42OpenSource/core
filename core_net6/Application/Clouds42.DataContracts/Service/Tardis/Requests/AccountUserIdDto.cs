﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Идентификатор пользователя облака.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountUserIdDto : IAccountUserIdDto
    {

        /// <summary>
        /// Идентификатор пользователя облака.
        /// </summary>
        [XmlElement(ElementName = "CoreUserID")]
        [DataMember(Name = "CoreUserID")]
        public Guid AccountUserId { get; set; }

    }
}
