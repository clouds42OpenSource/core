﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Доступ пользователя к информационной базы.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDatabaseAccessDto : IAccountDatabaseAccessDto
    {

        /// <summary>
        /// Идентификатор информационной базы.
        /// </summary>
        [XmlElement(ElementName = "BaseID")]
        [DataMember(Name = "BaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Идентификатор пользователя облака.
        /// </summary>
        [XmlElement(ElementName = "CoreUserID")]
        [DataMember(Name = "CoreUserID")]
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Статус досутпа.
        /// </summary>
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public int StatusValue { get; set; }

        [XmlIgnore]
        public StatusEnum Status
        {
            get => (StatusEnum)StatusValue;
            set => StatusValue = (int)value;
        }

        /// <summary>
        /// Тип доступа.
        /// </summary>
        [XmlElement(ElementName = nameof(AccessType))]
        [DataMember(Name = nameof(AccessType))]
        public int AccessTypeValue { get; set; }

        [XmlIgnore]
        public AccessTypeEnum AccessType
        {
            get => (AccessTypeEnum)AccessTypeValue;
            set => AccessTypeValue = (int)value;
        }

        /// <summary>
        /// Тип доступа.
        /// </summary>
        public enum AccessTypeEnum
        {
            Администратор = 1,
            ВедущийБухгалтер = 2,
            ПомощникБухгалтера = 3
        }

        /// <summary>
        /// Статус досутпа.
        /// </summary>
        public enum StatusEnum
        {

            /// <summary>
            /// Выключить доступ.
            /// </summary>
            Disable = 0,

            /// <summary>
            /// Включить доступ.
            /// </summary>
            Enable = 1
        }
    }
}