﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Идентификатор информационной базы.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDatabaseIdDto : IAccountDatabaseIdDto
    {

        /// <summary>
        /// Идентификатор информационной базы.
        /// </summary>
        [XmlElement(ElementName = "BaseID")]
        [DataMember(Name = "BaseID")]
        public Guid AccountDatabaseId { get; set; }

    }
}