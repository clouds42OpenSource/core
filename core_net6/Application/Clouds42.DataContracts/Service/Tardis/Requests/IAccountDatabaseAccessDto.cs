﻿using System;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Доступ пользователя к информационной базы.
    /// </summary>
    public interface IAccountDatabaseAccessDto
    {
        /// <summary>
        /// Идентификатор информационной базы.
        /// </summary>
        Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Идентификатор пользователя облака.
        /// </summary>
        Guid AccountUserId { get; set; }

        /// <summary>
        /// Статус досутпа.
        /// </summary>
        AccountDatabaseAccessDto.StatusEnum Status { get; set; }

        /// <summary>
        /// Тип доступа.
        /// </summary>
        AccountDatabaseAccessDto.AccessTypeEnum AccessType { get; set; }
    }
}