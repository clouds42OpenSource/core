﻿using System;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{
    /// <summary>
    /// Идентификатор информационной базы.
    /// </summary>
    public interface IAccountDatabaseIdDto
    {

        /// <summary>
        /// Идентификатор информационной базы.
        /// </summary>
        Guid AccountDatabaseId { get; set; }
    }
}