﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Tardis.Requests
{

    /// <summary>
    /// Создание информационной базы на разделителях.
    /// </summary>
    public class CreateDatabaseOnDelimitersDto : IAccountDatabaseIdDto
    {
        /// <summary>
        /// Идентификатор информационной базы.
        /// </summary>
        [XmlElement(ElementName = "BaseID")]
        [DataMember(Name = "BaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Идентификатор кода конфигурации информационной базы.
        /// </summary>
        [XmlElement(ElementName = "ConfigurationID")]
        [DataMember(Name = "ConfigurationID")]
        public string ConfigurationCode { get; set; }

    }
}
