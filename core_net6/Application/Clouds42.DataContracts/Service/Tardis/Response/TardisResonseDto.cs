﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Tardis.Response
{
    /// <summary>
    /// Модель на запрос при изменение пользователя облака.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class TardisResonseDto
    {

        /// <summary>
        /// Статус ответа.
        /// </summary>
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status { get; set; }

        /// <summary>
        /// Описание ответа.
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        private const string OkValue = "ok";
        public bool IsOk => Status.Equals(OkValue, StringComparison.OrdinalIgnoreCase);

    }
}