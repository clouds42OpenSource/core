﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Service
{
    /// <summary>
    /// Demo period management model for Fasta service
    /// </summary>
    public class ManageFastaDemoPeriodDto
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Demo period end date
        /// </summary>
        [Required]
        public DateTime DemoExpiredDate { get; set; }

        /// <summary>
        /// The maximum possible end date of the demo period
        /// </summary>
        public DateTime? MaxDemoExpiredDate { get; set; }
    }
}
