﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель ответа на проверку токена
    /// </summary>
    public sealed class TokenValidationResultDto
    {
        /// <summary>
        /// Индикатор валидности токена на запрос <see cref="TokenValidationDto"/>
        /// </summary>
        [DataMember(Name = "valid")]
        [JsonProperty("valid")]
        public bool IsValid {get;set;}
    }
}
