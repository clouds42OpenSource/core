﻿using System;

namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель данных шаблона рекламного баннера
    /// </summary>
    public class AdvertisingBannerTemplateDataDto
    {
        /// <summary>
        /// Id шаблона рекламного баннера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело баннера
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Описание ссылки
        /// </summary>
        public string CaptionLink { get; set; }

        /// <summary>
        /// Показывать баннер с
        /// </summary>
        public DateTime? DisplayBannerDateFrom { get; set; }

        /// <summary>
        /// Показывать баннер по
        /// </summary>
        public DateTime? DisplayBannerDateTo { get; set; }
    }
}
