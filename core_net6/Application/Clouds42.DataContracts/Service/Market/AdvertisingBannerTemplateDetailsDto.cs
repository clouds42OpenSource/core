﻿using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель шаблона рекламного баннера
    /// </summary>
    public class AdvertisingBannerTemplateDetailsDto : EditAdvertisingBannerTemplateDto
    {
        /// <summary>
        /// Результат подсчета аудитории рекламного баннера
        /// </summary>
        public CalculationAdvertisingBannerAudienceDto CalculationAdvertisingBannerAudience { get; set; }
    }
}
