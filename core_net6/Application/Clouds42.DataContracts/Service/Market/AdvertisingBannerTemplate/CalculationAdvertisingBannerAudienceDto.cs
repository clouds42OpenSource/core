﻿namespace Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate
{
    /// <summary>
    /// Результат подсчета аудитории рекламного баннера 
    /// </summary>
    public class CalculationAdvertisingBannerAudienceDto
    {
        /// <summary>
        /// Количество аккаунтов
        /// </summary>
        public int AccountsCount { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public int AccountUsersCount { get; set; }
    }
}
