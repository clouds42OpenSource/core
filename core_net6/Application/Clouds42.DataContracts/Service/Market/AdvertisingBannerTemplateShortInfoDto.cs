﻿namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель краткой ифнормации рекламного баннера
    /// </summary>
    public class AdvertisingBannerTemplateShortInfoDto
    {
        /// <summary>
        /// Id шаблона рекламного баннера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public string Header { get; set; }
    }
}
