﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель на проверку токена
    /// </summary>
    public sealed class TokenValidationDto
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [DataMember(Name = "user")]
        [JsonProperty("user")]
        public string User {get;set;}

        /// <summary>
        /// Токен пользователя
        /// </summary>
        [DataMember(Name = "uid")]
        [JsonProperty("uid")]
        public string Token { get; set; }
    }
}
