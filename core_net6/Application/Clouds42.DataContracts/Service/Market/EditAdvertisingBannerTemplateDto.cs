﻿namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель редактирования шаблона рекламного баннера
    /// </summary>
    public class EditAdvertisingBannerTemplateDto : CreateAdvertisingBannerTemplateDto
    {
        /// <summary>
        /// Id шаблона рекламного баннера
        /// </summary>
        public int TemplateId { get; set; }
    }
}
