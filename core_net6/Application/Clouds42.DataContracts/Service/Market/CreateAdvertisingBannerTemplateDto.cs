﻿using Microsoft.AspNetCore.Http;
using System;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Market.AdvertisingBannerTemplate;

namespace Clouds42.DataContracts.Service.Market
{
    /// <summary>
    /// Модель создания шаблона рекламного баннера
    /// </summary>
    public class CreateAdvertisingBannerTemplateDto 
    {
        /// <summary>
        /// Изображение баннера
        /// </summary>
        public FileDataDto<IFormFile> Image { get; set; }

        /// <summary>
        /// Заголовок баннера
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело баннера
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Описание ссылки
        /// </summary>
        public string CaptionLink { get; set; }

        /// <summary>
        /// Ссылка баннера
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Аудитория рекламного баннера
        /// </summary>
        public AdvertisingBannerAudienceDto AdvertisingBannerAudience { get; set; }

        /// <summary>
        /// Показывать баннер с
        /// </summary>
        public DateTime? ShowFrom { get; set; }

        /// <summary>
        /// Показывать баннер по
        /// </summary>
        public DateTime? ShowTo { get; set; }

    }
}
