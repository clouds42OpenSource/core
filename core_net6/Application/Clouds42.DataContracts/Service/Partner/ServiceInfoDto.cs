﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель описывает свойства сервиса.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceInfoDto
    {

        /// <summary>
        /// Название услуги.
        /// </summary>
        [XmlElement(ElementName = nameof(Name))]
        [DataMember(Name = nameof(Name))]
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги.
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Статус сервиса.
        /// </summary>        
        [XmlElement(ElementName = nameof(BillingServiceStatus))]
        [DataMember(Name = nameof(BillingServiceStatus))]
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Дата активации сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceActivationDate))]
        [DataMember(Name = nameof(ServiceActivationDate))]
        public DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Признак указывающий что сервис отключен
        /// </summary>
        [XmlElement(ElementName = nameof(IsServiceDisabled))]
        [DataMember(Name = nameof(IsServiceDisabled))]
        public bool IsServiceDisabled { get; set; }
    }
}
