﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Информация по состоянию сервиса у клиента.
    /// </summary>
    [XmlRoot(ElementName = nameof(Result))]
    [DataContract(Name = nameof(Result))]
    public class ServiceTypeStatusForUserDto : BoolDto
    {
        /// <summary>
        /// Признак что для сервиса активен демо период
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemoPeriod))]
        [DataMember(Name = nameof(IsDemoPeriod))]
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Признак указывающий что сервис отключен
        /// </summary>
        [XmlElement(ElementName = nameof(IsServiceDisabled))]
        [DataMember(Name = nameof(IsServiceDisabled))]
        public bool IsServiceDisabled { get; set; }
    }
}
