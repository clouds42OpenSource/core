﻿using System;

namespace Clouds42.DataContracts.Service.Partner.PartnerClients
{
    /// <summary>
    /// Модель связи аккаунтов
    /// </summary>
    public class AccountsBindingDto
    {
        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Id аккаунта клиента
        /// </summary>
        public Guid ClientAccountId { get; set; }
    }
}
