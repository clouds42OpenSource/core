﻿using System;

namespace Clouds42.DataContracts.Service.Partner.PartnerClients
{
    /// <summary>
    /// Информация об аккаунте для выпадающего списка.
    /// </summary>
    public class AccountInfoDto
    {
        /// <summary>
        /// Описание аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

    }
}
