﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель Id информационных баз
    /// </summary>
    [XmlRoot(ElementName = "DatabasesIds")]
    [DataContract(Name = "DatabasesIds")]
    public class DatabasesIdsDto(List<Guid> databaseIds)
    {
        public DatabasesIdsDto() : this([])
        {
        }

        /// <summary>
        /// Тип элемента
        /// </summary>
        [XmlAttribute(AttributeName = "type")] [IgnoreDataMember]
        public string Type = "List";


        /// <summary>
        /// Список Id информационных баз
        /// </summary>
        [XmlElement(ElementName = "DatabaseId")]
        [DataMember(Name = "DatabaseId")]
        public List<Guid> Ids { get; set; } = databaseIds;
    }
}
