﻿using System;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.UpdateServiceInfoInServiceManager
{
    /// <summary>
    /// Модель параметров для обновления информации
    /// о сервисе биллинга в МС
    /// </summary>
    public class UpdateServiceInfoInMsDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        [JsonIgnore]
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание сервиса
        /// </summary>
        [JsonProperty("short-description")]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        [JsonProperty("opportunities")]
        public string Opportunities { get; set; }

        /// <summary>
        /// Минимальная стоимость сервиса
        /// </summary>
        [JsonProperty("min-cost")]
        public decimal MinimumServiceCost { get; set; }

    }
}
