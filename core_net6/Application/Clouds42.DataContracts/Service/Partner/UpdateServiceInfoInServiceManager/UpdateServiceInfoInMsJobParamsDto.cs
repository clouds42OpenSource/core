﻿using System;

namespace Clouds42.DataContracts.Service.Partner.UpdateServiceInfoInServiceManager
{
    /// <summary>
    /// Модель параметров задачи для обновления информации
    /// о сервисе биллинга в МС
    /// </summary>
    public class UpdateServiceInfoInMsJobParamsDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }
    }
}
