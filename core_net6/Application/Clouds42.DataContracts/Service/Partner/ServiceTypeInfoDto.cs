﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель описывает свойства услуги сервиса.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceTypeInfoDto
    {

        /// <summary>
        /// Название услуги.
        /// </summary>
        [XmlElement(ElementName = nameof(Name))]
        [DataMember(Name = nameof(Name))]
        public string Name { get; set; }

        /// <summary>
        /// Описание услуги.
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Тип работы биллинга.
        /// 0 - по пользователю.
        /// 1 - по аккаунту.
        /// </summary>
        [XmlElement(ElementName = nameof(BillingType))]
        [DataMember(Name = nameof(BillingType))]
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Зависимость услуги.
        /// </summary>        
        [XmlElement(ElementName = nameof(DependServiceTypeId))]
        [DataMember(Name = nameof(DependServiceTypeId))]
        public Guid? DependServiceTypeId { get; set; }

        /// <summary>
        /// Стоимость услуги сервиса.
        /// </summary>        
        [XmlElement(ElementName = nameof(Cost))]
        [DataMember(Name = nameof(Cost))]
        public decimal Cost { get; set; }

    }
}