﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;
using JetBrains.Annotations;

namespace Clouds42.DataContracts.Service.Partner.PartnerTransactions
{
    /// <summary>
    /// Модель создания агентского платежа
    /// </summary>
    [XmlRoot(ElementName = "AgencyPaymentCreation")]
    [DataContract(Name = "AgencyPaymentCreation")]
    public class AgencyPaymentCreationDto
    {
        /// <summary>
        /// Id аккаунта агента
        /// </summary>
        [XmlElement(ElementName = nameof(AgentAccountId))]
        [DataMember(Name = nameof(AgentAccountId))]
        public Guid AgentAccountId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public decimal Sum { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        [XmlElement(ElementName = nameof(PaymentType))]
        [DataMember(Name = nameof(PaymentType))]
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [CanBeNull]
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        /// <summary>
        /// Тип источника агентского платежа
        /// </summary>
        [XmlElement(ElementName = nameof(AgentPaymentSourceType))]
        [DataMember(Name = nameof(AgentPaymentSourceType))]
        public AgentPaymentSourceTypeEnum AgentPaymentSourceType { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountNumber))]
        [DataMember(Name = nameof(AccountNumber))]
        public int? AccountNumber { get; set; }

        /// <summary>
        /// Сумма клиентского платежа
        /// </summary>
        [XmlElement(ElementName = nameof(ClientPaymentSum))]
        [DataMember(Name = nameof(ClientPaymentSum))]
        public decimal? ClientPaymentSum { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        [Required]
        [StringLength(50)]
        public string RequestNumber { get; set; }
    }
}
