﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель пагинации подходящих инф. баз для расширений сервиса
    /// </summary>
    public class AvailableDatabaseForServiceExtensionPagedDto
    {
        /// <summary>
        /// Всего элементов
        /// </summary>
        public int TotalItemCount { get; set; }

        /// <summary>
        /// Всего страниц
        /// </summary>
        public int PageCount { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public IEnumerable<AvailableDatabaseForServiceExtensionDto> Items { get; set; } = new List<AvailableDatabaseForServiceExtensionDto>();
    }
}
