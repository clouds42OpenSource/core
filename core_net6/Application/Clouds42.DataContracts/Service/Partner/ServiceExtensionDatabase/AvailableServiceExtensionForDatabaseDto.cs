﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель подходящего расширения сервиса для инф. базы
    /// </summary>
    public class AvailableServiceExtensionForDatabaseDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Последняя дата активности расширения
        /// </summary>
        public DateTime? ExtensionLastActivityDate { get; set; }

        /// <summary>
        /// Состояние расширения
        /// </summary>
        public ServiceExtensionDatabaseStatusEnum? ExtensionState { get; set; }

        /// <summary>
        /// Признак что расширение установлено
        /// </summary>
        public bool IsInstalled { get; set; }

        /// <summary>
        /// Признак что сервис подключен
        /// </summary>
        public bool IsActiveService { get; set; }

        /// <summary>
        /// Признак что сервис заблокирован
        /// </summary>
        public bool IsFrozenService { get; set; }

        public string Error { get; set; }

    }
}
