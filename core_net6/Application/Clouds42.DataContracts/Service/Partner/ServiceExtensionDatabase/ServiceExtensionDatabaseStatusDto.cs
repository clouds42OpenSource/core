﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель статуса расширения сервиса для инф. базы
    /// </summary>
    public class ServiceExtensionDatabaseStatusDto
    {
        /// <summary>
        /// Cтатуса расширения сервиса
        /// </summary>
        public ServiceExtensionDatabaseStatusEnum ExtensionDatabaseStatus { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public DateTime? SetStatusDateTime { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public bool ServiceStatus { get; set; }
    }
}
