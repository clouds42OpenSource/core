﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель статуса активности сервиса
    /// </summary>
    public class ServiceExtensionActivationStatusDto
    {
        /// <summary>
        /// Статус активности
        /// </summary>
        [JsonProperty("locked")]
        public bool IsLocked { get; set; }
    }
}
