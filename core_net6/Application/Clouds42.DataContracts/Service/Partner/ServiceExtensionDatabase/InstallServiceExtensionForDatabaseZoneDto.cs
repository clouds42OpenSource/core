﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель данных для установки расширения сервиса для области инф. базы
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class InstallServiceExtensionForDatabaseZoneDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceId))]
        [DataMember(Name = nameof(ServiceId))]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        [XmlElement(ElementName = nameof(DatabaseZone))]
        [DataMember(Name = nameof(DatabaseZone))]
        public int DatabaseZone { get; set; }
    }
}
