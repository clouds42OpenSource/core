﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель подходящего расширения сервиса для инф. базы
    /// </summary>
    public class AvailableServiceExtensionForDatabaseResponseMsDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        [JsonProperty("service-id")] 
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [JsonProperty("service-name")]
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        [JsonProperty("description")]
        public string ShortDescription { get; set; }

        /// <summary>
        /// Последняя дата активности расширения
        /// </summary>
        [JsonProperty("date")]
        public DateTime? ExtensionLastActivityDate { get; set; }

        /// <summary>
        /// Состояние расширения
        /// </summary>
        [JsonProperty("state")]
        public ServiceExtensionDatabaseStatusEnum? ExtensionState { get; set; }

        /// <summary>
        /// Признак что расширение установлено
        /// </summary>
        [JsonProperty("active")]
        public bool IsInstalled { get; set; }

        /// <summary>
        /// Признак что сервис активирован
        /// </summary>
        [JsonProperty("service-active-in-market")]
        public bool IsServiceActivated { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
