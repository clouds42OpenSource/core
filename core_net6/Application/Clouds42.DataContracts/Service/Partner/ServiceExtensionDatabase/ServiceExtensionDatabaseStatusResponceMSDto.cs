﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель статуса расширения сервиса для инф. базы
    /// </summary>
    public class ServiceExtensionDatabaseStatusResponceMSDto
    {
        /// <summary>
        /// Cтатуса расширения сервиса
        /// </summary>
        [JsonProperty("state")]
        public ServiceExtensionDatabaseStatusEnum ExtensionDatabaseStatus { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        [JsonProperty("date")]
        public DateTime? SetStatusDateTime { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        [JsonProperty("active")]
        public bool ServiceStatus { get; set; }
    }
}
