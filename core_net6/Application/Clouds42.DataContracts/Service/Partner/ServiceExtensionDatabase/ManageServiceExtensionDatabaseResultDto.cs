﻿namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель результата управления состоянием расширения сервиса
    /// для информационной базы
    /// </summary>
    public class ManageServiceExtensionDatabaseResultDto
    {
        /// <summary>
        /// Признак что операция завершена успешно
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Признак необходимости переотправлять запрос
        /// </summary>
        public bool NeedRetry { get; set; }
    }
}
