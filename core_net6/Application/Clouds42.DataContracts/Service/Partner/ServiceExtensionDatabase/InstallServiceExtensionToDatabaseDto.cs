﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель данных для установки расширения сервиса для инф. базы
    /// </summary>
    public class InstallServiceExtensionToDatabaseDto
    {
        /// <summary>
        /// Id сервиса, который установить в базу
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы, куда установить сервис
        /// </summary>
        public Guid DatabaseId { get; set; }
    }
}
