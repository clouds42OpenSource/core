﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель для установки статуса активации расширения сервиса
    /// для инф. базы
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetServiceExtensionDatabaseActivationStatusDto
    {
        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        [XmlElement(ElementName = "ServiceID")]
        [DataMember(Name = "ServiceID")]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Признак активности
        /// </summary>
        [XmlElement(ElementName = nameof(IsActive))]
        [DataMember(Name = nameof(IsActive))]
        public bool IsActive { get; set; }

        /// <summary>
        /// Признак активности
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemo))]
        [DataMember(Name = nameof(IsDemo))]
        public bool IsDemo { get; set; }
    }
}
