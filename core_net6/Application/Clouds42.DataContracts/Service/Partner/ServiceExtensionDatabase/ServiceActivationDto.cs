﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель статуса активации сервиса
    /// </summary>
    public class ServiceActivationDto
    {
        /// <summary>
        /// Статус активности
        /// </summary>
        [JsonProperty("active")]
        public bool IsActive { get; set; }
    }
}
