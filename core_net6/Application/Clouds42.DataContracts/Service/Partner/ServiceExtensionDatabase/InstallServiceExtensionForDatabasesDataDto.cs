﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель данных для установки расширения сервиса для инф. баз
    /// </summary>
    public class InstallServiceExtensionForDatabasesDataDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Информационные базы для установки расширения
        /// </summary>
        public List<DatabaseForInstallExtensionDto> Databases { get; set; } = [];
    }
}
