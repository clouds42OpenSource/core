﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Информационная база для установки расширения
    /// сервиса
    /// </summary>
    public class DatabaseForInstallExtensionDto
    {
        /// <summary>
        /// Id ифнормационной базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название базы 
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Имя базы для V82
        /// </summary>
        public string V82Name { get; set; }
        
        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Последняя дата активности
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Последняя дата активности, строковой формат
        /// </summary>
        public string LastActivityDateString => LastActivityDate.ToString("D");

        /// <summary>
        /// CSS класс иконки инф. базы
        /// </summary>
        public string DatabaseImageCssClass { get; set; }
    }
}
