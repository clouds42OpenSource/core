﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Billing service installation DTO for infobases
    /// </summary>
    public class InstallServiceExtensionForDatabasesDto
    {
        /// <summary>
        /// Billing service identifier
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Infobases identifiers
        /// </summary>
        public List<Guid> DatabaseIds { get; set; } = [];
    }
}
