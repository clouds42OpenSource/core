﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель данных для установки расширения сервиса для инф. базы
    /// </summary>
    public class UninstallServiceExtensionDto
    {
        /// <summary>
        /// Id сервиса, который удалить из базы
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы, у которой удалить сервис
        /// </summary>
        public Guid DatabaseId { get; set; }
    }
}
