﻿using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель подходящей инф. базы для расширения сервиса
    /// </summary>
    public class AvailableDatabaseForServiceExtensionDto
    {
        /// <summary>
        /// Id ифнормационной базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название базы 
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Имя базы для V82
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Последняя дата активности расширения
        /// </summary>
        public DateTime? ExtensionLastActivityDate { get; set; }

        /// <summary>
        /// Состояние расширения
        /// </summary>
        public ServiceExtensionDatabaseStatusEnum? ExtensionState { get; set; }
        
        /// <summary>
        /// Статус инф. базы
        /// </summary>
        public string DatabaseStateString { get; set; }

        /// <summary>
        /// Статус инф. базы
        /// </summary>
        public DatabaseState DatabaseState => (DatabaseState) Enum.Parse(typeof(DatabaseState), DatabaseStateString);

        /// <summary>
        /// Признак что расширение установлено
        /// </summary>
        public bool IsInstalled { get; set; }

        /// <summary>
        /// CSS класс иконки инф. базы
        /// </summary>
        public string DatabaseImageCssClass { get; set; }

        /// <summary>
        /// Расширенное описание ошибки расширения
        /// </summary>
        public string Error { get; set; }
    }
}
