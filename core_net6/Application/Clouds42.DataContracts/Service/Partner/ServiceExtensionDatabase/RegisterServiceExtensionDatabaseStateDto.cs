﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель для регистрации состояния
    /// расширения сервиса для информационной базы 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RegisterServiceExtensionDatabaseStateDto
    {
        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        [XmlElement(ElementName = "ServiceID")]
        [DataMember(Name = "ServiceID")]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Результат выполненной операции
        /// </summary>
        [XmlElement(ElementName = nameof(Result))]
        [DataMember(Name = nameof(Result))]
        public bool Result { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [XmlElement(ElementName = nameof(ErrorMessage))]
        [DataMember(Name = nameof(ErrorMessage))]
        public string ErrorMessage { get; set; }
    }
}
