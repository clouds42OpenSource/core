﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    public class ActivateDemoServiceDto
    {
        public Guid ServiceId { get; set; }
        public Guid AccountDatabaseId { get; set; }
        public Guid AccountId { get; set; }
    }
}
