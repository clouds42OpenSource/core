﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель управления состоянием расширения сервиса
    /// для информационной базы
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class ManageServiceExtensionDatabaseStateDto
    {
        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        [XmlElement(ElementName = "ServiceID")]
        [DataMember(Name = "ServiceID")]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }
    }
}
