﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель удаления расширения сервиса из информационной базы
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class DeleteServiceExtensionFromDatabaseDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceId))]
        [DataMember(Name = nameof(ServiceId))]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Id базы
        /// </summary>
        [XmlElement(ElementName = nameof(DatabaseId))]
        [DataMember(Name = nameof(DatabaseId))]
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Флаг указывающий установить или удалить сервис
        /// </summary>
        [XmlElement(ElementName = nameof(Install))]
        [DataMember(Name = nameof(Install))]
        public bool Install { get; set; }
    }
}
