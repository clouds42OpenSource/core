﻿using System;
using System.Collections.Generic;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.AccountDatabase;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Информационная база для установки сервиса
    /// сервиса
    /// </summary>
    public class DatabaseForServiceExtensionDto
    {
        /// <summary>
        /// Id ифнормационной базы
        /// </summary>
        [JsonIgnore]
        public Guid AccountDatabaseID 
        {
            get
            {
                var databaseId = Guid.Empty;

                if (!string.IsNullOrEmpty(DatabaseIDString))
                    Guid.TryParse(DatabaseIDString, out databaseId);

                return databaseId.IsNullOrEmpty()
                    ? Guid.Empty
                    : databaseId;
            }
            set
            {
                DatabaseIDString = value.ToString();
            }
        }

        /// <summary>
        /// Id ифнормационной базы
        /// </summary>
        [JsonProperty (nameof(AccountDatabaseID))]
        public string DatabaseIDString { get; set; }

        /// <summary>
        /// Название базы 
        /// </summary>
        public string AccountDatabaseName { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Состояние расширения
        /// </summary>
        public ServiceExtensionDatabaseStatusEnum AccountDatabaseServiceState { get; set; }

        public string Error { get; set; }

    }

    public class DatabaseForServiceExtensionListDto
    {
        public List<DatabaseForServiceExtensionDto> Result { get; set; } = [];
    }
}
