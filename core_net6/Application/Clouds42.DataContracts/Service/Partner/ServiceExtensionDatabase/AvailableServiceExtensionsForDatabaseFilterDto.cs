﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель фильтра на получение
    /// подходящих расширений сервиса для инф. базы
    /// </summary>
    public class AvailableServiceExtensionsForDatabaseFilterDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Тип расширений сервисов для инф. базы
        /// </summary>
        public ServiceExtensionsForDatabaseTypeEnum ServiceExtensionsForDatabaseType { get; set; }
    }
}
