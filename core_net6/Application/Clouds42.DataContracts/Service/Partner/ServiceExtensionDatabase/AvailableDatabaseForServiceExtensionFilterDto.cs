﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель фильтра подходящих инф. баз для расширений сервиса
    /// </summary>
    public class AvailableDatabaseForServiceExtensionFilterDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Номер страницы пагинатора
        /// </summary>
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Количество отображаемых записей в таблице
        /// </summary>
        public int PageSize { get; set; } = 10;
    }
}
