﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель статуса активности сервиса
    /// </summary>
    public class UsersActivationStatusDto
    {
        /// <summary>
        /// Статус активности
        /// </summary>
        [JsonProperty("locked")]
        public bool IsLocked { get; set; }

        /// <summary>
        /// Список пользователей
        /// </summary>
        [JsonProperty("users")]
        public List<Guid> UserIds { get; set; }
    }
}
