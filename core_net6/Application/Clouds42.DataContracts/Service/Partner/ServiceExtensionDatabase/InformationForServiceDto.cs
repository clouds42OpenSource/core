﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель информации о сервисе
    /// </summary>
    public class InformationForServiceDto
    {
        /// <summary>
        /// Идентификатор сервиса
        /// </summary>
        public string ServiceID { get; set; }
        /// <summary>
        /// Идентификатор сервиса в МС
        /// </summary>
        public string StringID { get; set; }
        /// <summary>
        /// Имя сервиса
        /// </summary>
        public string ServiceName { get; set; }
        /// <summary>
        /// Описание сервиса
        /// </summary>
        public string ServiceShortDescription { get; set; }
        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ServiceDescription { get; set; }
        /// <summary>
        /// Стоимость сервиса
        /// </summary>
        public string ServiceCost { get; set; }

        /// <summary>
        /// Личный сервис или нет
        /// </summary>
        public bool Private { get; set; }

        /// <summary>
        /// Url иконки
        /// </summary>
        public string ServiceIcon { get; set; }

        /// <summary>
        /// Идентификатор иконки
        /// </summary>
        public string ServiceIconID { get; set; }

        /// <summary>
        /// Версия сервиса
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Url инструкции
        /// </summary>
        public string InstructionURL { get; set; }

        /// <summary>
        /// Идентификатор инструкции
        /// </summary>
        public string InstructionID { get; set; }

        /// <summary>
        /// Валюта сервиса
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Период оплаты сервиса
        /// </summary>
        public string PricePerPeriod { get; set; }

    }

    /// <summary>
    /// Ответная модель при запросе данных от мс 
    /// </summary>
    public class ResultInformationForServiceDto
    {
        /// <summary>
        /// Идентификатор конфигурации
        /// </summary>
        [JsonProperty (nameof(Result))]
        public InformationForServiceDto Result { get; set; }

    }
    
}
