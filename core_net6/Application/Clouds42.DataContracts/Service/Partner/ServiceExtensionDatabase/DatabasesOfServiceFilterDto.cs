﻿using System;

namespace Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase
{
    /// <summary>
    /// Модель фильтра на получение
    /// информационных баз с сервисом
    /// </summary>
    public class DatabasesOfServiceFilterDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID сервиса
        /// </summary>
        public Guid ServiceId{ get; set; }
    }
}
