﻿using System;
using Clouds42.DataContracts.Service.Partner.AgentCashOutRequest;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.AgentRequisites;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Заполнитель модели отчета агента
    /// </summary>
    internal static class AgentReportDtoFillerDto
    {
        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="agentReport">Модель отчета агента</param>
        internal static void Fill(this AgentReportDto agentReport, string supplierReferenceName)
        {
            agentReport.AgentRequisites = new AgentRequisites
            {
                LegalPersonRequisites = new LegalPersonRequisites
                {
                    OrganizationName = "ОАО Строй Машина"
                }
            };
            agentReport.SupplierReferenceName = supplierReferenceName;
            agentReport.ActualAgencyAgreementEffectiveDate = DateTime.Now.AddDays(-5);
            agentReport.AgentName = "ОАО Строй Машина";
            agentReport.PrefixForFullName = "Иванов И.В.";
            agentReport.AgentRewardSum = 5000;
            agentReport.PeriodFrom = DateTime.Now.AddDays(-5);
            agentReport.PeriodTo = DateTime.Now;
            agentReport.AgentPayments =
            [
                new()
                {
                    Id = Guid.NewGuid(),
                    PaymentDateTime = DateTime.Now,
                    Payment = new Payment
                    {
                        Account = new Domain.DataModels.Account { IndexNumber = 4444 }, Sum = 500
                    },
                    Sum = 800
                }
            ];
        }
    }
}
