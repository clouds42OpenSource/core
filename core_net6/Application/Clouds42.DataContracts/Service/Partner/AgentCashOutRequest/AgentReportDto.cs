﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Service.PrintedHtmlForm;
using Clouds42.Domain.DataModels.billing.AgentPayments;
using Clouds42.Domain.DataModels.billing.AgentRequisites;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель отчета агента
    /// </summary>
    public class AgentReportDto: IPrintedHtmlFormModelDto
    {
        /// <summary>
        /// Дата вступления в силу
        /// актуального агентского соглашения
        /// </summary>
        public DateTime ActualAgencyAgreementEffectiveDate { get; set; }

        /// <summary>
        /// Номер реквизитов агента
        /// </summary>
        public string AgentRequisitesNumber { get; set; }

        /// <summary>
        /// Имя агента или название компании
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// Сумма агентского вознаграждения
        /// </summary>
        public decimal AgentRewardSum { get; set; }

        /// <summary>
        /// Список агентских платежей
        /// </summary>
        public List<AgentPayment> AgentPayments { get; set; }

        /// <summary>
        /// Начальный период отчета
        /// </summary>
        public DateTime PeriodFrom { get; set; }

        /// <summary>
        /// Конечный период отчета
        /// </summary>
        public DateTime PeriodTo { get; set; } = DateTime.Now;

        /// <summary>
        /// Префикс для ФИО в отчете агента
        /// </summary>
        public string PrefixForFullName { get; set; }

        /// <summary>
        /// Реквизиты агента
        /// </summary>
        public AgentRequisites AgentRequisites { get; set; }

        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Тестовые данные</returns>
        public void FillTestData(string siteAuthorityUrl = null, string localeName = null, string supplierReferenceName = null)
        {
            this.Fill(supplierReferenceName);
        }

        /// <summary>
        /// Получить название модели
        /// </summary>
        /// <returns>Название модели</returns>
        public string GetModelName() => "Отчет агента";
        public string SupplierReferenceName { get; set; }
    }
}
