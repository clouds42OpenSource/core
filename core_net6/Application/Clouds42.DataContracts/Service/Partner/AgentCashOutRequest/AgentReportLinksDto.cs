﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Ссылки на отчет агента
    /// </summary>
    [XmlRoot(ElementName = "AgentReportLinks")]
    [DataContract(Name = "AgentReportLinks")]
    public class AgentReportLinksDto
    {
        /// <inheritdoc />
        public AgentReportLinksDto()
        {
            Links = [];
        }

        /// <summary>
        /// Тип списка
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = "List";


        /// <summary>
        /// Ссылки
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<string> Links { get; set; }
    }
}
