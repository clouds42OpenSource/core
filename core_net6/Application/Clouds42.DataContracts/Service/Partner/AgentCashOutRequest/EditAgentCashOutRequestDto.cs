﻿using System;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель редактирования заявки на вывод средств агента 
    /// </summary>
    public class EditAgentCashOutRequestDto : CreateAgentCashOutRequestDto
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        public string RequestNumber { get; set; }
    }

    /// <summary>
    /// Модель редактирования заявки на вывод средств агента 
    /// </summary>
    public class EditAgentCashOutRequestInputDto : CreateAgentCashOutRequestInputDto
    {
        /// <summary>
        /// Id заявки
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        public string RequestNumber { get; set; }
    }
}
