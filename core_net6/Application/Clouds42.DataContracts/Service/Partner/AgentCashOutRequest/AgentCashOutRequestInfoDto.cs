﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель заявки для страницы партнерки
    /// </summary>
    public class AgentCashOutRequestInfoDto
    {
        /// <summary>
        /// ID заявки
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер аккаунта партнера.
        /// </summary>
        public int PartnerAccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта партнера
        /// </summary>
        public string PartnerAccountCaption { get; set; }

        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        public string RequestNumber { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Реквизиты агента
        /// </summary>
        public string AgentRequisites
        {
            get
            {
                return LegalPersonRequisitesId.HasValue
                    ? $"{AgentRequisitesTypeEnum.LegalPersonRequisites.Description()} №{PartnerAccountIndexNumber}-{Number}":
                      PhysicalPersonRequisitesId.HasValue ? $"{AgentRequisitesTypeEnum.PhysicalPersonRequisites.Description()} №{PartnerAccountIndexNumber}-{Number}" :
                        $"{AgentRequisitesTypeEnum.SoleProprietorRequisites.Description()} №{PartnerAccountIndexNumber}-{Number}";
            }
        }
        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum RequestStatus { get; set; }
        public AgentRequisitesTypeEnum? RequisiteType { get; set; }
        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }

        /// <summary>
        /// Сумма по заявке
        /// </summary>
        public decimal RequestedSum { get; set; }

        [JsonIgnore]
        public Guid? SoleProprietorRequisitesId { get; set; }
        [JsonIgnore]
        public Guid? PhysicalPersonRequisitesId { get; set; }
        [JsonIgnore]
        public Guid? LegalPersonRequisitesId { get; set; }
        [JsonIgnore]
        public int Number { get; set; }

        public Guid? AccountUserId { get; set; }

    }
}
