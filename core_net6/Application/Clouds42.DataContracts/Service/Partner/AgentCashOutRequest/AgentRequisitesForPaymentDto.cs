﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Агентские реквизиты для выплаты
    /// </summary>
    [XmlRoot(ElementName = "AgentRequisitesForPayment")]
    [DataContract(Name = "AgentRequisitesForPayment")]
    public class AgentRequisitesForPaymentDto
    {
        /// <summary>
        /// Получатель (Наименование организации/ФИО)
        /// </summary>
        [XmlElement(ElementName = nameof(Recipient))]
        [DataMember(Name = nameof(Recipient))]
        public string Recipient { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        [XmlElement(ElementName = nameof(Inn))]
        [DataMember(Name = nameof(Inn))]
        public string Inn { get; set; }

        /// <summary>
        /// Наименование банка
        /// </summary>
        [XmlElement(ElementName = nameof(BankName))]
        [DataMember(Name = nameof(BankName))]
        public string BankName { get; set; }

        /// <summary>
        /// Расчетный счет
        /// </summary>
        [XmlElement(ElementName = nameof(SettlementAccount))]
        [DataMember(Name = nameof(SettlementAccount))]
        public string SettlementAccount { get; set; }
    }
}
