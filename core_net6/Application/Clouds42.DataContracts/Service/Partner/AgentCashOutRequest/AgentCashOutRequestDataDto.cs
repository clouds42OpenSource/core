﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Данные о заявке на расходование средств
    /// </summary>
    [XmlRoot(ElementName = nameof(AgentCashOutRequest))]
    [DataContract(Name = nameof(AgentCashOutRequest))]
    public class AgentCashOutRequestDataDto
    {
        /// <summary>
        /// Номер заявки
        /// </summary>
        [XmlElement(ElementName = nameof(RequestNumber))]
        [DataMember(Name = nameof(RequestNumber))]
        public string RequestNumber { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountNumber))]
        [DataMember(Name = nameof(AccountNumber))]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [XmlElement(ElementName = nameof(CreationDateTime))]
        [DataMember(Name = nameof(CreationDateTime))]
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Сумма по заявке
        /// </summary>
        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public decimal Sum { get; set; }

        /// <summary>
        /// Ссылки на отчет агента
        /// </summary>
        [XmlElement(ElementName = nameof(AgentReportLinks))]
        [DataMember(Name = nameof(AgentReportLinks))]
        public AgentReportLinksDto AgentReportLinks { get; set; }
        
        /// <summary>
        /// Агентские реквизиты для выплаты
        /// </summary>
        [XmlElement(ElementName = nameof(AgentRequisitesForPayment))]
        [DataMember(Name = nameof(AgentRequisitesForPayment))]
        public AgentRequisitesForPaymentDto AgentRequisitesForPayment { get; set; }

        /// <summary>
        /// Поставщик
        /// </summary>
        [XmlElement(ElementName = nameof(SupplierCode))]
        [DataMember(Name = nameof(SupplierCode))]
        public string SupplierCode { get; set; }

        /// <summary>
        /// Выплата авторам или нет
        /// </summary>
        [XmlElement(ElementName = nameof(IsAuthorPayment))]
        [DataMember(Name = nameof(IsAuthorPayment))]
        public bool IsAuthorPayment { get; set; }
    }
}
