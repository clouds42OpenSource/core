﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Заявки на вывод средств в статусе "Новая"
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AgentCashOutRequestsInStatusNewDto
    {

        /// <summary>
        /// Дата актуального агентского соглашения
        /// </summary>
        [XmlElement(ElementName = nameof(ActualAgencyAgreementDate))]
        [DataMember(Name = nameof(ActualAgencyAgreementDate))]
        public DateTime ActualAgencyAgreementDate { get; set; }

        /// <summary>
        /// Ссылка на актуальное агентское вознаграждение
        /// </summary>
        [XmlElement(ElementName = nameof(ActualAgencyAgreementLink))]
        [DataMember(Name = nameof(ActualAgencyAgreementLink))]
        public string ActualAgencyAgreementLink { get; set; }

        /// <summary>
        /// Данные о заявках на вывод средств
        /// </summary>
        [XmlElement(ElementName = "AgentCashOutRequests")]
        [DataMember(Name = "AgentCashOutRequests")]
        public AgentCashOutRequestDatasDto AgentCashOutRequestDatas { get; set; }
    }
}
