﻿using System;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using JetBrains.Annotations;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель фильтра заявок на вывод средств агента
    /// </summary>
    public class AgentCashOutRequestsFilterDto : ISortingDto
    {
        /// <summary>
        /// Данные об аккаунте партнера
        /// </summary>
        [CanBeNull]
        public string PartnerAccountData { get; set; }

        /// <summary>
        /// Номер заявки
        /// </summary>
        [CanBeNull]
        public string RequestNumber { get; set; }

        /// <summary>
        /// Тип реквизитов агента
        /// </summary>
        public AgentRequisitesTypeEnum? AgentRequisitesType { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum? RequestStatus { get; set; }

        /// <summary>
        /// Период с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid? AccountUserId { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        [CanBeNull]
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
