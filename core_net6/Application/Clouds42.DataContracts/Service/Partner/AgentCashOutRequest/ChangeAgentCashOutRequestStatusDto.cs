﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;
using JetBrains.Annotations;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель смены статуса заявки на вывод средств
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class ChangeAgentCashOutRequestStatusDto
    {
        /// <summary>
        /// Номер заявки
        /// </summary>
        [CanBeNull]
        [XmlElement(ElementName = nameof(RequestNumber))]
        [DataMember(Name = nameof(RequestNumber))]
        public string RequestNumber { get; set; }

        /// <summary>
        /// Сумма по заявке
        /// </summary>
        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public decimal Sum { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        [XmlElement(ElementName = "Status")]
        [DataMember(Name = "Status")]
        public AgentCashOutRequestStatusEnum AgentCashOutRequestStatus { get; set; }
    }
}
