﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;
using Microsoft.AspNetCore.Http;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Модель создания заявки на вывод средств агента
    /// </summary>
    public class CreateAgentCashOutRequestDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID реквизитов агента
        /// </summary>
        public Guid AgentRequisitesId { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum RequestStatus { get; set; }

        /// <summary>
        /// Общая сумма к выводу
        /// </summary>
        public decimal TotalSum { get; set; }

        /// <summary>
        /// Сумма к выплате
        /// </summary>
        public decimal PaySum { get; set; }

        /// <summary>
        /// Файлы отчета
        /// </summary>
        public List<CloudFileDataDto<byte[]>> Files { get; set; }

        /// <summary>
        /// Режим редактирования
        /// </summary>
        public bool IsEditMode { get; set; }
    }

    /// <summary>
    /// Модель создания заявки на вывод средств агента
    /// </summary>
    public class CreateAgentCashOutRequestInputDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID реквизитов агента
        /// </summary>
        public Guid AgentRequisitesId { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        public AgentCashOutRequestStatusEnum RequestStatus { get; set; }

        /// <summary>
        /// Общая сумма к выводу
        /// </summary>
        public string TotalSum { get; set; }

        /// <summary>
        /// Сумма к выплате
        /// </summary>
        public string PaySum { get; set; }

        /// <summary>
        /// Файлы отчета
        /// </summary>
        public List<CloudFileDataDto<IFormFile>> Files { get; set; }

        /// <summary>
        /// Режим редактирования
        /// </summary>
        public bool IsEditMode { get; set; }

        public decimal TotalSumDecimal { get { return Convert.ToDecimal(TotalSum.Replace('.', ',')); } }
        public decimal PaySumDecimal { get { return Convert.ToDecimal(PaySum.Replace('.', ',')); } }
    }
}
