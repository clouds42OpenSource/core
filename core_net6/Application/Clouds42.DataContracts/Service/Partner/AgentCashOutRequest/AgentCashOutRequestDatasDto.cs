﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner.AgentCashOutRequest
{
    /// <summary>
    /// Данные о заявках на вывод средств
    /// </summary>
    [XmlRoot(ElementName = "AgentCashOutRequests")]
    [DataContract(Name = "AgentCashOutRequests")]
    public class AgentCashOutRequestDatasDto
    {
        /// <inheritdoc />
        public AgentCashOutRequestDatasDto()
        {
            AgentCashOutRequestDataList = [];
        }

        /// <summary>
        /// Тип списка
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = "Table"; 

        /// <summary>
        /// Список данных о заявках на вывод средств
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<AgentCashOutRequestDataDto> AgentCashOutRequestDataList { get; set; }
    }
}
