﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Информация по состоянию сервиса и подключенным услугам у клиента.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceTypeStateForUserDto
    {
        /// <summary>
        /// Признак активности сервиса у пользователя.
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceIsActive))]
        [DataMember(Name = nameof(ServiceIsActive))]
        public bool ServiceIsActive { get; set; }

        /// <summary>
        /// Признак что для сервиса активен демо период
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemoPeriod))]
        [DataMember(Name = nameof(IsDemoPeriod))]
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Дата окончания действия демо периода.
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceDemoExpiredDate))]
        [DataMember(Name = nameof(ServiceDemoExpiredDate))]
        public DateTime ServiceDemoExpiredDate { get; set; }

        /// <summary>
        /// Дата окончания действия текущей подписки на сервис у аккаунта пользователя.
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceExpiredDate))]
        [DataMember(Name = nameof(ServiceExpiredDate))]
        public DateTime ServiceExpiredDate { get; set; }   

        /// <summary>
        /// Список подключенных услуг сервиса у пользователя.
        /// </summary>
        [XmlElement(ElementName = nameof(EnabledServiceTypesList))]
        [DataMember(Name = nameof(EnabledServiceTypesList))]
        public GuidListItemDto EnabledServiceTypesList { get; set; }

        /// <summary>
        /// Признак указывающий что сервис отключен
        /// </summary>
        [XmlElement(ElementName = nameof(IsServiceDisabled))]
        [DataMember(Name = nameof(IsServiceDisabled))]
        public bool IsServiceDisabled { get; set; }
    }
}