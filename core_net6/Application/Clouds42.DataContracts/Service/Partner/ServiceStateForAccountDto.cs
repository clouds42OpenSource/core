﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.Billing.BillingService;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель статуса сервиса для аккаунта
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ServiceStateForAccountDto
    {
        /// <summary>
        /// Дата окончания действия сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ExpireDate))]
        [DataMember(Name = nameof(ExpireDate))]
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Список баз, в которые установлен сервис
        /// </summary>
        [XmlElement(ElementName = nameof(DatabasesIds))]
        [DataMember(Name = nameof(DatabasesIds))]
        public DatabasesIdsDto DatabasesIds { get; set; }

        /// <summary>
        /// Список пользователей для которых активны услуги сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ConnectedServiceTypesToUsers))]
        [DataMember(Name = nameof(ConnectedServiceTypesToUsers))]
        public ConnectedServiceTypesToUsersDto ConnectedServiceTypesToUsers { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(BillingServiceStatusForAccount))]
        [DataMember(Name = nameof(BillingServiceStatusForAccount))]
        public BillingServiceStatusForAccountEnum BillingServiceStatusForAccount { get; set; }

        /// <summary>
        /// Признак, что сервис на демо периоде
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemo))]
        [DataMember(Name = nameof(IsDemo))]
        public bool IsDemo { get; set; }
    }
}
