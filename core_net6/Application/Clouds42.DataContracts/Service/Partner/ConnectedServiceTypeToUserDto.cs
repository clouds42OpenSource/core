﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель сопоставления пользователей и услуг, которые активны для пользователей
    /// </summary>
    [XmlRoot(ElementName = "ConnectedServiceTypeToUser")]
    [DataContract(Name = "ConnectedServiceTypeToUser")]
    public class ConnectedServiceTypeToUserDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(UserId))]
        [DataMember(Name = nameof(UserId))]
        public Guid UserId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceTypeId))]
        [DataMember(Name = nameof(ServiceTypeId))]
        public Guid ServiceTypeId { get; set; }
    }
}
