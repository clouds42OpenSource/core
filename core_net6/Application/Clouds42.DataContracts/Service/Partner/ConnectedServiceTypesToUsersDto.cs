﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Partner
{
    /// <summary>
    /// Модель подсключенных услуг пользователям
    /// </summary>
    [XmlRoot(ElementName = nameof(ConnectedServiceTypesToUsers))]
    [DataContract(Name = nameof(ConnectedServiceTypesToUsers))]
    public class ConnectedServiceTypesToUsersDto
    {
        /// <summary>
        /// Тип элемента
        /// </summary>
        [XmlAttribute(AttributeName = "type")] [IgnoreDataMember]
        public string Type = "List";


        /// <summary>
        /// Список подсключенных услуг пользователям
        /// </summary>
        [XmlElement(ElementName = "ConnectedServiceTypeToUser")]
        [DataMember(Name = "ConnectedServiceTypeToUser")]
        public List<ConnectedServiceTypeToUserDto> ConnectedServiceTypesToUsers { get; set; } = [];
    }
}
