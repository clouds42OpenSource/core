﻿
using Clouds42.Common.Models;

namespace Clouds42.DataContracts.Service.WebAdministration
{
    /// <summary>
    /// Результат выполнения команды
    /// </summary>
    /// <typeparam name="TResult">Тип результата</typeparam>
    public class CommandExecutionResultDto<TResult>
    {
        /// <summary>
        /// Адрес сервера
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Результат
        /// </summary>
        public TResult Result { get; set; }

        /// <summary>
        /// Признак успешного выполнения команды
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Модель фиксации времени выполнения операции/команды
        /// </summary>
        public TraceRuntimeDataDc TraceRuntimeData { get; set; }
    }
}
