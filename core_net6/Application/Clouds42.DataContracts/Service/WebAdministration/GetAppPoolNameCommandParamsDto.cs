﻿namespace Clouds42.DataContracts.Service.WebAdministration
{
    /// <summary>
    /// Параметры команды на получение названия пула 
    /// </summary>
    public class GetAppPoolNameCommandParamsDto
    {
        /// <summary>
        /// Путь публикации инф. базы
        /// </summary>
        public string AcDbPublishPath { get; set; }

        /// <summary>
        /// Название сайта публикации
        /// </summary>
        public string PublishSiteName { get; set; }
    }
}
