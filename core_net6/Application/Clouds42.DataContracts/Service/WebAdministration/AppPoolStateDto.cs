﻿using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Service.WebAdministration
{
    /// <summary>
    /// Модель состояния пула
    /// </summary>
    public class AppPoolStateDto
    {
        /// <summary>
        /// Название сервера
        /// </summary>
        [DataMember(Name = "PSComputerName")]
        public string ServerName { get; set; }

        /// <summary>
        /// Название пула
        /// </summary>
        [DataMember(Name = nameof(AppPoolName))]
        public string AppPoolName { get; set; }

        /// <summary>
        /// Состояние пула
        /// </summary>
        [DataMember(Name = nameof(PoolState))]
        public string PoolState { get; set; }

        /// <summary>
        /// Кол-во рабочих процессов
        /// </summary>
        [DataMember(Name = nameof(WorkerProcessesCount))]
        public int WorkerProcessesCount { get; set; }
    }
}
