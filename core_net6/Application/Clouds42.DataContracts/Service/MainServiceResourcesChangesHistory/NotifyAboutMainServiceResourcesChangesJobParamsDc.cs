﻿using System;

namespace Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory
{
    /// <summary>
    /// Параметры задачи для уведомления
    /// об изменении ресурсов главного сервиса (Аредна 1С)
    /// </summary>
    public class NotifyAboutMainServiceResourcesChangesJobParamsDc
    {
        /// <summary>
        /// Дата изменений
        /// </summary>
        public DateTime ChangesDate { get; set; }
    }
}
