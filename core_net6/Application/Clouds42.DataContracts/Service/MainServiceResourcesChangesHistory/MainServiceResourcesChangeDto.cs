﻿namespace Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory
{
    /// <summary>
    /// Модель изменения ресурсов главного сервиса (Аренда1С)
    /// для аккаунта 
    /// </summary>
    public class MainServiceResourcesChangeDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Логин сейлс менеджера
        /// </summary>
        public string SaleManagerLogin { get; set; }

        /// <summary>
        /// Почтовый адрес сейсл менеджера
        /// </summary>
        public string SaleManagerEmail { get; set; }

        /// <summary>
        /// Количество отключенных ресурсов
        /// </summary>
        public int DisabledResourcesCount { get; set; }

        /// <summary>
        /// Количество подключенных ресурсов
        /// </summary>
        public int EnabledResourcesCount { get; set; }

        /// <summary>
        /// Итого подключенных ресурсов
        /// </summary>
        public int TotalEnabledResourcesCount => EnabledResourcesCount - DisabledResourcesCount;
    }
}
