﻿using System;

namespace Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory
{
    /// <summary>
    /// Модель для фиксирования истории изменений ресурсов
    /// главного сервиса(Аренда1С) 
    /// </summary>
    public class CommitMainServiceResourcesChangesHistoryDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество отключенных ресурсов
        /// </summary>
        public int DisabledResourcesCount { get; set; }

        /// <summary>
        /// Количество подключенных ресурсов
        /// </summary>
        public int EnabledResourcesCount { get; set; }
    }
}
