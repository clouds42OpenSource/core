﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.ProvidedServiceModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CorpActRequestDto
    {

        /// <summary>
        /// ИдРТиУ
        /// </summary>
        [XmlElement(ElementName = nameof(CorpUniquaNumber))]
        [DataMember(Name = nameof(CorpUniquaNumber))]
        public string CorpUniquaNumber { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date { get; set; }

        /// <summary>
        /// Номер акта
        /// </summary>
        [XmlElement(ElementName = nameof(ActNumber))]
        [DataMember(Name = nameof(ActNumber))]
        public int? ActNumber { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(IndexNumberAccount))]
        [DataMember(Name = nameof(IndexNumberAccount))]
        public int IndexNumberAccount { get; set; }

        /// <summary>
        /// Управленческий учет
        /// </summary>
        [XmlElement(ElementName = nameof(IsManagedAccounting))]
        [DataMember(Name = nameof(IsManagedAccounting))]
        public bool IsManagedAccounting { get; set; }

        /// <summary>
        /// Бухгалтерский учет
        /// </summary>
        [XmlElement(ElementName = nameof(IsBookerAccounting))]
        [DataMember(Name = nameof(IsBookerAccounting))]
        public bool IsBookerAccounting { get; set; }


        /// <summary>
        /// Перечень номенклатуры.
        /// </summary>
        [XmlElement(ElementName = "NomenclatureItems")]
        [DataMember(Name = "NomenclatureItems")]
        public CorpActNomenclatureListDto Nomenclatures { get; set; }
    }

    [XmlRoot(ElementName = "NomenclatureItems")]
    [DataContract(Name = "NomenclatureItems")]
    public class CorpActNomenclatureListDto
    {
        /// <summary>
        /// Список номенклатуры.
        /// </summary>
        [XmlElement(ElementName = "NomenclatureItem")]
        [DataMember(Name = "NomenclatureItem")]
        public List<CorpActNomenclatureItemDto> Items { get; set; }
    }
}
