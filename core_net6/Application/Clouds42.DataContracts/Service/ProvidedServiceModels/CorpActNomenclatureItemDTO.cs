﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.ProvidedServiceModels
{
    [XmlRoot(ElementName = "NomenclatureItem")]
    [DataContract(Name = "NomenclatureItem")]
    public class CorpActNomenclatureItemDto
    {

        /// <summary>
        /// Содержание услуги, доп сведения.
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Количество.
        /// </summary>
        [XmlElement(ElementName = nameof(Count))]
        [DataMember(Name = nameof(Count))]
        public int Count { get; set; }

        /// <summary>
        /// Ставка/тариф.
        /// </summary>
        [XmlElement(ElementName = nameof(Rate))]
        [DataMember(Name = nameof(Rate))]
        public decimal Rate { get; set; }

        /// <summary>
        /// Код номенклатуры.
        /// </summary>
        [XmlElement(ElementName = nameof(NomenclatureUniquaKey))]
        [DataMember(Name = nameof(NomenclatureUniquaKey))]
        public string NomenclatureUniquaKey { get; set; }
    }
}