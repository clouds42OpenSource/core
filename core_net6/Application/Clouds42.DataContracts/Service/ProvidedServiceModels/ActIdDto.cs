﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.ProvidedServiceModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class ActIdDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(CorpUniquaNumber))]
        [DataMember(Name = nameof(CorpUniquaNumber))]
        public string CorpUniquaNumber { get; set; }
        
    }
}