﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Service.PrintedHtmlForm
{
    /// <summary>
    /// Информация печатной формы HTML
    /// </summary>
    [DataContract]
    public class PrintedHtmlFormInfoDto
    {
        /// <summary>
        /// Id печатной формы
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Название печатной формы Html
        /// </summary>
        [DataMember]
        public string Name { get; set; }
    }
}
