﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.PrintedHtmlForm
{
    /// <summary>
    /// Печатная форма HTML
    /// </summary>
    [DataContract]
    public class PrintedHtmlFormDto
    {
        /// <summary>
        /// Id печатной формы
        /// </summary>
        [DataMember]
        public Guid? Id { get; set; }

        /// <summary>
        /// Название печатной формы Html
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Html разметка
        /// </summary>
        [DataMember]
        public string HtmlData { get; set; }

        /// <summary>
        /// Тип модели
        /// </summary>
        [DataMember]
        public string ModelType { get; set; }

        /// <summary>
        /// Тип моделей
        /// TODO : Убрать когда примут вкладку на React "Печатные формы HTML"
        /// </summary>
        public Dictionary<string, string> ModelTypes { get; set; } = new();

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        [DataMember]
        public List<CloudFileDto> Files { get; set; } = [];
    }
}
