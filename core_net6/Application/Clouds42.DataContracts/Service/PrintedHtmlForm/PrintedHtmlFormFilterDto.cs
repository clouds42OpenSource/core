﻿using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Service.PrintedHtmlForm
{
    /// <summary>
    /// Модель фильтра печатных форм HTML
    /// </summary>
    [DataContract]
    public class PrintedHtmlFormFilterDto
    {
        /// <summary>
        /// Название печатной формы
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        [DataMember]
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        [DataMember]
        public int PageSize { get; set; } = 10;
    }
}
