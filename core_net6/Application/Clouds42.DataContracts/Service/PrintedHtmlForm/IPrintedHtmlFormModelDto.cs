﻿namespace Clouds42.DataContracts.Service.PrintedHtmlForm
{
    /// <summary>
    /// Модель печатной формы HTML
    /// </summary>
    public interface IPrintedHtmlFormModelDto
    {

        /// <summary>
        /// Заполнить тестовыми данными
        /// </summary>
        /// <param name="localeName">Название локали</param>
        void FillTestData(string siteAuthorityUrl = null, string localeName = null, string supplierReferenceName = null);

        /// <summary>
        /// Получить название модели
        /// </summary>
        string GetModelName();
    }
}
