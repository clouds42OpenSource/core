﻿using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Service.Link.Interface
{
    public interface IAccountUserImpersonateDataDto
    {
        /// <summary>
        /// Данные пользователя аккаунта.
        /// </summary>
        AccountUserCommonDataDto AccountUserProperties { get; set; }
        
        /// <summary>
        /// Данные аккаунта.
        /// </summary>
        AccountCommonDataDto AccountProperties { get; set; }
        
        /// <summary>
        /// Данные по балансу аккаунта.
        /// </summary>
        AccountBalanceDto AccountDatabasesBalance { get; set; }
        
        /// <summary>
        /// Данные по сгменту аккаунта.
        /// </summary>
        CloudServicesSegmentDto SegmentData { get; set; }
        
        /// <summary>
        /// Роли пользователя облака.
        /// </summary>
        AccountUserRolesListDto AccountUserRolesList { get; set; }
    }
}