﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.Service.Link
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserRolesListDto
    {
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<AccountUserGroup> Items { get; set; }
    }
}