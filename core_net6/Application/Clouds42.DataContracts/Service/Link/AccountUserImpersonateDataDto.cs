﻿using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.Link.Interface;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountUserImpersonateDataDto : IAccountUserImpersonateDataDto
    {
        [XmlElement(ElementName = nameof(AccountUserProperties))]
        [DataMember(Name = nameof(AccountUserProperties))]
        public AccountUserCommonDataDto AccountUserProperties { get; set; }

        [XmlElement(ElementName = nameof(AccountProperties))]
        [DataMember(Name = nameof(AccountProperties))]
        public AccountCommonDataDto AccountProperties { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabasesBalance))]
        [DataMember(Name = nameof(AccountDatabasesBalance))]
        public AccountBalanceDto AccountDatabasesBalance { get; set; }

        [XmlElement(ElementName = nameof(SegmentData))]
        [DataMember(Name = nameof(SegmentData))]
        public CloudServicesSegmentDto SegmentData { get; set; }

        [XmlElement(ElementName = nameof(AccountUserRolesList))]
        [DataMember(Name = nameof(AccountUserRolesList))]
        public AccountUserRolesListDto AccountUserRolesList { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseList))]
        [DataMember(Name = nameof(AccountDatabaseList))]
        public AccountDatabaseListDto AccountDatabaseList { get; set; }
    }
}
