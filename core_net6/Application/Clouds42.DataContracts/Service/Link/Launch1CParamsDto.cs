﻿namespace Clouds42.DataContracts.Service.Link
{
    /// <summary>
    /// Модель параметров запуска 1С
    /// </summary>
    public class Launch1CParamsDto
    {
        /// <summary>
        /// Путь до клиента 1С.
        /// </summary>
        public string Client1CPath { get; set; }

        /// <summary>
        /// Параметры приложения.
        /// </summary>
        public string ExeParameters { get; set; }

        /// <summary>
        /// Путь к файлу лога
        /// </summary>
        public string LogFilePath { get; set; }

    }
}
