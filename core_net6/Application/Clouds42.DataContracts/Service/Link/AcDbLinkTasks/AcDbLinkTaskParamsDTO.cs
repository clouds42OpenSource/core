﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskParamsDto
    {
        [XmlElement(ElementName = nameof(TaskParams))]
        [DataMember(Name = nameof(TaskParams))]
        public string TaskParams { get; set; }

        public AcDbLinkTaskParamsDto()
        {
        }

        public AcDbLinkTaskParamsDto(string taskParams)
        {
            TaskParams = taskParams;
        }
    }
}

