﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskActionTypeDto
    {
        [XmlElement(ElementName = nameof(TaskActionType))]
        [DataMember(Name = nameof(TaskActionType))]
        public int TaskActionType { get; set; }

        public AcDbLinkTaskActionTypeDto(int taskActionType)
        {
            TaskActionType = taskActionType;
        }

        public AcDbLinkTaskActionTypeDto() { }
    }
}

