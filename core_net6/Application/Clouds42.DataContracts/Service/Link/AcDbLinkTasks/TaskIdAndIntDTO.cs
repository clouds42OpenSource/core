﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class TaskIdAndIntDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "TaskID")]
        [DataMember(Name = "TaskID")]
        public Guid TaskId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public int Value { get; set; }
    }
}