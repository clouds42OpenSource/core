﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskCommandDto
    {
        [XmlElement(ElementName = nameof(TaskCommand))]
        [DataMember(Name = nameof(TaskCommand))]
        public string TaskCommand { get; set; }

        public AcDbLinkTaskCommandDto()
        {
        }

        public AcDbLinkTaskCommandDto(string taskCommand)
        {
            TaskCommand = taskCommand;
        }
    }
}
