﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeGetTaskDto
    {
        [XmlElement(ElementName = "DateTimeGetTask")]
        [DataMember(Name = "DateTimeGetTask")]
        public DateTime? dateTimeGetTask { get; set; }

        public DateTimeGetTaskDto()
        { 
        
        }

        public DateTimeGetTaskDto(DateTime? DateTimeGetTask)
        {
            dateTimeGetTask = DateTimeGetTask;
        }
    }
}
