﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestDateTimeSetTaskDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        [XmlElement(ElementName = nameof(DateTimeSetTask))]
        [DataMember(Name = nameof(DateTimeSetTask))]
        public DateTime DateTimeSetTask { get; set; }

        public PostRequestDateTimeSetTaskDto()
        {

        }


    }
}