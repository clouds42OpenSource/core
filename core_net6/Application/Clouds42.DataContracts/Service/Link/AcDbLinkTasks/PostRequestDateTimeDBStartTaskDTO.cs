﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestDateTimeDBStartTaskDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        [XmlElement(ElementName = nameof(DateTimeDBStartTask))]
        [DataMember(Name = nameof(DateTimeDBStartTask))]
        public DateTime DateTimeDBStartTask { get; set; }

        public PostRequestDateTimeDBStartTaskDto()
        {

        }


    }
}