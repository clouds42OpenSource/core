﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeDBStartTaskDto
    {
        [XmlElement(ElementName = "DateTimeDBStartTask")]
        [DataMember(Name = "DateTimeDBStartTask")]
        public DateTime? dateTimeDBStartTask { get; set; }

        public DateTimeDBStartTaskDto()
        { 
        
        }

        public DateTimeDBStartTaskDto(DateTime? DateTimeDBStartTask)
        {
            dateTimeDBStartTask = DateTimeDBStartTask;
        }
    }
}
