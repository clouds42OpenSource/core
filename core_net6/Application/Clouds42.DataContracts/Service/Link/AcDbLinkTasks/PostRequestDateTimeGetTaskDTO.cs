﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestDateTimeGetTaskDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        [XmlElement(ElementName = nameof(DateTimeGetTask))]
        [DataMember(Name = nameof(DateTimeGetTask))]
        public DateTime DateTimeGetTask { get; set; }

        public PostRequestDateTimeGetTaskDto()
        {

        }


    }
}