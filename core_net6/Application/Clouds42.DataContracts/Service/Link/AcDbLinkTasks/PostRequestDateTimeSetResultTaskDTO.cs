﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestDateTimeSetResultTaskDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        [XmlElement(ElementName = nameof(DateTimeSetResultTask))]
        [DataMember(Name = nameof(DateTimeSetResultTask))]
        public DateTime DateTimeSetResultTask { get; set; }

        public PostRequestDateTimeSetResultTaskDto()
        {

        }


    }
}