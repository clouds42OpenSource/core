﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskListDto(List<AcDbLinkTaskItem> listItem)
    {
        [XmlElement(ElementName = nameof(TaskList))]
        [DataMember(Name = nameof(TaskList))]
        public List<AcDbLinkTaskItem> TaskList { get; set; } = listItem;

        public AcDbLinkTaskListDto() : this([])
        {
        }
    }

    public class AcDbLinkTaskItem
    {
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        [XmlElement(ElementName = nameof(LocalUser_id))]
        [DataMember(Name = nameof(LocalUser_id))]
        public Guid LocalUser_id { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabase_id))]
        [DataMember(Name = nameof(AccountDatabase_id))]
        public Guid AccountDatabase_id { get; set; }
        
    }

}
