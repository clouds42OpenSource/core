﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskIdsListDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "TaskIDs")]
        [DataMember(Name = "TaskIDs")]
        public GuidListItemDto TaskIdsList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AcDbLinkTaskIdsListDto()
        {
            TaskIdsList = new GuidListItemDto();
        }
    }
   
}
