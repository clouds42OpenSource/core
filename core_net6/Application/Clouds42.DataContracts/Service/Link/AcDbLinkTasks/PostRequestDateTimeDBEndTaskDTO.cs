﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestDateTimeDBEndTaskDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        [XmlElement(ElementName = nameof(DateTimeDBEndTask))]
        [DataMember(Name = nameof(DateTimeDBEndTask))]
        public DateTime DateTimeDBEndTask { get; set; }

        public PostRequestDateTimeDBEndTaskDto()
        {

        }


    }
}