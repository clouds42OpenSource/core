﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeSetTaskDto
    {
        [XmlElement(ElementName = "DateTimeSetTask")]
        [DataMember(Name = "DateTimeSetTask")]
        public DateTime? dateTimeSetTask { get; set; }

        public DateTimeSetTaskDto()
        { 
        
        }

        public DateTimeSetTaskDto(DateTime? DateTimeSetTask)
        {
            dateTimeSetTask = DateTimeSetTask;
        }
    }
}
