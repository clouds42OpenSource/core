﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeDBEndTaskDto
    {
        [XmlElement(ElementName = "DateTimeDBEndTask")]
        [DataMember(Name = "DateTimeDBEndTask")]
        public DateTime? dateTimeDBEndTask { get; set; }

        public DateTimeDBEndTaskDto()
        { 
        
        }

        public DateTimeDBEndTaskDto(DateTime? DateTimeDBEndTask)
        {
            dateTimeDBEndTask = DateTimeDBEndTask;
        }
    }
}
