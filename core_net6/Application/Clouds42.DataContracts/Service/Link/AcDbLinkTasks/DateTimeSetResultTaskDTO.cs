﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DateTimeSetResultTaskDto
    {
        [XmlElement(ElementName = "DateTimeSetResultTask")]
        [DataMember(Name = "DateTimeSetResultTask")]
        public DateTime? dateTimeSetResultTask { get; set; }

        public DateTimeSetResultTaskDto()
        { 
        
        }

        public DateTimeSetResultTaskDto(DateTime? DateTimeSetResultTask)
        {
            dateTimeSetResultTask = DateTimeSetResultTask;
        }
    }
}
