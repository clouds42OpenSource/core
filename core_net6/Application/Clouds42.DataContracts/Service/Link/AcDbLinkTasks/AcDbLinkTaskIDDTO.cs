﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link.AcDbLinkTasks
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbLinkTaskIDDto
    {
        [XmlElement(ElementName = nameof(LinkTaskID))]
        [DataMember(Name = nameof(LinkTaskID))]
        public Guid LinkTaskID { get; set; }

        public AcDbLinkTaskIDDto()
        {
        }

        public AcDbLinkTaskIDDto(Guid linkTaskID)
        {
            LinkTaskID = linkTaskID;
        }
    }
}
