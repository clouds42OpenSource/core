﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Service.Link
{

    /// <summary>
    /// Модель описывает необходимые параметры для подключения облачного диска.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class MountCloudDiskInfoDto
    {

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        [XmlElement(ElementName = nameof(AccountIndexNumber))]
        [DataMember(Name = nameof(AccountIndexNumber))]
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Путь до файлого хранилища сегмента.
        /// </summary>
        [XmlElement(ElementName = nameof(FileStoragePath))]
        [DataMember(Name = nameof(FileStoragePath))]
        public string FileStoragePath { get; set; }

        /// <summary>
        /// Полный путь до каталога клиентских файлов.
        /// </summary>
        [XmlElement(ElementName = nameof(ClientFileStorageFullPath))]
        [DataMember(Name = nameof(ClientFileStorageFullPath))]
        public string ClientFileStorageFullPath { get; set; }

    }
}
