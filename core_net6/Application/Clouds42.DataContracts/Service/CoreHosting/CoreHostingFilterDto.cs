﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;


namespace Clouds42.DataContracts.Service.CoreHosting
{
    /// <summary>
    /// Модель фильтра хостинга облака
    /// </summary>
    public class CoreHostingFilterDto : ISortingDto
    {
        /// <summary>
        /// Название хостинга
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Путь для загружаемых файлов
        /// </summary>
        public string UploadFilesPath { get; set; }

        /// <summary>
        /// Адрес API для загрузки файлов
        /// </summary>
        public string UploadApiUrl { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
