﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Service.CoreHosting
{
    /// <summary>
    /// Контракт описывающий хостинг облака
    /// </summary>
    public class CoreHostingDto
    {
        /// <summary>
        /// ID хостинга
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название хостинга
        /// </summary>
        [DisplayName(@"Название хостинга:")]
        [Required(ErrorMessage = @"Поле ""Название хостинга"" обязательное для заполнения")]
        public string Name { get; set; }

        /// <summary>
        /// Путь для загружаемых файлов
        /// </summary>
        [DisplayName(@"Путь для загружаемых файлов:")]
        [Required(ErrorMessage = @"Поле ""Путь для загружаемых файлов"" обязательное для заполнения")]
        public string UploadFilesPath { get; set; }

        /// <summary>
        /// Адрес API для загрузки файлов
        /// </summary>
        [DisplayName(@"Адрес API для загрузки файлов:")]
        [Required(ErrorMessage = @"Поле ""Адрес API для загрузки файлов"" обязательное для заполнения")]
        public string UploadApiUrl { get; set; }
    }
}
