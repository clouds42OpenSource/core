﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Service.Google
{
    /// <summary>
    /// Токен авторизации Google
    /// </summary>
    public class GoogleAuthTokenDataDto
    {
        /// <summary>
        /// Токен доступа
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Тип токена
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Время жизни токена
        /// </summary>
        [JsonProperty("expires_in")]
        public long? ExpiresInSeconds { get; set; }

        /// <summary>
        /// Токен обновления
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
