﻿using Clouds42.DataContracts.Service.SendGrid.LetterTemplate;

namespace Clouds42.DataContracts.Service.SendGrid
{
    /// <summary>
    /// Полезная нагрузка для выполнения команды
    /// </summary>
    public class CommandExecuteRequestPayloadDto
    {
        /// <summary>
        ///  Настройки запроса на отправку письма
        /// </summary>
        public EmailRequestSettingsDto EmailRequestSettings { get; set; }

        /// <summary>
        /// Общие данные для шаблона письма
        /// </summary>
        public LetterTemplateCommonDataDto LetterTemplateCommonData { get; set; }
    }
}
