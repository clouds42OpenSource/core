﻿using System;

namespace Clouds42.DataContracts.Service.SendGrid
{
    /// <summary>
    /// Информация о владельце эл. почты
    /// </summary>
    public class EmailAddressInfoDto(string email, string personName = null, Guid? id = null)
    {
        /// <summary>
        /// Эл. почта
        /// </summary>
        public string Email { get; } = email;

        /// <summary>
        /// Имя
        /// </summary>
        public string PersonName { get; } = personName;

        public Guid? Id { get; set; } = id;
    }
}
