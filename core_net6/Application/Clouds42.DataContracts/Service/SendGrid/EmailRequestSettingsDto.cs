﻿using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;
using System;

namespace Clouds42.DataContracts.Service.SendGrid
{
    /// <summary>
    /// Настройки запроса на отправку письма
    /// </summary>
    public class EmailRequestSettingsDto
    {
        /// <summary>
        /// Отправитель письма
        /// </summary>
        public EmailAddressInfoDto EmailSender { get; set; }

        /// <summary>
        /// Получатель письма
        /// </summary>
        public EmailAddressInfoDto EmailReceiver { get; set; }

        /// <summary>
        /// Получатели письма
        /// </summary>
        public List<EmailAddressInfoDto> EmailReceivers { get; set; }
        /// <summary>
        /// Копии письма
        /// </summary>
        public List<EmailAddressInfoDto> CopyToEmailAddresses { get; set; } = [];

        /// <summary>
        /// Категории письма
        /// </summary>
        public List<string> Categories { get; set; }

        /// <summary>
        /// Ключ АПИ для SendGrid
        /// </summary>
        public string SendGridApiKey { get; set; }

        /// <summary>
        /// Уникальный идентификатор агента SendGrid
        /// </summary>
        public Guid SendGridAgentUniqueId { get; set; }

        /// <summary>
        /// Название агента SendGrid
        /// </summary>
        public string SendGridAgentName { get; set; }

        /// <summary>
        /// ID шаблона SendGrid
        /// </summary>
        public string SendGridTemplateId { get; set; }

        /// <summary>
        /// Вложения к письму
        /// </summary>
        public List<DocumentDataDto> Attachments { get; set; } = [];

        public bool IsBccRecipients { get; set; }
    }
}
