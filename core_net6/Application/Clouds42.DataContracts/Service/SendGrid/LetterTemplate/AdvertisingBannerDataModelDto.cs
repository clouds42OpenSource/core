﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель данных рекламного баннера
    /// </summary>
    public class AdvertisingBannerDataModelDto
    {
        /// <summary>
        /// HTML данные
        /// </summary>
        public string HtmlData { get; set; }
    }
}
