﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель данных шаблона письма
    /// </summary>
    public class LetterTemplateDataDto
    {
        /// <summary>
        /// Id шаблона письма
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Тип письма
        /// </summary>
        public string LetterTypeName { get; set; }

        /// <summary>
        /// Название шаблона рекламного баннера
        /// </summary>
        public string AdvertisingBannerTemplateName { get; set; }
    }
}
