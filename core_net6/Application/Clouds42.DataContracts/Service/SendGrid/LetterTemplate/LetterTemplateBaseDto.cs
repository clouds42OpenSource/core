﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Базовая модель шаблона письма
    /// </summary>
    public class LetterTemplateBaseDto
    {
        /// <summary>
        /// Id шаблона письма
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Заголовок письма
        /// </summary>
        public string Header { get; set; }
        
        /// <summary>
        /// ID шаблона письма в SendGrid
        /// </summary>
        public string SendGridTemplateId { get; set; }

        /// <summary>
        /// Шаблон рекламного баннера
        /// </summary>
        public int? AdvertisingBannerTemplateId { get; set; }

        public string NotificationText { get; set; }
    }
}
