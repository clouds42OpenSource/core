﻿using Clouds42.DataContracts.Service.Market;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель шаблона письма
    /// </summary>
    public class LetterTemplateDetailsDto : EditLetterTemplateDto
    {
        /// <summary>
        /// Категория письма
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Краткая информация по шаблонам рекламного баннера (Для выпадающего списка)
        /// </summary>
        public IEnumerable<AdvertisingBannerTemplateShortInfoDto> AdvertisingBannerTemplatesShortInfo { get; set; }
    }
}
