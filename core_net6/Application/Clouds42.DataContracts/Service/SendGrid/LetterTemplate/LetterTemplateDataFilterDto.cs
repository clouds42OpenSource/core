﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель фильтра данных писем
    /// </summary>
    public class LetterTemplateDataFilterDto
    {
        /// <summary>
        /// Флаг показывающий что письмо является системным
        /// </summary>
        public bool IsSystemLetter { get; set; }
    }
}
