﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о блокировке сервиса
    /// </summary>
    public class LockServiceLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название заблокированных сервисов
        /// </summary>
        public string ServiceNames { get; set; }

        /// <summary>
        /// Причина блокировки
        /// </summary>
        public string LockReason { get; set; }

        /// <summary>
        /// Документ счета
        /// </summary>
        public DocumentDataDto InvoiceDocument { get; set; }

        /// <summary>
        /// Копии для отправки
        /// </summary>
        public List<string> EmailsToCopy { get; set; }
    }
}
