﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о переносе данных аккаунта в склеп
    /// </summary>
    public class TransferringAccountDataToTombLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Время жизни данных аккаунта в склепе(количество дней)
        /// </summary>
        public int LifetimeAccountDataInTombDaysCount { get; set; }
    }
}
