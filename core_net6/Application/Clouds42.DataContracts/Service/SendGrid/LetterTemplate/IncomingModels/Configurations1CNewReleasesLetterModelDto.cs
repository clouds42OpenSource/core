﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Configurations1c;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о новых релизах конфигураций
    /// </summary>
    public class Configurations1CNewReleasesLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Список конфигураций 1С и их версий
        /// </summary>
        public List<Configuration1CVersionsInfoDto> Configurations1CInfo { get; set; } = [];

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }
    }
}
