﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма уведомления об использовании инф. базы 
    /// </summary>
    public class AccountDatabaseUsedLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название информационной базы
        /// </summary>
        public string DatabaseCaption { get; set; }
    }
}
