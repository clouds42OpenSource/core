﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о уведомлении что демо период завершён, нужна оплата
    /// </summary>
    public class DemoPeriodEndedPaymentRequiredLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }
              
        /// <summary>
        /// URL до строницы управления сервисом
        /// </summary>
        public string BillingServicePageUrl { get; set; }
    }
}
