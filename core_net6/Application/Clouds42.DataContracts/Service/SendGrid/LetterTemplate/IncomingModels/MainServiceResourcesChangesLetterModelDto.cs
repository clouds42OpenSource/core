﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма об изменениях ресурсов главного сервиса (Аренда1С)
    /// </summary>
    public class MainServiceResourcesChangesLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Дата изменений с
        /// </summary>
        public DateTime ChangesDateFrom { get; set; }

        /// <summary>
        /// Дата изменений по
        /// </summary>
        public DateTime ChangesDateTo { get; set; }

        /// <summary>
        /// Изменения ресурсов главного сервиса (Аренда1С)
        /// </summary>
        public IEnumerable<MainServiceResourcesChangeDto> MainServiceResourcesChanges { get; set; } = new List<MainServiceResourcesChangeDto>();
    }
}