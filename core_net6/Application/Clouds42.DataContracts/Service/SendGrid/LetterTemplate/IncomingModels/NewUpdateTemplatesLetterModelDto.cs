﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.DbTemplates;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма со списком обновлённых фаловых шаблонов
    /// </summary>
    public class NewUpdateTemplatesLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Список обновлённых файловых шаблонов 
        /// </summary>
        public List<UpdateDatabaseTemplateResultDto> ListWithUpdatedFileTemplates { get; set; } = [];
    }
}
