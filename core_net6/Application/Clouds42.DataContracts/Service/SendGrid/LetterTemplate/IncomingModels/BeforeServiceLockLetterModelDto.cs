﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Cloud42Services;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скорой блокировке сервиса
    /// </summary>
    public class BeforeServiceLockLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата блокировки
        /// </summary>
        public DateTime LockServiceDate { set; get; }

        /// <summary>
        /// Баланс аккаунта
        /// </summary>
        public decimal AccountBalance { set; get; }

        /// <summary>
        /// Баланс бонусов аккаунта
        /// </summary>
        public decimal AccountBonusBalance { get; set; }

        /// <summary>
        /// Стоимсоть сервиса
        /// </summary>
        public decimal ServiceCost { set; get; }

        /// <summary>
        /// Счет
        /// </summary>
        public InvoiceDataDto Invoice { get; set; }

        /// <summary>
        /// Период счета
        /// </summary>
        public int InvoicePeriod { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }

        /// <summary>
        /// Урл на страницу баланса
        /// </summary>
        public string BalancePageUrl { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string BillingServiceName { get; set; }

        /// <summary>
        /// Копии для отправки
        /// </summary>
        public List<string> EmailsToCopy { get; set; }
    }
}
