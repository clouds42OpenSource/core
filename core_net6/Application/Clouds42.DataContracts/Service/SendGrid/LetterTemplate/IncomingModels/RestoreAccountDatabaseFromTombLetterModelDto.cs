﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о востановлении базы из склепа
    /// </summary>
    public class RestoreAccountDatabaseFromTombLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название информационной базы
        /// </summary>
        public string DatabaseCaption { get; set; }

        /// <summary>
        /// Урл на страницу ифн. баз
        /// </summary>
        public string UrlForAccountDatabasesPage { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; } = [];
    }
}
