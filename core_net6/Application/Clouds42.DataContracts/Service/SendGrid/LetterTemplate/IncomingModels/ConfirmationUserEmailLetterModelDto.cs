﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о подтверждении email пользователя
    /// </summary>
    public class ConfirmationUserEmailLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Урл для верефикации Email адреса
        /// </summary>
        public string UrlForVerifyEmail { get; set; }
    }
}
