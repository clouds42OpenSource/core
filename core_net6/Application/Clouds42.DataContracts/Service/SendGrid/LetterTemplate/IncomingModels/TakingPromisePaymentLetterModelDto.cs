﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о взятии обещанного платежа 
    /// </summary>
    public class TakingPromisePaymentLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id счета
        /// </summary>
        public Guid InvoiceId { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }

        /// <summary>
        /// Документ счета
        /// </summary>
        public DocumentDataDto InvoiceDocument { get; set; }

        /// <summary>
        /// Количество дней на обещанный платеж
        /// </summary>
        public int PromisePaymentDays { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; } = [];
    }
}
