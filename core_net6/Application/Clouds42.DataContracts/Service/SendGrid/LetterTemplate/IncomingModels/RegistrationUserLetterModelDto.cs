﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о регистрации пользователя
    /// </summary>
    public class RegistrationUserLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }
        
        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public string AccountUserFullName { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string AccountUserPsd { get; set; }

        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        public string AccountUserPhoneNumber { get; set; }

        /// <summary>
        /// Почта пользователя
        /// </summary>
        public string AccountUserEmail { get; set; }

        /// <summary>
        /// Источник регистрации
        /// </summary>
        public ExternalClient ExternalClient { get; set; }

        /// <summary>
        /// Урл страницы сервиса Delans
        /// </summary>
        public string UrlForOpenDelansServicePage { get; set; }

        /// <summary>
        /// Урл для сброса/восстановления пароля
        /// </summary>
        public string UrlForResetPassword { get; set; }

        /// <summary>
        /// Источник с которого пришел пользователь
        /// на страницу регистрации
        /// </summary>
        public string UserSource { get; set; }
        public string SaleManagerLogin { get; set; }
        public string SalesManagerDivision { get; set; }
        public string AccountSaleManagerCaption { get { return string.IsNullOrEmpty(SaleManagerLogin) ? "-" : $"{SaleManagerLogin} ({SalesManagerDivision})"; } }
    }
}
