﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Базовая модель письма о скором изменении сервиса
    /// </summary>
    public abstract class BeforeChangeServiceBaseLetterModelDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя аккаунта
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Id изменений сервиса
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Ссылка для открытия карточки сервиса
        /// </summary>
        public string UrlForOpenBillingServiceCard { get; set; }

        /// <summary>
        /// Электронная почта владельца сервиса
        /// </summary>
        public string ServiceOwnerEmail { get; set; }

        /// <summary>
        /// Номер телефона владельца сервиса
        /// </summary>
        public string ServiceOwnerPhoneNumber { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; }
    }
}
