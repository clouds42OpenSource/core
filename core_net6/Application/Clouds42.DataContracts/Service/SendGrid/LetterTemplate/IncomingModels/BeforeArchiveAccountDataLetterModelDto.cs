﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скорой архивации данных аккаунта
    /// </summary>
    public class BeforeArchiveAccountDataLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество дней до архивации
        /// </summary>
        public int DaysToArchive { get; set; }
    }
}
