﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма об изменении типа информационной базы(серверная/файловая)
    /// </summary>
    public class ChangeDatabaseTypeLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Информация об аккаунте
        /// </summary>
        public string AccountInfo { get; set; }

        /// <summary>
        /// Информация об инф. базе
        /// </summary>
        public string DatabaseInfo { get; set; }

        /// <summary>
        /// Изменена на серверный тип
        /// </summary>
        public bool HasChangedToServerType { get; set; }

        /// <summary>
        /// Телефон для связи с пользователем
        /// </summary>
        public string ContactPhoneNumber { get; set; }
    }
}
