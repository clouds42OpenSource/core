﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о предоставлении доступа к базе
    /// </summary>
    public class ProvidingAccessToDatabaseLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Id пользователя аккаунта
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Название компании
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Название информационной базы
        /// </summary>
        public string DatabaseCaption { get; set; }

        /// <summary>
        /// Урл на страницу ифн. баз
        /// </summary>
        public string UrlForAccountDatabasesPage { get; set; }
    }
}
