﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скором изменении стоимости сервиса
    /// </summary>
    public class BeforeChangeServiceCostLetterModelDto : BeforeChangeServiceBaseLetterModelDto, ILetterTemplateModel
    {
        public BeforeChangeServiceCostLetterModelDto()
        {
            EmailsToCopy = [];
            ServiceTypeCostChanges = [];
        }
        
        /// <summary>
        /// Дата изменения стоимости сервиса
        /// </summary>
        public DateTime ServiceCostChangeDate { get; set; }
        
        /// <summary>
        /// Изменения стоимости услуг
        /// </summary>
        public List<ServiceTypeCostChangedDto> ServiceTypeCostChanges { get; set; }
    }
}
