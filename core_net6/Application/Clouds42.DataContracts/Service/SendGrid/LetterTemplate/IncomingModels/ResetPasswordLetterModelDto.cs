﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о восстановлении пароля пользователя
    /// </summary>
    public class ResetPasswordLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string AccountUserLogin { get; set; }

        /// <summary>
        /// Внешний клиент(источник регистрации)
        /// </summary>
        public ExternalClient ExternalClient { get; set; }

        /// <summary>
        /// Урл для сброса/восстановления пароля
        /// </summary>
        public string UrlForResetPassword { get; set; }
    }
}
