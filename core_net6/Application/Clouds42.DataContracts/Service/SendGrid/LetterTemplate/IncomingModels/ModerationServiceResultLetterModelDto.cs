﻿using System;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма для отправки результата модерации сервиса
    /// </summary>
    public class ModerationServiceResultLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Результат модерации
        /// </summary>
        public ChangeServiceRequestModerationResultDto ModerationResult { get; set; }

        /// <summary>
        /// Ссылка для открытия карточки сервиса
        /// </summary>
        public string UrlForOpenBillingServiceCard { get; set; }
    }
}
