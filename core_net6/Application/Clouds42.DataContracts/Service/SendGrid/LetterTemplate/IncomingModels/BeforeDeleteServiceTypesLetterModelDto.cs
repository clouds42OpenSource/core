﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скором удалении услуг сервиса
    /// </summary>
    public class BeforeDeleteServiceTypesLetterModelDto : BeforeChangeServiceBaseLetterModelDto, ILetterTemplateModel
    {
        public BeforeDeleteServiceTypesLetterModelDto()
        {
            EmailsToCopy = [];
            ServiceTypeNamesToRemove = [];
        }
        
        /// <summary>
        /// Дата удаления услуг
        /// </summary>
        public DateTime ServiceTypesDeleteDate { get; set; }

        /// <summary>
        /// Названия услуг которые будут удалены
        /// </summary>
        public List<string> ServiceTypeNamesToRemove { get; set; }
    }
}
