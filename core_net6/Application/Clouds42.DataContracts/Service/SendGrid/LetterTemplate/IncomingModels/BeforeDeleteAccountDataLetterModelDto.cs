﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скором удалении данных аккаунта
    /// </summary>
    public class BeforeDeleteAccountDataLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество дней до удаления
        /// </summary>
        public int DaysToDelete { get; set; }
    }
}
