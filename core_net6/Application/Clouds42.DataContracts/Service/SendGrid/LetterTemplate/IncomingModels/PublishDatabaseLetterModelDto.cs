﻿using System;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о публикации базы
    /// </summary>
    public class PublishDatabaseLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// Веб ссылка для запуска инф. базы
        /// </summary>
        public string AccountDatabasePublishPath { get; set; }
    }
}
