﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.Interface;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о скорых работах (ТиИ/АО) с базой
    /// </summary>
    public class BeforeSupportDatabaseLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Список названий баз, в которых будет проведено ТиИ
        /// </summary>
        public IEnumerable<string> TehSupportDatabaseNames { get; set; } = new List<string>();

        /// <summary>
        /// Список баз, в которых будет проведено AO
        /// </summary>
        public IEnumerable<DatabaseSupportInfoDto> AutoUpdateDatabases { get; set; } = new List<DatabaseSupportInfoDto>();

        public DatabaseSupportOperation SupportOperation { get; set; }
    }
}
