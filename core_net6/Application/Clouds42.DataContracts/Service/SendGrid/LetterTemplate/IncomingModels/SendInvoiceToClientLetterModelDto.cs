﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма для отправки счета клиенту
    /// </summary>
    public class SendInvoiceToClientLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя аккаунта
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Id счета
        /// </summary>
        public Guid InvoiceId { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        public decimal InvoiceSum { get; set; }

        /// <summary>
        /// Урл страницы баланса
        /// </summary>
        public string BalancePageUrl { get; set; }
        
        /// <summary>
        /// Документ счета
        /// </summary>
        public DocumentDataDto InvoiceDocument { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; } = [];
    }
}
