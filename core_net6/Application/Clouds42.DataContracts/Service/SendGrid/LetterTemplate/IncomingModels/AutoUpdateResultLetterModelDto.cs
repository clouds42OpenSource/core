﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о результате проведения АО
    /// </summary>
    public class AutoUpdateResultLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Урл на страницу ифн. баз
        /// </summary>
        public string UrlForAccountDatabasesPage { get; set; }

        /// <summary>
        /// Инф. базы, для которых АО завершилось успешно
        /// </summary>
        public IEnumerable<AutoUpdateDatabaseSuccessesDto> AutoUpdateSuccessesDatabases { get; set; } = new List<AutoUpdateDatabaseSuccessesDto>();

        /// <summary>
        /// Инф. базы, для которых АО завершилось с ошибкой
        /// </summary>
        public IEnumerable<AutoUpdateDatabaseFailureDto> AutoUpdateFailureDatabases { get; set; } = new List<AutoUpdateDatabaseFailureDto>();
    }
}
