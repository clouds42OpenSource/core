﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о создании инф. базы
    /// </summary>
    public class CreateAccountDatabaseLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// Веб ссылка для запуска инф. базы
        /// </summary>
        public string AccountDatabasePublishPath { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; } = [];
    }
}
