﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о результате проведения ТиИ
    /// </summary>
    public class TehSupportResultLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Урл на страницу ифн. баз
        /// </summary>
        public string UrlForAccountDatabasesPage { get; set; }

        /// <summary>
        /// Названия инф. баз, которые успешно прошли ТиИ
        /// </summary>
        public IEnumerable<string> TehSupportSuccessesDatabaseNames { get; set; } = new List<string>();

        /// <summary>
        /// Инф. базы, для которых ТиИ завершилось с ошибкой
        /// </summary>
        public IEnumerable<TehSupportDatabaseFailureDto> TehSupportFailureDatabases { get; set; } = new List<TehSupportDatabaseFailureDto>();
    }
}
