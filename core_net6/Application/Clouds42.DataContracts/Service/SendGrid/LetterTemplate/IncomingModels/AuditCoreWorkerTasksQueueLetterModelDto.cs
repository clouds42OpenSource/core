﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о результатах аудита очереди задач воркеров
    /// </summary>
    public class AuditCoreWorkerTasksQueueLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Количество задач в очереди со статусом "Новая"
        /// </summary>
        public int JobsWithStatusNewCount { get; set; }

        /// <summary>
        /// Данные по очереди задач для каждого воркера
        /// </summary>
        public IEnumerable<AuditCoreWorkerTasksQueueResultDto> AuditCoreWorkerTasksQueueResults { get; set; } = new List<AuditCoreWorkerTasksQueueResultDto>();
    }
}
