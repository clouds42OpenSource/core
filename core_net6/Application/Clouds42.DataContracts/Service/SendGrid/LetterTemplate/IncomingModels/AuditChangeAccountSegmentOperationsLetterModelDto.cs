﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Account.AccountAudit;
using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels
{
    /// <summary>
    /// Модель письма о результатах аудита операций по смене сегмента у аккаунтов
    /// </summary>
    public class AuditChangeAccountSegmentOperationsLetterModelDto : ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Электронный адрес получателя
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Аккаунты с неверным сегментом после миграции
        /// </summary>
        public List<AccountWithInvalidSegmentAfterMigrationDto> AccountsWithInvalidSegmentAfterMigration { get; set; } =
            [];
    }
}
