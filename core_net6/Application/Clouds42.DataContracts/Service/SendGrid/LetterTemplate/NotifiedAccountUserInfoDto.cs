﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Информация об уведомляемом пользователе
    /// </summary>
    public class NotifiedAccountUserInfoDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
        
        /// <summary>
        /// Пользователь отписался от уведомлений
        /// </summary>
        public bool? IsUnsubscribed { get; set; }

        /// <summary>
        /// Почта пользователя
        /// </summary>
        public string Email { get; set; }
        
        public string EmailStatus { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// Валюта локали
        /// </summary>
        public string LocaleCurrency { get; set; }

        /// <summary>
        /// Электронные адреса в копию
        /// </summary>
        public List<string> EmailsToCopy { get; set; } = [];

        public List<Guid> UserIdsToSend { get; set; }
    }
}
