﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель контактной информации компании для письма
    /// </summary>
    public class CompanyContactInfoForLetterDto
    {
        /// <summary>
        /// Эл. почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Ссылка на сайт
        /// </summary>
        public string SiteLink { get; set; }

        /// <summary>
        /// Ссылка на фейсбук
        /// </summary>
        public string FacebookLink { get; set; }

        /// <summary>
        /// Ссылка на инстаграм
        /// </summary>
        public string InstagramLink { get; set; }

        /// <summary>
        /// Ссылка на телеграм
        /// </summary>
        public string TelegramLink { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Необходимоить показывать блок социальных сетей
        /// </summary>
        public bool NeedShowSocialMediaBlock { get; set; }
    }
}
