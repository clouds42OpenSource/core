﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Общие данные для шаблона письма
    /// </summary>
    public class LetterTemplateCommonDataDto
    {
        /// <summary>
        /// Модель данных письма
        /// </summary>
        public LetterTemplateDataModelDto LetterTemplateData { get; set; }

        /// <summary>
        /// Модель данных рекламного баннера
        /// </summary>
        public AdvertisingBannerDataModelDto AdvertisingBannerData { get; set; }

        /// <summary>
        /// Данные компании
        /// </summary>
        public CompanyDetailsDto CompanyDetails { get; set; }
    }
}
