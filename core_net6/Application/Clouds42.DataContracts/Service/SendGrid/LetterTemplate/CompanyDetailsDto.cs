﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Данные компании
    /// </summary>
    public class CompanyDetailsDto : CompanyContactInfoForLetterDto
    {
        /// <summary>
        /// Ссылка для отписки от рассылки
        /// </summary>
        public string UnsubscribeLink { get; set; }

        /// <summary>
        /// Текст ссылки для отписки от рассылки
        /// </summary>
        public string UnsubscribeText { get; set; }
    }
}
