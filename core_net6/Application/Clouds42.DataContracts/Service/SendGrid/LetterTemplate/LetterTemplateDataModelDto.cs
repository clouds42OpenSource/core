﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель данных письма 
    /// </summary>
    public class LetterTemplateDataModelDto
    {
        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Заголовок письма
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Тело письма
        /// </summary>
        public string Body { get; set; }

        public string NotificationText { get; set; }
    }
}
