﻿using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель данных шаблона письма, которая используется для уведомления
    /// </summary>
    public class LetterTemplateDataForNotificationDto : LetterTemplateBaseDto, ILetterTemplateDataForNotificationModelDto
    {
        /// <summary>
        /// Тело письма
        /// </summary>
        public string Body { get; set; }
    }
}
