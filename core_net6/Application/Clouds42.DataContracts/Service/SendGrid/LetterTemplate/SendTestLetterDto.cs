﻿namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель данных для отправки тестового письма
    /// </summary>
    public class SendTestLetterDto 
    {
        /// <summary>
        /// Модель данных шаблона письма, которая используется для уведомления 
        /// </summary>
        public EditLetterTemplateDto LetterTemplateDataForNotification { get; set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

    }
}
