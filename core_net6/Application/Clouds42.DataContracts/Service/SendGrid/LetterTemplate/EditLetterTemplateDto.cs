﻿using Clouds42.DataContracts.Service.SendGrid.Interface;

namespace Clouds42.DataContracts.Service.SendGrid.LetterTemplate
{
    /// <summary>
    /// Модель редактирования шаблона письма
    /// </summary>
    public class EditLetterTemplateDto : LetterTemplateBaseDto, ILetterTemplateDataForNotificationModelDto
    {
        /// <summary>
        /// Тело письма
        /// </summary>
        public string Body { get; set; }

    }
}