﻿using System;

namespace Clouds42.DataContracts.Service.SendGrid.Interface
{
    /// <summary>
    /// Информация об уведомляемом пользователе
    /// </summary>
    public interface ILetterTemplateModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        Guid AccountId { get; set; }
    }
}
