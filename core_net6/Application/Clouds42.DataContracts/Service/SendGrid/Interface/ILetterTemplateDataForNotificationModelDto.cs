﻿namespace Clouds42.DataContracts.Service.SendGrid.Interface
{
    /// <summary>
    /// Модель данных шаблона письма, для уведомления 
    /// </summary>
    public interface ILetterTemplateDataForNotificationModelDto
    {
        /// <summary>
        /// Id шаблона письма
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        /// Заголовок письма
        /// </summary>
        string Header { get; set; }

        /// <summary>
        /// ID шаблона письма в SendGrid
        /// </summary>
        string SendGridTemplateId { get; set; }

        /// <summary>
        /// Шаблон рекламного баннера
        /// </summary>
        int? AdvertisingBannerTemplateId { get; set; }

        /// <summary>
        /// Тело письма
        /// </summary>
        string Body { get; set; }

        string NotificationText { get; set; }
    }
}
