﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель свойств элемента отрасли для обновления или добавления
    /// </summary>
    public sealed class UpdateIndustryDataItemDto: IIndustry
    {
        /// <inheritdoc />
        [Required(ErrorMessage = "ID отрасли - обязательное поле")]
        public Guid Id { get; set; }

        /// <inheritdoc />
        [Required(ErrorMessage = "Название отрасли - обязательное поле")]
        [Display(Name = "Название отрасли")]
        [StringLength(255, ErrorMessage = "Название отрасли - длина не должна превышать 255 символов")]
        public string Name { get; set; }

        /// <inheritdoc />
        [Display(Name = "Описание отрасли")]
        [StringLength(int.MaxValue)]
        public string Description { get; set; }
    }
}