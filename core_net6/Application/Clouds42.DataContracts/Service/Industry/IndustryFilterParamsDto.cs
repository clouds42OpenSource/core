﻿namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Фильтр для выбора отраслей
    /// </summary>
    public sealed class IndustryFilterParamsDto
    {
        /// <summary>
        /// Название отрасли
        /// </summary>
        public string IndustryName { get; set; }
    }
}