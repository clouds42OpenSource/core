﻿using System;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель результата на добавление новой отрасли
    /// </summary>
    public sealed class AddIndustryResultDto
    {
        /// <summary>
        /// ID отрасли после добавления
        /// </summary>
        public Guid IndustryId { get; set; }
    }
}