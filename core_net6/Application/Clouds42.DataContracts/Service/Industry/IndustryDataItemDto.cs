﻿using System;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель свойств элемента отрасли 
    /// </summary>
    public sealed class IndustryDataItemDto: IIndustry
    {
        /// <inheritdoc />
        public Guid Id { get; set; }
        
        /// <inheritdoc />
        public string Name { get; set; }

        /// <inheritdoc />
        public string Description { get; set; }
    }
}