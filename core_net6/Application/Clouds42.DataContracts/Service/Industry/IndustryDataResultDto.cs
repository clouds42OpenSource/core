﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель ответа на запрос получения отраслей
    /// </summary>
    public sealed class IndustryDataResultDto: SelectDataResultCommonDto<IndustryDataItemDto>
    {
    }
}