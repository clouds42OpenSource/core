﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель выбора записей отраслей
    /// </summary>
    public sealed class GetIndustryParamsDto: SelectDataCommonDto<IndustryFilterParamsDto>
    {
    }
}