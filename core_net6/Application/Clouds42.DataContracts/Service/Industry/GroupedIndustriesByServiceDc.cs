﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Сгрупированные отрасли по сервису
    /// </summary>
    public class GroupedIndustriesByServiceDc
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Отрасли сервиса
        /// </summary>
        public List<Domain.DataModels.Industry> Industries { get; set; } = [];
    }
}
