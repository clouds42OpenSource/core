﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель свойств элемента отрасли для удаления
    /// </summary>
    public sealed class DeleteIndustryDataItemDto
    {
        /// <summary>
        /// Id отрасли для удаления
        /// </summary>
        [Required(ErrorMessage = "ID отрасли - обязательное поле")]
        public Guid IndustryId { get; set; }
    }
}