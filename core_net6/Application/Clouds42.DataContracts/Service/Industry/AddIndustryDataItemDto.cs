﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.Service.Industry
{
    /// <summary>
    /// Модель свойств элемента отрасли для обновления или добавления
    /// </summary>
    public class AddIndustryDataItemDto 
    {
        /// <summary>
        /// Название отрасли
        /// </summary>
        [Required(ErrorMessage = "Название отрасли - обязательное поле")]
        [Display(Name = "Название отрасли")]
        [StringLength(255, ErrorMessage = "Название отрасли - длина не должна превышать 255 символов")]
        public string Name { get; set; }

        /// <summary>
        /// Описание отрасли
        /// </summary>
        [Display(Name = "Описание отрасли")]
        [StringLength(int.MaxValue)]
        public string Description { get; set; }
    }
}