﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.Common.DataModels;
using Clouds42.Domain.Access;

namespace Clouds42.DataContracts.Access
{
    /// <summary>
    /// Модель пользователя
    /// </summary>
    public class AccountUserPrincipalDto : IUserPrincipalDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Токен пользователя
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid RequestAccountId { get; set; }

        /// <summary>
        /// Группы пользователя
        /// </summary>
        public List<AccountUserGroup> Groups { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID аккаунта в контексте
        /// </summary>
        public Guid ContextAccountId { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Почта пользователя
        /// </summary>
        public string Email { get; set; }

        public bool? IsEmailVerified { get; set; }

        public bool? IsPhoneVerified { get; set; }

        public bool IsNew { get; set; }

        public string Locale { get; set; }
        public string UserSource { get; set; }

        public bool IsService { get; set; }
        public int? Deployment { get; set; }

        public string FirstName { get; set; }

        /// <summary>
        /// Проверить что есть роль только "Пользователь"
        /// </summary>
        /// <returns>true - если установлена только роль "Пользователь"</returns>
        public bool IsAccountUser()
            => Groups.All(g => g == AccountUserGroup.AccountUser);
    }
}
