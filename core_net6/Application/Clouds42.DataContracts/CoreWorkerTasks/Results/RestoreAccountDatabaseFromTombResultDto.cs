﻿namespace Clouds42.DataContracts.CoreWorkerTasks.Results
{
    /// <summary>
    /// Результат восстановления базы из бэкапа
    /// </summary>
    public class RestoreAccountDatabaseFromTombResultDto
    {
        /// <summary>
        /// Результат выполнения
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
