﻿using System;

namespace Clouds42.DataContracts.CoreWorkerTasks.Parameters
{
    /// <summary>
    /// Ключ синхронизации задач воркера
    /// </summary>
    public static class TaskSynchronizationKeyDto
    {
        /// <summary>
        /// Редактирование пользователя в AD
        /// </summary>
        public static class EditAccountEntities
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="accountId">ID аккаунта</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountId)
                => $"EditAccountEntities->{accountId}";
        }

        /// <summary>
        /// Управление доступом к директории AD
        /// </summary>
        public static class RuleUserAccessDirectoryInAd
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="userName">Имя пользователя</param>
            /// <param name="directoryPath">Путь</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(string userName, string directoryPath)
                => $"RuleUserAccessDirectory->{userName}-{directoryPath}";
        }

        /// <summary>
        /// Управление доступом к группе AD
        /// </summary>
        public static class RuleGroupAccessDirectoryInAd
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="grName">Имя группы</param>
            /// <param name="directoryPath">Путь</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(string grName, string directoryPath)
                => $"RuleGroupAccessDirectory->{grName}-{directoryPath}";
        }

        /// <summary>
        /// Управление редиректом для инф. базы
        /// </summary>
        public static class ManageAcDbRedirect
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="v82Name">Номер базы</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(string v82Name)
                => $"Publication->{v82Name}";
        }

        /// <summary>
        /// Публикация инф. баз
        /// </summary>
        public static class PublishAccountDatabases
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="accountDatabaseId">ID базы</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountDatabaseId)
                => $"ManageAppOnIis->{accountDatabaseId}";
        }

        /// <summary>
        /// Управление состоянием блокировки пользователя в МС
        /// </summary>
        public static class ManageAccountUserLockState
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="accountUserId">ID пользователя</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountUserId)
                => $"ManageLockState=>{accountUserId}";
        }

        /// <summary>
        /// Управление сервером ARR
        /// </summary>
        public static class ManageArrServer
        {
            /// <summary>
            /// Создать ключ
            /// </summary>
            /// <param name="id">ID сервера</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid id)
                => $"ARRServer=>{id}";
        }

        /// <summary>
        /// Управление расширением сервиса
        /// для информационной базы в МС
        /// </summary>
        public static class ManageServiceExtensionDatabase
        {
            /// <summary>
            /// Создать ключ синхронизации
            /// </summary>
            /// <param name="accountDatabaseId">ID информационной базы</param>
            /// <param name="serverId">ID сервера</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountDatabaseId, Guid serverId) =>
                $"ManageServiceExtension_{accountDatabaseId}_{serverId}";
        }

        /// <summary>
        /// Установка статуса активации расширения сервиса для инф. базы
        /// </summary>
        public static class SetServiceExtensionDatabaseActivationStatus
        {
            /// <summary>
            /// Создать ключ синхронизации
            /// </summary>
            /// <param name="accountDatabaseId">ID информационной базы</param>
            /// <param name="serverId">ID сервера</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountDatabaseId, Guid serverId) =>
                $"SetServiceExtensionActivationStatus_{accountDatabaseId}_{serverId}";
        }

        /// <summary>
        /// Управление доступами в инф. базу
        /// </summary>
        public static class ManageAcDbAccesses
        {
            /// <summary>
            /// Создать ключ синхронизации
            /// </summary>
            /// <param name="accountDatabaseId">ID информационной базы</param>
            /// <returns>Ключ синхронизации</returns>
            public static string CreateKey(Guid accountDatabaseId) =>
                $"ManageAcDbAccesses_{accountDatabaseId}";
        }

        /// <summary>
        /// Экспорт данных пользователей в BigQuery
        /// </summary>
        public static class ExportAccountUsersDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportAccountUsersDataToBigQuery);
        }

        /// <summary>
        /// Экспорт данных аккаунтов в BigQuery
        /// </summary>
        public static class ExportAccountsDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportAccountsDataToBigQuery);
        }

        /// <summary>
        /// Экспорт данных ресурсов в BigQuery
        /// </summary>
        public static class ExportResourcesDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportResourcesDataToBigQuery);
        }

        /// <summary>
        /// Экспорт данных счетов на оплату в BigQuery
        /// </summary>
        public static class ExportInvoicesDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportInvoicesDataToBigQuery);
        }

        /// <summary>
        /// Экспорт данных платежей в BigQuery
        /// </summary>
        public static class ExportPaymentsDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportPaymentsDataToBigQuery);
        }

        /// <summary>
        /// Экспорт данных инф. баз в BigQuery
        /// </summary>
        public static class ExportDatabasesDataToBigQuery
        {
            /// <summary>
            /// Ключ синхронизации
            /// </summary>
            public const string Key = nameof(ExportDatabasesDataToBigQuery);
        }
    }
}
