﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.CoreWorkerTasks.Parameters
{
    /// <summary>
    ///     Модель параметров воркера для таски RestoreAccountDatabaseFromTombJob
    /// </summary>
    public class RestoreAccountDatabaseFromTombWorkerTaskParam
    {
        /// <summary>
        /// Признак необходимости менять статус базы
        /// </summary>
        public bool NeedChangeState { get; set; }

        /// <summary>
        /// Признак необходимости отправлять уведомление об ошибке
        /// </summary>
        public bool SendMailAboutError { get; set; }

        /// <summary>
        /// Тип восстановления
        /// </summary>
        public AccountDatabaseBackupRestoreType RestoreType { get; set; }

        /// <summary>
        /// Список ID паользователей для предоставления доступа
        /// </summary>
        public List<Guid> UsersIdForAddAccess { get; set; } = [];

        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseName { get; set; }
    }
}