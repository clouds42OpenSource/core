﻿using System;

namespace Clouds42.DataContracts.CoreWorkerTasks.Parameters
{
    /// <summary>
    /// Модель входных параметров джобы по отправки бэкапов в склеп.
    /// </summary>
    public class SendBackupAccountDatabaseToTombJobParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Признак что необходимо использовать принудительную загрузку
        /// </summary>
        public bool ForceUpload { get; set; }
    }
}
