﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.MyDisk
{
    /// <summary>
    /// Model to pay My Disk service tariff
    /// </summary>
    public class MyDiskChangeTariffDto
    {
        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// New disk size in GB
        /// </summary>
        [Required]
        public int SizeGb { get; set; }

        /// <summary>
        /// Use Promise Payment
        /// </summary>
        public bool ByPromisedPayment { get; set; }
    }
}
