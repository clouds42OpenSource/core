﻿using System;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;

namespace Clouds42.DataContracts.MyDisk
{
    public class ServiceMyDiskDto
    {      

        /// <summary>
        /// Service expire date
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Is the service blocked
        /// </summary>
        public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// Monthly fee
        /// </summary>
        public decimal MonthlyCost { get; set; }

        /// <summary>
        /// Locale info
        /// </summary>
        public LocaleDto Locale { get; set;}

        /// <summary>
        /// Current plan (size in GB)
        /// </summary>
        public int CurrentTariff { get; set; }        

        /// <summary>
        /// My Disk service info
        /// </summary>
        public MyDiskVolumeInfoDto MyDiskVolumeInfo { get; set; }

        /// <summary>
        /// Data relevance (date of counting client files)
        /// </summary>
        public DateTime? ClinetFilesCalculateDate { set; get; }        

        /// <summary>
        /// Is VIP account
        /// </summary>
        public bool AccountIsVip { get; set; }

        /// <summary>
        /// Identifier of the disk space recalculation task
        /// </summary>
        public Guid? RecalculationTaskId { get; set; }
    }
}
