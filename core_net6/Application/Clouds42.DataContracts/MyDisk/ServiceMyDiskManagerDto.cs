﻿using System;
using Clouds42.DataContracts.Account.Locale;

namespace Clouds42.DataContracts.MyDisk
{
    /// <summary>
    /// "My Disk" service management model
    /// </summary>
    public class ServiceMyDiskManagerDto
    {
        /// <summary>
        /// Is account VIP
        /// </summary>
        public bool AccountIsVip { get; set; } 
        
        /// <summary>
        /// Account locale
        /// </summary>
        public LocaleDto Locale { get; set; }    

        /// <summary>
        /// "My Disk" tarrif (disk GB size)
        /// </summary>
        public int Tariff { get; set; }

        /// <summary>
        /// Prolongation expire date
        /// </summary>
        public DateTime ExpireDate { get; set; }
        
        /// <summary>
        /// Discount group
        /// </summary>
        public int? DiscountGroup { get; set; }    
        
        /// <summary>
        /// Service cost
        /// </summary>
        public decimal? Cost { get; set; }
    }
}
