﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Common.Extensions;

namespace Clouds42.DataContracts.MyDisk
{
    /// <summary>
    /// Структура дискового пространства.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class MyDiskStructureModelDto
    {
        [XmlElement(ElementName = nameof(CatalogItem))]
        [DataMember(Name = nameof(CatalogItem))]
        public List<CatalogItem> CatalogItems { set; get; } = [];
    }

    [XmlRoot(ElementName = nameof(CatalogItem))]
    [DataContract(Name = nameof(CatalogItem))]
    public class CatalogItem
    {

        /// <summary>
        /// Тип элемента: Папка/файла/инф.База.
        /// </summary>
        [XmlElement(ElementName = nameof(Type))]
        [DataMember(Name = nameof(Type))]
        public string Type { get; set; }

        /// <summary>
        /// Имя папки.
        /// </summary>
        [XmlElement(ElementName = nameof(Name))]
        [DataMember(Name = nameof(Name))]
        public string Name { get; set; }

        /// <summary>
        /// Полная ссылка до элемента.
        /// </summary>
        [XmlElement(ElementName = nameof(Link))]
        [DataMember(Name = nameof(Link))]
        public string Link { get; set; }

        [XmlElement(ElementName = nameof(ChortName))]
        [DataMember(Name = nameof(ChortName))]
        public string ChortName
        {
            get
            {
                if (string.IsNullOrEmpty(Name) || Name.Length < 15)
                    return Name;

                return $"{Name[..14]}...";
            }
        }

        [XmlElement(ElementName = nameof(SizeInMb))]
        [DataMember(Name = nameof(SizeInMb))]
        public long SizeInMb { get; set; }

        [XmlIgnore]
        public string SizeRepresentation
        {
            get { return SizeInMb.MegaByteToClassicFormat(2); }
        }
    }
}
