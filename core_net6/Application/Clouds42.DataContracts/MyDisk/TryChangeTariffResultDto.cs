﻿namespace Clouds42.DataContracts.MyDisk
{
    public class TryChangeTariffResultDto
    {
        public bool Complete { get; set; }
        public string Error { get; set; }

        /// <summary>
        /// Недостаточно средств на счету.
        /// </summary>
        public decimal EnoughMoney { get; set; }

        /// <summary>
        /// Возможность воспользоваться обещанным платежем.
        /// </summary>
        public bool CanGetPromisePayment { get; set; }

        /// <summary>
        /// Стоимость которую нужно доплатить для активации тарифа.
        /// </summary>
        public decimal CostOftariff { get; set; }

        /// <summary>
        /// Ежемесячный платеж по тарифу.
        /// </summary>
        public decimal MonthlyCost { get; set; }

        /// <summary>
        /// Валюта аккаунта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Общая сумма на балансе
        /// </summary>
        public decimal Balance { get; set; }
    }
}
