﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.MyDisk
{
    
    /// <summary>
    /// Модель состояние дискового пространства.
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class MyDiskInfoModelDto
    {     

        /// <summary>
        /// Свободный объем дискового пространства.
        /// </summary>
        [XmlElement(ElementName = nameof(FreeSizeOnDisk))]
        [DataMember(Name = nameof(FreeSizeOnDisk))]
        public long FreeSizeOnDisk { get; set; }

        /// <summary>
        /// Используемый объем дискового пространства.
        /// </summary>
        [XmlElement(ElementName = nameof(UsedSizeOnDisk))]
        [DataMember(Name = nameof(UsedSizeOnDisk))]
        public long UsedSizeOnDisk { get; set; }


        /// <summary>
        /// Купленный объем клиентом. (МБ)
        /// </summary>
        [XmlElement(ElementName = nameof(PayedSize))]
        [DataMember(Name = nameof(PayedSize))]
        public long PayedSize { get; set; }

        /// <summary>
        /// Общий объем доступный клиенту. (МБ)
        /// </summary>
        [XmlElement(ElementName = nameof(AvailableSize))]
        [DataMember(Name = nameof(AvailableSize))]
        public long AvailableSize { get; set; }

        /// <summary>
        /// Используемое серверное простраснтво. (МБ)
        /// </summary>
        [XmlElement(ElementName = nameof(ServerDataBaseSize))]
        [DataMember(Name = nameof(ServerDataBaseSize))]
        public long ServerDataBaseSize { get; set; }

        /// <summary>
        /// Используемое пространство под файловые базы. (МБ)
        /// </summary>
        [XmlElement(ElementName = nameof(FileDataBaseSize))]
        [DataMember(Name = nameof(FileDataBaseSize))]
        public long FileDataBaseSize { get; set; } 
        
        /// <summary>
        /// Актуальность данных. (Дата подсчета клиентских файлов).
        /// </summary>
        [XmlElement(ElementName = nameof(ClinetFilesCalculateDate))]
        [DataMember(Name = nameof(ClinetFilesCalculateDate))]
        public DateTime? ClinetFilesCalculateDate { get; set; }

        /// <summary>
        /// Используемое место для клиенский файлов. (МБ)
        /// </summary>
        [XmlElement(ElementName = nameof(ClientFilesSize))]
        [DataMember(Name = nameof(ClientFilesSize))]
        public long ClientFilesSize { get; set; }

        /// <summary>
        /// Данные биллинга.
        /// </summary>
        [XmlElement(ElementName = nameof(BillingInfo))]
        [DataMember(Name = nameof(BillingInfo))]
        public MyDiskBillingModelDto BillingInfo { get; set; }
    }

    [XmlRoot(ElementName = "BillingInfo")]
    [DataContract(Name = "BillingInfo")]
    public class MyDiskBillingModelDto
    {
        [XmlElement(ElementName = nameof(MonthlyCost))]
        [DataMember(Name = nameof(MonthlyCost))]
        public decimal MonthlyCost { get; set; }

        [XmlElement(ElementName = nameof(CurrentTariffInGb))]
        [DataMember(Name = nameof(CurrentTariffInGb))]
        public int CurrentTariffInGb { get; set; }

    }    

}