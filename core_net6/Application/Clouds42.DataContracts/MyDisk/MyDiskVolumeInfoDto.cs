﻿namespace Clouds42.DataContracts.MyDisk
{
    public class MyDiskVolumeInfoDto
    {
        /// <summary>
        /// Total available disk space (MB)
        /// </summary>
        public long AvailableSize { set; get; }

        /// <summary>
        /// Available (MB)
        /// </summary>
        public long FreeSizeOnDisk { set; get; }

        /// <summary>
        /// Used for server bases (MB)
        /// </summary>
        public long ServerDataBaseSize { set; get; }


        /// <summary>
        /// Used for file bases (MB)
        /// </summary>
        public long FileDataBaseSize { set; get; }

        /// <summary>
        /// Used for client files (MB)
        /// </summary>
        public long ClientFilesSize { set; get; }

        /// <summary>
        /// Used total size (MB)
        /// </summary>
        public long UsedSizeOnDisk => FileDataBaseSize + ClientFilesSize + ServerDataBaseSize;
    }
}
