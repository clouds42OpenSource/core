﻿using System;

namespace Clouds42.DataContracts.MyDisk
{
    /// <summary>
    /// Модель свойств сервиса "Мой диск" по аккаунту
    /// </summary>
    public class MyDiskPropertiesByAccountDto
    {
        /// <summary>
        /// Id аккаунта,
        /// которому принадлежат свойства
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Размер файлов на диске
        /// </summary>
        public long MyDiskFilesSizeInMb { get; set; }

        /// <summary>
        /// Актульная дата оценки занимаемого места на диске
        /// </summary>
        public DateTime? MyDiskFilesSizeActualDateTime { get; set; }
    }
}
