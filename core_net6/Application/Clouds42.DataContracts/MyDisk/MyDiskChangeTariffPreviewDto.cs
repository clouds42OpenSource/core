﻿namespace Clouds42.DataContracts.MyDisk
{
    public class MyDiskChangeTariffPreviewDto
    {
        public decimal Cost { get; set; }
        public MyDiskVolumeInfoDto MyDiskVolumeInfo { get; set; }
    }
}