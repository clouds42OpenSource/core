﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.ScramAuthorization
{
    /// <summary>
    /// Челлендж, сформированный сервером в ответ на запрос клиента
    /// </summary>
    [XmlRoot("challenge")]
    public class ServerChallengeDto
    {
        /// <summary>
        /// Кодированное сообщение SCRAM
        /// </summary>
        [XmlText]
        public string Message { get; set; }
    }
}
