﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.ScramAuthorization
{
    /// <summary>
    /// Финальное сообщение клиента в процессе аутентификации.
    /// </summary>
    [XmlRoot("response")]
    public class ClientFinalMessageDto
    {
        /// <summary>
        /// Кодированное сообщение SCRAM
        /// </summary>
        [XmlText]
        public string Message { get; set; }
    }
}
