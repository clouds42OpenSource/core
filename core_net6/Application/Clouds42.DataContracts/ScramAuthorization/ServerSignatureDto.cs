﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.ScramAuthorization
{
    /// <summary>
    /// Сигнатура сервера
    /// </summary>
    [XmlRoot("success")]
    public class ServerSignatureDto
    {
        /// <summary>
        /// Кодированное сообщение SCRAM
        /// </summary>
        [XmlText]
        public string Message { get; set; }
    }
}
