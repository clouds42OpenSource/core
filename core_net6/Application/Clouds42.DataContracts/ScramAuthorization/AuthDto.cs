﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.ScramAuthorization
{
    /// <summary>
    /// Ответ клиента на серверный челлендж
    /// </summary>
    [XmlRoot("auth")]
    public class AuthDto
    {
        /// <summary>
        /// Требование протокола всегда указывать хэширующую функцию. В данном случае: "SCRAM-SHA-1"
        /// </summary>
        [XmlAttribute("mechanism")]
        public string Mechanism { get; set; } = "SCRAM-SHA-1";

        /// <summary>
        /// Кодированное сообщение SCRAM
        /// </summary>
        [XmlText]
        public string Message { get; set; }
    }
}
