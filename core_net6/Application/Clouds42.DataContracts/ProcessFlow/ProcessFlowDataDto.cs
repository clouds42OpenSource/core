﻿using System;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.DataContracts.ProcessFlow
{


    /// <summary>
    /// Контракт описывающий свойства рабочего процесса конечного автомата.
    /// </summary>
    public class ProcessFlowDataDto
    {

        /// <summary>
        /// Идентификатор процесса.
        /// </summary>        
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания процесса.
        /// </summary>
        public DateTime CreationDateTime { get; set; }        

        /// <summary>
        /// Имя процесса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Имя обрабатываемого процессом объекта.
        /// </summary>
        public string ProcessedObjectName { get; set; }

        /// <summary>
        /// Статус процесса.
        /// </summary>        
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// Описание статуса процесса.
        /// </summary>        
        public string StatusDescription => Status.Description();

        /// <summary>
        /// Описание состояния процесса.
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// Комментарий среды исполнения.
        /// </summary>        
        public string Comment { get; set; }
        
    }
}
