﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Модель данных процесса конечного автомата.
    /// </summary>
    public sealed class ProcessFlowDataModelDto: SelectDataResultCommonDto<ProcessFlowItemDto>
    {
    }
}