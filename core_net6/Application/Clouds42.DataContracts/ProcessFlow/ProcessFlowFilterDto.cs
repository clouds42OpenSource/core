﻿using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Модель описывающая значения фильтра для списка рабочих процессов
    /// </summary>
    public sealed class ProcessFlowFilterDto
    {
        /// <summary>
        /// Статус компоменты конечного автомата
        /// </summary>
        public StateMachineComponentStatus? Status { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchLine { get; set; }
    }
}