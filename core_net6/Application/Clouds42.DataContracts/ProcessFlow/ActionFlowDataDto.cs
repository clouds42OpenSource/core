﻿using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Модель данных шага рабочего процесса
    /// </summary>
    public class ActionFlowDataDto
    {
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Статус шага
        /// </summary>
        public StateMachineComponentStatus Status { get; set; }

        /// <summary>
        /// Количество попыток выполнения
        /// </summary>
        public int CountOfAttemps { get; set; }

        /// <summary>
        /// Название шага
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Описание шага
        /// </summary>
        public string ActionDescription { get; set; }
    }
}
