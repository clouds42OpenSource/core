﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Модель деталей рабочего процесса
    /// </summary>
    public class ProcessFlowDetailsDto : ProcessFlowItemDto
    {
        /// <summary>
        /// Список шагов рабочего процесса
        /// </summary>
        public List<ActionFlowDataDto> ActionFlows { get; set; } = [];
    }
}
