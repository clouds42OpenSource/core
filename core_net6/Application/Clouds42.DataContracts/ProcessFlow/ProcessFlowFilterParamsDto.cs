﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Модель описывающая значения фильтра для списка рабочих процессов
    /// </summary>
    public sealed class ProcessFlowFilterParamsDto : SelectDataCommonDto<ProcessFlowFilterDto>
    {
    }
}