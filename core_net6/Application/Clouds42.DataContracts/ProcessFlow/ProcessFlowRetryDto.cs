﻿using System;

namespace Clouds42.DataContracts.ProcessFlow
{
    public class ProcessFlowRetryDto
    {
        public Guid ProcessFlowId { get; set; }
    }
}
