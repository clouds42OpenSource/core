﻿using Clouds42.Domain.Enums.StateMachine;

namespace Clouds42.DataContracts.ProcessFlow
{
    /// <summary>
    /// Контракт описывающий значения фильтра для списка рабочих процессов.
    /// </summary>
    public class ProcessFlowDataFilterDto
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Статус процесса.
        /// </summary>        
        public StateMachineComponentStatus? Status { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchQueryString { get; set; }
    }

}
