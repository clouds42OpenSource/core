﻿using System.Collections.Generic;
using Rates = Clouds42.Domain.DataModels.billing.Rate;

namespace Clouds42.DataContracts.Billing
{
    /// <summary>
    /// Модель услуги загрузки документов
    /// </summary>
    public class ViewPageEsdlDto
    {
        /// <summary>
        /// Количество лет на лицензии
        /// </summary>
        public int LicenseYears { get; set; } = 1;

        /// <summary>
        /// Стоимость лицензии
        /// </summary>
        public decimal LicenseCost { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PagesCount { get; set; } = 1000;

        /// <summary>
        /// Стоимость страницы
        /// </summary>
        public decimal PagesCost { get; set; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        public decimal? TotalAmount { get; set; }

        /// <summary>
        /// Доступные тарифы для
        /// </summary>
        public List<Rates> AvailableRatesForPages { get; set; }
    }
}
