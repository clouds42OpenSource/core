﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель результата калькуляции агентского вознаграждения
    /// </summary>
    public class CalculateAgentRewardResultDto(decimal rewardSum, Guid? accountId = null)
    {
        /// <summary>
        /// Сумма вознагрождения.
        /// </summary>
        public decimal RewardSum { get; } = rewardSum;

        /// <summary>
        /// Id аккаунта агента.
        /// </summary>
        public Guid? AccountId { get; } = accountId;
    }
}
