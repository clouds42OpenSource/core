﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель выбора записей агентских соглашений
    /// </summary>
    public sealed class GetAgencyAgreementParamsDto : SelectDataCommonDto<AgencyAgreementFilterParamsDto>
    {
    }
}