﻿namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Фильтр для выбора агентских соглашений
    /// </summary>
    public sealed class AgencyAgreementFilterParamsDto
    {
        /// <summary>
        /// Название агентского соглашения
        /// </summary>
        public string AgencyAgreementName { get; set; }
    }
}