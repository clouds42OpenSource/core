﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель свойств элемента агентского соглашения
    /// </summary>
    public sealed class AgencyAgreementDataItemDto : AgencyAgreementBaseDataDto
    {
        /// <summary>
        /// ID агентского соглашения
        /// </summary>
        public Guid Id{ get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}