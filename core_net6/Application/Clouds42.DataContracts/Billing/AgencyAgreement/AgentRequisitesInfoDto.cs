﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Информация по реквизитам агента
    /// </summary>
    public class AgentRequisitesInfoDto
    {
        /// <summary>
        /// Id реквизитов
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        [NotMapped]
        public string Number { get => $"{AccountOwnerIndexNumber}-{RequisitesNumber}"; }

        [JsonIgnore]
        public int AccountOwnerIndexNumber { get; set; }

        [JsonIgnore]
        public int RequisitesNumber { get; set; }

        /// <summary>
        /// Статус реквизитов
        /// </summary>
        public AgentRequisitesStatusEnum AgentRequisitesStatus { get; set; }

        /// <summary>
        /// Тип реквизитов
        /// </summary>
        public AgentRequisitesTypeEnum AgentRequisitesType { get; set; }

        /// <summary>
        /// Номер аккаунта партнера.
        /// </summary>
        public int PartnerAccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта партнера
        /// </summary>
        public string PartnerAccountCaption { get; set; }

        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Тип реквизитов (описание)
        /// </summary>
        public string AgentRequisitesTypeString => AgentRequisitesType.Description();

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }
    }
}
