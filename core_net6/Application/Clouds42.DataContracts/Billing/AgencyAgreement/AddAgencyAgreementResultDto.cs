﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель результата на добавление нового агентского соглашений
    /// </summary>
    public sealed class AddAgencyAgreementResultDto
    {
        /// <summary>
        /// ID агентского соглашений после добавления
        /// </summary>
        public Guid Id { get; set; }
    }
}