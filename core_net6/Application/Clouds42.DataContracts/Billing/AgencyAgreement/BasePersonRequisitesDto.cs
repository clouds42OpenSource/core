﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель базового объекта договора
    /// </summary>
    public class BasePersonRequisitesDto
    {
        /// <summary>
        /// Расчетный счет
        /// </summary>
        public string SettlementAccount { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        public string Bik { get; set; }

        /// <summary>
        /// Наименование банка
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Адрес для отправки документов
        /// </summary>
        public string AddressForSendingDocuments { get; set; }

        /// <summary>
        /// Файлы договора
        /// </summary>
        public List<CloudFileDataDto<byte[]>> Files { get; set; }
    }
}
