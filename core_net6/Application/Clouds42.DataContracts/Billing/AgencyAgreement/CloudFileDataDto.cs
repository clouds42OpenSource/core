﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Файл реквизитов
    /// </summary>
    public class CloudFileDataDto<TContent>
    {
        /// <summary>
        /// Id файла облака
        /// </summary>
        public Guid CloudFileId { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Массив байтов
        /// </summary>
        public TContent Content { get; set; }

        /// <summary>
        /// Формат файла
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Строка base 64
        /// </summary>
        public string Base64 { get; set; }

        /// <summary>
        /// Тип файла реквизитов агента
        /// </summary>
        public AgentRequisitesFileTypeEnum AgentRequisitesFileType { get; set; }
    }
}