﻿namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель реквизитов юридического лица
    /// </summary>
    public class LegalPersonRequisitesDto : BasePersonRequisitesDto
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// ФИО руководителя
        /// </summary>
        public string HeadFullName { get; set; }

        /// <summary>
        /// Должность руководителя
        /// </summary>
        public string HeadPosition { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }
    }
}
