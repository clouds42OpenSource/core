﻿using System;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель фильтра договоров партнера
    /// </summary>
    public class PartnersAgentRequisitesFilterDto : ISortingDto
    {
        /// <summary>
        /// Данные об аккаунте партнера
        /// </summary>
        public string PartnerAccountData { get; set; }

        /// <summary>
        /// Статус реквизитов агента
        /// </summary>
        public AgentRequisitesStatusEnum? AgentRequisitesStatus { get; set; }

        /// <summary>
        /// Тип реквизитов агента
        /// </summary>
        public AgentRequisitesTypeEnum? AgentRequisitesType { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Период создания с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период создания по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}