﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Данные агентских соглашений
    /// </summary>
    public class AgencyAgreementsDataDto
    {
        /// <summary>
        /// Информация об агентских соглашениях
        /// </summary>
        public List<AgencyAgreementInfoDto> AgencyAgreementInfos { get; set; } = [];
    }
}
