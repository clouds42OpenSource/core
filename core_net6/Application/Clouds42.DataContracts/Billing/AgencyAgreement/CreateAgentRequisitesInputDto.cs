﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums;
using Microsoft.AspNetCore.Http;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    public class CreateAgentRequisitesInputDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Режим редактирования
        /// </summary>
        public bool IsEditMode { get; set; }

        /// <summary>
        /// Тип реквизитов агента
        /// </summary>
        public AgentRequisitesTypeEnum AgentRequisitesType { get; set; }

        /// <summary>
        /// Статус реквизитов агента
        /// </summary>
        public AgentRequisitesStatusEnum AgentRequisitesStatus { get; set; }

        /// <summary>
        /// Модель реквизитов юридического лица
        /// </summary>
        public LegalPersonRequisitesInputDto LegalPersonRequisites { get; set; } = new();

        /// <summary>
        /// Модель реквизитов физического лица
        /// </summary>
        public PhysicalPersonRequisitesInputDto PhysicalPersonRequisites { get; set; } = new();

        /// <summary>
        /// Модель реквизитов ИП
        /// </summary>
        public SoleProprietorRequisitesInputDto SoleProprietorRequisites { get; set; } = new();

        /// <summary>
        /// Номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Cокращенное имя
        /// (Фамилия с инициалами)
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }
    }

    /// <summary>
    /// Модель реквизитов юридического лица
    /// </summary>
    public class LegalPersonRequisitesInputDto : BasePersonRequisitesInputDto
    {
        /// <summary>
        /// Наименование организации
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// ФИО руководителя
        /// </summary>
        public string HeadFullName { get; set; }

        /// <summary>
        /// Должность руководителя
        /// </summary>
        public string HeadPosition { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }
    }

    /// <summary>
    /// Модель реквизитов физического лица
    /// </summary>
    public class PhysicalPersonRequisitesInputDto : BasePersonRequisitesInputDto
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; } = DateTime.Now;

        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string PassportSeries { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string PassportNumber { get; set; }

        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        public string WhomIssuedPassport { get; set; }

        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime PassportDateOfIssue { get; set; } = DateTime.Now;

        /// <summary>
        /// Адрес прописки
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// СНИЛС
        /// </summary>
        public string Snils { get; set; }
    }

    /// <summary>
    /// Модель реквизитов ИП
    /// </summary>
    public class SoleProprietorRequisitesInputDto : BasePersonRequisitesInputDto
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }
    }

    /// <summary>
    /// Модель базового объекта договора
    /// </summary>
    public class BasePersonRequisitesInputDto
    {
        /// <summary>
        /// Расчетный счет
        /// </summary>
        public string SettlementAccount { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        public string Bik { get; set; }

        /// <summary>
        /// Наименование банка
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Адрес для отправки документов
        /// </summary>
        public string AddressForSendingDocuments { get; set; }

        /// <summary>
        /// Файлы договора
        /// </summary>
        public List<CloudFileDataDto<IFormFile>> Files { get; set; }
    }
}
