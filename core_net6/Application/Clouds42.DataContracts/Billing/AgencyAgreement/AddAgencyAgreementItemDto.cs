﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Model for adding new agency agreement
    /// </summary>
    public class AddAgencyAgreementItemDto
    {
        /// <summary>
        /// Agency agreement name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Print form identifier
        /// </summary>
        public Guid PrintedHtmlFormId { get; set; }

        /// <summary>
        /// Effective date
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Reward for the service "Rent 1C" (percentage)
        /// </summary>
        public decimal Rent1CRewardPercent { get; set; }

        /// <summary>
        /// Reward for the service "My Disk" (percentage)
        /// </summary>
        public decimal MyDiskRewardPercent { get; set; }

        /// <summary>
        /// Reward for developer service (percentage)
        /// </summary>
        public decimal ServiceOwnerRewardPercent { get; set; }

        /// <summary>
        /// Reward for "Rent 1C" service for VIP accounts (percentage)
        /// </summary>
        public decimal Rent1CRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Reward for "My Disk" service for VIP accounts (percentage)
        /// </summary>
        public decimal MyDiskRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Reward for developer service for VIP accounts (percentage)
        /// </summary>
        public decimal ServiceOwnerRewardPercentForVipAccounts { get; set; }
    }
}
