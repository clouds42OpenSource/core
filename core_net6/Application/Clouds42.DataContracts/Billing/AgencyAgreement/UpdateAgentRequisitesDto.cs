﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель обновления реквизитов агента
    /// </summary>
    public class UpdateAgentRequisitesDto(
        Guid? legalPersonRequisitesId,
        Guid? physicalPersonRequisitesId,
        Guid? soleProprietorRequisitesId)
    {
        /// <summary>
        /// Id реквизитов с типом ИП
        /// </summary>
        public Guid? SoleProprietorRequisitesId { get; set; } = soleProprietorRequisitesId;

        /// <summary>
        /// Id реквизитов с типом физ.лицо
        /// </summary>
        public Guid? PhysicalPersonRequisitesId { get; set; } = physicalPersonRequisitesId;

        /// <summary>
        /// Id реквизитов с типом юр.лицо
        /// </summary>
        public Guid? LegalPersonRequisitesId { get; set; } = legalPersonRequisitesId;
    }
}
