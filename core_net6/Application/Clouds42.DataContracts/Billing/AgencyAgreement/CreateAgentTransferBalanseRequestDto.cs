﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель создания заявки на перевод баланса агента
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CreateAgentTransferBalanseRequestDto
    {
        /// <summary>
        /// Сумма по заявке
        /// </summary>
        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public decimal Sum { get; set; }

        /// <summary>
        /// От аккаунта (перевод баланса)
        /// </summary>
        [XmlElement(ElementName = nameof(FromAccountId))]
        [DataMember(Name = nameof(FromAccountId))]
        public Guid FromAccountId { get; set; }

        /// <summary>
        /// Для аккаунта (перевод баласа)
        /// </summary>
        [XmlElement(ElementName = nameof(ToAccountId))]
        [DataMember(Name = nameof(ToAccountId))]
        public Guid ToAccountId { get; set; }

        /// <summary>
        /// Инициатор создания заявки
        /// </summary>
        [XmlElement(ElementName = nameof(InitiatorId))]
        [DataMember(Name = nameof(InitiatorId))]
        public Guid InitiatorId { get; set; }

        /// <summary>
        /// Комментарий к заявке
        /// </summary>
        [CanBeNull]
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }
    }
}
