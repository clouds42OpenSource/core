﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель реквизитов физического лица
    /// </summary>
    public class PhysicalPersonRequisitesDto : BasePersonRequisitesDto
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime DateOfBirth { get; set; } = DateTime.Now;

        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string PassportSeries { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string PassportNumber { get; set; }

        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        public string WhomIssuedPassport { get; set; }

        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime PassportDateOfIssue { get; set; } = DateTime.Now;

        /// <summary>
        /// Адрес прописки
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// СНИЛС
        /// </summary>
        public string Snils { get; set; }
    }
}
