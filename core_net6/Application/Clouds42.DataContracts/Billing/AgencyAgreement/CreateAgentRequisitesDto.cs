﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель создания реквизитов агента
    /// </summary>
    public class CreateAgentRequisitesDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Режим редактирования
        /// </summary>
        public bool IsEditMode { get; set; }

        /// <summary>
        /// Тип реквизитов агента
        /// </summary>
        public AgentRequisitesTypeEnum AgentRequisitesType { get; set; }

        /// <summary>
        /// Статус реквизитов агента
        /// </summary>
        public AgentRequisitesStatusEnum AgentRequisitesStatus { get; set; }

        /// <summary>
        /// Модель реквизитов юридического лица
        /// </summary>
        public LegalPersonRequisitesDto LegalPersonRequisites { get; set; } = new();

        /// <summary>
        /// Модель реквизитов физического лица
        /// </summary>
        public PhysicalPersonRequisitesDto PhysicalPersonRequisites { get; set; } = new();

        /// <summary>
        /// Модель реквизитов ИП
        /// </summary>
        public SoleProprietorRequisitesDto SoleProprietorRequisites { get; set; } = new();

        /// <summary>
        /// Номер
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Cокращенное имя
        /// (Фамилия с инициалами)
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}
