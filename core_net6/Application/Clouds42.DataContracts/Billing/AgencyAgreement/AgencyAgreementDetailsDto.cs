﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Детальная информация об
    /// агентском соглашении
    /// </summary>
    public class AgencyAgreementDetailsDto : CreateAgencyAgreementDto
    {
        /// <summary>
        /// Id агентского соглашения
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название агентского соглашения
        /// </summary>
        public new string Name { get; set; }

        /// <summary>
        /// Название печатной формы
        /// </summary>
        public string PrintedHtmlFormName { get; set; }
    }
}
