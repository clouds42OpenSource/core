﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Базовые данные агентского соглашения
    /// </summary>
    public class AgencyAgreementBaseDataDto
    {
        /// <summary>
        /// Название агентского соглашения
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Печатная форма
        /// </summary>
        public Guid PrintedHtmlFormId { get; set; }

        /// <summary>
        /// Дата вступления в силу
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Аренда 1С"
        /// (проценты)
        /// </summary>
        public decimal Rent1CRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Мой диск"
        /// (проценты)
        /// </summary>
        public decimal MyDiskRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис разработчика
        /// (проценты)
        /// </summary>
        public decimal ServiceOwnerRewardPercent { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Аренда 1С" для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal Rent1CRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Вознаграждение за сервис "Мой диск" для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal MyDiskRewardPercentForVipAccounts { get; set; }

        /// <summary>
        /// Вознаграждение за сервис разработчика для вип аккаунтов
        /// (проценты)
        /// </summary>
        public decimal ServiceOwnerRewardPercentForVipAccounts { get; set; }
    }
}
