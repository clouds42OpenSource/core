﻿using System;
using Clouds42.DataContracts.Service.PrintedHtmlForm;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель печати агентского соглашения
    /// </summary>
    public class PrintedAgencyAgreementDto : IPrintedHtmlFormModelDto
    {
        /// <summary>
        /// Адрес актуального агентского соглашения
        /// </summary>
        public string ActualAgencyAgreementUrl { get; set; }

        public void FillTestData(string siteAuthorityUrl = null, string localeName = null, string supplierReferenceName = null)
        {
            ActualAgencyAgreementUrl = 
                new Uri($"{siteAuthorityUrl}/partners/open-actual-agency-agreement").AbsoluteUri;

            SupplierReferenceName = supplierReferenceName;
        }

        public string GetModelName() => "Агентское соглашение";

        public string SupplierReferenceName { get; set; }

    }
}
