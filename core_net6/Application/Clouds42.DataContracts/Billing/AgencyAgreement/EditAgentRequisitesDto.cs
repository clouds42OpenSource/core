﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель для редактирования реквизитов агента
    /// </summary>
    public class EditAgentRequisitesDto : CreateAgentRequisitesDto
    {
        /// <summary>
        /// Id агентского договора
        /// </summary>
        public Guid Id { get; set; }
    }

    public class EditAgentRequisitesInputDto : CreateAgentRequisitesInputDto
    {
        /// <summary>
        /// Id агентского договора
        /// </summary>
        public Guid Id { get; set; }
    }

    
}
