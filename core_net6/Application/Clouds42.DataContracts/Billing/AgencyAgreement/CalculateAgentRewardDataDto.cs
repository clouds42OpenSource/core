﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель данных для калькуляции агентского вознаграждения
    /// </summary>
    public class CalculateAgentRewardDataDto
    {
        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType TransactionType { get; set; }

        /// <summary>
        /// Клиентский аккаунт является вип
        /// </summary>
        public bool IsVipClientAccount { get; set; }

        /// <summary>
        /// Владелец сервиса, по которому был совершен платеж
        /// </summary>
        public Guid? ServiceOwnerId { get; set; }

        /// <summary>
        /// Id реферального аккаунта
        /// </summary>
        public Guid? ReferralAccountId { get; set; }

        /// <summary>
        /// Id аккаунта агента, для которого начисляется вознаграждение
        /// </summary>
        public Guid? AgentAccountId { get; set; }

        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        public Clouds42Service? SystemService { get; set; }
    }
}
