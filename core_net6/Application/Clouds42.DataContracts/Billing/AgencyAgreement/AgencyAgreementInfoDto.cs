﻿using System;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель информации об агентском соглашении
    /// </summary>
    public class AgencyAgreementInfoDto
    {
        /// <summary>
        /// Id агентского соглашения
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата вступления в силу
        /// </summary>
        public DateTime EffectiveDate { get; set; }
    }
}
