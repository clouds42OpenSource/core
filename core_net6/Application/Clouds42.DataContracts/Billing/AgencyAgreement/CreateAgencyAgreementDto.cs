﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель создания агентского соглашения
    /// </summary>
    public class CreateAgencyAgreementDto : AgencyAgreementBaseDataDto
    {

        public CreateAgencyAgreementDto()
        {
            PrintedHtmlForms = new Dictionary<Guid, string>();
            EffectiveDate = DateTime.Now;
        }

        /// <summary>
        /// Печатные формы Html
        /// </summary>
        public Dictionary<Guid, string> PrintedHtmlForms { get; set; }
    }
}
