﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель ответа на запрос получения агентских соглашений
    /// </summary>
    public sealed class AgencyAgreementDataResultDto : SelectDataResultCommonDto<AgencyAgreementDataItemDto>
    {
    }
}