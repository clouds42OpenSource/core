﻿namespace Clouds42.DataContracts.Billing.AgencyAgreement
{
    /// <summary>
    /// Модель реквизитов ИП
    /// </summary>
    public class SoleProprietorRequisitesDto : BasePersonRequisitesDto
    {
        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Юридический адрес
        /// </summary>
        public string LegalAddress { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }
    }
}
