﻿using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.DataContracts.Billing
{
    /// <summary>
    /// Данные биллинга
    /// </summary>
    public class BillingDataDto
    {
        /// <summary>
        /// Является ли аккаунт вип
        /// </summary>
        public bool AccountIsVip { get; set; }

        /// <summary>
        /// Является ли аккаунт демо
        /// </summary>
        public bool AccountIsDemo { get; set; }

        /// <summary>
        /// Текущий баланс
        /// </summary>
        public decimal CurrentBalance { get; set; }

        /// <summary>
        /// Текущий баланс бонусов
        /// </summary>
        public decimal CurrentBonusBalance { get; set; }

        /// <summary>
        /// Регулярный платеж
        /// </summary>
        public decimal RegularPayment { get; set; }

        /// <summary>
        /// Наличие ошибки в платеже
        /// </summary>
        public bool HasErrorInPayment { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Обещанный платеж
        /// </summary>
        public PromisePaymentAvailableDto PromisePayment { get; set; }

        /// <summary>
        /// Возможность досрочного погашения платежа
        /// </summary>
        public bool CanEarlyPromisePaymentRepay { get; set; }

        /// <summary>
        /// Возможность продлить обещанный платеж
        /// </summary>
        public bool CanProlongPromisePayment { get; set; }

        /// <summary>
        /// Предлагаемый платеж
        /// </summary>
        public SuggestedPaymentModelDto SuggestedPayment { get; set; }
    }
}
