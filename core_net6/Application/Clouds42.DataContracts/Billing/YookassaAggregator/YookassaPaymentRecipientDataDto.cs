﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Данные о получателе платежа агрегатора ЮKassa
    /// </summary>
    public class YookassaPaymentRecipientDataDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [JsonProperty("account_id")]
        public string AccountId { get; set; }

        /// <summary>
        /// Id шлюза
        /// </summary>
        [JsonProperty("gateway_id")]
        public string GatewayId { get; set; }
    }
}
