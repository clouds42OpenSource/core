﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель платежной карты
    /// </summary>
    public class YookassaPaymentCardDto
    {
        /// <summary>
        /// Первые шесть цифр номера карты
        /// </summary>
        [JsonProperty("first6")]
        public string FirstSixDigits { get; set; }

        /// <summary>
        /// Последние 4 цифры номера карты
        /// </summary>
        [JsonProperty("last4")]
        public string LastFourDigits { get; set; }

        /// <summary>
        /// Месяц истечения срока действия карты
        /// </summary>
        [JsonProperty("expiry_month")]
        public string ExpiryMonth { get; set; }

        /// <summary>
        /// Год истечения срока действия карты
        /// </summary>
        [JsonProperty("expiry_year")]
        public string ExpiryYear { get; set; }

        /// <summary>
        /// Тип карты
        /// </summary>
        [JsonProperty("card_type")]
        public string CardType { get; set; }

        /// <summary>
        /// Страна эмитента
        /// </summary>
        [JsonProperty("issuer_country")]
        public string IssuerCountry { get; set; }

        /// <summary>
        /// Название банка
        /// </summary>
        [JsonProperty("issuer_name")]
        public string IssuerName { get; set; }
    }
}
