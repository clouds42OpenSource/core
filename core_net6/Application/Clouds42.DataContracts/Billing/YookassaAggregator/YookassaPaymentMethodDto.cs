﻿using System;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Метод платежа
    /// </summary>
    public class YookassaPaymentMethodDto
    {
        /// <summary>
        /// Тип способа оплаты
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Id шаблона платежа
        /// </summary>
        [JsonProperty("id")]
        public Guid PaymentTemplateId { get; set; }

        /// <summary>
        /// Признак тогда, что шаблоно сохранен
        /// </summary>
        [JsonProperty("saved")]
        public bool IsSaved { get; set; }

        /// <summary>
        /// Платежная карта
        /// </summary>
        [JsonProperty("card")]
        public YookassaPaymentCardDto Card { get; set; }
    }
}
