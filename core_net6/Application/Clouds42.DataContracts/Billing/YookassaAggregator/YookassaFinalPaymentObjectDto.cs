﻿using System;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Финальный объект платежа Юкасса
    /// </summary>
    public class YookassaFinalPaymentObjectDto : YookassaPaymentObjectBaseDto
    {
        /// <summary>
        /// Сведения об авторизации платежа
        /// </summary>
        [JsonProperty("authorization_details")]
        public YookassaPaymentAuthorizationDetailsDto AuthorizationDetails { get; set; }

        /// <summary>
        /// Дата захвата платежа
        /// </summary>
        [JsonProperty("captured_at")]
        public DateTime CapturedDate { get; set; }
        
        /// <summary>
        /// Сумма дохода
        /// </summary>
        [JsonProperty("income_amount")]
        public YookassaPaymentAmountDataDto IncomeAmount { get; set; }

        /// <summary>
        /// Метод оплаты
        /// </summary>
        [JsonProperty("payment_method")]
        public YookassaPaymentMethodDto PaymentMethod { get; set; }

        [JsonProperty("confirmation")]
        public YookassaConfirmationDto Confirmation { get; set; }
    }

    public class YookassaConfirmationDto
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("confirmation_token")]
        public string ConfirmationToken { get; set; }

        [JsonProperty("confirmation_url")]
        public string ConfirmationRedirectUrl { get; set; }
    }
}
