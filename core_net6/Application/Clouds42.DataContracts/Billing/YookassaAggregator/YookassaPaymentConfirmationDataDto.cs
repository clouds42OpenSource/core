﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Данные о подтверждении платежа агрегатора ЮKassa
    /// </summary>
    public class YookassaPaymentConfirmationDataDto
    {
        /// <summary>
        /// Тип платежа
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Токен подтверждения
        /// </summary>
        [JsonProperty("confirmation_token")]
        public string ConfirmationToken { get; set; }

        /// <summary>
        /// Урл подтверждения платежа
        /// </summary>
        [JsonProperty("confirmation_url")]
        public string ConfirmationUrl{ get; set; }

        /// <summary>
        /// Урл подтверждения платежа
        /// </summary>
        [JsonProperty("return_url")]
        public string ReturnUrl { get; set; }
    }
}
