﻿using JetBrains.Annotations;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Данные для подтверждения платежа ЮKassa клиентом
    /// </summary>
    public class ConfirmYookassaPaymentForClientDataDto : PaymentResultDto
    {
        /// <summary>
        /// Токен подтверждения
        /// </summary>
        public string ConfirmationToken { get; set; }

        /// <summary>
        /// Урл на который пользователь вернется после оплаты 
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Тип подтверждеения
        /// </summary>
        [CanBeNull]
        public new string Type { get; set; }

        /// <summary>
        /// Урл на которой следует редиректить пользователя для подтверждения платежа
        /// </summary>
        [CanBeNull]
        public new string RedirectUrl { get; set; }
    }
}
