﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель данных для создания объекта платежа в ЮKassa
    /// </summary>
    public class YookassaPaymentObjectCreationDataDto
    {
        /// <summary>
        /// Id поставщика
        /// </summary>
        public Guid SupplierId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Ключ идэнтпотентности
        /// </summary>
        public string IdempotencyKey { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор сохраненного способа оплаты на стороне аггрегатора
        /// </summary>
        public string AggregatorPaymentMethodId { get; set; }

        /// <summary>
        /// Является ли автоплатежем
        /// </summary>
        public bool IsAutoPay { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        public string PaymentType { get; set; }
    }
}
