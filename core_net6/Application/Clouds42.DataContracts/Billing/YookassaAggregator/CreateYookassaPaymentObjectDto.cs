﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель создания объекта платежа ЮKassa
    /// </summary>
    public class CreateYookassaPaymentObjectDto
    {
        /// <summary>
        /// Данные о сумме платежа
        /// </summary>
        [JsonProperty("amount")]
        public YookassaPaymentAmountDataDto PaymentAmountData { get; set; }

        /// <summary>
        /// Данные о подтверждении платежа
        /// </summary>
        [JsonProperty("confirmation")]
        public YookassaPaymentConfirmationDataDto PaymentConfirmationData { get; set; }

        /// <summary>
        /// Автоматический прием поступившего платежа
        /// </summary>
        [JsonProperty("capture")]
        public bool IsCapture { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Необходимо сохранять платежный метод
        /// </summary>
        [JsonProperty("save_payment_method")]
        public bool NeedSavePaymentMethod { get; set; }

        /// <summary>
        /// Сохраненный платежный метод
        /// </summary>
        [JsonProperty("payment_method_id")]
        public string PaymentMethodId { get; set; }
        
    }
}
