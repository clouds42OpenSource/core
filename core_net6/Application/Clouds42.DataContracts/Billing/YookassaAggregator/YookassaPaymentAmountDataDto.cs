﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Данные о сумме платежа агрегатора ЮKassa
    /// </summary>
    public class YookassaPaymentAmountDataDto
    {
        /// <summary>
        /// Сумма платежа
        /// </summary>
        [JsonProperty("value")]
        public decimal Amount { get; set; }

        /// <summary>
        /// Валюта платежа
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
