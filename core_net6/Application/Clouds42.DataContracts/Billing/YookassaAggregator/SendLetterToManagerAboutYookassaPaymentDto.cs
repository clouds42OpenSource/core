﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель данных для отправки письма менеджеру
    /// о совершении оплаты через ЮKassa 
    /// </summary>
    public class SendLetterToManagerAboutYookassaPaymentDto
    {
        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
    }
}
