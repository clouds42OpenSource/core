﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Объект платежа ЮKassa
    /// </summary>
    public class YookassaPaymentObjectDto : YookassaPaymentObjectBaseDto
    {
        /// <summary>
        /// Данные о подтверждении платежа
        /// </summary>
        [JsonProperty("confirmation")]
        public YookassaPaymentConfirmationDataDto PaymentConfirmationData { get; set; }
    }
}
