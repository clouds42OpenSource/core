﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Сведения об авторизации платежа
    /// </summary>
    public class YookassaPaymentAuthorizationDetailsDto
    {
        /// <summary>
        /// Уникальный идентификатор банковской транзакции
        /// </summary>
        [JsonProperty("rrn")]
        public string ReferenceRetrievalNumber { get; set; }

        /// <summary>
        /// Код авторизации
        /// </summary>
        [JsonProperty("auth_code")]
        public string AuthCode { get; set; }
    }
}
