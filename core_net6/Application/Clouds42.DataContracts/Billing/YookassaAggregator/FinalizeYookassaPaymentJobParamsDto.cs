﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Параметры задачи для проверки статуса платежа агрегатора ЮKassa
    /// </summary>
    public class FinalizeYookassaPaymentJobParamsDto
    {
        /// <summary>
        /// Id платежа агрегатора ЮKassa
        /// </summary>
        public Guid YookassaAgregatorPaymentId { get; set; }
    }
}
