﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Ответ при оплате
    /// </summary>
    public class PaymentResultDto
    {
        /// <summary>
        /// Идентификатор платежа
        /// </summary>
        public Guid PaymentId { get; set; }

        /// <summary>
        /// Платежный агрегатор
        /// </summary>
        public string PaymentSystem { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal TotalSum { get; set; }
        
        /// <summary>
        /// Токен подтверждения (для виджета юкассы)
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Тип подтверждения
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Урл для перенаправления пользователя для подтверждения платежа
        /// </summary>
        public string RedirectUrl { get; set; }

        public PaymentResultDto()
        {

        }

        public PaymentResultDto(ConfirmYookassaPaymentForClientDataDto model)
        {
            PaymentId = model.PaymentId;
            PaymentSystem = model.PaymentSystem;
            TotalSum = model.TotalSum;
            Type = model.Type;
            Token = model.ConfirmationToken;
            RedirectUrl = model.RedirectUrl;
        }
    }
}
