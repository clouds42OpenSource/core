﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель для получения информации
    /// об объекте платежа агрегатора ЮKassa  
    /// </summary>
    public class GetYookassaPaymentObjectInfoDto
    {
        /// <summary>
        /// Id поставщика
        /// </summary>
        public Guid SupplierId { get; set; }

        /// <summary>
        /// Id платежа агрегатора ЮKassa
        /// </summary>
        public Guid YookassaPaymentId { get; set; }

        /// <summary>
        /// Ключ идэнтпотентности
        /// </summary>
        public string IdempotenceKey { get; set; }
    }
}
