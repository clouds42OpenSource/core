﻿using System;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Базовые данные объекта платежа Юкасса
    /// </summary>
    public abstract class YookassaPaymentObjectBaseDto
    {
        /// <summary>
        /// Id платежа на стороне ЮKassa
        /// </summary>
        [JsonProperty("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Статус платежа
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Признак оплачен ли платеж
        /// </summary>
        [JsonProperty("paid")]
        public bool IsPaid { get; set; }

        /// <summary>
        /// Данные о сумме платежа агрегатора ЮKassa
        /// </summary>
        [JsonProperty("amount")]
        public YookassaPaymentAmountDataDto PaymentAmountData { get; set; }

        /// <summary>
        /// Дата создания платежа
        /// </summary>
        [JsonProperty("created_at")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Метаданные платежа
        /// </summary>
        [JsonProperty("metadata")]
        public YookassaPaymentMetadataDto YookassaPaymentMetadata { get; set; }

        /// <summary>
        /// Данные о получателе платежа
        /// </summary>
        [JsonProperty("recipient")]
        public YookassaPaymentRecipientDataDto YookassaPaymentRecipientData { get; set; }

        /// <summary>
        /// Возможность провести возврат по API
        /// </summary>
        [JsonProperty("refundable")]
        public bool Refundable { get; set; }

        /// <summary>
        /// Платеж является тестовым
        /// </summary>
        [JsonProperty("test")]
        public bool Test { get; set; }
    }
}
