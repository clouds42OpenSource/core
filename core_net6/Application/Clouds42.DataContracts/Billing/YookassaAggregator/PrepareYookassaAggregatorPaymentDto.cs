﻿using System;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Модель для подготовки платежа агрегатора ЮKassa
    /// </summary>
    public class PrepareYookassaAggregatorPaymentDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Идентификатор сохраненного способа оплаты
        /// </summary>
        public Guid? PaymentMethodId { get; set; }

        /// <summary>
        /// Флаг указывающий на то что платеж был проведен через кнопку добавления автоплатежа
        /// </summary>
        public bool IsAutoPay { get; set; }

        /// <summary>
        /// URL переадресации после успешной оплаты
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Идентификатор поставщика
        /// </summary>
        public Guid? SupplierId { get; set; }

        /// <summary>
        /// Идентификатор сервиса
        /// </summary>
        public Guid? ServiceId { get; set; }
    }
}
