﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.Billing.YookassaAggregator
{
    /// <summary>
    /// Краткая информация о подтверждении платежа агрегатора ЮKassa
    /// </summary>
    public class YookassaPaymentConfirmationShortInfoDto
    {
        /// <summary>
        /// Тип платежа
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
