﻿using System;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.Billing.RecalculateServiceCost
{
    /// <summary>
    /// Модель фильтра данных по пересчету
    /// стоимости сервиса после изменений
    /// </summary>
    public class RecalculationServiceCostFilterDto : ISortingDto
    {
        /// <summary>
        /// Период даты создания задачи с
        /// </summary>
        public DateTime? PeriodCreationDateFrom { get; set; }

        /// <summary>
        /// Период даты создания задачи по
        /// </summary>
        public DateTime? PeriodCreationDateTo { get; set; }

        /// <summary>
        /// Период даты пересчета стоимости сервиса с
        /// </summary>
        public DateTime? PeriodRecalculationDateFrom { get; set; }

        /// <summary>
        /// Период даты пересчета стоимости сервиса по
        /// </summary>
        public DateTime? PeriodRecalculationDateTo { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid? ServiceId { get; set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        public CloudTaskQueueStatus? QueueStatus { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 50;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
