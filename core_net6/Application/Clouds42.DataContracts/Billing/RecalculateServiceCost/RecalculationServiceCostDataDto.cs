﻿using System;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.DataContracts.Billing.RecalculateServiceCost
{
    /// <summary>
    /// Данные по пересчету
    /// стоимости сервиса после изменений
    /// </summary>
    public class RecalculationServiceCostDataDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Дата пересчета стоимости сервиса
        /// </summary>
        public DateTime? RecalculationDate { get; set; }

        /// <summary>
        /// Старая стоимость сервиса
        /// </summary>
        public decimal OldServiceCost { get; set; }

        /// <summary>
        /// Новая стоимость сервиса
        /// </summary>
        public decimal NewServiceCost { get; set; }

        /// <summary>
        /// Id запущенной задачи
        /// на пересчет стоимости сервиса
        /// </summary>
        public Guid CoreWorkerTasksQueueId { get; set; }

        /// <summary>
        /// Комментарий по пересчету
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Статус задачи
        /// </summary>
        public string Status { private get; set; }

        /// <summary>
        /// Статус задачи(Enum)
        /// </summary>
        public CloudTaskQueueStatus QueueStatus
            => (CloudTaskQueueStatus)Enum.Parse(typeof(CloudTaskQueueStatus), Status);
    }
}
