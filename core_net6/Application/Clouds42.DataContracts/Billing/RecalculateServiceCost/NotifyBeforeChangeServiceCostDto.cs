﻿using System.Collections.Generic;
using System;

namespace Clouds42.DataContracts.Billing.RecalculateServiceCost
{
    /// <summary>
    /// Модель уведомления о скором изменении стоимости сервиса
    /// </summary>
    public class NotifyBeforeChangeServiceCostDto
    {
        /// <summary>
        /// Id изменений сервиса
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Дата изменения стоимости сервиса
        /// </summary>
        public DateTime ServiceCostChangeDate { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Админ аккаунта
        /// владельца сервиса
        /// </summary>
        public Domain.DataModels.AccountUser AccountAdminOwnerOfService { get; set; }

        /// <summary>
        /// Изменения стоимости услуг
        /// </summary>
        public List<ServiceTypeCostChangedDto> ServiceTypeCostChangeds { get; set; } = [];

        /// <summary>
        /// Ссылка для открытия страницы сервиса
        /// </summary>
        public string UrlForOpenBillingServicePage { get; set; }
    }
}
