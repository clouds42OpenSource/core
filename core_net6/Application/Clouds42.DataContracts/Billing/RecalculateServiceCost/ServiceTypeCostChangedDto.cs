﻿namespace Clouds42.DataContracts.Billing.RecalculateServiceCost
{
    /// <summary>
    /// Модель изменения стоимости услуги
    /// </summary>
    public class ServiceTypeCostChangedDto
    {
        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Старая стоимость услуги
        /// </summary>
        public decimal OldCost { get; set; }

        /// <summary>
        /// Новая стоимость услуги
        /// </summary>
        public decimal NewCost { get; set; }
    }
}
