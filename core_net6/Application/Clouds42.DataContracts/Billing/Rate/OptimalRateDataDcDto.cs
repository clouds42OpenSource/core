﻿namespace Clouds42.DataContracts.Billing.Rate
{
    /// <summary>
    /// Данные оптимального тарифа на услугу
    /// (с учетом тарифа на аккаунт)
    /// </summary>
    public class OptimalRateDataDcDto(decimal cost)
    {
        /// <summary>
        /// Стоимость тарифа
        /// </summary>
        public decimal Cost { get; } = cost;
    }
}
