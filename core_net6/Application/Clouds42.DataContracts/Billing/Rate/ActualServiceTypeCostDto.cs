﻿using System;

namespace Clouds42.DataContracts.Billing.Rate
{
    /// <summary>
    /// Модель актуальной стоимости услуги
    /// </summary>
    public class ActualServiceTypeCostDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Актуальная стоимость услуги
        /// </summary>
        public decimal Cost { get; set; }
    }
}
