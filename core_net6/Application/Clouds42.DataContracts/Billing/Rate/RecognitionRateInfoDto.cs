﻿namespace Clouds42.DataContracts.Billing.Rate
{
    /// <summary>
    /// Информация о стоимости лицензии рекогнишена
    /// </summary>
    public class RecognitionRateInfoDto
    {
        /// <summary>
        /// Количество страниц
        /// </summary>
        public decimal PagesCount { get; set; }
        
        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Cost { get; set; }
    }
}
