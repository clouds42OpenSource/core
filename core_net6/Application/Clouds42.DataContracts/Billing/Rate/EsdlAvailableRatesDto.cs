﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.Rate
{
    /// <summary>
    /// Доступные тарифы для сервиса ESDL
    /// </summary>
    public class EsdlAvailableRatesDto
    {
        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Услуги сервиса
        /// </summary>
        public List<string> ServiceTypes { get; set; } =
        [
            "Лицензия Recogniton42",
            "Страницы для распознавания"
        ];

        /// <summary>
        /// Стоимости лицензий рекогнишена
        /// </summary>
        public List<RecognitionRateInfoDto> RecognitionRates { get; set; }

        /// <summary>
        /// Стоимость годовой лицензии
        /// </summary>
        public decimal LicenseYearCost { get; set; }
    }
}
