﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Billing.Rate
{
    public abstract class RateInfoBaseDc
    {
        public decimal Cost { get; set; }
    }

    public abstract class RateInfoCommonDc : RateInfoBaseDc
    {
        public string RatePeriod { get; set; }
        public string AccountType { get; set; }
        public int? DiscountGroup { get; set; }
        public string DemoPeriod { get; set; }
        public string Remark { get; set; }
        public Guid? LocaleId { get; set; }
    }

    public class StandardRateInfoDc : RateInfoCommonDc
    {
    }

    public class DiskRateInfoDc : RateInfoCommonDc
    {
        public int UpperBound { get; set; }
        public int LowerBound { get; set; }
    }

    public class AccountRateInfoDc : RateInfoBaseDc
    {
        public Guid AccountId { get; set; }
    }

    public class AccountRateTariffInfoDc : AccountRateInfoDc
    {
        public Clouds42Service Service { get; set; }
        public ResourceType ResourceType { get; set; }
        public Guid RateId { get; set; }
    }

}
