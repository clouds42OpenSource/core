﻿using System;

namespace Clouds42.DataContracts.Billing
{
    /// <summary>
    /// Ресурс услуги мой диск
    /// </summary>
    public class MyDiskResourceDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Стоимость диска
        /// </summary>
        public decimal DiskCost { get; set; }

        /// <summary>
        /// Размер диска
        /// </summary>
        public int DiskSize { get; set; } = 5;

        /// <summary>
        /// Стоимость за один Гб пространства
        /// </summary>
        public decimal CostPerOneGb { get; set; }
    }
}
