﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Dc модель создания инвойса
    /// </summary>
    public class CreateInvoiceDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Общая сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Продукты
        /// </summary>
        public List<CreateInvoiceProductDc> Products { get; set; } = [];

        public Guid? AccountAdditionalCompanyId { get; set; }
    }

    /// <summary>
    /// Dc модель создания продукта инвойса
    /// </summary>
    public class CreateInvoiceProductDc
    {
        /// <summary>
        /// ID типа услуги сервиса
        /// </summary>
        public Guid? ServiceTypeId { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        public decimal TotalPrice { get; set; }
    }
}
