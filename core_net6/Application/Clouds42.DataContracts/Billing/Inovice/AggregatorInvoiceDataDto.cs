﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Модель данных счета агрегатора
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class AggregatorInvoiceDataDto : InvoicePaymentsDto
    {
        /// <summary>
        /// Платежная система
        /// </summary>
        [XmlElement(ElementName = nameof(PaymentSystem))]
        [DataMember(Name = nameof(PaymentSystem))]
        public string PaymentSystem { get; set; }
    }
}
