﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Модель данных списка счетов агрегаторов
    /// </summary>
    public class AggregatorInvoiceDataListDto
    {
        /// <summary>
        /// Тип элемента
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = "List";


        /// <summary>
        /// Элемент (список счетов)
        /// </summary>
        [XmlElement(ElementName = "Item")]
        [DataMember(Name = "Item")]
        public List<AggregatorInvoiceDataDto> AggregatorInvoiceDataList { get; set; } = [];
    }
}
