﻿namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Маппер моделей данных счета агрегатора
    /// </summary>
    public static class AggregatorInvoiceDataDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели данных счета агрегатора
        /// </summary>
        /// <param name="model">Модель данных счета агрегатора
        /// c указанимем кода поставщика</param>
        /// <returns>Модель данных счета агрегатора</returns>
        public static AggregatorInvoiceDataDto MapToAggregatorInvoiceDataDto(
            this AggregatorInvoiceWithSupplierCodeDataDto model) => new()
        {
                Id = model.Id,
                AccountId = model.AccountId,
                Description = model.Description,
                Date = model.Date,
                State = model.State,
                Sum = model.Sum,
                PaymentSystem = model.PaymentSystem,
                Currency = model.Currency,
                Uniq = model.Uniq,
                Comment= model.Comment,
                Requisite = model.Requisite
            };
    }
}