﻿using System;
using System.Runtime.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Модель данных по счету для отчета
    /// </summary>
    [DataContract]
    public class InvoiceReportDataDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [DataMember]
        public DateTime DateOfCreation { get; set; }


        /// <summary>
        /// Номер счета
        /// </summary>
        [DataMember]
        public string InvoiceNumber { get; set; }


        /// <summary>
        /// Сумма
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }

        /// <summary>
        /// Период
        /// </summary>
        [DataMember]
        public int? Period { get; set; }

        /// <summary>
        /// Платежная система
        /// </summary>
        [DataMember]
        public string PaymentSystem { get; set; }

        /// <summary>
        /// Значение статуса в виде строки
        /// </summary>
        [Domain.Attributes.BigQueryIgnore]
        public string StatusString { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [DataMember]
        public InvoiceStatus Status => (InvoiceStatus)Enum.Parse(typeof(InvoiceStatus), StatusString);
    }
}
