﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Список счетов агрегаторов
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class AggregatorInvoiceListDto
    {
        public AggregatorInvoiceListDto(List<AggregatorInvoiceDataDto> aggregatorInvoiceData)
        {
            AggregatorInvoiceDataList = new AggregatorInvoiceDataListDto
            {
                AggregatorInvoiceDataList = aggregatorInvoiceData
            };
        }

        public AggregatorInvoiceListDto()
        {
            AggregatorInvoiceDataList = new AggregatorInvoiceDataListDto();
        }

        /// <summary>
        /// Модель данных списка счетов агрегаторов
        /// </summary>
        [XmlElement(ElementName = "List")]
        [DataMember(Name = "List")]
        public AggregatorInvoiceDataListDto AggregatorInvoiceDataList { get; set; }
    }
}
