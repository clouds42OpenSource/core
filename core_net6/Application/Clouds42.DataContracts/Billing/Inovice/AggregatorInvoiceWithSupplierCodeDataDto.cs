﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Модель данных счета агрегатора
    /// c указанимем кода поставщика 
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class AggregatorInvoiceWithSupplierCodeDataDto : AggregatorInvoiceDataDto
    {
        /// <summary>
        /// Код поставщика
        /// </summary>
        [XmlElement(ElementName = nameof(SupplierCode))]
        [DataMember(Name = nameof(SupplierCode))]
        public string SupplierCode { get; set; }
    }
}
