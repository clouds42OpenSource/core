﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Список счетов агрегаторов
    /// c указанимем кода поставщика 
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class AggregatorInvoiceWithSupplierCodeListDto
    {
        public AggregatorInvoiceWithSupplierCodeListDto(
            List<AggregatorInvoiceWithSupplierCodeDataDto> aggregatorInvoiceWithSupplierCodeData)
        {
            AggregatorInvoiceWithSupplierCodeDataList = new AggregatorInvoiceWithSupplierCodeDataListDto
            {
                AggregatorInvoiceWithSupplierCodeDataList = aggregatorInvoiceWithSupplierCodeData
            };
        }

        public AggregatorInvoiceWithSupplierCodeListDto()
        {
            AggregatorInvoiceWithSupplierCodeDataList = new AggregatorInvoiceWithSupplierCodeDataListDto();
        }

        /// <summary>
        /// Модель данных списка счетов агрегаторов
        /// c указанимем кода поставщика
        /// </summary>
        [XmlElement(ElementName = "List")]
        [DataMember(Name = "List")]
        public AggregatorInvoiceWithSupplierCodeDataListDto AggregatorInvoiceWithSupplierCodeDataList { get; set; }
    }
}
