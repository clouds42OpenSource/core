﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.IDataModels;
using LocaleEntity = Clouds42.Domain.DataModels.Locale;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Модель всех(полных) данные счета на оплату
    /// </summary>
    public class InvoiceFullDataDto
    {
        /// <summary>
        /// Аккаунт, которому принадлежит счет на оплату
        /// </summary>
        public IAccount Account { get; set; }

        /// <summary>
        /// Локаль аккаунта
        /// </summary>
        public LocaleEntity Locale { get; set; }

        /// <summary>
        /// Id поставщика
        /// </summary>
        public Guid SupplierId { get; set; }

        /// <summary>
        /// Html форма счета на оплату
        /// </summary>
        public string InvoiceHtmlFormData { get; set; }

        /// <summary>
        /// Файлы Html формы счета на оплату
        /// </summary>
        public List<CloudFile> InvoiceHtmlFormFiles { get; set; } = [];

        /// <summary>
        /// Номер фискального чека
        /// </summary>
        public string ReceiptFiscalNumber { get; set; }

        /// <summary>
        /// Продукты счета на оплату
        /// </summary>
        public List<InvoiceProduct> InvoiceProducts { get; set; } = [];

        public Guid? AccountAdditionalCompanyId { get; set; }
        public AccountAdditionalCompany AccountAdditionalCompany { get; set; }
    }
}
