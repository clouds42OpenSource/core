﻿using System;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Dc модель создания инвойса на произвольную сумму
    /// </summary>
    public class CreateArbitraryAmountInvoiceDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Сумма инвойса
        /// </summary>
        public decimal Amount { get; set; }
    }
}
