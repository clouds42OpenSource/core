﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Inovice
{
    /// <summary>
    /// Данные для создания продукта счета
    /// </summary>
    public class CreateInvoiceProductDataDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        public Clouds42Service? SystemService { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int CountLicenses { get; set; }
    }
}
