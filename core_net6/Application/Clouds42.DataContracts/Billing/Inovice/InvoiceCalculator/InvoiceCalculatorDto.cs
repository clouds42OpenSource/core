﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.Account.Locale;

namespace Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator
{
    /// <summary>
    /// DC модель калькулятоа инвойсов
    /// </summary>
    public class InvoiceCalculatorDto
    {
        /// <summary>
        /// Имя покупателя
        /// </summary>
        public string BuyerName { get; set; }

        /// <summary>
        /// Имя поставщика
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Информация о валюте
        /// </summary>
        public CurrencyDto Currency { get; set; }

        /// <summary>
        /// Контроллер значений поля Платежный период
        /// </summary>
        public CalculatorControlDto PayPeriodControl { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Общая стоимость до НДС
        /// </summary>
        public decimal TotalBeforeVAT { get; set; }        

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Общная стоимость после НДС
        /// </summary>
        public decimal TotalAfterVAT { get; set; }

        /// <summary>
        /// Сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Калькуляторы продуктов
        /// </summary>
        public List<InvoiceProductCalculatorDc> Products { get; set; }
            = [];
    }
    
    /// <summary>
    /// DC модель калькулятора продукта инвойса
    /// </summary>
    public class InvoiceProductCalculatorDc
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Активен ли продукт
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Стоиость одной единицы продукта
        /// </summary>
        public decimal Rate { get; set; }


        /// <summary>
        /// Общая стоимость для выбранного платежного периода
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Сумма бонусов за продукт
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Имя продукта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание продукта
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Контроллер значений поля Quantity
        /// </summary>
        public CalculatorControlDto QuantityControl { get; set; }
    }
}
