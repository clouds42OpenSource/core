﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator
{
    /// <summary>
    /// DC модель контроллера значений поля
    /// </summary>
    public class CalculatorControlDto
    {
        /// <summary>
        /// Тип контроллера
        /// </summary>
        public string Type { get; set; }


        /// <summary>
        /// Настройки контроллера
        /// </summary>
        public List<KeyValuePair<string, string>> Settings { get; set; }
            = [];
    }
}
