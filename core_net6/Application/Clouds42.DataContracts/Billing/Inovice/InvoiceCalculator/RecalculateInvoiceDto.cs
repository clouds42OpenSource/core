﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator
{
    /// <summary>
    /// Dc модель запроса на перерасчет инвойса
    /// </summary>
    public class RecalculateInvoiceDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Назначение инвойса
        /// </summary>
        public InvoicePurposeType InvoicePurposeType { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Продукты
        /// </summary>
        public List<RecalculateInvoiceProductCalculatorDc> Products { get; set; }
            = [];
    }

    /// <summary>
    /// Dc модель запрсоса на перерасчет продукта инвойса
    /// </summary>
    public class RecalculateInvoiceProductCalculatorDc
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Признакт того что продукт включен в инвойс
        /// </summary>
        public bool IsActive { get; set; }
    }
}
