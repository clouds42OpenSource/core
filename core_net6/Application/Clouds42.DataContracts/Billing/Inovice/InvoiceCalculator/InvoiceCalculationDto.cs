﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator
{
    /// <summary>
    /// Расчет инвойса
    /// </summary>
    public class InvoiceCalculationDto
    {
        /// <summary>
        /// ID аккаунта для которого произведен расчет
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Назначение инвойса
        /// </summary>
        public InvoicePurposeType InvoicePurposeType { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Расчеты продуктов инвойса
        /// </summary>
        public List<InvoiceProductCalculationDc> Products { get; set; }
            = [];

        /// <summary>
        /// Общая стоимость до НДС
        /// </summary>
        public decimal TotalBeforeVAT { get; set; }

        /// <summary>
        /// Сумма НДС
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Общая стоимость после НДС
        /// </summary>
        public decimal TotalAfterVAT { get; set; }

        /// <summary>
        /// Сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        public Guid? AccountAdditionalCompanyId { get; set; }
    }

    /// <summary>
    /// Расчет продукта инвойса
    /// </summary>
    public class InvoiceProductCalculationDc
    {
        /// <summary>
        /// Идентификатор продукта
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Полатежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Стоиость одной единицы
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Общая стоимость продукта для выбранного периода
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Общая сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }
    }
}
