﻿using System.Collections.Generic;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing
{
    /// <summary>
    /// Модель основных услуг биллинга
    /// </summary>
    public class ViewPageBaseBillingServicesDto
    {
        /// <summary>
        /// Ресурсы услуг биллинга
        /// </summary>
        public List<BillingServiceTypeResourceDto> BillingServiceTypeResources { get; set; } = [];

        /// <summary>
        /// Ресурс услуги мой диск
        /// </summary>
        public MyDiskResourceDto MyDiskResource { get; set; }

        /// <summary>
        /// Размер бесплатного тарифа
        /// </summary>
        public int MyDiskFreeTariffSize { get; set; }

        /// <summary>
        /// Период оплаты
        /// </summary>
        public PayPeriod PayPeriod { get; set; } = PayPeriod.Month3;

        /// <summary>
        /// Итоговая сумму
        /// </summary>
        public decimal? TotalAmount => GetTotalAmountBillingServiceTypeResources();

        /// <summary>
        /// Получить общую стоимость ресурсов услуг
        /// </summary>
        /// <returns>Общая стоимость ресурсов услуг</returns>
        private decimal GetTotalAmountBillingServiceTypeResources()
        {
            decimal totalAmount = 0;

            BillingServiceTypeResources.ForEach(w =>
            {
                totalAmount += w.Amount;
            });

            return totalAmount + MyDiskResource.DiskCost;
        }
    }
}
