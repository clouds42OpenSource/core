﻿namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Модель предлагаемого платежа
    /// </summary>
    public class SuggestedPaymentModelDto
    {
        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Содержание услуги
        /// </summary>
        public string ServiceTypeContent { get; set; }
        
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
