﻿using System;

namespace Clouds42.DataContracts.Billing.Payments
{
    public class PayWithSavedPaymentMethodDto
    {
        /// <summary>
        /// Идентификатор аккаунта пользоватея
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Идентификатор сохраненного способа оплаты
        /// </summary>
        public Guid? PaymentMethodId { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal TotalSum { get; set; }

        /// <summary>
        /// Флаг указывающий на то что платеж будет добавлен в автоплатежи
        /// </summary>
        public bool IsAutoPay { get; set; }
    }
}
