﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Модель данных по платежу для отчета
    /// </summary>
    [DataContract]
    public class PaymentReportDataDto
    {
        /// <summary>
        /// Id платежа
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        [DataMember]
        public DateTime Date { get; set; }

        /// <summary>
        /// Описание платежа
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        [DataMember]
        public string PaymentType
        {
            get
            {
                var enumValue = Enum.Parse(typeof(PaymentType), OperationType);

                return ((PaymentType)enumValue).Description();
            }
        }

        [NotMapped]
        [IgnoreDataMember]

        public string OperationType { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }

        /// <summary>
        /// Статус платежа
        /// </summary>
        [DataMember]
        public string PaymentStatus {
            get
            {
                var enumValue = Enum.Parse(typeof(PaymentStatus), Status);

                return ((PaymentStatus)enumValue).Description();
            }
        }

        [NotMapped]
        [IgnoreDataMember]
        public string Status { get; set; }

        /// <summary>
        /// ФИО сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerFullName {
            get
            {
                return SaleManager?.AccountUser?.FullName ??
                       $"{SaleManager?.AccountUser?.LastName} {SaleManager?.AccountUser?.FirstName} {SaleManager?.AccountUser?.MiddleName}";
            }
        }

        /// <summary>
        /// Отдел сейл менеджера
        /// </summary>
        [DataMember]
        public string SaleManagerDivision
        {
            get
            {
                return SaleManager?.Division;
            }
        }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        [DataMember]
        public TransactionType TransactionType { get; set; }

        [NotMapped]
        [IgnoreDataMember]
        public AccountSaleManager SaleManager { get; set; }
    }
}
