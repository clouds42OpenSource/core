﻿using System;

namespace Clouds42.DataContracts.Billing.Payments
{
    public class PreparePaymentSystemDto
    {
        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal TotalSum { get; set; }

        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Адрес перенаправления после совершения платежа
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Флаг указывающий на то что платеж будет добавлен в автоплатежи
        /// </summary>
        public bool IsAutoPay { get; set; }
    }
}
