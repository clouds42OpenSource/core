﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Описание платежа
    /// </summary>
    public class PaymentDefinitionDto
    {
        /// <summary>
        /// ID платежа
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Аккаунт
        /// </summary>
        public Guid Account { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Total { get; set; }
        
        /// <summary>
        /// Остаток после проведенной транзакции
        /// </summary>
        public decimal? Remains { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        public PaymentType OperationType { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType TransactionType { get; set; } = TransactionType.Money;

        /// <summary>
        /// Дата совершения платежа
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Статус обработки платежа
        /// </summary>
        public PaymentStatus Status { get; set; }

        /// <summary>
        /// Платежная система
        /// </summary>
        public PaymentSystem System { get; set; }

        /// <summary>
        /// Источник платежа
        /// </summary>
        public string OriginDetails { get; set; }

        /// <summary>
        /// ID платежа в КОРП
        /// </summary>
        public Guid? ExchangeIdentifier { get; set; }

        /// <summary>
        /// Назначение платежа
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID сервиса биллинга
        /// </summary>
        public Guid? BillingServiceId { get; set; }

        /// <summary>
        /// Номер платежа
        /// </summary>
        public int Number { get; set; }
        
        /// <summary>
        /// Возможность провести платеж с уходом в минус.
        /// </summary>
        public bool CanOverdraft { get; set; }
    }
}
