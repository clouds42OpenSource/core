﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Payments
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetActDto
    {
        [XmlElement(ElementName = nameof(InvoicesId))]
        [DataMember(Name = nameof(InvoicesId))]
        public Guid InvoicesId { set; get; }

        [XmlElement(ElementName = nameof(ActID))]
        [DataMember(Name = nameof(ActID))]
        public Guid? ActID { set; get; }

        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { set; get; }

        [XmlElement(ElementName = nameof(RequiredSignature))]
        [DataMember(Name = nameof(RequiredSignature))]
        public bool RequiredSignature { get; set; }
    }
}
