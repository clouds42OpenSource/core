﻿namespace Clouds42.DataContracts.Billing.Payments
{
    public class PayboxHandlingDto
    {
        public int OrderId { get; set; }
        public decimal Sum { get; set; }
        public string PaymentSystem { get; set; }
        public int ExternalPaymentId { get; set; }
        public int PaymentResult { get; set; }
        public string Salt { get; set; }
        public string Description { get; set; }
    }
}
