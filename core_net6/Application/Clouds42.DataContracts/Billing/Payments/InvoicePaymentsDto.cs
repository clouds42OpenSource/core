﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Модель счета
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class InvoicePaymentsDto
    {
        /// <summary>
        /// Идентификатор счета
        /// </summary>
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public System.Guid Id { get; set; }

        /// <summary>
        /// Дата счета
        /// </summary>
        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public System.DateTime Date { get; set; }

        /// <summary>
        /// Сумма счета
        /// </summary>
        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public decimal Sum { get; set; }

        /// <summary>
        ///  Номер счета
        /// </summary>
        [XmlElement(ElementName = nameof(InvoiceNumber))]
        [DataMember(Name = nameof(InvoiceNumber))]
        public string InvoiceNumber { get; set; }

        /// <summary>
        /// Реквизит
        /// </summary>
        [XmlElement(ElementName = nameof(Requisite))]
        [DataMember(Name = nameof(Requisite))]
        public string Requisite { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [XmlElement(ElementName = nameof(State))]
        [DataMember(Name = nameof(State))]
        public string State { get; set; }

        /// <summary>
        /// Идентификатор аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public System.Guid AccountId { get; set; }

        /// <summary>
        /// Номер счета
        /// </summary>
        [XmlElement(ElementName = nameof(Uniq))]
        [DataMember(Name = nameof(Uniq))]
        public string Uniq { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [XmlElement(ElementName = nameof(Currency))]
        [DataMember(Name = nameof(Currency))]
        public string Currency { get; set; }

    }
}
