﻿using System.Collections.Generic;
using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Результат проведения платежа
    /// </summary>
    public class CreatePaymentResultDto(PaymentOperationResult operationStatus, List<Guid> paymentIds = null)
    {
        /// <summary>
        /// Статус операции.
        /// </summary>
        public PaymentOperationResult OperationStatus { get; } = operationStatus;

        /// <summary>
        /// Список платежей
        /// </summary>
        public List<Guid> PaymentIds { get; } = paymentIds;
    }
}
