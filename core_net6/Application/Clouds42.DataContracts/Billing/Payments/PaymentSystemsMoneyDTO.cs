﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class PaymentSystemsMoneyDto
    {
        [XmlElement(ElementName = nameof(RobokassaMoneyAmount))]
        [DataMember(Name = nameof(RobokassaMoneyAmount))]
        public decimal RobokassaMoneyAmount { get; set; }

        [XmlElement(ElementName = nameof(UkrPaysMoneyAmount))]
        [DataMember(Name = nameof(UkrPaysMoneyAmount))]
        public decimal UkrPaysMoneyAmount { get; set; }

        [XmlElement(ElementName = nameof(CorpUAMoneyAmount))]
        [DataMember(Name = nameof(CorpUAMoneyAmount))]
        public decimal CorpUAMoneyAmount { get; set; }

        [XmlElement(ElementName = nameof(CorpRUMoneyAmount))]
        [DataMember(Name = nameof(CorpRUMoneyAmount))]
        public decimal CorpRUMoneyAmount { get; set; }
    }

    public class PaymentSystemsMoney
    {       
        public decimal RobokassaMoneyAmount { get; set; }       
        public decimal UkrPaysMoneyAmount { get; set; }
        public decimal CorpUAMoneyAmount { get; set; }   
        public decimal CorpRUMoneyAmount { get; set; }
        }
}
