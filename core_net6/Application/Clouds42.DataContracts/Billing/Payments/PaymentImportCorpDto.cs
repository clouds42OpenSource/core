﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Payments
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PaymentImportCorpDto
    {
        [XmlElement(ElementName = nameof(DocUid))]
        [DataMember(Name = nameof(DocUid))]
        public Guid DocUid { set; get; }

        [XmlElement(ElementName = nameof(Kpp))]
        [DataMember(Name = nameof(Kpp))]
        public string Kpp { set; get; }

        [XmlElement(ElementName = nameof(Inn))]
        [DataMember(Name = nameof(Inn))]
        public string Inn { set; get; }

        [XmlElement(ElementName = nameof(TransactionType))]
        [DataMember(Name = nameof(TransactionType))]
        public PaymentType TransactionType { set; get; }

        [XmlElement(ElementName = nameof(Sum))]
        [DataMember(Name = nameof(Sum))]
        public string Sum { set; get; }

        private decimal? _sumValue;
        public decimal SumValue
        {
            get
            {
                if (!_sumValue.HasValue)
                {
                    _sumValue = Parse(Sum);
                }
                return _sumValue.Value;
            }
            set { _sumValue = value; }
        }

        [XmlElement(ElementName = nameof(State))]
        [DataMember(Name = nameof(State))]
        public PaymentStatus State { set; get; }

        [XmlElement(ElementName = nameof(Date))]
        [DataMember(Name = nameof(Date))]
        public DateTime Date { set; get; }

        [XmlElement(ElementName = "Result")]
        [DataMember(Name = "Result")]
        public string Comment { set; get; }


        public static decimal Parse(string val)
        {
            string wantedSeperator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            string alternateSeperator = (wantedSeperator == "," ? "." : ",");

            if (val.IndexOf(wantedSeperator) == -1 && val.IndexOf(alternateSeperator) != -1)
            {
                val = val.Replace(alternateSeperator, wantedSeperator);
            }
            return decimal.Parse(val, NumberStyles.Any);
        }
    }
}
