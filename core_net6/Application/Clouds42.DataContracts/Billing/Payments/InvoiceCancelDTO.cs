﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.Billing.Payments
{
    [XmlRoot("Request")]
    [DataContract(Name = "Request")]
    public class InvoiceCancelDto
    {
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { set; get; }

        [XmlElement(ElementName = nameof(InvoiceNumber))]
        [DataMember(Name = nameof(InvoiceNumber))]
        public string InvoiceNumber { set; get; }
    }
}