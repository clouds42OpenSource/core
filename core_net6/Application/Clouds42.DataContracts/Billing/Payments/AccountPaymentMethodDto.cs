﻿using System;

namespace Clouds42.DataContracts.Billing.Payments
{
    public class AccountPaymentMethodDto
    {
        /// <summary>
        /// Идентификтор аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Идентификатор сохраненного способа оплаты
        /// </summary>
        public Guid PaymentMethodId { get; set; }

        public string PaymentName { get; set; }
    }
}
