﻿namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary>
    /// Модель доступности обещанного платежа
    /// </summary>
    public class PromisePaymentAvailableDto
    {
        /// <summary>
        /// Возможность принять обещанный платеж
        /// </summary>
        public bool CanTakePromisePayment { get; set; }

        /// <summary>
        /// Возможность увеличить обещанный платеж
        /// </summary>
        public bool CanIncreasePromisePayment { get; set; }

        /// <summary>
        /// Причина блокировки
        /// </summary>
        public string BlockReason { get; set; }
    }
}
