﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.Billing.Payments
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class PaymentSystemRefillDto
    {
        [XmlElement(ElementName = nameof(PaymentSystem))]
        [DataMember(Name = nameof(PaymentSystem))]
        public PaymentSystem PaymentSystem { get; set;}

        [XmlElement(ElementName = nameof(RobokassaUrl))]
        [DataMember(Name = nameof(RobokassaUrl))]
        public string RobokassaUrl { get; set; }

        [XmlElement(ElementName = nameof(ServiceId))]
        [DataMember(Name = nameof(ServiceId))]
        public string ServiceId { get; set; }

        [XmlElement(ElementName = nameof(Successurl))]
        [DataMember(Name = nameof(Successurl))]
        public string Successurl { get; set; }

        [XmlElement(ElementName = nameof(RequestUrl))]
        [DataMember(Name = nameof(RequestUrl))]
        public string RequestUrl { get; set; }

        [XmlElement(ElementName = nameof(Order))]
        [DataMember(Name = nameof(Order))]
        public string Order { get; set; }

        [XmlElement(ElementName = nameof(Amount))]
        [DataMember(Name = nameof(Amount))]
        public decimal Amount { get; set; }

        [XmlElement(ElementName = nameof(Encoding))]
        [DataMember(Name = nameof(Encoding))]
        public string Encoding { get; set; }
    }
}