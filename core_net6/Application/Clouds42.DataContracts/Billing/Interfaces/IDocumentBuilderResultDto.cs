﻿namespace Clouds42.DataContracts.Billing.Interfaces
{
    /// <summary>
    /// Результат билда документа
    /// </summary>
    public interface IDocumentBuilderResultDto
    {
        /// <summary>
        /// Массив байтов
        /// </summary>
        byte[] Bytes { get; }

        /// <summary>
        /// Название документа
        /// </summary>
        string FileName { get; }
    }
}