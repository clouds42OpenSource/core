﻿using System;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.DataContracts.Billing
{
    /// <summary>
    /// Ресурс услуги биллинга
    /// </summary>
    public class BillingServiceTypeResourceDto
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Стоимость русурса услуги
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int CountLicenses { get; set; }

        /// <summary>
        /// Стоимость одной лицензии
        /// </summary>
        public decimal CostPerOneLicense { get; set; }

        /// <summary>
        /// Тип работы биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Тип системной услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Сервис является активным
        /// </summary>
        public bool IsActiveService { get; set; }
    }
}
