﻿using System;
using Clouds42.DataContracts.Account.Locale;
using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Модель поставщика
    /// </summary>
    [DataContract]
    public class SupplierModelDto
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        /// Название поставщика
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Код поставщика
        /// </summary>
        [DataMember]
        public string Code { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        [DataMember]
        public Guid LocaleId { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        [DataMember]
        public LocaleDto Locale { get; set; }

        /// <summary>
        /// Стандартный поставщик на локаль
        /// </summary>
        [DataMember]
        public bool IsDefault { get; set; }

        /// <summary>
        /// ID (Договор оферты)
        /// </summary>
        [DataMember]
        public Guid? AgreementId { get; set; }

        /// <summary>
        /// Файл (Договор оферты)
        /// </summary>
        [DataMember]
        public CloudFileDto Agreement { get; set; } = new();


        /// <summary>
        /// Печатная форма счета на оплату
        /// </summary>
        [DataMember]
        public Guid? PrintedHtmlFormInvoiceId { get; set; }

        /// <summary>
        /// Печатная форма фискального чека
        /// </summary>
        [DataMember]
        public Guid? PrintedHtmlFormInvoiceReceiptId { get; set; }
    }
}