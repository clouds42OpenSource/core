﻿namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Модель поставщика для вывода на таблицу
    /// </summary>
    public class SupplierTableDto : SupplierBaseDto
    {
        /// <summary>
        /// Название локали
        /// </summary>
        public string LocaleName { get; set; }
    }
}