﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Модель шаблона счета на оплату
    /// </summary>
    [DataContract]
    public class SupplierRazorTemplateDto
    {
        /// <summary>
        /// ID поставщика
        /// </summary>
        [DataMember]
        public Guid SupplierId { get; set; }

        /// <summary>
        /// HTML шаблона
        /// </summary>
        [DataMember]
        public string RazorData { get; set; }

        /// <summary>
        /// Прикрепленные файлы
        /// </summary>
        [DataMember]
        public List<CloudFileDto> Attachments { get; set; }
    }
}