﻿using System;
using System.Collections.Generic;
using System.Linq;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.DataModels;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Маппер моделей поставщика услуг
    /// </summary>
    public static class SupplierModelsDtoMapper
    {
        /// <summary>
        /// Выполнить маппинг к модели шаблона счета на оплату 
        /// </summary>
        /// <param name="supplierId">Id поставщика</param>
        /// <param name="htmlData">HTML данные печатной формы</param>
        /// <param name="cloudFiles">Файлы облака</param>
        /// <returns>Модель шаблона счета на оплату</returns>
        public static SupplierRazorTemplateDto MapToSupplierRazorTemplateDc(Guid supplierId, string htmlData,
            List<CloudFile> cloudFiles) => new()
        {
                SupplierId = supplierId,
                RazorData = htmlData,
                Attachments = cloudFiles.Select(cloudFile => new CloudFileDto
                {
                    Id = cloudFile.Id,
                    FileName = cloudFile.FileName,
                    ContentType = cloudFile.ContentType,
                    Base64 = Convert.ToBase64String(cloudFile.Content),
                    Bytes = cloudFile.Content
                }).ToList()
            };
    }
}