﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Результат поставщиков с пагинацией
    /// </summary>
    public class SuppliersPaginationDto : SelectDataResultCommonDto<SupplierTableDto>
    {
    }
}