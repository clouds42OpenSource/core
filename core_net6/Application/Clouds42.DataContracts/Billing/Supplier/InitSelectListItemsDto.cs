﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Модель для получения элементов комбобокса
    /// при инциализации страниц создания/редактирования поставщика
    /// </summary>
    public class InitSelectListItemsDto
    {
        /// <summary>
        /// Все локали
        /// </summary>
        public IEnumerable<ComboboxItemDto<Guid>> Locales { get; set; }

        /// <summary>
        /// Все печатные формы
        /// </summary>
        public IEnumerable<ComboboxItemDto<Guid>> PrintedHtmlForms { get; set; }
    }
}