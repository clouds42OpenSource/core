﻿using System;
using Microsoft.AspNetCore.Http;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Модель поставщика
    /// </summary>
    public class SupplierDto : SupplierBaseDto
    {
        /// <summary>
        /// Файл договора офферты
        /// </summary>
        public IFormFile CloudFile { get; set; }

        /// <summary>
        /// ID локали
        /// </summary>
        public Guid LocaleId { get; set; }

        /// <summary>
        /// ID Договор оферты
        /// </summary>
        public Guid? AgreementId { get; set; }

        /// <summary>
        /// Печатная форма счета на оплату
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceId { get; set; }

        /// <summary>
        /// Печатная форма фискального чека
        /// </summary>
        public Guid? PrintedHtmlFormInvoiceReceiptId { get; set; }

        /// <summary>
        /// Названия файла офферты
        /// </summary>
        public string AgreementFileName { get; set; }
    }
}