﻿using System;

namespace Clouds42.DataContracts.Billing.Supplier
{
    /// <summary>
    /// Базовый модель поставщика
    /// </summary>
    public class SupplierBaseDto
    {
        /// <summary>
        /// ID поставщика
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название поставщика
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Код поставщика
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Стандартный поставщик на локаль
        /// </summary>
        public bool IsDefault { get; set; }
    }
}