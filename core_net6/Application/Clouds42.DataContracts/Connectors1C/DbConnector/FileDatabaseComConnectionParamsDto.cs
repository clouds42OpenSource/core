﻿namespace Clouds42.DataContracts.Connectors1C.DbConnector
{
    /// <summary>
    /// Параметры подключения к файловой базе.
    /// </summary>
    public class FileDatabaseComConnectionParamsDto : ModelBaseDto
    {
        /// <summary>
        /// Путь до файловой базы.
        /// </summary>
        public string FilePath { get; set; }

        public override string GetArguments()
            => $"File=\'{FilePath}\';Usr=\'{Login}\';pwd=\'{Password}\';";

        public override string GetRegisterLog()
    => $"{FilePath}\\1Cv8Log";
    }
}
