﻿namespace Clouds42.DataContracts.Connectors1C.DbConnector
{
    public enum ConnectCodeDto
    {

        /// <summary>
        /// Подключение установлено.
        /// </summary>
        Success, 

        /// <summary>
        /// Не известная ошибка подключения.
        /// </summary>
        ConnectError,

        /// <summary>
        /// Не верный логин или пароль администратора.
        /// </summary>
        PasswordOrLoginIncorrect,

        /// <summary>
        /// Файл базы поврежден.
        /// </summary>
        DatabaseDamaged,

        /// <summary>
        ///  Ошибка формата файла журнала регистрации
        /// </summary>
        RegistrationLogIncorrect

    }
}
