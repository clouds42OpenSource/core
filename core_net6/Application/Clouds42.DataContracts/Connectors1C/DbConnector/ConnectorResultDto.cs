﻿namespace Clouds42.DataContracts.Connectors1C.DbConnector
{
    public class ConnectorResultDto<TResult>
    {

        /// <summary>
        /// Модель с данными.
        /// </summary>
        public TResult Result { get; set; }

        /// <summary>
        /// Код подключения.
        /// </summary>
        public ConnectCodeDto ConnectCode { get; set; }
        
        /// <summary>
        /// Сообщение подключения.
        /// </summary>
        public string Message { get; set; }
    }
}