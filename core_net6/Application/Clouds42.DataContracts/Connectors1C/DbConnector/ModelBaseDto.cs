﻿using Clouds42.DataContracts.Connectors1C.AgentConnector;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Connectors1C.DbConnector
{

    /// <summary>
    /// Параметры подключения к информационной базе.
    /// </summary>
    public abstract class ModelBaseDto : IPlatformDto
    {
        /// <summary>
        /// Имя информационной базы.
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Логин к базе.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль к базе.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Версия платформы С.
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// Путь к платформе 1С
        /// </summary>
        public string PathToPlatform { get; set; }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        public PlatformType PlatformType { get; set; }

        /// <summary>
        /// Получить строку параметров
        /// </summary>
        /// <returns>Строка параметров</returns>
        public abstract string GetArguments();

        /// <summary>
        /// Получить путь до журнала регистрации
        /// </summary>
        /// <returns>Строка параметров</returns>
        public abstract string GetRegisterLog();
    }
}
