﻿using System.Text;

namespace Clouds42.DataContracts.Connectors1C.DbConnector
{
    /// <summary>
    /// Класс, описывающий метаданные конфигурации инф. базы
    /// </summary>
    public class MetadataResultDto(
        string version,
        string synonymConfiguration)
    {
        /// <summary>
        /// Версия релиза.
        /// </summary>
        public string Version { get; } = version;

        /// <summary>
        /// Синоним конфигурации
        /// </summary>
        public string SynonymConfiguration { get; } = synonymConfiguration;

        public override string ToString()
            => new StringBuilder().
            AppendLine($"Версия: {Version},").
            AppendLine($"Синоним: {SynonymConfiguration}").ToString();
    }
}