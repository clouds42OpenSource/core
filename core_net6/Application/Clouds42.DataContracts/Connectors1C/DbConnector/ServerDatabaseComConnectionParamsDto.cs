﻿namespace Clouds42.DataContracts.Connectors1C.DbConnector
{
    /// <summary>
    /// Параметры подключения к серверному предприятию.
    /// </summary>
    public class ServerDatabaseComConnectionParamsDto : ModelBaseDto
    {
        /// <summary>
        /// Сервер предприятия.
        /// </summary>
        public string ServerEnterprise { get; set; }

        /// <summary>
        /// Имя информаионной базы.
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// Получить строку параметров
        /// </summary>
        /// <returns>Строка параметров</returns>
        public override string GetArguments()
            => $"Srvr=\'{ServerEnterprise}\';Ref=\'{DbName}\';Usr=\'{Login}\';pwd=\'{Password}\';";

        public override string GetRegisterLog() => $"{ServerEnterprise}\\1Cv8Log";

    }
}
