﻿using System;

namespace Clouds42.DataContracts.Connectors1C.AgentConnector
{
    /// <summary>
    /// Результат подключения к кластеру
    /// </summary>
    public class ClusterConnectionResultDto(
        dynamic comConnector = null,
        dynamic agent = null,
        dynamic cluster = null,
        Array sessions = null)
    {
        /// <summary>
        /// Ком коннектор.
        /// </summary>
        public dynamic ComConnector { get; } = comConnector;

        /// <summary>
        /// Агент кластера.
        /// </summary>
        public dynamic Agent { get; } = agent;

        /// <summary>
        /// Кластер.
        /// </summary>
        public dynamic Cluster { get; } = cluster;

        /// <summary>
        /// Сессии кластера.
        /// </summary>
        public Array Sessions { get; } = sessions;
    }
}
