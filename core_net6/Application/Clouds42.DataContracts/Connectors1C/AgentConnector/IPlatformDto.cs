﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Connectors1C.AgentConnector
{
    public interface IPlatformDto
    {
        /// <summary>
        /// Версия платформы 1С.
        /// </summary>
        string PlatformVersion { get; set; }

        /// <summary>
        /// Путь к платформе 1С
        /// </summary>
        string PathToPlatform { get; set; }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        PlatformType PlatformType { get; set; }
    }
}