﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.Connectors1C.AgentConnector
{
    /// <summary>
    /// Параметры подлкючения к кластеру 1С
    /// </summary>
    public class ClusterParametersDto : IPlatformDto
    {
        /// <summary>
        /// Адрес подключения к кластеру.
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Имя администратора кластера.
        /// </summary>
        public string AdminName { get; set; } = string.Empty;

        /// <summary>
        /// Пароль администратора кластера.
        /// </summary>
        public string AdminPassword { get; set; } = string.Empty;

        /// <summary>
        /// Версия платформы 1С.
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// Путь к платформе 1С
        /// </summary>
        public string PathToPlatform { get; set; }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        public PlatformType PlatformType { get; set; }
    }
}