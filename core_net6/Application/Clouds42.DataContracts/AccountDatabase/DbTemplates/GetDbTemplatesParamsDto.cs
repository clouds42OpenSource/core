﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель выбора записей шаблонов
    /// </summary>
    public sealed class GetDbTemplatesParamsDto: SelectDataCommonDto<DbTemplatesFilterParamsDto>
    {
    }
}