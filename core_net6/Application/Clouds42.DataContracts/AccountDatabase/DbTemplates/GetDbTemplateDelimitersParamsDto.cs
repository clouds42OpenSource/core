﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель выбора записей шаблонов баз на разделителях
    /// </summary>
    public sealed class GetDbTemplateDelimitersParamsDto : SelectDataCommonDto<object>
    {
    }
}