﻿using System;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель Шаблона
    /// </summary>
    public class DbTemplateItemDto
    {
        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание шаблона
        /// </summary>
        public string DefaultCaption { get; set; }

        /// <summary>
        /// Порядок
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string Configuration1CName { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public PlatformType Platform { get; set; }
        
        /// <summary>
        /// Локаль
        /// </summary>
        public Guid? LocaleId { get; set; }

        /// <summary>
        /// Шаблон с демо данными
        /// </summary>
        public Guid? DemoTemplateId { get; set; }

        /// <summary>
        /// Можно ли публиковать на web
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Логин администратора
        /// </summary>
        public string AdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора
        /// </summary>
        public string AdminPassword { get; set; }

        /// <summary>
        /// Необходимо ли автообновление
        /// </summary>
        public bool NeedUpdate { get; set; }
    }
}
