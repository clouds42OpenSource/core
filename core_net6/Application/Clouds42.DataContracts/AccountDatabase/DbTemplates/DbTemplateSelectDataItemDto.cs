﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель свойств элемента шаблона
    /// </summary>
    public sealed class DbTemplateSelectDataItemDto
    {
        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание шаблона
        /// </summary>
        public string DefaultCaption { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string Platform { get; set; }
    }
}