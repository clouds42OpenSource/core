﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель результата на добавление нового шаблона
    /// </summary>
    public sealed class AddDbTemplateResultDto
    {
        /// <summary>
        /// ID шаблона после добавления
        /// </summary>
        public Guid DbTemplateId { get; set; }
    }
}