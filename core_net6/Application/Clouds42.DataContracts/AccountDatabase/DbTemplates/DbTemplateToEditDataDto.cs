﻿namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель ответа на запрос редактирования шаблона
    /// </summary>
    public sealed class DbTemplateToEditDataDto
    {
        /// <summary>
        /// Шаблон для редактирования
        /// </summary>
        public DbTemplateItemDto DbTemplateToEdit { get; set; }
    }
}