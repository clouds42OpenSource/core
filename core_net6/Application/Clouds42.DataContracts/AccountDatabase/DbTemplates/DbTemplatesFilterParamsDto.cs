﻿namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Фильтр для выбора шаблонов
    /// </summary>
    public sealed class DbTemplatesFilterParamsDto
    {
        /// <summary>
        /// Название шаблона
        /// </summary>
        public string DbTemplateName { get; set; }

        /// <summary>
        /// Описание шаблона
        /// </summary>
        public string DbTemplateDescription { get; set; }
    }
}