﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Информация о информационной базе для демо аккаунта
    /// </summary>
    public class DemoAccountDatabaseInfoDto
    {
        /// <summary>
        /// База для демо аккаунта
        /// </summary>
        public string DemoAccountDatabaseCaption { get; set; }

        /// <summary>
        /// Id базы для демо аккаунта
        /// </summary>
        public Guid? DemoAccountDatabaseId { get; set; }
    }
}
