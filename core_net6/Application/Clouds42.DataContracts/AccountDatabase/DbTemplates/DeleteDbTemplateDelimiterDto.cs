﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель свойств элемента шаблона базы на разделителях для удаления
    /// </summary>
    public sealed class DeleteDbTemplateDelimiterDto
    {  
        /// <summary>
        /// Код конфигурации
        /// </summary>
        [Required(ErrorMessage = "Код конфигурации - обязательное поле")]
        [StringLength(10, ErrorMessage = "Длина не должна превышать 10 символов")]
        public string ConfigurationId { get; set; }

    }
}