﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель на запрос редактирования
    /// </summary>
    public sealed class DbTemplateEditDto
    {
        /// <summary>
        /// ID шаблона для редактирования
        /// </summary>
        public Guid? DbTemplateId { get; set; }
    }
}
