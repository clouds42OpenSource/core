﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель свойств элемента шаблона для удаления
    /// </summary>
    public sealed class DeleteDbTemplateDataItemDto
    {
        /// <summary>
        /// Id шаблона для удаления
        /// </summary>
        [Required(ErrorMessage = "ID шаблона - обязательное поле")]
        public Guid DbTemplateId { get; set; }
    }
}