﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель ответа на запрос получения шаблонов
    /// </summary>
    public sealed class DbTemplatesDataResultDto: SelectDataResultCommonDto<DbTemplateSelectDataItemDto>
    {
    }
}