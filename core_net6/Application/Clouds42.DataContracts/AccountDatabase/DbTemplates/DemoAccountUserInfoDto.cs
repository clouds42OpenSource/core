﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Информация о пользователе для демо аккаунта
    /// </summary>
    public class DemoAccountUserInfoDto
    {
        /// <summary>
        /// Пользователя для демо аккаунта
        /// </summary>
        public string DemoAccountUserCaption { get; set; }

        /// <summary>
        /// Id пользователя для демо аккаунта
        /// </summary>
        public Guid? DemoAccountUserId { get; set; }
    }
}
