﻿using System;
using Clouds42.Domain.Enums.DbTemplateUpdates;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель обновлённого файлового шаблона
    /// </summary>
    public class UpdateDatabaseTemplateResultDto
    {
        /// <summary>
        /// Статус шаблона
        /// </summary>
        public UpdateDatabaseTemplateStateEnum UpdateDatabaseTemplateState { get; set; }

        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Сообщение с результатом обработки шаблона
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateDefaultCaption { get; set; }

        /// <summary>
        /// Текущая версия шаблона
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Путь до обновлённого шаблона
        /// </summary>
        public string UpdateTemplatePath { get; set; }

        /// <summary>
        /// Новая версия шаблона
        /// </summary>
        public string NewVersion { get; set; }
    }
}
