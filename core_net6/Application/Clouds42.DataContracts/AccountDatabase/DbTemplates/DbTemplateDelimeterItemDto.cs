﻿using System;
using System.ComponentModel.DataAnnotations;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель Шаблона
    /// </summary>
    public sealed class DbTemplateDelimeterItemDto : IDbTemplateDelimiters
    {
        /// <summary>
        /// Код конфигурации
        /// </summary>
        [Required(ErrorMessage = "Код конфигурации - обязательное поле")]
        [StringLength(10, ErrorMessage = "Длина не должна превышать 10 символов")]
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        [Required(ErrorMessage = "Название конфигурации - обязательное поле")]
        [StringLength(100, ErrorMessage = "Длина не должна превышать 100 символов")]
        public string Name { get; set; }

        /// <summary>
        /// Имя шаблона
        /// </summary>
        [Required(ErrorMessage = "Имя шаблона - обязательное поле")]
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Короткое имя конфигурации(для загрузки zip)
        /// </summary>
        [Required(ErrorMessage = "Имя конфигурации - обязательное поле")]
        [StringLength(200, ErrorMessage = "Длина не должна превышать 200 символов")]
        public string ShortName { get; set; }

        /// <summary>
        /// Демо публикация
        /// </summary>
        [Required(ErrorMessage = "Демо публикация обязательное поле")]
        public string DemoDatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Текущая версия релиза конфигурации
        /// </summary>
        [StringLength(20, ErrorMessage = "Длина не должна превышать 20 символов")]
        [Required(ErrorMessage = "Текущая версия релиза конфигурации - обязательное поле")]
        public string ConfigurationReleaseVersion { get; set; }

        /// <summary>
        /// Минимальная версия конфигурации для обновления
        /// </summary>
        [StringLength(20, ErrorMessage = "Длина не должна превышать 20 символов")]
        [Required(ErrorMessage = "Минимальная версия - обязательное поле")]
        public string MinReleaseVersion { get; set; }
    }
}