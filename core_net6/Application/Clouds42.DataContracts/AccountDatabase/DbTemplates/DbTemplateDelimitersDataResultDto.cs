﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Модель ответа на запрос получения шаблонов баз на разделителях
    /// </summary>
    public sealed class DbTemplateDelimitersDataResultDto : SelectDataResultCommonDto<DbTemplateDelimeterItemDto>
    {
    }
}