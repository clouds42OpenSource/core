﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Информация об аккаунте для выпадающего списка.
    /// </summary>
    public class AccountInfoDto
    {
        [Required(ErrorMessage = "Материнская база - обязательное поле")]
        public string AccountCaption { get; set; }
        public Guid AccountId { get; set; }
    }
}