﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class TemlateDelimiterDto
    {
        [XmlElement(ElementName = nameof(СonfigurationCode))]
        [DataMember(Name = nameof(СonfigurationCode))]
        public string СonfigurationCode { get;set;}

        [XmlElement(ElementName = nameof(ReleaseVersion))]
        [DataMember(Name = nameof(ReleaseVersion))]
        public string ReleaseVersion { get;set;}
 
    }
}