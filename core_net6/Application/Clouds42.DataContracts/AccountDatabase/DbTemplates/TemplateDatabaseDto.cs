﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// Шаблонная информационная база.
    /// </summary>
    public class TemplateDatabaseDto : IUpdateDatabaseDto
    {

        /// <summary>
        /// Назание базы.
        /// </summary>
        public string UpdateDatabaseName { get; set; }

        /// <summary>
        /// База является файловой.
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Логин администратор.
        /// </summary>
        public string AdminLogin { get; set; }

        /// <summary>
        /// Пароль администратора.
        /// </summary>
        public string AdminPassword { get; set; }

        /// <summary>
        /// Адрес базы.
        /// </summary>
        public string ConnectionAddress { get; set; }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        public PlatformType PlatformType { get; set; }

        /// <summary>
        /// Версия платформы под которой работает база.
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// Путь до папки где лежит платформа 1С.
        /// </summary>
        public string FolderPath1C { get; set; }
    }
}