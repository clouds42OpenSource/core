﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    public class DbTemplateUpdateModelDto
    {
        public IDbTemplateUpdate Update { get; set; }

        public IDbTemplate Template { get; set; }

        public IAccountUser ResponsibleAccountUser { get; set; }

    }
}
