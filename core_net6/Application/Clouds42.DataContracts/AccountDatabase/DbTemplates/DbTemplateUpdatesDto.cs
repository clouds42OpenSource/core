﻿using System;
using Clouds42.Domain.Enums.DbTemplateUpdates;

namespace Clouds42.DataContracts.AccountDatabase.DbTemplates
{
    /// <summary>
    /// DTO модель для обновления шаблона баз
    /// </summary>
    public class DbTemplateUpdatesDto
    {
        /// <summary>
        /// ID Обновления шаблона баз
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Заголовок по умолчанию у шаблона базы
        /// </summary>
        public string DefaultCaption { get; set; }

        /// <summary>
        /// Версия обновления
        /// </summary>
        public string UpdateVersion { get; set; }

        /// <summary>
        /// Дата обновления
        /// </summary>
        public DateTime? UpdateVersionDate { get; set; }

        /// <summary>
        /// Полный путь до обновленного шаблона
        /// </summary>
        public string UpdateTemplatePath { get; set; }

        /// <summary>
        /// Статус проверки обновления на ошибки человеком
        /// </summary>
        public DbTemplateUpdateState ValidateState { get; set; }
    }
}