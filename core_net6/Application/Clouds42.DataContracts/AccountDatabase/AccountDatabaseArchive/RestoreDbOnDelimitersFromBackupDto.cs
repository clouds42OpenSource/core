﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель восстановления базы на разделителях из архива
    /// </summary>
    public class RestoreDbOnDelimitersFromBackupDto
    {
        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseName { get; set; }
    }
}
