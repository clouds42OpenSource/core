﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров для копирования инф. базы в склеп
    /// </summary>
    public class CopyAccountDatabaseBackupToTombParamsDto : BaseAccountDatabaseOperationParamsDto
    {
        /// <summary>
        /// ID бэкапа информационной базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Триггер для создания бэкапа инф. базы
        /// </summary>
        public CreateBackupAccountDatabaseTrigger BackupAccountDatabaseTrigger { get; set; }

        /// <summary>
        /// ID файла бэкапа инф. базы на гугл диске
        /// </summary>
        public string AccountDatabaseBackupFileId { get; set; }

        /// <summary>
        /// ID файлового хранилища, в которое нужно перенести базу
        /// (Нужен только если действия копирования используются при миграции инф. базы)
        /// </summary>
        public Guid DestinationFileStorageId { get; set; }

        /// <summary>
        /// Принудительная загрузка файла
        /// </summary>
        public bool ForceUpload { get; set; } = true;

        public List<string> ActionsForSkip { get; set; }

        public bool ActionsContains(string actionName)
        {
            return ActionsForSkip != null && ActionsForSkip.Contains(actionName);
        }
    }
}
