﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров задачи по загрузке файла бэкапа
    /// </summary>
    public class BackupFileFetchingJobParamsDto
    {
        /// <summary>
        /// Id бэкапа
        /// </summary>
        public Guid BackupId { get; set; }

        /// <summary>
        /// Id загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }
    }
}
