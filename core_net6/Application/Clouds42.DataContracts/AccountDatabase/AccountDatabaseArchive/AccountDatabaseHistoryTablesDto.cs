﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Таблицы для историй бэкапов баз аккаунтов
    /// </summary>
    public class AccountDatabaseHistoryTablesDto
    {
        /// <summary>
        /// Список старых историй бэкапов баз аккаунтов с пагинацией
        /// </summary>
        public AccountDatabaseBackupHistoryPaginationDto DangerTable { get; set; }

        /// <summary>
        /// Список новых историй бэкапов баз аккаунтов с пагинацией
        /// </summary>
        public AccountDatabaseBackupHistoryPaginationDto WarningTable { get; set; }

        /// <summary>
        /// Период старых историй бэкапов баз аккаунтов 
        /// </summary>
        public AccountDatabaseBackupHistoryPeriodDto DangerTablePeriod { get; set; }

        /// <summary>
        /// Период новых историй бэкапов баз аккаунтов 
        /// </summary>
        public AccountDatabaseBackupHistoryPeriodDto WarningTablePeriod { get; set; }
    }
}