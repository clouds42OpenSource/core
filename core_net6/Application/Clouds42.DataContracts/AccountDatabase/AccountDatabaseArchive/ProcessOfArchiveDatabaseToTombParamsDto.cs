﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    public class ProcessOfArchiveDatabaseToTombParamsDto : StartProcessOfArchiveDatabaseToTombParamsDto
    {
        /// <summary>
        /// Id инициатора
        /// </summary>
        public Guid InitiatorId { get; set; }
    }
}
