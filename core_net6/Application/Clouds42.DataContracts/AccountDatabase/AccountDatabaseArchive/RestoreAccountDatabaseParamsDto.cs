﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров восстановления инф. базы
    /// </summary>
    public class RestoreAccountDatabaseParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseName { get; set; }

        /// <summary>
        /// Тип восстановления
        /// </summary>
        public AccountDatabaseBackupRestoreType RestoreType { get; set; }

        /// <summary>
        /// Путь к бэкапу инф. базы
        /// </summary>
        public string AccountDatabaseBackupPath { get; set; }

        /// <summary>
        /// Признак небходимости менять статус базы
        /// </summary>
        public bool NeedChangeState { get; set; } = true;

        /// <summary>
        /// Признак необходимости отправлять уведомление
        /// </summary>
        public bool NeedSendEmail { get; set; } = true;

        /// <summary>
        /// Ключ рабочего процесса
        /// </summary>
        public string ProcessFlowKey { get; set; }

        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Список ID паользователей для предоставления доступа
        /// </summary>
        public List<Guid> UsersIdForAddAccess { get; set; }
    }
}
