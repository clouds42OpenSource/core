﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель для аудита и корректировки моделей восстановления инф. баз
    /// </summary>
    public class ManageAcDbRestoreModelDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }

        /// <summary>
        /// Модель восстановления инф. базы
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? AccountDatabaseRestoreModel { get; set; }

        /// <summary>
        /// Модель восстановления на SQL сервере
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum SqlServerRestoreModel { get; set; }
    }
}
