﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель для списка баз которые необходимо заархивировать
    /// </summary>
    public class ArchiveDatabaseModelDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid DataBaseId { get; set; }
        
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
        
        /// <summary>
        /// Признак что аккаунт демо
        /// </summary>
        public bool IsDemo { get; set; }
        
        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Признак что база на разделителях
        /// </summary>
        public bool IsDelimiter { get; set; }

    }
}