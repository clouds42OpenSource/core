﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Маппер модели параметров для запуска процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    public static class StartProcessOfArchiveDatabaseToTombParamsMapperDto
    {
        /// <summary>
        /// Выполнить маппинг к модели параметров процесса на архивацию
        /// информационной базы в склеп
        /// </summary>
        /// <param name="model">Модель параметров для запуска процесса на архивацию
        /// информационной базы в склеп</param>
        /// <param name="initiatorId">Id инициатора</param>
        /// <returns></returns>
        public static ProcessOfArchiveDatabaseToTombParamsDto MapToProcessOfArchiveDatabaseToTombParamsDc(
            this StartProcessOfArchiveDatabaseToTombParamsDto model, Guid initiatorId) =>
            new()
            {
                DatabaseId = model.DatabaseId,
                Trigger = model.Trigger,
                NeedForceFileUpload = model.NeedForceFileUpload,
                NeedNotifyHotlineAboutFailure = model.NeedNotifyHotlineAboutFailure,
                InitiatorId = initiatorId
            };
    }
}