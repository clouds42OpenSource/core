﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель данных для управления открытыми файлами
    /// инф. базы на файловом хранилище
    /// </summary>
    public class ManageOpenDatabaseFilesOnFileStorageDataDto
    {
        /// <summary>
        /// Номер инф. базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// DNS имя хранилища
        /// </summary>
        public string DnsName { get; set; }
    }
}
