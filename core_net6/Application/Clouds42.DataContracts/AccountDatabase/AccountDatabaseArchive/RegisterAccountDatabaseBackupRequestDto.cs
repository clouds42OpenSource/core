﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель регистрации бэкапа инфомационной базы.
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RegisterAccountDatabaseBackupRequestDto
    {
        /// <summary>
        /// Номер информационной базы.
        /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Полный путь до архива копии.
        /// </summary>
        [XmlElement(ElementName = nameof(BackupFullPath))]
        [DataMember(Name = nameof(BackupFullPath))]
        public string BackupFullPath { get; set; }

        /// <summary>
        /// Время создания бекапа
        /// </summary>
        [XmlElement(ElementName = nameof(CreationBackupDateTime))]
        [DataMember(Name = nameof(CreationBackupDateTime))]
        public DateTime? CreationBackupDateTime { get; set; }
        
        /// <summary>
        /// Коментарий.
        /// </summary>
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }  

        /// <summary>
        /// Триггер по которому проводится бэкапирование.
        /// </summary>
        [XmlElement(ElementName = nameof(Trigger))]
        [DataMember(Name = nameof(Trigger))]
        public CreateBackupAccountDatabaseTrigger Trigger { get; set; }

        /// <summary>
        /// Состояние бэкапа.
        /// </summary>
        [XmlElement(ElementName = nameof(State))]
        [DataMember(Name = nameof(State))]
        public AccountDatabaseBackupHistoryState State { get; set; }

        /// <summary>
        /// ID бэкапа инф. базы от МС
        /// </summary>
        [XmlIgnore]
        public Guid? ServiceManagerAcDbBackupId
        {
            get
            {
                var backupId = Guid.Empty;

                if (!string.IsNullOrEmpty(ServiceManagerAcDbBackupIdString))
                    Guid.TryParse(ServiceManagerAcDbBackupIdString, out backupId);

                return backupId.IsNullOrEmpty() 
                    ? (Guid?) null
                    : backupId;
            }
            set
            {
                ServiceManagerAcDbBackupIdString = value.ToString();
            }
        }

        /// <summary>
        /// ID бэкапа инф. базы от МС строкой
        /// </summary>
        [XmlElement(ElementName = "ApplicationBackupID")]
        [DataMember(Name = "ApplicationBackupID")]
        public string ServiceManagerAcDbBackupIdString { get; set; }
    }
}
