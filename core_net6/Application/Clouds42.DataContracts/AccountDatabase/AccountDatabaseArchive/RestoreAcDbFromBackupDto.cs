﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Model of infobase recovery parameters from backup
    /// </summary>
    public class RestoreAcDbFromBackupDto : BackupRestoringBaseDataDto
    {
        /// <summary>
        /// Restore type
        /// </summary>
        public AccountDatabaseBackupRestoreType RestoreType { get; set; }

        /// <summary>
        /// List of user identifiers to grant access
        /// </summary>
        public List<Guid> UsersIdForAddAccess { get; set; } = [];

        /// <summary>
        /// "My infobases" service identifier
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Need using Promise Payment
        /// </summary>
        public bool NeedUsePromisePayment { get; set; }
    }
}
