﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    public class AccountDatabaseBackupHistoryDto
    {
        public IAccountDatabaseBackupHistory AccountDatabaseBackupHistory { get; set; }
        public IAccountDatabase AccountDatabase { get; set; }
        public int AccountIndexNumber { get; set; }
        public Guid AccountId { get; set; }
    }

    public class AccountDatabaseBackupArmDataDto
    {
        /// <summary>
        /// Красная таблица, бэкапы не сделанные 7 дней и больше. 
        /// </summary>
        public List<AccountDatabaseBackupHistoryDto> RedTable { get; set; }

        /// <summary>
        /// Желтая таблица, бэкапы не сделанные от 3 до 6-ти дней. 
        /// </summary>
        public List<AccountDatabaseBackupHistoryDto> YellowTable { get; set; }
    }
}
