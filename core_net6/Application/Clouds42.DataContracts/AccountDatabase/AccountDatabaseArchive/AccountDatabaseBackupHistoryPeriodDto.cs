﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель периода от и до для историй бэкапов баз аккаунта
    /// </summary>
    public class AccountDatabaseBackupHistoryPeriodDto
    {
        public AccountDatabaseBackupHistoryPeriodDto()
        {
        }

        public AccountDatabaseBackupHistoryPeriodDto(int fromDays, int? toDays = null)
        {
            FromDays = fromDays;
            ToDays = toDays;
        }

        /// <summary>
        /// Период от
        /// </summary>
        public int FromDays { get; set; }

        /// <summary>
        /// Период до
        /// </summary>
        public int? ToDays { get; set; }
    }
}