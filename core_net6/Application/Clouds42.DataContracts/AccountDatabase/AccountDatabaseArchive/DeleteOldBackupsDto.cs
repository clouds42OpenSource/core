﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    public class DeleteOldBackupsDto
    {
        public int Delay { get; set; }
        public CreateBackupAccountDatabaseTrigger Trigger { get; set; }
    }
}
