﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров процесса удаления инф. базы
    /// </summary>
    public class ProcessOfRemoveDatabaseParamsDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Id инициатора
        /// </summary>
        public Guid InitiatorId { get; set; }

        /// <summary>
        /// Финальный статус удаления инф. базы
        /// </summary>
        public DatabaseState FinalDeletionDatabaseStatus { get; set; }
    }
}
