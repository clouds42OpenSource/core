﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    ///     Модель параметров воркера для таски DeleteAccountDatabaseToTombJob
    /// </summary>
    public class DeleteAccountDatabaseToTombJobParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Признак необходимости отправки письма клиенту
        /// </summary>
        public bool SendMail { get; set; }

        /// <summary>
        /// Принудительная загрузка файла
        /// </summary>
        public bool ForceUpload { get; set; }

        /// <summary>
        /// Триггер создания бэкапа инф. базы
        /// </summary>
        public CreateBackupAccountDatabaseTrigger Trigger { get; set; }

        /// <summary>
        /// ID инициатора операции
        /// </summary>
        public Guid InitiatorId { get; set; }

        public List<string> ActionsForSkip { get; set; }
    }
}
