﻿using System;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель для базы аккаунта с историями бэкапа
    /// </summary>
    public class AccountDatabaseHistoryDto
    {
        /// <summary>
        /// История бэкапа баз актуальной успешной
        /// </summary>
        public IAccountDatabaseBackupHistory LastDatabaseBackupHistoryDone { get; set; }

        /// <summary>
        /// История бэкапа баз актуальной с ошибкой
        /// </summary>
        public IAccountDatabaseBackupHistory LastDatabaseBackupHistoryError { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// ID Аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
    }
}