﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров задачи по загрузке бэкапа базы на разделителях
    /// </summary>
    public class UploadDbOnDelimitersBackupJobParamsDto
    {
        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }
    }
}
