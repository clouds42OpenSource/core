﻿using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель историй бэкапов баз аккаунтов с пагинацией
    /// </summary>
    public class AccountDatabaseBackupHistoryPaginationDto
    {
        /// <summary>
        /// Список историй бэкапов баз аккаунтов
        /// </summary>
        public List<DatabaseBackupHistoryPaginationDto> Records { get; set; }

        /// <summary>
        /// Пагинация
        /// </summary>
        public PaginationBaseDto Pagination { get; set; }
    }
}