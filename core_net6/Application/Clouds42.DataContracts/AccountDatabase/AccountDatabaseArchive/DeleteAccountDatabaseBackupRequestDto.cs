﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров удаления бэкапа инф. базы
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class DeleteAccountDatabaseBackupRequestDto
    {
        /// <summary>
        /// ID бэкапа инф. базы от МС
        /// </summary>
        [XmlElement(ElementName = "ApplicationBackupID")]
        [DataMember(Name = "ApplicationBackupID")]
        public Guid ServiceManagerAcDbBackupId { get; set; }
    }
}
