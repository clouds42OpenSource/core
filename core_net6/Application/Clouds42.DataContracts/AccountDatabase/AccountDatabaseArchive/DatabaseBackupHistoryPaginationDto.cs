﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель истории бэкапов баз
    /// </summary>
    public class DatabaseBackupHistoryPaginationDto
    {
        /// <summary>
        /// ID истории бэкапа базы аккаунта
        /// </summary>
        public Guid AccountDatabaseBackupHistoryId { get; set; }

        /// <summary>
        /// Дата создания записи.
        /// </summary>
        public DateTime? AccountDatabaseBackupHistoryCreateDateTime { get; set; }

        /// <summary>
        /// Коментарий архивации.
        /// </summary>
        public string AccountDatabaseBackupHistoryMessage { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// ID Аккаунта
        /// </summary>
        public Guid AccountId { get; set; }
    }
}