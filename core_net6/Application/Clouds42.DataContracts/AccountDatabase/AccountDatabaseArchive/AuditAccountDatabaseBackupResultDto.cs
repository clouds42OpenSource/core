﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Результат аудита бэкапа инф. базы
    /// </summary>
    public class AuditAccountDatabaseBackupResultDto
    {
        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid? AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Аудит выполнен успешно
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Путь к файлу бэкапа
        /// </summary>
        public string BackupPath { get; set; }
    }
}
