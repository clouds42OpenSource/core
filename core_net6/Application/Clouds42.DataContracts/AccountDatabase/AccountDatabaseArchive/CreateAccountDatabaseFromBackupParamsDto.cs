﻿using System;

using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров создания инф. базы на основании бэкапа
    /// </summary>
    public class CreateAccountDatabaseFromBackupParamsDto
    {
        /// <summary>
        /// ID созданной инф. базы
        /// </summary>
        public Guid CreatedAccountDatabaseId { get; set; }

        /// <summary>
        /// Ключ родительского рабочего процесса
        /// </summary>
        public string ParentProcessFlowKey { get; set; }

        /// <summary>
        /// Параметры создания инф. базы
        /// </summary>
        public InfoDatabaseDomainModelDto AccountDatabaseCreationParams { get; set; }
    }
}
