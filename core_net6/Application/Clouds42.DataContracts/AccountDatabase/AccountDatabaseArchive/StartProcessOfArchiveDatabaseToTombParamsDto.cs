﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Модель параметров для запуска процесса на архивацию
    /// информационной базы в склеп
    /// </summary>
    public class StartProcessOfArchiveDatabaseToTombParamsDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Уведомить хотлайн о неудачной архивации
        /// </summary>
        public bool NeedNotifyHotlineAboutFailure { get; set; }

        /// <summary>
        /// Необходима принудительная загрузка файла в склеп
        /// </summary>
        public bool NeedForceFileUpload { get; set; }

        /// <summary>
        /// Триггер отправки базы в склеп
        /// </summary>
        public CreateBackupAccountDatabaseTrigger Trigger { get; set; }
    }
}
