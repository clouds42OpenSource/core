﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Database recovery model
    /// </summary>
    public class BackupRestoringDto : BackupRestoringBaseDataDto
    {
        /// <summary>
        /// Uri for upload backup files
        /// </summary>
        public string UploadFilesUri { get; set; }

        /// <summary>
        /// Currency locale name
        /// </summary>
        public string CurrencyName { get; set; }

        /// <summary>
        /// Is infobase on delimiters
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// E-mail addresses to which a notification will be sent after the database is restored from the archive
        /// </summary>
        public List<string> EmailAddresses { get; set; }

        /// <summary>
        /// "My infobases" service identifier
        /// </summary>
        public Guid? MyDatabasesServiceId { get; set; }
    }
}
