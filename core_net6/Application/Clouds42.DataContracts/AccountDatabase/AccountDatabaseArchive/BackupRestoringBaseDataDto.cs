﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive
{
    /// <summary>
    /// Base data model for restoring a database from a backup
    /// </summary>
    public class BackupRestoringBaseDataDto
    {
        /// <summary>
        /// Infobase backup identifier
        /// </summary>
        public Guid AccountDatabaseBackupId { get; set; }

        /// <summary>
        /// Uploaded backup file identifier
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Infobase name
        /// </summary>
        public string AccountDatabaseName { get; set; }
    }
}
