﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    public class RegisterDatabaseOnDelimiterModelDto : RegisterDatabaseModelDto
    {
        /// <summary>
        /// Список номер аккаунт юзеров к которым должен быть предоставлен доступ.
        /// </summary>
        public List<Guid> AccountUserListToBeAddAccess { get; set; }

        /// <summary>
        /// Признак база демо или нет
        /// </summary>
        public bool IsDemoDatabase { get; set; }

    }
}
