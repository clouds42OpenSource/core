﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    /// <summary>
    /// Модель регистрации инф. базы из загруженного файла
    /// </summary>
    public class RegisterDatabaseFromUploadFileModelDto : RegisterDatabaseModelDto
    {
        /// <summary>
        /// Идентификатор загруженного файла.
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Список ID пользователей для предоставления доступов
        /// </summary>
        public List<Guid> UsersIdListForAddAccess { get; set; } = [];

        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid? TemplateId { get; set; }

        
        /// <summary>
        /// Флаг позволяющий вызывать готовый механизм создания серверных баз
        /// </summary>
        public bool CreateClusterForce { get; set; }
    }
}
