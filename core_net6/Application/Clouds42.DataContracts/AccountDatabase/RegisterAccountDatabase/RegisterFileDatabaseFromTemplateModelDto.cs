﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    /// <summary>
    /// Модель регистрации инф. базы из шаблона
    /// </summary>
    public class RegisterFileDatabaseFromTemplateModelDto : RegisterDatabaseModelDto
    {
        /// <summary>
        /// Признак необходимости публиковать инф. базу
        /// </summary>
        public bool NeedPublish { get; set; }

        /// <summary>
        /// Список ID пользователей для предоставления доступа
        /// </summary>
        public List<Guid> UsersIdsForAddAccess { get; set; } = [];
    }
}