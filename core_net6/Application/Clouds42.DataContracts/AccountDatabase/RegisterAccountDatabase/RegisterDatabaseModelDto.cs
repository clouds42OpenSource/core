﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    public class RegisterDatabaseModelDto
    {
        public Guid AccountDatabaseId { get; set; }
    }
}