﻿
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    public class RegisterDatabaseFromZipModelDto : RegisterDatabaseFromUploadFileModelDto
    {
        /// <summary>
        /// Список информации о пользователе полученного из зип пакета
        /// </summary>
        public List<InfoAboutUserFromZipPackageDto> ListInfoAboutUserFromZipPackage { get; set; }

        /// <summary>
        /// Список сервисов
        /// </summary>
        [JsonProperty("extensions")]
        public List<ExtensionIds> ExtensionsList { get; set; }
    }
}
