﻿
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.RegisterAccountDatabase
{
    public class RegisterDatabaseFromDTToZipModelDto : RegisterDatabaseFromUploadFileModelDto
    {
        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string LoginAdmin { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string PasswordAdmin { get; set; }

        /// <summary>
        /// Список информации о пользователе полученного из зип пакета
        /// </summary>
        public List<InfoAboutUserFromZipPackageDto> ListInfoAboutUserFromZipPackage { get; set; }
    }
}
