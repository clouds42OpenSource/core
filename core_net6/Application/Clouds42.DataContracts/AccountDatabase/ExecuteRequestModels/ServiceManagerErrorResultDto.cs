﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Ошибочный результат выполнения запроса в МС
    /// </summary>
    public class ServiceManagerErrorResultDto
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        [JsonProperty("error")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// Описание ошибки
        /// </summary>
        [JsonProperty("message")]
        public string Description { get; set; }

        /// <summary>
        /// Доп информация об ошибке
        /// </summary>
        [JsonProperty("detail")]
        public string DebugInfo { get; set; }
    }
}
