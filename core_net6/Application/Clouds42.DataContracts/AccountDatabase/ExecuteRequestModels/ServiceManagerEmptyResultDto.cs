﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Пустой результат выполнения запроса в МС
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    public class ServiceManagerEmptyResultDto
    {
    }
}
