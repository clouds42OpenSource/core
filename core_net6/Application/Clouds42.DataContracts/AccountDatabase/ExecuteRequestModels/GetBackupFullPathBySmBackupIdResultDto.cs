﻿using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Результат получения полного пути к бэкапу по ID от МС
    /// </summary>
    public class GetBackupFullPathBySmBackupIdResultDto
    {
        /// <summary>
        /// Путь к бэкапу
        /// </summary>
        [JsonProperty("file-path")]
        public string BackupPath { get; set; }
    }
}
