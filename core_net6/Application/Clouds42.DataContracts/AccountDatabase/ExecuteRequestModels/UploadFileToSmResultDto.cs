﻿using System;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Модель результата загрузки файла в МС
    /// </summary>
    public class UploadFileToSmResultDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        [JsonProperty("Id")]
        public Guid UploadedFileId { get; set; }
    }
}
