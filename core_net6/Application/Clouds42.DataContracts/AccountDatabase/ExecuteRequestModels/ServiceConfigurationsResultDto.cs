﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Модель результата получения списка конфигураций для сервиса
    /// </summary>
    public class ServiceConfigurationsResultDto
    {
        [JsonProperty("Result")]
        public List<Configurations> ConfigurationList { get; set; } = [];
    }


    public class Configurations
    {
        public string ConfigurationID { get; set; }
        public string ConfigurationName { get; set; }
    }


}
