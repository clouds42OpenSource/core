﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.AccountUsers;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Результат смены состояния блокировки пользователя
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    public class ChangeAccountUserLockStateResultDto
    {
        /// <summary>
        /// Состояние блокировки
        /// </summary>
        [XmlElement(ElementName = "State")]
        [DefaultValue(AccountUserLockState.Undefined)]
        public AccountUserLockState LockState { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        [XmlElement(ElementName = "Date")]
        public DateTime UpdateStateDateTime { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [XmlElement(ElementName = "Error")]
        public string ErrorMessage { get; set; }
    }
}
