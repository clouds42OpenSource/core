﻿using System;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Модель запроса на проверку статуса доступа
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    public class CheckAcDbAccessStateRequestDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        [XmlElement]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        [XmlElement(ElementName = "UserId")]
        public Guid AccountUserId { get; set; }
    }
}
