﻿using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Получение списка профилей запроса в МС
    /// </summary>
    public class ProfilesListResultDto
    {
        /// <summary>
        /// Код конфигурациии
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// Имя конфигурации
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Лист профилей
        /// </summary>
        public List<ProfilesUserFromZipPackageDto> profiles { get; set; } = [];
    }

}
