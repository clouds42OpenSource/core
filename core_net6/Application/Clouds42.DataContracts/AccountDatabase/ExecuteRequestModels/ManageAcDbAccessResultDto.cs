﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels
{
    /// <summary>
    /// Модель ответа на проверку статуса доступа
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    public class ManageAcDbAccessResultDto
    {
        /// <summary>
        /// Статус доступа
        /// </summary>
        [XmlElement(ElementName = "State")]
        [DefaultValue(AccountDatabaseAccessState.Undefined)]
        public AccountDatabaseAccessState AccessState { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        [XmlElement(ElementName = "Date")]
        public DateTime UpdateStateDateTime { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [XmlElement(ElementName = "Error")]
        public string ErrorMessage { get; set; }
    }
}
