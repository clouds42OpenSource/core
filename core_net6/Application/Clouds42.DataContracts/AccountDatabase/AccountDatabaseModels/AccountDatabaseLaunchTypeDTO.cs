﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLaunchTypeDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseLaunchType))]
        [DataMember(Name = nameof(AccountDatabaseLaunchType))]
        public int AccountDatabaseLaunchType { get; set; }

        public AccountDatabaseLaunchTypeDto()
        {
        }

        public AccountDatabaseLaunchTypeDto(int accountDatabaseLaunchType)
        {
            AccountDatabaseLaunchType = accountDatabaseLaunchType;
        }
    }
}