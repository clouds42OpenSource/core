﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestAccountIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        public bool IsValid
        {
            get { return AccountID != Guid.Empty; }
        }
    }
}
