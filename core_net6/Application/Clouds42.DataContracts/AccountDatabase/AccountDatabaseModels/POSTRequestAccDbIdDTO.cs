﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestAccDbIdDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseId))]
        [DataMember(Name = nameof(AccountDatabaseId))]
        public Guid AccountDatabaseId { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseId != Guid.Empty; }
        }
    }
}
