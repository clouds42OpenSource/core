﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseConfigurationVersionDto
    {
        [XmlElement(ElementName = nameof(ConfigurationVersion))]
        [DataMember(Name = nameof(ConfigurationVersion))]
        public string ConfigurationVersion { get; set; }

        public AccountDatabaseConfigurationVersionDto()
        {
        }

        public AccountDatabaseConfigurationVersionDto(string configurationVersion)
        {
            ConfigurationVersion = configurationVersion;
        }
    }
}