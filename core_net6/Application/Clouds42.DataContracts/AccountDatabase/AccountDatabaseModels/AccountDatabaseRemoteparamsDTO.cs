﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    // ReSharper disable once InconsistentNaming
    public class AccountDatabaseRemoteParamsDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseRemoteParams))]
        [DataMember(Name = nameof(AccountDatabaseRemoteParams))]
        public string AccountDatabaseRemoteParams { get; set; }

        [XmlElement(ElementName = nameof(UsingOutdatedWindows))]
        [DataMember(Name = nameof(UsingOutdatedWindows))]
        public bool UsingOutdatedWindows { get; set; }

        public AccountDatabaseRemoteParamsDto()
        {
        }

        public AccountDatabaseRemoteParamsDto(string value)
        {
            AccountDatabaseRemoteParams = value;
        }
    }
}
