﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLastPingDto
    {
        [XmlElement(ElementName = nameof(LastPingDateTime))]
        [DataMember(Name = nameof(LastPingDateTime))]
        public DateTime LastPingDateTime { get; set; }

        public AccountDatabaseLastPingDto()
        {
        }

        public AccountDatabaseLastPingDto(DateTime lastPingDateTime)
        {
            LastPingDateTime = lastPingDateTime;
        }
    }
}
