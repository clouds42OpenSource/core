﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    // ReSharper disable once InconsistentNaming
    public class AccountDatabaseListDto(List<AccountDatabasePropertiesDto> list) : IAccountDatabaseListDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseList))]
        [DataMember(Name = nameof(AccountDatabaseList))]
        public List<AccountDatabasePropertiesDto> AccountDatabaseList { get; set; } = list;

        public AccountDatabaseListDto() : this([])
        {
        }
    }
}
