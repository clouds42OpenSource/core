﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesIDsListDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabasesIDs))]
        [DataMember(Name = nameof(AccountDatabasesIDs))]
        public GuidListItemDto AccountDatabasesIDs { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AccountDatabasesIDsListDto()
        {
            AccountDatabasesIDs = new GuidListItemDto();
        }
    }
}