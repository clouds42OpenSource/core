﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseNameDto
    {
        [XmlElement(ElementName = nameof(DatabaseName))]
        [DataMember(Name = nameof(DatabaseName))]
        public string DatabaseName { get; set; }

        public AccountDatabaseNameDto()
        {
        }

        public AccountDatabaseNameDto(string databaseName)
        {
            DatabaseName = databaseName;
        }
    }
}