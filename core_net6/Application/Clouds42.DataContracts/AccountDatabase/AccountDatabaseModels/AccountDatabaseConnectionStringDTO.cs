﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseConnectionStringDto
    {
        [XmlElement(ElementName = nameof(ConnectionString))]
        [DataMember(Name = nameof(ConnectionString))]
        public string ConnectionString { get; set; }

        public AccountDatabaseConnectionStringDto()
        {
        }

        public AccountDatabaseConnectionStringDto(string connectionString)
        {
            ConnectionString = connectionString;
        }
    }
}