﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class PublishExpansionDto
    {
        [XmlElement(ElementName = nameof(PublishPath))]
        [DataMember(Name = nameof(PublishPath))]
        public string PublishPath { get; set; }

        public PublishExpansionDto(string PublishPath)
        {
            this.PublishPath = PublishPath;
        }

        public PublishExpansionDto()
        {
        }
    }
}
