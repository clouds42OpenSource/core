﻿using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseConfVersionDto
    {
        [XmlElement(ElementName = nameof(ConfigurationVersion))]
        [DataMember(Name = nameof(ConfigurationVersion))]
        public string ConfigurationVersion { get; set; }

        public AccountDatabaseConfVersionDto()
        {
        }

        public AccountDatabaseConfVersionDto(string configurationVersion)
        {
            ConfigurationVersion = configurationVersion;
        }

        public static AccountDatabaseConfVersionDto GetFromXml(XElement xml)
        {
            if(xml==null) return null;
            var node = xml.Descendants().FirstOrDefault();
            if (node==null) return null;
            var result = new AccountDatabaseConfVersionDto {ConfigurationVersion = node.Value.Trim()};
            return result;
        }
    }
}