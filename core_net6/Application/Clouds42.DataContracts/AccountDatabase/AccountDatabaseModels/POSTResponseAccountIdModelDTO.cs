﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostResponseAccountIdModelDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountId { get; set; }
    }
}