﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DelimitersDbsListDto(List<DelimitersDbsPropertiesDto> list)
    {
        [XmlElement(ElementName = "DbOnDelimitersProperties")]
        [DataMember(Name = "DbOnDelimitersProperties")]
        public List<DelimitersDbsPropertiesDto> DelimitersDtoList { get; set; } = list;

        public DelimitersDbsListDto() : this([])
        {
        }
    }
}
