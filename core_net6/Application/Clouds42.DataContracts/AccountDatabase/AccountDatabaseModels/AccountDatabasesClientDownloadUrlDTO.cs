﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesClientDownloadUrlDto
    {
        [XmlElement(ElementName = nameof(ThinClientDownloadLink))]
        [DataMember(Name = nameof(ThinClientDownloadLink))]
        public string ThinClientDownloadLink { get; set; }

        public AccountDatabasesClientDownloadUrlDto()
        {
        }

        public AccountDatabasesClientDownloadUrlDto(string thinClientDownloadLink)
        {
            ThinClientDownloadLink = thinClientDownloadLink;
        }
    }
}
