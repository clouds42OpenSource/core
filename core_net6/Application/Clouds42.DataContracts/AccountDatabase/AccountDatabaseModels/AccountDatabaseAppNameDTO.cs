﻿using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseAppNameDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(ApplicationName))]
        [DataMember(Name = nameof(ApplicationName))]
        public string ApplicationName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public AccountDatabaseAppNameDto()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationName"></param>
        public AccountDatabaseAppNameDto(string applicationName)
        {
            ApplicationName = applicationName;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static AccountDatabaseConfVersionDto GetFromXml(XElement xml)
        {
            if (xml == null) return null;
            var node = xml.Descendants().FirstOrDefault();
            if (node == null) return null;
            var result = new AccountDatabaseConfVersionDto { ConfigurationVersion = node.Value.Trim() };
            return result;
        }
    }
}