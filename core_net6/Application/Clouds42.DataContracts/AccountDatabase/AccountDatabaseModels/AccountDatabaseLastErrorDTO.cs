﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLastErrorDto
    {
        [XmlElement(ElementName = nameof(LastConnectionError))]
        [DataMember(Name = nameof(LastConnectionError))]
        public string LastConnectionError { get; set; }

        public AccountDatabaseLastErrorDto()
        {
        }

        public AccountDatabaseLastErrorDto(string lastConnectionError)
        {
            LastConnectionError = lastConnectionError;
        }
    }
}
