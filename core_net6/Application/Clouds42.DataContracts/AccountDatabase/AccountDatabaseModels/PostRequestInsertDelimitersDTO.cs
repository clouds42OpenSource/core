﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestInsertDelimitersDto
    {
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(Zone))]
        [DataMember(Name = nameof(Zone))]
        public int Zone { get; set; }

        [XmlElement(ElementName = nameof(SourceDatabaseId))]
        [DataMember(Name = nameof(SourceDatabaseId))]
        public Guid SourceDatabaseId { get; set; }


        [XmlElement(ElementName = nameof(ConfigurationId))]
        [DataMember(Name = nameof(ConfigurationId))]
        public string ConfigurationId { get; set; }


    }
}
