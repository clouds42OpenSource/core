﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLinkPasswordDto
    {
        [XmlElement(ElementName = nameof(LinkUserPassword))]
        [DataMember(Name = nameof(LinkUserPassword))]
        public string LinkUserPassword { get; set; }

        public AccountDatabaseLinkPasswordDto()
        {
        }

        public AccountDatabaseLinkPasswordDto(string linkUserPassword)
        {
            LinkUserPassword = linkUserPassword;
        }
    }
}
