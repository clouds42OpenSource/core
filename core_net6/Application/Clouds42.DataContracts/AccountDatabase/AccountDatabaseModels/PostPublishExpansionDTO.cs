﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostPublishExpansionDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseId))]
        [DataMember(Name = nameof(AccountDatabaseId))]
        public Guid AccountDatabaseId { get; set; }

        [XmlElement(ElementName = nameof(ExpansionName))]
        [DataMember(Name = nameof(ExpansionName))]
        public string ExpansionName { get; set; }

        public PostPublishExpansionDto()
        {
        }

        public bool IsValid
        {
            get { return AccountDatabaseId != Guid.Empty && !string.IsNullOrEmpty(ExpansionName); }
        }
    }
}
