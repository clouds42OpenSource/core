﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseFilePathDto
    {
        [XmlElement(ElementName = nameof(DatabaseFilePath))]
        [DataMember(Name = nameof(DatabaseFilePath))]
        public string DatabaseFilePath { get; set; }

        public AccountDatabaseFilePathDto()
        {
        }

        public AccountDatabaseFilePathDto(string databaseFilePath)
        {
            DatabaseFilePath = databaseFilePath;
        }
    }
}