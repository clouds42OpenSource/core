﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLaunchTypeDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(LaunchType))]
        [DataMember(Name = nameof(LaunchType))]
        public int LaunchType { get; set; }

        [XmlElement(ElementName = nameof(AccountUserId))]
        [DataMember(Name = nameof(AccountUserId))]
        public Guid AccountUserId { get; set; }

        public bool IsValid
        {
            get { return LaunchType is >= 0 and <= 6; }
        }
    }
}
