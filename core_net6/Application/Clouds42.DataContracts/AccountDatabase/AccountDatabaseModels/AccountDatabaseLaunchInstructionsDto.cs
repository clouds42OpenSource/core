﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Инструкция по запуску инф. базы
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLaunchInstructionsDto: IAccountDatabaseLaunchInstructionsDto
    {
		/// <summary>
		/// Запуск разрешен.
		/// </summary>
        [XmlElement(ElementName = nameof(LaunchAllowed))]
        [DataMember(Name = nameof(LaunchAllowed))]
        public bool LaunchAllowed { get; set; }

        /// <summary>
        /// Описание ошибок валидации.
        /// </summary>
        [XmlElement(ElementName = nameof(ValidationErrorMessage))]
        [DataMember(Name = nameof(ValidationErrorMessage))]
        public string ValidationErrorMessage { get; set; }

        /// <summary>
        /// Полный путь до клиента 1С на сервере запуска.
        /// </summary>
        [XmlElement(ElementName = nameof(Path1CClient))]
        [DataMember(Name = nameof(Path1CClient))]
        public string Path1CClient { get; set; }

        /// <summary>
        /// Полный путь до клиента 1С x64 на сервере запуска.
        /// </summary>
        [XmlElement(ElementName = nameof(Path1CClientX64))]
        [DataMember(Name = nameof(Path1CClientX64))]
        public string Path1CClientX64 { get; set; }

        /// <summary>
        /// Версия платформы под которой должна запускаться база.
        /// </summary>
        [XmlElement(ElementName = nameof(PlatformVersion))]
        [DataMember(Name = nameof(PlatformVersion))]
        public string PlatformVersion { get; set; }
    }
}