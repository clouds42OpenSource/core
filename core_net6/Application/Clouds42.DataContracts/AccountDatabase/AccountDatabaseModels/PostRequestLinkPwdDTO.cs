﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLinkPwdDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(LinkPassword))]
        [DataMember(Name = nameof(LinkPassword))]
        public string LinkPassword { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseID != Guid.Empty; }
        }
    }
}
