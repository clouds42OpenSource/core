﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class TestingAndRepairingInfoDto
    {
        [XmlElement(ElementName = nameof(AccountDatabasID))]
        [DataMember(Name = nameof(AccountDatabasID))]
        public Guid AccountDatabasID { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabasName))]
        [DataMember(Name = nameof(AccountDatabasName))]
        public string AccountDatabasName { get; set; }

        [XmlElement(ElementName = nameof(LogPath))]
        [DataMember(Name = nameof(LogPath))]
        public string LogPath { get; set; }

        [XmlElement(ElementName = nameof(State))]
        [DataMember(Name = nameof(State))]
        public SupportHistoryState State { get; set; }

        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }

        [XmlElement(ElementName = nameof(EditDate))]
        [DataMember(Name = nameof(EditDate))]
        public DateTime EditDate { get; set; }

    }
}