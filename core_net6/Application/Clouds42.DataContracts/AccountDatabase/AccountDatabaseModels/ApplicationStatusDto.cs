﻿using Newtonsoft.Json;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель ответа получаемая при опросе статуса
    /// созданной базы на разделителях
    /// </summary>
    public class ApplicationStatusDto
    {
        /// <summary>
        /// Статус ИБ
        /// </summary>
        [JsonProperty("state")]
        public string DatabaseState { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [JsonProperty("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Номер зоны базы на разделителях
        /// </summary>
        [JsonProperty("zone")]
        public int? Zone { get; set; }

        /// <summary>
        /// Индификатор базы источника
        /// </summary>
        [JsonProperty("database-id")]
        public Guid? DatabaseSourceId { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        [JsonProperty("url")]
        public string URL { get; set; }

    }
}
