﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель списка доступов пользователей к инф. базе
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    // ReSharper disable once InconsistentNaming
    public class AccountDatabaseUserAccessListDto
    {
        /// <summary>
        /// Список доступов пользователей к инф. базе
        /// </summary>
        [XmlElement(ElementName = "UserAccess")]
        [DataMember(Name = "UserAccess")]
        public List<AccountDatabaseUserAccessDto> AccessList { get; set; } = [];
    }
}
