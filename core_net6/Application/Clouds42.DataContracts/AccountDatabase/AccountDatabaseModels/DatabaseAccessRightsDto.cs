﻿
namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    public class DatabaseAccessRightsDto
    {

        public string ObjectAction { get; set; }
        public bool HasAccess { get; set; }
    }
}
