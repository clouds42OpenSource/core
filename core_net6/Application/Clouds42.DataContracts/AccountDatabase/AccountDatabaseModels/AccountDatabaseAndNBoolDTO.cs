﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDatabaseAndNBoolDto
    {
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public bool? Value { get; set; }
    }
}