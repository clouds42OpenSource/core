﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель для запроса удаление ролей у пользователей МЦОБ 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class McobRemoveUserRolesDto
	{
		/// <summary>
		/// Название Информационной базы 
		/// </summary>
        [XmlElement(ElementName = nameof(DataBaseName))]
        [DataMember(Name = nameof(DataBaseName))]
        public string DataBaseName { get; set; }

		/// <summary>
		/// Имя пользователя
		/// </summary>
        [XmlElement(ElementName = nameof(UserLogin))]
        [DataMember(Name = nameof(UserLogin))]
        public string UserLogin { get; set; }

		/// <summary>
		/// роли пользователей
		/// </summary>
        [XmlElement(ElementName = nameof(Roles))]
        [DataMember(Name = nameof(Roles))]
        public string Roles { get; set; }

		/// <summary>
		/// 0 - default, 1 - sauri invoke
		/// </summary>
        [XmlElement(ElementName = nameof(InvokeClient))]
        [DataMember(Name = nameof(InvokeClient))]
        public InvokeClientType InvokeClient { get; set; }
	}
}
