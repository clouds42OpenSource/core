﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PostCreateAccountDatabaseViaDelimiterDto
    {
        [XmlElement(ElementName = nameof(AccountUserId))]
        [DataMember(Name = nameof(AccountUserId))]
        public Guid AccountUserId { get; set; }

        [XmlElement(ElementName = nameof(ConfigurationCode))]
        [DataMember(Name = nameof(ConfigurationCode))]
        public string ConfigurationCode { get; set; }
	}

}
