﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesIsAccountFrozenDto
    {
        [XmlElement(ElementName = nameof(IsAccountFrozen))]
        [DataMember(Name = nameof(IsAccountFrozen))]
        public bool IsAccountFrozen { get; set; }

        public AccountDatabasesIsAccountFrozenDto()
        {
        }

        public AccountDatabasesIsAccountFrozenDto(bool isAccountFrozen)
        {
            IsAccountFrozen = isAccountFrozen;
        }
    }
}
