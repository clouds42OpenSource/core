﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDatabaseSetActiveLinkDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public bool Value { get; set; }

        public AccountDatabaseSetActiveLinkDto()
        { 
        
        }
    }
}
