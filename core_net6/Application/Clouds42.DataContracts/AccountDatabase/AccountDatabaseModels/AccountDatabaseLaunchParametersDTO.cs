﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AccountDatabaseLaunchParametersDto
    {
        [XmlElement(ElementName = nameof(LaunchParameter))]
        [DataMember(Name = nameof(LaunchParameter))]
        public string LaunchParameter { get; set; }

        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        public AccountDatabaseLaunchParametersDto()
        {
        }

        public AccountDatabaseLaunchParametersDto(string LaunchParameter, Guid AccountDatabaseID)
        {
            this.LaunchParameter = LaunchParameter;
            this.AccountDatabaseID = AccountDatabaseID;
        }

    }
}