﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.AccountDatabase;
using CommonLib.Enums;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель доступа пользователя к инф. базе
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseUserAccessDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        [XmlElement(ElementName = nameof(AccountUserId))]
        [DataMember(Name = nameof(AccountUserId))]
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Тип лицензии Аренды 1С
        /// </summary>
        [XmlElement(ElementName = nameof(Rent1CLicense))]
        [DataMember(Name = nameof(Rent1CLicense))]
        public ResourceType? Rent1CLicense { get; set; }

        /// <summary>
        /// Тип доступа
        /// </summary>
        [XmlElement(ElementName = nameof(AccessType))]
        [DataMember(Name = nameof(AccessType))]
        public AccountDatabaseAccessType AccessType { get; set; }
    }
}
