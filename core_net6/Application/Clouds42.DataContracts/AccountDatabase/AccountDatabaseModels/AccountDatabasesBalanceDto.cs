﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesBalanceDto
    {
        [XmlElement(ElementName = nameof(AccountBallanse))]
        [DataMember(Name = nameof(AccountBallanse))]
        public double AccountBallanse { get; set; }

        public AccountDatabasesBalanceDto()
        {
        }

        public AccountDatabasesBalanceDto(double ballanse)
        {
            AccountBallanse = ballanse;
        }
    }
}
