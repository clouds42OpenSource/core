﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestAccDbCaptionDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseID != Guid.Empty && !string.IsNullOrEmpty(Caption); }
        }
    }
}
