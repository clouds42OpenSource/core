﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesIsDemoDto
    {
        [XmlElement(ElementName = nameof(IsDemo))]
        [DataMember(Name = nameof(IsDemo))]
        public bool IsDemo { get; set; }

        public AccountDatabasesIsDemoDto()
        {
        }

        public AccountDatabasesIsDemoDto(bool isDemo)
        {
            IsDemo = isDemo;
        }
    }
}

