﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель для отправки данных о действии и версии Линка
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class LinkLaunchActionInfoDto
    {
        /// <summary>
        /// Тип запуска базы 1С
        /// </summary>
        [XmlElement(ElementName = nameof(LaunchType))]
        [DataMember(Name = nameof(LaunchType))]
        public LaunchType LaunchType { get; set; }

        /// <summary>
        /// Версия платформы на которой была запущена база
        /// </summary>
        [XmlElement(ElementName = nameof(PlatformVersion1C))]
        [DataMember(Name = nameof(PlatformVersion1C))]
        public string PlatformVersion1C { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Логин кто выполнел действие 
        /// </summary>
        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { get; set; }

        /// <summary>
        /// Версия Линка
        /// </summary>
        [XmlElement(ElementName = nameof(LinkAppVersion))]
        [DataMember(Name = nameof(LinkAppVersion))]
        public string LinkAppVersion { get; set; }

        /// <summary>
        /// Тип Линка
        /// </summary>
        [XmlElement(ElementName = nameof(LinkAppType))]
        [DataMember(Name = nameof(LinkAppType))]
        public AppModes LinkAppType { get; set; }

        /// <summary>
        /// Выполненное действие
        /// </summary>
        [XmlElement(ElementName = nameof(Action))]
        [DataMember(Name = nameof(Action))]
        public LinkActionType Action { get; set; }

        /// <summary>
        /// Внутренний IP адрес
        /// </summary>
        [XmlElement(ElementName = nameof(InternalIpAddress))]
        [DataMember(Name = nameof(InternalIpAddress))]
        public string InternalIpAddress { get; set; }

        [DataMember(Name = nameof(ExternalIpAddress))]
        public string ExternalIpAddress { get; set; }
    }
}
