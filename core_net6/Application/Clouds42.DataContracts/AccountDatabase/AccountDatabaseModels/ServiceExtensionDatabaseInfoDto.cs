﻿using Clouds42.DataContracts.BaseModel;
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Список сервисов с информацией о установки в базу
    /// </summary>
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class ServiceExtensionDatabaseInfoDto
    {
        /// <summary>
        /// Список сервисов с информацией о установки в базу
        /// </summary>
        [XmlElement(ElementName = nameof(Services))]
        [DataMember(Name = nameof(Services))]
        public GenericListItemDto<ServiceExtensionDatabaseInfoItemDto> Services { get; set; } = new();
    }


    /// <summary>
    /// Элемент сервиса с информацией о установки в базу
    /// </summary>
    public class ServiceExtensionDatabaseInfoItemDto
    {
        /// <summary>
        /// Id записи
        /// </summary>
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public int Id { get; set; }

        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceId))]
        [DataMember(Name = nameof(ServiceId))]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Статус расширения сервиса для информационной базы
        /// </summary>
        [XmlElement(ElementName = "Status")]
        [DataMember(Name = "Status")]
        public ServiceExtensionDatabaseStatusEnum ServiceExtensionDatabaseStatus { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        [XmlElement(ElementName = nameof(StateDate))]
        [DataMember(Name = nameof(StateDate))]
        public DateTime StateDate { get; set; }

        /// <summary>
        /// Дата создания записи
        /// </summary>
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public DateTime CreationDate { get; set; }

        [XmlElement(ElementName = nameof(Error))]
        [DataMember(Name = nameof(Error))]
        public string Error { get; set; }
    }
}
