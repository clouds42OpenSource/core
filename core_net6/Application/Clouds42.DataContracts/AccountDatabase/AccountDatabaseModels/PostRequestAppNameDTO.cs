﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class PostRequestSetDistributiveTypeDto
    {
        [XmlElement] [DataMember] public Guid AccountDatabaseID { get; set; }
        [XmlElement] [DataMember] public string DistributiveType { get; set; }
    }
}