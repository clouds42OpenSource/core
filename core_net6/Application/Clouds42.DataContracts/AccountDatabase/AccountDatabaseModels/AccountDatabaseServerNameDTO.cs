﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseServerNameDto
    {
        [XmlElement(ElementName = nameof(ServerName))]
        [DataMember(Name = nameof(ServerName))]
        public string ServerName { get; set; }

        public AccountDatabaseServerNameDto()
        {
        }

        public AccountDatabaseServerNameDto(string serverName)
        {
            ServerName = serverName;
        }
    }
}