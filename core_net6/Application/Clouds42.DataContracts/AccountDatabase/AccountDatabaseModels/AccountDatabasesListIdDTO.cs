﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Список id акаунтов
    /// </summary>
    public class AccountDatabasesListIdDTO
    {
        public Guid[] AccountDatabasesIDs { get; set; }

    }
}
