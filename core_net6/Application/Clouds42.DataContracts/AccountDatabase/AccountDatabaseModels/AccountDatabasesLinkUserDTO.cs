﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLinkUserDto
    {
        [XmlElement(ElementName = nameof(LinkUserName))]
        [DataMember(Name = nameof(LinkUserName))]
        public string LinkUserName { get; set; }

        public AccountDatabaseLinkUserDto()
        {
        }

        public AccountDatabaseLinkUserDto(string linkUserName)
        {
            LinkUserName = linkUserName;
        }
    }
}