﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель описывает данные о разделителе инфо базы.
    /// </summary>
    [XmlRoot(ElementName = "Delimiter")]
    [DataContract(Name = "Delimiter")]
    [Serializable]
    public class AccountDatabaseDelimiterDto
    {
        /// <summary>
        /// Номер области.
        /// </summary>
        [XmlElement(ElementName = nameof(Zone))]
        [DataMember(Name = nameof(Zone))]
        public int? Zone { get; set; }        

        /// <summary>
        /// Имя базы источника на кластере 1С.
        /// </summary>
        [XmlElement(ElementName = nameof(SourceDatabaseName))]
        [DataMember(Name = nameof(SourceDatabaseName))]
        public string SourceDatabaseName { get; set; }

        /// <summary>
        /// Демо область.
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemo))]
        [DataMember(Name = nameof(IsDemo))]
        public bool IsDemo { get; set; }

        /// <summary>
        /// Версия платформы конфигурации.
        /// </summary>
        [XmlElement(ElementName = nameof(PlatformVersion))]
        [DataMember(Name = nameof(PlatformVersion))]
        public string PlatformVersion { get; set; }

        /// <summary>
        /// База на разделителях должна использовать вэб сервис
        /// </summary>
        [XmlElement(ElementName = nameof(DelimiterDatabaseMustUseWebService))]
        [DataMember(Name = nameof(DelimiterDatabaseMustUseWebService))]
        public bool DelimiterDatabaseMustUseWebService { get; set; }
    }
}