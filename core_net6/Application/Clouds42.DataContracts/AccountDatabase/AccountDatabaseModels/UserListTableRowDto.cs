﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    public class UserListTableRowDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "LocalUserID")]
        [DataMember(Name = "LocalUserID")]
        public Guid LocalUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(LocalUserCS))]
        [DataMember(Name = nameof(LocalUserCS))]
        public string LocalUserCS { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserListTableRowDto()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="performanceCounterValue"></param>
        /// <param name="connString"></param>
        public UserListTableRowDto(Guid performanceCounterValue, string connString)
        {
            LocalUserId = performanceCounterValue;
            LocalUserCS = connString;
        }
    }

    public class UserListTable
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string type = "table"; 
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<UserListTableRowDto> Rows { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserListTable()
        {
            Rows = [];
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class UserListTableDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(UserListTable))]
        [DataMember(Name = nameof(UserListTable))]
        public UserListTable UserListTable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserListTableDto()
        {
            UserListTable = new UserListTable();
        }
    }
}
