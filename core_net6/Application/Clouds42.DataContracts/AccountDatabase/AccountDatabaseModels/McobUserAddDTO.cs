﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class McobUserAddDto
    {
        [XmlElement(ElementName = "dataBaseName")]
        [DataMember(Name = "dataBaseName")]
        public string DataBaseName { get; set; }

        [XmlElement(ElementName = "userLogin")]
        [DataMember(Name = "userLogin")]
        public string UserLogin { get; set; }

        [XmlElement(ElementName = "userName")]
        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        [XmlElement(ElementName = "roles")]
        [DataMember(Name = "roles")]
        public string Roles { get; set; }

        [XmlElement(ElementName = "JHORoles")]
        [DataMember(Name = "JHORoles")]
        public string JhoRoles { get; set; }
    }
}
