﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class TestingAndRepairingListDto
    {
        public AccountDatabaseTestingAndRepairingListDto Table { get; set; }

        public TestingAndRepairingListDto()
        {
            Table = new AccountDatabaseTestingAndRepairingListDto();
        }
        
        public TestingAndRepairingListDto(List<TestingAndRepairingInfoDto> list)
        {
            Table = new AccountDatabaseTestingAndRepairingListDto();
            list.ForEach(x => Table.testingAndRepairingList.Add(x));
        }
    }
}