﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    public class AccountDatabasesTaskDiagnosticInfoDto
    {
        [XmlElement(ElementName = nameof(TaskID))]
        [DataMember(Name = nameof(TaskID))]
        public Guid TaskID { get; set; }

        [XmlElement(ElementName = nameof(AcceptedToDb))]
        [DataMember(Name = nameof(AcceptedToDb))]
        public DateTime? AcceptedToDb { get; set; }

        [XmlElement(ElementName = nameof(AcceptedToSvc))]
        [DataMember(Name = nameof(AcceptedToSvc))]
        public DateTime? AcceptedToSvc { get; set; }

        [XmlElement(ElementName = nameof(ExecutedInSvc))]
        [DataMember(Name = nameof(ExecutedInSvc))]
        public DateTime? ExecutedInSvc { get; set; }

        [XmlElement(ElementName = nameof(ExecutedInDb))]
        [DataMember(Name = nameof(ExecutedInDb))]
        public DateTime? ExecutedInDb { get; set; }

        [XmlElement(ElementName = nameof(Executed))]
        [DataMember(Name = nameof(Executed))]
        public bool Executed { get; set; }

    }
}
