﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasePublishPathDto
    {
        [XmlElement(ElementName = nameof(WebPublishPath))]
        [DataMember(Name = nameof(WebPublishPath))]
        public string WebPublishPath { get; set; }

        public AccountDatabasePublishPathDto()
        {
        }

        public AccountDatabasePublishPathDto(string webPublishPath)
        {
            WebPublishPath = webPublishPath;
        }
    }
}
