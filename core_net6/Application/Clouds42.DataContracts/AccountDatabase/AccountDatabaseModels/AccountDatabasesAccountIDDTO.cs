﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseAccountIDDto
    {
        [XmlElement(ElementName = "AccountID")]
        [DataMember(Name = "AccountID")]
        public Guid AccountDatabaseCaption { get; set; }

        public AccountDatabaseAccountIDDto()
        {
        }

        public AccountDatabaseAccountIDDto(Guid accountDatabaseCaption)
        {
            AccountDatabaseCaption = accountDatabaseCaption;
        }
    }
}