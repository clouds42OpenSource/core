﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DelimitersDbsPropertiesDto
    {
        [XmlElement(ElementName = nameof(Id))]
        [DataMember(Name = nameof(Id))]
        public Guid Id { get; set; }

        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }

        [XmlElement(ElementName = nameof(DbName))]
        [DataMember(Name = nameof(DbName))]
        public string DbName { get; set; }

        [XmlElement(ElementName = nameof(WebLink))]
        [DataMember(Name = nameof(WebLink))]
        public string WebLink { get; set; }

        [XmlElement(ElementName = nameof(Zone))]
        [DataMember(Name = nameof(Zone))]
        public int? Zone { get; set; }

        [XmlElement(ElementName = nameof(SourceDatabaseId))]
        [DataMember(Name = nameof(SourceDatabaseId))]
        public Guid SourceDatabaseId { get; set; }
    }
}
