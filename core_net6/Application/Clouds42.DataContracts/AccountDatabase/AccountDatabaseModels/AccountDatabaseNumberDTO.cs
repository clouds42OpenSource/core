﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseNumberDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabaseNumber))]
        [DataMember(Name = nameof(AccountDatabaseNumber))]
        public int AccountDatabaseNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AccountDatabaseNumberDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountDatabaseNumber"></param>
        public AccountDatabaseNumberDto(int accountDatabaseNumber)
        {
            AccountDatabaseNumber = accountDatabaseNumber;
        }
    }
}