﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseCreationDateDto
    {
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public DateTime CreationDate { get; set; }

        public AccountDatabaseCreationDateDto()
        {
        }

        public AccountDatabaseCreationDateDto(DateTime creationDate)
        {
            CreationDate = creationDate;
        }
    }
}