﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabasesHasAccountDatabaseDto
    {
        [XmlElement(ElementName = "HasAccountDatabaseResource")]
        [DataMember(Name = "HasAccountDatabaseResource")]
        public bool IsAccountFrozen { get; set; }

        public AccountDatabasesHasAccountDatabaseDto()
        {
        }

        public AccountDatabasesHasAccountDatabaseDto(bool isAccountFrozen)
        {
            IsAccountFrozen = isAccountFrozen;
        }
    }
}

