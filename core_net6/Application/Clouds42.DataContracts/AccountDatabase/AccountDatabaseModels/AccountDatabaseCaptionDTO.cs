﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseCaptionDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseCaption))]
        [DataMember(Name = nameof(AccountDatabaseCaption))]
        public string AccountDatabaseCaption { get; set; }

        public AccountDatabaseCaptionDto()
        {
        }

        public AccountDatabaseCaptionDto(string accountDatabaseCaption)
        {
            AccountDatabaseCaption = accountDatabaseCaption;
        }
    }
}