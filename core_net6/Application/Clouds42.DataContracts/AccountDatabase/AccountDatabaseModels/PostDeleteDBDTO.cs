﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostDeleteDBDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(V82name))]
        [DataMember(Name = nameof(V82name))]
        public string V82name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "IsFILE")]
        [DataMember(Name = "IsFILE")]
        public bool IsFile { get; set; }
         /// <summary>
        /// 
        /// </summary>
         [XmlElement(ElementName = nameof(FilePath))]
         [DataMember(Name = nameof(FilePath))]
         public string FilePath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(DeleteSqlBase))]
        [DataMember(Name = nameof(DeleteSqlBase))]
        public bool DeleteSqlBase { get; set; }
         /// <summary>
        /// 
        /// </summary>
         [XmlElement(ElementName = nameof(Platform))]
         [DataMember(Name = nameof(Platform))]
         public PlatformType Platform { get; set; }

        public bool IsValid => AccountDatabaseID != Guid.Empty && !string.IsNullOrEmpty(V82name) && !string.IsNullOrEmpty(FilePath);
    }
}

