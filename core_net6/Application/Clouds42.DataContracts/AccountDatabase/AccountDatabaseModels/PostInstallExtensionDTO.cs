﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostInstallExtensionDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { set; get; }

        [XmlElement(ElementName = nameof(AccountUsersId))]
        [DataMember(Name = nameof(AccountUsersId))]
        public Guid AccountUsersId { set; get; }

        [XmlElement(ElementName = nameof(Login))]
        [DataMember(Name = nameof(Login))]
        public string Login { set; get; }

        [XmlElement(ElementName = nameof(Pass))]
        [DataMember(Name = nameof(Pass))]
        public string Pass { set; get; }

        [XmlElement(ElementName = nameof(ExtensionName))]
        [DataMember(Name = nameof(ExtensionName))]
        public string ExtensionName { set; get; }
    
        public PostInstallExtensionDto()
        {

        }

        public bool IsValid()
        {
             return AccountDatabaseID != Guid.Empty &&
                         AccountUsersId    != Guid.Empty &&
                         !string.IsNullOrEmpty(Login) && 
                         !string.IsNullOrEmpty(ExtensionName);

        }
    }
}
