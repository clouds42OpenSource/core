﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class AccountDatabaseViaDelimiterDto
    {
        /// <summary>
        /// Статус ИБ
        /// </summary>
        [XmlElement(ElementName = "State")]
        [DataMember(Name = "State")]
        public string DatabaseState { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [XmlElement(ElementName = nameof(Comment))]
        [DataMember(Name = nameof(Comment))]
        public string Comment { get; set; }

        /// <summary>
        /// Номер зоны базы на разделителях
        /// </summary>
        [XmlElement(ElementName = nameof(Zone))]
        [DataMember(Name = nameof(Zone))]
        public int? Zone { get; set; }

        /// <summary>
        /// Индификатор базы источника
        /// </summary>
        [XmlElement(ElementName = "DatabaseID")]
        [DataMember(Name = "DatabaseID")]
        public Guid? DatabaseSourceId { get; set; }

        /// <summary>
        /// Адрес подключения
        /// </summary>
        [XmlElement(ElementName = nameof(URL))]
        [DataMember(Name = nameof(URL))]
        public string URL { get; set; }

        /// <summary>
        /// Индификатор базы на разделителях
        /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// параметр перезапуска процесса
        /// </summary>
        [XmlIgnore]
        public int? RetryDelayInSeconds { get; set; }
    }
}
