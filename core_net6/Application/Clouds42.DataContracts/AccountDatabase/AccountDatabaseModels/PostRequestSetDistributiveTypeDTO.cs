﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestAppNameDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(ApplicationName))]
        [DataMember(Name = nameof(ApplicationName))]
        public string ApplicationName { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseID != Guid.Empty && !string.IsNullOrEmpty(ApplicationName); }
        }
    }
}
