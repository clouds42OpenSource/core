﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    /// <summary>
    /// Модель для отправки данных о регистрации выполненного действия на Линке
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestSetActivityDateDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        /// <summary>
        /// Данные о действии и версии Линка
        /// </summary>
        [XmlElement(ElementName = nameof(LinkLaunchActionInfo))]
        [DataMember(Name = nameof(LinkLaunchActionInfo))]
        public LinkLaunchActionInfoDto LinkLaunchActionInfo { get; set; }
    }
}
