﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    public class AccountDatabaseTestingAndRepairingListDto
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = "Type")]
        [IgnoreDataMember]
        public string type
        {
            get { return "Type"; }
        }

        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<TestingAndRepairingInfoDto> testingAndRepairingList { get; set; } = [];
    }
}