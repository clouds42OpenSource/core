﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseLastActivityDateDto
    {
        [XmlElement(ElementName = nameof(LastActivityDate))]
        [DataMember(Name = nameof(LastActivityDate))]
        public DateTime LastActivityDate { get; set; }

        public AccountDatabaseLastActivityDateDto()
        {
        }

        public AccountDatabaseLastActivityDateDto(DateTime lastActivityDate)
        {
            LastActivityDate = lastActivityDate;
        }
    }
}