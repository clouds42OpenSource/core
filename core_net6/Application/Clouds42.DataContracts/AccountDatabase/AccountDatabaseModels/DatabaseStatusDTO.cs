﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class DatabaseStatusDto
    {
        [XmlElement(ElementName = nameof(Status))]
        [DataMember(Name = nameof(Status))]
        public string Status { get; set; }

        [XmlElement(ElementName = nameof(Description))]
        [DataMember(Name = nameof(Description))]
        public string Description { get; set; }
    }
}
