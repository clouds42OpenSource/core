﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.Link42;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    public class LocalAccountDatabasesDto
    {
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }

        [XmlElement(ElementName = nameof(Platform))]
        [DataMember(Name = nameof(Platform))]
        public PlatformType Platform { get; set; }

        [XmlElement(ElementName = nameof(FilePath))]
        [DataMember(Name = nameof(FilePath))]
        public string FilePath { get; set; }

        [XmlElement(ElementName = nameof(ServerName))]
        [DataMember(Name = nameof(ServerName))]
        public string ServerName { get; set; }

        [XmlElement(ElementName = nameof(SqlName))]
        [DataMember(Name = nameof(SqlName))]
        public string SqlName { get; set; }

        [XmlElement(ElementName = nameof(IsFile))]
        [DataMember(Name = nameof(IsFile))]
        public bool IsFile { get; set; }

        [XmlElement(ElementName = nameof(Launch))]
        [DataMember(Name = nameof(Launch))]
        public LaunchType Launch { get; set; }
    }

    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class LocalAccountDatabasesListDto(List<LocalAccountDatabasesDto> list)
    {
        [XmlElement(ElementName = nameof(List))]
        [DataMember(Name = nameof(List))]
        public List<LocalAccountDatabasesDto> List { get; set; } = list;

        public LocalAccountDatabasesListDto() : this([])
        {
        }
    }
}
