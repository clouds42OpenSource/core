﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    public class RolesJhoDto
    {
        [XmlElement(ElementName = nameof(RoleJHO))]
        [DataMember(Name = nameof(RoleJHO))]
        public string RoleJHO { get; set; }
    }

    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class RolesJhoListResultDto(List<RolesJhoDto> list)
    {
        [XmlElement(ElementName = nameof(List))]
        [DataMember(Name = nameof(List))]
        public List<RolesJhoDto> List { get; set; } = list;

        public RolesJhoListResultDto() : this([])
        {
        }
    }

    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class RolesJhoListRequesttDto
    {
        [XmlElement(ElementName = nameof(List))]
        [DataMember(Name = nameof(List))]
        public List<RolesJhoDto> List { get; set; }

        public RolesJhoListRequesttDto()
        {
        }

        public RolesJhoListRequesttDto(List<RolesJhoDto> list)
        {
            List = list;
        }
    }
}
