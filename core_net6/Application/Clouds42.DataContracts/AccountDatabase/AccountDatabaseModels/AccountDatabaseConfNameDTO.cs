﻿using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AccountDatabaseConfNameDto
    {
        [XmlElement(ElementName = nameof(ConfigurationName))]
        [DataMember(Name = nameof(ConfigurationName))]
        public string ConfigurationName { get; set; }

        public AccountDatabaseConfNameDto()
        {
        }

        public AccountDatabaseConfNameDto(string configurationName)
        {
            ConfigurationName = configurationName;
        }

        public static AccountDatabaseConfNameDto GetFromXml(XElement xml)
        {
            if (xml == null) return null;
            var node = xml.Descendants().FirstOrDefault();
            if (node == null) return null;
            var result = new AccountDatabaseConfNameDto {ConfigurationName = node.Value};
            return result;
        }
    }
}