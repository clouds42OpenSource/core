﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestLinkLastConnectionErrorDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(LinkLastConnectionError))]
        [DataMember(Name = nameof(LinkLastConnectionError))]
        public string LinkLastConnectionError { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseID != Guid.Empty && !string.IsNullOrEmpty(LinkLastConnectionError); }
        }
    }
}
