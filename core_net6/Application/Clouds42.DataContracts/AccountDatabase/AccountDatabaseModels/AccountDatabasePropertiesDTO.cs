﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    [Serializable]
    public class AccountDatabasePropertiesDto: IAccountDatabasePropertiesDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountID))]
        [DataMember(Name = nameof(AccountID))]
        public Guid AccountID { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [XmlElement(ElementName = nameof(CreationDate))]
        [DataMember(Name = nameof(CreationDate))]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
        [XmlElement(ElementName = nameof(LastActivityDate))]
        [DataMember(Name = nameof(LastActivityDate))]
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Название базы пользовательское
        /// </summary>
        [XmlElement(ElementName = nameof(Caption))]
        [DataMember(Name = nameof(Caption))]
        public string Caption { get; set; }

        /// <summary>
        /// Конфигурация
        /// </summary>
        [XmlElement(ElementName = nameof(ConfigurationName))]
        [DataMember(Name = nameof(ConfigurationName))]
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Версия конфигурация
        /// </summary>
        [XmlElement(ElementName = nameof(ConfigurationVersion))]
        [DataMember(Name = nameof(ConfigurationVersion))]
        public string ConfigurationVersion { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        [XmlElement(ElementName = nameof(ApplicationName))]
        [DataMember(Name = nameof(ApplicationName))]
        public string ApplicationName { get; set; }

        /// <summary>
        /// Стабильная или альфа версия
        /// </summary>
        [XmlElement(ElementName = nameof(DistributionType))]
        [DataMember(Name = nameof(DistributionType))]
        public string DistributionType { get; set; }

        /// <summary>
        /// Версия стабильной платформы конфигурации.
        /// </summary>
        [XmlElement(ElementName = nameof(PlatformVersionStable))]
        [DataMember(Name = nameof(PlatformVersionStable))]
        public string PlatformVersionStable { get; set; }


        /// <summary>
        /// Версия альфа платформы конфигурации.
        /// </summary>
        [XmlElement(ElementName = nameof(PlatformVersionAlpha))]
        [DataMember(Name = nameof(PlatformVersionAlpha))]
        public string PlatformVersionAlpha { get; set; }

        /// <summary>
        /// Домен
        /// </summary>
        [XmlElement(ElementName = nameof(AccountDomainID))]
        [DataMember(Name = nameof(AccountDomainID))]
        public Guid? AccountDomainID { get; set; }

        /// <summary>
        /// Путь на веб публикацию
        /// </summary>
        [XmlElement(ElementName = nameof(WebPublishPath))]
        [DataMember(Name = nameof(WebPublishPath))]
        public string WebPublishPath { get; set; }

        /// <summary>
        /// Является ли база опубликованной
        /// </summary>
        [XmlElement(ElementName = nameof(IsPublished))]
        [DataMember(Name = nameof(IsPublished))]
        public bool IsPublished { get; set; }

        /// <summary>
        /// Статус публикации
        /// </summary>
        [XmlElement(ElementName = nameof(PublishState))]
        [DataMember(Name = nameof(PublishState))]
        public PublishState PublishState { get; set; }


        /// <summary>
        /// Название базы
        /// </summary>
        [XmlElement(ElementName = nameof(DbName))]
        [DataMember(Name = nameof(DbName))]
        public string DbName { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        [XmlElement(ElementName = nameof(DbNumber))]
        [DataMember(Name = nameof(DbNumber))]
        public int DbNumber { get; set; }

        /// <summary>
        /// Состояние базы
        /// </summary>
        [XmlElement(ElementName = nameof(State))]
        [DataMember(Name = nameof(State))]
        public string State { get; set; }

        /// <summary>
        /// Название SQL базы
        /// </summary>
        [XmlElement(ElementName = nameof(SqlName))]
        [DataMember(Name = nameof(SqlName))]
        public string SqlName { get; set; }

        /// <summary>
        /// Название сервера
        /// </summary>
        [XmlElement(ElementName = nameof(ServerName))]
        [DataMember(Name = nameof(ServerName))]
        public string ServerName { get; set; }

        /// <summary>
        /// Состояние блокировки базы
        /// </summary>
        [XmlElement(ElementName = nameof(LockedState))]
        [DataMember(Name = nameof(LockedState))]
        public string LockedState { get; set; }

        /// <summary>
        /// Является ли база файловой
        /// </summary>
        [XmlElement(ElementName = nameof(IsFile))]
        [DataMember(Name = nameof(IsFile))]
        public bool IsFile { get; set; }

        /// <summary>
        /// Путь к файловой базе
        /// </summary>
        [XmlElement(ElementName = nameof(FilePath))]
        [DataMember(Name = nameof(FilePath))]
        public string FilePath { get; set; }

        /// <summary>
        /// Дата бекапа
        /// </summary>
        [XmlElement(ElementName = nameof(BackupDate))]
        [DataMember(Name = nameof(BackupDate))]
        public DateTime? BackupDate { get; set; }

        /// <summary>
        /// Путь к архиву
        /// </summary>
        [XmlElement(ElementName = nameof(ArchivePath))]
        [DataMember(Name = nameof(ArchivePath))]
        public string ArchivePath { get; set; }

        /// <summary>
        /// Размер базы в мегабайтах
        /// </summary>
        [XmlElement(ElementName = "SizeInMB")]
        [DataMember(Name = "SizeInMB")]
        public int SizeInMb { get; set; }

        /// <summary>
        /// ID к шаблону
        /// </summary>
        [XmlElement(ElementName = nameof(TemplateId))]
        [DataMember(Name = nameof(TemplateId))]
        public Guid? TemplateId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        [XmlElement(ElementName = nameof(ServiceName))]
        [DataMember(Name = nameof(ServiceName))]
        public string ServiceName { get; set; }

        [XmlElement(ElementName = nameof(LinkUser))]
        [DataMember(Name = nameof(LinkUser))]
        public string LinkUser { get; set; }

        /// <summary>
        /// ID базы
        /// </summary>
        [XmlElement(ElementName = nameof(ID))]
        [DataMember(Name = nameof(ID))]
        public Guid ID { get; set; }

        /// <summary>
        /// Тип запуска
        /// </summary>
        [XmlElement(ElementName = nameof(LaunchType))]
        [DataMember(Name = nameof(LaunchType))]
        public int LaunchType { get; set; }

        [XmlElement(ElementName = nameof(RolesJHO))]
        [DataMember(Name = nameof(RolesJHO))]
        public string RolesJHO { get; set; }

        /// <summary>
        /// Ссылка на сервис
        /// </summary>
        [XmlElement(ElementName = nameof(LinkServicePath))]
        [DataMember(Name = nameof(LinkServicePath))]
        public string LinkServicePath { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountCaption))]
        [DataMember(Name = nameof(AccountCaption))]
        public string AccountCaption { get; set; }


        /// <summary>
        /// Истёк ли демо период
        /// </summary>
        [XmlElement(ElementName = nameof(IsDemoExpired))]
        [DataMember(Name = nameof(IsDemoExpired))]
        public bool IsDemoExpired { get; set; }

        /// <summary>
        /// Заблокирован ли доступ
        /// </summary>
        [XmlElement(ElementName = nameof(IsLock))]
        [DataMember(Name = nameof(IsLock))]
        public bool IsLock { get; set; }

        /// <summary>
        /// Параметры запуска 1С
        /// </summary>
        [XmlElement(ElementName = nameof(LaunchParameters))]
        [DataMember(Name = nameof(LaunchParameters))]
        public string LaunchParameters { get; set; }

        /// <summary>
        /// Данные о разделителе инфо базы.
        /// </summary>
        [XmlElement(ElementName = "Delimiter")]
        [DataMember(Name = "Delimiter")]
        public AccountDatabaseDelimiterDto DelimiterInfo { get; set; }

        /// <summary>
        /// Под какой разрядностью будет запускаться 1С
        /// </summary>
        [XmlElement(ElementName = nameof(Application1CBitDepth))]
        [DataMember(Name = nameof(Application1CBitDepth))]
        public ApplicationBitDepthEnum Application1CBitDepth { get; set; }

    }
}
