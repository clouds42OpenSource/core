﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class PostRequestConfVersionDto
    {
        [XmlElement(ElementName = nameof(AccountDatabaseID))]
        [DataMember(Name = nameof(AccountDatabaseID))]
        public Guid AccountDatabaseID { get; set; }

        [XmlElement(ElementName = nameof(ConfigurationVersion))]
        [DataMember(Name = nameof(ConfigurationVersion))]
        public string ConfigurationVersion { get; set; }

        public bool IsValid
        {
            get { return AccountDatabaseID != default && !string.IsNullOrEmpty(ConfigurationVersion); }
        }
    }
}