﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabase1CDocument
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class AcDbDocumentIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "AcDbDocumentID")]
        [DataMember(Name = "AcDbDocumentID")]
        public Guid AcDbDocumentId { get; set; }

        public AcDbDocumentIdDto()
        {
        }


        public AcDbDocumentIdDto(Guid acDbDocumentId)
        {
            AcDbDocumentId = acDbDocumentId;
        }
    }
}