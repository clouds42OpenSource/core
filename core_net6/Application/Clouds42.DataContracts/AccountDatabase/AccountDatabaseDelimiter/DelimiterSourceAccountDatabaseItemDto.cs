﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Модель материнской базы разделителей
    /// </summary>
    public class DelimiterSourceAccountDatabaseItemDto
    {
        /// <summary>
        ///  Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Код конфигурации базы на разделителях
        /// </summary>
        public string DbTemplateDelimiterCode { get; set; }

        /// <summary>
        /// Адрес публикации базы на разделителях
        /// </summary>
        public string DatabaseOnDelimitersPublicationAddress { get; set; }

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }
    }
}
