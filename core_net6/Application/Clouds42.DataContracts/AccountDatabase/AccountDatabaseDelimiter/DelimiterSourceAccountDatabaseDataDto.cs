﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Данные материнской базы разделителей
    /// </summary>
    public class DelimiterSourceAccountDatabaseDataDto : DelimiterSourceAccountDatabaseDto
    {
        /// <summary>
        /// Имя информационной базы
        /// </summary>
        public string AccountDatabaseName { get; set; }
    }
}
