﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Информация об информационной базе для выпадающего списка.
    /// </summary>
    public class AccountDatabaseInfoDto
    {
        /// <summary>
        /// Описание инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }

    }
}
