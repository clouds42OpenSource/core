﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter.Filters
{
    /// <summary>
    /// Модель фильтра записей материнских баз разделителей
    /// </summary>
    public sealed class DelimiterSourceAccountDatabaseFilterDto
    {
        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchLine { get; set; }
    }
}