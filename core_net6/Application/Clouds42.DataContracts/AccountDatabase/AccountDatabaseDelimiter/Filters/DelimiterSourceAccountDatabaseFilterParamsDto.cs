﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter.Filters
{
    /// <summary>
    /// Модель фильтра записей материнских баз разделителей
    /// </summary>
    public sealed class DelimiterSourceAccountDatabaseFilterParamsDto
    {
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Фильтр получения материнских баз разделителей
        /// </summary>
        public DelimiterSourceAccountDatabaseFilterDto Filter { get; set; }

        /// <summary>
        /// Информация о сортировке выбранного списка материнских баз разделителей
        /// </summary>
        public SortingDataDto SortingData { get; set; }

    }
}