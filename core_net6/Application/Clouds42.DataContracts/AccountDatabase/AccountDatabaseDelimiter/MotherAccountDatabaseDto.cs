﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Модель для создания материнской базы разделителей
    /// </summary>
    public class MotherAccountDatabaseDto
    {
        /// <summary>
        ///  Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Код конфигурации базы на разделителях
        /// </summary>
        public string DbTemplateDelimiterCode { get; set; }

        /// <summary>
        /// Имя материнской базы
        /// </summary>
        public string DatabaseCaption { get; set; }

        /// <summary>
        /// Адрес публикации базы на разделителях
        /// </summary>
        public string DatabaseOnDelimitersPublicationAddress { get; set; }
    }
}
