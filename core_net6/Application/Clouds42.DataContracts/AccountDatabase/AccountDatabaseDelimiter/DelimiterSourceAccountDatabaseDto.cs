﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Модель материнской базы разделителей
    /// </summary>
    public class DelimiterSourceAccountDatabaseDto
    {
        /// <summary>
        ///  Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Код конфигурации базы на разделителях
        /// </summary>
        public string DbTemplateDelimiterCode { get; set; }

        /// <summary>
        /// Адрес публикации базы на разделителях
        /// </summary>
        public string DatabaseOnDelimitersPublicationAddress { get; set; }
    }
}
