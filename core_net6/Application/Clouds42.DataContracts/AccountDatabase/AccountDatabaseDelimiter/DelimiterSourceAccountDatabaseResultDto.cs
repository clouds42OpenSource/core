﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseDelimiter
{
    /// <summary>
    /// Записи материнских баз разделителей
    /// </summary>
    public sealed class DelimiterSourceAccountDatabaseResultDto
    {
        /// <summary>
        /// Список записей материнских баз разделителей
        /// </summary>
       public DelimiterSourceAccountDatabaseItemDto[] Records { get; set; }

        /// <summary>
        /// Пагинация списка <see cref="Records"/> материнских баз разделителей
        /// </summary>
        public PaginationBaseDto Pagination { get; set; }
    }
}