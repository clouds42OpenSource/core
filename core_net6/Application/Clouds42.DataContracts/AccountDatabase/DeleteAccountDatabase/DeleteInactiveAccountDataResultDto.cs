﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase
{
    /// <summary>
    /// Результат удаления данных неактивного аккаунта
    /// </summary>
    public class DeleteInactiveAccountDataResultDto
    {
        /// <summary>
        /// Результат удаления
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Путь к папке аккаунта
        /// </summary>
        public string AccountFolderPath { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
