﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase
{
    /// <summary>
    /// Модель параметров удаления инф. базы
    /// </summary>
    public class DeleteAccountDatabaseParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID инициатора операции
        /// </summary>
        public Guid InitiatorId { get; set; }
    }
}
