﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase
{
    /// <summary>
    /// Параметры удаления данных неактивного аккаунта
    /// </summary>
    public class DeleteInactiveAccountDataParamsDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Путь к хранилищу файлов аккаунта
        /// </summary>
        public string AccountFileStoragePath { get; set; }

        /// <summary>
        /// Путь к архивам файлов аккаунта
        /// </summary>
        public string AccountArchiveDirectoryPath { get; set; }
    }
}
