﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase.DbSupport
{
    public class DbSupportFilterDto
    {
        public int? RowChart { get; set; }

        public int? ColumnChart { get; set; }

        public string DateStringChart { get; set; }

        public DateTime StartRange { get; set; } = DateTime.Now.AddDays(-7);

        public DateTime EndRange { get; set; } = DateTime.Now;

        public int? DbOperation { get; set; }

        public string Initiator { get; set; }

        public bool? ValidateState { get; set; }
    }
}