﻿using System;
using Clouds42.Common.Constants;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase.DbSupport
{
    public class DbSupportChartItemDto
    {
        public string DateString => Date.ToString(Globalization.ShortDateFormat);

        public DateTime Date { get; set; }

        /// <summary>
        /// ТиИ (план)
        /// </summary>
        public int TehSupportPlanCount { get; set; }

        /// <summary>
        /// ТиИ (факт)
        /// </summary>
        public int TehSupportFactCount { get; set; }

        /// <summary>
        /// Автообновление (план)
        /// </summary>
        public int AutoUpdatePlanCount { get; set; }

        /// <summary>
        /// Автообновление (факт)
        /// </summary>
        public int AutoUpdateFactCount { get; set; }

        /// <summary>
        /// Ошибки при ТиИ и АО
        /// </summary>
        public int ErrorsCount { get; set; }

        /// <summary>
        /// Ошибки подключения ТиИ и АО
        /// </summary>
        public int ErrorsConnectCount { get; set; }

    }
}
