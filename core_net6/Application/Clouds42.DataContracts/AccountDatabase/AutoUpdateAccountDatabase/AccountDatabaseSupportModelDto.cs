﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using System.Collections.Generic;
using System;
using System.Linq;
using System.IO;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель инфомационной базы с данными по тех потдержки.
    /// </summary>
    public class AccountDatabaseSupportModelDto : IAccountDatabaseEnterpriseDto, IUpdateDatabaseDto
    {
        /// <summary>
        /// Данные по потдержке информационной базы
        /// </summary>
        public IAcDbSupport Support { get; set; }

        /// <summary>
        /// Информационная база.
        /// </summary>
        public Domain.DataModels.AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Данные о конфигурации 1С.
        /// </summary>
        public IConfigurations1C ConfigurationInfo { get; set; }

        /// <summary>
        /// Данные по шаблону.
        /// (Опционально) есть только если база сзданна из шаблона.
        /// </summary>
        public IDbTemplate Template { get; set; }

        /// <summary>
        /// Данные о сегмента кластера.
        /// (Опционально) только для серверных баз.
        /// </summary>
        public CloudServicesEnterpriseServer EnterpriseServer { get; set; }

        /// <summary>
        /// Данные о SQL сервере.        
        /// (Опционально) только для серверных баз.
        /// </summary>
        public CloudServicesSqlServer SqlServer { get; set; }

        /// <summary>
        /// Сегмент в рамках которого запускается информационная база.
        /// </summary>
        public Domain.DataModels.CloudServicesSegment Segment { get; set; }

        /// <summary>
        /// Полный путь до файловой информационной базы.
        /// (Опционально - только для файловых баз.)
        /// </summary>
        public string AccountDatabasePath { get; set; }

        /// <summary>
        /// Назание базы.
        /// </summary>
        public string UpdateDatabaseName => AccountDatabase.V82Name;        

        /// <summary>
        /// База является файловой.
        /// </summary>
        public bool IsFile => AccountDatabase.IsFile ?? false;

        /// <summary>
        /// Логин администратор.
        /// </summary>
        public string AdminLogin => Support.Login;

        /// <summary>
        /// Пароль администратора.
        /// </summary>
        public string AdminPassword => Support.Password;

        /// <summary>
        /// Адрес базы.
        /// </summary>
        public string ConnectionAddress => IsFile ? AccountDatabasePath : EnterpriseServer?.ConnectionAddress;

        /// <summary>
        /// Тип платформы.
        /// </summary>
        public PlatformType PlatformType => AccountDatabase.PlatformType;

        /// <summary>
        /// Версия платформы под которой нужно выполнить операцию поддержки.
        /// </summary>
        public string PlatformVersion
        {
            get
            {
                if (!IsFile)
                    return AccountDatabase.PlatformType == PlatformType.V83
                        ? Segment.Stable83PlatformVersionReference.Version
                        : Segment.Stable82PlatformVersionReference.Version;

                if (AccountDatabase.DistributionType == DistributionType.Alpha.ToString())
                    return Segment.Alpha83PlatformVersionReference.Version;

                return AccountDatabase.PlatformType == PlatformType.V83
                        ? Segment.Stable83PlatformVersionReference.Version
                        : Segment.Stable82PlatformVersionReference.Version;
            }
        }

        /// <summary>
        /// Список платформ 1С установленных на облаке.
        /// </summary>
        public List<IPlatformVersionReference> CloudPlatforms { get; set; }

        /// <summary>
        /// Путь до папки где лежит платформа 1С.
        /// </summary>
        public string FolderPath1C
        {
            get
            {
                var platform = CloudPlatforms.FirstOrDefault(v =>
                    v.Version == PlatformVersion);

                if (platform == null)
                    throw new InvalidOperationException(
                        $"В справочнике 'Платформа 1С' не заполнены сведения по версии {PlatformVersion}");

                if (!Directory.Exists(platform.PathToPlatform))
                    throw new InvalidOperationException($"На сервере не установлено платформы версии {platform.Version}");

                return platform.PathToPlatform;
            }
        }


        /// <summary>
        /// Путь к платформе 1С x64
        /// </summary>
        public string PlatformPathX64
        {
            get
            {
                if (PlatformType == PlatformType.V82)
                {
                    if (string.IsNullOrEmpty(Segment.Stable82PlatformVersionReference.PathToPlatfromX64))
                        throw new InvalidOperationException(
                            $"У сегмента '{Segment.Name}' :: '{Segment.Description}' не задан путь до 1С 8.2 х64");

                    return Segment.Stable82PlatformVersionReference.PathToPlatfromX64;
                }

                if (PlatformType != PlatformType.V83)
                    throw new InvalidOperationException(
                        $"Для типа платформы {PlatformType} не определено откуда брать путь до 1С.");

                if (AccountDatabase.DistributionType == DistributionType.Alpha.ToString())
                {
                    if (string.IsNullOrEmpty(Segment.Alpha83PlatformVersionReference.PathToPlatfromX64))
                        throw new InvalidOperationException($"У сегмента '{Segment.Name}':: '{Segment.Description}' не задан путь до альфа 1С 8.3 х64");

                    return Segment.Alpha83PlatformVersionReference.PathToPlatfromX64;
                }

                if (string.IsNullOrEmpty(Segment.Stable83PlatformVersionReference.PathToPlatfromX64))
                    throw new InvalidOperationException($"У сегмента '{Segment.Name}':: '{Segment.Description}' не задан путь до стабильной 1С 8.3 х64");

                return Segment.Stable83PlatformVersionReference.PathToPlatfromX64;
            }
        }

        public string UploadedFileName { get; set; }
    }

}
