﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Параметры автообновления инф. базы
    /// </summary>
    public class AccountDatabaseAutoUpdateParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
    }
}
