﻿namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель информации информационной базы,
    /// для которой АО завершилось с успешно 
    /// </summary>
    public class AutoUpdateDatabaseSuccessesDto
    {
        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// Версия информационной базы после обновления
        /// </summary>
        public string NewVersion { get; set; }
    }
}
