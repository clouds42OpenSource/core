﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Детали авто обновления конфигурации 1С
    /// </summary>
    public class AutoUpdateConfiguration1CDetailsDto
    {
        /// <summary>
        /// Текущая версия релиза
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Актуальная версия релиза
        /// </summary>
        public string ActualVersion { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Список обновлений конфигурации с порядком обновления
        /// </summary>
        public List<UpdateConfigurationDataDto> UpdateConfigurationData { get; set; } = [];

        /// <summary>
        /// Количество попыток для обновления
        /// </summary>
        public int CountStepsForUpdate => UpdateConfigurationData.Count;
    }
}
