﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель данных технического результата АО инф. базы
    /// </summary>
    public class DatabaseAutoUpdateTechnicalResultDataDto
    {
        /// <summary>
        /// Время начала АО
        /// </summary>
        public DateTime? AutoUpdateStartDate { get; set; }

        /// <summary>
        /// Время завершения АО
        /// </summary>
        public DateTime? AutoUpdateEndDate { get; set; }

        /// <summary>
        /// Информация для отладки
        /// </summary>
        public string DebugInformation { get; set; }

        public Guid AccountDatabaseId { get; set; }
        public Guid CoreWorkerTasksQueueId { get; set; }
    }
}
