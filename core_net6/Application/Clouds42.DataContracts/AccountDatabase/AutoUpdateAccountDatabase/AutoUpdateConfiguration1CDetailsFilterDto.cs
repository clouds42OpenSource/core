﻿using JetBrains.Annotations;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель фильтра для получения деталей
    /// авто обновления конфигурации 1С
    /// </summary>
    public class AutoUpdateConfiguration1CDetailsFilterDto : IQueryFilter
    {
        /// <summary>
        /// Текущая версия релиза
        /// </summary>
        [CanBeNull]
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Актуальная версия релиза
        /// </summary>
        [CanBeNull]
        public string ActualVersion { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        [CanBeNull]
        public string ConfigurationName { get; set; }
    }
}
