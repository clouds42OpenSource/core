﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Результат поддержки инф. базы
    /// </summary>
    public class AccountDatabaseSupportResultDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabasesId { get; set; }

        /// <summary>
        /// Состояние процесса поддержки
        /// </summary>
        public AccountDatabaseSupportProcessorState SupportProcessorState { get; set; }

        /// <summary>
        /// Сообщение.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Детальная информация.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string Caption { get; set; }
    }
}
