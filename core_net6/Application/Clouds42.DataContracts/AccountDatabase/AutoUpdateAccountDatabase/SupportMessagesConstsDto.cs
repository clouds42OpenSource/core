﻿namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Констансты для сообщений поддержки
    /// </summary>
    public static class SupportMessagesConstsDto
    {
        /// <summary>
        /// Не верные авторизационные данные
        /// </summary>
        public const string InvalidAuthorizationData = "Не верные авторизационные данные";

        /// <summary>
        /// Описание для не верных авторизационных данных
        /// </summary>
        public const string InvalidLoginPasswordDescription =
            "Убедитесь, что:\n" +
            "1. Введены корректные регистрационные данные\n" +
            "2. Пользователь 1С является администратором в 1С\n";

        /// <summary>
        /// Полное сообщение о не верных авторизационных данных
        /// </summary>
        public const string InvalidLoginPasswordFull = InvalidAuthorizationData + " \n " + InvalidLoginPasswordDescription;
    }
}
