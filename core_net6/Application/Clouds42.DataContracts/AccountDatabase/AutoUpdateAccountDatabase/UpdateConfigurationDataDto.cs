﻿namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Данные обновления конфигурации
    /// </summary>
    public class UpdateConfigurationDataDto
    {
        /// <summary>
        /// Номер обновления
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Текущая версия
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Актуальная версия
        /// </summary>
        public string ActualVersion { get; set; }
    }
}
