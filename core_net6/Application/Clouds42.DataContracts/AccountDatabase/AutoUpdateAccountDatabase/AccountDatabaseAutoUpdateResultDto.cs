﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Результат автообновления инф. базы
    /// </summary>
    public class AccountDatabaseAutoUpdateResultDto : AccountDatabaseSupportResultDto
    {
        /// <summary>
        /// Версия информационной базы до обновления.
        /// </summary>
        public string OldVersion { get; set; }

        /// <summary>
        /// Версия информационной базы после обновления.
        /// </summary>
        public string NewVersion { get; set; }

        /// <summary>
        /// Информация для отладки
        /// </summary>
        public string DebugInformation { get; set; }

        /// <summary>
        /// ID задачи из очереди
        /// </summary>
        public Guid CoreWorkerTaskQueueId { get; set; }

        /// <summary>
        /// Получить информацию для отладки
        /// </summary>
        /// <returns>Информация для отладки</returns>
        public string GetDebugInformation()
            => $@"
                Инф. база: [{AccountDatabasesId}]-{Caption}. 
                Результат: {SupportProcessorState}. 
                Описание: {Message}; {Description}. 
                Старая версия: {OldVersion}. 
                Новая версия: {NewVersion}.
                Информация для отладки: {DebugInformation}";
        
    }
}
