﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель информации о технических работах с базой
    /// </summary>
    public class DatabaseSupportInfoDto
    {
        /// <summary>
        /// Именование базы.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Текущая версия конфигурации.
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Целевая версия конфигурации.
        /// </summary>
        public string UpdateVersion { get; set; }

        /// <summary>
        /// Время проведения операции
        /// </summary>
        public AutoUpdateTimeEnum TimeOfOperation { get; set; }
    }
}
