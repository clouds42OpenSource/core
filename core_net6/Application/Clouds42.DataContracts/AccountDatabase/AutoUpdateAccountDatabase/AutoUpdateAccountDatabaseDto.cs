﻿using System;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель данных базы которая в очереди на АО
    /// </summary>
    public class AutoUpdateAccountDatabaseDto
    {
        /// <summary>
        /// Дата добавления базы в очередь на АО
        /// </summary>
        public DateTime AddedDate { get; set; }

        /// <summary>
        /// В процессе авто обновления
        /// </summary>
        public bool IsProcessingAutoUpdate { get; set; }

        /// <summary>
        /// Id базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Текущая версия релиза
        /// </summary>
        public string CurrentVersion { get; set; }

        /// <summary>
        /// Актуальная версия релиза
        /// </summary>
        public string ActualVersion { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string PlatformVersion { get; set; }

        /// <summary>
        /// Дата подключения
        /// </summary>
        public DateTime? ConnectDate { get; set; }

        /// <summary>
        /// Дата последнего успешного АО
        /// </summary>
        public DateTime? LastSuccessAuDate { get; set; }

        public bool? IsVipAccountsOnly { get; set; }

        public bool? IsActiveRent1COnly { get; set; }

        public Guid AccountId { get; set; }

        /// <summary>
        /// Id задачи которая выполняла обновление
        /// </summary>
        public Guid CoreWorkerTasksQueueId { get; set; }

        /// <summary>
        /// Номер воркера который выполнял АО
        /// </summary>
        public short CapturedWorkerId { get; set; }

        /// <summary>
        /// Статус АО
        /// </summary>
        public SupportHistoryState Status { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Время начала АО
        /// </summary>
        public DateTime AutoUpdateStartDate { get; set; }

        public string Name { get; set; }
        public DateTime AddDate { get; set; }

        public int CurrentVersionMajor { get; set; }
        public int CurrentVersionMinor { get; set; }
        public int CurrentVersionPatch { get; set; }
        public int CurrentVersionBuild { get; set; }

        public int ActualVersionMajor { get; set; }
        public int ActualVersionMinor { get; set; }
        public int ActualVersionPatch { get; set; }
        public int ActualVersionBuild { get; set; }
    }
}
