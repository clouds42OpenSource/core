﻿namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель информации информационной базы,
    /// для которой ТиИ завершилось с ошибкой 
    /// </summary>
    public class TehSupportDatabaseFailureDto
    {
        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Описание ошибки
        /// </summary>
        public string Description { get; set; }
    }
}
