﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    public class AccountDatabaseSupportSettingDto
    {
        public Guid DatabaseId { get; set; }
        public bool IsConnects { get; set; }
        public bool IsError { get; set; }
        public SupportState SupportState { get; set; }
        public string SupportStateDescription { get; set; }
        public DateTime? LastHistoryDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<SupportDatabasLogDc> SupportLogs { get; set; }
    }

    public class SupportDatabasLogDc
    {
        public string Date { get; set; }
        public string Operation { get; set; }
        public string Description { get; set; }
    }
}
