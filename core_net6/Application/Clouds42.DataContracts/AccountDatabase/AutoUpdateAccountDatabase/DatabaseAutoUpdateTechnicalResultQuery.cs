﻿using System;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase
{
    /// <summary>
    /// Модель фильтра технического результата АО инф. базы
    /// </summary>
    public class DatabaseAutoUpdateTechnicalResultQuery : IQueryFilter, ISortedQuery
    {
        public string OrderBy { get; set; }

        public DatabaseAutoUpdateTechnicalResultQueryFilter Filter { get; set; }
    }

    public class DatabaseAutoUpdateTechnicalResultQueryFilter : IQueryFilter
    {
        /// <summary>
        /// Id базы
        /// </summary>
        public Guid? AccountDatabaseId { get; set; }

        /// <summary>
        /// Id задачи из очереди
        /// </summary>
        public Guid? CoreWorkerTasksQueueId { get; set; }
    }
}
