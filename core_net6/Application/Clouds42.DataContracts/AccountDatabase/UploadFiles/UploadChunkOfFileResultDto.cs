﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Результат загрузки части файла
    /// </summary>
    [XmlRoot(ElementName = nameof(Result))]
    [DataContract(Name = nameof(Result))]
    public class UploadChunkOfFileResultDto
    {
        /// <summary>
        /// Результат загрузки
        /// </summary>
        [XmlElement(ElementName = nameof(Result))]
        [DataMember(Name = nameof(Result))]
        public bool Result { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [XmlElement(ElementName = nameof(ErrorMessage))]
        [DataMember(Name = nameof(ErrorMessage))]
        public string ErrorMessage { get; set; }
    }
}
