﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Конфигурация инф. базы в дампе инф. базы
    /// </summary>
    [XmlRoot(ElementName = "Configuration", Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
    public class DataDumpConfigurationDto
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        [XmlElement(ElementName = nameof(Name), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public string Name { get; set; }
        
        /// <summary>
        /// Версия конфигурации
        /// </summary>
        [XmlElement(ElementName = nameof(Version), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public string Version { get; set; }
        
        /// <summary>
        /// Представление названия конфигурации
        /// </summary>
        [XmlElement(ElementName = nameof(Presentation), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public string Presentation { get; set; }
        
        /// <summary>
        /// Издатель
        /// </summary>
        [XmlElement(ElementName = nameof(Vendor), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public string Vendor { get; set; }
    }
}
