﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Объект метаданных дампа инф. базы
    /// </summary>
    [XmlRoot(ElementName = "XDTODataObject", Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
    public class DumpInfoMetadataDto
    {
        /// <summary>
        /// Модель данных дампа инф. базы
        /// </summary>
        [XmlElement(ElementName = nameof(DumpInfo), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public DataDumpInfoDto DumpInfo { get; set; }
        
        /// <summary>
        /// Пространство имен
        /// </summary>
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        
        /// <summary>
        /// Ядро предприятия 1С
        /// </summary>
        [XmlAttribute(AttributeName = "v8", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string V8 { get; set; }
        
        /// <summary>
        /// Схема данных
        /// </summary>
        [XmlAttribute(AttributeName = "xs", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xs { get; set; }
    }
}
