﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Информация из ZIP файла инф. базы
    /// </summary>
    public class AccountDatabaseInfoDto
    {
        /// <summary>
        /// ID конфигурации
        /// </summary>
        public string ConfigurationId { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }
        
        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Список пользователей
        /// </summary>
        public List<UserAccountDatabaseZipDto> Users { get; set; } = [];
    }
}