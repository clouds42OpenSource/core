﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Модель запроса инициализации загрузки файла
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class InitUploadFileRequestDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        [XmlElement(ElementName = nameof(FileName))]
        [DataMember(Name = nameof(FileName))]
        public string FileName { get; set; }
    }
}
