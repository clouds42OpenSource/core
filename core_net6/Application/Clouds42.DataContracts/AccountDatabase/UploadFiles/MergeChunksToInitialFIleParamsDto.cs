﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Параметры склейки частей в исходный файл
    /// </summary>
    public class MergeChunksToInitialFIleParamsDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Количество частей файла
        /// </summary>
        public int CountOfFileChunks { get; set; }

        /// <summary>
        /// Путь для частей файла
        /// </summary>
        public string PathForFileChunks { get; set; }
    }
}
