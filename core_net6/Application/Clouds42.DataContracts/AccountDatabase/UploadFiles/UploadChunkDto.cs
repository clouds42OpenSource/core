﻿using System;
using Microsoft.AspNetCore.Http;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    public class UploadChunkDto
    {
        public Guid FileId { get; set; }
        public IFormFile ChunkFile { get; set; }
    }
}
