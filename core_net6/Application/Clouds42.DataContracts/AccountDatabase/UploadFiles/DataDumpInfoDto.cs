﻿using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Модель данных дампа инф. базы
    /// </summary>
    [XmlRoot(ElementName = "DumpInfo", Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
    public class DataDumpInfoDto
    {
        /// <summary>
        /// Дата создания строкой
        /// </summary>
        [XmlElement(ElementName = nameof(Created), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public string Created { get; set; }
        
        /// <summary>
        /// Конфигурация инф. базы
        /// </summary>
        [XmlElement(ElementName = nameof(Configuration), Namespace = "http://www.1c.ru/1cFresh/Data/Dump/1.0.2.1")]
        public DataDumpConfigurationDto Configuration { get; set; }
        
        /// <summary>
        /// Префикс пространства имен
        /// </summary>
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }
}
