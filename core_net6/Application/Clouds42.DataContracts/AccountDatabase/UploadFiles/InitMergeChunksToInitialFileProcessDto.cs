﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    public class InitMergeChunksToInitialFileProcessDto
    {
        /// <summary>
        /// Айди загружаемого файла
        /// </summary>
        public Guid UploadFileId { get; set; }
        
        /// <summary>
        /// Количество чанков
        /// </summary>
        public int CountOfChunks { get; set; }
    }
}
