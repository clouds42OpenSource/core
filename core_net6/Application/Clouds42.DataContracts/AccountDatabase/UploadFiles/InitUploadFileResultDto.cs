﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{
    /// <summary>
    /// Модель ответа на запрос инициализации загрузки файла
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class InitUploadFileResultDto
    {
        /// <summary>
        /// ID загружаемого файла
        /// </summary>
        [XmlElement(ElementName = nameof(UploadedFileId))]
        [DataMember(Name = nameof(UploadedFileId))]
        public Guid UploadedFileId { get; set; }
    }
}
