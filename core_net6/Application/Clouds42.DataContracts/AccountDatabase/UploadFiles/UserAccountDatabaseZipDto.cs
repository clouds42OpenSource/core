﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.UploadFiles
{

    /// <summary>
    /// Модель zip базы пользователя
    /// </summary>
    public class UserAccountDatabaseZipDto
    {
        /// <summary>
        /// Полное имя
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// UUID
        /// </summary>
        public Guid? Uid { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid? AccountUserId { get; set; }

        /// <summary>
        /// Пользователь админ
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}