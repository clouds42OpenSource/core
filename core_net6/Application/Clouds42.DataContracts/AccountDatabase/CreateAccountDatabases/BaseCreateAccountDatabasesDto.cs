﻿namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Базовая модель создания инф. баз
    /// </summary>
    public class BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// Признак необходимости использовать ОП
        /// </summary>
        public bool NeedUsePromisePayment { get; set; }
    }
}
