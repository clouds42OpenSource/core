﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель параметров создания инф. базы из zip в МС
    /// </summary>
    public class CreateDatabaseFromZipInMsParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string AccountDatabaseCaption { get; set; }

        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// ID загруженного файла от МС
        /// </summary>
        public Guid? SmUploadedFileId { get; set; }

        /// <summary>
        /// Полный путь к загруженному файлу
        /// </summary>
        public string FullFilePath { get; set; }

        /// <summary>
        /// Список пользователей из zip архива
        /// </summary>
        public List<InfoAboutUserFromZipPackageDto> UserFromZipPackage { get; set; }

        /// <summary>
        /// Список сервисов
        /// </summary>
        [JsonProperty("extensions")]
        public List<ExtensionIds> ExtensionsList { get; set; }


        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string LoginAdmin { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string PasswordAdmin { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid? UserId { get; set; }
    }
}
