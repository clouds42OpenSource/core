﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель загружаемого файла
    /// </summary>
    public class UploadedFileDataDto
    {
        /// <summary>
        /// Отсортированный список частей файла для загрузки, упакованных в ZIP архивы
        /// </summary>
        public List<AccountDatabaseSortedFileDto> SortedFilesDc { get; set; }

        /// <summary>
        /// Длина содержимого файла
        /// </summary>
        public long ContentLength { get; set; }
    }
}
