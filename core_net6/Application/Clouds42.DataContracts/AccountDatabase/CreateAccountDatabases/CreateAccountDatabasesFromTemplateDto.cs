﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. баз из шаблона
    /// </summary>
    public class CreateAccountDatabasesFromTemplateDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// Список моделей создания инф. баз
        /// </summary>
        public List<InfoDatabaseDomainModelDto> CreateDatabasesModelDc { get; set; } = [];
    }
}
