﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания базы на разделителях(без регистрации в МС)
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class CreateDbOnDelimitersWithoutRegistrationInMsDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Код конфигурации разделителей
        /// </summary>
        [XmlElement(ElementName = nameof(ConfigurationId))]
        [DataMember(Name = nameof(ConfigurationId))]
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Список Id пользователей
        /// которым нужно предоставить доступ к созданной базе
        /// </summary>
        [XmlElement(ElementName = "UsersIDList")]
        [DataMember(Name = "UsersIDList")]
        public GuidListItemDto UsersToProvideAccess { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        [XmlElement(ElementName = nameof(ApplicationName))]
        [DataMember(Name = nameof(ApplicationName))]
        public string ApplicationName { get; set; }
    }
}
