﻿using Clouds42.DataContracts.BillingService;
using CommonLib.Enums;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Результат создания инф. базы
    /// </summary>
    public class CreateAccountDatabasesResultDto : CheckAbilityToPayForServiceTypeResultDto
    {
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Ресурс для редиректа
        /// </summary>
        public ResourceType? ResourceForRedirect { get; set; }
    }
}
