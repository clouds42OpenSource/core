﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы на разделителях из бекапа
    /// </summary>
    public class CreateAcDbOnDelimitersFromBackupDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// Модель создания инф. базы
        /// </summary>
        public InfoDatabaseDomainModelDto CreateAcDbModel { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }
    }
}
