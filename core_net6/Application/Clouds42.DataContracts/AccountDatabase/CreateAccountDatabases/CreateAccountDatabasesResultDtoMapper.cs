﻿using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;
using Clouds42.DataContracts.BillingService;
using CommonLib.Enums;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Маппер модели результата создания инф. базы
    /// </summary>
    public static class CreateAccountDatabasesResultDtoMapper
    {
        /// <summary>
        /// Смапить модель результата проверки возможности
        /// оплаты за услугу сервиса к модели результата создания инф. базы
        /// </summary>
        /// <param name="checkAbilityToPayForServiceTypeResult">Модель результата проверки возможности оплаты за услугу сервиса</param>
        /// <returns>Модель результата создания инф. базы</returns>
        public static CreateAccountDatabasesResultDto MapToCreateAccountDatabasesResult(
            this CheckAbilityToPayForServiceTypeResultDto checkAbilityToPayForServiceTypeResult)
            => new()
            {
                Amount = checkAbilityToPayForServiceTypeResult.Amount,
                CanUsePromisePayment = checkAbilityToPayForServiceTypeResult.CanUsePromisePayment,
                Currency = checkAbilityToPayForServiceTypeResult.Currency,
                IsComplete = checkAbilityToPayForServiceTypeResult.IsComplete,
                NotEnoughMoney = checkAbilityToPayForServiceTypeResult.NotEnoughMoney,
                ResourceForRedirect = ResourceType.CountOfDatabasesOverLimit
            };

        /// <summary>
        /// Смапить модель результата проверки возможности
        /// оплаты за услугу сервиса к модели результата покупки
        /// лицензий на размещение инф. баз
        /// </summary>
        /// <param name="checkAbilityToPayForServiceTypeResult">Модель результата проверки возможности
        /// оплаты за услугу сервиса</param>
        /// <returns>Модель результата покупки лицензий на размещение инф. баз</returns>
        public static BuyDatabasesPlacementResultDto MapToBuyDatabasesPlacementResultDc(
            this CheckAbilityToPayForServiceTypeResultDto checkAbilityToPayForServiceTypeResult) =>
            new()
            {
                Amount = checkAbilityToPayForServiceTypeResult.Amount,
                CanUsePromisePayment = checkAbilityToPayForServiceTypeResult.CanUsePromisePayment,
                Currency = checkAbilityToPayForServiceTypeResult.Currency,
                IsComplete = checkAbilityToPayForServiceTypeResult.IsComplete,
                NotEnoughMoney = checkAbilityToPayForServiceTypeResult.NotEnoughMoney
            };
    }
}
