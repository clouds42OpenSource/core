﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы из DT файла
    /// </summary>
    public class CreateAcDbFromDtDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string DbCaption { get; set; }

        /// <summary>
        /// Список пользователей для предоставления доступа
        /// </summary>
        public List<Guid> UsersIdForAddAccess { get; set; }

        /// <summary>
        ///  ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Идентификатор конфигурации
        /// </summary>
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Логин от базы
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль от базы
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Тип загружаемого файла
        /// </summary>
        public UploadedFileEnumDto TypeFile { get; set; }

        /// <summary>
        /// Текущая версия конфигурации
        /// </summary>
        public string CurrentVersion { get; set; }
        
        /// <summary>
        /// Нужно ставить на АО или нет
        /// </summary>
        public bool HasAutoupdate { get; set; }
        /// <summary>
        /// Нужно ставить на поддержку или нет
        /// </summary>
        public bool HasSupport { get; set; }
    }
}
