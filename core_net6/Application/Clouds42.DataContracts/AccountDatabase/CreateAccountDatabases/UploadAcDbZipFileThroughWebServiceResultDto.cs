﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Результат загрузки файла инф. базы через веб сервис
    /// </summary>
    public class UploadAcDbZipFileThroughWebServiceResultDto
    {
        /// <summary>
        /// Признак что операция завершена успешно
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// ID загруженного файла от МС
        /// </summary>
        public Guid? SmUploadedFileId { get; set; }
    }
}
