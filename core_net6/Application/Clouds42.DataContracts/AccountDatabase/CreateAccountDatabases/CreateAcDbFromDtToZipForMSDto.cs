﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы из Dt в Zip файла
    /// </summary>
    public class CreateAcDbFromDtToZipForMSDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        [JsonProperty("file-id")]
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        [JsonProperty("configuration")]
        public string ConfigurationName { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        [JsonProperty("databases-service-id")]
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        [JsonProperty("name")]
        public string DbCaption { get; set; }

        /// <summary>
        /// Логин администратора базы
        /// </summary>
        [JsonProperty("login")]
        public string LoginAdmin { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        [JsonProperty("password")]
        public string PasswordAdmin { get; set; }

        /// <summary>
        /// Список пользователей для предоставления доступа
        /// </summary>
        [JsonProperty("users")]
        public List<InfoAboutUserFromZipPackageDto> UserFromZipPackageList { get; set; }

        /// <summary>
        /// Нужно ставить на АО или нет
        /// </summary>
        public bool HasAutoupdate { get; set; }
        /// <summary>
        /// Нужно ставить на поддержку или нет
        /// </summary>
        public bool HasSupport { get; set; }
    }
}
