﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель регистрации инф. базы по шаблону
    /// при активации сервиса биллинга
    /// </summary>
    public class RegisterDatabaseUponServiceActivationDto
    {
        /// <summary>
        /// Id шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }
    }
}
