﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель данных шаблона информационной базы
    /// </summary>
    public class DatabaseTemplateDataDto
    {
        /// <summary>
        /// Id шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// CSS класс иконки шаблона инф. базы
        /// </summary>
        public string DatabaseTemplateImageCssClass { get; set; }
    }
}
