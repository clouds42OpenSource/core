﻿using Newtonsoft.Json;
using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Информации о пользователе полученного из зип пакета
    /// </summary>
    public class ProfilesUserFromZipPackageDto
    {
        /// <summary>
        /// Индификатор профиля 1С
        /// </summary>
        [JsonProperty("id")]
        public Guid ProfileId { get; set; }

        /// <summary>
        ///Имя профиля
        /// </summary>
        [JsonProperty("name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///Флаг профиля администратора
        /// </summary>
        [JsonProperty("admin")]
        public bool IsAdmin { get; set; }

        /// <summary>
        ///Флаг постовляемого профиля
        /// </summary>
        [JsonProperty("predefined")]
        public bool IsPredefined { get; set; }

    }

}
