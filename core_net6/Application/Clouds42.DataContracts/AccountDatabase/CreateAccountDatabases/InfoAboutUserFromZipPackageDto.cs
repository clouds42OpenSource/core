﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Информации о пользователе полученного из зип пакета
    /// </summary>
    public class InfoAboutUserFromZipPackageDto
    {
        /// <summary>
        /// Индификатор пользователя 1С
        /// </summary>
        [JsonProperty("data-dump-user-id")]
        public Guid? AccountDatabaseZipUserId { get; set; }

        /// <summary>
        /// Индификатор пользователя 1С в каталоге
        /// </summary>
        [JsonProperty("catalog-user-id")]
        public Guid? СatalogZipUserId { get; set; }

        /// <summary>
        ///Индификатор пользователя ЛК
        /// </summary>
        [JsonProperty("account-user-id")]
        public Guid AccountUserId { get; set; }

        /// <summary>
        ///Профиль пользователя в базе 1С
        /// </summary>
        [JsonProperty("profiles")]
        public List<ProfilesUserFromZipPackageDto> Profiles { get; set; } = [];
    }

}
