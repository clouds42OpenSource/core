﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Параметры для загрузки файла инф. базы через веб сервис
    /// </summary>
    public class UploadAcDbZipFileThroughWebServiceParamsDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }
    }
}
