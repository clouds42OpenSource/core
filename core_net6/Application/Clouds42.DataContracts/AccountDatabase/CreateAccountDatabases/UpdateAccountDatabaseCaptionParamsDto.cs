﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    public class UpdateAccountDatabaseCaptionParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string Caption { get; set; }
    }
}
