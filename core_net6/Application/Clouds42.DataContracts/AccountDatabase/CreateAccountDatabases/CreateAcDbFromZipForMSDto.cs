﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы из ZIP файла
    /// </summary>
    public class CreateAcDbFromZipForMSDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        [JsonProperty("file-id")]
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// ID аккаунта
        /// </summary>
        [JsonProperty("account-id")]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        [JsonProperty("configuration")]
        public string ConfigurationName { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        [JsonProperty("databases-service-id")]
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        [JsonProperty("name")]
        public string DbCaption { get; set; }

        /// <summary>
        /// Список пользователей для предоставления доступа
        /// </summary>
        [JsonProperty("users")]
        public List<InfoAboutUserFromZipPackageDto> UserFromZipPackageList { get; set; } = [];

        /// <summary>
        /// Список сервисов
        /// </summary>
        [JsonProperty("extensions")]
        public List<ExtensionIds> ExtensionsList { get; set; } = [];
    }

    public class ExtensionIds
    {
        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        [JsonProperty("id")]
        public Guid? ExtensionId { get; set; }
    }
}
