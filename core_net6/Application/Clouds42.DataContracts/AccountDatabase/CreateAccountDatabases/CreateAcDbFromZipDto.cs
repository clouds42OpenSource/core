﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы из ZIP файла
    /// </summary>
    public class CreateAcDbFromZipDto : BaseCreateAccountDatabasesDto
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid UploadedFileId { get; set; }

        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string DbCaption { get; set; }

        /// <summary>
        /// Список пользователей для предоставления доступа
        /// </summary>
        public List<InfoAboutUserFromZipPackageDto> UserFromZipPackageList { get; set; } = [];
    }
}
