﻿namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель типов загружаемых файлов
    /// </summary>
    public enum UploadedFileEnumDto
    {
        DtFile = 0,
        ZipFile = 1
    }
}
