﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases
{
    /// <summary>
    /// Модель создания инф. базы из DT файла после конвертации
    /// </summary>
    public class CreateAcDbDtDto 
    {
        /// <summary>
        /// ID загруженного файла
        /// </summary>
        public Guid? UploadedFileId { get; set; }

        /// <summary>
        ///  ID аккаунта
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string Description { get; set; }

    }
}
