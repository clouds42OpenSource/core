﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.DataContracts.AccountDatabase.DatabasesPlacement
{
    /// <summary>
    /// Модель результата покупки лицензий на размещение инф. баз
    /// </summary>
    public class BuyDatabasesPlacementResultDto : CheckAbilityToPayForServiceTypeResultDto
    {
        /// <summary>
        /// Список платежей
        /// </summary>
        public List<Guid> PaymentIds { get; set; } = [];
    }
}
