﻿using System;
using CommonLib.Enums;

namespace Clouds42.DataContracts.AccountDatabase.DatabasesPlacement
{
    /// <summary>
    /// Модель для покупки лицензий на размещение инф. баз
    /// </summary>
    public class BuyDatabasesPlacementDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Количество баз для размещения
        /// </summary>
        public int DatabasesForPlacementCount { get; set; }

        /// <summary>
        /// Тип системной услуги(услуги на аккаунт)
        /// </summary>
        public ResourceType SystemServiceType { get; set; }

        /// <summary>
        /// Признак что для оплаты будет использован ОП
        /// </summary>
        public bool IsPromisePayment { get; set; }

        /// <summary>
        /// Дата оплаты
        /// </summary>
        public DateTime? PaidDate { get; set; }
    }
}
