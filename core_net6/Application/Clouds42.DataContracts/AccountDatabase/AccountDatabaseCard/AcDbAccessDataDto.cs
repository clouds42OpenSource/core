﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных доступа к инф. базе
    /// </summary>
    public class AcDbAccessDataDto : UserInfoDataDto
    {
        /// <summary>
        /// Доступ является внешним(к базе чужого аккаунта)
        /// </summary>
        public bool IsExternalAccess { get; set; }

        /// <summary>
        /// Статус доступа
        /// </summary>
        public AccountDatabaseAccessState State { get; set; }

        /// <summary>
        /// Причина задержки выдачи доступа
        /// </summary>
        public string DelayReason { get; set; }
    }
}
