﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель фильтра бэкапов карточки инф. базы
    /// </summary>
    public class DatabaseCardDbBackupsFilterDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Дата создания с
        /// </summary>
        public DateTime? CreationDateFrom { get; set; }

        /// <summary>
        /// Дата создания по
        /// </summary>
        public DateTime? CreationDateTo { get; set; }
    }
}
