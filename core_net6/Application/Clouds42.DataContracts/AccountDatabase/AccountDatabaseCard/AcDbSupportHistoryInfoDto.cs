﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// История технической поддержки инф. базы
    /// </summary>
    public class AcDbSupportHistoryInfoDto
    {
        /// <summary>
        /// Дата проведения ТиИ
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Операция ТиИ
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// Описание ТиИ
        /// </summary>
        public string Description { get; set; }
    }
}
