﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель фильтра доступов информационной к инф. базе
    /// </summary>
    public class DatabaseCardAcDbAccessesFilterDto
    {
        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Строка поиска (логин, эл. почта, фио)
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Тип фильтра пользователей по доступам
        /// </summary>
        public UsersByAccessFilterTypeEnum UsersByAccessFilterType { get; set; }
    }
}
