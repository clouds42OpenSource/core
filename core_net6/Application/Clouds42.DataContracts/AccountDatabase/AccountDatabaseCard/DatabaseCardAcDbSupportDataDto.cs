﻿using System;
using System.Collections.Generic;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных технической поддержки инф. базы
    /// для карточки информационной базы
    /// </summary>
    public class DatabaseCardAcDbSupportDataDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Есть соединение(соединяется) с базой
        /// </summary>
        public bool IsConnects { get; set; }

        /// <summary>
        /// Пользователь не авторизован в базе
        /// </summary>
        public bool IsAuthorized { get; set; }

        /// <summary>
        /// Состояние авторизации
        /// </summary>
        public SupportState SupportState { get; set; }

        /// <summary>
        /// Описание состояния авторизации
        /// </summary>
        public string SupportStateDescription { get; set; }

        /// <summary>
        /// Дата последней технической поддержки
        /// </summary>
        public DateTime? LastTehSupportDate { get; set; }

        /// <summary>
        /// Логин для авторизации пользователя в базу
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль для авторизации пользователя в базу
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// История технической поддержки инф. баз
        /// </summary>
        public List<AcDbSupportHistoryInfoDto> AcDbSupportHistories { get; set; } = [];

        /// <summary>
        /// Время обновления инф базы
        /// </summary>
        public AutoUpdateTimeEnum TimeOfUpdate { get; set; }

        /// <summary>
        /// принудительное завершение инф баз
        /// </summary>
        public bool CompleteSessioin { get; set; }

        /// <summary>
        /// Наличие поддержки
        /// </summary>
        public bool HasSupport { get; set; }

        /// <summary>
        /// Наличие авто обновления
        /// </summary>
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Наличие поддержки информационной базы
        /// </summary>
        public bool HasAcDbSupport { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Признак, что в базе есть доработки
        /// </summary>
        public bool HasModifications { get; set; }
    }
}
