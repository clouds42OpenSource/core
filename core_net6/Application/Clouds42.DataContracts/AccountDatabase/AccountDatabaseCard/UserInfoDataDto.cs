﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель информации о пользователе
    /// </summary>
    public class UserInfoDataDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string UserLogin { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string UserLastName { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserFirstName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string UserMiddleName { get; set; }

        /// <summary>
        /// Элекстронный адрес пользователя
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// Номер аккаунта пользователя
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта пользователя
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        [NotMapped]
        public string UserFullName => $"{UserLastName} {UserFirstName} {UserMiddleName}".Trim();

        /// <summary>
        /// Информация об аккаунте пользователя
        /// </summary>
        [NotMapped]
        public string AccountInfo => $"аккаунт {AccountIndexNumber} “{AccountCaption}”";

        /// <summary>
        /// Есть доступ(имеется запись в базе)
        /// </summary>
        public bool HasAccess { get; set; }

        public bool HasRent { get; set; }

        public Guid BackupId { get; set; }

        public bool IsDeleted { get; set; }

        public Guid AccountId { get; set; }
    }
}
