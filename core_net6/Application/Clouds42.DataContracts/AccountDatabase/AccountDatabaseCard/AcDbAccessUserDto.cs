﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных доступа к инф. базе для пользователя
    /// </summary>
    public class AcDbAccessUserDto 
    {

        /// <summary>
        /// Есть доступ(имеется запись в базе)
        /// </summary>
        public bool HasAccess { get; set; }

        /// <summary>
        /// Статус доступа
        /// </summary>
        public AccountDatabaseAccessState State { get; set; }

        /// <summary>
        /// Id инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// База файловая
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// "My infobases" service identifier
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Template image name
        /// </summary>
        public string TemplateImageName { get; set; }

        /// <summary>
        /// Code for configuring bases on delimiters
        /// </summary>
        public string ConfigurationCode { get; set; }
    }
}
