﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных доступов (к базе)
    /// карточки инф. базы
    /// </summary>
    public class UserCardAcDbAccessesDataDto
    {
        /// <summary>
        /// Доступы к инф. базе
        /// </summary>
        public List<AcDbAccessUserDto> DatabaseAccesses { get; set; } = [];


        /// <summary>
        /// ID услуги "Доступ в серверную базу"
        /// </summary>
        public Guid AccessToServerDatabaseServiceTypeId { get; set; }
        
        /// <summary>
        /// валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid MyDatabaseBillingServiceId { get; set; }

    }
}
