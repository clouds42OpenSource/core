﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Данные внешнего пользователя для предоставления доступа
    /// </summary>
    public class ExternalAccountUserDataForGrandAccessDto : UserInfoDataDto
    {
        /// <summary>
        /// Стоимость доступа
        /// </summary>
        public decimal AccessCost { get; set; }

        /// <summary>
        /// Признак что у пользователя есть лицензия на доступ
        /// </summary>
        public bool HasLicense { get; set; }
    }
}
