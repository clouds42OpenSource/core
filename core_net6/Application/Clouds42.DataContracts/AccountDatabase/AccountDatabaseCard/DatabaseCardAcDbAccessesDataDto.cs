﻿using System.Collections.Generic;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных доступов (к базе)
    /// карточки инф. базы
    /// </summary>
    public class DatabaseCardAcDbAccessesDataDto
    {
        /// <summary>
        /// Доступы к инф. базе
        /// </summary>
        public List<AcDbAccessDataDto> DatabaseAccesses { get; set; } = [];

        /// <summary>
        /// Данные о стоимости тарифов подключения услуг
        /// </summary>
        public MyDatabasesServiceTypeBillingDataDto RateData { get; set; }
        
        /// <summary>
        /// валюта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// стоимость услуги клиент-серверного режима
        /// </summary>
        public decimal ClientServerAccessCost { get; set; }
    }
}
