﻿using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных бэкапов (базы)
    /// карточки инф. базы
    /// </summary>
    public class DatabaseCardDbBackupsDataDto
    {
        /// <summary>
        /// Количество бэкапов
        /// </summary>
        public int DbBackupsCount { get; set; }

        /// <summary>
        /// Бэкапы инф. базы
        /// </summary>
        public List<DatabaseBackupDataDto> DatabaseBackups { get; set; } = [];
    }
}
