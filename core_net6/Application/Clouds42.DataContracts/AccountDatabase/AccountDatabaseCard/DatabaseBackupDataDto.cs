﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель данных бэкапа информационной базы
    /// </summary>
    public class DatabaseBackupDataDto
    {
        /// <summary>
        /// Id бекапа
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Инициатор
        /// </summary>
        public string Initiator { get; set; }

        /// <summary>
        /// Дата создания бекапа
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Путь к бекапу
        /// </summary>
        [JsonIgnore]
        public string BackupPath { get; set; }
        
        
        
        /// <summary>
        /// Путь к бекапу в зависимости от источника для фронта
        /// </summary>
        [JsonPropertyName("BackupPath")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string ConditionalBackupPath => 
            SourceType == AccountDatabaseBackupSourceType.Local ? BackupPath : null;

        /// <summary>
        /// Тригер события
        /// </summary>
        public CreateBackupAccountDatabaseTrigger EventTrigger { get; set; }

        /// <summary>
        /// Описание триггера создания бекапа
        /// </summary>
        [NotMapped]
        public string EventTriggerDescription => EventTrigger.Description();

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Источник хранилища бэкапа
        /// </summary>
        public AccountDatabaseBackupSourceType SourceType { get; set; }
    }
}
