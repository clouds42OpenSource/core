﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Domain.Enums.DataBases;


namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard
{
    /// <summary>
    /// Модель общих/основных данных
    /// карточки информационной базы
    /// </summary>
    public class DatabaseCardGeneralDataDto
    {
        /// <summary>
        /// Id базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public int DbNumber { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public DatabaseState DatabaseState { get; set; }

        /// <summary>
        /// База файловая
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Шаблон базы
        /// </summary>
        public string DbTemplate { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Код конфигурации
        /// </summary>
        public string ConfigurationCode { get; set; }

        /// <summary>
        /// Возвращает <c>true</c>, если текущий аутентифицирующий пользователь
        /// имеет права редактировать информационную базу, иначе <c>false</c>
        /// </summary>
        public bool CanEditDatabase { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        public PlatformType TemplatePlatform { get; set; }

        /// <summary>
        /// Тип распространения
        /// </summary>
        public DistributionType DistributionType { get; set; }

        /// <summary>
        /// Тип платформы
        /// </summary>
        public PlatformType PlatformType { get; set; }

        /// <summary>
        /// Стабильная версия 82
        /// </summary>
        public string Stable82Version { get; set; }

        /// <summary>
        /// Альфа версия 83
        /// </summary>
        public string Alpha83Version { get; set; }

        /// <summary>
        /// Стабильная версия 83
        /// </summary>
        public string Stable83Version { get; set; }

        /// <summary>
        /// Сервер 82
        /// </summary>
        public string V82Server { get; set; }

        /// <summary>
        /// Sql сервер
        /// </summary>
        public string SqlServer { get; set; }

        /// <summary>
        /// Путь к файлу
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Возможность вэб публикации
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Статус публикации
        /// </summary>
        public PublishState PublishState { get; set; }

        /// <summary>
        /// Использование вэб сервиса
        /// </summary>
        public bool UsedWebServices { get; set; }

        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        public Guid? FileStorageId { get; set; }

        /// <summary>
        /// Тип модели восстановления
        /// </summary>
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }

        /// <summary>
        /// Признак что есть возможность сменить модель восстановления
        /// </summary>
        public bool CanChangeRestoreModel { get; set; }

        /// <summary>
        /// Наименование файлового хранилища
        /// </summary>
        public string FileStorageName { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// База на разделителях
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Признак что база демо на разделителях
        /// </summary>
        public bool IsDemoDelimiters { get; set; }

        /// <summary>
        /// Номер области (если база на разделителях)
        /// </summary>
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Признак, что в базе есть доработки
        /// </summary>
        public bool HasModifications { get; set; }

        /// <summary>
        /// Путь подключения к инф. базе
        /// </summary>
        public string DatabaseConnectionPath { get; set; }

        /// <summary>
        /// Доступные хранилища файлов
        /// </summary>
        public List<KeyValueDto<Guid>> AvailableFileStorages { get; set; } = [];

        /// <summary>
        /// Дата создания базы
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Id актуального бэкапа инф. базы
        /// </summary>
        public Guid? ActualDatabaseBackupId { get; set; }

        /// <summary>
        /// Cуществует ли процесс завершения сеансов в базе данных
        /// Запись со статусом "В процессе завершения сеансов"
        /// </summary>
        public bool IsExistTerminatingSessionsProcessInDatabase { get; set; }

        /// <summary>
        /// База в статусе ошибка создания
        /// </summary>
        public bool IsStatusErrorCreated { get; set; }

        /// <summary>
        /// Размер базы в мб
        /// </summary>
        public int SizeInMb { get; set; }

        /// <summary>
        /// Ссылка на облачное хранилище
        /// </summary>
        public string CloudStorageWebLink { get; set; }

        /// <summary>
        /// Путь до публикации
        /// </summary>
        public string WebPublishPath { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Дата последнего редактирования
        /// </summary>
        public DateTime LastEditedDateTime { get; set; }

        /// <summary>
        /// Дата создания бекапа
        /// </summary>
        public DateTime? BackupDate { get; set; }

        /// <summary>
        /// Дата пересчета размера базы
        /// </summary>
        public DateTime? CalculateSizeDateTime { get; set; }

        /// <summary>
        /// Путь до бекапа
        /// </summary>
        public string ArchivePath { get; set; }

        public AdditionalDataForWorkingWithDatabasesDto CommonDataForWorkingWithDB { get; set; }

        /// <summary>
        /// Операция с базой данных (Бекапирование, Архивация и тд)
        /// </summary>
        public List<DatabaseOperation> DatabaseOperations { get; set; }

        /// <summary>
        /// подключена ли база к автообновлению
        /// </summary>
        public bool? HasAutoUpdate { get; set; }

        /// <summary>
        /// Коментарий при создании базы
        /// </summary>
        public string CreateDatabaseComment { get; set; }

    }
}
