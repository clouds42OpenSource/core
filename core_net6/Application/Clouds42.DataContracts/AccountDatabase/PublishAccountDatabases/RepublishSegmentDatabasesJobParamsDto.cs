﻿using System;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    ///     Модель входных параметров джобы переопубликации базы.
    /// </summary>
    public class RepublishSegmentDatabasesJobParamsDto
    {
        /// <summary>
        ///     Список всех опубликованных баз в сегменте
        /// </summary>
        public Guid SegmentId { get; set; }

        /// <summary>
        ///     Версия платформы
        /// </summary>
        public PlatformType PlatformType { get; set; }

        /// <summary>
        ///     Версия дистрибутива
        /// </summary>
        public DistributionType DistributionType { get; set; }
    }
}
