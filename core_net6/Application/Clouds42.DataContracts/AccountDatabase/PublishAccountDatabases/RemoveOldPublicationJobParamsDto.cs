﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    /// Параметры джобы удаления старой публикации инф. базы
    /// </summary>
    public class RemoveOldPublicationJobParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID старого сегмента
        /// </summary>
        public Guid OldSegmentId { get; set; }
    }
}
