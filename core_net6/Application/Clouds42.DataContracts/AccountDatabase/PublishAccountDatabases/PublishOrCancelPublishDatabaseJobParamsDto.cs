﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    ///     Модель входных параметров джобы публикации базы.
    /// </summary>
    public class PublishOrCancelPublishDatabaseJobParamsDto
    {
        /// <summary>
        ///     Идентификатор базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
    }
}
