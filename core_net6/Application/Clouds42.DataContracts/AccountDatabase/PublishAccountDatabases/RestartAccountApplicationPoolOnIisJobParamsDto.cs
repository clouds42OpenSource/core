﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    ///     Модель входных параметров для джобы рестарта пула приложения аккаунта на нодах публикаций
    /// </summary>
    public class RestartAccountApplicationPoolOnIisJobParamsDto
    {
        /// <summary>
        /// ID базы аккаунта
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
    }
}
