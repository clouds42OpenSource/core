﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    /// Параметры управления редиректом для опубликованной базы
    /// </summary>
    public class ManagePublishedAcDbRedirectParamsDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Номер инф. базы
        /// </summary>
        public string AccountDatabaseV82Name { get; set; }

        /// <summary>
        /// Тип операции для управления редиректом инф. базы
        /// </summary>
        public ManageAcDbRedirectOperationType OperationType { get; set; }

        /// <summary>
        /// Название web группы компании
        /// </summary>
        public string CompanyWebGroupName { get; set; }

        /// <summary>
        /// Физический путь к опубликованной инф. базе
        /// </summary>
        public string PublishedDatabasePhysicalPath { get; set; }

        /// <summary>
        /// Путь к модулю 1С
        /// </summary>
        public string Module1CPath { get; set; }
    }
}
