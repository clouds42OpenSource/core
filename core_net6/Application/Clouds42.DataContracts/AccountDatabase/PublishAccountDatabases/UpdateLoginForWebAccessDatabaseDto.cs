﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases
{
    /// <summary>
    /// Модель параметров обновления логина
    /// для веб доступа базы
    /// </summary>
    public class UpdateLoginForWebAccessDatabaseDto
    {
        /// <summary>
        /// Старый логин пользователя
        /// </summary>
        public string OldLogin { get; set; }

        /// <summary>
        /// Новый логин пользователя
        /// </summary>
        public string NewLogin { get; set; }

        /// <summary>
        /// Идентификатор базы
        /// </summary>
        public List<Guid> AccountDatabasesId { get; set; }
    }
}
