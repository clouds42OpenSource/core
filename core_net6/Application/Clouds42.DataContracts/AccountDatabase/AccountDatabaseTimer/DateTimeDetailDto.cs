﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseTimer
{
    /// <summary>
    /// Модель деталей времени
    /// </summary>
    public class DateTimeDetailDto
        {
            /// <summary>
            /// Часы
            /// </summary>
            public int Hours { get; set; }

            /// <summary>
            /// Минуты
            /// </summary>
            public int Minutes { get; set; }

            /// <summary>
            /// Секунды
            /// </summary>
            public int Seconds { get; set; }

            /// <summary>
            /// Общее количество секунд
            /// </summary>
            public long TotalSeconds { get; set; }
        }
}
