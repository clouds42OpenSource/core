﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit
{

    /// <summary>
    /// Модель услуг сервисов аккаунта
    /// </summary>
    public class AccountBillingServiceTypesModelDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Список услуг сервисов аккаунта
        /// </summary>
        public List<AccountBillingServiceTypeModelDto> BillingServiceTypes { get; set; } = [];
    }
}
