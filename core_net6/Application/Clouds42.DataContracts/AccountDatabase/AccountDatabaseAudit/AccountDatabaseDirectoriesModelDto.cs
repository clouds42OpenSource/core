﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit
{
    /// <summary>
    /// Модель директорий информационной базы
    /// </summary>
    public class AccountDatabaseDirectoriesModelDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Список дерикторий для информационной базы
        /// </summary>
        public List<AccountDatabaseDirectoryDto> Directories { get; set; } = [];
    }
}
