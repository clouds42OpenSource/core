﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit
{
    /// <summary>
    /// Audit result model
    /// </summary>
    public class AuditResultDto
    {
        /// <summary>
        /// Audit success status
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Message for error (if the audit fails)
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Error type (if the audit fails)
        /// </summary>
        public AuditErrorType? ErrorType { get; set; }
    }

    /// <summary>
    /// Errors of the audit process
    /// </summary>
    public enum AuditErrorType
    {
        Unknown = 0,

        /// <summary>
        /// Mismatch of paths to the 1C module in the web.config file
        /// </summary>
        WebConfigPaths = 1,

        /// <summary>
        /// Mismatch of settings in the default.vrd file
        /// </summary>
        VrdConfigPaths = 2,
    }
}
