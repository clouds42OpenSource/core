﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit
{
    /// <summary>
    /// Модель услуг сервиса
    /// </summary>
    public class AccountBillingServiceTypeModelDto
    {
        /// <summary>
        /// Id услуги сервиса
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }

        /// <summary>
        /// Имя услуги сервиса
        /// </summary>
        public string BillingServiceTypeName { get; set; }
    }
}
