﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAudit
{
    /// <summary>
    /// Модель директории инф. базы
    /// </summary>
    public class AccountDatabaseDirectoryDto
    {
        /// <summary>
        /// Путь
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Соответсвует сегменту
        /// </summary>
        public bool MatchToSegment { get; set; }
    }
}
