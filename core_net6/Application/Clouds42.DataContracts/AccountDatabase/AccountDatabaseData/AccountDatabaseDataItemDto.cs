﻿using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Account database description model
    /// </summary>
    public class AccountDatabaseDataItemDto
    {
        /// <summary>
        /// Database identifier
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Database name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Database template name
        /// </summary>
        public string TemplateCaption { get; set; }

        /// <summary>
        /// Template image name
        /// </summary>
        public string TemplateImageName { get; set; }

        /// <summary>
        /// Database size (in MB)
        /// </summary>
        public int SizeInMb { get; set; }

        /// <summary>
        /// Date of last activity in the database
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Link to launch the database
        /// </summary>
        public string DatabaseLaunchLink { get; set; }

        /// <summary>
        /// Is the database a filebase
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// Is the database on delimiters
        /// </summary>
        public bool IsDbOnDelimiters { get; set; }

        /// <summary>
        /// Is the database demo
        /// </summary>
        public bool IsDemo { get; set; }

        /// <summary>
        /// Support available
        /// </summary>
        public bool HasSupport { get; set; }

        /// <summary>
        /// Auto update available
        /// </summary>
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Availability of support for the database
        /// </summary>
        public bool HasAcDbSupport { get; set; }

        /// <summary>
        /// Database state
        /// </summary>
        public DatabaseState State { get; set; }

        /// <summary>
        /// Publish state
        /// </summary>
        public PublishState PublishState { get; set; }

        /// <summary>
        /// Show the web link
        /// </summary>
        public bool NeedShowWebLink { get; set; }

        /// <summary>
        /// Database creation comment
        /// </summary>
        public string CreateAccountDatabaseComment { get; set; }

        /// <summary>
        /// Presence of the process of terminating sessions in the database
        /// </summary>
        public bool IsExistSessionTerminationProcess { get; set; }

        /// <summary>
        /// Имя конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }
        
        /// <summary>
        /// Номер базы
        /// </summary>

        public string V82Name { get; set; }
    }
}
