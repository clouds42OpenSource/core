﻿using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using System;


namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Общие/базовые данные для работы с информационными базами
    /// </summary>
    public class CommonDataForWorkingWithDatabasesDto
    {
        /// <summary>
        /// Id аккаунта,
        /// который будет работать с инф. базами
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя,
        /// который будет работать с инф. базами
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Признак что аккаунт вип
        /// </summary>
        public bool IsVipAccount { get; set; }
        
        /// <summary>
        /// Название локали аккаунта
        /// </summary>
        public string AccountLocaleName { get; set; }

        /// <summary>
        /// Id сервиса "Мои инф. базы"
        /// </summary>
        public Guid MyDatabaseServiceId { get; set; }

        /// <summary>
        /// ID услуги "Доступ в серверную базу"
        /// </summary>
        public Guid AccessToServerDatabaseServiceTypeId { get; set; }

        /// <summary>
        /// Главный сервис Аренда 1С доступен
        /// </summary>
        public bool IsMainServiceAllowed { get; set; }

        /// <summary>
        /// Информация о состоянии главного сервиса Аренда 1С
        /// (заблокирован сервис или нет)
        /// </summary>
        public ServiceStatusModelDto MainServiceStatusInfo { get; set; }

        /// <summary>
        /// Разрешения для работы с базой данных
        /// </summary>
        public PermissionsForWorkingWithDatabaseDto PermissionsForWorkingWithDatabase { get; set; }

        /// <summary>
        /// Информация об админе аккаунта,
        /// который будет работать с инф. базами
        /// </summary>
        public string AccountAdminInfo { get; set; }

        /// <summary>
        /// Телефон админа аккаунта
        /// </summary>
        public string AccountAdminPhoneNumber{ get; set; }

        /// <summary>
        /// Информация о разрешении добавления информационной базы
        /// </summary>
        public AllowAddingDatabaseInfoDto AllowAddingDatabaseInfo { get; set; }

        /// <summary>
        /// Дополнительные данные для работы с информационными базами
        /// </summary>
        public AdditionalDataForWorkingWithDatabasesDto AdditionalDataForWorkingWithDatabases { get; set; }
    }
}
