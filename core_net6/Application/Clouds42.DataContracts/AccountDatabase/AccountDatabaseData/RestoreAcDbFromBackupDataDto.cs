﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель данных для восстановления базы из бекапа
    /// </summary>
    public class RestoreAcDbFromBackupDataDto
    {
        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Доступные пользователи для выдачи доступа к инф. базе
        /// </summary>
        public List<UserInfoDataDto> AvailableUsersForGrantAccess { get; set; }
    }
}
