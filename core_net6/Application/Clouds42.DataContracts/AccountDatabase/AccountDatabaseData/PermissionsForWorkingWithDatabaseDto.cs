﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель разрешений для работы с базой данных
    /// </summary>
    public class PermissionsForWorkingWithDatabaseDto
    {
        /// <summary>
        /// Имеет разрешение для Rdp подключения
        /// </summary>
        public bool HasPermissionForRdp { get; set; }

        /// <summary>
        /// Имеет разрешение для Web подключения
        /// </summary>
        public bool HasPermissionForWeb { get; set; }

        /// <summary>
        /// Имеет разрешение для создания информационной базы
        /// </summary>
        public bool HasPermissionForCreateAccountDatabase { get; set; }

        /// <summary>
        /// Имеет разрешение на просмотр данных поддержки
        /// </summary>
        public bool HasPermissionForDisplaySupportData { get; set; }

        /// <summary>
        /// Имеет разрешение на просмотр платежей
        /// </summary>
        public bool HasPermissionForDisplayPayments { get; set; }

        /// <summary>
        /// Имеет разрешение на отображение кнопки АО
        /// </summary>
        public bool HasPermissionForDisplayAutoUpdateButton { get; set; }

        /// <summary>
        /// Имеет разрешение для множественных действий с базой
        /// </summary>
        public bool HasPermissionForMultipleActionsWithDb { get; set; }
    }
}
