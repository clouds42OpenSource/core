﻿using System;
using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Дополнительные данные для работы с информационными базами
    /// </summary>
    public class AdditionalDataForWorkingWithDatabasesDto
    {
        /// <summary>
        /// Доступные для выбора статусы инф. базы
        /// </summary>
        public List<KeyValueDto<int>> AvailableDatabaseStatuses { get; set; } = [];

        /// <summary>
        /// Доступные для выбора типы платформы
        /// </summary>
        public List<KeyValueDto<int>> AvailablePlatformTypes { get; set; } = [];

        /// <summary>
        /// Доступные для выбора модели восстановления инф. базы
        /// </summary>
        public List<KeyValueDto<int>> AvailableAccountDatabaseRestoreModelTypes { get; set; } = [];

        /// <summary>
        /// Доступные шаблоны инф. баз
        /// </summary>
        public List<KeyValueDto<Guid>> AvailableDbTemplates { get; set; } = [];

        /// <summary>
        /// Типы расширений сервиса для инф. базы
        /// </summary>
        public List<KeyValueDto<int>> ServiceExtensionsForDatabaseTypes { get; set; } = [];
    }
}
