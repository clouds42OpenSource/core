﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель фильтра информационных баз
    /// </summary>
    public class AccountDatabasesFilterDto
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Тип принадлежности базы
        /// </summary>
        public AccountDatabaseAffiliationTypeEnum AccountDatabaseAffiliationType { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Флаг нужно ли получить только серверные базы
        /// </summary>
        public bool IsServerDatabaseOnly { get; set; }
        
        /// <summary>
        /// Признак необходимости выбирать только базы в архиве
        /// </summary>
        public bool IsArchievedDatabaseOnly { get; set; }

        /// <summary>
        /// тип для фильтров информационных баз
        /// </summary>
        public AccountDatabaseTypeEnum AccountDatabaseTypeEnum { get; set; }

        /// <summary>
        /// Признак новой страницы
        /// </summary>
        public bool isNewPage { get; set; } = false;
    }
}
