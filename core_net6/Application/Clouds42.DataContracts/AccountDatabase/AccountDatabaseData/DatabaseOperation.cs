﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData;

public class DatabaseOperation
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public decimal Cost { get; set; }
    public string Currency { get; set; }
}
