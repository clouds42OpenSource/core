﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseCard;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель данных для создания инф. базы из шаблона
    /// </summary>
    public class CreateAcDbFromTemplateDataDto
    {
        /// <summary>
        /// Признак что сервис доступен
        /// </summary>
        public bool IsMainServiceAllow { get; set; }

        /// <summary>
        /// ID сервиса "Мои инф. базы"
        /// </summary>
        public Guid MyDatabasesServiceId { get; set; }

        /// <summary>
        /// Доступные для создания шаблоны
        /// </summary>
        public List<AvailableForCreateDbTemplateDto> AvailableTemplates { get; set; } = [];

        /// <summary>
        /// Доступные пользователи для выдачи доступа к инф. базе
        /// </summary>
        public List<UserInfoDataDto> AvailableUsersForGrantAccess { get; set; } = [];
    }
}
