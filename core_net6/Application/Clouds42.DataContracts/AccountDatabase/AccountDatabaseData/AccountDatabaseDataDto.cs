﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель данных информационной базы
    /// </summary>
    public class AccountDatabaseDataDto : SelectDataResultCommonDto<AccountDatabaseDataItemDto>
    {
        /// <summary>
        /// Количество баз в архиве
        /// </summary>
        public int ArchievedDatabasesCount { get; set; }

        /// <summary>
        /// Количество серверных баз
        /// </summary>
        public int ServerDatabasesCount { get; set; }

        /// <summary>
        /// Количество файловых баз
        /// </summary>
        public int FileDatabasesCount { get; set; }

        /// <summary>
        /// Количество баз на разделителях
        /// </summary>
        public int DelimeterDatabasesCount { get; set; }

        /// <summary>
        /// New databases count (in the basket)
        /// </summary>
        public int DeletedDatabases { get; set; }

        /// <summary>
        /// Количество всех баз
        /// </summary>
        public int AllDatabasesCount { get; set; }

        
    }
}
