﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Информация о разрешении добавления информационной базы
    /// </summary>
    public class AllowAddingDatabaseInfoDto
    {
        /// <summary>
        /// Разрешено ли создать базу
        /// </summary>
        public bool IsAllowedCreateDb { get; set; }

        /// <summary>
        /// Сообщение о заперете
        /// </summary>
        public string ForbiddeningReasonMessage { get; set; }

        /// <summary>
        /// Доплата за создание
        /// </summary>
        public decimal SurchargeForCreation { get; set; }
    }
}
