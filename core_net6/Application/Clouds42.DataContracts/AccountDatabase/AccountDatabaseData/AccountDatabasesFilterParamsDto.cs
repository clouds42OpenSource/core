﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Параметры фильтра для поиска информационных баз
    /// </summary>
    public class AccountDatabasesFilterParamsDto : SelectDataCommonDto<AccountDatabasesFilterDto>
    {
    }
}
