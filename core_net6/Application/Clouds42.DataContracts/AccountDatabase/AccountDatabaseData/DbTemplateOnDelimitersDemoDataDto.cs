﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Данные демо шаблона базы на разделителях
    /// </summary>
    public class DbTemplateOnDelimitersDemoDataDto
    {
        /// <summary>
        /// Наличие базы на разделителях с демо данными
        /// </summary>
        public bool HasDbOnDelimitersWithDemoData { get; set; }

        /// <summary>
        /// Путь к опубликованной базе
        /// </summary>
        public string WebPublishPath { get; set; }
    }
}
