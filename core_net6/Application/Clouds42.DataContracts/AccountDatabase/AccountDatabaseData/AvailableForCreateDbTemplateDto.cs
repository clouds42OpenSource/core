﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseData
{
    /// <summary>
    /// Модель доступного для создания шаблона
    /// </summary>
    public class AvailableForCreateDbTemplateDto
    {
        /// <summary>
        /// ID шаблона
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// ID услуги сервиса "Мои инф. базы"
        /// </summary>
        public Guid? MyDatabasesServiceTypeId { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Название шаблона для разделителей
        /// </summary>
        public string CaptionOnDelimiters { get; set; }

        /// <summary>
        /// Признак что демо данные доступны
        /// </summary>
        public bool IsDemoDataAvailable { get; set; }

        /// <summary>
        /// Признак что шаблон можно публиковать
        /// </summary>
        public bool CanWebPublish { get; set; }

        /// <summary>
        /// Признак что шаблон базы на разделителях
        /// </summary>
        public bool IsDbTemplateDelimiters { get; set; }

        /// <summary>
        /// Данные демо шаблона базы на разделителях
        /// </summary>
        public DbTemplateOnDelimitersDemoDataDto DbTemplateOnDelimitersDemoData { get; set; } = new();

        /// <summary>
        /// Ссылка на изображение шаблона
        /// </summary>
        public string TemplateImgUrl { get; set; }

        /// <summary>
        /// Версия шаблона
        /// </summary>
        public string VersionTemplate { get; set; }
    }
}
