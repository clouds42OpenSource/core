﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    /// <summary>
    /// Модель параметров управления доступом к инф. базе для пользователя
    /// </summary>
    public class ManageAcDbAccessParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Тип действия по управлению доступом
        /// </summary>
        public UpdateAcDbAccessActionType ActionType { get; set; }
    }
}
