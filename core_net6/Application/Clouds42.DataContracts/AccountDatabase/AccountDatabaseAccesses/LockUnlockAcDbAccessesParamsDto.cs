﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    /// <summary>
    /// Модель параметров блокировки/разблокировки доступов к инф. базам
    /// </summary>
    public class LockUnlockAcDbAccessesParamsDto
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Признак доступности
        /// </summary>
        public bool IsAvailable { get; set; }
    }
}
