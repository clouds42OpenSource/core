﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    /// <summary>
    /// Модель установки статуса доступа к базе на разделителях
    /// </summary>
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetAcDbOnDelimitersAccessStatusDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        [XmlElement(ElementName = "AccountDatabaseID")]
        [DataMember(Name = "AccountDatabaseID")]
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        [XmlElement(ElementName = "UserID")]
        [DataMember(Name = "UserID")]
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Статус доступа
        /// </summary>
        [XmlElement(ElementName = "State")]
        [DataMember(Name = "State")]
        public string AccessState { get; set; }
    }
}
