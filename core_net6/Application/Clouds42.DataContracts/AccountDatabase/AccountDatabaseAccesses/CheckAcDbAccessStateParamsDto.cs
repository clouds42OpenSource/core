﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    /// <summary>
    /// Модель параметров обновления доступа к инф. базе
    /// </summary>
    public class CheckAcDbAccessStateParamsDto
    {
        /// <summary>
        /// ID доступа к инф. базе
        /// </summary>
        public Guid AcDbAccessId { get; set; }

        /// <summary>
        /// Тип действия
        /// </summary>
        public UpdateAcDbAccessActionType ActionType { get; set; }
    }
}
