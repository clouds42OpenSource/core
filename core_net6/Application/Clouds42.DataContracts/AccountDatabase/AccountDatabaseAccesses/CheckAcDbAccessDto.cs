﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    /// <summary>
    /// Модель проверки доступа к инф. базе
    /// </summary>
    public class CheckAcDbAccessDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        public UpdateAcDbAccessActionType Operation { get; set; }

        /// <summary>
        /// Статус доступа
        /// </summary>
        public AccountDatabaseAccessState AccessState { get; set; }

        /// <summary>
        /// Признак что доступ внешнему пользователю
        /// </summary>
        public bool IsExternal { get; set; }

        /// <summary>
        /// Причина задержки
        /// </summary>
        public string DelayReason { get; set; }
    }
}
