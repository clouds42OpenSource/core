﻿using System.Collections.Generic;
using Clouds42.DataContracts.AccountUser;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses
{
    public class ManageAccessResultDto
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string DatabasesName { get; set; }
        public string UserEmail { get; set; }
    }

    public class GrandAccessForAllUsersOfAccountResultDc : ManageAccessResultDto
    {
        public List<AccessUserInfoDc> AccountUsers { get; set; }
    }
}
