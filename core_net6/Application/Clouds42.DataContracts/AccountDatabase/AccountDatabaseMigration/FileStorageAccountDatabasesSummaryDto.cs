﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Список хранилищ с информацией о размерах и количествах баз в них
    /// </summary>
    public sealed class FileStorageAccountDatabasesSummaryDto
    {
        /// <summary>
        /// Имя хранилища
        /// </summary>
        public string FileStorage { get; set; }

        /// <summary>
        /// Путь хранилища
        /// </summary>
        public string PathStorage { get; set; }
        
        /// <summary>
        /// Количество ИБ находящихся в этом хранилище
        /// </summary>
        public int DatabaseCount { get; set; }
        
        /// <summary>
        /// Место в Mb занимаемое ИБ в хранилище
        /// </summary>
        public int TotatlSizeOfDatabases { get; set; }
    }
}