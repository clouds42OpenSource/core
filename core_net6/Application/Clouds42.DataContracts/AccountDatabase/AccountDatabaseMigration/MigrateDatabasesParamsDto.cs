﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{

    /// <summary>
    /// Модель параметров на миграцию выбранных баз в новое хранилище
    /// </summary>
    public sealed class MigrateDatabasesParamsDto
    {
        /// <summary>
        /// Выбранные номера баз для миграции
        /// </summary>
        public string[] SelectedDatabaseNumbers { get; set; }

        /// <summary>
        /// Выбранное хранилище для миграции
        /// </summary>
        public Guid SelectedStorageId { get; set; }
    }
}