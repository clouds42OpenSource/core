﻿using System;
using System.Collections.Generic;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    ///  Модель для миграции информационных баз аккаунта
    /// </summary>
    public sealed class AccountDatabasesMigrationResultDto
    {
        /// <summary>
        /// Список хранилищ с информацией о размерах и количествах баз в них
        /// </summary>
        public FileStorageAccountDatabasesSummaryDto[] FileStorageAccountDatabasesSummary { get; set; }
        
        /// <summary>
        /// Базы аккаунта
        /// </summary>
        public PagedDto<AccountDatabaseMigrationItemDto> Databases { get; set; }

        /// <summary>
        /// Возможные файловые хранилища для миграции, где Key - ID файлового хранилища, Value - Название файлового хранилища 
        /// </summary>
        public KeyValuePair<Guid, string>[] AvalableFileStorages { get; set; }

        /// <summary>
        /// Cписка путей доступных хранилищ
        /// </summary>
        public string[] AvailableStoragePath { get; set; }

        /// <summary>
        /// Количество баз разрешенных для миграции
        /// </summary>
        public int NumberOfDatabasesToMigrate { get; set; }
    }
}
