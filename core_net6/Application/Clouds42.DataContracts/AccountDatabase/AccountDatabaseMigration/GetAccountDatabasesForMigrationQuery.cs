﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.BaseModel;
using JetBrains.Annotations;
using LinqExtensionsNetFramework;
using MediatR;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Модель выбора записей списка информационных баз аккаунта для миграции
    /// </summary>
    public class GetAccountDatabasesForMigrationQuery : ISortedQuery, IHasFilter<AccountDatabaseMigrationFilterParamsDto>, IPagedQuery, IRequest<ManagerResult<AccountDatabasesMigrationResultDto>>
    {
        public string OrderBy { get; set; } = $"{nameof(Domain.DataModels.AccountDatabase.V82Name)}.asc";
        [CanBeNull] public AccountDatabaseMigrationFilterParamsDto Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
