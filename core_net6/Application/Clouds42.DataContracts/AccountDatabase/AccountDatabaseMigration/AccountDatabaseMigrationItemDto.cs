﻿using System;
using Clouds42.Domain.Enums;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Модель информационной базы для миграций
    /// </summary>
    public sealed class AccountDatabaseMigrationItemDto
    {
        /// <summary>
        /// ID базы аккаунта
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Путь по которому располагается ИБ
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Занимаемый размер ИБ
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Статус ИБ
        /// </summary>
        public DatabaseState Status { get; set; }

        /// <summary>
        /// Web база
        /// </summary>
        public bool IsPublishDatabase { get; set; }

        /// <summary>
        /// Web сервис
        /// </summary>
        public bool IsPublishServices { get; set; }

        /// <summary>
        /// Выставляется тип информационной базы Файловая или Серверная
        /// </summary>
        public bool IsFile { get; set; }
    }
}
