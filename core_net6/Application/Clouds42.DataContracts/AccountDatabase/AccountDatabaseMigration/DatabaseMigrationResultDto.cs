﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Результат миграции инф. базы
    /// </summary>
    public class DatabaseMigrationResultDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Результат выполнения миграции
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// ID пользователя, инициировавшего миграцию
        /// </summary>
        public Guid AccountUserInitiatorId { get; set; }

        /// <summary>
        /// ID владельца инф. базы
        /// </summary>
        public Guid AccountDatabaseOwnerId { get; set; }
    }
}
