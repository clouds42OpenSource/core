﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Результат миграции инф. базы
    /// </summary>
    public class AccountDatabaseMigrationResultModelDto
    {
        /// <summary>
        /// Результат выполнения миграции
        /// </summary>
        public bool Successful { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid? AccountDatabaseId { get; set; }

        /// <summary>
        /// ID рабочего процесса
        /// </summary>
        public Guid? ProcessFlowId { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}
