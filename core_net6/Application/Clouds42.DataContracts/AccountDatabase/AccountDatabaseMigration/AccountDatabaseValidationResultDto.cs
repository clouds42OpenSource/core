﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Результат валидации инф. базы
    /// </summary>
    public class AccountDatabaseValidationResultDto
    {
        /// <summary>
        /// Результат валидации
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// Сообщение о результате валидации
        /// </summary>
        public string ValidationMessage { get; set; }
    }
}
