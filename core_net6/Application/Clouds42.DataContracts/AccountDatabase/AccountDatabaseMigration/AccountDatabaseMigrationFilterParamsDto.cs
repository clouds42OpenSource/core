﻿using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;
using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Модель фильтра для получения списка информационных баз аккаунта
    /// </summary>
    public class AccountDatabaseMigrationFilterParamsDto : IQueryFilter
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int? AccountIndexNumber { get; set; }

        /// <summary>
        /// Строка поиска
        /// </summary>
        [CanBeNull]
        public string SearchLine { get; set; }

        /// <summary>
        /// Тип хранилища, <c>true</c> - файловое, <c>false</c> - серверное, <c>null</c> - файловое и серверное
        /// </summary>
        public bool? IsTypeStorageFile { get; set; }

        [Required]
        public Guid AccountId { get; set; }
    }
}
