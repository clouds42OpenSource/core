﻿using System;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration
{
    /// <summary>
    /// Модель параметров миграции инф. базы
    /// </summary>
    public class MigrateAccountDatabaseParametersDto : BaseAccountDatabaseOperationParamsDto
    {
        /// <summary>
        /// ID файлового хранилища, в которое нужно перенести базу
        /// </summary>
        public Guid DestinationFileStorageId { get; set; }

        /// <summary>
        /// ID исходного файлового хранилища
        /// </summary>
        public Guid SourceFileStorageId { get; set; }

        /// <summary>
        /// Старое расположение инф. базы
        /// </summary>
        public string OldDatabasePath { get; set; }

        /// <summary>
        /// Ключ основного рабочего процесса
        /// </summary>
        public string ParentProcessFlowKey { get; set; }
    }
}
