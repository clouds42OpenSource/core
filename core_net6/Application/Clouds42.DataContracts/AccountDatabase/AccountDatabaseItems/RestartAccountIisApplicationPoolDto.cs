﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class RestartAccountIisApplicationPoolDto
    {
        public Guid databaseId { get; set; }
    }
}
