﻿using Clouds42.DataContracts.BaseModel;
using LinqExtensionsNetFramework;
using PagedListExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель выбора записей списка информационных баз
    /// </summary>
    public class GetAccountDatabaseListParamsDto : IHasFilter<AccountDatabaseItemsFilterParamsDto>, ISortedQuery, IPagedQuery
    {
        public string OrderBy { get; set; }
        public AccountDatabaseItemsFilterParamsDto Filter { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
}
