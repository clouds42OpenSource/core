﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель для принятия заявки на смену режима работы инф базы
    /// </summary>
    public class RequestToChangeAccDbTypeDto
    {
        public Guid AccountDatabseId { get; set; }
        public string AdminPhoneNumber { get; set; }
        public string DatabaseAdminLogin { get; set; }
        public string DatabaseAdminPassword { get; set; }
        public DateTime? ChangeTypeDate { get; set; }
    }
}
