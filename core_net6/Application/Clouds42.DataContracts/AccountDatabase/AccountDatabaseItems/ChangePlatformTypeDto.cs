﻿using System;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель для эндпоинта на смену платформы
    /// </summary>
    public class ChangePlatformTypeDto
    {
        public Guid DatabaseId { get; set; }
        public PlatformType PlatformType { get; set; }
        public DistributionType DistributionType { get; set; } = DistributionType.Stable;
    }
}
