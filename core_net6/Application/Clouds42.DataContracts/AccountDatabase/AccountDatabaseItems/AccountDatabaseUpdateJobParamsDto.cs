﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class AccountDatabaseUpdateJobParamsDto
    {
        public Guid AccountDatabaseId { get; set; }
    }
}
