﻿using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Domain.IDataModels;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Расширения для инф. баз
    /// </summary>
    public static class AccountDatabaseExtensionsDto
    {
        /// <summary>
        /// На разделителях база
        /// </summary>
        /// <param name="accountDatabase">Информаионная база</param>
        /// <returns>Флаг показывающий является ли база на разделителях</returns>
		public static bool IsDelimiter(this Domain.DataModels.AccountDatabase accountDatabase)
		    => accountDatabase.AccountDatabaseOnDelimiter != null;

        /// <summary>
        /// Информационная база является демо на разделителях
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>true - если база демо на разделителях</returns>
        public static bool IsDemoDelimiter(this Domain.DataModels.AccountDatabase accountDatabase)
        {
            var acDbOnDelimiters = accountDatabase.AccountDatabaseOnDelimiter;
            return acDbOnDelimiters is { IsDemo: true };
        }

        /// <summary>
        /// Получить аккаунт базы источника для разделителей
        /// </summary>
        /// <param name="accountDatabase">Информаионная база</param>
        /// <returns>Аккаунт базы источника в случае если он есть нет тогда null</returns>
        public static IAccount GetSourceAccount(this Domain.DataModels.AccountDatabase accountDatabase)
        {
	        return accountDatabase.AccountDatabaseOnDelimiter.SourceAccountDatabase?.Account;
        }

        /// <summary>
        /// Получить название шаблона для базы
        /// </summary>
        /// <param name="accountDatabase">Информаионная база</param>
        /// <returns>Название шаблона</returns>
        public static string GetTemplateName(this Domain.DataModels.AccountDatabase accountDatabase) => accountDatabase.DbTemplate != null
            ? accountDatabase.DbTemplate.DefaultCaption
            : "";

        /// <summary>
        /// Получить ссылку картинки шаблона
        /// </summary>
        /// <param name="accountDatabase">Информаионная база</param>
        /// <returns>Ссылка картинки шаблона</returns>
        public static string GetTemplateImgUrl(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.DbTemplate != null ? accountDatabase.DbTemplate.ImageUrl : "";

        /// <summary>
        /// Информационная база удалена
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Является ли база удаленной</returns>
        public static bool IsDeleted(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.StateEnum == DatabaseState.DeletedToTomb ||
            accountDatabase.StateEnum == DatabaseState.RestoringFromTomb ||
            accountDatabase.StateEnum == DatabaseState.DelitingToTomb ||
            accountDatabase.StateEnum == DatabaseState.DetachingToTomb ||
            accountDatabase.StateEnum == DatabaseState.DeletedFromCloud;

        /// <summary>
        /// Возможность выдать доступ в инф. базу
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Возможность выдать доступ в инф. базу</returns>
        public static bool CanAddAccessToDb(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.StateEnum == DatabaseState.DeletedToTomb ||
            accountDatabase.StateEnum == DatabaseState.DelitingToTomb ||
            accountDatabase.StateEnum == DatabaseState.DetachingToTomb ||
            accountDatabase.StateEnum == DatabaseState.DeletedFromCloud ||
            accountDatabase.StateEnum == DatabaseState.DetachedToTomb;

        /// <summary>
        /// Возможность перезапустить пул приложения инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Возможность перезапустить пул приложения</returns>
        public static bool CanRestartIisApplicationPool(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.PublishStateEnum == PublishState.Published &&
            accountDatabase.StateEnum == DatabaseState.Ready;

        /// <summary>
        /// Возможность завершить сеансы инф. базы на терминальном сервере
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Возможность завершить сеансы инф. базы</returns>
        public static bool CanTerminate1CProcessesInTerminalServer(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.IsFile == true && accountDatabase.StateEnum == DatabaseState.Ready &&
            !accountDatabase.IsDelimiter();

        /// <summary>
        /// Проверить, что база в одном из статусов Ошибка создания
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Признак, что база в одном из статусов Ошибка создания</returns>
        public static bool IsErrorCreate(this Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.StateEnum == DatabaseState.ErrorCreate ||
            accountDatabase.StateEnum == DatabaseState.ErrorDtFormat || 
            accountDatabase.StateEnum == DatabaseState.ErrorNotEnoughSpace;
    }
}
