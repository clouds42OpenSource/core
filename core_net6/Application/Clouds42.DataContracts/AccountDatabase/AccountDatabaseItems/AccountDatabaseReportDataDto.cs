﻿using System;
using System.Runtime.Serialization;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель данных по информационной базе для отчета
    /// </summary>
    [DataContract]
    public class AccountDatabaseReportDataDto
    {
        /// <summary>
        /// Номер аккаунта
        /// </summary>
        [DataMember]
        public int AccountNumber { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        [DataMember]
        public string DatabaseNumber { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [DataMember]
        public string Caption { get; set; }

        /// <summary>
        /// Шаблон
        /// </summary>
        [DataMember]
        public string Template { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [DataMember]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
        [DataMember]
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Состояние базы
        /// </summary>
        [DataMember]
        public string State { get; set; }

        /// <summary>
        /// Тип базы
        /// </summary>
        [DataMember]
        public string DatabaseType { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        [DataMember]
        public string Platform { get; set; }

        /// <summary>
        /// Релиз платформы
        /// </summary>
        [DataMember]
        public string PlatformRelease { get; set; }

        /// <summary>
        /// Размер базы
        /// </summary>
        [DataMember]
        public int DatabaseSize { get; set; }

        /// <summary>
        /// Опубликована база
        /// </summary>
        [DataMember]
        public bool IsPublished { get; set; }

        /// <summary>
        /// Количество внешних доступов к базе
        /// </summary>
        [DataMember]
        public int ExternalAccessesToDatabaseCount { get; set; }

        /// <summary>
        /// Количество внутренних доступов к базе
        /// </summary>
        [DataMember]
        public int InternalAccessesToDatabaseCount { get; set; }

        /// <summary>
        /// Наличие поддержки
        /// </summary>
        [DataMember]
        public bool HasSupport { get; set; }

        /// <summary>
        /// Наличие автообновления
        /// </summary>
        [DataMember]
        public bool HasAutoUpdate { get; set; }

        /// <summary>
        /// Номер области
        /// </summary>
        [DataMember]
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Название материнской базы
        /// </summary>
        [DataMember]
        public string SourceDbName { get; set; }
    }
}
