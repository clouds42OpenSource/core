﻿using System.IO;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Вспомогательный класс, для формирования пути к базам, согласно принятым правилам.
    /// </summary>
    public static class AccountDatabasePathDto
    {
        private const string Prefix = @"company_";
        private const string Suffix = @"\fileinfobases";

        /// <summary>
        /// Метод для создания строки пути к базе данных, согласно принятым правилам.
        /// </summary>
        /// <param name="connectionAddress">Адрес подключения</param>
        /// <param name="indexNumber">Индекс</param>
        /// <param name="dbName">Имя базы данных</param>
        /// <returns>Путь к базе данных</returns>
        public static string Create(string connectionAddress, int indexNumber, string dbName) =>
            Path.Combine(connectionAddress ?? string.Empty,
                $"{Prefix}{indexNumber}{Suffix}",
                dbName ?? string.Empty
            );
    }
}