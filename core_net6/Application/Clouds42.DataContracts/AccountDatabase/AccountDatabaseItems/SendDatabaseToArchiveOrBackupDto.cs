﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Объект создания бекапа или архива базы
    /// </summary>
    public  class SendDatabaseToArchiveOrBackupDto
    {
        public Guid DatabaseId { get; set; }
        public Guid ServiceId { get; set; }
        public Guid AccountId { get; set; }
        public DatabaseOperation Operation { get; set; }
        public bool UsePromisePayment { get; set; }
        public decimal Cost { get; set; }
        public string Currency { get; set; }
    }

    public enum DatabaseOperation
    {
        Archive = 1,
        Backup = 2,
        TechnicalBackup = 3
    }
}
