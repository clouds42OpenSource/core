﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class AccountDatabaseSearchResultItemDto
    {
        public string AccountDatabaseName { get; set; }
        public Guid AccountDatabaseId { get; set; }
        public string AccountDatabaseCaption { get; set; }
    }
}