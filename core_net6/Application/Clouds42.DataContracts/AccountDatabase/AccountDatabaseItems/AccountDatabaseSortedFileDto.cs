﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Отсортированная часть в списке для склейки при загрузке файла инф. базы
    /// </summary>
    public class AccountDatabaseSortedFileDto
    {
        /// <summary>
        /// Порядковый номер в списке
        /// </summary>
        public int FileOrder { get; set; }

        /// <summary>
        /// Название части файла
        /// </summary>
        public string FileName { get; set; }
    }
}
