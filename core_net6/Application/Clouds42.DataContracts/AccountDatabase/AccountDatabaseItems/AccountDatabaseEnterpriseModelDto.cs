﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.Domain.DataModels;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель серверной инфомационной базы.
    /// </summary>
    public class AccountDatabaseEnterpriseModelDto : IAccountDatabaseEnterpriseDto
    {        

        /// <summary>
        /// Информационная база.
        /// </summary>
        public Domain.DataModels.AccountDatabase AccountDatabase { get; set; }        

        /// <summary>
        /// Данные о сегмента кластера.
        /// (Опционально) только для серверных баз.
        /// </summary>
        public CloudServicesEnterpriseServer EnterpriseServer { get; set; }

        /// <summary>
        /// Данные о SQL сервере.        
        /// </summary>
        public CloudServicesSqlServer SqlServer { get; set; }

        /// <summary>
        /// Данные о сегменте
        /// </summary>
        public Domain.DataModels.CloudServicesSegment Segment { get; set; }

        /// <summary>
        /// Путь к платформе 1С x64
        /// </summary>
        public string PlatformPathX64 { get; set; }
    }
}
