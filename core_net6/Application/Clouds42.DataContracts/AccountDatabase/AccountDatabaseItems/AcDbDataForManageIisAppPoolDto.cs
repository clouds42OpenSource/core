﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Данные инф. базы для перезапуска пула
    /// </summary>
    public class AcDbDataForManageIisAppPoolDto
    {
        /// <summary>
        /// ID сервера публикаций
        /// </summary>
        public Guid ContentServerId { get; set; }

        /// <summary>
        /// Название сайта публикаций
        /// </summary>
        public string PublishSiteName { get; set; }

        /// <summary>
        /// Название пула
        /// </summary>
        public string AppPoolName { get; set; }
    }
}
