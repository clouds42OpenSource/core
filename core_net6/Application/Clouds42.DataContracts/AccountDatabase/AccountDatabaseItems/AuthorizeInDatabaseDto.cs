﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class SupportDataDto
    {
        public Guid DatabaseId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool CompleteSessioin { get; set; }
        public AutoUpdateTimeEnum TimeOfUpdate { get; set; }
        public bool? HasSupport { get; set; }
        public bool? HasAutoUpdate { get; set; }
    }
}
