﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class PublishDatabaseDto
    {
        /// <summary>
        /// Database identifier
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Need publish
        /// </summary>
        public bool Publish { get; set; }
    }
}
