﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель ответа на запрос получения списка информационных баз
    /// </summary>
    public sealed class AccountDatabaseListResultDto: SelectDataResultCommonDto<AccountDatabaseItemDto>
    {
    }
}