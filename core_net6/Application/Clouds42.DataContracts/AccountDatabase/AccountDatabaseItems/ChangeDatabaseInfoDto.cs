﻿using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class ChangeDatabaseInfoDto
    {
        public Guid DatabaseId { get; set; }
        public bool? UsedWebServices { get; set; }
        public DatabaseState? DatabaseState { get; set; }
        public Guid? TemplateId { get; set; }
        public string Caption { get; set; }
        public string V82Name { get; set; }
        public bool? HasModifications { get; set; }
        public AccountDatabaseRestoreModelTypeEnum? RestoreModel { get; set; }
    }
}
