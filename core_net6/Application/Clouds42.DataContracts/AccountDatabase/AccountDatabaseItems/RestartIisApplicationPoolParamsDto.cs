﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель параметров перезапуска пула инф. базы на нодах публикации
    /// </summary>
    public class RestartIisApplicationPoolParamsDto
    {
        /// <summary>
        /// ID сервера публикации
        /// </summary>
        public Guid ContentServerId { get; set; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Название пула инф. базы
        /// </summary>
        public string AppPoolName { get; set; }

        /// <summary>
        /// Дата и время начала операции
        /// </summary>
        public DateTime OperationStartDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// Номер информационной базы
        /// </summary>
        public string V82Name { get; set; }
    }
}
