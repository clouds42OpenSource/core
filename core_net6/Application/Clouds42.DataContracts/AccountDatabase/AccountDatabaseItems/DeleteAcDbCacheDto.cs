﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class DeleteAcDbCacheDto
    {
        public Guid AccountDatabaseId { get; set; }
        public bool ClearDatabaseLogs { get; set; } 
    }
}
