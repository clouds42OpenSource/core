﻿namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Метаданные инф. базы из 1С
    /// </summary>
    public class MetadataInfoFrom1CDto
    {
        /// <summary>
        /// Название конфигурации
        /// </summary>
        public string ConfigurationName { get; set; }

        /// <summary>
        /// Версия релиза
        /// </summary>
        public string ReleaseVersion { get; set; }
    }
}
