﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Базовая модель параметров операций с инф. базой
    /// </summary>
    public abstract class BaseAccountDatabaseOperationParamsDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }

        /// <summary>
        /// Дата и время начала операции
        /// </summary>
        public DateTime OperationStartDate { get; set; }

        /// <summary>
        /// ID пользователя, инициировавшего операцию
        /// </summary>
        public Guid AccountUserInitiatorId { get; set; }
    }
}
