﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{

    /// <summary>
    /// Модель описывает информационные базы аккаунта
    /// </summary>
    public class AccountDatabasesOfAccountModelDto
    {

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Список информационных баз
        /// </summary>
        public List<Guid> AccountDatabases { get; set; }
    }

}
