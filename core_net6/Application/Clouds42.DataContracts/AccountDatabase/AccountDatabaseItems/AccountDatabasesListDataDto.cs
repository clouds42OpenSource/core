﻿using Clouds42.DataContracts.BaseModel;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель списка информационных баз с пагинацией
    /// </summary>
    public class AccountDatabasesListDataDto
    {
        /// <summary>
        /// Список информационных баз
        /// </summary>
        public List<AccountDatabaseInfoDataDto> Databases { get; set; }

        /// <summary>
        /// Пагинация
        /// </summary>
        public PaginationBaseDto Pagination { get; set; }
    }
}
