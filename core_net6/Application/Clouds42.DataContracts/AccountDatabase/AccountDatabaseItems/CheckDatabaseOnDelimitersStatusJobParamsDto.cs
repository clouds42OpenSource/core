﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Параметры задачи по проверке статуса базы на разделителях
    /// </summary>
    public class CheckDatabaseOnDelimitersStatusJobParamsDto
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid AccountDatabaseId { get; set; }
    }
}
