﻿using System;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class DatabaseInfoDto
    {
        public string Name { get; set; }

        public bool IsFile { get; set; }

        public string FilePath { get; set; }

        public PlatformType Platform { get; set; }

        public string V82Name { get; set; }

        public Guid AccountId { get; set; }
    }
}
