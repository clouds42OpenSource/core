﻿using LinqExtensionsNetFramework;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Фильтр для получения списка информационных баз
    /// </summary>
    public class AccountDatabaseItemsFilterParamsDto: IQueryFilter, IHasSpecificSearch
    {
        /// <summary>
        /// Строка поиска списка информационных баз
        /// </summary>
        public string SearchLine { get; set; }
    }
}
