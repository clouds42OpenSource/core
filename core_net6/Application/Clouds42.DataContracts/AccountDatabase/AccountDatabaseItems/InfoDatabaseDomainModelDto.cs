﻿using System;
using System.Collections.Generic;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.Interface;
using Newtonsoft.Json;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель инф. базы
    /// </summary>
    public class InfoDatabaseDomainModelDto : IUploadDataDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id шаблона
        /// </summary>
        public virtual Guid TemplateId { get; set; }

        /// <summary>
        /// Имя создаваемой ИБ
        /// </summary>
        public string DataBaseName { get; set; }

        /// <summary>
        /// Выбрана или нет из списка
        /// </summary>
        public bool IsChecked { get; set; }

        /// <summary>
        /// Опубликовывать или нет
        /// </summary>
        public bool Publish { get; set; }

        /// <summary>
        /// С демо данными или нет
        /// </summary>
        public bool DemoData { get; set; }

        /// <summary>
        /// файловая или серверная
        /// </summary>
        public bool IsFile { get; set; } = true;

        /// <summary>
        /// на разделителях или нет
        /// </summary>
        public bool DbTemplateDelimiters { get; set; }

        /// <summary>
        /// размер zip файлы при загрузке баз на разделителях
        /// </summary>
        public long SizeZipFile { get; set; }

        /// <summary>
        /// Список номер аккаунт юзеров к которым должен быть предоставлен доступ.
        /// </summary>
        public List<Guid> UsersToGrantAccess { get; set; } = [];

        /// <summary>
        /// Список информации о пользователе полученного из зип пакета
        /// </summary>
        public List<InfoAboutUserFromZipPackageDto> ListInfoAboutUserFromZipPackage { get; set; }

        /// <summary>
        /// Список сервисов
        /// </summary>
        [JsonProperty("extensions")]
        public List<ExtensionIds> ExtensionsList { get; set; }


        /// <summary>
        /// Логин администратора базы
        /// </summary>
        public string LoginAdmin { get; set; }

        /// <summary>
        /// Пароль администратора базы
        /// </summary>
        public string PasswordAdmin { get; set; } = null;

        /// <summary>
        /// Идентификатор загруженного файла.
        /// </summary>
        public Guid? UploadedFileId { get; set; }

        /// <summary>
        /// ID бэкапа инф. базы
        /// </summary>
        public Guid? AccountDatabaseBackupId { get; set; }
        
        /// <summary>
        /// Нужно ставить на АО или нет
        /// </summary>
        public bool HasAutoupdate { get; set; }
        
        /// <summary>
        /// Нужно ставить на поддержку или нет
        /// </summary>
        public bool HasSupport { get; set; }
    }
}
