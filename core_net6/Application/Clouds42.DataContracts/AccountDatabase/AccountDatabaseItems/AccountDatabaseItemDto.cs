﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель свойств элемента инфомационной базы
    /// </summary>
    public sealed class AccountDatabaseItemDto
    {
        /// <summary>
        /// ID информационной базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// ID Аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Номер аккаунта
        /// </summary>
        public int AccountNumber { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата активности
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Состояние
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// База файловая
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Размер базы в мб
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Состояние блокировки
        /// </summary>
        public string LockedState { get; set; }
    }
}
