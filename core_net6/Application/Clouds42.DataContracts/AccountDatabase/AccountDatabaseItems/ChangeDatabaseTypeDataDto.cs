﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель данных для изменения типа базы
    /// (серверная/файловая)
    /// </summary>
    public class ChangeDatabaseTypeDataDto
    {
        /// <summary>
        /// Регулярный платеж аккаунта
        /// </summary>
        public decimal AccountRegularPayment { get; set; }

        /// <summary>
        /// Стоимость резмещения серверной базы
        /// </summary>
        public decimal PlacementServerDatabaseCost { get; set; }

        /// <summary>
        /// СТоимость доступа в сервреную базу
        /// </summary>
        public decimal AccessToServerDatabaseCost { get; set; }

        /// <summary>
        /// Дата окончания срока действия Аренды 1С
        /// </summary>
        public DateTime Rent1CExpireDate { get; set; }

        /// <summary>
        /// Номер телефона админа аккаунта
        /// </summary>
        public string AccountAdminPhoneNumber { get; set; }
    }
}
