﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель описания информационной базыы
    /// </summary>
    public class AccountDatabaseInfoDataDto
    {
        /// <summary>
        /// ID информационной базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Аккаунт владельца базы
        /// </summary>
        public Domain.DataModels.Account Account { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата активности
        /// </summary>
        public DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Состояние
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// База файловая
        /// </summary>
        public bool? IsFile { get; set; }

        /// <summary>
        /// Версия платформы
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// Размер базы в мб
        /// </summary>
        public int SizeInMB { get; set; }

        /// <summary>
        /// Состояние блокировки
        /// </summary>
        public string LockedState { get; set; }

        /// <summary>
        /// Номер базы
        /// </summary>
        public int DbNumber { get; set; }

        /// <summary>
        /// Номер зоны (если база на разделителях)
        /// </summary>
        public int? ZoneNumber { get; set; }

        /// <summary>
        /// Возможность редактировать базу
        /// </summary>
        public bool AbilityToEdit { get; set; }
    }
}
