﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class DeAuthorizationDatabaseDto
    {
        public Guid DatabaseId { get; set; }
    }
}
