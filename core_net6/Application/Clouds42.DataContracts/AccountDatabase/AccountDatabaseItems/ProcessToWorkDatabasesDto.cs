﻿
using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Модель процесса по работе с информационными базами
    /// </summary>
    public class ProcessToWorkDatabasesDto
    {
        /// <summary>
        /// Список Id информационных баз
        /// </summary>
        public List<Guid> DatabasesId { get; set; } = [];
    }
}
