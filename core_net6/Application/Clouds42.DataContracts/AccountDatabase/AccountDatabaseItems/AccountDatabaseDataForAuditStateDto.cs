﻿using System;
using Clouds42.Domain.DataModels;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    /// <summary>
    /// Данные инф. базы для аудита статуса
    /// </summary>
    public class AccountDatabaseDataForAuditStateDto
    {
        /// <summary>
        /// ID инф. базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название инф. базы
        /// </summary>
        public string V82Name { get; set; }

        /// <summary>
        /// Статус инф. базы
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Дата установки статуса
        /// </summary>
        public DateTime StateDateTime { get; set; }

        /// <summary>
        /// Инф. база на разделителях
        /// </summary>
        public AccountDatabaseOnDelimiters AccountDatabaseOnDelimiters { get; set; }

    }
}
