﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems
{
    public class ChangeAccountDatabaseTypeDto
    {
        public Guid DatabaseId { get; set; } 
        public AccountDatabaseRestoreModelTypeEnum? RestoreModelType { get; set; }
    }
}
