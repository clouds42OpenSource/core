﻿using Clouds42.Domain.Enums._1C;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Модель данных обновления базы.
    /// </summary>
    public interface IUpdateDatabaseDto
    {
        /// <summary>
        /// Назание базы.
        /// </summary>
        string UpdateDatabaseName { get; }                

        /// <summary>
        /// База является файловой.
        /// </summary>
        bool IsFile { get; }

        /// <summary>
        /// Логин администратор.
        /// </summary>
        string AdminLogin { get; }

        /// <summary>
        /// Пароль администратора.
        /// </summary>
        string AdminPassword { get; }

        /// <summary>
        /// Адрес базы.
        /// </summary>
        string ConnectionAddress { get; }

        /// <summary>
        /// Тип платформы.
        /// </summary>
        PlatformType PlatformType { get; }

        /// <summary>
        /// Версия платформы под которой работает база.
        /// </summary>
        string PlatformVersion { get; }

        /// <summary>
        /// Путь до папки где лежит платформа 1С.
        /// </summary>
        string FolderPath1C { get; }
    }
}