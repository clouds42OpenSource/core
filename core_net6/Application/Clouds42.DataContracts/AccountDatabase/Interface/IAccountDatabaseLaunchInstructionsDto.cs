﻿namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Инструкция по запуску инф. базы
    /// </summary>
    public interface IAccountDatabaseLaunchInstructionsDto
    {
        /// <summary>
        /// Запуск разрешен.
        /// </summary>
        bool LaunchAllowed { get; set; }

        /// <summary>
        /// Описание ошибок валидации.
        /// </summary>
        string ValidationErrorMessage { get; set; }

        /// <summary>
        /// Полный путь до клиента 1С на сервере запуска.
        /// </summary>
        string Path1CClient { get; set; }

        /// <summary>
        /// Полный путь до клиента 1С x64 на сервере запуска.
        /// </summary>
        string Path1CClientX64 { get; set; }

        /// <summary>
        /// Версия платформы под которой должна запускаться база.
        /// </summary>
        string PlatformVersion { get; set; }
    }
}