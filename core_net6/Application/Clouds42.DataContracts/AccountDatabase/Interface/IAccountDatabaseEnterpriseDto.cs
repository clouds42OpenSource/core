﻿using Clouds42.Domain.DataModels;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Информационная база с данными о кластере предприятия аккаунта.
    /// </summary>
    public interface IAccountDatabaseEnterpriseDto
    {
        /// <summary>
        /// Информационная база.
        /// </summary>
        Domain.DataModels.AccountDatabase AccountDatabase { get; set; }

        /// <summary>
        /// Данные о сегмента кластера.        
        /// </summary>
        CloudServicesEnterpriseServer EnterpriseServer { get; set; }

        /// <summary>
        /// Данные о SQL сервере.        
        /// </summary>
        CloudServicesSqlServer SqlServer { get; set; }

        /// <summary>
        /// Данные о сегменте
        /// </summary>
        Domain.DataModels.CloudServicesSegment Segment { get; set; }

        /// <summary>
        /// Путь к платформе 1С x64
        /// </summary>
        string PlatformPathX64 { get; }
    }
}
