﻿namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Результат проведения АО.
    /// </summary>
    public interface IAutoApdateSupportResultDto
    {
        /// <summary>
        /// Версия информационной базы до обновления.
        /// </summary>
        string OldVersion { get; }

        /// <summary>
        /// Версия информационной после обновления.
        /// </summary>
        string NewVersion { get; }
    }
}