﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using System;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Информация о базе
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public interface IAccountDatabasePropertiesDto
    {
        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Id аккаунта
        /// </summary>
        Guid AccountID { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата последней активности
        /// </summary>
        DateTime LastActivityDate { get; set; }

        /// <summary>
        /// Название базы пользовательское
        /// </summary>
        string Caption { get; set; }

        /// <summary>
        /// Конфигурация
        /// </summary>
        string ConfigurationName { get; set; }
        
        /// <summary>
        /// Версия конфигурация
        /// </summary>
        string ConfigurationVersion { get; set; }

        /// <summary>
        /// Платформа
        /// </summary>
        string ApplicationName { get; set; }

        /// <summary>
        /// Стабильная или альфа версия
        /// </summary>
        string DistributionType { get; set; }

        /// <summary>
        /// Версия стабильной платформы конфигурации.
        /// </summary>
        public string PlatformVersionStable { get; set; }

        /// <summary>
        /// Версия альфа платформы конфигурации.
        /// </summary>
        public string PlatformVersionAlpha { get; set; }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// Домен
        /// </summary>
        Guid? AccountDomainID { get; set; }

        /// <summary>
        /// Путь на веб публикацию
        /// </summary>
        string WebPublishPath { get; set; }
       
        /// <summary>
        /// Является ли база опубликованной
        /// </summary>
        bool IsPublished { get; set; }

        /// <summary>
        /// Статус публикации
        /// </summary>
        PublishState PublishState { get; set; }
        /// <summary>
        /// Название базы
        /// </summary>
        string DbName { get; set; }
        /// <summary>
        /// Номер базы
        /// </summary>
        int DbNumber { get; set; }
       
        /// <summary>
        /// Состояние базы
        /// </summary>
        string State { get; set; }

        /// <summary>
        /// Название SQL базы
        /// </summary>
        string SqlName { get; set; }
        
        /// <summary>
        /// Название сервера
        /// </summary>
        string ServerName { get; set; }
        
        /// <summary>
        /// Состояние блокировки базы
        /// </summary>
        string LockedState { get; set; }
       
        /// <summary>
        /// Является ли база файловой
        /// </summary>
        bool IsFile { get; set; }

        /// <summary>
        /// Путь к файловой базе
        /// </summary>
        string FilePath { get; set; }
        
        /// <summary>
        /// Дата бекапа
        /// </summary>
        DateTime? BackupDate { get; set; }
        
        /// <summary>
        /// Путь к архиву
        /// </summary>
        string ArchivePath { get; set; }
        
        /// <summary>
        /// Размер базы в мегабайтах
        /// </summary>
        int SizeInMb { get; set; }
        
        /// <summary>
        /// ID к шаблону
        /// </summary>
        Guid? TemplateId { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        string ServiceName { get; set; }
       
       
        [Obsolete("Не где не используется, в дальнейшем нужно будет убрать")]
        string LinkUser { get; set; }

// ReSharper disable once InconsistentNaming
        /// <summary>
        /// ID базы
        /// </summary>
        Guid ID { get; set; }

        /// <summary>
        /// Тип запуска
        /// </summary>
        int LaunchType { get; set; }

// ReSharper disable once InconsistentNaming
        string RolesJHO { get; set; }

        /// <summary>
        /// Ссылка на сервис
        /// </summary>
        string LinkServicePath { get; set; }
        
        /// <summary>
        /// Название аккаунта
        /// </summary>
        string AccountCaption { get; set; }
       
        /// <summary>
        /// Истёк ли демо период
        /// </summary>
        bool IsDemoExpired { get; set; }

        /// <summary>
        /// Заблокирован ли доступ
        /// </summary>
        bool IsLock { get; set; }

        /// <summary>
        /// Параметры запуска 1С
        /// </summary>
        string LaunchParameters { get; set; }
        
        /// <summary>
        /// Данные о разделителе инфо базы.
        /// </summary>
        AccountDatabaseDelimiterDto DelimiterInfo { get; set; }

        /// <summary>
        /// Под какой разрядностью будет запускаться 1С
        /// </summary>
        ApplicationBitDepthEnum Application1CBitDepth { get; set; }
    }
}
