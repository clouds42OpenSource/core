﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using System.Collections.Generic;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    // ReSharper disable once InconsistentNaming
    public interface IAccountDatabaseListDto
    {
        List<AccountDatabasePropertiesDto> AccountDatabaseList { get; set; }
    }
}