﻿using System;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{
    /// <summary>
    /// Модель загружаемых данных
    /// </summary>
    public interface IUploadDataDto
    {
        /// <summary>
        /// Идентификатор загруженного файла.
        /// </summary>
        Guid? UploadedFileId { get; set; }
    }
}
