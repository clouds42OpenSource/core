﻿using System;
using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.DataContracts.AccountDatabase.Interface
{

    /// <summary>
    /// Результат проведения процесса поддержки.
    /// </summary>
    public interface ISupportResultDto
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        Guid AccountId { get; }

        /// <summary>
        /// Номер информационной базы.
        /// </summary>
        string V82Name { get; }

        /// <summary>
        /// ID инф. базы
        /// </summary>
        Guid AccountDatabasesID { get; }

        /// <summary>
        /// Состояние процесса
        /// </summary>
        AccountDatabaseSupportProcessorState State { get;}

        /// <summary>
        /// Сообщение
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Детальная информация.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Название инф. базы.
        /// </summary>
        string Caption { get; }        

        /// <summary>
        /// Путь к бекапу.
        /// </summary>
        string BackupPath { get; }

    }
}