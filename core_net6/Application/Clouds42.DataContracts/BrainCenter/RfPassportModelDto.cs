﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BrainCenter
{
    internal static class Constants
    {
        public const string FormNamespace = "http://www.abbyy.com/FlexiCapture/Schemas/Export/FormData.xsd";
        public const string PassNamespace = "http://www.abbyy.com/FlexiCapture/Schemas/Export/PASS.xsd";
    }

    [Serializable]
    [DataContract(Name = "Documents", Namespace = Constants.FormNamespace)]
    [XmlRoot("Documents", Namespace = Constants.FormNamespace)]
    public class Template
    {
        [XmlElement(ElementName = "_PASS", Namespace = Constants.PassNamespace)]
        [DataMember(Name = "_PASS")]
        public PassportTemplate PassportTemplate { set; get; } = new();
    }

    [Serializable]
    [DataContract(Name = "_PASS", Namespace = Constants.PassNamespace)]
    [XmlRoot("_PASS", Namespace = Constants.PassNamespace)]
    public class PassportTemplate
    {
        [XmlElement(ElementName = "_Паспорт_РФ", Namespace = "")]
        [DataMember(Name = "_Паспорт_РФ")]
        public RfPassportTemplate RfPassportTemplate { set; get; } = new();
    }

    [Serializable]
    [DataContract(Name = "_Паспорт_РФ", Namespace = "")]
    [XmlRoot("_Паспорт_РФ", Namespace = "")]
    public class RfPassportTemplate
    {
        [XmlElement(ElementName = nameof(_PP_Kem))]
        [DataMember(Name = nameof(_PP_Kem))]
        public string _PP_Kem { set; get; }

        private string _pP_Date;

        [XmlElement(ElementName = nameof(_PP_Date))]
        [DataMember(Name = nameof(_PP_Date))]
        public string _PP_Date
        {
            set
            {
                try
                {
                    _pP_Date = DateTime.Parse(value).ToString("d", CultureInfo.CreateSpecificCulture("ru-RU"));
                }
                catch (Exception)
                {
                    _pP_Date = value;
                }                
            }
            get { return _pP_Date; }
        }

        [XmlElement(ElementName = nameof(_PP_Podr))]
        [DataMember(Name = nameof(_PP_Podr))]

        public string _PP_Podr { set; get; }

        [XmlElement(ElementName = nameof(_PP_SurName))]
        [DataMember(Name = nameof(_PP_SurName))]

        public string _PP_SurName { set; get; }

        [XmlElement(ElementName = nameof(_PP_Name))]
        [DataMember(Name = nameof(_PP_Name))]
        public string _PP_Name { set; get; }

        [XmlElement(ElementName = nameof(_PP_SecName))]
        [DataMember(Name = nameof(_PP_SecName))]
        public string _PP_SecName { set; get; }

        private string _pP_BirthDate;

        [XmlElement(ElementName = nameof(_PP_BirthDate))]
        [DataMember(Name = nameof(_PP_BirthDate))]
        public string _PP_BirthDate
        {
            set
            {
                try
                {
                    _pP_BirthDate = DateTime.Parse(value).ToString("d", CultureInfo.CreateSpecificCulture("ru-RU"));
                }
                catch (Exception)
                {
                    _pP_BirthDate = value;
                }                
            }
            get { return _pP_BirthDate; }
        }

        [XmlElement(ElementName = nameof(_PP_Sex))]
        [DataMember(Name = nameof(_PP_Sex))]
        public string _PP_Sex { set; get; }

        [XmlElement(ElementName = nameof(_PP_BirthPlace))]
        [DataMember(Name = nameof(_PP_BirthPlace))]
        public string _PP_BirthPlace { set; get; }

        [XmlElement(ElementName = nameof(_PP_Ser2))]
        [DataMember(Name = nameof(_PP_Ser2))]
        public string _PP_Ser2 { set; get; }

        [XmlElement(ElementName = nameof(_PP_Num2))]
        [DataMember(Name = nameof(_PP_Num2))]
        public string _PP_Num2 { set; get; }

        private string _pP_Ser;

        [XmlElement(ElementName = nameof(_PP_Ser))]
        [DataMember(Name = nameof(_PP_Ser))]
        public string _PP_Ser
        {
            set
            {
                try
                {
                    _pP_Ser = value;
                    if (_pP_Ser != "")
                        _PP_Ser2 = _pP_Ser;
                    else
                        _pP_Ser = _PP_Ser2;
                }
                catch (Exception)
                {
                    _pP_Ser = value;
                }
            }
            get { return _pP_Ser; }
        }

        private string _pP_Num;

        [XmlElement(ElementName = nameof(_PP_Num))]
        [DataMember(Name = nameof(_PP_Num))]
        public string _PP_Num
        {
            set
            {
                try
                {
                    _pP_Num = value;
                    if (_pP_Num != "")
                        _PP_Num2 = _pP_Num;
                    else
                        _pP_Num = _PP_Num2;
                }
                catch (Exception)
                {
                    _pP_Num = value;
                }
            }
            get { return _pP_Num; }
        }
    }
}