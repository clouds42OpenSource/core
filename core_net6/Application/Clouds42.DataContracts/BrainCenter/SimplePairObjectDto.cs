using System.Runtime.Serialization;
using System.Xml.Serialization;
using System;

namespace Clouds42.DataContracts.BrainCenter
{
    [Serializable]
    [XmlRoot(ElementName = "SimplePairObject")]
    [DataContract(Name = "SimplePairObject")]
    public class SimplePairObjectDto
    {
        [XmlElement(ElementName = nameof(Key))]
        [DataMember(Name = nameof(Key))]
        public string Key { set; get; }

        [XmlElement(ElementName = nameof(Value))]
        [DataMember(Name = nameof(Value))]
        public string Value { set; get; }
    }
}