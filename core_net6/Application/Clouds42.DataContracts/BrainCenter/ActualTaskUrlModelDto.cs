﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BrainCenter
{
    [Serializable]
    [XmlRoot("Result")]
    [DataContract(Name = "Result")]
    public class ActualTaskUrlModelDto
    {
        [XmlElement(ElementName = nameof(ActualTaskUrl))]
        [DataMember(Name = nameof(ActualTaskUrl))]
        public string ActualTaskUrl { set; get; }       
    }
}
