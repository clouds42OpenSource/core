using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.BrainCenter
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "SimpleResult")]
    [DataContract(Name = "SimpleResult")]
    public class SimpleResultDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(BoolValue))]
        [DataMember(Name = nameof(BoolValue))]
        public bool BoolValue { get; set; }
    }
}