﻿namespace Clouds42.DataContracts.BrainCenter
{
    public class PartialTransportModelDto
    {
        public int Index { set; get; }
        public object ViewModel { set; get; }
    }
}