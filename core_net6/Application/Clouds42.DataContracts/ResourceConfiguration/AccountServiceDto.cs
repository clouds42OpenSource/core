﻿using System;
using System.Collections.Generic;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// Информация о сервисе аккаунта
    /// </summary>
    public class AccountServiceDto
    {
        /// <summary>
        /// Нужна ли оплата за главный сервис, такой как аренда 1С,...
        /// </summary>
        public bool IsMainServiceNeedPayment { get; set; }

        /// <summary>
        /// Активен ли сервис
        /// </summary>
        public bool IsActive { get; set; }
        
        /// <summary>
        /// Отключён ли сервис
        /// </summary>
        public bool IsDisabled { get; set; }
        
        /// <summary>
        /// Дата завершения оплаченного периода за сервис
        /// </summary>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Информация о демо
        /// </summary>
        public ServiceDemoInfoDto Demo { get; set; } = new();

        /// <summary>
        /// Список включённых опций 
        /// </summary>
        public List<ServiceOptionInfoDto> Options { get; set; } = [];
    }

}
