﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary></summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CSResourceNameDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(ResourceName))]
        [DataMember(Name = nameof(ResourceName))]
        public string ResourceName { get; set; }

    }
}