﻿using System;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// Информация о демо сервиса
    /// </summary>
    public class ServiceDemoInfoDto
    {
        /// <summary>
        /// Включён ли демо период
        /// </summary>
        public bool IsEnabled { get; set; }
        
        /// <summary>
        /// Дата завершения демо периода
        /// </summary>
        public DateTime ExpiresOn { get; set; }

        /// <summary>
        /// Истёк ли срок демо периода
        /// </summary>
        public bool IsExpired { get; set; }
    }

}
