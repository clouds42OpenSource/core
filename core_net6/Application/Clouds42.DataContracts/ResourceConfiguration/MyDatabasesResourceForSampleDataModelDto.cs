﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// Модель данных ресурса "Мои инф. базы" для выборки
    /// </summary>
    public class MyDatabasesResourceForSampleDataModelDto
    {
        /// <summary>
        /// Ресурс услуги
        /// </summary>
        public Resource Resource { get; set; }

        /// <summary>
        /// Услуга
        /// </summary>
        public BillingServiceType ServiceType { get; set; }

        /// <summary>
        /// Ресурс услуги сервиса "Мои инф. базы"(обертка над обычным ресурсом)
        /// </summary>
        public MyDatabasesResource MyDatabasesResource { get; set; }
    }
}
