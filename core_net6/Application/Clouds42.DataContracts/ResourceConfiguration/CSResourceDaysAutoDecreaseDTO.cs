﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CSResourceDaysAutoDecreaseDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = nameof(DaysAutoDecrease))]
        [DataMember(Name = nameof(DaysAutoDecrease))]
        public string DaysAutoDecrease { get; set; }
        public CSResourceDaysAutoDecreaseDto()
        {
        }

        public CSResourceDaysAutoDecreaseDto(bool? daysAutoDecrease)
        {
            DaysAutoDecrease = daysAutoDecrease.HasValue ? daysAutoDecrease.ToString() : "Not set";
        }
    }
}