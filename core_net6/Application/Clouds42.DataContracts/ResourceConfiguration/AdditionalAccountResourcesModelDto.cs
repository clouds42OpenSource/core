﻿namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// Модель дополнительных ресурсов аккаунта
    /// </summary>
    public class AdditionalAccountResourcesModelDto
    {
        /// <summary>
        /// Название дополнительных ресурсов
        /// </summary>
        public string AdditionalResourcesName { get; set; }

        /// <summary>
        /// Стоимость дополнительных ресурсов
        /// </summary>
        public decimal AdditionalResourcesCost { get; set; }

        /// <summary>
        /// Необходимо показывать дополнительные ресурсы
        /// </summary>
        public bool NeedShowAdditionalResources { get; set; }
    }
}
