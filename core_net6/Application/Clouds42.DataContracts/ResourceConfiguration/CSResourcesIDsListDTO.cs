﻿using Clouds42.DataContracts.BaseModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CSResourcesIDsListDto
    {
        /// <summary></summary>
        [XmlElement(ElementName = nameof(CSResourcesIDs))]
        [DataMember(Name = nameof(CSResourcesIDs))]
        public GuidListItemDto CSResourcesIDs { get; set; }

        /// <summary></summary>
        public CSResourcesIDsListDto()
        {
            CSResourcesIDs = new GuidListItemDto { type = "List" };
        }
    }
}
