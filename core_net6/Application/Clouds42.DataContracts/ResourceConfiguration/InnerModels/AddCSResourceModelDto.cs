﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class AddCSResourceModelDto
    {
        [XmlElement(ElementName = "CloudServiceID")]
        [DataMember(Name = "CloudServiceID")]
        public string CloudServiceId { get; set; }

        [XmlElement(ElementName = nameof(ResourcesName))]
        [DataMember(Name = nameof(ResourcesName))]
        public string ResourcesName { get; set; }
    }
}