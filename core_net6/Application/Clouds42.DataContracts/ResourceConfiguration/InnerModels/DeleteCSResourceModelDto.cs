﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class DeleteCSResourceModelDto
    {
        [XmlElement(ElementName = "CSResourceID")]
        [DataMember(Name = "CSResourceID")]
        public Guid? CSResourceId { get; set; }
    }
}