﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetCSResourceNameModelDto
    {
        [XmlElement(ElementName = nameof(CSResourceID))]
        [DataMember(Name = nameof(CSResourceID))]
        public Guid? CSResourceID { get; set; }

        [XmlElement(ElementName = nameof(ResourcesName))]
        [DataMember(Name = nameof(ResourcesName))]
        public string ResourcesName { get; set; }
    }
}