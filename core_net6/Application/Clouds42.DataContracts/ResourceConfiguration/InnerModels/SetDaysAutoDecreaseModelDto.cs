﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration.InnerModels
{
    [XmlRoot(ElementName = "Request")]
    [DataContract(Name = "Request")]
    public class SetDaysAutoDecreaseModelDto
    {
        [XmlElement(ElementName = nameof(CSResourceID))]
        [DataMember(Name = nameof(CSResourceID))]
        public Guid? CSResourceID { get; set; }

        [XmlElement(ElementName = nameof(DaysAutoDecrease))]
        [DataMember(Name = nameof(DaysAutoDecrease))]
        public bool? DaysAutoDecrease { get; set; }
    }
}