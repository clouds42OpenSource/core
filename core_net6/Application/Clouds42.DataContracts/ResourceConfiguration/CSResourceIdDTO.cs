﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class CSResourceIdDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "CSResourceID")]
        [DataMember(Name = "CSResourceID")]
        public Guid CSResourceId { get; set; }

    }
}