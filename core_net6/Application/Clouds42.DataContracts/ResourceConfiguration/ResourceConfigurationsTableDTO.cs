﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Clouds42.DataContracts.ResourceConfiguration
{
    [XmlRoot(ElementName = "Result")]
    [DataContract(Name = "Result")]
    public class ResourceConfigurationsTableDto
    {
        [XmlElement(ElementName = nameof(ResourceConfigurationsTable))]
        [DataMember(Name = nameof(ResourceConfigurationsTable))]
        public ResourceConfigurationsTable ResourceConfigurationsTable { get; set; } = new();
    }

    public class ResourceConfigurationsTable
    {
        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute(AttributeName = nameof(Type))] [IgnoreDataMember]
        public string Type = "Table";

        [XmlElement(ElementName = "Row")]
        [DataMember(Name = "Row")]
        public List<ResourceConfigurationsTableRow> Rows { get; set; } = [];
    }

    public class ResourceConfigurationsTableRow
    {
        [XmlElement(ElementName = nameof(AccountId))]
        [DataMember(Name = nameof(AccountId))]
        public Guid AccountId { get; set; }

        [XmlElement(ElementName = nameof(ExpireDate))]
        [DataMember(Name = nameof(ExpireDate))]
        public DateTime ExpireDate { get; set; }


    }
}
