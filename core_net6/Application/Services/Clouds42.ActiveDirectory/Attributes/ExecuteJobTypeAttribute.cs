﻿namespace Clouds42.ActiveDirectory.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ExecuteJobTypeAttribute(Type jobType) : Attribute
    {
        public Type JobType { get; } = jobType;
    }
}