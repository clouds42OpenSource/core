﻿using System.Linq.Expressions;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.TaskProviders;

namespace Clouds42.ActiveDirectory
{
    /// <summary>
    /// Обработчик задач по управлению контроллером домена.
    /// </summary>
    public class ActiveDirectoryTaskProcessor(AdProviderFactory adProviderFactory) : IActiveDirectoryTaskProcessor
    {
        /// <summary>
        /// Попытаться выполнить таск немедленно.
        /// Пимечание: если контекс выполнения "воркер" и "воркер" может выполнять требуюмую задачу - выполнение произойдет немделенно.
        /// Если выполнить незамедлительно нет возможности, но есть рабочие воркеры - запускается таска и ожидаем результат выполения.
        /// Если нет рабочего воркера - таска ставится в очаредь на выполнение.
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        public void TryDoImmediately(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            var methodCallExp = (MethodCallExpression)actionExpression.Body;
            var action = actionExpression.Compile();
            action(adProviderFactory.CreateProvider(methodCallExp.Method.Name));
        }

        /// <summary>
        /// Запустить таску.
        /// </summary>
        /// <param name="actionExpression">Выражение выполняемого действие.</param>
        public void RunTask(Expression<Action<IActiveDirectoryProvider>> actionExpression)
        {
            var action = actionExpression.Compile();
            action(adProviderFactory.CreateTaskProvider());
        }

    }
}