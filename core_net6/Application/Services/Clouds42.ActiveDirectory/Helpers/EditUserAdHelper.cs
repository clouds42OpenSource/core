﻿using Clouds42.ActiveDirectory.Contracts.Extensions;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.Helpers
{
    public static class EditUserAdHelper
    {
        public static void EditUser(string userName, IPrincipalUserAdapterDto userModel)
        {

            if (userName != userModel.GetUserName())
            {
                ActiveDirectoryUserFunctions.ChangeLogin(userName, userModel.GetUserName(), userModel.Email);
            }

            if (!string.IsNullOrEmpty(userModel.Password))
            {
                ActiveDirectoryUserFunctions.ChangePassword(userModel.GetUserName(), userModel.Password);
            }

            ActiveDirectoryUserFunctions.ChangeName(userModel.GetUserName(), userModel.FirstName);
            ActiveDirectoryUserFunctions.ChangeMiddleName(userModel.GetUserName(), userModel.MiddleName);
            ActiveDirectoryUserFunctions.ChangeLastName(userModel.GetUserName(), userModel.LastName);
            ActiveDirectoryUserFunctions.ChangePhoneNumber(userModel.GetUserName(), PhoneHelper.ClearNonDigits(userModel.PhoneNumber));
            ActiveDirectoryUserFunctions.ChangeDisplayName(userModel.GetUserName(), userModel.GetDisplayName());

        }

    }
}
