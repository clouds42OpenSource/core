﻿using System.Configuration;
using System.Reflection;
using Clouds42.ActiveDirectory.Attributes;
using Clouds42.ActiveDirectory.TaskProviders;

namespace Clouds42.ActiveDirectory.Helpers
{
    internal static class ActiveDirectoryTaskHelper
    {

        public static Type GetJobTypeByMethodName(string methodName)
        {
            var propertyInfo = typeof(ActiveDirectoryTaskProvider).GetMembers(BindingFlags.Public | BindingFlags.Instance)
                                   .FirstOrDefault(p => p.Name == methodName) ??
                               throw new InvalidOperationException(
                                   $"У типа '{nameof(ActiveDirectoryTaskProvider)}' не удалось прочитать информацию о методе '{methodName}'");

            var executeJobTypeAttribute =
                (ExecuteJobTypeAttribute)propertyInfo.GetCustomAttributes(typeof(ExecuteJobTypeAttribute), false)
                    .FirstOrDefault()! ??
                throw new ConfigurationErrorsException(
                    $"У метода '{nameof(ActiveDirectoryTaskProvider)} {methodName}' не указан обязательный аттрибут '{nameof(ExecuteJobTypeAttribute)}'");

            return executeJobTypeAttribute.JobType;
        }
    }
}
