﻿using Clouds42.ActiveDirectory.Contracts;

namespace Clouds42.ActiveDirectory.Helpers
{
    /// <summary>
    /// Синхронизатор регистрации аккаунта
    /// </summary>
    internal class RegisterAccountSynchronize : IRegisterAccountSynchronize
    {
        /// <summary>
        /// Синхронизировать регистрацию
        /// </summary>
        public void Synchronize()
        {
            Task.Delay(TimeSpan.FromSeconds(30)).Wait();
        }
    }
}
