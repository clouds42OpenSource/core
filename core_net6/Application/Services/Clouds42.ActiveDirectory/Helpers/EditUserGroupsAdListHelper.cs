﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.DataContracts.ActiveDirectory;

namespace Clouds42.ActiveDirectory.Helpers
{
    public class EditUserGroupsAdListHelper
    {

        public void EditList(EditUserGroupsAdListModel model)
        {
            using var ctx = ContextActivatorFactory.Create().OpenRootUO();
            foreach (var item in model.EditUserGroupsModel.SelectMany(s=>s.Items).OrderBy(o => o.Operation))
            {
                try
                {
                    if (item.Operation == EditUserGroupsAdModelDto.OperationEnum.Include)
                    {
                        ActiveDirectoryCompanyFunctions.AddToGroupIfNoExist(ctx, item.UserName, item.GroupName);
                    }
                    else
                    {
                        ActiveDirectoryCompanyFunctions.RemoveFromGroupIfExist(ctx, item.UserName, item.GroupName);
                    }
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
