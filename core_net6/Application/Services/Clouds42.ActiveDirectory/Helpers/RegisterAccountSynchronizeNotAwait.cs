﻿using Clouds42.ActiveDirectory.Contracts;

namespace Clouds42.ActiveDirectory.Helpers
{
    /// <summary>
    /// Синхронизатор регистрации аккаунта без ожидания
    /// </summary>
    public class RegisterAccountSynchronizeNotAwait : IRegisterAccountSynchronize
    {
        /// <summary>
        /// Синхронизировать регистрацию
        /// </summary>
        public void Synchronize()
        {
            // имплементация для тестов
        }
    }
}
