﻿using Clouds42.Accounts.Account.Helpers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.CreateAccount
{
    /// <summary>
    /// Хэлпер регистрации аккаунта в ActiveDirectory
    /// </summary>
    internal class RegisterAccountAdHelper(
        AccountSegmentHelper accountSegmentHelper,
        AccountDataProvider accountDataProvider,
        IRegisterAccountSynchronize registerAccountSynchronize)
        : IRegisterAccountAdHelper
    {
        /// <summary>
        /// Зарегистрировать аккаунт
        /// </summary>
        /// <param name="model">Модель создания нового аккаунта</param>
        public void RegisterAccount(ICreateNewAccountModel model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);

            var fullClientFileStoragePath = accountSegmentHelper.GetFullClientFileStoragePath(account.Id);
            var companyStorage = accountSegmentHelper.GetAccountFileStoragePath(account.Id);

            var groupName = DomainCoreGroups.CompanyGroupBuild(account.IndexNumber);
            var webGroupName = DomainCoreGroups.WebCompanyGroupBuild(account.IndexNumber);

            using (var ctx = ContextActivatorFactory.Create().OpenClientsUO())
            {
                ActiveDirectoryUserFunctions.CreateUser(ctx, model);

                using (var ctxGrp = ContextActivatorFactory.Create().OpenCompanyUO())
                {
                    ActiveDirectoryCompanyFunctions.CreateGroups(ctxGrp, groupName, webGroupName);
                    CreateFolder(companyStorage);
                    ActiveDirectoryFolderFunctions.SetDirectoryAcl(ctxGrp, groupName, companyStorage);
                    CreateFolder(fullClientFileStoragePath);
                }

                ActiveDirectoryCompanyFunctions.AddToGroups(ctx, model.GetUserName(), DomainCoreGroups.ClientsMk, groupName);
            }

            ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(model.ClientFilesPath, "R:", model.GetUserName());
            registerAccountSynchronize.Synchronize();
        }

        /// <summary>
        /// Создать директорию.
        /// </summary>
        /// <param name="path">Путь директории.</param>
        private void CreateFolder(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Проверяет существует ли логин в Active Directory
        /// </summary>
        /// <param name="login">Логин для проверки существования</param>
        /// <returns>Возвращает <c>true</c> если логин существует, иначе <c>false</c></returns>
        public bool IsLoginInActiveDirectoryExist(string login) 
            => ActiveDirectoryUserFunctions.UsersExistsInDomain(login);
    }
}
