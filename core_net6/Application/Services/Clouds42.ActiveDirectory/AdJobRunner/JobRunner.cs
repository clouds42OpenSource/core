﻿using Clouds42.CoreWorker.BaseJobs;
using Clouds42.CoreWorker.JobWrappersBase;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.ActiveDirectory.AdJobRunner
{
    internal class JobRunner(IServiceProvider serviceProvider)
    {
        public TJobWrapper Run<TJobWrapper, TJob, TParams>(TParams parameters)
            where TJobWrapper : TypingParamsJobWrapperBase<TJob, TParams> where TJob : CoreWorkerParamsJob<TParams> where TParams : class
        {
            var wrapperInstance = serviceProvider.GetRequiredService<TJobWrapper>();
            wrapperInstance.Start(parameters);
            return wrapperInstance;
        }
    }
}
