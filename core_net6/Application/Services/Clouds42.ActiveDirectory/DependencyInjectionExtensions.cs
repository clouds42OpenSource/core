﻿using Clouds42.ActiveDirectory.AdJobRunner;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.CreateAccount;
using Clouds42.ActiveDirectory.CreateUser;
using Clouds42.ActiveDirectory.Helpers;
using Clouds42.ActiveDirectory.TaskProviders;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.ActiveDirectory
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddActiveDirectory(this IServiceCollection services)
        {
            services.AddTransient<IRegisterAccountAdHelper, RegisterAccountAdHelper>();
            services.AddTransient<IRegisterUserInAccountAdHelper, RegisterUserInAccountAdHelper>();
            services.AddTransient<IActiveDirectoryTaskProcessor, ActiveDirectoryTaskProcessor>();
            services.AddTransient<IRegisterAccountSynchronize, RegisterAccountSynchronize>();
            services.AddTransient<IActiveDirectoryProvider, ActiveDirectoryProvider>();
            services.AddTransient<JobRunner>();
            services.AddTransient<ActiveDirectoryTaskProvider>();
            services.AddTransient<ActiveDirectoryAwaitTaskProvider>();
            services.AddTransient<EditUserGroupsAdListHelper>();
            services.AddTransient<AdProviderFactory>();
            return services;
        }
    }
}
