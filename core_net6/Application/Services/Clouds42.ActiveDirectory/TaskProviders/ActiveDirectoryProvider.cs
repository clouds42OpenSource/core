﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Helpers;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.TaskProviders
{

    /// <summary>
    /// Провайдер контроллера домена.
    /// </summary>
    public class ActiveDirectoryProvider(
        IRegisterAccountAdHelper createAccountInAdHelper,
        IRegisterUserInAccountAdHelper registerUserInAccountAdHelper,
        EditUserGroupsAdListHelper editUserGroupsAdListHelper)
        : IActiveDirectoryProvider
    {
        public void RegisterNewAccount(ICreateNewAccountModel model) 
            => createAccountInAdHelper.RegisterAccount(model);

        public void RegisterNewUser(ICreateNewUserModel userModel) 
            => registerUserInAccountAdHelper.RegisterUser(userModel);

        public virtual void DeleteUser(Guid accountUserId, string username) 
            => ActiveDirectoryUserFunctions.DeleteUser(username);
        
        /// <summary>
        /// Сменить логин
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя.</param>
        /// <param name="username">Логин пользователя.</param>
        /// <param name="newLogin">Новый логин.</param>
        public virtual void ChangeLogin(Guid accountUserId, string username, string newLogin, string email) 
            => ActiveDirectoryUserFunctions.ChangeLogin(username, newLogin, email);

        /// <inheritdoc />
        /// <summary>
        /// Сменить пароль
        /// </summary>
        /// <param name="username">логин пользователя</param>
        /// <param name="accountUserId">Идентификатор пользователя.</param>
        /// <param name="newPassword">пароль</param>
        public virtual void ChangePassword(Guid accountUserId, string username, string newPassword) 
            => ActiveDirectoryUserFunctions.ChangePassword(username, newPassword);

        public virtual void EditUser(Guid accountUserId, string userName, IPrincipalUserAdapterDto userModel) 
            => EditUserAdHelper.EditUser(userName, userModel);

        public void EditUserGroupsAdList(EditUserGroupsAdListModel model)
            => editUserGroupsAdListHelper.EditList(model);

        /// <summary>
        /// Установка прав на папку.
        /// </summary>
        /// <param name="groupName">Имя группы</param>
        /// <param name="strPath">Путь до папки</param>
        public virtual void SetDirectoryAcl(string groupName, string strPath) 
            => ActiveDirectoryFolderFunctions.SetDirectoryAcl(groupName, strPath);

        /// <summary>
        /// Уставновка прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>        
        public virtual void SetDirectoryAclForUser(string userName, string strPath) 
            => ActiveDirectoryFolderFunctions.SetDirectoryAclForUser(userName, strPath);

        /// <summary>
        /// Удаление прав на папку для пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="strPath">Путь до папки</param>
        public virtual void RemoveDirectoryAclForUser(string userName, string strPath) 
            => ActiveDirectoryFolderFunctions.RemoveDirectoryAclForUser(userName, strPath);
    }

}
