﻿using Clouds42.ActiveDirectory.AdJobRunner;
using Clouds42.ActiveDirectory.Attributes;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangeLogin;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.ChangePassword;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.DeleteUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.EditUserGroup;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewAccount;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RegisterNewUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.RemoveDirectoryAclForUser;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAcl;
using Clouds42.CoreWorker.ActiveDirectory.Jobs.SetDirectoryAclForUser;
using Clouds42.CoreWorker.JobWrappersBase;
using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.ActiveDirectory.Interface;

namespace Clouds42.ActiveDirectory.TaskProviders
{

    internal class ActiveDirectoryTaskProvider(JobRunner jobRunner) : IActiveDirectoryProvider
    {
        protected event TaskHandler ExecuteEvent;
        protected delegate void TaskHandler(JobWrapperBase processWrapper);

        [ExecuteJobType(typeof(RegisterNewAccountInAdJob))]
        public void RegisterNewAccount(ICreateNewAccountModel model)
        {
            RiseEvent(jobRunner
                .Run<RegisterNewAccountInAdJobWrapper, RegisterNewAccountInAdJob, CreateNewAccountInAdParamsModel>(
                    new CreateNewAccountInAdParamsModel
                    {
                        AccountUserId = model.GetAccountUserId(),
                        Login = model.GetUserName(),
                        Password = model.Password,
                        AccountId = model.AccountId,
                        PhoneNumber = model.PhoneNumber,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        MiddleName = model.MiddleName,
                        Email = model.Email,
                        ClientFilesPath = model.ClientFilesPath
                    }));
        }

        [ExecuteJobType(typeof(RegisterNewUserInAdJob))]
        public void RegisterNewUser(ICreateNewUserModel userModel)
            => RiseEvent(jobRunner
                .Run<RegisterNewUserInAdJobWrapper, RegisterNewUserInAdJob, RegisterNewUserInAdParamsModel>(
                    new RegisterNewUserInAdParamsModel
                    {
                        AccountUserId = userModel.GetAccountUserId(),
                        Login = userModel.GetUserName(),
                        Password = userModel.Password,
                        PhoneNumber = userModel.PhoneNumber,
                        FirstName = userModel.FirstName,
                        LastName = userModel.LastName,
                        MiddleName = userModel.MiddleName,
                        AccountId = userModel.AccountId,
                        Email = userModel.Email,
                        ClientFilesPath = userModel.ClientFilesPath
                    }));

        [ExecuteJobType(typeof(DeleteUserInAdJob))]
        public void DeleteUser(Guid accountUserId, string username)
            => RiseEvent(jobRunner.Run<DeleteUserInAdJobWrapper, DeleteUserInAdJob, DeleteUserInAdParamsModel>(
                new DeleteUserInAdParamsModel
                {
                    AccountUserId = accountUserId,
                    UserName = username
                }));

        [ExecuteJobType(typeof(ChangeUserLoginInAdJob))]
        public void ChangeLogin(Guid accountUserId, string username, string newLogin, string email)
        {
            RiseEvent(jobRunner.Run<ChangeUserLoginInAdJobWrapper, ChangeUserLoginInAdJob, ChangeUserLoginInAdParamsModel>(
                new ChangeUserLoginInAdParamsModel
                {
                    AccountUserId = accountUserId,
                    UserName = username,
                    NewLogin = newLogin,
                    Email = email
                }));
        }

        [ExecuteJobType(typeof(ChangeUserPasswordInAdJob))]
        public void ChangePassword(Guid accountUserId, string username, string newPassword)
        {
            RiseEvent(jobRunner
                .Run<ChangeUserPasswordInAdJobWrapper, ChangeUserPasswordInAdJob, ChangeUserPasswordInAdParamsModel>(
                    new ChangeUserPasswordInAdParamsModel
                    {
                        AccountUserId = accountUserId,
                        UserName = username,
                        NewPassword = newPassword
                    }));
        }

        [ExecuteJobType(typeof(EditUserInAdJob))]
        public void EditUser(Guid accountUserId, string userName, IPrincipalUserAdapterDto userModel)
            => RiseEvent(jobRunner.Run<EditUserInAdJobWrapper, EditUserInAdJob, EditUserInAdParamsModel>(new EditUserInAdParamsModel
            {
                OldLogin = userName,
                AccountUserId = accountUserId,
                UserName = userModel.GetUserName(),
                Password = userModel.Password,
                PhoneNumber = userModel.PhoneNumber,
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                MiddleName = userModel.MiddleName
            }));

        [ExecuteJobType(typeof(EditUserGroupsInAdListInAdJob))]
        public void EditUserGroupsAdList(EditUserGroupsAdListModel model)
            => RiseEvent(jobRunner.Run<EditUserGroupsInAdWrapper, EditUserGroupsInAdListInAdJob, EditUserGroupsAdListModel>(model));

        [ExecuteJobType(typeof(SetDirectoryAclInAdJob))]
        public void SetDirectoryAcl(string groupName, string strPath)
            => RiseEvent(jobRunner.Run<SetDirectoryAclInAdJobWrapper, SetDirectoryAclInAdJob, SetDirectoryAclInAdParamsModel>(
                new SetDirectoryAclInAdParamsModel
                {
                    GroupName = groupName,
                    StrPath = strPath
                }));

        [ExecuteJobType(typeof(SetDirectoryAclForUserInAdJob))]
        public void SetDirectoryAclForUser(string userName, string strPath)
            => RiseEvent(jobRunner
                .Run<SetDirectoryAclForUserInAdJobWrapper, SetDirectoryAclForUserInAdJob,
                    SetDirectoryAclForUserInAdParamsModel>(
                    new SetDirectoryAclForUserInAdParamsModel
                    {
                        UserName = userName,
                        StrPath = strPath
                    }));

        [ExecuteJobType(typeof(RemoveDirectoryAclForUserInJob))]
        public void RemoveDirectoryAclForUser(string userName, string strPath)
            => RiseEvent(jobRunner
                .Run<RemoveDirectoryAclForUserInAdJobWrapper, RemoveDirectoryAclForUserInJob,
                    RemoveDirectoryAclForUserInAdParamsModel>(
                    new RemoveDirectoryAclForUserInAdParamsModel
                    {
                        UserName = userName,
                        StrPath = strPath
                    }));

        private void RiseEvent(JobWrapperBase processWrapper)
        {
            ExecuteEvent?.Invoke(processWrapper);
        }
    }
}
