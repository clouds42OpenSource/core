﻿using Clouds42.ActiveDirectory.AdJobRunner;
using Clouds42.CoreWorker.JobWrappersBase;

namespace Clouds42.ActiveDirectory.TaskProviders
{
    internal class ActiveDirectoryAwaitTaskProvider : ActiveDirectoryTaskProvider {

        public ActiveDirectoryAwaitTaskProvider(JobRunner jobRunner) : base(jobRunner)
        {
            ExecuteEvent += OnExecuteEvent;
        }

        private void OnExecuteEvent(JobWrapperBase processWrapper)
        {
            processWrapper.Wait();
        }

    }
}