﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Helpers;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.RuntimeContext;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.ActiveDirectory.TaskProviders
{
    public class AdProviderFactory(
        IServiceProvider serviceProvider,
        ICoreExecutionContext coreExecutionContext,
        IUnitOfWork dbLayer)
    {
        public IActiveDirectoryProvider CreateProvider(string methodName)
        {
            if (coreExecutionContext is ICoreWorkerExecutionContext context && CoreWorkerCanProcessTask(ActiveDirectoryTaskHelper.GetJobTypeByMethodName(methodName), context.WorkerId))
                return serviceProvider.GetRequiredService<IActiveDirectoryProvider>();

            return HasAvailableWorkers(methodName) ? serviceProvider.GetRequiredService<ActiveDirectoryAwaitTaskProvider>() : CreateTaskProvider();
        }

        public IActiveDirectoryProvider CreateTaskProvider()
            => serviceProvider.GetRequiredService<ActiveDirectoryTaskProvider>();

        private bool HasAvailableWorkers(string methodName)
        {
            var jobType = ActiveDirectoryTaskHelper.GetJobTypeByMethodName(methodName);

            return CoreWorkerCanProcessTask(jobType, null);
        }

        private bool CoreWorkerCanProcessTask(Type jobType, short? coreWorkerId)
            => dbLayer.CoreWorkerRepository.CoreWorkerCanProcessTask(jobType, coreWorkerId);

    }
}
