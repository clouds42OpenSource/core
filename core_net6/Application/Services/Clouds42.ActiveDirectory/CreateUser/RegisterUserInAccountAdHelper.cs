﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.ActiveDirectory.Interface;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.ActiveDirectory.CreateUser
{
    internal class RegisterUserInAccountAdHelper(IUnitOfWork dbLayer) : IRegisterUserInAccountAdHelper
    {
        public void RegisterUser(ICreateNewUserModel userModel)
        {

            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == userModel.AccountId) ??
                          throw new NotFoundException($"По идентификатору '{userModel.AccountId}' не найден аккаунт.");

            var groupName = DomainCoreGroups.CompanyGroupBuild(account.IndexNumber);

            ActiveDirectoryUserFunctions.CreateUser(userModel);
            ActiveDirectoryCompanyFunctions.AddToGroups(userModel.GetUserName(), DomainCoreGroups.ClientsMk, groupName);
            ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(userModel.ClientFilesPath, "R:", userModel.GetUserName());
        }

    }
}
