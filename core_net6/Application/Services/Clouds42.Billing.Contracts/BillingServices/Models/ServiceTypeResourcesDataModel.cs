﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Данные по ресурсам услуги биллинга
    /// </summary>
    public class ServiceTypeResourcesDataModel
    {
        /// <summary>
        /// Полная стоимость услуги по ресурсам
        /// </summary>
        public decimal ServiceTypeAmount { get; set; }

        /// <summary>
        /// Количество используемых лицензий
        /// </summary>
        public int UsedLicensesCount { get; set; }

        /// <summary>
        /// Количество спонсорских лицензий, принадлежащих аккаунту
        /// </summary>
        public int IamSponsorLicensesCount { get; set; }

        /// <summary>
        /// Количество спонсорских лицензий, принадлежащих спонсору
        /// </summary>
        public int MeSponsorLicensesCount { get; set; }
    }
}
