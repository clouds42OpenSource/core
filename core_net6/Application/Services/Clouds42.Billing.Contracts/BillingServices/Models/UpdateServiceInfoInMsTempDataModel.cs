﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель временных данных (выбранных из базы)
    /// для обновления информации о сервисе биллинга в МС
    /// </summary>
    public class UpdateServiceInfoInMsTempDataModel
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание сервиса
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// Id файла иконки сервиса 
        /// </summary>
        public Guid IconFileId { get; set; }

        /// <summary>
        /// Id файла инструкции сервиса 
        /// </summary>
        public Guid InstructionFileId { get; set; }

        /// <summary>
        /// Список Id файлов скриншотов сервиса
        /// </summary>
        public List<Guid> ScreenshotFileIds { get; set; } = [];

        /// <summary>
        /// Список отраслей, от которых зависит сервис
        /// </summary>
        public List<string> Industries { get; set; } = [];
    }
}
