﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Типы подключения к информационной базе 1С
    /// </summary>
    public class TypesOfConnectionToInformationDbDto
    {
        /// <summary>
        /// Id услуги Rdp подключения
        /// </summary>
        public Guid MyEntUserServiceTypeId { get; set; }

        /// <summary>
        /// Id услуги Web подключения
        /// </summary>
        public Guid MyEntUserWebServiceTypeId { get; set; }
    }
}
