﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Клиент партнера
    /// </summary>
    public class PartnerClientDto
    {
        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Номер аккаунта партнера.
        /// </summary>
        public int PartnerAccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта партнера
        /// </summary>
        public string PartnerAccountCaption { get; set; }

        /// <summary>
        /// Дата окончания сервиса
        /// </summary>
        public DateTime? ServiceExpireDate { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Аккаунт с признаком VIP
        /// </summary>
        public bool IsVipAccount { get; set; }

        /// <summary>
        /// Тип партнерства
        /// </summary>
        public TypeOfPartnership TypeOfPartnership { get; set; }

        /// <summary>
        /// Название сервиса который использует клиент.
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Дата подключения клиентом сервиса. 
        /// </summary>
        public DateTime? ClientActivationDate { get; set; }

        /// <summary>
        /// Активность сервиса у клиента. 
        /// </summary>
        public bool ServiceIsActiveForClient { get; set; }

        /// <summary>
        /// Ежемесячное вознаграждение агента по клиенту. 
        /// </summary>
        public decimal MonthlyBonus { get; set; }

        /// <summary>
        /// Флаг показывающий
        /// что сервис находиться в демо периоде
        /// </summary>
        public bool IsDemoPeriod { get; set; }

        public Guid ServiceId { get; set; }

        public string Inn { get; set; }
        public string PartnerInn { get; set; }
    }
}
