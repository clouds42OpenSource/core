﻿
namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    ///     Сервис облака
    /// </summary>
    public class DopSessionServiceDto 
    {
        /// <summary>
        ///     ID сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Услуга от которой зависит сервис
        /// </summary>
        public Guid? DependServiceId { get; set; }

        /// <summary>
        /// Дата окончания использования сервиса
        /// </summary>
        public DateTime ServiceExpireDate { get; set; }

        /// <summary>
        /// Сервис зависит от аренды
        /// </summary>
        public bool ServiceDependsOnRent { get; set; }

        /// <summary>
        /// Активность обещанного платежа
        /// </summary>
        public bool IsActivatedPromisPayment { get; set; }

        /// <summary>
        /// Cтоимость сервиса.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Валюта аккаунта
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Колличество используемых лицензий
        /// </summary>
        public int UsedLicenses { get; set; }



    }
}
