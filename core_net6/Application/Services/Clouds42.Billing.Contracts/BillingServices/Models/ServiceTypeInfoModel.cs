﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель информации по услуге биллинга
    /// </summary>
    public class ServiceTypeInfoModel
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Id аккаунта спонсора 
        /// </summary>
        public Guid? AccountSponsorId { get; set; }

        /// <summary>
        /// Название аккаунта спонсора 
        /// </summary> 
        public string AccountSponsorCaption { get; set; }

        /// <summary>
        /// Список родительских услуг
        /// </summary>
        public IEnumerable<BillingServiceTypeRelation> ParentServiceTypeRelations { get; set; } = [];
    }
}
