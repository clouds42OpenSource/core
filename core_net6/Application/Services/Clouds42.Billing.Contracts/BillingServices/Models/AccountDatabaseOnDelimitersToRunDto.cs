﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// База на разделителях для запуска
    /// </summary>
    public class AccountDatabaseOnDelimitersToRunDto
    {
        /// <summary>
        /// Id базы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название базы
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Ссылка веб публикации
        /// </summary>
        public string WebPublishPath { get; set; }

        /// <summary>
        /// Статус базы
        /// </summary>
        public DatabaseState DatabaseState { get; set; }
    }
}
