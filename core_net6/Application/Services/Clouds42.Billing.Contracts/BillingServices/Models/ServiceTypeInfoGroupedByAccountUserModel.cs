﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Сгруппированные данные услуг по пользователю
    /// </summary>
    public class ServiceTypeInfoGroupedByAccountUserModel
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }

        /// <summary>
        /// Информация по услугам биллинга
        /// </summary>
        public List<ServiceTypeInfoModel> ServiceTypesInfo { get; set; } = [];
    }
}
