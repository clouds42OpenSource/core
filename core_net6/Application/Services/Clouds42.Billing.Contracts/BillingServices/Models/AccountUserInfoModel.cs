﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель информации о пользователе аккаунта
    /// </summary>
    public class AccountUserInfoModel
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Пользователь является спонсором
        /// </summary>
        public bool IsSponsor { get; set; }
    }
}
