﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Иформация о сервисе для его активации
    /// </summary>
    public class BillingServiceActivationInfoDto
    {
        /// <summary>
        ///     ID сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// ID иконки
        /// </summary>
        public Guid? IconCloudFileId { get; set; }

        /// <summary>
        /// Url картинки
        /// </summary>
        public string IconCloudUrl { get; set; }

        /// <summary>
        /// Иконка
        /// </summary>
        public CloudFileDto IconCloudFile { get; set; }

        /// <summary>
        /// Ссылка для активации сервиса
        /// </summary>
        public string ServiceActivationLink { get; set; }
    }
}
