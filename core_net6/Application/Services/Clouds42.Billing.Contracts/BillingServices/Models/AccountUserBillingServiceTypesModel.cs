﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель данных по услугам пользователя
    /// </summary>
    public class AccountUserBillingServiceTypesModel
    {
        /// <summary>
        ///  Информация о пользователе аккаунта
        /// </summary>
        public AccountUserInfoModel AccountUserInfo { get; set; }

        /// <summary>
        /// Информация по услугам пользователя
        /// </summary>
        public List<ServiceTypeInfoModel> ServiceTypesInfoList { get; set; } = [];
    }
}
