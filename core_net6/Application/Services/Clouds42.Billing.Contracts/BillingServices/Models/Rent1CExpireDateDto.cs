﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Model for inforamtion about Rent1C expire date
    /// </summary>
    public class Rent1CExpireDateDto
    {
        /// <summary>
        /// Expire date of service
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// Service is in demo period
        /// </summary>
        public bool IsDemo { get; set; }
    }
}
