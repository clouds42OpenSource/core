﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Файл сервиса
    /// </summary>
    public class CloudFileDto
    {
        /// <summary>
        /// Название сервиса
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Массив байтов
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// Формат файла
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// Данные иконки в base64
        /// </summary>
        public string DataBase64 => Convert.ToBase64String(Content);
    }
}