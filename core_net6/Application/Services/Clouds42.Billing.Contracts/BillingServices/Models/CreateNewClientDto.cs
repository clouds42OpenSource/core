﻿
namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    public class CreateNewClientDto
    {
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public Guid ReferralAccountId { get; set; }
    }
}
