﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель данных услуги сервиса
    /// </summary>
    public class ServiceTypeDataModel
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal ServiceTypeCost { get; set; }
    }
}
