﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель сокращенной информации пользователя аккаунта 
    /// </summary>
    public class AccountUserShortInfoDto
    {
        /// <summary>
        /// Название аккаунта
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Признак, что пользователь активирован
        /// </summary>
        public bool Activated { get; set; }
    }
}
