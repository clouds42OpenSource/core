﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Конфигурация 1С на разделителях
    /// </summary>
    public class Configuration1COnDelimitersDto
    {
        /// <summary>
        /// Код конфигурации
        /// </summary>
        public string ConfigurationId { get; set; }

        /// <summary>
        /// Название конфигурации 1С
        /// </summary>
        public string Name { get; set; }
    }
}
