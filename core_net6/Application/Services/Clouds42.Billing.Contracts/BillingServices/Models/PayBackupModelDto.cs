﻿using System.ComponentModel.DataAnnotations;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Model for paying backup
    /// </summary>
    public class PayBackupModelDto
    {
        /// <summary>
        /// Service identifier
        /// </summary>
        [Required]
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Account identifier
        /// </summary>
        [Required]
        public Guid AccountId { get; set; }

        /// <summary>
        /// Size of payment in rubbles
        /// </summary>
        public decimal PaymentSum { get; set; }

        /// <summary>
        /// Flag for using promise payment
        /// </summary>
        public bool UsePromisePayment { get; set; }
    }
}
