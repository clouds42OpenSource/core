﻿namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель запуска базы
    /// </summary>
    public class AccountDatabaseToRunDto
    {
        /// <summary>
        /// Наличие соответствующих конфигураций 1С
        /// </summary>
        public bool AvailabilityOfSuitableConfigurations1C { get; set; }

        /// <summary>
        /// Id шаблона дефолтной конфигурации
        /// </summary>
        public Guid TemplateId { get; set; }

        /// <summary>
        /// Название шаблонов совместимых конфигурации
        /// </summary>
        public string TemplateNames { get; set; }

        /// <summary>
        /// Базы на разделителях для запуска
        /// </summary>
        public List<AccountDatabaseOnDelimitersToRunDto> AccountDatabasesOnDelimitersToRun { get; set; } = [];

        /// <summary>
        /// Существуют ли совместимые базы со статусом готова
        /// </summary>
        public bool IsCompatibleDatabasesWithStatusReadyExist { get; set; }

        /// <summary>
        /// Существуют ли совместимые базы со статусом новая
        /// </summary>
        public bool IsCompatibleDatabasesWithStatusNewExist { get; set; }
    }
}
