﻿using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель данных краткой информации о ресурсе сервиса
    /// </summary>
    public class ServiceTypeResourceDataModel
    {
        /// <summary>
        /// Название услуги
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Объем(количество ресурсов) 
        /// </summary>
        public int VolumeInQuantity { get; set; }

        /// <summary>
        /// Системный тип услуги
        /// </summary>
        public ResourceType? SystemServiceType { get; set; }

        /// <summary>
        /// Тип биллинга
        /// </summary>
        public BillingTypeEnum BillingType { get; set; }

        /// <summary>
        /// Системный сервиса
        /// </summary>
        public Clouds42Service? SystemService { get; set; }
    }
}
