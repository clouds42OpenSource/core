﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.DataModels.billing.BillingChanges;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель обновления стоимости услуг сервиса
    /// </summary>
    public class UpdateBillingServiceTypesCostDto
    {
        /// <summary>
        /// Id сервиса биллинга
        /// </summary>
        public Guid BillingServiceId { get; set; }

        /// <summary>
        /// Id изменений по сервису биллинга
        /// </summary>
        public Guid BillingServiceChangesId { get; set; }

        /// <summary>
        /// Список Id аккаунтов которые используют сервис
        /// </summary>
        public List<Guid> AccountIdsThatUseService { get; set; }

        /// <summary>
        /// Связь услуг сервиса и их изменений
        /// </summary>
        public Dictionary<BillingServiceType, BillingServiceTypeChange> ServiceTypeChangesRelations { get; set; }

        /// <summary>
        /// Новая стоимость услуг
        /// </summary>
        public decimal NewServiceTypesCost { get; set; }

        /// <summary>
        /// Старая стоимость услуг
        /// </summary>
        public decimal OldServiceTypesCost { get; set; }
    }
}
