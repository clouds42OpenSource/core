﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель фильтра транзакций партнера
    /// </summary>
    public class PartnerTransactionsFilterDto : ISortingDto
    {
        /// <summary>
        /// Данные об аккаунте
        /// </summary>
        public string? AccountData { get; set; }

        /// <summary>
        /// Данные об аккаунте партнера
        /// </summary>
        public string? PartnerAccountData { get; set; }

        /// <summary>
        /// Данные об операции
        /// </summary>
        public string? OperationData { get; set; }

        /// <summary>
        /// Период с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string? SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
