﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Транзакция партнера
    /// </summary>
    public class PartnerTransactionDto
    {
        /// <summary>
        /// Дата совершения транзакции
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Название компании
        /// </summary>
        public string AccountCaption { get; set; }

        /// <summary>
        /// Номер аккаунта.
        /// </summary>
        public int AccountIndexNumber { get; set; }

        /// <summary>
        /// Аккаунт с признаком VIP
        /// </summary>
        public bool IsVipAccount { get; set; }

        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Номер аккаунта партнера.
        /// </summary>
        public int PartnerAccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта партнера
        /// </summary>
        public string PartnerAccountCaption { get; set; }

        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Операция
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// Сумма клиентского платежа
        /// </summary>
        public decimal ClientPaymentSum { get; set; }

        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public decimal TransactionSum { get; set; }

        /// <summary>
        /// Тип источника агентского платежа
        /// </summary>
        public AgentPaymentSourceTypeEnum AgentPaymentSourceType { get; set; }

        /// <summary>
        /// Тип платежа
        /// </summary>
        public PaymentType PaymentType { get; set; }

        /// <summary>
        /// Тип транзакции
        /// </summary>
        public TransactionType? TransactionType { get; set; }
    }
}
