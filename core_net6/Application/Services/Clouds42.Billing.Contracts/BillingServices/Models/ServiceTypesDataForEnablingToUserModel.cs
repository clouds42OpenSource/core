﻿using Clouds42.DataContracts.BillingService.BillingServiceGet;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель данных по услугам для подключения к пользователю
    /// </summary>
    public class ServiceTypesDataForEnablingToUserModel
    {
        /// <summary>
        /// Данные на подключение услуги
        /// </summary>
        public CalculateBillingServiceTypeResultDto ServiceTypeEnablingData { get; set; }

        /// <summary>
        /// Список данных по услугам для подключения
        /// </summary>
        public List<ServiceTypeDataModel> ServiceTypesDataForEnabling { get; set; } = [];
    }
}
