﻿using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.CloudServicesSegment.InnerModels;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    ///     Сервис облака
    /// </summary>
    public class BillingServiceDto : IBillingService
    {
        /// <summary>
        ///     ID сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Краткое описание
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Возможности сервиса
        /// </summary>
        public string Opportunities { get; set; }

        /// <summary>
        /// ID иконки
        /// </summary>
        public Guid? IconCloudFileId { get; set; }

        /// <summary>
        /// Иконка
        /// </summary>
        public CloudFileDto IconCloudFile { get; set; }

        /// <summary>
        /// Url иконки
        /// </summary>
        public string IconUrl { get; set; }

        /// <summary>
        /// Основной сервис
        /// </summary>
        public Guid? MainServiceId { get; set; }

        /// <summary>
        /// Услуга от которой зависит сервис
        /// </summary>
        public Guid? DependServiceTypeId { get; set; }

        /// <summary>
        /// Аккаунт которому пренадлежит сервис
        /// </summary>
        public Guid? AccountOwnerId { get; set; }

        /// <summary>
        /// Сервис является системным, номер соотвествия
        /// </summary>
        public Clouds42Service? SystemService { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Дата активации сервиса
        /// </summary>
        public DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Дата окончания использования сервиса
        /// </summary>
        public DateTime ServiceExpireDate { get; set; }

        /// <summary>
        /// Данные по основному сервису.
        /// </summary>
        public IResourcesConfiguration MainServiceData { get; set; }

        /// <summary>
        /// Сервис работает в демо периоде.
        /// </summary>
        public bool IsDemoPeriod { get; set; }

        /// <summary>
        /// Сервис зависит от аренды
        /// </summary>
        public bool ServiceDependsOnRent { get; set; }

        /// <summary>
        /// Данные о стоимости сервиса.
        /// </summary>
        public BillingServiceAmountDataModelDto AmountData { get; set; }

        /// <summary>
        ///     Статус сервиса
        /// </summary>
        public ServiceStatusModelDto ServiceStatus { get; set; }

        /// <summary>
        /// Статус сервиса "Мой диск"
        /// </summary>
        public ServiceStatusModelDto MyDiskStatusModel { get; set; }

        /// <summary>
        /// Сервис является системным
        /// </summary>
        public bool IsSystemService => SystemService.HasValue;

        /// <summary>
        /// Модель запуска базы
        /// </summary>
        public AccountDatabaseToRunDto AccountDatabaseToRun { get; set; }

        /// <summary>
        /// Id инструкции сервиса
        /// </summary>
        public Guid? InstructionServiceId { get; set; }

        /// <summary>
        /// Id инструкции сервиса
        /// </summary>
        public string InstructionServiceUrl { get; set; }

        /// <summary>
        /// Признак что можно установить расширение сервиса
        /// </summary>
        public bool CanInstallExtension { get; set; }

        /// <summary>
        /// Признак, указывающий, нужно ли создавать базу для активации сервиса
        /// </summary>
        public bool NeedCreateDatabaseToActivateService { get; set; }

        /// <summary>
        /// Признак означающий что сервис - гибридный
        /// </summary>
        public bool IsHybridService { get; set; }
    }
}
