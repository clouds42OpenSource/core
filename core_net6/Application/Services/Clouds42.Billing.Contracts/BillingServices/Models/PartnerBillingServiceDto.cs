﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Сервис партнера
    /// </summary>
    public class PartnerBillingServiceDto
    {
        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название сервиса
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата активации сервиса
        /// </summary>
        public DateTime? ServiceActivationDate { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public int CountUsers { get; set; }

        /// <summary>
        /// Вознограждение
        /// </summary>
        public decimal ServiceCost { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum BillingServiceStatus { get; set; }

        /// <summary>
        /// Время обновления статуса
        /// </summary>
        public DateTime? StatusDateTime { get; set; }

        /// <summary>
        /// Номер аккаунта партнера.
        /// </summary>
        public int PartnerAccountIndexNumber { get; set; }

        /// <summary>
        /// Название аккаунта партнера
        /// </summary>
        public string PartnerAccountCaption { get; set; }

        /// <summary>
        /// Id аккаунта партнера
        /// </summary>
        public Guid PartnerAccountId { get; set; }

        /// <summary>
        /// Сервис активен
        /// </summary>
        public bool IsActive { get; set; }
    }
}
