﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель фильтра клиентов партнера
    /// </summary>
    public class PartnersClientFilterDto : ISortingDto
    {
        /// <summary>
        /// Данные об аккаунте
        /// </summary>
        public string? AccountData { get; set; }

        /// <summary>
        /// Данные об аккаунте партнера
        /// </summary>
        public string? PartnerAccountData { get; set; }

        /// <summary>
        /// Тип партнерства
        /// </summary>
        public TypeOfPartnership? TypeOfPartnership { get; set; }

        /// <summary>
        /// Период подключения с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период подключения по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Период даты окончания сервиса с
        /// </summary>
        public DateTime? PeriodServiceExpireDateFrom { get; set; }

        /// <summary>
        /// Период даты окончания сервиса по
        /// </summary>
        public DateTime? PeriodServiceExpireDateTo { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid? ServiceId { get; set; }

        /// <summary>
        /// Активность сервиса у клиента. 
        /// </summary>
        public bool? ServiceIsActiveForClient { get; set; }

        /// <summary>
        /// Индификатор аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int? PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int? PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string? SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
