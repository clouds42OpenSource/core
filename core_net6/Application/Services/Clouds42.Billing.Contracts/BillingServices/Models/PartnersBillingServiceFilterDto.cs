﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель фильтра сервисов партнера
    /// </summary>
    public class PartnersBillingServiceFilterDto : ISortingDto
    {
        /// <summary>
        /// Название сервиса
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Данные об аккаунте партнера
        /// </summary>
        public string? PartnerAccountData { get; set; }

        /// <summary>
        /// Период активации с
        /// </summary>
        public DateTime? PeriodFrom { get; set; }

        /// <summary>
        /// Период активации по
        /// </summary>
        public DateTime? PeriodTo { get; set; }

        /// <summary>
        /// Статус сервиса
        /// </summary>
        public BillingServiceStatusEnum? BillingServiceStatus { get; set; }

        /// <summary>
        /// Индификатор аккаунта
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Название поля для сортировки
        /// </summary>
        public string? SortFieldName { get; set; }

        /// <summary>
        /// Тип сортировки
        /// </summary>
        public SortType? SortType { get; set; }
    }
}
