﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Models
{
    /// <summary>
    /// Модель временных данных биллинга по услуге сервиса
    /// </summary>
    public class BillingServiceTypeInfoTempDataModel
    {
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Услуга сервиса биллинга
        /// </summary>
        public BillingServiceType ServiceType { get; set; }

        /// <summary>
        /// Стоимость услуги сервиса
        /// </summary>
        public decimal ServiceTypeCost { get; set; }

        /// <summary>
        /// Список Id дочерних услуг(зависимых услуг)
        /// </summary>
        public IEnumerable<Guid> ChildServiceTypeIds { get; set; }

        /// <summary>
        /// Список Id родительских услуг(от которых зависит данная услуга)
        /// </summary>
        public IEnumerable<Guid> ParentServiceTypeIds { get; set; }
    }
}
