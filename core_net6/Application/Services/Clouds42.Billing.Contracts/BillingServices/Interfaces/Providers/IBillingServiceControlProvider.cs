﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для управления сервисом
    /// </summary>
    public interface IBillingServiceControlProvider
    {
        /// <summary>
        /// Выполнить управление
        /// </summary>
        /// <param name="manageBillingServiceDto">Модель управления сервисом</param>
        void Manage(ManageBillingServiceDto manageBillingServiceDto);
    }
}
