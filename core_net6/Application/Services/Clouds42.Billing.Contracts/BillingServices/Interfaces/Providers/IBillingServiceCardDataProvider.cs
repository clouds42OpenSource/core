﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными карточки сервиса
    /// </summary>
    public interface IBillingServiceCardDataProvider
    {
        /// <summary>
        /// Получить данные карточки сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Модель карточки сервиса биллинга</returns>
        BillingServiceCardDto GetData(Guid serviceId);

        /// <summary>
        /// Попытаться получить дату следующего доступного
        /// изменения стоимости сервиса
        /// Если у сервиса стоимость не менялась то вернет Null
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Дата следующего доступного
        /// изменения стоимости сервиса</returns>
        DateTime? TryGetNextPossibleEditServiceCostDate(Guid serviceId);

        /// <summary>
        /// Проверить что существует заявка на изменение/создание сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Результат проверки</returns>
        bool IsExistsChangeServiceRequest(Guid serviceId);
    }
}
