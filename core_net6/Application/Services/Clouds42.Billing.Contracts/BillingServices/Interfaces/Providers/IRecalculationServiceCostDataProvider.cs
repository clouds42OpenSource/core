﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.RecalculateServiceCost;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными
    /// по пересчету стоимости сервиса 
    /// </summary>
    public interface IRecalculationServiceCostDataProvider
    {
        /// <summary>
        /// Получить данные
        /// по пересчету стоимости сервисов
        /// </summary>
        /// <param name="filter">Фильтр данных</param>
        /// <returns>Данные по пересчету стоимости сервисов</returns>
        PaginationDataResultDto<RecalculationServiceCostDataDto> GetData(RecalculationServiceCostFilterDto filter);
    }
}
