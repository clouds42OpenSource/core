﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    public interface IBillingServiceInfoProvider
    {
        /// <summary>
        /// Принять подписку демо сервиса.
        /// </summary>
        /// <param name="billingServiceId"></param>
        /// <param name="accountId">ID аккаунта</param>
        BillingServiceDto GetBillingService(Guid billingServiceId, Guid accountId);

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        List<BillingServiceTypeInfoDto> GetBillingServiceTypesInfo(Guid billingServiceId, Guid accountId);

        /// <summary>
        /// Получить информацию о услугах сервиса для аккаунта
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        DopSessionServiceDto? GetBillingServiceInfo(Guid serviceId, Guid accountId);
        

        /// <summary>
        /// Получить стоимость услуги
        /// влючая стоимость услуг которые входят в нее
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>Стоимость услуги</returns>
        decimal GetServiceTypeAndServiceTypeIncludesCost(Guid serviceTypeId, IAccount account);

        /// <summary>
        /// Получить список опций услуги
        /// </summary>
        /// <param name="serviceId">Для какой услуги получить список опций</param>
        /// <returns>Список опций услуги</returns>
        List<ServiceOptionDto> GetServiceOptions(Guid serviceId);

        ///<summary>
        /// Получить информацию по сервисам доступным аккаунту
        /// </summary>        
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные по доступным сервисам</returns>
        List<AccountBillingServiceDataDto> GetAccountServicesData(Guid accountId);

        /// <summary>
        /// Получить информацию о сервисе для его активации
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о сервисе</returns>
        BillingServiceActivationInfoDto GetBillingServiceForActivation(Guid billingServiceId, Guid accountId);

        /// <summary>
        /// Получить базу на разделителях для запуска
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>База на разделителях для запуска</returns>
        AccountDatabaseOnDelimitersToRunDto GetAccountDatabaseOnDelimitersToRun(Guid accountDatabaseId);

        /// <summary>
        /// Получить модель запуска базы
        /// </summary>
        /// <param name="billingServiceId">Id cервиса биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель запуска базы</returns>
        Task<AccountDatabaseToRunDto> GetAccountDatabaseToRun(Guid billingServiceId, Guid accountId);

        /// <summary>
        /// Попробовать получить ID сервиса по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id сервиса</returns>
        Guid TryGetServiceIdByKey(Guid key);

        /// <summary>
        /// Попробовать получить ID услуги по ключу(Id)
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns>Id услуги</returns>
        Guid TryGetServiceTypeIdByKey(Guid key);

        /// <summary>
        /// Получить типы подключения к информационной базе 1С
        /// </summary>
        /// <returns>Подключения к инф. базе</returns>
        TypesOfConnectionToInformationDbDto GetTypesOfConnectionToInformationDb();

        /// <summary>
        ///     Получить все конфигураций 1С на разделителях
        /// </summary>
        /// <returns>Конфигурации 1С</returns>
        List<Configuration1COnDelimitersDto> GetAllConfigurations1COnDelimiters();

        /// <summary>
        ///     Получить все отрасли
        /// </summary>
        /// <returns>Отрасли</returns>
        List<IndustryDto> GetAllIndustries();

        /// <summary>
        /// Проверить уникальность названия нового сервиса
        /// </summary>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        bool CheckUniquenessOfNameOfNewService(string serviceName);

        /// <summary>
        /// Проверить уникальность названия сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <returns>Результат проверки</returns>
        bool CheckUniquenessOfServiceName(Guid? serviceId, string serviceName);

        /// <summary>
        /// Получить текстовое описание назначения платежа по сервису.
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>        
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>Результат проверки</returns>
        string GetPaymentPurposeText(Guid serviceId, Guid accountId);
    }
}
