﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер создания услуги сервиса биллинга
    /// для конфигурации 1С (Для сервиса "Мои инф. базы")
    /// </summary>
    public interface ICreateServiceTypeForConfiguration1CProvider
    {
        /// <summary>
        /// Создать услугу сервиса для конфигурации 1С
        /// </summary>
        /// <param name="model">Модель создания услуги для конфигурации 1С</param>
        void Create(PerformOperationOnServiceTypeByConfig1CDto model);
    }
}
