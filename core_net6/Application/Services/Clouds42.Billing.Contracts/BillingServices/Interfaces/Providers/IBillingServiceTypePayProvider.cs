﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер оплаты услуг сервиса
    /// </summary>
    public interface IBillingServiceTypePayProvider
    {
        /// <summary>
        /// Применить все изменения указанные для аккаунта и пользователей
        /// </summary>
        /// <param name="billingServiceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Обещанный платеж</param>
        /// <param name="dataAccount">Услуги на подключение к аккаунту</param>
        /// <param name="dataAccountUsers">Услуги на подключение к пользователям</param>
        BillingServiceTypeApplyOrPayResultDto ApplyOrPayForAllChangesAccountServiceTypes(Guid accountId,
            Guid billingServiceId, bool isPromisePayment,
            ICollection<CalculateBillingServiceTypeDto> dataAccount,
            ICollection<CalculateBillingServiceTypeDto> dataAccountUsers);

    }
}