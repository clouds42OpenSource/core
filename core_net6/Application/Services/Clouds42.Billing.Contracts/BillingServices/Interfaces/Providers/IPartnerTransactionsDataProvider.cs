using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер данных транзакций партнера
    /// </summary>
    public interface IPartnerTransactionsDataProvider
    {
        /// <summary>
        /// Получить порцию данных транзакций партнера
        /// </summary>
        /// <param name="filter">Данные фильтра.</param>
        /// <returns>Порция данных транзакций</returns>
        PaginationDataResultDto<PartnerTransactionDto> GetPartnerTransactions(PartnerTransactionsFilterDto filter);
    }
}
