﻿namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для уведомления
    /// о скором изменении стоимости сервиса
    /// </summary>
    public interface INotifyBeforeChangeServiceCostProvider
    {
        /// <summary>
        /// Уведомить аккаунтов о скором
        /// изменении стоимости сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Модель по уведомлению
        /// о скором изменении стоимости сервиса</param>
        /// <param name="accountIds">Список Id аккаунтов для которых
        /// нужно сделать уведомление</param>
        /// <param name="serviceCostChangeDate">Дата изменения стоимости сервиса</param>
        void NotifyAccounts(Guid billingServiceChangesId, List<Guid> accountIds, DateTime serviceCostChangeDate);
    }
}
