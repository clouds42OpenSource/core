﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными сервиса биллинга
    /// </summary>
    public interface IBillingServiceDataProvider
    {
        /// <summary>
        /// Получить конфигурации 1С совместимые с сервисом биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса биллинга</param>
        /// <returns>Конфигурации 1С совместимые с сервисом биллинга</returns>
        IEnumerable<string> GetConfigurationsCompatibleWithService(Guid serviceId);

        /// <summary>
        /// Получить сервис биллинга
        /// или выкинуть ошибку
        /// </summary>
        /// <param name="billingServiceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        BillingService GetBillingServiceOrThrowNewException(Guid billingServiceId);

        /// <summary>
        /// Необходимость создавать информационную базу для активации сервиса
        /// </summary>
        /// <param name="billingService">Сервис биллинга</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Признак, указывающий, нужно ли создавать базу для активации сервиса</returns>
        bool NeedCreateDatabaseToActivateService(BillingService billingService, Guid accountId);

        /// <summary>
        /// Получить список совместимых с конфигурацией сервиса инф. баз
        /// </summary>
        /// <param name="billingService">Сервис облака</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Список совместимых с конфигурацией сервиса инф. баз</returns>
        IEnumerable<AccountDatabase> GetAccountDatabasesWithEqualServiceConfiguration(
            BillingService billingService, Guid accountId);

        /// <summary>
        /// Получить список данных о шаблонах инф. баз
        /// которые совместимы с сервисом
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список данных о шаблонах инф. баз
        /// которые совместимы с сервисом</returns>
        IEnumerable<DatabaseTemplateDataDto> GetDatabaseTemplatesCompatibleWithService(Guid serviceId);
    }
}