﻿using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы
    /// с заявкой изменения сервиса на модерацию
    /// </summary>
    public interface IChangeServiceRequestProvider
    {
        /// <summary>
        /// Получить заявку изменения сервиса на модерацию
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка изменения сервиса на модерацию</returns>
        ChangeServiceRequestDto Get(Guid changeServiceRequestId);

        /// <summary>
        /// Получить заявку на изменение сервиса
        /// </summary>
        /// <param name="billingServiceChangesId">Id изменения по сервису биллинга</param>
        /// <returns>Заявка на изменение сервиса</returns>
        EditServiceRequest GetEditServiceRequest(Guid billingServiceChangesId);
    }
}
