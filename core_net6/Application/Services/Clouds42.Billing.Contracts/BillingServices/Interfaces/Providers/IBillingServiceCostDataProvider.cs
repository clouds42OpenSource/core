﻿namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными стоимости сервиса
    /// </summary>
    public interface IBillingServiceCostDataProvider
    {
        /// <summary>
        /// Получить минимальную стоимость сервиса
        /// (минимальная стоимость по услуге сервиса с учетом зависимых услуг)
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Минимальная стоимость сервиса</returns>
        decimal GetMinimalCost(Guid serviceId);
    }
}
