﻿using Clouds42.Billing.Contracts.BillingServices.Models;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер данных клиентов агента.
    /// </summary>
    public interface IPartnersClientDataProvider
    {
        /// <summary>
        /// Получить порцию данных клиентов агента.
        /// </summary>
        /// <param name="filter">Данные фильтра.</param>
        /// <returns>Порция данных клиентов.</returns>
        PaginationDataResultDto<PartnerClientDto> GetPartnerClients(PartnersClientFilterDto filter);

        public PaginationDataResultDto<PartnerClientDto> GetPartnerClientsConsiderAdditionalMonths(
            PartnersClientFilterDto filter);

    }
}
