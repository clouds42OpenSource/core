﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для редактирования сервиса
    /// </summary>
    public interface IEditBillingServiceProvider
    {
        /// <summary>
        /// Отредактировать сервис
        /// </summary>
        /// <param name="editBillingServiceDto">Модель изменения сервиса</param>
        void Edit(EditBillingServiceDto editBillingServiceDto);
    }
}
