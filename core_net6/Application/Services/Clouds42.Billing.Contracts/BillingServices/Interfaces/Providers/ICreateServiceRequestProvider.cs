﻿namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с заявками
    /// сервиса на модерацию при создании
    /// </summary>
    public interface ICreateServiceRequestProvider
    {
        /// <summary>
        /// Создать заявку сервиса на модерацию при создании
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="serviceName">Название сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="сreateServiceRequestId">Id заявки на модерацию</param>
        void Create(Guid serviceId, string serviceName, Guid? accountId, Guid? сreateServiceRequestId);
    }
}
