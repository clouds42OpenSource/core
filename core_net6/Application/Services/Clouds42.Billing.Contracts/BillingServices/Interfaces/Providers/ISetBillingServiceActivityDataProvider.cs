﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер данных для установки активности сервиса
    /// </summary>
    public interface ISetBillingServiceActivityDataProvider
    {
        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        BillingService GetService(Guid serviceId);

        /// <summary>
        /// Получить сервис биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис биллинга</returns>
        BillingService GetServiceNoThrowException(Guid serviceId);

        /// <summary>
        /// Получить ресурсы сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Ресурсы сервиса биллинга</returns>
        IEnumerable<Resource> GetServiceResources(Guid serviceId);

        /// <summary>
        /// Проверить наличие активных конфигураций ресурсов
        /// у сервиса биллинга
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Наличие активных конфигураций ресурсов у сервиса</returns>
        bool CheckAvailabilityActiveResourceConfigurationAtService(Guid serviceId);
    }
}
