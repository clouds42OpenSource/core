﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.BillingService;
using Clouds42.Domain.DataModels.billing.BillingChanges;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными заявок
    /// сервиса на модерацию
    /// </summary>
    public interface IChangeServiceRequestDataProvider
    {
        /// <summary>
        /// Получить порцию данных
        /// заявок сервиса на модерацию
        /// </summary>
        /// <param name="filter">Модель фильтра заявок сервиса на модерацию</param>
        /// <returns>Порция данных
        /// заявок сервиса на модерацию</returns>
        PaginationDataResultDto<ChangeServiceRequestDataDto> GetChangeServiceRequests(
            ChangeServiceRequestsFilterDto filter);


        /// <summary>
        /// Получить модель изменений по сервису биллинга
        /// </summary>
        /// <param name="editServiceRequestId">Id заявки редактирования сервиса на модерацию</param>
        /// <returns>Модель изменений по сервису биллинга</returns>
        BillingServiceChangesDto GetBillingServiceChangesDto(Guid editServiceRequestId);

        /// <summary>
        /// Получить изменения по сервису биллинга
        /// </summary>
        /// <param name="editServiceRequestId">Id заявки редактирования сервиса на модерацию</param>
        /// <returns>Изменения по сервису биллинга</returns>
        BillingServiceChanges GetBillingServiceChanges(Guid editServiceRequestId);

        /// <summary>
        /// Получить заявку на модерацию сервиса
        /// </summary>
        /// <param name="changeServiceRequestId">Id заявки</param>
        /// <returns>Заявка на модерацию сервиса</returns>
        ChangeServiceRequest GetChangeServiceRequest(Guid changeServiceRequestId);
    }
}
