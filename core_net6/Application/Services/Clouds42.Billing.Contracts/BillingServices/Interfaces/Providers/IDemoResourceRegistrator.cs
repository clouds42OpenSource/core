﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Регистратор демо ресурсов.
    /// </summary>
    public interface IDemoResourceRegistrator
    {

        /// <summary>
        /// Зарегистрировать демо ресурсы. 
        /// </summary>
        /// <param name="resConfigBillingService">Сервис.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="resourceCount">Количество демо ресмурсов.</param>
        void RegisterDemoResources(
            BillingService resConfigBillingService,
            IAccount account,
            int resourceCount);

        /// <summary>
        /// Зарегистрировать демо ресурсы. 
        /// </summary>
        /// <param name="serviceType">Сервис.</param>
        /// <param name="account">Аккаунт.</param>
        /// <param name="freeResourceCount">Кол-во демо лицензий.</param>
        void Register(IBillingServiceType serviceType, IAccount account, int freeResourceCount);

    }
}