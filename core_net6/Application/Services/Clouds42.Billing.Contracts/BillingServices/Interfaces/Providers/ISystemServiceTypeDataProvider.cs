﻿using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными системной услуги сервиса биллинга
    /// </summary>
    public interface ISystemServiceTypeDataProvider
    {
        /// <summary>
        /// Получить услугу по типу системной услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Системная услуга биллинга</returns>
        BillingServiceType GetServiceTypeBySystemServiceType(ResourceType systemServiceType);

        /// <summary>
        /// Получить Id услуги по типу системной услуги
        /// </summary>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Id системной услуги биллинга</returns>
        Guid GetServiceTypeIdBySystemServiceType(ResourceType systemServiceType);
    }
}
