﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с заявками
    /// сервиса на модерацию при редактировании
    /// </summary>
    public interface IEditServiceRequestProvider
    {
        /// <summary>
        /// Создать заявку сервиса на модерацию при редактировании
        /// </summary>
        /// <param name="editBillingServiceDto">Модель редактирования сервиса</param>
        void Create(EditBillingServiceDto editBillingServiceDto);
    }
}
