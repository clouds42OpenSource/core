﻿using Clouds42.Domain.DataModels;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными владельца сервиса
    /// </summary>
    public interface IBillingServiceOwnerDataProvider
    {
        /// <summary>
        /// Получить админа аккаунта
        /// для владельца сервиса
        /// </summary>
        /// <param name="service">Сервис биллинга</param>
        /// <returns>Админ аккаунта</returns>
        AccountUser GetAccountAdminOwnerOfService(BillingService service);

        /// <summary>
        /// Получить админа аккаунта
        /// для владельца сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса биллинга</param>
        /// <returns>Админ аккаунта</returns>
        AccountUser GetAccountAdminOwnerOfService(Guid serviceId);

        /// <summary>
        /// Получить аккаунта владелец сервиса
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Id аккаунта владельца</returns>
        Guid GetAccountOwnerByServiceId(Guid serviceId);
    }
}
