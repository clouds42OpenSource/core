﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для редактирования черновика сервиса
    /// </summary>
    public interface IEditBillingServiceDraftProvider
    {
        /// <summary>
        /// Редактировать черновик сервиса
        /// </summary>
        /// <param name="editBillingServiceDraftDto">Модель редактирования черновика</param>
        void Edit(EditBillingServiceDraftDto editBillingServiceDraftDto);
    }
}
