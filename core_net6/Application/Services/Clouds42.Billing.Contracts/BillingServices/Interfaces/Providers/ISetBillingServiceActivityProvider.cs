﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для установки активности сервиса биллинга
    /// </summary>
    public interface ISetBillingServiceActivityProvider
    {
        /// <summary>
        /// Установить активность сервиса
        /// </summary>
        /// <param name="model">Модель для установки активности сервиса биллинга</param>
        void SetActivity(SetBillingServiceActivityDto model);

        /// <summary>
        /// Отключения сервиса
        /// </summary>
        /// <param name="model">Модель для отключения сервиса биллинга</param>
        void ShutdownService(SetBillingServiceActivityDto model);
    }
}
