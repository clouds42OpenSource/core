﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для создания сервиса биллинга
    /// </summary>
    public interface ICreateBillingServiceProvider
    {
        /// <summary>
        /// Создать сервис
        /// </summary>
        /// <param name="createBillingServiceDto">Модель создания сервиса</param>
        /// <returns>ID сервиса</returns>
        Guid Create(CreateBillingServiceDto createBillingServiceDto);
    }
}
