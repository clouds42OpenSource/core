﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными связи конфигурации 1С
    /// с услугой сервиса(Мои информационные базы)
    /// </summary>
    public interface IConfigurationServiceTypeRelationDataProvider
    {
        /// <summary>
        /// Получить услугу по названию конфигурации
        /// или выкинуть исключение
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        /// <returns>Услуга сервиса(Мои информационные базы)</returns>
        BillingServiceType GetServiceTypeOrThrowException(string configurationName);

        /// <summary>
        /// Получить Id услуги по названию конфигурации
        /// или выкинуть исключение
        /// </summary>
        /// <param name="configurationName">Название конфигурации</param>
        /// <returns>Id услуги сервиса(Мои информационные базы)</returns>
        Guid GetServiceTypeIdOrThrowException(string configurationName);
    }
}
