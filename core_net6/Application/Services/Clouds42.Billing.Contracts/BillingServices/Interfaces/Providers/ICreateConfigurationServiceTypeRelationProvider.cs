﻿namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для создания связи конфигурации 1С
    /// с услугой сервиса(Мои информационные базы)
    /// </summary>
    public interface ICreateConfigurationServiceTypeRelationProvider
    {
        /// <summary>
        /// Создать связь конфигурации 1С
        /// с услугой сервиса
        /// </summary>
        /// <param name="serviceTypeId">Id услуги сервиса</param>
        /// <param name="configurationName">Название конфигурации 1С</param>
        void Create(Guid serviceTypeId, string configurationName);
    }
}
