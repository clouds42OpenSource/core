﻿using Clouds42.Domain.Enums.AccountDatabase;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers
{
    /// <summary>
    /// Провайдер сервиса биллинга Тардис
    /// </summary>
    public interface ITardisBillingServiceProvider
    {
        /// <summary>
        /// Изменить доступ к инф. базе тардис
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="updateAcDbAccessActionType">Тип действия</param>
        void ManageAccessToTardisAccountDatabase(Guid? accountUserId, Guid billingServiceId, UpdateAcDbAccessActionType updateAcDbAccessActionType);
    }
}
