﻿using System.Xml;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Services
{
    /// <summary>
    /// Служба парсинга zip файла разработки 1С 
    /// </summary>
    public interface IParseZipDevelopmentFileService
    {
        /// <summary>
        /// Выполнить парсинг файла
        /// </summary>
        /// <param name="fileData">Данные файла</param>
        /// <returns>Xml документ содержащий метаданные</returns>
        XmlDocument Parse(byte[] fileData);
    }
}
