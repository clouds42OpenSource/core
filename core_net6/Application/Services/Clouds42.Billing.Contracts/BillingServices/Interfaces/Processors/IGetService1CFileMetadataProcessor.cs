﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.DataContracts.BillingService.BillingService1C;
using Microsoft.AspNetCore.Http;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Processors
{
    /// <summary>
    /// Процессор для получения метаданных файла разработки 1С
    /// </summary>
    public interface IGetService1CFileMetadataProcessor
    {
        /// <summary>
        /// Получить метаданные файла разработки 1С
        /// </summary>
        /// <param name="file">Файл разработки 1С</param>
        /// <returns>Метаданные файла разработки 1С</returns>
        Service1CFileMetadataDto GetMetadata(CloudFileDataDto<IFormFile> file);
    }
}
