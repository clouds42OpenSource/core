﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers
{
    public interface IBillingServiceTypeRelationHelper
    {
        IEnumerable<BillingServiceType> GetAllDependencyRelations(BillingServiceType billingServiceType, Guid accountId);
        IEnumerable<BillingServiceType> GetChildDependencyRelations(BillingServiceType billingServiceType, Guid accountId);
        int GetChildDependencyRelationsCount(BillingServiceType billingServiceType, Guid accountId);
        IEnumerable<BillingServiceType> GetChildDependencyRelationsWithCurrent(BillingServiceType billingServiceType, Guid accountId);
        IEnumerable<BillingServiceType> GetParentDependencyRelations(BillingServiceType billingServiceType, Guid accountId);
    }
}
