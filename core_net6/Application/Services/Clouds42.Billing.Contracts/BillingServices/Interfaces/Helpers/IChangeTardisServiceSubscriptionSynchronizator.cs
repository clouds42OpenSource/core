﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingServices.Interfaces.Helpers
{
    /// <summary>
    /// Синхронизатор изменения подписки сервиса Тардис с апи Тардис.
    /// </summary>
    public interface IChangeTardisServiceSubscriptionSynchronizator
    {
        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resources">Список изменяемых ресурсов.</param>
        void SynchronizeWithTardis(List<Resource> resources);

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="resource">Изменяемый ресурс.</param>
        void SynchronizeWithTardis(Resource resource);

        /// <summary>
        /// Синхронизировать с апи Тардис изменение подписки пользователя.
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        void SynchronizeWithTardis(Guid accountUserId);
    }
}
