﻿namespace Clouds42.Billing.Contracts.Billing.Models;

public class AccountIndexNumberDbNumberDto
{
    public int AccountIndexNumber { get; set; }
    public int DbNumber { get; set; }
    public string Login { get; set; }
}
