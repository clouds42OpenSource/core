﻿using Clouds42.Domain.DataModels;

namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Результат создания обещанного платежа
    /// </summary>
    public class CreatePromisePaymentResult
    {
        /// <summary>
        ///  Выполнилось создание обещанного платежа
        /// </summary>
        public bool Complete { get; set; }

        /// <summary>
        /// Сумма обещанного платежа
        /// </summary>
        public decimal PromisePaymentSum { get; set; }

        /// <summary>
        /// Локаль
        /// </summary>
        public Locale Locale { get; set; }
    }
}
