﻿namespace Clouds42.Billing.Contracts.Billing.Models.PaymentSystem.Interfaces
{
    /// <summary>
    ///     Генератор подписи
    /// </summary>
    public interface ISignatureGenerator
    {
        string GenerateSignature(Guid supplierId);
    }
}
