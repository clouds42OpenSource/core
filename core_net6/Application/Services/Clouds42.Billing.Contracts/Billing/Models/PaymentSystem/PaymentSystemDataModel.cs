﻿namespace Clouds42.Billing.Contracts.Billing.Models.PaymentSystem
{
    public class PaymentSystemDataModel
    {
        public Domain.Enums.PaymentSystem PaymentSystem { get; set; }

        public string RobokassaUrl { get; set;}
        public string PayboxUrl { get; set; }

        public string RequestUrl { get; set; }

        public string ServiceId { get; set; }

        public string Successurl { get; set; }

        public string Order { get; set; }

        public decimal Amount { get; set; }

        public string Encoding { get; set; }
    }
}