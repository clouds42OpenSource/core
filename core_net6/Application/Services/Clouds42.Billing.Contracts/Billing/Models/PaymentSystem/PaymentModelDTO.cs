﻿using Clouds42.Billing.Contracts.Factories.Models;

namespace Clouds42.Billing.Contracts.Billing.Models.PaymentSystem
{
    /// <summary>
    /// Модель платежа
    /// </summary>
    public class PaymentModelDto
    {
        /// <summary>
        /// Модель для UkrPay
        /// </summary>
        public UkrPayInitDataModel ModelForUkrPays { get; set; }
        
        /// <summary>
        /// Другие модели
        /// </summary>
        public string OtherPaymentModels { get; set; }
    }
}
