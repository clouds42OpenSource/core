﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Структура ресурс конфигурации сервиса с зависимостями.
    /// </summary>
    public class ResConfigDependenciesModel(
        ResourcesConfiguration resourcesConfiguration,
        List<ResourcesConfiguration> dependedResourceConfigurations)
    {
        /// <summary>
        /// Ресурс конфигурация сервиса.
        /// </summary>
        public ResourcesConfiguration ResourcesConfiguration { get; } = resourcesConfiguration;

        /// <summary>
        /// Зависимые сервисы.
        /// </summary>
        public List<ResourcesConfiguration> DependedResourceConfigurations { get; } = dependedResourceConfigurations;
    }
}
