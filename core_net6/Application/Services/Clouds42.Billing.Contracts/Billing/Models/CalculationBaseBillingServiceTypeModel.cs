﻿namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Модель калькуляции основных услуг биллинга
    /// </summary>
    public class CalculationBaseBillingServiceTypeModel
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Количество лицензий
        /// </summary>
        public int CountLicenses { get; set; }
    }
}
