﻿namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Модель нижнего коллонтитула аккаунта
    /// </summary>
    public class AccountFooterModel
    {
        /// <summary>
        /// Количество зарегестрированных компаний
        /// </summary>
        public int RegisteredCompanyAmount { get; set; }

        /// <summary>
        /// Количество зарегестрированных компаний в день
        /// </summary>
        public int RegisteredCompanyAmountToday { get; set; }

        /// <summary>
        /// Последняя регистрация
        /// </summary>
        public string LastRegistration { get; set; }

        /// <summary>
        /// Последняя зарегестрированная компания
        /// </summary>
        public string LastRegisteredCompany { get; set; }

        /// <summary>
        /// Количество пользователей
        /// </summary>
        public long UsersAmount { get; set; }

        /// <summary>
        /// Количество баз данных
        /// </summary>
        public long DatabasesAmount { get; set; }

        /// <summary>
        /// Всего платежей по услугам
        /// </summary>
        public string TotalPaymentByServices { get; set; }

        /// <summary>
        /// Всего платежей за месяц
        /// </summary>
        public string MonthTotalPayment { get; set; }
    }
}
