﻿using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Модель запроса на калькуляцию счета
    /// </summary>
    public class CalculationOfInvoiceRequestModel
    {
        /// <summary>
        /// Предлагаемый платеж
        /// </summary>
        public SuggestedPaymentModelDto SuggestedPayment { get; set; }

        /// <summary>
        /// Бонусное вознаграждение
        /// </summary>
        public decimal BonusReward { get; set; }

        /// <summary>
        /// Размер диска
        /// </summary>
        public int? MyDiskSize { get; set; }

        /// <summary>
        /// Мой диск отключен
        /// </summary>
        public bool MyDiskDisabled { get; set; }

        /// <summary>
        /// Дополнительные ресурсы отключены
        /// </summary>
        public bool AdditionalResourcesDisabled { get; set; }

        /// <summary>
        /// Период оплаты
        /// </summary>
        public PayPeriod PayPeriod { get; set; }

        /// <summary>
        /// Количество лет на лицензии
        /// </summary>
        public int? EsdlLicenseYears { get; set; }

        /// <summary>
        /// Количество страниц на распознование
        /// </summary>
        public int? EsdlPagesCount { get; set; }

        /// <summary>
        /// Признак необходимости отправлять письмо
        /// </summary>
        public bool NeedSendEmail { get; set; }

        /// <summary>
        /// Адрес для отправки счета на оплату
        /// </summary>
        public Guid AccountUserIdForSendNotify { get; set; }

        /// <summary>
        /// Калькуляция основных услуг биллинга
        /// </summary>
        public List<CalculationBaseBillingServiceTypeModel> CalculationBaseBillingServiceTypes { get; set; } = [];
    }
}