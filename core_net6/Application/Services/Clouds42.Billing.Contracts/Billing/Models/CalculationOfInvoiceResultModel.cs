﻿namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Модель результата калькуляции счета
    /// </summary>
    public class CalculationOfInvoiceResultModel
    {
        /// <summary>
        /// Стоимость диска
        /// </summary>
        public decimal MyDiskCost { get; set; }

        /// <summary>
        /// Калькуляция основных услуг биллинга
        /// </summary>
        public List<CalculationBaseBillingServiceTypeModel> CalculationBaseBillingServiceTypes { get; set; } = [];

        /// <summary>
        /// Общая стоимость основных услуг биллинга
        /// </summary>
        public decimal TotalAmountOfBaseBillingServiceTypes => GetTotalAmountOfBaseBillingServiceTypes();

        /// <summary>
        /// Стоимость лицензий загрузки документов
        /// </summary>
        public decimal EsdlLicenseCost { get; set; }

        /// <summary>
        /// Стоисомть страниц загрузки документов
        /// </summary>
        public decimal EsdlPagesCost { get; set; }

        /// <summary>
        /// Общая стоимость сервиса Загрука документов
        /// </summary>
        public decimal EsdlTotalAmount => EsdlLicenseCost + EsdlPagesCost;

        /// <summary>
        /// Дополнительная стоимость сервиса
        /// </summary>
        public decimal AdditionalServiceCost { get; set; }

        /// <summary>
        /// Итоговая стоимость
        /// </summary>
        public decimal TotalSum => GetTotalSum();

        /// <summary>
        /// Получить общую стоимость основных услуг биллинга
        /// </summary>
        /// <returns></returns>
        private decimal GetTotalAmountOfBaseBillingServiceTypes()
        {
            decimal totalAmount = 0;

            CalculationBaseBillingServiceTypes.ForEach(w =>
            {
                totalAmount += w.Amount;
            });

            return totalAmount + MyDiskCost;
        }

        /// <summary>
        /// Получить итоговую стоимость
        /// </summary>
        /// <returns>Итоговая стоимость</returns>
        private decimal GetTotalSum()
        {
            return AdditionalServiceCost + EsdlTotalAmount + TotalAmountOfBaseBillingServiceTypes;
        }
    }
}
