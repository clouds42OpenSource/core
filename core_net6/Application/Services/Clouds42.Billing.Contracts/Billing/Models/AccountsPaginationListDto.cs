﻿using Clouds42.Accounts.Contracts.Account.Models;
using Clouds42.DataContracts.BaseModel;

namespace Clouds42.Billing.Contracts.Billing.Models
{
    /// <summary>
    /// Модель отображения информации об аккаунтах
    /// </summary>
    public class AccountsPaginationListDto
    {
        /// <summary>
        /// Информация об аккаунтах
        /// </summary>
        public List<AccountDetailsDto> AccountsDetails { get; set; }
        
        /// <summary>
        /// Пагинация
        /// </summary>
        public PaginationBaseDto Pagination {get; set; }

        /// <summary>
        /// Id ссылки
        /// </summary>
        public Guid? ReferrerId { get; set; }

        /// <summary>
        /// Модель нижнего коллонтитула аккаунта
        /// </summary>
        public AccountFooterModel Footer { get; set; }
    }
}
