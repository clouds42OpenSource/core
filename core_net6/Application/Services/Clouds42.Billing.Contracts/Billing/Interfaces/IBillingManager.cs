﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.Billing.Contracts.Billing.Interfaces
{
    public interface IBillingManager
    {
        /// <summary>
        /// Заблокировать сервисы с просроченным демо периодом
        /// </summary>
        ManagerResult LockExpiredDemoServices();

        /// <summary>
        /// Уведомить клиентов о скорой блокировке сервисов.
        /// </summary>
        ManagerResult NotifyBeforeLockServices();

        /// <summary>
        /// Уведомить клиентов о том, что заканчивается демо период.
        /// </summary>
        ManagerResult NotifyServiceDemoPeriodIsComingToEnd();

        /// <summary>
        /// Обработать просроченные обещанные платежи.
        /// </summary>
        ManagerResult ProcessExpiredPromisePayments();

        /// <summary>
        /// Разблокировать заблокированные сервисы на которые есть деньги что бы разблокировать.
        /// </summary>     
        ManagerResult ProlongLockedServices();

        /// <summary>
        /// Продлить или заблокировать просроченные сервисы.
        /// </summary>      
        ManagerResult ProlongOrLockExpiredServices();

        /// <summary>
        /// Продлить или заблокировать просроченные гибридные сервисы.
        /// </summary>    
        ManagerResult ProlongOrLockHybridExpiredServices();
        
        /// <summary>
        /// Продление заблокированных сервисов у спонсируемых аккаунтов
        /// при нулевой цене и активном спонсировании 
        /// </summary>
        ManagerResult UnlockSponsoredServices();

        /// <summary>
        /// Уведомить клиентов о скором автосписании для продления сервисов
        /// </summary>
        ManagerResult NotifyBeforeLockServicesAutoPay();
    }
}
