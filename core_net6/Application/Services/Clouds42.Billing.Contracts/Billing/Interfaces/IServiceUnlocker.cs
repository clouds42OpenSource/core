﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces
{
    public interface IServiceUnlocker
    {
        void UnlockExpiredOrPayLockedServicesAccount(Guid accountId);
        void UnlockExpiredOrPayLockedService(ResourcesConfiguration configuration);
        void ProlongServiceIfTheCostIsLess(Guid accountId, decimal oldCostOfService,
            ResourcesConfiguration resourceConfiguration,
            PaymentActionType paymentActionType = PaymentActionType.PayService);

    }
}
