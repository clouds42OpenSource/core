﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для обновления дополнительных ресурсов аккаунта
    /// </summary>
    public interface IUpdateAdditionalResourcesForAccountProvider
    {
        /// <summary>
        /// Обновить дополнительные ресурсы для аккаунта
        /// </summary>
        /// <param name="model">Модель для обновления дополнительных ресурсов аккаунта</param>
        void Update(UpdateAdditionalResourcesForAccountDto model);
    }
}
