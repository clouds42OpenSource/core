﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Обработчик продления сервиса или блокирования.
    /// </summary>
    public interface IProlongActiveOrLockServiceProcessor
    {
        /// <summary>
        /// Продлить активный сервис или заблокировать с учетом всех зависимых сервисов.
        /// </summary>
        /// <param name="resourceConfiguration">Конфигурация сервиса.</param>        
        void Process(ResourcesConfiguration resourceConfiguration);
    }
}