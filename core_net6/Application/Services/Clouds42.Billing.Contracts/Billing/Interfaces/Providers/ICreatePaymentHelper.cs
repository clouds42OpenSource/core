﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface ICreatePaymentHelper
    {
        List<Guid> MakePaymentsForResConfigDependencies(ResConfigDependenciesModel resConfModel, PaymentActionType paymentActionType = PaymentActionType.ProlongService);
        CreatePaymentResultDto MakePayment(ResourcesConfiguration configuration, string paymentDescription,
            decimal cost, PaymentSystem paymentSystem, PaymentType type, string originDetails,
            bool canOverdraft = false);

    }
}
