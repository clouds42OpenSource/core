﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными дополнительных ресурсов аккаунта
    /// </summary>
    public interface IAdditionalResourcesDataProvider
    {
        /// <summary>
        /// Получить данные дополнительных ресурсов аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель данных о дополнительных ресурсах аккаунта</returns>
        AdditionalResourcesDataDto GetAdditionalResourcesData(Guid accountId);
    }
}
