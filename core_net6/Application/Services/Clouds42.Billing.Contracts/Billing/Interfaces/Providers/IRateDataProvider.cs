﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными тарифа услуги
    /// </summary>
    public interface IRateDataProvider
    {
        /// <summary>
        /// Получить тариф услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги</returns>
        Rate GetRate(Guid serviceTypeId);

        /// <summary>
        /// Получить тариф услуги или выкинуть исключение
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги</returns>
        Rate GetRateOrThrowException(Guid serviceTypeId);

        /// <summary>
        /// Получить стоимость услуги по тарифу
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Стоимость услугипо тарифу</returns>
        decimal GetServiceTypeCost(Guid serviceTypeId);

        /// <summary>
        /// Получить тариф услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="accountType">Тип аккаунта биллинга</param>
        /// <param name="localId">Id локали</param>
        /// <returns>Тариф услуги</returns>
        Rate? GetRate(Guid serviceTypeId, string accountType, Guid? localId = null);
    }
}
