﻿using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер получения или создания нового счета на оплату.
    /// </summary>
    public interface IGetOrCreateInvoiceDataProvider
    {
        /// <summary>
        /// Получить счет на оплату или создать по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="invoiceNumber">Номер счета.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        Task<InvoicePaymentsDto?> GetProcessingOrCreateNewInvoiceAsync(string invoiceNumber, decimal sum);

        /// <summary>
        /// Получить необработанный инвойс на заданную сумму.
        /// </summary>
        /// <param name="sum">Сумма счёта.</param>
        /// <param name="inn">ИНН аккаунта.</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        Task<InvoicePaymentsDto?> GetProcessingInvoice(string inn, decimal sum);

        /// <summary>
        /// Получить или создать счёт по заданным параметрам.
        /// </summary>
        /// <param name="sum">Сумма к оплате, на которую бедут сформирован или получен счёт</param>
        /// <param name="forceCreate">Флаг принудительного создания счёта</param>
        /// <param name="invoiceNumber">Номер инвойса.</param>
        /// <param name="inn">ИНН аккаунта для которого будет сформирован или получен счёт</param>
        /// <returns>Ok с инкапсулированным счётом, либо NotFound</returns>
        Task<InvoicePaymentsDto?> GetOrCreateInvoiceAsync(string invoiceNumber, bool forceCreate, string inn, decimal sum);


    }
}
