﻿using Clouds42.Billing.Contracts.Billing.Models;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    ///     Класс для обработки уведомлений "Выставление счета"
    /// </summary>
    public interface IInvoiceNotificationHelper
    {
        /// <summary>
        ///     Обработка уведомления Обещанного платежа
        /// </summary>
        /// <param name="invoice">Счет</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="promiseSum">Сумма</param>
        /// <param name="date">Дата</param>
        /// <param name="productsInfo">Список услуг</param>
        /// <param name="attachFile">Прикрепленный файл</param>
        void CreatePromisePaymentNotify(Domain.DataModels.billing.Invoice invoice, Guid accountId, decimal promiseSum, DateTime date,
            string productsInfo, byte[] attachFile);
        
        /// <summary>
        /// Создать уведомление счета
        /// </summary>
        /// <param name="invoiceId">Id счета</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="productsInfo">Информация о продуктах счета</param>
        /// <param name="attachFile">Прикрепленный файл</param>
        /// <param name="calculationOfInvoiceRequestModel">Модель калькуляции счета</param>
        void CreateInvoiceNotify(Guid invoiceId, Guid accountId, string productsInfo, byte[] attachFile,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequestModel);
    }
}