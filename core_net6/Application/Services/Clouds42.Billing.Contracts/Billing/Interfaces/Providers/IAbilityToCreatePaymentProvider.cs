﻿using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер проверки возможности создать платеж
    /// </summary>
    public interface IAbilityToCreatePaymentProvider
    {
        /// <summary>
        /// Проверить возможность создать платеж на указанную сумму и взять ОП если необходимо
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="billingServiceId">ID сервиса биллинга</param>
        /// <param name="amount">Сумма</param>
        /// <param name="isPromisePayment">Признак что это ОП</param>
        /// <returns>Результат проверки</returns>
        CheckAbilityToPayForServiceTypeResultDto CheckAbilityAndGetPromisePaymentIfNeeded(Guid accountId,
            Guid billingServiceId, decimal amount, bool isPromisePayment);

        bool CanUsePromisePayment(Guid accountId);

        /// <summary>
        /// Проверить, доступно ли увеличение ОП для данного аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Результат проверки</returns>
        bool CanIncreasePromisePayment(Guid accountId);
    }
}
