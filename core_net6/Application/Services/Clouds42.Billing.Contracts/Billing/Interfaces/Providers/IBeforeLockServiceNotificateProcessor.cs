﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Процессор нотификаций аккаунта о блокировки сервиса
    /// </summary>
    public interface IBeforeLockServiceNotificateProcessor
    {
        /// <summary>
        /// Отпарвить уведомление о блокировке сервиса
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="reasonMsg">Причина блокировки</param>
        /// <param name="needSendInvoice">Необходимость отправлять счет</param>
        void Notify(ResourcesConfiguration resourcesConfiguration, string? reasonMsg,
            bool needSendInvoice);
    }
}
