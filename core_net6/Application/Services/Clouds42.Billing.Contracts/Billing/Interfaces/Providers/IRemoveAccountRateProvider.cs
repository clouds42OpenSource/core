﻿namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для удаления тарифа услуги на аккаунт
    /// </summary>
    public interface IRemoveAccountRateProvider
    {
        /// <summary>
        /// Удалить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунт</param>
        /// <param name="serviceTypeId">Id услуги</param>
        void Remove(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Удалить тарифы услуг на аккаунт по сервису
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        void RemoveAccountRatesByService(Guid accountId, Guid serviceId);
    }
}
