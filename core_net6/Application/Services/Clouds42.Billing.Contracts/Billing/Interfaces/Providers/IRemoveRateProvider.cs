﻿namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для удаления тарифа услуги
    /// </summary>
    public interface IRemoveRateProvider
    {
        /// <summary>
        /// Удалить тариф для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        void Remove(Guid serviceTypeId);
    }
}
