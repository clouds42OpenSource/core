﻿namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    ///     Создание фискального чека
    /// </summary>
    public interface IInvoiceReceiptProvider
    {
        /// <summary>
        ///     Создать фискальный чек по счету на оплату
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        void CreateFiscalReceiptByInvoice(Domain.DataModels.billing.Invoice invoice);
    }
}