﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Процессор для работы со счетом
    /// </summary>
    public interface IInvoiceDcProcessor
    {
        /// <summary>
        ///     Получить DC модель чека
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        InvoiceReceiptDto GetReceiptModel(Guid invoiceId);
    }
}
