﻿using Clouds42.Billing.Contracts.Billing.Models;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IPromisePaymentProvider
    {

        /// <summary>
        /// Создать обещанный платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель запроса на калькуляцию счета</param>
        /// <param name="prolongServices">Выполнить пролонгацию сервисов</param>
        /// <returns>Результат создания обещанного платежа</returns>
        CreatePromisePaymentResult CreatePromisePayment(Guid accountId, CalculationOfInvoiceRequestModel model, bool prolongServices = true);

        /// <summary>
        /// Начисление обещанного платежа
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <param name="promiseSum">Сумма обещанного платежа.</param>
        /// <param name="prolongServices">Пролонгировать сервисы.</param>
        /// <param name="date">Дата и время создания ОП.</param>
        /// <returns>Признок успешности создания ОП.</returns>
        bool CreatePromisePayment(Guid accountId, decimal promiseSum, bool prolongServices, out DateTime date);
    }
}