﻿namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер пролонгации услуг.
    /// </summary>
    public interface IProlongServicesProvider
    {
        /// <summary>
        /// Обработать просроченные обещанные платежи.
        /// </summary>
        void ProcessExpiredPromisePayments();

        /// <summary>
        /// Продлить или заблокировать просроченные сервисы.
        /// </summary>
        void ProlongOrLockExpiredServices();

        /// <summary>
        /// Уведомить клиентов о скорой блокировке сервисов.
        /// </summary>
        void NotifyBeforeLockServices();

        /// <summary>
        /// Асинхронно создает счета-фактуры для услуг с датой окончания действия, не превышающей 5 дней.
        /// </summary>
        void IssueInvoicesBeforeRentalEnd();

        /// <summary>
        /// Разблокировать заблокированные сервисы которые заблокированны, на которые есть деньги что бы разблокировать и у аккаунтов которых нет просроченного обещанного платежа.
        /// </summary>
        void ProlongPaidLockedServices();

        /// <summary>
        /// Продление заблокированных сервисов у спонсируемых аккаунтов
        /// при нулевой цене и активном спонсировании 
        /// </summary>
        void UnlockSponsoredServices();

        /// <summary>
        /// Заблокировать просроченные демо сервисы
        /// </summary>
        void LockExpiredDemoServices();

        /// <summary>
        /// Продлить или заблокировать просроченные гибридные сервисы.
        /// </summary>
        void ProlongOrLockHybridExpiredServices();

        /// <summary>
        /// Уведомить клиентов о том, что заканчиваетсчя демо период.
        /// </summary>
        void NotifyServiceDemoPeriodIsComingToEnd();

        /// <summary>
        /// Уведомить клиентов о скором автосписании для продления сервисов.
        /// </summary>
        void NotifyBeforeLockServicesAutoPay();

    }
}
