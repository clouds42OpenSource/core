﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы со счетом
    /// </summary>    
    public interface IInvoiceProvider
    {
        /// <summary>
        /// Получить или создать счет
        /// </summary>
        /// <param name="resourcesConfiguration">Конфигурация ресурса</param>
        /// <param name="billingAccount">Аккаунт билинга</param>
        /// <returns>Счет</returns>
        Domain.DataModels.billing.Invoice GetOrCreateInvoiceByResourcesConfiguration(ResourcesConfiguration resourcesConfiguration, BillingAccount billingAccount);

        /// <summary>
        /// Создать счёт для аккаунта на указанную сумму
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="amount">Сумма счета</param>
        /// <param name="period">Период счета</param>
        /// <returns>Счет на оплату</returns>
        Domain.DataModels.billing.Invoice CreateInvoiceForSpecifiedInvoiceAmount(Guid accountId, decimal amount, int? period = null);

        /// <summary>
        /// Создать счет на основании модели калькуляции счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <returns>Счет на оплату</returns>
        Domain.DataModels.billing.Invoice CreateInvoiceBasedOnCalculationInvoiceModel(Guid accountId,
            CalculationOfInvoiceRequestModel calculationOfInvoiceRequest);
    }
}
