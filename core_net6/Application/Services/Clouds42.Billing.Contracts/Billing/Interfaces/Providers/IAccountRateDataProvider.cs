﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными тарифа на аккаунт
    /// </summary>
    public interface IAccountRateDataProvider
    {
        /// <summary>
        /// Получить тариф услуги на аккаунт
        /// или выкинуть исключение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги на аккаунт</returns>
        AccountRate GetAccountRateOrThrowException(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Получить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Тариф услуги на аккаунт</returns>
        AccountRate GetAccountRate(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Получить тарифы услуг на аккаунт для сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Тарифы услуг на аккаунт для сервиса</returns>
        IEnumerable<AccountRate> GetAccountRatesForService(Guid accountId, Guid serviceId);
    }
}
