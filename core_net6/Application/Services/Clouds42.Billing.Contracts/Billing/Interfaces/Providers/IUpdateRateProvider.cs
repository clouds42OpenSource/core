﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер обновления тарифа для услуги
    /// </summary>
    public interface IUpdateRateProvider
    {
        /// <summary>
        /// Обновить стоимость тарифа для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="newCost">Новая услуга</param>
        void UpdateRateCost(Guid serviceTypeId, decimal newCost);

        /// <summary>
        /// Обновить стоимость тарифа для услуги
        /// </summary>
        /// <param name="rate">Тариф услуги</param>
        /// <param name="newCost">Новая услуга</param>
        void UpdateRateCost(Rate rate, decimal newCost);
    }
}
