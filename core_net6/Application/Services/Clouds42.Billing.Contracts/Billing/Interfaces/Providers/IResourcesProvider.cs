﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.DataContracts.MyDisk;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IResourcesProvider
    {
        /// <summary>
        /// Изменить демо период для Аренды 1С и начислить свободные ресурсы 
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель изменения аренды 1С</param>
        void ChangeRent1CDemoPeriodExpireDateAndRegisterDemoResources(Guid accountId, Rent1CServiceManagerDto model);

        /// <summary>
        /// Изменить стоимость ресурса
        /// </summary>
        /// <param name="rent1CUserManagerDc">Модель управления ресурсом</param>
        /// <param name="accountId">Id аккаунта</param>
        void EditResourceCost(Rent1CUserManagerDto rent1CUserManagerDc, Guid accountId);

        /// <summary>
        /// Получить модель управления ресурсом
        /// </summary>
        /// <param name="resourceId">Id ресурса</param>
        /// <returns>Модель управления ресурсом</returns>
        Rent1CUserManagerDto GetRent1CUserManagerDc(Guid resourceId);

        /// <summary>
        /// Получить информацию о демо периоде аренды 1С
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о демо периоде аренды 1С</returns>
        Rent1CServiceManagerDto GetRent1DemoPeriodInfo(Guid accountId);

        /// <summary>
        /// Получить ресурс
        /// </summary>
        /// <param name="resourceId">Id ресурса</param>
        /// <returns>Ресурс</returns>
        Resource GetResource(Guid resourceId);

        /// <summary>
        /// Получить модель управления сервисом "Мой диск"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель управления сервисом "Мой диск"</returns>
        ServiceMyDiskManagerDto GetServiceMyDiskManagerInfo(Guid accountId);
    }
}