﻿using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IProvidedServiceHelper
    {
        void RegisterServiceActivation(Guid accountId, Clouds42Service service, DateTime? serviceExpireDate, ResourceType resourceType,
            KindOfServiceProvisionEnum kind, int freeResourceCount = 1);
        void RegisterSinglePay(Guid accountId, ResourcesConfiguration resConf,
            ResourceType resourceType, decimal rate);
        void RegisterPayDisk(Guid accountId, ResourcesConfiguration resConf, decimal rate);
        void RegisterRent1CChanges(Guid accountId, ResourcesConfiguration resConf,
            AccountBillingDataForBuyRent1CDto buyRent1CData);
        void RegisterServiceProlongation(ResourcesConfiguration resConfig);
        void ChangeRent1CExpireDate(ResourcesConfiguration resConfig, DateTime newDateToValue);

    }
}
