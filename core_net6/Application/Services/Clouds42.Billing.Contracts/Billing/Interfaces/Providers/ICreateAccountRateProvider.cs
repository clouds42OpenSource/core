﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для создания тарифа услуги на аккаунт
    /// </summary>
    public interface ICreateAccountRateProvider
    {
        /// <summary>
        /// Создать тариф услуги на аккаунт
        /// </summary>
        /// <param name="model">Модель создания тарифа на аккаунт</param>
        void Create(CreateAccountRateDto model);
    }
}
