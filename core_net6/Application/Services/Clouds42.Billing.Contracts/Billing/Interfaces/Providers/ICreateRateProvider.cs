﻿namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для создания тарифа для услуг
    /// </summary>
    public interface ICreateRateProvider
    {
        /// <summary>
        /// Создать тариф для услуги
        /// </summary>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <param name="serviceTypeCost">Стоимость услуги</param>
        /// <param name="localeId">Id локали</param>
        void Create(Guid serviceTypeId, decimal serviceTypeCost, Guid? localeId = null);
    }
}
