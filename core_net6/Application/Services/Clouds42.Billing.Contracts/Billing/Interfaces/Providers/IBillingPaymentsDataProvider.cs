﻿using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IBillingPaymentsDataProvider
    {
        /// <summary>
        /// Аккаунт может взять обещанный платеж.
        /// </summary>
        /// <param name="accountId">Номер аккаунта.</param>
        /// <returns>true - аккаунт может вязть ОП и нет в проивном случае.</returns>
        bool CanGetPromisePayment(Guid accountId);

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountUid">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param>
        /// <returns>Баланс аккаунта</returns>
        decimal GetBalance(Guid accountUid, TransactionType transactionType = TransactionType.Money);

        /// <summary>
        /// Получить список платежей
        /// </summary>
        /// <param name="account">ID аккаунта</param>
        /// <returns>Список платежей</returns>
        List<PaymentDefinitionDto> GetPaymentsList(Guid account);

        /// <summary>
        /// Получить дату взятия ОП
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Дата взятия ОП</returns>
        DateTime? GetPromiseDateTime(Guid accountId);

        /// <summary>
        /// Получить сумму ОП
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Сумма ОП</returns>
        decimal? GetPromisePaymentCost(Guid accountId);

        /// <summary>
        /// Получить номер платежа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="sum">Сумма</param>
        /// <param name="paymentSystem">Платежная система</param>
        /// <returns>Номер платежа</returns>
        int PreparePayment(Guid accountId, decimal sum, PaymentSystem paymentSystem);

        /// <summary>
        /// Пересчет баланса компании. Вызывается после каждой платежной операции.
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакций</param> 
        void RecalculateAccountTotals(Guid accountId, TransactionType transactionType = TransactionType.Money);
    }
}