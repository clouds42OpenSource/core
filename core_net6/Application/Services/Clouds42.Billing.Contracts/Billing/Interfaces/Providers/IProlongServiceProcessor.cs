﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{

    /// <summary>
    /// Обработчик продления сервиса.
    /// </summary>
    public interface IProlongServiceProcessor
    {

        /// <summary>
        /// Пролонгировать сервис.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="prolongConditionFunc">Условие выполнения пролонгации сервиса.</param>
        void ProlongService(ResourcesConfiguration resourcesConfiguration,
            Func<ResourcesConfiguration, bool> prolongConditionFunc);

        /// <summary>
        /// Проверить возможность продления сервиса.
        /// </summary>
        /// <param name="resourcesConfiguration">Ресурс конфигурация сервиса.</param>
        /// <param name="reasonMsg">Текст причины не возможности пролонгации.</param>
        /// <returns>Признак возможности пролонгации.</returns>
        bool CanProlongServiceWithDependencies(ResourcesConfiguration resourcesConfiguration, out string? reasonMsg);

    }
}
