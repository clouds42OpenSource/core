﻿using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для обновления тарифа услуги на аккаунт
    /// </summary>
    public interface IUpdateAccountRateProvider
    {
        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// </summary>
        /// <param name="model">Модель обновления тарифа на аккаунт</param>
        void Update(UpdateAccountRateDto model);

        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// </summary>
        /// <param name="accountRate">Тариф на аккант</param>
        /// <param name="cost">Новая стоимость тарифа</param>
        void Update(AccountRate accountRate, decimal cost);

        /// <summary>
        /// Обновить тариф услуги на аккаунт
        /// если он существует, иначе создать
        /// </summary>
        /// <param name="model">Модель обновления тарифа на аккаунт</param>
        void UpdateIfAvailableOtherwiseCreate(UpdateAccountRateDto model);
    }
}
