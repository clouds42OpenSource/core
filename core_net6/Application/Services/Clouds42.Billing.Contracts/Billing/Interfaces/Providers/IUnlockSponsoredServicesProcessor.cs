﻿using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Обработчик продления заблокированных сервисов у спонсируемых аккаунтов.
    /// </summary>
    public interface IUnlockSponsoredServicesProcessor
    {

        /// <summary>
        /// Выполнить обработку.
        /// </summary>
        /// <param name="configuration">Ресурс конфигурация сервиса.</param>
        void Process(ResourcesConfiguration configuration);
    }
}