﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IResourceConfigurationDataProvider
    {
        ResourcesConfiguration GetResourceConfigurationOrThrowException(Guid billingServiceId, Guid accountId);
        ResourcesConfiguration GetResourceConfigurationOrThrowException(IBillingService billingService, Guid accountId);
        ResourcesConfiguration GetResourceConfigurationOrThrowException(Guid accountId, Clouds42Service service);
        ResourcesConfiguration GetResourceConfiguration(Guid accountId, Clouds42Service service);
        decimal GetTotalServiceCost(ResourcesConfiguration resourceConfiguration);
        decimal GetTotalServiceCost(ResConfigDependenciesModel resConfModel);
        ResConfigDependenciesModel GetConfigWithDependencies(ResourcesConfiguration resourceConfiguration);
        ResConfigDependenciesModel GetConfigWithDependenciesIncludingDemo(ResourcesConfiguration resourceConfiguration);
        string GetServiceNames(ResourcesConfiguration resConf);
        DateTime GetExpireDateOrThrowException(ResourcesConfiguration resourcesConfiguration);
        DateTime? GetExpireDate(ResourcesConfiguration resourcesConfiguration);

    }
}
