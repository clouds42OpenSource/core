﻿using Clouds42.DataContracts.Account.AccountModels;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер обработки принятия счетов на оплату.
    /// </summary>
    public interface IInvoiceConfirmationProvider
    {

        /// <summary>
        /// Подтвердить счет на оплату.
        /// </summary>
        /// <param name="confirmationInvoice"></param>
        void ConfirmInvoice(ConfirmationInvoiceDto confirmationInvoice);
    }
}