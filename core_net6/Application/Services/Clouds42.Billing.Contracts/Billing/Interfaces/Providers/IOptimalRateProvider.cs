﻿using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с оптимальным тарифом
    /// </summary>
    public interface IOptimalRateProvider
    {
        /// <summary>
        /// Получить оптимальный тариф для услуги
        /// (с учетом тарифа на аккаунт)
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="serviceTypeId">Id услуги</param>
        /// <returns>Оптимальный тариф на услугу</returns>
        OptimalRateDataDcDto GetOptimalRate(BillingAccount billingAccount, Guid serviceTypeId);

        /// <summary>
        /// Получить оптимальный тариф для услуги
        /// (с учетом тарифа на аккаунт)
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        /// <returns>Оптимальный тариф на услугу</returns>
        OptimalRateDataDcDto GetOptimalRate(BillingAccount billingAccount, ResourceType systemServiceType);
    }
}
