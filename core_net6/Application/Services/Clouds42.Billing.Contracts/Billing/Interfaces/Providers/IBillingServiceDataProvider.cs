﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IBillingServiceDataProvider
    {
        IBillingService GetSystemService(Clouds42Service systemServiceType);
        IBillingService GetBillingServiceOrThrowException(Guid serviceId);
        IBillingServiceType GetSystemServiceTypeOrThrowException(ResourceType resourceType);
    }
}
