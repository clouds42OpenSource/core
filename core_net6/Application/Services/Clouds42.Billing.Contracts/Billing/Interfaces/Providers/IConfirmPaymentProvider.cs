﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    /// <summary>
    /// Провайдер подтверждения платежей
    /// </summary>
    public interface IConfirmPaymentProvider
    {
        /// <summary>
        /// Подтвердить платеж.
        /// </summary>
        /// <param name="num">Номер платежа</param>
        /// <param name="sum">Сумма платежа</param>
        /// <param name="accepted">Принятый</param>
        /// <returns>Результат платежной операции</returns>
        PaymentOperationResult ConfirmPayment(int num, decimal sum, bool accepted);
    }
}
