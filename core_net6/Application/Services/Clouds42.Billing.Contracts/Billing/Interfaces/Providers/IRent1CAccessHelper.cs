﻿using Clouds42.DataContracts.ActiveDirectory;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.IDataModels;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IRent1CAccessHelper
    {
        /// <summary>
        /// Проверить возможность оплатить лицензии сервиса Аренда 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="changeRent1CAccesses">Изменения доступов к Аренде 1С</param>
        /// <param name="result">Результат обновления лицензий</param>
        /// <returns>Возможность оплатить лицензии сервиса Аренда 1С</returns>
        bool CanPayRent1CAccesses(Guid accountId, List<ChangeRent1CAccessDto> changeRent1CAccesses, out UpdateAccessRent1CResultDto result);

        /// <summary>
        /// Поменять ресурсы Аренды 1С у аккаунта на список новых.
        /// </summary>
        /// <param name="account">Аккаунт.</param>        
        /// <param name="resources">Список новых подключений к лицензиям аренды 1С.</param>
        /// <returns></returns>
        List<EditUserGroupsAdModelDto> ChangeRen1CResourcesOfAccount(IAccount account, List<ChangeRent1CAccessDto> resources);

        /// <summary>
        /// Получить данные биллинга аккаунта для покупки сервиса Аренда 1С
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <returns>Данные биллинга аккаунта для покупки сервиса Аренда 1С</returns>
        AccountBillingDataForBuyRent1CDto GetAccountBillingDataForBuyRent1C(Guid accountUserId);

        /// <summary>
        /// Получить данные биллинга аккаунта для покупки сервиса Аренда 1С
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <param name="expireDateOfService">Дата пролонгации сервиса</param>
        /// <returns>Данные биллинга аккаунта для покупки сервиса Аренда 1С</returns>
        AccountBillingDataForBuyRent1CDto GetAccountBillingDataForBuyRent1C(IAccount account, DateTime expireDateOfService);

        /// <summary>
        /// Найти внешнего пользователя(пользователя другого аккаунта)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="email">Электронная поста</param>
        /// <returns>Внешний пользователь</returns>
        SearchExternalUserRent1CResultDto SearchExternalUser(Guid accountId, string email);
    }
}