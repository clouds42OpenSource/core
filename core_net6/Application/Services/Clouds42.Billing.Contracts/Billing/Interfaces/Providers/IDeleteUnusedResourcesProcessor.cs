﻿using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Billing.Interfaces.Providers
{
    public interface IDeleteUnusedResourcesProcessor
    {
        /// <summary>
        /// Удалить неиспользуемые ресурсы и перерасчитать стоимость сервиса. 
        /// </summary>  
        void Process(IResourcesConfiguration service);

        /// <summary>
        /// Удалить неиспользуемые ресурсы и перерасчитать стоимость для сервиса
        /// и зависимых от него сервисов
        /// </summary>
        /// <param name="resConfModel">Запись о сервисе и его зависимых сервисах</param>
        void Process(ResConfigDependenciesModel resConfModel);
    }
}