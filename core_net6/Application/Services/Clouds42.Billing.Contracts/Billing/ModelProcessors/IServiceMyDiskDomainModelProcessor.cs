﻿using Clouds42.DataContracts.MyDisk;

namespace Clouds42.Billing.Contracts.Billing.ModelProcessors
{
    public interface IServiceMyDiskDomainModelProcessor
    {


        /// <summary>
        /// Получить модель сервиса Мой диск
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель сервиса Мой диск</returns>
        ServiceMyDiskDto GetModel(Guid accountId);
    }
}