﻿using Clouds42.AgencyAgreement.Contracts.DocumentBuilder;
using Clouds42.Billing.Contracts.Billing.Models;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Billing.Managers
{
    public interface IBillingAccountManager
    {
        /// <summary>
        /// Построить pdf документ счета на оплату
        /// </summary>
        /// <param name="invoiceId">ID счета на оплату</param>
        /// <returns>pdf документ счета на оплату</returns>
        ManagerResult<DocumentBuilderResult> BuildInvoicePdfDocument(Guid invoiceId);

        /// <summary>
        /// Создать счет на основании модели калькуляции счета
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="calculationOfInvoiceRequest">Модель запроса на калькуляцию счета</param>
        /// <returns>Идентификатор созданного счёта</returns>
        ManagerResult<Guid> CreateInvoiceBasedOnCalculationInvoiceModel(Guid accountId, CalculationOfInvoiceRequestModel calculationOfInvoiceRequest);

        /// <summary>
        /// Создать счёт для аккаунта на указанную сумму
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="sum">Сумма, на которую будет выставлен счёт</param>
        /// <param name="period">Период</param>
        /// <returns>Идентификатор созданного счёта</returns>
        ManagerResult<Guid> CreateInvoiceForSpecifiedInvoiceAmount(Guid accountId, decimal sum, int? period = null);

        /// <summary>
        /// Создать обещанный платеж
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="model">Модель запроса на калькуляцию счета</param>
        /// <returns>Успешность создания</returns>
        ManagerResult<bool> CreatePromisePayment(Guid accountId, CalculationOfInvoiceRequestModel model);

        /// <summary>
        /// Получить баланс аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Баланс аккаунта</returns>
        ManagerResult<decimal> GetAccountBalance(Guid accountId);

        /// <summary>
        /// Получить данные биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для аккаунта</returns>
        ManagerResult<BillingAccountInfoDto> GetBillingData(Guid accountId);

        /// <summary>
        /// Получить данные биллинга для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для аккаунта</returns>
        ManagerResult<BillingDataDto> GetBillingDataV2(Guid accountId);

        /// <summary>
        /// Получить модель DC счета
        /// </summary>
        /// <param name="id">Id счета</param>
        /// <returns>Модель DC счета</returns>
        ManagerResult<InvoiceDc> GetInvoiceDc(Guid id);

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        ManagerResult<PagitationCollection<InvoiceInfoDto>> GetInvoicesForAccount(Guid accountId, int page);

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали фильтрации и пагинации</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        ManagerResult<SelectDataResultCommonDto<InvoiceInfoDto>> GetPaginatedInvoicesForAccount(Guid accountId, SelectDataCommonDto<InvoicesFilterDto> request);

        /// <summary>
        /// Получить системный сервис биллинга
        /// </summary>
        /// <param name="clouds42Service">Тип системного сервиса</param>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Системный сервис биллинга</returns>
        ManagerResult<IBillingService> GetSystemBillingService(Clouds42Service clouds42Service, Guid accountId);

        /// <summary>
        /// Продлить обещанный платёж
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
        ManagerResult<bool> ProlongPromisePayment(BillingAccount billingAccount);

        /// <summary>
        /// Преждeвременное погашение обещанного платежа
        /// </summary>
        /// <param name="billingAccount"></param>
        /// <returns></returns>
        ManagerResult<bool> PromisePaymentEarlyRepay(BillingAccount billingAccount);

        /// <summary>
        /// Отправка письма в случае успешной оплаты
        /// </summary>
        /// <param name="paymenNumber">Номер платежа</param>
        /// <returns>Результат операции</returns>
        ManagerResult SendRobokassaSuccessImportReport(int paymenNumber);

        /// <summary>
        /// Отменить счет на оплату
        /// </summary>
        /// <param name="invoiceId">Номер счета</param>
        /// <returns>true - если счет отменен</returns>
        ManagerResult CancelInvoice(Guid invoiceId);
    }
}
