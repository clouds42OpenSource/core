﻿namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors
{
    /// <summary>
    /// Обработчик начисления агентского вознаграждения.
    /// </summary>
    public interface IIncreaseAgentPaymentProcessor
    {
        /// <summary>
        /// Обработать начисления агентского вознаграждения.
        /// </summary>
        /// <param name="paymentId">Номер клиентского платежа.</param>
        void Process(Guid paymentId);

        /// <summary>
        /// Обработать начисления агентского вознаграждения.
        /// </summary>
        /// <param name="payments">Номера клиентских платежей.</param>
        void Process(List<Guid> payments);
    }
}
