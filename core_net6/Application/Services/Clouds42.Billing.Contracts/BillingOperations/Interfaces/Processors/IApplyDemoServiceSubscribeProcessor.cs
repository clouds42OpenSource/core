﻿using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.DataContracts.BillingService;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Processors
{
    /// <summary>
    /// Обработчик по принятию демо подписки сервиса.
    /// </summary>
    public interface IApplyDemoServiceSubscribeProcessor
    {
        /// <summary>
        /// Принять подписку демо сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usePromisePayment">Покупка за счет обещанного платежа.</param>
        ApplyDemoServiceSubscribeResultDto Apply(Guid serviceId, Guid accountId, bool usePromisePayment);


        /// <summary>
        /// Получить информацию о оплате сервиса.
        /// </summary>
        /// <param name="serviceId">ID сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        BillingServiceAmountDataModelDto GetBillingServiceAmountData(Guid serviceId, Guid accountId);

        void SetPromisePaymentProvider(IPromisePaymentProvider promisePaymentProvider);
    }
}
