﻿using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для подготовки платежа агрегатора ЮKassa
    /// </summary>
    public interface IPayYookassaAggregatorPaymentProvider
    {
        /// <summary>
        /// Подготовить платеж агрегатора ЮKassa для клиента
        /// </summary>
        /// <param name="model">Модель для подготовки платежа агрегатора ЮKassa</param>
        /// <returns>Данные для подтверждения платежа ЮKassa клиентом</returns>
        ConfirmYookassaPaymentForClientDataDto PreparePaymentOrPayWithSavedPaymentMethod(PrepareYookassaAggregatorPaymentDto model);
    }
}
