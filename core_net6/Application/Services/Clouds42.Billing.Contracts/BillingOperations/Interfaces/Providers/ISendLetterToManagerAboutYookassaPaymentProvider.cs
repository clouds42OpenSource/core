﻿using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для отправки письма менеджеру
    /// о совершении оплаты через ЮKassa
    /// </summary>
    public interface ISendLetterToManagerAboutYookassaPaymentProvider
    {
        /// <summary>
        /// Отправить письмо менеджеру о совершении оплаты
        /// через ЮKassa
        /// </summary>
        /// <param name="model">Модель данных для отправки письма менеджеру
        /// о совершении оплаты через ЮKassa</param>
        void SendLetter(SendLetterToManagerAboutYookassaPaymentDto model);
    }
}
