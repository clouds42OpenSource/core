﻿using Clouds42.DataContracts.Account.AccountBilling;
using Clouds42.DataContracts.Account.AccountBilling.Fillers;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер данных биллинга
    /// </summary>
    public interface IBillingDataProvider
    {
        /// <summary>
        /// Получить регулярный платеж аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Регулярный платеж аккаунта</returns>
        decimal GetAccountRegularPayment(Guid accountId);

        /// <summary>
        /// Получить список счетов на оплату аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        PagitationCollection<InvoiceInfoDto> GetInvoicesForAccount(Guid accountId, int page = 1);


        /// <summary>
        /// Получить список счетов на оплату аккаунта V2
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="page">Номер страницы</param>
        /// <returns>Список счетов на оплату аккаунта</returns>
        SelectDataResultCommonDto<InvoiceInfoDto> GetPaginatedInvoicesForAccount(Guid accountId, SelectDataCommonDto<InvoicesFilterDto> request);

        /// <summary>
        /// Получить список транзакций аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список транзакций аккаунта</returns>
        BillingOperationDataDto GetTransactionsPagers(Guid accountId, ProvidedServiceAndTransactionsFilterDto filter);

        /// <summary>
        /// Получить список транзакций аккаунта v2
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Фильтр поиска</param>
        /// <returns>Список транзакций аккаунта</returns>
        SelectDataResultCommonDto<BillingOperationItemDto> GetPaginatedTransactionsForAccount(Guid accountId,
            SelectDataCommonDto<PaymentsFilterDto> request);

        /// <summary>
        /// Получить данные для формы пополнения баланса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак, что используется ОП</param>
        /// <param name="suggestedPaymentModel">Модель предлагаемого платежа</param>
        /// <returns>Данные для формы пополнения баланса</returns>
        ReplenishBalanceInfoDto GetDataForReplenishBalance(Guid accountId, bool isPromisePayment,
            SuggestedPaymentModelDto suggestedPaymentModel);
    }
}
