﻿namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для проверки статуса платежа агрегатора ЮKassa
    /// Получает статус платежа по API ЮKassa и вызывает
    /// обработку события на изменение статуса
    /// </summary>
    public interface IFinalizeYookassaAggregatorPaymentProvider
    {
        /// <summary>
        /// Проверить изменение статуса платежа агрегатора ЮKassa
        /// При изменении вызовится обработчик события на изменение статуса
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа агрегатора ЮKassa</param>
        /// <returns>Результат проверки изменения статуса</returns>
        bool FinalizePayment(Guid yookassaPaymentId);
    }
}
