﻿namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для запуска задачи по проверке статуса платежа агрегатора ЮKassa
    /// </summary>
    public interface IRunTaskToCheckYookassaPaymentStatusProvider
    {
        /// <summary>
        /// Запустить задачу по проверке
        /// статуса платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="paymentId">Id системного платежа</param>
        void RunTask(Guid paymentId);
    }
}
