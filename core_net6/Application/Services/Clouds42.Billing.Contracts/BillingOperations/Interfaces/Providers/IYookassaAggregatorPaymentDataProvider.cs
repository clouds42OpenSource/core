﻿using Clouds42.DataContracts.Billing.YookassaAggregator;
using Clouds42.Domain.DataModels.AccountPaymentsMethods;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными платежей агрегатора ЮKassa
    /// </summary>
    public interface IYookassaAggregatorPaymentDataProvider
    {
        IEnumerable<Domain.DataModels.billing.Payment> GetPendingPayments(DateTime periodFrom, DateTime periodTo);

        /// <summary>
        /// Получить платеж системы по Id платежа ЮKassa
        /// или выкинуть исключение
        /// </summary>
        /// <param name="yookassaPaymentId">Id платежа ЮKassa</param>
        /// <returns>Платеж системы</returns>
        Domain.DataModels.billing.Payment GetPaymentByYookassaPaymentIdOrThrowException(Guid yookassaPaymentId);

        /// <summary>
        /// Получить сервис по Id 
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Сервис</returns>
        BillingService GetService(Guid? serviceId);

        /// <summary>
        /// Получить идентификатор поставщика 
        /// </summary>
        /// <param name="supplierName">название поставщика</param>
        /// <returns>идентификатор поставщика</returns>
        Guid GetSupplier(string supplierName);

        /// <summary>
        /// Сохраняет данные о способе оплаты пользователя
        /// </summary>
        /// <param name="model">Сохраненный способ оплаты</param>
        /// <param name="yookassaPaymentId">Идентификатор платежа</param>
        /// <returns>Сохраненный способ оплаты</returns>
        SavedPaymentMethod? SaveYookassaAccountPaymentMethodAndBankCardInfoIfExist(YookassaPaymentMethodDto model, Guid yookassaPaymentId);
    }
}
