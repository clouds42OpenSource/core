﻿using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    public interface IYookassaAggregatorPaymentFinalizerProvider
    {
        bool FinalizePayment(YookassaFinalPaymentObjectDto paymentObject);
    }
}
