﻿namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{

    /// <summary>
    /// Провайдер для активации обещанного платежа
    /// </summary>
    public interface IPromisedPaymentActivationProvider
    {
        /// <summary>
        /// Активировать обещанный платеж для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <param name="prolongServices">Нужно ли продливать сервисы после успешной активации</param>
        /// <returns></returns>
        Task ActivatePromisedPaymentAsync(Guid accountId, Guid invoiceId, bool prolongServices = true);

        /// <summary>
        /// Увеличение обещанного платежа для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <param name="prolongServices">Нужно ли продливать сервисы после успешной активации</param>
        /// <returns></returns>
        Task IncreasePromisedPaymentAsync(Guid accountId, Guid invoiceId, bool prolongServices = true);
    }
}
