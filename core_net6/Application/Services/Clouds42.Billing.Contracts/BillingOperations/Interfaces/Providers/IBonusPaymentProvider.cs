﻿using Clouds42.Domain.Enums;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Обработчик бонусных платежей
    /// </summary>
    public interface IBonusPaymentProvider
    {
        /// <summary>
        /// Создать входящий бонусный платеж
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoice">Счет на оплату</param>
        PaymentOperationResult CreateInflowBonusPayment(Guid accountId, InvoiceEntity invoice);

        /// <summary>
        /// Создать исходящий бонусный платеж
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        PaymentOperationResult CreateOutflowBonusPayment(InvoiceEntity invoice);
    }
}
