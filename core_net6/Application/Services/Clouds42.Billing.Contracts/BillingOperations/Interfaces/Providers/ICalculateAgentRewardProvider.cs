﻿using Clouds42.DataContracts.Billing.AgencyAgreement;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для калькуляции агентского вознаграждения
    /// </summary>
    public interface ICalculateAgentRewardProvider
    {
        /// <summary>
        /// Посчитать агентское вознаграждение
        /// </summary>
        /// <param name="clientPaymentId">Номер клиенской платежной транзакции,
        /// на основание данного платежа будет производится начисление агенту.</param>
        /// <returns>Результат расчета агентского вознаграждения</returns>
        CalculateAgentRewardResultDto Calculate(Guid clientPaymentId);

        /// <summary>
        /// Посчитать агентское вознаграждение
        /// </summary>
        /// <param name="paymentSum">Сумма платежа</param>
        /// <param name="isVipClientAccount">Аккаунт клиента является ВИП</param>
        /// <param name="systemService">Тип системного сервиса</param>
        /// <returns>Результат расчета агентского вознаграждения</returns>
        decimal Calculate(decimal paymentSum, bool isVipClientAccount, Clouds42Service? systemService);
    }
}
