﻿using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для создания шаблона платежа агрегатора ЮKassa
    /// </summary>
    public interface ICreateYookassaAggregatorPaymentTemplateProvider
    {
        /// <summary>
        /// Создать шаблона платежа агрегатора ЮKassa
        /// </summary>
        /// <param name="model">Метод платежа</param>
        /// <param name="aggregatorPaymentId">Id платежа агрегатора ЮKassa</param>
        void Create(YookassaPaymentMethodDto model, int aggregatorPaymentId);
    }
}
