﻿using Clouds42.DataContracts.Billing.Payments;
using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер платежей биллинга
    /// </summary>
    public interface IBillingPaymentsProvider
    {
        /// <summary>
        /// Получить доступный баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Доступный баланс аккаунта</returns>
        decimal GetAccountAvailableBalance(Guid accountId);

        /// <summary>
        /// Провести транзакции по описанию платежа
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Результат проведения транзакций</returns>
        CreatePaymentResultDto MakePayment(PaymentDefinitionDto paymentDefinition);

        /// <summary>
        /// Создать платеж
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <returns>Результат операции</returns>
        PaymentOperationResult CreatePayment(PaymentDefinitionDto paymentDefinition);

        /// <summary>
        /// Добавить платеж
        /// </summary>
        /// <param name="payment">Модель платежа</param>
        /// <returns>Результат операции</returns>
        PaymentOperationResult AddPayment(Domain.DataModels.billing.Payment payment);

        /// <summary>
        /// Установить данные для обещанного платежа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="date">Дата взятия ОП</param>
        /// <param name="sum">Сумма ОП</param>
        void SetPromisePaymentCredentials(Guid accountId, DateTime date, decimal sum);

        /// <summary>
        /// Пересчитать баланс аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="transactionType">Тип транзакции (денежная или бонусная)</param>
        void RecalculateAccountTotals(Guid accountId, TransactionType transactionType = TransactionType.Money);

        /// <summary>
        /// Проверить возможность создания денежной проводки.
        /// </summary>
        /// <param name="paymentDefinition">Описание платежа</param>
        /// <param name="needMoney">Сумма денег которой не хватает для проведения.</param>
        /// <returns>Результат проведения транзакции.</returns>
        PaymentOperationResult CanCreatePayment(PaymentDefinitionDto paymentDefinition, out decimal needMoney);

        /// <summary>
        /// Проверить возможность создания денежной проводки.
        /// </summary>
        /// <param name="accountId">Номер аккантуа.</param>
        /// <param name="paymentSum">Сумма платежа.</param>
        /// <param name="needMoney">Сумма денег которой не хватает для проведения.</param>
        /// <returns>Результат проведения транзакции.</returns>
        PaymentOperationResult CanCreatePayment(Guid accountId, decimal paymentSum, out decimal needMoney);
    }
}
