﻿using Clouds42.DataContracts.Billing.AgencyAgreement;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными калькуляции вознаграждения агента
    /// </summary>
    public interface ICalculateAgentRewardDataProvider
    {
        /// <summary>
        /// Получить данные для калькуляции агентского вознаграждения
        /// </summary>
        /// <param name="clientPaymentId">Id клиенской платежной транзакции</param>
        /// <returns>Данные для калькуляции агентского вознаграждения</returns>
        CalculateAgentRewardDataDto GetDataFoCalculateAgentReward(Guid clientPaymentId);
    }
}
