﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers
{
    public interface ICheckYookassaAggregatorPaymentStatusManager
    {
        ManagerResult CheckPaymentStatusChange(Guid yookassaPaymentId);
    }
}
