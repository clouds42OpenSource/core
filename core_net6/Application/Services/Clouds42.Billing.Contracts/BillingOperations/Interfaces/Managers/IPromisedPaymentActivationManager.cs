﻿using Clouds42.Common.ManagersResults;

namespace Clouds42.Billing.Contracts.BillingOperations.Interfaces.Managers
{
    public interface IPromisedPaymentActivationManager
    {
        /// <summary>
        /// Активировать обещанный платеж для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <returns></returns>
        Task<ManagerResult> ActivatePromisedPaymentAsync(Guid accountId, Guid invoiceId);

        /// <summary>
        /// Увеличение обещанного платежа для аккаунта на основании инвойса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="invoiceId">ID исходного инвойса</param>
        /// <returns></returns>
        Task<ManagerResult> IncreasePromisedPaymentAsync(Guid accountId, Guid invoiceId);
    }
}
