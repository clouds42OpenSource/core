﻿namespace Clouds42.Billing.Contracts.Payment
{
    /// <summary>
    /// Информация о сохраненном платежном сопособе
    /// </summary>
    public class SavedPaymentMethodDto
    {
        /// <summary>
        /// Идентификатор сохраненного платежного способа
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Идентификатор пользотвателя которому принадлежит
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Дата сохранения
        /// </summary>
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// Оглавление
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// Название сохраненного способа оплаты автоплатежа
        /// </summary>
        public string PaymentName { get; set; }

        /// <summary>
        /// Тип (BankCard, YooMoney, SberPay)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Дополнительная информация о карте
        /// </summary>
        public SavedPaymentMethodBankCardDto? BankCardDetails { get; set; }

        /// <summary>
        /// Токен способа оплаты на стороне платежного агрегатора
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Метаданные
        /// </summary>
        public string Metadata { get; set; }

        /// <summary>
        /// Является ли способом оплаты по умолчанию для автосписаний
        /// </summary>
        public bool DefaultPaymentMethod { get; set; }
    }
}
