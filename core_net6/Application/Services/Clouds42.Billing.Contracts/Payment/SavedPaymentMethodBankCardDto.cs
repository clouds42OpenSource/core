﻿namespace Clouds42.Billing.Contracts.Payment
{
    /// <summary>
    /// Информация о карте сохраненного платежного метода
    /// </summary>
    public class SavedPaymentMethodBankCardDto
    {
        /// <summary>
        /// Первые 6 цифр
        /// </summary>
        public string FirstSixDigits { get; set; }

        /// <summary>
        /// Последние 4 цифры
        /// </summary>
        public string LastFourDigits { get; set; }

        /// <summary>
        /// Месяц окончания
        /// </summary>
        public string ExpiryMonth { get; set; }

        /// <summary>
        /// Год окончания
        /// </summary>
        public string ExpiryYear { get; set; }

        /// <summary>
        /// Тип карты
        /// </summary>
        public string CardType { get; set; }
    }
}
