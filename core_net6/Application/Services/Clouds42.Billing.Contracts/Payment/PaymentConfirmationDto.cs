﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.Payment
{
    public class PaymentConfirmationDto
    {
        public string? Token { get; set; }
        public string? Type { get; set; }
        public string? RedirectUrl { get; set; }
        public PaymentStatus? Status { get; set; }
        public Guid? PaymentId { get; set; }
    }
}
