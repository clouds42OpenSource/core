﻿using System.Xml.Serialization;
using Clouds42.Billing.Contracts.Factories.Models;
using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.Payment
{
    [XmlInclude(typeof(ConfirmYookassaPaymentForClientDataDto))]
    [XmlInclude(typeof(UkrPayInitDataModel))]
    public class PaymentServiceAggreagtorDataResult
    {
        /// <summary>
        /// Идентификатор платежа в 42clouds
        /// </summary>
        public Guid? PaymentId { get; set; }

        /// <summary>
        /// Платежный агрегатор
        /// </summary>
        public string PaymentSystem { get; set; }

        /// <summary>
        /// Специфичные данные платежного агрегатора для совершения платежа
        /// </summary>
        [XmlElement(ElementName = nameof(Data))]
        public object Data { get; set; }

        public PaymentServiceAggreagtorDataResult()
        {

        }

        public PaymentServiceAggreagtorDataResult(string paymentSystem, object data)
        {
            PaymentSystem = paymentSystem;
            Data = data;
        }

        public PaymentServiceAggreagtorDataResult(Guid paymentId, string paymentSystem, object data)
        {
            PaymentId = paymentId;
            PaymentSystem = paymentSystem;
            Data = data;
        }
    }
}
