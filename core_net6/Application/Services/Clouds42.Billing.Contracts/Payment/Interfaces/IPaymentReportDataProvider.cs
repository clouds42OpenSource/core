﻿using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Billing.Contracts.Payment.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными платежа для отчета
    /// </summary>
    public interface IPaymentReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по платежу для отчета
        /// </summary>
        /// <returns>Список моделей данных по платежу для отчета</returns>
        List<PaymentReportDataDto> GetPaymentReportDataDcs();
    }
}
