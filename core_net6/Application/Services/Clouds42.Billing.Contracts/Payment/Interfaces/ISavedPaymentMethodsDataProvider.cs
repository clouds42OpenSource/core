﻿using Clouds42.DataContracts.Billing.Payments;

namespace Clouds42.Billing.Contracts.Payment.Interfaces
{
    public interface ISavedPaymentMethodsDataProvider
    {
        /// <summary>
        /// Получить сохраненные способы оплат
        /// </summary>
        /// <param name="accountId">Фильтр по аккаунту</param>
        /// <returns>Сохраненные способы оплат</returns>
        Task<List<SavedPaymentMethodDto>> GetSavedPaymentMethodsAsync(Guid? accountId);


        /// <summary>
        /// Поулчить сохраненный способ оплаты
        /// </summary>
        /// <param name="id">Идентификатор способа оплаты</param>
        /// <returns>Поулчить сохраненный способ оплаты</returns>
        Task<SavedPaymentMethodDto?> GetSavedPaymentMethodAsync(Guid id);

        /// <summary>
        /// Удалить сохраненный способ оплат
        /// </summary>
        /// <param name="id">Идентификатор способа оплаты</param>
        /// <returns>Удачно/Неудачно</returns>
        Task<bool> DeleteSavedPaymentMethodAsync(Guid id);

        /// <summary>
        /// Установить по умолчанию способ для автосписываний
        /// </summary>
        /// <param name="model">Модель способа оплаты и аккаунта</param>
        /// <returns>Удачно/Неудачно</returns>
        bool MakeDefault(AccountPaymentMethodDto model);
    }
}
