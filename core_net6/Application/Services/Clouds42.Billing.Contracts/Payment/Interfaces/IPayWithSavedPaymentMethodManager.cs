﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Payments;
using Clouds42.DataContracts.Billing.YookassaAggregator;

namespace Clouds42.Billing.Contracts.Payment.Interfaces
{
    public interface IPayWithSavedPaymentMethodManager
    {
        Task<ManagerResult<PaymentResultDto>> PayWithSavedPaymentMethodAsync(PayWithSavedPaymentMethodDto model);
    }
}
