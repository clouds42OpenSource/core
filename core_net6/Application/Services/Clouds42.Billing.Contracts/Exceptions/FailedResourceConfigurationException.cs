﻿using System.Runtime.Serialization;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.Exceptions
{
    /// <summary>
    /// Неверный ресурс конфигурации сервиса
    /// </summary>
    [Serializable]
    public class FailedResourceConfigurationErrorsException : Exception
    {
        public ResourcesConfiguration ResourcesConfiguration { get; }
        public FailedResourceConfigurationErrorsException(string message, ResourcesConfiguration resourcesConfiguration) : base(message)
        {
            ResourcesConfiguration = resourcesConfiguration;
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected FailedResourceConfigurationErrorsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
