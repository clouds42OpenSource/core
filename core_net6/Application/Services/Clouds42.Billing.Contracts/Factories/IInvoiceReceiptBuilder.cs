﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Factories
{
    /// <summary>
    ///     Интерфейс создания фискального чека
    /// </summary>
    public interface IInvoiceReceiptBuilder
    {
        /// <summary>
        ///     Создать фискальный чек
        /// </summary>
        /// <param name="receiptInfo">Данные для получения чека</param>
        InvoiceReceiptDto Create(InvoiceReceiptDto receiptInfo);
    }
}