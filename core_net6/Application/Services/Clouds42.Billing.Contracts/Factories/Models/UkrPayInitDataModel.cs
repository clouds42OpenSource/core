﻿
namespace Clouds42.Billing.Contracts.Factories.Models
{
    public class UkrPayInitDataModel
    {
        public string ServiceId { get; set; }

        public string Successurl { get; set; }

        public string RequestUrl { get; set; }

        public string Order { get; set; }

        public decimal Amount { get; set; }

        public string Encoding { get; set; }
    }
}