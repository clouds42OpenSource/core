﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Billing.Contracts.Factories
{
    public interface IPreparePaymentModel<out TParam>
    {
        TParam PrepareModel(IUnitOfWork unitOfWork, IAccessProvider accessProvider, PreparePaymentStruct parameters, ILogger42 logger);
    }

    public class PreparePaymentStruct
    {
        public Guid AccountId { get; set; }
        public decimal Sum { get; set; }
        public PaymentSystem PaymentSystem { get; set; }
    }
}
