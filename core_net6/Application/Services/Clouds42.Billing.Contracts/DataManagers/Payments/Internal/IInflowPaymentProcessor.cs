﻿namespace Clouds42.Billing.Contracts.DataManagers.Payments.Internal
{
    /// <summary>
    /// Процессор для обработки входящих платежей
    /// </summary>
    public interface IInflowPaymentProcessor
    {
        /// <summary>
        /// Обработать входящий платеж.
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="needRepayPromisePayment">Признак определяющий нужно ли погасить обещаный платеж если присутствует</param>
        /// </summary>        
        void Process(Guid accountId, bool needRepayPromisePayment = true);
    }
}
