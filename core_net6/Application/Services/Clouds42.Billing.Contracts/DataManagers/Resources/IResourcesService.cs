﻿using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.DataManagers.Resources
{

    public interface IResourcesService
    {
        void AddOrResignResource(Guid accountId, ResourcesConfiguration resourceConfig, decimal rateCost, Guid subjectId, ResourceType resourceType, int? tag = null);
        ResourcesConfiguration CreateNewResourcesConfiguration(Guid accountId, IBillingService service, DateTime? resConfExpireDate = null);
        ResourcesConfiguration CreateNewResourcesConfigurationForDependentService(Guid accountId, IBillingService service);
        void CreateResource(Resource resource);
        void DeleteUnusedResources(Guid accountId, IBillingService service, bool isDemoService);
        BillingAccount GetAccountIfExistsOrCreateNew(Guid accountId);
        ResourcesConfiguration? GetResourceConfig(Guid accountId, Clouds42Service service);
        ResourcesConfiguration GetResourceConfig(Guid accountId, IBillingService service);
        List<Resource> GetResources(Guid accountId, Clouds42Service service, ResourceType? resourceType = null);
        ResourcesConfiguration GetResourcesConfigurationOrThrowException(Guid accountId, Guid serviceId);
        List<ResourcesConfiguration> GetResourcesConfigurations(Guid accountId);
    }
}
