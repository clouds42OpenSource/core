﻿using Clouds42.DataContracts.Billing.Rate;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.Billing.Contracts.DataManagers.Resources
{
    public interface IRateProvider
    {
        RateInfoBaseDc GetOptimalRate(Guid billingAccountId, Guid billingServiceTypeId);
        void UpdateAccountRates(Guid billingAccountId, IBillingService service, params RateChangeItem[] ratesToChange);
    }

    /// <summary>
    /// Модель для изменения тарифов
    /// </summary>
    public class RateChangeItem
    {
        /// <summary>
        /// идентификатор типа сервиса
        /// </summary>
        public Guid BillingServiceTypeId { get; set; }
        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Cost { get; set; }
    }
}