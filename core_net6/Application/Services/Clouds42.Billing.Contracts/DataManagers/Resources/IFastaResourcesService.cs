﻿using Clouds42.DataContracts.BaseModel;

namespace Clouds42.Billing.Contracts.DataManagers.Resources
{
    public interface IFastaResourcesService
    {
        BuyingEsdlResultModelDto BuyEsdlAndRecognition42(Guid accountId, int yearsAmount, int pagesAmount);
        BuyingEsdlResultModelDto BuyEsdl(Guid accountId, int yearsAmount);
        BuyingEsdlResultModelDto BuyRecognition42(Guid accountId, int pagesAmount);
    }
}