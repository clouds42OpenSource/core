﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Models
{
    /// <summary>
    /// Объект состояния расчета калькулятора
    /// </summary>
    /// <remarks>используется для передачи состояния между процессорами</remarks>
    public class ServiceCalculatorState
    {
        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Платежный период
        /// </summary>
        public int? PayPeriod { get; set; }

        /// <summary>
        /// Cтоимость одной единицы
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// Общая сумма бонусов
        /// </summary>
        public decimal TotalBonus { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this)) return true;

            if (obj is not ServiceCalculatorState other) return false;

            return
                other.Quantity == Quantity &&
                other.PayPeriod == PayPeriod &&
                other.Rate == Rate &&
                other.TotalPrice == TotalPrice &&
                other.TotalBonus == TotalBonus;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return new
            {
                Quantity,
                PayPeriod,
                Rate,
                TotalPrice,
                TotalBonus
            }.GetHashCode();
        }
    }
}
