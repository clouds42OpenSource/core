﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Generic Контроллер значений поля калькулятора
    /// </summary>
    /// <typeparam name="TValue">Тип контролирумого поля</typeparam>
    public interface ICalculatorControl<TValue>
    {
        /// <summary>
        /// Получить валидное значение исходя из предоставленного
        /// </summary>
        /// <param name="value">Предоставленное значение</param>
        /// <returns>Валидное значение</returns>
        TValue GetValidated(TValue value);

        /// <summary>
        /// Получить тип контроллера
        /// </summary>
        /// <returns>Тип контроллера</returns>
        string GetControlType();

        /// <summary>
        /// Получить настройки контроллера
        /// </summary>
        /// <returns>Коллекция Ключ-Значение с настройками контроллера</returns>
        IEnumerable<KeyValuePair<string, string>> GetControlSettings();
    }
}
