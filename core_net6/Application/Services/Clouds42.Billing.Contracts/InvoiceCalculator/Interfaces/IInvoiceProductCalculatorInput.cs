﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Может быть использован в роли входных параметров для калькулятора продукта инвойса
    /// </summary>
    public interface IInvoiceProductCalculatorInput : IHasProductIdentifier
    {
        /// <summary>
        /// Количество
        /// </summary>
        int Quantity { get; }

        /// <summary>
        /// Активен ли продукт
        /// </summary>
        bool IsActive { get; }
    }
}
