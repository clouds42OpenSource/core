﻿using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums;
using CommonLib.Enums;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Калькулятор услуги сервиса
    /// </summary>
    public interface IServiceCalculator
    {
        /// <summary>
        /// Идентификтора
        /// </summary>
        ServiceCalculatorIdentifier Identifier { get; }

        /// <summary>
        /// Платежный период
        /// </summary>
        int? PayPeriod { get; }

        /// <summary>
        /// Платное количество 
        /// </summary>
        int BillableQuantity { get; }

        /// <summary>
        /// Стоимость одной единицы
        /// </summary>
        decimal Rate { get; }

        /// <summary>
        /// Общая сумма бонусов
        /// </summary>
        decimal TotalBonus { get; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        decimal TotalPrice { get; }

        /// <summary>
        /// Беспланое количество
        /// </summary>
        int FreeQuantity { get; }

        /// <summary>
        /// Общее количество
        /// </summary>
        int TotalQuantity { get; }

        /// <summary>
        /// Имя калькулятора
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Описание калькулятора
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Тип ресурса системной услуги
        /// </summary>
        ResourceType? ResourceType { get; }

        /// <summary>
        /// Тип системного сервиса
        /// </summary>
        Clouds42Service? Clouds42Service { get; }

        /// <summary>
        /// Имя услуги в документе инвойса
        /// </summary>
        string NomenclatureName { get; }

        /// <summary>
        /// Описание услуги в документе инвойса
        /// </summary>
        string NomenclatureDescription { get; }

        /// <summary>
        /// ID услуги сервиса
        /// </summary>
        Guid? ServiceTypeId { get; }

        /// <summary>
        /// Активен ли сервис
        /// </summary>
        bool IsActiveService { get; }

        /// <summary>
        /// Контроллер платежного периода
        /// </summary>
        ICalculatorControl<int> PayPeriodControl { get; }

        /// <summary>
        /// Контроллер количества
        /// </summary>
        ICalculatorControl<int> QuantityControl { get; }

        /// <summary>
        /// Процессор расчетов
        /// </summary>
        ICalculationProcessor CalculationProcessor { get; }

        /// <summary>
        /// Пересчитать состояние согласно параметрам
        /// </summary>
        /// <param name="quantity">Количество</param>
        /// <param name="payPeriod">Платежный период</param>
        /// <returns>Произошло ли изменение состояния</returns>
        bool Set(int quantity, int? payPeriod);
    }
}