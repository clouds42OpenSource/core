﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Калькулятор инвойса. Содержит методы для построения расчета инвойса для аккаунта
    /// </summary>
    public interface IInvoiceCalculator
    {
        /// <summary>
        /// ID аккаунта
        /// </summary>
        Guid AccountId { get; }

        /// <summary>
        /// ID поставщика
        /// </summary>
        Guid SupplierId { get; }

        /// <summary>
        /// Имя покупателя
        /// </summary>
        string BuyerName { get; }

        /// <summary>
        /// Имя поставщика
        /// </summary>
        string SupplierName { get; }

        /// <summary>
        /// Код валюты
        /// </summary>
        int? CurrencyCode { get; }

        /// <summary>
        /// Имя валюты
        /// </summary>
        string Currency { get; }

        /// <summary>
        /// Платежный период
        /// </summary>
        int? PayPeriod { get; }

        /// <summary>
        /// Калькуляторы продуктов
        /// </summary>
        IEnumerable<IInvoiceProductCalculator> Products { get; }

        /// <summary>
        /// Расчитанная общая стоимость до НДС
        /// </summary>
        decimal TotalBeforeVAT { get; }

        /// <summary>
        /// Расчитанная сумма НДС
        /// </summary>
        decimal VAT { get; }

        /// <summary>
        /// Расчитнанная общая стоимость после НДС
        /// </summary>
        decimal TotalAfterVAT { get; }

        /// <summary>
        /// Расчитанная сумма бонусов
        /// </summary>
        decimal TotalBonus { get; }

        /// <summary>
        /// Контроллер платежного периода
        /// </summary>
        ICalculatorControl<int> PayPeriodControl { get; }

        /// <summary>
        /// Поменять состояние исходя из параметров
        /// </summary>
        /// <param name="payPeriod">Платежный период</param>
        /// <param name="productInputs">Входные параметры продуктов</param>
        /// <param name="disableOtherProducts">
        /// Определяет как обрабатывать продукты которых не было передано в пераметре inputs.
        /// Если true, отсутствующие продукты будут отключены. 
        /// Если false, отсутствующие продукты будут проигнорированы и останутся в последнем валидном состоянии</param>
        /// <returns>Были ли изменения в состоянии калькулятора</returns>
        bool Set(int? payPeriod, IEnumerable<IInvoiceProductCalculatorInput> productInputs, bool disableOtherProducts);

        /// <summary>
        /// Поменять платежный период
        /// </summary>
        /// <param name="payPeriod">Платежный период</param>
        /// <returns>Показывает были ли изменения в состоянии калькулятора</returns>
        bool SetPayPeriod(int? payPeriod);

        /// <summary>
        /// Поменять состояние исходя из параметров
        /// </summary>
        /// <param name="productInputs">Входные параметры продуктов</param>
        /// <param name="disableOtherProducts">
        /// Определяет как обрабатывать продукты которых не было передано в пераметре inputs.
        /// Если true, отсутствующие продукты будут отключены. 
        /// Если false, отсутствующие продукты будут проигнорированы и останутся в последнем валидном состоянии</param>
        /// <returns>Показывает были ли изменения в состоянии калькулятора</returns>
        bool SetProducts(IEnumerable<IInvoiceProductCalculatorInput> productInputs, bool disableOtherProducts);
    }
}
