﻿using Clouds42.Billing.Contracts.InvoiceCalculator.Models;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Процессор вычислений калькулятора сервиса
    /// </summary>
    public interface ICalculationProcessor
    {
        /// <summary>
        /// Произвести вычисления для состояния
        /// </summary>
        /// <param name="state">Объект состояния вычислений</param>
        void Process(ServiceCalculatorState state);
    }
}
