﻿using Clouds42.Billing.Contracts.InvoiceCalculator.structs;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    public interface IServiceCalculatorsProvider
    {
        /// <summary>
        /// Получить калькуляторы сервисов аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="typeFilter">Фильтр типов сервисов</param>
        /// <returns>Коллекция калькуляторов сервисов аккаунта</returns>
        IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorType>? typeFilter = null);


        /// <summary>
        /// Получить калькуляторы указанных сервисов аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceFilter">Фильтр сервисов</param>
        /// <returns>Коллекция калькуляторов сервисов аккаунта</returns>
        IEnumerable<IServiceCalculator> GetProductCalculatorForAccount(Guid accountId,
            IEnumerable<ServiceCalculatorIdentifier> serviceFilter);
    }
}
