﻿using Clouds42.Billing.Contracts.InvoiceCalculator.structs;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Используется для абстракции классов, у которых есть поле идентификатора сервиса
    /// </summary>
    public interface IHasProductIdentifier
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        ServiceCalculatorIdentifier Identifier { get; }
    }
}
