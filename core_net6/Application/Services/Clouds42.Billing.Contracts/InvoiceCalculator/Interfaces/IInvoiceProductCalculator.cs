﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Калькулятор продукта инвойса. Расширенная версия IServiceCalculator с возможностью пометки калькулятора как "Неактивный"
    /// </summary>
    public interface IInvoiceProductCalculator : IServiceCalculator
    {
        /// <summary>
        /// Поментка об активности калькулятора продукта
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Установить состояние активности
        /// </summary>
        /// <param name="isActive">Новое состояние активности</param>
        /// <returns>Поменялось ли состояние в результе операции</returns>
        bool SetIsActive(bool isActive);

        /// <summary>
        /// Поменять состояние калькулятора согласно параметрам
        /// </summary>
        /// <param name="quantity">Количество</param>
        /// <param name="payPeriod">Платежный период</param>
        /// <param name="isActive">Состояние активности калькулятора</param>
        /// <returns></returns>
        bool Set(int quantity, int? payPeriod, bool isActive);
    }
}