﻿namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    public interface IBonusRewardConfigurationProvider
    {
        IDictionary<int, decimal> GetPayPeriodToBonusRewardMap();
    }
}