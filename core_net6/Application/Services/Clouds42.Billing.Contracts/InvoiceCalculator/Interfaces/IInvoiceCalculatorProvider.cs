﻿using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    public interface IInvoiceCalculatorProvider
    {
        /// <summary>
        /// Получить калькулятор инвойсов для аккаунта
        /// </summary>
        /// <param name="accountId">ID калькулятора инвойсов</param>
        /// <param name="invoiceType">Тип инвойса</param>
        /// <returns>Калькулятор инвойсов</returns>
        IInvoiceCalculator GetInvoiceCalculatorForAccount(Guid accountId,
            InvoicePurposeType invoiceType);

        /// <summary>
        /// Получить калькулятор инвойсов для аккаунта с указанными сервисами и начальными данными продуктов
        /// Результирующий калькулятор будет содежрать продукты, для которых были предсотавленны начальные данные serviceInputs
        /// Предоставленные в serviceInputs продукты которые не относятся к указанному invoiceType будут исключены
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="payPeriod">Начальное значение периода оплаты калькулятора</param>
        /// <param name="serviceInputs">Начальные значения продуктов инвойса</param>
        /// <returns>Калькулятор инвойсов</returns>
        IInvoiceCalculator GetInvoiceCalculatorForAccount(Guid accountId,
            InvoicePurposeType invoiceType,
            int? payPeriod,
            IEnumerable<IInvoiceProductCalculatorInput> serviceInputs);
    }
}
