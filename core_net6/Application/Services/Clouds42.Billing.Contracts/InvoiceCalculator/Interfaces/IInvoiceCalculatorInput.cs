﻿using Clouds42.Domain.Enums;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.Interfaces
{
    /// <summary>
    /// Может быть использован для входных данных калькулятора инвойса
    /// </summary>
    public interface IInvoiceCalculatorInput
    {
        /// <summary>
        /// Платежнй период
        /// </summary>
        PayPeriod PayPeriod { get; }

        /// <summary>
        /// Входные данные продуктов инвойса
        /// </summary>
        IEnumerable<IInvoiceProductCalculatorInput> Products { get; }
    }
}