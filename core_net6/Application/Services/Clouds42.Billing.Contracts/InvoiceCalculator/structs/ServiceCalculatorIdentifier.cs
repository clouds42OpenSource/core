﻿using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.InvoiceCalculator.structs
{
    /// <summary>
    /// Идентификатор продукта инвойса
    /// </summary>
    public struct ServiceCalculatorIdentifier
    {
        public ServiceCalculatorIdentifier(ServiceCalculatorType serviceCategory, string? id = null)
        {
            Id = id ?? "";
            ServiceCalculatorType = serviceCategory;
        }
        public ServiceCalculatorIdentifier(ServiceCalculatorType productCategory, Guid id)
        {
            Id = id.ToString();
            ServiceCalculatorType = productCategory;
        }

        /// <summary>
        /// Тип продукта
        /// </summary>
        public ServiceCalculatorType ServiceCalculatorType { get; set; }

        /// <summary>
        /// Идентрификатор продукта внутри типа
        /// </summary>
        public string Id { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj) => obj is ServiceCalculatorIdentifier other && Equals(other);

        public bool Equals(ServiceCalculatorIdentifier other) =>
            ServiceCalculatorType == other.ServiceCalculatorType &&
            string.Equals(
                Id?.Trim() ?? string.Empty,
                other.Id?.Trim() ?? string.Empty,
                StringComparison.OrdinalIgnoreCase);

        /// <inheritdoc/>
        public override int GetHashCode() =>
            new { ServiceCalculatorType, Id = Id?.Trim() ?? string.Empty }
            .GetHashCode();

        public static bool operator ==(ServiceCalculatorIdentifier lhs, ServiceCalculatorIdentifier rhs) =>
            lhs.Equals(rhs);
        public static bool operator !=(ServiceCalculatorIdentifier lhs, ServiceCalculatorIdentifier rhs) =>
            !(lhs == rhs);

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{(int)ServiceCalculatorType}{IdentifierSeparator}{Id?.Trim() ?? string.Empty}";
        }

        /// <summary>
        /// Разелитель типа и id компонентов идентификатора в строчном представлении
        /// </summary>
        const char IdentifierSeparator = ':';

        /// <summary>
        /// Создать идентификатор из строчного представления
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public static ServiceCalculatorIdentifier Parse(string source)
        {
            var split = source.Split(IdentifierSeparator);
            if (split.Length != 2 || !int.TryParse(split[0], out var category))
                throw new InvalidOperationException($"Неверный формат идентификатора {source}");
            return new ServiceCalculatorIdentifier((ServiceCalculatorType)category, split[1]);
        }
    }
}
