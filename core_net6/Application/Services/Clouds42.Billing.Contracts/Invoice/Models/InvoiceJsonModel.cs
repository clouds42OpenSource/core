﻿namespace Clouds42.Billing.Contracts.Invoice.Models
{
    public class InvoiceJsonModel
    {
        public string Sum { get; set; }

        public string Description { get; set; }
    }
}