﻿using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными платежей агрегаторов
    /// </summary>
    public interface IAggregatorInvoicesDataProvider
    {
        /// <summary>
        /// Получить данные счетов агрегаторов
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <returns>Данные счетов агрегаторов</returns>
        List<AggregatorInvoiceWithSupplierCodeDataDto> GetAggregatorInvoicesData(DateTime dateFrom,
            DateTime dateTo);
    }
}
