﻿using Clouds42.DataContracts.Account.AccountBilling;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными счета на оплату
    /// </summary>
    public interface IInvoiceDataProvider
    {
        /// <summary>
        /// Получить данные счета на оплату
        /// (модель включает в себя все возможнные данные по счету,
        /// которые нужны для формирования и печати)
        /// </summary>
        /// <param name="invoice">Счет на оплату</param>
        /// <returns>Модель данных счета на оплату</returns>
        InvoiceDc GetInvoiceData(Domain.DataModels.billing.Invoice invoice);
    }
}
