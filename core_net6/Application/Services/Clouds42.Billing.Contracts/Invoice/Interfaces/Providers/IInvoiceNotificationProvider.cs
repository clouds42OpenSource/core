﻿namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Providers
{
    public interface IInvoiceNotificationProvider
    {
        void NotifyInvoiceCreated(Guid invoiceId, Guid recipientAccountUserId);
    }
}