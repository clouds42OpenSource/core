﻿using Clouds42.DataContracts.Billing.Inovice;
using InvoiceEntity = Clouds42.Domain.DataModels.billing.Invoice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Providers
{

    /// <summary>
    /// Провайдер создания инвойса из указанной модели
    /// </summary>
    /// <typeparam name="T">Тип модели инвойса</typeparam>
    public interface IInvoiceCreationProvider<T>
    {
        /// <summary>
        /// Создать инвойс на основании модели
        /// </summary>
        /// <param name="model">Модель инвойса</param>
        /// <returns>Созданный инвойс</returns>
        InvoiceEntity CreateInvoice(T model);

        /// <summary>
        /// Асинхронно создать инвойс на основании модели
        /// </summary>
        /// <param name="model">Модель инвойса</param>
        /// <returns>Созданный инвойс</returns>
        Task<InvoiceEntity> CreateInvoiceAsync(T model);
    }

    /// <summary>
    /// Стандартный провайдер создания инвойса
    /// </summary>
    public interface IInvoiceCreationProvider : IInvoiceCreationProvider<CreateInvoiceDto> { }
}