﻿using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Providers
{
    /// <summary>
    /// Провайдер для работы с данными счета для отчета 
    /// </summary>
    public interface IInvoiceReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по счету для отчета
        /// </summary>
        /// <returns>Список моделей данных по счету для отчета</returns>
        List<InvoiceReportDataDto> GetInvoiceReportDataDcs();
    }
}
