﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Managers
{
    /// <summary>
    /// Менеджер созданий инвойса
    /// </summary>
    public interface IInvoiceCreationManager
    {
        /// <summary>
        /// Создать инвойс на основании расчета калькулятора инвойсов
        /// </summary>
        /// <param name="calculation">Расчет калькулятора инвйосов</param>
        /// <param name="notifyAccountUserId">ID пользователя для отправки уведомления о создании инвойса</param>
        /// <returns>Результат операции с ID созданного инвойса</returns>
        ManagerResult<Guid> CreateInvoiceFromCalculation(
            InvoiceCalculationDto calculation,
            Guid? notifyAccountUserId = null);

        /// <summary>
        /// Создать инвойс на произвольную сумму
        /// </summary>
        /// <param name="request">Данные для создания инвойса</param>
        /// <param name="notifyAccountUserId">ID пользователя для отправки уведомления о создании инвойса</param>
        /// <returns>Результат операции с ID созданного инвойса</returns>
        ManagerResult<Guid> CreateArbitraryAmountInvoice(
            CreateArbitraryAmountInvoiceDto request,
            Guid? notifyAccountUserId = null);
    }
}