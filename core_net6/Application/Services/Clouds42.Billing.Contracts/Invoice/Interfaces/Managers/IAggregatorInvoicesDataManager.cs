﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Managers
{
    /// <summary>
    /// Менеджер для работы с данными платежей агрегаторов
    /// </summary>
    public interface IAggregatorInvoicesDataManager
    {
        /// <summary>
        /// Получить данные счетов агрегаторов
        /// </summary>
        /// <param name="dateFrom">Начальный период</param>
        /// <param name="dateTo">Конечный период</param>
        /// <returns>Данные счетов агрегаторов</returns>
        ManagerResult<List<AggregatorInvoiceWithSupplierCodeDataDto>> GetAggregatorInvoicesData(DateTime dateFrom, DateTime dateTo);
    }
}