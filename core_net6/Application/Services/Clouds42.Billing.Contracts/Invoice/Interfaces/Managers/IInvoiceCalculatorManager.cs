﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;
using Clouds42.Domain.Enums.Billing;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Managers
{
    /// <summary>
    /// Менеджер калькуляторов инвойсов
    /// </summary>
    public interface IInvoiceCalculatorManager
    {

        /// <summary>
        /// Получить модель калькулятора инвойсов для аккаунта
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="invoiceType">Калькулятор инвойсов</param>
        /// <returns></returns>
        ManagerResult<InvoiceCalculatorDto?> GetInvoiceCalculatorForAccount(Guid accountId,
            InvoicePurposeType invoiceType);

        /// <summary>
        /// Получить пересчитанный калькулятор инвойсов на основании запроса
        /// </summary>
        /// <param name="request">Запроса</param>
        /// <returns>Калькулятор инвойсов</returns>
        ManagerResult<InvoiceCalculatorDto?> RecalculateInvoiceForAccount(
            RecalculateInvoiceDto request);
    }
}
