﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Managers
{
    /// <summary>
    /// Менеджер для работы с данными счета для отчета 
    /// </summary>
    public interface IInvoiceReportDataManager
    {

        /// <summary>
        /// Получить список моделей данных по счету для отчета
        /// </summary>
        /// <returns>Список моделей данных по счету для отчета</returns>
        ManagerResult<List<InvoiceReportDataDto>> GetInvoiceReportDataDcs();
    }
}
