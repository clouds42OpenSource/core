﻿using Clouds42.DataContracts.Billing.Inovice.InvoiceCalculator;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Validators
{
    /// <summary>
    /// Валидатор расчета инвойса
    /// </summary>
    public interface IInvoiceCalculationValidator
    {
        /// <summary>
        /// Проверка валидности расчета инвойса
        /// </summary>
        /// <param name="calculation">Ка</param>
        /// <returns>Результат валидации</returns>
        bool IsValid(InvoiceCalculationDto calculation);
    }
}
