﻿using Clouds42.DataContracts.Billing.Inovice;

namespace Clouds42.Billing.Contracts.Invoice.Interfaces.Selectors
{
    public interface IInvoiceDataSelector
    {
        InvoiceFullDataDto GetInvoiceFullData(Guid invoiceId);
    }
}