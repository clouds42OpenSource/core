﻿using Clouds42.Configurations1C.CheckReleasesOfConfiguration1C.Providers;
using Clouds42.Configurations1C.Configuration1CUpdateMapping.Managers;
using Clouds42.Configurations1C.Configuration1CUpdateMapping.Providers;
using Clouds42.Configurations1C.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Configurations1C
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddCheckReleasesOfConfiguration1C(this IServiceCollection services)
        {
            services.AddTransient<IConfiguration1CReleasesProvider, Configuration1CReleasesProvider>();
            services.AddTransient<IConfiguration1CUpdateMappingDataProvider, Configuration1CUpdateMappingDataProvider>();
            services.AddTransient<Configuration1CUpdateMappingManager>();
            return services;
        }
    }
}
