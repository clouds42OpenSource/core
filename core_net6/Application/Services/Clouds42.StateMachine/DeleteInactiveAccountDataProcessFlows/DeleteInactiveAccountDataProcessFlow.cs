﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.DeleteInactiveAccountData;
using Clouds42.StateMachine.Contracts.DeleteInactiveAccountDataProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.DeleteInactiveAccountDataProcessFlows
{
    /// <summary>
    /// Процесс удаления данных неактивного аккаунта
    /// </summary>
    class DeleteInactiveAccountDataProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        AccountDataProvider accountDataProvider)
        : StateMachineBaseFlow<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataResultDto>(configurator,
                uow),
            IDeleteInactiveAccountDataProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(DeleteInactiveAccountDataParamsDto model)
            => GetProcessedObjectName(model);

        /// <summary>
        /// Инициализировать карту удаления данных неактивного аккаунта
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<PrepareAccountDataBeforeDeleteAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>(
                next: () => Configurator.CreateAction<DeleteAccountFilesFromFileStorageAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>(
                next: () => Configurator.CreateAction<DeleteAccountFilesFormTombAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>(
                next: () => Configurator.CreateAction<DeleteAccountDatabasesBackupAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>(
                next: () => Configurator.CreateAction<DeleteAccountDatabasesAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>(
                next: () => Configurator.CreateAction<FinalizeDeletingProcessAction, DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataResultDto>(
            ))))));
        }

        /// <summary>
        /// Сконвертировать результат удаления данных в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override DeleteInactiveAccountDataResultDto ConvertResultToOutput(object result)
            => (DeleteInactiveAccountDataResultDto) result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(DeleteInactiveAccountDataParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            return $"{account.Id}::{account.IndexNumber}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Удаление данных неактивного аккаунта.";
    }
}
