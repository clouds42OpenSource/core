﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Accounts.SegmentMigration.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb;
using Clouds42.StateMachine.Actions.MigrateAccountDatabase;
using Clouds42.StateMachine.Contracts.MigrateAccountDatabaseProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.MigrateAccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс миграции инф. базы
    /// </summary>
    internal class MigrateAccountDatabaseProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        SynchronizeProcessFlowsHelper synchronizeProcessFlowsHelper)
        : StateMachineBaseFlow<MigrateAccountDatabaseParametersDto, DatabaseMigrationResultDto>(configurator, uow),
            IMigrateAccountDatabaseProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель миграции инф. базы</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(MigrateAccountDatabaseParametersDto model)
        {
            return model.AccountDatabaseId.ToString();
        }

        /// <summary>
        /// Инициализировать карту миграции инф. базы
        /// </summary>
        protected override void InitConfig()
        {
            Configurator
                .CreateAction<PrepareParamsForSendDbToTombAction, MigrateAccountDatabaseParametersDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<SetTransferDbStateAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<CreateAccountDatabaseBackupAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<UploadAccountDatabaseBackupAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<ValidateUploadedAccountDatabaseAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<ClearAccountDatabaseLocalDataAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<ActualizeAccountDatabaseAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                    next: () => Configurator.CreateAction<FinalizeСopyingAccountDatabaseToTombAction, CopyAccountDatabaseBackupToTombParamsDto, MigrateAccountDatabaseParametersDto>(
                    next: () => Configurator.CreateAction<CopyDirectoryToNewFileStorageAction, MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>(tryCount: 3,
                    next: () => Configurator.CreateAction<RepublishFileAccountDatabaseAction, MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>(tryCount: 3,
                    next: () => Configurator.CreateAction<UpdateDatabaseWebSocketSyncAction, MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>(tryCount: 3,
                    next: () => Configurator.CreateAction<AddAccessToExternalUsersAction, MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>(tryCount: 3,
                    next: () => Configurator.CreateAction<DeleteOldDatabasePathAction, MigrateAccountDatabaseParametersDto, DatabaseMigrationResultDto>(tryCount: 3)
            ))))))))))));
        }

        /// <summary>
        /// Сконвертировать результат миграции в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override DatabaseMigrationResultDto ConvertResultToOutput(object result)
            => (DatabaseMigrationResultDto) result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(MigrateAccountDatabaseParametersDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            return synchronizeProcessFlowsHelper.GetProcessedObjectNameForMigrateAcDbProcessFlow(accountDatabase,
                model.ParentProcessFlowKey);
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Миграция информационной базы.";
    }
}
