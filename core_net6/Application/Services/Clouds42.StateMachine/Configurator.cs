﻿using Clouds42.StateMachine.Contracts.ActionTypes;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.StateMachine
{
    /// <summary>
    /// Конфигурация рабочего процесса.
    /// </summary>
    public class Configurator(IServiceProvider serviceProvider)
    {
        public class Item
        {
            /// <summary>
            /// Ключ действия.
            /// </summary>
            public string Key { get; set; }

            /// <summary>
            /// Действие.
            /// </summary>
            public object Action { get; set; }

            /// <summary>
            /// Тип входящей модели.
            /// </summary>
            public Type ModelType { get; set; }

            /// <summary>
            /// Тип резельтирующей модели.
            /// </summary>
            public Type ResultType { get; set; }

            /// <summary>
            /// Тип смодели снэпшота.
            /// </summary>
            public Type RollbackSnapshotType { get; set; }

            /// <summary>
            /// Количество попыток.
            /// </summary>
            public byte TryCount { get; set; }

            /// <summary>
            /// Возможность отката процесса.
            /// </summary>
            public bool CanRollback => RollbackSnapshotType != null;
        }

        /// <summary>
        /// Очередь действий.
        /// </summary>
        public Queue<Item> Actions { get; } = new();

        /// <summary>
        /// Создать действие.
        /// </summary>
        /// <typeparam name="TAction">Тип действия.</typeparam>
        /// <typeparam name="TModel">Тип входящей модели в действие.</typeparam>
        /// <typeparam name="TResult">Тип резукльтирующей модели действия.</typeparam>
        /// <param name="tryCount">Количество попыток действия.</param>
        /// <param name="next">Следующее дейтсвие.</param>
        /// <returns>Конфигурация процесса.</returns>
        public Configurator CreateAction<TAction, TModel, TResult>(byte tryCount = 5, Action next = null)
            where TAction : ISimpleAction<TModel, TResult>
        {
            Actions.Enqueue(CreateConfigItem<TAction, TModel, TResult>(tryCount));

            next?.Invoke();

            return this;
        }

        /// <summary>
        /// Создать конфигурацию действие.
        /// </summary>
        /// <typeparam name="TAction">Тип действия.</typeparam>
        /// <typeparam name="TModel">Тип входящей модели в действие.</typeparam>
        /// <typeparam name="TResult">Тип резукльтирующей модели действия.</typeparam>
        /// <param name="tryCount">Количество попыток действия.</param>
        /// <returns>Конфигурация действия.</returns>
        private Item CreateConfigItem<TAction, TModel, TResult>(byte tryCount) where TAction :
            IAction<TModel, TResult>
        {
            return new Item
            {
                Key = typeof(TAction).AssemblyQualifiedName,
                Action = serviceProvider.GetRequiredService<TAction>(),
                ModelType = typeof(TModel),
                ResultType = typeof(TResult),
                TryCount = tryCount,
            };
        }
    }
}
