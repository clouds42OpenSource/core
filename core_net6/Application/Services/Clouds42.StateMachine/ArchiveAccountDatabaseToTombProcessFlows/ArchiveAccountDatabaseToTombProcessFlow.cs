﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.ArchiveAccountDatabaseToTomb;
using Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb;
using Clouds42.StateMachine.Contracts.ArchiveAccountDatabaseToTombProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.ArchiveAccountDatabaseToTombProcessFlows
{
    /// <summary>
    /// Процесс архивации инф. базы в склеп
    /// </summary>
    class ArchiveAccountDatabaseToTombProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : StateMachineBaseFlow<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                configurator, uow),
            IArchiveAccountDatabaseToTombProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            return model.AccountDatabaseId.ToString();
        }

        /// <summary>
        /// Инициализировать карту архивации инф. базы в склеп
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<CreateAccountDatabaseBackupAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<UploadAccountDatabaseBackupAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ValidateUploadedAccountDatabaseAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ClearAccountDatabaseLocalDataAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ActualizeAccountDatabaseAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<UnpublishAccountDatabaseAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<DeleteLocalAccountDatabaseAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>()))))));
        }

        /// <summary>
        /// Сконвертировать результат архивации в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override CopyAccountDatabaseBackupToTombParamsDto ConvertResultToOutput(object result)
            => (CopyAccountDatabaseBackupToTombParamsDto)result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            return $"{accountDatabase.Id}::{accountDatabase.V82Name}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Архивация информационной базы в склеп";
    }
}
