﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase;
using Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.ServiceManagerProcessFlows
{
    /// <summary>
    /// Процесс создания инф. базы из zip в МС
    /// </summary>
    class CreateDatabaseFromZipInMsProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : StateMachineBaseFlow<CreateDatabaseFromZipInMsParamsDto, bool>(configurator, uow),
            ICreateDatabaseFromZipInMsProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель создания инф. базы из zip в МС</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(CreateDatabaseFromZipInMsParamsDto model)
            => model.AccountDatabaseId.ToString();

        /// <summary>
        /// Инициализировать карту создания инф. базы из zip в МС
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<PrepareUploadedFileForCreateDatabaseAction, CreateDatabaseFromZipInMsParamsDto, CreateDatabaseFromZipInMsParamsDto>(
                next: () => Configurator.CreateAction<CreateDatabaseFromZipInMsAction, CreateDatabaseFromZipInMsParamsDto, bool>());
        }

        /// <summary>
        /// Сконвертировать результат обновления в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(CreateDatabaseFromZipInMsParamsDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            return $"{accountDatabase.Id}::{accountDatabase.V82Name}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Создание инф. базы из zip в Менеджере Сервисов.";
    }
}
