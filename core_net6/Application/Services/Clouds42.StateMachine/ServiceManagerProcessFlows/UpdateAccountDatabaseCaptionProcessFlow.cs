﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase;
using Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.ServiceManagerProcessFlows
{
    /// <summary>
    /// Процесс обновления названия инф. базы
    /// </summary>
    class UpdateAccountDatabaseCaptionProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : StateMachineBaseFlow<UpdateAccountDatabaseCaptionParamsDto, bool>(configurator, uow),
            IUpdateAccountDatabaseCaptionProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель обновления названия инф. базы</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(UpdateAccountDatabaseCaptionParamsDto model)
        {
            return model.AccountDatabaseId.ToString();
        }

        /// <summary>
        /// Инициализировать карту обновления названия инф. базы
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.
                CreateAction<ChangeAccountDatabaseCaptionInMsAction, UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>(
                    next: () => Configurator.CreateAction<ChangeAccountDatabaseCaptionInDbAction, UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>(
                    next: () => Configurator.CreateAction <UpdateDatabaseWebSocketSyncAction, UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>(
                    next: () => Configurator.CreateAction<SynchronizeAccountDatabaseWithTardisAction, UpdateAccountDatabaseCaptionParamsDto, bool>())));
        }

        /// <summary>
        /// Сконвертировать результат обновления в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(UpdateAccountDatabaseCaptionParamsDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            return $"{accountDatabase.Id}::{accountDatabase.V82Name}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Обновление названия информационной базы.";
    }
}
