﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Exceptions;
using Clouds42.StateMachine.Contracts.Internal;

namespace Clouds42.StateMachine
{
    /// <summary>
    /// Менеджер перезапуска рабочих процессов.
    /// </summary>
    public class ProcessFlowRetryManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IProcessFlowRetryProcessor processFlowRetryProcessor)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Перезапустить рабочий процесс
        /// </summary>
        /// <param name="processFlowId">Номенр рабочего процесса.</param>
        public ManagerResult Retry(Guid processFlowId)
        {

            AccessProvider.HasAccess(ObjectAction.ProcessFlow_Retry);
            try
            {
                processFlowRetryProcessor.Retry(processFlowId);
                return Ok();
            }
            catch (Exception ex)
            {
                if(ex.GetType() != typeof(RetryObsoleteProcessFlowException))
                    _handlerException.Handle(ex, "[Получение списка рабочих процессов]");
                return PreconditionFailed(ex.Message);
            }
        }

    }
}
