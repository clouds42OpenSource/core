﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.RestartIisApplicationPool;
using Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.IisApplicaionPoolProcessFlows
{
    /// <summary>
    /// Конечный автомат для перезапуска пула приложения на IIS
    /// </summary>
    internal class RestartIisApplicationPoolFlow(Configurator configurator, IUnitOfWork uow)
        : StateMachineBaseFlow<RestartIisApplicationPoolParamsDto, bool>(configurator, uow),
            IRestartIisApplicationPoolFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель для управления пулом приложения на IIS</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(RestartIisApplicationPoolParamsDto model)
            => Guid.NewGuid().ToString("N");

        /// <summary>
        /// Инициализировать карту перезапуска пула на ноде публикаций
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<ChangeDbPublishStateToRestartingPoolAction, RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>(
                next: () => Configurator.CreateAction<GetIisAppPoolNameAction, RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>(
                next: () => Configurator.CreateAction<RestartAppPoolsOnIisAction, RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>(
                next: () => Configurator.CreateAction<FinalizeRestartAppPoolsProcessAction, RestartIisApplicationPoolParamsDto, bool>(
            ))));
        }

        /// <summary>
        /// Сконвертировать результат перезапуска в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Модель для управления пулом приложения на IIS</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(RestartIisApplicationPoolParamsDto model)
            => $"{model.AccountDatabaseId}::{model.ContentServerId}";

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Перезапуск пула на нодах публикаций.";

    }
}
