﻿using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.ManagePublishedAcDbRedirect;
using Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.IisApplicaionPoolProcessFlows
{
    /// <summary>
    /// Процесс установки редиректа опубликованной инф. базы
    /// </summary>
    class InstallPublishedAcDbRedirectProcessFlow(Configurator configurator, IUnitOfWork uow)
        : StateMachineBaseFlow<ManagePublishedAcDbRedirectParamsDto, bool>(configurator, uow),
            IInstallPublishedAcDbRedirectProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров управления редиректом опубликованной инф. базы</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(ManagePublishedAcDbRedirectParamsDto model)
            => $"{model.AccountDatabaseId}::{model.AccountDatabaseV82Name}";

        /// <summary>
        /// Инициализировать карту установки редиректа опубликованной инф. базы
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<GetDataForAcDbApplicationOnIisAction, ManagePublishedAcDbRedirectParamsDto, ManagePublishedAcDbRedirectParamsDto>(
                next: () =>  Configurator.CreateAction<InstallPublishedAcDbRedirectAction, ManagePublishedAcDbRedirectParamsDto, bool>());
        }

        /// <summary>
        /// Сконвертировать результат установки редиректа в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(ManagePublishedAcDbRedirectParamsDto model)
            => $"{model.AccountDatabaseId}::{model.AccountDatabaseV82Name}";

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Установка редиректа для опубликованной инф. базы";
    }
}
