﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.ChangeAccountSegment;
using Clouds42.StateMachine.Contracts.AccountProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountProcessFlows
{
    /// <summary>
    /// Процесс смены сегмента для аккаунта
    /// </summary>
    internal class ChangeAccountSegmentProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        AccountDataProvider accountDataProvider)
        : StateMachineBaseFlow<ChangeAccountSegmentParamsDto, MigrationResultDto>(configurator, uow),
            IChangeAccountSegmentProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(ChangeAccountSegmentParamsDto model)
            => GetProcessedObjectName(model);

        /// <summary>
        /// Инициализировать карту смены сегмента для аккаунта
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<MigrateAccountFileDatabasesAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(tryCount: 3,
                next: () => Configurator.CreateAction<ChangeAccountFileStorageAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(
                next: () => Configurator.CreateAction<ChangeAccountClientFileStorageAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(
                next: () => Configurator.CreateAction<ChangeAccountBackupStorageAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(
                next: () => Configurator.CreateAction<ChangeAccountSegmentAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(tryCount: 2,
                next: () => Configurator.CreateAction<MigrateAccountServerDatabasesAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(tryCount: 3,
                next: () => Configurator.CreateAction<RepublishAccountDatabasesAction, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(tryCount: 3, 
                next: () => Configurator.CreateAction<RemountDiskRPathForAccountUsers, ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>(tryCount: 3, 
                next: () => Configurator.CreateAction<FinalizeMigrationProcessAction, ChangeAccountSegmentParamsDto, MigrationResultDto>(tryCount: 3
            )))))))));
        }

        /// <summary>
        /// Сконвертировать результат миграции в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override MigrationResultDto ConvertResultToOutput(object result)
            => (MigrationResultDto) result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(ChangeAccountSegmentParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            return $"{account.Id}::{account.IndexNumber}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Смена сегмента для аккаунта";
    }
}
