﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.ProcessFlow;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Internal;
using Clouds42.StateMachine.Helpers;
using PagedList.Core;
using ActionFlow = Clouds42.Domain.DataModels.StateMachine.ActionFlow;

namespace Clouds42.StateMachine.Internal
{
    /// <summary>
    /// Провайдер получения данных о рабочих процессах.
    /// </summary>
    internal class ProcessFlowDataProvider(IUnitOfWork dbLayer) : IProcessFlowDataProvider
    {
        /// <summary>
        /// Получить список рабочих процессов по фильтру.
        /// </summary>
        /// <param name="args">Значения фильтра.</param>
        /// <returns>Список рабочих процессов.</returns>
        public ProcessFlowDataModelDto GetProcessFlowData(ProcessFlowFilterParamsDto args)
        {
            const byte pageSize = 50;
            var processFlowsQuery = dbLayer.ProcessFlowRepository.WhereLazy();

            if (args?.Filter?.Status.HasValue ?? false)
            {
                processFlowsQuery = processFlowsQuery.Where(p => p.Status == args.Filter.Status);
            }

            processFlowsQuery = ApplyFilter(processFlowsQuery, args?.Filter?.SearchLine);

            var processFlowList = processFlowsQuery.OrderByDescending(p => p.CreationDateTime).Take(200000);

            var pageNumber = args?.PageNumber ?? 1;

            return GetProcessFlowData(pageNumber, processFlowList, pageSize);
        }

        /// <summary>
        /// Получить детали рабочего процесса по ID
        /// </summary>
        /// <param name="processFlowId">ID рабочего процесса</param>
        /// <returns>Детали рабочего процесса</returns>
        public ProcessFlowDetailsDto GetProcessFlowDetailsById(Guid processFlowId)
        {
            var processFlowTempDataModel = GetProcessFlowTempModel(processFlowId);
            return CreateProcessFlowDetailsDto(processFlowTempDataModel);
        }

        /// <summary>
        /// Получить временную модель рабочего процесса
        /// </summary>
        /// <param name="processFlowId">ID рабочего процесса</param>
        /// <returns>Временная модель рабочего процесса</returns>
        private (ProcessFlow ProcessFlow, List<ActionFlow> ActionFlows) GetProcessFlowTempModel(Guid processFlowId)
            => (
                    from processFlow in dbLayer.ProcessFlowRepository.WhereLazy()
                    join actionFlow in dbLayer.ActionFlowRepository.WhereLazy() on processFlow.Id equals actionFlow
                            .ProcessFlowId
                        into actionFlows
                    where processFlow.Id == processFlowId
                    select new
                    {
                        ProcessFlow = processFlow,
                        ActionFlows = actionFlows.ToList()
                    }
                ).AsEnumerable().Select(acFlTempData => (acFlTempData.ProcessFlow, acFlTempData.ActionFlows))
                .FirstOrDefault();

        /// <summary>
        /// Получить данные шагов рабочего процесса
        /// </summary>
        /// <param name="actionFlows">Список шагов рабочего процесса</param>
        /// <returns>Данные шагов рабочего процесса</returns>
        private List<ActionFlowDataDto> GetActionFlowsData(List<ActionFlow> actionFlows)
            => actionFlows.Select(acFl => new
            {
                acFl.Status,
                acFl.CountOfAttempts,
                acFl.ErrorMessage,
                ActionsChain = ProcessFlowStateExtension.GetActionsCallChain(acFl.StateRefKey)
            }).OrderBy(acFl => acFl.ActionsChain.Count).ToList().Select(acFl =>
            {
                var currentActionType = acFl.ActionsChain.LastOrDefault();
                return new ActionFlowDataDto
                {
                    ErrorMessage = acFl.ErrorMessage,
                    CountOfAttemps = acFl.CountOfAttempts,
                    Status = acFl.Status,
                    ActionName = ProcessFlowStateExtension.GetActionFlowName(currentActionType),
                    ActionDescription = ProcessFlowStateExtension.GetFlowActionDescription(currentActionType)
                };
            }).ToList();

        /// <summary>
        /// Получить список рабочих процессов c учётом страниц
        /// </summary>
        /// <param name="pageNumber">Какую страницу выбрать</param>
        /// <param name="records">Записи которые разбить на страницы</param>
        /// <param name="itemsPerPage">Количество записей на страницу</param>
        /// <returns>Список рабочих процессов c учётом страниц</returns>
        private static ProcessFlowDataModelDto GetProcessFlowData(int pageNumber, IQueryable<ProcessFlow> records, int itemsPerPage)
            => new()
            {
                Records = records.ToPagedList(pageNumber, itemsPerPage).Select(p => new ProcessFlowItemDto
                    {
                        Id = p.Id,
                        Name = p.Name,
                        CreationDateTime = p.CreationDateTime,
                        Status = p.Status,
                        Comment = p.Comment,
                        ProcessedObjectName = p.ProcessedObjectName,
                        StateDescription = p.GetStateDescription()
                    }).ToArray(),
                Pagination = new PaginationBaseDto(pageNumber, records.Count(), itemsPerPage)
            };
        
        /// <summary>
        /// Применить фильтр к последовательности рабочих процессов
        /// </summary>
        /// <param name="processFlowsQuery">Последовательность рабочих процессов</param>
        /// <param name="searchQueryString">Строка поиска</param>
        /// <returns>Отфильтрованная последовательность рабочих процессов</returns>
        private IQueryable<ProcessFlow> ApplyFilter(IQueryable<ProcessFlow> processFlowsQuery, string searchQueryString)
        {
            if (string.IsNullOrEmpty(searchQueryString))
            {
                return processFlowsQuery;
            }

            var processFlowId = searchQueryString.ToGuid();

            if (processFlowId != Guid.Empty)
            {
                var result = processFlowsQuery.Where(p => p.Id == processFlowId);
                if (result.Any())
                    return result;
            }

            if (!string.IsNullOrEmpty(searchQueryString))
            {
                return processFlowsQuery.Where(p => p.ProcessedObjectName.ToLower().Contains(searchQueryString.ToLower()));
            }

            return processFlowsQuery;
        }

        /// <summary>
        /// Создать модель деталей рабочего процесса
        /// </summary>
        /// <param name="processFlowTempDataModel">Временная модель рабочего процесса</param>
        /// <returns>Модель деталей рабочего процесса</returns>
        private ProcessFlowDetailsDto CreateProcessFlowDetailsDto(
            (ProcessFlow ProcessFlow, List<ActionFlow> ActionFlows) processFlowTempDataModel)
            => new()
            {
                Id = processFlowTempDataModel.ProcessFlow.Id,
                Status = processFlowTempDataModel.ProcessFlow.Status,
                Comment = processFlowTempDataModel.ProcessFlow.Comment,
                Name = processFlowTempDataModel.ProcessFlow.Name,
                ProcessedObjectName = processFlowTempDataModel.ProcessFlow.ProcessedObjectName,
                StateDescription = processFlowTempDataModel.ProcessFlow.GetStateDescription(),
                CreationDateTime = processFlowTempDataModel.ProcessFlow.CreationDateTime,
                ActionFlows = GetActionFlowsData(processFlowTempDataModel.ActionFlows)
            };
    }
}
