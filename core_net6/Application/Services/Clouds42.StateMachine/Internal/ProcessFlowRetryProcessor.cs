﻿using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums.StateMachine;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Exceptions;
using Clouds42.StateMachine.Contracts.Internal;
using Clouds42.StateMachine.Contracts.StateMachineFlow;
using Clouds42.StateMachine.Extensions;
using Clouds42.StateMachine.Helpers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.StateMachine.Internal
{
    /// <summary>
    /// Обработчик по перезапуску рабочих процессов.
    /// </summary>
    internal class ProcessFlowRetryProcessor(
        IUnitOfWork dbLayer,
        IServiceProvider servicesCollection)
        : IProcessFlowRetryProcessor
    {
        /// <summary>
        /// Перезапустить рабочий процесс
        /// </summary>
        /// <param name="processFlowId">Номенр рабочего процесса.</param>
        public void Retry(Guid processFlowId)
        {
            if (processFlowId == Guid.Empty)
                throw new ArgumentNullException($"Значение processFlowId недопустимо '{processFlowId}'");

            var processFlow = dbLayer.ProcessFlowRepository.FirstOrDefault(p => p.Id == processFlowId) ??
                              throw new ArgumentException(
                                  $"По номеру '{processFlowId}'  не удалось получить рабочий процесс.");

            CheckRetryPossibility(processFlow);

            var processFlowType = processFlow.GetProcessFlowType();

            var processFlowInstance =
                (IRetryProcess)servicesCollection.GetRequiredService(
                    processFlowType.DumpInterface().FirstOrDefault() ?? throw new NotFoundException($"Не найден базовый интерфейс для класса {processFlowType.FullName}"));
            processFlowInstance.Retry(processFlow);

        }

        /// <summary>
        /// Проверить возможность перезапуска рабочего процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        private void  CheckRetryPossibility(ProcessFlow processFlow)
        {
            if (GetActualProcessFlow(processFlow) != null)
                throw new RetryObsoleteProcessFlowException($"Перезапускаемый рабочий процесс '{processFlow.Id}' устарел");

            if (processFlow.Status != StateMachineComponentStatus.Error &&
                processFlow.Status != StateMachineComponentStatus.RolledBack)
                throw new InvalidOperationException($"Не возможно перезапустить рабочий процесс в статусе {processFlow.Status}");
        }

        /// <summary>
        /// Проверить актуальность рабочего процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <returns>Актуальный рабочий процесс по обрабатываему объекту исходного процесса.</returns>
        private ProcessFlow GetActualProcessFlow(ProcessFlow processFlow)
            => dbLayer.ProcessFlowRepository.FirstOrDefault(p =>
                p.Id != processFlow.Id && p.Key == processFlow.Key &&
                p.CreationDateTime > processFlow.CreationDateTime);
    }
}
