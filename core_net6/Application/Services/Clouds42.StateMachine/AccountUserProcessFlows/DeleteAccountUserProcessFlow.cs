﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.DeleteAccountUser;
using Clouds42.StateMachine.Contracts.AccountUserProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountUserProcessFlows
{
    /// <summary>
    /// Конечный автомат для удаления пользователя
    /// </summary>
    class DeleteAccountUserProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountUserDataProvider accountUserDataProvider)
        : StateMachineBaseFlow<AccountUserIdModel, bool>(configurator, uow), IDeleteAccountUserProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель аккаунта пользователя</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(AccountUserIdModel model)
        {
            if (model.AccountUserID != Guid.Empty)
                return model.AccountUserID.ToString();

            throw new ArgumentException("Не задан параметр Id удаляемого пользователя.");
        }

        /// <summary>
        /// Инициализировать карту удаления пользователя
        /// </summary>
        protected override void InitConfig()
        {
            Configurator
                .CreateAction<DisableRent1CForAccountUserAction, AccountUserIdModel, AccountUser>(
                    next: () =>
                        Configurator.CreateAction<DisableServicesForAccountUserAction, AccountUser, AccountUser>(
                            next: () => Configurator
                                .CreateAction<DeleteAccountUserInOpenIdProviderAction, AccountUser, AccountUser>(
                                    next: () => Configurator
                                        .CreateAction<DeleteAccountUserInAdAction, AccountUser, AccountUser>(
                                            next: () => Configurator
                                                .CreateAction<DeleteAccountUserInDatabaseAction, AccountUser,
                                                    AccountUserIdModel>(
                                                    next: () => Configurator
                                                        .CreateAction<SynchronizeWithTardisAction, AccountUserIdModel,
                                                            bool>())))));
        }

        /// <summary>
        /// Сконвертировать результат перезапуска в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(AccountUserIdModel model)
            => accountUserDataProvider.GetAccountUserOrThrowException(model.AccountUserID).Login;

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Удаление пользователя.";
        
    }
}
