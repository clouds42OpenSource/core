﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.UpdateAccountUser;
using Clouds42.StateMachine.Contracts.AccountUserProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountUserProcessFlows
{

    /// <summary>
    /// Процесс по редактированию пользователя.
    /// </summary>
    class UpdateAccountUserProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountUserDataProvider accountUserDataProvider)
        : StateMachineBaseFlow<AccountUserDto, bool>(configurator, uow), IUpdateAccountUserProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(AccountUserDto model)
        {
            if (model.Id.HasValue)
                return model.Id.Value.ToString();

            throw new ArgumentException("Не задан параметр Id номер редактируемого пользователя.");
        }

        /// <summary>
        /// Инициализировать карту редактирования пользователя
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<EditAccountUserInAdAction, AccountUserDto, AccountUserDto>(tryCount: 1,
                next: () => Configurator.CreateAction<EditAccountUserInOpenIdProviderAction, AccountUserDto, AccountUserDto>(
                next: () => Configurator.CreateAction<UpdateLoginForWebAccessDatabaseAction, AccountUserDto, AccountUserDto>(
                next: () => Configurator.CreateAction<EditAccountUserInDatabaseAction, AccountUserDto, AccountUserIdModel>(
                next: () => Configurator.CreateAction<SynchronizeWithTardisAction, AccountUserIdModel, bool>()))));
        }

        /// <summary>
        /// Сконвертировать результат выполнения в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <inheritdoc />
        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(AccountUserDto model)
            => $"{accountUserDataProvider.GetAccountUserOrThrowException(model.Id ?? throw new InvalidOperationException("Значение model.Id не может быть пустым")).Login} :: {model.Id}";

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Редактирование пользователя.";

    }
}
