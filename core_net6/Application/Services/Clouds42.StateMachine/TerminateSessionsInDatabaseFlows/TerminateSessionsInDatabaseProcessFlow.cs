﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.TerminateSessionsInDatabase;
using Clouds42.StateMachine.Contracts.TerminateSessionsInDatabaseProcessFlow;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.TerminateSessionsInDatabaseFlows
{
    /// <summary>
    /// Процесс завершения сеансов в информационной базе
    /// </summary>
    class TerminateSessionsInDatabaseProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider)
        : StateMachineBaseFlow<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>(configurator, uow), ITerminateSessionsInDatabaseProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(TerminateSessionsInDatabaseJobParamsDto model) =>
            model.TerminationSessionsInDatabaseId.ToString();

        /// <summary>
        /// Инициализировать карту завершения сеансов в информационной базе
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<StopDatabaseIisApplicationPoolAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>(3,
            () => Configurator.CreateAction<CloseOpenDatabaseFilesOnFileStorageAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>(5,
            () => Configurator.CreateAction<CloseOpenSessionsFromClusterAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>(3,
            () => Configurator.CreateAction<CloseOpenSessionsFromDelimitersAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>(3,
            () => Configurator.CreateAction<StartDatabaseIisApplicationPoolAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>(3,
            () => Configurator.CreateAction<RegisterTerminationSessionsInDatabaseResultAction, TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>())))));
        }

        /// <summary>
        /// Сконвертировать результат архивации в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override TerminateSessionsInDatabaseJobParamsDto ConvertResultToOutput(object result) =>
            (TerminateSessionsInDatabaseJobParamsDto)result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(TerminateSessionsInDatabaseJobParamsDto model)
        {
            var accountDatabase =
                terminationSessionsInDatabaseDataProvider.GetDatabaseByTerminationSessionsIdOrThrowException(
                    model.TerminationSessionsInDatabaseId);

            return $"{accountDatabase.Id}::{accountDatabase.V82Name}";
        }

        /// <summary>
        /// Имя рабочего процесса
        /// </summary>
        protected override string ProcessFlowName => "Завершение сеансов в информационной базе";
    }
}
