﻿using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Segment.Contracts;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Завершение процесса миграции аккаунта
    /// </summary>
    [FlowActionDescription("Завершение процесса миграции аккаунта")]
    public class FinalizeMigrationProcessAction(
        ISegmentHistoryProvider segmentHistoryProvider,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, MigrationResultDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Результат миграции аккаунта</returns>
        public MigrationResultDto Do(ChangeAccountSegmentParamsDto model)
        {
            try
            {
                RegisterMigrationHistory(model);
                return CreateMigrationResult(model.AccountId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"{model.AccountId}: [Ошибка миграции аккаунта]");
                throw;
            }
        }

        /// <summary>
        /// Зарегистрировать результат миграции
        /// </summary>
        /// <param name="changeAccountSegmentParams">Параметры смены сегмента для аккаунта</param>
        private void RegisterMigrationHistory(ChangeAccountSegmentParamsDto changeAccountSegmentParams)
        {
            segmentHistoryProvider.RegisterMigrationHistory(new MigrationAccountHistory
            {
                AccountId = changeAccountSegmentParams.AccountId,
                SourceSegmentId = changeAccountSegmentParams.SegmentFromId,
                TargetSegmentId = changeAccountSegmentParams.SegmentToId,
                MigrationHistory = segmentHistoryProvider.CreateMigrationHistoryObject(changeAccountSegmentParams.InitiatorId, changeAccountSegmentParams.MigrationStartDateTime, true)
            });
        }

        /// <summary>
        /// Создать результат миграции
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="migrationSuccess">Признак, что миграция завершена успешно</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат миграции</returns>
        private static MigrationResultDto CreateMigrationResult(Guid accountId, bool migrationSuccess = true, string errorMessage = null)
            => new()
            {
                AccountId = accountId,
                ErrorMessage = errorMessage,
                Successful = migrationSuccess
            };
    }
}
