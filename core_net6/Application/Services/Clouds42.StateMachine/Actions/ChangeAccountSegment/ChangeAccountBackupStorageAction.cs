﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Смена хранилища бэкапов аккаунта
    /// </summary>
    [FlowActionDescription("Смена хранилища бэкапов аккаунта")]
    public class ChangeAccountBackupStorageAction(
        IChangeAccountBackupServerProvider changeAccountBackupServerProvider,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Параметры смены сегмента для аккаунта</returns>
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            try
            {
                changeAccountBackupServerProvider.ChangeAccountBackupServer(model);
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смене хранилища бэкапов для аккаунта] {model.AccountId}");
                throw;
            }
        }
    }
}
