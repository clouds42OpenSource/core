﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.DataContracts.Account;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Дествие для переопубликации баз аккаунта
    /// </summary>
    [FlowActionDescription("Пересборка пути маунта диска R для пользователей аккаунта")]
    public class RemountDiskRPathForAccountUsers(
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IAccountFolderHelper accountFolderHelper)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            var accountUsers = unitOfWork.AccountUsersRepository
                .AsQueryableNoTracking()
                .Where(x => x.AccountId == model.AccountId && x.CorpUserSyncStatus != "Deleted" && x.CorpUserSyncStatus != "SyncDeleted")
                .ToList();

            var logins = accountUsers.Select(x => x.Login).ToArray();

            var account = unitOfWork.AccountsRepository
                .AsQueryableNoTracking()
                .FirstOrDefault(x => x.Id == model.AccountId);

            try
            {
                var segment = unitOfWork.CloudServicesSegmentRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.CloudServicesFileStorageServer)
                    .FirstOrDefault(x => x.ID == model.SegmentToId);

                UpdateMountDiskRPath(segment!, account!, logins);
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"Ошибка смена пути маунта диска R у пользователей аккаута {model.AccountId}. Выполняется попытка отката к старому значению");

                try
                {
                    var baseSegment = unitOfWork.CloudServicesSegmentRepository
                        .AsQueryableNoTracking()
                        .Include(x => x.CloudServicesFileStorageServer)
                        .FirstOrDefault(x => x.ID == model.SegmentFromId);

                    UpdateMountDiskRPath(baseSegment!, account!, logins);
                }

                catch (Exception e)
                {
                    logger.Error(e, $"Ошибка при попытке отката смены пути маунта диска R у пользователей аккаута {model.AccountId}.");

                    throw;
                }
                
            }

            return model;
        }

        private void UpdateMountDiskRPath(CloudServicesSegment segment, Account account, string[] logins)
        {
            if (segment.NotMountDiskR)
            {
                ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(null, null, logins);
            }

            else
            {
                var path = accountFolderHelper.GetAccountClientFileStoragePath(segment, account);

                ActiveDirectoryUserFunctions.SetUserFilesHomeDirectory(path, "R:", logins);
            }
        }
    }
}
