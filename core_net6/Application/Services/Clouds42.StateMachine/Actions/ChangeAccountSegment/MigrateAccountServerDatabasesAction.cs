﻿using Clouds42.DataContracts.Account;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Миграция серверных баз аккаунта
    /// </summary>
    [FlowActionDescription("Миграция серверных баз аккаунта")]
    public class MigrateAccountServerDatabasesAction(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Параметры смены сегмента для аккаунта</returns>
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            logger.Info($"[{model.AccountId}]: Начинаем миграцию для серверных баз аккаунта");

            var serverAccountDatabases = GetServerAccountDatabases(model.AccountId);
            if (!serverAccountDatabases.Any())
            {
                logger.Info($"[{model.AccountId}]: Не найдены серверные базы для миграции");
                return model;
            }

            try
            {
                ChangingPlatformForServerDatabasesFromAlphaToStable(serverAccountDatabases);
                logger.Info($"[{model.AccountId}]: Все серверные базы мигрировали");
                
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка миграции серверных баз] {model.AccountId}");
                throw;
            }
        }

        /// <summary>
        /// Изменение платформы для серверных баз c Alpha на Stable
        /// </summary>
        /// <param name="databases">Список серверных баз</param>
        private void ChangingPlatformForServerDatabasesFromAlphaToStable(List<Domain.DataModels.AccountDatabase> databases)
        {
            logger.Info("Начинаем изменение платформы для серверных баз c Alpha на Stable");
            var databasesOnAlpha = databases.Where(d => d.DistributionType == DistributionType.Alpha.ToString()).ToList();
            if (!databasesOnAlpha.Any())
                return;

            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                databasesOnAlpha.ForEach(accountDatabase =>
                {
                    accountDatabase.DistributionTypeEnum = DistributionType.Stable;
                    dbLayer.DatabasesRepository.Update(accountDatabase);
                });
                    
                dbLayer.Save();
                transaction.Commit();
                logger.Info("Для всех серверных баз изменена платформа c Alpha на Stable");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка изменения платформы для серверных баз]");
                transaction.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Получить серверные базы аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Серверные базы аккаунта</returns>
        private List<Domain.DataModels.AccountDatabase> GetServerAccountDatabases(Guid accountId)
            => dbLayer.DatabasesRepository
                .Where(d => d.AccountId == accountId && (!d.IsFile.HasValue || !d.IsFile.Value) &&
                            d.AccountDatabaseOnDelimiter == null).ToList();
    }
}
