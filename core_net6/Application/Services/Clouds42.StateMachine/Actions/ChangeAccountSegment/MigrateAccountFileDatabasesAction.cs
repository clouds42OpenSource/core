﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Миграция файловых баз аккаунта
    /// </summary>
    [FlowActionDescription("Миграция файловых баз аккаунта")]
    public class MigrateAccountFileDatabasesAction(
        IMigrateAccountFileDatabasesProvider migrateAccountFileDatabasesProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Параметры смены сегмента для аккаунта</returns>
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            try
            {
                var resultList = migrateAccountFileDatabasesProvider.MigrateAccountDatabases(model);
                
                ProcessMigrationResults(resultList);
                return model;
            }
            catch (ValidateException ex)
            {
                logger.Warn(ex, $"Ошибка валидации баз для миграции {model.AccountId}");
                throw;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка миграции файловых баз аккаунта] {model.AccountId}");
                throw;
            }
        }

        /// <summary>
        /// Обработать результаты миграции
        /// </summary>
        /// <param name="databaseMigrationResults">Результаты миграции</param>
        private void ProcessMigrationResults(List<AccountDatabaseMigrationResultModelDto> databaseMigrationResults)
        {
            logger.Trace($"Миграция файловых баз аккаунта завершена. Обработано {databaseMigrationResults.Count} инф. баз");
            if (databaseMigrationResults.All(res => res.Successful))
                return;

            var errorMessage = string.Empty;
            databaseMigrationResults.ForEach(result =>
            {
                errorMessage += GetMigrationErrorDescription(result);
            });

            logger.Trace(errorMessage);

            if(databaseMigrationResults.Any(res => !res.Successful))
                throw new InvalidOperationException($"Ошибка миграции файловых баз аккаунта. <ul>{errorMessage}</ul>");
        }

        /// <summary>
        /// Получить описание ошибки при миграции инф. базы
        /// </summary>
        /// <param name="migrationResult">Результат миграции</param>
        /// <returns>Описание ошибки</returns>
        private string GetMigrationErrorDescription(AccountDatabaseMigrationResultModelDto migrationResult)
        {
            if (migrationResult.Successful || !migrationResult.AccountDatabaseId.HasValue)
                return null;

            var accountDatabase = dbLayer.DatabasesRepository.GetAccountDatabase(migrationResult.AccountDatabaseId.Value);
            return $"Инф. база {accountDatabase.V82Name} не перенесена. Причина: {migrationResult.Message}";
        }
    }
}
