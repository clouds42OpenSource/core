﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Смена файлового хранилища аккаунта
    /// </summary>
    [FlowActionDescription("Смена файлового хранилища аккаунта")]
    public class ChangeAccountFileStorageAction(
        IChangeAccountFileStorageProvider changeAccountFileStorageProvider,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Параметры смены сегмента для аккаунта</returns>
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            try
            {
                changeAccountFileStorageProvider.ChangeAccountFileStorage(model);
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены файлового хранилища для аккаунта] {model.AccountId}");
                throw;
            }
        }
    }
}
