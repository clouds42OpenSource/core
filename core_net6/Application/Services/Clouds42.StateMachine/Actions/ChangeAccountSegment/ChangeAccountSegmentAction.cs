﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.StateMachine.Actions.ChangeAccountSegment
{
    /// <summary>
    /// Смена сегмента для аккаунта
    /// </summary>
    [FlowActionDescription("Смена сегмента для аккаунта")]
    public class ChangeAccountSegmentAction(
        IUnitOfWork dbLayer,
        AccountDataProvider accountDataProvider,
        IHandlerException handlerException)
        : ISimpleAction<ChangeAccountSegmentParamsDto, ChangeAccountSegmentParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <returns>Параметры смены сегмента для аккаунта</returns>
        public ChangeAccountSegmentParamsDto Do(ChangeAccountSegmentParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            var targetSegment = accountDataProvider.GetCloudServicesSegmentById(model.SegmentToId);

            try
            {
                dbLayer.AccountConfigurationRepository.AsQueryable()
                    .Where(x => x.AccountId == account.Id)
                    .ExecuteUpdate(x => x.SetProperty(y => y.FileStorageId, targetSegment.FileStorageServersID)
                        .SetProperty(y => y.SegmentId, targetSegment.ID));
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены cегмента для аккаунта] {model.AccountId}");
                throw;
            }

            return model;
        }
    }
}
