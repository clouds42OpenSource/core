﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.BLL.Common;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountDatabaseToTomb
{
    /// <summary>
    /// Удалить локальные файлы информационной базы
    /// </summary>
    [FlowActionDescription("Удаление локальных файлов информационной базы")]
    public class DeleteLocalAccountDatabaseAction(
        AccountDatabaseDeleteProvider accountDatabaseDeleteProvider,
        IUnitOfWork dbLayer,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров архивации инф. базы в склеп</param>
        /// <returns>Модель параметров архивации инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            if (model.ActionsContains(nameof(DeleteLocalAccountDatabaseAction)))
                return model;

            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                var result = accountDatabaseDeleteProvider.DeleteLocalAccountDatabase(accountDatabase);
                if (!result)
                    throw new InvalidOperationException(
                        $"Не удалось удалить информационную базу: {accountDatabase.V82Name}");

                logger.Info(
                    $"[{model.AccountDatabaseId}] :: Удаление локальных файлов информационной базы {accountDatabase.V82Name} уыспешно завершено");

                UpdateAccountDatabaseState(accountDatabase);

                var backupPath =
                    dbLayer.AccountDatabaseBackupRepository.GetAccountDatabaseBackupPath(
                        model.AccountDatabaseBackupId);

                LogEventHelper.LogEventInitiator(dbLayer, accountDatabase.AccountId,
                    LogActions.MovingDbToArchive,
                    $"База “{AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}” перемещена в архив. Резервная копия доступна по ссылке: {backupPath}",
                    model.AccountUserInitiatorId, handlerException);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления локальных файлов инф. базы] {model.AccountDatabaseId}");
                throw;
            }
            return model;
        }

        /// <summary>
        /// Обновить статус инф. базы
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        private void UpdateAccountDatabaseState(Domain.DataModels.AccountDatabase accountDatabase)
        {
            try
            {
                accountDatabaseChangeStateProvider.ChangeState(accountDatabase, DatabaseState.DetachedToTomb);

                accountDatabase.SizeInMB = 0;
                accountDatabase.CalculateSizeDateTime = DateTime.Now;

                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();
            }
            catch (Exception ex)
            {
                logger.Info($"[{accountDatabase.Id}] :: Ошибка актуализации данных: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
