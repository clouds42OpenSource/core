﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountDatabaseToTomb
{
    /// <summary>
    /// Отменить публикацию информационной базы
    /// </summary>
    [FlowActionDescription("Отмена публикации информационной базы")]
    public class UnpublishAccountDatabaseAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IExecuteDatabasePublishTasksProvider executeDatabasePublishTasksProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров архивации инф. базы в склеп</param>
        /// <returns>Модель параметров архивации инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            if (model.ActionsContains(nameof(UnpublishAccountDatabaseAction)))
                return model;

            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (accountDatabase.IsDelimiter())
                    return model;

                if (accountDatabase.PublishStateEnum != PublishState.Published)
                    return model;

                logger.Info($"[{model.AccountDatabaseId}] :: Отменяем публикацию базы");
                executeDatabasePublishTasksProvider.CreateAndStartCancelPublishTask(accountDatabase).Wait();
                logger.Info($"[{model.AccountDatabaseId}] :: Отмена публикации базы завершена");

                dbLayer.DatabasesRepository.Reload(accountDatabase);

                accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
                logger.Info($"[{model.AccountDatabaseId}] :: статус публикации у базы {accountDatabase.PublishState}");
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка отмены публикации инф. базы] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
