﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для создания записи инф. базы в БД
    /// </summary>
    [FlowActionDescription("Создание записи инф. базы в БД")]
    public class InsertDatabaseRecordAction(
        IServiceProvider serviceProvider,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        public CreateAccountDatabaseFromBackupParamsDto Do(CreateAccountDatabaseFromBackupParamsDto model)
        {
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
                var template =
                    dbLayer.DbTemplateRepository.FirstOrDefault(
                        tmpl => tmpl.Id == model.AccountDatabaseCreationParams.TemplateId);

                logger.Trace(
                    $"Начало добавления базы {model.AccountDatabaseCreationParams.DataBaseName} по шаблону {template.Name} для аккаунта {model.AccountDatabaseCreationParams.AccountId}");

                var accountDatabase = CreateNewDataBase(model.AccountDatabaseCreationParams, template);
                CreateAcDbSupport(accountDatabase, template);

                model.CreatedAccountDatabaseId = accountDatabase.Id;

                transaction.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания записи об инф. базе] {model.CreatedAccountDatabaseId}");
                transaction.Rollback();
                throw;
            }

            return model;
        }

        /// <summary>
        /// Создать новую запись информационной базы.
        /// </summary>
        /// <param name="databaseDomainModel">Параметры создания инф. базы.</param>
        /// <param name="template">Шаблон инф. базы</param>
        private Domain.DataModels.AccountDatabase CreateNewDataBase(InfoDatabaseDomainModelDto databaseDomainModel, DbTemplate template)
        {
            var account = dbLayer.AccountsRepository.GetById(databaseDomainModel.AccountId);
            var segmentHelper = serviceProvider.GetRequiredService<ISegmentHelper>();
            var fileStorage = segmentHelper.GetFileStorageServer(account);
            var accountDatabase = dbLayer.DatabasesRepository.CreateNewRecord(template, account, GetPlatformTypeForTemplate(template),
                databaseDomainModel.DataBaseName, fileStorage, databaseDomainModel.IsFile);

            return accountDatabase;
        }

        /// <summary>
        /// Получить версию платформы у шаблона.
        /// </summary>
        /// <returns>Версия платформы</returns>
        private static PlatformType GetPlatformTypeForTemplate(DbTemplate dbTemplate)
            => dbTemplate.PlatformType == PlatformType.Undefined ? PlatformType.V83 : dbTemplate.PlatformType;

        /// <summary>
        /// Создать объект AcDbSupport
        /// </summary>
        /// <param name="accountDatabase">База данных</param>
        /// <param name="template">Шаблон</param>
        private void CreateAcDbSupport(IAccountDatabase accountDatabase, DbTemplate template)
        {
            if (template.CurrentVersion == null)
                return;

            var acDbSupport = new AcDbSupport
            {
                AccountDatabasesID = accountDatabase.Id,
                CurrentVersion = template.CurrentVersion,
                State = (int)SupportState.NotAutorized,
            };

            dbLayer.AcDbSupportRepository.Insert(acDbSupport);
            dbLayer.Save();
        }
    }
}
