﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для обновления статуса инф. базы
    /// </summary>
    [FlowActionDescription("Обновление статуса инф. базы")]
    public class UpdateAccountDatabaseStateAction(
        DatabaseStatusHelper databaseStatusHelper,
        IHandlerException handlerException)
        : ISimpleAction<CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        public CreateAccountDatabaseFromBackupParamsDto Do(CreateAccountDatabaseFromBackupParamsDto model)
        {
           
            try
            {
                databaseStatusHelper.SetDbStatus(model.CreatedAccountDatabaseId, DatabaseState.Ready);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка установки статуса для инф. базы] {model.CreatedAccountDatabaseId}");
                throw;
            }
            

            return model;
        }
    }
}
