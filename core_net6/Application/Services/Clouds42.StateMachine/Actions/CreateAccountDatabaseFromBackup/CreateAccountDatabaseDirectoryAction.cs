﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для создания директории инф. базы
    /// </summary>
    [FlowActionDescription("Создание директории инф. базы")]
    public class CreateAccountDatabaseDirectoryAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabasePathHelper accountDatabasePathHelper,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        public CreateAccountDatabaseFromBackupParamsDto Do(CreateAccountDatabaseFromBackupParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.CreatedAccountDatabaseId);

                if (accountDatabase.IsFile != true)
                    return model;

                var accountDatabasePath = accountDatabasePathHelper.GetPath(accountDatabase);
                if (!Directory.Exists(accountDatabasePath))
                    Directory.CreateDirectory(accountDatabasePath);

                logger.Trace($"Создана директория для инф. базы {accountDatabase.V82Name}");

            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания директории инф. базы] {model.CreatedAccountDatabaseId}");
                throw;
            }

            return model;
        }
    }
}
