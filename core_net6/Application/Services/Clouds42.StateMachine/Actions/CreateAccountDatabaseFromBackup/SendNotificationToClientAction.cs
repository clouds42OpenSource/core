﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для отправки уведомления клиенту о создании инф. базы
    /// </summary>
    [FlowActionDescription("Отправка уведомления клиенту о создании инф. базы")]
    public class SendNotificationToClientAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabaseWebPublishPathHelper acDbPublishWebPublishPathHelper,
        IEmailAddressesDataProvider emailsAddressesDataProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CreateAccountDatabaseFromBackupParamsDto, Guid>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>ID созданной инф. базы</returns>
        public Guid Do(CreateAccountDatabaseFromBackupParamsDto model)
        {
            try
            {
                logger.Info($"Начало отправки письма клиенту о создании ИБ {model.CreatedAccountDatabaseId}");
                
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.CreatedAccountDatabaseId);

                var acDbPublishPath = acDbPublishWebPublishPathHelper.GetWebPublishPath(accountDatabase);

                var emailsToCopy = emailsAddressesDataProvider.GetAllAccountEmailsToSend(accountDatabase.AccountId);

                letterNotificationProcessor.TryNotify<CreateAccountDatabaseLetterNotification, CreateAccountDatabaseLetterModelDto>(
                        new CreateAccountDatabaseLetterModelDto
                        {
                            AccountId = accountDatabase.AccountId,
                            AccountDatabaseCaption = accountDatabase.Caption,
                            AccountDatabasePublishPath = acDbPublishPath,
                            EmailsToCopy = emailsToCopy
                        });

                logger.Info($"Сообщение отправлено клиенту о создании ИБ {model.CreatedAccountDatabaseId}");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка отправки уведомления клиенту о создании инф. базы] {model.CreatedAccountDatabaseId}");
                throw;
            }

            return model.CreatedAccountDatabaseId;
        }
    }
}
