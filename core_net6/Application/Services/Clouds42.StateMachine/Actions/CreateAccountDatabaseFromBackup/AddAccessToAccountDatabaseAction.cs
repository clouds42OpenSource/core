﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для выдачи доступа к инф. базе
    /// </summary>
    [FlowActionDescription("Выдача доступа к инф. базе")]
    public class AddAccessToAccountDatabaseAction(
        IUnitOfWork dbLayer,
        IAcDbAccessProvider acDbAccessProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>Модель параметров создания инф. базы</returns>
        public CreateAccountDatabaseFromBackupParamsDto Do(CreateAccountDatabaseFromBackupParamsDto model)
        {
            try
            {
                var accountAdminId = dbLayer.AccountsRepository.GetAccountAdminIds(model.AccountDatabaseCreationParams.AccountId).FirstOrDefault();
                AddAccess( accountAdminId, model.CreatedAccountDatabaseId, model.AccountDatabaseCreationParams.AccountId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка выдачи доступа к инф. базе] {model.CreatedAccountDatabaseId}");
                throw;
            }

            return model;
        }

        /// <summary>
        /// Добавить доступ к инф. базе 
        /// администратору аккаунта
        /// </summary>
        /// <param name="accountAdminId">ID админа аккаунта</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        private void AddAccess(Guid accountAdminId, Guid accountDatabaseId, Guid accountId)
        {
            if (accountAdminId == Guid.Empty)
                return;

            logger.Debug($"Добавление доступа к базе {accountDatabaseId} для админа {accountAdminId}");

            acDbAccessProvider.GetOrCreateAccess(new AcDbAccessPostAddModelDto
            {
                AccountDatabaseID = accountDatabaseId,
                AccountID = accountId,
                LocalUserID = Guid.Empty,
                AccountUserID = accountAdminId,
                SendNotification = true
            });
        }
    }
}
