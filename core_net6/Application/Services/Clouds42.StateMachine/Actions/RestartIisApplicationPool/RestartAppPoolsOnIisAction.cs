﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.StateMachine.Contracts.Exceptions;

namespace Clouds42.StateMachine.Actions.RestartIisApplicationPool
{
    /// <summary>
    /// Действие для перезапуска пулов приложения на IIS
    /// </summary>
    [FlowActionDescription("Перезапуск пулов приложения на IIS")]
    public class RestartAppPoolsOnIisAction(
        IManageIisApplicationPoolProvider manageIisApplicationPoolProvider,
        IAccountDatabaseChangePublishStateProvider accountDatabaseChangePublishStateProvider)
        : ISimpleAction<RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры перезапуска пула инф. базы на нодах публикации</param>
        /// <returns>Параметры перезапуска пула инф. базы на нодах публикации</returns>
        public RestartIisApplicationPoolParamsDto Do(RestartIisApplicationPoolParamsDto model)
        {
            try
            {
                manageIisApplicationPoolProvider.Restart(model.AppPoolName, model.ContentServerId, model.AccountDatabaseId, model.V82Name);
                return model;
            }
            catch (Exception ex)
            {
                accountDatabaseChangePublishStateProvider.ChangePublishState(PublishState.Published, model.AccountDatabaseId);
                throw new ActionExecutionException(
                    $"Не удалось перезапустить пул инф. базы {model.AppPoolName} на всех нодах публикации. Причина: {ex.Message}");
            }
        }
    }
}
