﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.StateMachine.Contracts.Exceptions;

namespace Clouds42.StateMachine.Actions.RestartIisApplicationPool
{
    /// <summary>
    /// Действие для смены статуса инф. базы на "База в процессе перезапуска пула"
    /// </summary>
    [FlowActionDescription("Запуск пулла IIS")]
    public class ChangeDbPublishStateToRestartingPoolAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseChangePublishStateProvider accountDatabaseChangePublishStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры перезапуска пула инф. базы на нодах публикации</param>
        /// <returns>Параметры перезапуска пула инф. базы на нодах публикации</returns>
        public RestartIisApplicationPoolParamsDto Do(RestartIisApplicationPoolParamsDto model)
        {
            try
            {
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                accountDatabaseChangePublishStateProvider.ChangePublishState(PublishState.RestartingPool, accountDatabase.Id);

                logger.Trace(
                    $"Статус публикации для инф. базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} изменен на {PublishState.RestartingPool.Description()}");

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"При смене статуса публикации инф. базы {model.AccountDatabaseId} произошла ошибка.";
                handlerException.Handle(ex, $"[Ошибка смены статуса публикации инф. базы] {model.AccountDatabaseId}");
                throw new ActionExecutionException($"{errorMessage} Причина: {ex.Message}");
            }
        }
    }
}
