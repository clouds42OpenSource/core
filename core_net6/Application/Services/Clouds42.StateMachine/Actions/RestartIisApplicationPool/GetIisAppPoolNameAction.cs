﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.StateMachine.Contracts.Exceptions;

namespace Clouds42.StateMachine.Actions.RestartIisApplicationPool
{
    /// <summary>
    /// Действие для получения названия пула приложения на IIS
    /// </summary>
    [FlowActionDescription("Получение названия пула приложения на IIS")]
    public class GetIisAppPoolNameAction(
        IIisApplicationPoolDataProvider iisApplicationPoolDataProvider,
        IAccountDatabaseChangePublishStateProvider accountDatabaseChangePublishStateProvider,
        ILogger42 logger)
        : ISimpleAction<RestartIisApplicationPoolParamsDto, RestartIisApplicationPoolParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры перезапуска пула инф. базы на нодах публикации</param>
        /// <returns>Параметры перезапуска пула инф. базы на нодах публикации</returns>
        public RestartIisApplicationPoolParamsDto Do(RestartIisApplicationPoolParamsDto model)
        {
            try
            {
                var operationStartDateTime = DateTime.Now;

                model.AppPoolName = iisApplicationPoolDataProvider.GetAccountDatabaseAppPoolName(model.AccountDatabaseId, model.V82Name);

                logger.Trace(
                    $"Получение названия пула инф. базы {model.AccountDatabaseId} завершено. " +
                    $"Затрачено {operationStartDateTime.CalculateElapsedTimeInSeconds():F1} сек.");

                return model;
            }
            catch (Exception ex)
            {
                accountDatabaseChangePublishStateProvider.ChangePublishState(PublishState.Published, model.AccountDatabaseId);

                throw new ActionExecutionException(
                    $"Не удалось получить название пула для инф. базы {model.AccountDatabaseId}. Причина: {ex.Message}");
            }
        }
    }
}
