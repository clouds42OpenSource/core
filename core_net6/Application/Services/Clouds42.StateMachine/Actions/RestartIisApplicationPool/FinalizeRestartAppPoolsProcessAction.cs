﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.StateMachine.Contracts.Exceptions;

namespace Clouds42.StateMachine.Actions.RestartIisApplicationPool
{
    /// <summary>
    /// Действие для завершения процесса перезапуска пулов приложения на IIS
    /// </summary>
    [FlowActionDescription("Завершение процесса перезапуска пулов приложения на IIS")]
    public class FinalizeRestartAppPoolsProcessAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseChangePublishStateProvider accountDatabaseChangePublishStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<RestartIisApplicationPoolParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Параметры перезапуска пула инф. базы на нодах публикации</param>
        /// <returns>Результат выполнения</returns>
        public bool Do(RestartIisApplicationPoolParamsDto model)
        {
            try
            {
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                accountDatabaseChangePublishStateProvider.ChangePublishState(PublishState.Published, accountDatabase.Id);

                var elapsedTimeToPerformOperation = (DateTime.Now - model.OperationStartDateTime).TotalSeconds;

                logger.Trace(
                    $"Перезапуск пула для инф. базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} завершен. " +
                    $"Затрачено {elapsedTimeToPerformOperation:F1} сек.");

                return true;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"При завершении процесса перезапуска пулов инф. базы {model.AccountDatabaseId} произошла ошибка.";
                handlerException.Handle(ex, $"[Ошибка завершения процесса перезапуска пулов инф. базы] {model.AccountDatabaseId} ");
                throw new ActionExecutionException($"{errorMessage} Причина: {ex.Message}");
            }
        }
            
            
            
    }
}
