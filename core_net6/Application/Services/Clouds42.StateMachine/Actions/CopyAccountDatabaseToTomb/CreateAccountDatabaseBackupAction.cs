﻿using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Сделать копию информационной базы
    /// </summary>
    [FlowActionDescription("Создание копии информационной базы")]
    public class CreateAccountDatabaseBackupAction(
        AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            try
            {

                logger.Trace($"Создание бэкапа для информационной базы {model.AccountDatabaseId}");

                var backupId = accountDatabaseLocalBackupProvider.CreateBackupAccountDatabase(model.AccountDatabaseId,
                    model.BackupAccountDatabaseTrigger, model.AccountUserInitiatorId);

                model.AccountDatabaseBackupId = backupId;

                logger.Trace(
                    $"Создание бэкапа информационной базы {model.AccountDatabaseId} завершено успешно. ID бэкапа: {backupId}");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создании бэкапа информационной базы] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
