﻿using Clouds42.AccountDatabase.Contracts.Backup.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Загрузить бэкап инф. базы в склеп
    /// </summary>
    [FlowActionDescription("Загрузка бэкапа информационной базы в склеп")]
    public class UploadAccountDatabaseBackupAction(
        TombRunnerProvider tombRunnerProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IBackupFileProvider backupFileProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                logger.Trace($"Начало переноса бэкапа '{model.AccountDatabaseBackupId}' инф. базы '{accountDatabase.Id}' {accountDatabase.V82Name} в склеп.");

                backupFileProvider.CheckBackupPathAndUpdateIfNeed(model.AccountDatabaseBackupId);

                model.AccountDatabaseBackupFileId = tombRunnerProvider.UploadBackupToTomb(accountDatabase, model.AccountDatabaseBackupId, model.ForceUpload);

                logger.Trace(
                    $"Перенос бэкапа '{model.AccountDatabaseBackupId}' инф. базы '{accountDatabase.Id}' {accountDatabase.V82Name}" + 
                    $" в склеп успешно заврешен. ID файла: {model.AccountDatabaseBackupFileId}");

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage = $"При переносе бэкапа инф. базы {model.AccountDatabaseId} в склеп возникла ошибка. Причина: ";
                handlerException.Handle(ex, $"[Ошибка переноса бэкапа инф. базы в склеп] {model.AccountDatabaseId}");
                throw new InvalidOperationException($"{errorMessage}{ex.GetMessage()}");
            }
        }
    }
}
