﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Установить статус базе - TransferDb
    /// </summary>
    [FlowActionDescription("Установить статус базе - TransferDb")]
    public class SetTransferDbStateAction(
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            accountDatabaseChangeStateProvider.ChangeState(model.AccountDatabaseId, DatabaseState.TransferDb);
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(model.AccountDatabaseId);

            return model;
        }

    }
}