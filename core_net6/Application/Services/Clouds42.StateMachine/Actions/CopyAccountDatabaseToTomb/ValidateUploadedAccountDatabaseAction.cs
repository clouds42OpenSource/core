﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Проверить загруженный в склеп бэкап информационной базы
    /// </summary>
    [FlowActionDescription("Проверка загруженного в склеп бэкапа информационной базы")]
    public class ValidateUploadedAccountDatabaseAction(
        TombAnalysisProvider tombAnalysisProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                var validationResult = tombAnalysisProvider.ValidateUploadedAccountDatabase(accountDatabase,
                    model.AccountDatabaseBackupId, model.AccountDatabaseBackupFileId);

                if (!validationResult)
                    throw new InvalidOperationException(
                        $"Произошла ошибка при проверке загруженного файла {model.AccountDatabaseBackupFileId} инф. базы {accountDatabase.V82Name}.");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка проверки загруженного файла] {model.AccountDatabaseBackupFileId} инф. базы {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
