﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Удалить локальные данные базы после архивации в склеп
    /// </summary>
    [FlowActionDescription("Удаление локальных данных базы после архивации в склеп")]
    public class ClearAccountDatabaseLocalDataAfterArchivingToTombAction(
        TombCleanerProvider tombCleanerProvider,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                tombCleanerProvider.ClearAccountDatabaseDataAfterArchiveToTomb(accountDatabase,
                    model.AccountDatabaseBackupId);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления локальных данных инф. базы после архивации в склеп] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
