﻿using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb
{
    /// <summary>
    /// Актуализировать данные инф. базы после архивации в склеп
    /// </summary>
    [FlowActionDescription("Актуализация данных информационной базы после архивации в склеп")]
    public class ActualizeAccountDatabaseAfterArchivingToTombAction(
        TombActualizeProvider tombActualizeProvider,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            try
            {
                tombActualizeProvider.ActualizeAccountDatabaseAfterArchiveToTomb(model.AccountDatabaseId,
                    model.AccountDatabaseBackupId, model.AccountDatabaseBackupFileId);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка актуализации данных инф. базы после архивации в склеп] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
