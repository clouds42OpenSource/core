﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.UpdateAccountUser
{
    /// <summary>
    /// Действие по редактированию пользователя в OpenIdProvider
    /// </summary>
    [FlowActionDescription("Редактирование пользователя в OpenIdProvider")]
    public class EditAccountUserInOpenIdProviderAction(
        IOpenIdProvider openIdProvider,
        DesEncryptionProvider desEncryptionProvider,
        IAccountUserDataProvider accountUserDataProvider,
        ILogger42 logger)
        : ISimpleAction<AccountUserDto, AccountUserDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров пользователя</param>
        /// <returns>Модель параметров пользователя</returns>
        public AccountUserDto Do(AccountUserDto model)
        {
            var domainModel = accountUserDataProvider.GetAccountUserOrThrowException(GetAccountUserId(model));
            var locale = domainModel.Account.AccountConfiguration.Locale.Name;
            var openIdModel = new SauriOpenIdControlUserModel
            {
                AccountUserID = domainModel.Id,
                AccountID = domainModel.Account.Id,
                Login = model.Login,
                FullName = CreateUserFullName(model.FirstName, model.LastName, model.MiddleName),
                Phone = PhoneHelper.ClearNonDigits(model.PhoneNumber),
                Password = NeedSendUserPassword(model, domainModel.Password)
                    ? model.Password
                    : string.Empty,
                Email = model.Email
            };

            UpdateInOpenProvider(openIdModel, locale);

            return model;
        }        

        /// <summary>
        /// Обновить данные пользователя в OpenId
        /// </summary>
        /// <param name="openIdModel">Модель параметров пользователя</param>
        private void UpdateInOpenProvider(SauriOpenIdControlUserModel openIdModel, string locale)
        {
            if ((new[] { openIdModel.FullName, openIdModel.Login, openIdModel.Password, openIdModel.Phone, openIdModel.Email }).All(x => x == null))
                return;            

            openIdProvider.ChangeUserProperties(openIdModel, locale);

            logger.Info($"Для пользователя Id={openIdModel.AccountUserID} успешно измены данные" +
                       $"Логин:{openIdModel.Login}, Пароль:{openIdModel.Password}, Почта:{openIdModel.Email}," +
                       $" Телефон:{openIdModel.Phone} Полное Имя:{openIdModel.FullName}, в провайдере OpenID");

        }

        /// <summary>
        /// Признак необходимости отправлять пароль пользователя
        /// </summary>
        /// <param name="accountUserDc">Модель параметров пользователя</param>
        /// <param name="currentUserPasswordHash">Текущий хэш пароля пользователя</param>
        /// <returns>true - если указнный пароль не пуст и не равен текущему паролю</returns>
        private bool NeedSendUserPassword(AccountUserDto accountUserDc, string currentUserPasswordHash)
        {
            if (string.IsNullOrEmpty(accountUserDc.Password))
                return false;

            return desEncryptionProvider.Encrypt(accountUserDc.Password) != currentUserPasswordHash;
        }

        /// <summary>
        /// Получить полное имя пользователя
        /// </summary>
        /// <param name="firstName">Имя</param>
        /// <param name="lastName">Фамилия</param>
        /// <param name="middleName">Отчество</param>
        /// <returns>Полное имя пользователя</returns>
        private string CreateUserFullName(string firstName, string lastName, string middleName)
            => $"{lastName} {firstName} {middleName}";

        /// <summary>
        /// Получить ID пользователя
        /// </summary>
        /// <param name="model">Модель параметров пользователя</param>
        /// <returns>ID пользователя</returns>
        private static Guid GetAccountUserId(AccountUserDto model)
            => model.Id ?? throw new ArgumentException("Не задано значение модели Id");

    }
}
