﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.DataContracts.AccountUser;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.UpdateAccountUser
{
    /// <summary>
    /// Действие редактирования пользователя в ActiveDirectory
    /// </summary>
    [FlowActionDescription("Редактирование пользователя в АД")]
    public class EditAccountUserInAdAction(
        IAccountUserDataProvider accountUserDataProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor)
        : ISimpleAction<AccountUserDto, AccountUserDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <returns>Модель пользователя</returns>
        public AccountUserDto Do(AccountUserDto model)
        {
            var domainModel = accountUserDataProvider.GetAccountUserOrThrowException(GetAccountUserIdOrThrowException(model));
            activeDirectoryTaskProcessor.TryDoImmediately(provider=>provider.EditUser(domainModel.Id, domainModel.Login, model));
            return model;
        }

        /// <summary>
        /// Получить ID пользователя или выкинуть исключение
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <returns>ID пользователя</returns>
        private static Guid GetAccountUserIdOrThrowException(AccountUserDto model)
            => model.Id ?? throw new ArgumentException("Не задано значение модели Id");

    }
}
