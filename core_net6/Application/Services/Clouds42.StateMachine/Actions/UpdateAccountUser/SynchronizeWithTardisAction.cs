﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.UpdateAccountUser
{
    /// <summary>
    /// Действие для уведомления Тардис
    /// </summary>
    [FlowActionDescription("Уведомление Тардиса")]
    public class SynchronizeWithTardisAction(
        SynchronizeAccountUserWithTardisHelper synchronizeAccountUserWithTardisHelper)
        : ISimpleAction<AccountUserIdModel, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель ID пользователя</param>
        /// <returns>Результат выполнения</returns>
        public bool Do(AccountUserIdModel model)
        {
            synchronizeAccountUserWithTardisHelper.SynchronizeAccountUserUpdateViaNewThread(model.AccountUserID);
            return true;
        }
    }
}