﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.CoreWorker.JobWrappers.WebAccessToDatabase;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.UpdateAccountUser
{
    /// <summary>
    /// Действие по обновлению логина
    /// веб доступа базы
    /// </summary>
    [FlowActionDescription("Обновление логина для веб доступа информационных баз")]
    public class UpdateLoginForWebAccessDatabaseAction : ISimpleAction<AccountUserDto, AccountUserDto>
    {
        private readonly IAccountUserDataProvider _accountUserDataProvider;
        private readonly IUnitOfWork _dbLayer;
        private readonly IUpdateLoginForWebAccessDatabaseJobWrapper _updateLoginForWebAccessDatabaseJobWrapper;
        private readonly IHandlerException _handlerException;
        public UpdateLoginForWebAccessDatabaseAction(IAccountUserDataProvider accountUserDataProvider,
            IUnitOfWork dbLayer, 
            IUpdateLoginForWebAccessDatabaseJobWrapper updateLoginForWebAccessDatabaseJobWrapper,
            IHandlerException handlerException)
        {
            _accountUserDataProvider = accountUserDataProvider;
            _dbLayer = dbLayer;
            _updateLoginForWebAccessDatabaseJobWrapper = updateLoginForWebAccessDatabaseJobWrapper;
            _updateLoginForWebAccessDatabaseJobWrapper = updateLoginForWebAccessDatabaseJobWrapper;
            _handlerException = handlerException;
        }

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров пользователя</param>
        /// <returns>Модель параметров пользователя</returns>
        public AccountUserDto Do(AccountUserDto model)
        {
            if (!model.Id.HasValue)
                throw new InvalidOperationException($"Не определен параметр {nameof(model.Id)}");

            try
            {
                var domainModel =
                    _accountUserDataProvider.GetAccountUserOrThrowException(model.Id.Value);

                if (domainModel.Login == model.Login)
                    return model;

                RunUpdateLoginForWebAccessDatabaseTasks(domainModel.Id, domainModel.Login, model.Login);

                return model;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка обновления доступов пользователя к опубликованным инф. базам] {model.Id} ");
                throw;
            }
        }

        /// <summary>
        /// Запустить задачи на обновление логина
        /// для веб доступа информационных баз
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="oldLogin">Старый логин</param>
        /// <param name="newLogin">Новый логин</param>
        private void RunUpdateLoginForWebAccessDatabaseTasks(Guid accountUserId, 
            string oldLogin, string newLogin)
        {
            var accountUserAccesses = GetAccountUserAccessesForPublishedDb(accountUserId);
            var databasesIds = accountUserAccesses.Select(x => x.AccountDatabaseID).ToList();
            _updateLoginForWebAccessDatabaseJobWrapper.Start(new UpdateLoginForWebAccessDatabaseDto
            {
                OldLogin = oldLogin,
                NewLogin = newLogin,
                AccountDatabasesId = databasesIds
            });
        }

        /// <summary>
        /// Получить список доступов пользователя к опубликованным базам
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <returns>Список доступов пользователя к опубликованным базам</returns>
        private List<AcDbAccess> GetAccountUserAccessesForPublishedDb(Guid accountUserId)
            => _dbLayer.AcDbAccessesRepository.Where(access =>
                access.AccountUserID == accountUserId 
                && access.Database.PublishState == PublishState.Published.ToString() 
                && access.Database.State == DatabaseState.Ready.ToString() 
                && access.Database.AccountDatabaseOnDelimiter == null).ToList();
    }
}
