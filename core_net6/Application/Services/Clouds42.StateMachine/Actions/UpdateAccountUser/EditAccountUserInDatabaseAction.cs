﻿using System.Text;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.UpdateAccountUser
{
    /// <summary>
    /// Действие редактирования пользователя в БД
    /// </summary>
    [FlowActionDescription("Редактирование пользователя в БД ядра")]
    public class EditAccountUserInDatabaseAction(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IAccountUserDataProvider accountUserDataProvider,
        IAccountUserUpdateProvider accountUserUpdateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<AccountUserDto, AccountUserIdModel>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <returns>Модель Id пользователя</returns>
        public AccountUserIdModel Do(AccountUserDto model)
        {
            try
            {
                if (!model.Id.HasValue)
                    throw new InvalidOperationException($"Не определен параметр {nameof(model.Id)}");

                var domainModel = accountUserDataProvider
                    .GetAccountUserOrThrowException(GetAccountUserId(model));

                dbLayer.AccountUsersRepository.Reload(domainModel);

                var message = $"Редактирование пользователя {domainModel.Login}";
                var traceInfoBuilder = new StringBuilder();
                traceInfoBuilder.AppendLine($"{message}. ");
                logger.Info(message);

                var updateResult = accountUserUpdateProvider.UpdateExceptRoles(domainModel, model);

                var hasAccessToEditRoles =
                    accessProvider.HasAccessBool(ObjectAction.AccountUsers_RoleEdit, () => domainModel.AccountId);
                var isUserCloudAdmin =
                    domainModel.AccountUserRoles.Any(r => r.AccountUserGroup == AccountUserGroup.CloudAdmin);
                if (hasAccessToEditRoles || isUserCloudAdmin)
                    updateResult.Absorb(accountUserUpdateProvider.UpdateOnlyRoles(domainModel, model));

                var result = new AccountUserIdModel { AccountUserID = model.Id.Value };

                if (!updateResult.WereChangesMade)
                    return result;

                AppendLines(traceInfoBuilder, updateResult.Messages);
                SaveChanges(domainModel, traceInfoBuilder.ToString());
                return result;
            }

            catch(Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования пользователя {model.Login} в БД]");

                throw;
            }
            
        }

        /// <summary>
        /// Получить Id пользователя
        /// </summary>
        /// <param name="model">Модель пользователя</param>
        /// <returns>Id пользователя</returns>
        private static Guid GetAccountUserId(AccountUserDto model) =>
            model.Id ?? throw new ArgumentException($"Не задано значение модели {nameof(model.Id)}");

        /// <summary>
        /// Добавить все строки в аккумулятор
        /// </summary>
        /// <param name="stringBuilder">Аккумулятор</param>
        /// <param name="lines">Аккумулируемые строки</param>
        private void AppendLines(StringBuilder stringBuilder, string[] lines)
        {
            foreach (var message in lines)
            {
                stringBuilder.Append(message);
                stringBuilder.AppendLine(". ");
            }
        }

        /// <summary>
        /// Сохранить изменения в базу данных
        /// </summary>
        /// <param name="accountUser">Отредактированный объект пользователя</param>
        /// <param name="trace">Сообщение для логирования события</param>
        private void SaveChanges(AccountUser accountUser, string trace)
        {
            if (accountUser == null)
                return;

            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                accountUser.EditDate = DateTime.Now;
                dbLayer.AccountUsersRepository.Update(accountUser);

                LogEventHelper.LogEvent(dbLayer, accountUser.AccountId, accessProvider, LogActions.EditUserCard, trace);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception)
            {
                dbScope.Rollback();
                throw;
            }
        }
    }
}
