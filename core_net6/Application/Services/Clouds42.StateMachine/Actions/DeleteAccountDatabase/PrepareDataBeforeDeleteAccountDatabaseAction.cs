﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabase
{
    /// <summary>
    /// Подготовка данных перед удалением инф. базы
    /// </summary>
    [FlowActionDescription("Подготовка данных перед удалением инф. базы")]
    public class PrepareDataBeforeDeleteAccountDatabaseAction : ISimpleAction<DeleteAccountDatabaseParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы</param>
        /// <returns>Модель параметров для копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(DeleteAccountDatabaseParamsDto model)
            => new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                AccountUserInitiatorId = model.InitiatorId
            };
    }
}
