﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabase
{
    /// <summary>
    /// Действие для смены статуса инф. базы на "Удалена с облака"
    /// </summary>
    [FlowActionDescription("Смена статуса инф. базы")]
    public class ChangeAcDbStateToDeletedAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IHandlerException handlerException)
        : ISimpleAction<DeleteAccountDatabaseParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы</param>
        /// <returns>Результат удаления</returns>
        public bool Do(DeleteAccountDatabaseParamsDto model)
        {
           var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
           
           try
           {
               accountDatabaseChangeStateProvider.ChangeState(accountDatabase, DatabaseState.DeletedToTomb);
           }
           catch (Exception ex)
           {
               handlerException.Handle(ex,
                   $"[Ошибка обновления статуса инф. базы] {accountDatabase.Id} :: {accountDatabase.V82Name}");
               throw;
           }

           return true;
        }
    }
}
