﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabase
{
    /// <summary>
    /// Удаление бэкапов инф. базы
    /// </summary>
    [FlowActionDescription("Удаление бэкапов инф. базы")]
    public class DeleteAccountDatabaseBackupsAction : ISimpleAction<DeleteAccountDatabaseParamsDto, DeleteAccountDatabaseParamsDto>
    {
        private readonly IAccountDatabaseDataProvider _accountDatabaseDataProvider;
        private readonly ILogger42 _logger;
        private readonly IDictionary<AccountDatabaseBackupSourceType, Action<Guid>> _mapBackupSourceTypeToDeleteAction;
        private readonly IHandlerException _handlerException;

        public DeleteAccountDatabaseBackupsAction(
            IAccountDatabaseDataProvider accountDatabaseDataProvider, 
            AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider, 
            TombCleanerProvider tombCleanerProvider,
            ILogger42 logger, IHandlerException handlerException)
        {
            _logger = logger;
            _accountDatabaseDataProvider = accountDatabaseDataProvider;
            _handlerException = handlerException;
            _mapBackupSourceTypeToDeleteAction = new Dictionary<AccountDatabaseBackupSourceType, Action<Guid>>
            {
                { AccountDatabaseBackupSourceType.Local, backupId => accountDatabaseLocalBackupProvider.DeleteBackupAccountDatabase(backupId, true) },
                { AccountDatabaseBackupSourceType.GoogleCloud, backupId => tombCleanerProvider.DeleteCloudAccountDatabaseBackup(backupId, true) }
            };
        }

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы</param>
        /// <returns>Модель параметров удаления инф. базы</returns>
        public DeleteAccountDatabaseParamsDto Do(DeleteAccountDatabaseParamsDto model)
        {
            try
            {
                var accountDatabase =
                    _accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (accountDatabase.IsDelimiter())
                    return model;

                DeleteAccountDatabaseBackups(accountDatabase.Id);
                return model;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка удаления бэкапов инф. базы] {model.AccountDatabaseId}");
                throw;
            }
        }

        /// <summary>
        /// Удалить бэкапы инф базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        private void DeleteAccountDatabaseBackups(Guid accountDatabaseId)
        {
            var accountDatabaseBackups = _accountDatabaseDataProvider.GetAccountDatabaseBackups(accountDatabaseId).ToList();

            accountDatabaseBackups.ForEach(backup =>
            {
                _logger.Trace($"Удаление бэкапа {backup.BackupPath} инф. базы {backup.AccountDatabase.V82Name}");
                _mapBackupSourceTypeToDeleteAction[backup.SourceType](backup.Id);
            });
        }
    }
}
