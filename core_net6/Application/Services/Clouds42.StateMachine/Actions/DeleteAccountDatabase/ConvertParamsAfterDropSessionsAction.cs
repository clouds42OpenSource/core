﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabase
{
    /// <summary>
    /// Конвертация модели параметров после удаления активных сессий в базе
    /// </summary>
    [FlowActionDescription("Конвертация модели параметров после удаления активных сессий в базе")]
    public class ConvertParamsAfterDropSessionsAction : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, DeleteAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для копирования инф. базы в склеп</param>
        /// <returns>Модель параметров удаления инф. базы</returns>
        public DeleteAccountDatabaseParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
            => new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                InitiatorId = model.AccountUserInitiatorId
            };
        
    }
}
