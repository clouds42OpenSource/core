﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabase
{
    /// <summary>
    /// Удаление информационной базы
    /// </summary>
    [FlowActionDescription("Удаление информационной базы")]
    public class DeleteAccountDatabaseAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabaseDeleteProvider accountDatabaseDeleteProvider,
        ILogger42 logger)
        : ISimpleAction<DeleteAccountDatabaseParamsDto, DeleteAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы</param>
        /// <returns>Результат удаления</returns>
        public DeleteAccountDatabaseParamsDto Do(DeleteAccountDatabaseParamsDto model)
        {
            try
            {
                logger.Info($"[{model.AccountDatabaseId}] :: Начато физическое удаление базы.");

                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (accountDatabase.StateEnum == DatabaseState.Ready || accountDatabase.StateEnum == DatabaseState.DelitingToTomb)
                    DeleteAccountDatabase(accountDatabase);

                return model;
            }
            catch (Exception ex)
            {
                logger.Warn(
                    $"При удалении инф. базы {model.AccountDatabaseId} произошла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }

        /// <summary>
        /// Удалить инф. базу аккаунта
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        private void DeleteAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.IsDelimiter())
                return;

            logger.Info($"[{accountDatabase.Id}::{accountDatabase.V82Name}] :: Физическое удаление базы НЕ на разделителях.");

            var result = accountDatabaseDeleteProvider.DeleteLocalAccountDatabase(accountDatabase);
            if (!result)
                throw new InvalidOperationException($"Не удалось удалить информационную базу: {accountDatabase.V82Name}");
        }
    }
}
