﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ManagePublishedAcDbRedirect
{
    /// <summary>
    /// Установить редирект для опубликованной инф. базы
    /// </summary>
    [FlowActionDescription("Установка редиректа для опубликованной инф. базы")]
    public class InstallPublishedAcDbRedirectAction(
        AccountDatabaseRedirectFileHelper accountDatabaseRedirectFileHelper,
        ILogger42 logger)
        : ISimpleAction<ManagePublishedAcDbRedirectParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров управления редиректом опубликованной инф. базы</param>
        /// <returns>Результат установки</returns>
        public bool Do(ManagePublishedAcDbRedirectParamsDto model)
        {
            try
            {
                var lockXmlForWebConfig = accountDatabaseRedirectFileHelper.GetLockXmlForWebConfig(model.AccountId);

                var lockWebConfigFullPath =
                    accountDatabaseRedirectFileHelper.GetFullPathForLockWebConfigFile(model.PublishedDatabasePhysicalPath);

                var webConfigFullPath =
                    accountDatabaseRedirectFileHelper.GetFullPathForWebConfigFile(model.PublishedDatabasePhysicalPath);

                accountDatabaseRedirectFileHelper.DeleteWebConfigFile(lockWebConfigFullPath);

                if (File.Exists(webConfigFullPath))
                {
                    var currentWtbConfigData = File.ReadAllText(webConfigFullPath);
                    File.WriteAllText(lockWebConfigFullPath, currentWtbConfigData);
                    File.WriteAllText(webConfigFullPath, lockXmlForWebConfig);
                }
                else
                {
                    File.WriteAllText(webConfigFullPath, lockXmlForWebConfig);
                }

                if (!File.Exists(webConfigFullPath))
                    accountDatabaseRedirectFileHelper.CreateWebConfigFile(model.CompanyWebGroupName,
                        model.PublishedDatabasePhysicalPath, model.Module1CPath);

                logger.Trace($"Установлен редирект для базы '{webConfigFullPath}' ");

                return true;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"При установке редиректа для инф. базы {model.AccountDatabaseV82Name} возникла ошибка. Прична: {ex.GetFullInfo(false)}");
            }
        }
    }
}
