﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ManagePublishedAcDbRedirect
{
    /// <summary>
    /// Удалить редирект для опубликованной инф. базы
    /// </summary>
    [FlowActionDescription("Удаление редиректа для опубликованной инф. базы")]
    public class RemovePublishedAcDbRedirectAction(
        AccountDatabaseRedirectFileHelper accountDatabaseRedirectFileHelper,
        ILogger42 logger)
        : ISimpleAction<ManagePublishedAcDbRedirectParamsDto, ManagePublishedAcDbRedirectParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров управления редиректом опубликованной инф. базы</param>
        /// <returns>Результат удаления</returns>
        public ManagePublishedAcDbRedirectParamsDto Do(ManagePublishedAcDbRedirectParamsDto model)
        {
            try
            {
                var lockWebConfigFullPath =
                    accountDatabaseRedirectFileHelper.GetFullPathForLockWebConfigFile(model.PublishedDatabasePhysicalPath);

                var webConfigFullPath =
                    accountDatabaseRedirectFileHelper.GetFullPathForWebConfigFile(model.PublishedDatabasePhysicalPath);

                if (accountDatabaseRedirectFileHelper.LockFileContainsConfigurations(lockWebConfigFullPath))
                {
                    var lockConfigText = File.ReadAllText(lockWebConfigFullPath);
                    File.WriteAllText(webConfigFullPath, lockConfigText);
                    logger.Trace($"Разблокирование базы. Скопировали старые конфигурации публикации для базы '{webConfigFullPath}'");
                }
                else
                {
                    accountDatabaseRedirectFileHelper.DeleteWebConfigFile(webConfigFullPath);

                    accountDatabaseRedirectFileHelper.CreateWebConfigFile(model.CompanyWebGroupName,
                        model.PublishedDatabasePhysicalPath, model.Module1CPath);
                    logger.Trace($"Разблокирование базы. Создали новый файл публикации '{webConfigFullPath}'");
                }

                accountDatabaseRedirectFileHelper.DeleteWebConfigFile(lockWebConfigFullPath);

                return model;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"При удалении редиректа для инф. базы {model.AccountDatabaseV82Name} возникла ошибка. Прична: {ex.GetFullInfo(false)}");
            }
        }
    }
}
