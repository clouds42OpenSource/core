﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ManagePublishedAcDbRedirect
{
    /// <summary>
    /// Изменить платформу после снятия редиректа с базы
    /// </summary>
    [FlowActionDescription("Смена платформы после снятия редиректа с базы")]
    public class ChangePlatformVersionAfterRemoveRedirectAction(IDatabaseWebPublisher databaseWebPublisher)
        : ISimpleAction<ManagePublishedAcDbRedirectParamsDto, bool>
    {
        public bool Do(ManagePublishedAcDbRedirectParamsDto model)
        {
            try
            {
                databaseWebPublisher.ModifyVersionPlatformInWebConfig(model.AccountDatabaseV82Name,
                    model.AccountId.GetEncodeGuid(), model.Module1CPath);
                return true;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    $"При смене платформы после снятия редиректа с базы {model.AccountDatabaseV82Name} произошла обшибка. Описание ошибки: {ex.Message}");
            }
        }
    }
}
