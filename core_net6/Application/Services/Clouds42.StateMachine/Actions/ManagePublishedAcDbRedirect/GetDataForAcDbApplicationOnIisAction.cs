﻿using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ManagePublishedAcDbRedirect
{
    /// <summary>
    /// Получить данные для инф. базы на сервере публикаций
    /// </summary>
    [FlowActionDescription("Получение данных для инф. базы на сервере публикаций")]
    public class GetDataForAcDbApplicationOnIisAction(
        IUnitOfWork dbLayer,
        AccountDataProvider accountDataProvider,
        ISegmentHelper segmentHelper,
        ILogger42 logger,
        IisApplicationDataHelper iisApplicationDataHelper,
        IHandlerException handlerException)
        : ISimpleAction<ManagePublishedAcDbRedirectParamsDto, ManagePublishedAcDbRedirectParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров управления редиректом опубликованной инф. базы</param>
        /// <returns>Результат установки</returns>
        public ManagePublishedAcDbRedirectParamsDto Do(ManagePublishedAcDbRedirectParamsDto model)
        {
            try
            {
                var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);

                var accountContentServer = segmentHelper.GetContentServer(account);
                var publishSiteName = segmentHelper.GetPublishSiteName(account);

                logger.Debug($"Сервер публикаций аккаунта: {accountContentServer.Name}");

                string publishedDatabasePhysicalPath;
                var contentServerNodes = dbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
                var site = iisApplicationDataHelper.GetPublishSite(contentServerNodes.PublishNodeReference.Address,
                    publishSiteName);
                logger.Debug($"Стандартный сайт, в который публикуются все базы: {site}");

                var folder = site.PhysicalPath;
                logger.Debug($"Путь к приложению инф. базы {model.AccountDatabaseV82Name}: {folder}");

                publishedDatabasePhysicalPath = segmentHelper.GetPublishedDatabasePhycicalPath(folder, account.GetEncodeAccountId(), model.AccountDatabaseV82Name, account);

                model.CompanyWebGroupName = $"company_{account.IndexNumber}_web";
                model.PublishedDatabasePhysicalPath = publishedDatabasePhysicalPath;

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения данных для инф. базы на сервере публикации] {model.AccountDatabaseV82Name}");
                throw;
            }
        }
    }
}
