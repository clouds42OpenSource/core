﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums.AccountDatabase;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Подготовить модель параметров копирования инф. базы в склеп 
    /// </summary>
    [FlowActionDescription("Подготовка модели параметров копирования инф. базы в склеп")]
    public class PrepareParamsForSendDbToTombAction : ISimpleAction<MigrateAccountDatabaseParametersDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(MigrateAccountDatabaseParametersDto model)
            => CreateAccountDatabaseBackupToTombParamsDc(model);

        /// <summary>
        /// Создать модель параметров копирования инф. базы в склеп
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        private CopyAccountDatabaseBackupToTombParamsDto CreateAccountDatabaseBackupToTombParamsDc(MigrateAccountDatabaseParametersDto model)
            => new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                AccountUserInitiatorId = model.AccountUserInitiatorId,
                BackupAccountDatabaseTrigger = CreateBackupAccountDatabaseTrigger.MigrateAccountDatabase,
                DestinationFileStorageId = model.DestinationFileStorageId,
                OperationStartDate = model.OperationStartDate
            };
    }
}
