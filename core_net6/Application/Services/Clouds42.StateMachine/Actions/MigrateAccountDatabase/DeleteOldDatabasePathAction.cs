﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Migration.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Действие для удаления старой директории базы
    /// </summary>
    [FlowActionDescription("Удаление директории инф. базы в старом хранилище")]
    public class DeleteOldDatabasePathAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        DatabaseMigrationHelper databaseMigrationHelper,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<MigrateAccountDatabaseParametersDto, DatabaseMigrationResultDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель результата миграции инф. базы</returns>
        public DatabaseMigrationResultDto Do(MigrateAccountDatabaseParametersDto model)
        {
            try
            {
                logger.Trace($"Удаление старой директории инф. базы {model.AccountDatabaseId} по пути {model.OldDatabasePath}");
                Directory.Delete(model.OldDatabasePath, true);

                logger.Trace($"Удаление старой директории инф. базы {model.AccountDatabaseId} по пути {model.OldDatabasePath} выполнено успешно.");

                accountDatabaseChangeStateProvider.ChangeState(model.AccountDatabaseId, DatabaseState.Ready);

                return CreateMigrationResult(model);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления старой директории инф. базы] {model.AccountDatabaseId} по пути {model.OldDatabasePath}");
                throw;
            }
        }

        /// <summary>
        /// Создать результат миграции инф. базы
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель результата миграции инф. базы</returns>
        private DatabaseMigrationResultDto CreateMigrationResult(MigrateAccountDatabaseParametersDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            var sourceFileStorage = databaseMigrationHelper.GetFileStorageServerOrThrowException(model.SourceFileStorageId);
            var targetFileStorage = databaseMigrationHelper.GetFileStorageServerOrThrowException(model.DestinationFileStorageId);

            var resultMessage =
                $"База \"{accountDatabase.Caption}\"({accountDatabase.V82Name}) перенесена с хранилища {sourceFileStorage.ConnectionAddress} в хранилище {targetFileStorage.ConnectionAddress}.";

            logger.Trace(resultMessage);

            return new DatabaseMigrationResultDto
            {
                Result = true,
                DatabaseId = model.AccountDatabaseId,
                ErrorMessage = resultMessage,
                AccountUserInitiatorId = model.AccountUserInitiatorId,
                AccountDatabaseOwnerId = accountDatabase.AccountId
            };
        }
    }
}
