﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Завершить процесс копирования инф. базы в склеп
    /// </summary>
    [FlowActionDescription("Завершение процесса копирования инф. базы в склеп")]
    public class FinalizeСopyingAccountDatabaseToTombAction : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, MigrateAccountDatabaseParametersDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
            => CreateMigrateAccountDatabaseParametersDc(model);

        /// <summary>
        /// Создать модель параметров миграции инф. базы
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        private MigrateAccountDatabaseParametersDto CreateMigrateAccountDatabaseParametersDc(CopyAccountDatabaseBackupToTombParamsDto model)
            => new()
            {
                AccountDatabaseId = model.AccountDatabaseId,
                AccountUserInitiatorId = model.AccountUserInitiatorId,
                OperationStartDate = model.OperationStartDate,
                DestinationFileStorageId = model.DestinationFileStorageId,
            };
    }
}
