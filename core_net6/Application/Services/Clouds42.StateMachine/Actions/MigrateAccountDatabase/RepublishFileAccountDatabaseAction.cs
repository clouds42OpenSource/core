﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Действие для переопубликации файловой инф. базы
    /// </summary>
    [FlowActionDescription("Переопубликация файловой инф. базы")]
    public class RepublishFileAccountDatabaseAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AccountDatabasePublishManager accountDatabasePublishManager,
        ILogger42 logger)
        : ISimpleAction<MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto Do(MigrateAccountDatabaseParametersDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (accountDatabase.PublishStateEnum != PublishState.Published && accountDatabase.PublishStateEnum != PublishState.RestartingPool)
                    return model;

                logger.Info($"[{accountDatabase.Id}] :: Начало процесса перепубликации базы {accountDatabase.V82Name}.");
                accountDatabasePublishManager.RepublishDatabaseWithWaiting(accountDatabase.Id);
                logger.Info($"[{accountDatabase.Id}] :: Процесс перепубликации базы {accountDatabase.V82Name} успешно завершен.");

                return model;
            }
            catch (Exception ex)
            {
                logger.Info($"[{model.AccountDatabaseId}] :: В процессе перепубликации базы возникла ошибка. Причина: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
