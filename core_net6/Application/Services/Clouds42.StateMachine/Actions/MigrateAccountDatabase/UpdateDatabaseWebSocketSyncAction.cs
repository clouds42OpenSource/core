﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Действие для синхронизации информационной базы с WebSocketBridge
    /// </summary>
    [FlowActionDescription("Синхронизация информационной базы с WebSocketBridge")]
    public class UpdateDatabaseWebSocketSyncAction(IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider)
        : ISimpleAction<MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto Do(MigrateAccountDatabaseParametersDto model)
        {
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(model.AccountDatabaseId);
            return model;
        }
    }
}