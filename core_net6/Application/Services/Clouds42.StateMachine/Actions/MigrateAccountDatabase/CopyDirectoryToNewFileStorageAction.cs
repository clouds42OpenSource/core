﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Скопировать директорию в новое хранилище
    /// </summary>
    [FlowActionDescription("Копирование директории в новое хранилище")]
    public class CopyDirectoryToNewFileStorageAction(
        IUnitOfWork dbLayer,
        AccountDatabasePathHelper accountDatabasePathHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        CreatePathForNewFileStorageHelper createPathForNewFileStorageHelper,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto Do(MigrateAccountDatabaseParametersDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                var sourcePath = accountDatabasePathHelper.GetPath(accountDatabase);

                if (accountDatabase.FileStorageID == null)
                    throw new InvalidOperationException($"Для базы {accountDatabase.V82Name} не указан ID хранилища");

                model.OldDatabasePath = sourcePath;
                model.SourceFileStorageId = accountDatabase.FileStorageID.Value;

                var targetPath = createPathForNewFileStorageHelper.Create(accountDatabase, model.DestinationFileStorageId);

                logger.Trace(
                    $"Начало процесса переноса файлов клиента {accountDatabase.Account.IndexNumber} из {sourcePath} в {targetPath}");

                CopyDirectory(sourcePath, targetPath);

                logger.Trace(
                    $"Перенос файлов клиента {accountDatabase.Account.IndexNumber} из {sourcePath} в {targetPath} успешно заврешен");

                UpdateAccountDatabaseFileStorageId(accountDatabase, model.DestinationFileStorageId);

                accountDatabaseChangeStateProvider.ChangeState(accountDatabase.Id, DatabaseState.Ready);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка переноса файлов клиента]");
                throw;
            }
        }

        /// <summary>
        /// Скопировать директорию
        /// </summary>
        /// <param name="sourcePath">Исходный путь</param>
        /// <param name="targetPath">Новый путь</param>
        private void CopyDirectory(string sourcePath, string targetPath)
        {
            try
            {
                logger.Trace($"Начало процесса переноса директории {sourcePath} в {targetPath}");
                var dirInf = new DirectoryInfo(sourcePath);

                if (!Directory.Exists(targetPath))
                    Directory.CreateDirectory(targetPath);

                ClearTargetDirectory(targetPath);

                foreach (var fileInfo in dirInf.GetFiles())
                {
                    var newPath = Path.Combine(targetPath, fileInfo.Name);
                    logger.Trace($"new path: {newPath}");
                    fileInfo.CopyTo(newPath);
                }

                foreach (var directoryInfo in dirInf.GetDirectories())
                {
                    CopyDirectory(directoryInfo.FullName, Path.Combine(targetPath, directoryInfo.Name));
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка переноса директории] из {sourcePath} в {targetPath}");
                throw;
            }
        }

        /// <summary>
        /// Очистить папку инф. базы в новом файловом хранилище
        /// </summary>
        /// <param name="targetPath">Новый путь</param>
        private void ClearTargetDirectory(string targetPath)
        {
            var dir = new DirectoryInfo(targetPath);

            foreach (var fi in dir.GetFiles())
            {
                try
                {
                    fi.Delete();
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Невозможно удалить файл] {fi.FullName}");
                }
            }

            foreach (var di in dir.GetDirectories())
            {
                ClearTargetDirectory(di.FullName);
                try
                {
                    di.Delete();
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Невозможно удалить файл] {di.FullName}");
                }
            }
        }

        /// <summary>
        /// Обновить ID файлового хранилища для инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="newFileStorageId">ID файлового хранилища</param>
        private void UpdateAccountDatabaseFileStorageId(Domain.DataModels.AccountDatabase accountDatabase, Guid newFileStorageId)
        {
            accountDatabase.FileStorageID = newFileStorageId;
            dbLayer.DatabasesRepository.Update(accountDatabase);

            dbLayer.Save();
        }
    }
}
