﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Managers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.MigrateAccountDatabase
{
    /// <summary>
    /// Выдать доступ на папку внешним пользователям
    /// </summary>
    [FlowActionDescription("Выдача доступа на папку внешним пользователям")]
    public class AddAccessToExternalUsersAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        AcDbAccessManager acDbAccessManager,
        ILogger42 logger)
        : ISimpleAction<MigrateAccountDatabaseParametersDto, MigrateAccountDatabaseParametersDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <returns>Модель параметров миграции инф. базы</returns>
        public MigrateAccountDatabaseParametersDto Do(MigrateAccountDatabaseParametersDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            var myUsers = accountDatabase.Account.AccountUsers.Select(a => a.Id);
            var users = accountDatabase.AcDbAccesses.Select(a => a.AccountUser.Id).Except(myUsers).ToList();
            
            foreach (var accountUserId in users)
            {
                try
                {
                    var result = acDbAccessManager.ChangeAccessForDbFolder(accountDatabase.Id, accountUserId, AclAction.SetAccessControl);
                    if (result.Error)
                        throw new InvalidOperationException(result.Message);
                }
                catch (Exception ex)
                {
                    logger.Info(
                        $"[{accountDatabase.Id}] :: Ошибка при выдаче прав на папку инф. базы {accountDatabase.V82Name} пользователю {accountUserId}. {ex.GetFullInfo()}");
                }
            }

            return model;
        }
    }
}
