﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{
    /// <summary>
    /// Действие для создания инф. базы из zip в МС
    /// </summary>
    [FlowActionDescription("Создание инф. базы из zip в Менеджере Сервисов")]
    public class CreateDatabaseFromZipInMsAction(
        DatabaseStatusHelper databaseStatusHelper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        CreateFromZipNewDelimiterCommand createFromZipNewDelimiterCommand,
        IHandlerException handlerException)
        : ISimpleAction<CreateDatabaseFromZipInMsParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы из zip в МС</param>
        /// <returns>Результат выполнения</returns>
        public bool Do(CreateDatabaseFromZipInMsParamsDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            try
            {
                var result = createFromZipNewDelimiterCommand.Execute(model);
                if (!string.IsNullOrEmpty(result))
                {
                    databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorCreate, result);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                databaseStatusHelper.SetDbStatus(accountDatabase.V82Name, DatabaseState.ErrorCreate, "Ошибка при загрузке данных");

                handlerException.Handle(ex,
                    $"[Ошибка выполнения запроса на создание инф. базы в МС] {model.AccountDatabaseId}");

                throw new InvalidOperationException($"При выполнении запроса на создание инф. базы {model.AccountDatabaseId} в МС возникла ошибка.");
            }
        }
    }
}
