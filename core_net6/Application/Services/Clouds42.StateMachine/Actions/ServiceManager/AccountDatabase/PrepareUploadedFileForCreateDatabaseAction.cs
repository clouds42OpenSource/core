﻿using Clouds42.Configurations.Configurations;
using Clouds42.CoreWorker.AccountDatabasesOnDelimiters.JobWrappers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{
    /// <summary>
    /// Действие для подготовки загруженного файла к созданию инф. базы в МС
    /// </summary>
    [FlowActionDescription("Подготовка загруженного файла к созданию инф. базы в Менеджере Сервисов")]
    public class PrepareUploadedFileForCreateDatabaseAction(
        IUploadAcDbZipFileThroughWebServiceJobWrapper uploadAcDbZipFileThroughWebServiceJobWrapper,
        IHandlerException handlerException)
        : ISimpleAction<CreateDatabaseFromZipInMsParamsDto, CreateDatabaseFromZipInMsParamsDto>
    {
        private readonly bool _needUseNewMechanismForUploadFile = CloudConfigurationProvider.DbTemplateDelimiters.GetNeedUseNewMechanismForUploadFile();

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы из zip в МС</param>
        /// <returns>Результат выполнения</returns>
        public CreateDatabaseFromZipInMsParamsDto Do(CreateDatabaseFromZipInMsParamsDto model)
        {
            try
            {
                if (!_needUseNewMechanismForUploadFile)
                    return model;

                var uploadResult = uploadAcDbZipFileThroughWebServiceJobWrapper.Start(
                    new UploadAcDbZipFileThroughWebServiceParamsDto
                    {
                        UploadedFileId = model.UploadedFileId
                    }).WaitResult();

                if (!uploadResult.IsSuccess)
                    return model;

                model.SmUploadedFileId = uploadResult.SmUploadedFileId;
                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"При подготовке загруженного файла {model.UploadedFileId} к созданию инф. базы {model.AccountDatabaseId} в МС возникла ошибка.";

                handlerException.Handle(ex, $"[Ошибка подготовки загруженного файла к созданию инф. базы в МС] файл: {model.UploadedFileId} база: {model.AccountDatabaseId}");
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
