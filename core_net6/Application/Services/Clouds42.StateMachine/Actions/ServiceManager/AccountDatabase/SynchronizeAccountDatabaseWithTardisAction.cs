﻿using Clouds42.AccountDatabase.Helpers;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{
    /// <summary>
    /// Действие для синхронизации информационной базы с Тардис
    /// </summary>
    [FlowActionDescription("Синхронизация информационной базы с Тардис")]
    public class SynchronizeAccountDatabaseWithTardisAction(
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        IHandlerException handlerException)
        : ISimpleAction<UpdateAccountDatabaseCaptionParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров смены названия инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public bool Do(UpdateAccountDatabaseCaptionParamsDto model)
        {
            try
            {
                synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(model.AccountDatabaseId);

                return true;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка синхронизации инф. базы c Тардисом] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
