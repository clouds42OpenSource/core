﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{

    /// <summary>
    /// Действие для синхронизации информационной базы с WebSocketBridge
    /// </summary>
    [FlowActionDescription("Синхронизация информационной базы с WebSocketBridge")]
    public class UpdateDatabaseWebSocketSyncAction(IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider)
        : ISimpleAction<UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров смены названия инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public UpdateAccountDatabaseCaptionParamsDto Do(UpdateAccountDatabaseCaptionParamsDto model)
        {
            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(model.AccountDatabaseId);
            return model;
        }

    }
}