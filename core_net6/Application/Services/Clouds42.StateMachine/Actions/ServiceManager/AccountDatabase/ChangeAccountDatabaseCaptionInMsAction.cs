﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{
    /// <summary>
    /// Действие для смены названия инф. базы в МС
    /// </summary>
    [FlowActionDescription("Смена названия инф. базы в Менеджере Сервисов")]
    public class ChangeAccountDatabaseCaptionInMsAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        RenameApplicationCommand renameApplicationCommand,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров смены названия инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public UpdateAccountDatabaseCaptionParamsDto Do(UpdateAccountDatabaseCaptionParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (!accountDatabase.IsDelimiter())
                    return model;

                if (accountDatabase.AccountDatabaseOnDelimiter.IsDemo && accountDatabase.AccountDatabaseOnDelimiter.Zone == null)
                {
                    logger.Trace($"[{accountDatabase.Id}]: База демо менять ничего не надо");
                    return model;
                }

                var location = accountDatabase.Account.AccountConfiguration.Locale.Name;
                logger.Trace($"[{accountDatabase.Id}]: Отправка на изменение имени базы на {model.Caption}");
                renameApplicationCommand.Execute(accountDatabase.Id, model.Caption, location);
                logger.Trace($"[{accountDatabase.Id}]: Имя базы в МС изменено на {model.Caption}");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены названия инф. базы в МС] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
