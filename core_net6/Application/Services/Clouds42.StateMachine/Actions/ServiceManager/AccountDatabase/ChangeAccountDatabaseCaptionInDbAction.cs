﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ServiceManager.AccountDatabase
{
    /// <summary>
    /// Действие для смены названия инф. базы в Бд
    /// </summary>
    [FlowActionDescription("Смена названия инф. базы в базе данных")]
    public class ChangeAccountDatabaseCaptionInDbAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ISimpleAction<UpdateAccountDatabaseCaptionParamsDto, UpdateAccountDatabaseCaptionParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров смены названия инф. базы</param>
        /// <returns>Результат выполнения</returns>
        public UpdateAccountDatabaseCaptionParamsDto Do(UpdateAccountDatabaseCaptionParamsDto model)
        {
            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                accountDatabase.Caption = model.Caption;
                dbLayer.DatabasesRepository.InsertOrUpdateAccountDatabaseValue(accountDatabase);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены названия инф. базы] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
