﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Segment.Contracts.DatabaseFilesOnFileStorageServer.Interfaces;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для закрытия открытых файлов инф. базы на файловом хранилище
    /// </summary>
    [FlowActionDescription("Закрытие открытых файлов инфомационной базы на файловом хранилище")]
    public class CloseOpenDatabaseFilesOnFileStorageAction(
        ILogger42 logger,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        ICloseOpenDatabaseFilesOnFileStorageProvider closeOpenDatabaseFilesOnFileStorageProvider,
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            try
            {
                logger.Trace("Закрытие открытых файлов инфомационной базы на файловом хранилище");
                var accountDatabase = terminationSessionsInDatabaseDataProvider.GetDatabaseByTerminationSessionsIdOrThrowException(model.TerminationSessionsInDatabaseId);

                if (!accountDatabase.CanTerminate1CProcessesInTerminalServer())
                    return model;

                closeOpenDatabaseFilesOnFileStorageProvider.Close(accountDatabase.Id);

                logger.Trace(
                    $"Закрытие открытых файлов инфомационной базы '{accountDatabase.Id}':'{accountDatabase.V82Name}' на файловом хранилище завершилось успешно");
                

                 return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Закрытие открытых файлов инфомационной базы на файловом хранилище завершено с ошибкой :: {ex.Message}";
                registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Error
                });
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
