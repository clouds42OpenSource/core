﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для закрытия открытых сессий для баз на разделителях
    /// </summary>
    [FlowActionDescription("Закрытие открытых сессий для баз на разделителях")]
    public class CloseOpenSessionsFromDelimitersAction(
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        ICloseSessionInDbOnDelimitersCommand closeSessionInDbOnDelimitersCommand,
        ILogger42 logger,
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            try
            {
                logger.Trace("Закрытие открытых сессий для баз на разделителях");
                var accountDatabase = terminationSessionsInDatabaseDataProvider.GetDatabaseByTerminationSessionsIdOrThrowException(model.TerminationSessionsInDatabaseId);

                if (!accountDatabase.IsDelimiter())
                    return model;

                return closeSessionInDbOnDelimitersCommand.Execute(accountDatabase, model);
            }
            catch (Exception ex)
            {
                registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Error
                });
                var errorMessage =
                    $"Закрытие открытых сессий для баз на разделителях завершено с ошибкой :: {ex.Message}";

                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
