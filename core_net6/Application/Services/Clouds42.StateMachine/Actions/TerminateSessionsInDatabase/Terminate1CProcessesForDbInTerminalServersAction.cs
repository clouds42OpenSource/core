﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для завершения 1С процессов инфомационной базы на терминальных серверах
    /// </summary>
    [FlowActionDescription("Завершения 1С процессов инфомационной базы на терминальных серверах")]
    public class Terminate1CProcessesForDbInTerminalServersAction(
        ILogger42 logger,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        ITerminate1CProcessesForDbInTerminalServersProvider terminate1CProcessesForDbInTerminalServersProvider,
        IHandlerException handlerException)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            try
            {
                var accountDatabase = terminationSessionsInDatabaseDataProvider
                    .GetDatabaseByTerminationSessionsIdOrThrowException(model.TerminationSessionsInDatabaseId);

                if (!accountDatabase.CanTerminate1CProcessesInTerminalServer())
                    return model;

                terminate1CProcessesForDbInTerminalServersProvider.Terminate(accountDatabase.Id);

                logger.Trace(
                    $"Завершение 1С процессов инфомационной базы '{accountDatabase.Id}':'{accountDatabase.V82Name}' на терминальных серверах завершено успешно");

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    "Завершение процессов инфомационной базы на терминальных серверах завершено с ошибкой";
                handlerException.Handle(ex, "[Завершение процессов инфомационной базы на терминальных серверах]");
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
