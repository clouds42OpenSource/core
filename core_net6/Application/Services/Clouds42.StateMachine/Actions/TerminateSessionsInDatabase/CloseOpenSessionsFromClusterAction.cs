﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для закрытия открытых сессий на кластере и sql
    /// </summary>
    [FlowActionDescription("Закрытие открытых сессий на кластере и sql")]
    public class CloseOpenSessionsFromClusterAction(
        ILogger42 logger,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        IDropSessionsFromClusterJobWrapper dropSessionsFromClusterJobWrapper,
        AccountDatabaseDeleteProvider accountDatabaseDeleteProvider,
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            try
            {
                logger.Trace("Закрытие открытых сессий на кластере и sql");
                var accountDatabase = terminationSessionsInDatabaseDataProvider.GetDatabaseByTerminationSessionsIdOrThrowException(model.TerminationSessionsInDatabaseId);

                if (accountDatabase.IsDelimiter())
                    return model;

                if (accountDatabase.IsFile == true)
                    return model;

                dropSessionsFromClusterJobWrapper.Start(new DropSessionsFromClusterJobParams
                {
                    AccountDatabaseId = accountDatabase.Id
                }).Wait();

                logger.Info($"[{accountDatabase.Id}] :: Удаление активных сессий. Done!");

                accountDatabaseDeleteProvider.DropSessionOnSql(accountDatabase);
                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Закрытие открытых сессий на кластере и sql завершено с ошибкой :: {ex.Message}";
                registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Error
                });
                throw new InvalidOperationException(errorMessage);
            }
        }
    }
}
