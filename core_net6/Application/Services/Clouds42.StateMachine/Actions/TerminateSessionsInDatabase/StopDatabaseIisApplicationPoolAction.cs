﻿using Clouds42.AccountDatabase.Contracts.IISApplication.Interfaces;
using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для остановки пула приложения инф. базы на всех нодах публикации
    /// </summary>
    [FlowActionDescription("Остановка пула приложения инф. базы на всех нодах публикации")]
    public class StopDatabaseIisApplicationPoolAction(
        ILogger42 logger,
        ITerminationSessionsInDatabaseDataProvider terminationSessionsInDatabaseDataProvider,
        IManageIisApplicationPoolProvider manageIisApplicationPoolProvider,
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto, TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            try
            {
                logger.Trace(
                    "Остановка пула приложения инф. базы на всех нодах публикаци");
                var accountDatabase = terminationSessionsInDatabaseDataProvider
                .GetDatabaseByTerminationSessionsIdOrThrowException(model.TerminationSessionsInDatabaseId);

                if (!accountDatabase.CanRestartIisApplicationPool())
                    return model;

                if (accountDatabase.IsDelimiter())
                    return model;

                manageIisApplicationPoolProvider.StopForDatabase(accountDatabase.Id, accountDatabase.V82Name);

                logger.Trace(
                    $"Остановка пула приложения инф. базы '{accountDatabase.Id}':'{accountDatabase.V82Name}' завершилась успешно");

            }
            catch (Exception ex)
            {
                logger.Warn(
                    $"Остановка пула приложения инф. базы '{model.TerminationSessionsInDatabaseId}' завершилась с ошибкой {ex.Message}");
                registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Error
                });
            }

            return model;
        }
    }
}
