﻿using Clouds42.AccountDatabase.Contracts.TerminateSessions;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.TerminateSessionsInDatabase
{
    /// <summary>
    /// Действие для для регистрации результата завершения
    /// сеансов в информационной базе
    /// </summary>
    [FlowActionDescription("Регистрация результата завершения сеансов в информационной базе")]
    public class RegisterTerminationSessionsInDatabaseResultAction(
        IRegisterTerminationSessionsInDatabaseResultProvider registerTerminationSessionsInDatabaseResultProvider)
        : ISimpleAction<TerminateSessionsInDatabaseJobParamsDto,
            TerminateSessionsInDatabaseJobParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <returns>Модель параметров для завершения сеансов в информационной базе</returns>
        public TerminateSessionsInDatabaseJobParamsDto Do(TerminateSessionsInDatabaseJobParamsDto model)
        {
            registerTerminationSessionsInDatabaseResultProvider.Register(
                new RegisterTerminationSessionsInDatabaseResultDto
                {
                    TerminationSessionsInDatabaseId = model.TerminationSessionsInDatabaseId,
                    TerminateSessionsInDbStatus = TerminateSessionsInDbStatusEnum.Success
                });

            return model;
        }
    }
}
