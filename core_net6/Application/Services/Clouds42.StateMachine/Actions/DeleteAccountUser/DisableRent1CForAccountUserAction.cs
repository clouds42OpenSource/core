﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser;

/// <summary>
/// Действие по отключению аренды 1с для пользователя
/// </summary>
[FlowActionDescription("Отключение аренды 1с для удаляемого пользователя")]
public class DisableRent1CForAccountUserAction(
    ILogger42 logger,
    IRent1CConfigurationAccessProvider rent1CConfigurationAccessProvider,
    IAccountUserDataProvider accountUserDataProvider)
    : ISimpleAction<AccountUserIdModel, AccountUser>
{
    /// <summary>
    /// Удаление сервисов у пользователей
    /// </summary>
    /// <param name="model">Модель Id пользователя аккаунта</param>
    /// <returns>Пользователь аккаунта</returns>
    public AccountUser Do(AccountUserIdModel model)
    {
        var user = accountUserDataProvider.GetAccountUserOrThrowException(model.AccountUserID);
        logger.Info($"Конфигурация сервисов для пользователя {user.Name} при удалении началась");
        try 
        {
            rent1CConfigurationAccessProvider.ConfigureAccesses(user.AccountId, [new UpdaterAccessRent1CRequestDto { AccountUserId = user.Id }]);

            return user;

        } 
        catch (Exception ex) 
        {
            logger.Error($"Ошибка при конфигурации сервисов для пользователя {user.Name}: {ex.Message}");

            throw;
        }
    }
}
