﻿using Clouds42.CloudServices.Contracts;
using Clouds42.CloudServices.Contracts.Triggers;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.Service.Tardis.Requests;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser
{
    /// <summary>
    /// Действие для уведомления Тардис
    /// </summary>
    [FlowActionDescription("Уведомление Тардиса")]
    public class SynchronizeWithTardisAction(
        IHandlerException handlerException,
        ITardisSynchronizerHelper tardisSynchronizerHelper)
        : ISimpleAction<AccountUserIdModel, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель ID пользователя</param>
        /// <returns>Результат выполнения</returns>
        public bool Do(AccountUserIdModel model)
        {
            try
            {
                tardisSynchronizerHelper.SynchronizeAccountUser<DeleteAccountUserTrigger, AccountUserIdDto>(new AccountUserIdDto
                {
                    AccountUserId = model.AccountUserID
                });
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка уведомления Тардис о удаление пользователя.]");
            }

            return true;
        }
    }
}
