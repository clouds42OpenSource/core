﻿using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Microsoft.Extensions.Configuration;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser
{
    /// <summary>
    /// Действие по удалению пользователя из базы
    /// </summary>
    [FlowActionDescription("Удаление пользователя в БД ядра")]
    public class DeleteAccountUserInDatabaseAction(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IConfiguration configuration,
        IHandlerException handlerException)
        : ISimpleAction<AccountUser, AccountUserIdModel>
    {
        /// <summary>
        /// Удалить пользователя из базы
        /// </summary>
        /// <param name="model">Аккаунт пользователя</param>
        public AccountUserIdModel Do(AccountUser model)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                logger.Info($"Начало процесса удаления пользователя {model.Login} - Id: {model.Id}");

                logger.Info("Подготовка модели пользователя для удаления: " + model);
                var deleteModel = GetDeleteModel(model);

                logger.Debug("Удаление пользователя из базы данных: " + deleteModel);
                dbLayer.AccountUsersRepository.Update(deleteModel);

                logger.Debug($"Удаление доступов пользователя {model.Login}");
                DeleteAccountDatabaseAccess(model.Id);
                    
                dbLayer.Save();

                logger.Info($"Пользователь {model.Login} - Id: {model.Id} удален");

                dbScope.Commit();

                return new AccountUserIdModel
                {
                    AccountUserID = model.Id
                };
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка удаления пользователя]");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Удалить доступы пользователя аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя аккаунта</param>
        private void DeleteAccountDatabaseAccess(Guid accountUserId)
        {
            var accessList = dbLayer.AcDbAccessesRepository.WhereLazy(access => access.AccountUserID == accountUserId)
                .ToList();

            accessList.ForEach(access =>
            {
                var manageAcDbAccess =
                    dbLayer.ManageDbOnDelimitersAccessRepository.FirstOrDefault(w => w.Id == access.ID);

                var acDbAccessRolesJho =
                    dbLayer.AcDbAccRolesJhoRepository.Where(role => role.AcDbAccessesID == access.ID).ToList();

                dbLayer.ManageDbOnDelimitersAccessRepository.Delete(manageAcDbAccess);
                dbLayer.AcDbAccRolesJhoRepository.DeleteRange(acDbAccessRolesJho);
            });

            dbLayer.AcDbAccessesRepository.DeleteRange(accessList);
        }

        /// <summary>
        /// Подготовка модели пользователя для удаления его в базе данных
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        private AccountUser GetDeleteModel(AccountUser accountUser)
        {
            accountUser.Removed = true;
            accountUser.CreatedInAd = false;
            accountUser.Email += ConvertCurrentDateTime();
            accountUser.PhoneNumber = ConvertPhoneForDelete(accountUser.PhoneNumber);
            accountUser.FirstName = configuration["stub:removedUserCaption"];
            accountUser.MiddleName = string.Empty;
            accountUser.LastName = string.Empty;
            accountUser.FullName = accountUser.FirstName;
            accountUser.Login = ConvertLoginForDelete(accountUser.Login);
            accountUser.CorpUserSyncStatus = "Deleted";
            return accountUser;
        }

        /// <summary>
        /// Возвращает первые 14 символов <paramref name="login"/> с добавленой текущей датой в конце
        /// </summary>
        /// <param name="login">Логин аккаунта</param>
        private string ConvertLoginForDelete(string login)
        {
            var strDate = ConvertCurrentDateTime();

            if (login.Length > 14)
            {
                login = login[..13];
            }
            login = login + "_" + strDate;
            return login;
        }

        /// <summary>
        /// Добавляет текущий день и месяц в конец телефонного номера
        /// </summary>
        /// <param name="phone">Номер телефона</param>
        private string ConvertPhoneForDelete(string phone)
        {
            phone = phone + DateTime.Now.Day + DateTime.Now.Month;
            return phone;
        }

        /// <summary>
        /// Преобразование текущей даты в строку с цифрами без разделителей
        /// </summary>
        private string ConvertCurrentDateTime()
        {
            var strDate = DateTime.Now.ToString();

            var listForReplace = new List<string> { ".", " ", ":" };

            foreach (var item in listForReplace)
            {
                strDate = strDate.Replace(item, "");
            }
            return strDate;
        }
    }
}
