﻿using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser
{
    /// <summary>
    /// Действие по удалению пользователя из АД
    /// </summary>
    [FlowActionDescription("Удаление пользователя в АД")]
    public class DeleteAccountUserInAdAction(
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<AccountUser, AccountUser>
    {
        /// <summary>
        /// Удаление пользователя из АД
        /// </summary>
        /// <param name="model">Аккаунт пользователя</param>
        public AccountUser Do(AccountUser model)
        {
            logger.Debug($"Начало удаления пользователя {model.Login} из АД");
            try
            {
                activeDirectoryTaskProcessor.TryDoImmediately(provider=>provider.DeleteUser(model.Id, model.Login));

                return model;
            }
            catch (Exception ex)
            {
                var error = $"[Ошибка удаления пользователя] {model.Login} из АД.";
                handlerException.Handle(ex,$"[Ошибка удаления пользователя из АД] {model.Login} ");
                throw new InvalidOperationException(error, ex);
            }
        }
    }
}
