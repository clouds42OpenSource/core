﻿using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.StateMachine.Contracts.Exceptions;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser
{
    /// <summary>
    /// Действие по отключению ресурсов сервисов для удаляемого пользователя
    /// </summary>
    [FlowActionDescription("Отключение ресурсов сервисов для удаляемого пользователя")]
    public class DisableServicesForAccountUserAction(
        AccountUserActivateProvider accountUserActivateProvider,
        ILogger42 logger)
        : ISimpleAction<AccountUser, AccountUser>
    {
        /// <summary>
        /// Удаление сервисов у пользователей
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <returns>Пользователь аккаунта</returns>
        public AccountUser Do(AccountUser accountUser)
        {
            try
            {
                logger.Debug($"Начало отключения ресурсов для пользователя {accountUser.Login}");
                accountUserActivateProvider.DisableAllBillingServices(accountUser);
                logger.Debug($"Отключения ресурсов для пользователя {accountUser.Login} завершено.");

                return accountUser;
            }
            catch (Exception ex)
            {
                logger.Debug($"Ошибка отключения ресурсов для удаляемого пользователя. Причина: {ex.GetFullInfo()}");
                throw new ActionExecutionException(ex.Message);
            }
        }
    }
}
