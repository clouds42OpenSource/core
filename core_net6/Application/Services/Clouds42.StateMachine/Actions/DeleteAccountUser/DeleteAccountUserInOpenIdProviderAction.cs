﻿using Clouds42.CloudServices.Contracts;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using System.Net;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.Billing.Contracts.Billing.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.StateMachine.Actions.DeleteAccountUser
{
    /// <summary>
    /// Действие по удалению пользователя из OpenId провайдера
    /// </summary>
    [FlowActionDescription("Удаление пользователя в OpenIdProvider")]
    public class DeleteAccountUserInOpenIdProviderAction(
        IUnitOfWork dbLayer,
        IRent1CConfigurationAccessProvider rent1CConfigurationAccessProvider,
        IOpenIdProvider openIdProvider,
        IBillingServiceDataProvider billingServiceDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<AccountUser, AccountUser>
    {
        /// <summary>
        /// Удалить пользователя из провайдера
        /// </summary>
        /// <param name="model">Аккаунт пользователя</param>
        /// <returns>Аккаунт пользователя</returns>
        public AccountUser Do(AccountUser model)
        {
            try
            {
                var user = dbLayer.AccountUsersRepository
                    .AsQueryable()
                    .Include(x => x.AccountUserRoles)
                    .FirstOrDefault(u => u.Id == model.Id);

                if (user == null)
                    throw new NotFoundException($"Account user with id: '{model.Id}' not found");

                var billingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);

                var resourcesExists = dbLayer.ResourceRepository
                    .AsQueryableNoTracking()
                    .Any(r => r.Subject == user.Id && r.BillingServiceType.ServiceId == billingService.Id);

                if (resourcesExists)
                {
                    rent1CConfigurationAccessProvider.ConfigureAccesses(user.AccountId,
                    [
                        new UpdaterAccessRent1CRequestDto { AccountUserId = user.Id }
                    ]);
                }

                logger.Debug($"Удаление ролей для пользователя {user.Login}");

                dbLayer.AccountUserRoleRepository.DeleteRange(user.AccountUserRoles);
                dbLayer.Save();

                DeleteUserInOpenIdProvider(user);

                return user;
            }
            catch (Exception ex)
            {
                throw new ActionExecutionException(ex.Message);
            }
        }

        /// <summary>
        /// Удалить пользователя из провайдера
        /// </summary>
        /// <param name="accountUser">Аккаунт пользователя</param>
        private void DeleteUserInOpenIdProvider(AccountUser accountUser)
        {
            logger.Debug($"Начало удаления пользователя {accountUser.Login} из провайдера");
            try
            {
                openIdProvider.DeleteUser(new SauriOpenIdControlUserModel
                {
                    AccountUserID = accountUser.Id
                });
            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse)ex.Response;
                var error = $"Ошибка удаления пользователя {accountUser.Login} из провайдера.";
                handlerException.Handle(ex, $"[Ошибка удаления пользователя из провайдера] {accountUser.Login}");
                
                if (response.StatusCode != HttpStatusCode.NotFound)
                {
                    throw new InvalidOperationException(error, ex);
                }
            }
        }
    }
}
