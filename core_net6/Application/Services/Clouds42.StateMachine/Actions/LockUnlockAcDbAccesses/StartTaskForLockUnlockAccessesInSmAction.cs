﻿using Clouds42.CoreWorker.AccountUserJobs.ManageLockState;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountUser;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.LockUnlockAcDbAccesses
{
    /// <summary>
    /// Действие для запуска задачи смены состояния блокировки пользователя в МС
    /// </summary>
    [FlowActionDescription("Смена состояния блокировки пользователя в МС")]
    public class StartTaskForLockUnlockAccessesInSmAction(
        ManageAccountUserLockStateJobWrapper manageAccountUserLockStateJobWrapper,
        IHandlerException handlerException)
        : ISimpleAction<LockUnlockAcDbAccessesParamsDto, LockUnlockAcDbAccessesParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров блокировки/разблокировки доступов к инф. базам</param>
        /// <returns>Результат удаления</returns>
        public LockUnlockAcDbAccessesParamsDto Do(LockUnlockAcDbAccessesParamsDto model)
        {
            try
            {
                manageAccountUserLockStateJobWrapper.Start(new ManageAccountUserLockStateParamsDto
                {
                    AccountUserId = model.AccountUserId,
                    IsAvailable = model.IsAvailable
                });

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка запуска задачи смены состояния блокировки пользователя в МС] {model.AccountUserId}");
                throw;
            }
        }
    }
}
