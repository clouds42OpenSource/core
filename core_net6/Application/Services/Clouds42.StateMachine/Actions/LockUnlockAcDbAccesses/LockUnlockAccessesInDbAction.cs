﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.LockUnlockAcDbAccesses
{
    /// <summary>
    /// Действие для смены состояния блокировки пользователя в БД
    /// </summary>
    [FlowActionDescription("Смена состояния блокировки пользователя в БД")]
    public class LockUnlockAccessesInDbAction(
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : ISimpleAction<LockUnlockAcDbAccessesParamsDto, bool>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров блокировки/разблокировки доступов к инф. базам</param>
        /// <returns>Результат удаления</returns>
        public bool Do(LockUnlockAcDbAccessesParamsDto model)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                var accessList = dbLayer.AcDbAccessesRepository
                    .Where(access => access.AccountUserID == model.AccountUserId).ToList();

                accessList.ForEach(access =>
                {
                    access.IsLock = !model.IsAvailable;
                    dbLayer.AcDbAccessesRepository.Update(access);
                });

                dbLayer.Save();
                dbScope.Commit();

                return true;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены состояния блокировки пользователя] {model.AccountUserId} в БД.");
                dbScope.Rollback();
                throw;
            }
        }
    }
}
