﻿using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Завершение процесса удаления данных неактивного аккаунта
    /// </summary>
    [FlowActionDescription("Завершение процесса удаления данных неактивного аккаунта")]
    public class FinalizeDeletingProcessAction : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataResultDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Результат удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataResultDto Do(DeleteInactiveAccountDataParamsDto model)
            => new()
            {
                AccountId = model.AccountId,
                Success = true,
                AccountFolderPath = model.AccountFileStoragePath
            };
    }
}
