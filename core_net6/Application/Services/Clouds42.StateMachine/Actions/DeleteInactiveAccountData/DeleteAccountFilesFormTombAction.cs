﻿using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Удаление файлов неактивного аккаунта из склепа
    /// </summary>
    [FlowActionDescription("Удаление файлов неактивного аккаунта из склепа")]
    public class DeleteAccountFilesFormTombAction(
        AccountDataProvider accountDataProvider,
        TombRunnerProvider tombRunnerProvider,
        TombCleanerProvider tombCleanerProvider,
        ILogger42 logger)
        : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Модель параметров удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataParamsDto Do(DeleteInactiveAccountDataParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            try
            {                
                var accountFolderId = tombRunnerProvider.GetAccountFolderId(account);
                if (string.IsNullOrEmpty(accountFolderId))
                    return model;

                logger.Trace($"Папка аккаунта в склепе найдена {accountFolderId}. Начинаем процесс удаления.");
                tombCleanerProvider.DeleteAccountDataFromTomb(accountFolderId);

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Удаление папки аккаунта {accountDataProvider.GetAccountFullName(account)} в склепе завершено с ошибкой.";
                logger.Trace($"{errorMessage} Причина: {ex.GetFullInfo()}");
                throw new InvalidOperationException($"{errorMessage} Причина: {ex.Message}");
            }
        }
    }
}
