﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Удаление инф. баз неактивного аккаунта
    /// </summary>
    [FlowActionDescription("Удаление инф. баз неактивного аккаунта")]
    public class DeleteAccountDatabasesAction(
        IUnitOfWork dbLayer,
        AccountDatabaseDeleteProvider accountDatabaseDeleteProvider,
        IDeleteDelimiterCommand deleteDelimiterCommand,
        AccountDataProvider accountDataProvider,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        IAccessProvider accessProvider,
        ILogger42 logger)
        : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Модель параметров удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataParamsDto Do(DeleteInactiveAccountDataParamsDto model)
        {
            var accountFullName = accountDataProvider.GetAccountFullName(model.AccountId);
            try
            {
                var accountDatabases = dbLayer.DatabasesRepository.Where(acDb =>
                    acDb.AccountId == model.AccountId && acDb.State != DatabaseState.DeletedFromCloud.ToString()).ToList();
                accountDatabases.ForEach(ProcessAccountDatabase);

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage = $"Удаление инф. баз неактивного аккаунта {accountFullName} завершено с ошибкой.";
                logger.Warn($"{errorMessage} Причина: {ex.GetFullInfo()}");
                throw new InvalidOperationException($"{errorMessage} Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Обработать инф. базу аккаунта
        /// </summary>
        /// <param name="accountDatabase">Инф. база аккаунта</param>
        private void ProcessAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.IsDelimiter())
            {
                DeleteDatabaseOnDelimiters(accountDatabase);
                accountDatabaseChangeStateProvider.ChangeState(accountDatabase, DatabaseState.DeletedFromCloud);
                return;
            }

            var result = DeleteLocalAccountDatabase(accountDatabase);
            if (!result)
                throw new InvalidOperationException($"Ошибка удаления инф. базы {accountDatabase.V82Name}");

            accountDatabaseChangeStateProvider.ChangeState(accountDatabase, DatabaseState.DeletedFromCloud);

            LogEventHelper.LogEvent(dbLayer, accessProvider.ContextAccountId, accessProvider, LogActions.DeleteAccountDatabase,
                    $"База {accountDatabase.V82Name} полностью удалена с облака так как аккаунт не активен");
        }
        
        /// <summary>
        /// Удалить директорию инф. базы аккаунта
        /// </summary>
        /// <param name="accountDatabase">Инф. база аккаунта</param>
        private bool DeleteLocalAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase)
            => accountDatabaseDeleteProvider.DeleteLocalAccountDatabase(accountDatabase);

        /// <summary>
        /// Удалить базу на разделителях
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        private void DeleteDatabaseOnDelimiters(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.StateEnum != DatabaseState.Ready || 
                (accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone ==null))
                return;
            var locale = accountDatabase.Account.AccountConfiguration.Locale.Name;
            deleteDelimiterCommand.Execute(accountDatabase.Id, locale);
        }
    }
}
