﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Удаление файлов неактивного аккаунта из хранилища
    /// </summary>
    [FlowActionDescription("Удаление файлов неактивного аккаунта из хранилища")]
    public class DeleteAccountFilesFromFileStorageAction(
        AccountDataProvider accountDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Модель параметров удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataParamsDto Do(DeleteInactiveAccountDataParamsDto model)
        {
            var accountFullName = accountDataProvider.GetAccountFullName(model.AccountId);
            try
            {
                DeleteDirectoryAndFiles(model.AccountArchiveDirectoryPath);
                DeleteDirectoryAndFiles(model.AccountFileStoragePath);

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Удаление данных аккаунта {accountFullName} по пути \"{model.AccountArchiveDirectoryPath}\" и \"{model.AccountFileStoragePath}\" завершено с ошибкой.";
                handlerException.Handle(ex, $"[Ошибка удаления данных аккаунта] {accountFullName} по пути \"{model.AccountArchiveDirectoryPath}\" и \"{model.AccountFileStoragePath}\"");
                throw new InvalidOperationException($"{errorMessage} Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Удалить файлы и папки с храниолища
        /// </summary>
        /// <param name="accountFolderPath">Путь к папке аккаунта</param>
        private void DeleteDirectoryAndFiles(string accountFolderPath)
        {
            logger.Trace($"Удаление файлов и папок аккаунта по пути {accountFolderPath}");

            if (!DirectoryHelper.Exists(accountFolderPath))
                return;
            DirectoryHelper.SetAttributesNormal(accountFolderPath);
            DirectoryHelper.DeleteDirectoryRecursively(accountFolderPath);

            logger.Trace($"Удаление файлов и папок аккаунта по пути {accountFolderPath} завершено успешно");
        }
    }
}
