﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Действие для подготовки данных аккаунта перед удалением
    /// </summary>
    [FlowActionDescription("Подготовка данных аккаунта перед удалением")]
    public class PrepareAccountDataBeforeDeleteAction(
        AccountDataProvider accountDataProvider,
        IServiceProvider serviceProvider,
        IHandlerException handlerException)
        : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Модель параметров удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataParamsDto Do(DeleteInactiveAccountDataParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);
            try
            {
                var segmentHelper =  serviceProvider.GetRequiredService<ISegmentHelper>();

                model.AccountFileStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
                
                var archivePath = segmentHelper.GetBackupStorage(account);
                model.AccountArchiveDirectoryPath = Path.Combine(archivePath, $"account_{account.IndexNumber}");

                return model;
            }
            catch (Exception ex) 
            {
                var errorMessage = $"При подготовке данных аккаунта {accountDataProvider.GetAccountFullName(account)} перед удалением произошла ошибка.";
                handlerException.Handle(ex, $"[Ошибка подготовки данных аккаунта перед удалением] {accountDataProvider.GetAccountFullName(account)}");
                throw new InvalidOperationException($"{errorMessage} Причина: {ex.Message}");
            }
        }
    }
}
