﻿using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.Accounts.Account.Providers;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteInactiveAccountData
{
    /// <summary>
    /// Удаление бэкапов баз неактивного аккаунта
    /// </summary>
    [FlowActionDescription("Удаление бэкапов баз неактивного аккаунта")]
    public class DeleteAccountDatabasesBackupAction(
        IUnitOfWork dbLayer,
        AccountDatabaseLocalBackupProvider accountDatabaseLocalBackupProvider,
        TombCleanerProvider tombCleanerProvider,
        AccountDataProvider accountDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<DeleteInactiveAccountDataParamsDto, DeleteInactiveAccountDataParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <returns>Модель параметров удаления данных неактивного аккаунта</returns>
        public DeleteInactiveAccountDataParamsDto Do(DeleteInactiveAccountDataParamsDto model)
        {
            var accountFullName = accountDataProvider.GetAccountFullName(model.AccountId);
            try
            {
                var accountDatabases = dbLayer.DatabasesRepository
                    .Where(acDb => acDb.AccountId == model.AccountId && acDb.AccountDatabaseBackups.Any()).ToList();

                logger.Trace($"Список баз аккаунта, которые имеют бэкапы получен. Количество: {accountDatabases.Count}");

                accountDatabases.ForEach(database =>
                {
                    var accountDatabasesBackups = database.AccountDatabaseBackups.ToList();
                    DeleteAccountDatabaseBackups(accountDatabasesBackups);
                });

                return model;
            }
            catch (Exception ex)
            {
                var errorMessage =
                    $"Удаление бекапов инф. баз неактивного аккаунта {accountFullName} завершено с ошибкой.";
                handlerException.Handle(ex, $"[Ошибка удаления бекапов инф. баз неактивного аккаунта] {accountFullName}");
                throw new InvalidOperationException($"{errorMessage} Причина: {ex.Message}");
            }
        }

        /// <summary>
        /// Удалить бэкапы инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackups">Список бэкапов инф. базы</param>
        private void DeleteAccountDatabaseBackups(List<AccountDatabaseBackup> accountDatabaseBackups)
        {
            accountDatabaseBackups.ForEach(backup =>
            {
                logger.Trace($"Удаление бэкапа {backup.BackupPath} инф. базы {backup.AccountDatabase.V82Name}");

                if (backup.SourceType == AccountDatabaseBackupSourceType.GoogleCloud)
                {
                    tombCleanerProvider.DeleteCloudAccountDatabaseBackup(backup.Id, true);
                    return;
                }
                
                accountDatabaseLocalBackupProvider.DeleteBackupAccountDatabase(backup.Id, true);
            });
        }
    }
}
