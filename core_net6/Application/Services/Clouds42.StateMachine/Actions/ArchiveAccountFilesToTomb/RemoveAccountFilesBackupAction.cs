﻿using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;

using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Удаление бэкапа файлов аккаунта
    /// </summary>
    [FlowActionDescription("Удаление бэкапа файлов аккаунта")]
    public class RemoveAccountFilesBackupAction : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();
        private readonly IHandlerException _handlerException = HandlerException42.GetHandler();

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            var message = $"Удаление бэкапа {model.AccountFilesBackupFilePath} файлов аккаунта {model.AccountId}";
            try
            {
                _logger.Trace(message);

                if (!File.Exists(model.AccountFilesBackupFilePath))
                    throw new DirectoryNotFoundException($"[{model.AccountId}] :: Архив {model.AccountFilesBackupFilePath} не найден");

                File.Delete(model.AccountFilesBackupFilePath);

                _logger.Trace($"{message} завершилось успешно.");

                return model;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка удаления бэкапа файлов] {model.AccountFilesBackupFilePath} аккаунта {model.AccountId}");
                throw;
            }
        }
    }
}
