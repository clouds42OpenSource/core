﻿using Clouds42.Accounts.Account.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.DataContracts.Account;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Обновить ссылку на склеп для аккаунта
    /// </summary>
    [FlowActionDescription("Обновление сслыки на склеп аккаунта")]
    public class UpdateAccountCloudStorageWebLinkAction(
        AccountDataProvider accountDataProvider,
        IUpdateAccountConfigurationProvider updateAccountConfigurationProvider)
        : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            var account = accountDataProvider.GetAccountOrThrowException(model.AccountId);

            var folder = $"company_{account.IndexNumber}";
            
            updateAccountConfigurationProvider.UpdateCloudStorageWebLink(model.AccountId, folder);
            return model;
        }
    }
}
