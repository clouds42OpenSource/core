﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.BLL.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Создание бэкапа файлов аккаунта
    /// </summary>
    [FlowActionDescription("Создание бэкапа файлов аккаунта")]
    public class CreateAccountFilesBackupAction(
        ISegmentHelper segmentHelper,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            try
            {
                logger.Trace($"Создание бэкапа файлов аккаунта {model.AccountId}");
                
                var account = dbLayer.AccountsRepository.FirstOrThrowException(a => a.Id == model.AccountId,
                    $"Не удалось найти аккаунт по Id= '{model.AccountId}'");

            
                var accountFilesStoragePath = segmentHelper.GetFullClientFileStoragePath(account);
                var archivePath = segmentHelper.GetBackupStorage(account);

                var accountFilesBackupFilePath = Path.Combine(archivePath, $"account_{account.IndexNumber}",
                    $"{account.IndexNumber}_{DateTime.Now:yyyy-MM-dd}_files.zip");
                var archiveDirectoryPath = Path.Combine(archivePath, $"account_{account.IndexNumber}");

                if (!Directory.Exists(archiveDirectoryPath))
                    Directory.CreateDirectory(archiveDirectoryPath);

                if (string.IsNullOrEmpty(accountFilesStoragePath) || !Directory.Exists(accountFilesStoragePath))
                    throw new DirectoryNotFoundException($"Не найдена папка аккаунта по адресу {accountFilesStoragePath}");

                ZipHelper.SafelyCreateZipFromDirectory(accountFilesStoragePath, accountFilesBackupFilePath);

                logger.Trace($"Создание бэкапа файлов аккаунта из каталога {accountFilesStoragePath}, адрес архива {accountFilesBackupFilePath} завершилось успешно.");

                model.AccountFilesStoragePath = accountFilesStoragePath;
                model.AccountFilesBackupFilePath = accountFilesBackupFilePath;

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка создания бэкапа файлов аккаунта] {model.AccountId}");
                throw;
            }
        }
    }
}
