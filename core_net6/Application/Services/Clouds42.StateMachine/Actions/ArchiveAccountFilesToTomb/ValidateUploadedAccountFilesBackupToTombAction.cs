﻿using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Выполнить валидацию бэкапа файлов аккаунта в склепе
    /// </summary>
    [FlowActionDescription("Валидация бэкапа файлов аккаунта в склепе")]
    public class ValidateUploadedAccountFilesBackupToTombAction(
        IUnitOfWork dbLayer,
        TombAnalysisProvider tombAnalysisProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            try
            {
                logger.Info($"Выполнение проверки бэкапа файлов аккаунта '{model.AccountId}' в склепе. FileId= '{model.AccountFilesBackupFileId}'");

                var account =
                    dbLayer.AccountsRepository.FirstOrThrowException(a => a.Id == model.AccountId);

                var validationResult = tombAnalysisProvider.ValidateUploadedAccountFilesBackupToTomb(account,
                    model.AccountFilesBackupFileId, model.AccountFilesBackupFilePath);

                if (!validationResult)
                    throw new InvalidOperationException(
                        $"Произошла ошибка при проверке загруженного файла {model.AccountFilesBackupFileId} бекапа файлов аккаунта {model.AccountId}.");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка проверки загруженного файла] {model.AccountFilesBackupFileId}");
                throw;
            }
        }
    }
}
