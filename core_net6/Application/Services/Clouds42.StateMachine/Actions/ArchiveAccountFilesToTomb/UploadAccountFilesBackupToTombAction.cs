﻿using Clouds42.AccountDatabase.Tomb.Internal.Providers;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Загрузить бэкап файлов аккаунта в склеп
    /// </summary>
    [FlowActionDescription("Загрузка бэкапа файлов аккаунта в склеп")]
    public class UploadAccountFilesBackupToTombAction(
        IUnitOfWork dbLayer,
        TombRunnerProvider tombRunnerProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {

            try
            {
                logger.Trace($"Загрузка бэкапа {model.AccountFilesBackupFilePath} файлов аккаунта {model.AccountId} в склеп");
                var account = dbLayer.AccountsRepository.FirstOrThrowException(a => a.Id == model.AccountId);

                model.AccountFilesBackupFileId = tombRunnerProvider.UploadAccountFilesBackupToTomb(model.AccountFilesBackupFilePath, account, true);

                logger.Trace(
                    $"Перенос бэкапа '{model.AccountFilesBackupFilePath}' файлов аккаунта '{model.AccountId}'" +
                    $" в склеп успешно заврешен. ID файла: {model.AccountFilesBackupFileId}");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex ,$"[Ошибка загрузки бэкапа файлов в склеп] {model.AccountFilesBackupFilePath} аккаунта {model.AccountId}");
                throw;
            }
        }
    }
}
