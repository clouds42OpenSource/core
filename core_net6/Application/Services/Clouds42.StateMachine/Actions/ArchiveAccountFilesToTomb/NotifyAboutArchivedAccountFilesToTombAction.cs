﻿using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.Account;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Уведомить об архивации файлов аккаунта в склеп
    /// </summary>
    [FlowActionDescription("Уведомление об архивации файлов аккаунта в склеп")]
    public class NotifyAboutArchivedAccountFilesToTombAction(
        ILetterNotificationProcessor letterNotificationProcessor,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<CopyAccountFilesBackupToTombParamsDto,
            CopyAccountFilesBackupToTombParamsDto>
    {
        private readonly Lazy<int> _lifetimeAccountDataInTombDaysCount = new(CloudConfigurationProvider.ConfigRental1C.ServiceExpired
            .GetLifetimeAccountDataInTombDaysCount);

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            var message = $"Уведомление об архивации файлов аккаунта {model.AccountId} в склеп." +
                          $"AccountFilesBackupFileId = {model.AccountFilesBackupFileId}";
            try
            {
                logger.Trace(message);

                letterNotificationProcessor.TryNotify<TransferringAccountDataToTombLetterNotification, TransferringAccountDataToTombLetterModelDto>
                    (new TransferringAccountDataToTombLetterModelDto
                    {
                        AccountId = model.AccountId,
                        LifetimeAccountDataInTombDaysCount = _lifetimeAccountDataInTombDaysCount.Value
                    });

                logger.Trace($"{message} завершилось успешно.");

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка уведомления об архивации файлов в склеп] аккаунта {model.AccountId}" +
                          $"AccountFilesBackupFileId = {model.AccountFilesBackupFileId}");
                throw;
            }
        }
    }
}
