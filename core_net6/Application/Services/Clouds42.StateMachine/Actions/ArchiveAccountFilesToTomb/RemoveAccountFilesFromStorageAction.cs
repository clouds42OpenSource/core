﻿using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.StateMachine.Contracts.ActionTypes;
using Core42.Application.Features.ProcessFlowContext.Attributes;

namespace Clouds42.StateMachine.Actions.ArchiveAccountFilesToTomb
{
    /// <summary>
    /// Удаление файлов аккаунта из хранилища
    /// </summary>
    [FlowActionDescription("Удаление файлов аккаунта из хранилища")]
    public class RemoveAccountFilesFromStorageAction : ISimpleAction<CopyAccountFilesBackupToTombParamsDto, CopyAccountFilesBackupToTombParamsDto>
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();
        private static readonly IHandlerException _handlerException = HandlerException42.GetHandler();
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования файлов аккаунта в склеп</param>
        /// <returns>Модель параметров копирования файлов аккаунта в склеп</returns>
        public CopyAccountFilesBackupToTombParamsDto Do(CopyAccountFilesBackupToTombParamsDto model)
        {
            var message = $"Удаление файлов аккаунта {model.AccountId} из хранилища {model.AccountFilesStoragePath}";
            try
            {
                _logger.Trace(message);

                if (!DirectoryHelper.Exists(model.AccountFilesStoragePath))
                {
                    _logger.Info($"Директория не найдена: {model.AccountFilesStoragePath}");
                    return model;
                }

                DirectoryHelper.SetAttributesNormal(model.AccountFilesStoragePath);
                DirectoryHelper.TryDeleteDirectoryOrThrowException(model.AccountFilesStoragePath, 200);

                _logger.Trace($"{message} завершилось успешно.");

                return model;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"[Ошибка удаления файлов аккаунта] {model.AccountId} из хранилища {model.AccountFilesStoragePath}");
                throw;
            }
        }
    }
}
