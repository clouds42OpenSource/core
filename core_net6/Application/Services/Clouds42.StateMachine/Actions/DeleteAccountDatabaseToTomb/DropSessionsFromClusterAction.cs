﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.JobWrappers.DropSessions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.DeleteAccountDatabaseToTomb
{
    /// <summary>
    /// Удалить сессии инф. базы в кластере
    /// </summary>
    [FlowActionDescription("Удаление сессий инф. базы в кластере")]
    public class DropSessionsFromClusterAction(
        IDropSessionsFromClusterJobWrapper dropSessionsFromClusterJobWrapper,
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        ILogger42 logger)
        : ISimpleAction<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Модель параметров копирования инф. базы в склеп</returns>
        public CopyAccountDatabaseBackupToTombParamsDto Do(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            if (model.ActionsContains(nameof(DropSessionsFromClusterAction)))
                return model;

            try
            {
                var accountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                if (accountDatabase.IsDelimiter())
                    return model;

                if (accountDatabase.StateEnum != DatabaseState.Ready)
                    return model;

                if (accountDatabase.IsFile == true)
                    return model;

                dropSessionsFromClusterJobWrapper.Start(new DropSessionsFromClusterJobParams
                {
                    AccountDatabaseId = accountDatabase.Id
                }).Wait();

                logger.Info($"[{accountDatabase.Id}] :: Удаление активных сессий. Done!");

                return model;
            }

            catch (Exception ex)
            {
                logger.Info($"[{model.AccountDatabaseId}] :: Ошибка при удалении активных сессий в базе: {ex.GetFullInfo()}");
                throw;
            }
        }
    }
}
