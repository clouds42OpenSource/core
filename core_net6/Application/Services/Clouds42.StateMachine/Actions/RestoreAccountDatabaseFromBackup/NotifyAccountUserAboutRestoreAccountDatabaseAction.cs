﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Locales.Contracts.Interfaces;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для завершения восстановления инф. базы из бэкапа
    /// </summary>
    [FlowActionDescription("Завершение процесса восстановления инф. базы из бэкапа")]
    public class
        NotifyAccountUserAboutRestoreAccountDatabaseAction(
            ILetterNotificationProcessor letterNotificationProcessor,
            IAccountDatabaseDataProvider accountDatabaseDataProvider,
            IEmailAddressesDataProvider emailAddressesDataProvider,
            ILocalesConfigurationDataProvider localesConfigurationDataProvider,
            IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto,
            RestoreAccountDatabaseParamsDto>
    {
        private readonly Lazy<string> _routeForOpenAccountDatabasesPage = new(CloudConfigurationProvider.Cp.GetRouteForOpenAccountDatabasesPage);

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                if (model.RestoreType == AccountDatabaseBackupRestoreType.ToNew)
                    return model;

                var accountDatabaseBackup =
                    accountDatabaseDataProvider
                        .GetAccountDatabaseBackupOrThrowException(model.AccountDatabaseBackupId);

                var cpSiteUrl =
                    localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountDatabaseBackup.AccountDatabase
                        .AccountId);

                letterNotificationProcessor
                    .TryNotify<RestoreAccountDatabaseFromTombLetterNotification,
                        RestoreAccountDatabaseFromTombLetterModelDto>(new RestoreAccountDatabaseFromTombLetterModelDto
                        {
                            AccountId = accountDatabaseBackup.AccountDatabase.AccountId,
                            AccountDatabaseId = accountDatabaseBackup.AccountDatabaseId,
                            DatabaseCaption = accountDatabaseBackup.AccountDatabase.Caption,
                            EmailsToCopy =
                                emailAddressesDataProvider.GetAllAccountEmailsToSend(accountDatabaseBackup.AccountDatabase
                                    .AccountId),
                            UrlForAccountDatabasesPage =
                                new Uri($"{cpSiteUrl}/{_routeForOpenAccountDatabasesPage.Value}").AbsoluteUri
                        });

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка уведомления пользователя о восстановлении инф. базы] {model.AccountDatabaseId}");
                throw;
            }
        }
    }
}
