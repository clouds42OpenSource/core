﻿using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для выдачи доступов к восстановленной инф. базе
    /// </summary>
    [FlowActionDescription("Выдача доступов к восстановленной инф. базе")]
    public class GrantAcDbAccessesAction(
        StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager,
        IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                var result = startProcessToWorkDatabaseManager.StartProcessesOfGrandInternalAccesses(
                    new StartProcessesOfManageAccessesToDbDto
                    {
                        DatabaseId = model.AccountDatabaseId,
                        UsersId = model.UsersIdForAddAccess
                    });

                if (result.Error)
                    throw new InvalidOperationException(result.Message);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка выдачи доступов к инф. базе]");
                throw;
            }
        }
    }
}
