﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Tomb.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для получения бэкапа инф. базы для восстановления
    /// </summary>
    [FlowActionDescription("Получение бэкапа инф. базы для восстановления")]
    public class GetAccountDatabaseBackupAction : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        private readonly ITombManager _tombManager;
        private readonly IAccountDatabaseDataProvider _accountDatabaseDataProvider;
        private readonly IHandlerException _handlerException;
        private readonly Dictionary<AccountDatabaseBackupSourceType, Func<AccountDatabaseBackup, string>>
            _mapBackupSourceTypeToBackupPath;

        public GetAccountDatabaseBackupAction(
            ITombManager tombManager,
           IAccountDatabaseDataProvider accountDatabaseDataProvider,
           IHandlerException handlerException)
        {
            _tombManager = tombManager;
            _accountDatabaseDataProvider = accountDatabaseDataProvider;
            _handlerException = handlerException;

            _mapBackupSourceTypeToBackupPath = new Dictionary<AccountDatabaseBackupSourceType, Func<AccountDatabaseBackup, string>>
            {
                {AccountDatabaseBackupSourceType.Local, backup => backup.BackupPath},
                {AccountDatabaseBackupSourceType.GoogleCloud, GetGoogleDriveAccountDatabaseBackupPath}
            };
        }

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                var accountDatabaseBackup = _accountDatabaseDataProvider.GetAccountDatabaseBackupOrThrowException(model.AccountDatabaseBackupId);

                model.AccountDatabaseBackupPath =
                    _mapBackupSourceTypeToBackupPath[accountDatabaseBackup.SourceType](accountDatabaseBackup);

                return model;
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения бэкапа инф. базы для восстановления]");
                throw;
            }
        }

        /// <summary>
        /// Получить путь для скачанного бэкапа с Google диска
        /// </summary>
        /// <param name="accountDatabaseBackup">Бэкап инф. базы</param>
        /// <returns>Путь для скачанного бэкапа с Google диска</returns>
        private string GetGoogleDriveAccountDatabaseBackupPath(AccountDatabaseBackup accountDatabaseBackup)
        {
            var downloadAccountDatabaseBackupResult = _tombManager.DownloadBackup(accountDatabaseBackup);

            if (downloadAccountDatabaseBackupResult.Error)
                throw new InvalidOperationException($"Не удалось скачать бэкап инф. базы {accountDatabaseBackup.AccountDatabase.V82Name}");

            return downloadAccountDatabaseBackupResult.Result;
        }
    }
}
