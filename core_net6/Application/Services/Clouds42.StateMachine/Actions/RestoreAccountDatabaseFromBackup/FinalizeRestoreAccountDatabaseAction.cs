﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для завершения восстановления инф. базы из бэкапа
    /// </summary>
    [FlowActionDescription("Завершение процесса восстановления инф. базы из бэкапа")]
    public class FinalizeRestoreAccountDatabaseAction(
        IUnitOfWork dbLayer,
        IAccountDatabaseManager accountDatabaseManager,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                if (model.NeedChangeState)
                {
                    accountDatabaseChangeStateProvider.ChangeState(model.AccountDatabaseId, DatabaseState.Ready, DateTime.Now);
                    logger.Trace($"Обновлен статус базы на статус {DatabaseState.Ready}");
                }

                accountDatabaseManager.RecalculateSizeOfAccountDatabase(model.AccountDatabaseId);
                DeleteAccountDatabaseBackup(model.AccountDatabaseBackupId, model.AccountDatabaseBackupPath);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка заврешения процесса восстановления инф. базы] {model.AccountDatabaseId}");
                throw;
            }
        }

        /// <summary>
        /// Удалить файл бэкапа инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа</param>
        /// <param name="backupPath">Путь к бэкапу</param>
        private void DeleteAccountDatabaseBackup(Guid accountDatabaseBackupId, string backupPath)
        {
            var accountDatabaseBackup =
                dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                    backup.Id == accountDatabaseBackupId) ??
                throw new NotFoundException($"Бэкап по ID {accountDatabaseBackupId} не найден");

            if (accountDatabaseBackup.SourceType == AccountDatabaseBackupSourceType.Local)
                return;

            System.IO.File.Delete(backupPath);
        }
    }
}
