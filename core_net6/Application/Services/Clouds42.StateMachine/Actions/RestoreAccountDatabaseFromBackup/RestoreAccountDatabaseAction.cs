﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для восстановления инф. базы из бэкапа
    /// </summary>
    [FlowActionDescription("Восстановление инф. базы из бэкапа")]
    public class RestoreAccountDatabaseAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IRestoreAccountDatabaseProvider restoreFileAccountDatabaseProvider,
        IRestoreServerAccountDatabaseProvider restoreServerAccountDatabaseProvider,
        ILogger42 logger,
        IUnitOfWork unitOfWork,
        IAccessProvider accessProvider,
        IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);
                var destAccountDatabase = accountDatabaseDataProvider.GetAccountDatabaseByBackupIdOrThrowException(
                    model.AccountDatabaseBackupId);
                logger.Info($"[{accountDatabase.Id}] :: Начинаем восстановление бэкапа");
                if (accountDatabase.IsFile is true)
                {
                    logger.Info($"[{accountDatabase.Id}] :: Восстановление файловой базы");
                    restoreFileAccountDatabaseProvider.Restore(model);
                }
                else
                {
                    logger.Info($"[{accountDatabase.Id}] :: Восстановление серверной базы");
                    restoreServerAccountDatabaseProvider.Restore(model);
                }
                if (model.RestoreType == AccountDatabaseBackupRestoreType.ToNew)
                {
                    LogEventHelper.LogEvent(unitOfWork, accountDatabase.AccountId, accessProvider, LogActions.RestoreAccountDatabase,
                                       $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} " +
                                       $"восстановленна из базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(destAccountDatabase)}.");
                }
                   
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка восстановления инф. базы из бэкапа]");
                throw;
            }
        }
    }
}
