﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Create.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для получения инф. базы для восстановления
    /// </summary>
    [FlowActionDescription("Получение инф. базы для восстановления")]
    public class GetDestinationAccountDatabaseAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        CreateAccountDatabaseFromBackupManager createAccountDatabaseFromBackupManager,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                var destAccountDatabase =
                    accountDatabaseDataProvider.GetAccountDatabaseByBackupIdOrThrowException(
                        model.AccountDatabaseBackupId);

                if (model.RestoreType != AccountDatabaseBackupRestoreType.ToNew)
                {
                    model.AccountDatabaseId = destAccountDatabase.Id;
                    return model;
                }

                var newAccountDatabaseId = CreateNewAccountDatabase(model.AccountDatabaseBackupId,
                    model.AccountDatabaseName, model.ProcessFlowKey);

                logger.Info($"Создана новая база для восстановления бэкапа из склепа {newAccountDatabaseId}");
                model.AccountDatabaseId = newAccountDatabaseId;
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения инф. базы для восстановления]");
                throw;
            }
        }

        /// <summary>
        /// Создать новую базу для восстановления
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <param name="accountDatabaseName">Название инф. базы</param>
        /// <param name="parentProcessFlowKey">Ключ родительского рабочего процесса</param>
        /// <returns>ID созданной базы</returns>
        private Guid CreateNewAccountDatabase(Guid accountDatabaseBackupId, string accountDatabaseName, string parentProcessFlowKey)
        {
            var createAccountDatabaseResult = createAccountDatabaseFromBackupManager.Create(accountDatabaseBackupId,
                accountDatabaseName, parentProcessFlowKey);

            if (!createAccountDatabaseResult.Error) 
                return createAccountDatabaseResult.Result.Id;

            var message = $"Ошибка создания новой информационной базы, на основе бэкапа '{accountDatabaseBackupId}'";
            throw new InvalidOperationException(message);
        }
    }
}
