﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Restore.Interfaces;
using Clouds42.AccountDatabase.Restore.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для восстановления инф. базы из бэкапа
    /// </summary>
    [FlowActionDescription("Восстановление инф. базы из бэкапа после неуспешной попытки АО")]
    public class RestoreAccountDatabaseAfterFailedAutoUpdateAction(
        IAccountDatabaseDataProvider accountDatabaseDataProvider,
        IRestoreAccountDatabaseProvider restoreFileAccountDatabaseProvider,
        RestoreServerAcDbAfterFailedAutoUpdateProvider restoreServerAcDbAfterFailedAutoUpdateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        private readonly List<(bool?, IRestoreAccountDatabaseProvider)> _restoreAccountDatabaseActionList =
        [
            (true, restoreFileAccountDatabaseProvider),
            (false, restoreServerAcDbAfterFailedAutoUpdateProvider)
        ];

        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            try
            {
                var accountDatabase = accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

                logger.Info($"[{accountDatabase.Id}] :: Начинаем восстановление бэкапа");

                var restoreAction = _restoreAccountDatabaseActionList
                                        .FirstOrDefault(tuple => tuple.Item1 == accountDatabase.IsFile)
                                        .Item2
                                    ?? throw new InvalidOperationException(
                                        $"Не удалось найти действие для восстановления инф. базы {accountDatabase.V82Name} из бэкапа");

                restoreAction.Restore(model);
                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка восстановления инф. базы из бэкапа]");
                throw;
            }
        }
    }
}
