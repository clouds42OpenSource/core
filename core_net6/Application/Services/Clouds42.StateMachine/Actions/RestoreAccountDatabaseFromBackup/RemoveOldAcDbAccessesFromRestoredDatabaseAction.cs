﻿using Clouds42.AccountDatabase.Processors.Processes.Managers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Clouds42.StateMachine.Contracts.ActionTypes;

namespace Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup
{
    /// <summary>
    /// Действие для удаления старых доступов из восстановленной инф. базы.
    /// При условии что восстановление в текущую базу.
    /// </summary>
    [FlowActionDescription("Удаление старых доступов из восстановленной инф. базы")]
    public class
        RemoveOldAcDbAccessesFromRestoredDatabaseAction(
            IUnitOfWork dbLayer,
            StartProcessToWorkDatabaseManager startProcessToWorkDatabaseManager,
            IHandlerException handlerException)
        : ISimpleAction<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>
    {
        /// <summary>
        /// Выполнить действие
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <returns>Модель параметров восстановления инф. базы</returns>
        public RestoreAccountDatabaseParamsDto Do(RestoreAccountDatabaseParamsDto model)
        {
            if (model.RestoreType != AccountDatabaseBackupRestoreType.ToCurrent)
                return model;
            
            try
            {
                var userIdsWhoNeedRemoveAccessToDatabase =
                    GetUserIdsWhoNeedRemoveAccessToDatabase(model.AccountDatabaseId).ToList();

                if (!userIdsWhoNeedRemoveAccessToDatabase.Any())
                    return model;

                StartProcessesOfRemoveAccesses(model.AccountDatabaseId, userIdsWhoNeedRemoveAccessToDatabase);

                return model;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    "[Ошибка удаления старых доступов из восстановленной инф. базы] ");
                throw;
            }
        }

        /// <summary>
        /// Запустить процесс удаления доступов
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <param name="userIds">Cписок Id пользователей,
        /// которым нужно удалить доступ к базе</param>
        private void StartProcessesOfRemoveAccesses(Guid databaseId, List<Guid> userIds)
        {
            var managerResult = startProcessToWorkDatabaseManager.StartProcessesOfRemoveAccesses(
                new StartProcessesOfManageAccessesToDbDto
                {
                    DatabaseId = databaseId,
                    UsersId = userIds
                });

            if (managerResult.Error)
                throw new InvalidOperationException(managerResult.Message);
        }

        /// <summary>
        /// Получить список Id пользователей,
        /// которым нужно удалить доступ к базе 
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Cписок Id пользователей,
        /// которым нужно удалить доступ к базе</returns>
        private IEnumerable<Guid> GetUserIdsWhoNeedRemoveAccessToDatabase(Guid databaseId) =>
            (from dbAccess in dbLayer.AcDbAccessesRepository.WhereLazy()
                join manageDbOnDelimitersAccess in dbLayer.ManageDbOnDelimitersAccessRepository.WhereLazy() on
                    dbAccess.ID equals manageDbOnDelimitersAccess.Id into manageDbOnDelimitersAccesses
                from manageAccess in manageDbOnDelimitersAccesses.Take(1).DefaultIfEmpty()
                where dbAccess.AccountDatabaseID == databaseId
                      && (manageAccess == null || manageAccess.AccessState == AccountDatabaseAccessState.Done)
                      && dbAccess.AccountUserID != null
                select dbAccess.AccountUserID.Value).Distinct().AsEnumerable();
    }
}
