﻿using Clouds42.Domain.DataModels.StateMachine;
using Core42.Application.Features.ProcessFlowContext.Attributes;
using Core42.Application.Features.ProcessFlowContext.Dtos;
using Core42.Application.Features.ProcessFlowContext.Constants;

namespace Clouds42.StateMachine.Helpers
{
    /// <summary>
    /// Расширения рабочего процесса.
    /// </summary>
    public static class ProcessFlowStateExtension
    {

        /// <summary>
        /// Получить описание действия на котором находится процесс.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <returns>Человекопонятное описание действия процесса находящееся в FlowActionDescriptionAttribute</returns>
        public static string GetStateDescription(this ProcessFlow processFlow)
        {
            if (string.IsNullOrEmpty(processFlow.State))
                return null;

            var lastActionType = GetActionsCallChain(processFlow.State).LastOrDefault();
            if (lastActionType == null)
                return null;

            return GetFlowActionDescription(lastActionType) ?? lastActionType;
        }

        /// <summary>
        /// Получить тип рабочего процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <returns>Тип рабочего процесса.</returns>
        public static Type GetProcessFlowType(this ProcessFlow processFlow)
        {
            var processFlowStringType = GetActionsCallChain(processFlow.Key).FirstOrDefault();

            if (string.IsNullOrEmpty(processFlowStringType))
                throw new InvalidOperationException($"Не удалось получить тип рабочего процесса из ключа '{processFlow.Key}'");

            return Type.GetType(processFlowStringType) ??
                   throw new InvalidOperationException(
                       $"Не удалось получить тип рабочего процесса по значению ключа '{processFlowStringType}'");
        }

        /// <summary>
        /// Получить цепочку вызовов рабочего процесса
        /// </summary>
        /// <param name="stateMachineComponentState">Состояние компонента конечного автомата</param>
        /// <returns>Цепочка вызовов рабочего процесса</returns>
        public static List<string> GetActionsCallChain(string stateMachineComponentState)
            => stateMachineComponentState.Split([ProcessFlowsConstants.StateActionDelimiter],
                StringSplitOptions.None).ToList();

        /// <summary>
        /// Получить описание действия процесса из аттрибута FlowActionDescriptionAttribute.
        /// </summary>
        /// <param name="flowActionType">Тип действия процесса.</param>
        /// <returns>Человекопонятное описание действия процесса.</returns>
        public static string GetFlowActionDescription(string flowActionType)
        {
            var typeOfAction = GetActionFlowType(flowActionType);

            var flowActionDescriptionAttr = typeOfAction?.GetCustomAttributes(
                typeof(FlowActionDescriptionAttribute), true
            ).FirstOrDefault() as FlowActionDescriptionAttribute;

            return flowActionDescriptionAttr?.ActionDescription;
        }

        /// <summary>
        /// Получить название шага рабочего процесса
        /// </summary>
        /// <param name="flowActionType"></param>
        /// <returns>Название шага рабочего процесса</returns>
        public static string GetActionFlowName(string flowActionType)
            => GetActionFlowType(flowActionType)?.Name;

        /// <summary>
        /// Получить тип шага рабочего процесса
        /// </summary>
        /// <param name="flowActionType">Тип действия процесса.</param>
        /// <returns>Тип шага рабочего процесса</returns>
        private static Type GetActionFlowType(string flowActionType)
            => Type.GetType(flowActionType);

        /// <summary>
        /// Получить данные шагов рабочего процесса
        /// </summary>
        /// <param name="actionFlows">Список шагов рабочего процесса</param>
        /// <returns>Данные шагов рабочего процесса</returns>
        public static List<ActionFlowDto> GetActionFlowsData(this ICollection<ActionFlow> actionFlows)
            => actionFlows.Select(acFl => new
            {
                acFl.Status,
                acFl.CountOfAttempts,
                acFl.ErrorMessage,
                ActionsChain = GetActionsCallChain(acFl.StateRefKey)
            })
            .OrderBy(acFl => acFl.ActionsChain.Count)
            .ToList()
            .Select(acFl =>
            {
                var currentActionType = acFl.ActionsChain.LastOrDefault();
                return new ActionFlowDto
                {
                    ErrorMessage = acFl.ErrorMessage,
                    CountOfAttemps = acFl.CountOfAttempts,
                    Status = acFl.Status,
                    ActionName = GetActionFlowName(currentActionType),
                    ActionDescription = GetFlowActionDescription(currentActionType)
                };
            }).ToList();

    }
}
