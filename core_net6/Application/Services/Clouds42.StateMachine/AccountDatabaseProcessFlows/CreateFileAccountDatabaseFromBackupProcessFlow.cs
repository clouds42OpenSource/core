﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.CreateAccountDatabaseFromBackup;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс создания фаловой инф. базы на основании бэкапа
    /// </summary>
    internal class CreateFileAccountDatabaseFromBackupProcessFlow(
        Configurator configurator,
        IUnitOfWork uow) :
        StateMachineBaseFlow<CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>(
            configurator, uow),
        ICreateFileAccountDatabaseFromBackupProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(CreateAccountDatabaseFromBackupParamsDto model)
            => $"{model.AccountDatabaseCreationParams.AccountDatabaseBackupId}:{model.AccountDatabaseCreationParams.DataBaseName}";

        /// <summary>
        /// Инициализировать карту создания инф. базы из бэкапа
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<InsertDatabaseRecordAction, CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>(
                next: () => Configurator.CreateAction<CreateAccountDatabaseDirectoryAction, CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>(
                next: () => Configurator.CreateAction<AddAccessToAccountDatabaseAction, CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>(
                next: () => Configurator.CreateAction<UpdateAccountDatabaseStateAction, CreateAccountDatabaseFromBackupParamsDto, CreateAccountDatabaseFromBackupParamsDto>(
            ))));
        }

        /// <summary>
        /// Сконвертировать результат создания в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override CreateAccountDatabaseFromBackupParamsDto ConvertResultToOutput(object result)
            => (CreateAccountDatabaseFromBackupParamsDto) result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(CreateAccountDatabaseFromBackupParamsDto model)
            => SynchronizeAccountDatabaseProcessFlowsHelper.GetProcessedObjectNameForCreateAccountDatabaseFromBackup(
                model.AccountDatabaseCreationParams, model.ParentProcessFlowKey);

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Создание файловой инф. базы на основании бэкапа";
    }
}
