﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.CopyAccountDatabaseToTomb;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс переноса бэкапа инф. базы в склеп
    /// </summary>
    class MoveAccountDatabaseBackupToTombProcessFlow(
        Configurator configurator,
        IUnitOfWork uow,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : StateMachineBaseFlow<CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                configurator, uow),
            IMoveAccountDatabaseBackupToTombProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(CopyAccountDatabaseBackupToTombParamsDto model)
            => $"{model.AccountDatabaseBackupId}::{model.AccountDatabaseId}";

        /// <summary>
        /// Инициализировать карту переноса бэкапа инф. базы в склеп
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<UploadAccountDatabaseBackupAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ValidateUploadedAccountDatabaseAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ClearAccountDatabaseLocalDataAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
                next: () => Configurator.CreateAction<ActualizeAccountDatabaseAfterArchivingToTombAction, CopyAccountDatabaseBackupToTombParamsDto, CopyAccountDatabaseBackupToTombParamsDto>(
            ))));
        }

        /// <summary>
        /// Сконвертировать результат копирования в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override CopyAccountDatabaseBackupToTombParamsDto ConvertResultToOutput(object result)
            => (CopyAccountDatabaseBackupToTombParamsDto)result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(CopyAccountDatabaseBackupToTombParamsDto model)
        {
            var accountDatabase =
                accountDatabaseDataProvider.GetAccountDatabaseOrThrowException(model.AccountDatabaseId);

            return $"{model.AccountDatabaseBackupId}::{accountDatabase.V82Name}";
        }

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Перенос бэкапа инф. базы в склеп";
    }
}
