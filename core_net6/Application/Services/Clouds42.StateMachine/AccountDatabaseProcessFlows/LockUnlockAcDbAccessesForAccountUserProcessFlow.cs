﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.LockUnlockAcDbAccesses;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс блокировки/разблокировки доступов пользователя к инф. базам
    /// </summary>
    internal class LockUnlockAcDbAccessesForAccountUserProcessFlow(Configurator configurator, IUnitOfWork uow) :
        StateMachineBaseFlow<LockUnlockAcDbAccessesParamsDto, bool>(configurator, uow),
        ILockUnlockAcDbAccessesForAccountUserProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(LockUnlockAcDbAccessesParamsDto model)
            => GetProcessedObjectName(model);

        /// <summary>
        /// Инициализировать карту выполнения рабочего процесса
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<StartTaskForLockUnlockAccessesInSmAction, LockUnlockAcDbAccessesParamsDto, LockUnlockAcDbAccessesParamsDto>(
                next: () => Configurator.CreateAction<LockUnlockAccessesInDbAction, LockUnlockAcDbAccessesParamsDto, bool>());
        }

        /// <summary>
        /// Сконвертировать результат копирования в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override bool ConvertResultToOutput(object result)
            => true;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(LockUnlockAcDbAccessesParamsDto model)
            => $"{model.AccountUserId}::{model.IsAvailable}";

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName 
            => "Управление состоянием доступов пользователя к инф. базам";
    }
}
