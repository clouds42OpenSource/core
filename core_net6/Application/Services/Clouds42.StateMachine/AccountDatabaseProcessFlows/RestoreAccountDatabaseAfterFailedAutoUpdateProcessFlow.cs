﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Actions.RestoreAccountDatabaseFromBackup;
using Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows;
using Clouds42.StateMachine.StateMachineFlow;

namespace Clouds42.StateMachine.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс восстановления инф. базы из бэкапа после не успешной попытки АО
    /// </summary>
    internal class RestoreAccountDatabaseAfterFailedAutoUpdateProcessFlow(Configurator configurator, IUnitOfWork uow) :
        StateMachineBaseFlow<RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>(configurator, uow),
        IRestoreAccountDatabaseAfterFailedAutoUpdateProcessFlow
    {
        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <returns>Ключ процесса</returns>
        protected override string CreateFlowIdentityKey(RestoreAccountDatabaseParamsDto model)
            => $"{model.AccountDatabaseBackupId}::{model.RestoreType}";

        /// <summary>
        /// Инициализировать карту восстановления инф. базы из бэкапа
        /// </summary>
        protected override void InitConfig()
        {
            Configurator.CreateAction<GetDestinationAccountDatabaseAction, RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>(
                next: () => Configurator.CreateAction<GetAccountDatabaseBackupAction, RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>(
                next: () => Configurator.CreateAction<RestoreAccountDatabaseAfterFailedAutoUpdateAction, RestoreAccountDatabaseParamsDto, RestoreAccountDatabaseParamsDto>(
            )));
        }

        /// <summary>
        /// Сконвертировать результат восстановления в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected override RestoreAccountDatabaseParamsDto ConvertResultToOutput(object result)
            => (RestoreAccountDatabaseParamsDto)result;

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected override string GetProcessedObjectName(RestoreAccountDatabaseParamsDto model)
            => CreateFlowIdentityKey(model);

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected override string ProcessFlowName => "Восстановление инф. базы из бэкапа после не успешной попытки автообновления";
    }
}
