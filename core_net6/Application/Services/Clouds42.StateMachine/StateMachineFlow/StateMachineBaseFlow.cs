﻿using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels.StateMachine;
using Clouds42.Domain.Enums.StateMachine;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Exceptions;
using Clouds42.StateMachine.Contracts.Models;
using Clouds42.StateMachine.Contracts.StateMachineFlow;
using Clouds42.StateMachine.Helpers;
using Core42.Application.Features.ProcessFlowContext.Constants;

namespace Clouds42.StateMachine.StateMachineFlow
{
    /// <summary>
    /// Базовая реализация конечного автомата.
    /// </summary>
    /// <typeparam name="TInput">Тип модели подающийся на вход в автомат.</typeparam>
    /// <typeparam name="TOutput">Тип модели результата работы автомата.</typeparam>
    public abstract class StateMachineBaseFlow<TInput, TOutput>(Configurator configurator, IUnitOfWork dbLayer)
        : IRetryProcess
    {

        private readonly Stack<ActionFlow> _flowActions = new();

        protected readonly Configurator Configurator = configurator;

        /// <summary>
        /// Запустить в работу процесс КА.
        /// </summary>
        /// <param name="model">Входящая модель.</param>
        /// <param name="throwExceptionIfError">Сгенирировать исключение если есть ошибки выполнения.</param>
        /// <returns>Результат выполнения процесса.</returns>
        public StateMachineResult<TOutput> Run(TInput model, bool throwExceptionIfError = true)
        {            
            InitConfig();

            if (!Configurator.Actions.Any())
                throw new InvalidOperationException("В процессе не определено ни одного действия");

            var processFlow = RegisterFlow(CreateFlowIdentityKey(model), model);

            var result = ProcessFlowActions(model, processFlow);

            ThrowExceptionIfAnyError(() => throwExceptionIfError, result);

            return result;
        }

        /// <summary>
        /// Запустить повторную попытку выполнения процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="throwExceptionIfError">Сгенирировать исключение если есть ошибки выполнения.</param>
        /// <returns>Результат выполнения процесса.</returns>
        public void Retry(ProcessFlow processFlow, bool throwExceptionIfError = true)
        {

            var currentFlowAction = dbLayer.ActionFlowRepository.FirstOrDefault(a =>
                                        a.ProcessFlowId == processFlow.Id && a.StateRefKey == processFlow.State) ??
                                    throw new RetryProcessFlowException(processFlow,
                                        "Не найден блок действия по состоянию процесса");

            InitConfig();
            SkipProcessedActions(processFlow);

            var currentActionConfig = Configurator.Actions.Peek();
            var model = currentFlowAction.InputDataInJson.DeserializeFromJson(currentActionConfig.ModelType);

            var result = ProcessFlowActions(model, processFlow);

            ThrowExceptionIfAnyError(() => throwExceptionIfError, result);
        }

        /// <summary>
        /// Создать идентификационный ключ процесса.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected abstract string CreateFlowIdentityKey(TInput model);

        /// <summary>
        /// Инициализировать карту выполнения рабочего процесса.
        /// </summary>
        protected abstract void InitConfig();

        /// <summary>
        /// Сконвертировать результат выполнения выполнения рабочего процесса в требумый тип данных.
        /// </summary>
        /// <param name="result">Результат выполения рабочего процесса.</param>
        /// <returns>Результат конвертации.</returns>
        protected abstract  TOutput ConvertResultToOutput(object result);

        /// <summary>
        /// Получить имя обрабатываемого процессом объекта.
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки.</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        protected abstract string GetProcessedObjectName(TInput model);

        /// <summary>
        /// Имя рабочего процесса.
        /// </summary>
        protected abstract string ProcessFlowName { get; }

        /// <summary>
        /// Выаолнить действия рабочего процесса КА.
        /// </summary>
        /// <param name="model">Входящая модель.</param>
        /// <param name="processFlow">Выполняемый процесс.</param>
        /// <returns>Результат выполнения процесса.</returns>
        private StateMachineResult<TOutput> ProcessFlowActions(object model, ProcessFlow processFlow)
        {
            var needRollback = false;
            var result = default(TOutput);

            bool finish = false;
            string message = null;

            try
            {
                var intRes = RunProcessFlow(model, processFlow);

                if (intRes is not TOutput)
                    throw new InvalidOperationException($"Тип результата выполнения '{intRes.GetType()}' не соответствует ожидаемому '{typeof(TOutput)}'");

                result = ConvertResultToOutput(intRes);
                finish = true;
                UpdateFlow(processFlow, StateMachineComponentStatus.Finish);
            }
            catch (ActionExecutionException ex)
            {
                message = ex.GetFullInfo(true);
                UpdateFlow(processFlow, StateMachineComponentStatus.Error, message);
                needRollback = true;
            }

            if (needRollback)
            {
                ProcessRollBack(processFlow);
            }

            return new StateMachineResult<TOutput>
            {
                ResultModel = result,
                Finish = finish,
                Message = message,
                ProcessFlowId = processFlow.Id
            };
        }

        /// <summary>
        /// Сгенирировать исключение если процесс завершился с ошибками. 
        /// </summary>
        private static void ThrowExceptionIfAnyError(Func<bool> checkCondition, StateMachineResult<TOutput> result)
        {
            if (checkCondition() && !result.Finish)
                throw new InvalidOperationException(result.Message);
        }

        /// <summary>
        /// Исключить из очереди выполненные задачи
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        private void SkipProcessedActions(ProcessFlow processFlow)
        {
            string key = null;

            while (true)
            {
                var actionConfig = Configurator.Actions.Peek();
                key = key == null ? actionConfig.Key : ToRefKeyFormat(key, actionConfig.Key);

                if (processFlow.State == key)
                    break;

                Configurator.Actions.Dequeue();
            }
        }

        /// <summary>
        /// Выполнить откат процесса в исходное состояние.
        /// </summary>
        /// <param name="processFlow">Выполняемый процесс.</param>
        private void ProcessRollBack(ProcessFlow processFlow)
        {
            ReInitConfig();

            if (!Configurator.Actions.Any(c=>c.CanRollback))
                return;

            UpdateFlow(processFlow, StateMachineComponentStatus.RollingBack);

            while (true)
            {
                var flowAction = _flowActions.Pop();

                var actionConfig = GetActionConfig(flowAction.StateRefKey);

                if (actionConfig.CanRollback)
                    RollbackAction(processFlow, actionConfig, flowAction);

                RegisterRollbackFlowState(processFlow);

                if (!_flowActions.Any())
                    break;
            }

            UpdateFlow(processFlow, StateMachineComponentStatus.RolledBack);
        }

        /// <summary>
        /// Выполнить откат действия процесса в исходное состояние.
        /// </summary>
        private void RollbackAction(ProcessFlow processFlow, Configurator.Item actionConfig, ActionFlow actionFlow)
        {
            var model = actionFlow.InputDataInJson.DeserializeFromJson(actionConfig.ModelType);
            var snapShot = actionFlow.SnapshotRollbackDataInJson.DeserializeFromJson(actionConfig.RollbackSnapshotType);

            try
            {
                TryInvokeAction(actionConfig, actionFlow, "Rollback", model, snapShot);
            }
            catch (ActionExecutionException ex)
            {
                SetExceptionForRollbackFlowAction(actionFlow, ex);
                UpdateFlow(processFlow, StateMachineComponentStatus.Error, ex.Message);
                throw;
            }            
        }

        /// <summary>
        /// Попытаться выполнить действие процесса.
        /// </summary>
        /// <param name="actionConfig">Конфигурация действия.</param>
        /// <param name="actionFlow">Данные о действие КА.</param>
        /// <param name="methodName">Имя выполняемого метода.</param>
        /// <param name="modelList">Список параметров.</param>
        /// <returns></returns>
        private object TryInvokeAction(Configurator.Item actionConfig, ActionFlow actionFlow, string methodName, params object[] modelList)
        {

            var thisType = actionConfig.Action.GetType();
            var theMethod = thisType.GetMethod(methodName);

            if (theMethod == null)
            {
                var message = $"У объекта {thisType}  не удалось получить метод {methodName}";
                UpdateFlowAction(actionFlow, StateMachineComponentStatus.Error, message);
                throw new InvalidOperationException(message);
            }

            if (actionConfig.TryCount == 0)
                throw new InvalidOperationException($"У блока обработки '{thisType.FullName}' установлено недопустимное значение настройки 'tryCount = 0'");

            var exceptionMessage = "";
            for (var attempts = 0; attempts < actionConfig.TryCount; attempts++)
            {
                try
                {
                    return theMethod.Invoke(actionConfig.Action, modelList);
                }
                catch (Exception ex)
                {
                    exceptionMessage = ex.GetFullInfo(true);
                }
            }

            throw new ActionExecutionException(exceptionMessage);
        }

        /// <summary>
        /// Получить конфигурацию действия по составному ключу состояния.
        /// </summary>
        /// <param name="stateRefKey">Составной ключ состояния.</param>
        /// <returns>Конфигурация действия.</returns>
        private Configurator.Item GetActionConfig(string stateRefKey)
        {

            string key = null;
            foreach (var actionConfig in Configurator.Actions)
            {
                key = key == null ? actionConfig.Key : ToRefKeyFormat(key, actionConfig.Key);

                if (stateRefKey == key)
                    return actionConfig;
            }

            throw new InvalidOperationException($"По ссылке '{stateRefKey}' не удалось получить элмент конфигурации");
        }

        /// <summary>
        /// Переинициализировать карту выполнения рабочего процесса.
        /// </summary>
        private void ReInitConfig()
        {
            Configurator.Actions.Clear();
            InitConfig();
        }

        /// <summary>
        /// Запустить процесс конечного автомата.
        /// </summary>
        /// <param name="modelObject">Модель подающаяся на вход КА.</param>
        /// <param name="processFlow">Выполняемый процесс.</param>
        /// <returns>Результат выполнения процесса КА.</returns>
        private object RunProcessFlow(object modelObject, ProcessFlow processFlow)
        {
            while (Configurator.Actions.Any())
            {
                var actionConfig = Configurator.Actions.Dequeue();

                RegisterNextFlowState(processFlow, actionConfig);

                var flowAction = CreateOrGetFlowAction(processFlow, modelObject);

                _flowActions.Push(flowAction);                

                if (actionConfig.CanRollback)
                {
                    var snapshotObject = TryInvokeAction(actionConfig, flowAction, "CreateSnapshotObject", modelObject);
                    flowAction.SnapshotRollbackDataInJson = snapshotObject.ToJson();
                    UpdateFlowAction(flowAction);
                }

                modelObject = DoAction(modelObject, actionConfig, flowAction);

                flowAction.ResultDataInJson = modelObject.ToJson();
                UpdateFlowAction(flowAction, StateMachineComponentStatus.Finish);
            }

            return modelObject;
        }

        /// <summary>
        /// Выполнить блок Do 
        /// </summary>
        private object DoAction(object modelObject, Configurator.Item actionConfig, ActionFlow flowAction)
        {
            try
            {
                modelObject = TryInvokeAction(actionConfig, flowAction, "Do", modelObject);
                return modelObject;
            }
            catch (ActionExecutionException ex)
            {
                SetExceptionForFlowAction(flowAction, ex);
                throw;
            }
        }

        /// <summary>
        /// Обновить состояние КА. 
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="state">Состояние рабочего процесса.</param>
        private void UpdateFlowState(ProcessFlow processFlow, string state)
        {
            processFlow.State = state;
            dbLayer.ProcessFlowRepository.Update(processFlow);
            dbLayer.Save();
        }

        /// <summary>
        /// Зарегистрировать переход на следующее действие. 
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="actionConfig">Конфигурация операции.</param>
        private void RegisterNextFlowState(ProcessFlow processFlow, Configurator.Item actionConfig)
        {
            var state = CreateActionKey(processFlow, actionConfig);            
            UpdateFlowState(processFlow, state);
        }

        /// <summary>
        /// Зарегистрировать откат на предидущее действие. 
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>        
        private void RegisterRollbackFlowState(ProcessFlow processFlow)
        {
            if (string.IsNullOrEmpty(processFlow.State))
                return;

            var actionTypes = ProcessFlowStateExtension.GetActionsCallChain(processFlow.State).ToArray();

            if (actionTypes.Length == 1)
                return;

            var state = string.Join(ProcessFlowsConstants.StateActionDelimiter, actionTypes.SubArray(0, actionTypes.Length - 1));
            UpdateFlowState(processFlow, state);
        }

        /// <summary>
        /// Создать ключ действия процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="actionConfig">Конфигурация операции.</param>
        /// <returns>Ключ действия.</returns>
        private string CreateActionKey(ProcessFlow processFlow, Configurator.Item actionConfig)
        {
            if (!string.IsNullOrEmpty(processFlow.State))
            {
                var lastActionKey = ProcessFlowStateExtension.GetActionsCallChain(processFlow.State).LastOrDefault();

                if (lastActionKey == actionConfig.Key)
                    return processFlow.State;
            }

            if (string.IsNullOrEmpty(processFlow.State) || processFlow.State == actionConfig.Key)
            {
                return actionConfig.Key;
            }

            return ToRefKeyFormat(processFlow.State, actionConfig.Key);
        }

        /// <summary>
        /// Привести к формату построения ключа КА.
        /// </summary>
        /// <param name="arg1">Аргумент 1.</param>
        /// <param name="arg2">Аргумент 2.</param>
        /// <returns>Значение составног ключа.</returns>
        private string ToRefKeyFormat(string arg1, string arg2)
            => arg1 + $"{ProcessFlowsConstants.StateActionDelimiter}{arg2}";

        /// <summary>
        /// Зарегистрировать рабочий процесс.
        /// </summary>        
        private ProcessFlow RegisterFlow(string flowIdentitiesKey, TInput model)
        {
            var processedObjectName = GetProcessedObjectName(model);

            var flow = new ProcessFlow
            {
                Id = Guid.NewGuid(),
                CreationDateTime = DateTime.Now,
                Name = ProcessFlowName,
                Key = ToRefKeyFormat(this.GetType().AssemblyQualifiedName, flowIdentitiesKey),
                Status = StateMachineComponentStatus.Processing,
                ProcessedObjectName = processedObjectName
            };            

            dbLayer.ProcessFlowRepository.Insert(flow);
            dbLayer.Save();

            return flow;
        }

        /// <summary>
        /// Обновить рабочий процесс КА.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="status">Устанавливаемый статус процесса.</param>
        /// <param name="message">Устанавливаемое сообщение к процессу.</param>
        private void UpdateFlow(ProcessFlow processFlow, StateMachineComponentStatus status, string message = null)
        {
            processFlow.Status = status;

            if (!string.IsNullOrEmpty(message))
                processFlow.Comment = message;

            dbLayer.ProcessFlowRepository.Update(processFlow);
            dbLayer.Save();
        }

        /// <summary>
        /// Зарегистрировать или получить действие процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="inputModel">Модель подающаяся на вход к действию.</param>        
        /// <returns></returns>
        private ActionFlow CreateOrGetFlowAction(ProcessFlow processFlow, object inputModel)
        {
            var actionFlow = dbLayer.ActionFlowRepository.FirstOrDefault(a =>
                a.ProcessFlowId == processFlow.Id && a.StateRefKey == processFlow.State);

            if (actionFlow != null)
                return actionFlow;

            actionFlow = new ActionFlow
            {
                Id = Guid.NewGuid(),
                ProcessFlowId = processFlow.Id,
                CreateDateTime = DateTime.Now,
                Status = StateMachineComponentStatus.Processing,
                InputDataInJson = inputModel.ToJson(),
                CountOfAttempts = 0,
                StateRefKey = processFlow.State
            };
            
            dbLayer.ActionFlowRepository.Insert(actionFlow);
            dbLayer.Save();

            return actionFlow;
        }

        /// <summary>
        /// Обновить действие процесса.
        /// </summary>
        /// <param name="actionFlow">Действие процесса.</param>
        /// <param name="status">Задаваемый статус действия.</param>
        /// <param name="message">Задаваемое сообщение действия.</param>
        private void UpdateFlowAction(ActionFlow actionFlow, StateMachineComponentStatus? status = null, string message = null)
        {
            actionFlow.CountOfAttempts += 1;

            if (status.HasValue)
                actionFlow.Status = status.Value;

            actionFlow.Comment = message;
            
            dbLayer.ActionFlowRepository.Update(actionFlow);
            dbLayer.Save();
        }

        /// <summary>
        /// Обновить действие процесса.
        /// </summary>
        /// <param name="actionFlow">Действие процесса.</param>
        /// <param name="exception">Исключение выполнения действия процесса.</param>        
        private void SetExceptionForFlowAction(ActionFlow actionFlow, ActionExecutionException exception)
        {
            if(exception.Message.Length > 1023)
                actionFlow.ErrorMessage = exception.Message[..1023];
            else
                actionFlow.ErrorMessage = exception.Message;

            actionFlow.ErrorStackTrace = exception.GetFullInfo();            
            dbLayer.ActionFlowRepository.Update(actionFlow);
            dbLayer.Save();
        }

        /// <summary>
        /// Обновить действие процесса.
        /// </summary>
        /// <param name="actionFlow">Действие процесса.</param>
        /// <param name="exception">Исключение выполнения действия процесса.</param>        
        private void SetExceptionForRollbackFlowAction(ActionFlow actionFlow, ActionExecutionException exception)
        {
            actionFlow.RollbackErrorMessage = exception.Message;
            actionFlow.RollbackErrorStackTrace = exception.GetFullInfo();            
            dbLayer.ActionFlowRepository.Update(actionFlow);
            dbLayer.Save();
        }

    }
}
