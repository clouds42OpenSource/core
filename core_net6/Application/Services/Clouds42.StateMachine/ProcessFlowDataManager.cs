﻿using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.ProcessFlow;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.Internal;

namespace Clouds42.StateMachine
{

    /// <summary>
    /// Менеджер доступа к данным рабочих процессов.
    /// </summary>
    public class ProcessFlowDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IProcessFlowDataProvider processFlowDataProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить список рабочих процессов по фильтру.
        /// </summary>
        /// <param name="args">Значения фильтра.</param>
        /// <returns>Список рабочих процессов.</returns>
        public ManagerResult<ProcessFlowDataModelDto> GetProcessFlowData(ProcessFlowFilterParamsDto args)
        {

            AccessProvider.HasAccess(ObjectAction.ProcessFlow_ViewList);
            try
            {
                return Ok(processFlowDataProvider.GetProcessFlowData(args));
            }
            catch (Exception e)
            {
                Logger.Warn(e, "Получение списка рабочих процессов");
                return PreconditionFailed<ProcessFlowDataModelDto>(e.Message);
            }
        }

        /// <summary>
        /// Получить детали рабочего процесса по ID
        /// </summary>
        /// <param name="processFlowId">ID рабочего процесса</param>
        /// <returns>Детали рабочего процесса</returns>
        public ManagerResult<ProcessFlowDetailsDto> GetProcessFlowDetailsById(Guid processFlowId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.ProcessFlow_ViewList);
                return Ok(processFlowDataProvider.GetProcessFlowDetailsById(processFlowId));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения деталей рабочего процесса]");
                return PreconditionFailed<ProcessFlowDetailsDto>(ex.Message);
            }
        }
    }
}
