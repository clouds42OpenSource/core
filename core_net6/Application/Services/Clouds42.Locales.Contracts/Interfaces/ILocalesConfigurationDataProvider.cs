﻿using Clouds42.DataContracts.Account.Locale;

namespace Clouds42.Locales.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для получения данных по конфигурациям локалей
    /// </summary>
    public interface ILocalesConfigurationDataProvider
    {
        /// <summary>
        /// Получить конфигурацию локали для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Конфигурация локали для аккаунта</returns>
        LocaleConfigurationDto GetLocaleConfigurationForAccount(Guid accountId);

        /// <summary>
        /// Получить адрес сайта личного кабинета для локали по аккаунту
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Адрес сайта личного кабинета для локали</returns>
        string GetCpSiteUrlForAccount(Guid accountId);

        /// <summary>
        /// Получить ID сегмента по умолчанию для локали
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>ID сегмента по умолчанию для локали</returns>
        Guid GetDefaultSegmentId(Guid localeId);

        /// <summary>
        /// Получить полный путь до экшена
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="partialActionPath">Частичный путь до экшена</param>
        /// <returns>Полный путь до экшена</returns>
        string GetFullPathToAction(Guid accountId, string partialActionPath);
    }
}
