﻿using Clouds42.DataContracts.Account.Locale;
using Clouds42.Domain.DataModels;

namespace Clouds42.Locales.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер для работы с локалью
    /// </summary>
    public interface ILocaleProvider
    {
        /// <summary>
        /// Получить данные локалей
        /// </summary>
        /// <returns>Данные локалей</returns>
        List<LocaleDataDto> GetLocales();

        /// <summary>
        /// Получить локаль
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Локаль</returns>
        Locale GetLocale(string localeName);

        /// <summary>
        /// Получить данные локалей по фильтру
        /// </summary>
        /// <param name="filterData">Данные фильтра</param>
        /// <returns>Данные локалей по фильтру</returns>
        LocalesDataDto GetByFilter(LocalesFilterDto filterData);

        /// <summary>
        /// Получить данные локали по ID
        /// </summary>
        /// <param name="localeId">ID локали</param>
        /// <returns>Данные локали</returns>
        LocaleItemDto GetById(Guid localeId);

        /// <summary>
        /// Изменить локаль
        /// </summary>
        /// <param name="model">Модель редактирования локали</param>
        void Edit(EditLocaleDto model);
    }
}
