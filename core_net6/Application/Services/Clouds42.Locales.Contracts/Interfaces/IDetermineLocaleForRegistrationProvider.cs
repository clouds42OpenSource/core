﻿using Clouds42.DataContracts.Account.Locale;

namespace Clouds42.Locales.Contracts.Interfaces
{
    /// <summary>
    /// Провайдер определения локали для регистрации аккаунта
    /// </summary>
    public interface IDetermineLocaleForRegistrationProvider
    {
        /// <summary>
        ///  Определить название локали
        ///  для регистрации аккаунта
        /// </summary>
        /// <param name="model">Модель данных для определения локали,
        /// для регистрации аккаунта</param>
        /// <returns>Название локали</returns>
        string? DetermineLocaleName(DetermineLocaleNameForRegistrationDto model);
    }
}
