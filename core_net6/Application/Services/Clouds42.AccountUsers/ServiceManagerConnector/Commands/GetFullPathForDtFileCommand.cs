﻿using System.Net;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для получения полного пути к файлу dt
    /// </summary>
    public class GetFullPathForDtFileCommand(
        ExecuteRequestCommand executeRequestCommand,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IGetFullPathForDtFileCommand
    {
        private readonly Lazy<string> _urlForGetFullPath = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForFullPathOnDt);

        /// <summary>
        /// Выполнить запрос по смене состояния блокировки пользователя в МС
        /// </summary>
        public string Execute(Guid fileId, Guid accountId)
        {
            logger.Trace(
                $"Начало формирование запроса с параметрами ID:{fileId}");

            var responseParameterNameForUploadUrl = CloudConfigurationProvider.DbTemplateDelimiters.GetResponseParameterNameForDtFilel();

            var fullUrl = string.Format(_urlForGetFullPath.Value, fileId);
            var response = executeRequestCommand.ExecuteJsonRequestBasicAuthorization(fullUrl, HttpMethod.Get);
            var urlForDt = GetValueFromResponseHeaders(response, responseParameterNameForUploadUrl);
             if (urlForDt == null)
            {
                return response.StatusCode switch
                {
                    HttpStatusCode.NotFound => throw new NotFoundException($"Файл dt по ID {fileId} не найден"),
                    _ => throw new InvalidOperationException(
                        $"Запрос вернул ошибку, строка запроса {fullUrl}, код ошибки {response.StatusCode}")
                };
            }

            var accountConfiguration = accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountId);

            var uploadedFilePath = GetPathForUploadingFile(accountConfiguration.Account.IndexNumber,
                accountConfiguration.Segment.CoreHosting.UploadFilesPath);

            var fullFilePath =  Path.Combine(uploadedFilePath, $"{Guid.NewGuid().GetEncodeGuid()}.dt");

            if (!Directory.Exists(uploadedFilePath))
            {
                Directory.CreateDirectory(uploadedFilePath);
                logger.Info($"Создана директория {uploadedFilePath}");
            }

            using var client = new HttpClient();
            
            var fileResponse = client.GetAsync(urlForDt, HttpCompletionOption.ResponseHeadersRead).Result;

            fileResponse.EnsureSuccessStatusCode();

            using var contentStream = response.Content.ReadAsStreamAsync().Result;
            using var fileStream = new FileStream(fullFilePath, FileMode.Create, FileAccess.Write, FileShare.None, 8192,
                true);

            contentStream.CopyToAsync(fileStream).GetAwaiter().GetResult();
            
            if (!File.Exists(fullFilePath))
            {
                logger.Info($"[{fullFilePath}] :: размер файла : {File.ReadAllBytes(fullFilePath)}");
                logger.Info($"[{fileId}] :: Файл сохранен по пути: {fullFilePath}");
            }

            logger.Info($"[{fileId}] :: Файл сохранен по пути: {fullFilePath}");

            return fullFilePath;
        }

        /// <summary>
        /// Получить значение из заголовков ответа
        /// </summary>
        /// <param name="response">Ответ на запрос</param>
        /// <param name="parameterName">Название параметра</param>
        /// <returns>Значение из заголовков ответа</returns>
        private static string? GetValueFromResponseHeaders(HttpResponseMessage response, string parameterName)
        {
            response.Headers.TryGetValues(parameterName, out var values);

            return values?.FirstOrDefault();
        }

        /// <summary>
        /// Получить имя папки для аккаунта
        /// </summary>
        /// <param name="accountIndexNumber">Номер аккаунта</param>
        /// <returns>Имя папки для аккаунта</returns>
        private static string GetAccountFolderName(int accountIndexNumber)
            => $"company_{accountIndexNumber}";

        /// <summary>
        /// Получить путь для загружаемого файла
        /// </summary>
        /// <param name="accountNumber">Номер аккаунта</param>
        /// <param name="uploadFilesPath">Путь для загрузки файлов из сегмента</param>
        /// <returns>Путь для загружаемого файла</returns>
        private static string GetPathForUploadingFile(int accountNumber, string uploadFilesPath) =>
            Path.Combine(uploadFilesPath, GetAccountFolderName(accountNumber));

    }
}
