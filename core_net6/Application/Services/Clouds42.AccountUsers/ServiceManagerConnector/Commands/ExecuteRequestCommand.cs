﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.CloudServiceContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.PowerShell.Commands;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
	/// Команда для выполнения запроса
	/// </summary>
    public class ExecuteRequestCommand(
        ILogger42 logger,
        IHandlerException handlerException,
        IHttpClientFactory httpClientFactory,
        ISender sender,
        IUnitOfWork unitOfWork)
    {
        private readonly SemaphoreSlim  _semaphoreSlim = new(1, 1);

        /// <summary>
        /// HTTP код ответа
        /// </summary>
        public HttpStatusCode StatusCode { private set; get; } = HttpStatusCode.Accepted;

        /// <summary>
        /// Содержимое ответа
        /// </summary>
        public string Content { set; get; }

        /// <summary>
        /// Выполнить json запрос
        /// </summary>
        /// <param name="fullUrl">Адрес для отправки</param>
        /// <param name="method">метод запроса</param>
        /// <param name="body">Тело запроса</param>
        public HttpResponseMessage ExecuteJsonRequest(string fullUrl, HttpMethod method, string? body = null)
        {
            try
            {
                var client = httpClientFactory.CreateClient();
                var jwtToken = GetOrRefreshJwtTokenAsync().Result;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));

                var request = new HttpRequestMessage(method, fullUrl);

                if (body != null)
                {
                    request.Content = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);
                }

                logger.Debug($"Запрос сформирован и отправляется по адресу {fullUrl} методом {method} с параметрами {body}");

                var response = client.Send(request);

                if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    throw new HttpResponseException("Удаленный сервер вернул конфликт (HTTP 409): " + response.Content.ReadAsStringAsync().Result, response);
                }
                
                return response;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запроса] Строка запроса {body}, адрес {fullUrl}");

                throw;
            }
        }

        public async Task<HttpResponseMessage> ExecuteJsonRequestAsync(string fullUrl, HttpMethod method, string? body = null)
        {
            try
            {
                var client = httpClientFactory.CreateClient();
                var jwtToken = await GetOrRefreshJwtTokenAsync();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwtToken);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypeNames.Application.Json));

                var request = new HttpRequestMessage(method, fullUrl);

                if (body != null)
                {
                    request.Content = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);
                }

                logger.Debug($"Запрос сформирован и отправляется по адресу {fullUrl} методом {method} с параметрами {body}");

                var response = await client.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.Conflict)
                {
                    throw new HttpResponseException("Удаленный сервер вернул конфликт (HTTP 409): " + await response.Content.ReadAsStringAsync(), response);
                }

                return response;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запроса] Строка запроса {body}, адрес {fullUrl}");

                throw;
            }
        }

        private async Task<string> GetOrRefreshJwtTokenAsync()
        {
            await _semaphoreSlim.WaitAsync();

            try
            {
                var cloudService = GetCloudService();
                
                if (cloudService?.JsonWebToken == null || TokenNeedsRefresh(cloudService.JsonWebToken))
                {
                    return sender.Send(new GetJwtByServiceIdQuery(cloudService!.CloudServiceId)).Result.Result.JsonWebToken;
                }

                return Encoding.UTF8.GetString(cloudService.JsonWebToken);
            }

            finally
            {
                _semaphoreSlim.Release();
            }
        }
        
        private static bool TokenNeedsRefresh(byte[] jwtTokenBytes)
        {
            JwtSecurityToken jwtSecurityToken;

            try
            {
                var jwtToken = Encoding.UTF8.GetString(jwtTokenBytes);

                jwtSecurityToken = new JwtSecurityToken(jwtToken);
            }

            catch (Exception)
            {
                return true;
            }

            return jwtSecurityToken.ValidTo <= DateTime.UtcNow.AddMinutes(-5);
        }

        /// <summary>
        /// Выполнить json запрос
        /// </summary>
        /// <param name="fullUrl">Адрес для отправки</param>
        /// <param name="method">метод запроса</param>
        public HttpResponseMessage ExecuteJsonRequestBasicAuthorization(string fullUrl, HttpMethod method)
        {
            try
            {

                var loginForRegistrationZone = CloudConfigurationProvider.DbTemplateDelimiters.GetLoginForRegistrationZone();
                var passwordForRegistrationZone = CloudConfigurationProvider.DbTemplateDelimiters.GetPasswordForRegistrationZone();
                var handler = new HttpClientHandler
                {
                    AllowAutoRedirect = false
                };
                var client = new HttpClient(handler);

                client.DefaultRequestHeaders
                       .Authorization = new AuthenticationHeaderValue(
                   "Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(
                $"{loginForRegistrationZone}:{passwordForRegistrationZone}")));

                var request = new HttpRequestMessage(method, fullUrl);

                logger.Debug($"Запрос сформирован и отправляется по адресу {fullUrl} методом {method}");

                var response = client.Send(request);

                return response;
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запроса] Строка запроса {fullUrl}");

                throw;
            }
        }


        /// <summary>
        /// Выполнить json запрос
        /// </summary>
        /// <param name="fullUrl">Адрес для отправки</param>
        /// <param name="method">метод запроса</param>
        /// <param name="body">Тело запроса</param>
        /// <param name="headerParameterName">Название параметра в заголовке</param>
        /// <param name="headerParameterValue">Значение параметра в заголовке</param>
        public HttpResponseMessage ExecuteJsonRequestWithHeaderParameter(string fullUrl, HttpMethod method, string headerParameterName,
            string headerParameterValue,  string? body = null)
        {
            try
            {
                var client = httpClientFactory.CreateClient();
                var token = GetOrRefreshJwtTokenAsync().Result;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                
                var request = new HttpRequestMessage(method, fullUrl);

                if (body != null)
                    request.Content = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);
                
                request.Headers.Add(headerParameterName, headerParameterValue);
                logger.Debug($"Запрос сформирован и отправляется по адресу {fullUrl} методом {method} с параметрами {body}");

                return client.Send(request);
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запроса] Строка запроса {body}, адрес {fullUrl}");

                throw;
            }
        }

        public async Task<HttpResponseMessage> ExecuteJsonRequestWithHeaderParameterAsync(string fullUrl, HttpMethod method, string headerParameterName,
            string headerParameterValue, string? body = null)
        {
            try
            {
                var client = httpClientFactory.CreateClient();
                var token = await GetOrRefreshJwtTokenAsync();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var request = new HttpRequestMessage(method, fullUrl);

                if (body != null)
                    request.Content = new StringContent(body, Encoding.UTF8, MediaTypeNames.Application.Json);

                request.Headers.Add(headerParameterName, headerParameterValue);
                logger.Debug($"Запрос сформирован и отправляется по адресу {fullUrl} методом {method} с параметрами {body}");

                return await client.SendAsync(request);
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка запроса] Строка запроса {body}, адрес {fullUrl}");

                throw;
            }
        }

        /// <summary>
        /// Получить объект ответа от МС
        /// </summary>
        /// <typeparam name="TOut">Тип ответа</typeparam>
        /// <returns>Объект ответа от МС</returns>
        public TOut? TryGetResponseObject<TOut>() where TOut : class
        {
            try
            {
                return Content.DeserializeXml<TOut>();
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка десериализазии ответа МС] {Content} в тип {typeof(TOut)}");

                return null;
            }
        }

        private CloudService? GetCloudService()
        {
            var service = unitOfWork.CloudServiceRepository
                .AsQueryableNoTracking()
                .Include(x => x.AccountUser)
                .FirstOrDefault(x => x.CloudServiceId.ToLower() == "CORE42".ToLower());
            
            return service;
        }
    }
}
