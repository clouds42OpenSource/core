﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;


namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда создания инф. базы из dt в zip в МС
    /// </summary>
    public class CreateFromDTToZipNewDelimiterCommand
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IConfiguration _configuration;
        private readonly ExecuteRequestCommand _executeRequestCommand;
        private readonly ILogger42 _logger;
        private readonly string _urlForApplications;
        private readonly IDictionary<HttpStatusCode, Func<string, string>> _mapResultCode;

        public CreateFromDTToZipNewDelimiterCommand(
            IUnitOfWork dbLayer,
            ExecuteRequestCommand executeRequestCommand,
            ILogger42 logger, IConfiguration configuration)
        {
            _dbLayer = dbLayer;
            _executeRequestCommand = executeRequestCommand;
            _urlForApplications = CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForApplications();
            _mapResultCode =
                new Dictionary<HttpStatusCode, Func<string, string>>
                {
                    { HttpStatusCode.NotFound, ExecutionErrorResult },
                    { HttpStatusCode.Conflict, ExecutionErrorResult },
                    { HttpStatusCode.Accepted, ExecutionAcceptedResult},
                    { HttpStatusCode.OK, ExecutionAcceptedResult}
                };
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Выполнить команду по созданию базы в МС
        /// </summary>
        /// <param name="createDatabaseFromZipInMsParams">Модель параметров создания инф. базы в МС</param>
        /// <returns>результат отправленного запроса</returns>
        public string Execute(CreateDatabaseFromZipInMsParamsDto createDatabaseFromZipInMsParams)
        {
            var dbTemplateDelimiters =
                _dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(t =>
                    t.TemplateId == createDatabaseFromZipInMsParams.TemplateId);
            if (dbTemplateDelimiters == null)
            {
                _logger.Warn($"Базы на разделителях в справочнике по такому Id: {createDatabaseFromZipInMsParams.TemplateId} не найдено ");
                throw new InvalidOperationException($"Базы на разделителях в справочнике по такому Id: {createDatabaseFromZipInMsParams.TemplateId} не найдено ");
            }
            var locale = _dbLayer.AccountsRepository.GetLocale(createDatabaseFromZipInMsParams.AccountId, 
                Guid.Parse(_configuration["DefaultLocale"] ?? "")).Name;

            _logger.Info($"Начало формирование xml с параметрами" +
                      $" AccountID:{createDatabaseFromZipInMsParams.AccountId}, AccountDatabaseID:{createDatabaseFromZipInMsParams.AccountDatabaseId}," +
                      $"ApplicationName:{createDatabaseFromZipInMsParams.AccountDatabaseCaption}, " +
                      $"SmUploadedFileId {createDatabaseFromZipInMsParams.UploadedFileId}," +
                      $"Login {createDatabaseFromZipInMsParams.LoginAdmin} Password {createDatabaseFromZipInMsParams.PasswordAdmin}");

            var addApplicationModel = GetModel(createDatabaseFromZipInMsParams, dbTemplateDelimiters);
            string? jsonAddApplication = addApplicationModel.ToJson();


            //полный адрес api запрос на создание новой зоны из dt в zip
            var fullUrl = string.Format(_urlForApplications, createDatabaseFromZipInMsParams.AccountDatabaseId, locale);
            _logger.Info($"Модельль запроса {jsonAddApplication} и адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var _response = _executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Put, jsonAddApplication);
            var responseData = _response.Content.ReadAsStringAsync().Result;
            _logger.Info($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимыи: {responseData}.");

            return _mapResultCode.ContainsKey(_response.StatusCode)!
                ? _mapResultCode[_response.StatusCode](responseData)
                : throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {_response.RequestMessage?.RequestUri}, код ошибки {_response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");

        }

        /// <summary>
        /// Получить модель запроса 
        /// </summary>
        /// <param name="createDatabaseFromZipInMsParams">Модель параметров создания инф. базы в МС</param>
        /// <param name="dbTemplateDelimiters">Шаблон ьазы на разделителях</param>
        /// <returns>Модель запроса</returns>
        private static AddApplicationFromDtToZipUploadFileDto GetModel(CreateDatabaseFromZipInMsParamsDto createDatabaseFromZipInMsParams,
            DbTemplateDelimiters dbTemplateDelimiters)
        {

            return new AddApplicationFromDtToZipUploadFileDto
            {
                AdministrationInMs = new AdministrationInMs
                {
                    LoginAdmin = createDatabaseFromZipInMsParams.LoginAdmin,
                    PasswordAdmin = createDatabaseFromZipInMsParams.PasswordAdmin
                },
                UsersList = createDatabaseFromZipInMsParams.UserFromZipPackage,
                AccountId = createDatabaseFromZipInMsParams.AccountId,
                FileType = "dt",
                ConfigurationId = dbTemplateDelimiters.ConfigurationId,
                ApplicationName = createDatabaseFromZipInMsParams.AccountDatabaseCaption,
                SmUploadedFileId = createDatabaseFromZipInMsParams.UploadedFileId
            };

        }

        /// <summary>
        /// Обработать результат ошибки выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionErrorResult(string data)
        {
            var result = data.DeserializeFromJson<ServiceManagerErrorResultDto>();

            return result?.DebugInfo!;
        }

        /// <summary>
        /// Обработать результат успешного выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private static string ExecutionAcceptedResult(string data)
        {
            data = String.Empty;
            return data;
        }

    }
}
