﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Logger;
using Newtonsoft.Json;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для получения списка конфигураций информационных баз
    /// </summary>
    internal class GetConfigurationDatabaseCommand(ExecuteRequestCommand executeRequestCommand)
        : IGetConfigurationDatabaseCommand
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Выполнить запрос в МС для получения списка конфигураций информационных баз для сервиса
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public List<string> Execute(Guid serviceId)
        {
            var prefix = $"Получение списка конфигураций для сервиса '{serviceId}' в МС";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            var _urlForConfigurationDatabase = new Lazy<string>( CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForConfigurationDatabase);
            //полный адрес api запрос на создание новой зоны
            var fullUrl = string.Format(_urlForConfigurationDatabase.Value, serviceId);

            //api запрос на создание новой зоны
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Get);
            var responseData = response.Content.ReadAsStringAsync().Result;
            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode is not (HttpStatusCode.OK or HttpStatusCode.Accepted))
            {
                return [];
            }

            var model = response.Content.ReadAsStringAsync().Result.DeserializeFromJson<ServiceConfigurationsResultDto>();

            return model.ConfigurationList.Select(conf => conf.ConfigurationID).ToList();

        }

        /// <summary>
        /// Выполнить запрос в МС для получения списка конфигураций информационных баз для сервиса
        /// </summary>
        /// <param name="serviceId">Идентификатор сервиса
        /// для информационной базы</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Результат выполнения</returns>
        public async Task<List<string>> ExecuteAsync(Guid serviceId, CancellationToken cancellationToken)
        {
            var prefix = $"Получение списка конфигураций для сервиса '{serviceId}' в МС";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            var urlForConfigurationDatabase = new Lazy<string>(CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForConfigurationDatabase);
            var fullUrl = string.Format(urlForConfigurationDatabase.Value, serviceId);
            var response = await executeRequestCommand.ExecuteJsonRequestAsync(fullUrl, HttpMethod.Get);
            var responseData = await response.Content.ReadAsStringAsync(cancellationToken);
            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode is not (HttpStatusCode.OK or HttpStatusCode.Accepted))
            {
                return [];
            }

            var model =JsonConvert.DeserializeObject<ServiceConfigurationsResultDto>(responseData);

            return model?.ConfigurationList?.Select(conf => conf.ConfigurationID)?.ToList() ?? [];
        }
    }
}
