﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для установки расширения сервиса в информационную базу
    /// </summary>
    internal class InstallServiceExtensionDatabaseCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand)
        : ChangeServiceExtensionDatabaseStateCommand(dbLayer, executeRequestCommand),
            IInstallServiceExtensionDatabaseCommand
    {
        private readonly Lazy<string> _urlForServiceExtensionDatabase = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForServiceExtensionDatabase);

        /// <summary>
        /// Выполнить запрос в МС для установки расширения в базу
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public ManageServiceExtensionDatabaseResultDto Execute(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
                => ChangeServiceExtensionDatabaseState(manageServiceExtensionDatabaseState, _urlForServiceExtensionDatabase.Value);
    }
}
