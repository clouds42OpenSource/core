﻿using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Core42.Application.Contracts.Features.CloudServiceContext.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{

    /// <summary>
    /// Команда для загрузки файлов в МС
    /// </summary>
    public class UploadFileCommand(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IHandlerException handlerException,
        IHttpClientFactory httpClientFactory,
        ISender sender)
        : IUploadFileCommand
    {
        private string _cookieValue;
        private readonly string _urlForUploadFiles = CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForUploadZipFiles();
        private readonly string _ibSessionParameterName = CloudConfigurationProvider.DbTemplateDelimiters.GetIbSessionParameterName();
        private readonly string _responseParameterNameForUploadUrl = CloudConfigurationProvider.DbTemplateDelimiters.GetResponseParameterNameForUploadUrl();
        private readonly string _ibSessionStartParameterValue = CloudConfigurationProvider.DbTemplateDelimiters.GetIbSessionStartParameterValue();
        private readonly int _uploadChunkFileBytesRequestTimeout = CloudConfigurationProvider.DbTemplateDelimiters.GetUploadChunkFileBytesRequestTimeout();
        private readonly SemaphoreSlim _semaphoreSlim = new(1, 1);

        /// <summary>
        /// Получить ID загружаемого файла
        /// </summary>
        /// <returns>ID загружаемого файла</returns>
        public string GetUploadedFileId(string uploadedFileName)
        {
            var urlForUploadFiles = $"{_urlForUploadFiles}{uploadedFileName}";
            try
            {
                var client = httpClientFactory.CreateClient();
                var jwtToken = GetOrRefreshJwtTokenAsync().Result;

                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", jwtToken);

                var request = CreateRequestMessage(urlForUploadFiles, HttpMethod.Post,
                    [new(_ibSessionParameterName, _ibSessionStartParameterValue)]);
                var response = client.Send(request);
                
                HandleExecutionResult(response);
                logger.Info($"Запрос отправлен успешно, строкой запроса {response.RequestMessage?.RequestUri}.");
                var idSessionCookies = GetCookieFromResponse(response);
                _cookieValue = idSessionCookies.Keys.First()+ "=" + idSessionCookies["ibsession"].ToString();
              
                return GetValueFromResponseHeaders(response, _responseParameterNameForUploadUrl) ?? "";
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при запуске сессии для загрузки файлов] Адрес {urlForUploadFiles}");
                throw;
            }
        }

        private async Task<string> GetOrRefreshJwtTokenAsync()
        {
            await _semaphoreSlim.WaitAsync();

            try
            {
                var cloudService = GetCloudService();
                var jwtToken = Encoding.UTF8.GetString(cloudService.JsonWebToken);

                if (string.IsNullOrEmpty(jwtToken) || TokenNeedsRefresh(jwtToken))
                {
                    return sender.Send(new GetJwtByServiceIdQuery(cloudService.CloudServiceId)).Result.Result.JsonWebToken;
                }

                return jwtToken;
            }

            finally
            {
                _semaphoreSlim.Release();
            }
        }

        private static bool TokenNeedsRefresh(string jwtToken)
        {
            JwtSecurityToken jwtSecurityToken;

            try
            {
                jwtSecurityToken = new JwtSecurityToken(jwtToken);
            }

            catch (Exception)
            {
                return true;
            }

            return jwtSecurityToken.ValidTo <= DateTime.UtcNow.AddMinutes(-5);
        }
        /// <summary>
        /// Получить ID загружаемого файла
        /// </summary>
        /// <returns>ID загружаемого файла</returns>
        public void StopUploadedFile(string uploadedFileName)
        {
            var urlForUploadFiles = $"{_urlForUploadFiles}{uploadedFileName}";

            try
            {
                var client = httpClientFactory.CreateClient();
               
                var jwtToken = GetOrRefreshJwtTokenAsync().Result;
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", jwtToken);

                var request = CreateRequestMessage(urlForUploadFiles, HttpMethod.Post,
                    [new(_ibSessionParameterName, "stop")]);

                request.Headers.Add(nameof(Cookie), _cookieValue);

                client.Send(request);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при запуске сессии для загрузки файлов] Адрес {urlForUploadFiles}");

                throw;
            }
        }
        /// <summary>
        /// Загрузить массив байтов части файла
        /// </summary>
        /// <param name="requestHeaders">Заголовки запроса</param>
        /// <param name="chunkData">Массив байтов части файла</param>
        /// <param name="uploadUrl">URL для загрузки</param>
        public Guid UploadChunkFileBytes(List<KeyValuePair<string, object>> requestHeaders, byte[] chunkData, string uploadUrl)
        {
            try
            {
                var client = httpClientFactory.CreateClient();
              
                var jwtToken = GetOrRefreshJwtTokenAsync().Result;
                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", jwtToken);

                client.Timeout = TimeSpan.FromMilliseconds(_uploadChunkFileBytesRequestTimeout);
                var request = CreateRequestMessage(uploadUrl, HttpMethod.Put, requestHeaders);
                request.Content = new ByteArrayContent(chunkData);
                request.Headers.Add(nameof(Cookie), _cookieValue);
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/octet-stream");

                var response = client.Send(request);
                
                logger.Info($"Запрос отправлен, строка запроса {response.RequestMessage?.RequestUri},код ответа {response.StatusCode} результат {response.Content}.");
                HandleExecutionResult(response);

                if (response.StatusCode != HttpStatusCode.Created)
                    return Guid.Empty;

                var uploadFileToSmResultDc = response.Content.ReadAsStringAsync().Result.DeserializeFromJson<UploadFileToSmResultDto>();

                return uploadFileToSmResultDc.UploadedFileId;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка загрузки массива байтов] Адрес {_urlForUploadFiles}");
                throw;
            }
        }

        /// <summary>
        /// Создать запрос
        /// </summary>
        /// <param name="urlForUploadFiles">URL для загрузки файлов</param>
        /// <param name="requestHeaders">Заголовки запроса</param>
        /// <param name="methodType">Тип метода</param>
        /// <returns>Запрос</returns>
        private HttpRequestMessage CreateRequestMessage(string urlForUploadFiles, HttpMethod methodType, List<KeyValuePair<string, object>> requestHeaders = null)
        {
            var request = new HttpRequestMessage(methodType, urlForUploadFiles);

            if(requestHeaders?.Any() ?? false)
                requestHeaders.ForEach(x => request.Headers.Add(x.Key, x.Value.ToString()));
            
            return request;
        }

        /// <summary>
        /// Получить куки из ответа
        /// </summary>
        /// <param name="response">Ответ на запрос</param>
        /// <returns>Куки из ответа</returns>
        private static Dictionary<string, string> GetCookieFromResponse(HttpResponseMessage response)
        {
            var cookie = response.Headers.FirstOrDefault(cook => cook.Key.Equals("Set-Cookie", StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
            Dictionary<string, string> keyValuePairs = cookie.Split(';')
            .Select(value => value.Split('='))
            .ToDictionary(pair => pair[0], pair => pair[1]);

            return keyValuePairs;
        }

        /// <summary>
        /// Получить значение из заголовков ответа
        /// </summary>
        /// <param name="response">Ответ на запрос</param>
        /// <param name="parameterName">Название параметра</param>
        /// <returns>Значение из заголовков ответа</returns>
        private static string? GetValueFromResponseHeaders(HttpResponseMessage response, string parameterName)
        {
            response.Headers.TryGetValues(parameterName, out var values);

            return values?.FirstOrDefault();
        }

        /// <summary>
        /// Обработать результат выполнения запроса
        /// </summary>
        /// <param name="response">Результат выполнения запроса</param>
        private void HandleExecutionResult(HttpResponseMessage response)
        {
            if (response.StatusCode is HttpStatusCode.OK or HttpStatusCode.Accepted or HttpStatusCode.Created)
                return;

            var error = response.Content.ReadAsStringAsync().Result;

            throw new InvalidOperationException($"Запрос вернул ошибку,строка запроса {response.RequestMessage?.RequestUri}, адрес {_urlForUploadFiles}, ошибка : {error}");
        }

        private CloudService GetCloudService()
        {
            var service = dbLayer.CloudServiceRepository
                .AsQueryable()
                .Include(x => x.AccountUser)
                .FirstOrDefault(x => x.CloudServiceId.ToLower() == "Core42".ToLower());

            if (service?.ServiceToken == null)
                throw new NotFoundException("Токен службы CORE42 не найден");

            return service;
        }
    }
}
