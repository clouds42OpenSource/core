﻿using System.Net;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для изменения состояния расширения сервиса в МС
    /// для информационной базы
    /// </summary>
    public abstract class ChangeServiceExtensionDatabaseStateCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand)
    {
        private readonly ILogger42 _logger = Logger42.GetLogger();
        private HttpResponseMessage _response;

        /// <summary>
        /// Изменить состояние сервиса для информационной базы
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <param name="urlServiceExtension">Aдрес для отправки запроса</param>
        /// <param name="isInstall">Операция установки</param>
        /// <returns>Результат смены состояния</returns>
        protected ManageServiceExtensionDatabaseResultDto ChangeServiceExtensionDatabaseState(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState, string urlServiceExtension,
            bool isInstall = true)
        {
            var operation = isInstall ? "Установка расширения." : "Удаление расширения.";

            var prefix =
                $"{operation} Сервис: {manageServiceExtensionDatabaseState.ServiceId}. База: {manageServiceExtensionDatabaseState.AccountDatabaseId}.";

            
            var locale = dbLayer.DatabasesRepository.GetAccountDatabase(manageServiceExtensionDatabaseState.AccountDatabaseId).Account.AccountConfiguration.Locale.Name;
            _logger.Info($"{prefix} Начинаем выполнять запрос в МС.");
            //полный адрес api запрос на создание новой зоны из zip

            var fullUrl = string.Format(urlServiceExtension, manageServiceExtensionDatabaseState.AccountDatabaseId, manageServiceExtensionDatabaseState.ServiceId, locale);
            _logger.Info($"Адрес {fullUrl} сформированы.");
            var method = isInstall ? HttpMethod.Put : HttpMethod.Delete;
            try
            {
                //api запрос на создание новой зоны
                _response = executeRequestCommand.ExecuteJsonRequest(fullUrl, method);
                var responseData = _response.Content.ReadAsStringAsync().Result;
                _logger.Info($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимыи: {responseData}.");

                return ProcessExecutionResult(responseData);
            }
            catch (Exception)
            {
                var errorResult = executeRequestCommand.TryGetResponseObject<ServiceManagerErrorResultDto>();
                return CreateResult(false, false, errorResult?.Description?? "Запрос отправлен и вернулся с ошибкой которую не смогли распарсить");
            }
        }

        /// <summary>
        /// Обработать результат выполнения запроса
        /// </summary>
        /// <returns>Результат обработки</returns>
        private ManageServiceExtensionDatabaseResultDto ProcessExecutionResult(string responseData)

        {
            if (_response.StatusCode is HttpStatusCode.OK or HttpStatusCode.Accepted)
                return CreateResult(true, false);

            var result = responseData.DeserializeFromJson<ServiceManagerErrorResultDto>();

            return CreateResult(false, false, result.Description);
        }

        /// <summary>
        /// Создать результат выполнения
        /// </summary>
        /// <param name="isSuccess">Признак что операция завершена успешно</param>
        /// <param name="needRetry">Признак необходимости переотправлять запрос</param>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        /// <returns>Результат выполнения</returns>
        private static ManageServiceExtensionDatabaseResultDto CreateResult(bool isSuccess, bool needRetry, string errorMessage = null)
            => new()
            {
                IsSuccess = isSuccess,
                NeedRetry = needRetry,
                ErrorMessage = errorMessage
            };
    }
}
