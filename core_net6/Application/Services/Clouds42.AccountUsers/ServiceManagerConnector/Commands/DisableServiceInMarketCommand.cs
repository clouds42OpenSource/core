﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для отключения сервиса в маркете
    /// </summary>
    internal class DisableServiceInMarketCommand(ExecuteRequestCommand executeRequestCommand)
        : IDisableServiceInMarketCommand
    {
        private readonly Lazy<string> _urlDisableServiceInMarket = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlDisableServiceInMarket);

        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Отключить сервис от маркета
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        public void Execute(Guid serviceId, bool isActive)
        {
            var flag = isActive ? "Включение" : "Отключение";
            var prefix = $"{flag} сервиса {serviceId} в маркете";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlDisableServiceInMarket.Value, serviceId);
            var jsonAddApplication = new ServiceActivationDto { IsActive = isActive }.ToJson();
            //api запрос
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Post, jsonAddApplication);
            var responseData = response.Content.ReadAsStringAsync().Result;
            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode is HttpStatusCode.OK or HttpStatusCode.Accepted)
                return;

            throw new InvalidOperationException($"При отключении сервиса {serviceId} от маркета в МС запрос пришел с невалидным статусом {response.StatusCode}");
        }

    }
}
