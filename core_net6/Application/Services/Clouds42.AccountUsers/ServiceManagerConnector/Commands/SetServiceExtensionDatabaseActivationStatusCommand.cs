﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using CommonLib.Enums;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для установки статуса активации
    /// расширения сервиса для инф. базы 
    /// </summary>
    internal class SetServiceExtensionDatabaseActivationStatusCommand : ISetServiceExtensionDatabaseActivationStatusCommand
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly ExecuteRequestCommand _executeRequestCommand;
        private readonly ILogger42 _logger;
        private readonly Lazy<string> _urlForServiceExtensionDatabase;
        private readonly Lazy<string> _urlForActivateUser;
        private readonly IDictionary<HttpStatusCode, Func<string, string>> _mapResultCode;
        private HttpResponseMessage _response;
        private string _fullUrl;

        public SetServiceExtensionDatabaseActivationStatusCommand(IUnitOfWork dbLayer, ExecuteRequestCommand executeRequestCommand, ILogger42 logger)
        {
            _dbLayer = dbLayer;
            _logger = logger;
            _executeRequestCommand = executeRequestCommand;
            _urlForServiceExtensionDatabase = new Lazy<string>(
                CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForServiceExtensionDatabase);
            _urlForActivateUser = new Lazy<string>(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForActivateUser);
            _mapResultCode =
                    new Dictionary<HttpStatusCode, Func<string, string>>
                    {
                        { HttpStatusCode.NotFound, ExecutionErrorResult },
                        { HttpStatusCode.Accepted, ExecutionAcceptedResult},
                        { HttpStatusCode.OK, ExecutionAcceptedResult}
                    };
        }

        /// <summary>
        /// Выполнить запрос в МС для установки статуса активации
        /// расширения сервиса для инф. базы
        /// </summary>
        /// <param name="serviceExtensionDatabaseActivationStatusDto">Модель для установки статуса активации расширения сервиса
        /// для инф. базы</param>
        public string Execute(SetServiceExtensionDatabaseActivationStatusDto serviceExtensionDatabaseActivationStatusDto)
        {
            var prefix =
                $@"Установка статуса активации {serviceExtensionDatabaseActivationStatusDto.IsActive}. 
                Сервис: {serviceExtensionDatabaseActivationStatusDto.ServiceId}. 
                Аккаунт: {serviceExtensionDatabaseActivationStatusDto.AccountId}.
                Демо период {serviceExtensionDatabaseActivationStatusDto.IsDemo}";

            _logger.Info($"{prefix} Начинаем выполнять запрос в МС.");
            var responseData = string.Empty;
            try
            {

                var locale = _dbLayer.AccountsRepository.GetAccount(serviceExtensionDatabaseActivationStatusDto.AccountId).AccountConfiguration.Locale.Name;

                if (serviceExtensionDatabaseActivationStatusDto.IsDemo && serviceExtensionDatabaseActivationStatusDto.ServiceId != Guid.Empty)
                {
                    _fullUrl = string.Format(_urlForServiceExtensionDatabase.Value, serviceExtensionDatabaseActivationStatusDto.AccountId, serviceExtensionDatabaseActivationStatusDto.ServiceId, locale);
                    _logger.Debug($"Модель запроса пустая и адрес {_fullUrl} сформированы.");
                    //api запрос 
                    _response = _executeRequestCommand.ExecuteJsonRequest(_fullUrl, HttpMethod.Delete);
                    responseData = _response.Content.ReadAsStringAsync().Result;
                    _logger.Debug($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимым: {responseData}.");

                    return _mapResultCode.ContainsKey(_response.StatusCode)!
                        ? _mapResultCode[_response.StatusCode](responseData)
                        : throw new InvalidOperationException(
                        $"Запрос вернул ошибку, строка запроса {_response.RequestMessage?.RequestUri}, код ошибки {_response.StatusCode}, адрес {_fullUrl}, ошибка : {responseData}");
                }

                var userIds = _dbLayer.ResourceRepository
                    .Where(r => (r.AccountId == serviceExtensionDatabaseActivationStatusDto.AccountId &&
                                    r.AccountSponsorId == null ||
                                    r.AccountSponsorId == serviceExtensionDatabaseActivationStatusDto.AccountId) &&
                                r.Subject != null
                                && (r.BillingServiceType.SystemServiceType == ResourceType.MyEntUser
                                     || r.BillingServiceType.SystemServiceType == ResourceType.MyEntUserWeb))
                    .Select(r => r.Subject.Value)
                    .Distinct()
                    .ToList();

                _logger.Debug($"Количество пользователей{userIds.Count}.");

                var jsonAddApplication = new UsersActivationStatusDto
                {
                    IsLocked = !serviceExtensionDatabaseActivationStatusDto.IsActive,
                    UserIds = userIds
                }.ToJson();

                _fullUrl = string.Format(_urlForActivateUser.Value, serviceExtensionDatabaseActivationStatusDto.AccountId);

                _logger.Debug($"Модель запроса {jsonAddApplication} и адрес {_fullUrl} сформированы.");

                _response = _executeRequestCommand.ExecuteJsonRequest(_fullUrl, HttpMethod.Post, jsonAddApplication);

                responseData = _response.Content.ReadAsStringAsync().Result;

                _logger.Debug($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимым: {responseData}.");
            }
            catch (Exception ex)
            {
                _logger.Debug($"Ошибка в запросе: {ex.Message}.");
            }

            return _mapResultCode.ContainsKey(_response.StatusCode)!
                   ? _mapResultCode[_response.StatusCode](responseData)
                   : throw new InvalidOperationException(
                   $"Запрос вернул ошибку, строка запроса {_response.RequestMessage?.RequestUri}, код ошибки {_response.StatusCode}, адрес {_fullUrl}, ошибка : {responseData}");
        }

        /// <summary>
        /// Обработать результат ошибки выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionErrorResult(string data)
        {
            var result = data.DeserializeFromJson<ServiceManagerErrorResultDto>();
            _logger.Debug($@"Отправка запроса в МС на установку статуса активации завершилось с ошибкой: {result.Description}");
            return result.Description;
        }

        /// <summary>
        /// Обработать результат успешного выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionAcceptedResult(string data)
        {
            return string.Empty;
        }
    }
}
