﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для удаления расширения сервиса из информационной базы
    /// </summary>
    internal class DeleteServiceExtensionDatabaseCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand)
        : ChangeServiceExtensionDatabaseStateCommand(dbLayer, executeRequestCommand),
            IDeleteServiceExtensionDatabaseCommand
    {
        private readonly Lazy<string> _urlForServiceExtensionDatabase = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForServiceExtensionDatabase);

        /// <summary>
        /// Выполнить запрос в МС для удаления расширения в базе
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        /// <returns>Результат выполнения</returns>
        public ManageServiceExtensionDatabaseResultDto Execute(ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState) =>
            ChangeServiceExtensionDatabaseState(manageServiceExtensionDatabaseState, _urlForServiceExtensionDatabase.Value, false);
    }
}
