﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.IDataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;


namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда создания инф. базы на разделителях
    /// </summary>
    public class CreateNewDelimiterCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand,
        ILogger42 logger,
        IConfiguration configuration)
        : ICreateNewDelimiterCommand
    {
        private readonly Lazy<string> _urlForApplications = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForApplications);

        /// <summary></summary>
        public void Execute(List<IAccountUser> accountUsers, string applicationName, Guid templateId, Guid accountId, Guid accountDatabaseId, bool demoData)
        {
            // Поиск базы на разделителях в справочнике
            var dbTemplateDelimiters = dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(t => t.TemplateId == templateId);
            if (dbTemplateDelimiters == null)
            {
                logger.Warn($"Базы на разделителях в справочнике по такому Id: {templateId} не найдено ");
                throw new InvalidOperationException($"Базы на разделителях в справочнике по такому Id: {templateId} не найдено ");
            }

            logger.Trace($"Начало формирование json с параметрами account-id:{accountId}, name:{applicationName}, users:{accountUsers.Select(au => au.Id).ToList()}," +
                    $"configuration:{dbTemplateDelimiters.ConfigurationId}, demo:{demoData}");

            //создание json модели для запроса
            var addApplicationModel = new AddApplicationDto
            {
                UsersList = accountUsers.Select(au => au.Id).ToList(),
                AccountId = accountId,
                ApplicationName = applicationName,
                ConfigurationId = dbTemplateDelimiters.ConfigurationId,
                Demo = demoData
            };

            var jsonAddApplication = addApplicationModel.ToJson();
            var locale = dbLayer.AccountsRepository.GetLocale(accountId, Guid.Parse(configuration["DefaultLocale"] ?? "")).Name;
            //полный адрес api запрос на создание новой зоны
            var fullUrl = string.Format(_urlForApplications.Value, accountDatabaseId, locale);
            logger.Debug($"Модель запроса {jsonAddApplication} и адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Put, jsonAddApplication);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Debug($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode != HttpStatusCode.Accepted)
                throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, код ошибки {response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");
        }

    }
}
