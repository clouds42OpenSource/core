﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для получения информации о сервисе из МС
    /// </summary>
    internal class GetServiceInfoCommand(ExecuteRequestCommand executeRequestCommand) : IGetServiceInfoCommand
    {
        private readonly Lazy<string> _urlInformationsFromService = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlInformationsFromService);

        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Получить информации о сервисе из МС
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Статус расширения</returns>
        public InformationForServiceDto? Execute(Guid serviceId)
        {
            var prefix = $"Получение информация о сервисе '{serviceId}'";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlInformationsFromService.Value, serviceId);

            //api запрос
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Get);
            var responseData = response.Content.ReadAsStringAsync().Result;

            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
            {
                return null;
            }

            var content = responseData.DeserializeFromJson<ResultInformationForServiceDto>();

            return content.Result;

        }

    }
}
