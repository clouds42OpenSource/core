﻿using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Newtonsoft.Json;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для начисления доп сеансов аккаунту
    /// </summary>
    public class CreateAccountUsersSessionsInMsCommand(ExecuteRequestCommand executeRequestCommand)
        : ICreateAccountUsersSessionsInMsCommand
    {
        /// <summary>
        /// Выполнить запрос в МС для начисления доп сеансов
        /// </summary>
        /// <param name="commonSessions">кол-во общих сеансов
        /// <param name="userSessions">кол-во по пользователям сеансов
        /// <param name="accountId">идентификатор аккаунта
        public void CreateAccountUsersSessionsInMs(int commonSessions, List<DataContracts.Cloud42Services.AccountUserSession>? userSessions, Guid accountId)
        {

            var url = string.Format(CloudConfigurationProvider.AdditionalSessions.GetMsUrl(), accountId);

            var body = new { CommonSessions = commonSessions, UserSessions = userSessions };

            var responseMessage = executeRequestCommand.ExecuteJsonRequest(url, HttpMethod.Post, JsonConvert.SerializeObject(body));

            if (!responseMessage.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Не удалось выполнить покупку сеансов");
            }
        }

    }
}
