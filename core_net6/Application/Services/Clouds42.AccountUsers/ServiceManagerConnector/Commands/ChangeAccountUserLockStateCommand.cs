﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountUsers;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для смены состояния блокировки пользователя в МС
    /// </summary>
    public class ChangeAccountUserLockStateCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand,
        ILogger42 logger)
    {
        private readonly Lazy<string> _urlForChangeAccountUserLockState = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForChangeAccountUserLockState);

        /// <summary>
        /// Выполнить запрос по смене состояния блокировки пользователя в МС
        /// </summary>
        /// <param name="userId">ID пользователя.</param>
        /// <param name="isAvailable">Признак доступности.</param>
        public ChangeAccountUserLockStateResultDto Execute(Guid userId, bool isAvailable)
        {
            var user = dbLayer.AccountUsersRepository.GetAccountUser(userId) ??
                       throw new InvalidOperationException(
                           $"Пользователь по Id {userId} не найден");

            var locale = user.Account.AccountConfiguration.Locale.Name;

            var jsonAddApplication = new LockStateApiModelDto { LockedState = !isAvailable }.ToJson();

            //полный адрес api запрос на создание новой зоны
            var fullUrl = string.Format(_urlForChangeAccountUserLockState.Value, userId, locale);
            logger.Info($"Модельль запроса {jsonAddApplication} и адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Post, jsonAddApplication);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Info($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимыи: {responseData}");

            switch (response.StatusCode)
            {
                case HttpStatusCode.Accepted:
                case HttpStatusCode.OK:
                    return new ChangeAccountUserLockStateResultDto
                    {
                        UpdateStateDateTime = DateTime.Now,
                        LockState = AccountUserLockState.Done
                    };
                case HttpStatusCode.NotFound:
                {
                    var result = responseData.DeserializeFromJson<ServiceManagerErrorResultDto>();

                    return new ChangeAccountUserLockStateResultDto
                    {
                        UpdateStateDateTime = DateTime.Now,
                        LockState = AccountUserLockState.Error,
                        ErrorMessage = result.Description
                    };
                }
                default:
                    throw new InvalidOperationException(
                        $"Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, код ошибки {response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");
            }
        }

    }
}
