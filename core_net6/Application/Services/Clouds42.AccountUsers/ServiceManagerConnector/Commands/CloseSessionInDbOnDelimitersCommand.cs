﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для запрос по завершению всех сеансов в базе на разделителях
    /// </summary>
    public class CloseSessionInDbOnDelimitersCommand(ExecuteRequestCommand executeRequestCommand, ILogger42 logger)
        : ICloseSessionInDbOnDelimitersCommand
    {
        private readonly Lazy<string> _urlForApplications = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForCloseSession);

        /// <summary>
        /// Выполнить запрос по завершению всех сеансов в базе на разделителях
        /// </summary>
        /// <param name="accountDatabase">информационные базы.</param>
        /// <param name="model">модель</param>
        public TerminateSessionsInDatabaseJobParamsDto Execute(Domain.DataModels.AccountDatabase accountDatabase, 
            TerminateSessionsInDatabaseJobParamsDto model)
        {

            var locale = accountDatabase.Account.AccountConfiguration.Locale.Name;

            //полный адрес api запрос на создание новой зоны
            var fullUrl = string.Format(_urlForApplications.Value, accountDatabase.Id, locale);
            logger.Info($"Адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Delete);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Info($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимыи: {responseData}.");

            if (response.StatusCode is HttpStatusCode.Accepted or HttpStatusCode.OK or HttpStatusCode.NotFound)
                return model;

            throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, код ошибки {response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");

        }

    }
}
