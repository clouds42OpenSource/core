﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для управления доступом к инф. базе в МС
    /// </summary>
    public class AccessToDbOnDelimiterCommand(
        IUnitOfWork dbLayer,
        ExecuteRequestCommand executeRequestCommand,
        ILogger42 logger)
        : IAccessToDbOnDelimiterCommand
    {
        /// <summary>
        /// Выполнить запрос в сервис менеджер.
        /// </summary>
        /// <param name="database">Информационная база.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="url">Адрес СМ.</param>
        /// <param name="corpParameters">Опционально. Список ролей пользователя.</param>
        /// <param name="isInstall">Операция установки доступов</param>
        public ManageAcDbAccessResultDto Execute(Domain.DataModels.AccountDatabase database, Guid userId, string url, string? corpParameters, bool isInstall = true)
        {

            if (!database.AccountDatabaseOnDelimiter.Zone.HasValue)
            {
                logger.Warn($"Для базы {database.Id} зона не найдена при предоставлении доступов");
                throw new InvalidOperationException($"Для базы {database.Id} не найдена зона при предоставлении доступов");
            }

            var accountUser = dbLayer.AccountUsersRepository.GetAccountUser(userId);
            if (accountUser == null)
            {
                logger.Warn($"Пользователь с таким Id: {userId} не найден");
                throw new InvalidOperationException($"Пользователь с таким Id: {userId} не найден");
            }

            var locale = accountUser.Account.AccountConfiguration.Locale.Name;

            var jsonModel = isInstall ? new ProfilesListResultDto(): null;

            if (!string.IsNullOrEmpty(corpParameters) && isInstall)
            {
                var urlForProfilesToDb = new Lazy<string>(CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForProfilesToDb);
                var urlForProfiles = string.Format(urlForProfilesToDb.Value, database.AccountId);

                logger.Info($"Адрес {urlForProfiles} сформированы.");

                var listProfiles = executeRequestCommand.ExecuteJsonRequest(urlForProfiles, HttpMethod.Get);

                if (listProfiles.StatusCode is HttpStatusCode.Accepted or HttpStatusCode.OK)
                {
                    var resultProfiles = listProfiles.Content.ReadAsStringAsync().Result.DeserializeFromJson<List<ProfilesListResultDto>>();

                    var profilesForDb = resultProfiles.FirstOrDefault(p => p.code == database.AccountDatabaseOnDelimiter.DbTemplateDelimiterCode);

                    logger.Info($"Получили список профилей {profilesForDb}.");

                    if (profilesForDb != null)
                    {
                        var corpProf = corpParameters.Split(';').ToList();

                        logger.Info($"Сформирована КОРП профили {corpProf}.");

                        var profiles = profilesForDb.profiles.Where(p => corpProf.Contains(p.ProfileName) && p.IsAdmin).ToList();

                        logger.Info($"Сформирована профили из МС {string.Join(",", profiles)} в количестве {profiles.Count}.");

                        jsonModel = new ProfilesListResultDto
                        {
                            code = profilesForDb.code,
                            name = profilesForDb.name,
                            profiles = profiles,
                        };

                        logger.Info($"Добавили профиль {string.Join(",", jsonModel.profiles)}, теперь количество {jsonModel.profiles.Count}.");

                    }

                }

            }
            var fullUrl = string.Format(url, database.Id, userId, locale);
            logger.Info($"Адрес {fullUrl} сформированы.");

            var method = isInstall ? HttpMethod.Put  : HttpMethod.Delete;
            
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, method, jsonModel.ToJson() );
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Info($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                case HttpStatusCode.Conflict:
                {
                    var result = responseData.DeserializeFromJson<ServiceManagerErrorResultDto>();
                    return new ManageAcDbAccessResultDto
                    {
                        AccessState = AccountDatabaseAccessState.Error,
                        UpdateStateDateTime = DateTime.Now,
                        ErrorMessage = result.Description
                    };
                }
                case HttpStatusCode.Accepted:
                case HttpStatusCode.OK:
                    return new ManageAcDbAccessResultDto
                    {
                        AccessState = AccountDatabaseAccessState.ProcessingGrant,
                        UpdateStateDateTime = DateTime.Now
                    };
                default:
                    return new ManageAcDbAccessResultDto
                    {
                        AccessState = AccountDatabaseAccessState.Error,
                        UpdateStateDateTime = DateTime.Now,
                        ErrorMessage = $"Ошибка выдачи доступа, код ошибки {response.StatusCode}, ошибка : {responseData}"
                    };
            }
        }
    }
}
