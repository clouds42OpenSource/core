﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для получения статуса установкисервиса в информационную базу
    /// </summary>
    internal class GetServiceExtensionDatabaseStatusCommand(ExecuteRequestCommand executeRequestCommand)
        : IGetServiceExtensionDatabaseStatusCommand
    {
        private readonly Lazy<string> _urlForServiceExtensionDatabaseStatus = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForServiceExtensionDatabaseStatus);

        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Получить статус расширения сервиса для информационной базы
        /// </summary>
        /// <param name="serviceId">Id сервиса</param>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        public ServiceExtensionDatabaseStatusDto Execute(Guid serviceId, Guid accountDatabaseId)
        {
            var prefix = $"Получение статус установки сервиса '{serviceId}' в информационную базу {accountDatabaseId}";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlForServiceExtensionDatabaseStatus.Value, accountDatabaseId, serviceId);

            //api запрос
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Get);
            var responseData = response.Content.ReadAsStringAsync().Result;
            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
            {
                return new ServiceExtensionDatabaseStatusDto();
            }

            var result = responseData.DeserializeFromJson<ServiceExtensionDatabaseStatusResponceMSDto>();

            return new ServiceExtensionDatabaseStatusDto
            {
                ExtensionDatabaseStatus = result.ExtensionDatabaseStatus,
                ServiceStatus = result.ExtensionDatabaseStatus is ServiceExtensionDatabaseStatusEnum.DoneInstall or ServiceExtensionDatabaseStatusEnum.ProcessingInstall,
                SetStatusDateTime = result.SetStatusDateTime
            };

        }

    }
}
