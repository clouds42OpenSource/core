﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для получения списка сервисов доступных для установки в информационную базу
    /// </summary>
    internal class GetServicesExtensionForDatabaseCommand(ExecuteRequestCommand executeRequestCommand)
        : IGetServicesExtensionForDatabaseCommand
    {
        private readonly Lazy<string> _urlServicesExtensionForDatabase = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlServicesExtensionForDatabase);

        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Получить списка сервисов доступных для установки в информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">Id информационной базы</param>
        /// <returns>Статус расширения</returns>
        public async Task<List<AvailableServiceExtensionForDatabaseDto>> Execute(Guid accountDatabaseId)
        {
            var prefix = $"Получение списка сервисов доступных для установки в информационную базу {accountDatabaseId}";
            var model = new List<AvailableServiceExtensionForDatabaseDto>();
            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlServicesExtensionForDatabase.Value, accountDatabaseId);

            //api запрос
            var response = await executeRequestCommand.ExecuteJsonRequestAsync(fullUrl, HttpMethod.Get);

            var responseData =  await response.Content.ReadAsStringAsync();

            _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} ");

            if (response.StatusCode is not (HttpStatusCode.OK or HttpStatusCode.Accepted))
            {
                _logger.Debug($"{prefix}:Запрос отправлен и вернулся со статусом: {response.StatusCode} Тело: {responseData}");

                return model;
            }

            var res = responseData.DeserializeFromJson<List<AvailableServiceExtensionForDatabaseResponseMsDto>>();
            if (res != null)
            {
                return res.Select(r => new AvailableServiceExtensionForDatabaseDto
                    {
                        ExtensionLastActivityDate = r.ExtensionLastActivityDate,
                        ExtensionState = r.ExtensionState,
                        Id = r.Id,
                        IsInstalled = r.ExtensionState is ServiceExtensionDatabaseStatusEnum.DoneInstall or ServiceExtensionDatabaseStatusEnum.ProcessingInstall,
                        Name = r.Name,
                        ShortDescription = r.ShortDescription,
                        Error = r.Error
                    })
                    .ToList();
            }

            return model;
        }

    }
}
