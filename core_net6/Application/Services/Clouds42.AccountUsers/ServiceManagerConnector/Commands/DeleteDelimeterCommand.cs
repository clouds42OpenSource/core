﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Удаление баз на разделителях из МС
    /// </summary>
    public class DeleteDelimeterCommand : IDeleteDelimiterCommand
    {
        private readonly ExecuteRequestCommand _executeRequestCommand;
        private readonly ILogger42 _logger;
        private readonly Lazy<string> _urlForApplications;
        private readonly IDictionary<HttpStatusCode, Func<string, string>> _mapResultCode;
        /// <summary>
        /// Ответ на запрос
        /// </summary>
        public string Answer { set; get; }

        /// <summary>
        /// Статус выполнения
        /// </summary>
        public HttpStatusCode StatusCode { set; get; }

        public DeleteDelimeterCommand(ExecuteRequestCommand executeRequestCommand, ILogger42 logger)
        {
            _executeRequestCommand = executeRequestCommand;
            _urlForApplications = new Lazy<string>(
                CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForApplications);
            _mapResultCode =
                new Dictionary<HttpStatusCode, Func<string, string>>
                {
                    { HttpStatusCode.NotFound, ExecutionErrorResult },
                    { HttpStatusCode.Accepted, ExecutionAcceptedResult},
                    { HttpStatusCode.OK, ExecutionAcceptedResult}
                };
            _logger = logger;
        }

        /// <summary>
        /// Выполнения запроса на удаление базы на разделителях в МС
        /// </summary>
        /// <returns>true - если удаление запроса прошло успешно</returns>
        public string Execute(Guid accountDatabaseId, string locale, string type = "manual")
        {

            var url = string.Format(_urlForApplications.Value, accountDatabaseId, locale);
            var fullUrl = url + $"&type={type}";
            _logger.Debug($"Адрес {fullUrl} сформирован.");

            //api запрос на удаление области
            var _response = _executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Delete);
            var responseData = _response.Content.ReadAsStringAsync().Result;
            _logger.Debug($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимым: {responseData}.");

            return _mapResultCode.ContainsKey(_response.StatusCode)!
                    ? _mapResultCode[_response.StatusCode](responseData)
                    : throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {_response.RequestMessage?.RequestUri}, код ошибки {_response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");

        }

        /// <summary>
        /// Обработать результат ошибки выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionErrorResult(string data)
        {
            var result = data.DeserializeFromJson<ServiceManagerErrorResultDto>();
            return result.Description;
        }

        /// <summary>
        /// Обработать результат успешного выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionAcceptedResult(string data)
        {
            data = String.Empty;
            return data;
        }
    }
}
