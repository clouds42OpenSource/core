﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Комманда для получения списка информационных баз доступных для установки сервиса
    /// </summary>
    internal class GetDatabasesForServicesExtensionCommand(ExecuteRequestCommand executeRequestCommand)
        : IGetDatabasesForServicesExtensionCommand
    {
        private readonly Lazy<string> _urlServiceExtensionForAccount = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlServiceExtensionForAccount);

        private readonly ILogger42 _logger = Logger42.GetLogger();

        /// <summary>
        /// Получить список информационных баз доступных для установки сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список ИБ для установки расширения</returns>
        public List<DatabaseForServiceExtensionDto>? Execute(Guid accountId, Guid serviceId)
        {
            var prefix = $"Получение списка информационных баз аккаунта {accountId} доступных для установки сервиса {serviceId}";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlServiceExtensionForAccount.Value, serviceId);

            //api запрос
            var response = executeRequestCommand.ExecuteJsonRequestWithHeaderParameter(fullUrl, HttpMethod.Get, "AccountID", accountId.ToString());

            var responseData = response.Content.ReadAsStringAsync().Result;
            _logger.Debug($"Запрос вернулся со статусом кода {response.StatusCode} и содержанием {responseData}");
            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
            {
                return null;
            }

            var model = responseData.DeserializeFromJson<DatabaseForServiceExtensionListDto>();

            return model.Result.Where(m=> m.AccountDatabaseID != Guid.Empty).ToList();
        }

        /// <summary>
        /// Получить список информационных баз доступных для установки сервиса
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceId">Id сервиса</param>
        /// <returns>Список ИБ для установки расширения</returns>
        public async Task<List<DatabaseForServiceExtensionDto>> ExecuteAsync(Guid accountId, Guid serviceId)
        {
            var prefix = $"Получение списка информационных баз аккаунта {accountId} доступных для установки сервиса {serviceId}";

            _logger.Info($"{prefix}: Начинаем выполнять запрос в МС.");

            //полный адрес api запроса
            var fullUrl = string.Format(_urlServiceExtensionForAccount.Value, serviceId);

            //api запрос
            var response = await executeRequestCommand.ExecuteJsonRequestWithHeaderParameterAsync(fullUrl, HttpMethod.Get, "AccountID", accountId.ToString());

            var responseData = await response.Content.ReadAsStringAsync();
            _logger.Debug($"Запрос вернулся со статусом кода {response.StatusCode} и содержанием {responseData}");
            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
            {
                return null;
            }

            var model = responseData.DeserializeFromJson<DatabaseForServiceExtensionListDto>();

            return model.Result.Where(m => m.AccountDatabaseID != Guid.Empty).ToList();
        }

    }
}
