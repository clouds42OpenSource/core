﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Редактирование имени базы на разделителях в МС
    /// </summary>
    public class RenameApplicationCommand(
        ExecuteRequestCommand executeRequestCommand,
        ILogger42 logger)
    {
        private readonly Lazy<string> _urlForApplications = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForApplications);

        /// <summary>
        /// Выполнения запроса на редактирование имени базы на разделителях в МС
        /// </summary>
        /// <param name="accountDatabaseId">Индификатор базы</param>
        /// <param name="caption">новое имя базы</param>
        /// <param name="locale">локаль пользователя</param>
        public void Execute(Guid accountDatabaseId, string caption, string locale)
        {
            logger.Trace(
                $"Начало формирования xml с параметрами AccountDatabaseID:{accountDatabaseId}, ApplicationName {caption} " +
                "для редактирования имени базы на разделителях");

            var jsonAddApplication = new RenameApplicationApiModelDto { ApplicationName = caption }.ToJson();

            //полный адрес api запрос на создание новой зоны
            var fullUrl = string.Format(_urlForApplications.Value, accountDatabaseId, locale);
            logger.Debug($"Модельль запроса {jsonAddApplication} и адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Post, jsonAddApplication);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Debug($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимыи: {responseData}.");

            if (response.StatusCode != HttpStatusCode.OK)
                throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, код ошибки {response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");

        }
    }
}
