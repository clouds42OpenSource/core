﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Model;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;


namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда создания инф. базы из zip в МС
    /// </summary>
    public class CreateFromZipNewDelimiterCommand
    {
        private readonly IUnitOfWork _dbLayer;
        private readonly IConfiguration _configuration;
        private readonly ExecuteRequestCommand _executeRequestCommand;
        private readonly ILogger42 _logger;
        private readonly string _urlForApplications;
        private readonly IDictionary<HttpStatusCode, Func<string, string>> _mapResultCode;
        private readonly bool _needUseNewMechanismForUploadFile;

        public CreateFromZipNewDelimiterCommand(
            IUnitOfWork dbLayer,
            ExecuteRequestCommand executeRequestCommand,
            ILogger42 logger, IConfiguration configuration)
        {
            _dbLayer = dbLayer;
            _executeRequestCommand = executeRequestCommand;
            _needUseNewMechanismForUploadFile = CloudConfigurationProvider.DbTemplateDelimiters.GetNeedUseNewMechanismForUploadFile();
            _urlForApplications = CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForApplications();
            _mapResultCode =
                new Dictionary<HttpStatusCode, Func<string, string>>
                {
                    { HttpStatusCode.NotFound, ExecutionErrorResult },
                    { HttpStatusCode.Conflict, ExecutionErrorResult },
                    { HttpStatusCode.Accepted, ExecutionAcceptedResult},
                    { HttpStatusCode.OK, ExecutionAcceptedResult}
                };
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// Выполнить команду по созданию базы из zip в МС
        /// </summary>
        /// <param name="createDatabaseFromZipInMsParams">Модель параметров создания инф. базы из zip в МС</param>
        /// <returns>результат отправленного запроса</returns>
        public string Execute(CreateDatabaseFromZipInMsParamsDto createDatabaseFromZipInMsParams)
        {
            var dbTemplateDelimiters =
                _dbLayer.DbTemplateDelimitersReferencesRepository.FirstOrDefault(t =>
                    t.TemplateId == createDatabaseFromZipInMsParams.TemplateId);
            var locale = _dbLayer.AccountsRepository.GetLocale(createDatabaseFromZipInMsParams.AccountId, 
                Guid.Parse(_configuration["DefaultLocale"] ?? "")).Name;
            if (dbTemplateDelimiters == null)
            {
                _logger.Warn($"Базы на разделителях в справочнике по такому Id: {createDatabaseFromZipInMsParams.TemplateId} не найдено ");
                throw new InvalidOperationException($"Базы на разделителях в справочнике по такому Id: {createDatabaseFromZipInMsParams.TemplateId} не найдено ");
            }

            _logger.Info($"Начало формирование xml с параметрами" +
                      $" AccountID:{createDatabaseFromZipInMsParams.AccountId}, AccountDatabaseID:{createDatabaseFromZipInMsParams.AccountDatabaseId}," +
                        $"ConfigurationID:{dbTemplateDelimiters.ConfigurationId}, ApplicationName:{createDatabaseFromZipInMsParams.AccountDatabaseCaption}, " +
                          $"FileAddress {createDatabaseFromZipInMsParams.FullFilePath}, SmUploadedFileId {createDatabaseFromZipInMsParams.SmUploadedFileId}," +
                          $"Extensions {string.Join(",", createDatabaseFromZipInMsParams.ExtensionsList)}");
            string? jsonAddApplication;

            if (!string.IsNullOrEmpty(createDatabaseFromZipInMsParams.FullFilePath))
            {
                var addApplicationModel = GetRequestModel(createDatabaseFromZipInMsParams, dbTemplateDelimiters);
                jsonAddApplication = addApplicationModel.ToJson();
            }
            else
            {
                var addApplicationModel = GetModel(createDatabaseFromZipInMsParams, dbTemplateDelimiters);
                jsonAddApplication = addApplicationModel.ToJson();
            }

            //полный адрес api запрос на создание новой зоны из zip
            var fullUrl = string.Format(_urlForApplications, createDatabaseFromZipInMsParams.AccountDatabaseId, locale);
            _logger.Info($"Модельль запроса {jsonAddApplication} и адрес {fullUrl} сформированы.");

            //api запрос на создание новой зоны
            var _response = _executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Put, jsonAddApplication);
            var responseData = _response.Content.ReadAsStringAsync().Result;
            _logger.Info($"Запрос отправлен и вернулся со статусом: {_response.StatusCode} и содержимыи: {responseData}.");

            return _mapResultCode.ContainsKey(_response.StatusCode)!
                ? _mapResultCode[_response.StatusCode](responseData)
                : throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {_response.RequestMessage?.RequestUri}, код ошибки {_response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");

        }

        /// <summary>
        /// Получить модель запроса 
        /// </summary>
        /// <param name="createDatabaseFromZipInMsParams">Модель параметров создания инф. базы из zip в МС</param>
        /// <param name="dbTemplateDelimiters">Шаблон ьазы на разделителях</param>
        /// <returns>Модель запроса</returns>
        private AddApplicationFromFileDto GetRequestModel(CreateDatabaseFromZipInMsParamsDto createDatabaseFromZipInMsParams,
            DbTemplateDelimiters dbTemplateDelimiters)
        {
            var isCreateAdmin = createDatabaseFromZipInMsParams.UserFromZipPackage.Any(a => a.AccountDatabaseZipUserId == null);

            var addApplicationModel = new AddApplicationFromFileDto
            {
                UsersList = isCreateAdmin ? [] : createDatabaseFromZipInMsParams.UserFromZipPackage,
                AccountId = createDatabaseFromZipInMsParams.AccountId,
                ApplicationName = createDatabaseFromZipInMsParams.AccountDatabaseCaption,
                ConfigurationId = dbTemplateDelimiters.ConfigurationId
            };

            if (createDatabaseFromZipInMsParams.ExtensionsList.Count > 0)
                addApplicationModel.ExtensionsList = createDatabaseFromZipInMsParams.ExtensionsList;

            if (!_needUseNewMechanismForUploadFile)
                addApplicationModel.FileAddress = createDatabaseFromZipInMsParams.FullFilePath;
            else
                addApplicationModel.SmUploadedFileId = createDatabaseFromZipInMsParams.SmUploadedFileId;

            if (isCreateAdmin)
                addApplicationModel.UserId = createDatabaseFromZipInMsParams.UserFromZipPackage.First(a => a.AccountDatabaseZipUserId == null).AccountUserId;

            return addApplicationModel;
        }

        /// <summary>
        /// Получить модель запроса 
        /// </summary>
        /// <param name="createDatabaseFromZipInMsParams">Модель параметров создания инф. базы из zip в МС</param>
        /// <param name="dbTemplateDelimiters">Шаблон ьазы на разделителях</param>
        /// <returns>Модель запроса</returns>
        private static AddApplicationFromUploadFileDto GetModel(CreateDatabaseFromZipInMsParamsDto createDatabaseFromZipInMsParams, DbTemplateDelimiters dbTemplateDelimiters)
        {

            return new AddApplicationFromUploadFileDto
            {
                UsersList = createDatabaseFromZipInMsParams.UserFromZipPackage,
                AccountId = createDatabaseFromZipInMsParams.AccountId,
                ApplicationName = createDatabaseFromZipInMsParams.AccountDatabaseCaption,
                ConfigurationId = dbTemplateDelimiters.ConfigurationId,
                SmUploadedFileId = createDatabaseFromZipInMsParams.UploadedFileId,
                ExtensionsList = createDatabaseFromZipInMsParams.ExtensionsList
            };

        }

        /// <summary>
        /// Обработать результат ошибки выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionErrorResult(string data)
        {
            var result = data.DeserializeFromJson<ServiceManagerErrorResultDto>();
            return result.DebugInfo;
        }

        /// <summary>
        /// Обработать результат успешного выполнения
        /// </summary>
        /// <returns>описание результата</returns>
        private string ExecutionAcceptedResult(string data)
        {
            data = String.Empty;
            return data;
        }

    }
}
