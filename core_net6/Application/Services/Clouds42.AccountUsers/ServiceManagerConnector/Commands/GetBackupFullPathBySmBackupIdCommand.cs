﻿using System.Net;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.DataModels;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ServiceManagerConnector.Commands
{
    /// <summary>
    /// Команда для получения полного пути к бэкапу по ID от МС
    /// </summary>
    public class GetBackupFullPathBySmBackupIdCommand(
        ExecuteRequestCommand executeRequestCommand,
        IUnitOfWork dbLayer,
        ILogger42 logger)
        : IGetBackupFullPathBySmBackupIdCommand
    {
        private readonly Lazy<string> _urlForGetBackupId = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetUrlForGetBackupFullPathBySmBackupId);

        /// <summary>
        /// Выполнить запрос по смене состояния блокировки пользователя в МС
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        public string Execute(Guid accountDatabaseBackupId)
        {
            var accountDatabaseBackup = GetAccountDatabaseBackupOrThrowException(accountDatabaseBackupId);

            if (accountDatabaseBackup.ServiceManagerAcDbBackup == null)
                throw new InvalidOperationException($"Для бэкапа {accountDatabaseBackupId} не найдена связь с бэкапом от МС");
            var locale = accountDatabaseBackup.AccountDatabase.Account.AccountConfiguration.Locale.Name;
            logger.Trace(
                $"Начало формирование запроса с параметрами ApplicationBackupID:{accountDatabaseBackup.ServiceManagerAcDbBackup.ServiceManagerAcDbBackupId}");

            var fullUrl = string.Format(_urlForGetBackupId.Value, accountDatabaseBackup.ServiceManagerAcDbBackup.ServiceManagerAcDbBackupId, locale);
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, HttpMethod.Get);
            var responseData = response.Content.ReadAsStringAsync().Result;
            return response.StatusCode switch
            {
                HttpStatusCode.Accepted or HttpStatusCode.OK => responseData.DeserializeFromJson<GetBackupFullPathBySmBackupIdResultDto>()
                    .BackupPath,
                HttpStatusCode.NotFound => throw new NotFoundException(
                    $"Бэкап инф. базы по ID {accountDatabaseBackupId} не найден, причина " +
                    $"{responseData.DeserializeFromJson<ServiceManagerErrorResultDto>().Description}"),
                _ => throw new InvalidOperationException(
                    $"Запрос вернул ошибку, строка запроса {fullUrl}, код ошибки {response.StatusCode}, ошибка : {responseData}")
            };
        }

        /// <summary>
        /// Получить бэкап инф. базы
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Бэкап инф. базы</returns>
        private AccountDatabaseBackup GetAccountDatabaseBackupOrThrowException(Guid accountDatabaseBackupId)
            => dbLayer.AccountDatabaseBackupRepository.FirstOrDefault(backup =>
                   backup.Id == accountDatabaseBackupId) ??
               throw new NotFoundException($"Бэкап инф. базы по ID {accountDatabaseBackupId} не найден");
    }
}
