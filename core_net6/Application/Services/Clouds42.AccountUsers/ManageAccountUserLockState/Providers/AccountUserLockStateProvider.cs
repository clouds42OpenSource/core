﻿using Clouds42.DataContracts.AccountUser;
using Clouds42.Logger;
using Clouds42.AccountUsers.Contracts.ManageAccountUserLockState.Interfaces;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Domain.Enums.AccountUsers;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ManageAccountUserLockState.Providers
{
    /// <summary>
    /// Провайдер управления блокировкой пользователей 
    /// </summary>
    public class AccountUserLockStateProvider(
        IUnitOfWork dbLayer,
        ChangeAccountUserLockStateCommand changeAccountUserLockStateCommand,
        ILogger42 logger,
        IHandlerException handlerException)
        : IAccountUserLockStateProvider
    {
        /// <summary>
        /// Сменить состояние блокировки
        /// </summary>
        /// <param name="manageAcUsrLockStateParams">Параметры управления состоянием блокировки пользователя в МС</param> 
        public void ChangeLockState(ManageAccountUserLockStateParamsDto manageAcUsrLockStateParams)
        {
            try
            {
                var result = changeAccountUserLockStateCommand.Execute(manageAcUsrLockStateParams.AccountUserId, manageAcUsrLockStateParams.IsAvailable);
                ProcessExecutionResult(manageAcUsrLockStateParams, result);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка смены состояния блокировки пользователя] {manageAcUsrLockStateParams.AccountUserId}");
                throw;
            }
        }

        /// <summary>
        /// Обработать результат выполнения
        /// </summary>
        /// <param name="manageAcUsrLockStateParams">Параметры управления состоянием блокировки пользователя в МС</param>
        /// <param name="changeLockStateResult">Результат управления состоянием</param>
        /// <returns>Признак что операция завершилась успешно</returns>
        private void ProcessExecutionResult(ManageAccountUserLockStateParamsDto manageAcUsrLockStateParams, ChangeAccountUserLockStateResultDto changeLockStateResult)
        {
            if (changeLockStateResult.LockState == AccountUserLockState.Done)
                return;

            if (changeLockStateResult.LockState == AccountUserLockState.Error && manageAcUsrLockStateParams.IsAvailable)
            {
                try
                {
                    logger.Debug("При смене состояния блокировки пользователя такого не найдено. Блокируем доступы у нас");
                    var accessUser = dbLayer.AcDbAccessesRepository
                        .FirstOrDefault(access => access.AccountUserID == manageAcUsrLockStateParams.AccountUserId);

                    if (accessUser == null)
                        return;

                    logger.Debug($"Для пользователя {manageAcUsrLockStateParams.AccountUserId}  меняем блокировку с {accessUser.IsLock} на {manageAcUsrLockStateParams.IsAvailable}");
                    accessUser.IsLock = manageAcUsrLockStateParams.IsAvailable;
                    dbLayer.AcDbAccessesRepository.Update(accessUser);

                    dbLayer.Save();

                    logger.Debug($"Смены состояния блокировки пользователя {manageAcUsrLockStateParams.AccountUserId} в БД прошла успешно");
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex, $"[Ошибка смены состояния блокировки пользователя в БД] {manageAcUsrLockStateParams.AccountUserId}");
                    throw;
                }
            }


        }



    }
}
