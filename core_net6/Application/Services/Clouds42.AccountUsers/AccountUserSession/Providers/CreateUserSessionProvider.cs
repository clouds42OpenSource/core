﻿using Clouds42.AccountUsers.AccountUserSession.Helpers;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUserSession.Providers
{
    /// <summary>
    ///     Класс управления сессиями
    /// </summary>
    internal class CreateUserSessionProvider(
        IUnitOfWork unitOfWork,
        AccountUserSessionValidator accountUserSessionValidator,
        IConfiguration configuration)
    {
        /// <summary>
        ///     Создать новую сессию
        /// </summary>
        public Guid CreateNewSession(Domain.DataModels.AccountUserSession newSession)
        {
            var existingSession = unitOfWork.AccountUserSessionsRepository
                .WhereLazy(w => w.AccountUserId == newSession.AccountUserId)
                .OrderByDescending(w => w.TokenCreationTime)
                .FirstOrDefault();

            if (existingSession != null && accountUserSessionValidator.SessionValid(existingSession, configuration["SessionLifeTime"]??"365"))
            {
                existingSession.AccountUserId = newSession.AccountUserId;
                existingSession.StaticToken = newSession.StaticToken;
                existingSession.ClientDescription = newSession.ClientDescription;
                existingSession.ClientDeviceInfo = newSession.ClientDeviceInfo;
                existingSession.ClientIPAddress = newSession.ClientIPAddress;
                existingSession.TokenCreationTime = DateTime.Now;

                unitOfWork.AccountUserSessionsRepository.Update(existingSession);
                unitOfWork.Save();

                return existingSession.Token;
            }

            newSession.Token = Guid.NewGuid();
            newSession.Id = Guid.NewGuid();
            newSession.TokenCreationTime = DateTime.Now;

            unitOfWork.AccountUserSessionsRepository.Insert(newSession);
            unitOfWork.Save();

            return newSession.Token;
        }
    }
}
