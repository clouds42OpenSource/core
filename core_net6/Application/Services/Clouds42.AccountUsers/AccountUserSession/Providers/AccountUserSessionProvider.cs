﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUserSession.Providers
{
    /// <summary>
    /// Провайдер сессий пользователей
    /// </summary>
    internal class AccountUserSessionProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        CreateUserSessionProvider createUserSessionProvider,
        IConfiguration configuration)
        : IAccountUserSessionProvider
    {
        private readonly IEncryptionProvider _encryptor = new DesEncryptionProvider(configuration);

        /// <summary>
        /// Проверить пароль пользователя
        /// </summary>
        /// <param name="loginModel">Модель данных для логина</param>
        /// <returns>true - если хэш введенного пароля совпадает со значением в базе</returns>
        public bool ValidateAccountUserPassword(AccountLoginModelDto loginModel)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(
                u =>
                    u.Login == loginModel.Username && u.CorpUserSyncStatus != "Deleted" &&
                    u.CorpUserSyncStatus != "SyncDeleted") ?? throw new NotFoundException($"Пользователь по логину {loginModel.Username} не найден");

            if (string.IsNullOrEmpty(loginModel.Password))
                throw new InvalidOperationException("Необходимо ввести пароль.");

            var loginModelPasswordHash = _encryptor.Encrypt(loginModel.Password);
            return loginModelPasswordHash.Equals(accountUser.Password, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Создать сессию
        /// </summary>
        /// <param name="loginModel">Модель данных для логина</param>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Токен пользователя</returns>
        public Guid CreateSession(LoginModelDto loginModel, Guid accountUserId)
        {
            var accUsr = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId);

            var newUserSession = new Domain.DataModels.AccountUserSession
            {
                AccountUserId = accUsr.Id,
                StaticToken = false,
                ClientDescription = loginModel.ClientDescription,
                ClientDeviceInfo = loginModel.ClientDeviceInfo + $"[{accessProvider.GetUserAgent()}]",
                ClientIPAddress = accessProvider.GetUserHostAddress()
            };

            var newToken = createUserSessionProvider.CreateNewSession(newUserSession);

            accUsr.LastLoggedIn = DateTime.Now;
            dbLayer.AccountUsersRepository.InsertOrUpdateAccountUser(accUsr);
            dbLayer.Save();

            return newToken;
        }
    }
}
