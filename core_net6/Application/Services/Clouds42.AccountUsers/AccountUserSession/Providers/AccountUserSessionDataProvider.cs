﻿using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUserSession.Providers
{
    /// <summary>
    /// Провайдер для получения данных по сессиям пользователя
    /// </summary>
    internal class AccountUserSessionDataProvider(IUnitOfWork dbLayer) : IAccountUserSessionDataProvider
    {
        /// <summary>
        /// Получить сессию пользователя
        /// </summary>
        /// <param name="sessionId">ID сессии</param>
        /// <returns>Сессия пользователя</returns>
        public Domain.DataModels.AccountUserSession GetSessionsById(Guid sessionId)
            => dbLayer.AccountUserSessionsRepository.GetAccountUserSession(sessionId);

        /// <summary>
        /// Получить все сессии пользователя
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <returns>Список сессий пользователя</returns>
        public List<Domain.DataModels.AccountUserSession> GetAllSessionsByUserId(Guid userId)
            => dbLayer.AccountUserSessionsRepository.GetAccountUserSessionByUserId(userId).ToList();

        /// <summary>
        /// Получить логин пользователя по токену
        /// </summary>
        /// <param name="token">Токен сессии</param>
        /// <returns>Логин пользователя</returns>
        public string GetLoginByToken(Guid token)
            => (
                from session in dbLayer.AccountUserSessionsRepository.WhereLazy()
                join accountUser in dbLayer.AccountUsersRepository.WhereLazy() on session.AccountUserId equals
                    accountUser.Id
                where session.Token == token
                select accountUser.Login
            ).FirstOrDefault()?? throw new InvalidOperationException("Не удалось получить сесссию по токену");
    }
}
