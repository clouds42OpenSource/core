﻿using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.Logger;
using Clouds42.AccountUsers.AccountUserSession.Helpers;
using Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces;
using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.AccountUsers.Contracts.ActivateAccountUser.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUserSession.Providers
{
    /// <summary>
    /// Провайдер входа пользователя через сессию
    /// </summary>
    internal class AccountUserSessionLoginProvider(
        IUnitOfWork dbLayer,
        AccountUserSessionValidator accountUserSessionValidator,
        IAccountUserSessionProvider accountUserSessionProvider,
        IActivateAccountUserProvider activateAccountUserProvider,
        IAccountUsersDataProvider accountUsersDataProvider,
        ILogger42 logger)
        : IAccountUserSessionLoginProvider
    {
        /// <summary>
        /// Войти с помощью почты
        /// </summary>
        /// <param name="email">Почта</param>
        /// <param name="pass">Пароль</param>
        /// <returns>ID сессии</returns>
        public Guid LoginByEmail(string email, string pass)
        {
            var accountUser = dbLayer.AccountUsersRepository.GetAccountUserByEmail(email)
                       ?? throw new NotFoundException("Account user not found");

            return Login(new LoginModelDto
            {
                AccountUserLogin = accountUser.Login,
                AccountUserPassword = pass
            });
        }

        /// <summary>
        /// Выполнить вход в облако по средствам логин/пароля. Создается запись с новой сессией пользователя.
        /// </summary>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Идентификатор созданной сессии. SessionToken.</returns>
        /// <remarks>
        /// Сопоставление логина и почты необходимо, тк есть возможность авторизоваться в Линк посредством почты,
        /// которая передается через логин (a.Email == model.AccountUserLogin)
        /// </remarks>
        public Guid Login(LoginModelDto model)
        {
            var accountUser = accountUsersDataProvider.SearchAccountUser(model.AccountUserLogin) ??
                              throw new NotFoundException("Пользователь по указанным реквизитам не найден");

            if (accountUserSessionValidator.ValidateUserCredentials((Domain.DataModels.AccountUser)accountUser, model.AccountUserPassword))
            {
                logger.Info($"Создаём сесcию для пользователя {accountUser.Login}");
                return accountUserSessionProvider.CreateSession(model, accountUser.Id);
            }

            logger.Info("Пытаемся активировать пользователя с помощью полученного кода");
            var activateResult = activateAccountUserProvider.ActivateUserByPhone(accountUser.Id, model.AccountUserPassword);

            if (activateResult)
                return CreateUserSession(accountUser.Id, model.AccountUserLogin, model.ClientDescription, model.ClientDeviceInfo);

            throw new NotAuthorizedException($"Не удалось авторизовать пользователя {accountUser.Login}");
        }


        /// <summary>
        /// Выполнить вход в облако по средствам логин для межсервисного взаимодействия. Создается запись с новой сессией пользователя.
        /// </summary>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Токен сессии.</returns>
        public Guid CrossServiceLogin(CrossServiceLoginModelDto model)
        {
            var accountUser = accountUsersDataProvider.SearchAccountUser(model.AccountUserLogin) ??
                              throw new NotFoundException("Пользователь по указанным реквизитам не найден");

            if (accountUser.Id == model.AccountUserId)
            {
                return CreateUserSession(accountUser.Id, model.AccountUserLogin, model.ClientDescription, model.ClientDeviceInfo);
            }

            throw new NotAuthorizedException($"Не удалось авторизовать пользователя {accountUser.Login}");
        }

        /// <summary>
        /// Создать сессию для пользователя
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="userLogin">Логин пользователя</param>
        /// <param name="clientDescriotion">Описание клиента</param>
        /// <param name="clientDeviceInfo">Описание устройства клиента</param>
        /// <returns>Токен сессии.</returns>
        private Guid CreateUserSession(Guid accountUserId, string userLogin, string clientDescriotion, string clientDeviceInfo)
        {
            logger.Info($"Создаём сесcию для пользователя {userLogin}");
            return accountUserSessionProvider.CreateSession(new LoginModelDto
            {
                AccountUserLogin = userLogin,
                ClientDescription = clientDescriotion,
                ClientDeviceInfo = clientDeviceInfo
            }, accountUserId);
        }
    }
}
