﻿using Clouds42.Common.Encrypt;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUserSession.Helpers
{
    /// <summary>
    /// Валидатор пользовательской сессии
    /// </summary>
    public class AccountUserSessionValidator(IConfiguration configuration)
    {
        private readonly IEncryptionProvider _encryptor = new DesEncryptionProvider(configuration);

        /// <summary>
        ///     Валидация пользователя
        /// </summary>
        /// <param name="accUsr">Пользователь аккаунта</param>
        /// <param name="password">Пароль</param>
        public bool ValidateUserCredentials(Domain.DataModels.AccountUser accUsr, string password)
        {
            if (accUsr == null || !accUsr.Activated)
                return false;

            return _encryptor.Encrypt(password)
                .Equals(accUsr.Password, StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Признак что сессия валидна
        /// </summary>
        /// <param name="session">Сессия</param>
        /// <returns>Признак валидности сессии</returns>
        public bool SessionValid(Domain.DataModels.AccountUserSession session, string sessionLifeTime)
        {
            // static token never expire
            if (session.StaticToken.HasValue && session.StaticToken.Value)
                return true;

            if (!int.TryParse(sessionLifeTime, out var expDays))
                expDays = 3; // default expiration time

            // check if session wasn`t expired "expDays" ago
            return session.TokenCreationTime > DateTime.Now.AddDays(-expDays);
        }
    }
}
