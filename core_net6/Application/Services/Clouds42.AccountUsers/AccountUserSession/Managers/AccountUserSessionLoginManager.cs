﻿using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUserSession.Managers
{
    /// <summary>
    /// Менеджер управления логированием пользователя
    /// </summary>
    public class AccountUserSessionLoginManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountUserSessionLoginProvider accountUserSessionLoginProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Логирование с помощью почты
        /// </summary>
        /// <param name="email"></param>
        /// <param name="pass"></param>
        public ManagerResult<Guid> LoginByEmail(string email, string pass)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_Login);
                return Ok(accountUserSessionLoginProvider.LoginByEmail(email, pass));
            }
            catch (Exception)
            {
                return NotFound<Guid>("Account user not found");
            }
        }

        /// <summary>
        /// Выполнить вход в облако по средствам логин/пароля. Создается запись с новой сессией пользователя.
        /// </summary>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Идентификатор созданной сессии. SessionToken.</returns>
        /// <remarks>
        /// Сопоставление логина и почты необходимо, тк есть возможность авторизоваться в Линк посредством почты,
        /// которая передается через логин (a.Email == model.AccountUserLogin)
        /// </remarks>
        public ManagerResult<Guid> Login(LoginModelDto model)
        {
            if (model == null)
                return PreconditionFailed<Guid>("Model is null");

            AccessProvider.HasAccess(ObjectAction.AccountUserSessions_Login);

            try
            {
                return Ok(accountUserSessionLoginProvider.Login(model));
            }
            catch (NotAuthorizedException)
            {
                return Forbidden<Guid>("Username or password is invalid");
            }
            catch (NotFoundException)
            {
                return NotFound<Guid>("Account user not found");
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message, "Ошибка выполнения Login", () => model.AccountUserLogin,
                    () => model.ClientDescription, () => model.ClientDeviceInfo);
                return BadRequest<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Выполнить вход в облако по средствам логина для кросс-сервисного взаимодействия.
        /// </summary>
        /// <remarks>
        /// Не должен использовать как открытый метод входа пользователя.
        /// </remarks>
        /// <param name="model">Параметры входа.</param>
        /// <returns>Токен сессии.</returns>
        public ManagerResult<Guid> CrossServiceLogin(CrossServiceLoginModelDto model)
        {
            if (model == null)
                return PreconditionFailed<Guid>("Model is null");

            AccessProvider.HasAccess(ObjectAction.AccountUserSessions_Login);

            try
            {
                return Ok(accountUserSessionLoginProvider.CrossServiceLogin(model));
            }
            catch (NotAuthorizedException)
            {
                return Forbidden<Guid>("Username or password is invalid");
            }
            catch (NotFoundException)
            {
                return NotFound<Guid>("Account user not found");
            }
            catch (Exception ex)
            {
                handlerException.HandleWarning(ex.Message, "Ошибка выполнения Login", () => model.AccountUserLogin);
                return BadRequest<Guid>(ex.Message);
            }
        }
    }
}
