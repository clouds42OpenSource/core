﻿using System.Diagnostics;
using System.Security.Claims;
using Clouds42.AccountUsers.Contracts;
using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.ManagersResults;
using Clouds42.Configurations;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;
using Clouds42.DataContracts.AccountUser.AccountUserSession.InnerModels;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUserSession.Managers
{
    /// <summary>
    /// Менеджер сессий пользователей
    /// </summary>
    public class AccountUserSessionManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHashProvider hashProvider,
        IHandlerException handlerException,
        IAccountUserSessionDataProvider accountUserSessionDataProvider,
        IAccountUserSessionProvider accountUserSessionProvider,
        ILogger42 logger,
        IConfiguration configuration)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountUserSessionManager
    {

        /// <summary>
        /// Получить свойства пользователя для заданного токена сессии.
        /// </summary>
        /// <param name="token">Токен сессии пользователя</param>
        /// <returns>Возвращает свойства пользователя, если они доступны или ошибку в противном случае</returns>
        public async Task<ManagerResult<Domain.DataModels.AccountUser>> GetUserPropertiesByTokenAsync(Guid token)
        {
            logger.Trace($"[GetUserPropertiesByTokenAsync]Начало получения свойства пользователя для токена {token}");

            Domain.DataModels.AccountUser accountUser;
            try
            {
                logger.Trace($"[GetUserPropertiesByTokenAsync][Token-{token}]:Отправление запроса SQL ");
                accountUser = await DbLayer.AccountUserSessionsRepository.GetAccountUserForTokenAsync(token);
                logger.Trace($"[GetUserPropertiesByTokenAsync][Token-{token}]:Получен ответ");

                if (accountUser == null)
                {
                    logger.Warn($"[GetUserPropertiesByTokenAsync][Token-{token}]:Пользователь не найден");
                    return NotFound<Domain.DataModels.AccountUser>($"Token {token} is not valid");
                }

                logger.Trace(
                    $"[GetUserPropertiesByTokenAsync][Token-{token}]:Найден пользователь с логином {accountUser.Login} и AccountId {accountUser.AccountId}");
                logger.Trace(
                    $"[GetUserPropertiesByTokenAsync][Token-{token}]:Начало HasAccess для AccountId {accountUser.AccountId}");
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => accountUser.AccountId);
                logger.Trace(
                    $"[GetUserPropertiesByTokenAsync][Token-{token}]:Конец HasAccess для AccountId {accountUser.AccountId}");

                logger.Trace(
                    $"[GetUserPropertiesByTokenAsync][Token-{token}]:Результат сформирован для пользователя с логином {accountUser.Login}");

                return Ok(accountUser);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $" [Ошибка получения свойства пользователя для заданного токена сессии] Token-{token}");
                return PreconditionFailed<Domain.DataModels.AccountUser>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить корректность токена сессии.
        /// </summary>
        /// <param name="token">Токен сессии</param>
        /// <returns>Состояние корректности токена</returns>
        public async Task<ManagerResult<bool>> CheckTokenValidityAsync(Guid token)
        {
            var sessionLifeTime = configuration["SessionLifeTime"];
            if (!int.TryParse(sessionLifeTime, out var expDays)) expDays = 3;

            var userSession = await DbLayer.AccountUserSessionsRepository.GetAccountUserSessionInfoAsync(token, expDays);
          
            if (userSession == null)
            {
                var accountId = await DbLayer.AccountUserSessionsRepository.GetAccountIdBySessionTokenAsync(token);
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => accountId);
                return Ok(false);
            }

            AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => userSession.AccountId);

            return Ok(true);
        }

        /// <summary>
        /// Получить идентификатор пользователя по токену сессии.
        /// </summary>
        /// <param name="token">Токен сесии</param>
        /// <returns>Идентификатор пользователя</returns>
        public ManagerResult<ResultDto<Guid>> GetAccountUserIdByToken(Guid token)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var userSession = DbLayer.AccountUserSessionsRepository.Where(s => s.Token == token).MaxBy(s => s.TokenCreationTime);

            stopWatch.Stop();
            var logString = "&ReadDB:" + stopWatch.ElapsedMilliseconds;

            if (userSession == null)
                return NotFound<ResultDto<Guid>>("Invalid Token. Token not found");
            
            stopWatch.Restart();
            if (userSession.StaticToken.HasValue && userSession.StaticToken.Value)
            {
                var sessionLifeTime = configuration["SessionLifeTime"];
                if (!int.TryParse(sessionLifeTime, out var expDays))
                    expDays = 3;

                if (userSession.TokenCreationTime > DateTime.Now.AddDays(-expDays))
                    return NotFound<ResultDto<Guid>>("Invalid Token. Token not found");
            }

            userSession.TokenCreationTime = DateTime.Now;
            DbLayer.AccountUserSessionsRepository.Update(userSession);
            DbLayer.Save();

            stopWatch.Stop();
            logString += "&updateDB:" + stopWatch.ElapsedMilliseconds;
            return Ok(new ResultDto<Guid> {Data = userSession.AccountUserId, Head = logString});
        }

        /// <summary>
        /// Получение списка идентификаторов пользовательских сессий для указанного пользователя
        /// </summary>
        /// <param name="accountUserId"></param>
        /// <returns>Спикос идентификторов сессий пользователя</returns>
        public ManagerResult<List<Guid>> GetIDs(Guid accountUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, null, () => accountUserId);

                var accSessionList = accountUserSessionDataProvider.GetAllSessionsByUserId(accountUserId);
                return !accSessionList.Any() ? NotFound<List<Guid>>("No sessions were found") : Ok(accSessionList.Select(x => x.Id).ToList());
            }
            catch (Exception ex)
            {
                return PreconditionFailed<List<Guid>>(ex.Message);
            }
        }

        /// <summary>
        /// Возвращает свойства пользовательской сессии
        /// </summary>
        /// <param name="accountUserSessionId"></param>
        /// <returns>Свойства пользовательской сессии.</returns>
        public ManagerResult<Domain.DataModels.AccountUserSession> GetProperties(Guid accountUserSessionId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => AccountIdByAccountUserSession(accountUserSessionId));

                var accSession = accountUserSessionDataProvider.GetSessionsById(accountUserSessionId);
                return accSession == null ? NotFound<Domain.DataModels.AccountUserSession>("Session was not found") : Ok(accSession);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<Domain.DataModels.AccountUserSession>(ex.Message);
            }
        }

        /// <summary>
        /// Возвращает количество пользовательских сессий по указанному пользователю
        /// </summary>
        /// <param name="accountUserId"></param>
        /// <returns>Количество сессий пользователя.</returns>
        public ManagerResult<int> Count(Guid accountUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => AccountIdByAccountUser(accountUserId));
                var accSessionList = accountUserSessionDataProvider.GetAllSessionsByUserId(accountUserId);
                return !accSessionList.Any() ? NotFound<int>("No sessions were found") : Ok(accSessionList.Count);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<int>(ex.Message);
            }
        }

        /// <summary>
        /// Проверить пароль пользователя
        /// </summary>
        /// <param name="loginModel">Модель данных для логина</param>
        /// <returns>OK - если хэш введенного пароля совпадает со значением в базе</returns>
        public ManagerResult ValidateAccountUserPassword(AccountLoginModelDto loginModel)
        {
            const string errorMessage = "Неверный логин и/или пароль.";

            try
            {
                var passwordIsValid = accountUserSessionProvider.ValidateAccountUserPassword(loginModel);
                return passwordIsValid
                    ? Ok()
                    : PreconditionFailed<AuthorizationResultDto>(errorMessage);
            }
            catch (Exception)
            {
                return PreconditionFailed(errorMessage);
            }
        }

        /// <summary>
        /// Возвращает логин пользователя по токену
        /// </summary>
        /// <param name="token">Токен сессии</param>
        /// <returns>Логин пользователя</returns>
        public ManagerResult<string> GetLoginByToken(Guid token)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_LoginByToken);

                var accountUserLogin = accountUserSessionDataProvider.GetLoginByToken(token);
                return string.IsNullOrEmpty(accountUserLogin)
                    ? PreconditionFailed<string>("Не удалось получить сесссию по токену")
                    : Ok(accountUserLogin);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<string>(ex.Message);
            }
        }

        /// <summary>
        /// Возвращает токен авторизации по хешу ResetCode
        /// </summary>
        /// <param name="resetCode">Код сброса</param>
        /// <returns>Токен</returns>
        public ManagerResult<Guid> GetTokenByResetCode(string resetCode)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_LoginByToken);

                if (string.IsNullOrEmpty(resetCode))
                    return PreconditionFailed<Guid>("код восстановление равен нулю");

                var accountUser = DbLayer.AccountUsersRepository.GetAll().FirstOrDefault(u => hashProvider.VerifyHash(u.ResetCode.ToString(), resetCode));

                if (accountUser == null)
                    return NotFound<Guid>("По коду восстановления пользователь не найден ");

                var model = new LoginModelDto
                {
                    AccountUserLogin = accountUser.Login,
                    AccountUserPassword = accountUser.ConfirmationCode,
                    ClientDescription = "Promo",
                    ClientDeviceInfo = null
                };

                return Ok(accountUserSessionProvider.CreateSession(model, accountUser.Id));

            }
            catch (Exception ex)
            {
                return PreconditionFailed<Guid>($"Ошибка входа пользователя по коду сброса {resetCode} {ex.Message}");
            }
        }

        /// <summary>
        /// Возвращает токен пользователя по его логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Токен сессии</returns>
        public ManagerResult<Guid> GetTokenByLogin(string login)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_TokenByLogin);
                logger.Info("GetTokenByLogin START");

                if (string.IsNullOrEmpty(login))
                    return PreconditionFailed<Guid>("Login is empty");

                Domain.DataModels.AccountUser accountUser;
                accountUser = DbLayer.AccountUsersRepository.GetAccountUserByLogin(login);

                if (accountUser == null)
                    return NotFound<Guid>("user not found by login");

                if (!accountUser.Activated)
                    return PreconditionFailed<Guid>("Пользователь заблокирован");

                var newToken = accountUserSessionProvider.CreateSession(new LoginModelDto
                {
                    AccountUserLogin = accountUser.Login,
                    ClientDescription = nameof(GetTokenByLogin),
                    ClientDeviceInfo = $"[{AccessProvider.GetUserAgent()}]",
                }, accountUser.Id);

                return Ok(newToken);
            }
            catch (Exception)
            {
                return PreconditionFailed<Guid>($"Ошибка входа пользователя по логину {login}");
            }
        }

        /// <summary>
        /// Создаёт новую сессию для пользователя с указанным идентификатором.
        /// </summary>
        /// <param name="accountUserId">Идентификатор пользователя</param>
        /// <returns>Идентификатор новой сессии</returns>
        public ManagerResult<Guid> CreateSession(Guid accountUserId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_View, () => AccountIdByAccountUser(accountUserId));

                var token = accountUserSessionProvider.CreateSession(new LoginModelDto
                {
                    ClientDescription = "Сессия для ЗД"
                }, accountUserId);

                return Ok(token);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Подтвердить логин по номеру телефона
        /// </summary>
        /// <param name="confirmCode">Код подтверждения телефонного номера</param>
        /// <returns>Идентифиатор сессии</returns>
        public ManagerResult<Guid> ConfirmLoginByPhoneNumber(string confirmCode)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUserSessions_Auth);
                var user = DbLayer.AccountUsersRepository.FirstOrDefault(x => x.LoginCode.Equals(confirmCode));
                if (user == null)
                    return NotFound<Guid>("User not found");

                var minLife = ConfigurationHelper.GetConfigurationValue<int>("LifetimeLoginCodeMin");

                if (user.DateLoginCode.HasValue && user.DateLoginCode.Value.AddMinutes(minLife) < DateTime.Now)
                    return Conflict<Guid>("time out to confirm code");

                var token = accountUserSessionProvider.CreateSession(new LoginModelDto
                {
                    ClientDescription = "Sauri",
                    ClientDeviceInfo = "",
                    AccountUserLogin = user.Login
                }, user.Id);

                return Ok(token);
            }
            catch (Exception)
            {
                return PreconditionFailed<Guid>($"Ошибка входа пользователя по токену подтверждения {confirmCode}");
            }
        }


        //TO DO переписать
        public ManagerResult<ClaimsPrincipal> CreateClaimsPrincipal(AccountLoginModelDto loginModel)
        {
            Domain.DataModels.AccountUser accountUser = new Domain.DataModels.AccountUser();

            try
            {
                accountUser =
                    DbLayer.AccountUsersRepository.FirstOrDefault(
                        u =>
                            u.Login == loginModel.Username && u.CorpUserSyncStatus != "Deleted" &&
                            u.CorpUserSyncStatus != "SyncDeleted");
            }
            catch (Exception e)
            {
                logger.Trace("Ошибка при логине {0}, inner exception: {1}", e.Message, (e.InnerException == null ? "is null" : e.InnerException.Message));
                return PreconditionFailed<ClaimsPrincipal>("Login error. User not found");
            }
            var userData = accountUser.AuthGuid?.ToString("N") ?? string.Empty;

            var claimsIdentity = new ClaimsIdentity(authenticationType: "Platofrm42OpenId");
            
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, loginModel.Username));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userData));
            
            return Ok(new ClaimsPrincipal(claimsIdentity));
        }

        public async Task<ManagerResult<ClaimsPrincipal>> CreateClaimsPrincipalForApi(AccountSignInModelDto loginModel)
        {
            try
            {
                var accountUser =
                    await DbLayer.AccountUsersRepository.FirstOrDefaultAsync(
                        u =>
                            u.Login == loginModel.Username && u.CorpUserSyncStatus != "Deleted" &&
                            u.CorpUserSyncStatus != "SyncDeleted");

                var claims = new List<Claim>
                {
                    new (ClaimTypes.Name, loginModel.Username),
                    new (ClaimTypes.NameIdentifier, accountUser.Id.ToString("N"))
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                return Ok(new ClaimsPrincipal(claimsIdentity));
            }

            catch (Exception e)
            {
                logger.Trace("Ошибка при логине {0}, inner exception: {1}", e.Message, e.InnerException == null ? "is null" : e.InnerException.Message);

                return PreconditionFailed<ClaimsPrincipal>("Login error. User not found");
            }
        }
    }

}
