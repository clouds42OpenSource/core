﻿using System.Security.Claims;
using Clouds42.AccountUsers.Contracts;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser.AccountUser.Promo;

namespace Clouds42.AccountUsers.AccountUserSession.Managers;

public interface IAccountUserSessionManager
{
    /// <summary>
    /// Получить свойства пользователя для заданного токена сессии.
    /// </summary>
    /// <param name="token">Токен сессии пользователя</param>
    /// <returns>Возвращает свойства пользователя, если они доступны или ошибку в противном случае</returns>
    Task<ManagerResult<Domain.DataModels.AccountUser>> GetUserPropertiesByTokenAsync(Guid token);

    /// <summary>
    /// Проверить корректность токена сессии.
    /// </summary>
    /// <param name="token">Токен сессии</param>
    /// <returns>Состояние корректности токена</returns>
    Task<ManagerResult<bool>> CheckTokenValidityAsync(Guid token);

    /// <summary>
    /// Получить идентификатор пользователя по токену сессии.
    /// </summary>
    /// <param name="token">Токен сесии</param>
    /// <returns>Идентификатор пользователя</returns>
    ManagerResult<ResultDto<Guid>> GetAccountUserIdByToken(Guid token);

    /// <summary>
    /// Получение списка идентификаторов пользовательских сессий для указанного пользователя
    /// </summary>
    /// <param name="accountUserId"></param>
    /// <returns>Спикос идентификторов сессий пользователя</returns>
    ManagerResult<List<Guid>> GetIDs(Guid accountUserId);

    /// <summary>
    /// Возвращает свойства пользовательской сессии
    /// </summary>
    /// <param name="accountUserSessionId"></param>
    /// <returns>Свойства пользовательской сессии.</returns>
    ManagerResult<Domain.DataModels.AccountUserSession> GetProperties(Guid accountUserSessionId);

    /// <summary>
    /// Возвращает количество пользовательских сессий по указанному пользователю
    /// </summary>
    /// <param name="accountUserId"></param>
    /// <returns>Количество сессий пользователя.</returns>
    ManagerResult<int> Count(Guid accountUserId);

    /// <summary>
    /// Проверить пароль пользователя
    /// </summary>
    /// <param name="loginModel">Модель данных для логина</param>
    /// <returns>OK - если хэш введенного пароля совпадает со значением в базе</returns>
    ManagerResult ValidateAccountUserPassword(AccountLoginModelDto loginModel);

    /// <summary>
    /// Возвращает логин пользователя по токену
    /// </summary>
    /// <param name="token">Токен сессии</param>
    /// <returns>Логин пользователя</returns>
    ManagerResult<string> GetLoginByToken(Guid token);

    /// <summary>
    /// Возвращает токен авторизации по хешу ResetCode
    /// </summary>
    /// <param name="resetCode">Код сброса</param>
    /// <returns>Токен</returns>
    ManagerResult<Guid> GetTokenByResetCode(string resetCode);

    /// <summary>
    /// Возвращает токен пользователя по его логину
    /// </summary>
    /// <param name="login">Логин пользователя</param>
    /// <returns>Токен сессии</returns>
    ManagerResult<Guid> GetTokenByLogin(string login);

    /// <summary>
    /// Создаёт новую сессию для пользователя с указанным идентификатором.
    /// </summary>
    /// <param name="accountUserId">Идентификатор пользователя</param>
    /// <returns>Идентификатор новой сессии</returns>
    ManagerResult<Guid> CreateSession(Guid accountUserId);

    /// <summary>
    /// Подтвердить логин по номеру телефона
    /// </summary>
    /// <param name="confirmCode">Код подтверждения телефонного номера</param>
    /// <returns>Идентифиатор сессии</returns>
    ManagerResult<Guid> ConfirmLoginByPhoneNumber(string confirmCode);

    ManagerResult<ClaimsPrincipal> CreateClaimsPrincipal(AccountLoginModelDto loginModel);

    Task<ManagerResult<ClaimsPrincipal>> CreateClaimsPrincipalForApi(AccountSignInModelDto loginModel);
}
