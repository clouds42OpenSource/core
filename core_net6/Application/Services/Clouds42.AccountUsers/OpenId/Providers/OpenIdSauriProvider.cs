﻿using System.Net;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.ExecuteRequestModels;
using Clouds42.Logger;

namespace Clouds42.AccountUsers.OpenId.Providers
{
    /// <inheritdoc />
    /// <summary>
    ///     Sauri OpenId провайдер
    /// </summary>
    public class OpenIdSauriProvider(ExecuteRequestCommand executeRequestCommand, ILogger42 logger)
        : IOpenIdProvider
    {
        /// <inheritdoc />
        public void AddUser(SauriOpenIdControlUserModel model, string locale)
        {
            var fullUrl = string.Format(SauriControlUserUrl, model.AccountUserID, locale);
            logger.Debug($"Адрес {fullUrl} сформированы.");
            InvokeSauriRegSvc(model, fullUrl, HttpMethod.Put);
        }

        public void ChangeUserProperties(SauriOpenIdControlUserModel model, string locale)
        {
            var fullUrl = string.Format(SauriControlUserUrl, model.AccountUserID, locale);
            logger.Debug($"Адрес {fullUrl} сформированы.");
            InvokeSauriRegSvc(model, fullUrl, HttpMethod.Patch);
        }

        /// <inheritdoc />
        public void DeleteUser(SauriOpenIdControlUserModel model)
        {
            var fullUrl = string.Format(SauriControlUserUrl, model.AccountUserID, "ru-ru");
            logger.Debug($"Адрес {fullUrl} сформированы.");
            InvokeSauriRegSvc(model, fullUrl, HttpMethod.Delete);
        }

        /// <summary>
        /// Обобщённый метод вызова API провайдера OpenID
        /// </summary>
        /// <typeparam name="TModel">Тип модели, может быть получен из фактической модели</typeparam>
        /// <param name="model">Модель</param>
        /// <param name="fullUrl">URL метода API</param>
        private void InvokeSauriRegSvc<TModel>(TModel model, string fullUrl, HttpMethod methodType)
        {
            var jsonModel = methodType == HttpMethod.Delete ? null : model.ToJson();
            var response = executeRequestCommand.ExecuteJsonRequest(fullUrl, methodType, jsonModel);
            var responseData = response.Content.ReadAsStringAsync().Result;
            logger.Debug($"Запрос отправлен и вернулся со статусом: {response.StatusCode} и содержимым: {responseData}.");

            HandleExecutionResult(response, fullUrl, methodType, responseData);
            logger.Debug("Результат запроса обработан.");
        }

        /// <summary>
        /// Обработать результат выполнения
        /// </summary>
        /// <param name="response">Ответ</param>
        /// <param name="fullUrl">URL для отправки запроса</param>
        /// <param name="methodType">Вызываемый метод</param>
        /// <param name="responseData"></param>
        private static void HandleExecutionResult(HttpResponseMessage response, string fullUrl, HttpMethod methodType, string responseData)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.Accepted or HttpStatusCode.OK or HttpStatusCode.Created:
                case HttpStatusCode.NotFound when methodType == HttpMethod.Put:
                    return;
                case HttpStatusCode.Conflict:
                {
                    var result = responseData.DeserializeFromJson<ServiceManagerErrorResultDto>();
                    throw new ArgumentException($"При выполнении запроса {methodType} по адресу {fullUrl} OpenID вернул ошибку: {result.Description}");
                }
                case HttpStatusCode.NotFound:
                    throw new NotFoundException("При редактировании, такого пользователя не найдено в OpenID, удалите этого пользователя и создайте заново.");
                default:
                    throw new InvalidOperationException(
                        $"Запрос вернул ошибку, строка запроса {response.RequestMessage?.RequestUri}, код ошибки {response.StatusCode}, адрес {fullUrl}, ошибка : {responseData}");
            }
        }

        /// <summary>
        /// Урл для управления состоянием пользователя
        /// </summary>
        private static string SauriControlUserUrl
            => CloudConfigurationProvider.OpenId.Sauri.GetUrlControlUser();
    }
}
