﻿using Clouds42.AccountUsers.AccountUser;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.AccountUsers.AccountUser.Internal;
using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.AccountUsers.AccountUser.Managers;
using Clouds42.AccountUsers.AccountUser.Providers;
using Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate;
using Clouds42.AccountUsers.AccountUserData.Providers;
using Clouds42.AccountUsers.AccountUserSession.Helpers;
using Clouds42.AccountUsers.AccountUserSession.Managers;
using Clouds42.AccountUsers.AccountUserSession.Providers;
using Clouds42.AccountUsers.ActivateAccountUser.Providers;
using Clouds42.AccountUsers.Contracts;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces;
using Clouds42.AccountUsers.Contracts.AccountUserSession.Interfaces;
using Clouds42.AccountUsers.Contracts.ActivateAccountUser.Interfaces;
using Clouds42.AccountUsers.Contracts.ManageAccountUserLockState.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.AccountUsers.ManageAccountUserLockState.Providers;
using Clouds42.AccountUsers.OpenId.Providers;
using Clouds42.AccountUsers.ServiceManagerConnector.Commands;
using Clouds42.BLL.Common.Access;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountUsers
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddAccountUser(this IServiceCollection services)
        {
            services.AddTransient<IAccountUserManager, AccountUserManager>();
            services.AddTransient<IAccountUserReportDataProvider, AccountUserReportDataProvider>();
            services.AddTransient<IAccountUserProvider, AccountUserProvider>();
            services.AddTransient<IAccountUserResetPasswordNotificationProvider, AccountUserResetPasswordNotificationProvider>();
            services.AddTransient<IAccountUserUpdateProvider, AccountUserUpdateProvider>();
            services.AddTransient<IAccountUserPermissionsProvider, AccountUserPermissionsProvider>();
            services.AddTransient<IAccessMapping, AccessMapping>();
            services.AddTransient<IAccessMappingsProvider, AccessMappingsProvider>();
            services.AddTransient<IAccountUsersDataProvider, AccountUsersDataProvider>();
            services.AddTransient<IAccountUsersCommonDataProvider, AccountUsersCommonDataProvider>();
            services.AddTransient<IAccountUserSessionDataProvider, AccountUserSessionDataProvider>();
            services.AddTransient<IAccountUserSessionLoginProvider, AccountUserSessionLoginProvider>();
            services.AddTransient<IAccountUserSessionProvider, AccountUserSessionProvider>();
            services.AddTransient<IActivateAccountUserProvider, ActivateAccountUserProvider>();
            services.AddTransient<IAccountUserLockStateProvider, AccountUserLockStateProvider>();
            services.AddTransient<IDeleteDelimiterCommand, DeleteDelimeterCommand>();
            services.AddTransient<IGetDatabasesForServicesExtensionCommand, GetDatabasesForServicesExtensionCommand>();
            services.AddTransient<IGetConfigurationDatabaseCommand, GetConfigurationDatabaseCommand>();
            services.AddTransient<IGetServiceInfoCommand, GetServiceInfoCommand>();
            services.AddTransient<IGetServiceExtensionDatabaseStatusCommand, GetServiceExtensionDatabaseStatusCommand>();
            services.AddTransient<IGetServicesExtensionForDatabaseCommand, GetServicesExtensionForDatabaseCommand>();
            services.AddTransient<IDeleteServiceExtensionDatabaseCommand, DeleteServiceExtensionDatabaseCommand>();
            services.AddTransient<IInstallServiceExtensionDatabaseCommand, InstallServiceExtensionDatabaseCommand>();
            services.AddTransient<ICreateAccountUsersSessionsInMsCommand, CreateAccountUsersSessionsInMsCommand>();
            services.AddTransient<IDisableServiceInMarketCommand, DisableServiceInMarketCommand>();
            services.AddTransient<ISetServiceExtensionDatabaseActivationStatusCommand, SetServiceExtensionDatabaseActivationStatusCommand>();
            services.AddTransient<IAccessToDbOnDelimiterCommand, AccessToDbOnDelimiterCommand>();
            services.AddTransient<ICreateNewDelimiterCommand, CreateNewDelimiterCommand>();
            services.AddTransient<IOpenIdProvider, OpenIdSauriProvider>();
            services.AddTransient<IAccountUserDataProvider, AccountUserDataProvider>();
            services.AddTransient<IAccountUserRegistrationNotificationProvider, AccountUserRegistrationNotificationProvider>();
            services.AddTransient<IAccountUserAccessDataProvider, AccountUserAccessDataProvider>();
            services.AddTransient<Contracts.AccountUser.Interfaces.Helpers.IAccountUserProfileHelper, AccountUserProfileHelper>();
            services.AddTransient<IAccountUsersManager, AccountUsersManager>();
            services.AddTransient<IAccountUserDataProvider, AccountUserDataProvider>();
            services.AddTransient<IGetBackupFullPathBySmBackupIdCommand, GetBackupFullPathBySmBackupIdCommand>();
            services.AddTransient<IGetFullPathForDtFileCommand, GetFullPathForDtFileCommand>();
            services.AddTransient<ISynchronizationCorpHelper, SynchronizationCorpHelper>();
            services.AddTransient<IUploadFileCommand, UploadFileCommand>();
            services.AddTransient<ICloseSessionInDbOnDelimitersCommand,CloseSessionInDbOnDelimitersCommand>();
            services.AddTransient<CreateFromDTToZipNewDelimiterCommand>();
            services.AddTransient<PhoneNumberUpdater>();
            services.AddTransient<PasswordUpdater>();
            services.AddTransient<LoginUpdater>();
            services.AddTransient<FioUpdater>();
            services.AddTransient<EmailUpdater>();
            services.AddTransient<AuthTokenUpdater>();
            services.AddTransient<ExecuteRequestCommand>();
            services.AddTransient<AccountHelper>();
            services.AddTransient<RolesUpdater>();
            services.AddTransient<AccountUserRolesProvider>();
            services.AddTransient<AccountUserSessionValidator>();
            services.AddTransient<CreateUserSessionProvider>();
            services.AddTransient<ChangeAccountUserLockStateCommand>();
            services.AddTransient<AccountUsersProfileManager>();
            services.AddTransient<PasswordResetter>();
            services.AddTransient<AccountUsersHelper>();
            services.AddTransient<AccountUserResetPasswordNotificationManager>();
            services.AddTransient<AccountUsersActivateManager>();
            services.AddTransient<AccountUserDataManager>();
            services.AddTransient<AccountUserSessionLoginManager>();
            services.AddTransient<IAccountUserSessionManager, AccountUserSessionManager>();
            services.AddTransient<AccountUserActivateProvider>();
            services.AddTransient<AccountUserDataHelper>();
            services.AddTransient<AccountUserManager>();
            services.AddTransient<AccountUsersSearchManager>();
            services.AddTransient<AccountUsersSearchHelper>();
            services.AddTransient<RenameApplicationCommand>();
            services.AddTransient<CreateFromZipNewDelimiterCommand>();
            services.AddTransient<AuthTokenUpdater>();
            services.AddTransient<EmailUpdater>();
            services.AddTransient<FioUpdater>();
            services.AddTransient<LoginUpdater>();
            services.AddTransient<PasswordUpdater>();
            services.AddTransient<PhoneNumberUpdater>();

            return services;
        }
    }
}
