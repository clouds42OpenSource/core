﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountUser;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    ///     Провайдер управления нотификациями по сбросу пароля для аккаунт юзера
    /// </summary>
    internal class AccountUserResetPasswordNotificationProvider(
        IUnitOfWork dbLayer,
        IHashProvider hashProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IAccountUserResetPasswordNotificationProvider
    {
        private readonly IDictionary<InternalCloudServiceEnum, ExternalClient> _mapInternalCloudServiceToExternalClient = new Dictionary<InternalCloudServiceEnum, ExternalClient>
        {
            {InternalCloudServiceEnum.Delans, ExternalClient.Delans},
            {InternalCloudServiceEnum.Kladovoy, ExternalClient.Kladovoy},
            {InternalCloudServiceEnum.Sauri, ExternalClient.Sauri}
        };

        private readonly Lazy<string> _routeForResetPassword = new(CloudConfigurationProvider.Cp.GetRouteForResetPassword);

        /// <summary>
        ///     Сформировать ResetToken и отправить брендированное письмо
        /// </summary>
        /// <param name="email">Почтовый адрес пользователя</param>
        public void SendPasswordChangeRequest(string email)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Email == email) ??
                              throw new NotFoundException($"Пользователь не найден по email {email}");

            var resetCode = Guid.NewGuid();
            accountUser.ResetCode = resetCode;

            dbLayer.AccountUsersRepository.Update(accountUser);
            dbLayer.Save();

            var token = hashProvider.ComputeHash(resetCode.ToString());

            var cpSiteUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountUser.AccountId);

            letterNotificationProcessor.TryNotify<ResetPasswordLetterNotification, ResetPasswordLetterModelDto>(
                new ResetPasswordLetterModelDto
                {
                    AccountId = accountUser.AccountId,
                    AccountUserId = accountUser.Id,
                    AccountUserLogin = accountUser.Login,
                    ExternalClient = GetExternalClient(accountUser),
                    UrlForResetPassword = new Uri($"{cpSiteUrl}/{_routeForResetPassword.Value}{token}").AbsoluteUri
                });
        }

        /// <summary>
        /// Получить бренд аккаунта
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        /// <returns>Бренд аккаунта</returns>
        private ExternalClient GetExternalClient(Domain.DataModels.AccountUser accountUser)
        {
            if (!accountUser.Account.ReferralAccountID.HasValue)
                return ExternalClient.Promo;

            var billingService =
                dbLayer.BillingServiceRepository.FirstOrDefault(w =>
                    w.AccountOwnerId == accountUser.Account.ReferralAccountID.Value && w.InternalCloudService != null);

            if (billingService?.InternalCloudService == null)
                return ExternalClient.Promo;

            return _mapInternalCloudServiceToExternalClient[billingService.InternalCloudService.Value];
        }
    }
}
