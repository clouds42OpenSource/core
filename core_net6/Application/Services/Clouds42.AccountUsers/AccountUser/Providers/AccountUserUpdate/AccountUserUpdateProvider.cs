﻿using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;
using Clouds42.Logger;
using Microsoft.Extensions.DependencyInjection;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Провайдер для обновления пользователя аккаунта
    /// </summary>
    internal class AccountUserUpdateProvider : IAccountUserUpdateProvider
    {
        private readonly ILogger42 _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly RolesUpdater _rolesUpdater;
        private readonly List<IAccountUserUpdater> _commonUpdaters;

        public AccountUserUpdateProvider(RolesUpdater rolesUpdater, IServiceProvider serviceProvider)
        {
            _rolesUpdater = rolesUpdater;
            _serviceProvider = serviceProvider;
            _commonUpdaters = GetAccountUserUpdaters();
            _logger = serviceProvider.GetRequiredService<ILogger42>();
        }

        /// <summary>
        /// Обновить пользователя за исключением его ролей
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo UpdateExceptRoles(AccountUserDm model, AccountUserDto updates)
        {
            var result = UpdateObjectDataInfo.CreateWithoutChanges();
            _commonUpdaters.ForEach(updater =>
            {
                var intermediateResult = updater.Update(model, updates);
                if (intermediateResult.WereChangesMade)
                    intermediateResult.Messages.ToList().ForEach(_logger.Info);
                result.Absorb(intermediateResult);
            });
            return result;
        }

        /// <summary>
        /// Обновить только роли пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления ролей</returns>
        public UpdateObjectDataInfo UpdateOnlyRoles(AccountUserDm model, AccountUserDto updates)
        {
            var result = _rolesUpdater.Update(model, updates);
            if (result.WereChangesMade)
                result.Messages.ToList().ForEach(_logger.Info);
            return result;
        }

        /// <summary>
        /// Получить список классов для обновления модели пользователя
        /// </summary>
        /// <returns>Список классов для обновления модели пользователя</returns>
        private List<IAccountUserUpdater> GetAccountUserUpdaters()
            =>
            [
                _serviceProvider.GetRequiredService<AuthTokenUpdater>(),
                _serviceProvider.GetRequiredService<EmailUpdater>(),
                _serviceProvider.GetRequiredService<FioUpdater>(),
                _serviceProvider.GetRequiredService<LoginUpdater>(),
                _serviceProvider.GetRequiredService<PasswordUpdater>(),
                _serviceProvider.GetRequiredService<PhoneNumberUpdater>()
            ];
    }
}
