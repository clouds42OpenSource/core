using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления адреса электронной почты пользователя
    /// </summary>
    internal class EmailUpdater : IAccountUserUpdater
    {
        private readonly string _uncheckedEmailStatus = "Unchecked";

        /// <summary>
        /// Обновить адрес электронной почты пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            if (model.Email == updates.Email)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var message = $"Изменена электронная почта с \"{model.Email}\" на \"{updates.Email}\"";
            model.Email = updates.Email;
            model.EmailStatus = _uncheckedEmailStatus;
            return UpdateObjectDataInfo.CreateWithChanges(message);
        }
    }
}
