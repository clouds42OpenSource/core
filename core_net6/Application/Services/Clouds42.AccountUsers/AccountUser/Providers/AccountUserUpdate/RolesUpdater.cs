﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.Extensions;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;
using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления ролей пользователя
    /// </summary>
    internal class RolesUpdater(
        AccountUserRolesProvider accountUserRolesProvider,
        IAccessProvider accessProvider)
        : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить список ролей пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            if (updates.Roles == null)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var oldGroups = model.AccountUserRoles.Select(role => role.AccountUserGroup).ToList();
            var newGroups = updates.Roles;
            if (oldGroups.Count == newGroups.Count && !oldGroups.Except(newGroups).Any())
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var message = "Изменены роли доступа " + 
                          $"с {string.Join(',', oldGroups.Select(x => x.Description()))} " +
                          $"на {string.Join(',', newGroups.Select(x => x.Description()))}";

            accountUserRolesProvider.SetAccountUserRoles(
                model, accessProvider.GetUser().Groups, newGroups);

            return UpdateObjectDataInfo.CreateWithChanges(message);
        }
    }
}
