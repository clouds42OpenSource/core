﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления пароля пользователя
    /// </summary>
    internal class PasswordUpdater(IUnitOfWork dbLayer, IConfiguration configuration) : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить пароль пользователя в базе данных
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            if (string.IsNullOrEmpty(updates.Password))
                return UpdateObjectDataInfo.CreateWithoutChanges();
            dbLayer.AccountUsersRepository.SetUserPassword(model, updates.Password, configuration);
            return UpdateObjectDataInfo.CreateWithChanges("Изменен пароль.");
        }
    }
}
