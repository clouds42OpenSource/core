using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления авторизационного токена пользователя
    /// </summary>
    internal class AuthTokenUpdater : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить авторизационный токен пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            if (model.AuthToken == updates.AuthToken)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            model.AuthToken = updates.AuthToken;
            return UpdateObjectDataInfo.CreateWithChanges();
        }
    }
}
