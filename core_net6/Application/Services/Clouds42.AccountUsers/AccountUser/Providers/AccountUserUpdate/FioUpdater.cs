﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления фамилии, имени и отчества пользователя
    /// </summary>
    internal class FioUpdater : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить фамилию, имя и отчество пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            return UpdateObjectDataInfo.CreateWithoutChanges()
                .Absorb(UpdateField(model.FirstName, updates.FirstName, firstName => model.FirstName = firstName, "Изменено имя"))
                .Absorb(UpdateField(model.MiddleName, updates.MiddleName, middleName => model.MiddleName = middleName, "Изменено отчество"))
                .Absorb(UpdateField(model.LastName, updates.LastName, lastName => model.LastName = lastName, "Изменена фамилия"));
        }

        /// <summary>
        /// Обновить отдельное поле модели
        /// </summary>
        /// <param name="oldValue">Текущее значение</param>
        /// <param name="newValue">Новое значение</param>
        /// <param name="setter">Сеттер нового значения</param>
        /// <param name="prefix">Префикс сообщения об изменении</param>
        /// <returns>Результат обновления</returns>
        private static UpdateObjectDataInfo UpdateField(string oldValue, string newValue, Action<string> setter, string prefix)
        {
            if (oldValue == newValue)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var message = $"{prefix} с \"{oldValue}\" на \"{newValue}\"";
            setter(newValue);
            return UpdateObjectDataInfo.CreateWithChanges(message);
        }
    }
}
