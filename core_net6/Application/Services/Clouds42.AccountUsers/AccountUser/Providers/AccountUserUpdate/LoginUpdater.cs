using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления логина пользователя
    /// </summary>
    internal class LoginUpdater(ICreateDirectoryJobWrapper createDirectoryJobWrapper) : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить логин пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            if (model.Login == updates.Login)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var message = $"Изменен логин с \"{model.Login}\" на \"{updates.Login}\"";
            model.Login = updates.Login;
            model.DateTimeOfLastChangePasswordOrLogin = DateTime.Now;

            var path = AccountUserProfileDirectoryHelper.GetPathForCreate1CeStartDirectory(updates.Login);
            createDirectoryJobWrapper.Start(new CreateDirectoryInfoParams { DirectoryPath = path });

            return UpdateObjectDataInfo.CreateWithChanges(message);
        }
    }
}
