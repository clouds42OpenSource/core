﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.DataModels;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountUser;
using AccountUserDm = Clouds42.Domain.DataModels.AccountUser;

namespace Clouds42.AccountUsers.AccountUser.Providers.AccountUserUpdate
{
    /// <summary>
    /// Класс для обновления номера телефона пользователя
    /// </summary>
    internal class PhoneNumberUpdater : IAccountUserUpdater
    {
        /// <summary>
        /// Обновить номер телефона пользователя
        /// </summary>
        /// <param name="model">Обновляемая модель</param>
        /// <param name="updates">Модель обновлений</param>
        /// <returns>Результат обновления</returns>
        public UpdateObjectDataInfo Update(AccountUserDm model, AccountUserDto updates)
        {
            var newNumber = PhoneHelper.ClearNonDigits(updates.PhoneNumber);

            if (model.PhoneNumber == newNumber)
                return UpdateObjectDataInfo.CreateWithoutChanges();

            var message = $"Изменен телефон с \"{model.PhoneNumber}\" на \"{newNumber}\"";
            model.PhoneNumber = newNumber;
            model.IsPhoneVerified = false;

            return UpdateObjectDataInfo.CreateWithChanges(message);
        }
    }
}
