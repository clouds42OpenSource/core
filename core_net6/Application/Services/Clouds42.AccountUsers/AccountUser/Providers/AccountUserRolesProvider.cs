﻿using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    /// Провайдер управления ролями пользователя
    /// </summary>
    public class AccountUserRolesProvider(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Запись ролей аккаунта исходя из роли Инициатора изменения и текущих ролей
        /// </summary>
        /// <param name="accountUser">Идентификатор аккаунта которому добавлять роли</param>
        /// <param name="initiatorGroups">Идентификатор аккаунта инициатора</param>
        /// <param name="newAccountUserGroupList">Новые роли</param>
        public void SetAccountUserRoles(Domain.DataModels.AccountUser accountUser, List<AccountUserGroup> initiatorGroups, List<AccountUserGroup>? newAccountUserGroupList)
        {
            if (newAccountUserGroupList == null)
            {
                return;
            }

            var availableToEdit = GetAvailableAccountUserRolesList(initiatorGroups);

            newAccountUserGroupList = newAccountUserGroupList.Intersect(availableToEdit).ToList();

            if (availableToEdit.Count == 0) return;
            if (availableToEdit.Count > newAccountUserGroupList.Count)
            {
                var accountUserRolesToDelete = FilterRolesToDeleteList(accountUser.AccountUserRoles, availableToEdit, newAccountUserGroupList);
                RemoveAccountUserRoles(accountUserRolesToDelete);
            }

            if (newAccountUserGroupList.Count <= 0)
            {
                return;
            }

            var accountUserRolesToAdd = FilterRolesToAddList(accountUser.AccountUserRoles, availableToEdit, newAccountUserGroupList);
            AddAccountUserRoles(accountUserRolesToAdd, accountUser.Id);
        }

        /// <summary>
        /// Фильтрация текущего списка групп для удаления
        /// </summary>
        /// <param name="currentAccountUserRoles">Текущий список ролей</param>
        /// <param name="availableToEditGroupList">Список доступный для редактирования</param>
        /// <param name="newAccountUserGroupList">Список новых ролей</param>
        private static List<AccountUserRole> FilterRolesToDeleteList(
            ICollection<AccountUserRole> currentAccountUserRoles,
            IEnumerable<AccountUserGroup> availableToEditGroupList,
            IEnumerable<AccountUserGroup>? newAccountUserGroupList)
        {
            if (!currentAccountUserRoles.Any())
            {
                return currentAccountUserRoles.ToList();
            }

            var availableToDelete = availableToEditGroupList.Except(newAccountUserGroupList).ToList();
            currentAccountUserRoles = currentAccountUserRoles.Where(role => availableToDelete.Any(a => role.AccountUserGroup.Equals(a))).ToList();
            return currentAccountUserRoles.ToList();
        }

        /// <summary>
        /// Фильтрация текущего списка групп для добавления
        /// </summary>
        /// <param name="currentAccountUserRoles">Текущий список ролей</param>
        /// <param name="availableToEditGroupList">Список доступный для редактирования</param>
        /// <param name="newAccountUserGroupList">Список новых ролей</param>
        private static List<AccountUserGroup>? FilterRolesToAddList(
            ICollection<AccountUserRole> currentAccountUserRoles,
            IEnumerable<AccountUserGroup> availableToEditGroupList,
            List<AccountUserGroup>? newAccountUserGroupList)
        {
            currentAccountUserRoles = currentAccountUserRoles.Where(role => availableToEditGroupList.Any(a => a == role.AccountUserGroup)).ToList();

            if (currentAccountUserRoles.Any())
            {
                newAccountUserGroupList = newAccountUserGroupList?.Where(g => currentAccountUserRoles.All(c => c.AccountUserGroup != g)).ToList();
            }
            return newAccountUserGroupList;
        }

        /// <summary>
        /// Удаление ролей аккаунта
        /// </summary>
        /// <param name="accountUserRoles">Список ролей для удаления</param>
        public void RemoveAccountUserRoles(List<AccountUserRole> accountUserRoles)
        {
            if (accountUserRoles.Count == 0) 
                return;

            dbLayer.AccountUserRoleRepository.DeleteRange(accountUserRoles);
            dbLayer.Save();
        }

        /// <summary>
        /// Добавление ролей аккаунту
        /// </summary>
        /// <param name="accountUserGroupList">Список добавляемых ролей</param>
        /// <param name="accountUserId">Идентификатор аккаунта</param>
        public void AddAccountUserRoles(List<AccountUserGroup>? accountUserGroupList, Guid accountUserId)
        {
            if (accountUserGroupList == null || !accountUserGroupList.Any()) return;

            var accountUserRoles = accountUserGroupList
                .Select(userGroup => new AccountUserRole
                {
                    Id = Guid.NewGuid(),
                    AccountUserId = accountUserId,
                    AccountUserGroup = userGroup
                })
                .ToList();

            dbLayer.AccountUserRoleRepository.InsertRange(accountUserRoles);
            dbLayer.Save();
        }

        /// <summary>
        ///  Получение списка доступных ролей для изменения
        /// </summary>
        /// <param name="initiatorGroups">Роли инициатора по изменению ролей</param>
        public static List<AccountUserGroup> GetAvailableAccountUserRolesList(List<AccountUserGroup> initiatorGroups)
            => initiatorGroups.Any(x => x == AccountUserGroup.CloudAdmin)
                ? new AccountUserRolesBuilderFactory(AccountUserGroup.CloudAdmin).Build()
                : initiatorGroups.Any(x => x == AccountUserGroup.Hotline)
                    ? new AccountUserRolesBuilderFactory(AccountUserGroup.Hotline).Build()
                    : [];
        
    }

    /// <summary>
    /// Фабрика для построения списка доступных для изменения групп аккаунном с определенной ролью
    /// </summary>
    class AccountUserRolesBuilderFactory
    {
        private readonly AccountUserGroup _accountUserGroup;
        private readonly Dictionary<AccountUserGroup, Func<List<AccountUserGroup>>> _builders;

        /// <summary>
        /// Фабрика для построения списка доступных для изменения групп аккаунном с определенной ролью
        /// </summary>
        internal AccountUserRolesBuilderFactory(AccountUserGroup accountUserGroup)
        {
            _accountUserGroup = accountUserGroup;
            _builders = new Dictionary<AccountUserGroup, Func<List<AccountUserGroup>>>
            {
                {AccountUserGroup.CloudAdmin, CloudAdminRolesBuilder},
                {AccountUserGroup.Hotline, HotlineRolesBuilder}
            };
        }

        /// <summary>
        /// Получение списка доступных групп для изменения
        /// </summary>
        public List<AccountUserGroup> Build()
        {
            if (!_builders.ContainsKey(_accountUserGroup))
                throw new InvalidOperationException($"Не определена функция для постройки списка ролей для группы {_accountUserGroup.ToString()}");

            return _builders[_accountUserGroup]();
        }

        /// <summary>
        /// Формирование списка ролей для Администратора облака
        /// </summary>
        private List<AccountUserGroup> CloudAdminRolesBuilder()
        {
            var accountUserRolesList = new List<AccountUserGroup>
            {
                AccountUserGroup.AccountUser,
                AccountUserGroup.AccountAdmin,
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.Cloud42Service,
                AccountUserGroup.Hotline,
                AccountUserGroup.CloudSE,
                AccountUserGroup.ProcOperator,
                AccountUserGroup.ExternalService,
                AccountUserGroup.ArticleEditor
            };

            return accountUserRolesList;
        }

        /// <summary>
        /// Формирование списка ролей для Хотлайна
        /// </summary>
        private List<AccountUserGroup> HotlineRolesBuilder()
        {
            var accountUserRolesList = new List<AccountUserGroup>
            {
                AccountUserGroup.AccountUser,
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.AccountAdmin
            };

            return accountUserRolesList;
        }
    }
}
