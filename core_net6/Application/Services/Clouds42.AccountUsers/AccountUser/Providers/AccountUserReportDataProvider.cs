﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    /// Провайдер для работы с данными пользователя для отчета
    /// </summary>
    internal class AccountUserReportDataProvider(IUnitOfWork dbLayer) : IAccountUserReportDataProvider
    {
        /// <summary>
        /// Получить список моделей данных по пользователям для отчета
        /// </summary>
        /// <returns>Список моделей данных по пользователям для отчета</returns>
        public List<AccountUserReportDataDto> GetAccountUserReportDataDcs() =>
            dbLayer.ExecuteSqlQueryList<AccountUserReportDataDto>(SqlQueries.Reports.AccountUser.GetData);

    }
}
