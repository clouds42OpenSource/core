﻿using Microsoft.EntityFrameworkCore;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.Filters;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Helpers;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    /// Провайдер для работы с пользователем
    /// </summary>
    internal class AccountUserProvider(
        IUnitOfWork dbLayer,
        DesEncryptionProvider desEncryptionProvider,
        AesEncryptionProvider aesEncryptionProvider)
        : IAccountUserProvider
    {
        /// <summary>
        /// Проверить текущий пароль
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="password">Текущий пароль</param>
        /// <returns>Результат проверки</returns>
        public bool CheckCurrentPassword(Guid userId, string password)
        {
            var accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == userId);

            if (accountUser == null || string.IsNullOrEmpty(accountUser.Password) || string.IsNullOrEmpty(password))
                return false;

            return TryValidatePasswordByHash(password, accountUser.PasswordHash, aesEncryptionProvider) ||
                   TryValidatePasswordByHash(password, accountUser.Password, desEncryptionProvider);
        }


        /// <summary>
        /// Запросить пагинированный список пользователей аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали запроса</param>        
        /// <returns>Пагинированный список пользователей</returns>
        public SelectDataResultCommonDto<AccountUserDto> GetPaginatedUsersForAccount(Guid accountId, PagniationRequestDto<AccountUsersFilterDto> request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            //Pre-filter by accountId
            var query = dbLayer.AccountUsersRepository
                .GetValidUsersQuery()
                .Where(x => x.AccountId == accountId);

            var filter = request.Filter;
            if (!string.IsNullOrEmpty(filter))
            {
                var searchAsPhone = PhoneHelper.ClearNonDigits(filter);
                query = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(a => (
                        (a.AccountId == accountId)
                        && ((a.Login != null && a.Login.ToLower().Contains(filter.ToLower())) ||
                            (a.FirstName != null && a.FirstName.ToLower().Contains(filter.ToLower())) ||
                            (a.LastName != null && a.LastName.ToLower().Contains(filter.ToLower())) ||
                            (a.MiddleName != null && a.MiddleName.ToLower().Contains(filter.ToLower())) ||
                            (a.FullName != null && a.FullName.ToLower().Contains(filter.ToLower())) ||
                            (a.Email != null && a.Email.ToLower().Contains(filter.ToLower()))
                            || ((!string.IsNullOrEmpty(searchAsPhone)) && (searchAsPhone.Length >= 5) &&
                                (a.PhoneNumber != null) && (a.PhoneNumber).Contains(searchAsPhone))
                        )
                    ))
                    .Include(i => i.Account)
                    .Include(i => i.AccountUserRoles);

            }

            var result = (IPagedList<Domain.DataModels.AccountUser>)new PagedList<Domain.DataModels.AccountUser>(query, request.PageNumber, request.PageSize);

            if (request.SortingData?.FieldName?.Equals(nameof(Domain.DataModels.AccountUser.FullName)) ?? false)
            {
                result = (request.SortingData.SortKind == SortType.Asc
                        ? query.OrderBy(o => (o.LastName + "" + o.FirstName + "" + o.MiddleName).Trim())
                        : query.OrderByDescending(o => (o.LastName + "" + o.FirstName + "" + o.MiddleName).Trim()))
                    .ToPagedList(request.PageNumber, request.PageSize);
            }
            else
            {
                result = query
                    .MakeSortOrDefault(request.SortingData, new SortingDataDto { FieldName = nameof(Domain.DataModels.AccountUser.Login) })
                    .ToPagedList(request.PageNumber, request.PageSize);
            }
            



            return new SelectDataResultCommonDto<AccountUserDto>
            {
                Pagination = new PaginationBaseDto(result.PageNumber, result.TotalItemCount, result.PageSize),
                Records = result.Select(x => MapToAccountUserDc(x)).ToArray()
            };

        }

        /// <summary>
        /// Создать DC модель юзера сущности юзера
        /// </summary>
        /// <param name="user">Исходная сущность юзера</param>
        /// <returns>DC модель юзера</returns>
        private static AccountUserDto MapToAccountUserDc(Domain.DataModels.AccountUser user)
        {
            return new AccountUserDto
            {
                Activated = user.Activated,
                AccountId = user.AccountId,
                CreatedInAd = user.CreatedInAd,
                CreationDate = user.CreationDate,
                Email = user.Email,
                MiddleName = user.MiddleName,
                FirstName = user.FirstName,
                FullName = user.FullName,
                Id = user.Id,
                LastName = user.LastName,
                Login = user.Login,
                PhoneNumber = user.PhoneNumber,
                Unsubscribed = user.Unsubscribed,
                AccountRoles = user.AccountUserRoles
                    .Select(x => x.AccountUserGroup)
                    .Where(x => !x.IsHidden())
                    .Distinct()
                    .Select(x => x.ToString())
                    .ToList()
            };
        }

        /// <summary>
        /// Попытаться провалидировать пароль по хешу пароля
        /// </summary>
        /// <param name="password">Пароль</param>
        /// <param name="currentPasswordHash">Текущий хеш пароля</param>
        /// <param name="encryptionProvider">Провайдер шифрования</param>
        /// <returns>Результат проверки</returns>
        private static bool TryValidatePasswordByHash(string password, string currentPasswordHash, IEncryptionProvider encryptionProvider)
        {
            if (string.IsNullOrEmpty(currentPasswordHash))
                return false;

            try
            {
                return encryptionProvider.Encrypt(password) == currentPasswordHash;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Получить первого админ-аккаунта
        /// </summary>
        /// <param name="accountId">Для какого акконта получить админа</param>
        /// <returns>Если найдент, то админ аакаунта иначе null</returns>
        public AccountUserDto? GetFirstAccountAdminFor(Guid accountId)
        {
            if (accountId.Equals(Guid.Empty))
            {
                return null;
            }

            var foundAccountAdmin = dbLayer.AccountUsersRepository.FirstOrDefault(user => user.AccountId == accountId && user.AccountUserRoles.Any(
                item => item.AccountUserGroup == AccountUserGroup.AccountAdmin));

            return foundAccountAdmin == null
                ? null
                : MapToAccountUserDc(foundAccountAdmin);
        }
    }
}
