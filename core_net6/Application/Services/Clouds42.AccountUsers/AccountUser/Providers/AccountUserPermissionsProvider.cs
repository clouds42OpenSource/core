﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    /// Класс для работы с разрешениями пользователя
    /// </summary>
    public class AccountUserPermissionsProvider : IAccountUserPermissionsProvider
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IAccessMappingsProvider _accessMappingsProvider;
        readonly IDictionary<AccessLevel, Func<Guid, Guid?, Guid?, bool>> _accessLevelMap;

        public AccountUserPermissionsProvider(IUnitOfWork dbLayer,
            IAccessMappingsProvider accessMappingProvider)
        {
            _accessMappingsProvider = accessMappingProvider ?? throw new ArgumentNullException(nameof(accessMappingProvider));
            _unitOfWork = dbLayer ?? throw new ArgumentNullException(nameof(dbLayer));
            _accessLevelMap = CreateAccessLevelMap();
        }

        private IDictionary<AccessLevel, Func<Guid, Guid?, Guid?, bool>> CreateAccessLevelMap() =>
            new Dictionary<AccessLevel, Func<Guid, Guid?, Guid?, bool>>
            {
                {AccessLevel.Allow, (userId,targetAccountId,targetUserId) => true },
                {AccessLevel.HimSelfUser, (userId,targetAccountId,targetUserId) => userId == targetUserId },
                {AccessLevel.HimSelfAccount, (userId,targetAccountId,targetUserId) => CheckSelfAccountAccessCondition(userId,targetAccountId)  },
                {AccessLevel.ControlledAccounts, (userId,targetAccountId,targetUserId) => CheckControlledAccountAccessCondition(userId,targetAccountId,targetUserId) }
            };

        /// <summary>
        /// Получить список разрешений юзера для контекстных значений
        /// </summary>
        /// <param name="userId">ID юзера разрешения которого запрашиваются</param>
        /// <param name="targetAccountId">ID аккаунта для операций над которым запрашиваются разрешения</param>
        /// <param name="targetUserId">ID юзера для операций над которым запрашиваются разрешения</param>
        /// <param name="actionsFilter">ID фильтр запрашиваемых разрешений. Если null - будут возвращены все доступные разрешения</param>
        /// <returns>Список разрешений пользователя</returns>
        public List<ObjectAction> GetUserPermissionsForTargets(Guid userId,
            Guid? targetAccountId,
            Guid? targetUserId,
            IEnumerable<ObjectAction> actionsFilter = null)
        {
            var accessModels = GetUserAccessModels(userId, actionsFilter?.ToList());
            var checkAccessFunctions = _accessLevelMap.ToDictionary(
                x => x.Key,
                x => GetSafeLazy(() => x.Value(userId, targetAccountId, targetUserId)));

            var result = new HashSet<ObjectAction>();
            foreach (var accessMapping in accessModels)
            {
                if (result.Contains(accessMapping.Action))
                    continue;
                if (checkAccessFunctions.TryGetValue(accessMapping.Level, out var hasAccessLevelLazy) &&
                    hasAccessLevelLazy.Value)
                    result.Add(accessMapping.Action);
            }
            return result.ToList();
        }


        /// <summary>
        /// Запрос разрешений, доступных юзеру
        /// </summary>
        /// <param name="userId">ID юзера</param>
        /// <param name="actionsFilter">Фильтр разрешений</param>
        /// <returns>Лист разрешений доступных юзеру</returns>
        private List<AccessModel> GetUserAccessModels(Guid userId, List<ObjectAction> actionsFilter = null)
        {
            if (actionsFilter is { Count: 0 })
                return [];
            var groups = _unitOfWork.AccountUserRoleRepository
                .GetUserRoles(userId)
                .Select(r => r.AccountUserGroup)
                .ToList();
            if (groups.Count == 0)
                return [];
            return (actionsFilter == null ?
                _accessMappingsProvider.GetAccess(groups) :
                _accessMappingsProvider.GetAccess(groups, actionsFilter))
                .ToList();
        }

        /// <summary>
        /// Условие предоставления доступа на уровне SelfAccount
        /// </summary>
        /// <param name="userId">ID юзера для которого выполняется проверка</param>
        /// <param name="targetAccountId">ID аккаунта для которого проверяется условие</param>
        /// <returns>Результат проверки</returns>
        private bool CheckSelfAccountAccessCondition(Guid userId, Guid? targetAccountId) =>
            targetAccountId.HasValue && targetAccountId == GetAccountIdByUserId(userId);


        /// <summary>
        /// Проверка разрешать ли юзеру доступ к аккаунту на уровне ControlledAccount
        /// </summary>
        /// <param name="userId">ID юзера для которого производится проверка</param>
        /// <param name="targetAccountId">ID аккаунта к которому проверяется доступ</param>
        /// <param name="targetUserId">ID юзера к аккаунту которго проверяется доступ</param>
        /// <remarks>Для успешной проверки должен быть предоствлен 
        /// либо <paramref name="targetUserId"/>, /// либо <paramref name="targetAccountId"/>.
        /// Если предоставлены оба параметра, то проверка будет успешна если доступ есть хотя бы на основании одного из них
        /// </remarks>
        /// <returns>Результат проверки</returns>
        private bool CheckControlledAccountAccessCondition(Guid userId, Guid? targetAccountId = null, Guid? targetUserId = null)
        {
            if (!targetAccountId.HasValue && !targetUserId.HasValue)
                return false;

            var accountId = GetAccountIdByUserId(userId);
            if (accountId == null)
                return false;

            Guid? targetAccountIdByUserId = targetUserId.HasValue ? GetAccountIdByUserId(targetUserId.Value) : null;

            return targetAccountId == accountId ||
                   targetAccountIdByUserId == accountId ||
                   IsUserSaleManagerForAccount(userId, targetAccountId) ||
                   IsUserSaleManagerForAccount(userId, targetAccountIdByUserId);
        }

        private Guid? GetAccountIdByUserId(Guid userId) => _unitOfWork.AccountUsersRepository
            .GetAccountUser(userId)?.AccountId;

        private bool IsUserSaleManagerForAccount(Guid userId, Guid? accountId) =>
            accountId.HasValue &&
            _unitOfWork.AccountSaleManagerRepository.GetForAccount(accountId.Value)?.SaleManagerId == userId;

        private static Lazy<bool> GetSafeLazy(Func<bool> func) => new(
            () => { try { return func.Invoke(); } catch (Exception) { return false; } });
    }
}