﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common.Access;
using Clouds42.Domain.Access;

namespace Clouds42.AccountUsers.AccountUser.Providers
{
    /// <summary>
    /// Класс для работы с маппингом разрешений к ролям
    /// </summary>
    public class AccessMappingsProvider(IAccessMapping accessMapping) : IAccessMappingsProvider
    {

        /// <summary>
        /// Получает доступы для действия <see cref="objectAction"/> и переданных групп <see cref="groups"/>
        /// </summary>
        /// <param name="objectAction">действие над которым получить доступ</param>
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        public IEnumerable<AccessModel> GetAccess(ObjectAction objectAction, IEnumerable<AccountUserGroup> groups)
            => accessMapping.GetAccess(objectAction, groups?.ToList());


        /// <summary>
        /// Получает доступы для действий <see cref="objectActions"/> и переданных групп <see cref="groups"/>
        /// </summary>        
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        /// /// <param name="objectActions">действия для которых нужно получить доступ</param>
        public IEnumerable<AccessModel> GetAccess(
            IEnumerable<AccountUserGroup> groups,
            IEnumerable<ObjectAction> objectActions) => accessMapping.GetAccess(groups?.ToList(), objectActions?.ToList());


        /// <summary>
        /// Получает доступы для действий по переданным группам <see cref="groups"/>
        /// </summary>                
        /// <param name="groups">Группы которые проверяются при получении доступа</param>
        public IEnumerable<AccessModel> GetAccess(IEnumerable<AccountUserGroup> groups) => accessMapping.GetAccess(groups?.ToList());

    }
}
