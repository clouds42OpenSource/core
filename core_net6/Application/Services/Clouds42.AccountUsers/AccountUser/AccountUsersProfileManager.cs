﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.AccountUsers.AccountUser.Internal;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.CloudServices.Contracts;
using Clouds42.Common.Constants;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.Cloud42Services.Rent1C;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.SmsClient;
using Clouds42.StateMachine.Contracts.AccountUserProcessFlows;
using Clouds42.Validators.Account.Helpers;
using Clouds42.Validators.Account.Validators;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Менеджер профилей пользователей
    /// </summary>
    public class AccountUsersProfileManager(
        IRent1CConfigurationAccessProvider rent1CConfigurationAccessProvider,
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountUserAccessDataProvider accountUserAccessDataProvider,
        PasswordResetter passwordResetter,
        IHandlerException handlerException,
        CreateNewUserAvailableLoginValidator createNewUserAvailableLoginValidator,
        IUpdateAccountUserProcessFlow updateAccountUserProcessFlow,
        IDeleteAccountUserProcessFlow deleteAccountUserProcessFlow,
        AccountUsersHelper accountUsersHelper,
        AccountUserDataValidator accountUserDataValidator,
        ISmsSenderManager smsSenderManager,
        ILogger42 logger,
        IAccountFolderHelper accountFolderHelper)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Установить пароль для пользователя
        /// </summary>
        /// <param name="pswChangingModel">Модель параметров для смены пароля</param>
        /// <returns>Результат выполнения</returns>
        public ManagerResult SetPassword(PasswordChangingModelDto pswChangingModel)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => pswChangingModel.AccountUserID);

            if (!pswChangingModel.AccountUserID.HasValue)
                return Ok();

            var accountUser =
                DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == pswChangingModel.AccountUserID);

            if (accountUser == null)
                return NotFound($"Пользователь с Id={pswChangingModel.AccountUserID} не существует");

            try
            {
                accountUserAccessDataProvider.SetPassword(pswChangingModel.AccountUserID.Value, pswChangingModel.NewPassword,
                    pswChangingModel.OldPassword);
                return Ok();
            }
            catch (KeyNotFoundException kNFex)
            {
                return NotFound(kNFex.Message);
            }
            catch (PreconditionException pex)
            {
                return PreconditionFailed(pex.Message);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки пароля для пользователя]");
                return Forbidden($"Method execution failed, {ex.Message}");
            }
        }
        
        /// <summary>
        /// Установить логин для пользователя
        /// </summary>
        /// <param name="model">Модель параметров для установки логина</param>
        /// <returns>Реузльтат выполнения</returns>
        /// <remarks>Обновление Scram-записей в этом случае не работает, поскольку мы не можем вытащить пароль.</remarks>
        public ManagerResult SetLogin(AccountUsersLoginModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);

            var loginValidation = new LoginValidation(model.Login);
            if (!loginValidation.LoginSymbolsIsValid(out var validationMessage)) return PreconditionFailed(validationMessage);
            if (!loginValidation.LoginLengthIsValid()) return PreconditionFailed($"Допустимое количество символов: {AccountUserConstants.MinLoginLength}–{AccountUserConstants.MaxLoginLength}");
            if (!createNewUserAvailableLoginValidator.LoginIsAvailable(model.Login)) return Conflict("Пользователь с таким логином уже существует");
            logger.Trace("is login: {0}", !createNewUserAvailableLoginValidator.LoginIsAvailable(model.Login));

            if (!model.AccountUserID.HasValue)
                return Ok();

            var user = DbLayer.AccountUsersRepository.GetAccountUser(model.AccountUserID.Value);
            if (user == null)
                return NotFound($"Account user with Id: {model.AccountUserID.Value} not found");

            try
            {
                accountUserAccessDataProvider.SetLogin(model.AccountUserID.Value, model.Login);

                return Ok();
            }
            catch (KeyNotFoundException kNFex)
            {
                return NotFound(kNFex.Message);
            }
            catch (PreconditionException pex)
            {
                return PreconditionFailed(pex.Message);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки логина для пользователя]");
                return Forbidden("Method execution failed");
            }
        }

        /// <summary>
        /// Установить новый пароль для пользователя
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        public ManagerResult SetNewUserPassword(string login, string password)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_SetNewUserPassword, null, () =>
                {
                    var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(a => a.Login == login);
                    return accountUser?.Id;
                });

                passwordResetter.SetNewUserPassword(login, password);

                return Ok();
            }
            catch (Exception ex)
            {
                return PreconditionFailed($"Ошибка смены пароля: {ex.Message}");
            }
            
        }

        /// <summary>
        ///     Метод смены пароля с помощью зашифрованного ключа
        /// </summary>
        /// <param name="model">Входящие параметры</param>
        public ManagerResult SetPasswordWithResetToken(ChangePasswordWithTokenModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit);

            try
            {
                passwordResetter.ChangePasswordWithResetToken(model.ResetToken, model.NewPassword);

                return Ok();
            }
            catch (KeyNotFoundException kNFex)
            {
                return NotFound(kNFex.Message);
            }
            catch (PreconditionException pex)
            {
                return PreconditionFailed(pex.Message);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка смены пороля]");
                return Forbidden("Method execution failed");
            }
        }

        /// <summary>
        ///     Добавить пользователя AccountUser в аккаунт Account.
        /// </summary>
        /// <param name="userInfo">Данные добавляемого пользователя.</param>
        public async Task<ManagerResult<Guid>> AddToAccount(AccountUserRegistrationModelDto userInfo)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_AddToAccount, () => userInfo.AccountId);
            logger.Info("AddToAccount START");

            if (userInfo == null)
                return PreconditionFailed<Guid>("accountUserModel is null");

            logger.Info("AccountIdString: {0}", userInfo.AccountId);

            accountUsersHelper.FillEmptyPassword(userInfo);

            logger.Info("Call validate");
            var validationResult = await accountUserDataValidator.ValidateAccountUserDataAsync(userInfo);

            logger.Info($"errorlist length: {validationResult.Length}");

            if (validationResult.Any())
            {
                var validationErrors = string.Join(", ", validationResult);
                logger.Info("Validation errors: " + validationErrors);
                return PreconditionFailed<Guid>(validationErrors);
            }

            try
            {
                logger.Info("begin AddUserToAccount");

                var account = await DbLayer.AccountsRepository
                    .AsQueryableNoTracking()
                    .Include(x => x.AccountConfiguration)
                    .ThenInclude(x => x.Segment)
                    .ThenInclude(x => x.CloudServicesFileStorageServer)
                    .FirstOrDefaultAsync(x => x.Id == userInfo.AccountId);

                var clientFilesFolder =
                    accountFolderHelper.GetAccountClientFileStoragePath(account.AccountConfiguration.Segment, account);

                var userModel = new AccountUserDto
                {
                    Login = userInfo.Login,
                    Password = userInfo.Password,
                    FirstName = userInfo.FirstName,
                    MiddleName = userInfo.MiddleName,
                    LastName = userInfo.LastName,
                    Email = userInfo.Email,
                    PhoneNumber = userInfo.FullPhoneNumber,
                    AccountId = userInfo.AccountId,
                    ResetCode = Guid.NewGuid(),
                    ClientFilesPath = clientFilesFolder
                };

                var newAccountUser = accountUsersHelper.CreateUser(userModel);
                logger.Info("Пользователь {0} успешно зарегистрирован", userInfo.Login);

                smsSenderManager.NotifyAccountUser(newAccountUser, userInfo, userInfo.Password);

                logger.Info("User id: " + newAccountUser);
                return Ok(newAccountUser.Id);
            }
            catch (Exception ex)
            {
                logger.Warn($"Error adding user: {userInfo.Login} to company :: {ex.GetFullInfo()}");
                return Forbidden<Guid>(ex.Message);
            }
        }

        /// <summary>
        /// Добавить пользователя к аккаунту
        /// </summary>
        /// <param name="accountUserDc">Модель пользователя</param>
        /// <returns>ID созданного пользователя</returns>
        public ManagerResult<Guid> AddToAccount(AccountUserDto accountUserDc)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_AddToAccount, () => accountUserDc.AccountId);

            var accountUser = accountUsersHelper.CreateUser(accountUserDc);
            return Ok(accountUser.Id);
        }

        /// <summary>
        /// Изменить пользователя облака.
        /// </summary>
        /// <param name="accountUser">Измененная модель пользователя облака.</param>
        public ManagerResult Update(AccountUserDto accountUser)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => accountUser.Id);

            try
            {
                if (!accountUser.Id.HasValue) throw new InvalidOperationException($"Не найден пользователь с логином {accountUser.Login}");

                updateAccountUserProcessFlow.Run(accountUser);

                return Ok();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка редактирования пользователя] {accountUser.Id} {accountUser.Login}");

                LogEvent(() => accountUser.AccountId, LogActions.EditUserCard, $"Ошибка редактирования пользователя \"{accountUser.Login}\". Причина: {ex.Message}");
				
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удаление аккаунта по модели идентификатора
        /// </summary>
        /// <param name="model">Модель идентификатора аккаунта</param>
        public ManagerResult Delete(AccountUserIdModel model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Delete, null, () => model.AccountUserID);

            var user = DbLayer.AccountUsersRepository
                .AsQueryable()
                .FirstOrDefault(x => x.Id == model.AccountUserID);

            if (user == null)
                return NotFound($"Account user with id: {model.AccountUserID} not found");

            if (user.Removed.HasValue && user.Removed.Value)
                return Conflict("User was already deleted!");

            var message = $"Пользователь \"{user.Login}\" удален";

            //TODO: этот вызов является дубликатом, доступы удаляются в DisableRent1CForAccountUserAction
            //ConfigureServices(user);

            var result = deleteAccountUserProcessFlow.Run(model);
            if (!result.Finish)
                throw new InvalidOperationException(result.Message);

            LogEvent(() => user.AccountId, LogActions.RemoveUser, message);

            return Ok();
        }

        /// <summary>
        /// Удаление аккаунта по идентификатору
        /// </summary>
        /// <param name="accountUserId">Идентификатор аккаунта</param>
        /// <returns>Возвращает истину при успешном удалении</returns>
        public bool Delete(Guid accountUserId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Delete, null, () => accountUserId);

            var user = DbLayer.AccountUsersRepository.Where(u => u.Id == accountUserId).SingleOrDefault();
            if (user == null)
            {
                logger.Debug($"Пользователь {accountUserId} для удаления не найден");
                throw new NotFoundException("Account user not found!");
            }

            var accountUserIdModel = new AccountUserIdModel
            {
                AccountUserID = user.Id
            };

            var message = $"Пользователь \"{user.Login}\" удален";
            var result = deleteAccountUserProcessFlow.Run(accountUserIdModel);
            if (!result.Finish)
                throw new InvalidOperationException(result.Message);
            LogEvent(() => user.AccountId, LogActions.RemoveUser, message);

            return true;
        }
    }
}
