﻿using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUser.Internal
{
    /// <summary>
    ///     Класс для генерации/установки паролей
    /// </summary>
    public class PasswordResetter(
        IUnitOfWork unitOfWork,
        IOpenIdProvider openIdProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        AesEncryptionProvider aesEncryptionProvider,
        ILogger42 logger,
        IConfiguration configuration)
    {
        private readonly IEncryptionProvider _encryptionProvider = new DesEncryptionProvider(configuration);


        /// <summary>
        ///     Установить новый пароль для пользователя
        /// </summary>   
        public void SetNewUserPassword(string login, string password)
        {
            var user = unitOfWork.AccountUsersRepository.FirstOrDefault(u => u.Login == login) ?? throw new KeyNotFoundException($"Пользователь не найден по логину {login}");
            if (!string.IsNullOrEmpty(password))
            {
                var passwordValidator = new PasswordValidation(password);
                var passwordValidationResult = passwordValidator.PasswordIsValid();

                if (!passwordValidationResult.DataIsValid)
                    throw new KeyNotFoundException(string.Join(", ", passwordValidationResult.ValidationMessages));
            }
            var locale = user.Account.AccountConfiguration.Locale.Name;
            var newPasswordHash = _encryptionProvider.Encrypt(password);

            openIdProvider.ChangeUserProperties(new SauriOpenIdControlUserModel
            {
                AccountUserID = user.Id,
                AccountID = user.Account.Id,
                Login = login,
                Password = password
            }, locale);

            logger.Trace("Для пользователя Id={0} успешно изменён пароль в провайдере OpenID на {1}", user.Id, password);

            activeDirectoryTaskProcessor.RunTask(provider => provider.ChangePassword(user.Id, user.Login, password));

            user.Password = newPasswordHash;
            user.PasswordHash = aesEncryptionProvider.Encrypt(password);
            user.DateTimeOfLastChangePasswordOrLogin = DateTime.Now;

            user.ResetCode = null;

            unitOfWork.AccountUsersRepository.Update(user);
            unitOfWork.Save();
        }

        /// <summary>
        ///     Метод смены пароля на новый, с помощью зашифрованного ключа
        /// </summary>
        public void ChangePasswordWithResetToken(string resetToken, string newPassword)
        {
            if (string.IsNullOrWhiteSpace(resetToken))
                throw new PreconditionException("Токен не валиден. Токен пуст либо содержит не верные символы");

            var accountUser = unitOfWork.AccountUsersRepository.GetAccountUserByResetToken(resetToken) ?? throw new KeyNotFoundException("Пользователь по токену не найден!");
            logger.Trace($"Начинаем процедуру смены пароля для пользователя {accountUser.Id}");

            var passwordValidator = new PasswordValidation(newPassword);
            var passwordValidationResult = passwordValidator.PasswordIsValid();
            if (!passwordValidationResult.DataIsValid)
                throw new PreconditionException(string.Join("", passwordValidationResult.ValidationMessages));

            var locale = accountUser.Account.AccountConfiguration.Locale.Name;

            openIdProvider.ChangeUserProperties(new SauriOpenIdControlUserModel
            {
                AccountUserID = accountUser.Id,
                AccountID = accountUser.Account.Id,
                Password = newPassword
            }, locale);

            activeDirectoryTaskProcessor.RunTask(provider => provider.ChangePassword(accountUser.Id, accountUser.Login, newPassword));

            accountUser.Password = _encryptionProvider.Encrypt(newPassword);
            accountUser.PasswordHash = aesEncryptionProvider.Encrypt(newPassword);
            accountUser.DateTimeOfLastChangePasswordOrLogin = DateTime.Now;
            accountUser.ResetCode = null;

            unitOfWork.AccountUsersRepository.InsertOrUpdateAccountUser(accountUser);
            unitOfWork.Save();

            logger.Trace($"Успешно сменили пароль для пользователя {accountUser.Id}");
        }
    }
}
