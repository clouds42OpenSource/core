﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Internal.Providers
{
    /// <summary>
    /// Провадйер управления подключениями пользователя.
    /// </summary>
    internal class AccountUserConnectionsProvider(IUnitOfWork dbLayer, IAccessProvider accessProvider)
    {
        /// <summary>
        /// Получение списка пользователей из базы для текущего аккаунта
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<AccountUserDto> GetAccountUsersListByAccount(Guid accountId)
        {
            var currentUser = accessProvider.GetUser();

            var accountUsers = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                .Where(au => au.AccountId == accountId)
                .Include(i => i.Account)
                .Include(i => i.AccountUserRoles)
                .ToList();

            var res = new List<AccountUserDto>();
            var userInitator = dbLayer.AccountUsersRepository.GetAccountUserByLogin(currentUser.Name);
            if (currentUser.Groups.Any(g => g == AccountUserGroup.AccountUser) && userInitator.AccountId == accountId)
            {
                var user = accountUsers.FirstOrDefault(x => x.Login == currentUser.Name);
                res.Add(user.MapToAccountUserDc());
                return res;
            }

            res.AddRange(accountUsers.Select(accountUser => accountUser.MapToAccountUserDc()));
            return res;
        }
    }
}
