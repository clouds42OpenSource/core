﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Internal.Providers
{
    /// <summary>
    /// Провайдер для работы с данными пользователя
    /// </summary>
    public class AccountUserDataProvider(IUnitOfWork dbLayer) : IAccountUserDataProvider
    {
        /// <summary>
        /// Получить хэш пароля по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Хэш пароля пользователя</returns>
        public string GetPasswordHashByLogin(string login) =>
            dbLayer.AccountUsersRepository.FirstOrDefault(w => w.Login == login)?.Password ??
            throw new NotFoundException($"Пользователь по логину {login} не найден");

        /// <summary>
        /// Получить объект аккаунт юзер по номеру или сгенерировать исключение.
        /// </summary>        
        public Domain.DataModels.AccountUser GetAccountUserOrThrowException(Guid accountUserId)
            => dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId) ??
               throw new NotFoundException($"По номеру '{accountUserId}' не удалось найти пользователя аккаунта.");

        /// <summary>
        /// Получить ID аккаунта по логину пользователя
        /// </summary>
        /// <param name="userLogin">Логин пользователя</param>
        /// <returns>ID аккаунта</returns>
        public Guid GetAccountIdByUserLogin(string userLogin)
            => dbLayer.AccountUsersRepository.GetAccountUserByLogin(userLogin)?.AccountId ??
               throw new NotFoundException($"Пользователь по логину {userLogin} не найден");

    }
}
