﻿using System.Text;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AlphaNumericsSupport;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.Billing.BillingService;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountUser;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.SmsClient;

namespace Clouds42.AccountUsers.AccountUser.Internal.Providers
{
    /// <summary>
    /// Провайдер для уведомления пользователя о регистрации
    /// </summary>
    internal class AccountUserRegistrationNotificationProvider(
        IUnitOfWork dbLayer,
        ISmsClient smsClient,
        ILetterNotificationProcessor letterNotificationProcessor,
        IHashProvider hashProvider,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider)
        : IAccountUserRegistrationNotificationProvider
    {
        private readonly Lazy<string> _routeValueForOpenBillingServicePage = new(CloudConfigurationProvider.Cp.GetRouteValueForOpenBillingServicePage);
        private readonly Lazy<string> _routeForResetPassword = new(CloudConfigurationProvider.Cp.GetRouteForResetPassword);


        /// <summary>
        /// Уведомить пользователя о регистрации на сервисе.
        /// </summary>
        /// <param name="accountUser">Зарегистрированный пользователь.</param>
        /// <param name="registrationSource">Источник регистрации.</param>
        /// <param name="generatedPassword">Автосгенирированный пароль</param>
        public void NotifyAccountUser(Domain.DataModels.AccountUser accountUser, IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword)
        {
            SendLetter(accountUser, registrationSource, generatedPassword);
        }

        /// <summary>
        /// Отправить письмо о регистрации пользователя 
        /// </summary>
        /// <param name="accountUser">Зарегистрированный пользователь.</param>
        /// <param name="registrationSource">Источник регистрации.</param>
        /// <param name="generatedPassword">Автосгенирированный пароль</param>
        private void SendLetter(Domain.DataModels.AccountUser accountUser, IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword)
        {
            var resetCodeHash = hashProvider.ComputeHash(accountUser.ResetCode.ToString());
            var externalClient = registrationSource.GetExternalClientInfo();

            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountUser.AccountId);

            letterNotificationProcessor.TryNotify<RegistrationAccountUserLetterNotification, RegistrationUserLetterModelDto>(
                new RegistrationUserLetterModelDto
                {
                    AccountId = accountUser.AccountId,
                    AccountIndexNumber = accountUser.Account.IndexNumber,
                    AccountUserEmail = accountUser.Email,
                    AccountUserLogin = accountUser.Login,
                    AccountUserPhoneNumber = accountUser.PhoneNumber,
                    AccountUserId = accountUser.Id,
                    AccountUserFullName = accountUser.FullName,
                    ExternalClient = externalClient,
                    UserSource = registrationSource.UserSource,
                    AccountUserPsd = string.IsNullOrEmpty(generatedPassword) ? "*******" : generatedPassword,
                    UrlForOpenDelansServicePage = GetUrlForOpenDelansServicePage(externalClient, siteAuthorityUrl),
                    UrlForResetPassword = GetUrlForResetPassword(resetCodeHash, siteAuthorityUrl),
                    SaleManagerLogin = accountUser.Account?.AccountSaleManager?.AccountUser?.Login,
                    SalesManagerDivision = accountUser.Account?.AccountSaleManager?.Division
                });
        }

        /// <summary>
        /// Отправить письмо о регистрации аккаунта 
        /// </summary>
        /// <param name="accountUser">Зарегистрированный пользователь.</param>
        /// <param name="registrationSource">Источник регистрации.</param>
        /// <param name="generatedPassword">Автосгенерированный пароль</param>
        public void SendAccountCreateLetter(Domain.DataModels.AccountUser accountUser, IRegistrationSourceInfoAdapter registrationSource,
            string generatedPassword)
        {
            var resetCodeHash = hashProvider.ComputeHash(accountUser.ResetCode.ToString());
            var externalClient = registrationSource.GetExternalClientInfo();

            var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(accountUser.AccountId);

            letterNotificationProcessor.TryNotify<RegistrationUserLetterNotification, RegistrationUserLetterModelDto>(
                new RegistrationUserLetterModelDto
                {
                    AccountId = accountUser.AccountId,
                    AccountUserEmail = accountUser.Email,
                    AccountUserLogin = accountUser.Login,
                    AccountUserPhoneNumber = accountUser.PhoneNumber,
                    AccountUserId = accountUser.Id,
                    AccountUserFullName = accountUser.FullName,
                    ExternalClient = externalClient,
                    UserSource = registrationSource.UserSource,
                    AccountUserPsd = string.IsNullOrEmpty(generatedPassword) ? "*******" : generatedPassword,
                    UrlForOpenDelansServicePage = GetUrlForOpenDelansServicePage(externalClient, siteAuthorityUrl),
                    UrlForResetPassword = GetUrlForResetPassword(resetCodeHash, siteAuthorityUrl)
                });
        }

        /// <summary>
        /// Получить URL для страницы сервиса Delans
        /// </summary>
        /// <param name="externalClient">Источник регистрации</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <returns>URL для страницы сервиса Delans</returns>
        private string GetUrlForOpenDelansServicePage(ExternalClient externalClient, string siteAuthorityUrl) =>
            externalClient == ExternalClient.Delans
                ? new Uri(
                        $"{siteAuthorityUrl}/{_routeValueForOpenBillingServicePage.Value}{GetDelanceServiceId()}")
                    .AbsoluteUri
                : string.Empty;

        /// <summary>
        /// Получить URL для восстановления пароля
        /// </summary>
        /// <param name="resetCode">Код восстановления</param>
        /// <param name="siteAuthorityUrl">Урл сайта ЛК</param>
        /// <returns>URL для восстановления пароля</returns>
        private string GetUrlForResetPassword(string resetCode, string siteAuthorityUrl) =>
            new Uri($"{siteAuthorityUrl}/{_routeForResetPassword.Value}{resetCode}").AbsoluteUri;

        /// <summary>
        /// Получить Id сервиса Delans
        /// </summary>
        /// <returns>Id сервиса Delans</returns>
        private Guid GetDelanceServiceId() =>
            dbLayer.BillingServiceRepository
                .FirstOrDefault(s => s.InternalCloudService == InternalCloudServiceEnum.Delans)?.Id ??
            throw new NotFoundException("Не удалось получить Id сервиса Delans");

        /// <summary>
        ///     Генерируем новый код активации и
        /// отправляем смс
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        public void SendNewActivationSms(string login)
        {
            var user = dbLayer.AccountUsersRepository.GetAccountUserByLogin(login);

            var confirmationCode = GenerateActivationCode();
            user.ConfirmationCode = confirmationCode;

            dbLayer.AccountUsersRepository.InsertOrUpdateAccountUser(user);

            var number = $"+{PhoneHelper.ClearNonDigits(user.PhoneNumber)}";

            smsClient.SendSms(number, $"Код активации учетной записи 42Clouds: {confirmationCode}");
        }

        /// <summary>
        ///     Генерирует код активации
        /// </summary>
        private static string GenerateActivationCode()
        {
            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            return alphaNumeric42CloudsGenerator.GenerateConfirmationCode();
        }

        /// <summary>
        /// Отправить СМС уведомление на телефон.
        /// </summary>
        /// <param name="accountUser">Зарегистрированный клиент.</param>
        /// <param name="generatedPassword">пароль клиента.</param>
        /// <param name="client">Источник регистрации.</param>
        private void SendSmsLogPas(Domain.DataModels.AccountUser accountUser, string generatedPassword, ExternalClient client)
        {
            var number = $"+{PhoneHelper.ClearNonDigits(accountUser.PhoneNumber)}";

            var sb = new StringBuilder();
            sb.Append("Ваши учетные данные для входа");

            switch (client)
            {
                case ExternalClient.Promo:
                    sb.AppendLine(":");
                    sb.AppendLine($"Логин: {accountUser.Login}");
                    break;
                case ExternalClient.Delans:
                case ExternalClient.ProfitAccount:
                case ExternalClient.Dostavka:
                case ExternalClient.Efsol:
                case ExternalClient.Yashchenko:
                case ExternalClient.Market:
                    sb.AppendLine(" в личный кабинет 42Clouds:");
                    sb.AppendLine($"Логин: {accountUser.Login}");
                    break;
                case ExternalClient.Sauri:
                    sb.AppendLine(" в сервис Sauri:");
                    sb.AppendLine($"Логин: {accountUser.Login}");
                    break;
                case ExternalClient.AbonCenter:
                    sb.AppendLine(" в Абонцентр:");
                    sb.AppendLine($"Логин: {accountUser.Email}");
                    break;
                case ExternalClient.Roznica42:
                    break;
                case ExternalClient.Kladovoy:
                    break;
                case ExternalClient.MCOB:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(client), client, null);
            }

            if (!string.IsNullOrEmpty(generatedPassword))
                sb.AppendLine($"Пароль: {generatedPassword}");

            smsClient.SendSms(number, sb.ToString());
        }
    }
}
