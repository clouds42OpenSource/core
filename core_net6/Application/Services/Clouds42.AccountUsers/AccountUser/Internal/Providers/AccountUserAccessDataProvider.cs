﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUser.Internal.Providers
{
    /// <summary>
    /// Провадйер управления авторизационными даными пользователя.
    /// </summary>
    internal class AccountUserAccessDataProvider(
        IUnitOfWork dbLayer,
        IOpenIdProvider openIdProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        AesEncryptionProvider aesEncryptionProvider,
        IConfiguration configuration)
        : IAccountUserAccessDataProvider
    {
        private readonly DesEncryptionProvider _encryptionProvider = new(configuration);

        /// <summary>
        /// Сменить логин у пользователя.
        /// </summary>      
        public void SetLogin(Guid accountUserId, string newLogin)
        {
            var accountUser = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId);
            if (accountUser == null)
                throw new KeyNotFoundException("Account user not found");

            activeDirectoryTaskProcessor.RunTask(provider => provider.ChangeLogin(accountUserId, accountUser.Login, newLogin, accountUser.Email));

            accountUser.Login = newLogin;
            dbLayer.AccountUsersRepository.InsertOrUpdateAccountUser(accountUser);
            dbLayer.Save();
        }

        /// <summary>
        /// Установить новый пароль пользователю.
        /// </summary>        
        public void SetPassword(Guid accountUserId, string newPassword, string oldPassword)
        {
            var usr = dbLayer.AccountUsersRepository.GetAccountUser(accountUserId) ?? throw new KeyNotFoundException("Account user not found");
            if (!_encryptionProvider.Encrypt(oldPassword)
                    .Equals(usr.Password, StringComparison.InvariantCulture))
                throw new PreconditionException("Login or password is incorrect");
            
            ValidateAccountUserNewPassword(newPassword);

            var locale = usr.Account.AccountConfiguration.Locale.Name;

            openIdProvider.ChangeUserProperties(new SauriOpenIdControlUserModel
            {
                AccountUserID = usr.Id,
                AccountID = usr.Account.Id,
                Password = newPassword
            }, locale);

            activeDirectoryTaskProcessor.RunTask(provider => provider.ChangePassword(usr.Id, usr.Login, newPassword));

            usr.Password = _encryptionProvider.Encrypt(newPassword);
            usr.PasswordHash = aesEncryptionProvider.Encrypt(newPassword);
            usr.DateTimeOfLastChangePasswordOrLogin = DateTime.Now;

            dbLayer.AccountUsersRepository.InsertOrUpdateAccountUser(usr);
            dbLayer.Save();
        }

        /// <summary>
        /// Провалидировать новый пароль пользователя
        /// </summary>
        /// <param name="newPassword">Новый пароль пользователя</param>
        private static void ValidateAccountUserNewPassword(string newPassword)
        {
            var passwordValidator = new PasswordValidation(newPassword);
            var passwordValidationResult = passwordValidator.PasswordIsValid();

            if (!passwordValidationResult.DataIsValid)
                throw new PreconditionException(string.Join(", ", passwordValidationResult.ValidationMessages));
        }

    }
}
