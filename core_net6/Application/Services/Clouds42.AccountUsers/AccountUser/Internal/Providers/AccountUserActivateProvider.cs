﻿using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Billing.Contracts.BillingServices.Interfaces.Providers;
using Clouds42.CloudServices.Contracts;
using Clouds42.DataContracts.BillingService;
using Clouds42.DataContracts.Service.MainServiceResourcesChangesHistory;
using Clouds42.Domain.DataModels.billing;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using IBillingServiceDataProvider = Clouds42.Billing.Contracts.Billing.Interfaces.Providers.IBillingServiceDataProvider;

namespace Clouds42.AccountUsers.AccountUser.Internal.Providers
{
    /// <summary>
    /// Провадйер управления активацией пользователя.
    /// </summary>
    public class AccountUserActivateProvider(
        IUnitOfWork dbLayer,
        IBillingServiceTypePayProvider billingServiceTypePayProvider,
        IRent1CConfigurationAccessProvider rent1CConfigurationAccessProvider,
        ICommitMainServiceResourcesChangesHistoryProvider commitMainServiceResourcesChangesHistoryProvider,
        IResourceDataProvider resourceDataProvider,
        IBillingServiceDataProvider billingServiceDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Установка статуса активации для пользователя
        /// </summary>
        public void SetActivateStatusForUser(Domain.DataModels.AccountUser accountUser, bool activationValue)
        {
            using var dbScope = dbLayer.SmartTransaction.Get();
            try
            {
                if (!activationValue)
                {
                    DeactivateConfigurateAccesses(accountUser);
                    DisableAllBillingServices(accountUser);
                }

                logger.Debug($"Активировать / деактивировать пользователя {accountUser.Login} в базе");
                accountUser.Activated = activationValue;
                dbLayer.AccountUsersRepository.Update(accountUser);
                dbLayer.Save();
                dbScope.Commit();
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"Ошибка [Активировании / деактивировании пользователя] {accountUser.Login}");
                dbScope.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Отключение аренды для пользователя при деактивации
        /// </summary>
        /// <param name="accountUser">пользователь</param>
        private void DeactivateConfigurateAccesses(Domain.DataModels.AccountUser accountUser)
        {
            var billingService = billingServiceDataProvider.GetSystemService(Clouds42Service.MyEnterprise);
            var resources = resourceDataProvider.GetServiceResourcesForAccountUser(billingService.Id, accountUser.Id)
                .ToList();

            if (resources.Count == 0)
            {
                logger.Debug(
                    $"Аренда для пользователя {accountUser.Login} не подключена, при деактивации пользователя ничего делать не надо");
                return;
            }

            logger.Debug(
                $"Аренда для пользователя {accountUser.Login} подключена, при деактивации пользователя необходимо отключить у него аренду");

            rent1CConfigurationAccessProvider.ConfigureAccesses(accountUser.AccountId,
            [
                new() { AccountUserId = accountUser.Id }
            ]);

            logger.Debug($"Аренда для пользователя {accountUser.Login} отключена");
        }

        /// <summary>
        /// Отключить все услуги для пользователя
        /// </summary>
        /// <param name="accountUser">Пользователь аккаунта</param>
        public void DisableAllBillingServices(Domain.DataModels.AccountUser accountUser)
        {
            logger.Debug($"Начало отключения услуг для пользователя {accountUser.Login}");

            using (var dbScope = dbLayer.SmartTransaction.Get())
            {
                try
                {
                    var accountUserResources = resourceDataProvider.GetAccountUserResources(accountUser.Id).ToList();

                    accountUserResources.ForEach(resource => { DisableBillingService(resource, accountUser); });

                    if (accountConfigurationDataProvider.IsVipAccount(accountUser.AccountId) &&
                        WasRent1CDisabled(accountUserResources))
                        CommitMainServiceResourcesChanges(accountUser.AccountId);

                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    handlerException.Handle(ex,
                        $"[Ошибка отключения услуг для пользователя пользователя] \"{accountUser.Login}\"");
                    dbScope.Rollback();
                    throw;
                }
            }

            logger.Debug($"Все услуги для пользователя \"{accountUser.Login}\" отключены");
        }

        /// <summary>
        /// Отключение услуги, связанной с ресурсом
        /// </summary>
        /// <param name="resource">Ресурс, связанный с услугой</param>
        /// <param name="accountUser">Пользователь, для которого производится отключение услуги</param>
        private void DisableBillingService(Resource resource, Domain.DataModels.AccountUser accountUser)
        {
            if(resource == null) 
                return;

            logger.Debug(
                $"Начало отключения услуги \"{resource.BillingServiceType.Name}\" для пользователя \"{accountUser.Login}\"");
            try
            {
                billingServiceTypePayProvider.ApplyOrPayForAllChangesAccountServiceTypes(accountUser.AccountId,
                resource.BillingServiceType.ServiceId, false, null,
                new List<CalculateBillingServiceTypeDto>
                {
                    new()
                    {
                        Subject = accountUser.Id,
                        Status = false,
                        BillingServiceTypeId = resource.BillingServiceTypeId
                    }
                });
                logger.Debug(
                $"Услуга \"{resource.BillingServiceType.Name}\" для пользователя \"{accountUser.Login}\" отключена");
            }
            catch(Exception ex)
            {
                logger.Warn(
                $"Ошибка при отключении услуги \"{resource.BillingServiceType.Name}\" для пользователя \"{accountUser.Login}\":: {ex.Message}");
            }
            
            
        }

        /// <summary>
        /// Зафиксировать историю изменений ресурсов главного сервиса
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        private void CommitMainServiceResourcesChanges(Guid accountId)
            => commitMainServiceResourcesChangesHistoryProvider.Commit(new CommitMainServiceResourcesChangesHistoryDto
            {
                AccountId = accountId,
                DisabledResourcesCount = 1
            });

        /// <summary>
        /// Определить, была ли отключена услуга "Аренда 1С"
        /// </summary>
        /// <param name="resources">Ресурсы, связанные с услугами, которые были отключены</param>
        /// <returns>Признак присутствия услуги "Аренда 1С"</returns>
        private bool WasRent1CDisabled(IEnumerable<Resource> resources)
            => (
                from resource in resources
                join serviceType in dbLayer.BillingServiceTypeRepository.AsQueryable()
                    on resource.BillingServiceTypeId equals serviceType.Id
                join service in dbLayer.BillingServiceRepository.WhereLazy(serv =>
                        serv.SystemService == Clouds42Service.MyEnterprise)
                    on serviceType.ServiceId equals service.Id
                select resource
            ).Any();
    }
}
