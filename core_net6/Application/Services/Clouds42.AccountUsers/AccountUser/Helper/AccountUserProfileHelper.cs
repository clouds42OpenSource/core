﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using Clouds42.CoreWorker.JobWrappers.CreateDirectory;
using Clouds42.CoreWorker.JobWrappers.CreateDirectoryInfoModel;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    /// <summary>
    /// Хелпер для профиля пользователя
    /// </summary>
    public class AccountUserProfileHelper(CreateDirectoryJobWrapper createDirectoryJobWrapper)
        : IAccountUserProfileHelper
    {
        /// <summary>
        /// Запустить задачу по созданию директории 1CeStart.
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        public void RunTaskCreate1CStartFolder(string login)
            => createDirectoryJobWrapper.Start(new CreateDirectoryInfoParams
                {DirectoryPath = AccountUserProfileDirectoryHelper.GetPathForCreate1CeStartDirectory(login)});
    }
}