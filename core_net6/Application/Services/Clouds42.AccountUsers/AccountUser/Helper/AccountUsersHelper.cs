﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using Clouds42.AccountUsers.Contracts.OpenId.Interfaces;
using Clouds42.AccountUsers.Contracts.OpenId.Sauri.Models;
using Clouds42.ActiveDirectory.Contracts;
using Clouds42.AlphaNumericsSupport;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Encrypt;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.Account.Registration;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    public class AccountUsersHelper(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IOpenIdProvider openIdProvider,
        IActiveDirectoryTaskProcessor activeDirectoryTaskProcessor,
        IAccountUserProfileHelper accountUserProfileHelper,
        ILogger42 logger,
        IConfiguration configuration,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Создание пользователя в провайдере
        /// </summary>
        /// <param name="accountUser">Аккаунт пользователя</param>
        /// <param name="password">Пароль не шифрованый</param>
        /// <param name="phoneNumber">Номер телефона при регистрации</param>
        private void CreateSauriOpenIdUser(Domain.DataModels.AccountUser accountUser, string password, string phoneNumber)
        {
            try
            {
                var locale = accountUser.Account.AccountConfiguration.Locale.Name;
                openIdProvider.AddUser(new SauriOpenIdControlUserModel
                {
                    AccountID = accountUser.AccountId,
                    AccountUserID = accountUser.Id,
                    Login = accountUser.Login,
                    Name = accountUser.Name,
                    Password = password,
                    Email = accountUser.Email,
                    Phone = phoneNumber,
                    FullName = accountUser.FullName
                }, locale);

                logger.Trace("Пользователь {0} Id={1} успешно зарегистрирован в провайдере OpenID", accountUser.Login,
                    accountUser.Id); 
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка добавления пользователя в провайдер] {accountUser.Login}");
                throw;
            }
        }

        /// <summary>
        /// Заполнить гененрированным значением поле пароля.
        /// </summary>
        /// <param name="userInfo">Данные регистраци.</param>
        public void FillEmptyPassword(AccountUserRegistrationModelDto userInfo)
        {
            if (!string.IsNullOrEmpty(userInfo.Password))
                return;

            var alphaNumeric42CloudsGenerator = new AlphaNumeric42CloudsGenerator(new CryptoRandomGeneric());
            userInfo.Password = alphaNumeric42CloudsGenerator.GeneratePassword();
        }

        /// <summary>
        /// Полное создание пользователя
        /// </summary>
        /// <param name="accountUserDc">Аккаунт пользователя</param>
        public Domain.DataModels.AccountUser CreateUser(AccountUserDto accountUserDc)
        {
            //Если нет права устанавливать роли, то ставим AccountUser
            if (!accessProvider.HasAccessBool(ObjectAction.AccountUsers_RoleEdit,
                    () => accountUserDc.AccountId) || accountUserDc.Roles == null)
                accountUserDc.Roles = [AccountUserGroup.AccountUser];

            var account = dbLayer.AccountsRepository
                              .AsQueryable()
                              .Include(x => x.AccountSaleManager)
                              .ThenInclude(x => x.AccountUser)
                              .FirstOrDefault(a => a.Id == accountUserDc.AccountId)
                ?? throw new InvalidOperationException($"Не удалось получить Id аккаунта по Id пользователя {accountUserDc.AccountId}");

            Domain.DataModels.AccountUser accountUser;
            accountUserDc.Id = Guid.NewGuid();
            using var transaction = dbLayer.SmartTransaction.Get();
            try
            {
              
                accountUser = new Domain.DataModels.AccountUser
                {
                    Id = accountUserDc.Id.Value,
                    AccountId = accountUserDc.AccountId,
                    Email = accountUserDc.Email,
                    EmailStatus = "Unchecked",
                    Login = accountUserDc.Login,
                    Password = new DesEncryptionProvider(configuration).Encrypt(accountUserDc.Password),
                    PasswordHash = new AesEncryptionProvider(configuration).Encrypt(accountUserDc.Password),
                    LastName = accountUserDc.LastName,
                    FirstName = accountUserDc.FirstName,
                    MiddleName = accountUserDc.MiddleName,
                    AuthToken = accountUserDc.AuthToken,
                    Activated = true,
                    PhoneNumber = PhoneHelper.ClearNonDigits(accountUserDc.PhoneNumber),
                    CreatedInAd = true,
                    CorpUserSyncStatus = "Added",
                    CreationDate = DateTime.Now,
                    ResetCode = accountUserDc.ResetCode
                };

                var roles = accountUserDc.Roles.Select(w =>
                    new AccountUserRole
                    {
                        AccountUserGroup = w,
                        Id = Guid.NewGuid(),
                        AccountUserId = accountUser.Id
                    });

                dbLayer.AccountUsersRepository.Insert(accountUser);
                dbLayer.AccountUserRoleRepository.InsertRange(roles);
                dbLayer.Save();

                logger.Trace("Пользователь {0} успешно сохранен в контексте компании {1}({2})", accountUser.Login,
                    account.AccountCaption, account.IndexNumber);

                CreateSauriOpenIdUser(accountUser, accountUserDc.Password, accountUserDc.PhoneNumber);
                dbLayer.Save();

                activeDirectoryTaskProcessor.RunTask(provider => provider.RegisterNewUser(accountUserDc));
                accountUserProfileHelper.RunTaskCreate1CStartFolder(accountUser.Login);

                LogEventHelper.LogEvent(dbLayer, account.Id, accessProvider, LogActions.AddingUser, $"Добавлен пользователь \"{accountUserDc.Login}\"");

                transaction.Commit();
                accountUser.Account = account;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка во время создания пользователя] {accountUserDc.Login}");

                try
                {
                    openIdProvider.DeleteUser(
                        new SauriOpenIdControlUserModel { AccountUserID = accountUserDc.Id.Value, });

                    activeDirectoryTaskProcessor.RunTask(provider => provider.DeleteUser(accountUserDc.Id.Value, accountUserDc.Login));
                }

                catch (Exception ex2)
                {
                    handlerException.Handle(ex2, $"[Ошибка отката пользователя {accountUserDc.Login} в AD или OpedId при добавлении из ЛК]");

                    transaction.Rollback();

                    throw;
                }

                transaction.Rollback();

                throw;
            }

            return accountUser;
        }
    }
}
