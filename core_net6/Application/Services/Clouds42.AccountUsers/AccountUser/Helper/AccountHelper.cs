using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    /// <summary>
    /// Хелпер для получения аккаунтов пользователя по Id
    /// </summary>
    public class AccountHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Получает аккаунт пользователя по Id
        /// </summary>
        /// <param name="accountUserId">Id аккаунта</param>
        /// <param name="accountUser">out параметр Аккаунт пользователя</param>
        /// <returns>Признак нашелся ли аккаунт по Id</returns>
        public bool TryGetAccountUserById(Guid accountUserId, out Domain.DataModels.AccountUser accountUser)
        {
            accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId);

            if (accountUser == null)
                return false;
            return true;
        }

        /// <summary>
        /// Получает аккаунт пользователя по Email
        /// </summary>
        /// <param name="accountUserId">Id аккаунта</param>
        /// <param name="accountUser">out параметр Аккаунт пользователя</param>
        /// <returns>Признак нашелся ли аккаунт по Email</returns>
        public bool TryGetAccountUserByEmail(string accountUserEmail, out Domain.DataModels.AccountUser accountUser)
        {
            accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(f => f.Email == accountUserEmail);

            if (accountUser == null)
                return false;
            return true;
        }
    }
}
