﻿using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    /// <summary>
    /// Класс содержит методы для поиска пользователей аккаунта
    /// и определение их общего количества. 
    /// </summary>
    public class AccountUsersSearchHelper(IAccessProvider accessProvider, IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Считывает пользователей аккаунта с БД в соответствии условию
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="filter">набор парамертров по которому юзеры выбираються из БД</param>
        /// <returns>Список пользователей для одной страницы пагинации и их общее количество</returns>
        public UsersSeachResultModel GetAccountUsersSeachResult(Guid accountId, AccountUsersFilterModel filter)
        {
            var currentUser = accessProvider.GetUser();
            var currentUserRoles = new HashSet<AccountUserGroup>(currentUser.Groups.ToList());
            //Просматривать карточки других пользователей могут те, у кого есть роль из перечисленых
            var rolesWithRightToEdit = new HashSet<AccountUserGroup>(new List<AccountUserGroup>
            {   AccountUserGroup.AccountAdmin,
                AccountUserGroup.AccountSaleManager,
                AccountUserGroup.Hotline,
                AccountUserGroup.CloudAdmin,
                AccountUserGroup.CloudSE
            });
            currentUserRoles.IntersectWith(rolesWithRightToEdit);//пересечение
            bool canSeeOtherPeoplesCards = (currentUserRoles.Count > 0);

            List<Domain.DataModels.AccountUser> accountUsers;
            int accountUsersCount;
            UsersSeachResultModel usersSeachResultModel = new UsersSeachResultModel();

            if (!canSeeOtherPeoplesCards)
            {
                //получаем лишь инициатора
                accountUsers = [dbLayer.AccountUsersRepository.GetAccountUser(currentUser.Id)];
                accountUsersCount = 1;
            }
            else
            {
                accountUsers = GetAccountUsersFilteredList(accountId, filter);
                accountUsersCount = GetAccountUsersCount(accountId, filter);
            }

            var res = new List<AccountUserDto>();
            res.AddRange(accountUsers.Select(accountUser => accountUser.MapToAccountUserDc()));
            usersSeachResultModel.Users = res;
            usersSeachResultModel.AccountUsersCount = accountUsersCount;
            return usersSeachResultModel;
        }
        /// <summary>
        /// Возвращает предвариительный список пользователей аккаунта
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private List<Domain.DataModels.AccountUser> GetAccountUsersFilteredList(Guid accountId, AccountUsersFilterModel filter)
        {
            var itemsToSkip = (filter.Page - 1) * filter.PageSize;
            List<Domain.DataModels.AccountUser> accountUsers;
            if (string.IsNullOrEmpty(filter.SearchQuery) || filter.SearchQuery.Equals(""))
            {
                var query = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(au => au.AccountId == accountId);

                query = ApplyOrder(query, filter.SortColumn, filter.DescendingFlag);

                accountUsers = query
                    .Skip(itemsToSkip)
                    .Take(filter.PageSize)
                    .ToList();
            }
            else
            {
                var searchAsPhone = PhoneHelper.ClearNonDigits(filter.SearchQuery);
                var query = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(a => (
                        (a.AccountId == accountId)
                        && ((a.Login != null && a.Login.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.FirstName != null && a.FirstName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.LastName != null && a.LastName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.MiddleName != null && a.MiddleName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.FullName != null && a.FullName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.Email != null && a.Email.ToLower().Contains(filter.SearchQuery.ToLower()))
                            || ((!string.IsNullOrEmpty(searchAsPhone)) && (searchAsPhone.Length >= 5) &&
                                (a.PhoneNumber != null) && (a.PhoneNumber).Contains(searchAsPhone))
                        )
                    ));
                query = ApplyOrder(query, filter.SortColumn, filter.DescendingFlag);
                accountUsers = query
                    .Skip(itemsToSkip)
                    .Take(filter.PageSize)
                    .ToList();

            }

            return accountUsers;
        }

        /// <summary>
        /// Возвращает количество пользователей аккаунта соответствующих критерию
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="filter">Набор критериев</param>
        /// <returns>количество пользователей</returns>
        public int GetAccountUsersCount(Guid accountId, AccountUsersFilterModel filter)
        {
            var searchAsPhone = PhoneHelper.ClearNonDigits(filter.SearchQuery);
            int accountUsersCount = 0;
            if (string.IsNullOrEmpty(filter.SearchQuery) || filter.SearchQuery.Equals(""))
            {
                accountUsersCount = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(au => au.AccountId == accountId)
                    .Include(i => i.Account)
                    .Include(i => i.AccountUserRoles)
                    .ToList().Count;
            }
            else
            {
                accountUsersCount = dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
                    .Where(a => (
                        (a.AccountId == accountId)
                        && ((a.Login != null && a.Login.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.FirstName != null && a.FirstName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.LastName != null && a.LastName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.MiddleName != null && a.MiddleName.ToLower().Contains(filter.SearchQuery.ToLower())) ||
                            (a.Email != null && a.Email.ToLower().Contains(filter.SearchQuery.ToLower()))
                            || ((!string.IsNullOrEmpty(searchAsPhone)) && (searchAsPhone.Length >= 5) &&
                                (a.PhoneNumber != null) && (a.PhoneNumber).Contains(searchAsPhone))
                        )
                    ))
                    .Include(i => i.Account)
                    .Include(i => i.AccountUserRoles)
                    .ToList().Count;
            }

            return accountUsersCount;
        }

        /// <summary>
        /// Дописывает методов расширения Order 
        /// </summary>
        /// <param name="query">схема пользователей</param>
        /// <param name="column">свойство для сортировки</param>
        /// <param name="descending">направление сортировки</param>
        /// <returns></returns>
        private static IQueryable<Domain.DataModels.AccountUser> ApplyOrder(IQueryable<Domain.DataModels.AccountUser> query, string column, bool? descending)
        {
            IQueryable<Domain.DataModels.AccountUser> OrderString = column switch
            {
                nameof(Domain.DataModels.AccountUser.Login) => descending == false
                    ? query.OrderBy(o => o.Login)
                    : query.OrderByDescending(o => o.Login),
                nameof(Domain.DataModels.AccountUser.FullName) => descending == false
                    ? query.OrderBy(o => ((o.LastName + "" + o.FirstName + "" + o.MiddleName).Trim()))
                    : query.OrderByDescending(o => ((o.LastName + "" + o.FirstName + "" + o.MiddleName).Trim())),
                nameof(Domain.DataModels.AccountUser.Email) => descending == false
                    ? query.OrderBy(o => o.Email)
                    : query.OrderByDescending(o => o.Email),
                nameof(Domain.DataModels.AccountUser.Activated) => descending == false
                    ? query.OrderBy(o => o.Activated)
                    : query.OrderByDescending(o => o.Activated),
                _ => null
            };

            return OrderString;
        }
    }
}
