﻿using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    /// <summary>
    /// Хелпер для работы с данными пользователя
    /// </summary>
    public class AccountUserDataHelper(IUnitOfWork dbLayer)
    {
        /// <summary>
        /// Проверить существование пользователя по логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Результат проверки</returns>
        public bool AccountUserIsExists(string login) =>
            dbLayer.AccountUsersRepository.GetAccountUserByLogin(login) != null;
    }
}
