﻿using System.Net;
using System.Net.Http.Headers;
using System.Web;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Helpers;
using Clouds42.Configurations.Configurations;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    public class SynchronizationCorpHelper : ISynchronizationCorpHelper
    {
        private readonly string _username = CloudConfigurationProvider.CorpApi.GetUsername();
        private readonly string _password = CloudConfigurationProvider.CorpApi.GetPassword();

        private readonly Uri _corpApiUrlGetAct = CloudConfigurationProvider.CorpApi.Url.GetUrlGetAct();

        public async Task<byte[]> GetAct(Guid actId)
        {
            var handler = new HttpClientHandler
            {
                Credentials = new NetworkCredential(_username, _password),
                PreAuthenticate = true
            };
            using var client = new HttpClient(handler);

            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("CORE"));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            client.DefaultRequestHeaders.UserAgent.ParseAdd("CORE");

            var parameters = HttpUtility.ParseQueryString(string.Empty);

            parameters.Add(nameof(Guid), actId.ToString());

            var uriBuilder = new UriBuilder(_corpApiUrlGetAct)
            {
                Query = parameters.ToString()
            };

            var resp = await client.GetAsync(uriBuilder.Uri);

            if(resp.StatusCode != HttpStatusCode.OK)
                throw new InvalidOperationException($"[GetAct] код ответа {resp.StatusCode}");

            return await resp.Content.ReadAsByteArrayAsync();
        }
    }
}
