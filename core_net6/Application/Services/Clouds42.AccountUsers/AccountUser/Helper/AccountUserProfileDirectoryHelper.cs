﻿using Clouds42.Configurations.Configurations;

namespace Clouds42.AccountUsers.AccountUser.Helper
{
    /// <summary>
    /// Хелпер для работы с директорией пользователя
    /// </summary>
    public static class AccountUserProfileDirectoryHelper
    {
        /// <summary>
        /// Получить путь до директории 1CeStart.
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Путь до директории 1CeStart</returns>
        public static string GetPathForCreate1CeStartDirectory(string login) =>
            string.Format(Path.Combine(CloudConfigurationProvider.Files.GetAccountUserProfilesDirectory(),
                CloudConfigurationProvider.Files.PathTo1CeStart()), login);
    }
}
