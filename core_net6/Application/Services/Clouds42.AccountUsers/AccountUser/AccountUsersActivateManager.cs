﻿using Clouds42.AccountUsers.AccountUser.Internal.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Менеджер активации/дезактивации пользователя
    /// </summary>
    public class AccountUsersActivateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        AccountUserActivateProvider accountUserActivateProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Отключение/подключение пользователя
        /// </summary>
        /// <param name="accountUserId">Индификатор пользователя</param>
        /// <param name="isActivate">статус активации</param>
        /// <returns></returns>
        public ManagerResult Activate(Guid accountUserId, bool isActivate)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Activate, null, () => accountUserId);

            var accountUser = DbLayer.AccountUsersRepository.FirstOrDefault(au => au.Id == accountUserId);
            if (accountUser == null)
                throw new InvalidOperationException("Account user not found");

            accountUserActivateProvider.SetActivateStatusForUser(accountUser, isActivate);

            logger.Trace($"Сменен статус активности для {accountUser.Login} на {isActivate}, пользователем {AccessProvider.Name}.");

            return Ok();
        }
    }
}
