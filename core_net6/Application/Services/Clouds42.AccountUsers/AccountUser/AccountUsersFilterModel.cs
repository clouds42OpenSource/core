﻿
using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// набор парамертров по которому юзеры выбираються из БД
    /// </summary>
    public class AccountUsersFilterModel
    {
        //номер страницы в пейджере
        public int Page { get; set; } = 1;

        //количество записей на странице
        public int PageSize { get; set; } = 14;

        //строка поиска
        public string SearchQuery { get; set; } = null;

        //имя столбца по которому будет проводиться сортировка
        public string SortColumn { get; set; } = nameof(IAccountUser.Login);

        //признак сортировки по убыванию
        public bool DescendingFlag { get; set; } = false;
    }

}