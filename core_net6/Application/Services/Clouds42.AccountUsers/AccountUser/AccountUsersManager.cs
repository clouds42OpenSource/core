﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using Clouds42.AccountUsers.Contracts;
using Clouds42.Billing.Contracts.DataManagers.Resources;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.DataValidations;
using Clouds42.Common.Encrypt.Hashes;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.DataContracts.AccountUser.AccountUserBitrixChatInfo;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.Resources.Contracts.Interfaces;
using Clouds42.Validators.Account.Helpers;
using CommonLib.Enums;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Менеджер для работы с пользователем аккаунта
    /// </summary>
    public class AccountUsersManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ILogger42 logger,
        IResourcesService resourcesService,
        ICloudLocalizer cloudLocalizer,
        IResourceDataProvider resourceDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountUsersManager
    {

        /// <summary>
        /// Получить свойства пользователя облака.
        /// </summary>
        /// <param name="accountUserId">Номер пользователя облака.</param>
        /// <returns>Свойства пользователя облака.</returns>
        public ManagerResult<Domain.DataModels.AccountUser> GetProperties(Guid accountUserId)
        {
            ProcessWithTimingFixation("Auth",
                () => AccessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () => accountUserId));

            try
            {
                var accountUser = ProcessWithTimingFixation("ReadDB",
                    () => DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Id == accountUserId));

                return accountUser == null ? NotFound<Domain.DataModels.AccountUser>($"Account user with Id: {accountUserId} not found") : Ok(accountUser);
            }
            catch (Exception ex)
            {
                return PreconditionFailed<Domain.DataModels.AccountUser>(ex.Message);
            }
        }
        /// <summary>
        /// Получение информации для чата Битрикс
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="currentUrl"></param>
        /// <returns></returns>
        public ManagerResult<List<AccountUserBitrixChatInfoItemDto>> GetUserInfoForBitrix(Guid currentUserId, string currentUrl)
        {
            try
            {
                if (currentUserId == Guid.Empty)
                    return Ok(CreateDefaultUserInfo());

                var currentUser = DbLayer.AccountUsersRepository.GetAccountUser(currentUserId);

                var result = CreateAccountUserBitrixChatInfo(currentUser, currentUrl);
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.Warn(ex, "[Ошибка создания модели информации о пользователе для чата Битрикс24]");
                return Ok(CreateDefaultUserInfo());
            }
        }

        /// <summary>
        /// Создать модель информации о пользователе для чата Битрикс24
        /// </summary>
        /// <param name="accountUser">Текущий пользователь</param>
        /// <param name="currentUrl">URL с которого пришел запрос</param>
        /// <returns>Модель информации о пользователе для чата Битрикс24</returns>
        private List<AccountUserBitrixChatInfoItemDto> CreateAccountUserBitrixChatInfo(Domain.DataModels.AccountUser accountUser, string currentUrl)
            =>
            [
                new AccountUserBitrixChatInfoItemDto
                {
                    KeyName = BitrixChatHelperConstants.UserLoginKeyName,
                    Value = GetKeyValueOrDefault(accountUser?.Login),
                    DisplayType = BitrixChatHelperConstants.InlineDisplayTypeName
                },

                new AccountUserBitrixChatInfoItemDto
                {
                    KeyName = BitrixChatHelperConstants.UserAccountNumberKeyName,
                    Value = GetKeyValueOrDefault(accountUser?.Account?.IndexNumber.ToString()),
                    DisplayType = BitrixChatHelperConstants.InlineDisplayTypeName
                },

                new AccountUserBitrixChatInfoItemDto
                {
                    KeyName = BitrixChatHelperConstants.UserFullNameKeyName,
                    Value = GetKeyValueOrDefault(accountUser?.GetFullName()),
                    DisplayType = BitrixChatHelperConstants.InlineDisplayTypeName
                },

                new AccountUserBitrixChatInfoItemDto
                {
                    KeyName = BitrixChatHelperConstants.UserRequestedSiteUriKeyName,
                    Value = GetKeyValueOrDefault(currentUrl),
                    DisplayType = BitrixChatHelperConstants.InlineDisplayTypeName
                }
            ];

        /// <summary>
        /// Создать дефолтную модель информации о пользователе
        /// </summary>
        /// <returns>Дефолтная модель информации о пользователе</returns>
        private static List<AccountUserBitrixChatInfoItemDto> CreateDefaultUserInfo()
            =>
            [
                new AccountUserBitrixChatInfoItemDto
                {
                    KeyName = BitrixChatHelperConstants.UserLoginKeyName,
                    Value = BitrixChatHelperConstants.UndefinedValue,
                    DisplayType = BitrixChatHelperConstants.InlineDisplayTypeName
                }
            ];

        /// <summary>
        /// Получить значение ключа или вернуть дефолтное если значения нет
        /// </summary>
        /// <param name="value">Значение ключа</param>
        /// <returns>Занчение ключа либо дефолтное значение</returns>
        private static string GetKeyValueOrDefault(string value)
            => string.IsNullOrEmpty(value) ? BitrixChatHelperConstants.UndefinedValue : value;
        

        /// <summary>
        /// Get account user id by email
        /// </summary>
        /// <returns></returns>
        public ManagerResult<AccountUserIdAndVerifireStatusDTO> GetIdByEmail(string email, out string headString)
        {
            var timer = new Stopwatch();
            timer.Start();
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () =>
            {
                var acu = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Email == email);
                return acu?.Id;
            });
            timer.Stop();
            headString = "Auth:" + timer.ElapsedMilliseconds;

            if (string.IsNullOrEmpty(email))
                return PreconditionFailed<AccountUserIdAndVerifireStatusDTO>("Bad value for Email");

            var emailValidation = new EmailValidation(email);
            if (!emailValidation.EmailMaskIsValid())
                return PreconditionFailed<AccountUserIdAndVerifireStatusDTO>("Bad value for Email");

            timer.Restart();
            var accountUser =
                DbLayer.AccountUsersRepository.GetAccountUserByVerifiedEmail(email);

            if (accountUser == null)
                accountUser = DbLayer.AccountUsersRepository.GetAccountUserByNoVerifiedEmail(email);

            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;

            return accountUser == null 
                ? NotFound<AccountUserIdAndVerifireStatusDTO>($"Account user with email: {email} not found") 
                : Ok(new AccountUserIdAndVerifireStatusDTO
                {
                    AccountUserID = accountUser.Id,
                    IsVerified = accountUser.EmailStatus == "Checked"
                });
        }

        /// <summary>
        /// Get account user id by login
        /// </summary>
        /// <returns></returns>
        public ManagerResult<Guid> GetIdByLogin(string login, out string headString)
        {
            var timer = new Stopwatch();
            timer.Start();
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () =>
            {
                var acu = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.Login == login);
                return acu?.Id;
            });
            timer.Stop();
            headString = "Auth:" + timer.ElapsedMilliseconds;

            timer.Restart();
            var accountUser =
                DbLayer.AccountUsersRepository.GetAccountUserByLogin(login);
            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;

            return accountUser == null ? NotFound<Guid>($"Account user with login: {login} not found") : Ok(accountUser.Id);
        }

        /// <summary>
        /// Get account user id by phone number
        /// </summary>
        /// <returns></returns>
        public ManagerResult<AccountUserIdAndVerifireStatusDTO> GetIdByPhoneNumber(string phoneNumber)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () =>
            {
                var acu = DbLayer.AccountUsersRepository.FirstOrDefault(u => u.PhoneNumber == phoneNumber);
                return acu?.Id;
            });

            var phoneNumberValidation = new PhoneNumberValidation(phoneNumber);

            if (!phoneNumberValidation.PhoneNumberMaskIsValid())
                return PreconditionFailed<AccountUserIdAndVerifireStatusDTO>("Bad value for PhoneNumber");

            var accountUser =
                DbLayer.AccountUsersRepository.GetAccountUserByeVerifiedPhoneNumber(phoneNumber);

            if(accountUser == null)
                accountUser = DbLayer.AccountUsersRepository.GetAccountUserByeNoVerifiedPhoneNumber(phoneNumber);

            return accountUser == null 
                ? NotFound<AccountUserIdAndVerifireStatusDTO>($"Account user with phoneNumber: {phoneNumber} not found") 
                : Ok(new AccountUserIdAndVerifireStatusDTO 
                {
                    AccountUserID = accountUser.Id,
                    IsVerified = accountUser.IsPhoneVerified.HasValue? accountUser.IsPhoneVerified.Value : false,
                });
        }

        public ManagerResult<bool> IsLastAdminAccount(Guid accountId)
        {
            var count = DbLayer.AccountUsersRepository
                .AsQueryableNoTracking()
                .Count(user => user.AccountId == accountId && user.AccountUserRoles.Any(item => item.AccountUserGroup == AccountUserGroup.AccountAdmin));

            logger.Info($"В аккаунте {accountId} найдено администраторов {count}");

            return Ok(count == 1);
        }

        /// <summary>Get all account user id for given account</summary>
        public ManagerResult<ResultDto<List<Guid>>> GetIDs(Guid accountId)
        {
            var timer = new Stopwatch();
            timer.Start();
            AccessProvider.HasAccess(ObjectAction.AccountUsers_GetIDs, () => accountId);
            timer.Stop();
            var headString = "Auth:" + timer.ElapsedMilliseconds;


            // try to find account
            timer.Restart();
            var account = DbLayer.AccountsRepository.GetAccount(accountId);
            timer.Stop();
            headString += "&ReadDBA:" + timer.ElapsedMilliseconds;

            if (account == null)
                return NotFound<ResultDto<List<Guid>>>("Account not found.");

            // try to get account users id
            timer.Restart();
            var userList = DbLayer.AccountUsersRepository.GetAllAccountUsers(account.Id);
            timer.Stop();
            headString += "&ReadDBAU:" + timer.ElapsedMilliseconds;

            return !userList.Any() ? NotFound<ResultDto<List<Guid>>>("AccountUsers not found in this account.") :
                // cover it with result model
                Ok(new ResultDto<List<Guid>>{Data = userList.Select(x => x.Id).ToList(), Head = headString});
        }

        /// <summary></summary>
        public async Task<ManagerResult> SetPhoneNumber(PhoneNumberModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);

            var phoneNumberValidation = new PhoneNumberValidation(model.PhoneNumber);

            if (!model.AccountUserID.HasValue)
                return Ok();
            try
            {
                var user = GetAccountUser(model.AccountUserID.Value);

                if (!await phoneNumberValidation.PhoneNumberIsAvailable(DbLayer))
                {
                    return Conflict("Phone Number already exist");
                }

                user.PhoneNumber = model.PhoneNumber;
                UpdateUser(user);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки номера телефона для пользователя]");
                return BadRequest("Method execution failed");
            }

            return Ok();
        }


        public ManagerResult SetFirstName(AccountUserFirstNameModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);
            if (model.FirstName == null)
                return PreconditionFailed("FirstName is null");
            try
            {
                var user = GetAccountUser(model.AccountUserID);
                if (user == null)
                    return NotFound($"Account user with id: {model.AccountUserID} not found");

                // update user
                user.FirstName = model.FirstName;
                // refresh fullname prop
                user.FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}";
                UpdateUser(user);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки имени для пользователя]");
                return BadRequest("Method execution failed");
            }

            return Ok();
        }

        public ManagerResult SetLastName(AccountUserLastNameModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);
            if (model.LastName == null)
                return PreconditionFailed("LastName is null");
            try
            {
                var user = GetAccountUser(model.AccountUserID);
                if (user == null)
                    return NotFound($"Account user with id: {model.AccountUserID} not found");

                // update user
                user.LastName = model.LastName;
                // refresh fullname prop
                user.FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}";
                UpdateUser(user);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка установки фамилии для пользователя]");
                return BadRequest("Method execution failed");
            }

            return Ok();
        }

        public ManagerResult SetMiddleName(AccountUserMiddleNameModelDto model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);
            if (model.MiddleName == null)
                return PreconditionFailed("MiddleName is null");
            try
            {
                var user = GetAccountUser(model.AccountUserID);
                if (user == null)
                    return NotFound($"Account user with id: {model.AccountUserID} not found");

                // update user
                user.MiddleName = model.MiddleName;
                // refresh fullname prop
                user.FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}";
                UpdateUser(user);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка редактирвоания пользователя]");
                return BadRequest("Method execution failed");
            }

            return Ok();
        }

        public ManagerResult SetActivated(AccountUserActivatedModel model)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => model.AccountUserID);
            try
            {
                var user = GetAccountUser(model.AccountUserID);

                if (user == null)
                    return NotFound($"Account user with id: {model.AccountUserID} not found");

                user.Activated = model.Activated;
                UpdateUser(user);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка редактирвоания пользователя]");
                return BadRequest("Method execution failed");
            }

            return Ok();
        }
        
        public async Task<ManagerResult> Unsubscribed(string key, string type)
        {
            try
            {
                logger.Trace("Убирем подписку для пользователя с ключом {0}, тип = {1}", key, type);

                var resetCode = type == nameof(GuidEncryption)
                    ? GuidEncryption.Decrypt(key)
                    : SimpleIdHash.GetIdByString(key);

                if (!resetCode.HasValue)
                    return PreconditionFailed($"Невозможно прочитать ID по хешу ({key})");

                var accountUser = await DbLayer.AccountUsersRepository.Unsubscribe(resetCode.Value);

                logger.Info("Отписка от рассылки. Выполнено. Id = {0}", resetCode);

                LogEventHelper.LogEventInitiator(DbLayer, accountUser.AccountId, LogActions.MailNotification, "Клиент отказался от рассылки", accountUser.Id, handlerException);

                return Ok();
            }
            catch (Exception ex)
            {
                logger.Warn("Ошибка при отписке от рассылки. Ключ={0}. Описание ошибки: {1}", key, ex.Message);

                return PreconditionFailed(ex.Message);
            }
        }

        public ManagerResult<AccountUserDto> Get(Guid id)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, null, () => id);
            var userToEdit = DbLayer.AccountUsersRepository.WhereLazy()
                .Include(i => i.Account)
                .Include(i => i.AccountUserRoles)
                .FirstOrDefault(au => au.Id == id);

            return Ok(userToEdit.MapToAccountUserDc());
        }

        public Domain.DataModels.AccountUser GetCurrent()
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => AccessProvider.ContextAccountId);
            var userToEdit = DbLayer.AccountUsersRepository.GetByLogin(AccessProvider.Name);
            return userToEdit;
        }

        public AccountUserDto Sync(Guid accountUserId, out bool isUpdatedByCorp)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_Sync, null, () => accountUserId);

            var user = Get(accountUserId).Result;
            if (user.CorpUserSyncStatus == "UpdatedByCorp")
            {
                user.CorpUserSyncStatus = nameof(Sync);
                DbLayer.Save();
                isUpdatedByCorp = true;
            }
            else
            {
                isUpdatedByCorp = true;
            }

            return user;
        }

        public ManagerResult<List<AccountUserPropertiesDto>> GetAccountUsersList(Guid accountId)
        {
            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => accountId);

            var list = DbLayer.AccountUsersRepository.Where(x => x.AccountId == accountId).Where(z => z.Activated);

            var answer = new List<AccountUserPropertiesDto>();
            foreach (var x in list)
            {
                var isAnyResources = resourceDataProvider.GetResourcesLazy(Clouds42Service.MyEnterprise,
                res => res.Subject == x.Id,
                ResourceType.MyEntUserWeb,
                ResourceType.MyEntUser).Any();

                answer.Add(new AccountUserPropertiesDto
                {
                    ID = x.Id,
                    AccountId = x.AccountId,
                    Login = x.Login ?? string.Empty,
                    Email = x.Email ?? string.Empty,
                    FirstName = x.FirstName ?? string.Empty,
                    LastName = x.LastName ?? string.Empty,
                    MiddleName = x.MiddleName ?? string.Empty,
                    CorpUserID = x.CorpUserID,
                    CorpUserSyncStatus = x.CorpUserSyncStatus ?? string.Empty,
                    Removed = x.Removed.HasValue && x.Removed.Value,
                    FullPhoneNumber = x.PhoneNumber ?? string.Empty,
                    CreationDate = x.CreationDate.HasValue ? x.CreationDate.Value.ToString("O") : string.Empty,
                    Activated = isAnyResources,
                    IsManager  = x.AccountUserRoles.Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin)
                });

            }
                

            return Ok(answer);
        }

        private Domain.DataModels.AccountUser GetAccountUser(Guid accountUserId)
        {
            return DbLayer.AccountUsersRepository.Where(u => u.Id == accountUserId).SingleOrDefault();
        }

        private void UpdateUser(Domain.DataModels.AccountUser user)
        {
            if (user == null)
                return;

            user.EditDate = DateTime.Now;

            DbLayer.AccountUsersRepository.Update(user);
            DbLayer.Save();
        }

        public ManagerResult<ResultDto<bool>> IsAccountAdmin(Guid accountUserId)
        {
            var timer = new Stopwatch();
            timer.Start();

            var accountId = AccountIdByAccountUser(accountUserId);
            if (accountId == null)
            {
                var message = $"По номеру пользователя {accountUserId} не удалось получить номер аккаунта";
                logger.Warn(message);
                throw new InvalidOperationException(message);
            }

            AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => accountId);
            timer.Stop();
            var headString = "Auth:" + timer.ElapsedMilliseconds;

            timer.Restart();
            var accountAdmins = AccessProvider.GetAccountAdmins(accountId.Value);
            timer.Stop();
            headString += "&ReadDB:" + timer.ElapsedMilliseconds;

            return Ok(new ResultDto<bool>{Data = accountAdmins.Any(au => au == accountUserId), Head = headString});
        }

        public bool IsManager(Guid accountUserId)
        {
            var isManager = DbLayer.AccountUserRoleRepository.Where(x => x.AccountUserId == accountUserId)
                .Any(z => z.AccountUserGroup == AccountUserGroup.AccountAdmin);
            return isManager;
        }

        /// <summary>
        /// Получить информацию о заблокированном сервисе для аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Информация о заблокированном сервисе</returns>
        public ManagerResult<string> GetLockServiceInformation(Guid accountId)
        {
            AccessProvider.HasAccessBool(ObjectAction.AccountUsers_LockServiceInformation, () => accountId);

            try
            {
                var resConfig = resourcesService.GetResourceConfig(accountId, Clouds42Service.MyEnterprise);

                return Ok(resConfig is { FrozenValue: true } ? cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.Rent1C,accountId) : string.Empty);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка получения информации о заблокированном сервисе] для аккаунта {accountId} произошла ошибка {ex.Message}");
                return Ok(string.Empty);
            }
        }
    }
}
