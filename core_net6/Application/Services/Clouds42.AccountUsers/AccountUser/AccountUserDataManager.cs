﻿using Clouds42.Accounts.Contracts.Account;
using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Менеджер для работы с данными пользователя
    /// </summary>
    public class AccountUserDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountUserDataProvider accountUserDataProvider,
        AccountUserDataHelper accountUserDataHelper,
        IHandlerException handlerException,
        IAccountDataProvider accountDataProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {

        /// <summary>
        /// Получить хэш пароля по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Хэш пароля пользователя</returns>
        public ManagerResult<string> GetPasswordHashByLogin(string login)
        {
            try
            {
                logger.Info($"Получение пароля по логину {login}");
                AccessProvider.HasAccess(ObjectAction.AccountUsers_GetPasswordHash);

                if (!accountUserDataHelper.AccountUserIsExists(login))
                {
                    var message = $"Пользователь по логину {login} не найден";
                    logger.Warn(message);
                    return PreconditionFailed<string>(message);
                }

                var password = accountUserDataProvider.GetPasswordHashByLogin(login);
                return Ok(password);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка получения пароля по логину]");
                return PreconditionFailed<string>(ex.Message);
            }
        }

        /// <summary>
        /// Получить информацию о админе аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Информация о админе аккаука</returns>
        public string GetAccountAdminInfo(Guid accountId)
            => AccessProvider.HasAccessBool(ObjectAction.Payments_View, () => accountId)
                ? ""
                : accountDataProvider.GetAccountAdminDescription(accountId);
    }
}
