﻿namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Константы для хэлпера чата Битрикс24
    /// </summary>
    public static class BitrixChatHelperConstants
    {
        /// <summary>
        /// Название ключа для логина пользователя
        /// </summary>
        public const string UserLoginKeyName = "Логин";

        /// <summary>
        /// Название ключа для номера аккаунта пользователя
        /// </summary>
        public const string UserAccountNumberKeyName = "Номер аккаунта";

        /// <summary>
        /// Название ключа для имени пользователя
        /// </summary>
        public const string UserFullNameKeyName = "Имя";

        /// <summary>
        /// Название ключа для страницы, с которой пришел пользователь
        /// </summary>
        public const string UserRequestedSiteUriKeyName = "Страница сайта";

        /// <summary>
        /// Название ключа для дефолтного значения
        /// </summary>
        public const string UndefinedValue = "Не определено";

        /// <summary>
        /// Название ключа для типа отображения в Битрикс24
        /// </summary>
        public const string InlineDisplayTypeName = "LINE";
    }
}
