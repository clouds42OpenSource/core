﻿using Clouds42.AccountUsers.AccountUser.Helper;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser
{
    public class AccountUsersSearchManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        AccountUsersSearchHelper accountUsersSearchHelper)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Считывает пользователей аккаунта с БД в соответствии условию
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ManagerResult<UsersSeachResultModel> GetAccountUsersSearchResult(Guid accountId, AccountUsersFilterModel filter)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => AccessProvider.ContextAccountId);
                var searchResult = accountUsersSearchHelper.GetAccountUsersSeachResult(accountId, filter);
                return Ok(searchResult);
            }
            catch (Exception exception)
            {
                return PreconditionFailed<UsersSeachResultModel>(exception.Message);
            }
        }

    }
}
