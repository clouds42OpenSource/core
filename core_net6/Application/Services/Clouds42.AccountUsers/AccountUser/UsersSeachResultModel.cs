﻿using Clouds42.DataContracts.AccountUser;

namespace Clouds42.AccountUsers.AccountUser
{
    /// <summary>
    /// Результат поиска юзеров (отфильтрованная коллекция и общее количество)
    /// </summary>
    public class UsersSeachResultModel
    {
        public List<AccountUserDto> Users { get; set; }

        public int AccountUsersCount { get; set; }
    }
}
