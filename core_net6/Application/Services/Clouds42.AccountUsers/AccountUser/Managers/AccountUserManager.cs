﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Managers;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountUser;
using Clouds42.DataContracts.AccountUser.Filters;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;


namespace Clouds42.AccountUsers.AccountUser.Managers
{
    /// <summary>
    /// Менеджер для работы с пользователем
    /// </summary>
    public class AccountUserManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountUserProvider accountUserProvider,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountUserManager
    {

        /// <summary>
        /// Проверить текущий пароль
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <param name="password">Текущий пароль</param>
        /// <returns>Результат проверки</returns>
        public ManagerResult<bool> CheckCurrentPassword(Guid userId, string password)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_Edit, null, () => userId);
                return Ok(accountUserProvider.CheckCurrentPassword(userId, password));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Проверка текущего пароля для пользователя {userId} завершилась ошибкой]");
                return PreconditionFailed<bool>(ex.Message);
            }
        }

        /// <summary>
        /// Получить пагинацию пользователей аккаунта
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="request">Детали запроса пагинации</param>
        /// <returns>Результат запроса</returns>
        public ManagerResult<SelectDataResultCommonDto<AccountUserDto>> GetPaginatedUsersForAccount(Guid accountId,
            PagniationRequestDto<AccountUsersFilterDto> request)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountUsers_View, () => accountId);
                return Ok(accountUserProvider.GetPaginatedUsersForAccount(accountId, request));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Ошибка при получении пользователей аккаунта]", () => accountId);
                return PreconditionFailed<SelectDataResultCommonDto<AccountUserDto>>(ex.Message);
            }
        }
    }
}
