﻿using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUser.Managers
{

    /// <summary>
    /// Менеджер разрешений юзера
    /// </summary>
    public class AccountUserPermissionsManager(
        IAccessProvider accessProvider,
        IAccountUserPermissionsProvider accountUserPermissionsProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        readonly IAccountUserPermissionsProvider _accountUserPermissionsProvider = accountUserPermissionsProvider ??
                                                                                   throw new ArgumentNullException(nameof(accountUserPermissionsProvider));

        /// <summary>
        /// Получить разрешения юзера для указаных таргетов
        /// </summary>
        /// <param name="accountUserId">ID пользователя, разрешения которго проверяются</param>
        /// <param name="targetAccountId">ID аккаунта для операций над которым проверяются разрешения</param>
        /// <param name="targetAccountUserId">ID пользователя для операций над которым проверяются разрешения</param>
        /// <param name="filterPermissions">Фильтр проверяемых разрешений. Если NULL - вернутся все доступные разрешения</param>
        /// <returns>Manager Result со списком доступных разрешений</returns>
        public ManagerResult<List<ObjectAction>> GetUserPermissionsForTargets(
            Guid accountUserId,
            Guid? targetAccountId,
            Guid? targetAccountUserId,
            IEnumerable<ObjectAction> filterPermissions = null)
        {
            try
            {
                AccessProvider.HasAccess(
                    ObjectAction.AccountUserRoles_View,
                    getAccountUserId: () => accountUserId);
                return Ok(_accountUserPermissionsProvider.GetUserPermissionsForTargets(
                    accountUserId,
                    targetAccountId,
                    targetAccountUserId,
                    filterPermissions
                    ));
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Проверка разрешений для пользователя {accountUserId} завершилась ошибкой]");
                return PreconditionFailed<List<ObjectAction>>(ex.Message);
            }
        }
    }
}
