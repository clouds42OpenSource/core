﻿using System.Text;
using Clouds42.AccountUsers.Contracts.AccountUser.Interfaces.Providers;
using Clouds42.AccountUsers.Contracts.AccountUser.Models;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataValidations;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountUsers.AccountUser.Managers
{
    /// <summary>
    /// Менеджер управления нотификациями по сбросу пароля для аккаунт юзера
    /// </summary>
    public class AccountUserResetPasswordNotificationManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IAccountUserResetPasswordNotificationProvider accountUserResetPasswordNotificationProvider,
        IHandlerException handlerException,
        AccountUsersProfileManager accountUsersProfileManager)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        ///     Сформировать ResetToken и отправить брендированное письмо
        /// </summary>
        /// <param name="email">Почтовый адрес пользователя</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult SendPasswordChangeRequest(string email)
        {
            if (string.IsNullOrEmpty(email))
                return PreconditionFailed("Значение почты пустое");

            if (!new EmailValidation(email).EmailMaskIsValid())
                return PreconditionFailed("Неверный формат эл. почты");

            var accountId = GetAccountIdByUserEmail(email);
            if (!accountId.HasValue)
                return NotFound($"Не удалось определить аккаунт пользователя по email {email}");

            try
            {
                accountUserResetPasswordNotificationProvider.SendPasswordChangeRequest(email);
                Logger.Info($"Запрос на сброс пароля отправлен на почту {email}");
                return Ok();
            }
            catch (Exception ex)
            {
                LogEventHelper.LogEvent(DbLayer, accountId.Value, AccessProvider, LogActions.MailNotification,
                    $"Ошибка отправки письма \"Восстановление пароля\" на адрес {email}. Причина: {ex.Message}");
                handlerException.Handle(ex, $"[Ошикбка отправки письма восстановления пароля] на адрес {email}");
                return PreconditionFailed(ex.Message); 
            }
        }


        /// <summary>
        ///     Сформировать ResetToken и отправить брендированное письмо
        /// </summary>
        /// <param name="model">Почтовый адрес пользователя</param>
        /// <returns>Результат выполнения операции</returns>
        public ManagerResult SetNewPassword(ConfirmPasswordModel model)
        {
            try
            {
                var accountUserSession = dbLayer.AccountUserSessionsRepository.AsQueryableNoTracking()
                    .Include(i => i.AccountUser)
                    .FirstOrDefault(s => s.Token == model.LoginToken);

                if (accountUserSession == null)
                    return Ok(new ResponseResult(false, $"Не удалось найти сессию пользователя по токену {model.LoginToken}"));


                var passwordValidationResult = new PasswordValidation(model.Password).PasswordIsValid();
                if (!passwordValidationResult.DataIsValid)
                {
                    var validationErrorMessage = new StringBuilder();
                    passwordValidationResult.ValidationMessages.ForEach(message =>
                    {
                        validationErrorMessage.Append(message);
                    });
                    return ValidationError(validationErrorMessage.ToString());
                }

                var result = accountUsersProfileManager.SetNewUserPassword(accountUserSession.AccountUser.Login, model.Password);
                return result;
            }
            catch (Exception ex)
            {
                
                handlerException.Handle(ex, $"[Ошикбка установки нового пароля] для пользователя {model.Login}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
