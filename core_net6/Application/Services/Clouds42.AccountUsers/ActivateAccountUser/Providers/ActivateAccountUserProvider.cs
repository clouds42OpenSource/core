﻿using Clouds42.AccountUsers.Contracts.ActivateAccountUser.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.ActivateAccountUser.Providers
{
    /// <summary>
    /// Провайдер активации пользователей
    /// </summary>
    internal class ActivateAccountUserProvider(IUnitOfWork dbLayer) : IActivateAccountUserProvider
    {
        /// <summary>
        /// Активировать по номеру телефона
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="confirmationCode">Код подтверждения</param>
        /// <returns>Результат активации</returns>
        public bool ActivateUserByPhone(Guid accountUserId, string confirmationCode)
        {
            var user = dbLayer.AccountUsersRepository.Where(u => u.Id == accountUserId).FirstOrDefault()
                       ?? throw new NotFoundException("Пользователь с данным логином не найден");

            if (!user.Activated)
                throw new UserNotActivatedException("Пользователь заблокирован");

            if (user.ConfirmationAttemptsLeft is null or < 1)
                return false;

            user.ConfirmationAttemptsLeft--;

            if (user.ConfirmationCode == confirmationCode)
            {
                user.ConfirmationAttemptsLeft = 0;
                user.Activated = true;
                dbLayer.AccountUsersRepository.Update(user);
                dbLayer.Save();

                return true;
            }

            dbLayer.AccountUsersRepository.Update(user);
            dbLayer.Save();
            return false;
        }
    }
}
