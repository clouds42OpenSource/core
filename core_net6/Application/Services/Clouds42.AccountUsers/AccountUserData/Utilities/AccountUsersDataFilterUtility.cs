﻿using Clouds42.Common.DataModels;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.Domain.Access;

namespace Clouds42.AccountUsers.AccountUserData.Utilities
{
    /// <summary>
    /// Утилита для фильтра моделей данных пользователей аккаунта
    /// </summary>
    public static class AccountUsersDataFilterUtility
    {
        /// <summary>
        /// Список групп пользователя у которых есть доступ к отображению всех пользователей аккаунта
        /// </summary>
        private static readonly List<AccountUserGroup> GroupsWithAccessToDisplayAllUsers =
        [
            AccountUserGroup.AccountAdmin,
            AccountUserGroup.AccountSaleManager,
            AccountUserGroup.Hotline,
            AccountUserGroup.CloudAdmin,
            AccountUserGroup.CloudSE
        ];

        /// <summary>
        /// Применить фильтр для моделей данных пользователей аккаунта
        /// </summary>
        /// <param name="userItems">Модели пользователей аккаунта</param>
        /// <param name="accountUsersFilter">Модель фильтра пользователей аккаунта</param>
        /// <param name="currentUser">Текущий пользователь(авторизованный)</param>
        /// <returns>Отфильтрованные модели данных пользователей аккаунта</returns>
        public static IQueryable<AccountUserDataItemDto> ApplyFilter(this IQueryable<AccountUserDataItemDto> userItems,
            AccountUsersFilterDto accountUsersFilter, IUserPrincipalDto currentUser)
        {
            if (!currentUser.HasAccessToDisplayAllUsers())
                return userItems.Where(userItem => userItem.Id == currentUser.Id);

            return userItems.ApplyFilterBySearchString(accountUsersFilter.SearchString);
        }

        /// <summary>
        /// Проверить что у пользователя есть доступ на отображение(просмотр) всех пользователей аккаунта
        /// </summary>
        /// <param name="currentUser">Текущий пользователь</param>
        /// <returns>Есть доступ на отображение всех пользователей</returns>
        private static bool HasAccessToDisplayAllUsers(this IUserPrincipalDto currentUser) =>
            currentUser.Groups.Intersect(GroupsWithAccessToDisplayAllUsers).AsQueryable().Any();

        /// <summary>
        /// Применить фильтр для моделей данных пользователей аккаунта по строке поиска
        /// </summary>
        /// <param name="userItems">Модели пользователей аккаунта</param>
        /// <param name="searchString">Строка поиска</param>
        /// <returns></returns>
        private static IQueryable<AccountUserDataItemDto> ApplyFilterBySearchString(
            this IQueryable<AccountUserDataItemDto> userItems, string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
                return userItems;

            var searchPhoneString = PhoneHelper.ClearNonDigits(searchString);

            return from userItem in userItems
                   where userItem.Login != null && userItem.Login.ToLower().Contains(searchString.ToLower()) ||
                         userItem.FirstName != null && userItem.FirstName.ToLower().Contains(searchString.ToLower()) ||
                         userItem.LastName != null && userItem.LastName.ToLower().Contains(searchString.ToLower()) ||
                         userItem.MiddleName != null && userItem.MiddleName.ToLower().Contains(searchString.ToLower()) ||
                         userItem.FullName != null && userItem.FullName.ToLower().Contains(searchString.ToLower()) ||
                         userItem.Email != null && userItem.Email.ToLower().Contains(searchString.ToLower()) ||
                         !string.IsNullOrEmpty(searchPhoneString) && searchPhoneString.Length >= 5 &&
                         userItem.PhoneNumber != null && userItem.PhoneNumber.Contains(searchPhoneString)
                   select userItem;
        }
    }
}
