﻿using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.BaseModel;
using PagedList.Core;
using Clouds42.AccountUsers.AccountUserData.Utilities;
using Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces;
using Clouds42.Accounts.Contracts.Account;
using Clouds42.Common.DataModels;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.Domain.DataModels.AccountProperties;
using Clouds42.Domain.IDataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountUsers.AccountUserData.Providers
{
    /// <summary>
    /// Провайдер для работы с данными пользователей аккаунта
    /// </summary>
    internal class AccountUsersDataProvider(
        IUnitOfWork dbLayer,
        IAccessProvider accessProvider,
        IAccountDataProvider accountDataProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountUsersDataProvider
    {
        private readonly Lazy<string> _marketSourceUrl = new(CloudConfigurationProvider.Market.GetUrlWithSource);

        /// <summary>
        /// Получить данные пользователей аккаунта по фильтру
        /// </summary>
        /// <param name="args">Параметры фильтра для поиска пользователей аккаунта</param>
        /// <returns>Модель данных пользователей аккаунта</returns>
        public AccountUsersDataDto GetAccountUsersDataByFilter(AccountUsersFilterParamsDto args)
        {
            var accountUsersData = SelectAccountUsers(args.Filter.AccountId).ApplyFilter(args.Filter, GetCurrentUser())
                .OrderBy(data => data.Login).MakeSorting(args.SortingData);

            return new AccountUsersDataDto
            {
                Pagination = new PaginationBaseDto(args.PageNumber ?? 1, accountUsersData.Count(), args.Filter?.PageSize ?? 50),
                Records = accountUsersData.ToPagedList(args.PageNumber ?? 1, args.Filter?.PageSize ?? 50).ToArray()
            };
        }

        /// <summary>
        /// Найти пользователя
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Пользователь</returns>
        public IAccountUser? SearchAccountUser(string searchString)
        {
            var phoneNumber = GetPhoneNumberFromSearchString(searchString);

            return dbLayer.AccountUsersRepository
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefault(accountUser => accountUser.Login == searchString || 
                                               accountUser.Email == searchString ||
                                               !string.IsNullOrEmpty(phoneNumber) && 
                                               accountUser.PhoneNumber == phoneNumber);
        }

        /// <summary>
        /// Получить детальную информацию о пользователе аккаунта
        /// </summary>
        /// <param name="accountUserId">Id пользователя</param>
        /// <param name="contextAccountId">ID аккаунта, в контексте которого находится текущий пользователь</param>
        /// <returns>Детальная информацию о пользователе аккаунта</returns>
        public AccountUserDetailedInfoDto? GetAccountUserDetailedInfo(Guid accountUserId, Guid contextAccountId)
        {
            var marketSourceUrl = _marketSourceUrl.Value;
            var mainPageNotification = dbLayer.CloudCoreRepository.FirstOrDefault()?.MainPageNotification;
            var currentAccountUser =
                dbLayer.AccountUsersRepository.FirstOrThrowException(acUs => acUs.Id == accountUserId,
                    $"Пользователь по ID {accountUserId} не найден");

            var account = accountConfigurationDataProvider
                .GetAccountConfigurationsDataByLocale().FirstOrDefault(x => x.Account.Id == contextAccountId);

            if(account == null)
            {
                return null;
            }

            var accountRequisites = dbLayer.GetGenericRepository<AccountRequisites>().FirstOrDefault(z => z.AccountId == account.Account.Id);
            var admins = accountDataProvider.GetAdministratorsGroupedByAccount(account.Account.Id);

            var dto = new AccountUserDetailedInfoDto
            {
                AccountId = account.Account.Id,
                Email = currentAccountUser.Email,
                FirstName = currentAccountUser.FirstName,
                Login = currentAccountUser.Login,
                AccountUserId = currentAccountUser.Id,
                AccountUserCompanyData = new AccountUserCompanyDataDto
                {
                    AccountCaption = account.Account.AccountCaption,
                    AccountInn = accountRequisites.Inn,
                    AccountLocaleName = account.Locale.Name,
                    AccountAdministrators = admins?.Select(user =>
                        new AccountAdministratorInfoDto
                        {
                            Email = user.Email,
                            PhoneNumber = user.PhoneNumber,
                            LastName = user.LastName,
                            MiddleName = user.MiddleName,
                            FirstName = user.FirstName
                        }).ToList()
                },
                AdditionalInfo = new AdditionalInfoForAccountUserDto
                {
                    MarketUrlWithSource = marketSourceUrl,
                    MainPageNotification = mainPageNotification
                }
            };

            return dto;
        }

        /// <summary>
        /// Получить номер телефона из строки поиска
        /// </summary>
        /// <param name="searchString">Строка поиска</param>
        /// <returns>Номер телефона</returns>
        private static string GetPhoneNumberFromSearchString(string searchString)
        {
            var reg = new Regex(@"^\+?([\d]{10,13})$", RegexOptions.None, TimeSpan.FromSeconds(.5));
            var match = reg.Match(searchString);
            var phoneNumber = match.Groups[1].Value;
            return PhoneHelper.ClearNonDigits(phoneNumber);
        }

        /// <summary>
        /// Выбрать пользователей аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Пользователи аккаунта</returns>
        private IQueryable<AccountUserDataItemDto> SelectAccountUsers(Guid accountId) =>
            from accountUser in dbLayer.AccountUsersRepository.GetNotDeletedAccountUsersQuery()
            join accountUserRole in dbLayer.AccountUserRoleRepository.WhereLazy() on accountUser.Id equals
                accountUserRole.AccountUserId into accountUserRoles
            where accountUser.AccountId == accountId
            select new AccountUserDataItemDto
            {
                Id = accountUser.Id,
                Email = accountUser.Email,
                Login = accountUser.Login,
                PhoneNumber = accountUser.PhoneNumber,
                LastName = accountUser.LastName,
                MiddleName = accountUser.MiddleName,
                FirstName = accountUser.FirstName,
                IsActivated = accountUser.Activated,
                FullName = accountUser.FullName ??
                           accountUser.LastName + " " + accountUser.FirstName + " " + accountUser.MiddleName,
                IsAccountAdmin =
                    accountUserRoles.Any(role => role.AccountUserGroup == AccountUserGroup.AccountAdmin)
            };

        /// <summary>
        /// Получить текущего пользователя
        /// </summary>
        /// <returns>Текущий пользователь</returns>
        private IUserPrincipalDto GetCurrentUser()
        {
            var currentUser = accessProvider.GetUser();

            if (currentUser == null)
                throw new InvalidOperationException("Не удалось получить текущего пользователя");

            return currentUser;
        }
    }
}
