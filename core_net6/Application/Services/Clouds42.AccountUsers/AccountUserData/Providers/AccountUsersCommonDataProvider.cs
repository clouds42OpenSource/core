﻿using Clouds42.AccountUsers.Contracts.AccountUserData.Interfaces;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.Domain.Access;
using Clouds42.MyDatabaseService.Contracts.Data.Interfaces;

namespace Clouds42.AccountUsers.AccountUserData.Providers
{
    /// <summary>
    /// Провайдер для работы с общими/базовыми
    /// данными пользователей аккаунта
    /// </summary>
    internal class AccountUsersCommonDataProvider(
        IAccessProvider accessProvider,
        IMyDatabasesServiceDataProvider myDatabasesServiceDataProvider)
        : IAccountUsersCommonDataProvider
    {
        /// <summary>
        /// Получить общие/базовые данные для работы с пользователями аккаунта
        /// </summary>
        /// <returns>Общие/базовые данные для работы с пользователями аккаунта</returns>
        public CommonDataForWorkingWithUsersDto GetCommonDataForWorkingWithUsers()
        {
            var accountId = accessProvider.ContextAccountId;

            return new CommonDataForWorkingWithUsersDto
            {
                AccountId = accountId,
                MyDatabaseServiceId = myDatabasesServiceDataProvider.GetMyDatabasesServiceId(),
                PermissionsForWorkingWithUsers = GetPermissionsForWorkingWithUsers(accountId)
            };
        }

        /// <summary>
        /// Получить модель разрешений для работы с пользователями аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Модель разрешений для работы с пользователями аккаунта</returns>
        private PermissionsForWorkingWithUsersDto GetPermissionsForWorkingWithUsers(Guid accountId) =>
            new()
            {
                HasPermissionForAddAccountUser =
                    accessProvider.HasAccessBool(ObjectAction.AccountUsers_Add, () => accountId)
            };
    }
}