﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;
using Clouds42.Domain.DataModels.billing;

namespace Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными услуги сервиса "Мои информационные базы"
    /// </summary>
    public interface IMyDatabasesServiceTypeDataProvider
    {
        /// <summary>
        /// Получить услугу по Id базы
        /// </summary>
        /// <param name="databaseId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        BillingServiceType GetServiceTypeByDatabaseId(Guid databaseId);

        /// <summary>
        /// Получить услугу по Id шаблона базы
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        BillingServiceType GetServiceTypeByDbTemplateId(Guid dbTemplateId);

        /// <summary>
        /// Получить Id услуги по Id базы
        /// </summary>
        /// <param name="databaseId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        Guid? GetServiceTypeIdByDatabaseId(Guid databaseId);

        /// <summary>
        /// Получить Id услуги по Id шаблона базы
        /// </summary>
        /// <param name="dbTemplateId">Id шаблона информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        Guid? GetServiceTypeIdByDbTemplateId(Guid dbTemplateId);

        /// <summary>
        /// Получить Id услуги по имени конфигурации базы
        /// </summary>
        /// <param name="configurationName">имя конфигурации информационной базы</param>
        /// <returns>Услуга сервиса "Мои информационные базы"</returns>
        Guid? GetServiceTypeIdByConfigurationName(string configurationName);

        /// <summary>
        /// Получить ID услуги "Доступ в серверную базу"
        /// </summary>
        /// <returns>ID услуги "Доступ в серверную базу"</returns>
        Guid GetAccessToServerDatabaseServiceTypeId();

        /// <summary>
        /// Получить данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы
        /// </summary>
        /// <returns>Данные связей услуги сервиса "Мои информационные базы"
        /// с шаблоном инф. базы</returns>
        IQueryable<DbTemplateServiceTypeRelationDataDto> GetDbTemplateServiceTypeRelationsData();

        /// <summary>
        /// Получить данные стоимости услуги для инф. базы(по конфигурации базы)
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных стоимости услуги для инф. базы(по конфигурации базы)</returns>
        ServiceTypeCostDataForDatabaseDto GetServiceTypeCostDataForDatabase(Guid databaseId);

        /// <summary>
        /// Получить данные услуг сервиса "Мои инф. базы"
        /// для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные услуг сервиса "Мои инф. базы"
        /// для аккаунта</returns>
        IQueryable<MyDatabasesServiceTypeDataDto> GetMyDatabasesServiceTypesData(Guid accountId);

        /// <summary>
        /// Получить данные стоимости услуги "Доступ в серверную базу" по инф. базе
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        /// <returns>Модель данных стоимости услуги "Доступ в серверную базу" по инф. базе</returns>
        ServiceTypeCostDataForDatabaseDto GetAccessToServerDatabaseCostData(Guid databaseId);
    }
}
