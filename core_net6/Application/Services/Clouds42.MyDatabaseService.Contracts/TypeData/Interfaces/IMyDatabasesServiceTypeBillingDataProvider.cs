﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;

namespace Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными билинга услуги сервиса "Мои инф. базы"
    /// </summary>
    public interface IMyDatabasesServiceTypeBillingDataProvider
    {
        /// <summary>
        /// Получить данные биллинга для услуг сервиса "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Данные биллинга для услуг сервиса "Мои инф. базы"</returns>
        List<MyDatabasesServiceTypeBillingDataDto> GetBillingDataForMyDatabasesServiceTypes(Guid accountId);
    }
}
