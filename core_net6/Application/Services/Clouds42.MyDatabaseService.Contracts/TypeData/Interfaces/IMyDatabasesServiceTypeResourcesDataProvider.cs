﻿using Clouds42.DataContracts.Billing;

namespace Clouds42.MyDatabaseService.Contracts.TypeData.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными ресурсов услуги сервиса "Мои инф. базы"
    /// </summary>
    public interface IMyDatabasesServiceTypeResourcesDataProvider
    {
        /// <summary>
        /// Получить данные по ресурсам услуг сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="payPeriod">Период оплаты</param>
        /// <returns>Данные по ресурсам услуг</returns>
        List<BillingServiceTypeResourceDto> GetResourcesDataForMyDatabasesServiceTypes(Guid accountId, int payPeriod);
    }
}
