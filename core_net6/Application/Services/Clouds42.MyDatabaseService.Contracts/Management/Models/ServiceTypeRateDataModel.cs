﻿namespace Clouds42.MyDatabaseService.Contracts.Management.Models
{
    /// <summary>
    /// Модель данных тарифа по услуге
    /// </summary>
    public class ServiceTypeRateDataModel
    {
        /// <summary>
        /// Id аккаунта для которому принадлежит тариф
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id услуги
        /// </summary>
        public Guid ServiceTypeId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string ServiceTypeName { get; set; }

        /// <summary>
        /// Стоимость
        /// </summary>
        public decimal Cost { get; set; }
    }
}
