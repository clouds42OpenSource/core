﻿using System.Text;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement;

namespace Clouds42.MyDatabaseService.Contracts.Management.Interfaces
{
    /// <summary>
    /// Провайдер для управления сервисом "Мои информационные базы"
    /// </summary>
    public interface IManageMyDatabasesServiceProvider
    {
        /// <summary>
        /// Выполнить управление сервисом "Мои информационные базы"
        /// </summary>
        /// <param name="model">Модель для управления сервисом "Мои информационные базы"</param>
        /// <param name="changeMessageBuilder">Билдер сообщений об изменениях</param>
        void Manage(ManageMyDatabasesServiceDto model, StringBuilder changeMessageBuilder);
    }
}
