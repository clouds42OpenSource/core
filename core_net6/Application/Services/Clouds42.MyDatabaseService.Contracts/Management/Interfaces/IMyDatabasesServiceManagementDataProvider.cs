﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceManagement;
using BillingAccountEntity = Clouds42.Domain.DataModels.billing.BillingAccount;

namespace Clouds42.MyDatabaseService.Contracts.Management.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными управления
    /// сервиса "Мои информационные базы"
    /// </summary>
    public interface IMyDatabasesServiceManagementDataProvider
    {
        /// <summary>
        /// Получить данные по управлению
        /// сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="billingAccount">Аккаунт биллинга</param>
        /// <returns>Данные по управлению
        /// сервиса "Мои информационные базы"</returns>
        MyDatabasesServiceManagementDataDto GetManagementData(BillingAccountEntity billingAccount);
    }
}
