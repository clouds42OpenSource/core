﻿using Clouds42.DataContracts.MyDatabasesService;

namespace Clouds42.MyDatabaseService.Contracts.CardData.Interfaces
{
    /// <summary>
    /// Провайдер получения данных карточки сервиса "Мои инф. базы"
    /// </summary>
    public interface IMyDatabasesServiceCardDataProvider
    {
        /// <summary>
        /// Получить данные карточки по критериям
        /// </summary>
        /// <param name="serviceTypeId">ID услуги сервиса</param>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Данные карточки</returns>
        MyDatabasesServiceTypeCardDto GetCardDataByCriteria(Guid serviceTypeId, Guid accountId);
    }
}
