﻿namespace Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces
{
    /// <summary>
    /// Провайдер создания лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    public interface ICreateLimitOnFreeCreationDbForAccount
    {
        /// <summary>
        /// Создать лимит на
        /// бесплатное количество баз для создания
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="limitValue">Лимит</param>
        void Create(Guid accountId, int limitValue);
    }
}
