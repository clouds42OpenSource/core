﻿namespace Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces
{
    /// <summary>
    /// Провайдер для обновления лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    public interface IUpdateLimitOnFreeCreationDbForAccountProvider
    {
        /// <summary>
        ///  Обновить лимита на бесплатное количество баз для
        ///  создания если он существует, иначе создать
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="limitValue">Новый лимит</param>
        void UpdateIfAvailableOtherwiseCreate(Guid accountId, int limitValue);
    }
}
