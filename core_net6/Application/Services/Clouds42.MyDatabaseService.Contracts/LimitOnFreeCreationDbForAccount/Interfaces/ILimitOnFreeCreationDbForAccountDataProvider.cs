﻿using Clouds42.Domain.DataModels;

namespace Clouds42.MyDatabaseService.Contracts.LimitOnFreeCreationDbForAccount.Interfaces
{
    /// <summary>
    /// Провайдер по работе с данными лимита на
    /// бесплатное количество баз для создания(для аккаунта)
    /// </summary>
    public interface ILimitOnFreeCreationDbForAccountDataProvider
    {
        /// <summary>
        /// Получить значение лимита на бесплатное количество баз для создания
        /// Возьмет заначение по аккаунту или дефолтное значение
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Лимит на бесплатное количество баз для создания</returns>
        int GetLimitOnFreeCreationDbValue(Guid accountId);

        /// <summary>
        /// Получить информацию об аккаунте
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>информация об аккаунте</returns>
        Account GetAccountInfo(Guid accountId);

        /// <summary>
        /// Получить лимит на бесплатное количество баз для создания (для аккаунта)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Лимит на бесплатное количество баз для создани</returns>
        Domain.DataModels.billing.LimitOnFreeCreationDbForAccount GetLimitOnFreeCreationDbForAccount(Guid accountId);

        /// <summary>
        /// Получить количество инф. баз
        /// для которых требуется оплата(сверх лимита)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        /// <returns>Количество инф. баз
        /// для которых требуется оплата</returns>
        int GetDatabasesCountForWhichYouNeedToPay(Guid accountId, int databasesCount);

        /// <summary>
        /// Получить количество баз для которых необходима оплата
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="countOfDatabasesToCreate">Количество баз, которые необходимо создать</param>
        /// <param name="countOfDatabasesCreated">Количество созданных баз</param>
        /// <returns>Количество баз, за которые нужно платить</returns>
        int GetDatabasesCountForWhichYouNeedPayment(Guid accountId, int countOfDatabasesToCreate,
            int countOfDatabasesCreated);

        /// <summary>
        /// Получить количество инф. баз
        /// для которых требуется оплата(сверх лимита)
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="databasesCount">Количество баз</param>
        /// <param name="limitOnFreeCreationDb">Лимит на бесплатное количество создания баз</param>
        /// <returns>Количество инф. баз
        /// для которых требуется оплата</returns>
        int GetDatabasesCountForWhichYouNeedToPay(Guid accountId, int databasesCount, int limitOnFreeCreationDb);
    }
}
