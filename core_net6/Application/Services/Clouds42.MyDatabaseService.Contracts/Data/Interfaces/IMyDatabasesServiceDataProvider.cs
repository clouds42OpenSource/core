﻿using Clouds42.DataContracts.BaseModel;
using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceData;

namespace Clouds42.MyDatabaseService.Contracts.Data.Interfaces
{
    /// <summary>
    /// Провайдер для работы с данными сервиса "Мои информационные базы"
    /// </summary>
    public interface IMyDatabasesServiceDataProvider
    {
        /// <summary>
        /// Получить Id сервиса "Мои информационные базы"
        /// </summary>
        /// <returns>Id сервиса "Мои информационные базы"</returns>
        Guid GetMyDatabasesServiceId();

        /// <summary>
        /// Получить Id услуги от которой зависит сервис
        /// (услуга сервиса) "Мои информационные базы"
        /// </summary> 
        /// <returns>Id услуги от которой зависит сервис</returns>
        Guid GetServiceTypeIdOnWhichServiceDepends();

        /// <summary>
        /// Получить список Id услуг сервиса "Мои информационные базы"
        /// </summary>
        /// <returns>Список Id услуг</returns>
        IEnumerable<Guid> GetMyDatabasesServiceTypeIds();

        /// <summary>
        /// Получить список пар ID-Название активных услуг сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="locale"></param>
        /// <returns>Список пар ID-Название активных услуг сервиса "Мои информационные базы"</returns>
        Task<List<KeyValueDto<Guid>>> GetAccountActivePaidServiceTypesIdNamePairs(Guid accountId, string locale);

        /// <summary>
        /// Получить краткие данные по сервису "Мои инф. базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <returns>Краткие данные по сервису "Мои инф. базы"</returns>
        MyDatabasesServiceShortDataDto GetMyDatabasesServiceShortData(Guid accountId);

        /// <summary>
        /// Получить данные с краткой информацией
        /// по услугам сервиса "Мои инф. базы"
        /// </summary>
        /// <returns>Список моделей данных с краткой инфомацией
        /// по услугам сервиса "Мои инф. базы"</returns>
        IEnumerable<MyDatabasesServiceTypeShortDataDto> GetShortDataByMyDatabasesServiceTypes();
    }
}
