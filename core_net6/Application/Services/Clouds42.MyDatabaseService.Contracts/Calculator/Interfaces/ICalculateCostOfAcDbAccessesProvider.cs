﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;

namespace Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces
{
    /// <summary>
    /// Провайдер для расчета стоимости доступов
    /// </summary>
    public interface ICalculateCostOfAcDbAccessesProvider
    {
        /// <summary>
        /// Рассчитать стоимость доступов в конфигурации
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypesIdsList">Список ID услуг сервиса</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        List<AccountUserLicenceForConfigurationDto> CalculateAccessesCostForConfigurations(Guid accountId,
            List<Guid> serviceTypesIdsList, List<AccountUserAccessDto> accountUserDataModelsList, DateTime? paidDate = null);

        /// <summary>
        /// Рассчитать стоимость доступов для загруженного файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="configirationId">ID конфигурации</param>
        /// <param name="configurationName">имя конфигурации</param>
        /// <returns>Стоимость доступов в конфигурации</returns>
        AccountUserLicenceForUploudFileDto CalculateAccessesCostForUploadFile(Guid accountId, string configirationId, string configurationName);

        /// <summary>
        /// Получить общую стоимость доступов в конфигурации
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypesIdsList">Список ID услуг сервиса</param>
        /// <param name="accountUserDataModelsList">Список моделей пользователей</param>
        /// <returns>Общая стоимость доступов в конфигурации</returns>
        decimal GetTotalAccessesCostForConfigurations(Guid accountId, List<Guid> serviceTypesIdsList,
            List<AccountUserAccessDto> accountUserDataModelsList);
    }
}
