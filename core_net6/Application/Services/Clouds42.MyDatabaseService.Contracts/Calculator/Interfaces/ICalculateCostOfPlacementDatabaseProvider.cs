﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using Clouds42.DataContracts.MyDisk;

namespace Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces
{
    /// <summary>
    /// Провайдер для подсчета стоимости размещения инф. базы
    /// </summary>
    public interface ICalculateCostOfPlacementDatabaseProvider
    {
        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        CalculationCostOfPlacementDatabasesResultDto CalculateCostOfPlacementDatabases(
            CalculateCostOfPlacementDatabasesDto model);


        /// <summary>
        /// Посчитать стоимость размещения инф. баз для аккаунта
        /// </summary>
        /// <param name="model">Модель подсчета стоимости размещения инф. баз для аккаунта</param>
        /// <returns>Модель результата подсчета стоимости
        /// размещения инф. баз для аккаунта</returns>
        Task<TryChangeTariffResultDto> CalculateCostDatabases(
            CalculateCostOfPlacementDatabasesDto model);
    }
}
