﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceCalculator;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Contracts.Calculator.Interfaces
{
    /// <summary>
    /// Провайдер по работе с данными для калькуляции
    /// услуги сервиса "Мои информационные базы"
    /// </summary>
    public interface IMyDatabasesCalculationCostDataProvider
    {
        /// <summary>
        /// Получить данные для калькуляции
        /// услуги сервиса "Мои информационные базы"
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги(услуги сервиса "Мои информационные базы")</param>
        /// <returns>Данные для калькуляции
        /// услуги сервиса "Мои информационные базы"</returns>
        MyDatabasesCalculationCostDataDto GetMyDatabasesCalculationCostForAccountServiceTypeData(Guid accountId,
            ResourceType systemServiceType);

        Task<MyDatabasesCalculationCostDataDto> GetMyDatabasesCalculationCostForAccountServiceTypeDataAsync(
            Guid accountId,
            ResourceType systemServiceType);

        /// <summary>
        /// Получить данные для рассчета стоимости доступа в конфигурацию
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="serviceTypeId">ID услуги</param>
        /// <returns>Данные для рассчета стоимости доступа в конфигурацию</returns>
        CalculationDataForAccessToConfiguration1CDto GetCalculationDataForAccessToConfiguration1C(Guid accountId, Guid serviceTypeId);
    }
}
