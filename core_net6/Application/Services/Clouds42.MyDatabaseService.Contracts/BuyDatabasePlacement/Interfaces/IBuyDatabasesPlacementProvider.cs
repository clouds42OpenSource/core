﻿using Clouds42.DataContracts.AccountDatabase.DatabasesPlacement;

namespace Clouds42.MyDatabaseService.Contracts.BuyDatabasePlacement.Interfaces
{
    /// <summary>
    /// Провайдер для покупки лицензий на размещение инф. баз
    /// </summary>
    public interface IBuyDatabasesPlacementProvider
    {
        /// <summary>
        /// Совершить покупку(применение) лицензий на размещение инф. баз
        /// </summary>
        /// <param name="model">Модель для покупки лицензий на размещение инф. баз</param>
        /// <returns>Модель результата покупки лицензий на размещение инф. баз</returns>
        BuyDatabasesPlacementResultDto BuyDatabasesPlacement(BuyDatabasesPlacementDto model);
    }
}
