﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces
{
    /// <summary>
    /// Провайдер проверки возможности оплаты за сервис "Мои инф. базы"
    /// </summary>
    public interface IAbilityToPayForMyDatabasesServiceProvider
    {
        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Результат проверки</returns>
        CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment, List<InfoDatabaseDomainModelDto> createDatabasesModelDc);

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <returns>Результат проверки</returns>
        CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment);

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="userFromZipPackageList">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Результат проверки</returns>
        CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment, List<InfoAboutUserFromZipPackageDto> userFromZipPackageList, Guid? myDatabasesServiceTypeId);

        /// <summary>
        /// Проверить возможность
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="isPromisePayment">Признак что для оплаты будет использован ОП</param>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        /// <returns>Результат проверки</returns>
        CreateAccountDatabasesResultDto CheckAbility(Guid accountId, bool isPromisePayment,
            List<Guid> usersIdForAddAccess, Guid? myDatabasesServiceTypeId);
    }
}
