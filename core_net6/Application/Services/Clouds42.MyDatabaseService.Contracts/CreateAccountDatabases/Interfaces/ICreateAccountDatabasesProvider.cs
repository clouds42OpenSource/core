﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces
{
    /// <summary>
    /// Провайдер создания инф. баз
    /// </summary>
    public interface ICreateAccountDatabasesProvider
    {
        /// <summary>
        /// Создать инф. базы из шаблона
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAccountDatabasesFromTemplateDto">Модель создания инф. баз из шаблона</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateAccountDatabasesFromTemplate(Guid accountId,
            CreateAccountDatabasesFromTemplateDto createAccountDatabasesFromTemplateDto);

        /// <summary>
        /// Создать инф. базу из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateAccountDatabaseFromZip(Guid accountId, CreateAcDbFromZipDto createAcDbFromZipDto);

        /// <summary>
        /// Создать инф. базу на разделителях из ZIP файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из ZIP файла</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateDatabaseOnDelimiterFromZip(Guid accountId, CreateAcDbFromZipForMSDto createAcDbFromZipDto);


        /// <summary>
        /// Создать инф. базу на разделителях из DT в ZIP
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromZipDto">Модель создания инф. баз из DT в ZIP</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateDatabaseOnDelimiterFromDtToZip(Guid accountId, CreateAcDbFromDtToZipForMSDto createAcDbFromDtToZipDto);

        /// <summary>
        /// Создать инф. базу из Dt файла
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbFromDtDto">Модель создания инф. баз из DT файла</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateAccountDatabaseFromDtFile(Guid accountId, CreateAcDbFromDtDto createAcDbFromDtDto);

        /// <summary>
        /// Создать инф. базу на разделителях из бекапа
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createAcDbOnDelimitersFromBackupDto">Модель создания инф. базы на разделителях из бекапа</param>
        /// <returns>Результат создания</returns>
        CreateAccountDatabasesResultDto CreateAcDbOnDelimitersFromBackup(Guid accountId, CreateAcDbOnDelimitersFromBackupDto createAcDbOnDelimitersFromBackupDto);
    }
}
