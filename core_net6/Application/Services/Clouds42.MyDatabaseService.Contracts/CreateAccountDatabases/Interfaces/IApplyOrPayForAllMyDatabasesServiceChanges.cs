﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;

namespace Clouds42.MyDatabaseService.Contracts.CreateAccountDatabases.Interfaces
{
    /// <summary>
    /// Провайдер для принятия/оплаты изменений сервиса "Мои инф. базы"
    /// </summary>
    public interface IApplyOrPayForAllMyDatabasesServiceChanges
    {
        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="createDatabasesModelDc">Список моделей создания инф. баз</param>
        /// <returns>Результат выполнения</returns>
        void ApplyOrPay(Guid accountId, List<InfoDatabaseDomainModelDto> createDatabasesModelDc);

        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="userFromZipPackageList">Список моделей создания инф. баз</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        void ApplyOrPay(Guid accountId, List<InfoAboutUserFromZipPackageDto> userFromZipPackageList,
            Guid? myDatabasesServiceTypeId);

        /// <summary>
        /// Принять/оплатить изменения
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="usersIdForAddAccess">Список пользователей для предоставления доступа</param>
        /// <param name="myDatabasesServiceTypeId">ID услуги сервиса "Мои инф. базы"</param>
        void ApplyOrPay(Guid accountId, List<Guid> usersIdForAddAccess, Guid? myDatabasesServiceTypeId);

        /// <summary>
        /// Заплатить за размещение инф. баз
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="databasesCount">Кол-во баз</param>
        void PayForDatabasePlacement(Guid accountId, int databasesCount);
    }
}
