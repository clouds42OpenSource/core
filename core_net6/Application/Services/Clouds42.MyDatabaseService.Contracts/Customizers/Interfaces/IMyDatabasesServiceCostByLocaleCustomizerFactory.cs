﻿namespace Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces
{
    /// <summary>
    /// Фабрика для создания кастомизатора стоимости сервиса "Мои инф. базы"
    /// по локали аккаунта
    /// </summary>
    public interface IMyDatabasesServiceCostByLocaleCustomizerFactory
    {
        /// <summary>
        /// Создать кастомизатор стоимости сервиса "Мои инф. базы"
        /// по локали аккаунта
        /// </summary>
        /// <param name="localeName">Название локали</param>
        /// <returns>Кастомизатор стоимости сервиса "Мои инф. базы"</returns>
        IMyDatabasesServiceCostForLocaleCustomizer CreateByLocale(string localeName);

        /// <summary>
        /// Создать кастомизатор стоимости сервиса "Мои инф. базы"
        /// по локали аккаунта
        /// </summary>
        /// <param name="localeId">Id локали</param>
        /// <returns>Кастомизатор стоимости сервиса "Мои инф. базы"</returns>
        IMyDatabasesServiceCostForLocaleCustomizer CreateByLocale(Guid localeId);
    }
}
