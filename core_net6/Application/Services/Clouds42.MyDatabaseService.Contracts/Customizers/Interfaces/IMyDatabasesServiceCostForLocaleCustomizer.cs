﻿namespace Clouds42.MyDatabaseService.Contracts.Customizers.Interfaces
{
    /// <summary>
    /// Кастомизатор стоимости сервиса "Мои инф. базы" для локали
    /// </summary>
    public interface IMyDatabasesServiceCostForLocaleCustomizer
    {
        /// <summary>
        /// Кастомизировать стоимость сервиса
        /// "Мои инф. базы" для локали аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="isNewAccount">Признак, указывающий что аккаунт является новым
        /// (нет, дополнительных тарифов на аккаунт)</param>
        void Customize(Guid accountId, bool isNewAccount = false);
    }
}
