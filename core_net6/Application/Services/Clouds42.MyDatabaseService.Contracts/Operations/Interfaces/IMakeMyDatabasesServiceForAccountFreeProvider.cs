﻿namespace Clouds42.MyDatabaseService.Contracts.Operations.Interfaces
{
    /// <summary>
    /// Провайдер, делающий сервис "Мои информационные базы" бесплатным для аккаунта
    /// </summary>
    public interface IMakeMyDatabasesServiceForAccountFreeProvider
    {
        /// <summary>
        /// Сделать сервис "Мои информационные базы" бесплатным для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void MakeServiceFree(Guid accountId);
    }
}
