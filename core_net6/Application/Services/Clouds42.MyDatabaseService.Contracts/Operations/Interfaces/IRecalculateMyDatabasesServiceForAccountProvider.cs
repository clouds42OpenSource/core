﻿namespace Clouds42.MyDatabaseService.Contracts.Operations.Interfaces
{
    /// <summary>
    /// Провайдер по пересчету стоимости сервиса "Мои информационные базы" для аккаунта
    /// Удаляет тарифы на аккаунт, обновляет стоимость ресурсов согласно тарифу
    /// и пересчитывает стоимость сервиса
    /// </summary>
    public interface IRecalculateMyDatabasesServiceForAccountProvider
    {
        /// <summary>
        /// Пересчитать стоимость сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void RecalculateServiceCost(Guid accountId);
    }
}
