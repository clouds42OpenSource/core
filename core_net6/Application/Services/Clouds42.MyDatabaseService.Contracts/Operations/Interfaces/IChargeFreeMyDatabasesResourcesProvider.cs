﻿namespace Clouds42.MyDatabaseService.Contracts.Operations.Interfaces
{
    /// <summary>
    /// Провайдер для начисления бесплатных ресурсов
    /// по сервису "Мои информационные базы"
    /// </summary>
    public interface IChargeFreeMyDatabasesResourcesProvider
    {
        /// <summary>
        /// Начислить бесплатные ресурсы по сервису
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void ChargeFreeResourcesForAccount(Guid accountId);
    }
}
