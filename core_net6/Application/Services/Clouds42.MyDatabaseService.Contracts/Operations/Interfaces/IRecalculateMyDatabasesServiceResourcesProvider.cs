﻿using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Contracts.Operations.Interfaces
{
    /// <summary>
    /// Провайдер для пересчета стоимости ресурсов сервиса "Мои информационные базы"
    /// </summary>
    public interface IRecalculateMyDatabasesServiceResourcesProvider
    {
        /// <summary>
        /// Пересчитать стоимость ресурсов сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void RecalculateResources(Guid accountId);
       
        /// <summary>
        /// Пересчитать стоимость ресурсов услуги сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="serviceTypeId">Id услуги</param>
        void RecalculateResourcesForServiceType(Guid accountId, Guid serviceTypeId);

        /// <summary>
        /// Пересчитать стоимость ресурсов услуги сервиса
        /// "Мои информационные базы" для аккаунта
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="systemServiceType">Тип системной услуги</param>
        void RecalculateResourcesForServiceType(Guid accountId, ResourceType systemServiceType);
    }
}
