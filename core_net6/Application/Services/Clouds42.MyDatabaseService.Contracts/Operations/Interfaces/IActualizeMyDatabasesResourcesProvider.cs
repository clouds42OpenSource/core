﻿namespace Clouds42.MyDatabaseService.Contracts.Operations.Interfaces
{
    /// <summary>
    /// Актуализировать ресурсы сервиса "Мои информационные базы"
    /// </summary>
    public interface IActualizeMyDatabasesResourcesProvider
    {
        /// <summary>
        /// Актуализировать ресурсы сервиса "Мои информационные базы"
        /// c пересчетом значений количества баз
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        void ActualizeForAccount(Guid accountId);
    }
}
