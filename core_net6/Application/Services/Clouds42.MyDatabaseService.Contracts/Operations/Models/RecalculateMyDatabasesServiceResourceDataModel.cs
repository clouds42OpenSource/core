﻿using Clouds42.Domain.DataModels.billing;
using CommonLib.Enums;

namespace Clouds42.MyDatabaseService.Contracts.Operations.Models
{
    /// <summary>
    /// Модель данных для пересчета стоимости ресурса
    /// сервиса "Мои информационные базы"
    /// </summary>
    public class RecalculateMyDatabasesServiceResourceDataModel
    {
        /// <summary>
        /// Ресурс сервиса
        /// </summary>
        public Resource Resource { get; set; }

        /// <summary>
        /// Тип ресурса системной услуги
        /// </summary>
        public ResourceType ResourceType { get; set; }

        /// <summary>
        /// Актуальная стоимость
        /// </summary>
        public decimal ActualCost { get; set; }

        /// <summary>
        /// Актуальное количество инф. баз
        /// Значение существует если услуга на аккаунт
        /// </summary>
        public int? ActualDatabasesCount { get; set; }
    }
}
