﻿namespace Clouds42.MyDatabaseService.Contracts.Triggers.Models
{
    /// <summary>
    /// Данные по состоянию сервиса "Мои инф. базы"
    /// в разрезе инф. базы(определены по инф. базе)
    /// </summary>
    public class MyDatabasesServiceStateByDbContextDataModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id сервиса
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// Сервис заблокирован
        /// </summary>
        public bool IsFrozen { get; set; }

        public bool IsServerDatabase { get; set; }

        public int MatchingDatabasesByTemplateCount { get; set; }
        public int ServerDatabasesCount { get; set; }
        public Guid AccessToConfiguration1CServiceTypeId { get; set; }
        public Guid AccessToServerDatabaseServiceTypeId { get; set; }

        /// <summary>
        /// Список Id услуг которые можно отключить у пользователей
        /// (по которым можно отключить лицензии)
        /// </summary>
        public List<Guid> ServiceTypeIdListCanBeDisabled { get; set; } = [];
    }
}
