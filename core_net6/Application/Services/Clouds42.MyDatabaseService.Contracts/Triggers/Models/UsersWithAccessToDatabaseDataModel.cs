﻿namespace Clouds42.MyDatabaseService.Contracts.Triggers.Models
{
    /// <summary>
    /// Модель данных пользователей с доступом к инф. базе
    /// (или доступ в процессе выдачи)
    /// </summary>
    public class UsersWithAccessToDatabaseDataModel
    {
        /// <summary>
        /// Id информационной базы
        /// </summary>
        public Guid DatabaseId { get; set; }

        /// <summary>
        /// Список Id пользователей
        /// </summary>
        public List<Guid> UserIds { get; set; } = [];
    }
}
