﻿namespace Clouds42.MyDatabaseService.Contracts.Triggers.Models
{
    /// <summary>
    /// Данные пользователя по ресурсу
    /// </summary>
    public class AccountUserDataByResourceModel
    {
        /// <summary>
        /// Id аккаунта
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Id пользователя
        /// </summary>
        public Guid AccountUserId { get; set; }
    }
}
