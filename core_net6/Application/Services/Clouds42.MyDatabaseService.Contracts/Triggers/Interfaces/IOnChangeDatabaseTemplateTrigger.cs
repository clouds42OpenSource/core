﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;

namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на изменение шаблона у инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public interface IOnChangeDatabaseTemplateTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение шаблона у инф. базы
        /// </summary>
        /// <param name="model">Модель триггера на изменение
        /// шаблона у инф. базы</param>
        void Execute(OnChangeDatabaseTemplateTriggerDto model);
    }
}
