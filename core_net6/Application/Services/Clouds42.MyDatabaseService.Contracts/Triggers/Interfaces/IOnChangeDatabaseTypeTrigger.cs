﻿namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на изменение типа базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public interface IOnChangeDatabaseTypeTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение типа базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        void Execute(Guid databaseId);
    }
}
