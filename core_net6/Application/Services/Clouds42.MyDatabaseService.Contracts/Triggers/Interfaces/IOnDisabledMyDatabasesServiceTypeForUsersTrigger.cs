﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;

namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на отключение лицензии по услуге сервиса
    /// "Мои информационные базы" у пользователей
    /// </summary>
    public interface IOnDisabledMyDatabasesServiceTypeForUsersTrigger
    {
        /// <summary>
        /// Выполнить триггер на отключение
        /// лицензии по услуге у пользователей
        /// </summary>
        /// <param name="model">Модель триггера на отключение лицензии
        /// по услуге сервиса "Мои информационные базы" у пользователей</param>
        void Execute(OnDisabledMyDatabasesServiceTypeForUsersTriggerDto model);
    }
}
