﻿using Clouds42.DataContracts.MyDatabasesService.MyDatabasesServiceTriggers;

namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на отключение Аренды 1С у пользователей
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public interface IOnDisabledRent1CForUsersTrigger
    {
        /// <summary>
        /// Выполнить триггер на отключение
        /// Аренды 1С у пользователей
        /// </summary>
        /// <param name="model">Модель триггера на отключение
        /// Аренды 1С у пользователей</param>
        void Execute(OnDisabledRent1CForUsersTriggerDto model);
    }
}
