﻿namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на изменение состояния инф. базы
    /// (триггер сервиса Мои инф. базы)
    /// </summary>
    public interface IOnChangeDatabaseStateTrigger
    {
        /// <summary>
        /// Выполнить триггер на изменение состояния инф. базы
        /// </summary>
        /// <param name="databaseId">Id инф. базы</param>
        void Execute(Guid databaseId);
    }
}
