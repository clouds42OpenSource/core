﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.MyDatabaseService.Contracts.Triggers.Interfaces
{
    /// <summary>
    /// Триггер на регистрацию инф. базы после активации сервиса
    /// </summary>
    public interface IOnRegisterDatabaseAfterServiceActivationTrigger
    {
        /// <summary>
        /// Выполнить триггер на регистрацию инф. базы после активации сервиса
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountUser">Пользователь</param>
        void Execute(Guid accountDatabaseId, IAccountUser accountUser);
    }
}
