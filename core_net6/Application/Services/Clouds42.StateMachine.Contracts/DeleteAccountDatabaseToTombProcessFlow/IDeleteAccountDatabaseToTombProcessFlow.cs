﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.DeleteAccountDatabaseToTombProcessFlow
{
    /// <summary>
    /// Процесс удаления инф. базы в склеп
    /// </summary>
    public interface IDeleteAccountDatabaseToTombProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы в склеп</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат архивации</returns>
        StateMachineResult<CopyAccountDatabaseBackupToTombParamsDto> Run(CopyAccountDatabaseBackupToTombParamsDto model, bool throwExceptionIfError = false);
    }
}
