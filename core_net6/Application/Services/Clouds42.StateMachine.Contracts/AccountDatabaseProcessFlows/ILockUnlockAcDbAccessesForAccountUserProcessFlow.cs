﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс блокировки/разблокировки доступов пользователя к инф. базам
    /// </summary>
    public interface ILockUnlockAcDbAccessesForAccountUserProcessFlow
    {
        /// <summary>
        /// Запустить процесс
        /// </summary>
        /// <param name="model">Входящая модель данных в процесс обработки</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат выполнения</returns>
        StateMachineResult<bool> Run(LockUnlockAcDbAccessesParamsDto model, bool throwExceptionIfError = true);
    }
}
