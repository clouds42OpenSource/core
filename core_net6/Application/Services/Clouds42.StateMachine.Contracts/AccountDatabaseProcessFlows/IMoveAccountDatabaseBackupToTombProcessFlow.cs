﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс переноса бэкапа инф. базы в склеп
    /// </summary>
    public interface IMoveAccountDatabaseBackupToTombProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат переноса</returns>
        StateMachineResult<CopyAccountDatabaseBackupToTombParamsDto> Run(CopyAccountDatabaseBackupToTombParamsDto model, bool throwExceptionIfError = true);
    }
}
