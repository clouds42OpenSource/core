﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс восстановления инф. базы из бэкапа после не успешной попытки АО
    /// </summary>
    public interface IRestoreAccountDatabaseAfterFailedAutoUpdateProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров восстановления инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат восстановления</returns>
        StateMachineResult<RestoreAccountDatabaseParamsDto> Run(RestoreAccountDatabaseParamsDto model, bool throwExceptionIfError = true);
    }
}
