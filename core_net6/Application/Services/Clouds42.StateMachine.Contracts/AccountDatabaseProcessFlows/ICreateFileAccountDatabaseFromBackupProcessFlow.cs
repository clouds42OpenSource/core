﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс создания файловой инф. базы на основании бэкапа
    /// </summary>
    public interface ICreateFileAccountDatabaseFromBackupProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров создания инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат создания</returns>
        StateMachineResult<CreateAccountDatabaseFromBackupParamsDto> Run(CreateAccountDatabaseFromBackupParamsDto model, bool throwExceptionIfError = true);
    }
}
