﻿using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountDatabaseProcessFlows
{
    /// <summary>
    /// Процесс удаления инф. базы
    /// </summary>
    public interface IDeleteAccountDatabaseProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров удаления инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат удаления</returns>
        StateMachineResult<bool> Run(DeleteAccountDatabaseParamsDto model, bool throwExceptionIfError = false);
    }
}
