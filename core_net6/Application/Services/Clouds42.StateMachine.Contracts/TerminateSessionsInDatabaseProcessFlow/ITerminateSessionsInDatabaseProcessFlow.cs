﻿using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.TerminateSessionsInDatabaseProcessFlow
{
    /// <summary>
    /// Процесс завершения сеансов в информационной базе
    /// </summary>
    public interface ITerminateSessionsInDatabaseProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров для завершения сеансов в информационной базе</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат архивации</returns>
        StateMachineResult<TerminateSessionsInDatabaseJobParamsDto> Run(TerminateSessionsInDatabaseJobParamsDto model,
            bool throwExceptionIfError = false);
    }
}
