﻿using Clouds42.DataContracts.AccountDatabase.CreateAccountDatabases;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.ServiceManagerProcessFlows
{
    /// <summary>
    /// Процесс обновления названия инф. базы
    /// </summary>
    public interface IUpdateAccountDatabaseCaptionProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров обновления названия инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат обновления названия</returns>
        StateMachineResult<bool> Run(UpdateAccountDatabaseCaptionParamsDto model, bool throwExceptionIfError = true);
    }
}
