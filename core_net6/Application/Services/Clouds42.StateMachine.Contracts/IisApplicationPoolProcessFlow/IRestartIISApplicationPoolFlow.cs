﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow
{
    /// <summary>
    /// Конечный автомат для перезапуска пула приложения на IIS
    /// </summary>
    public interface IRestartIisApplicationPoolFlow
    {
        /// <summary>
        /// Запустить рабочий процесс
        /// </summary>
        /// <param name="model">Модель параметров перезапуска пула инф. базы на нодах публикации</param>
        /// <param name="throwExceptionIfError">Прокидывать ошибку если навернулось исключение</param>
        /// <returns>Результат выполнения процесса</returns>
        StateMachineResult<bool> Run(RestartIisApplicationPoolParamsDto model, bool throwExceptionIfError = true);
    }
}
