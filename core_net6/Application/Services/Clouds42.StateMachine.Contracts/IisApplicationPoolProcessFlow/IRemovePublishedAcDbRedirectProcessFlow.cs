﻿using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow
{
    /// <summary>
    /// Процесс удаления редиректа опубликованной инф. базы
    /// </summary>
    public interface IRemovePublishedAcDbRedirectProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров удаления редиректа опубликованной инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат удаления</returns>
        StateMachineResult<bool> Run(ManagePublishedAcDbRedirectParamsDto model, bool throwExceptionIfError = true);
    }
}
