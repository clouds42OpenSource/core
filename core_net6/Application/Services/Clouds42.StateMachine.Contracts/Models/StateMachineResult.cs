﻿namespace Clouds42.StateMachine.Contracts.Models
{
    public class StateMachineResult<TOutput>
    {

        /// <summary>
        /// Результат выполнения работы процесса.
        /// </summary>
        public TOutput ResultModel { get; set; }

        /// <summary>
        /// Признак завершения работы процесса.
        /// </summary>
        public bool Finish { get; set; }

        /// <summary>
        /// Сообщение окончания выполнения.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// ID рабочего процесса
        /// </summary>
        public Guid ProcessFlowId { get; set; }
    }
}