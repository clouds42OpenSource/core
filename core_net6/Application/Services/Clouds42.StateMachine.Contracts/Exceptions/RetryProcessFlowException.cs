﻿using System.Runtime.Serialization;
using Clouds42.Domain.DataModels.StateMachine;

namespace Clouds42.StateMachine.Contracts.Exceptions
{
    /// <summary>
    /// Исключение попытки перевыполнить ошибочный рабочий процесс.
    /// </summary>
    [Serializable]
    public class RetryProcessFlowException : Exception
    {
        public RetryProcessFlowException(ProcessFlow processFlow, string message) : base(
            $"Ошибка выполнения Retry процесса '{processFlow.Id}', состояние процесса: '{processFlow.State}' Сообщение об ошибке: '{message}'")
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected RetryProcessFlowException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}