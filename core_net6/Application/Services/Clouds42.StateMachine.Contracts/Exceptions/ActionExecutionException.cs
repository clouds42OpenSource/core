﻿using System.Runtime.Serialization;

namespace Clouds42.StateMachine.Contracts.Exceptions
{
    /// <summary>
    /// Исключение выполнения действия процесса.
    /// </summary>
    [Serializable]
    public class ActionExecutionException : Exception
    {
        public ActionExecutionException(string message) : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected ActionExecutionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}