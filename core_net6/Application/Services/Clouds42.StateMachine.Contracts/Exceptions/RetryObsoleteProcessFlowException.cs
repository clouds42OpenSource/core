﻿using System.Runtime.Serialization;

namespace Clouds42.StateMachine.Contracts.Exceptions
{
    /// <summary>
    /// Исклоючение попытки перезапуска устаревшего рабочего процесса.
    /// </summary>
    [Serializable]
    public class RetryObsoleteProcessFlowException : Exception
    {
        public RetryObsoleteProcessFlowException(string message) : base(message)
        {
        }

        /// <summary>
        /// Конструктор для обеспечения корректной сериализации/десериализации
        /// </summary>
        /// <param name="info">Данные для сериализации</param>
        /// <param name="context">Контекст потока сериализации</param>
        protected RetryObsoleteProcessFlowException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}