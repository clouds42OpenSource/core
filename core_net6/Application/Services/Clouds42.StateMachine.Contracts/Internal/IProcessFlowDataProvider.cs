﻿using Clouds42.DataContracts.ProcessFlow;

namespace Clouds42.StateMachine.Contracts.Internal
{
    /// <summary>
    /// Провайдер получения данных о рабочих процессах.
    /// </summary>
    public interface IProcessFlowDataProvider
    {
        /// <summary>
        /// Получить список рабочих процессов по фильтру.
        /// </summary>
        /// <param name="args">Значения фильтра.</param>
        /// <returns>Список рабочих процессов.</returns>
        ProcessFlowDataModelDto GetProcessFlowData(ProcessFlowFilterParamsDto args);

        /// <summary>
        /// Получить детали рабочего процесса по ID
        /// </summary>
        /// <param name="processFlowId">ID рабочего процесса</param>
        /// <returns>Детали рабочего процесса</returns>
        ProcessFlowDetailsDto GetProcessFlowDetailsById(Guid processFlowId);
    }
}