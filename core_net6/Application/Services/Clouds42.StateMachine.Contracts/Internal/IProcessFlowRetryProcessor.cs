﻿namespace Clouds42.StateMachine.Contracts.Internal
{
    /// <summary>
    /// Обработчик по перезапуску рабочих процессов.
    /// </summary>
    public interface  IProcessFlowRetryProcessor
    {

        /// <summary>
        /// Перезапустить рабочий процесс
        /// </summary>
        /// <param name="processFlowId">Номенр рабочего процесса.</param>
        void Retry(Guid processFlowId);

    }
}