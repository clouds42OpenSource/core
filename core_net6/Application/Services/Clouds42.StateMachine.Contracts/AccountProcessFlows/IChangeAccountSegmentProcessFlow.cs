﻿using Clouds42.DataContracts.Account;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountProcessFlows
{
    /// <summary>
    /// Процесс смены сегмента для аккаунта
    /// </summary>
    public interface IChangeAccountSegmentProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Параметры смены сегмента для аккаунта</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат смены сегмента</returns>
        StateMachineResult<MigrationResultDto> Run(ChangeAccountSegmentParamsDto model, bool throwExceptionIfError = false);
    }
}
