﻿using Clouds42.Domain.DataModels.StateMachine;

namespace Clouds42.StateMachine.Contracts.StateMachineFlow
{
    /// <summary>
    /// Перезапускаемый процесс.
    /// </summary>
    public interface IRetryProcess
    {
        /// <summary>
        /// Запустить повторную попытку выполнения процесса.
        /// </summary>
        /// <param name="processFlow">Рабочий процесс.</param>
        /// <param name="throwExceptionIfError">Сгенирировать исключение если есть ошибки выполнения.</param>
        /// <returns>Результат выполнения процесса.</returns>
        void Retry(ProcessFlow processFlow, bool throwExceptionIfError = true);

    }
}