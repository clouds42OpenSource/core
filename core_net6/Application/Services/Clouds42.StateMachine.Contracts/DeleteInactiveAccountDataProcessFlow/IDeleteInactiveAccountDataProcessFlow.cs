﻿using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.DeleteInactiveAccountDataProcessFlow
{
    /// <summary>
    /// Процесс удаления данных неактивного аккаунта
    /// </summary>
    public interface IDeleteInactiveAccountDataProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров удаления данных неактивного аккаунта</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат удаления</returns>
        StateMachineResult<DeleteInactiveAccountDataResultDto> Run(DeleteInactiveAccountDataParamsDto model, bool throwExceptionIfError = false);
    }
}
