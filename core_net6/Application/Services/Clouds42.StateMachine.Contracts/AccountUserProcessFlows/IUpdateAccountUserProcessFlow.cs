﻿using Clouds42.DataContracts.AccountUser;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountUserProcessFlows
{
    /// <summary>
    /// Процесс редактирования аккаунта пользователя
    /// </summary>
    public interface IUpdateAccountUserProcessFlow
    {
        StateMachineResult<bool> Run(AccountUserDto model, bool throwExceptionIfError = true);
    }
}
