﻿using Clouds42.DataContracts.AccountUser.AccountUser.InnerModel;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.AccountUserProcessFlows
{
    /// <summary>
    /// Процесс удаления аккаунта пользователя
    /// </summary>
    public interface IDeleteAccountUserProcessFlow
    {
        StateMachineResult<bool> Run(AccountUserIdModel model, bool throwExceptionIfError = true);
    }
}
