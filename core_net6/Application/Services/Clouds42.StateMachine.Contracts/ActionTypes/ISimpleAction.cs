﻿namespace Clouds42.StateMachine.Contracts.ActionTypes
{
    public interface ISimpleAction<in TModel, out TResult> : IAction<TModel, TResult>
    {
    }
}