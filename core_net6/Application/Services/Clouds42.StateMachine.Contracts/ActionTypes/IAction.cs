﻿namespace Clouds42.StateMachine.Contracts.ActionTypes
{
    public interface IAction<in TModel, out TResult>
    {
        TResult Do(TModel model);
    }
}