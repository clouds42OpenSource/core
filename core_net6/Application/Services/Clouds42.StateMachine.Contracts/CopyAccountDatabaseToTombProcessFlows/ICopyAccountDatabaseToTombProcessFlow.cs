﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.CopyAccountDatabaseToTombProcessFlows
{
    /// <summary>
    /// Процесс копирования инф. базы в склеп
    /// </summary>
    public interface ICopyAccountDatabaseToTombProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров копирования инф. базы в склеп</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат миграции</returns>
        StateMachineResult<CopyAccountDatabaseBackupToTombParamsDto> Run(CopyAccountDatabaseBackupToTombParamsDto model, bool throwExceptionIfError = true);
    }
}
