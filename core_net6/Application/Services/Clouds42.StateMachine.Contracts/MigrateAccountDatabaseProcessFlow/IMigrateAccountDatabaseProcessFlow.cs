﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseMigration;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.MigrateAccountDatabaseProcessFlow
{
    /// <summary>
    /// Процесс миграции инф. базы
    /// </summary>
    public interface IMigrateAccountDatabaseProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров миграции инф. базы</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат миграции</returns>
        StateMachineResult<DatabaseMigrationResultDto> Run(MigrateAccountDatabaseParametersDto model, bool throwExceptionIfError = false);
    }
}
