﻿using Clouds42.DataContracts.Account;
using Clouds42.StateMachine.Contracts.Models;

namespace Clouds42.StateMachine.Contracts.ArchiveAccountFilesToTombProcessFlow
{
    /// <summary>
    /// Процесс архивации файлов аккаунта в склеп
    /// </summary>
    public interface IArchiveAccountFilesToTombProcessFlow
    {
        /// <summary>
        /// Выполнить процесс
        /// </summary>
        /// <param name="model">Модель параметров архивации файлов аккаунта в склеп</param>
        /// <param name="throwExceptionIfError">Признак необходимости вызывать исключение при ошибке выполнения</param>
        /// <returns>Результат архивации</returns>
        StateMachineResult<CopyAccountFilesBackupToTombParamsDto> Run(CopyAccountFilesBackupToTombParamsDto model, bool throwExceptionIfError = false);
    }
}
