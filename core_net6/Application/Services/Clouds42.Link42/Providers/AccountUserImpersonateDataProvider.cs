﻿using System.Data;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Link42.Contracts;
using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.DataContracts.Service.Link;
using Clouds42.DataContracts.Account.AccountModels;
using Clouds42.DataContracts.AccountUser.AccountUser;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.BaseModel;
using Clouds42.Domain.Access;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Link42.Providers
{

    /// <summary>
    /// Провайдер данных имперсонации пользователя облака.
    /// </summary>
    internal class AccountUserImpersonateDataProvider(
        IUnitOfWork dbLayer,
        IAccountDatabaseDataProvider accountDatabaseDataProvider)
        : IAccountUserImpersonateDataProvider
    {
        /// <summary>
        /// Получить данные имперсонации пользователя по ИД.
        /// </summary>
        /// <param name="accountUserId">ИД пользователя облака. AccountUser.Login</param>
        public async Task<AccountUserImpersonateDataDto> GetByAccountUserIdAsync(Guid accountUserId)
        {
            using var tran = dbLayer.SmartTransaction.Get(IsolationLevel.ReadUncommitted);
            var userImpersonateData = await dbLayer.AccountUsersRepository.GetAccountUserImpersonateDataByAccountUserIdAsync(accountUserId) ?? throw new InvalidOperationException($"Не удалось получить данные пользователя по Идентификатору '{accountUserId}'");
            var databases = await accountDatabaseDataProvider.GetAccessDatabaseListAsync(accountUserId);
            tran.Commit();

            return new AccountUserImpersonateDataDto
            {
                SegmentData = CreateSegmentDto(userImpersonateData),
                AccountUserRolesList = new AccountUserRolesListDto { Items = userImpersonateData.Groups },
                AccountDatabaseList = new AccountDatabaseListDto(databases),
                AccountDatabasesBalance = GetAccountBalanceDto(userImpersonateData),
                AccountProperties = CreateAccountDto(userImpersonateData),
                AccountUserProperties = CreateAccountUserDto(userImpersonateData)
            };
        }

        /// <summary>
        /// Получить модель баланса аккаунта
        /// </summary>
        /// <param name="userImpersonateData">Модель данных имперсонации пользователя</param>
        /// <returns>Модель баланса аккаунта</returns>
        private AccountBalanceDto GetAccountBalanceDto(AccountUserImpersonateQueryDataModelDto userImpersonateData)
            => userImpersonateData.Groups.Any(gr => gr != AccountUserGroup.AccountUser)
                ? new AccountBalanceDto
                {
                    AccountBalance = userImpersonateData.AccountBalance
                }
                : null;

        /// <summary>
        /// Создать DTO модель имперсонации пользователя из доменной модели.
        /// </summary>
        /// <param name="userImpersonateData">Доменная модель имперсонации.</param>
        private AccountUserCommonDataDto CreateAccountUserDto(AccountUserImpersonateQueryDataModelDto userImpersonateData)
            => new()
            {
                Id = userImpersonateData.AccountUserId,
                AccountId = userImpersonateData.AccountId,
                Login = userImpersonateData.Login,
                Email = userImpersonateData.Email,
                MiddleName = userImpersonateData.MiddleName,
                LastName = userImpersonateData.LastName,
                FirstName = userImpersonateData.FirstName,
                Removed = userImpersonateData.AccountUserRemoved.HasValue &&
                          userImpersonateData.AccountUserRemoved.Value,
                CorpUserSyncStatus = userImpersonateData.CorpUserSyncStatus,
                FullPhoneNumber = userImpersonateData.PhoneNumber,
                CorpUserId = userImpersonateData.CorpUserID,
                CreationDate = userImpersonateData.AccountUserCreationDate
            };

        /// <summary>
        /// Создать DTO модель аккаунта из доменной модели имперсонации пользователя.
        /// </summary>
        /// <param name="userImpersonateData">Доменная модель имперсонации.</param>
        private AccountCommonDataDto CreateAccountDto(AccountUserImpersonateQueryDataModelDto userImpersonateData)
            => new()
            {
                ReferralAccountId = userImpersonateData.ReferralAccountId,
                IndexNumber = userImpersonateData.AccountIndexNumber,
                Description = userImpersonateData.AccountDescription,
                Removed = userImpersonateData.AccountRemoved.HasValue && userImpersonateData.AccountRemoved.Value,
                AccountCaption = userImpersonateData.AccountCaption,
                Currency = userImpersonateData.AccountCurrency,
                Inn = userImpersonateData.AccountInn,
                LocaleId = userImpersonateData.AccountLocaleId,
                RegistrationDate = userImpersonateData.AccountRegistrationDate
            };

        /// <summary>
        /// Создать DTO модель сегмента из доменной модели имперсонации пользователя.
        /// </summary>
        /// <param name="userImpersonateData">Доменная модель имперсонации.</param>
        private CloudServicesSegmentDto CreateSegmentDto(AccountUserImpersonateQueryDataModelDto userImpersonateData)
            => new()
            {
                ID = userImpersonateData.SegmentId,
                Alpha83Version = userImpersonateData.SegmentAlpha83Version,
                Stable83Version = userImpersonateData.SegmentStable83Version,
                Stable82Version = userImpersonateData.SegmentStable82Version
            };

    }
}
