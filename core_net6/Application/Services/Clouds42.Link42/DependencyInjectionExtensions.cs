﻿using Clouds42.Link42.Contracts;
using Clouds42.Link42.Managers;
using Clouds42.Link42.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.Link42
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddLink42(this IServiceCollection services)
        {
            services.AddTransient<IAccountUserImpersonateDataProvider, AccountUserImpersonateDataProvider>();
            services.AddTransient<IAccountUserImpersonateDataManager, AccountUserImpersonateDataManager>();
            services.AddTransient<LaunchLinkInstructionManager>();

            return services;
        }
    }
}
