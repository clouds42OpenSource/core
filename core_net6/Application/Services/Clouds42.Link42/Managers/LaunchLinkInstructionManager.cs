﻿using Clouds42.Accounts.Account.Helpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Link;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.Link42.Managers
{

    /// <summary>
    /// Менеджер по управлению запуском линк42.
    /// </summary>
    public class LaunchLinkInstructionManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        /// <summary>
        /// Получить данные по маунтингу облачного диска на терминале.
        /// </summary>
        /// <param name="accountUserId">Номер пользователя.</param>        
        public async Task<ManagerResult<MountCloudDiskInfoDto>> GetMountCloudDiskInfo(Guid accountUserId)
        {
            try
            {
                var accountUser = DbLayer.AccountUsersRepository.AsQueryableNoTracking().Include(i => i.Account).FirstOrDefault(au => au.Id == accountUserId);

                if (accountUser == null)
                    return NotFound<MountCloudDiskInfoDto>($"Не удалось получить по номеру '{accountUserId}' данные пользователя");

                AccessProvider.HasAccess(ObjectAction.CloudFileStorageServers_View);

                var fileStorageData = await DbLayer.CloudServicesFileStorageServerRepository.GetFileStorageData(accountUserId);

                if (fileStorageData == null)
                    return NotFound<MountCloudDiskInfoDto>($"Не удалось получить по номеру '{accountUserId}' данные файлого хранилища");

                return Ok(new MountCloudDiskInfoDto
                {
                    AccountIndexNumber = accountUser.Account.IndexNumber,
                    FileStoragePath = fileStorageData.ConnectionAddress,
                    ClientFileStorageFullPath = ClientFileStoragePathCreator.Create(accountUser.Account, fileStorageData)
                });
            }
            catch (Exception ex)
            {
                return NotFound<MountCloudDiskInfoDto>(ex.Message);
            }
        }

    }
}
