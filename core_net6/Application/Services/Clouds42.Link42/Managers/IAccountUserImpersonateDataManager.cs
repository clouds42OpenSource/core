﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Link;

namespace Clouds42.Link42.Managers;

public interface IAccountUserImpersonateDataManager
{
    /// <summary>
    /// Получить данные имперсонации пользователя по ИД.
    /// </summary>
    /// <param name="accountUserId">ИД пользователя облака. AccountUser.Id</param>
    Task<ManagerResult<AccountUserImpersonateDataDto>> GetByAccountUserIdAsync(Guid accountUserId);

    /// <summary>
    /// Получить данные имперсонации пользователя по логину.
    /// </summary>
    /// <param name="login">Логин пользователя облака. AccountUser.Login</param>
    Task<ManagerResult<AccountUserImpersonateDataDto>> GetByAccountUserLoginAsync(string login);

    /// <summary>
    /// Получить данные имперсонации пользователя по сессионному токену.
    /// </summary>
    /// <param name="token">Сессионный токен доступа пользователя облака.</param>
    Task<ManagerResult<AccountUserImpersonateDataDto>> GetByTokenAsync(Guid token);

    Task<ManagerResult<AccountUserImpersonateDataDto>> GetBySignedNonceAsync(string nonce);
}
