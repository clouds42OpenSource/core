﻿using Microsoft.EntityFrameworkCore;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Link;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Link42.Contracts;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.Link42.Managers
{
    public class AccountUserImpersonateDataManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IAccountUserImpersonateDataProvider accountUserImpersonateDataProvider)
        : BaseManager(accessProvider, dbLayer, handlerException), IAccountUserImpersonateDataManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Получить данные имперсонации пользователя по ИД.
        /// </summary>
        /// <param name="accountUserId">ИД пользователя облака. AccountUser.Id</param>
        public async Task<ManagerResult<AccountUserImpersonateDataDto>> GetByAccountUserIdAsync(Guid accountUserId)
        {
            try
            {

                AccessProvider.HasAccess(ObjectAction.LinkUserImpersonate_Get, null, () => accountUserId);

                var accountUser = await DbLayer.AccountUsersRepository.FirstOrDefaultAsync(s => s.Id == accountUserId);

                return accountUser == null ?
                    BadRequest<AccountUserImpersonateDataDto>("Invalid request") :
                    Ok(await accountUserImpersonateDataProvider.GetByAccountUserIdAsync(accountUser.Id));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения данных имперсонации пользователя облака]", () => accountUserId);
                return PreconditionFailed<AccountUserImpersonateDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные имперсонации пользователя по логину.
        /// </summary>
        /// <param name="login">Логин пользователя облака. AccountUser.Login</param>
        public async Task<ManagerResult<AccountUserImpersonateDataDto>> GetByAccountUserLoginAsync(string login)
        {
            try
            {
                var accountUser = await DbLayer.AccountUsersRepository.AsQueryableNoTracking().FirstOrDefaultAsync(acUs => acUs.Login == login || acUs.Email == login);

                return accountUser == null
                    ? BadRequest<AccountUserImpersonateDataDto>("Invalid request")
                    : Ok(await accountUserImpersonateDataProvider.GetByAccountUserIdAsync(accountUser.Id));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения данных имперсонации пользователя облака]", () => login);
                return PreconditionFailed<AccountUserImpersonateDataDto>(ex.Message);
            }
        }

        /// <summary>
        /// Получить данные имперсонации пользователя по сессионному токену.
        /// </summary>
        /// <param name="token">Сессионный токен доступа пользователя облака.</param>
        public async Task<ManagerResult<AccountUserImpersonateDataDto>> GetByTokenAsync(Guid token)
        {
            try
            {
                var accountUserSession = await DbLayer.AccountUserSessionsRepository.FirstOrDefaultAsync(s => s.Token == token);
                AccessProvider.HasAccess(ObjectAction.LinkUserImpersonate_Get, null, () => accountUserSession?.AccountUserId);

                return accountUserSession == null ?
                    BadRequest<AccountUserImpersonateDataDto>("Invalid request") :
                    Ok(await accountUserImpersonateDataProvider.GetByAccountUserIdAsync(accountUserSession.AccountUserId));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения данных имперсонации пользователя облака]", () => token);
                return PreconditionFailed<AccountUserImpersonateDataDto>(ex.Message);
            }
        }

        public async Task<ManagerResult<AccountUserImpersonateDataDto>> GetBySignedNonceAsync(string nonce)
        {
            IUserPrincipalDto currentUser = null;

            try
            {
                currentUser = await AccessProvider.GetUserAsync();

                if (currentUser == null)
                {
                    return NotFound<AccountUserImpersonateDataDto>("Анонимный запрос недопустим.");
                }

                AccessProvider.HasAccess(ObjectAction.LinkUserImpersonate_Get, null, () => currentUser.Id);

                return Ok(await accountUserImpersonateDataProvider.GetByAccountUserIdAsync(currentUser.Id));
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, "[Ошибка получения данных имперсонации пользователя облака]", () => currentUser.Id);
                return PreconditionFailed<AccountUserImpersonateDataDto>(ex.Message);
            }
        }
    }
}
