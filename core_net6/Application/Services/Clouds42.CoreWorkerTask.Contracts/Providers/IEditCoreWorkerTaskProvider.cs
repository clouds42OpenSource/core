﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{

    /// <summary>
    /// Провайдер для редактирования задачи воркера
    /// </summary>
    public interface IEditCoreWorkerTaskProvider
    {
        /// <summary>
        /// Выполнить редактирование задачи воркера
        /// </summary>
        /// <param name="model">Модель редактирования задачи воркера</param>
        void Edit(EditCoreWorkerTaskDto model);
    }
}
