﻿using Clouds42.DataContracts.CoreWorker;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер регистрации задачи в очереди
    /// </summary>
    public interface IRegisterTaskInQueueProvider
    {
        /// <summary>
        /// Зарегистрировать задачу
        /// </summary>
        /// <param name="coreWorkerTaskId">ID задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения</param>
        /// <param name="parametrizationModel">Параметры задачи</param>
        /// <param name="queueStatus">Статус задачи в очереди</param>
        /// <returns>ID задачи в очереди</returns>
        Guid RegisterTask(Guid coreWorkerTaskId, string comment, DateTime? dateTimeDelayOperation,
            ParametrizationModelDto parametrizationModel, CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New);

        /// <summary>
        /// Зарегистрировать задачу
        /// </summary>
        /// <param name="taskType">Тип задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения</param>
        /// <param name="parametrizationModel">Параметры задачи</param>
        /// <param name="queueStatus">Статус задачи в очереди</param>
        /// <returns>ID задачи в очереди</returns>
        Guid RegisterTask(
                CoreWorkerTaskType taskType,
                ParametrizationModelDto? parametrizationModel = null,
                string? comment = null,
                DateTime? dateTimeDelayOperation = null,
                CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New);

        /// <summary>
        /// Зарегистрировать задачу
        /// </summary>
        /// <param name="taskName">Имя задачи</param>
        /// <param name="comment">Комментарий</param>
        /// <param name="dateTimeDelayOperation">Задержка выполнения</param>
        /// <param name="parametrizationModel">Параметры задачи</param>
        /// <param name="queueStatus">Статус задачи в очереди</param>
        /// <returns>ID задачи в очереди</returns>
        Guid RegisterTask(
                string taskName,
                ParametrizationModelDto? parametrizationModel = null,
                string? comment = null,
                DateTime? dateTimeDelayOperation = null,
                CloudTaskQueueStatus queueStatus = CloudTaskQueueStatus.New);
    }
}

