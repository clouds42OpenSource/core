﻿using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер очереди задач воркера
    /// </summary>
    public interface ICoreWorkerTasksQueueProvider
    {
        /// <summary>
        /// Получить задачу в очереди
        /// </summary>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Модель задачи</returns>
        CoreWorkerTasksQueue? GetTasksInQueue(Guid taskId);

        /// <summary>
        /// Получить список ID задач
        /// </summary>
        /// <param name="status">Статус задач</param>
        /// <param name="maxRecordsCount">Максимально количество записей</param>
        /// <returns>Список ID задач</returns>
        IEnumerable<CoreWorkerTasksQueue> GetTasksQueuesId(string status, int maxRecordsCount);

        /// <summary>
        /// Установить комментарий для задачи
        /// </summary>
        /// <param name="taskInfo">Модель параметров задачи</param>
        /// <returns>Результат выполнения</returns>
        bool SetComment(CoreWorkerTasksQueueSetCommentDto taskInfo);

        /// <summary>
        /// Получить ID задачи из очереди
        /// </summary>
        /// <param name="captureTaskRequestDto">Параметры для захвата задачи из очереди</param>
        /// <returns>ID задачи из очереди</returns>
        Task<Guid?> CaptureTaskFromQueueAsync(CaptureTaskRequestDto captureTaskRequestDto);

        Task UpdateTaskStatusAsync(UpdateTaskStatusRequestDto  updateTaskStatusRequestDto);

        /// <summary>
        /// Отменить запущенную задачу
        /// </summary>
        /// <param name="id">Id запущенной задачи</param>
        /// <param name="reasonCancellation">Причина отмены задачи</param>
        Task CancelCoreWorkerTasksQueue(Guid id, string reasonCancellation);
    }
}

