﻿namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер поиска задач воркера 
    /// </summary>
    public interface ISearchCoreWorkerTaskProvider
    {
        /// <summary>
        /// Получить весь список задач воркера
        /// </summary>
        /// <returns>Весь список задач воркера</returns>
        List<KeyValuePair<Guid, string>> GetAllCoreWorkerTasks();
    }
}

