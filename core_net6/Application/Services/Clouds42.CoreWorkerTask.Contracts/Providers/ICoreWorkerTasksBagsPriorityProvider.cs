﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер для работы с приоритетом доступных задач воркера
    /// </summary>
    public interface ICoreWorkerTasksBagsPriorityProvider
    {
        /// <summary>
        /// Получить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="workerId">ID воркера</param>
        /// <param name="taskId">ID задачи</param>
        /// <returns>Приоритет доступной задачи воркера</returns>
        CoreWorkerTaskBagPriorityDataDto GetWorkerTaskBagPriority(short workerId, Guid taskId);

        /// <summary>
        /// Сменить приоритет доступной задачи воркера
        /// </summary>
        /// <param name="changeCoreWorkerTaskBagPriority">Модель для изменения приоритета доступной задачи воркера</param>
        void ChangeWorkerTaskBagPriority(ChangeCoreWorkerTaskBagPriorityDto changeCoreWorkerTaskBagPriority);
    }
}

