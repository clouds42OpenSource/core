﻿namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер проверки очереди задач воркеров
    /// </summary>
    public interface IAuditCoreWorkerTasksQueueProvider
    {
        /// <summary>
        /// Выполнить проверку очереди задач воркеров
        /// </summary>
        void AuditTasksQueue();
    }
}

