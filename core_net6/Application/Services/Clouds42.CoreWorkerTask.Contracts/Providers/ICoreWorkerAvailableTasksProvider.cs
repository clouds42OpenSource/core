﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер для работы с доступными задачами воркера
    /// </summary>
    public interface ICoreWorkerAvailableTasksProvider
    {
        /// <summary>
        /// Получить список доступных задач воркера
        /// </summary>
        /// <param name="coreWorkerId">ID воркера</param>
        /// <returns>Список доступных задач воркера</returns>
        CoreWorkerTasksBagsDataDto GetWorkerAvailableTasksBags(short coreWorkerId);

        /// <summary>
        /// Изменить доступные задачи воркера
        /// </summary>
        /// <param name="changeWorkerAvailableTasksBags">Модель изменения доступных задач воркера</param>
        void ChangeWorkerAvailableTasksBags(ChangeWorkerAvailableTasksBagsDto changeWorkerAvailableTasksBags);
    }
}

