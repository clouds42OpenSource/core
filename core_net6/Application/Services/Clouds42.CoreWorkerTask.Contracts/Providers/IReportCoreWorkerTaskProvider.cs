﻿namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    public interface IReportCoreWorkerTaskProvider
    {
        void SendDailyReportToTelegram();
    }
}
