﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер данных очереди задачи воркера
    /// </summary>
    public interface ICoreWorkerTasksQueueDataProvider
    {
        /// <summary>
        /// Получить информацию по задаче из очереди задач воркера
        /// </summary>
        /// <param name="taskInQueueItemId">ID задачи из очереди</param>
        /// <returns>Информация по задаче из очереди задач воркера</returns>
        TaskInQueueItemInfoDto? GetTaskInQueueItemInfo(Guid taskInQueueItemId);

        /// <summary>
        /// Получить данные по очереди задач вокреров
        /// </summary>
        /// <param name="args">Фильтр поиска</param>
        /// <returns>Данные по очереди задач вокреров</returns>
        CoreWorkerTasksInQueueDataDto GetCoreWorkerTasksQueueData(CoreWorkerTasksQueueFilterParamsDto args);
    }
}

