﻿using Clouds42.DataContracts.CoreWorker;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер для получения данных по воркерам
    /// </summary>
    public interface ICoreWorkersDataProvider
    {
        /// <summary>
        /// Получить весь спиок воркеров
        /// </summary>
        /// <returns>Весь спиок воркеров</returns>
        List<CoreWorkerDescriptionDto> GetAllWorkers();

        /// <summary>
        /// Получить настрйоки воркера
        /// </summary>
        /// <returns></returns>
        CoreWorkerSettingsDto GetWorkerConfiguration();
    }
}

