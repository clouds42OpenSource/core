﻿using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Providers
{
    /// <summary>
    /// Провайдер для работы с данными задач воркера
    /// </summary>
    public interface ICoreWorkerTaskDataProvider
    {
        /// <summary>
        /// Получить задачи воркера
        /// </summary>
        /// <param name="filter">Модель фильтра задач воркера</param>
        /// <returns>Задачи воркера</returns>
        CoreWorkerTasksControlDataDto GetCoreWorkerTasks(CoreWorkerTaskFilterDto filter);

        /// <summary>
        /// Получить задачу воркера
        /// </summary>
        /// <param name="id">Id задачи воркера</param>
        /// <returns>Задача воркера</returns>
        Domain.DataModels.CoreWorkerTask GetCoreWorkerTask(Guid id);

        /// <summary>
        /// Получить данные по доступным задачам воркеров
        /// </summary>
        /// <param name="args">Фильтр для поиска записей</param>
        /// <returns>Данные по доступным задачам воркеров</returns>
        CoreWorkersAvailableTasksBagsDataDto GetCoreWorkerAvailableTasksBag(
            CoreWorkersAvailableTasksBagsFilterDto args);
    }
}

