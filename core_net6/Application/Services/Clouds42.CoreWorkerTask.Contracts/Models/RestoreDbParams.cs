﻿using System.Runtime.Serialization;

namespace Clouds42.CoreWorkerTask.Contracts.Models
{
    [DataContract]
    public class RestoreDbParams
    {
        [DataMember] private Guid _accountDatabaseId;

        [DataMember] private bool _createNewDb;

        public Guid AccountDatabaseId
        {
            get => _accountDatabaseId;
            set => _accountDatabaseId = value;
        }

        public bool CreateNewDb
        {
            get => _createNewDb;
            set => _createNewDb = value;
        }
    }
}

