﻿using System.Runtime.Serialization;

namespace Clouds42.CoreWorkerTask.Contracts.Models
{
    [DataContract]
    public class DatabasesMigrationParams 
    {
        [DataMember] private List<Guid> _accountDatabaseIds;
        [DataMember] private Guid _toFileStorageId;

        /// <summary>
        /// Ид инициатора миграции информационной базы
        /// </summary>
        [DataMember] private Guid _initiatorId;

        /// <summary>
        /// Список Id информационных баз аккаунта
        /// </summary>
        public List<Guid> AccountDatabaseIds
        {
            get => _accountDatabaseIds;
            set => _accountDatabaseIds = value;
        }

        /// <summary>
        /// Id файлового хранилища
        /// </summary>
        public Guid ToFileStorageId
        {
            get => _toFileStorageId;
            set => _toFileStorageId = value;
        }

        /// <summary>
        /// Id инициатора миграции информационной базы
        /// </summary>
        public Guid InitiatorId
        {
            get => _initiatorId;
            set => _initiatorId = value;
        }
    }
}

