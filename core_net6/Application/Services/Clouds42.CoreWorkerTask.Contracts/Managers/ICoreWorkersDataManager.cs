﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CoreWorker;

namespace Clouds42.CoreWorkerTask.Contracts.Managers
{
    public interface ICoreWorkersDataManager
    {
        ManagerResult<List<CoreWorkerDescriptionDto>> GetAllWorkers();
        ManagerResult<CoreWorkerSettingsDto> GetWorkerConfiguration();
    }
}