﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasks;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Managers
{
    public interface ICoreWorkerTaskManager
    {
        ManagerResult ChangeWorkerAvailableTasksBags(ChangeWorkerAvailableTasksBagsDto changeWorkerAvailableTasksBags);
        ManagerResult ChangeWorkerTaskBagPriority(ChangeCoreWorkerTaskBagPriorityDto changeCoreWorkerTaskBagPriority);
        ManagerResult EditCoreWorkerTask(EditCoreWorkerTaskDto model);
        ManagerResult<List<KeyValuePair<Guid, string>>> GetAllCoreWorkerTasks();
        ManagerResult<CoreWorkersAvailableTasksBagsDataDto> GetCoreWorkerAvailableTasksBag(CoreWorkersAvailableTasksBagsFilterDto args);
        ManagerResult<CoreWorkerTasksControlDataDto> GetCoreWorkerTasks(CoreWorkerTaskFilterDto args);
        ManagerResult<CoreWorkerTasksBagsDataDto> GetWorkerAvailableTasksBags(short coreWorkerId);
        ManagerResult<CoreWorkerTaskBagPriorityDataDto> GetWorkerTaskBagPriority(short workerId, Guid taskId);
    }
}
