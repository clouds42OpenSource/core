﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;

namespace Clouds42.CoreWorkerTask.Contracts.Managers
{
    public interface ICoreWorkerTasksQueueDataManager
    {
        ManagerResult<CoreWorkerTasksInQueueDataDto> GetCoreWorkerTasksQueueData(CoreWorkerTasksQueueFilterParamsDto args);
        ManagerResult<TaskInQueueItemInfoDto?> GetTaskInQueueItemInfo(Guid taskInQueueItemId);
    }
}
