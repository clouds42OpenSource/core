﻿using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.DataContracts.CoreWorker.CoreWorkerTasksQueue;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums.CoreWorker;

namespace Clouds42.CoreWorkerTask.Contracts.Managers
{
    public interface ICoreWorkerTasksQueueManager
    {
        ManagerResult<Guid> Add(CoreWorkerTasksDto taskInfo);
        ManagerResult<Guid> Add(CoreWorkerTaskType taskType, string comment, object taskParams = null);
        ManagerResult<Guid> AddTaskQueue(CoreWorkerTasksDto taskInfo);
        Task<ManagerResult> CancelCoreWorkerTasksQueue(Guid id, string reasonCancellation);
        Task<ManagerResult<Guid?>> CaptureTaskFromQueueAsync(CaptureTaskRequestDto captureTaskRequestDto);
        ManagerResult<string> GetComment(Guid taskId);
        ManagerResult<short?> GetCoreWorkerId(Guid taskId);
        ManagerResult<Guid> GetCoreWorkerTaskId(Guid taskId);
        ManagerResult<DateTime> GetDate(Guid taskId);
        ManagerResult<IEnumerable<CoreWorkerTasksQueue>> GetIds(string status, int maxRecordsCount);
        ManagerResult<CoreWorkerTasksQueue> GetProperties(Guid taskId);
        ManagerResult<string> GetTaskStatus(Guid taskId);
        ManagerResult SetComment(CoreWorkerTasksQueueSetCommentDto taskInfo);
    }
}
