﻿namespace Clouds42.AccountDatabase.DataHelpers.Constants
{
    /// <summary>
    /// Константы для работы с временной зоной
    /// </summary>
    public static class TimezoneConst
    {
        /// <summary>
        /// Временная зона мск
        /// </summary>
        public const string MoscowTimezone = "мск";

        /// <summary>
        /// Временная зона киев
        /// </summary>
        public const string KievTimezone = "киев";
    }
}
