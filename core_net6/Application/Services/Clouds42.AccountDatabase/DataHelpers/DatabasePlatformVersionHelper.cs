﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    ///     Хелпер для получения версии платформы базы
    /// </summary>
    public class DatabasePlatformVersionHelper(
        IUnitOfWork unitOfWork,
        ISegmentHelper segmentHelper,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IDatabasePlatformVersionHelper
    {
        private readonly Lazy<Guid> _serviceAccountIdForDemoDelimiterDatabases = new(
            CloudConfigurationProvider.DbTemplateDelimiters.GetServiceAccountIdForDemoDelimiterDatabases);

        /// <summary>
        /// Получить путь к веб расширению 1С
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Путь к веб расширению 1С</returns>
        public string? GetPathToModule1C(Domain.DataModels.AccountDatabase accountDatabase)
            => accountDatabase.IsFile == true
                ? CreatePathTo1CModuleForFileDb(accountDatabase.AccountId,
                    accountDatabase.PlatformType, accountDatabase.DistributionTypeEnum)
                : CreatePathTo1CModuleForServerDb(accountDatabase.AccountId,
                    accountDatabase.PlatformType);

        /// <summary>
        ///     Создать путь к модулю IIS для серверной базы
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="platformType">Платформа (8.2 или 8.3)</param>
        public string? CreatePathTo1CModuleForServerDb(Guid accountId, PlatformType platformType)
        {
            var account = unitOfWork.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            return platformType == PlatformType.V82
                ? segmentHelper.CreatePathToModuleV82Stable(account)
                : segmentHelper.CreatePathToModuleV83Stable(account);
        }

        /// <summary>
        ///     Создать путь к модулю IIS для файловой базы
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="platformType">Платформа (8.2 или 8.3)</param>
        /// <param name="distributionType">Альфа или Стабильная версия 8.3</param>
        public string? CreatePathTo1CModuleForFileDb(Guid accountId, PlatformType platformType,
            DistributionType distributionType)
        {
            var account = unitOfWork.AccountsRepository.FirstOrDefault(a => a.Id == accountId);
            if (platformType == PlatformType.V82)
                return segmentHelper.CreatePathToModuleV82Stable(account);

            if (!HasAlpha83VersionInSegment(accountId) && distributionType == DistributionType.Alpha)
                throw new NotFoundException("В сегменте не указан тип версии 8.3 Альфа");

            return distributionType == DistributionType.Alpha
                ? segmentHelper.CreatePathToModuleV83Alpha(account)
                : segmentHelper.CreatePathToModuleV83Stable(account);
        }

        /// <summary>
        ///     Проверить указана ли в сегменте альфа версия
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        public bool HasAlpha83VersionInSegment(Guid accountId) =>
            accountConfigurationDataProvider.GetAccountSegment(accountId).Alpha83PlatformVersionReference != null;

        /// <summary>
        ///     Получить Альфа версию 8.3 из сегмента
        /// в котором находится аккаунт
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        public string? GetAlpha83VersionFromSegment(Guid accountId) => accountConfigurationDataProvider
            .GetAccountSegment(accountId).Alpha83PlatformVersionReference?.Version;

        /// <summary>
        ///     Получить Стабильную версию 8.2 из сегмента
        /// в котором находится аккаунт
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта</param>
        public string? GetStable82VersionFromSegment(Guid accountId) => accountConfigurationDataProvider
            .GetAccountSegment(accountId).Stable82PlatformVersionReference?.Version;

        /// <summary>
        /// Получить Стабильную версию 8.3 из сегмента
        /// в котором находится аккаунт
        /// </summary>        
        public string? GetStable83VersionFromSegment(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.IsDelimiter())
                return GetPlatformVersionForDelimiterDatabase(accountDatabase)?.Version;

            return accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId)
                .Stable83PlatformVersionReference?.Version;
        }

        /// <summary>
        /// Получение версию платформы 1С.
        /// </summary>
        /// <param name="accountDatabase">Информационная база.</param>
        /// <param name="demoAccountId"></param>
        /// <returns>Версия платформы.</returns>
        public PlatformVersionReference GetPlatformVersion(Domain.DataModels.AccountDatabase accountDatabase, Guid? demoAccountId = null)
        {
            var segment = accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId);

            if (accountDatabase.IsDelimiter())
                return GetPlatformVersionForDelimiterDatabase(accountDatabase, demoAccountId);

            if (accountDatabase.IsFile == false)
                return accountDatabase.PlatformType == PlatformType.V82
                    ? segment.Stable82PlatformVersionReference
                    : segment.Stable83PlatformVersionReference;

            if (accountDatabase.DistributionTypeEnum == DistributionType.Alpha)
            {
                if (segment.Alpha83PlatformVersionReference == null)
                    throw new InvalidOperationException("Альфа версия 8.3 не выбрана.");

                return segment.Alpha83PlatformVersionReference;
            }

            return accountDatabase.PlatformType == PlatformType.V82
                ? segment.Stable82PlatformVersionReference
                : segment.Stable83PlatformVersionReference;
        }

        /// <summary>
        /// Получить версию платформы 1С
        /// для базы на разделителях
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Версия платформы</returns>
        private PlatformVersionReference GetPlatformVersionForDelimiterDatabase(Domain.DataModels.AccountDatabase accountDatabase, Guid? demoAccountId = null)
        {
            if (accountDatabase.StateEnum is DatabaseState.ErrorCreate or DatabaseState.NewItem)
                return null;

            var accountId = GetAccountIdToDefineSegment(accountDatabase, demoAccountId);

            if (!accountId.HasValue)
                throw new NotFoundException(GenerateErrorMessageAboutReceivingStable83PlatformVersion(accountDatabase));

            return accountConfigurationDataProvider.GetAccountSegment(accountId.Value)
                       .Stable83PlatformVersionReference ??
                   throw new NotFoundException(
                       GenerateErrorMessageAboutReceivingStable83PlatformVersion(accountDatabase));
        }

        /// <summary>
        /// Сформировать сообщение об ошибке о получении стабильной версии платформы 8.3
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Сообщение об ошибке о получении стабильной версии платформы 8.3</returns>
        private string? GenerateErrorMessageAboutReceivingStable83PlatformVersion(Domain.DataModels.AccountDatabase accountDatabase) =>
            accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone == null
                ? $"Не удалось получить версию платформы для демо базы на разделителях {accountDatabase.Id} по служебному аккаунту {_serviceAccountIdForDemoDelimiterDatabases.Value}"
                : $"Не удалось получить версию платформы для базы на разделителях {accountDatabase.Id}";

        /// <summary>
        /// Получить Id аккаунта для определение сегмента
        /// </summary>
        /// <param name="accountDatabase">Id инф. базы</param>
        /// <param name="demoAccountId"></param>
        /// <returns>Id аккаунта для определение сегмента</returns>
        private Guid? GetAccountIdToDefineSegment(Domain.DataModels.AccountDatabase accountDatabase, Guid? demoAccountId) => 
            accountDatabase.IsDemoDelimiter() && accountDatabase.AccountDatabaseOnDelimiter.Zone == null
            ? demoAccountId ?? _serviceAccountIdForDemoDelimiterDatabases.Value
            : accountDatabase.AccountDatabaseOnDelimiter.SourceAccountDatabase?.AccountId;
    }
}
