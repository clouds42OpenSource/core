﻿using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Билдер сообщений в лог для инф. баз
    /// </summary>
    public class AccountDatabaseLogEventMessageBuilder
    {
        private readonly List<(Func<Domain.DataModels.AccountDatabase, bool>, Func<Domain.DataModels.AccountDatabase, string, string>)>
            _mapAcDbConditionToLogMessageList;
        
        private readonly IUnitOfWork _dbLayer;
        private readonly ISegmentHelper _segmentHelper;

        public AccountDatabaseLogEventMessageBuilder(ISegmentHelper segmentHelper, IUnitOfWork dbLayer)
        {
            _dbLayer = dbLayer;
            _segmentHelper = segmentHelper;
            _mapAcDbConditionToLogMessageList =
            [
                (acDb => acDb.IsDelimiter(), GetLogEventMessageForAcDbOnDelimiters),
                (acDb => acDb.IsFile == true, GetLogEventMessageForFileDatabase),
                (acDb => acDb.IsFile != true, GetLogEventMessageForServerDatabase)
            ];
        }

        /// <summary>
        /// Построить сообщение об удалении инф. базы для записи в логи
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Сообщение об удалении инф. базы для записи в логи</returns>
        public string BuildMessageAboutDeletion(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var commonLogMessage = $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} удалена.";
            return GetFullMessage(accountDatabase, commonLogMessage);
        }

        /// <summary>
        /// Построить сообщение перед удалением инф. базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Сообщение перед удалением инф. базы</returns>
        public string BuildMessageBeforeDeletion(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var commonLogMessage = $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} поставлена в очередь на удаление.";
            return GetFullMessage(accountDatabase, commonLogMessage);
        }

        /// <summary>
        /// Получить полное сообщение
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="commonLogMessage">Общее сообщение</param>
        /// <returns>Полное сообщение</returns>
        private string GetFullMessage(Domain.DataModels.AccountDatabase accountDatabase, string commonLogMessage)
        {
            var getLogMessageFunc = _mapAcDbConditionToLogMessageList
                .FirstOrDefault(tuple => tuple.Item1(accountDatabase)).Item2;

            return getLogMessageFunc == null
                ? commonLogMessage
                : getLogMessageFunc(accountDatabase, commonLogMessage);
        }

        /// <summary>
        /// Получить сообщение для базы на разделителях
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="commonLogMessage">Общее сообщение</param>
        /// <returns>Сообщение для базы на разделителях</returns>
        private static string GetLogEventMessageForAcDbOnDelimiters(Domain.DataModels.AccountDatabase accountDatabase, string commonLogMessage)
        {
            var isDemo = accountDatabase.IsDemoDelimiter()
                ? "да"
                : "нет";
            var zoneNumber = accountDatabase.AccountDatabaseOnDelimiter.Zone.HasValue
                ? $"{accountDatabase.AccountDatabaseOnDelimiter.Zone}"
                : "не создан";

            return
                $"{commonLogMessage} Область - {zoneNumber}. Демо - {isDemo}";
        }

        /// <summary>
        /// Получить сообщение для файловой базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="commonLogMessage">Общее сообщение</param>
        /// <returns>Сообщение для файловой базы</returns>
        private string GetLogEventMessageForFileDatabase(Domain.DataModels.AccountDatabase accountDatabase, string commonLogMessage)
        {
            var cloudServiceFileStorageServer =
                _dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(w =>
                    w.ID == accountDatabase.FileStorageID);
             var filePath = cloudServiceFileStorageServer == null 
                ? "не найдено"
                : Path.Combine(cloudServiceFileStorageServer.ConnectionAddress,
                    $@"company_{accountDatabase.Account.IndexNumber}\fileinfobases",
                    accountDatabase.V82Name);

            return $"{commonLogMessage} Фаловая база. Хранилище: {cloudServiceFileStorageServer?.Name}. Путь к хранилищу базы: {filePath}";
        }

        /// <summary>
        /// Получить сообщение для серверной базы
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="commonLogMessage">Общее сообщение</param>
        /// <returns>Сообщение для серверной базы</returns>
        private string GetLogEventMessageForServerDatabase(Domain.DataModels.AccountDatabase accountDatabase, string commonLogMessage)
        {
            var enterpriseServer = accountDatabase.PlatformType == PlatformType.V82
                ? _segmentHelper.GetEnterpriseServer82(accountDatabase.Account)
                : _segmentHelper.GetEnterpriseServer83(accountDatabase.Account);

            var sqlServer = _segmentHelper.GetSQLServer(accountDatabase.Account);
            return $"{commonLogMessage} Серверная база. Сервер предприятия: {enterpriseServer}; SQL сервер: {sqlServer}";
        }
    }
}
