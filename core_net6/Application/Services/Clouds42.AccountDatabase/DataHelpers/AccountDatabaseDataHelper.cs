﻿using Clouds42.Common.Exceptions;
using Clouds42.Domain.DataModels;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хелпер для работы с данными информационных баз
    /// </summary>
    public class AccountDatabaseDataHelper(IUnitOfWork dbLayer, AccountDatabasePathHelper accountDatabasePathHelper)
    {
        /// <summary>
        /// Получить информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>Информационная база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(Guid accountDatabaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId) ??
            throw new NotFoundException($"Не удалось получить базу данных по Id {accountDatabaseId}");

        /// <summary>
        /// Получить информационную базу
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <returns>Информационная база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId) =>
            dbLayer.DatabasesRepository.FirstOrDefault(w => w.Id == accountDatabaseId);

        /// <summary>
        /// Получить информационную базу
        /// </summary>
        /// <param name="v82Name">Название базы</param>
        /// <returns>Информационная база</returns>
        public Domain.DataModels.AccountDatabase GetAccountDatabaseOrThrowException(string v82Name) =>
            dbLayer.DatabasesRepository.FirstOrDefault(w => w.V82Name == v82Name) ??
            throw new NotFoundException($"Не удалось получить базу данных по названию {v82Name}");

        /// <summary>
        /// Получить путь подключения к инф. базе
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Путь подключения к инф. базе</returns>
        public string GetDatabaseConnectionPath(Domain.DataModels.AccountDatabase accountDatabase) => accountDatabase.IsFile == true
            ? accountDatabasePathHelper.GetPath(accountDatabase)
            : accountDatabasePathHelper.GetServerDatabaseConnectionPath(accountDatabase);

        /// <summary>
        /// Получить первую попавшуюся запись
        /// данных технической поддержки инф. базы
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        /// <returns>Поддержка инф. базы</returns>
        public AcDbSupport? GetFirstAcDbSupport(Guid databaseId) =>
            dbLayer.AcDbSupportRepository.FirstOrDefault(x => x.AccountDatabasesID == databaseId);
    }
}
