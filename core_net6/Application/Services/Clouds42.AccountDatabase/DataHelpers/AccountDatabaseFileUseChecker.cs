﻿using Clouds42.DataContracts.AccountDatabase.Interface;
using Clouds42.UploadedFiles.Helpers;
using Clouds42.AccountDatabase.Contracts.Check.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.PowerShellClient;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.DataHelpers
{
    public class AccountDatabaseFileUseCheck : IAccountDatabaseFileUseCheck
    {
        /// <inheritdoc/>
        public bool Check(string accountDatabasePath) =>
            AccountDatabaseFileUseChecker.Check(accountDatabasePath);

        /// <inheritdoc/>
        public void WaitWhileFileDatabaseUsing(string accountDatabasePath, int minutes = 15) =>
            AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(accountDatabasePath, minutes);

        /// <inheritdoc/>
        public void WaitWhileFileDatabaseUsing(IUpdateDatabaseDto database, string databaseName, string dnsName, int minutes = 15) =>
            AccountDatabaseFileUseChecker.WaitWhileFileDatabaseUsing(database, databaseName, dnsName, minutes);
    }

    /// <summary>
    /// Класс проверки наличия активных сессий в файловой информационной базе.
    /// </summary>
    public static class AccountDatabaseFileUseChecker
    {
        private static readonly ILogger42 _logger = Logger42.GetLogger();
        /// <summary>
        /// Выполнить проверку наличия активных сессий в файловой информационной базе.
        /// </summary>
        /// <returns>true - есть активные сессии, false - активных сессий нет.</returns>
        public static bool Check(string accountDatabasePath)
        {
            var filePath = $@"{accountDatabasePath}\1Cv8.1CD";
            if (!File.Exists(filePath))
                throw new NotFoundException($"Файл по пути {filePath} не найден.");

            return FileUseChecker.CheckFileUsage(filePath);
        }

        /// <summary>
        /// Ожидать пока база используется
        /// </summary>
        /// <param name="accountDatabasePath">Путь к базе</param>
        /// <param name="minutes">Кол-во минут ожидания</param>
        public static void WaitWhileFileDatabaseUsing(string accountDatabasePath, int minutes = 15)
        {
            var filePath = Path.Combine(accountDatabasePath, "1Cv8.1CD");
            if (!File.Exists(filePath))
                throw new NotFoundException($"Файл по пути {filePath} не найден.");

            FileUseChecker.WaitWhileFileUsing(filePath, 60, minutes * 60);
        }

        /// <summary>
        /// Ожидать пока база данных файловой базы используется.
        /// </summary>        
        public static void WaitWhileFileDatabaseUsing(IUpdateDatabaseDto database, string databaseName, string dnsName, int minutes = 15)
        {
            try
            {
                var filePath = $@"{database.ConnectionAddress}\1Cv8.1CD";
                _logger.Info($"{database.ConnectionAddress}. Ожидать пока база данных файловой базы используется");
                FileUseChecker.WaitWhileFileUsing(filePath, 60, minutes * 60);
            }
            catch 
            {
                _logger.Info($"{database.ConnectionAddress}. Завершаем все открытые сессии");

                var powerShellClientWrapper = new PowerShellClientWrapper(_logger);
                powerShellClientWrapper.ExecuteScript(
                        "Get-SmbOpenFile | where {$_.Path –like \"*UpdateTemplates*\"} | Close-SmbOpenFile -Force",
                        dnsName);
                powerShellClientWrapper.ExecuteScript(
                        $"Get-SmbOpenFile | where {{$_.Path –like \"*{databaseName}*\"}} | Close-SmbOpenFile -Force",
                        dnsName);
                _logger.Info($"{database.ConnectionAddress}. Сессии завершены");
            }
            
        }
    }
}
