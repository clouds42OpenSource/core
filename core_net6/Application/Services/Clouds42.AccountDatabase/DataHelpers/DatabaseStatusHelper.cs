﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хелпер для работы со статусом информационной базы
    /// </summary>
    public class DatabaseStatusHelper(
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger,
        IHandlerException handlerException)
    {
        /// <summary>
        /// Установить статус базе
        /// </summary>
        /// <param name="v82Name">Номер базы</param>
        /// <param name="status">Статус</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        public void SetDbStatus(string v82Name, DatabaseState status, string? createAccountDatabaseComment = null) =>
            SetDbStatus(accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(v82Name), status, createAccountDatabaseComment);

        /// <summary>
        /// Установить статус базе
        /// </summary>
        /// <param name="accountDatabaseId">Id базы</param>
        /// <param name="status">Статус</param>
        /// <param name="createAccountDatabaseComment">Комментарий по созданию базы</param>
        public void SetDbStatus(Guid accountDatabaseId, DatabaseState status,
            string? createAccountDatabaseComment = null) => SetDbStatus(
            accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(accountDatabaseId), status,
            createAccountDatabaseComment);
        
        /// <summary>
        /// Установить статус базе
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <param name="status">Статус</param>
        /// <param name="createAccountDatabaseComment">Коммментарий по созданию базы</param>
        public void SetDbStatus(Domain.DataModels.AccountDatabase accountDatabase, DatabaseState status,
            string? createAccountDatabaseComment = null)
        {
            try
            {
                logger.Trace("Установка статуса базы данных {0} {1}", accountDatabase.Id, status.ToString());

                accountDatabaseChangeStateProvider.ChangeState(accountDatabase, status, createAccountDatabaseComment);

                logger.Trace("Статус базы данных установлен {0} {1}", accountDatabase.Id, status.ToString());
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка при уставноке статуса базы] {accountDatabase.Id} ");
                throw;
            }
        }
    }
}
