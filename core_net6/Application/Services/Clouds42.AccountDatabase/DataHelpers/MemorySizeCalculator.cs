﻿using System.Collections;
using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Domain;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using File = System.IO.File;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Калькулятор размера файлов
    /// </summary>
    public class MemorySizeCalculator(
        ISegmentHelper segmentHelper,
        IAccountDatabasePathHelper accountDatabasePathHelper,
        IUnitOfWork unitOfWork,
        ILogger42 logger,
        IHandlerException handlerException)
        : IMemorySizeCalculator
    {
        /// <summary>
        /// Подсчитавыает место, занимаемое файловой информационной базой
        /// Вовзращает значение в Мегабайтах
        /// </summary>
        /// <param name="dbInfo">Инф. база</param>
        /// <returns>Место, занимаемое файловой информационной базой</returns>
        public int CalculateFile1CDataBase(Domain.DataModels.AccountDatabase dbInfo)
        {
            long? dbSize = 0;
            var filepath = accountDatabasePathHelper.GetPath(dbInfo);
            try
            {
                if (Directory.Exists(filepath))
                    dbSize += CalculateCatalog(filepath);

                if(dbSize == null)
                {
                    return 0;
                }

                return (int)(dbSize / 1024 / 1024);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения размера файловой базы] Ошибка получения размера файловой базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dbInfo)} по пути {filepath}.");
                return 0;
            }
        }

        /// <summary>
        /// Подсчитавыает место, занимаемое файловой информационной базой
        /// Вовзращает значение в Мегабайтах
        /// </summary>
        /// <param name="dbInfo">Инф. база</param>
        /// <returns>Место, занимаемое файловой информационной базой</returns>
        public DateTime CalculateLastActiveDataBase(Domain.DataModels.AccountDatabase dbInfo)
        {
            var accountDatabasePath = accountDatabasePathHelper.GetPath(dbInfo);
            try
            {
                if (!Directory.Exists(accountDatabasePath))
                {
                    return dbInfo.LastActivityDate;
                }

                var filePath = $@"{accountDatabasePath}\1Cv8.1CD";
                if (!File.Exists(filePath))
                {
                    logger.Error($"Файл по пути {filePath} не найден.");
                    return dbInfo.LastActivityDate;
                }
                var date = File.GetLastWriteTime(filePath);
                if (dbInfo.CreationDate > date)
                {
                    logger.Info($"База только создана, поэтому дата активноостим равно дате создания: {dbInfo.CreationDate}.");
                    return dbInfo.CreationDate;
                }
                          
                logger.Info($"Дата последнего изменения: {date}.");

                return date;


            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения размера файловой базы] Ошибка получения размера файловой базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(dbInfo)} по пути {accountDatabasePath}.");

                return dbInfo.LastActivityDate;
            }
        }

        /// <summary>
        /// Подсчитать место занимаемое файлами указанного каталога.
        /// </summary>
        /// <param name="catalogPath">Путь для проверки</param>
        /// <returns>Размер занимаемого места</returns>
        public long? CalculateCatalog(string catalogPath)
        {
          
            if (!Directory.Exists(catalogPath))
            {
                logger.Debug($"Указаный каталог отсутствует {catalogPath}");

                return 0;
            }

            logger.Info($"Начало расчета размера каталога {catalogPath}");
            
            try
            {
                return CalcDirSize(new DirectoryInfo(catalogPath));
            }

            catch (Exception ex)
            {
                logger.Error(ex, $"Ошибка рассчета размера каталога {catalogPath}");

                return null;
            }
        }

        private long CalcDirSize(DirectoryInfo di)
        {
            long size = 0;
            try
            {
                var fiEntries = di.GetFiles();

                foreach (var fiEntry in fiEntries)
                {
                    Interlocked.Add(ref size, fiEntry.Length);
                }

                var diEntries = di.GetDirectories("*.*", SearchOption.TopDirectoryOnly);

                var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = Math.Max(1, System.Environment.ProcessorCount * 2 / 3) };

                Parallel.For<long>(0, diEntries.Length, parallelOptions, () => 0, (i, loopState, subtotal) =>
                    {
                        try
                        {
                            if ((diEntries[i].Attributes & FileAttributes.ReparsePoint) == FileAttributes.ReparsePoint)
                                return subtotal;

                            subtotal += CalcDirSize(diEntries[i]);

                            return subtotal;
                        }

                        catch (IOException ex)
                        {
                            logger.Error(ex, $"Ошибка калькульяции размеров файлов в директории {diEntries[i].FullName}. Работа будет продолжена.");

                            return subtotal;
                        }

                        catch (UnauthorizedAccessException uae)
                        {
                            logger.Error(uae, $"Ошибка доступа к директории. {diEntries[i].FullName}. Работа будет продолжена.");
                            
                            return subtotal;
                        }

                        catch (Exception uae)
                        {
                            logger.Error(uae, $"Необработанное исключение в параллельном запросе. Директория {diEntries[i].FullName}. Работа будет продолжена.");

                            return subtotal;
                        }
                    },
                    x => Interlocked.Add(ref size, x));

            }

            catch (IOException ex)
            {
                logger.Error(ex, $"Общая ошибка калькуляции размеров файлов в директории {di.FullName}");
            }

            catch (UnauthorizedAccessException uae)
            {
                logger.Error(uae, $"Общая ошибка доступа к директории {di.FullName}");
                
            }

            catch (Exception e)
            {
                logger.Error(e, $"Необработанная ошибка доступа к директории {di.FullName}");
            }

            return size;
        }

        /// <summary>
        /// Подсчитывает размер базы данных на SQL сервере
        /// Вовзращает значение в Мегабайтах
        /// </summary>
        /// <param name="dbName">Имя базы</param>
        /// <returns>Размер базы данных на SQL сервере</returns>
        public int CalculateSqlDataBase(string dbName)
        {
            try
            {
                var accountDatabase = unitOfWork.DatabasesRepository.FirstOrDefault(acDb => acDb.SqlName == dbName);
                var account = unitOfWork.AccountsRepository.FirstOrDefault(a => a.Id == accountDatabase.AccountId);
                var result = unitOfWork.Execute1CSqlReader(string.Format(SqlQueries.DatabaseCore.GetDatabaseSizeInMb, dbName), segmentHelper.GetSQLServer(account));

                return GetDbSizeFromSqlResult(result);
            }
            catch (Exception ex)
            {
                logger.Warn($"Произошла ошибка при расчете размера серверной инф. базы {dbName}. Причина: {ex.GetFullInfo()}");
                return 0;
            }
        }

        /// <summary>
        /// Подсчитать объем клиентских файлов.
        /// </summary>
        /// <param name="account">Аккаунт</param>
        /// <returns>Объем клиентских файлов</returns>
        public long CalculateClintFilesSize(Account account, long? size)
        {
            try
            {
                logger.Info($"Перерасчет клиентских файлов: {account.IndexNumber}");                
                var fileStorage = segmentHelper.GetFullClientFileStoragePath(account);

                var catalogPath = Path.Combine(fileStorage);

                if (!Directory.Exists(catalogPath))
                    return 0;

                logger.Info($"Путь к клиентским файлам: {catalogPath}");

                var clientFileSize = CalculateCatalog(catalogPath);
                if(clientFileSize == null)
                {
                    return size ?? 0;
                }
                var inMb = clientFileSize.Value / 1024 / 1024;

                logger.Info($"Размер клиентских файлов: Аккаунт - {account.IndexNumber}; Размер: {inMb} MB");

                return inMb;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Error while calculation client file size]. accIndex = {account.IndexNumber}");

                return 0;
            }
        }



        /// <summary>
        /// Получить размер базы из результата выполнения SQL запроса
        /// </summary>
        /// <param name="result">результат выполнения SQL запроса</param>
        /// <returns>размер базы в мегабайтах</returns>
        private int GetDbSizeFromSqlResult(IEnumerable result)
        {
            try
            {
                var res = result.Cast<int?>().FirstOrDefault();
                return res ?? 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
