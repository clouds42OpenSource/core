﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.IDataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Для формирования пути нахождения информационных баз
    /// </summary>
    public class AccountDatabasePathHelper(
        IUnitOfWork dbLayer,
        ILogger42 logger,
        ISegmentHelper segmentHelper,
        IHandlerException handlerException)
        : IAccountDatabasePathHelper
    {
        /// <summary>
        /// Название параметра для строки подключения к серверу 1С
        /// </summary>
        private const string Server1CConnectionNameParam = "Srvr=";

        /// <summary>
        /// Название параметра для строки подключения к инф. базе
        /// </summary>
        private const string AccountDatabaseNameConnectionParam = "Ref=";

        /// <summary>
        /// Получить путь к ИБ
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Путь к ИБ</returns>
        public string GetPath(Guid accountDatabaseId)
        {
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.Id == accountDatabaseId);
            if (accountDatabase == null)
                throw new NotFoundException($"Не удалось получить данные о информационной базе по номеру {accountDatabaseId}");

            return GetPath(accountDatabase);
        }

        /// <summary>
        /// Получить путь к ИБ
        /// </summary>
        /// <param name="database">ИБ аккаунта</param>
        /// <returns>путь к ИБ</returns>
        public string GetPath(IAccountDatabase database)
        {
            if (database == null)
            {
                logger.Trace("Базы нет и сформировать путь невозможно.");
                throw new ArgumentNullException(nameof(database));
            }

            logger.Trace($"Получение пути для базы {database.V82Name}.");
            try
            {
                if (!database.IsFile.HasValue || database.IsFile == false)
                {
                    logger.Trace($"Пустой путь для серверной базы {database.V82Name}.");
                    return null;
                }
                if (!database.FileStorageID.HasValue)
                {
                    var errorMessage = $"Не указан ID для хранилища для базы {database.V82Name}.";
                    throw new ArgumentNullException(errorMessage);
                }
                var cloudServiceFileStorageServer =
                    dbLayer.CloudServicesFileStorageServerRepository.FirstOrDefault(w => w.ID == database.FileStorageID);

                if (cloudServiceFileStorageServer == null)
                {
                    var errorMessage = $"Хранилище для базы {database.V82Name} по ID {database.FileStorageID} не найдено.";
                    throw new ArgumentNullException(errorMessage);
                }

                var account = GetAccountOrThrowException(database.AccountId);
                var filePath = Path.Combine(cloudServiceFileStorageServer.ConnectionAddress,
                    $@"company_{account.IndexNumber}\fileinfobases",
                    database.V82Name);

                logger.Trace("Получен путь для базы данных {0} {1}", database.V82Name, filePath);
                return filePath;
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка получения пути к базе] База: {database.V82Name}");
                throw;
            }
        }

        /// <summary>
        /// Метод формирует потенциальный путь размещения ИБ после миграции
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="targetStorage">Хранилище, в которое нужно перенести базу</param>
        /// <returns>Потенциальный путь размещения ИБ</returns>
        public string GetPath(Domain.DataModels.AccountDatabase accountDatabase, CloudServicesFileStorageServer targetStorage)
        {
            try
            {
                var companyName = GenerateCompanyName(accountDatabase);

                var companyPath = Path.Combine(targetStorage.ConnectionAddress, companyName);

                return Path.Combine(companyPath, "fileinfobases", accountDatabase.V82Name);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка формирования потенциального пути для миграции ИБ] '{accountDatabase.V82Name}'");
                throw;
            }
        }
        /// <summary>
        /// Генерирует имя папки, в которой хранятся папки с файлами ИБ 
        /// </summary>
        /// <param name="accountDatabase"></param>
        /// <returns></returns>
        public static string GenerateCompanyName(Domain.DataModels.AccountDatabase accountDatabase)
        {
            return $"company_{accountDatabase.Account.IndexNumber}";
        }

        /// <summary>
        /// Получить строку подключения к серверной инф. базе
        /// </summary>
        /// <returns>Строка подключения к серверной инф. базе</returns>
        public string GetServerDatabaseConnectionPath(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var account = GetAccountOrThrowException(accountDatabase.AccountId);
            if (accountDatabase.IsDelimiter())
                account = (Account)accountDatabase.GetSourceAccount();
            
            var server1C = accountDatabase.PlatformType == PlatformType.V83
                ? segmentHelper.GetEnterpriseServer83(account)
                : segmentHelper.GetEnterpriseServer82(account);
            return $"{Server1CConnectionNameParam}\'{server1C}\';{AccountDatabaseNameConnectionParam}\'{accountDatabase.V82Name}\'";
        }

        /// <summary>
        /// Получить аккаунт или выкинуть исключение если он не найден
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Account GetAccountOrThrowException(Guid accountId)
            => dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountId) ??
               throw new NotFoundException($"Аккаунт по ID {accountId} не найден");
    }
}
