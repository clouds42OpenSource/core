﻿using System.Text.RegularExpressions;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Enums._1C;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Вспомогательный класс для формирования строки подключения к 1С из составляющих частей
    /// </summary>
    internal static class AccountDatabase1CServerName
    {
        private static readonly Tuple<Regex, PlatformType>[] RegexTuples =
        [
            Tuple.Create(new Regex("^(?:V82|8.2)$"), PlatformType.V82),
            Tuple.Create(new Regex("^(?:V83|8.3)$"), PlatformType.V83),
            Tuple.Create(new Regex("^Undefined$"), PlatformType.Undefined)
        ];

        private static PlatformType ParsePlatform(string platformStr)
        {
            foreach (var regexTuple in RegexTuples)
            {
                if (regexTuple.Item1.IsMatch(platformStr))
                    return regexTuple.Item2;
            }

            return PlatformType.V82;
        }

        /// <summary>
        /// Метод, возвращающий строку подключения к 1С согласно принятым правилам
        /// </summary>
        public static string Create(AccessDatabaseListQueryDataDto accountDatabaseRecord)
        {

            if (!string.IsNullOrEmpty(accountDatabaseRecord.SourceDatabaseClusterName))
                return accountDatabaseRecord.SourceDatabaseClusterName;

            if (!string.IsNullOrEmpty(accountDatabaseRecord.V82Server))
                return accountDatabaseRecord.V82Server;

            return ParsePlatform(accountDatabaseRecord.Platform ?? string.Empty) switch
            {
                PlatformType.V82 => accountDatabaseRecord.ConnectionAddressV82,
                PlatformType.V83 => accountDatabaseRecord.ConnectionAddressV83,
                PlatformType.Undefined => string.Empty,
                _ => string.Empty
            };
        }
    }
}
