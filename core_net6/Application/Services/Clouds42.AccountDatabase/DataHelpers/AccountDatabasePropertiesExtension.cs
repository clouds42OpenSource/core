﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.DataHelpers
{
    public static class AccountDatabasePropertiesExtension
    {

        /// <summary>
        /// Преобразовать в DTO модель.
        /// </summary>
        /// <param name="accessDatabaseListQueryDataItem">Доменная модель базы данных.</param>
        public static AccountDatabasePropertiesDto ConvertToDto(this AccessDatabaseListQueryDataDto accessDatabaseListQueryDataItem)
        {
            var accountDatabaseItem = new AccountDatabasePropertiesDto
            {
                ID = accessDatabaseListQueryDataItem.Id,
                AccountID = accessDatabaseListQueryDataItem.AccountId,
                CreationDate = accessDatabaseListQueryDataItem.CreationDate,
                LastActivityDate = accessDatabaseListQueryDataItem.LastActivityDate,
                Caption = accessDatabaseListQueryDataItem.Caption ?? string.Empty,
                ConfigurationName = accessDatabaseListQueryDataItem.ConfigurationName ?? string.Empty,
                ConfigurationVersion = accessDatabaseListQueryDataItem.ConfigurationVersion ?? string.Empty,
                ApplicationName = accessDatabaseListQueryDataItem.ApplicationName ?? string.Empty,
                LinkServicePath = accessDatabaseListQueryDataItem.LinkServicePath,
                WebPublishPath = AccountDatabaseWebPublishPath.GetWebPublishPath(accessDatabaseListQueryDataItem),
                IsPublished = accessDatabaseListQueryDataItem.PublishState == PublishState.Published.ToString(),
                PublishState = (PublishState) Enum.Parse(typeof(PublishState), accessDatabaseListQueryDataItem.PublishState),
                DbName = accessDatabaseListQueryDataItem.DbName,
                DbNumber = accessDatabaseListQueryDataItem.DbNumber,
                State = (accessDatabaseListQueryDataItem.State == "DetachedToTomb")
                    ? "Detached"
                    : accessDatabaseListQueryDataItem.State,
                SqlName = accessDatabaseListQueryDataItem.SqlName,
                ServerName = AccountDatabase1CServerName.Create(accessDatabaseListQueryDataItem),
                LockedState = accessDatabaseListQueryDataItem.LockedState,
                IsFile = accessDatabaseListQueryDataItem.IsFile ?? false,
                FilePath =
                    accessDatabaseListQueryDataItem.IsFile ?? false
                        ? AccountDatabasePathDto.Create(
                            accessDatabaseListQueryDataItem.ConnectionAddress,
                            accessDatabaseListQueryDataItem.IndexNumber,
                            accessDatabaseListQueryDataItem.DbName
                        )
                        : null,
                SizeInMb = accessDatabaseListQueryDataItem.SizeInMb,
                TemplateId = accessDatabaseListQueryDataItem.TemplateId,
                ServiceName = accessDatabaseListQueryDataItem.ServiceName,
                LaunchType = accessDatabaseListQueryDataItem.LaunchType ?? 0,
                IsLock = accessDatabaseListQueryDataItem.IsLock ?? false,
                RolesJHO = accessDatabaseListQueryDataItem.RolesJhoString,
                AccountCaption = accessDatabaseListQueryDataItem.AccountCaption,
                IsDemoExpired = accessDatabaseListQueryDataItem.IsDemoExpired,
                LaunchParameters = accessDatabaseListQueryDataItem.LaunchParameters ?? string.Empty,
                DelimiterInfo = AccountDatabaseDelimiterInfo.GetDelimiterInfo(accessDatabaseListQueryDataItem),
                DistributionType = accessDatabaseListQueryDataItem.DistributionType,
                PlatformVersionAlpha = accessDatabaseListQueryDataItem.SegmentAlpha83Version,
                PlatformVersionStable = !string.IsNullOrEmpty(accessDatabaseListQueryDataItem.ApplicationName) && accessDatabaseListQueryDataItem.ApplicationName == "8.2"
                ? accessDatabaseListQueryDataItem.SegmentStable82Version
                : accessDatabaseListQueryDataItem.SegmentStable83Version,
            };
            return accountDatabaseItem;
        }
    }
}
