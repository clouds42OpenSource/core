﻿namespace Clouds42.AccountDatabase.DataHelpers
{

    /// <summary>
    /// Калькулятор оптимального времени суток переноса  архивов в склеп.
    /// Примечание: это нужно что бы не грузить сервера бэкапапирования ночью.
    /// </summary>
    public static class OptimalTimeForSendBackupToTombCalculator
    {
        /// <summary>
        ///     Получить время оптимального переноса в склеп
        /// </summary>
        public static DateTime Calculate()
        {
            var now = DateTime.Now;

            var minTime = new DateTime(now.Year, now.Month, now.Day, 10, 0, 0);
            var maxTime = new DateTime(now.Year, now.Month, now.Day, 21, 0, 0);

            var startTask = now.AddMinutes(5);
            if (startTask >= minTime && startTask <= maxTime)
                return startTask;

            var daysInMonth = DateTime.DaysInMonth(now.Year, now.Month);

            if (daysInMonth < now.Day + 1)
            {
                return new DateTime(now.Year, now.Hour < 10 ? now.Month : now.Month + 1, now.Hour < 10 ? now.Day : 1,
                    10, 0, 0);
            }
            return new DateTime(now.Year, now.Month, now.Hour < 10 ? now.Day : now.Day + 1, 10, 0, 0);
        }
    }
}
