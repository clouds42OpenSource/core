﻿using Clouds42.Common.Extensions;
using Clouds42.DataContracts.SQLNativeModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Вспомогательный класс, для формирования адреса к опубликованной базе, согласно принятым правилам.
    /// </summary>
    public static class AccountDatabaseWebPublishPath
    {
        /// <summary>
        /// Формирование адреса к опубликованной базе на основании имени сайта полученного из сегмента
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        public static string GetWebPublishPath(AccessDatabaseListQueryDataDto accountDatabase)
        {
            if (accountDatabase.PublishState != PublishState.Published.ToString())
                return null;

            if (accountDatabase.State != DatabaseState.Ready.ToString())
                return null;

            if (accountDatabase is { DelimiterDatabaseId: not null, IsDemo: true })
            {
                return accountDatabase.DemoSiteName;
            }

            return IsDbOnDelimiters(accountDatabase)
                ? GetWebPublishPathForOnDelimiters(accountDatabase)
                : Create(accountDatabase.PublishSiteName,
                    accountDatabase.GroupByAccount,
                    accountDatabase.AccountId,
                    accountDatabase.DbName);
        }

        /// <summary>
        /// Формирование адреса к опубликованной базе на основании имени сайта полученного из сегмента
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        public static string GetWebPublishPath(DelimitersDatabaseQueryDataDto accountDatabase)
        {
            if (accountDatabase.Zone == null)
            {
                return null;
            }

            return GetWebPublishPathForOnDelimiters(accountDatabase);
        }

        /// <summary>
        /// Метод для формирования адреса к опубликованной базе.
        /// </summary>
        /// <param name="publishSiteName">Cайта публикации</param>
        /// <param name="groupByAccount">Включена ли группировка на ContentServer</param>
        /// <param name="accountId">Идентификатор аккаунта</param>
        /// <param name="dbName">Имя базы данных</param>
        /// <returns>Путь к базе данных</returns>
        public static string Create(string publishSiteName, bool groupByAccount, Guid accountId, string dbName)
        {
            if (string.IsNullOrWhiteSpace(publishSiteName)) return null;
            var webPublishPath = groupByAccount
                ?$"{accountId.GetEncodeGuid()}/{dbName}"
                :$"{dbName}";

            return WebPublishPath(publishSiteName, Uri.UriSchemeHttps, webPublishPath);
        }

        /// <summary>
        /// Метод для формирования адреса к опубликованной базе для разделителей.
        /// </summary>
        /// <param name="accountDatabase"></param>
        /// <returns>Путь к базе данных</returns>
        private static string GetWebPublishPathForOnDelimiters(IPublishDatabaseDataDto accountDatabase)
        {
            var builder = new UriBuilder(accountDatabase.SiteName);
            var path = $"{builder.Path.TrimEnd('/')}/{accountDatabase.DbSourceName}/{accountDatabase.Zone}";
            return WebPublishPath(builder.Uri.Host, builder.Scheme, path);
        }

        /// <summary>
        /// Формирование строки веб сайта
        /// </summary>
        /// <param name="publishSiteName"></param>
        /// <param name="scheme"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string WebPublishPath(string publishSiteName, string scheme, string path)
        {
            var builder = new UriBuilder
            {
                Host = publishSiteName,
                Path = path,
                Scheme = scheme
            };
            return builder.Uri.AbsoluteUri;
        }

        /// <summary>
        /// Проверить база данных на разделителях или нет
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns></returns>
        private static bool IsDbOnDelimiters(AccessDatabaseListQueryDataDto accountDatabase)
        {
            return accountDatabase.Zone != null && !string.IsNullOrWhiteSpace(accountDatabase.DbSourceName)
                                                && !string.IsNullOrWhiteSpace(accountDatabase.SiteName);
        }
    }
}