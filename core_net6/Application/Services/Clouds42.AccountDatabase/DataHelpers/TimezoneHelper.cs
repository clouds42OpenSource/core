﻿using Clouds42.AccountDatabase.DataHelpers.Constants;
using Clouds42.Configurations.Configurations;
using Clouds42.DataContracts.Account.Locale;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хелпер для работы с временной зоной
    /// </summary>
    public static class TimezoneHelper
    {
        /// <summary>
        /// Получить временную зону
        /// </summary>
        /// <param name="accountLocale">Название локали аккаунта</param>
        /// <returns>Временная зона</returns>
        public static string GetTimezone(string accountLocale) => accountLocale == LocaleConstantsDto.Rus
            ? TimezoneConst.MoscowTimezone
            : TimezoneConst.KievTimezone;

        /// <summary>
        /// Время по временной зоне
        /// </summary>
        /// <param name="accountLocale">Название локали аккаунта</param>
        /// <param name="date">Время</param>
        /// <returns>Время по временной зоне</returns>
        public static DateTime? DateTimeToTimezone(string accountLocale, DateTime? date)
        {
            if (!date.HasValue)
                return null;

            return DateTimeToTimezone(accountLocale, date.Value);
        }

        /// <summary>
        /// Время по временной зоне
        /// </summary>
        /// <param name="accountLocale">Локаль аккаунта</param>
        /// <param name="date">Время</param>
        /// <returns>Время по временной зоне</returns>
        public static DateTime DateTimeToTimezone(string accountLocale, DateTime date)
        {
            if (accountLocale == LocaleConstantsDto.Rus)
                return date;

            var timeDiff = CloudConfigurationProvider.CloudCore.GetHoursDifferenceOfUkraineAndMoscow();

            return date.AddHours(timeDiff);
        }
    }
}