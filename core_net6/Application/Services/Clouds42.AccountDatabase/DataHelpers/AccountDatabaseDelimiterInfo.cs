﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseModels;
using Clouds42.DataContracts.SQLNativeModels;

namespace Clouds42.AccountDatabase.DataHelpers
{
	/// <summary>
	/// Вспомагательный класс для заполнения информации о разделителях
	/// </summary>
	static class AccountDatabaseDelimiterInfo
	{

		/// <summary>
		/// Формирование данных о разделителях
		/// </summary>
		/// <param name="accountDatabase">Информационная база</param>
		public static AccountDatabaseDelimiterDto GetDelimiterInfo(AccessDatabaseListQueryDataDto accountDatabase)
		{
			return accountDatabase.DelimiterDatabaseId != null
				? new AccountDatabaseDelimiterDto
					{
						Zone = accountDatabase.Zone,
						SourceDatabaseName = accountDatabase.DbSourceName,
                        IsDemo = accountDatabase.IsDemo ?? false,
                        PlatformVersion = accountDatabase.DelimiterPlatformVersion,
                        DelimiterDatabaseMustUseWebService = accountDatabase.DelimiterDatabaseMustUseWebService
                }
				: null;
		}

	}
}
