﻿using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хэлпер для синхронизации рабочих процессов по инф. базам
    /// </summary>
    public static class SynchronizeAccountDatabaseProcessFlowsHelper
    {
        /// <summary>
        /// Создать ключ основного рабочего процесса
        /// </summary>
        /// <param name="accountDatabaseBackupId">ID бэкапа инф. базы</param>
        /// <returns>Ключ основного рабочего процесса</returns>
        public static string CreateParentProcessFlowId(Guid accountDatabaseBackupId)
            => $"{Guid.NewGuid()}::{accountDatabaseBackupId}";

        /// <summary>
        /// Получить имя обрабатываемого объекта для рабочего процесса создания инф. базы на основании бэкапа.
        /// </summary>
        /// <param name="infoDatabaseDomainModel">Модель параметров создания инф. базы на основании бэкапа</param>
        /// <param name="parentProcessFlowKey">Ключ основного рабочего процесса</param>
        /// <returns>Имя обрабатываемого объекта.</returns>
        public static string GetProcessedObjectNameForCreateAccountDatabaseFromBackup(InfoDatabaseDomainModelDto infoDatabaseDomainModel, string parentProcessFlowKey = null)
        {
            if (infoDatabaseDomainModel == null)
                throw new InvalidOperationException("Модель инф. базы пуста");

            var additionalParam = !string.IsNullOrEmpty(parentProcessFlowKey)
                ? $"::{parentProcessFlowKey}"
                : null;

            return $"{infoDatabaseDomainModel.AccountDatabaseBackupId}::{infoDatabaseDomainModel.AccountId}{additionalParam}";
        }
    }
}
