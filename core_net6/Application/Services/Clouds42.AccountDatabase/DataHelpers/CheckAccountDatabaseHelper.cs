﻿using Clouds42.Common.DataModels;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Helpers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хелпер для выполнения проверок над информационной базой
    /// </summary>
    public class CheckAccountDatabaseHelper(AccountDatabasePathHelper accountDatabasePathHelper)
    {
        /// <summary>
        /// Проверить возможность переноса информационной базы в склеп.
        /// </summary>
        /// <param name="database">Информационная база.</param>
        public void CheckPossibilitiesArchiveDatabasesToTomb(Domain.DataModels.AccountDatabase database)
        {
            if (database.StateEnum != DatabaseState.Ready)
                throw new InvalidOperationException($"[{database.Id}] ::Информационная база не в статусе реди {database.StateEnum.ToString()}");

            if (database.IsFile == true && !database.IsDelimiter())
            {
                var path = accountDatabasePathHelper.GetPath(database);
                if (!Directory.Exists(path))
                    throw new NotFoundException($"[{database.Id}] ::Отсутствует директория информационной базы по пути {path}");

                if (!DirectoryHelper.DirectoryHas1CDatabaseFile(path))
                    throw new NotFoundException($"[{database.Id}] ::В директории информационной базы по пути {path} не найден файл инфорамционной базы.");
            }

            if (database.IsDelimiter() && database.AccountDatabaseOnDelimiter.Zone == null)
                throw new NotFoundException($"[{database.Id}] :: В базе на разделителях нет номера зоны");
        }

        /// <summary>
        /// Авторизованный пользователь может редактировать базу
        /// </summary>
        /// <param name="userPrincipal">Авторизованный пользователь</param>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Возможность редактировать базу</returns>
        public static bool CanEditDatabase(IUserPrincipalDto userPrincipal, Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (userPrincipal == null)
                return false;

            if (userPrincipal.Groups.All(x => x != AccountUserGroup.CloudAdmin && x != AccountUserGroup.Hotline))
                return accountDatabase.Account?.AccountSaleManager != null &&
                       accountDatabase.Account.AccountSaleManager.SaleManagerId == userPrincipal.Id;

            return true;
        }
    }
}
