﻿using Clouds42.Domain.IDataModels;

namespace Clouds42.AccountDatabase.DataHelpers
{
    public static class AccountDatabase1CInfoBaseHelper
    {
        /// <summary>
        /// Получить полное название базы 1С
        /// </summary>
        /// <param name="caption">Заголовок базы 1С</param>
        /// <param name="v82Name">Номер информационной базы</param>
        /// <returns>Полное имя базы 1С</returns>
        public static string GetDatabase1CFullName(string caption, string v82Name) => $"\"{caption}\" ({v82Name})";

        /// <summary>
        /// Получить полное название базы 1С
        /// </summary>
        /// <param name="accountDatabase"> Модель AccountDatabase</param>
        /// <returns>Полное имя базы 1С</returns>
        public static string GetDatabase1CFullName(IAccountDatabase accountDatabase) =>
            accountDatabase == null ? "" : GetDatabase1CFullName(accountDatabase.Caption, accountDatabase.V82Name);
    }
}
