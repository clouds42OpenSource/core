﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.DataHelpers
{
    /// <summary>
    /// Хелпер для работы путем публикации инф. базы
    /// </summary>
    public class AccountDatabaseWebPublishPathHelper(
        IUnitOfWork dbLayer,
        IAccountConfigurationDataProvider accountConfigurationDataProvider)
        : IAccountDatabaseWebPublishPathHelper
    {
        /// <summary>
        /// Формирование адреса к опубликованной базе на основании имени сайта полученного из сегмента
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Адрес к опубликованной базе</returns>
        public string GetWebPublishPath(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.AccountDatabaseOnDelimiter == null)
                return GetWebPublishPathForRegular(accountDatabase);

            return accountDatabase.StateEnum != DatabaseState.Ready ? null : GetDatabaseOnDelimitersPublicationAddress(accountDatabase);
        }

        /// <summary>
        /// Сформировать адрес к опубликованной базе на основании имени сайта полученного из сегмента
        /// </summary>
        /// <param name="databaseId">Id информационной базы</param>
        /// <returns>Адрес к опубликованной базе</returns>
        public string GetWebPublishPath(Guid databaseId) => GetWebPublishPath(GetAccountDatabase(databaseId));

        /// <summary>
        /// Формирование адреса к опубликованной базе без проверок,
        /// на данный момент используется для метода АПИ
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Адрес к опубликованной базе</returns>
        public string GetPublishPathWithoutChecks(Domain.DataModels.AccountDatabase accountDatabase)
        {
            if (accountDatabase.IsDelimiter())
                return GetDatabaseOnDelimitersPublicationAddress(accountDatabase);

            var accountConfigurationData =
                accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountDatabase.AccountId);

            var cloudServicesContent = ((CloudServicesSegment)accountConfigurationData.Segment).CloudServicesContentServer;
            var publishSiteName = cloudServicesContent.PublishSiteName;
            var path = cloudServicesContent.GroupByAccount
                ? $"{((Account)accountConfigurationData.Account).GetEncodeAccountId()}/{accountDatabase.V82Name}"
                : $"{accountDatabase.V82Name}";

            return WebPublishPath(publishSiteName, Uri.UriSchemeHttps, path);
        }

        /// <summary>
        /// Получить адрес публикации для базы на разделителях
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Адрес публикации</returns>
        private string GetDatabaseOnDelimitersPublicationAddress(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var accountDatabaseOnDelimiter = accountDatabase.AccountDatabaseOnDelimiter;

            switch (accountDatabaseOnDelimiter)
            {
                case null:
                    return null;
                case { IsDemo: true, Zone: null }:
                {
                    var dbTemplateDelimiter = GetDbTemplateDelimiters(accountDatabaseOnDelimiter.DbTemplateDelimiterCode);
                    return dbTemplateDelimiter?.DemoDatabaseOnDelimitersPublicationAddress;
                }
            }

            if (accountDatabaseOnDelimiter.SourceAccountDatabase == null)
                return null;

            var delimiterSourceAccountDatabase = GetDelimiterSourceAccountDatabase(
                accountDatabaseOnDelimiter.SourceAccountDatabase.Id,
                accountDatabaseOnDelimiter.DbTemplateDelimiterCode);

            if (delimiterSourceAccountDatabase == null)
                return null;

            var builder = new UriBuilder(delimiterSourceAccountDatabase.DatabaseOnDelimitersPublicationAddress);
            var path =
                $"{builder.Path.TrimEnd('/')}/{accountDatabaseOnDelimiter.SourceAccountDatabase.V82Name}/{accountDatabaseOnDelimiter.Zone}";

            return WebPublishPath(builder.Uri.Host, builder.Scheme, path);
        }

        /// <summary>
        /// Получить материнскую базу разделителей
        /// </summary>
        /// <param name="accountDatabaseId">Id базы источника на разделителях</param>
        /// <param name="dbTemplateDelimiterCode">Код шаблона на разделителях</param>
        /// <returns>Материнской базы разделителей</returns>
        private DelimiterSourceAccountDatabase? GetDelimiterSourceAccountDatabase(Guid accountDatabaseId, string dbTemplateDelimiterCode) =>
            dbLayer.DelimiterSourceAccountDatabaseRepository.AsQueryableNoTracking().FirstOrDefault(dsdb =>
                dsdb.AccountDatabaseId == accountDatabaseId &&
                dsdb.DbTemplateDelimiterCode == dbTemplateDelimiterCode);

        /// <summary>
        /// Получить шаблон базы на разделителях
        /// </summary>
        /// <param name="dbTemplateDelimiterCode">Код конфигурации</param>
        /// <returns>Шаблон</returns>
        public DbTemplateDelimiters GetDbTemplateDelimiters(string dbTemplateDelimiterCode) =>
            dbLayer.DbTemplateDelimitersReferencesRepository.AsQueryableNoTracking().FirstOrDefault(dbt =>
                dbt.ConfigurationId == dbTemplateDelimiterCode);

        /// <summary>
        /// Формирование адреса к обычной опубликованной базе
        /// </summary>
        /// <param name="accountDatabase">Информационная база</param>
        /// <returns>Адрес к обычной опубликованной базе</returns>
        private string GetWebPublishPathForRegular(Domain.DataModels.AccountDatabase accountDatabase)
        {
            var accountSegment = accountDatabase?.Account?.AccountConfiguration?.Segment ?? accountConfigurationDataProvider.GetAccountSegment(accountDatabase.AccountId);

            if (accountDatabase.PublishStateEnum != PublishState.Published || accountSegment == null)
                return null;

            var cloudServicesContent = accountSegment.CloudServicesContentServer;
            var publishSiteName = cloudServicesContent.PublishSiteName;
            return AccountDatabaseWebPublishPath.Create(publishSiteName, cloudServicesContent.GroupByAccount,
                accountDatabase.AccountId, accountDatabase.V82Name);
        }

        /// <summary>
        /// Формирование строки веб сайта
        /// </summary>
        /// <param name="publishSiteName">Имя сайта публикации</param>
        /// <param name="scheme">Схема</param>
        /// <param name="path">Путь к инф. базе</param>
        /// <returns>Строка веб сайта</returns>
        private static string WebPublishPath(string publishSiteName, string scheme, string path)
        {
            var builder = new UriBuilder
            {
                Host = publishSiteName,
                Path = path,
                Scheme = scheme
            };
            return builder.Uri.AbsoluteUri;
        }

        /// <summary>
        /// Получить информационную базы
        /// </summary>
        /// <param name="id">Id базы</param>
        /// <returns>Информационная база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid id) =>
            dbLayer.DatabasesRepository.GetAccountDatabase(id) ??
            throw new NotFoundException($"Не удалось получить базу по Id {id}");
    }
}
