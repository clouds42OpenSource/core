﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Publishes.Managers
{
    /// <summary>
    /// Менеджер пулов баз на нодах публикаций
    /// </summary>
    public class DatabaseIisApplicationPoolManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IRestartDatabaseIisApplicationPoolProvider restartDatabaseIisApplicationPoolProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Перезапустить пул базы аккаунта на нодах публикаций
        /// </summary>
        /// <param name="accountDatabaseId">Id инф. базы аккаунта</param>
        /// <returns>Ok - если пул инф. баз успешно перезапущен на всех нодах</returns>
        public ManagerResult RestartIisApplicationPool(Guid accountDatabaseId)
        {
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_RestartIisAppPool);

                restartDatabaseIisApplicationPoolProvider.RestartIisApplicationPool(accountDatabaseId);
                logger.Info($"Перезапуск пула базы {accountDatabaseId} аккаунта на нодах публикаций заершен успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.HandleWarning(ex.Message, $"Ошибка рестарта пула для базы {accountDatabaseId}");
                return PreconditionFailed(ex.Message);
            }
        }
    }
}
