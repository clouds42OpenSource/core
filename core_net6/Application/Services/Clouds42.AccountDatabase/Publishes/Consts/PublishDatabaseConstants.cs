﻿namespace Clouds42.AccountDatabase.Publishes.Consts
{
    /// <summary>
    /// Константы для публикации инф. базы
    /// </summary>
    public static class PublishDatabaseConstants
    {
        /// <summary>
        /// Название файла web.config
        /// </summary>
        public const string WebConfigFileName = "web.config";

        /// <summary>
        /// Название файла web.config
        /// </summary>
        public const string WebLockConfigFileName = "lock_web.config";

        /// <summary>
        /// Название веб расширения (модуля) 1С
        /// </summary>
        public const string WebExtension1CName = "1C Web-service Extension";

        /// <summary>
        /// Название атрибута веб обработчка в который записывается путь к расширению
        /// </summary>
        public const string WebExtension1CPathAttributeName = "scriptProcessor";

        /// <summary>
        /// Название тега веб обработчика
        /// </summary>
        public const string WebHandlersTagName = "add";

        /// <summary>
        /// Название атрибута веб обработчика в который записывается название расширения
        /// </summary>
        public const string WebHandlersNameAttributeValue = "name";
    }
}
