﻿using System.Xml;
using Clouds42.AccountDatabase.Contracts.IISApplication.Constants;
using Clouds42.AccountDatabase.Contracts.IISApplication.Models;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.AccountDatabase.Publishes.Consts;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Exceptions;
using Clouds42.Common.Extensions;
using Clouds42.Domain.DataModels;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    /// Класс для публикации информационных баз
    /// </summary>
    public class DatabaseWebPublisher(
        ISegmentHelper segmentHelper,
        IUnitOfWork unitOfWork,
        IisHttpClient iisProvider,
        ILogger42 logger,
        ICloudLocalizer cloudLocalizer,
        IisApplicationDataHelper iisApplicationDataHelper,
        IWebAccessHelper webAccessHelper,
        IHandlerException handlerException)
        : IDatabaseWebPublisher
    {
        private const string ConfigFileName = "web.config";
        private const string VrdFileName = "default.vrd";
        private readonly WebConfigHelper _webConfigHelper = new(cloudLocalizer);

        /// <summary>
        /// Публикация базы на IIS
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void PublishAccountDatabase(string ibName, string accountEncodeId, string ibPath, string module1CPath)
        {
            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);

            logger.Info($"Сервер публикаций аккаунта: {accountContentServer.Name}");
            var node = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site =
                iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);
            var baseApiAddress = node.PublishNodeReference.Address;
            logger.Info($"***CompanySite GetLocationPath: {site.PhysicalPath} хост {baseApiAddress}");
            
            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(site.PhysicalPath, accountEncodeId, ibName, account);
            var baseAddress = segmentHelper.GetIisWebAddress(accountEncodeId, ibName, account);

            if (!Directory.Exists(basePath))
            {
                logger.Info($"Создание виртуальной папки по пути {basePath}");
                Directory.CreateDirectory(basePath);
            }
            else
            {
                logger.Info($"Пытаемся удалить старое приложение по адресу {baseAddress}");
                var appRoot =
                    iisProvider.Get<Root>(baseApiAddress, site.Links.Webapps.Href);

                var oldApp = appRoot?.Webapps.FirstOrDefault(a => a.Path == baseAddress);

                if (oldApp != null)
                {
                    logger.Info($"По пути {baseAddress} нашли старое приложение. Начинаем удаление.");

                    var webApp = iisProvider.Get<SiteModel>(baseApiAddress, oldApp.Links.Self.Href);

                    iisProvider.Delete(baseApiAddress, oldApp.Links.Self.Href);

                    logger.Info($"{webApp?.Name} | {webApp?.PhysicalPath} старое приложение успешно удалено.");

                    var pool = iisProvider.Get<ApplicationPool>(baseApiAddress,
                            webApp!.ApplicationPool.Links.Self.Href);

                    logger.Info($"Пытаемся удалить пул {pool.Name} приложения по адресу {baseAddress}");

                    var poolHelper = new ApplicationPoolHelper(iisProvider, logger);

                    poolHelper.RemoveApplicationPool(pool, baseApiAddress, baseAddress);

                }

                else
                {
                    logger.Info($"По пути {baseAddress} не удалось получить старое приложение.");
                }
            }

            try
            {
               var applicationPool = new ApplicationPoolHelper(iisProvider, logger).CreateApplicationPool(accountDatabase,
                    node.PublishNodeReference.Address);

                logger.Info($"{ibName} :: Создан пул для приложения {applicationPool?.Name} ");

                if (applicationPool == null)
                    throw new InvalidDataException($"{ibName} :: Ошибка создания пула для приложения");

                iisProvider.Post<SiteApplicationCreateModel, SiteApplicationModel>(
                    node.PublishNodeReference.Address, 
                    new SiteApplicationCreateModel
                    {
                        Path = baseAddress, 
                        PhysicalPath = basePath, 
                        Website = new WebsiteModel
                        {
                            Id = site.Id
                        },
                        ApplicationPool = new ApplicationPool
                        {
                            Id = applicationPool.Id
                        }
                    },IISLinksConstants.WebApps);

                logger.Info($"Создано приложение по адресу {baseAddress}");


                CreateDefaultVrdFile(ibPath, basePath, baseAddress,
                    segmentHelper.GetWindowsThinkClientDownloadLink(account, accountDatabase));

                logger.Info($"{ibName} :: Создан vrd файл по пути {basePath} :: {ibPath} :: {baseAddress}");
                 
                var accessGroupName = DomainCoreGroups.GetDatabaseGroupName(account.IndexNumber, accountDatabase.DbNumber);

                CreateWebConfigFile(basePath, module1CPath, accessGroupName);

                logger.Info($"{ibName} :: Создан web config по пути {basePath}");
                
                var externalAndGrandAccesses = unitOfWork.AcDbAccessesRepository
                    .WhereLazy(x => x.AccountDatabaseID == accountDatabase.Id &&
                                    x.AccountUserID != Guid.Empty)
                    .Select(x => x.AccountUser.Login).ToList();

                webAccessHelper.ManageAccess([accountDatabase], externalAndGrandAccesses, null);

                logger.Info($"{ibName} :: Предоставлен доступ {applicationPool?.Name} внешним пользователям");
            }

            catch (Exception ex)
            {
                logger.Warn(ex, "[Ошибка публикации базы на IIS]");

                throw;
            }
        }

        /// <summary>
        /// Переопубликация базы на IIS
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="ibPath">
        /// Строка подключения к информационной базе 1С
        /// (необходима для формирования .vrd-файла)
        /// </param>
        public void RePublishAccountDatabase(string ibName, string accountEncodeId, string ibPath)
        {
            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);

            logger.Info($"Сервер публикаций аккаунта: {accountContentServer.Name}");

            var publishSiteName = segmentHelper.GetPublishSiteName(account);
            var node = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site =
                iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);

            logger.Info($"Хост {node.PublishNodeReference.Address}");

            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(site.PhysicalPath, accountEncodeId, ibName, account);
            var baseAddress = segmentHelper.GetIisWebAddress(accountEncodeId, ibName, account);
            
            try
            {
                logger.Info("Изменение VRD файла");
                EditOrCreateVrdFile(ibPath, basePath, baseAddress, segmentHelper.GetWindowsThinkClientDownloadLink(account, accountDatabase));
                logger.Info("Изменение VRD файла завершено. Все изменения на IIS закончены");
            }

            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Не удалось создать default.vrd файл при его отсутствии при редактировании]");
            }
        }

        /// <summary>
        ///     Переопубликация базы на IIS с изменением web.config файла
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="module1CPath">Путь к модулю публикации IIS</param>
        public void ModifyVersionPlatformInWebConfig(string ibName, string accountEncodeId, string module1CPath)
        {
            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);

            logger.Debug($"Сервер публикации аккаунта: {accountContentServer.Name}");
            var node = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site =
                iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);

            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(site.PhysicalPath, accountEncodeId, ibName, account);
            
            try
            {
                if (!Directory.Exists(basePath))
                {
                    Directory.CreateDirectory(basePath);
                    logger.Info($"Папка {basePath} отсутствует, создана новая");
                }
                if (!File.Exists(Path.Combine(basePath, ConfigFileName)))
                {
                    var accessGroupName = DomainCoreGroups.GetDatabaseGroupName(account.IndexNumber, accountDatabase.DbNumber);
                    logger.Info("Файл web.config не был обнаружен при редактировании. Создание файла");

                    CreateWebConfigFile(basePath, module1CPath, accessGroupName);
                }
                else
                {
                    logger.Info("Изменение web.config файла");
                    EditWebConfig(basePath, module1CPath);

                    logger.Info("Изменение web.config файла завершено");
                }
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, "[Не удалось создать web.config файл при его отсутствии при редактировании]");
            }
        }

        /// <summary>
        /// Отмена доменной аутентификации
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название папки для компании-владельца данной базы
        /// (в формате зашифрованного ID компании)
        /// </param>
        /// <param name="accountGroupName">Название группы компании</param>
        /// <param name="module1CPath">
        /// Путь к модулю публикации IIS
        /// </param>
        public void DisableDomainAuthentication(string ibName, string accountEncodeId, string accountGroupName, string module1CPath)
        {
            logger.Warn("DisableDomainAuthentication start");

            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);
            var node = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site = iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);
            var folder = site.PhysicalPath;

            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(folder, accountEncodeId, ibName, account);

            logger.Info($"basePath = {basePath}");

            var baseAddress = segmentHelper.GetIisWebAddress(accountEncodeId, ibName, account);
            logger.Info($"baseAddress = {baseAddress}");

            var authentication = iisProvider.Get<Authentication>(node.PublishNodeReference.Address,
                site.Links.Authentication.Href);
            
            var basicAuth = iisProvider
                .Get<Authentication>(node.PublishNodeReference.Address, authentication.Links.BasicAuth.Href);

            var anonAuth = iisProvider
                .Get<Authentication>(node.PublishNodeReference.Address, authentication.Links.AnonymousAuth.Href);

            if (anonAuth != null)
            {
                anonAuth.Enabled = "true";
                iisProvider.Patch<Authentication, Authentication>(node.PublishNodeReference.Address,
                    anonAuth, IISLinksConstants.AnonAuth);
            }
            if (basicAuth != null)
            {
                basicAuth.Enabled = "false";
                iisProvider.Patch<Authentication, Authentication>(node.PublishNodeReference.Address,
                    basicAuth, IISLinksConstants.BasicAuth);
            }
            logger.Info("CreateWebConfigFileWithoutAuthorization start");
            CreateWebConfigFileWithoutAuthorization(accountGroupName, basePath, module1CPath);
            logger.Info("CreateWebConfigFileWithoutAuthorization finish");
        }

        /// <summary>
        /// Создание .vrd-файла базы с необходимыми параметрами
        /// </summary>
        /// <param name="ibName">
        /// Название папки, в которой будет размещена база
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="ibPath">Физический путь информационной базы 1С</param>
        /// <param name="expansionName">Название расширения</param>
        /// <param name="thinkClientLink">Ссылка на скачивание тонкого клиента</param>
        public string PublishExpansion(string ibName, string accountEncodeId, string ibPath, string expansionName, string? thinkClientLink)
        {
            logger.Info("PublishExpansion start");

            var accountDatabase = GetAccountDatabase(ibName);
            var account = GetAccount(accountDatabase.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);
            var node = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site =
                iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);

            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(site.PhysicalPath, accountEncodeId, ibName, account);

            var vrd = Path.Combine(basePath, VrdFileName);
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <point xmlns=""http://v8.1c.ru/8.2/virtual-resource-system""
		                            xmlns:xs=""http://www.w3.org/2001/XMLSchema""
		                            xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
		                            base=""{basePath}""
		                            ib=""{ibPath}""
                                    pubdst=""{thinkClientLink}"">
	                            <ws enable=""false""/>
                                <httpServices publishByDefault=""true"" publishExtensionsByDefault=""true"" >
                                    <service name=""{expansionName}"" rootUrl=""{expansionName}"" enable=""true"" />
                                </httpServices>
                                </point>";
            File.WriteAllText(vrd, text);

            return Path.Combine(publishSiteName, accountEncodeId, ibName);
        }

        /// <summary>
        /// Удаление опубликованной базы с IIS
        /// </summary>
        /// <param name="baseName">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        public void UnpublishDatabase(string baseName, string accountEncodeId)
        {
            var database = GetAccountDatabase(baseName);
            var account = GetAccount(database.AccountId);

            var accountContentServer = segmentHelper.GetContentServer(account);
            var publishSiteName = segmentHelper.GetPublishSiteName(account);
            var contentServerNode = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var baseAddress = contentServerNode.PublishNodeReference.Address;
            var site = iisApplicationDataHelper.GetPublishSite(baseAddress,
                publishSiteName);
            var appPath = segmentHelper.GetIisWebAddress(accountEncodeId, baseName, account);
            var folder = site?.PhysicalPath;
            var webAppRoot = iisProvider.Get<Root>(baseAddress,
                site?.Links.Webapps.Href!);

            var application = webAppRoot?.Webapps.FirstOrDefault(x => x.Path == appPath);

            if (application == null)
            {
                var errorMessage = $"Приложение не найденно : {appPath}";
                logger.Warn(errorMessage);
            }
            else
            {
                var poolLink = iisProvider.Get<SiteApplicationModel>(baseAddress, application.Links.Self.Href)?.ApplicationPool.Links.Self.Href;

                iisProvider.Delete(baseAddress,
                    $"{IISLinksConstants.WebApps}?id={application.Id}");
                logger.Info($"Приложение {appPath} удалено");

                var applicationPoolHelper = new ApplicationPoolHelper(iisProvider, logger);

                applicationPoolHelper.RemoveApplicationPool(iisProvider.Get<ApplicationPool>(baseAddress, poolLink), baseAddress, appPath);

                var accountDirectoryPath = segmentHelper.CreateAccountDirectoryPath(folder, accountEncodeId, account);

                CleanAndDelete(accountDirectoryPath, baseName);
            }
        }

        /// <summary>
        /// Удалить старую публикацию инф. базы
        /// </summary>
        /// <param name="acDbV82Name">
        /// Название базы, публикацию которой нужно удалить
        /// (в формате V82Name)
        /// </param>
        /// <param name="accountEncodeId">
        /// Название аккаунта-владельца данной базы
        /// (в формате зашифрованного ID аккаунта)
        /// </param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        public void RemoveOldDatabasePublication(string acDbV82Name, string accountEncodeId, Guid oldSegmentId)
        {
            try
            {
                var contentServer = GetContentServerBySegmentId(oldSegmentId);
                RemoveApplicationOnIis(contentServer, acDbV82Name, accountEncodeId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка удаления старой публикации инф. базы {acDbV82Name}.]");
            }
        }

        /// <summary>
        /// Создание web-config-а базы с необходимыми параметрами
        /// </summary>
        /// <param name="versionTitle">Необходимая версия 1С</param>
        /// <param name="companyGroupName">Название группы компании</param>
        /// <param name="basePath">Физический путь опубликованного приложения (куда писать файл)</param>
        public void CreateWebConfigFile__(string versionTitle, string companyGroupName, string basePath)
        {
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                        <add name=""1C Web-service Extension"" path=""*"" verb=""*""
                                                            modules=""IsapiModule""
                                                            scriptProcessor=""C:\Program Files (x86)\1cv82\{versionTitle}\bin\wsisapi.dll""
                                                            resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\{companyGroupName}"" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";

            File.WriteAllText(Path.Combine(basePath, ConfigFileName), text);
        }

        /// <summary>
        /// Удалить приложение на IIS
        /// </summary>
        /// <param name="contentServer">Сервер публикаций</param>
        /// <param name="acDbV82Name">Номер базы</param>
        /// <param name="accountEncodeId">Уникальный код аккаунта</param>
        private void RemoveApplicationOnIis(CloudServicesContentServer contentServer, string acDbV82Name, string accountEncodeId)
        {
            var publishSiteName = contentServer.PublishSiteName;
            var contentServerNode = unitOfWork.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == contentServer.ID);
            var site = iisApplicationDataHelper.GetPublishSite(contentServerNode.PublishNodeReference.Address,
                publishSiteName);
            var appPath = segmentHelper.GetIisWebAddress(contentServer, accountEncodeId, acDbV82Name);

            var folder = site!.PhysicalPath;

            var webAppRoot = iisProvider.Get<Root>(contentServerNode.PublishNodeReference.Address,
                site?.Links.Webapps.Href!);

            var application = webAppRoot?.Webapps.FirstOrDefault(x => x.Path == appPath);

            if (application == null)
            {
                var errorMessage = $"Приложение не найденно : {appPath}";
                logger.Warn(errorMessage);

                throw new NotFoundException(errorMessage);
            }

            iisProvider.Delete(contentServerNode.PublishNodeReference.Address,
                $"{IISLinksConstants.WebApps}?id={application.Id}");

            logger.Info($"Приложение {appPath} удалено");

            var applicationPoolHelper = new ApplicationPoolHelper(iisProvider, logger);

            var appPool = iisProvider.Get<ApplicationPool>(contentServerNode.PublishNodeReference.Address,
                    site.ApplicationPool.Links.Self.Href);

            applicationPoolHelper.RemoveApplicationPool(appPool, contentServerNode.PublishNodeReference.Address, appPath);

            var accountDirectoryPath = segmentHelper.CreateAccountDirectoryPath(contentServer, folder, accountEncodeId);

            CleanAndDelete(accountDirectoryPath, acDbV82Name);
            
        }

        /// <summary>
        /// Создать web.config без авторизации
        /// </summary>
        /// <param name="companyGroupName">Название группы компании</param>
        /// <param name="basePath">Путь к приложению</param>
        /// <param name="pathTo1CModule">Путь к модулю 1С</param>
        private static void CreateWebConfigFileWithoutAuthorization(string companyGroupName, string basePath, string pathTo1CModule)
        {
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                          <add name=""1C Web-service Extension"" path=""*"" verb=""*"" modules=""IsapiModule"" scriptProcessor=""{pathTo1CModule}"" resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\{companyGroupName}"" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                            <add accessType=""Allow"" roles=""*"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";

            File.WriteAllText(Path.Combine(basePath, ConfigFileName), text);
        }

        /// <summary>
        /// Создание .vrd-файла базы с необходимыми параметрами
        /// </summary>
        /// <param name="ibPath">Физический путь информационной базы 1С</param>
        /// <param name="basePath">Физический путь опубликованного приложения (базы)</param>
        /// <param name="publishUrl">Web-адрес приложения (базы)</param>
        /// <param name="thinkClientLink">Ссылка на скачивание тонкого клиента</param>
        private static void CreateDefaultVrdFile(string ibPath, string basePath, string publishUrl,string? thinkClientLink)
        {
            var filePath = Path.Combine(basePath, VrdFileName);
            if (File.Exists(filePath))
                File.Delete(filePath);

            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                <point xmlns=""http://v8.1c.ru/8.2/virtual-resource-system""
		                                xmlns:xs=""http://www.w3.org/2001/XMLSchema""
		                                xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
		                                base=""{publishUrl}""
		                                ib=""{ibPath}""
                                        pubdst=""{thinkClientLink}"">
	                                <ws enable=""false""/>
                                </point>";
            File.WriteAllText(filePath, text);
        }

        /// <summary>
        ///     Метод редактирования web.config файла опубликованной базы
        /// </summary>
        /// <param name="basePath">Путь к базе</param>
        /// <param name="module1CPath">Путь к модулю публикации</param>
        private bool EditWebConfig(string basePath, string module1CPath)
        {
            try
            {
                var webConfigFilePath = _webConfigHelper.GetFilePath(basePath);
                var webXmlDoc = _webConfigHelper.GetFileContent(basePath);
                var xmlRoot = _webConfigHelper.GetFileRootElementOrThrowException(webXmlDoc);

                var elements = xmlRoot.GetElementsByTagName(PublishDatabaseConstants.WebHandlersTagName);

                foreach (var element in elements)
                {
                    var node = (XmlNode)element;
                    if (node.Attributes?[PublishDatabaseConstants.WebHandlersNameAttributeValue]?.Value !=
                        PublishDatabaseConstants.WebExtension1CName ||
                        node.Attributes?[PublishDatabaseConstants.WebExtension1CPathAttributeName] == null)
                    {
                        continue;
                    }

                    logger.Info($"Атрибуты найдены, меняем путь до модуля с {node.Attributes[PublishDatabaseConstants.WebExtension1CPathAttributeName]!.Value} на {module1CPath}");
                    node.Attributes[PublishDatabaseConstants.WebExtension1CPathAttributeName]!.Value = module1CPath;

                }

                webXmlDoc.Save(webConfigFilePath);

                logger.Info($"Содержание измененного веб конфига {File.ReadAllText(webConfigFilePath)} ");

                File.WriteAllText(webConfigFilePath, File.ReadAllText(webConfigFilePath));
                return true;
            }
            catch (Exception e)
            {
                handlerException.Handle(e, "[Ошибка при изменении web.config файла.]");
                return false;
            }
        }

        /// <summary>
        /// Изменение .vrd-файла базы при переопубликации
        /// </summary>
        public bool EditOrCreateVrdFile(string ibPath, string basePath, string publishUrl, string? thinkClientPath)
        {
            logger.Info("Начинаю смену редактирование файла");
            try
            {
                if (!Directory.Exists(basePath))
                {
                    Directory.CreateDirectory(basePath);
                    logger.Info($"Папка {basePath} отсутствует, создана новая");
                }

                if (!File.Exists(Path.Combine(basePath, VrdFileName)))
                {
                    logger.Info("Файл default.vrd не был обнаружен при редактировании. Создание файла");
                     CreateDefaultVrdFile(ibPath, basePath, publishUrl, thinkClientPath);

                     return true;
                }

                var vrdFilePath = Path.Combine(basePath, VrdFileName);
                var vrdXml = vrdFilePath.GetXmlDocumentFromPath();

                var point = vrdXml["point"];
                if (point == null)
                {
                    logger.Debug($"Не валидный файл {vrdFilePath}");
                    return false;
                }

                var xmlbase = point.Attributes["base"];
                if (xmlbase != null)
                {
                    xmlbase.Value = publishUrl;
                }

                var xmlib = point.Attributes["ib"];
                if (xmlib != null)
                {
                    xmlib.Value = ibPath.Replace("amp;", "");
                }

                var xmlpubdst = point.Attributes["pubdst"];
                
                if (xmlpubdst != null)
                {
                     xmlpubdst.Value = thinkClientPath;
                }
                else
                {
                     var attribute = vrdXml.CreateAttribute("pubdst");
                     attribute.Value = thinkClientPath;
                     point.Attributes.Append(attribute);
                }

                vrdXml.Save(vrdFilePath);


                File.WriteAllText(vrdFilePath, File.ReadAllText(vrdFilePath).Replace("amp;", ""));

                using StringWriter stringWriter = new StringWriter();
                using XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
                vrdXml.WriteTo(xmlTextWriter);
                string xmlContent = stringWriter.ToString();
                logger.Info($"Содержимое vrd файла изменено на:\n{xmlContent}");

                return true;
            }
            catch (Exception e)
            {
                handlerException.Handle(e, "[Ошибка при изменение vrd файла.]");
                return false;
            }
        }

        /// <summary>
        /// Создание web-config-а базы с необходимыми параметрами
        /// </summary>
        /// <param name="basePath">Физический путь опубликованного приложения (куда писать файл)</param>
        /// <param name="pathTo1CModule">Путь к модулю публикации</param>
        private static void CreateWebConfigFile(string basePath, string pathTo1CModule, string accessGroupName)
        {
            var text = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                            <configuration>
                                                <system.webServer>
                                                    <handlers>
                                                          <add name=""1C Web-service Extension"" path=""*"" verb=""*"" modules=""IsapiModule"" scriptProcessor=""{pathTo1CModule}"" resourceType=""Unspecified"" requireAccess=""None"" />
                                                    </handlers>
                                                    <security>
                                                        <authorization>
                                                            <remove users=""*"" roles="""" verbs="""" />
                                                            <add accessType=""Allow"" roles=""ad\Администраторы домена"" />
                                                            <add accessType=""Allow"" roles=""ad\{accessGroupName}"" />
                                                        </authorization>
                                                    </security>
                                                </system.webServer>
                                            </configuration>";

            var filePath = Path.Combine(basePath, ConfigFileName);
            if (File.Exists(filePath))
                File.Delete(filePath);

            File.WriteAllText(filePath, text);
        }

        /// <summary>
        /// Удаление каталога базы и удаление каталога аккаунта если баз больше нет, включая вложенные файлы.
        /// </summary>
        private void CleanAndDelete(string accountDirectoryPath, string baseName)
        {
            // физический путь, где размещена база
            var dbDirectoryPath = Path.Combine(accountDirectoryPath, baseName);

            var dbDirectory = new DirectoryInfo(dbDirectoryPath);

            if (!dbDirectory.Exists)
                throw new NotFoundException($"Не обнаружен каталог по пути {dbDirectoryPath}");

            Directory.Delete(dbDirectory.FullName, true);

            logger.Info($"Каталог {dbDirectoryPath} удалён");

            var accountDirectory = new DirectoryInfo(accountDirectoryPath);

            if (accountDirectory.GetDirectories().Any() || accountDirectory.GetFiles().Any())
            {
                return;
            }

            Directory.Delete(accountDirectory.FullName);

            logger.Info($"Полностью удален каталог {accountDirectory.FullName}");
        }

        /// <summary>
        /// Получить инф. базу по номеру
        /// </summary>
        /// <param name="v82Name">Номер инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(string v82Name)
            => unitOfWork.DatabasesRepository.FirstOrDefault(acDb => acDb.V82Name == v82Name)
               ?? throw new NotFoundException($"Инф. база по номеру {v82Name} не найдена");

        /// <summary>
        /// Получить аккаунт по ID
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <returns>Аккаунт</returns>
        private Account GetAccount(Guid accountId)
            => unitOfWork.AccountsRepository.FirstOrDefault(ac => ac.Id == accountId)
               ?? throw new NotFoundException($"Аккаунт по ID {accountId} не найден");

        /// <summary>
        /// Получить сервер публикаций по ID сегмента или выкинуть исключение
        /// </summary>
        /// <param name="segmentId">ID сегмента</param>
        /// <returns>Сервер публикаций по ID сегмента</returns>
        private CloudServicesContentServer GetContentServerBySegmentId(Guid segmentId)
        {
            var segment = unitOfWork.CloudServicesSegmentRepository.FirstOrThrowException(segm => segm.ID == segmentId,
                $"Сегмент по ID {segmentId} не найден");
            var contentServer =
                unitOfWork.CloudServicesContentServerRepository.FirstOrThrowException(serv =>
                        serv.ID == segment.ContentServerID,
                    $"Сервер публикации по ID {segment.ContentServerID} не найден.");
            return contentServer;
        }
        
    }
}
