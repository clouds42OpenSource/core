﻿using System.Text;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.Common.Exceptions;
using Clouds42.CoreWorkerTask;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums._1C;
using Clouds42.Logger;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    /// Провайдер перепубликации баз
    /// </summary>
    public class RepublishSegmentDatabasesProvider(
        IUnitOfWork dbLayer,
        PublishHelper publishHelper,
        IServiceProvider serviceProvider,
        ILogger42 logger,
        IDbObjectLockProvider dbObjectLockProvider)
        : IRepublishSegmentDatabasesProvider
    {
        /// <summary>
        /// Переопубликовать базы сегмента
        /// </summary>
        /// <param name="segmentId">Id сегмента</param>
        /// <param name="platformType">Версия платформы 1С</param>
        /// <param name="distributionType">Версия дистрибутива</param>
        /// <returns>Сообщение о результате рестарта пулов</returns>
        public void RepublishSegmentDatabases(Guid segmentId, PlatformType platformType, DistributionType distributionType)
        {
            var databases = new List<Domain.DataModels.AccountDatabase>();

            using (dbObjectLockProvider.AcquireLock(nameof(RepublishSegmentDatabases), TimeSpan.FromSeconds(20)))
            {
                databases = dbLayer.DatabasesRepository.GetPublishedDbs(segmentId, platformType, distributionType);

                databases.ForEach(database =>
                {
                    database.PublishStateEnum = PublishState.PendingPublication;
                });

                dbLayer.BulkUpdate(databases);
                dbLayer.Save();
            }

            RepublishDatabases(databases);

            var poolsRestartingMessage = RestartApplicationsPools(segmentId, databases);
            IisApplicationDataHelper.NotifyHotlineAboutAppPoolRestartError(poolsRestartingMessage);
        }

        /// <summary>
        /// Переопубликовать базы с изменением web.config
        /// </summary>
        /// <param name="databases">Список баз сегмента</param>
        private void RepublishDatabases(List<Domain.DataModels.AccountDatabase> databases)
        {
            databases.ForEach(database =>
            {
                logger.Info($"Начинаю переопубликацию базы {database.Id}");

                publishHelper.ModifyVersionPlatformInVrdFile(database.Id);
                publishHelper.ModifyVersionPlatformInWebConfig(database.Id);
            });
        }

        /// <summary>
        /// Перезапустить пулы приложений на нодах публикаций
        /// </summary>
        /// <returns>Сообщение о результате рестарта пулов</returns>
        private string RestartApplicationsPools(Guid segmentId, List<Domain.DataModels.AccountDatabase> databases)
        {
            var segment = dbLayer.CloudServicesSegmentRepository.FirstOrDefault(segm => segm.ID == segmentId)
                         ?? throw new NotFoundException($"Сегмент с id '{segmentId}' не найден");

            var poolsRestartingMessageBuilder = new StringBuilder();

            databases.ForEach(database =>
            {
                var result = serviceProvider.GetRequiredService<IRestartIisApplicationPoolFlow>().Run(new RestartIisApplicationPoolParamsDto
                {
                    AccountDatabaseId = database.Id,
                    ContentServerId = segment.ContentServerID
                });

                if (result.Finish)
                    return;

                poolsRestartingMessageBuilder.AppendLine(result.Message);
            });

            return poolsRestartingMessageBuilder.ToString();
        }
    }
}
