﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Publishes.Helpers;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    /// Провайдер для перепубликации инф. баз
    /// </summary>
    internal class RepublishAccountDatabaseProvider(PublishHelper publishHelper) : IRepublishAccountDatabaseProvider
    {
        /// <summary>
        /// Переопубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void Republish(Guid accountDatabaseId)
        {
            publishHelper.RePublish(accountDatabaseId);
        }

        /// <summary>
        ///     Переопубликовать базу с изменением файла web.config
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        public void RepublishWithChangingWebConfig(Guid accountDatabaseId)
        {
            publishHelper.ModifyVersionPlatformInVrdFile(accountDatabaseId);
            publishHelper.ModifyVersionPlatformInWebConfig(accountDatabaseId);
        }
    }
}
