﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Publishes.Helpers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    ///     Основная реализация публикаций баз
    /// </summary>
    public sealed class PublishDbProvider : IPublishDbProvider
    {
        /// <summary>
        ///     Опубликовать
        /// </summary>
        public void Publish(IServiceProvider serviceProvider, Guid databaseId)
        {
            serviceProvider.GetRequiredService<PublishHelper>().Publish(databaseId);
            serviceProvider.GetRequiredService<SuccessPublishAccountDatabaseMailNotifyHelper>().SendMail(databaseId);
        }

        /// <summary>
        ///     Отмена публикации
        /// </summary>
        public void CancelPublish(IServiceProvider serviceProvider, Guid databaseId)
        {
            serviceProvider.GetRequiredService<PublishHelper>().CancelPublish(databaseId);
        }

        /// <summary>
        /// Удалить старую публикацию инф. базы
        /// </summary>
        /// <param name="serviceProvider">Service Provider</param>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        public void RemoveOldDatabasePublication(IServiceProvider serviceProvider, Guid accountDatabaseId, Guid oldSegmentId)
        {
            serviceProvider.GetRequiredService<PublishHelper>().RemoveOldDatabasePublication(accountDatabaseId, oldSegmentId);
        }

        /// <summary>
        /// Обновить логин
        /// для веб доступа информационной базы
        /// </summary>
        /// <param name="serviceProvider">Service Provider</param>
        /// <param name="updateLoginForWebAccessDatabase">Модель параметров обновления логина
        /// для веб доступа базы</param>
        public void UpdateLoginForWebAccessDatabase(IServiceProvider serviceProvider,
            UpdateLoginForWebAccessDatabaseDto updateLoginForWebAccessDatabase)
        {
            var webAccessHelper = serviceProvider.GetRequiredService<IWebAccessHelper>();
            webAccessHelper.UpdateLoginForWebAccessDatabase(updateLoginForWebAccessDatabase);
        }

        /// <summary>
        /// Предоставления веб прав к базе
        /// </summary>
        public void AddWebAccessToDatabase(IServiceProvider serviceProvider, Guid databaseId, Guid accountUserId)
        {
            var publishHelper = serviceProvider.GetRequiredService<PublishHelper>();
            publishHelper.AddWebAccess(databaseId, accountUserId);
        }

        /// <summary>
        /// Удаление веб прав к базе
        /// </summary>
        public void RemoveWebAccessToDatabase(IServiceProvider serviceProvider, Guid databaseId, Guid accountUserId)
        {
            var publishHelper = serviceProvider.GetRequiredService<PublishHelper>();
            publishHelper.RemoveWebAccess(databaseId, accountUserId);
        }
    }
}
