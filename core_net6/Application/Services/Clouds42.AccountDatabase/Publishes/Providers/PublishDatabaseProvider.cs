﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.Providers;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    /// Класс для публикации информационных баз
    /// </summary>
    public class PublishDatabaseProvider(
        IServiceProvider serviceProvider,
        IHandlerException handlerException,
        FetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        IPublishDbProvider publishDbProvider,
        IRepublishAccountDatabaseProvider republishAccountDatabaseProvider,
        ILogger42 logger)
        : IPublishDatabaseProvider
    {
        /// <summary>
        /// Отменить публикацию базы
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если публикация базы отменена</returns>
        public void CancelPublishDatabase(Guid accountDatabaseId)
        {
            try
            {
                publishDbProvider.CancelPublish(serviceProvider, accountDatabaseId);
                fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabaseId);
                logger.Info($"Отмена публикации базы {accountDatabaseId} завершена успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка отмены публикации базы] {accountDatabaseId}.");
            }
        }

        /// <summary>
        /// Опубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns> 
        public void PublishDatabase(Guid accountDatabaseId)
        {
            try
            {
                publishDbProvider.Publish(serviceProvider, accountDatabaseId);
                fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabaseId);
                logger.Info($"Публикация базы {accountDatabaseId} завершена успешно");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {accountDatabaseId}.");
            }
        }

        /// <summary>
        /// Переопубликовать инф. базу
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns>
        public void RepublishDatabase(Guid accountDatabaseId)
        {
            try
            {
                republishAccountDatabaseProvider.Republish(accountDatabaseId);
                fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabaseId);
                logger.Info($"Перепубликация базы {accountDatabaseId} завершена успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка перепубликации базы] {accountDatabaseId}.");
            }
        }

        /// <summary>
        /// Переопубликовать базу с изменением файла web.config
        /// </summary>
        /// <param name="accountDatabaseId">ID базы аккаунта</param>
        /// <returns>Ok - если база переопубликована</returns>
        public void RepublishWithChangingWebConfig(Guid accountDatabaseId)
        {
            try
            {
                republishAccountDatabaseProvider.RepublishWithChangingWebConfig(accountDatabaseId);
                fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().UpdateDatabaseHandle(accountDatabaseId);
                logger.Info($"Перепубликация базы {accountDatabaseId} с изменением конфиг файла завершена успешно");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex, $"[Ошибка перепубликации базы с изменением конфиг файла] база : {accountDatabaseId}");
            }
        }

    }
}
