﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Clouds42.StateMachine.Contracts.IisApplicationPoolProcessFlow;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Publishes.Providers
{
    /// <summary>
    /// Провайдер рестарта пулов баз на нодах публикаций
    /// </summary>
    internal class RestartDatabaseIisApplicationPoolProvider(
        IUnitOfWork dbLayer,
        IServiceProvider serviceProvider,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        ILogger42 logger)
        : IRestartDatabaseIisApplicationPoolProvider
    {
        /// <summary>
        /// Перезапустить пул базы аккаунта на нодах публикаций
        /// </summary>
        /// <param name="accountDatabaseId">Id инф. базы аккаунта</param>
        public void RestartIisApplicationPool(Guid accountDatabaseId)
        {
            var accountDatabase = GetAccountDatabase(accountDatabaseId);
            var accountConfigurationData =
                accountConfigurationDataProvider.GetAccountConfigurationBySegmentData(accountDatabase.AccountId);

            logger.Trace($"Начинаем перезапуск пула для аккаунта: {accountConfigurationData.Account.IndexNumber}");

            serviceProvider.GetRequiredService<IRestartIisApplicationPoolFlow>().Run(new RestartIisApplicationPoolParamsDto
            {
                AccountDatabaseId = accountDatabaseId,
                ContentServerId = accountConfigurationData.Segment.ContentServerID,
                V82Name = accountDatabase.V82Name
            });
        }

        /// <summary>
        /// Получить инф. базу по ID
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <returns>Инф. база</returns>
        private Domain.DataModels.AccountDatabase GetAccountDatabase(Guid accountDatabaseId)
            => dbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.Id == accountDatabaseId)
               ?? throw new NotFoundException($"Инф. база по ID {accountDatabaseId} не найдена");
    }
}