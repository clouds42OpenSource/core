﻿using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.Extensions.DependencyInjection;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    ///     Хэлпер для отправки письма в случае успешной публикации базы
    /// </summary>
    public class SuccessPublishAccountDatabaseMailNotifyHelper(IServiceProvider serviceProvider)
    {
        private readonly IUnitOfWork _dbLayer = serviceProvider.GetRequiredService<IUnitOfWork>();
        private readonly AccountDatabaseWebPublishPathHelper _accountDatabaseWebPublishPathHelper = serviceProvider.GetRequiredService<AccountDatabaseWebPublishPathHelper>();
        private readonly ILetterNotificationProcessor _letterNotificationProcessor = serviceProvider.GetRequiredService<ILetterNotificationProcessor>();

        /// <summary>
        /// Отправить уведомление
        /// </summary>
        /// <param name="accDatabaseId"></param>
        public void SendMail(Guid accDatabaseId)
        {
            var accountDatabase = _dbLayer.DatabasesRepository.GetAccountDatabase(accDatabaseId);

            if (accountDatabase == null)
                return;

            _letterNotificationProcessor.TryNotify<PublishDatabaseLetterNotification, PublishDatabaseLetterModelDto>(
                new PublishDatabaseLetterModelDto
                {
                    AccountId = accountDatabase.AccountId,
                    AccountDatabaseCaption = accountDatabase.Caption,
                    AccountDatabasePublishPath = _accountDatabaseWebPublishPathHelper.GetWebPublishPath(accountDatabase)
                });
        }
    }
}