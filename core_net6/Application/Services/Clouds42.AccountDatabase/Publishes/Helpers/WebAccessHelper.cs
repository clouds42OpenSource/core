﻿using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.DataContracts.AccountDatabase.PublishAccountDatabases;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    /// Хэлпер для управления web доступом к инф. базе
    /// </summary>
    public class WebAccessHelper(IUnitOfWork dbLayer, ILogger42 logger) : IWebAccessHelper
    {
        /// <summary>
        /// Обновить логин
        /// для веб доступа информационной базы
        /// </summary>
        /// <param name="updateLoginForWebAccessDatabase">Модель параметров обновления логина
        /// для веб доступа базы</param>
        public void UpdateLoginForWebAccessDatabase(UpdateLoginForWebAccessDatabaseDto updateLoginForWebAccessDatabase)
        {
            var accountDatabases = GetAccountDatabases(updateLoginForWebAccessDatabase.AccountDatabasesId);

            if (!accountDatabases.Any())
                return;

            ManageAccess(accountDatabases,
                new List<string> { updateLoginForWebAccessDatabase.NewLogin },
                new List<string> { updateLoginForWebAccessDatabase.OldLogin });
        }

        /// <summary>
        /// Настроить доступы
        /// </summary>
        /// <param name="databases">Инф. базы</param>
        /// <param name="usersForGrandAccess">Список пользователей для предоставления доступа</param>
        /// <param name="usersForDeleteAccess">Список пользователей для удаления доступа</param>
        public void ManageAccess(List<Domain.DataModels.AccountDatabase> databases,
            List<Guid> usersForGrandAccess, List<Guid> usersForDeleteAccess)
        {
            var usersIds = usersForGrandAccess.Concat(usersForDeleteAccess);
            var accountUsers = dbLayer.AccountUsersRepository.Where(a => usersIds.Contains(a.Id)).ToList();

            ManageAccess(databases,
                    accountUsers.Where(a => usersForGrandAccess.Contains(a.Id)).Select(s => s.Login).ToList(),
                    accountUsers.Where(a => usersForDeleteAccess.Contains(a.Id)).Select(s => s.Login).ToList()
                    );

        }

        /// <summary>
        /// Настроить доступы
        /// </summary>
        /// <param name="databases">Инф. база</param>
        /// <param name="usersForGrandAccess">Список пользователей для предоставления доступа</param>
        /// <param name="usersForDeleteAccess">Список пользователей для удаления доступа</param>
        public void ManageAccess(List<Domain.DataModels.AccountDatabase> databases,
            List<string>? usersForGrandAccess, List<string>? usersForDeleteAccess)
        {
            databases.ForEach(database =>
            {
                var account = database.Account;
                var accessGroupName = DomainCoreGroups.GetDatabaseGroupName(account.IndexNumber, database.DbNumber);

                logger.Info($"" +
                             $"Выполняем настройку доступов пользователей к информационной базе в IIS. " +
                $"Информационная база: {database.Id} | {database.V82Name}" +
                $"Группа ad: {accessGroupName}" +
                             $"Пользователи с разрешенным доступом: {(usersForGrandAccess != null && usersForGrandAccess.Any() ? string.Join(",", usersForGrandAccess) : "отсутствуют")}. " +
                             $"Пользователи с запрещенным доступом: {(usersForDeleteAccess != null && usersForDeleteAccess.Any() ? string.Join(",", usersForDeleteAccess) : "отсутствуют")}");

                if (!ActiveDirectoryCompanyFunctions.GroupExists(accessGroupName))
                {
                    ActiveDirectoryCompanyFunctions.CreateGroups(accessGroupName);
                    logger.Info($"Создана группа в ад {accessGroupName}");
                }

                if (usersForGrandAccess != null)
                    AddAccessToAdGroup(accessGroupName, usersForGrandAccess);

                if (usersForDeleteAccess != null)
                    DeleteAccessToAdGroup(accessGroupName, usersForDeleteAccess);

                logger.Info($"Махинации с доступами для группы {accessGroupName} окончены");
            });
           
        }

        /// <summary>
        /// Получить информационную базу
        /// </summary>
        /// <param name="databasesId">Id баз</param>
        /// <returns>Информационная база</returns>
        private List<Domain.DataModels.AccountDatabase> GetAccountDatabases(List<Guid> databasesId) =>
            dbLayer.DatabasesRepository.Where(d => databasesId.Contains(d.Id)).ToList();

        public void AddAccessToAdGroup(string accessGroupName, List<string>? usersForGrandAccess)
        {
            if (usersForGrandAccess == null)
            {
                return;
            }

            foreach (var userForGrandAccess in usersForGrandAccess)
            {
                try
                {
                    ActiveDirectoryCompanyFunctions.AddToGroups(userForGrandAccess, accessGroupName);
                    logger.Info($"пользователю {userForGrandAccess} выдан доступ в {accessGroupName}");
                }
                catch (Exception ex)
                {
                    logger.Warn(ex,
                        $"Ошибка выдачи доступа пользователю {userForGrandAccess} в группу {accessGroupName}");
                }
            }
        }

        public void DeleteAccessToAdGroup(string accessGroupName, List<string>? usersForDeleteAccess)
        {
            if (usersForDeleteAccess == null)
            {
                return;
            }

            foreach (var userForDeleteAccess in usersForDeleteAccess)
            {
                try
                {
                    ActiveDirectoryCompanyFunctions.RemoveFromGroups(userForDeleteAccess, accessGroupName);
                    logger.Info($"Пользователю {userForDeleteAccess} удален доступ к {accessGroupName}");
                }
                catch (Exception ex)
                {
                    logger.Warn(ex, $"Ошибка удаления доступа пользователю {userForDeleteAccess}");
                }
            }
        }
    }
}
