﻿using Clouds42.AccountDatabase.Contracts.DataHelpers;
using Clouds42.AccountDatabase.Contracts.Publishes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.ActiveDirectory.Contracts.Functions;
using Clouds42.ActiveDirectory.Contracts.Helpers;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    /// Хэлпер публикаций инф. баз
    /// </summary>
    public class PublishHelper(
        IUnitOfWork dbLayer,
        IDatabaseWebPublisher databaseWebPublisher,
        AccountDatabasePathHelper accountDatabasesPathHelper,
        IDatabasePlatformVersionHelper databasePlatformVersionHelper,
        IisApplicationDataHelper iisApplicationDataHelper,
        ILogger42 logger,
        IAccessProvider accessProvider,
        IWebAccessHelper webAccessHelper,
        IHandlerException handlerException,
        ISegmentHelper segmentHelper)
    {
        /// <summary>
        /// Опубликовать инф. базу
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        public void Publish(Guid databaseId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountEncodeId = account.GetEncodeAccountId();
            var dbName = accountDatabase.V82Name;

            var pathToAccountDatabase = GetPathToAccountDatabase(accountDatabase);
            var pathTo1CModule = GetPathToModule1C(accountDatabase);

            logger.Info($"Публикация базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");

            try
            {
                databaseWebPublisher.PublishAccountDatabase(dbName, accountEncodeId, pathToAccountDatabase,
                    pathTo1CModule);

                accountDatabase.PublishStateEnum = PublishState.Published;
                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();

                logger.Info(
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно опубликована");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase, 
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно опубликована.");

            }
            catch (Exception ex)
            {
                logger.Warn(ex,
                    $"[Ошибка публикации базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase,
                $"Ошибка публикации базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}: {ex.Message}.");
                ChangePublishStateUponError(accountDatabase);
                throw;
            }
        }

        /// <summary>
        /// Переопубликовать инф. базу
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        public void RePublish(Guid databaseId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountEncodeId = account.GetEncodeAccountId();
            var pathToAccountDatabase = GetPathToAccountDatabase(accountDatabase);
            var pathTo1CModule = GetPathToModule1C(accountDatabase);
            logger.Info($"Публикация базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");

            try
            {
                databaseWebPublisher.RePublishAccountDatabase(accountDatabase.V82Name, accountEncodeId,
                    pathToAccountDatabase);
                databaseWebPublisher.ModifyVersionPlatformInWebConfig(accountDatabase.V82Name,
                    accountEncodeId, pathTo1CModule);
                accountDatabase.PublishStateEnum = PublishState.Published;
                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();

                logger.Info(
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно опубликована");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase,
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно переопубликована.");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка публикации базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase,
                $"Ошибка при переопубликации базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}: {ex.Message}.");
                ChangePublishStateUponError(accountDatabase);
            }
        }

        /// <summary>
        ///     Переопубликация с изменением web.config файла
        /// </summary>
        /// <param name="databaseId">Идентификатор базы</param>
        public void ModifyVersionPlatformInWebConfig(Guid databaseId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountEncodeId = account.GetEncodeAccountId();
            var dbName = accountDatabase.V82Name;

            var pathTo1CModule = GetPathToModule1C(accountDatabase);
            logger.Info($"Публикация базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
            logger.Info($"Путь до модуля 1С {pathTo1CModule}");

            try
            {
                databaseWebPublisher.ModifyVersionPlatformInWebConfig(dbName, accountEncodeId, pathTo1CModule);

                accountDatabase.PublishStateEnum = PublishState.Published;

                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();

                logger.Info(
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно опубликована");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                ChangePublishStateUponError(accountDatabase);
            }
        }

        /// <summary>
        ///     Переопубликация с изменением vrd файла
        /// </summary>
        /// <param name="databaseId">Идентификатор базы</param>
        public void ModifyVersionPlatformInVrdFile(Guid databaseId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountContentServer = segmentHelper.GetContentServer(account);

            var publishSiteName = segmentHelper.GetPublishSiteName(account);
            var node = dbLayer.CloudServicesContentServerNodeRepository.FirstOrDefault(w => w.ContentServerId == accountContentServer.ID);
            var site =
                iisApplicationDataHelper.GetPublishSite(node.PublishNodeReference.Address, publishSiteName);

            var pathToAccountDatabase = GetPathToAccountDatabase(accountDatabase);

            var accountEncodeId = account.GetEncodeAccountId();

            var basePath = segmentHelper.GetPublishedDatabasePhycicalPath(site.PhysicalPath, accountEncodeId, accountDatabase.V82Name, account);
            var baseAddress = segmentHelper.GetIisWebAddress(accountEncodeId, accountDatabase.V82Name, account);

            logger.Info($"Публикация базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");

            try
            {
                databaseWebPublisher.EditOrCreateVrdFile(pathToAccountDatabase, basePath, baseAddress, segmentHelper.GetWindowsThinkClientDownloadLink(account, accountDatabase));

                logger.Info(
                    $"База {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно переопубликована");
            }
            catch (Exception ex)
            {
                logger.Warn(ex, $"[Ошибка публикации базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                ChangePublishStateUponError(accountDatabase);
            }
        }
        

        /// <summary>
        /// Отменить публикацию инф. базы
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        public void CancelPublish(Guid databaseId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountEncodeId = account.GetEncodeAccountId();
            var dbName = accountDatabase.V82Name;

            logger.Info(
                $"Отмена публикации для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");

            try
            {
                databaseWebPublisher.UnpublishDatabase(dbName, accountEncodeId);

                var groupName = DomainCoreGroups.GetDatabaseGroupName(accountDatabase.Account.IndexNumber, accountDatabase.DbNumber);
                ActiveDirectoryCompanyFunctions.DeleteGroups(groupName);

                accountDatabase.PublishStateEnum = PublishState.Unpublished;
                accountDatabase.UsedWebServices = false;
                dbLayer.DatabasesRepository.Update(accountDatabase);
                dbLayer.Save();

                logger.Info(
                    $"Публикация для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно отменена");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase,
                $"Публикация для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} успешно отменена.");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка отмены публикации базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                LogEventHelper.LogEvent(dbLayer, accountDatabase.AccountId, accessProvider, LogActions.PublishInfoBase,
                $"Ошибка отмены публикации базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}: {ex.Message}.");
                ChangePublishStateUponError(accountDatabase);
                throw;
            }
        }

        /// <summary>
        /// Удалить старую публикацию инф. базы
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="oldSegmentId">ID старого сегмента</param>
        public void RemoveOldDatabasePublication(Guid accountDatabaseId, Guid oldSegmentId)
        {
            if (!TryGetAccountDatabase(accountDatabaseId, out var accountDatabase))
                return;

            if (!TryGetAccount(accountDatabase.AccountId, out var account))
                return;

            var accountEncodeId = account.GetEncodeAccountId();
            var dbName = accountDatabase.V82Name;

            logger.Info(
                $"Удаление старой публикации инф. базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");

            try
            {
                databaseWebPublisher.RemoveOldDatabasePublication(dbName, accountEncodeId, oldSegmentId);
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка удаления старой публикации инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}");
                throw;
            }
        }

        /// <summary>
        ///     Сменить статус публикации на "В процессе публикации"
        /// </summary>
        /// <param name="database">База</param>
        private void ChangePublishStateUponError(Domain.DataModels.AccountDatabase database)
        {
            database.PublishStateEnum = database.PublishStateEnum == PublishState.PendingPublication
                ? PublishState.Unpublished
                : PublishState.Published;
            dbLayer.DatabasesRepository.Update(database);
            dbLayer.Save();
        }

        /// <summary>
        /// Предоставление веб доступов
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void AddWebAccess(Guid databaseId, Guid accountUserId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccountUser(accountUserId, out var accountUser))
                return;

            logger.Info(
                $"Добавление веб прав для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login}");

            try
            {
                webAccessHelper.ManageAccess([accountDatabase], [accountUser.Id],
                    []);

                logger.Info(
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login} успешно добавлены веб права");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка добавления веб прав для базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login}");
                throw;
            }
        }

        /// <summary>
        /// Удаление веб доступов
        /// </summary>
        /// <param name="databaseId">ID инф. базы</param>
        /// <param name="accountUserId">ID пользователя</param>
        public void RemoveWebAccess(Guid databaseId, Guid accountUserId)
        {
            if (!TryGetAccountDatabase(databaseId, out var accountDatabase))
                return;

            if (!TryGetAccountUser(accountUserId, out var accountUser))
                return;

            logger.Info(
                $"Удаление веб прав для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login}");

            try
            {
                webAccessHelper.ManageAccess([accountDatabase], [],
                    [accountUser.Id]);

                logger.Info(
                    $"Для базы {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login} успешно удалены веб права");
            }
            catch (Exception ex)
            {
                handlerException.Handle(ex,
                    $"[Ошибка при удалении веб прав для базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} и пользователя {accountUser.Login}");
                throw;
            }
        }

        /// <summary>
        /// Попытаться получить инф. базу по ID
        /// </summary>
        /// <param name="accountDatabaseId">ID инф. базы</param>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>true - если база найдена</returns>
        private bool TryGetAccountDatabase(Guid accountDatabaseId, out Domain.DataModels.AccountDatabase accountDatabase)
        {
            accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(acDb => acDb.Id == accountDatabaseId);
            if (accountDatabase != null)
                return true;

            logger.Warn($"Инф. база по ID {accountDatabaseId} не найдена");
            return false;
        }

        /// <summary>
        /// Попытаться получить аккаунт по ID
        /// </summary>
        /// <param name="accountId">ID аккаунта</param>
        /// <param name="account">Аккаунт</param>
        /// <returns>true - если аккаунт найден</returns>
        private bool TryGetAccount(Guid accountId, out Account account)
        {
            account = dbLayer.AccountsRepository.FirstOrDefault(ac => ac.Id == accountId);
            if (account != null)
                return true;

            logger.Warn($"Аккаунт по ID {accountId} не найден");
            return false;
        }

        /// <summary>
        /// Попытаться получить пользователя по ID
        /// </summary>
        /// <param name="accountUserId">ID пользователя</param>
        /// <param name="accountUser">Аккаунт</param>
        /// <returns>true - если пользователя найден</returns>
        private bool TryGetAccountUser(Guid accountUserId, out AccountUser accountUser)
        {
            accountUser = dbLayer.AccountUsersRepository.FirstOrDefault(acUs => acUs.Id == accountUserId);
            if (accountUser != null)
                return true;

            logger.Warn($"Пользователь по ID {accountUserId} не найден");
            return false;
        }

        /// <summary>
        /// Получить путь к инф. базе
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Путь к инф. базе</returns>
        private string GetPathToAccountDatabase(Domain.DataModels.AccountDatabase accountDatabase)
            => accountDatabase.IsFile == true
                ? iisApplicationDataHelper.GetConnectionStringToFileInfoBase(
                    accountDatabasesPathHelper.GetPath(accountDatabase))
                : iisApplicationDataHelper.GetConnectionStringToServerInfoBase(accountDatabase.V82Name,
                    accountDatabase.ApplicationName);

        /// <summary>
        /// Получить путь к модулю 1С
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <returns>Путь к модулю 1С</returns>
        private string GetPathToModule1C(Domain.DataModels.AccountDatabase accountDatabase)
            => accountDatabase.IsFile == true
                ? databasePlatformVersionHelper.CreatePathTo1CModuleForFileDb(accountDatabase.AccountId,
                    accountDatabase.PlatformType, accountDatabase.DistributionTypeEnum)
                : databasePlatformVersionHelper.CreatePathTo1CModuleForServerDb(accountDatabase.AccountId,
                    accountDatabase.PlatformType);
    }
}
