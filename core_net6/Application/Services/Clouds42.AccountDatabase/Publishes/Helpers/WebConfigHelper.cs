﻿using System.Xml;
using Clouds42.AccountDatabase.Publishes.Consts;
using Clouds42.BLL.Common.CloudsLocalization;
using Clouds42.Common.Extensions;
using Clouds42.Domain.Enums;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    /// Хэлпер для работы с файлом web.config
    /// </summary>
    public class WebConfigHelper(ICloudLocalizer cloudLocalizer)
    {
        /// <summary>
        /// Получить содержимое файла
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Путь к опубликованной базе</param>
        /// <returns>Содержимое файла</returns>
        public XmlDocument GetFileContent(string publishedDatabasePhysicalPath)
        {
            var webConfigFilePath = GetFilePath(publishedDatabasePhysicalPath);
            var webXmlDoc = webConfigFilePath.GetXmlDocumentFromPath();

            return webXmlDoc;
        }

        /// <summary>
        /// Получить содержимое файла
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Путь к опубликованной базе</param>
        /// <returns>Содержимое файла</returns>
        public XmlDocument GetLockFileContent(string publishedDatabasePhysicalPath)
        {
            var webConfigFilePath = GetLockFilePath(publishedDatabasePhysicalPath);
            var webXmlDoc = webConfigFilePath.GetXmlDocumentFromPath();

            return webXmlDoc;
        }

        /// <summary>
        /// Получить корневой элемент файла или выкинуть исключение
        /// </summary>
        /// <param name="fileContent">Содержимое файла</param>
        /// <returns>Корневой элемент файла</returns>
        public XmlElement GetFileRootElementOrThrowException(XmlDocument fileContent)
            => fileContent.DocumentElement ??
               throw new InvalidOperationException($"Не удалось прочитать корневой элемент файла {fileContent.Name}");

        /// <summary>
        /// Получить путь к файлу
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Путь к опубликованной базе</param>
        /// <returns>Путь к файлу</returns>
        public string GetFilePath(string publishedDatabasePhysicalPath)
            => Path.Combine(publishedDatabasePhysicalPath, PublishDatabaseConstants.WebConfigFileName);

        /// <summary>
        /// Получить путь к файлу
        /// </summary>
        /// <param name="publishedDatabasePhysicalPath">Путь к опубликованной базе</param>
        /// <returns>Путь к файлу</returns>
        public string GetLockFilePath(string publishedDatabasePhysicalPath)
            => Path.Combine(publishedDatabasePhysicalPath, PublishDatabaseConstants.WebLockConfigFileName);

        /// <summary>
        /// Получить путь к веб расширению 1С
        /// </summary>
        /// <param name="accountId">Id аккаунта</param>
        /// <param name="publishedDatabasePhysicalPath">Путь к опубликованной базе</param>
        /// <returns>Путь к веб расширению 1С</returns>
        public string Get1CWebExtensionPath(Guid accountId, string publishedDatabasePhysicalPath)
        {
            if (!File.Exists(GetFilePath(publishedDatabasePhysicalPath)))
                return string.Empty;

            XmlDocument fileContent;
            if (!File.Exists(GetLockFilePath(publishedDatabasePhysicalPath)))
                fileContent = GetFileContent(publishedDatabasePhysicalPath);
            else
                fileContent = GetLockFileContent(publishedDatabasePhysicalPath);

            var fileRootElement = GetFileRootElementOrThrowException(fileContent);

            foreach (var element in fileRootElement.GetElementsByTagName(PublishDatabaseConstants.WebHandlersTagName))
            {
                var node = (XmlNode)element;
                if (node.Attributes?[PublishDatabaseConstants.WebHandlersNameAttributeValue]?.Value ==
                    PublishDatabaseConstants.WebExtension1CName &&
                    node.Attributes?[PublishDatabaseConstants.WebExtension1CPathAttributeName] != null)
                    return node.Attributes[PublishDatabaseConstants.WebExtension1CPathAttributeName].Value;
            }

            var failedToGetPathTo1CWebExtensionFromFilePhrase =
                cloudLocalizer.GetValueByAccount(CloudLocalizationKeyEnum.FailedToGetPathTo1CWebExtensionFromFile,accountId);

            throw new InvalidOperationException(
                $"{failedToGetPathTo1CWebExtensionFromFilePhrase} {publishedDatabasePhysicalPath}");

        }
    }
}
