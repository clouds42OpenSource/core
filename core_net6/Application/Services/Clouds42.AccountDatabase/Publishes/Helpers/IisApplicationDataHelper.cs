﻿using Clouds42.AccountDatabase.Contracts.IISApplication.Constants;
using Clouds42.AccountDatabase.Contracts.IISApplication.Models;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Accounts.Contracts.SegmentMigration.Interfaces;
using Clouds42.Common.Exceptions;
using Clouds42.Configurations;
using Clouds42.LetterNotification.Mails.Efsol;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    /// Хэлпер для получения данных приложения на IIS сервера публикаций
    /// </summary>
    public class IisApplicationDataHelper(
        ISegmentHelper segmentHelper,
        IUnitOfWork dbLayer,
        ILogger42 logger,
        IisHttpClient connectionProviderIis)
    {
        /// <summary>
        /// Получить физический путь сайта публикации
        /// </summary>
        /// <param name="address"></param>
        /// <param name="publishSiteName">Название сайта публикации</param>
        /// <returns>Физический путь сайта публикации</returns>
        public SiteModel? GetPublishSite(string address, string publishSiteName)
        {
            var siteRoot = connectionProviderIis.Get<Root>(address, IISLinksConstants.WebSites);

            if (siteRoot?.Websites == null || !siteRoot.Websites.Any())
            {
                throw new InvalidOperationException("Нода не доступна");
            }

            var site = siteRoot.Websites.FirstOrDefault(x => x.Name == publishSiteName) ?? throw new InvalidOperationException($"Не найдено опубликованное приложение {publishSiteName} на ноде {address}");
            logger.Debug("Получение дефолтного сайта, в который публикуются все базы");
            var siteInfo = connectionProviderIis.Get<SiteModel>(address, site.Links.Self.Href);

            return siteInfo;
        }

        /// <summary>
        /// Уведомить хотлайн о проблемах при перезапуске пулов
        /// </summary>
        /// <param name="errorMessage">Сообщение об ошибке</param>
        public static void NotifyHotlineAboutAppPoolRestartError(string errorMessage)
        {
            if (string.IsNullOrEmpty(errorMessage))
                return;

            var cloudServices = ConfigurationHelper.GetConfigurationValue<string>("cloud-services");

            new Manager42CloudsMail()
                .DisplayName("Команда 42Clouds")
                .To(cloudServices)
                .Subject("Проблема перезапуска пулов на нодах публикаций")
                .Body(errorMessage)
                .SendViaNewThread();

            throw new InvalidOperationException(errorMessage);
        }

        /// <summary>
        /// Возвращает название группы компании в нужном формате
        /// </summary>
        /// <param name="accountIndexNumber">Номер компании</param>
        /// <returns>Название группы компании</returns>
        public static string GetCompanyGroupName(string accountIndexNumber)
            => $"company_{accountIndexNumber}_web";

        /// <summary>
        /// Возвращает строку подключения для файловой информационной базы 1С
        /// </summary>
        /// <param name="filePath">Путь к инф. базе</param>
        /// <returns>Строка подключения для файловой инф. базы</returns>
        public string GetConnectionStringToFileInfoBase(string filePath)
            => $"File=&quot;{filePath}&quot;;";

        /// <summary>
        /// Возвращает строку подключения для серверной информационной базы 1С
        /// </summary>
        /// <param name="v82Name">Название папки, в которой будет размещена база (в формате V82Name)</param>
        /// <param name="platform">Версия платформы ("8.2" или "8.3")</param>
        /// <returns>Строка подключения для серверной инф. базы</returns>
        public string GetConnectionStringToServerInfoBase(string v82Name, string platform)
        {
            var curPlatform = string.IsNullOrEmpty(platform) ? "8.2" : platform.Trim();
            var accountDatabase = dbLayer.DatabasesRepository.FirstOrDefault(d => d.V82Name == v82Name)
                                  ?? throw new NotFoundException($"Инф. база по номеру {v82Name} не найдена");

            var account = dbLayer.AccountsRepository.FirstOrDefault(a => a.Id == accountDatabase.AccountId) ??
                          throw new NotFoundException($"Аккаунт по номеру {accountDatabase.AccountId} не найден");

            var server1C = curPlatform == "8.3"
                ? segmentHelper.GetEnterpriseServer83(account)
                : segmentHelper.GetEnterpriseServer82(account);

            return $"Srvr=&quot;{server1C}&quot;;Ref=&quot;{v82Name}&quot;;";
        }
    }
}
