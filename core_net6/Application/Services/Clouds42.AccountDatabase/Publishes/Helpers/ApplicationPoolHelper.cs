﻿using Clouds42.AccountDatabase.Contracts.IISApplication.Constants;
using Clouds42.AccountDatabase.Contracts.IISApplication.Models;
using Clouds42.AccountDatabase.IISApplication;
using Clouds42.Common.Constants;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Publishes.Helpers
{
    /// <summary>
    /// Хелпер для работы с пулом приложения на IIS
    /// </summary>
    public class ApplicationPoolHelper(IisHttpClient connectionProviderIis, ILogger42 logger)
    {
        /// <summary>
        /// Создать пул приложения на IIS для инф. базы
        /// </summary>
        /// <param name="database">Инф. база</param>
        /// <returns>Название созданного пула приложения</returns>
        public ApplicationPool? CreateApplicationPool(Domain.DataModels.AccountDatabase database, string host)
        {
            var applicationPoolName = GenerateApplicationPoolName(database);
            var pools = connectionProviderIis.Get<Root>(host, IISLinksConstants.AppPools);
            return pools != null && pools.AppPools.All(p => p.Name != applicationPoolName) ? CreateApplicationPool(applicationPoolName, host)
                : pools?.AppPools.FirstOrDefault(p => p.Name == applicationPoolName);
        }

        /// <summary>
        /// Удалить пул приложения на IIS
        /// </summary>
        /// <param name="pool">Пул, который необходимо удалить</param>
        /// <param name="host">Базовый адрес</param>
        /// <param name="appPath">Путь приложения</param>
        public void RemoveApplicationPool(ApplicationPool pool, string host, string appPath)
        {
            if (ContainsOthersApps(pool.Links.Webapps.Href, appPath, host))
            {
                logger.Warn($"Пул {pool.Id} | {pool.Name} содержит приложения. Удаление невозможно");

                return;
            }

            connectionProviderIis.Delete(host, $"{IISLinksConstants.AppPools}?id={pool.Id}");

            logger.Info($"Пул {pool.Id} | {pool.Name} удален");
        }

        /// <summary>
        /// Сформировать название пула приложения на IIS для инф. базы
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <returns>Название пула приложения на IIS для инф. базы</returns>
        private static string GenerateApplicationPoolName(Domain.DataModels.AccountDatabase database) =>
            string.IsNullOrEmpty(database.V82Name) || database.V82Name.Contains(DatabaseConst.FileDatabasePrefix) || database.V82Name.Contains(DatabaseConst.AllLocalesFileDatabasePrefix)
                ? $"{database.Account.IndexNumber}_{database.DbNumber}"
                : database.V82Name;

        /// <summary>
        /// Создать пул приложения на IIS
        /// </summary>
        /// <param name="poolName">Название пула приложения</param>
        /// <param name="host">Хост сервера</param>
        private ApplicationPool? CreateApplicationPool(string poolName, string host)
        {
             return connectionProviderIis.Post<ApplicationPool, ApplicationPool>(host,
                new ApplicationPool{ Name = poolName }, IISLinksConstants.AppPools);
        }

        /// <summary>
        /// Проверить возможность удалить пул приложения
        /// (проверяется, используется ли этот пул другими приложениями)
        /// </summary>
        /// <param name="href">Ссылка на приложения пула</param>
        /// <param name="appPath">Путь приложения</param>
        /// <param name="host">Хост</param>
        /// <returns>Признак указывающий, можно ли удалить пул приложения</returns>
        private bool ContainsOthersApps(string href, string appPath, string host)
        {
            logger.Info($"Выполняется проверка на наличие других приложений связанных с пулом. Ссылка на приложение пула {href}. Путь приложения {appPath}. Хост {host}");

            var webAppRoot = connectionProviderIis.Get<Root>(host, href);

            return webAppRoot?.Webapps.Any(x => x.Path != appPath) ?? false;
        }
    }
}
