﻿using System.Diagnostics;
using System.Text.Json;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Models;
using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Internal.Providers;
using Clouds42.Accounts.Contracts.Configuration.Interfaces;
using Clouds42.Common.DataModels;
using Clouds42.CoreWorkerTask.Contracts.Providers;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.DataContracts.CoreWorker;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using static Clouds42.Configurations.Configurations.CloudConfigurationProvider.AccountDatabase;
using Clouds42.AccountDatabase.Contracts.AutoUpdate.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.Configurations.Configurations;
using Clouds42.Domain.Constants;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Environment.Providers
{
    /// <summary>
    /// Провайдер функций проведения автообновление информационных баз.
    /// </summary>
    internal class AutoUpdateAccountDatabasesProvider(
        AccountDatabasePathHelper accountDatabasePathHelper,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILogger42 logger42,
        IUnitOfWork dbLayer,
        IHandlerException handler,
        IAccountConfigurationDataProvider accountConfigurationDataProvider,
        IStartProcessOfTerminateSessionsInDatabaseProvider startProcessOfTerminateSessionsInDatabaseProvider,
        IRegisterTaskInQueueProvider registerTaskInQueueProvider,
        IAccountDatabaseAutoUpdateProvider accountDatabaseAutoUpdateProvider,
        IAccessProvider accessProvider)
        : IAutoUpdateAccountDatabasesProvider
    {
        readonly CloudServicesEnterpriseServerProvider _cloudServicesEnterpriseServerProvider = new(dbLayer);

        /// <summary>
        /// Провести автообновление информационных баз.
        /// </summary>
        /// <param name="accountDatabasesSupports">Спсок информационных баз</param>
        public void ProcessAutoUpdate(List<AcDbSupport> accountDatabasesSupports)
        {
            if (!accountDatabasesSupports.Any())
            {
                logger42.Info("Нет баз для проведения обслуживания");
                return;
            }
            logger42.Info($"Всего баз попало на обслуживание {accountDatabasesSupports.Count}");
            int iterator = 0;
            var nodes = dbLayer.AutoUpdateNodeRepository.Where(x => !x.DontRunInGlobalAU).ToList();
            var vipNode = nodes.FirstOrDefault(n => n.AutoUpdateNodeType == AutoUpdateNodeType.Vip);
            var htznrNode = nodes.FirstOrDefault(n => n.AutoUpdateNodeType == AutoUpdateNodeType.Htznr);

            nodes.Remove(vipNode);
            nodes.Remove(htznrNode);

            accountDatabasesSupports.ForEach(accountDatabaseSupport =>
            {
                try
                {
                    var accountSegment = accountConfigurationDataProvider.GetAccountSegment(accountDatabaseSupport.AccountDatabase.AccountId);

                    var updateNode = nodes.ElementAt(iterator);

                    if (accountSegment.AutoUpdateNode is not null)
                    {
                        updateNode = accountSegment.AutoUpdateNode;
                    }
                    else if (accountDatabaseSupport.AccountDatabase.Account.AccountConfiguration.IsVip && vipNode is not null)
                    {
                        if (accountSegment.Name.ToLower().Contains("htznr".ToLower()) && htznrNode is not null)
                            updateNode = htznrNode;
                        else
                            updateNode = vipNode;
                    }
                    else
                        iterator = iterator < nodes.Count - 1 ? iterator + 1 : 0;

                    var pathUpdateFolder = updateNode.AutoUpdateObnovlyatorPath + $"{ObnovlyatorConsts.ImportFolderPath}\\{accountDatabaseSupport.AccountDatabase.V82Name}.json";
                    StartUpdate(accountDatabaseSupport, pathUpdateFolder);
                    var tiIDateList = CloudConfigurationProvider.AccountDatabase.Support.GetTiIDateList().Split(',').Select(int.Parse).ToArray();
                    if (tiIDateList.Contains(DateTime.Now.AddHours(-7).Day))
                    {
                        LogEventHelper.LogEvent(dbLayer, accountDatabaseSupport.AccountDatabase.AccountId, accessProvider, LogActions.SuccessAutoUpdate,
                            $"Техническое обслуживание базы {accountDatabaseSupport.AccountDatabase.V82Name} запланировано на ноде {updateNode.NodeAddress}");
                    }
                }
                catch (Exception ex)
                {

                    string message = $"[Ошибка импорта в обновлятор] базы: {accountDatabaseSupport.AccountDatabasesID}" + ex.Message ;
                    LogEventHelper.LogEvent(dbLayer, accountDatabaseSupport.AccountDatabase.AccountId, accessProvider,
                        LogActions.ErrorAutoUpdate, message);
                    handler.Handle(ex, message);
                }
            });

        }


        /// <summary>
        /// Начать обновление закинуть конфиг базы на ноду ао
        /// </summary>
        /// <returns></returns>
        public void StartUpdate(AcDbSupport acDbSupport, string pathUpdateFolder)
        {
            accountDatabaseChangeStateProvider.ChangeState(acDbSupport.AccountDatabasesID, DatabaseState.ProcessingSupport);
            if (acDbSupport.CompleteSessioin)
            {
                if (!startProcessOfTerminateSessionsInDatabaseProvider.TryStartProcess
                (new StartProcessOfTerminateSessionsInDatabaseDto { DatabaseId = acDbSupport.AccountDatabasesID }, out var errorMessage))
                {
                    logger42.Warn($"завершение сеансов для базы {acDbSupport.AccountDatabasesID} не запущено по причине {errorMessage}");
                }
                
            }

            var obnovlyator1CImportDto = CreateObnovlyator1CImportModel(acDbSupport);
            var jsonImportModel = JsonSerializer.Serialize(obnovlyator1CImportDto);
            logger42.Debug($"сформирован json для базы {acDbSupport.AccountDatabasesID}:\n{jsonImportModel}");


            File.WriteAllText(pathUpdateFolder, jsonImportModel);
            logger42.Info($"Файл базы {acDbSupport.AccountDatabasesID} отправлен по пути {pathUpdateFolder}");
        }


        /// <summary>
        /// Создать модель импорта в обновлятор
        /// </summary>
        public Obnovlyator1CImportDto CreateObnovlyator1CImportModel(AcDbSupport acDbSupport, string comment = "")
        {
            var entServer = _cloudServicesEnterpriseServerProvider.GetEnterpriseServerData(acDbSupport.AccountDatabase);
            var accountSegment = accountConfigurationDataProvider.GetAccountSegment(acDbSupport.AccountDatabase.AccountId);
            var sqlAdminName = Cluster.GetSqlLogin();
            var sqlAdminPassword = Cluster.GetSqlPassword();

            var typeBase = "FileProf";
            var template1C = acDbSupport.AccountDatabase.PlatformType == PlatformType.V83
                ? accountSegment.Stable83PlatformVersionReference.Version
                : accountSegment.Stable82PlatformVersionReference.Version;
            var databasePath = accountDatabasePathHelper.GetPath(acDbSupport.AccountDatabase);

            if (acDbSupport.AccountDatabase.IsFile != null && !acDbSupport.AccountDatabase.IsFile.Value)
            {
                typeBase = "ClientServer";
                databasePath = $"{Path.Combine(entServer.ConnectionAddress, acDbSupport.AccountDatabase.V82Name)}";
            }

            return new Obnovlyator1CImportDto
            {
                Operation = "AddBase",
                BaseSettings = new BaseSettings
                {
                    Group = $"{acDbSupport.AccountDatabase.Account.IndexNumber}",
                    Name = $"{acDbSupport.AccountDatabase.V82Name}",
                    Path = databasePath,
                    Version = "bv1C8",
                    User = new CommonLoginModelDto { Name = acDbSupport.Login, Password = acDbSupport.Password },
                    Platform = new Obnovlyator1CPlatformDto
                    {
                        Template = template1C,
                        Bitness = "ba64",
                        Type = typeBase
                    },
                    Comment = comment,
                    LaunchParameters = acDbSupport.AccountDatabase.LaunchParameters,
                    Cluster = new Obnovlyator1CCluster
                    {
                        Admin = entServer is not null
                        ? new CommonLoginModelDto { Name = entServer.AdminName, Password = entServer.AdminPassword }
                        : new CommonLoginModelDto { },
                        LstConfigUrl = entServer is not null
                        ? entServer.ClusterSettingsPath
                        : "D:\\1cv8\\srvinfo\\reg_1541\\1CV8Clst.lst"

                    },
                    DatabaseServer = new Obnovlyator1CDatabaseServer
                    {
                        SqlType = "MSSQLServer",
                        AddressForUpdater = accountSegment.CloudServicesSQLServer.ConnectionAddress,
                        AddressForCluster = accountSegment.CloudServicesSQLServer.ConnectionAddress,
                        Admin = new CommonLoginModelDto { Name = sqlAdminName, Password = sqlAdminPassword },
                        SqlServerIsOtherMachine = true
                    },
                    SqlBackupSettings = new Obnovlyator1CSqlBackupSettings
                    {
                        CompressBackup = true,
                        CompressByUpdater = true,
                        OptimizeClusterTestForSqlBackup = false,
                        IsDtBackupDisabled = true,
                    },
                    CompleteSession = acDbSupport.CompleteSessioin
                }
                
            };
        }


        /// <summary>
        /// Ручное обновление базы
        /// </summary>
        public void UpdateDatabase(Guid accountDatabaseId)
        {
            logger42.Debug($"Начинаю ручное обновление базы {accountDatabaseId}");
            
            var acDbSupport =  dbLayer.AcDbSupportRepository.FirstOrDefault(x => x.AccountDatabasesID == accountDatabaseId);
            if (acDbSupport is null)
            {
                logger42.Info($"поддержка базы {accountDatabaseId} не найдена");
                return;
            }

            if (acDbSupport.AccountDatabase.State != DatabaseState.Ready.ToString())
                throw new InvalidOperationException($"Инф база {acDbSupport.AccountDatabase} не готова к обслуживанию ");

            LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.ManualUpdateDatabase,
                $"Начато ручное обновление базы {acDbSupport.AccountDatabase.V82Name}");

            var soloUpdateNode =  GetCorrespondenceFreeNode(acDbSupport);
            if (soloUpdateNode is null)
            {
                if (!dbLayer.UpdateNodeQueueRepository.Any(x => x.AccountDatabaseId == acDbSupport.AccountDatabasesID))
                {
                    logger42.Warn("Данная база уже поставлена в очередь на обновление");
                    return;
                }

                var updateQueue = new UpdateNodeQueue { Id = Guid.NewGuid(), AccountDatabaseId = accountDatabaseId, AddDate = DateTime.Now };
                dbLayer.UpdateNodeQueueRepository.Insert(updateQueue);
                dbLayer.Save();    
                logger42.Info($"Свободная нода не найдена база {accountDatabaseId} поставлена в очередь на обновление");
                return;
            }

            try
            {
                var pathUpdateFolder = $"\\\\{soloUpdateNode.NodeAddress}{ObnovlyatorConsts.ImportFolderPath}\\{acDbSupport.AccountDatabase.V82Name}.json";

                ChangeNodeState(soloUpdateNode, true);
                StartUpdate(acDbSupport, pathUpdateFolder);
                logger42.Info($"На ноде {soloUpdateNode.NodeAddress} начато обновление базы {acDbSupport.AccountDatabasesID}");
                
                StartObnovlyatorUpdate(acDbSupport, soloUpdateNode);

                HandleUpdateResult(acDbSupport, soloUpdateNode);

            }
            catch (Exception ex)
            {
                handler.Handle(ex, $"[Ошибка ручного обновления базы] {ex.Message}");
                LogEventHelper.LogEvent(dbLayer, acDbSupport.AccountDatabase.AccountId, accessProvider, LogActions.ErrorManualUpdate,
                    $"Ошибка ручного обновления базы {acDbSupport.AccountDatabase.V82Name}: {ex.Message}");
            }

            accountDatabaseChangeStateProvider.ChangeState(acDbSupport.AccountDatabasesID, DatabaseState.Ready);
            ChangeNodeState(soloUpdateNode, false);
            СontinueUpdateQueue();
        }


        /// <summary>
        /// Запустить обнволение в обновляторе 
        /// </summary>
        public void StartObnovlyatorUpdate(AcDbSupport acDbSupport, AutoUpdateNode autoUpdateNode)
        {
            var databasePath = accountDatabasePathHelper.GetPath(acDbSupport.AccountDatabase);
            var entServer = _cloudServicesEnterpriseServerProvider.GetEnterpriseServerData(acDbSupport.AccountDatabase);

            if (acDbSupport.AccountDatabase.IsFile != null && !acDbSupport.AccountDatabase.IsFile.Value)
                databasePath = $"{Path.Combine(entServer.ConnectionAddress, acDbSupport.AccountDatabase.V82Name)}";

            string arguments = @$"-Update -BasePath {databasePath}";
            StartObnovlyatorProcess(autoUpdateNode, arguments);
        }

        /// <summary>
        /// Запустить процесс через командную строку
        /// </summary>
        private void StartObnovlyatorProcess(AutoUpdateNode autoUpdateNode, string arguments)
        {
            var updater1CPath = Path.Combine(autoUpdateNode.ManualUpdateObnovlyatorPath, "Updater1C.exe");
            var userName = Configurations.Configurations.CloudConfigurationProvider.AutoUpdateDatabase.GetObnovlyator1CAccessUserName();
            var password = Configurations.Configurations.CloudConfigurationProvider.AutoUpdateDatabase.GetObnovlyator1CAccessPassword();
            var script = $@"\\{autoUpdateNode.NodeAddress} -u {userName} -p {password} -i ""{updater1CPath}"" {arguments}";

            logger42.Debug($"Запускаю обнволятор на ноде {autoUpdateNode.NodeAddress}");
            logger42.Debug($"выполняю скрипт: {script}");
            string currentDir = AppDomain.CurrentDomain.BaseDirectory;

            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(currentDir, "PsExec.exe"),
                Arguments = script,
                UseShellExecute = false,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
            };

            using Process process = Process.Start(startInfo);
            process.WaitForExit();
            string result = process.StandardOutput.ReadToEnd();
            logger42.Info($"Результат выполнения скрипта: {result}");

            if (process.ExitCode != 0)
                logger42.Error("Процесс по запуску обновлятора прошел с ошибкой");
        }

        /// <summary>
        /// Обработать результат ручного обновления
        /// </summary>
        public void HandleUpdateResult(AcDbSupport acDbSupport, AutoUpdateNode soloUpdateNode)
        {
            var updateNodeDto = new UpdateNodeDto
            {
                Id = soloUpdateNode.Id,
                ImportFolderPath = "\\\\" + soloUpdateNode.NodeAddress + ObnovlyatorConsts.ImportFolderPath,
                ReportFolderPath = "\\\\" + soloUpdateNode.NodeAddress + ObnovlyatorConsts.ReportFolderPath,
                NodeAddress = soloUpdateNode.NodeAddress
            };

            var resultReportDir = new DirectoryInfo(updateNodeDto.ReportFolderPath).GetDirectories().ToList()
                .FirstOrDefault(x => x.Name.Contains($"{acDbSupport.AccountDatabase.V82Name}"))
                ?? throw new InvalidOperationException($"Отчёт по базе {acDbSupport.AccountDatabasesID} в папке {updateNodeDto.ReportFolderPath} не найден ");


            var encodeReport = accountDatabaseAutoUpdateProvider.GetEncodeReport(resultReportDir);
            accountDatabaseAutoUpdateProvider.HandleAutoUpdateResult(encodeReport, true);

            accountDatabaseAutoUpdateProvider.DeleteAllReports(updateNodeDto);
            accountDatabaseAutoUpdateProvider.DeleteAllBasesFromObnovlyator(updateNodeDto);
            StartObnovlyatorProcess(soloUpdateNode, "-Update");
        }

        /// <summary>
        /// Изменить состояние ноды
        /// </summary>
        private void ChangeNodeState(AutoUpdateNode autoUpdateNode, bool state)
        {
            autoUpdateNode.IsBusy = state;
            dbLayer.AutoUpdateNodeRepository.Update(autoUpdateNode);
            dbLayer.Save();
            logger42.Info($"Состояние занятости ноды {autoUpdateNode.NodeAddress} изменено на {state}");
        }

        /// <summary>
        /// Получить соответствующую аккаунту свободную ноду
        /// </summary>
        public AutoUpdateNode GetCorrespondenceFreeNode(AcDbSupport acDbSupport)
        {
            var accountSegment = accountConfigurationDataProvider.GetAccountSegment(acDbSupport.AccountDatabase.AccountId);

            if (accountSegment.AutoUpdateNode is null || accountSegment.AutoUpdateNode.IsBusy)
            {
                return !acDbSupport.AccountDatabase.Account.AccountConfiguration.IsVip
                    ? GetUpdateNode(AutoUpdateNodeType.Common)
                    : GetUpdateNode(accountSegment.Name.ToLower().Contains("htznr".ToLower())
                        ? AutoUpdateNodeType.Htznr
                        : AutoUpdateNodeType.Vip);
            }

            if (string.IsNullOrEmpty(accountSegment.AutoUpdateNode.ManualUpdateObnovlyatorPath))
                throw new InvalidOperationException($"Ваша нода обновления не настроена обратитесь в поддержку");
            return accountSegment.AutoUpdateNode;
        }

        public AutoUpdateNode GetUpdateNode(AutoUpdateNodeType autoUpdateNodeType)
        {
            var nodes = dbLayer.AutoUpdateNodeRepository.Where(x => x.AutoUpdateNodeType == autoUpdateNodeType && !string.IsNullOrEmpty(x.ManualUpdateObnovlyatorPath));
            if (nodes is null || !nodes.Any())
                throw new InvalidOperationException($"Ваша нода обновления не настроена обратитесь в поддержку");

            return nodes.FirstOrDefault(x => !x.IsBusy);
        }

        public void СontinueUpdateQueue()
        {
            var acDbInQueue = dbLayer.UpdateNodeQueueRepository.OrderBy(x=>x.AddDate).FirstOrDefault();
            if (acDbInQueue is not null)
            {
                registerTaskInQueueProvider.RegisterTask(
                    CoreWorkerTasksCatalog.AccountDatabaseUpdateJob,
                    new ParametrizationModelDto(new AccountDatabaseUpdateJobParamsDto { AccountDatabaseId = acDbInQueue.AccountDatabaseId }.ToJson()),
                    $"Обновление базы {acDbInQueue.AccountDatabaseId}");
                dbLayer.UpdateNodeQueueRepository.Delete(acDbInQueue);
                dbLayer.Save();
                logger42.Info($"База {acDbInQueue.AccountDatabaseId} удалена из очереди, так как начато обновление");
            }
        }
    }
}
