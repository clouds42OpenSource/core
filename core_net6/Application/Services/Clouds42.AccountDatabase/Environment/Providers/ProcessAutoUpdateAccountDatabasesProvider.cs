﻿using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.AccountDatabase.Processors.Helpers;
using Clouds42.AccountDatabase.Processors.SupportProcessors;
using Clouds42.DataContracts.AccountDatabase.AutoUpdateAccountDatabase;
using Clouds42.Domain.DataModels;

namespace Clouds42.AccountDatabase.Environment.Providers
{
    /// <summary>
    /// Провайдер функций проведения автообновления информационных баз.
    /// </summary>
    internal class ProcessAutoUpdateAccountDatabasesProvider(
        SupportProcessorFactory<AutoUpdateSupportProcessor> supportProcessorFactory)
        : SupportJobsAccountDatabasesProviderBase<AutoUpdateSupportProcessor>(supportProcessorFactory),
            IProcessAutoUpdateAccountDatabasesProvider
    {
        /// <summary>
        /// Обработать инф. базу
        /// </summary>
        /// <param name="acDbSupport">Модель поддержки инф. базы</param>
        /// <returns>Результат обработки</returns>
        public AccountDatabaseAutoUpdateResultDto ProcessAccountDatabase(AcDbSupport acDbSupport)
            => ProcessSingleAccountDatabase(acDbSupport);

        /// <summary>
        /// Создать результат автообновления
        /// </summary>
        /// <param name="processor">Процессор АО</param>
        /// <returns>Результат автообновления</returns>
        protected override AccountDatabaseSupportResultDto CreateSupportResult(AutoUpdateSupportProcessor processor)
            => SupportProcessDataHelper.CreateSupportResult(processor);
    }
}