﻿using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;
using Microsoft.EntityFrameworkCore;

namespace Clouds42.AccountDatabase.Environment.Providers
{
    /// <summary>
    /// Хелпер для смены состояния баз
    /// </summary>
    public class ChangeDatabasesStateProvider(IUnitOfWork unitOfWork, ILogger42 logger) : IChangeDatabasesStateProvider
    {
        /// <summary>
        /// Обновить статус баз, которые залипли в статусе ProcessingSupport после АО/ТиИ
        /// </summary>
        /// <param name="acDbSupports">Список моделей поддержки инф. базы</param>
        public void UpdateAccountDatabasesState(List<AcDbSupport> acDbSupports)
        {
            logger.Info("Начинаем проверку статусов баз после АО");
            var accountDatabasesIds = GetAccountDatabasesIds(acDbSupports);
            try
            {
                var databasesIds = unitOfWork.DatabasesRepository.Where(db =>
                    accountDatabasesIds.Contains(db.Id) && db.State == DatabaseState.ProcessingSupport.ToString()).Select(db => db.Id).ToList();

                if (!databasesIds.Any())
                    return;

                logger.Info($"Для баз прошедших АО статусы не сменились для {databasesIds.Count} баз(ы).");

                unitOfWork.DatabasesRepository
                    .AsQueryable()
                    .Where(x => databasesIds.Contains(x.Id))
                    .ExecuteUpdate(x => x.SetProperty(y => y.State, DatabaseState.Ready.ToString()));

                logger.Info("Статусы баз успешно обновлены");
            }
            catch (Exception ex)
            {
                logger.Info($"При смене статусов ИБ возникла ошибка {ex.Message}");
            }
        }

        /// <summary>
        /// Получить список Id баз из списка моделей поддержки инф. базы
        /// </summary>
        /// <param name="acDbSupports">Список моделей поддержки инф. базы</param>
        /// <returns>Список Id информационных баз</returns>
        private static List<Guid> GetAccountDatabasesIds(List<AcDbSupport> acDbSupports) =>
            acDbSupports.Select(ads => ads.AccountDatabasesID).ToList();
        
    }
}
