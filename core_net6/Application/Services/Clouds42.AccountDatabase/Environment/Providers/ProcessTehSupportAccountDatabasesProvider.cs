﻿using Clouds42.AccountDatabase.Contracts.Edit.Interfaces;
using Clouds42.AccountDatabase.Contracts.Environment.Interfaces;
using Clouds42.AccountDatabase.Processors.SupportProcessors;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.DataContracts.Service.SendGrid.LetterTemplate.IncomingModels;
using Clouds42.Domain.DataModels;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums._1C;
using Clouds42.Domain.Enums.AccountDatabase;
using Clouds42.HandlerExeption.Contract;
using Clouds42.LetterNotification.Contracts;
using Clouds42.LetterNotification.LetterNotifications.AccountDatabase;
using Clouds42.Locales.Contracts.Interfaces;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Environment.Providers
{
    /// <summary>
    /// Провайдер функций проведения ТиИ для информационных баз.
    /// </summary>
    internal class ProcessTehSupportAccountDatabasesProvider(
        IAccessProvider accessProvider,
        SupportProcessorFactory<TehSupportProcessor> supportProcessorFactory,
        IHandlerException handlerException,
        IUnitOfWork dbLayer,
        IAccountDatabaseChangeStateProvider accountDatabaseChangeStateProvider,
        ILetterNotificationProcessor letterNotificationProcessor,
        ILocalesConfigurationDataProvider localesConfigurationDataProvider,
        IChangeDatabasesStateProvider changeDatabasesStateProvider,
        ILogger42 logger)
        : SupportAccountDatabasesProviderBase<TehSupportProcessor>(accessProvider, supportProcessorFactory,
                handlerException, logger, dbLayer),
            IProcessTehSupportAccountDatabasesProvider
    {
        private readonly IUnitOfWork _dbLayer = dbLayer;

        /// <summary>
        /// Провести ТиИ для информационных баз.
        /// </summary>
        /// <param name="accountDatabasesSupports">Спсок информационных баз</param>
        /// <returns>Признак успешности</returns>
        public bool Process(List<AcDbSupport> accountDatabasesSupports)
        {
            var techSupportResult = ProcessAccountDatabases(accountDatabasesSupports);
            changeDatabasesStateProvider.UpdateAccountDatabasesState(accountDatabasesSupports);

            return techSupportResult;
        }

        /// <summary>
        /// Завершить обработку процессора ТиИ.
        /// </summary>
        protected override void FinalizeProcess(TehSupportProcessor processor)
        {
            using (var dbScope = _dbLayer.SmartTransaction.Get())
            {
                try
                {
                    var accountDatabaseSupport =
                        _dbLayer.AcDbSupportRepository.FirstOrDefault(s =>
                            s.AccountDatabasesID == processor.AccountDatabasesID);

                    accountDatabaseChangeStateProvider.ChangeState(accountDatabaseSupport.AccountDatabasesID,
                        DatabaseState.Ready);

                    accountDatabaseSupport.PreparedForTehSupport = false;
                    if (processor.State == AccountDatabaseSupportProcessorState.Success)
                        accountDatabaseSupport.TehSupportDate = DateTime.Now;

                    _dbLayer.AcDbSupportRepository.Update(accountDatabaseSupport);
                    _dbLayer.Save();
                    dbScope.Commit();
                }
                catch (Exception ex)
                {
                    _logger.Warn(ex, $"[Ошибка завершения обработки ТиИ] номер базы: {processor.V82Name}");
                    dbScope.Rollback();
                    throw;
                }
            }

            RegisterSupportHistory(processor);
        }

        /// <summary>
        /// Уведомить о проведении ТиИ.
        /// </summary>
        protected override void NotifyCompleteOperation()
        {
            
            var supportGroups = Results.GroupBy(res => res.AccountId).ToList();
            supportGroups.ForEach(supportGroup =>
            {
                var successfullyCompletedResults = supportGroup.Where(l =>
                    l.State == AccountDatabaseSupportProcessorState.Success ||
                    l.State == AccountDatabaseSupportProcessorState.Skip).ToList();

                if (!successfullyCompletedResults.Any())
                    return;

                var siteAuthorityUrl = localesConfigurationDataProvider.GetCpSiteUrlForAccount(supportGroup.Key);

                letterNotificationProcessor
                    .TryNotify<TehSupportResultLetterNotification, TehSupportResultLetterModelDto>(
                        GetTehSupportResultLetterModel(supportGroup.Key, successfullyCompletedResults,
                            siteAuthorityUrl));
            });
        }

        /// <summary>
        /// Создать историческую запись по резкльтату операции ТиИ.
        /// </summary>
        protected override AcDbSupportHistory CreateSupportHistory(TehSupportProcessor supportResult)
        {
            var errorOperation = supportResult.State == AccountDatabaseSupportProcessorState.Error ||
                                 supportResult.State == AccountDatabaseSupportProcessorState.FatalError ||
                                 supportResult.State == AccountDatabaseSupportProcessorState.FatalValidationError;

            var history = new AcDbSupportHistory
            {
                ID = Guid.NewGuid(),
                AccountDatabaseId = supportResult.AccountDatabasesID,
                EditDate = DateTime.Now,
                Description = supportResult.Message ?? supportResult.State.Description(),
                Operation = Operation,
                State = errorOperation ? SupportHistoryState.Error : SupportHistoryState.Success
            };

            return history;
        }

        /// <summary>
        /// Операция обработчика.
        /// </summary>
        protected override DatabaseSupportOperation Operation
            => DatabaseSupportOperation.TechSupport;

        /// <summary>
        /// Обозначение успешного выполнения в логе.
        /// </summary>
        protected override LogActions LogSuccessAction
            => LogActions.SuccessTehSupport;

        /// <summary>
        /// Обозначение ошибки в логе.
        /// </summary>
        protected override LogActions LogErrorAction
            => LogActions.ErrorTehSupport;
    }
}
