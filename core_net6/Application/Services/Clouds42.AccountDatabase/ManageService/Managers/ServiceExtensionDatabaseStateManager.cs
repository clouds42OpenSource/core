﻿using Clouds42.AccountDatabase.Contracts.ManageService.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;
using Clouds42.Domain.Access;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Logger;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.ManageService.Managers
{
    /// <summary>
    /// Менеджер для работы с состоянием расширения сервиса
    /// для информационной базы в МС
    /// </summary>
    public class ServiceExtensionDatabaseStateManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        ISetServiceExtensionDatabaseActivationStatusCommand serviceExtensionDatabaseActivationStatusCommand,
        IServiceExtensionDatabaseStateProvider serviceExtensionDatabaseStateProvider,
        ILogger42 logger)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Установить расширение сервиса
        /// для информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        public ManagerResult InstallServiceExtensionDatabase(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            var message =
                $"[Отправка запроса в МС на установку расширения сервиса] '{manageServiceExtensionDatabaseState.ServiceId}'" + 
                $" для информационной базы '{manageServiceExtensionDatabaseState.AccountDatabaseId}'";
            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabaseInMs,
                    () => AccountIdByAccountDatabase(manageServiceExtensionDatabaseState.AccountDatabaseId));

                logger.Info(message);
                serviceExtensionDatabaseStateProvider.InstallServiceExtensionDatabase(manageServiceExtensionDatabaseState);
                logger.Info($"{message} завершилась успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"{message} завершилась с ошибкой.");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Удалить расширение сервиса
        /// из информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        public ManagerResult DeleteServiceExtensionDatabase(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            var message =
                $"[Отправка запроса в МС на удаление расширения сервиса ]'{manageServiceExtensionDatabaseState.ServiceId}'" +
                $" из информационной базы '{manageServiceExtensionDatabaseState.AccountDatabaseId}'";

            try
            {
                AccessProvider.HasAccess(ObjectAction.ManageServiceExtensionDatabaseInMs,
                    () => AccountIdByAccountDatabase(manageServiceExtensionDatabaseState.AccountDatabaseId));

                logger.Info(message);
                serviceExtensionDatabaseStateProvider.DeleteServiceExtensionDatabase(manageServiceExtensionDatabaseState);
                logger.Info($"{message} завершилась успешно");

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"{message} завершилась с ошибкой.");
                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Установить статус активации расширения сервиса для инф. базы
        /// </summary>
        /// <param name="serviceExtensionDatabaseActivationStatusDto">Модель для установки статуса активации
        /// расширения сервиса для инф. базы</param>
        /// <returns>Результат установки статуса</returns>
        public ManagerResult SetServiceExtensionDatabaseActivationStatus(
            SetServiceExtensionDatabaseActivationStatusDto serviceExtensionDatabaseActivationStatusDto)
        {
            var message =
                $@"[Отправка запроса в МС на установку статуса активации] '{serviceExtensionDatabaseActivationStatusDto.IsActive}' 
                расширения сервиса '{serviceExtensionDatabaseActivationStatusDto.ServiceId}' 
                для аккаунта '{serviceExtensionDatabaseActivationStatusDto.AccountId}'";

            try
            {
                AccessProvider.HasAccess(ObjectAction.SetServiceExtensionDatabaseActivationStatusInMs,
                    () => AccountIdByAccountDatabase(serviceExtensionDatabaseActivationStatusDto.AccountId));

                logger.Info(message);
                var result = serviceExtensionDatabaseActivationStatusCommand.Execute(serviceExtensionDatabaseActivationStatusDto);
                if (!string.IsNullOrEmpty(result))
                {
                    logger.Info($"{message} завершилась с ошибкой: {result}.");
                    return Ok();
                }

                logger.Info($"{message} завершилась успешно");
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex, $"{message} завершилась с ошибкой.");
                return PreconditionFailed(ex.Message);
            }
        }

    }
}
