﻿using Clouds42.AccountDatabase.Contracts.ManageService.Interfaces;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.Service.Partner.ServiceExtensionDatabase;

namespace Clouds42.AccountDatabase.ManageService.Providers
{
    /// <summary>
    /// Провайдер для работы с состоянием расширения сервиса
    /// для информационной базы в МС
    /// </summary>
    internal class ServiceExtensionDatabaseStateProvider(
        IDeleteServiceExtensionDatabaseCommand deleteServiceExtensionCommand,
        IInstallServiceExtensionDatabaseCommand installServiceExtensionCommand)
        : IServiceExtensionDatabaseStateProvider
    {
        /// <summary>
        /// Установить расширение сервиса
        /// для информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        public void InstallServiceExtensionDatabase(
            ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            installServiceExtensionCommand.Execute(manageServiceExtensionDatabaseState);
        }

        /// <summary>
        /// Удалить расширение сервиса
        /// из информационной базы в МС
        /// </summary>
        /// <param name="manageServiceExtensionDatabaseState">Модель управления состоянием расширения сервиса
        /// для информационной базы</param>
        public void DeleteServiceExtensionDatabase(ManageServiceExtensionDatabaseStateDto manageServiceExtensionDatabaseState)
        {
            deleteServiceExtensionCommand.Execute(manageServiceExtensionDatabaseState);
        }
       
       
    }
}
