﻿using Clouds42.AccountDatabase.Contracts.Processors.Managers;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Managers
{
    /// <summary>
    /// Менеджер по выполнению процесса для работы с информационной базой
    /// </summary>
    public class ExecuteProcessForWorkingWithDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IRemoveAccountDatabaseProcess removeAccountDatabaseProcess,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IHandlerException handlerException,
        AccountDatabaseLogEventMessageBuilder accountDatabaseLogEventMessageBuilder,
        IArchiveAccountDatabaseToTombProcess archiveAccountDatabaseToTombProcess)
        : BaseManager(accessProvider, dbLayer, handlerException), IExecuteProcessForWorkingWithDatabaseManager
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Выполнить процесс по удалению инф. базы
        /// </summary>
        /// <param name="processParams">Модель параметров процесса удаления инф. базы</param>
        public ManagerResult ExecuteProcessOfRemoveDatabase(ProcessOfRemoveDatabaseParamsDto processParams)
        {
            var accountDatabase = accountDatabaseDataHelper.GetAccountDatabase(processParams.DatabaseId);
            if (accountDatabase == null)
                return PreconditionFailed<bool>($"Не удалось найти инф. базу по Id {processParams.DatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_DeleteToTomb, () => accountDatabase.AccountId);

                removeAccountDatabaseProcess.ExecuteProcessOfRemoveDatabase(processParams);

                LogEvent(() => accountDatabase.AccountId, LogActions.DeleteAccountDatabase,
                    accountDatabaseLogEventMessageBuilder.BuildMessageAboutDeletion(accountDatabase), processParams.InitiatorId);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка удаления инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");

                return PreconditionFailed<bool>(
                    $"Не удалось удалить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.Message}");
            }
        }

        /// <summary>
        /// Выполнить процесс по архивации
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров процесса на архивацию
        /// информационной базы в склеп</param>
        public ManagerResult ExecuteProcessOfArchiveDatabaseToTomb(ProcessOfArchiveDatabaseToTombParamsDto processParams)
        {
            var accountDatabase = accountDatabaseDataHelper.GetAccountDatabase(processParams.DatabaseId);
            if (accountDatabase == null)
                return PreconditionFailed<bool>($"Не удалось найти инф. базу по Id {processParams.DatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_Change, () => accountDatabase.AccountId);
                archiveAccountDatabaseToTombProcess.ExecuteProcessOfArchiveDatabaseToTomb(processParams);

                return Ok(true);
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка архивации ИБ] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп.");

                return PreconditionFailed<bool>(
                    $"Не удалось перенести базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп. Описание ошибки: {ex.Message}");
            }
        }
    }
}
