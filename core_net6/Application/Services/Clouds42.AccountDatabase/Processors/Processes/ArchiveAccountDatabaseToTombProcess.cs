﻿using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountDatabase.Tomb.Managers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.Domain.Enums;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Processors.Processes
{
    /// <summary>
    /// Процесс архивации информационной базы в склеп
    /// </summary>
    internal class ArchiveAccountDatabaseToTombProcess(
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IDeleteDelimiterCommand deleteDelimiterCommand,
        TombTaskCreatorManager tombTaskCreatorManager,
        DatabaseStatusHelper databaseStatusHelper,
        ILogger42 logger)
        : IArchiveAccountDatabaseToTombProcess
    {
        /// <summary>
        /// Выполнить процесс по архивации
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров процесса на архивацию
        /// информационной базы в склеп</param>
        public void ExecuteProcessOfArchiveDatabaseToTomb(ProcessOfArchiveDatabaseToTombParamsDto processParams)
        {
            var database = accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(processParams.DatabaseId);

            if (database.StateEnum == DatabaseState.DetachedToTomb)
            {
                logger.Info($"[{processParams.DatabaseId}] :: Информационная база уже в статусе : {database.State}.");
                return;
            }

            DeleteDatabaseOnDelimiterInMs(database);
            CreateTaskForArchiveAccountDatabaseToTomb(processParams, database);
            synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(processParams.DatabaseId);
        }

        /// <summary>
        /// Создать новую задачу воркера на архивацию базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров процесса на архивацию
        /// информационной базы в склеп</param>
        /// <param name="database">Инф. база</param>
        private void CreateTaskForArchiveAccountDatabaseToTomb(ProcessOfArchiveDatabaseToTombParamsDto processParams, Domain.DataModels.AccountDatabase database)
        {
            if (database.IsDelimiter())
                return;

            tombTaskCreatorManager.CreateTaskForArchivateAccountDatabaseToTomb(new DeleteAccountDatabaseToTombJobParamsDto
            {
                AccountDatabaseId = processParams.DatabaseId,
                SendMail = processParams.NeedNotifyHotlineAboutFailure,
                ForceUpload = processParams.NeedForceFileUpload,
                Trigger = processParams.Trigger,
                InitiatorId = processParams.InitiatorId
            });
        }

        /// <summary>
        /// Удалить базу на разделителях в МС
        /// </summary>
        /// <param name="database">Инф. база</param>
        public void DeleteDatabaseOnDelimiterInMs(Domain.DataModels.AccountDatabase database)
        {
            if (!database.IsDelimiter())
                return;
            var locale = database.Account.AccountConfiguration.Locale.Name;
            logger.Info($"[{database.Id}] :: Запрос на архивацию базы на разделителях отправлен в МС.");
            var result = deleteDelimiterCommand.Execute(database.Id, locale, "auto");
            if (!string.IsNullOrEmpty(result))
            {
                logger.Info($"[{database.Id}] :: Запрос на архивацию базы на разделителях вернул ошибку {result}.");
                databaseStatusHelper.SetDbStatus(database.Id, DatabaseState.DeletedFromCloud, result);
                return;
            }

            logger.Info($"[{database.Id}] :: Запрос на архивацию базы на разделителях успешно отправлен в МС.");
        }
    }
}
