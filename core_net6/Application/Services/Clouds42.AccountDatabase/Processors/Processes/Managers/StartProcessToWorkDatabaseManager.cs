﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.BLL.Common;
using Clouds42.BLL.Common.Access.Providers.Interfaces;
using Clouds42.Common.ManagersResults;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseAccesses;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AcDbAccess;
using Clouds42.DataContracts.CloudServicesSegment.TerminateSessionsInDatabase;
using Clouds42.Domain.Access;
using Clouds42.Domain.Enums;
using Clouds42.HandlerExeption.Contract;
using Clouds42.Repositories.Interfaces.Common;

namespace Clouds42.AccountDatabase.Processors.Processes.Managers
{
    /// <summary>
    /// Менеджер для запуска процесса по работе с инф. базой
    /// </summary>
    public class StartProcessToWorkDatabaseManager(
        IAccessProvider accessProvider,
        IUnitOfWork dbLayer,
        IHandlerException handlerException,
        IStartProcessOfRemoveDatabaseProvider startProcessOfRemoveDatabaseProvider,
        IStartProcessOfArchiveDatabaseToTombProvider startProcessOfArchiveDatabaseToTombProvider,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IStartProcessOfRemoveAccessesFromDatabaseProvider startProcessOfRemoveAccessesFromDatabaseProvider,
        IStartProcessOfGrandAccessesToDatabaseProvider startProcessOfGrandAccessesToDatabaseProvider,
        IAccountDatabaseProvider accountDatabaseProvider,
        IStartProcessOfTerminateSessionsInDatabaseProvider startProcessOfTerminateSessionsInDatabaseProvider)
        : BaseManager(accessProvider, dbLayer, handlerException)
    {
        private readonly IHandlerException _handlerException = handlerException;

        /// <summary>
        /// Запустить процесс удаления инф. базы
        /// </summary>
        /// <param name="databaseId">Id базы</param>
        public ManagerResult StartProcessOfRemoveDatabase(Guid databaseId)
        {
            
            var accountDatabase = accountDatabaseDataHelper.GetAccountDatabase(databaseId);
            if (accountDatabase == null)
                return PreconditionFailed<bool>($"Не удалось найти инф. базу по Id {databaseId}");
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_StartProcessOfRemove,
                    () => accountDatabase.AccountId);
                startProcessOfRemoveDatabaseProvider.StartProcessOfRemoveDatabase(databaseId);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка удаления инф. базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}.");

                return PreconditionFailed<bool>(
                    $"Не удалось удалить базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)}. Описание ошибки: {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процессы удаления инф. баз
        /// </summary>
        /// <param name="databasesId">Id баз</param>
        public ManagerResult StartProcessOfRemoveDatabases(List<Guid> databasesId)
        {
            
            if (IsSourceDatabase(databasesId.FirstOrDefault()))
            {
                string message = "База является материнской";
                Logger.Warn(message);
                return PreconditionFailed<List<ManageAccessResultDto>>(message);
            }
            var accountId = GetAccountId(databasesId);
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_StartProcessOfRemove, () => accountId);
                startProcessOfRemoveDatabaseProvider.StartProcessOfRemoveDatabases(databasesId);
                return Ok();
            }
            catch (Exception ex)
            {
                var message = $"[Запуск процессов удаления баз завершился с ошибкой: {ex.Message}]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"{message}");
            }
        }

        /// <summary>
        /// Запустить процесс на архивацию
        /// информационной базы в склеп
        /// </summary>
        /// <param name="processParams">Модель параметров для запуска процесса на архивацию
        /// информационной базы в склеп</param>
        public ManagerResult StartProcessOfArchiveDatabaseToTomb(
            StartProcessOfArchiveDatabaseToTombParamsDto processParams)
        {
            var accountDatabase = accountDatabaseDataHelper.GetAccountDatabase(processParams.DatabaseId);
            if (accountDatabase == null)
                return PreconditionFailed<bool>($"Не удалось найти инф. базу по Id {processParams.DatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_OfArchiveToTomb, () => accountDatabase.AccountId);
                startProcessOfArchiveDatabaseToTombProvider.StartProcessOfArchiveDatabaseToTomb(processParams);
                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $"[Ошибка архивации ИБ {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп.]");

                return PreconditionFailed<bool>(
                    $"Не удалось перенести базу {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} в склеп. Описание ошибки: {ex.Message}");
            }
        }

        /// <summary>
        /// Run processes for archiving info bases to the crypt
        /// </summary>
        /// <param name="databasesId">List of database ids</param>
        public ManagerResult StartProcessOfArchiveDatabasesToTomb(List<Guid> databasesId)
        {
            var accountId = GetAccountId(databasesId);

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabase_OfArchiveToTomb, () => accountId);
                startProcessOfArchiveDatabaseToTombProvider.StartProcessOfArchiveDatabasesToTomb(databasesId);
                return Ok();
            }
            catch (Exception ex)
            {
                var message = "[Запуск процессов архивации баз завершился с ошибкой]";
                _handlerException.Handle(ex, message);
                return PreconditionFailed($"{message}. {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процессы на удаление доступов из инф. базы
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public ManagerResult<List<ManageAccessResultDto>> StartProcessesOfRemoveAccesses(
            StartProcessesOfManageAccessesToDbDto model)
        {
            
            if (IsSourceDatabase(model.DatabaseId))
            {
                string message = "База является материнской";
                Logger.Warn(message);
                return PreconditionFailed<List<ManageAccessResultDto>>(message);
            }
            
            if (!accountDatabaseProvider.TryGetAccountDatabaseById(model.DatabaseId, out var accountDatabase))
                return PreconditionFailed<List<ManageAccessResultDto>>($"Удаление доступов не возможно. Не найдена база по Id {model.DatabaseId}");

            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_RemoveUserAccess,
                    () => AccountIdByAccountDatabase(model.DatabaseId));

                var results = startProcessOfRemoveAccessesFromDatabaseProvider.StartProcessesOfRemoveAccesses(model)
                    .ToList();

                return Ok(results);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Запуск процессов на удаление доступов для информационной базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} завершился с ошибкой";

                _handlerException.Handle(ex, message);
                return PreconditionFailed<List<ManageAccessResultDto>>($"{message}. {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процессы на удаление доступов из карточки пользователей
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public ManagerResult<List<ManageAccessResultDto>> StartProcessesOfRemoveAccessesForUserCard(
            StartProcessesAccessesToDbForUserCardDto model)
        {
            try
            {
                var results = startProcessOfRemoveAccessesFromDatabaseProvider.StartProcessesOfRemoveAccessesForUserCard(model)
                    .ToList();

                return Ok(results);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Запуск процессов на удаление доступов из карточки пользователей] {model.UsersId} завершился с ошибкой";

                _handlerException.Handle(ex, message);
                return PreconditionFailed<List<ManageAccessResultDto>>($"{message}. {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процессы на выдачу внутренних доступов в инф. базу
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public ManagerResult<List<ManageAccessResultDto>> StartProcessesOfGrandInternalAccesses(
            StartProcessesOfManageAccessesToDbDto model)
        {
            if (IsSourceDatabase(model.DatabaseId))
            {
                string message = "База является материнской";
                Logger.Warn(message);
                return PreconditionFailed<List<ManageAccessResultDto>>(message);
            }
            
            if (!accountDatabaseProvider.TryGetAccountDatabaseById(model.DatabaseId, out var accountDatabase))
                return PreconditionFailed<List<ManageAccessResultDto>>($"Выдача доступов не возможна. Не найдена база по Id {model.DatabaseId}");
            
            try
            {
                AccessProvider.HasAccess(ObjectAction.AccountDatabases_GrantAccessToUser,
                    () => AccountIdByAccountDatabase(model.DatabaseId));

                var results = startProcessOfGrandAccessesToDatabaseProvider
                    .StartProcessesOfGrandInternalAccesses(model)
                    .ToList();

                return Ok(results);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Запуск процессов на выдачу внутренних доступов для информационной базы] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} завершился с ошибкой";

                _handlerException.Handle(ex, message);
                return PreconditionFailed<List<ManageAccessResultDto>>($"{message}. {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процессы на доступов в инф. базу из карточки пользователя
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по управлению доступами в инф. базе</param>
        /// <returns>Коллекцию результатов по управлению доступами</returns>
        public ManagerResult<List<ManageAccessResultDto>> StartProcessesOfAccessesforUserCard(
            StartProcessesAccessesToDbForUserCardDto model)
        {
            try
            {
                var results = startProcessOfGrandAccessesToDatabaseProvider
                    .StartProcessesOfAccessesForUserCard(model)
                    .ToList();

                return Ok(results);
            }
            catch (Exception ex)
            {
                var message =
                    $"[Запуск процессов на выдачу доступов из карточки пользователя] {model.UsersId} завершился с ошибкой";

                _handlerException.Handle(ex, message);
                return PreconditionFailed<List<ManageAccessResultDto>>($"{message}. {ex.Message}");
            }
        }

        /// <summary>
        /// Запустить процесс по завершению
        /// сеансов в информационной базе
        /// </summary>
        /// <param name="model">Модель для запуска процесса
        /// по завершению сеансов в информационной базе</param>
        /// <returns>Результат запуска процесса</returns>
        public ManagerResult StartProcessOfTerminateSessionsInDatabase(
            StartProcessOfTerminateSessionsInDatabaseDto model)
        {

            if (!accountDatabaseProvider.TryGetAccountDatabaseById(model.DatabaseId, out var accountDatabase))
                return PreconditionFailed(
                    $"Запуск процесса по завершению сеансов в базе не возможна. Не найдена база по Id {model.DatabaseId}");

            try
            {
                var accountId = GetAccountIdByDatabase(model.DatabaseId);

                AccessProvider.HasAccess(ObjectAction.AccountDatabase_TerminateSessions,
                    () => accountId);

                if (!startProcessOfTerminateSessionsInDatabaseProvider.TryStartProcess(model, out var errorMessage))
                    return PreconditionFailed(errorMessage);

                LogEvent(() => accountId, LogActions.TerminateSessionsInDatabase, 
                    $"Завершение сеансов для базы {accountDatabase.V82Name}", AccessProvider.GetUser()?.Id ?? AccessProvider.ContextAccountId);

                return Ok();
            }
            catch (Exception ex)
            {
                _handlerException.Handle(ex,
                    $@"[Запуск процесса по завершению сеансов в информационной 
                                базе] {AccountDatabase1CInfoBaseHelper.GetDatabase1CFullName(accountDatabase)} выполнен с ошибкой");

                return PreconditionFailed(ex.Message);
            }
        }

        /// <summary>
        /// Получить Id аккаунта, исходя из списка Id инф. баз
        /// </summary>
        /// <param name="databasesId">Список Id ифн. баз</param>
        /// <returns>Id аккаунта</returns>
        private Guid GetAccountId(List<Guid> databasesId) =>
            dbLayer.DatabasesRepository.AsQueryableNoTracking().FirstOrDefault(x => x.Id == databasesId.FirstOrDefault())?.AccountId ?? AccessProvider.ContextAccountId;
        
        
        private bool IsSourceDatabase(Guid dbId) => 
            DbLayer.DelimiterSourceAccountDatabaseRepository.AsQueryableNoTracking().Any(ds => ds.AccountDatabaseId == dbId);
        
    }
}
