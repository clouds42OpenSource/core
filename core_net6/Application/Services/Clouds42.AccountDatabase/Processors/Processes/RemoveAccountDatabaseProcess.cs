﻿using Clouds42.AccountDatabase.Contracts.Interfaces;
using Clouds42.AccountDatabase.Contracts.Processors.Processes.Interfaces;
using Clouds42.AccountDatabase.DataHelpers;
using Clouds42.AccountDatabase.Helpers;
using Clouds42.AccountUsers.Contracts.ServiceManagerConnector.Interfaces;
using Clouds42.Common.Extensions;
using Clouds42.CoreWorker.JobWrappers.DeleteAccountDatabase;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseArchive;
using Clouds42.DataContracts.AccountDatabase.AccountDatabaseItems;
using Clouds42.DataContracts.AccountDatabase.DeleteAccountDatabase;
using Clouds42.Domain.Enums;
using Clouds42.Domain.Enums.DataBases;
using Clouds42.Logger;

namespace Clouds42.AccountDatabase.Processors.Processes
{
    /// <summary>
    /// Процесс удаления информационной базы
    /// </summary>
    internal class RemoveAccountDatabaseProcess(
        DatabaseStatusHelper databaseStatusHelper,
        AccountDatabaseDataHelper accountDatabaseDataHelper,
        IFetchConnectionsBridgeApiProvider fetchConnectionsBridgeApiProvider,
        SynchronizeAccountDatabaseWithTardisHelper synchronizeAccountDatabaseWithTardisHelper,
        IDeleteAccountDatabaseJobWrapper deleteAccountDatabaseJobWrapper,
        IDeleteDelimiterCommand deleteDelimeterCommand,
        ILogger42 logger,
        AccountDatabasePathHelper accountDatabasesPathHelper)
        : IRemoveAccountDatabaseProcess
    {
        /// <summary>
        /// Выполнить процесс по удалению инф. базы
        /// </summary>
        /// <param name="processParams">Модель параметров процесса удаления инф. базы</param>
        public void ExecuteProcessOfRemoveDatabase(ProcessOfRemoveDatabaseParamsDto processParams)
        {
            var database = accountDatabaseDataHelper.GetAccountDatabaseOrThrowException(processParams.DatabaseId);

            if (database.StateEnum == processParams.FinalDeletionDatabaseStatus)
                return;

            logger.Info($"[{processParams.DatabaseId}] :: Информационная база {database.V82Name} поставлена в очередь на удаление");

            fetchConnectionsBridgeApiProvider.ExecuteInBackgroundThread().DeleteDatabaseHandle(processParams.DatabaseId);
            synchronizeAccountDatabaseWithTardisHelper.SynchronizeAccountDatabaseViaNewThread(processParams.DatabaseId);

            if (database.IsDelimiter())
            {
                DeleteAccountDatabaseOnDelimiters(database, processParams);
                return;
            }

            if (database.StateEnum == DatabaseState.ErrorCreate)
            {
                if (database.IsFile == true)
                {
                    var filePath = accountDatabasesPathHelper.GetPath(database);
                    if(filePath.ToDirectoryInfo().GetSize() != 0)
                    {
                        CreateTaskForDeleteAccountDatabase(processParams);
                        SetFinalDbStatus(database, processParams.FinalDeletionDatabaseStatus);
                        return;
                    }

                }

                SetFinalDbStatus(database, DatabaseState.DeletedToTomb);
                return;
            }
            CreateTaskForDeleteAccountDatabase(processParams);
            SetFinalDbStatus(database, processParams.FinalDeletionDatabaseStatus);
        }

        /// <summary>
        /// Создать задачу на удаление инф. базы
        /// </summary>
        /// <param name="processParams">Модель параметров процесса удаления инф. базы</param>
        private void CreateTaskForDeleteAccountDatabase(ProcessOfRemoveDatabaseParamsDto processParams)
        {
            if (processParams.FinalDeletionDatabaseStatus != DatabaseState.DeletedToTomb)
                return;

            deleteAccountDatabaseJobWrapper.Start(new DeleteAccountDatabaseParamsDto
            {
                AccountDatabaseId = processParams.DatabaseId,
                InitiatorId = processParams.InitiatorId
            });
        }

        /// <summary>
        /// Установить финальный статус для инф. базы (если это необходимо)
        /// </summary>
        /// <param name="database">Информационная база</param>
        /// <param name="finalStatus">Финальный статус</param>
        private void SetFinalDbStatus(Domain.DataModels.AccountDatabase database, DatabaseState finalStatus)
        {
            if (finalStatus != DatabaseState.DeletedFromCloud )
                return;
            if (database.PublishStateEnum != PublishState.Unpublished && !database.IsDelimiter())
                finalStatus = DatabaseState.DelitingToTomb;

            databaseStatusHelper.SetDbStatus(database.Id, finalStatus);
        }

        /// <summary>
        /// Удалить инф. базу на разделителях
        /// </summary>
        /// <param name="accountDatabase">Инф. база</param>
        /// <param name="processParams">Модель параметров процесса удаления инф. базы</param>
        private void DeleteAccountDatabaseOnDelimiters(Domain.DataModels.AccountDatabase accountDatabase,
            ProcessOfRemoveDatabaseParamsDto processParams)
        {
            if (accountDatabase.AccountDatabaseOnDelimiter.Zone == null)
            {
                SetFinalDbStatus(accountDatabase, DatabaseState.DeletedFromCloud);
                return;
            }
            var locale = accountDatabase.Account.AccountConfiguration.Locale.Name;
            logger.Info($"[{accountDatabase.Id}] :: Запрос на удаление базы на разделителях отправлен в МС.");
            
            var result = deleteDelimeterCommand.Execute(accountDatabase.Id, locale);
            
            logger.Info($"[{accountDatabase.Id}] :: Запрос на удаление базы на разделителях успешно отправлен в МС.");
           
            processParams.FinalDeletionDatabaseStatus = DatabaseState.Unknown;
                databaseStatusHelper.SetDbStatus(accountDatabase.Id, DatabaseState.DeletedToTomb, result);

        }
    }
}
